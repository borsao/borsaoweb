package borsao.e_borsao.server.Login;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.MapeoUsuarios;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.server.consultaUsuariosServer;

public class LoginServer
{
	private connectionManager conManager = null;
	private Connection con = null;	
	public static String nombre = null;
	
	public LoginServer()
	{
		this.conManager = new connectionManager();		
	}
	
	public MapeoUsuarios comprobarUsuario(String r_usuario, String r_password)
	{
		MapeoUsuarios mapeo = null;
		
		consultaUsuariosServer cus = new consultaUsuariosServer(null);
		mapeo=cus.comprobarUsuario(r_usuario, r_password);
		return mapeo;
	}
	
	public boolean comprobarUsuarioAlerta(String r_usuario)
	{
		Statement csl = null;
		ResultSet rsl = null;

		StringBuffer sql = null;
		try
		{
			sql = new StringBuffer();
			sql.append(" select count(*) as semaforos ");
			sql.append(" from glb_user_alert ");
			sql.append(" where usuario='" + r_usuario + "' ");
			
			if (con==null)
			{
				con= this.conManager.establecerConexion();
			}
			csl = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsl = csl.executeQuery(sql.toString());
			while(rsl.next())
			{
				if (rsl.getString("semaforos")!=null && rsl.getInt("semaforos")>0)
				{
					return true;
				}
			}
		}
		catch (Exception ex)
		{
		}
		finally
		{
			try
			{
				if (rsl!=null)
				{
					rsl.close();
				}
				if (csl!=null)
				{
					csl.close();
				}
			}
			catch (Exception ex)
			{
			}
		}
		return false;
	}
}
