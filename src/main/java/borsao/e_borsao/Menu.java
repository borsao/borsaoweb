package borsao.e_borsao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Imagen;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.CalendarioView;

/**
 * Responsive navigation menu presenting a list of available views to the user.
 */
public class Menu extends CssLayout {

	
    private static final String VALO_MENUITEMS = "valo-menuitems";
    private static final String VALO_MENU_TOGGLE = "valo-menu-toggle";
    private static final String VALO_MENU_VISIBLE = "valo-menu-visible";
    private Navigator navigator;
    private Map<String, Button> viewButtons = new HashMap<String, Button>();

    private CssLayout menuItemsLayout;
    private CssLayout menuItemsVentas;
    private CssLayout menuItemsFinanciero;
    private CssLayout menuItemsCalidad;
    private CssLayout menuItemsLaboratorio;
    private CssLayout menuItemsProduccion;
    private CssLayout menuItemsInformatica;
    private CssLayout menuItemsCompras;
    private CssLayout menuItemsAlmacen;
    private CssLayout menuItemsPruebas;
    private CssLayout menuItemsPlanificacion;
    private CssLayout menuItemsGerencia;
    
    private Button lblUser = null;
    public static CssLayout menuPart;
    public static Imagen img;

    public Menu(Navigator navigator) {
        this.navigator = navigator;
        setPrimaryStyleName(ValoTheme.MENU_ROOT);
        menuPart = new CssLayout();
        menuPart.addStyleName(ValoTheme.MENU_PART);

        // header of the menu
        img = new Imagen(null, new ThemeResource("img/" + eBorsao.strLogoEmpresa));        
        
        final VerticalLayout top = new VerticalLayout();
        top.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);
        top.addStyleName(ValoTheme.MENU_TITLE);
//        top.addStyleName("panelLogo");
        top.setSizeUndefined();		
        if (eBorsao.get().accessControl.getDispositivo().equals(""))
        {
        	if (eBorsao.strEstiloLogo.equals("cuadrado"))
        	{
        		img.setWidth("175px");
        		img.setHeight("150px");
        	}
        	else
        	{
        		img.setWidth("200px");
        		img.setHeight("65px");        		
        	}
        	top.addComponent(img);
        	
        	Label title = new Label("<p>INTRANET</p> ", ContentMode.HTML);        
            title.addStyleName(ValoTheme.LABEL_H3);
            title.setSizeUndefined();
            top.addComponent(title);
            
            lblUser = new Button();
            lblUser.setCaption(eBorsao.get().accessControl.getNombre());
            if (eBorsao.get().accessControl.getUsuarioCuentaCorreo()!=null && eBorsao.get().accessControl.getUsuarioCuentaCorreo().length()>0)
        	{
            	lblUser.setIcon(FontAwesome.CALENDAR);
        	}
            lblUser.addStyleName(ValoTheme.BUTTON_BORDERLESS);
            lblUser.setSizeUndefined();
            top.addComponent(lblUser);
        }
        else
        {
        	Label title = new Label("INTRANET - " + eBorsao.get().accessControl.getNombre(), ContentMode.HTML);        
        	if (Page.getCurrent().getWebBrowser().getScreenWidth()>Page.getCurrent().getWebBrowser().getScreenHeight())
        	{
        		title.addStyleName(ValoTheme.LABEL_SMALL);
        	}
        	else
        	{
        		title.addStyleName(ValoTheme.LABEL_H3);
        	}
            title.setSizeUndefined();
            top.addComponent(title);
        }
        
        menuPart.addComponent(top);

        // logout menu item
        MenuBar menu = new MenuBar();
        menu.addItem("Logout", FontAwesome.SIGN_OUT, new Command() {

            @Override
            public void menuSelected(MenuItem selectedItem) {
                VaadinSession.getCurrent().getSession().invalidate();
                Page.getCurrent().reload();
                System.out.println("Usuario desconectado: " + eBorsao.get().accessControl.getNombre() + " " + new Date());
            }
        });

        
        menu.addStyleName("user-menu");
        menuPart.addComponent(menu);

        // button for toggling the visibility of the menu when on a small screen
        final Button showMenu = new Button("Menu", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                if (menuPart.getStyleName().contains(VALO_MENU_VISIBLE)) {
                    menuPart.removeStyleName(VALO_MENU_VISIBLE);                    
                    img.removeStyleName(VALO_MENU_VISIBLE);
                } else {
                    menuPart.addStyleName(VALO_MENU_VISIBLE);
                    img.addStyleName(VALO_MENU_VISIBLE);
                }
            }
        });
        showMenu.addStyleName(ValoTheme.BUTTON_PRIMARY);
        showMenu.addStyleName(ValoTheme.BUTTON_SMALL);
        showMenu.addStyleName(VALO_MENU_TOGGLE);
        showMenu.setIcon(FontAwesome.NAVICON);
        menuPart.addComponent(showMenu);

        // container for the navigation buttons, which are added by addView()
        menuItemsLayout = new CssLayout();
        menuItemsLayout.setPrimaryStyleName(VALO_MENUITEMS);
        menuItemsLayout.addStyleName(VALO_MENU_VISIBLE);
        menuItemsLayout.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsLayout);

        menuItemsAlmacen = new CssLayout();
        menuItemsAlmacen.addStyleName(VALO_MENU_VISIBLE);
        menuItemsAlmacen.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsAlmacen);

        menuItemsCalidad= new CssLayout();
        menuItemsCalidad.addStyleName(VALO_MENU_VISIBLE);
        menuItemsCalidad.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsCalidad);

        menuItemsVentas = new CssLayout();
        menuItemsVentas.addStyleName(VALO_MENU_VISIBLE);
        menuItemsVentas.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsVentas);
        
        menuItemsCompras = new CssLayout();
        menuItemsCompras.addStyleName(VALO_MENU_VISIBLE);
        menuItemsCompras.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsCompras);

        menuItemsFinanciero = new CssLayout();
        menuItemsFinanciero.addStyleName(VALO_MENU_VISIBLE);
        menuItemsFinanciero.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsFinanciero);
        
        menuItemsLaboratorio = new CssLayout();
        menuItemsLaboratorio.addStyleName(VALO_MENU_VISIBLE);
        menuItemsLaboratorio.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsLaboratorio);

        menuItemsProduccion = new CssLayout();
        menuItemsProduccion.addStyleName(VALO_MENU_VISIBLE);
        menuItemsProduccion.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsProduccion);

        menuItemsInformatica = new CssLayout();
        menuItemsInformatica.addStyleName(VALO_MENU_VISIBLE);
        menuItemsInformatica.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsInformatica);
        
        menuItemsPruebas = new CssLayout();
        menuItemsPruebas.addStyleName(VALO_MENU_VISIBLE);
        menuItemsPruebas.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsPruebas);

        menuItemsPlanificacion = new CssLayout();
        menuItemsPlanificacion.addStyleName(VALO_MENU_VISIBLE);
        menuItemsPlanificacion.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsPlanificacion);

        menuItemsGerencia = new CssLayout();
        menuItemsGerencia.addStyleName(VALO_MENU_VISIBLE);
        menuItemsGerencia.addStyleName(ValoTheme.MENU_SUBTITLE);
        menuPart.addComponent(menuItemsGerencia);

        addComponent(menuPart);
        this.setResponsive(true);
        this.cargarListeners();
        
    }

    private void cargarListeners()
    {
    	
    	if (eBorsao.get().accessControl.getDispositivo().equals("") && eBorsao.get().accessControl.getUsuarioCuentaCorreo()!=null && eBorsao.get().accessControl.getUsuarioCuentaCorreo().length()>0)
    	{
	        this.lblUser.addClickListener((Button.ClickEvent event) -> {
	        	verMenu(false);
				navigator.navigateTo(CalendarioView.VIEW_NAME);
	        });
    	}
    }
    /**
     * Register a pre-created view instance in the navigation menu and in the
     * {@link Navigator}.
     *
     * @see Navigator#addView(String, View)
     *
     * @param view
     *            view instance to register
     * @param name
     *            view name
     * @param caption
     *            view caption in the menu
     * @param icon
     *            view icon in the menu
     */
    public void addView(View view, final String name, String caption,
            Resource icon,String r_area,boolean r_visible, serverBasico r_semaforo) {
        navigator.addView(name, view);
        createViewButton(name, caption, icon, r_area, r_visible, r_semaforo);
    }

    public void addView(String caption, String r_area) {        
        createViewLabel(caption, r_area);
    }

    /**
     * Register a view in the navigation menu and in the {@link Navigator} based
     * on a view class.
     *
     * @see Navigator#addView(String, Class)
     *
     * @param viewClass
     *            class of the views to create
     * @param name
     *            view name
     * @param caption
     *            view caption in the menu
     * @param icon
     *            view icon in the menu
     */
    public void addView(Class<? extends View> viewClass, final String name,
            String caption, Resource icon,String r_area, boolean r_visible, serverBasico r_semaforo) {
        navigator.addView(name, viewClass);
        createViewButton(name, caption, icon, r_area, r_visible, r_semaforo);
    }

    private void createViewButton(final String name, String caption,
            Resource icon, String r_area, boolean r_visible, serverBasico r_semaforo) {
        Button button = new Button(caption, new ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	verMenu(caption.equals("Home")||caption.equals("About")||caption.equals(""));
                navigator.navigateTo(name);

            }
        });
        button.setPrimaryStyleName(ValoTheme.MENU_ITEM);
//        if (CurrentUser.getPantalla().equals(DashboardLightsView.VIEW_NAME)) r_semaforo=null;
//        if (r_semaforo!=null)
//        {
//	        String error = ((serverBasico) r_semaforo).semaforo();
//	        if (error.equals("2"))
//	        {
//	        	button.setStyleName("valo-menu-item-v-icon-red");
//	        }
//	        else if (error.equals("1"))
//	        {
//	        	button.setStyleName("valo-menu-item-v-icon-yellow");
//	        }
//        }
        button.setIcon(icon);
        button.setVisible(r_visible);
        
        CssLayout menu = obtenerMenu(r_area);
        menu.addComponent(button);
        viewButtons.put(name, button);
        r_semaforo=null;
    }

    private void createViewLabel(String caption,String r_area) 
    {
    	 Button button = new Button(caption, new ClickListener() {

             @Override
             public void buttonClick(ClickEvent event) {
            	 CssLayout menu = obtenerMenu(r_area);
            	 for (int i=0;i<=menu.getComponentCount()-1;i++)
            	 {
            		 if (menu.getComponent(i).getIcon()!=null) menu.getComponent(i).setVisible(!menu.getComponent(i).isVisible());
            	 }
            	 ArrayList<CssLayout> listaMenus = obtenerRestoMenu(r_area);
            	 for (int i=0;i<=listaMenus.size()-1;i++)
            	 {
                	 for (int j=0;j<=((CssLayout) listaMenus.get(i)).getComponentCount()-1;j++)
                	 {
                		 if (((CssLayout) listaMenus.get(i)).getComponent(j).getIcon()!=null) ((CssLayout) listaMenus.get(i)).getComponent(j).setVisible(false);
                	 }
            		 
            	 }
             }
         });
        button.setPrimaryStyleName(ValoTheme.MENU_SUBTITLE);
        CssLayout menu = obtenerMenu(r_area);
        menu.addComponent(button);
    }

    /**
     * Highlights a view navigation button as the currently active view in the
     * menu. This method does not perform the actual navigation.
     *
     * @param viewName
     *            the name of the view to show as active
     */
    public void setActiveView(String viewName) {
        for (Button button : viewButtons.values()) {
            button.removeStyleName("selected");
        }
        Button selected = viewButtons.get(viewName);
        if (selected != null) {
            selected.addStyleName("selected");
            menuPart.removeStyleName(VALO_MENU_VISIBLE);
        }
        else
        	menuPart.addStyleName(VALO_MENU_VISIBLE);
    }
    
    private CssLayout obtenerMenu(String r_area)
    {
    	CssLayout menu = null;
        switch (r_area)
        {
        	case "V":
        	{
        		menu=menuItemsVentas;
        		break;
        	}
        	case "C":
        	{
        		menu=menuItemsCompras;
        		break;
        	}
        	case "F":
        	{
        		menu=menuItemsFinanciero;
        		break;
        	}
        	case "A":
        	{
        		menu=menuItemsAlmacen;
        		break;
        	}
        	case "Q":
        	{
        		menu=menuItemsCalidad;
        		break;
        	}
        	case "L":
        	{
        		menu= menuItemsLaboratorio;
        		break;
        	}
        	case "G":
        	{
        		menu=menuItemsLayout;
        		break;
        	}
        	case "P":
        	{
        		menu=menuItemsProduccion;
        		break;
        	}
        	case "I":
        	{
        		menu=menuItemsInformatica;
        		break;
        	}
        	case "T":
        	{
        		menu=menuItemsPruebas;
        		break;
        	}
        	case "R":
        	{
        		menu=menuItemsPlanificacion;
        		break;
        	}
        	case "D":
        	{
        		menu=menuItemsGerencia;
        		break;
        	}
        }
        return menu;
    }
    private ArrayList<CssLayout> obtenerRestoMenu(String r_area)
    {
    	ArrayList<CssLayout> listaMenus = new ArrayList<CssLayout>();
    	
    	CssLayout menu = null;
        switch (r_area)
        {
        	case "V":
        	{
//        		menu=menuItemsVentas;
//        		listaMenus.add(menu);
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);
        		break;
        	}
        	case "A":
        	{
        		menu= menuItemsCompras;
        		listaMenus.add(menu);       
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
//        		menu= menuItemsAlmacen;
//        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;        		
        		listaMenus.add(menu);
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}
        	case "Q":
        	{
        		menu= menuItemsCompras;
        		listaMenus.add(menu);     
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);
//        		menu= menuItemsCalidad;
//        		listaMenus.add(menu); 
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;        		
        		listaMenus.add(menu);
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}        	
        	case "C":
        	{
//        		menu= menuItemsCompras;
//        		listaMenus.add(menu);        	
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;        		
        		listaMenus.add(menu);
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);
        		
        		break;
        	}
        	case "L":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);      
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
//        		menu= menuItemsLaboratorio;
//        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}
        	case "G":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        	
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsLaboratorio;        		
        		listaMenus.add(menu);
//        		menu=menuItemsLayout;
//        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}
        	case "P":
        	{
//        		menu= menuItemsProduccion;
//        		listaMenus.add(menu);
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}
        	case "T":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

//        		menu=menuItemsPruebas;
//        		listaMenus.add(menu);
        		break;
        	}
        	case "F":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
//        		menu= menuItemsFinanciero;
//        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}  
        	case "I":
        	{
//        		menu= menuItemsInformatica;
//        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}
        	case "R":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
//        		menu=menuItemsPlanificacion;
//        		listaMenus.add(menu);
        		menu=menuItemsGerencia;
        		listaMenus.add(menu);

        		break;
        	}          	
        	case "D":
        	{
        		menu= menuItemsInformatica;
        		listaMenus.add(menu);        		        		
        		menu= menuItemsProduccion;
        		listaMenus.add(menu);
        		menu= menuItemsFinanciero;
        		listaMenus.add(menu);        		
        		menu=menuItemsVentas;
        		listaMenus.add(menu);
        		menu= menuItemsAlmacen;
        		listaMenus.add(menu);        
        		menu= menuItemsCalidad;
        		listaMenus.add(menu); 
        		menu= menuItemsCompras;
        		listaMenus.add(menu);        		
        		menu= menuItemsLaboratorio;
        		listaMenus.add(menu);
        		menu=menuItemsLayout;
        		listaMenus.add(menu);
        		menu=menuItemsPruebas;
        		listaMenus.add(menu);
        		menu=menuItemsPlanificacion;
        		listaMenus.add(menu);
//        		menu=menuItemsGerencia;
//        		listaMenus.add(menu);
        		
        		break;
        	}          	
        }
        return listaMenus;
    }

    public void verMenu(boolean r_ver)
    {
		if (r_ver)
			menuPart.addStyleName(VALO_MENU_VISIBLE);
		else
			menuPart.removeStyleName(VALO_MENU_VISIBLE);
		
		menuPart.setVisible(r_ver);
		img.setVisible(r_ver);
    }
}

