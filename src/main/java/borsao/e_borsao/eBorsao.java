package borsao.e_borsao;
//
 //import javax.servlet.annotation.WebServlet; 
//import javax.servlet.http.HttpServletRequest;
//
//import com.vaadin.annotations.Theme;
//import com.vaadin.annotations.VaadinServletConfiguration;
//import com.vaadin.annotations.Viewport;
//import com.vaadin.navigator.Navigator;
//import com.vaadin.navigator.ViewChangeListener;
//import com.vaadin.server.Page;
//import com.vaadin.server.Responsive;
//import com.vaadin.server.VaadinRequest;
//import com.vaadin.server.VaadinServlet;
//import com.vaadin.server.VaadinServletRequest;
//import com.vaadin.server.VaadinSession;
//import com.vaadin.ui.UI;
//
//import borsao.e_borsao.Login.LoginView;
//import borsao.e_borsao.ventas.consultaVentas;
//
///**
// * This UI is the application entry point. A UI may either represent a browser window 
// * (or tab) or some part of a html page where a Vaadin application is embedded.
// * <p>
// * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
// * overridden to add component to the user interface and initialize non-component functionality.
// */
//@Viewport("user-scalable=no,initial-scale=1.0")
//@Theme("mytheme")
//public class eBorsao extends UI {
//
//    public String dispositivo=null;
//
//    @Override    
//    protected void init(VaadinRequest vaadinRequest) {
//        Responsive.makeResponsive(this);
//    	Page.getCurrent().setTitle("Bodegas Borsao, S.A.");
//
//		if (vaadinRequest instanceof VaadinServletRequest) {
//		    HttpServletRequest httpRequest = ((VaadinServletRequest)vaadinRequest).getHttpServletRequest();
//		    dispositivo = httpRequest.getHeader("User-Agent").toLowerCase();
//		}
//    	VaadinSession.getCurrent().getSession().setMaxInactiveInterval(3600);
//    	//
//        // Create a new instance of the navigator. The navigator will attach
//        // itself automatically to this view.
//        //
//        new Navigator(this, this);
//
//        //
//        // The initial log view where the user can login to the application
//        //
//        getNavigator().addView(LoginView.NAME, LoginView.class);//
//
//        //
//        // Add the main view of the application
//        //
//        getNavigator().addView(consultaVentas.NAME, consultaVentas.class);
//        //
//        // We use a view change handler to ensure the user is always redirected
//        // to the login view if the user is not logged in.
//        //
//        getNavigator().addViewChangeListener(new ViewChangeListener() {
//
//            @Override
//            public boolean beforeViewChange(ViewChangeEvent event) {
//
//                // Check if a user has logged in
//                boolean isLoggedIn = getSession().getAttribute("user") != null;
//                boolean isLoginView = event.getNewView() instanceof LoginView;
//
//                if (!isLoggedIn && !isLoginView) {
//                    // Redirect to login view always if a user has not yet
//                    // logged in
//                    getNavigator().navigateTo(LoginView.NAME);
//                    return false;
//
//                } else if (isLoggedIn && isLoginView) {
//                    // If someone tries to access to login view while logged in,
//                    // then cancel
//                    return false;
//                }
//
//                return true;
//            }
//
//            @Override
//            public void afterViewChange(ViewChangeEvent event) {
//
//            }
//        });
//    }	
//	
//    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
//    @VaadinServletConfiguration(ui = eBorsao.class, productionMode = true)
//    public static class MyUIServlet extends VaadinServlet {
//    }
//}


import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.util.Date;
import java.util.Enumeration;

//package borsao.crud.crud;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ui.Transport;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.trabajos.lanzador;
import borsao.e_borsao.modelo.Login.AccessControl;
import borsao.e_borsao.modelo.Login.BasicAccessControl;
import borsao.e_borsao.views.MainScreen;
import borsao.e_borsao.views.Login.LoginScreen;
import borsao.e_borsao.views.Login.LoginScreen.LoginListener;
import borsao.e_borsao.views.error.MiErrorView;

/**
 * Main UI class of the application that shows either the login screen or the
 * main view of the application depending on whether a user is signed in.
 *
 * The @Viewport annotation configures the viewport meta tags appropriately on
 * mobile devices. Instead of device based scaling (default), using responsive
 * layouts.
 */

// color web am d93629

@Viewport("user-scalable=no,initial-scale=1.0")
@Theme("mytheme")
//@Widgetset("borsao.crud.crud.MyAppWidgetset")
@Push(transport=Transport.LONG_POLLING)
//@Push
public class eBorsao extends UI {

    public AccessControl accessControl = new BasicAccessControl();
    public LecturaProperties prop = null;
    public MainScreen mainScreen = null;
    public static boolean superUsuario = false; 
    
    public static final String strEmpresaAlias = "Borsao";
    public static final String strEmpresa = "BODEGAS BORSAO, S. A.";
    public static final String strLogoEmpresa = "borlogo.png";
    public static final String strLogoGEmpresa = "borlogo.png";
    public static final String strDominio = "bodegasborsao.com";
    public static final String strEstiloLogo = "cuadrado";
    public static final String strCIF = "A50889955";
    
//    public static final String strEmpresaAlias = "AM";
//    public static final String strEmpresa = "Bodegas Alto Moncayo, S. A.";
//    public static final String strLogoEmpresa = "amLogo.jpg";
//    public static final String strLogoGEmpresa = "amLogoG.png";
//    public static final String strDominio = "bodegasaltomoncayo.com";   
//    public static final String strEstiloLogo = "rectangular";
//	  public static final String strCIF = "A50933886";
    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	
    	getPage().setTitle("INTRANET " + strEmpresa);
        addStyleName(ValoTheme.UI_WITH_MENU);
        Responsive.makeResponsive(this);
        
        
    	setLocale(java.util.Locale.getDefault());
    	System.out.println(getLocale().toString());
    	VaadinSession.getCurrent().getSession().setMaxInactiveInterval(3600);
    		
    	System.out.println(UI.getCurrent().getPage().getLocation().getHost());
    	System.out.println(UI.getCurrent().getPage().getLocation().getPath());
    	System.out.println(UI.getCurrent().getPage().getLocation().getPort());
    	System.out.println(UI.getCurrent().getPage().getLocation().getQuery());
//    	this.leerCertificado(vaadinRequest);

    	try
    	{
    		boolean propiedades=LecturaProperties.cargarProperties();
    		
    		if (propiedades)
    		{
				if (LecturaProperties.cron.equals("S"))
				{
					lanzador lanzadorPRocesos = new lanzador();
					lanzadorPRocesos.arrancarProcesosCron();
				}
				
				if (!accessControl.isUserSignedIn()) {
					setContent(new LoginScreen(accessControl, new LoginListener() {
						@Override
						public void loginSuccessful() {
							if (eBorsao.get().accessControl.getDispositivo().equals(""))
			    			{
			    				showMainView();
			    			}
			    			else
			    			{
			    				prueba();
			    			}
						}
					}));
				}
				else 
				{
					showMainView();
				}
			}
    		else
    		{
    			showErrorView();
    		}
    	}
    	catch (IOException ioEx)
    	{
    		showErrorView();
    	}
    }

    protected void showMainView() {
    	this.addDetachListener(new DetachListener() {
    		@Override
    		public void detach(DetachEvent event) {
    			System.out.println("Usuario desconectado: " + new Date());
    		}
    	});
    	
    	this.prueba();
    	
        mainScreen = new MainScreen(eBorsao.this);
//        if (accessControl.isSinMenu())
//        {
//        	getNavigator().addView(ProgramacionView.VIEW_NAME, ProgramacionView.class);
//        	getNavigator().navigateTo(ProgramacionView.VIEW_NAME);
//        }
//        else
//        {
    	addStyleName(ValoTheme.UI_WITH_MENU);
    	mainScreen.menu.setActiveView(this.accessControl.getPantalla());
    	getNavigator().navigateTo(this.accessControl.getPantalla());
//        }
        setContent(mainScreen);
        
    }
    
    protected void showErrorView() {
//        MainScreen mainScreen = new MainScreen(eBorsao.this);
//        mainScreen.menu.setActiveView("Error");        
//        setContent(mainScreen);
        setContent(new MiErrorView());
        
    }
    
  private void prueba()
  {
//  	HttpURLConnection conn = (HttpsURLConnection) (new URL()).openConnection();
//  	conn.setRequestProperty("Content-Type", "application/json");
//  	conn.setDoOutput(true);
//  	conn.setDoInput(true);
//  	conn.setRequestMethod("GET");
  	try 
  	{
  		/*
  		 * prueba conexion con la tienda on line
  		 */
//  		Connection con = null;
//  		con = connectionManager.getInstance().establecerConexionSQLInd();
//  		Upload upload = new Upload("Capture the image", null);

  		/*
  		 * Carga y reproduccion video
  		 */
  		/*
  		FileResource fileResource = new FileResource(new File("/Users/user/Downloads/DBTI_1991_teaser_HD.mp4"));
        Video vd = new Video();
        vd.setAutoplay(true);
        vd.setSource(fileResource);
        this.addComponent(vd);
  		*/

  		
  		/*
  		 * Pruebas lectura switches
  		 */
//  		URL url = new URL("http://192.168.30.20/rest/v3/ports");
//
//  		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8")); 
//  	
//  	    for (String line; (line = reader.readLine()) != null;) 
//  	    {
//  	        System.out.println(line);
//  	    }
  	    
//  	    url = new URL("http://192.168.30.20/rest/v1/port-statistics/2");
//
//

//  	OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
//  	out.write(data);
//  	 
//  	BufferedReader reader = new BufferedReader ( new InputStreamReader(conn.getInputStream()));
//  	
//  	for (String line; (line = reader.readLine()) != null;) {
//  	        System.out.println(line);
//  	}
//  	reader.close();
//  	out.close();
  	}
  	catch (Exception ex)
  	{
  		Notificaciones.getInstance().mensajeError(ex.getMessage());
  	}
  	//Now add all the data to this object using accumulate.
  	 
  	}
  
    public static eBorsao get() {
        return (eBorsao) UI.getCurrent();
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true,loadOnStartup=1)
    @VaadinServletConfiguration(ui = eBorsao.class, productionMode = true) //closeIdleSessions = true
    public static class MyUIServlet extends VaadinServlet {
    }
    
    
    private void leerCertificado(VaadinRequest r_req)
    {
		try
		{
	        KeyStore p12 = KeyStore.getInstance("pkcs12");
	        p12.load(new FileInputStream("C:/Users/Javier/Documents/Borsao/GREENsys/AEAT SII/binSii/BORSAO FNMT 21.04.17.p12"), "FNMT2017".toCharArray());
	        Enumeration<String> e = p12.aliases();
	        while (e.hasMoreElements()) {
	            String alias = (String) e.nextElement();
	            X509Certificate c = (X509Certificate) p12.getCertificate(alias);
	            Principal subject = c.getSubjectDN();
	            String subjectArray[] = subject.toString().split(",");
	            for (String s : subjectArray) {
	                String[] str = s.trim().split("=");
	                String key = str[0];
	                String value = str[1];
	                System.out.println(key + " - " + value);
	            }
	        }
		}
		catch (Exception ex)
		{
			
		}
    }
//    package com.javacodegeeks.process;
//    
//    import java.io.IOException;
//    import java.io.InputStream;
//    import java.io.InputStreamReader;
//    import java.util.Scanner;
//     
//    public class ProcessBuilderMultipleCommandsExample {
//        public static void main(String[] args) throws InterruptedException,
//                IOException {
//            // multiple commands
//            // /C Carries out the command specified by string and then terminates
//            ProcessBuilder pb = new ProcessBuilder("cmd.exe",
//                    "/C dir & echo example of & echo working dir");
//            pb.directory(new File("src"));
//     
//            Process process = pb.start();
//            IOThreadHandler outputHandler = new IOThreadHandler(
//                    process.getInputStream());
//            outputHandler.start();
//            process.waitFor();
   
//            System.out.println(outputHandler.getOutput());
//        }
//     
//        private static class IOThreadHandler extends Thread {
//            private InputStream inputStream;
//            private StringBuilder output = new StringBuilder();
//     
//            IOThreadHandler(InputStream inputStream) {
//                this.inputStream = inputStream;
//            }
//     
//            public void run() {
//                Scanner br = null;
//                try {
//                    br = new Scanner(new InputStreamReader(inputStream));
//                    String line = null;
//                    while (br.hasNextLine()) {
//                        line = br.nextLine();
//                        output.append(line
//                                + System.getProperty("line.separator"));
//                    }
//                } finally {
//                    br.close();
//                }
//            }
//     
//            public StringBuilder getOutput() {
//                return output;
//            }
//        }
//    }
//    //"E:\Program Files\AnyDesk.exe" 592937547

}
