package borsao.e_borsao.views.inicial;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Imagen;


public class InicialView extends VerticalLayout implements View {

	
    public static final String VIEW_NAME = "";

    public InicialView() {
//        CustomLayout aboutContent = new CustomLayout("aboutview");
//        aboutContent.setStyleName("about-content");
//        you can add Vaadin components in predefined slots in the custom
//        layout
//        Resource res = new ThemeResource("../../../../../../jetty-6.1.21/webapps/root/imagenes/articulos/0102105.jpg");
    	Imagen image =null;

    	Resource res = new ThemeResource("img/" + eBorsao.strLogoGEmpresa +"");
        image = new Imagen(null, res);
        
        if (eBorsao.strEstiloLogo.equals("cuadrado"))
        {
        	image.setWidth("25%");
        	image.setHeight("75%");
        }
        else
        {
        	image.setWidth("50%");
        	image.setHeight("35%");        	
        }
        addComponent(image);

    	Label lblTitulo = new Label(FontAwesome.CLOUD.getHtml() + " INTRANET " + eBorsao.strEmpresa, ContentMode.HTML);
        lblTitulo.addStyleName("labelPanel");
        lblTitulo.setWidth("100%");
        lblTitulo.setHeight("50%");

        addComponent(lblTitulo);

        setSizeFull();
        setComponentAlignment(image, Alignment.TOP_CENTER);
        setComponentAlignment(lblTitulo, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	
    }

}
