package borsao.e_borsao.views.error;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.Version;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class MiErrorView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "Error";

    public MiErrorView() {
        CustomLayout errorContent = new CustomLayout("MiErrorView");
        errorContent.setStyleName("about-content");

        // you can add Vaadin components in predefined slots in the custom
        // layout
        errorContent.addComponent(
                new Label(FontAwesome.TIMES_CIRCLE.getHtml()
                        + " VAADIN "
                        + Version.getFullVersion(), ContentMode.HTML), "info");

        setSizeFull();
        setStyleName("about-view");
        addComponent(errorContent);
        setComponentAlignment(errorContent, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    }

}
