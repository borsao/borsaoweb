package borsao.e_borsao.views;

import java.util.ArrayList;
import java.util.Date;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

import borsao.e_borsao.Menu;
import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.view.CargaInventarioSGAView;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.view.EtiquetasCorteInglesView;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.view.EtiquetasPrepedidosView;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.view.EtiquetasUbicacionView;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.view.consultaInventarioSGAGsysView;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.consultaInventarioView;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view.consultaInventarioSGAView;
import borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.view.CosteMaquinaView;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.view.CostePersonalView;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.view.DistribucionPersonalView;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.InformeTiemposView;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.NoConformidadesView;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.mermasPtView;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.view.ParametrosCalidadView;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.controlesCalidadProcesoView;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.view.EtiquetasRecepcionView;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.PedidosComprasView;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.server.consultaArticulosIntranetServer;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.view.ArticulosIntranetView;
import borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.view.FacturasVentasNacionalView;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.server.consultaFacturasVentasServer;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.view.PreparacionPedidosView;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.PedidosVentasView;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.consultaVentasLoteView;
import borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.view.OpcionesPTPNView;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.view.VentasView;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.view.consultaStockExternosView;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.view.OpcionesUSAView;
import borsao.e_borsao.Modulos.CRM.vistas.view.VistasView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.RecepcionEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.SeguimientoEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.view.PresentadoresView;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.view.AlbaranesRCAView;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.view.CalidadesUvaView;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.view.EquivalenciasRCAView;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.view.ParametrosRCAView;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.sincronizadorRCA.view.SincronizadorRCAView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.view.cambioArticulosView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.view.CodigosNCView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.view.DiferenciasMenosView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.view.DocumentosIdentificativosView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view.MovimientosSilicieView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.view.RegimenesFiscalesView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.view.TipoJustificantesView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.view.TipoOperacionesView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.view.TiposMovimientoView;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view.ArticulosNuevosView;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view.NotasArticulosView;
import borsao.e_borsao.Modulos.GENERALES.Avisos.modelo.MapeoAvisos;
import borsao.e_borsao.Modulos.GENERALES.Avisos.server.consultaAvisosServer;
import borsao.e_borsao.Modulos.GENERALES.Avisos.view.AvisosView;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.CalendarioView;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.VacacionesView;
import borsao.e_borsao.Modulos.GENERALES.Contadores.view.ContadoresView;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.view.ContadoresEjercicioView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardArticuloView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardBIBView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardComprasView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardControlTurnosView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardEmbotelladoraView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardEnvasadoraView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardExistenciasView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardLightsView;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.DashboardMermasPtView;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.view.ImpresorasView;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.view.ImpresorasUsuariosView;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.view.UsuarioAlertaView;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.view.UsuariosView;
import borsao.e_borsao.Modulos.INFORMATICA.actualizacionServidor.view.actualizadorView;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.view.equiposConectadosView;
import borsao.e_borsao.Modulos.INFORMATICA.racks.view.racksView;
import borsao.e_borsao.Modulos.INFORMATICA.sedes.view.SedesView;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.view.SincronizadorSGAManualView;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.view.SincronizadorView;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.view.SincronizarTablasView;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.view.AnaliticasView;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view.FincasVendimiaView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view.BarricasView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.view.DesgloseNavesView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.view.DashboardBarricasView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.view.NavesView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view.OrdenesTrabajoView;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.view.granelesView;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.view.VariedadesDgaView;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.view.TrazabilidadView;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.view.cambiosProvocadosView;
import borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.view.previsionEmbotelladosView;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.view.HorasTrabajadasBIBView;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.ControlEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.view.HorasTrabajadasEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.InventarioAlmacenView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view.ProgramacionEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.view.OpcionesMPEView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.view.OpcionesPTEView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.server.consultaSituacionMPEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.view.consultaMateriaPrimaEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view.ControlTurnoEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.server.consultaSituacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view.SituacionEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.view.EstadosTareasEscandalloView;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.view.EstadosTareasProtocoloView;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.view.EtiquetasPaletsView;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.view.HorasTrabajadasEmbotelladoraView;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.view.OpcionesView;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.view.OperariosView;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.ProgramacionView;
import borsao.e_borsao.Modulos.PRODUCCION.ProgramacionAux.view.ProgramacionAuxView;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.view.TareasEscandalloView;
import borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.view.TareasProtocoloView;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.view.TiposOFView;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.view.TurnosView;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view.SituacionEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view.CamaraVisionArtificialView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.AyudaProduccionView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.ProduccionDiariaView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.consultaSituacionEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.consultaSituacionEmbotelladoVinoMesaView;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view.RetrabajosView;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.view.EquiposProduccionView;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.view.EquiposProduccionSemanaView;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.parteProduccionView;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.view.StockMinimoPTView;
import borsao.e_borsao.modelo.Login.CurrentUser;
import borsao.e_borsao.views.about.AboutView;
import borsao.e_borsao.views.inicial.InicialView;

/**
 * Content of the UI when the user is logged in.
 * 
 * 
 */
public class MainScreen extends HorizontalLayout {
    public Menu menu;
    private int intervaloRefresco = 5*60*1000; //milisegundos

    public MainScreen(eBorsao ui) {

    	setSizeFull();
    	addStyleName("mainview");
    	setSpacing(false);
    	
        CssLayout viewContainer = new CssLayout();
        viewContainer.addStyleName("valo-content");
        viewContainer.setSizeFull();

        final Navigator navigator = new Navigator(ui, viewContainer);
        navigator.setErrorView(ErrorView.class);
        
        if (!eBorsao.get().accessControl.isSinMenu())
        {
	        menu = new Menu(navigator);
	        cargarMenu(menu, eBorsao.get().accessControl.getDepartamento(), eBorsao.get().accessControl.getDispositivo());
	        navigator.addViewChangeListener(viewChangeListener);
	        addComponent(menu);
        }
        else
        {
        	/*
        	 * cargaremos menu especial moviles
        	 */
	        menu = new Menu(navigator);
	        cargarMenu(menu, eBorsao.get().accessControl.getDepartamento(), eBorsao.get().accessControl.getDispositivo());
//	        navigator.addViewChangeListener(viewChangeListener);
	        addComponent(menu);
        	
        }
        
        addComponent(viewContainer);
        setExpandRatio(viewContainer, 1);
        
//        setSizeUndefined();
        this.setResponsive(true);
        
        this.ejecutarTimer(intervaloRefresco);
    }

    // notify the view menu about view changes so that it can display which view
    // is currently active
    ViewChangeListener viewChangeListener = new ViewChangeListener() {

        @Override
        public boolean beforeViewChange(ViewChangeEvent event) {
            return true;
        }

        @Override
        public void afterViewChange(ViewChangeEvent event) {
            menu.setActiveView(event.getViewName());
        }
    };
    
    private void cargarMenu(Menu r_menu, String r_departamento, String r_dispositivo)
    {
    	if (r_dispositivo.length()==0)
    	{
	    	this.cargarMenuGeneral(r_menu,r_departamento,null);
	    	this.cargarCalidad(r_menu,r_departamento,null);
	    	this.cargarInformatica(r_menu,r_departamento,null);
	    	this.cargarAlmacen(r_menu,r_departamento,null);
	    	this.cargarLaboratorio(r_menu,r_departamento,null);
	    	this.cargarProduccion(r_menu,r_departamento,null);
	    	this.cargarVentas(r_menu,r_departamento,null);
	    	this.cargarCompras(r_menu,r_departamento,null);
	    	this.cargarFinanciero(r_menu,r_departamento,null);
	    	this.cargarGerencia(r_menu,r_departamento,null);
	    	
	    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario) this.cargarPlanificacion(r_menu, r_departamento);
	    	
	    	if (eBorsao.get().accessControl.getNombre().contains("Claveria")) this.cargarPruebas(r_menu, r_departamento);
	    	if (eBorsao.get().accessControl.getNombre().contains("LLanas")) this.cargarPruebas(r_menu, r_departamento);
    	}
    	else if (r_dispositivo.equals("phone") || r_dispositivo.equals("Ipad") || r_dispositivo.equals("?"))
    	{

//    		this.cargarMenuGeneral(r_menu,r_departamento, "G");
//	    	this.cargarLaboratorio(r_menu,r_departamento,"G");
	    	this.cargarVentas(r_menu,r_departamento,"G");
	    	this.cargarProduccion(r_menu,r_departamento,"G");
	    	this.cargarCompras(r_menu,r_departamento,"G");
    	}
    }

    private void cargarMenuGeneral(Menu r_menu, String r_departamento, String r_destino)
    {
    	
        r_menu.addView("GENERAL", "G");
        r_menu.addView(new InicialView(), InicialView.VIEW_NAME, "Home",
        		FontAwesome.HOME, "G", eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
        
        if (eBorsao.get().accessControl.getUsuarioCuentaCorreo()!=null && eBorsao.get().accessControl.getUsuarioCuentaCorreo().length()>0 && !eBorsao.get().accessControl.isAccesoCliente())
        {
        	if (!eBorsao.get().accessControl.getNombre().contains("ecepcion"))
        	{
	        	r_menu.addView(new ProgramacionView(), ProgramacionView.VIEW_NAME, "Programacion ", FontAwesome.CALENDAR_CHECK_O, "G", true,null);
	        	r_menu.addView(new ProgramacionEnvasadoraView(), ProgramacionEnvasadoraView.VIEW_NAME, "Programacion Envasadora", FontAwesome.CALENDAR_CHECK_O, "G", true,null);
	//        	if (eBorsao.get().accessControl.getNombre().contains("Claveria")) 
	        	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.getNombre().contains("Gracia") || eBorsao.get().accessControl.getNombre().contains("botell") || eBorsao.get().accessControl.getNombre().contains("lmac"))
	        	{
	        		r_menu.addView(new RetrabajosView(), RetrabajosView.VIEW_NAME, "Control RETRABAJOS", FontAwesome.ARCHIVE, "G",true, null);
	        	}
        	}
	        r_menu.addView(new CalendarioView(), CalendarioView.VIEW_NAME, "Agenda", FontAwesome.CALENDAR, "G",true ,null);
        }
        
        if (eBorsao.get().accessControl.isUsuarioAlerta())
        {
        	r_menu.addView(new DashboardLightsView(), DashboardLightsView.VIEW_NAME, "Situación General", FontAwesome.DASHBOARD, "G",eBorsao.get().accessControl.isUsuarioAlerta() ,null);
        }
        
        if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") && r_destino==null)
		{
        	
	        r_menu.addView(new UsuariosView(), UsuariosView.VIEW_NAME, "Usuarios",
	        		FontAwesome.USER, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);

	        r_menu.addView(new AvisosView(), AvisosView.VIEW_NAME, "Avisos",
	        		FontAwesome.BULLHORN, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);

	        r_menu.addView(new UsuarioAlertaView(), UsuarioAlertaView.VIEW_NAME, "Semaforos",
	        		FontAwesome.LIGHTBULB_O, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);

	        r_menu.addView(new ImpresorasView(), ImpresorasView.VIEW_NAME, "Impresoras",
	        		FontAwesome.PRINT, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);

	        r_menu.addView(new ImpresorasUsuariosView(), ImpresorasUsuariosView.VIEW_NAME, "Impresoras Usuarios",
	        		FontAwesome.PRINT, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	        
	        r_menu.addView(new ContadoresView(), ContadoresView.VIEW_NAME, "Contadores",
	        		FontAwesome.PRINT, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	        
	        r_menu.addView(new ContadoresEjercicioView(), ContadoresEjercicioView.VIEW_NAME, "Contadores por Ejercicio",
	        		FontAwesome.PRINT, "G",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);

		}
        
        if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals(""))
		{
    		r_menu.addView(new consultaStockExternosView(), consultaStockExternosView.VIEW_NAME, "Almacenes Externos",
    				FontAwesome.SHOPPING_BASKET, "G",false , null);
		}
        
        r_menu.addView(new AboutView(), AboutView.VIEW_NAME, AboutView.VIEW_NAME,
        		FontAwesome.INFO_CIRCLE,"G", eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
        
    }
    
    private void cargarVentas(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial"))
    	{
        	if (r_destino==null) 
    		{
        		r_menu.addView("VENTAS", "V");
        		r_destino="V";
    		}
        	
        	if (!eBorsao.get().accessControl.isAccesoCliente())
        	{
	        	//Ventas Lote
	    	    r_menu.addView(new VentasView(), VentasView.VIEW_NAME, "Ventas",
		        		FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") || r_destino.equals("G") ,null);
	    	    
	
	    	    if (eBorsao.get().accessControl.getUsuarioCuentaCorreo()!=null && eBorsao.get().accessControl.getUsuarioCuentaCorreo().length()>0)
	    	    {
	    	    	
	    	    	r_menu.addView(new OpcionesUSAView(), OpcionesUSAView.VIEW_NAME, "Articulos USA",
	    	    			FontAwesome.USD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") || r_destino.equals("G") ,null);
	    	    	
	    	    	r_menu.addView(new FacturasVentasNacionalView(), FacturasVentasNacionalView.VIEW_NAME, "Facturas Venta",
		        		FontAwesome.PAPER_PLANE_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,new consultaFacturasVentasServer(CurrentUser.get()));
	
	    	    	r_menu.addView(new consultaStockExternosView(), consultaStockExternosView.VIEW_NAME, "Almacenes Externos",
	    	    			FontAwesome.SHOPPING_BASKET, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial"), null);
	    	    	
	    	    	r_menu.addView(new consultaVentasLoteView(), consultaVentasLoteView.VIEW_NAME, "Salidas por Articulo Lote",
	    	    			FontAwesome.SEND, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial"), null);
	    	    	
	    	    	r_menu.addView(new SituacionEmbotelladoView(), SituacionEmbotelladoView.VIEW_NAME, "Calculo Necesidades",
	    	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	    	    	
	    	    	r_menu.addView(new SituacionEnvasadoraView(), SituacionEnvasadoraView.VIEW_NAME, "Situacion Envasadora",
	    	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	    	    	
	        	    r_menu.addView(new PedidosVentasView(), PedidosVentasView.VIEW_NAME, "Pedidos Ventas",
	    	        		FontAwesome.SHOPPING_CART, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);

        			r_menu.addView(new PreparacionPedidosView(), PreparacionPedidosView.VIEW_NAME, "Preparacion Pedidos",
            	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);        			

	        	    r_menu.addView(new SeguimientoEMCSView(), SeguimientoEMCSView.VIEW_NAME, "Seguimiento EMCS",
	    	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);

	        	    r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
	        				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);

	        	    r_menu.addView(new AsignacionSalidasEMCSView(), AsignacionSalidasEMCSView.VIEW_NAME, "Asignacion Salidas EMCS",
	    	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	        	    r_menu.addView(new InventarioAlmacenView(), InventarioAlmacenView.VIEW_NAME, "Cargas Vino Mesa",
	        	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	        	    
	        	    if (eBorsao.get().accessControl.getNombre().contains("Revilla") || eBorsao.get().accessControl.getNombre().contains("Guillermo") )
	        	    {
	        	    	r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", 
	        	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial"),null);
	        	    	r_menu.addView(new DashboardEnvasadoraView(), DashboardEnvasadoraView.VIEW_NAME, "CM Envasadora", FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	        	    	r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", FontAwesome.DASHBOARD, r_destino ,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	        	    	r_menu.addView(new DashboardComprasView(), DashboardComprasView.VIEW_NAME, "CM Compras", FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
	        	    }
	    	    }
	
	    	}
        	else
        	{
        		
        	    r_menu.addView(new PedidosVentasView(), PedidosVentasView.VIEW_NAME, "Pedidos Ventas",
    	        		FontAwesome.SHOPPING_CART, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);

        		r_menu.addView(new FacturasVentasNacionalView(), FacturasVentasNacionalView.VIEW_NAME, "Facturas Venta",
        				FontAwesome.PAPER_PLANE_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,new consultaFacturasVentasServer(CurrentUser.get()));
        		
        	}
    	}
    	
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") && r_destino.equals("V"))
    	{
		    r_menu.addView(new VistasView(), VistasView.VIEW_NAME, "Vistas CM",
	        		FontAwesome.TABLE, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Comercial") ,null);
    	}
    }

    private void cargarCompras(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("COMPRAS", "C");
        		r_destino="C";
    		}
        	
    	    r_menu.addView(new PedidosComprasView(), PedidosComprasView.VIEW_NAME, "Pedidos Compras",
	        		FontAwesome.SHOPPING_CART, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") || r_destino.equals("G") ,null);
    	   
    	    r_menu.addView(new SituacionEmbotelladoView(), SituacionEmbotelladoView.VIEW_NAME, "Calculo Necesidades",
    	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") || r_destino.equals("G") ,null);

		    r_menu.addView(new consultaSituacionEmbotelladoView(), consultaSituacionEmbotelladoView.VIEW_NAME, "Control Programacion",
	        		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") || r_destino.equals("G") , null);

		    r_menu.addView(new consultaSituacionEmbotelladoVinoMesaView(), consultaSituacionEmbotelladoVinoMesaView.VIEW_NAME, "Control Programacion Envasadora",
	        		FontAwesome.DASHBOARD,r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") || r_destino.equals("G") , null);

    		r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);

		    r_menu.addView(new SeguimientoEMCSView(), SeguimientoEMCSView.VIEW_NAME, "Seguimiento EMCS",
	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);
		    
		    r_menu.addView(new DashboardComprasView(), DashboardComprasView.VIEW_NAME, "CM Compras", FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);
		    
    	    if (r_destino.equals("C"))
    	    {
        		r_menu.addView(new NotasArticulosView(), NotasArticulosView.VIEW_NAME, "Notas Articulos",
        				FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);

	    	    r_menu.addView(new ArticulosIntranetView(), ArticulosIntranetView.VIEW_NAME, "Errores Intranet",
		        		FontAwesome.SHOPPING_CART, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras"), new consultaArticulosIntranetServer(CurrentUser.get()));
	    	    
		    	r_menu.addView(new AyudaProduccionView(), AyudaProduccionView.VIEW_NAME, "Ayuda Produccion", 
		    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);
	
	    	    r_menu.addView(new ProduccionDiariaView(), ProduccionDiariaView.VIEW_NAME, "Produccion Diaria", 
	    	    		FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Compras") ,null);
	    	    
    	    }
    	}
    }

    private void cargarFinanciero(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") || (eBorsao.get().accessControl.getNombre().contains("Carmen")))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("FINANCIERO", "F");
        		r_destino="F";
    		}

	    	if (eBorsao.get().accessControl.getNombre().contains("ecepcion"))
	    	{
	    		r_menu.addView(new VacacionesView(), VacacionesView.VIEW_NAME, "Control Vacaciones ",
	    			FontAwesome.SUN_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"), null);
	    	}
	    	else
	    	{
	    		
	    		r_menu.addView(new MovimientosSilicieView(), MovimientosSilicieView.VIEW_NAME, "Movimientos SILICIE",
	    				FontAwesome.BOOK, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	
	    		
	    		if (!eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"))
	    	    {
	    			r_menu.addView(new AsignacionSalidasEMCSView(), AsignacionSalidasEMCSView.VIEW_NAME, "Asignacion Salidas EMCS",
	    	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	    			
	        	    r_menu.addView(new AsignacionEntradasEMCSView(), AsignacionEntradasEMCSView.VIEW_NAME, "Asignacion Entradas EMCS",
	    	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	        	    
	        	    r_menu.addView(new VentasView(), VentasView.VIEW_NAME, "Ventas",
			        		FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	    	    }
	    		
		    	if (eBorsao.get().accessControl.getNombre().contains("David Ferrandez"))
		    	{
		    		r_menu.addView(new VacacionesView(), VacacionesView.VIEW_NAME, "Control Vacaciones ",
		    				FontAwesome.SUN_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"), null);
		    	}
	
	    	    if (r_destino.equals("F"))
		    	{
	    	    	
	        		r_menu.addView(new PresentadoresView(), PresentadoresView.VIEW_NAME , "Presentadores", FontAwesome.BUILDING, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new TiposMovimientoView(), TiposMovimientoView.VIEW_NAME , "Tipos Movimiento", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new DocumentosIdentificativosView(), DocumentosIdentificativosView.VIEW_NAME , "Documentos Identificativos", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new TipoJustificantesView(), TipoJustificantesView.VIEW_NAME , "Tipo Justificantes", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new TipoOperacionesView(), TipoOperacionesView.VIEW_NAME , "Tipo Operaciones", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new DiferenciasMenosView(), DiferenciasMenosView.VIEW_NAME , "Diferencias Menos", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new RegimenesFiscalesView(), RegimenesFiscalesView.VIEW_NAME , "Regimenes Fiscales", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);

	        		r_menu.addView(new CodigosNCView(), CodigosNCView.VIEW_NAME , "Codigos NC", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new CalidadesUvaView(), CalidadesUvaView.VIEW_NAME , "Calidades UVA", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new ParametrosRCAView(), ParametrosRCAView.VIEW_NAME , "Parametros RCA", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new EquivalenciasRCAView(), EquivalenciasRCAView.VIEW_NAME , "Equivalencias RCA", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
	        		r_menu.addView(new AlbaranesRCAView(), AlbaranesRCAView.VIEW_NAME , "Albaranes RCA", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero"),null);
		    		r_menu.addView(new SincronizadorRCAView(), SincronizadorRCAView.VIEW_NAME, "Sincronizar RCA",FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	
	    	    }
	    	    
	    	    if ((eBorsao.get().accessControl.getNombre().contains("Emilio")))
	    	    {
	    	    	r_menu.addView(new DashboardEnvasadoraView(), DashboardEnvasadoraView.VIEW_NAME, "CM Envasadora", FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	    	    	r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", FontAwesome.DASHBOARD, r_destino ,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Financiero") ,null);
	    	    }
	    	}
    	}
    }

    private void cargarAlmacen(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("ALMACEN", "A");
        		r_destino="A";

        		if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") )
        		{
        			r_menu.addView(new OpcionesPTEView(), OpcionesPTEView.VIEW_NAME, "Producto Terminado Envasadora",
        					FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
        			
        			r_menu.addView(new OpcionesPTPNView(), OpcionesPTPNView.VIEW_NAME, "Producto Terminado Pedidos",
        					FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);			    

//        			r_menu.addView(new OpcionesPTPEView(), OpcionesPTPEView.VIEW_NAME, "Producto Terminado Pedidos Exportacion",
//        					FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
//        			
//        			r_menu.addView(new OpcionesPTPUView(), OpcionesPTPUView.VIEW_NAME, "Producto Terminado Pedidos USA",
//        					FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);			    

        		}
    		}
        	
    		r_menu.addView(new CargaInventarioSGAView(), CargaInventarioSGAView.VIEW_NAME, "Carga Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    		r_menu.addView(new consultaInventarioView(), consultaInventarioView.VIEW_NAME, "Consulta Inventario",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    		r_menu.addView(new consultaInventarioSGAGsysView(), consultaInventarioSGAGsysView.VIEW_NAME, "Comparador Inventario SGA Gsys",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    		r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    	    r_menu.addView(new EtiquetasPrepedidosView(), EtiquetasPrepedidosView.VIEW_NAME, "Etiquetas Pre-Pedidos",
	        		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
    	    r_menu.addView(new EtiquetasRecepcionView(), EtiquetasRecepcionView.VIEW_NAME, "Etiquetas Inventario",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    	    r_menu.addView(new EtiquetasUbicacionView(), EtiquetasUbicacionView.VIEW_NAME, "Etiquetas Ubicacion",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
    	    r_menu.addView(new EtiquetasCorteInglesView(), EtiquetasCorteInglesView.VIEW_NAME, "Etiquetas Corte Ingles",
	        		FontAwesome.SHOPPING_BASKET, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
	    	r_menu.addView(new AyudaProduccionView(), AyudaProduccionView.VIEW_NAME, "Ayuda Produccion", 
	    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    	    r_menu.addView(new ProduccionDiariaView(), ProduccionDiariaView.VIEW_NAME, "Produccion Diaria", 
    	    		FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
    	    r_menu.addView(new PedidosVentasView(), PedidosVentasView.VIEW_NAME, "Pedidos Ventas",
	        		FontAwesome.SHOPPING_CART, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
			r_menu.addView(new PreparacionPedidosView(), PreparacionPedidosView.VIEW_NAME, "Preparacion Pedidos",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);        			
    	    
	    	r_menu.addView(new SituacionEnvasadoraView(), SituacionEnvasadoraView.VIEW_NAME, "Situacion Envasadora",
	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
	    	
	    	r_menu.addView(new ControlEmbotelladoView(), ControlEmbotelladoView.VIEW_NAME, "Control Embotelladora",
    				FontAwesome.ARCHIVE, r_destino , eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
	    	
	    	r_menu.addView(new SeguimientoEMCSView(), SeguimientoEMCSView.VIEW_NAME, "Seguimiento EMCS",
	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

    	    r_menu.addView(new InventarioAlmacenView(), InventarioAlmacenView.VIEW_NAME, "Cargas Vino Mesa",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
    	    
    	    r_menu.addView(new mermasPtView(), mermasPtView.VIEW_NAME, "Mermas PT", 
    	    		FontAwesome.ADJUST,r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);

//    	    r_menu.addView(new AsignacionSalidasEMCSView(), AsignacionSalidasEMCSView.VIEW_NAME, "Asignacion Salidas EMCS",
//	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Almacen") ,null);
	    	
    	}
    }
    
    private void cargarCalidad(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ||eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("CALIDAD", "Q");
        		r_destino="Q";
    		}
        	
	    	r_menu.addView(new NoConformidadesView(), NoConformidadesView.VIEW_NAME, "Incidencias Calidad ",
	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

	    	if (eBorsao.get().accessControl.getNombre().contains("Claveria"))
	    	{
		    	r_menu.addView(new ParametrosCalidadView(), ParametrosCalidadView.VIEW_NAME, "Parametros Calidad ",
		    			FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
		    	r_menu.addView(new CostePersonalView(), CostePersonalView.VIEW_NAME, "Coste Personal",
		    			FontAwesome.EURO, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
		    	r_menu.addView(new CosteMaquinaView(), CosteMaquinaView.VIEW_NAME, "Coste Maquina",
		    			FontAwesome.EURO, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
		    	r_menu.addView(new DistribucionPersonalView(), DistribucionPersonalView.VIEW_NAME, "Distribucion Personal Area",
		    			FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
		    	r_menu.addView(new VacacionesView(), VacacionesView.VIEW_NAME, "Control Vacaciones ",
		    			FontAwesome.SUN_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"), null);
		    	r_menu.addView(new VacacionesView(), VacacionesView.VIEW_NAME, "Control Vacaciones ",
		    			FontAwesome.SUN_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"), null);
	    	}	    	
	    	if (eBorsao.get().accessControl.getNombre().contains("Carmen"))
	    	{
			    r_menu.addView(new granelesView(), granelesView.VIEW_NAME, "Datos Graneles",
			    		FontAwesome.GLASS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

			    r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", 
			    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"),null);
			    r_menu.addView(new DashboardComprasView(), DashboardComprasView.VIEW_NAME, "CM Compras", 
			    		FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", 
			    		FontAwesome.DASHBOARD, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new InformeTiemposView(), InformeTiemposView.VIEW_NAME, "Informe Tiempos ",
			    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new CamaraVisionArtificialView(), CamaraVisionArtificialView.VIEW_NAME, "Control Camara Vision Artificial", 
			    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"),null);

			    r_menu.addView(new cambioArticulosView(), cambioArticulosView.VIEW_NAME, "Parametrizacion Articulo-Lote",
			    		FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new SeguimientoEMCSView(), SeguimientoEMCSView.VIEW_NAME, "Seguimiento EMCS",
			    		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new RecepcionEMCSView(), RecepcionEMCSView.VIEW_NAME, "Control Guías Entrada",
			    		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new AsignacionEntradasEMCSView(), AsignacionEntradasEMCSView.VIEW_NAME, "Asignacion Entradas EMCS",
			    		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
			    
			    r_menu.addView(new AsignacionSalidasEMCSView(), AsignacionSalidasEMCSView.VIEW_NAME, "Asignacion Salidas EMCS",
			    		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
	    	}

	    	r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

    		r_menu.addView(new parteProduccionView(), parteProduccionView.VIEW_NAME, "Parte Producción",
    				FontAwesome.ARCHIVE, r_destino , eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"), null);
	    	r_menu.addView(new consultaVentasLoteView(), consultaVentasLoteView.VIEW_NAME, "Salidas por Articulo Lote",
	    			FontAwesome.SEND, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad"), null);

	    	r_menu.addView(new ControlEmbotelladoView(), ControlEmbotelladoView.VIEW_NAME, "Control Embotelladora",
    				FontAwesome.ARCHIVE, r_destino , eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
	    	
    	    r_menu.addView(new controlesCalidadProcesoView(), controlesCalidadProcesoView.VIEW_NAME, "Controles Calidad Proceso",
    	    		FontAwesome.CHECK_SQUARE_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

		    r_menu.addView(new DashboardExistenciasView(), DashboardExistenciasView.VIEW_NAME, "CM Stock", 
		    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

    	    r_menu.addView(new mermasPtView(), mermasPtView.VIEW_NAME, "Mermas PT", 
    	    		FontAwesome.ADJUST,r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);

    	    if (r_destino.equals("Q"))
    	    {
		    	r_menu.addView(new AyudaProduccionView(), AyudaProduccionView.VIEW_NAME, "Ayuda Produccion", 
		    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
	
	    	    r_menu.addView(new ProduccionDiariaView(), ProduccionDiariaView.VIEW_NAME, "Produccion Diaria", 
	    	    		FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Calidad") ,null);
    	    }
    	}
    }
    
    private void cargarInformatica(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("INFORMATICA", "I");
        		r_destino="I";
    		}
        	
	    	r_menu.addView(new SedesView(), SedesView.VIEW_NAME, "Sedes Informaticas",
	    			FontAwesome.CUBES, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    	r_menu.addView(new racksView(), racksView.VIEW_NAME, "Switches",
	    			FontAwesome.LIST, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    	r_menu.addView(new equiposConectadosView(), equiposConectadosView.VIEW_NAME, "Equipos Conectados",
	    			FontAwesome.LAPTOP, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    	r_menu.addView(new SincronizadorSGAManualView(), SincronizadorSGAManualView.VIEW_NAME, "SincronizadorSGA",
	    			FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    	
	    	if (eBorsao.get().accessControl.getNombre().contains("Claveria"))
	    	{
	    		r_menu.addView(new SincronizarTablasView(), SincronizarTablasView.VIEW_NAME, "Sincronizar Tablas",
	    				FontAwesome.TABLE, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    		r_menu.addView(new SincronizadorView(), SincronizadorView.VIEW_NAME, "Sincronizador",
	    				FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    		r_menu.addView(new actualizadorView(), actualizadorView.VIEW_NAME, "Actualizador",
	    				FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") ,null);
	    	}
    	}
    }

    private void cargarLaboratorio(Menu r_menu, String r_departamento,String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("LABORATORIO", "L");
        		r_destino="L";
    		}
	    	
		    r_menu.addView(new AnaliticasView(), AnaliticasView.VIEW_NAME, "Analiticas",
	        		FontAwesome.EYEDROPPER, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new TrazabilidadView(), TrazabilidadView.VIEW_NAME, "Trazabilidad",
	        		FontAwesome.FLASK, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new granelesView(), granelesView.VIEW_NAME, "Datos Graneles",
		    		FontAwesome.GLASS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);

		    r_menu.addView(new SeguimientoEMCSView(), SeguimientoEMCSView.VIEW_NAME, "Seguimiento EMCS",
	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);
    	    r_menu.addView(new RecepcionEMCSView(), RecepcionEMCSView.VIEW_NAME, "Control Guías Entrada",
	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);
    	    r_menu.addView(new AsignacionEntradasEMCSView(), AsignacionEntradasEMCSView.VIEW_NAME, "Asignacion Entradas EMCS",
	        		FontAwesome.CERTIFICATE, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);
    		r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);

		    r_menu.addView(new BarricasView(), BarricasView.VIEW_NAME, "Ficha Barricas",
	        		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new NavesView(), NavesView.VIEW_NAME, "Naves Barricas",
		    		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new DesgloseNavesView(), DesgloseNavesView.VIEW_NAME, "Estructura Naves Barricas",
		    		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new OrdenesTrabajoView(), OrdenesTrabajoView.VIEW_NAME, "Ordenes Trabajo Barricas",
	        		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);
		    r_menu.addView(new DashboardBarricasView(), DashboardBarricasView.VIEW_NAME, "Consulta General Barricas ",
	        		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);


		    r_menu.addView(new VariedadesDgaView(), VariedadesDgaView.VIEW_NAME , "Variedades DGA", 
		    		FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio"),null);
		    r_menu.addView(new FincasVendimiaView(), FincasVendimiaView.VIEW_NAME, "Control Fincas",
	        		FontAwesome.BUILDING, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") || r_destino.equals("G"),null);


		    
    	    if (r_destino.equals("L"))
    	    {
		    	r_menu.addView(new AyudaProduccionView(), AyudaProduccionView.VIEW_NAME, "Ayuda Produccion", 
		    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);
	
	    	    r_menu.addView(new ProduccionDiariaView(), ProduccionDiariaView.VIEW_NAME, "Produccion Diaria", 
	    	    		FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Laboratorio") ,null);
    	    }
    	}
    }
    
    private void cargarPruebas(Menu r_menu, String r_departamento)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") || eBorsao.get().accessControl.isUserInRole(r_departamento).contains("Alma"))
    	{
//    		r_menu.addView("PRUEBAS", "T");
//    		
//	        r_menu.addView(new CalendarioView(), CalendarioView.VIEW_NAME, "Agenda",
//	        		FontAwesome.CALENDAR, "T",false ,null);

    	}
    		
    	else if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master")) 
    	{
    		r_menu.addView("PRUEBAS", "T");

//    		r_menu.addView(new consultaVentas(), consultaVentas.NAME, "Pruebas", FontAwesome.AMBULANCE, "T",false,null);
//    		r_menu.addView(new PedidosUSAView(), PedidosUSAView.VIEW_NAME, "Pedidos USA",
//    				FontAwesome.SHOPPING_BASKET, "T",false , null);
    		/*
        	 * Presentacion Información
        	 */
//    		r_menu.addView(new DashboardEnvasadoraView(), DashboardEnvasadoraView.VIEW_NAME, "CM Envasadora", FontAwesome.DASHBOARD, "T",false ,null);
//    		r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", FontAwesome.DASHBOARD, "T",false ,null);
    	    r_menu.addView(new ControlTurnoEnvasadoraView(), ControlTurnoEnvasadoraView.VIEW_NAME, "Control Turno Envasadora", FontAwesome.COGS, "T", false, null);

    	}
    }

    private void cargarPlanificacion(Menu r_menu, String r_departamento)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") || eBorsao.get().accessControl.isUserInRole(r_departamento).contains("Alma"))
    	{
//    		r_menu.addView("PRUEBAS", "T");
//    		
//	        r_menu.addView(new CalendarioView(), CalendarioView.VIEW_NAME, "Agenda",
//	        		FontAwesome.CALENDAR, "T",false ,null);

    	}
    		
    	else if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master")) 
    	{
    		r_menu.addView("PLANIFICACION", "R");
    		
    		/*
    		 * Programacion Vino Mesa y BIB
    		 */

    		r_menu.addView(new SituacionEnvasadoraView(), SituacionEnvasadoraView.VIEW_NAME, "Situacion Envasadora",
    				FontAwesome.DASHBOARD, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,new consultaSituacionEnvasadoraServer(CurrentUser.get()));
    		r_menu.addView(new ProgramacionEnvasadoraView(), ProgramacionEnvasadoraView.VIEW_NAME, "Programacion Envasadora", FontAwesome.CALENDAR_CHECK_O, "R", true,null);
    		
    		r_menu.addView(new consultaSituacionEmbotelladoVinoMesaView(), consultaSituacionEmbotelladoVinoMesaView.VIEW_NAME, "Control Programacion Envasadora",
    				FontAwesome.DASHBOARD, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") , null);
    		
    		/*
    		 * Programacion Embotellado
    		 */
    		
    		r_menu.addView(new StockMinimoPTView(), StockMinimoPTView.VIEW_NAME, "Stock Minimo PT",
    				FontAwesome.DASHBOARD, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
    		
    		r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, "R", eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

    		r_menu.addView(new NotasArticulosView(), NotasArticulosView.VIEW_NAME, "Notas Articulos",
    				FontAwesome.COGS, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

    		r_menu.addView(new SituacionEmbotelladoView(), SituacionEmbotelladoView.VIEW_NAME, "Calculo Necesidades",
    				FontAwesome.DASHBOARD, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
        	r_menu.addView(new ProgramacionView(), ProgramacionView.VIEW_NAME, "Programacion ", FontAwesome.CALENDAR_CHECK_O, "R", true,null);
        	r_menu.addView(new ProgramacionAuxView(), ProgramacionAuxView.VIEW_NAME, "Programacion Auxiliar", FontAwesome.CALENDAR_CHECK_O, "R", true,null);
        	r_menu.addView(new consultaSituacionEmbotelladoView(), consultaSituacionEmbotelladoView.VIEW_NAME, "Control Programacion",
        			FontAwesome.DASHBOARD, "R",eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") , null);
        	
        	/*
        	 * Programacion Retrabajos
        	 */
        	r_menu.addView(new RetrabajosView(), RetrabajosView.VIEW_NAME, "Control RETRABAJOS",
        			FontAwesome.ARCHIVE, "R",(eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.getNombre().contains("Gracia") || eBorsao.get().accessControl.getNombre().contains("botell") || eBorsao.get().accessControl.getNombre().contains("lmac")), null);

        	r_menu.addView(new previsionEmbotelladosView(), previsionEmbotelladosView.VIEW_NAME, "Prevision Embotellados", FontAwesome.ELLIPSIS_V, "R",false ,null);
		    r_menu.addView(new granelesView(), granelesView.VIEW_NAME, "Datos Graneles",FontAwesome.GLASS, "R",false,null);

        	r_menu.addView(new HorasTrabajadasEnvasadoraView(), HorasTrabajadasEnvasadoraView.VIEW_NAME, "Horas Envasadora", FontAwesome.ADJUST, "R",false ,null);
        	r_menu.addView(new HorasTrabajadasEmbotelladoraView(), HorasTrabajadasEmbotelladoraView.VIEW_NAME, "Horas Embotelladora", FontAwesome.ADJUST, "R",false ,null);
        	r_menu.addView(new HorasTrabajadasBIBView(), HorasTrabajadasBIBView.VIEW_NAME, "Horas BIB", FontAwesome.ADJUST, "R",false ,null);
        	r_menu.addView(new mermasPtView(), mermasPtView.VIEW_NAME, "Mermas PT", FontAwesome.ADJUST,"R",false ,null);
        	/*
        	 * Presentacion Información
        	 */
    		r_menu.addView(new DashboardEnvasadoraView(), DashboardEnvasadoraView.VIEW_NAME, "CM Envasadora", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardMermasPtView(), DashboardMermasPtView.VIEW_NAME, "CM Mermas Produccion", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardComprasView(), DashboardComprasView.VIEW_NAME, "CM Compras", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardExistenciasView(), DashboardExistenciasView.VIEW_NAME, "CM Stock", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new cambiosProvocadosView(), cambiosProvocadosView.VIEW_NAME, "Cambios Planificacion", FontAwesome.DASHBOARD, "R",false ,null);
    		r_menu.addView(new DashboardControlTurnosView(), DashboardControlTurnosView.VIEW_NAME, "Control Turnos Ejercicio", FontAwesome.DASHBOARD, "R",false ,null);
    		
    	}
    }

    private void cargarGerencia(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") || eBorsao.get().accessControl.getNombre().contains("LLanas") || eBorsao.get().accessControl.getNombre().contains("Ferrandez") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Gerencia")
    			|| eBorsao.get().accessControl.getNombre().contains("uillermo") || eBorsao.get().accessControl.getNombre().contains("evilla") || eBorsao.get().accessControl.getNombre().contains("icote")
    			|| eBorsao.get().accessControl.getNombre().contains("Vidal"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("GERENCIA", "D");
        		r_destino="D";
    		}
    	
		    if (r_destino.equals("D") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Gerencia"))
		    {
	    		
	    		/*
	    		 * Presentacion Información
	    		 */
		    	r_menu.addView(new DashboardBIBView(), DashboardBIBView.VIEW_NAME, "CM BIB", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		r_menu.addView(new DashboardEnvasadoraView(), DashboardEnvasadoraView.VIEW_NAME, "CM Envasadora", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		r_menu.addView(new DashboardComprasView(), DashboardComprasView.VIEW_NAME, "CM Compras", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		if (eBorsao.get().accessControl.getNombre().contains("Claveria")) r_menu.addView(new DashboardExistenciasView(), DashboardExistenciasView.VIEW_NAME, "CM Stock", FontAwesome.DASHBOARD, r_destino,false ,null);
//	    		r_menu.addView(new cambiosProvocadosView(), cambiosProvocadosView.VIEW_NAME, "Cambios Planificacion", FontAwesome.DASHBOARD, r_destino,false ,null);
	    		
	    	}
    	}
    }

    private void cargarProduccion(Menu r_menu, String r_departamento, String r_destino)
    {
    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master") || eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"))
    	{
    		if (r_destino==null) 
    		{
    			r_menu.addView("PRODUCCION", "P");
        		r_destino="P";
    		}
    		
    		r_menu.addView(new parteProduccionView(), parteProduccionView.VIEW_NAME, "Parte Producción",
    				FontAwesome.ARCHIVE, r_destino , eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

    		r_menu.addView(new ControlEmbotelladoView(), ControlEmbotelladoView.VIEW_NAME, "Control Embotelladora",
    				FontAwesome.ARCHIVE, r_destino , eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	
	    	r_menu.addView(new ProduccionDiariaView(), ProduccionDiariaView.VIEW_NAME, "Produccion Diaria", 
	    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

	    	r_menu.addView(new AyudaProduccionView(), AyudaProduccionView.VIEW_NAME, "Ayuda Produccion", 
	    			FontAwesome.INDUSTRY, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	
	    	r_menu.addView(new SituacionEnvasadoraView(), SituacionEnvasadoraView.VIEW_NAME, "Situacion Envasadora",
	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,new consultaSituacionEnvasadoraServer(CurrentUser.get()));

    		r_menu.addView(new consultaInventarioSGAView(), consultaInventarioSGAView.VIEW_NAME, "Consulta Inventario SGA",
    				FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

	    	r_menu.addView(new consultaMateriaPrimaEnvasadoraView(), consultaMateriaPrimaEnvasadoraView.VIEW_NAME, "Stock Materia Seca Envasadora",
	    			FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") , new consultaSituacionMPEnvasadoraServer(CurrentUser.get()));
	    	
    	    r_menu.addView(new InventarioAlmacenView(), InventarioAlmacenView.VIEW_NAME, "Cargas Vino Mesa",
    	    		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

    	    r_menu.addView(new controlesCalidadProcesoView(), controlesCalidadProcesoView.VIEW_NAME, "Controles Calidad Proceso",
    	    		FontAwesome.CHECK_SQUARE_O, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

    	    if (eBorsao.get().accessControl.getNombre().contains("Gracia"))
    	    {
    	    	r_menu.addView(new ControlTurnoEnvasadoraView(), ControlTurnoEnvasadoraView.VIEW_NAME, "Control Turno Envasadora", FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
        	    r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", 
        	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);
        	    r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", 
        	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);
    	    }

    	}

    	if (eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Master"))
    	{
    	    r_menu.addView(new ArticulosNuevosView(), ArticulosNuevosView.VIEW_NAME, "Articulos Nuevos", 
    	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);

    	    r_menu.addView(new CamaraVisionArtificialView(), CamaraVisionArtificialView.VIEW_NAME, "Control Camara Vision Artificial", 
    	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);

    	    r_menu.addView(new DashboardArticuloView(), DashboardArticuloView.VIEW_NAME, "CM Articulo", 
    	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);

    	    r_menu.addView(new DashboardEmbotelladoraView(), DashboardEmbotelladoraView.VIEW_NAME, "CM Embotelladora", 
    	    		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null);

		    r_menu.addView(new SituacionEmbotelladoView(), SituacionEmbotelladoView.VIEW_NAME, "Calculo Necesidades",
	        		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);

		    r_menu.addView(new consultaSituacionEmbotelladoView(), consultaSituacionEmbotelladoView.VIEW_NAME, "Control Programacion",
	        		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") , null);
		    
		    r_menu.addView(new consultaSituacionEmbotelladoVinoMesaView(), consultaSituacionEmbotelladoVinoMesaView.VIEW_NAME, "Control Programacion Envasadora",
	        		FontAwesome.DASHBOARD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") , null);

		    r_menu.addView(new ControlTurnoEnvasadoraView(), ControlTurnoEnvasadoraView.VIEW_NAME, "Control Turno Envasadora", FontAwesome.COGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"), null);
		    
		    if (r_destino.equals("P"))
		    {
			    r_menu.addView(new OperariosView(), OperariosView.VIEW_NAME, "Operarios",
		        		FontAwesome.USER_PLUS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
			    r_menu.addView(new EquiposProduccionView(), EquiposProduccionView.VIEW_NAME, "Equipos Produccion",
		        		FontAwesome.USER_MD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
			    r_menu.addView(new EquiposProduccionSemanaView(), EquiposProduccionSemanaView.VIEW_NAME, "Turnos Produccion",
		        		FontAwesome.USER_MD, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);			    
			    r_menu.addView(new OpcionesView(), OpcionesView.VIEW_NAME, "Materia Seca",
		        		FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
			    r_menu.addView(new TurnosView(), TurnosView.VIEW_NAME, "Turnos",
		        		FontAwesome.CLOCK_O, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
			    r_menu.addView(new TiposOFView(), TiposOFView.VIEW_NAME, "Tipos OF",
		        		FontAwesome.TAGS, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion"),null );
			    r_menu.addView(new OpcionesMPEView(), OpcionesMPEView.VIEW_NAME, "Materia Seca Envasadora",
		        		FontAwesome.ELLIPSIS_V, r_destino,eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new EtiquetasPaletsView(), EtiquetasPaletsView.VIEW_NAME, "Etiquetas Palets",
		        		FontAwesome.SHOPPING_BAG, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new TareasEscandalloView(), TareasEscandalloView.VIEW_NAME, "Tareas Escandallo",
		        		FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new EstadosTareasEscandalloView(), EstadosTareasEscandalloView.VIEW_NAME, "Estado Tareas Escandallo",
		        		FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new TareasProtocoloView(), TareasProtocoloView.VIEW_NAME, "Tareas Protocolo",
		        		FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new EstadosTareasProtocoloView(), EstadosTareasProtocoloView.VIEW_NAME, "Estado Tareas Protocolo",
		        		FontAwesome.COGS, r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
	    	    r_menu.addView(new mermasPtView(), mermasPtView.VIEW_NAME, "Mermas PT", 
	    	    		FontAwesome.ADJUST,r_destino, eBorsao.get().accessControl.isUserInRole(r_departamento).equals("Produccion") ,null);
		    }
    	}
    }

    private void comprobarAvisos()
    {
    	ArrayList<MapeoAvisos> mensaje=null;
    	
    	consultaAvisosServer cas = new consultaAvisosServer(CurrentUser.get());
    	mensaje = cas.datosOpcionesGlobal(eBorsao.get().accessControl.getUsuario());
    	
    	if (mensaje!=null && !mensaje.isEmpty())
    	{
    		for (int i=0;i<mensaje.size();i++)
    		{
    			MapeoAvisos mapeo=mensaje.get(i);
    			Notificaciones.getInstance().mensajeError(mapeo.getDescripcion());
    		}
    	}
    	
    	mensaje=null;
    	cas=null;
    }

    private void comprobarAvisosCalendario()
    {
    	ArrayList<MapeoAvisos> mensaje=null;
    	
    	consultaCalendarioServer cas = new consultaCalendarioServer(CurrentUser.get());
    	mensaje = cas.comprobarAvisosCalendario(eBorsao.get().accessControl.getNombre());
    	
    	if (mensaje!=null && !mensaje.isEmpty())
    	{
    		for (int i=0;i<mensaje.size();i++)
    		{
    			MapeoAvisos mapeo=mensaje.get(i);
    			Notificaciones.getInstance().mensajeAvisoCalendario(mapeo.getDescripcion());
    		}
    	}
    	
    	mensaje=null;
    	cas=null;
//    	Notificaciones.getInstance().mensajeInformativo("Pendiente de implementar. Metodo en MainScreen");
    }

    private void ejecutarTimer(int r_intervaloRefresco)
    {
    	if (LecturaProperties.avisos || LecturaProperties.avisosCalendario)
    	{
    		Refresher REFRESHER = new Refresher();
    		REFRESHER.setRefreshInterval(r_intervaloRefresco);
    		ChatRefreshListener cr = new ChatRefreshListener();
    		REFRESHER.addListener(cr);
    		addExtension(REFRESHER);
    	}
    }

	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) 
    	{
    		Integer minActual = new Integer(RutinasFechas.minutoFecha(new Date()));
    		if (minActual%5==0)
    		{
    			if (LecturaProperties.avisos) comprobarAvisos();
    		}
    		if (LecturaProperties.avisosCalendario) comprobarAvisosCalendario();
    		
    		Notificaciones.getInstance().mensajeSeguimiento("referesco" + new Date()); 
    	}
    }

}


