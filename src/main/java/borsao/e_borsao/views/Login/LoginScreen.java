package borsao.e_borsao.views.Login;

import java.io.Serializable;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.Imagen;
import borsao.e_borsao.ClasesPropias.PasswordSee;
import borsao.e_borsao.modelo.Login.AccessControl;

/**
 * UI content when the user is not logged in yet.
 */
public class LoginScreen extends CssLayout {

	
    private TextField username;
    private PasswordSee password;
    private Button login;
    private Button forgotPassword;
    private LoginListener loginListener;
    private AccessControl accessControl;
    public static final String NAME = "login";
    
    public LoginScreen(AccessControl accessControl, LoginListener loginListener) {
    	
        this.loginListener = loginListener;
        this.accessControl = accessControl;
        buildUI();
        username.focus();
    }

    private void buildUI() {
        addStyleName("login-screen");

        // login form, centered in the available part of the screen
        Component loginForm = buildLoginForm();

        // layout to center login form when there is sufficient screen space
        // - see the theme for how this is made responsive for various screen
        // sizes
        VerticalLayout centeringLayout = new VerticalLayout();
        centeringLayout.setStyleName("centering-layout");
        centeringLayout.addComponent(loginForm);
        centeringLayout.setComponentAlignment(loginForm,
                Alignment.MIDDLE_CENTER);

        // information text about logging in
        CssLayout loginInformation = buildLoginInformation();
        
        addComponent(centeringLayout);
        addComponent(loginInformation);
    }

    private Component buildLoginForm() {
        FormLayout loginForm = new FormLayout();
        HorizontalLayout loginUser = new HorizontalLayout();
        
        loginForm.addStyleName("login-form");
        loginForm.setSizeUndefined();
        loginForm.setMargin(false);
        
        username = new TextField();
		username.setIcon(FontAwesome.USER);
		username.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        loginUser.addComponent(username);
        loginForm.addComponent(loginUser);
        username.setWidth(20, Unit.EM);
        username.setDescription("Write mail account");
        loginForm.addComponent(password = new PasswordSee("Password"));
        password.setWidth(15, Unit.EM);
        password.setDescription("Write your password");
        CssLayout buttons = new CssLayout();
        buttons.setStyleName("buttons");
        loginForm.addComponent(buttons);

        buttons.addComponent(login = new Button("Login"));
        login.setDisableOnClick(true);
        login.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    login();
                } finally {
                    login.setEnabled(true);
                }
            }
        });
        login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        login.addStyleName(ValoTheme.BUTTON_PRIMARY);

        buttons.addComponent(forgotPassword = new Button("Forgot password?"));
        forgotPassword.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
            	if (username.getValue()=="") 
            	{
            		showNotification(new Notification("Debes rellenar el usuario.",Notification.Type.HUMANIZED_MESSAGE));
            	}
            	else
            	{
					ClienteMail.getInstance().envioMail("j.claveria@bodegasborsao.com",null,null, "Olvido contraseña","INTRANET - Resetear cuenta de: " + username.getValue(),null,false);
					showNotification(new Notification("Forgot Password?", "We send message to " + eBorsao.strEmpresa + " support team.", Notification.Type.HUMANIZED_MESSAGE));
            	}
            }
        });
        forgotPassword.addStyleName(ValoTheme.BUTTON_LINK);
        return loginForm;
    }

    private CssLayout buildLoginInformation() {
        CssLayout loginInformation = new CssLayout();        
        loginInformation.setStyleName("login-information");

        Label loginInfoText = new Label(
                "<p><h1>" + eBorsao.strEmpresa + "<h1></p>", ContentMode.HTML);
        loginInfoText.setStyleName("labelTitulo");

        Label loginInfoText2 = new Label("<P> Welcome to ON-LINE " + eBorsao.strEmpresaAlias + " INTRANET </p>"
                		+ "<p>&nbsp</p>"
                        + "<p> Log in as &quot;userName@" + eBorsao.strDominio + "&quot; to have access. </p>",
                ContentMode.HTML);
        
        
        Resource res = new ThemeResource("img/" + eBorsao.strLogoEmpresa +"");
        Imagen image = new Imagen(null, res);
        image.setWidth("100%");

        if (eBorsao.strEstiloLogo.contentEquals("cuadrado"))
        {
        	image.setWidth("100%");
        	image.setHeight("25%");
        }
        else
        {
        	image.setWidth("90%");
        	image.setHeight("10%");
        }
        
        loginInformation.addComponent(image);
     	loginInformation.addComponent(loginInfoText);
     	loginInformation.addComponent(loginInfoText2);
        return loginInformation;
    }

    private void login() {
    	
    	if (!username.getValue().contains("@")) username.setValue(username.getValue() + "@" + eBorsao.strDominio);
    	if (password.txtTexto.isVisible() && password.txtTexto.getValue().length()>0) password.txtPass.setValue(password.txtTexto.getValue());
        if (accessControl.signIn(username.getValue(), password.txtPass.getValue())) {
            loginListener.loginSuccessful();
        } else {
            showNotification(new Notification("Login failed",
                    "Please check your username and password and try again.",
                    Notification.Type.HUMANIZED_MESSAGE));
            username.focus();
        }
    }

    private void showNotification(Notification notification) {
        // keep the notification visible a little while after moving the
        // mouse, or until clicked
        notification.setDelayMsec(2000);
        notification.show(Page.getCurrent());
    }

    public interface LoginListener extends Serializable {
        void loginSuccessful();
    }
}
