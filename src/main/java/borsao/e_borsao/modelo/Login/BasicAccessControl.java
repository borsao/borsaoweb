package borsao.e_borsao.modelo.Login;

import java.util.Date;

import com.vaadin.server.Page;
import com.vaadin.server.WebBrowser;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.MapeoUsuarios;
import borsao.e_borsao.server.Login.LoginServer;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */
public class BasicAccessControl implements AccessControl {

	public String usuario = null;
	public String nombre = null;
	public String firma = null;
	public String codigoGestion = null;
	public String vista = null;
	public String pantalla = null;
	public String dispositivo = null;
	public String departamento= null;
	public String passwordCuenta= null;
	public String usuarioCuentaCorreo= null;
	public boolean vistaSemaforo = false;
	public boolean usuarioAlerta= false;
	public boolean sinMenu = false;
	public boolean accesoCliente = false;

	
    @Override
    public boolean signIn(String username, String password) {
        if (username == null || username.isEmpty())
        {
            return false;
        }
        else
        {
        	LoginServer logServer = new LoginServer();
            
            /*
             * Parte Servidora
             * 
             * Lamada para devoler usuario y password contrastada con la base de datos
             * 
             * Devuelve un booleano para saber si es correcto o no
             * 
             * 
             * 
             */
            MapeoUsuarios map = logServer.comprobarUsuario(username,password);
            if (map==null) {
//            	Notificaciones serNotif = Notificaciones.getInstance();
//            	serNotif.mensajeError(isValid);
            	return false;
            }
            else
            {
            	boolean semaforos = logServer.comprobarUsuarioAlerta(map.getUsuario());
            	this.usuario=username;
            	this.usuarioAlerta=semaforos;
            	this.vistaSemaforo=map.getViewDefecto().startsWith("DashBoard");
            	
            	this.nombre=map.getNombre();
				this.firma=map.getFirma();
				this.vista=map.getVista();
				this.pantalla=map.getViewDefecto();
				this.departamento=map.getDepartamento();
				this.passwordCuenta=password;
				this.usuarioCuentaCorreo=map.getUsuarioCorreo();
				this.codigoGestion=map.getCodigoGestion();
				
				try{
					Integer cli = null;
	     			consultaClientesServer ccs = consultaClientesServer.getInstance(CurrentUser.get());
	     			cli = new Integer(eBorsao.get().accessControl.getCodigoGestion());
	     			this.setAccesoCliente(ccs.comprobarAccesoCliente(cli));
				}
				catch(Exception ex)
				{
					this.setAccesoCliente(false);
				}
            	
		    	WebBrowser browser = (WebBrowser) Page.getCurrent().getWebBrowser();
		    	if (browser.isIPad())
		    	{
		    		this.dispositivo="Ipad";
		    	}
		    	else if (!browser.isIPad() && (browser.getBrowserApplication().contains("Mobile") || browser.isIPhone() || browser.isWindowsPhone()))
		    	{
		    		this.dispositivo="phone"; 
		    	}
		    	else if (browser.isAndroid())
		    	{    		
		    		this.dispositivo="?";
		    	}
		    	else 
		    	{
		    		this.dispositivo="";
		    	}

		    	if (this.dispositivo.equals("")) Notificaciones.getInstance().mensajeSeguimiento("usuario logueado con ordenador " ); else Notificaciones.getInstance().mensajeSeguimiento("usuario logueado con " + getDispositivo());
//		    	if (!this.dispositivo.equals(""))
		    	{
		    		Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(browser.getScreenHeight()));
		    		Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(browser.getScreenWidth()));
		    		Notificaciones.getInstance().mensajeSeguimiento(browser.getBrowserApplication());
		    	}
		    	if (browser.getScreenWidth()<1000) sinMenu=true;
		    	Notificaciones.getInstance().mensajeSeguimiento("usuario logueado " + username  + " : " + new Date());
            	CurrentUser.set(username);
            	return true;
            }
        }
    }

    public String getUsuario()
    {
    	return this.usuario;
    }
    @Override
    public boolean isUserSignedIn() {
    	if (CurrentUser.get()==null) return false;    	
        return !CurrentUser.get().isEmpty();
    }

    @Override
    public String isUserInRole(String role) {
        if (role.toUpperCase().equals("INFORMATICA")) {
            // Only the "admin" user is in the "admin" role
            return "Master";
        }
        else if (role.toUpperCase().equals("COMERCIAL")) {
            // Only the "admin" user is in the "admin" role
            return "Comercial";
        }
        else if (role.toUpperCase().equals("LABORATORIO")) {
            // Only the "admin" user is in the "admin" role
            return "Laboratorio";
        }
        else if (role.toUpperCase().equals("COMPRAS")) {
            // Only the "admin" user is in the "admin" role
            return "Compras";
        }
        else if (role.toUpperCase().equals("CALIDAD")) {
            // Only the "admin" user is in the "admin" role
            return "Calidad";
        }

        else if (role.toUpperCase().equals("ALMACEN")) {
            // Only the "admin" user is in the "admin" role
            return "Almacen";
        }        
        else if (role.toUpperCase().equals("PRODUCCION")) {
            // Only the "admin" user is in the "admin" role
            return "Produccion";
        }        
        else if (role.toUpperCase().equals("FINANCIERO")) {
            // Only the "admin" user is in the "admin" role
            return "Financiero";
        }        
        else if (role.toUpperCase().equals("GERENCIA")) {
        	// Only the "admin" user is in the "admin" role
        	return "Gerencia";
        }        
        // All users are in all non-admin roles
        return "";
    }

    @Override
    public String getPrincipalName() {
        return CurrentUser.get();
    }

	@Override
	public String getNombre() {
		return this.nombre;
	}

	@Override
	public String getFirma() {
		return this.firma;
	}

	@Override
	public String getVista() {
		return this.vista;
	}

	@Override
	public String getPantalla() {
		return this.pantalla;
	}

	@Override
	public String getDispositivo() {
		return this.dispositivo;
	}

	@Override
	public String getDepartamento() {
		return this.departamento;
	}

	@Override
	public String getPasswordCuenta() {
		return this.passwordCuenta;
	}

	@Override
	public String getUsuarioCuentaCorreo() {
		return this.usuarioCuentaCorreo;
	}

	@Override
	public String getCodigoGestion() {
		return this.codigoGestion;
	}

	@Override
	public boolean isVistaSemaforo() {
		return this.vistaSemaforo;
	}

	@Override
	public boolean isUsuarioAlerta() {
		return this.usuarioAlerta;
	}

	public boolean isSinMenu() {
		return sinMenu;
	}

	public void setSinMenu(boolean sinMenu) {
		this.sinMenu = sinMenu;
	}

	public boolean isAccesoCliente() {
		return accesoCliente;
	}

	public void setAccesoCliente(boolean accesoCliente) {
		this.accesoCliente = accesoCliente;
	}

}
