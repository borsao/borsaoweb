package borsao.e_borsao.modelo.Login;

import java.io.Serializable;

/**
 * Simple interface for authentication and authorization checks.
 */
public interface AccessControl extends Serializable {

    public boolean signIn(String username, String password);

    public boolean isUserSignedIn();

    public String isUserInRole(String role);

    public String getPrincipalName();
    
    public String getUsuario();
	public String getNombre();
	public String getFirma();
	public String getVista();
	public String getPantalla();
	public String getDispositivo();
	public String getDepartamento();
	public String getPasswordCuenta();
	public String getCodigoGestion();
	public String getUsuarioCuentaCorreo();
	public boolean isVistaSemaforo();
	public boolean isUsuarioAlerta();
	public boolean isSinMenu();
	public boolean isAccesoCliente();
}
