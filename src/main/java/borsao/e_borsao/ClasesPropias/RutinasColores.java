package borsao.e_borsao.ClasesPropias;

import java.awt.Color;

public class RutinasColores
{
	public RutinasColores()
	{
		
	}
	
	public static Integer convertirHexaRGB(String r_valor)
	{
		Integer colorDevolver= null;
		Color color = null;


		if (r_valor.charAt(0)=='#') r_valor=r_valor.substring(1, 7);
		
		int R = Integer.valueOf(r_valor.substring(0,2),16);
		int G = Integer.valueOf(r_valor.substring(2,4),16);
		int B = Integer.valueOf(r_valor.substring(4,6),16);
		
		color = new Color(R,G,B);
		
		colorDevolver=color.getRGB();
		
		return colorDevolver;
	}
}
