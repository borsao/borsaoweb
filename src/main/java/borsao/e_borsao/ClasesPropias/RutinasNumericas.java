package borsao.e_borsao.ClasesPropias;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class RutinasNumericas
{
	
	
	public RutinasNumericas()
	{
		
	}
	
	public static String formatearDouble(Double r_valor)
	{
		if (r_valor==null) return "";
		DecimalFormat format = new DecimalFormat();		
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);
		return format.format(r_valor);
	}

	public static String formatearDoubleDecimales(Double r_valor, Integer r_decimales)
	{
		if (r_valor==null) return "";
		DecimalFormat format = new DecimalFormat();		
		format.setMinimumFractionDigits(r_decimales);
		format.setMaximumFractionDigits(r_decimales);
		return format.format(r_valor);
	}

	public static Double formatearDoubleDecimalesDouble(Double r_valor, Integer r_decimales)
	{
		if (r_valor==null) return new Double(0);
		DecimalFormat format = new DecimalFormat();		
		format.setMinimumFractionDigits(r_decimales);
		format.setMaximumFractionDigits(r_decimales);
		return new Double(RutinasCadenas.reemplazarComaMiles(format.format(r_valor)));
	}

	public static String formatearDouble(String r_valor)
	{
		Double conversion=null;
		DecimalFormat format = new DecimalFormat();
		format.setMinimumFractionDigits(2);
		format.setMaximumFractionDigits(2);

		try {
			NumberFormat nf = NumberFormat.getInstance();
			conversion= nf.parse(r_valor).doubleValue();
			return (format.format(conversion));
		}
		catch (Exception pex)
		{
			return "";
		}
	}
	
	public static Double formatearDoubleDeESP(String r_valor)
	{
		Double conversion=null;

		try {
			NumberFormat nf = NumberFormat.getInstance();
			conversion= nf.parse(r_valor).doubleValue();
			return (conversion);
		}
		catch (Exception pex)
		{
			return null;
		}
	}		

	public static Integer formatearIntegerDeESP(String r_valor)
	{
		Integer conversion=null;

		try {
			NumberFormat nf = NumberFormat.getInstance();
			conversion= nf.parse(r_valor).intValue();
			return (conversion);
		}
		catch (Exception pex)
		{
			return null;
		}
	}		

	public static String formatearIntegerDigitos(Integer r_valor, Integer r_ceros)
	{
		String valor_resultante = "";
		String formato="";
		try {
		
			for(int i=1;i<=r_ceros;i++)
			{
				formato = formato + "0";
			}
			
			DecimalFormat nf = new DecimalFormat(formato);
			valor_resultante=nf.format(r_valor);
			return valor_resultante;
		}
		catch (Exception pex)
		{
			return null;
		}
				
	}
	public static String formatearIntegerDigitosIzqda(Integer r_valor, Integer r_ceros)
	{
		String valor_resultante = "";
		String formato="";
		try {
		
			for(int i=1;i<=r_ceros;i++)
			{
				formato =  "0" + formato;
			}
			
			DecimalFormat nf = new DecimalFormat(formato);
			valor_resultante=nf.format(r_valor);
			return valor_resultante;
		}
		catch (Exception pex)
		{
			return null;
		}
	}
	
	public static boolean esNumero(String r_valor, String r_tipo)
	{
		switch (r_tipo.toUpperCase())
		{
			case "INTEGER":
			{
				try
				{
					Integer valor = new Integer(r_valor);
					return true;
				}
				catch (Exception ex)
				{
					return false;
				}
			}
			default:
				return false;
		}
	}
}
