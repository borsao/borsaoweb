package borsao.e_borsao.ClasesPropias;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.vaadin.server.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.pantallaImagenesNoConformidades;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.ArticulosNuevosGrid;

public class CargaImagenes extends Ventana  
{
    private static final long serialVersionUID = -4292553844521293140L;
    
    private Button btnBotonCVentana = null;
    private Button btnBotonAVentana = null;
    private InputStream inStr = null;
    private Window origen =null;
    private Grid origenGrid =null;
    private String ruta = null;
    private String archivo = null;
    private ImageReceiver receiver =null;
    
    public CargaImagenes(String context, Window r_app, String r_ruta, String r_file)
    {
    	VerticalLayout layout = new VerticalLayout();
    	layout.setSizeFull();
    	this.origen=r_app;
    	this.setRuta(r_ruta + "/");
    	this.archivo=r_file;
    	
		this.setModal(true);
		this.setCaption("Pantalla carga imagenes");
		this.setClosable(true);
		this.setDraggable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("550px");
		
		this.comprobarTemporales();
		
        if ("basic".equals(context))
            basic(layout);
//        else if ("advanced".equals(context))
//            advanced(layout);
        else
            layout.addComponent(new Label("Invalid context: " + context));
        
        this.setContent(layout);
        this.cargarListeners();
    }

    public CargaImagenes(String context, Grid r_app, InputStream r_is)
    {
    	VerticalLayout layout = new VerticalLayout();
    	layout.setSizeFull();
    	this.inStr=r_is;    	
    	this.origenGrid=r_app;
    	this.setRuta(eBorsao.get().prop.baseImagePath);
		this.setModal(true);
		this.setCaption("Pantalla carga imagenes");
		this.setClosable(true);
		this.setDraggable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		this.removeAllCloseShortcuts();
		
        if ("basic".equals(context))
            basic(layout);
//        else if ("advanced".equals(context))
//            advanced(layout);
        else
            layout.addComponent(new Label("Invalid context: " + context));
        
        this.setContent(layout);
        this.cargarListeners();
    }

    public CargaImagenes(String context, Grid r_app, String r_ruta, String r_file)
    {
    	VerticalLayout layout = new VerticalLayout();
    	layout.setSizeFull();
    	this.setRuta(r_ruta + "/");
    	this.archivo=r_file;
    	this.origenGrid=r_app;
    	
		this.setModal(true);
		this.setCaption("Pantalla carga imagenes");
		this.setClosable(true);
		this.setDraggable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("550px");
		
		this.comprobarTemporales();
		
        if ("basic".equals(context))
            basic(layout);
//        else if ("advanced".equals(context))
//            advanced(layout);
        else
            layout.addComponent(new Label("Invalid context: " + context));
        
        this.setContent(layout);
        this.cargarListeners();
    }
    
    public void basic(VerticalLayout layout) 
    {
    	File fil = null;
//		VerticalLayout controles = new VerticalLayout();
//		controles.setMargin(true);
//		controles.setSpacing(true);
		
	    	Panel panel = new Panel("Seleccionar Imagen");
	    	
	    		VerticalLayout panelContent = new VerticalLayout();
	    		panelContent.setMargin(true);
	    		// Show uploaded file in this placeholder
	    			Image imagenh1 = new Image("Imagen Cargada");
	    			
	    			if (this.archivo!=null)
    				{
	    				fil = new File(this.archivo);
	    				if (fil.exists()) imagenh1.setSource(new FileResource(fil)); 
	    				imagenh1.setVisible(true);
    				}
	    			else if (this.inStr!=null)
	    			{
	    				FileOutputStream fw=null;
						try 
						{
							String rutaArchivo = eBorsao.get().prop.baseImagePath + "Img"+ RutinasFechas.horaActualSinSeparador() + ".jpg";
							
							this.inStr.reset();
							
							fw = new FileOutputStream(rutaArchivo);
							// Bucle de lectura del blob y escritura en el fichero, de 1024
							// en 1024 bytes.
							byte bytes[] = new byte[1024];
							int leidos = this.inStr.read(bytes);
							while (leidos > 0) {
								fw.write(bytes);
								leidos = this.inStr.read(bytes);
							}
							fw.close();
							this.inStr.close();
							
							
							fil = new File(rutaArchivo);
							imagenh1.setSource(new FileResource(fil));
							imagenh1.setVisible(true);
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}

	    				
	    			}
	    			else
	    			{
	    				archivo=null;
	    				imagenh1.setVisible(false);
	    			}
	    			imagenh1.setWidth(500, Unit.PIXELS);	
	
	    			// Implement both receiver that saves upload in a file and
	    			// listener for successful upload
	        
	    			receiver = new ImageReceiver(imagenh1,this.getRuta()); 
	
	    			// Create the upload with a caption and set receiver later
	    			final Upload upload = new Upload(null, receiver);
	    			upload.setButtonCaption("Proccesar");
	    			upload.addSucceededListener(receiver);
	    			
	    		// Put the components in a panel
	    		panelContent.addComponents(upload, imagenh1);
	
	        panel.setContent(panelContent);
	
	        ((VerticalLayout) panel.getContent()).setSpacing(true);
	        panel.setWidth("-1");        
	        
	        HorizontalLayout botonera = new HorizontalLayout();
			botonera.setMargin(true);
			botonera.setWidthUndefined();
			botonera.setHeight("100px");
			
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
				btnBotonCVentana = new Button("Salir");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
			botonera.addComponent(btnBotonAVentana);
			botonera.addComponent(btnBotonCVentana);

		layout.addComponent(panel);
		layout.addComponent(botonera);
		
//        layout.addComponent(controles);
        
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.setHeight("-1");
    }
    
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if ((archivo!=null && !archivo.equals(receiver.getFilePath())) || receiver.getFile()!=null)
				{
					receiver.getFile().delete();
				}
				close();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				/*
				 * cogemos la ruta completa de la imagen y lo insertaremos
				 */
//				if (receiver.getFilePath()!=null) Notificaciones.getInstance().mensajeAdvertencia(receiver.getFilePath());
				
				if (archivo!=null && !archivo.equals(receiver.getFilePath())) 
				{
					File f = new File(archivo);
					f.delete();
				}
				else if (receiver.getFilePath()!=null && receiver.image.getAlternateText()==null)
				{
					receiver.getFile().delete();
				}

				if (receiver !=null && receiver.getFile()!=null && receiver.getFile().getName()!=null)
				{
					ejecutarAccion("Actualizar", receiver.getFile().getName()); 
				}
				close();
			}
		});
	}
	
    private void comprobarTemporales()
    {
        // Create uploads directory
        File uploads = new File(this.getRuta());
        if (!uploads.exists() && !uploads.mkdir())
        	Notificaciones.getInstance().mensajeError("No se ha podido crear la carpeta de cargas temporal: " + this.getRuta());
    }
	
	private void ejecutarAccion(String r_accion, String r_archivo)
	{
		FileInputStream fils=null;;
		
		if (this.origen!=null && this.origen instanceof pantallaImagenesNoConformidades)
		{
			((pantallaImagenesNoConformidades) origen).ejecutarAccion(r_accion, r_archivo);
		}
		if (this.origenGrid!=null && this.origenGrid instanceof ArticulosNuevosGrid)
		{
			try 
			{
				fils = new FileInputStream(receiver.file);
				((ArticulosNuevosGrid) origenGrid).recuperarImagen((InputStream) fils);
				fils.close();
				receiver.getFile().delete();
			}
			catch (FileNotFoundException ffex)
			{
				Notificaciones.getInstance().mensajeError("Error al procesar la imagen");
			}
			catch (Exception e) 
			{
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Se va a eliminar la foto asociada. Continuamos?", "Si", "No", "eliminarFoto", "");
				getUI().addWindow(vt);
			}
		}
	}
	
	private String getRuta() {
		return ruta;
	}
	
	private void setRuta(String ruta) {
		this.ruta = ruta;
	}

	@Override
	public void aceptarProceso(String r_accion) {

		switch(r_accion)
		{
			case "eliminarFoto":
			{
				((ArticulosNuevosGrid) origenGrid).recuperarImagen(null);
			}
		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		// TODO Auto-generated method stub
		
	}
}