package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.VerticalLayout;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SelectorFechaHora extends VerticalLayout
{
	 private InlineDateField dateField;
	    private NativeSelect hourPicker;
	    private NativeSelect minutePicker;

	    public SelectorFechaHora(String caption) {
	        setSizeUndefined();

	        dateField = new InlineDateField(caption);
	        hourPicker = new NativeSelect();
	        hourPicker.setNullSelectionAllowed(false);
	        hourPicker.setNewItemsAllowed(false);
	        minutePicker = new NativeSelect();
	        minutePicker.setNullSelectionAllowed(false);
	        minutePicker.setNewItemsAllowed(false);
	        
	        HorizontalLayout hourLayout = new HorizontalLayout();
	        hourLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
	        Label colonLabel = new Label("<b>:</b>");
	        colonLabel.setContentMode(ContentMode.HTML);
	        hourLayout.addComponents(hourPicker, colonLabel, minutePicker);

	        addComponents(dateField, hourLayout);
	        setComponentAlignment(hourLayout, Alignment.MIDDLE_CENTER);

	        setValues();
	    }

	    private void setValues() {
	        hourPicker.setContainerDataSource(new BeanItemContainer<>(String.class, new ArrayList<>(Arrays.asList("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21", "22", "23"))));
	        minutePicker.setContainerDataSource(new BeanItemContainer<>(String.class, new ArrayList<>(Arrays.asList("00", "15", "30", "45"))));

	    	dateField.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaActual()));
	    	
	        hourPicker.select(RutinasFechas.horaFecha(new Date()));
	        minutePicker.select("00");
	    }

	    public Date getValue() {
	        
	    	GregorianCalendar date = new GregorianCalendar();
	    	date.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
	    	date.setTime(dateField.getValue());
	    	date.add(java.util.Calendar.HOUR, Integer.valueOf(hourPicker.getValue().toString()));
	    	date.add(java.util.Calendar.MINUTE, Integer.valueOf(minutePicker.getValue().toString()));

	        // set hour and minutes to date

	        return date.getTime();
	    }

	    public void setValue(Date r_valor)
	    {
	    	this.dateField.setValue(RutinasFechas.conversion(r_valor));
	    	this.hourPicker.setValue(RutinasFechas.horaFecha(r_valor));
	    	this.minutePicker.setValue(RutinasFechas.minutoFecha(r_valor));
	    }
}