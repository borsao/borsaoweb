package borsao.e_borsao.ClasesPropias;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.EventClick;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoCalendario;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.CalendarioView;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaEventoAgenda extends Window 
{
	private String accion=null;
	private CalendarioView calendario = null;
	private String propietario = null;
	private String almacen = null;
	private EventClick eventoEditado = null;
	private AgendaEvento ev = null;
	private Agenda ag = null;
	private Date rangoFechas = null;
	private boolean isSoloLectura = true;
	
	public TextFieldNumero cantidad= null;
	public TextFieldNumero numero = null;
	public TextArea txtObservaciones=null;
	public TextField titulo= null;
	public SelectorFechaHora fecha = null;
	public CheckBox todoDia = null;
	public Combo intervalo = null;
	public CheckBox mostrarAviso = null;
	public Combo empleado= null;
	public Combo area= null;
	public Combo tipo= null;
	public Combo aviso=null;
	public Date fechaAviso = null;
	public DateField fechaFin = null;
	public TextField ejercicioContable = null;
	
    public Button opcNuevo = null;
    public Button opcEliminar = null;
    public Button opcSalir = null;
    
	private HorizontalLayout topLayout = null;
	private VerticalLayout principal = null;
	
	private HorizontalLayout panelEvento = null;
	private VerticalLayout panelEvento2 = null;
	private HorizontalLayout panelEvento3 = null;
	private HorizontalLayout panelDuracion = null;
	private HorizontalLayout panelFrecuencia = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaEventoAgenda(CalendarioView r_app)
    {
    	this.calendario = r_app;
    	this.ag=r_app.agen;
    	this.isSoloLectura=this.ag.isSoloLectura;
    	this.accion="Nuevo";
    	this.propietario=ag.identificadorCalendario;
    	this.almacen=ag.identificadorAlmacen;
    	
		this.setCaption("");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("900px");
		this.setHeight("550px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		this.habilitarBotones();
		this.cargarListeners();
    }

	public PantallaEventoAgenda(CalendarioView r_app, String r_accion, Agenda r_agenda, Date r_rango)
    {
		this.calendario=r_app;
    	this.rangoFechas=r_rango;
    	this.ag = r_agenda;
    	this.isSoloLectura=this.ag.isSoloLectura;
    	this.accion=r_accion;
    	this.propietario=ag.identificadorCalendario;
    	this.almacen=ag.identificadorAlmacen;
    	
		this.setCaption("");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("900px");
		this.setHeight("550px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		this.habilitarBotones();
		this.cargarListeners();
    }

	public PantallaEventoAgenda(CalendarioView r_app, boolean r_soloLectura, String r_propietario, String r_almacen, EventClick r_evento)
    {
    	this.eventoEditado=r_evento;
    	this.calendario=r_app;
    	this.ev = (AgendaEvento) this.eventoEditado.getCalendarEvent();
    	this.propietario=r_propietario;
    	this.almacen=r_almacen;
    	this.accion="Editar";
    	this.isSoloLectura=r_soloLectura;
    	
		this.setCaption("");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("900px");
		this.setHeight("650px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		this.cargarListeners();
		this.habilitarBotones();
		
		this.rellenarEntradasDatos(r_evento);
    }

	public PantallaEventoAgenda(CalendarioView r_app, String r_accion, Agenda r_agenda, AgendaEvento r_evento)
    {
		this.calendario=r_app;
    	this.ev=r_evento;
    	this.ag=r_agenda;
    	this.isSoloLectura=this.ag.isSoloLectura;
    	this.accion=r_accion;
    	this.propietario=ag.identificadorCalendario;
    	this.almacen=ag.identificadorAlmacen;
    	
		this.setCaption("");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("900px");
		this.setHeight("650px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		this.cargarListeners();
		this.habilitarBotones();
		
		this.rellenarEntradasDatos(null);
    }

    public void cargarPantalla() 
    {
		principal = new VerticalLayout();
//		principal.setSizeFull();
		principal.setMargin(true);
		principal.setSpacing(true);
		
		this.topLayout = new HorizontalLayout();
    	this.topLayout.setSpacing(true);
//    	this.topLayout.setMargin(true);
    	
	    	this.opcNuevo= new Button("Guardar");
	    	this.opcNuevo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	this.opcNuevo.setIcon(FontAwesome.SAVE);
	    	this.opcNuevo.setEnabled(false);

	    	this.opcEliminar= new Button("Eliminar");
	    	this.opcEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
	    	this.opcEliminar.setIcon(FontAwesome.REMOVE);
	    	this.opcEliminar.setEnabled(false);

	    	this.opcSalir= new Button("Salir");
	    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
	
    	this.topLayout.addComponent(this.opcNuevo);
    	this.topLayout.addComponent(this.opcEliminar);
    	this.topLayout.addComponent(this.opcSalir);
    	
	    	this.panelEvento = new HorizontalLayout();
//	    	this.panelEvento.setCaption("Evento");
	    	this.panelEvento.setSpacing(true);
//	    	this.panelEvento.setMargin(true);

	    		this.fecha = new SelectorFechaHora("Fecha");
		    	this.fecha.setEnabled(true);
		    	if (this.rangoFechas!=null) this.fecha.setValue(this.rangoFechas);
	
		    	this.panelEvento2 = new VerticalLayout();
		    	this.panelEvento2.setSpacing(true);

			    	this.titulo=new TextField();
			    	this.titulo.setEnabled(true);
			    	this.titulo.setWidth("200px");
 			    	
			    	this.empleado=new Combo();
			    	this.empleado.setEnabled(true);
			    	this.empleado.setVisible(true);
			    	this.empleado.setNullSelectionAllowed(false);
			    	this.empleado.setNewItemsAllowed(false);
			    	this.empleado.setWidth("250px");
			    	this.empleado.setCaption("Empleado");

			    	this.panelEvento3 = new HorizontalLayout();
			    	this.panelEvento3.setSpacing(true);
			    	
				    	this.area=new Combo();
				    	this.area.setEnabled(true);
				    	this.area.setVisible(true);
				    	this.area.setNullSelectionAllowed(false);
				    	this.area.setNewItemsAllowed(false);
				    	this.area.setWidth("145px");
				    	this.area.setCaption("Area");

				    	this.tipo=new Combo();
				    	this.tipo.setEnabled(true);
				    	this.tipo.setVisible(true);
				    	this.tipo.setNullSelectionAllowed(false);
				    	this.tipo.setNewItemsAllowed(false);
				    	this.tipo.setWidth("145px");
				    	this.tipo.setCaption("Tipo");
				    	
				    	this.cargarCombo();

				    	this.ejercicioContable=new TextField();
				    	this.ejercicioContable.setEnabled(true);
				    	this.ejercicioContable.setWidth("200px");
				    	this.ejercicioContable.setCaption("Año Vacaciones");

				    	
				    	this.panelEvento3.addComponent(this.ejercicioContable);
				    	this.panelEvento3.setComponentAlignment(this.ejercicioContable, Alignment.BOTTOM_LEFT);
				    	this.panelEvento3.addComponent(this.tipo);
				    	this.panelEvento3.addComponent(this.area);
				    	
					this.txtObservaciones=new TextArea();
					this.txtObservaciones.setEnabled(true);
					this.txtObservaciones.setWidth("500px");
					this.txtObservaciones.setCaption("Detalle");
	
					this.panelDuracion = new HorizontalLayout();
			    	this.panelDuracion.setSpacing(true);
				    	
				    	this.todoDia = new CheckBox("Todo el día");
				    	this.todoDia.setEnabled(true);
				    	this.todoDia.setWidth("100px");

				    	this.numero=new TextFieldNumero();
						this.numero.setEnabled(true);
						this.numero.setWidth("100px");
						this.numero.addStyleName("rightAligned");
						this.numero.setCaption("Duracion (Horas)");

				    	this.mostrarAviso = new CheckBox("Aviso");
				    	this.mostrarAviso.setEnabled(true);
				    	this.mostrarAviso.setWidth("75px");
				    	
				    	this.aviso = new Combo();
				    	this.aviso.setEnabled(false);
				    	this.aviso.setWidth("150px");
				    	this.aviso.addItem("5 minutos");
				    	this.aviso.addItem("15 minutos");
				    	this.aviso.addItem("30 minutos");
				    	this.aviso.addItem("1 Hora");
				    	this.aviso.addItem("2 Horas");
				    	this.aviso.addItem("1 Semana");
				    	this.aviso.addItem("2 Semanas");
				    	this.aviso.setValue("5 minutos");
				    	
					this.panelDuracion.addComponent(this.todoDia);
					this.panelDuracion.setComponentAlignment(this.todoDia, Alignment.MIDDLE_LEFT);
					this.panelDuracion.addComponent(this.numero);
					this.panelDuracion.setComponentAlignment(this.numero, Alignment.TOP_LEFT);
					this.panelDuracion.addComponent(this.mostrarAviso);
					this.panelDuracion.setComponentAlignment(this.mostrarAviso, Alignment.MIDDLE_LEFT);
					this.panelDuracion.addComponent(this.aviso);
					this.panelDuracion.setComponentAlignment(this.aviso, Alignment.MIDDLE_LEFT);
					
				if (this.calendario.tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
				{
					this.empleado.setCaption("Nombre Empleado");
					this.panelEvento2.addComponent(this.empleado);
					this.panelEvento2.addComponent(this.panelEvento3);
				}
		    	else
		    	{
		    		this.titulo.setCaption("Titulo");
		    		this.panelEvento2.addComponent(this.titulo);
		    	}
			    	
				this.panelEvento2.addComponent(this.txtObservaciones);
				this.panelEvento2.addComponent(this.panelDuracion);
			
			this.panelEvento.addComponent(this.fecha);
			this.panelEvento.addComponent(this.panelEvento2);
			
			this.panelFrecuencia = new HorizontalLayout();
//			this.panelFrecuencia.setCaption("Frecuencia");
//			this.panelFrecuencia.addStyleName("miPanel");
//			this.panelFrecuencia.setStyleName("miPanel");
			this.panelFrecuencia.setSpacing(true);
//			this.panelFrecuencia.setMargin(true);
			
				this.cantidad=new TextFieldNumero();
				this.cantidad.setEnabled(true);
				this.cantidad.setCaption("Repeticiones");
				this.cantidad.addStyleName("rightAligned");
				this.cantidad.setValue("0");
				
				this.fechaFin= new DateField("Fecha Finalizacion");
				this.fechaFin.setEnabled(true);
				
		    	this.intervalo = new Combo("Periodicidad");
		    	this.intervalo.setEnabled(true);
		    	this.intervalo.setNullSelectionAllowed(false);
		    	this.intervalo.setNewItemsAllowed(false);
		    	this.intervalo.setWidth("150px");
		    	
		    	this.intervalo.addItem("1 Dia");
		    	this.intervalo.addItem("1 Semana");
		    	this.intervalo.addItem("2 Semanas");
		    	this.intervalo.addItem("3 Semanas");		    	
		    	this.intervalo.addItem("1 Mes");
		    	this.intervalo.addItem("3 Meses");
		    	this.intervalo.addItem("4 Meses");
		    	this.intervalo.addItem("6 Meses");
		    	this.intervalo.addItem("1 Año");

			this.panelFrecuencia.addComponent(this.cantidad);
			this.panelFrecuencia.addComponent(this.intervalo);
			this.panelFrecuencia.addComponent(this.fechaFin);
			this.panelFrecuencia.setComponentAlignment(this.intervalo, Alignment.BOTTOM_LEFT);
		
    	principal.addComponent(this.topLayout);
    	principal.addComponent(this.panelEvento);
    	principal.addComponent(this.panelFrecuencia);
    	
    }

    private void cargarListeners()
    {

//    	this.fecha.addValueChangeListener(new ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				hora.setValue(fecha.getValue());
//			}
//		});
    	
    	this.todoDia.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (todoDia.getValue())
				{
					numero.setEnabled(false);					
				}
				else
				{
					numero.setEnabled(true);
				}
			}
		});
    	
    	this.mostrarAviso.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (mostrarAviso.getValue())
				{
					aviso.setEnabled(true);					
				}
				else
				{
					aviso.setEnabled(false);
				}
			}
		});
    	
    	this.opcNuevo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			newForm();
    		}
    	});

    	this.opcEliminar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eliminarEvento();
    		}
    	});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			close();
    		}
    	});
    	
    }
    
	private void newForm() 
	{
		Integer duracion=null;
		Integer repeticiones=0;
		

		if (this.ag!=null && this.accion.equals("Nuevo"))
		{
			if (this.cantidad.getValue()!=null && Integer.valueOf(this.cantidad.getValue())!=0)
			{
				repeticiones = Integer.valueOf(this.cantidad.getValue());
			}
			else if (this.fechaFin.getValue()!=null)
			{
				GregorianCalendar endDate = new GregorianCalendar();
				endDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				endDate.setTime(this.fechaFin.getValue());
				endDate.add(java.util.Calendar.HOUR, 24);
				Integer tiempo = 23;
				endDate.add(java.util.Calendar.HOUR, tiempo);

				repeticiones = RutinasFechas.diferenciaEnDias2(endDate.getTime(), this.fecha.getValue());
			}
			
			for (int i=0;i<repeticiones+1;i++)
			{

				int intervalo = 1;
				this.calcularAviso();
				
				GregorianCalendar startDate = new GregorianCalendar();
				startDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				startDate.setTime(this.fecha.getValue());

				if (this.intervalo.getValue()!=null)
				{
					if (this.intervalo.getValue().toString().contentEquals("1 Dia"))
					{
						intervalo = 1;
					}
					else if (this.intervalo.getValue().toString().contentEquals("1 Semana"))
					{
						intervalo = 7;
					}
					else if (this.intervalo.getValue().toString().contentEquals("2 Semanas"))
					{
						intervalo = 14;
					}
					else if (this.intervalo.getValue().toString().contentEquals("3 Semanas"))
					{
						intervalo = 21;
					}
					else if (this.intervalo.getValue().toString().contentEquals("1 Mes"))
					{
						intervalo = 30;
					}
					else if (this.intervalo.getValue().toString().contentEquals("2 Meses"))
					{
						intervalo = 60;
					}
					else if (this.intervalo.getValue().toString().contentEquals("3 Meses"))
					{
						intervalo = 90;
					}
					else if (this.intervalo.getValue().toString().contentEquals("4 Meses"))
					{
						intervalo = 120;
					}
					else if (this.intervalo.getValue().toString().contentEquals("6 Meses"))
					{
						intervalo = 180;
					}
					else if (this.intervalo.getValue().toString().contentEquals("1 Año"))
					{
						intervalo = 365;
					}
				}
				
				Integer tiempo = i * 24 * intervalo;
				
				startDate.add(java.util.Calendar.HOUR, tiempo);
				
				if (!this.todoDia.getValue() && this.numero.getValue()!=null && this.numero.getValue()!="")
				{
					duracion = new Integer(this.numero.getValue());
				}
				else
				{
					duracion=1;
				}
				
				String area  = null;
				if (this.area.isVisible()) area = this.area.getValue().toString();
				
				if (this.calendario.tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
					this.ag.añadirEvento(this.propietario, this.empleado.getValue().toString(), this.txtObservaciones.getValue(), startDate.getTime() , duracion ,this.todoDia.getValue(),this.fechaAviso, this.mostrarAviso.getValue(), this.aviso.getValue().toString(), area, this.almacen,true, tipo.getValue().toString(), ejercicioContable.getValue());
		    	else
		    		this.ag.añadirEvento(this.propietario, this.titulo.getValue(), this.txtObservaciones.getValue(), startDate.getTime() , duracion ,this.todoDia.getValue(),this.fechaAviso, this.mostrarAviso.getValue(), this.aviso.getValue().toString(), area, this.almacen,true, tipo.getValue().toString(), ejercicioContable.getValue());
		    	
			}
		}
		else
		{
			this.guardarEvento();
		}
		close();
	}

	private void guardarEvento()
	{
		/*
		 * Rellenaremos el mapeoEvento y lo mandaremos a la base de datos
		 */
		Integer duracion = 1;
		MapeoCalendario mapeo = new MapeoCalendario();
		
		this.calcularAviso();

		if (this.propietario.equals("Personal"))
		{
			this.ev.setPropietario(eBorsao.get().accessControl.getNombre());
		}
		else
		{
			this.ev.setPropietario(this.propietario);
		}
		if (this.area.getValue()!=null) this.ev.setAlmacen(this.area.getValue().toString());
		else
			this.ev.setAlmacen(this.almacen);
		
		if (this.calendario.tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
			this.ev.setCaption(this.empleado.getValue().toString());
    	else
    		this.ev.setCaption(this.titulo.getValue());

		this.ev.setDescription(this.txtObservaciones.getValue());
		this.ev.setAllDay(todoDia.getValue());
		this.ev.setEjercicioContable(this.ejercicioContable.getValue().toString());
		this.ev.setValorAviso(this.aviso.getValue().toString());
		this.ev.setAviso(this.mostrarAviso.getValue());
		this.ev.setAviso(fechaAviso);
		if (this.area.getValue()!=null) this.ev.setArea(this.area.getValue().toString());
		if (this.tipo.getValue()!=null) this.ev.setTipo(this.tipo.getValue().toString());
		

		if (!this.todoDia.getValue() && this.numero.getValue()!=null && this.numero.getValue()!="")
		{
			duracion = new Integer(this.numero.getValue());
		}

    	GregorianCalendar start = new GregorianCalendar();
    	start.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	start.setTime(this.fecha.getValue());
    	
    	GregorianCalendar end = new GregorianCalendar();
    	end.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	end.setTime(start.getTime());
    	end.add(java.util.Calendar.HOUR, duracion);
		
    	this.ev.setStart(start.getTime());
    	this.ev.setEnd(end.getTime());

    	if (this.eventoEditado!=null)
    	{
    		this.eventoEditado.getComponent().markAsDirty();
    	}
    	else
    	{
    		if (this.ag!=null) this.ag.markAsDirty();
    	}
    	
    	if (this.ev.getId()!= null) mapeo.setId(new Integer(this.ev.getId()));
    	
    	mapeo.setPropietario(this.ev.getPropietario());
    	mapeo.setAlmacen(this.ev.getAlmacen());
    	
//		if (this.propietario.equals("Turnos"))
//		{
//			mapeo.setAlmacen(this.area.getValue().toString());
//		}
//		else
//		{
//			mapeo.setAlmacen(this.almacen);
//		}
    	if (this.calendario.tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
			mapeo.setCaption(this.empleado.getValue().toString());
    	else
    		mapeo.setCaption(this.titulo.getValue());

    	mapeo.setDescription(this.txtObservaciones.getValue());
    	mapeo.setAllDay(todoDia.getValue());
    	mapeo.setValorAviso(this.aviso.getValue().toString());
    	mapeo.setAviso(this.mostrarAviso.getValue());
    	mapeo.setStyleName(this.ev.getStyleName());
    	mapeo.setAviso(fechaAviso);
    	mapeo.setEjercicioContable(this.ejercicioContable.getValue());
		if (this.area.getValue()!=null) mapeo.setArea(this.area.getValue().toString());
		if (this.tipo.getValue()!=null) mapeo.setTipo(this.tipo.getValue().toString());
		mapeo.setStart(start.getTime());
		mapeo.setEnd(end.getTime());

		guardarCambiosBBDD(mapeo);
		
		close();
	}

	public void guardarCambiosBBDD(MapeoCalendario r_mapeo)
	{
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());

		ccs.guardarCambios(r_mapeo);
	}
	
	private void eliminarEvento()
	{
		/*
		 * Rellenaremos el mapeoEvento y lo mandaremos a la base de datos
		 */
		AgendaEvento e = (AgendaEvento) this.eventoEditado.getCalendarEvent();

		eliminarDatosBBDD(e);
		
		e.setCaption(null);
		e.setDescription(null);
		e.setStart(null);
		e.setEnd(null);
		e.setAviso(null);
		
		this.eventoEditado.getComponent().markAsDirty();
		
		close();
	}

	private void rellenarEntradasDatos(EventClick r_event)
	{
		AgendaEvento e = null;
		if (r_event!=null)
		{
			e = (AgendaEvento) r_event.getCalendarEvent();
		}
		else
		{
			e=this.ev;
		}
		this.fecha.setValue(e.getStart());
		if (this.calendario.tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
		{
			this.empleado.setValue(e.getCaption());
			this.ejercicioContable.setValue(e.getEjercicioContable());
			this.tipo.setValue(e.getTipo());
		}
    	else
    		this.titulo.setValue(e.getCaption());

		this.txtObservaciones.setValue(e.getDescription());
		this.todoDia.setValue(e.isAllDay());
		this.numero.setEnabled(!e.isAllDay());
		this.mostrarAviso.setValue(e.isAviso());
		this.aviso.setValue(e.getValorAviso());
		this.ejercicioContable.setValue(e.getEjercicioContable());
		this.area.setValue(e.getArea());
		this.tipo.setValue(e.getTipo());
		
		if (!e.isAllDay())
		{
			Long duracion = RutinasFechas.restarFechasHoras(e.getStart(),e.getEnd());
			this.numero.setValue(duracion.toString());
		}
	}
	
	public void destructor()
	{
	}

	private void eliminarDatosBBDD(AgendaEvento r_evento)
	{
		
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());
		ccs.eliminar(Integer.valueOf(r_evento.getId()));

	}
	
	private void habilitarBotones()
	{
		if (this.isSoloLectura==false)
		{
			this.opcNuevo.setEnabled(true);
			this.opcEliminar.setEnabled(true);
 		}
		if (this.propietario.equals("Personal"))
		{
			this.panelEvento3.setVisible(false);
//			this.ejercicioContable.setVisible(false);
		}
	}

	private void calcularAviso()
	{
		String formulaAviso=null;
		if (this.mostrarAviso.getValue())
		{
			GregorianCalendar avisoCalculado = new GregorianCalendar();
			avisoCalculado.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
			avisoCalculado.setTime(this.fecha.getValue());
			
			formulaAviso=this.aviso.getValue().toString();
			switch (formulaAviso)
			{
				case "5 minutos":
				{
			    	avisoCalculado.add(java.util.Calendar.MINUTE, -5);
					break;
				}
				case "15 minutos":
				{
					avisoCalculado.add(java.util.Calendar.MINUTE, -15);
					break;
				}
				case "30 minutos":
				{
					avisoCalculado.add(java.util.Calendar.MINUTE, -30);
					break;
				}
				case "1 Hora":
				{
					avisoCalculado.add(java.util.Calendar.HOUR, -1);
					break;
				}
				case "2 Horas":
				{
					avisoCalculado.add(java.util.Calendar.HOUR, -2);
					break;
				}
				case "1 Semana":
				{
					avisoCalculado.add(java.util.Calendar.HOUR, -168);
					break;
				}
				case "2 Semanas":
				{
					avisoCalculado.add(java.util.Calendar.HOUR, -336);
					break;
				}
			}
					
			fechaAviso=avisoCalculado.getTime();
		}
		else
		{
			fechaAviso=null;
		}
	}
	@Override
	public void close()
	{
		if (this.calendario!=null)
		{
			this.calendario.botonesGenerales(true);
			this.calendario.opcImprimir.setEnabled(true);
		}
		super.close();
	}
	
	private void cargarCombo()
	{
		this.empleado.removeAllItems();
		this.area.removeAllItems();
		this.tipo.removeAllItems();
		
		consultaOperariosServer cops = consultaOperariosServer.getInstance(CurrentUser.get());
		HashMap<Integer, String> operarios = cops.datosOperarios();

		for (int i=0; i<operarios.size(); i++)
		{
			this.empleado.addItem(operarios.get(i));
		}
		
		if (this.propietario=="Almacen")
		{
			this.area.addItem("Carga");
			this.area.addItem("Descarga");
		}
		else if (this.propietario=="Turnos")
		{
			this.area.addItem("Embotelladora");
			this.area.addItem("Envasadora");
			this.area.addItem("BIB");
		}
		else if (this.propietario=="Vacaciones")
		{
			this.area.addItem("Generales");
			this.area.addItem("Almacen");
			this.area.addItem("Bodega");
			this.area.addItem("Limpieza");
			this.area.addItem("Laboratorio");
			this.area.addItem("Tecnicos");
			this.area.addItem("Embotelladora");
			this.area.addItem("Envasadora");
			this.area.addItem("BIB");
			
			this.tipo.addItem("Vacaciones");
			this.tipo.addItem("Permiso Retribuido");
			this.tipo.addItem("Cambio Sabado");
		}

	}
}

