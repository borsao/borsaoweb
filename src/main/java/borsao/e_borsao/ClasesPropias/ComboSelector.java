package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;

public class ComboSelector extends Panel
{
	public Combo cmb = null;
	public Button btn = null;
	
	public ComboSelector()
	{
		this.cmb=new Combo();
		this.cmb.addItem("S");
		this.cmb.addItem("N");
		this.cmb.setWidth("150px");
		this.cmb.setCaption("Prueba");
		
		this.btn=new Button("...");
		this.btn.setWidth("50px");
		
        HorizontalLayout h = new HorizontalLayout();
        h.setWidth("200px");
        h.setHeight("80px");
        h.addComponent(this.cmb);
        h.addComponent(this.btn);
        h.setComponentAlignment(this.cmb,Alignment.TOP_LEFT);
        h.setComponentAlignment(this.btn,Alignment.MIDDLE_RIGHT);
        
        this.setContent(h);
	}
	
}