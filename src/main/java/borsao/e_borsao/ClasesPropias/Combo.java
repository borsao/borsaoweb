package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.ComboBox;

public class Combo extends ComboBox
{
	public Combo()
	{
		this.setNewItemsAllowed(false);
		this.addStyleName("combo");
	}
	
	public Combo(String r_caption)
	{
		this.setCaption(r_caption);
		this.setNewItemsAllowed(false);
		this.addStyleName("combo");
	}
}