package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.Window;

public abstract class Ventana extends Window
{
	public abstract void aceptarProceso(String r_accion);
	public abstract void cancelarProceso(String r_accion);
}