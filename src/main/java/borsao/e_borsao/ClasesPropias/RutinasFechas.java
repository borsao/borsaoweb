package borsao.e_borsao.ClasesPropias;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class RutinasFechas
{
	public RutinasFechas()
	{
		
	}
	
	public static int restarFechas(Date r_inicial, Date r_final)
	{
		int dias=(int) ((r_final.getTime()-r_inicial.getTime())/86400000);
		
		return dias;
	}

	public static long restarFechasHoras(Date r_inicial, Date r_final)
	{
		long horas = (long) ((r_final.getTime()-r_inicial.getTime())/3600000);
		return horas;
	}

	public static long restarFechasMinutos(Date r_inicial, Date r_final)
	{
		long horas = (long) ((r_final.getTime()-r_inicial.getTime())/60000);
		return horas;
	}

	public static int obtenerDiaDeLaSemana(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(d);
		return cal.get(Calendar.DAY_OF_WEEK);		
	}

	public static int obtenerDiasDelAño(Date d)
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate ld = LocalDate.parse(RutinasFechas.convertirDateToString(d), dtf);
		int yearDays  = ld.lengthOfYear();
		return yearDays;		
	}
	
	
	public static String obtenerDiaDeLaSemanaLetra(Date d){
		String[] dias={"Domingo","Lunes","Martes","Miercoles", "Jueves","Viernes","Sabado"};
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.SUNDAY);
		cal.setTime(d);
		int dia = cal.get(Calendar.DAY_OF_WEEK);
		return dias[dia-1];
	}

	public static String obtenerMesLetra(int r_mes){
		String[] meses={"Enero","Febrero","Marzo","Abril", "Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		return meses[r_mes-1];
	}
	
	public static int obtenerSemanaFecha(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(d);
		return cal.get(Calendar.WEEK_OF_YEAR);		
	}
	
	public static String primerDiaSemana(int r_ejercicio, int r_semana)
	{
		String rdo = null;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.MONDAY);

		cal.clear();
		cal.set(Calendar.WEEK_OF_YEAR, r_semana);
		cal.set(Calendar.YEAR, r_ejercicio);

		Date fecha = cal.getTime();
		rdo = RutinasFechas.convertirDateToString(fecha);
		return rdo;
	}
	
	public static String ultimoDiaSemana(int r_ejercicio, int r_semana)
	{
		String rdo = null;
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.MONDAY);

		cal.clear();
		cal.set(Calendar.WEEK_OF_YEAR, r_semana);
		cal.set(Calendar.YEAR, r_ejercicio);
		cal.add(Calendar.DAY_OF_YEAR, 6);
		Date fecha = cal.getTime();
		rdo = RutinasFechas.convertirDateToString(fecha);
		return rdo;
	}

	public static String obtenerFranjaFecha(Date d){
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.setTime(d);
		if (cal.get(Calendar.HOUR_OF_DAY)>=6 && cal.get(Calendar.HOUR_OF_DAY)<14) return "1";  
		else if (cal.get(Calendar.HOUR_OF_DAY)>=14 && cal.get(Calendar.HOUR_OF_DAY)<22) return "2";
		else if (cal.get(Calendar.HOUR_OF_DAY)>=22 && cal.get(Calendar.HOUR_OF_DAY)<24) return "3";
		else if (cal.get(Calendar.HOUR_OF_DAY)>=0 && cal.get(Calendar.HOUR_OF_DAY)<6) return "3";
		return "";
	}

	public static int semanaActual(String r_ejercicio)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setMinimalDaysInFirstWeek( 4 );
		int numberWeekOfYear=1;
		
		calendar.setTime(new Date());
		numberWeekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

		return numberWeekOfYear;
	}
	
	public static int semanasAño(String r_ejercicio)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setMinimalDaysInFirstWeek( 4 );
		if (r_ejercicio==null)
		{
			calendar.setTime(new Date());					
		}
		else
		{
			calendar.setTime(RutinasFechas.conversionDeString("01/01/" + r_ejercicio));
		}
		
		int semanas = calendar.getWeeksInWeekYear();
		return semanas;
	}

	public static boolean esBisiesto(String r_ejercicio)
	{
		Integer anio = new Integer(r_ejercicio);
		
		if ((anio % 4 == 0 && anio % 100 != 0) || (anio % 100 == 0 && anio % 400 == 0))
	    {
			return true;
	    } 
		else 
	    {
			return false;
	    }
		
	}
	
	public static String fechaActual()
	{
		Date fecha = new Date();
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}

	public static String diaActualDD()
	{
		Date fecha = new Date();
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}
	public static String mesActualMM()
	{
		Date fecha = new Date();
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}
	public static String añoActualYY()
	{
		Date fecha = new Date();
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yy");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}

	public static String añoActualYYYY()
	{
		Date fecha = new Date();
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}

	public static String horaActual()
	{
		Date fecha = new Date();
		String horaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		horaDevolver=sdf.format(fecha);
		
		return horaDevolver;
	}

	public static String horaFecha(Date r_date)
	{
//		Date fecha = new Date();
		String horaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH");
		horaDevolver=sdf.format(r_date);
		
		return horaDevolver;
	}
	public static String minutoFecha(Date r_date)
	{
//		Date fecha = new Date();
		String horaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("mm");
		horaDevolver=sdf.format(r_date);
		
		return horaDevolver;
	}

	public static String horaActualSinSeparador()
	{
		Date fecha = new Date();
		String horaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		horaDevolver=sdf.format(fecha);
		horaDevolver=horaDevolver.replace(":", "");
		return horaDevolver;
	}
	
	
	public static Date conversion(Date r_date)
	{
		if (r_date == null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		try{
			Calendar cal = Calendar.getInstance();
			sdf.setCalendar(cal);
			cal.setTime(r_date);
			String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +         cal.get(Calendar.YEAR);
			return sdf.parse(formatedDate);
		}catch(ParseException p){
//			p.printStackTrace();
		}
		return null;
	}
	
	public static Date conversionDeString(String r_date)
	{		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
//		Calendar cal = Calendar.getInstance();
//		sdf.setCalendar(cal);
//		formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +         cal.get(Calendar.YEAR);
		try
		{
			sdf = new SimpleDateFormat("dd/MM/yy");
			if (r_date==null) return null;
			return sdf.parse(r_date);
		}
		catch(ParseException p)
		{
			try
			{
				sdf = new SimpleDateFormat("dd/MM/yyyy");
				return sdf.parse(r_date);
			}
			catch(ParseException p2)
			{
			}				
		}
		return null;
	}

	public static Date conversionDeStringFechaHora(String r_date)
	{		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
//		Calendar cal = Calendar.getInstance();
//		sdf.setCalendar(cal);
//		formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +         cal.get(Calendar.YEAR);
		try
		{
			sdf = new SimpleDateFormat("dd/MM/yy HH:mm");
			if (r_date==null) return null;
			return sdf.parse(r_date);
		}
		catch(ParseException p)
		{
			try
			{
				sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				return sdf.parse(r_date);
			}
			catch(ParseException p2)
			{
				Notificaciones.getInstance().mensajeError(p2.getMessage());
			}				
		}
		return null;
	}

	
	public static String fechaHoy()
	{
		Date fechaHoy= new Date();
		String fechaDia = null;
		String formatoFecha="dd/MM/yyyy";
//		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
	    sd.setLenient(false);
	    fechaDia = sd.format(fechaHoy);            
		return fechaDia;
	}
	
	public static String fechaHoyVista()
	{
		Date fechaHoy= new Date();
		String fechaDia = null;
		String formatoFecha="dd/MM/yyyy";
//		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
		sd.setLenient(false);
		fechaDia = sd.format(fechaHoy);            
		return fechaDia;
	}
	public static String fechaHoraVista()
	{
		Date fechaHoy= new Date();		
		String fechaDia = null;
		String formatoFecha="yyy/MM/dd hh:mm:ss";
//		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
		sd.setLenient(false);
		fechaDia = sd.format(fechaHoy);            
		return fechaDia;
	}

	public static String fechaHoraVista(Date r_date)
	{
		Date fechaHoy= r_date;		
		String fechaDia = null;
		String formatoFecha="yyy/MM/dd hh:mm:ss";
//		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sd = new SimpleDateFormat(formatoFecha);
		sd.setLenient(false);
		fechaDia = sd.format(fechaHoy);            
		return fechaDia;
	}

    public static long convertirAFecha(String fechaConvertir) throws Exception
	{
	    Date fechaConvertida = null;
	    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	    sd.setLenient(false);
        try
        {
            fechaConvertida = sd.parse(fechaConvertir);            
        }
        catch (ParseException e)
        {            
            SimpleDateFormat sd1 = new SimpleDateFormat("ddMMyy");
            sd1.setLenient(false);
            try
            {
                fechaConvertida = sd1.parse(fechaConvertir);            
            }
            catch (ParseException e1)
            {            
                SimpleDateFormat sd2 = new SimpleDateFormat("ddMMyyyy");
                sd2.setLenient(false);
                try
                {
                    fechaConvertida = sd2.parse(fechaConvertir);            
                }
                catch (ParseException e2)
                {            
                    SimpleDateFormat sd3 = new SimpleDateFormat("dd-MM-yy");
                    sd3.setLenient(false);
                    try
                    {
                        fechaConvertida = sd3.parse(fechaConvertir);            
                    }
                    catch (ParseException e3)
                    {            
                        SimpleDateFormat sd4 = new SimpleDateFormat("dd-MM-yyyy");
                        sd4.setLenient(false);
                        try
                        {
                            fechaConvertida = sd4.parse(fechaConvertir);            
                        }
                        catch (ParseException e4)
                        {   
                            e.printStackTrace();
                        } 
                    } 
                } 
            } 
        } 
        return(fechaConvertida.getTime());
	}

	public static boolean comprobarFecha(String fechaComprobar) throws Exception
	{
//	    Date fechaConvertida = null;
	    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
        try
        {
//            fechaConvertida = 
    		sd.parse(fechaComprobar);            
        }
        catch (ParseException e)
        {           
            return(false);
        } 
	    return(true);
	}
	
	public  static String convertirDeFecha(String fechaConvertirRecibo) throws Exception
	{
	    Date fechaConvertida = null;
	    String fechaDevolver = null;
	    
	    SimpleDateFormat fechadm2b = new SimpleDateFormat("dd/MM/yyyy");
	    fechadm2b.setLenient(false);	    
        try
        {
            fechaConvertida = fechadm2b.parse(fechaConvertirRecibo);            
        }
        catch (ParseException e)
        {           
    	    SimpleDateFormat fecha2ymdg = new SimpleDateFormat("yyyy-MM-dd");
    	    fecha2ymdg.setLenient(false);
            try
            {
                fechaConvertida = fecha2ymdg.parse(fechaConvertirRecibo);                
            }
            catch (ParseException ex)
            {                           
        	    SimpleDateFormat fechadmy = new SimpleDateFormat("ddMMyy");
        	    fechadmy.setLenient(false);
                try
                {
                    fechaConvertida = fechadmy.parse(fechaConvertirRecibo);                    
                }
                catch (ParseException ex1)
                {   
                    return(null);
                }
            }
        }
        fechaDevolver = fechadm2b.format(fechaConvertida);
        return(fechaDevolver);
	}
	public static String convertirAFechaHoraMysql(String parmFechaConvertir) throws Exception
	{
		Date fechaConvertida = null;
		String fechaDevolver = null;
		
		SimpleDateFormat fecha2ymdg = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		fecha2ymdg.setLenient(false);
		SimpleDateFormat fechadm2b = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		fechadm2b.setLenient(false);	    
		try
		{
			fechaConvertida = fechadm2b.parse(parmFechaConvertir);            
		}
		catch (ParseException e)
		{           
			try
			{
				fechaConvertida = fecha2ymdg.parse(parmFechaConvertir);                
			}
			catch (ParseException ex)
			{                           
				SimpleDateFormat fechadmy = new SimpleDateFormat("ddMMyy");
				fechadmy.setLenient(false);
				try
				{
					fechaConvertida = fechadmy.parse(parmFechaConvertir);                    
				}
				catch (ParseException ex1)
				{   
					return(null);
				}
			}
		}
		fechaDevolver = fecha2ymdg.format(fechaConvertida);
		return(fechaDevolver);
	}
	
	public static String convertirAFechaHoraObjective(String parmFechaConvertir) throws Exception
	{
		Date fechaConvertida = null;
		
		SimpleDateFormat fecha2ymdg = new SimpleDateFormat("yyyyMMdd");
		fecha2ymdg.setLenient(false);
		try
		{
			fechaConvertida = fecha2ymdg.parse(parmFechaConvertir);                
		}
		catch (ParseException ex)
		{                           
				return(null);
		}
		return(fecha2ymdg.format(fechaConvertida));
	}
	
	public static String convertirAFechaMysql(String parmFechaConvertir) throws Exception
	{
		Date fechaConvertida = null;
		String fechaDevolver = null;
		
		SimpleDateFormat fecha2ymdg = new SimpleDateFormat("yyyy-MM-dd");
		fecha2ymdg.setLenient(false);
		SimpleDateFormat fechadm2b = new SimpleDateFormat("dd/MM/yyyy");
		fechadm2b.setLenient(false);	    
		try
		{
			fechaConvertida = fechadm2b.parse(parmFechaConvertir);            
		}
		catch (ParseException e)
		{           
			try
			{
				fechaConvertida = fecha2ymdg.parse(parmFechaConvertir);                
			}
			catch (ParseException ex)
			{                           
				SimpleDateFormat fechadmy = new SimpleDateFormat("ddMMyy");
				fechadmy.setLenient(false);
				try
				{
					fechaConvertida = fechadmy.parse(parmFechaConvertir);                    
				}
				catch (ParseException ex1)
				{   
					return(null);
				}
			}
		}
		fechaDevolver = fecha2ymdg.format(fechaConvertida);
		return(fechaDevolver);
	}

	public static String convertirAFechaObjective(String parmFechaConvertir) throws Exception
	{
		Date fechaConvertida = null;
		String fechaDevolver = null;
		
		SimpleDateFormat fecha2ymdg = new SimpleDateFormat("dd-mm-yyyy");
		fecha2ymdg.setLenient(false);
		try
		{
			fechaConvertida = fecha2ymdg.parse(parmFechaConvertir);                
		}
		catch (ParseException ex)
		{                           
			SimpleDateFormat fechadmy = new SimpleDateFormat("ddMMyy");
			fechadmy.setLenient(false);
			try
			{
				fechaConvertida = fechadmy.parse(parmFechaConvertir);                    
			}
			catch (ParseException ex1)
			{   
				return(null);
			}
		}
		fechaDevolver = fecha2ymdg.format(fechaConvertida);
		return(fechaDevolver);
	}
	
	public static int diferenciaEnDias2(Date fechaMayor, Date fechaMenor) 
	{
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		return (int) dias;
	}

	public static int diferenciaEnHoras(Date fechaMayor, Date fechaMenor) 
	{
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		Long dias = diferenciaEn_ms / (1000 * 60 * 60 * 24);
		int horas = dias.intValue()*24;
		return horas;
	}

	public static long diferenciaEnMs(Date fechaMayor, Date fechaMenor) 
	{
		long diferenciaEn_ms = fechaMayor.getTime() - fechaMenor.getTime();
		return (long) diferenciaEn_ms;
	}

	public static String restarDiasFecha(String r_fecha, Integer r_dias) throws Exception
	{
		Date fechaConvertida = null;
		    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	    sd.setLenient(false);
        try
        {
            fechaConvertida = sd.parse(convertirDeFecha(r_fecha));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaConvertida); // Configuramos la fecha que se recibe
            calendar.add(Calendar.DAY_OF_YEAR, r_dias * (-1));
            fechaConvertida = calendar.getTime();
            
    		sd.setLenient(false);
    		return sd.format(fechaConvertida);            
           
        }
        catch(ParseException ex)
        {
        	return convertirDeFecha(r_fecha);	
        }
		
	}

	public static String sumarDiasFecha(String r_fecha, Integer r_dias) throws Exception
	{
		Date fechaConvertida = null;
		    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	    sd.setLenient(false);
        try
        {
            fechaConvertida = sd.parse(convertirDeFecha(r_fecha));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaConvertida); // Configuramos la fecha que se recibe
            calendar.add(Calendar.DAY_OF_YEAR, r_dias);
            fechaConvertida = calendar.getTime();
            
    		sd.setLenient(false);
    		return sd.format(fechaConvertida);            
           
        }
        catch(ParseException ex)
        {
        	return convertirDeFecha(r_fecha);	
        }
		
	}

	public static String sumarMinutosFecha(String r_fecha, int r_minutos) throws Exception
	{
		Date fechaConvertida = null;
		    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    sd.setLenient(false);
        try
        {
        	GregorianCalendar gc = new GregorianCalendar();
        	gc.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
            fechaConvertida = sd.parse(convertirDeFecha(r_fecha));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaConvertida); // Configuramos la fecha que se recibe
            calendar.add(Calendar.MINUTE, r_minutos);
            fechaConvertida = calendar.getTime();
            
    		sd.setLenient(false);
    		Notificaciones.getInstance().mensajeSeguimiento("Fecha mas minutos: " + fechaConvertida.toString());
    		return sd.format(fechaConvertida);            
           
        }
        catch(ParseException ex)
        {
        	ex.printStackTrace();
//        	return convertirDeFecha(r_fecha);	
        }
		return null;
	}

	public static String sumarMinutosFechaVista(String r_fecha, int r_minutos) throws Exception
	{
		Date fechaConvertida = null;
		    
	    SimpleDateFormat sd = new SimpleDateFormat("yyyy/MM/dd HH:mm");
	    sd.setLenient(false);
        try
        {
        	GregorianCalendar gc = new GregorianCalendar();
        	gc.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
            fechaConvertida = sd.parse(convertirDeFecha(r_fecha));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaConvertida); // Configuramos la fecha que se recibe
            calendar.add(Calendar.MINUTE, r_minutos);
            fechaConvertida = calendar.getTime();
            
    		sd.setLenient(false);
    		Notificaciones.getInstance().mensajeSeguimiento("Fecha mas minutos: " + fechaConvertida.toString());
    		return sd.format(fechaConvertida);            
           
        }
        catch(ParseException ex)
        {
        	ex.printStackTrace();
//        	return convertirDeFecha(r_fecha);	
        }
		return null;
	}

	public  static Date convertirADate(String r_fecha) throws Exception
	{
		Date fechaConvertida = null;
		    
	    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
	    sd.setLenient(false);
        try
        {
            fechaConvertida = sd.parse(convertirDeFecha(r_fecha));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fechaConvertida); // Configuramos la fecha que se recibe
            //calendar.add(Calendar.DAY_OF_YEAR, r_dias * (-1));
            fechaConvertida = calendar.getTime();
            
    		sd.setLenient(false);
    		          
           
        }
        catch(ParseException ex)
        {
//        	return this.convertirDeFecha(r_fecha);	
        }
        return (fechaConvertida);  
	}
	
	public static String convertirDateToString(Date r_date)
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fecha = sdf.format(r_date);
		return fecha;			
	}
	public static String convertirDateToString(Date r_date,String r_format)
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat(r_format);
		fecha = sdf.format(r_date);
		return fecha;			
	}

	public static String convertirDateToStringYYYMMDD(Date r_date)
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		fecha = sdf.format(r_date);
		return fecha;			
	}

	public static String convertirDateTimeToString(Date r_date)
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		fecha = sdf.format(r_date);
		return fecha;			
	}

	public static String fechaSumandoAños(Date r_fecha, Integer r_años)
	{
		Date nuevaFecha = null;
		
        Calendar cal = Calendar.getInstance(); 
        cal.setTime(r_fecha); 
        cal.add(Calendar.YEAR,r_años);
        nuevaFecha = cal.getTime();
        
		
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaDevolver=sdf.format(nuevaFecha);
		
		return fechaDevolver;
	}

	public static String fechaActualSumandoAños(Integer r_años)
	{
		Date nuevaFecha = new Date();


        Calendar cal = Calendar.getInstance(); 
        cal.setTime(nuevaFecha); 
        cal.add(Calendar.YEAR,r_años);
        nuevaFecha = cal.getTime();
        
		
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaDevolver=sdf.format(nuevaFecha);
		
		return fechaDevolver;
	}

	public static String diaFecha(Date r_fecha)
	{
		Date fecha = r_fecha;
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}
	public static String mesFecha(Date r_fecha)
	{
		Date fecha = r_fecha;
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}
	public static String añoFecha(Date r_fecha)
	{
		Date fecha = r_fecha;
		String fechaDevolver = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		fechaDevolver=sdf.format(fecha);
		
		return fechaDevolver;
	}
	
	public int obtenerUltimoDiaMes (int r_ejercicio, int r_mes) 
	{
		Calendar calendario=Calendar.getInstance();
		calendario.set(r_ejercicio, r_mes-1, 1);
		return calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static int obtenerUltimoDiaMesActual (int r_ejercicio, int r_mes) 
	{
		Calendar calendario=Calendar.getInstance();
		calendario.set(r_ejercicio, r_mes-1, 1);
		return calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

}
