package borsao.e_borsao.ClasesPropias;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.TextField;

public class TextFieldNumero extends TextField
{
	Property<Integer> integerProperty=null;
	
	public TextFieldNumero()
	{
		final FieldNumero fN = new FieldNumero();
		BeanItem<FieldNumero> beanItem = new BeanItem<FieldNumero>(fN);
		this.integerProperty = (Property<Integer>) beanItem.getItemProperty("value");
		
		this.setPropertyDataSource(integerProperty);
		this.cargarListeners();
	}

	private void cargarListeners()
	{
		this.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				if (!isValid())
				Notificaciones.getInstance().mensajeError("Debes introducir un numero");
			}
		});
	}
}