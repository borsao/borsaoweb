package borsao.e_borsao.ClasesPropias;

public class RutinasCodigoBarras
{
	public RutinasCodigoBarras()
	{
		
	}

	public static String calcula_codigo(String r_pcodigo) {
		Integer impar = 0;
		Integer par = 0;
		Integer rdo = 0;
		Integer digito = 0;
		Double decena = new Double(0);

		impar = new Integer(r_pcodigo.substring(0, 1)) * 3 + new Integer(r_pcodigo.substring(2, 3)) * 3
				+ new Integer(r_pcodigo.substring(4, 5)) * 3 + new Integer(r_pcodigo.substring(6, 7)) * 3
				+ new Integer(r_pcodigo.substring(8, 9)) * 3 + new Integer(r_pcodigo.substring(10, 11)) * 3 + new Integer(r_pcodigo.substring(12, 13)) * 3;

		par = new Integer(r_pcodigo.substring(1, 2)) * 1 + new Integer(r_pcodigo.substring(3, 4)) * 1
				+ new Integer(r_pcodigo.substring(5, 6)) * 1 + new Integer(r_pcodigo.substring(7, 8)) * 1
				+ new Integer(r_pcodigo.substring(9, 10)) * 1 + new Integer(r_pcodigo.substring(11, 12)) * 1;

		rdo = impar + par;

		if (rdo % 10 == 0)
			digito = 0;
		else {
			decena = new Double(rdo / 10);
			decena = (decena + 1) * 10;
			digito = decena.intValue() - rdo.intValue();
		}
		return (r_pcodigo + digito);
	}

	public static String calcula_ean(String r_pcodigo) {
		Integer impar = 0;
		Integer par = 0;
		Integer rdo = 0;
		Integer digito = 0;
		Double decena = new Double(0);

		impar = new Integer(r_pcodigo.substring(0, 1)) * 3 + new Integer(r_pcodigo.substring(2, 3)) * 3
				+ new Integer(r_pcodigo.substring(4, 5)) * 3 + new Integer(r_pcodigo.substring(6, 7)) * 3
				+ new Integer(r_pcodigo.substring(8, 9)) * 3 + new Integer(r_pcodigo.substring(10, 11)) * 3
				+ new Integer(r_pcodigo.substring(12, 13)) * 3 + new Integer(r_pcodigo.substring(14, 15)) * 3 + new Integer(r_pcodigo.substring(16, 17)) * 3 ;

		par = new Integer(r_pcodigo.substring(1, 2)) * 1 + new Integer(r_pcodigo.substring(3, 4)) * 1
				+ new Integer(r_pcodigo.substring(5, 6)) * 1 + new Integer(r_pcodigo.substring(7, 8)) * 1
				+ new Integer(r_pcodigo.substring(9, 10)) * 1 + new Integer(r_pcodigo.substring(11, 12)) * 1
				+ new Integer(r_pcodigo.substring(13, 14)) * 1 + new Integer(r_pcodigo.substring(15, 16)) * 1 ;

		rdo = impar + par;

		if (rdo % 10 == 0)
			digito = 0;
		else {
			decena = new Double(rdo / 10);
			decena = (decena + 1) * 10;
			digito = decena.intValue() - rdo.intValue();
		}
		return (r_pcodigo + digito);
	}
}
