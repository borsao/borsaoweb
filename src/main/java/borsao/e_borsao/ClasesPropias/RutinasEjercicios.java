package borsao.e_borsao.ClasesPropias;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class RutinasEjercicios
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;

	public RutinasEjercicios()
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public ArrayList<String> cargarEjercicios()
	{
		ResultSet rsUser = null;	
		ArrayList<String> ejer = null;
		String sql = null;
		try
		{
			ejer = new ArrayList<String>();
			sql="select distinct ejercicio as eje from vi_ejercicios order by ejercicio desc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsUser = cs.executeQuery(sql);
			while(rsUser.next())
			{
				ejer.add(rsUser.getString("eje"));				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsUser!=null)
				{
					rsUser.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return ejer;
	}
	
	
	public Integer cargarEjercicioActual()
	{
		ResultSet rsUser = null;	
		Integer ejer = null;
		String sql = null;
		try
		{
			sql="select max(ejercicio) as eje from vi_ejercicios" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsUser = cs.executeQuery(sql);
			while(rsUser.next())
			{
				ejer = rsUser.getInt("eje");				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsUser!=null)
				{
					rsUser.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return ejer;
	}
	
	
	public Integer cargarDigitoEjercicio(Integer r_ejercicio)
	{
		Integer digito = null;
		Integer ejerActual = null;
		
		ejerActual = this.cargarEjercicioActual();
		digito =ejerActual - r_ejercicio;
		
		return digito;
	}

}

