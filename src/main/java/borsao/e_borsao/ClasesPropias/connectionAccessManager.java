package borsao.e_borsao.ClasesPropias;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;

import net.ucanaccess.jdbc.JackcessOpenerInterface;

public class connectionAccessManager implements JackcessOpenerInterface 
{
    public Database open(File f, String pwd) throws IOException 
    {
        DatabaseBuilder db = new DatabaseBuilder(f);
//        db.setCharset(Charset.forName("ISO-8859-1"));
        db.setCharset(Charset.forName("cp1253"));
        try {
            db.setReadOnly(false);
            return db.open();
        } catch (IOException e) {
            db.setReadOnly(true);
            return db.open();
        }
   }
}
