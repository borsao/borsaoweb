package borsao.e_borsao.ClasesPropias.graficas;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoBarras extends MapeoGlobal
{
	private String clave = null;
	private Double valorActual = null;
	private Double valorSerie1 = null;
	private Double valorSerie2 = null;
	
	public MapeoBarras()
	{
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Double getValorActual() {
		return valorActual;
	}

	public void setValorActual(Double valorActual) {
		this.valorActual = valorActual;
	}

	public Double getValorSerie1() {
		return valorSerie1;
	}

	public void setValorSerie1(Double valorSerie1) {
		this.valorSerie1 = valorSerie1;
	}

	public Double getValorSerie2() {
		return valorSerie2;
	}

	public void setValorSerie2(Double valorSerie2) {
		this.valorSerie2 = valorSerie2;
	}
	
}