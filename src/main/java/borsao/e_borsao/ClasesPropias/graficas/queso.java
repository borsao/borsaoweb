package borsao.e_borsao.ClasesPropias.graficas;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PieLabelLinkStyle;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.ui.Panel;

public class queso
{
	private static PieDataset createDataset(ArrayList<MapeoValores> r_vector) 
	{

		ArrayList<MapeoValores> vector = r_vector;
		
		DefaultPieDataset dataset = new DefaultPieDataset( );
			
		for (int i = 0; i<vector.size(); i++)
		{
			MapeoValores mapeo = (MapeoValores) vector.get(i);
			dataset.setValue(mapeo.getClave(), mapeo.getValorDouble());
		}
      	return dataset;         
	}
	   
	private static JFreeChart createChart( PieDataset dataset, String r_explode , HashMap<Integer, Color>  r_hasColores, Color r_color) 
	{
		JFreeChart chart = ChartFactory.createPieChart(      
	         "",   // chart title 
	         dataset,          // data    
	         true,             // include legend   
	         true, 
	         false);
		chart.setBorderVisible(false);
		chart.setBackgroundImage(null);
		chart.setBackgroundPaint(new Color(68,77,80));
	      
		PiePlot plot = (PiePlot) chart.getPlot();
		if (r_explode != null) plot.setExplodePercent(r_explode, 0.10);
		plot.setBackgroundImage(null);
		plot.setOutlineVisible(false);
		plot.setOutlineStroke(null);
		plot.setBackgroundPaint(new Color(68,77,80));
		
		PiePlot piePlot = (PiePlot) chart.getPlot();
		
		if (r_hasColores!=null)
		{
			for (int i=0; i<r_hasColores.size(); i++)
			{
				piePlot.setSectionPaint(i, r_hasColores.get(i));
			}
		}
			
		plot.setSimpleLabels(true);
		
		PieSectionLabelGenerator gen = new StandardPieSectionLabelGenerator(
	            "{1} ({2})", new DecimalFormat("0.00"), new DecimalFormat("0.00%"));
	        plot.setLabelGenerator(gen);
	        
		plot.setLabelBackgroundPaint(null);
		plot.setLabelPaint(r_color);
		plot.setLabelShadowPaint(null);
		plot.setMaximumLabelWidth(0.20);
		plot.setLabelLinkStyle(PieLabelLinkStyle.STANDARD);
          
		return chart;
	}
	   
	public static Panel generarGrafico(String r_caption, String r_ancho, String r_alto, ArrayList<MapeoValores> r_vector, String r_explode, HashMap<Integer, Color>  r_hasColores) 
	{
		   
		Panel panelQueso = new Panel(r_caption);
		panelQueso.setSizeUndefined();

		JFreeChart chart = createChart(createDataset(r_vector), r_explode ,r_hasColores, Color.white );

		JFreeChartWrapper wrapper = new JFreeChartWrapper(chart); 
//		{
//			@Override
//			public void attach() {
//				super.attach();
//				setResource("src", getSource());
//			}
//		};
		wrapper.setWidth(r_ancho);
		wrapper.setHeight(r_alto);
		panelQueso.setContent(wrapper);
		panelQueso.setStyleName("panelGrafico");
		return  panelQueso; 
	}
	public static Panel generarGrafico(String r_caption, String r_ancho, String r_alto, ArrayList<MapeoValores> r_vector, String r_explode, HashMap<Integer, Color>  r_hasColores, Color r_color) 
	{
		
		Panel panelQueso = new Panel(r_caption);
		panelQueso.setSizeUndefined();
		
		JFreeChart chart = createChart(createDataset(r_vector), r_explode ,r_hasColores, r_color );
		
		JFreeChartWrapper wrapper = new JFreeChartWrapper(chart); 
//		{
//			@Override
//			public void attach() {
//				super.attach();
//				setResource("src", getSource());
//			}
//		};
		wrapper.setWidth(r_ancho);
		wrapper.setHeight(r_alto);
		panelQueso.setContent(wrapper);
		panelQueso.setStyleName("panelGrafico");
		return  panelQueso; 
	}
}