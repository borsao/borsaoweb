package borsao.e_borsao.ClasesPropias.graficas;

import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.d3Gauge.Gauge;
import org.vaadin.addons.d3Gauge.GaugeConfig;
import org.vaadin.addons.d3Gauge.Zone;

import com.vaadin.ui.Panel;

public class reloj
{
	public Integer zonaRojaDesde = 0;
	public Integer zonaRojaHasta = 2000;
	public Integer zonaVerdeDesde = 4201;
	public Integer zonaVerdeHasta = 9000;
	public Integer zonaAmarillaDesde = 2001;
	public Integer zonaAmarillaHasta = 4200;
	
	private Gauge gauge = null;	
	private GaugeConfig config = null; 

	public static Panel generarReloj(String r_vista, Integer r_valor, Integer r_inicioVerde, Integer r_finVerde, Integer r_inicioAmarillo, Integer r_finAmarillo, Integer r_inicioRojo, Integer r_finRojo, Integer r_min, Integer r_max, String r_estilo, Integer r_tamaño)
    {
    	Panel gauge = null;
    	reloj reloj = new reloj();
    	reloj.establecerConfiguracion(r_inicioVerde, r_finVerde, r_inicioAmarillo, r_finAmarillo, r_inicioRojo, r_finRojo, r_min, r_max, r_estilo);
    	gauge = reloj.generar(r_vista,r_valor,r_tamaño);
    	
    	return gauge;
    }

	private Panel generar(String r_caption, Integer r_Valor, Integer r_tamaño)
	{
		Panel panelReloj = new Panel(r_caption);
		panelReloj.setSizeUndefined();
        /*
         * Implementacion del reloj
         * Recibe:
         * 		Titulo
         * 		valor actual
         * 		tamaño del gráfico
         * 		sesion de configuracion/personalizacion del reloj
         */
		
		gauge = new Gauge("",r_Valor, r_tamaño,this.config);
		/*
         * posicionamos la pantalla y su diseño
         */
//        layout.addComponent(gauge);
//        layout.setComponentAlignment(gauge, Alignment.MIDDLE_CENTER);
		panelReloj.setStyleName("panelGrafico");
		panelReloj.setContent(gauge);
		return panelReloj;
	}

	public void establecerConfiguracion(Integer r_verdedsd, Integer r_verdehst, Integer r_amarillodsd, Integer r_amarillohst, Integer r_rojodsd, Integer r_rojohst, Integer r_min, Integer r_max, String r_estilo)
	{
		/*
		 * Iniciamos la configuracion personalizada del reloj
		 */
		this.config = new GaugeConfig();
		/*
		 * Estilo visual del reloj
		 */
		config.setStyle(r_estilo);
		/*
		 * configuramos los rangos de valor indicativos del semaforo
		 */
		List<Zone> greenZones = new ArrayList<Zone>();
		List<Zone> yellowZones = new ArrayList<Zone>();
		List<Zone> redZones = new ArrayList<Zone>();
		
		Zone red = new Zone(r_rojodsd, r_rojohst);		
		redZones.add(red);
		
		Zone yellow = new Zone(r_amarillodsd, r_amarillohst);
		yellowZones.add(yellow);
		
		Zone green = new Zone(r_verdedsd, r_verdehst);
		greenZones.add(green);
		
		config.setRedZones(redZones);
		config.setYellowZones(yellowZones);
		config.setGreenZones(greenZones);
		

		/*
		 * valores máximo y minimo del reloj
		 */
		config.setMin(r_min);
		config.setMax(r_max);
		
		
		/*
		 * valores de tamaño de las rayas indicadoras del reloj
		 */
		config.setMinorTicks(5);
		config.setMajorTicks(5);
		/*
		 * movimiento de la aguja al generar los datos, suele usarse para relojes que tardan en cargar
		 */
        config.setTransitionDuration(1500);
        /*
         * muestra titulo valor actual, en ingles, current
         */
        config.setShowCurrentLabel(true);
//        config.setTrackAvg(true);
//        config.setTrackMax(true);
//        config.setTrackMin(true);
		
        
	}
}