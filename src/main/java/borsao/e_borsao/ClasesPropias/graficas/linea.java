package borsao.e_borsao.ClasesPropias.graficas;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.HashMap;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.TextAnchor;
import org.vaadin.addon.JFreeChartWrapper;

public class linea
{
	private static XYSeriesCollection createDataset(HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, HashMap<String, Double> hashSerie3, String r_titulo_serie1, String r_titulo_serie2, String r_titulo_serie3, Integer r_valorrInicialSeries ) 
	{
		XYSeries series1 = null;
		XYSeries series2 = null;
		XYSeries series3 = null;
		XYSeriesCollection dataset = new XYSeriesCollection();
		
		if (hashSerie1!=null)
		{
			series1 = new XYSeries(r_titulo_serie1);
			
	        for (int i=r_valorrInicialSeries + 0;i < r_valorrInicialSeries + hashSerie1.size();i++)
		    {
	        	if (hashSerie1.get(String.valueOf(i+1))!=null && hashSerie1.get(String.valueOf(i+1))>0) series1.add(i+1, hashSerie1.get(String.valueOf(i+1))/1); else series1.add(i+1, null);
		    }
	        dataset.addSeries(series1);
		}
		if (hashSerie2!=null)
		{
	        series2 = new XYSeries(r_titulo_serie2);
		    for (int i=r_valorrInicialSeries+0;i < r_valorrInicialSeries+hashSerie2.size();i++)
		    {
		    	if (hashSerie2.get(String.valueOf(i+1))!=null && hashSerie2.get(String.valueOf(i+1))>0) series2.add(i+1, hashSerie2.get(String.valueOf(i+1))/1); else series2.add(i+1, null);
		    }
		    dataset.addSeries(series2);
		}	    
		if (hashSerie3!=null)
		{
	        series3 = new XYSeries(r_titulo_serie3);
		    for (int i=r_valorrInicialSeries+0;i < r_valorrInicialSeries+hashSerie3.size();i++)
		    {
		    	if (hashSerie3.get(String.valueOf(i+1))!=null && hashSerie3.get(String.valueOf(i+1))>0) series3.add(i+1, hashSerie3.get(String.valueOf(i+1))/1); else series3.add(i+1, null);
		    }
		    dataset.addSeries(series3);
		}	    
	    
		return dataset;
	}
	private static XYSeriesCollection createDataset(HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, HashMap<String, Double> hashSerie3, String r_titulo_serie1, String r_titulo_serie2, String r_titulo_serie3, Integer r_valorrInicialSeries, Integer r_maxSerie ) 
	{
		XYSeries series1 = null;
		XYSeries series2 = null;
		XYSeries series3 = null;
		XYSeriesCollection dataset = new XYSeriesCollection();
		
		if (hashSerie1!=null)
		{
			series1 = new XYSeries(r_titulo_serie1);
			
			for (int i=r_valorrInicialSeries + 0;i < r_maxSerie;i++)
			{
				if (hashSerie1.get(String.valueOf(i+1))!=null && hashSerie1.get(String.valueOf(i+1))>0) series1.add(i+1, hashSerie1.get(String.valueOf(i+1))/1); else series1.add(i+1, null);
			}
			dataset.addSeries(series1);
		}
		if (hashSerie2!=null)
		{
			series2 = new XYSeries(r_titulo_serie2);
			for (int i=r_valorrInicialSeries+0;i < r_maxSerie;i++)
			{
				if (hashSerie2.get(String.valueOf(i+1))!=null && hashSerie2.get(String.valueOf(i+1))>0) series2.add(i+1, hashSerie2.get(String.valueOf(i+1))/1); else series2.add(i+1, null);
			}
			dataset.addSeries(series2);
		}	    
		if (hashSerie3!=null)
		{
			series3 = new XYSeries(r_titulo_serie3);
			for (int i=r_valorrInicialSeries+0;i < r_maxSerie;i++)
			{
				if (hashSerie3.get(String.valueOf(i+1))!=null && hashSerie3.get(String.valueOf(i+1))>0) series3.add(i+1, hashSerie3.get(String.valueOf(i+1))/1); else series3.add(i+1, null);
			}
			dataset.addSeries(series3);
		}	    
		
		return dataset;
	}
	
	private static JFreeChart createChart(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, XYSeriesCollection dataset, double r_rangoMin, double r_rangoMax, boolean r_verValoresSerie1, boolean r_verValoresSerie2, boolean r_verValoresSerie3) {
		// create the chart...
		 
	        JFreeChart chart = ChartFactory.createXYLineChart(
	            r_titulo,
	            r_titulo_ejex,
	            r_titulo_ejey,
	            dataset,
	            PlotOrientation.VERTICAL,
	            true,
	            true,
	            false
	        );
	        
	        XYPlot plot = (XYPlot) chart.getPlot();
	        XYLineAndShapeRenderer r1 = (XYLineAndShapeRenderer) plot.getRenderer(0);
	        
	        if (r_verValoresSerie1||r_verValoresSerie2||r_verValoresSerie3)
	        {
				DecimalFormat NumberFormat = new DecimalFormat("##.##");
				ItemLabelPosition p = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,TextAnchor.HALF_ASCENT_CENTER,TextAnchor.TOP_RIGHT,-20);
	
		        XYItemLabelGenerator generator = new StandardXYItemLabelGenerator(StandardXYItemLabelGenerator.DEFAULT_ITEM_LABEL_FORMAT, NumberFormat, NumberFormat);
		        r1.setBaseItemLabelGenerator(generator);
		        r1.setBasePositiveItemLabelPosition(p);
		        r1.setBaseItemLabelFont(r1.getBaseItemLabelFont().deriveFont(8f));
		        r1.setBaseItemLabelsVisible(true);
		        r1.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
	        }
	        
		    r1.setSeriesPaint(0, new Color(229,153,5)); 
		    r1.setSeriesShapesVisible(0,  false);
		    r1.setSeriesItemLabelsVisible(0, r_verValoresSerie1);
		    r1.setSeriesItemLabelPaint(0, new Color(255,255,255));
		    
		    if (dataset.getSeriesCount()>1)
		    {
		    	r1.setSeriesPaint(1, new Color(0x00, 0xff, 0x00)); 
		    	r1.setSeriesShapesVisible(1,  false);
		    	r1.setSeriesItemLabelsVisible(1, r_verValoresSerie2);
		    	r1.setSeriesItemLabelPaint(1, new Color(255,255,255));
		    }
		    if (dataset.getSeriesCount()>2)
		    {
		    	r1.setSeriesPaint(2, new Color(0x80,0xC0,0xFF)); 
		    	r1.setSeriesShapesVisible(2,  false);
		    	r1.setSeriesItemLabelsVisible(2, r_verValoresSerie3);
		    	r1.setSeriesItemLabelPaint(2, new Color(255,255,255));
		    }
		    
//		    r1.setToolTipGenerator(xyToolTipGenerator);
//		    r1.setBaseToolTipGenerator(xyToolTipGenerator);
		    
		    XYLineAndShapeRenderer r2 = new XYLineAndShapeRenderer();
		    r2.setSeriesPaint(0, new Color(229,153,5)); 
		    r2.setSeriesShapesVisible(0,  false);
		    if (dataset.getSeriesCount()>1)
		    {
		    	r2.setSeriesPaint(1, new Color(0x00, 0xff, 0x00)); 
		    	r2.setSeriesShapesVisible(1,  false);
		    }
		    if (dataset.getSeriesCount()>2)
		    {
		    	r2.setSeriesPaint(2, new Color(0x80,0xC0,0xFF)); 
		    	r2.setSeriesShapesVisible(2,  false);
		    }
//		    r2.setToolTipGenerator(xyToolTipGenerator);
//		    r2.setBaseToolTipGenerator(xyToolTipGenerator);
		    
		    
//	        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
//	        renderer.setSeriesLinesVisible(0, true);
//	        renderer.setSeriesShapesVisible(0, false);
//	        renderer.setSeriesLinesVisible(1, true);
//	        renderer.setSeriesShapesVisible(1, false);        
////	        renderer.setBaseToolTipGenerator(new StandardXYToolTipGenerator());
	        
	        ValueAxis axis = plot.getRangeAxis(0);
	        axis.setLabelPaint(new Color(255,255,255));
	        axis.setTickLabelPaint(new Color(255,255,255));
//	        axis.setAutoRange(false);
	        
	        if (r_rangoMax != 0 )        axis.setRange(r_rangoMin, r_rangoMax);
	        
	        
	        DecimalFormat formatoEje = new DecimalFormat("##");
	        NumberAxis y = new NumberAxis();
	        y.setTickUnit(new NumberTickUnit(1, formatoEje));
//	        
//	        ValueAxis y = plot.getDomainAxis(0);
	        y.setLabelPaint(new Color(255,255,255));
	        y.setTickLabelPaint(new Color(255,255,255));
	        y.setVerticalTickLabels(true);
	        plot.setDomainAxis(y);
	        
//	        plot.setRenderer(render);
	        plot.setRenderer(0, r1);
	        plot.setRenderer(1, r2);

	        plot.setBackgroundPaint(new Color(68,77,80));
	        plot.setDomainGridlinePaint(new Color(212,132,3));
	        plot.setRangeGridlinePaint(new Color(212,132,3));
	        
	        chart.setBackgroundPaint(new Color(112,124,129));
	        LegendTitle legend = chart.getLegend();
	        Color legendBackground = new Color(0, 0, 0, 0);
	        legend.setBackgroundPaint(legendBackground);
	        
	        TextTitle tt = chart.getTitle();
	        tt.setPaint(new Color(255,255,255));
	        
		return chart;
	}

	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, String r_titulo_serie1, String r_titulo_serie2 , String r_ancho, String r_alto, Integer r_valorrInicialSeries, double r_rangoMin, double r_rangoMax ) 
	{
		XYSeriesCollection dataset = createDataset(hashSerie1, hashSerie2, null, r_titulo_serie1, r_titulo_serie2, null, r_valorrInicialSeries);
        
		JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, dataset, r_rangoMin, r_rangoMax,false, false, false);
		
    	JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
            @Override
            public void attach() {
            	super.attach();
                setResource("src", getSource());
            }
        };
        wrapper.setWidth(r_ancho);
        wrapper.setHeight(r_alto);

        
		return  wrapper ; 
   }
	
	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, HashMap<String, Double> hashSerie3, String r_titulo_serie1, String r_titulo_serie2, String r_titulo_serie3 , String r_ancho, String r_alto, Integer r_valorrInicialSeries,double r_rangoMin, double r_rangoMax ) 
	{
		XYSeriesCollection dataset = createDataset(hashSerie1, hashSerie2, hashSerie3,  r_titulo_serie1, r_titulo_serie2, r_titulo_serie3, r_valorrInicialSeries);
		JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, dataset, r_rangoMin, r_rangoMax, false,false,false);
		
    	JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
            @Override
            public void attach() {
            	super.attach();
                setResource("src", getSource());
            }
        };
        if (r_ancho==null)
        	wrapper.setWidth("100%");
        else
        	wrapper.setWidth(r_ancho);
        if (r_alto==null)
        	wrapper.setHeight("100%");
        else
        	wrapper.setHeight(r_alto);

        
		return  wrapper ; 
   }
	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, HashMap<String, Double> hashSerie3, String r_titulo_serie1, String r_titulo_serie2, String r_titulo_serie3 , String r_ancho, String r_alto, Integer r_valorrInicialSeries,double r_rangoMin, double r_rangoMax, boolean r_verValoresSerie1, boolean r_verValoresSerie2, boolean r_verValoresSerie3 ) 
	{
		XYSeriesCollection dataset = createDataset(hashSerie1, hashSerie2, hashSerie3,  r_titulo_serie1, r_titulo_serie2, r_titulo_serie3, r_valorrInicialSeries);
		JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, dataset, r_rangoMin, r_rangoMax, r_verValoresSerie1,r_verValoresSerie2,r_verValoresSerie3);
		
		JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
			@Override
			public void attach() {
				super.attach();
				setResource("src", getSource());
			}
		};
		if (r_ancho==null)
			wrapper.setWidth("100%");
		else
			wrapper.setWidth(r_ancho);
		if (r_alto==null)
			wrapper.setHeight("100%");
		else
			wrapper.setHeight(r_alto);
		
		
		return  wrapper ; 
	}
	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, HashMap<String, Double> hashSerie1, HashMap<String, Double> hashSerie2, HashMap<String, Double> hashSerie3, String r_titulo_serie1, String r_titulo_serie2, String r_titulo_serie3 , String r_ancho, String r_alto, Integer r_valorrInicialSeries,double r_rangoMin, double r_rangoMax, boolean r_verValoresSerie1, boolean r_verValoresSerie2, boolean r_verValoresSerie3, Integer r_valorSerieMax ) 
	{
		XYSeriesCollection dataset = createDataset(hashSerie1, hashSerie2, hashSerie3,  r_titulo_serie1, r_titulo_serie2, r_titulo_serie3, r_valorrInicialSeries,r_valorSerieMax);
		JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, dataset, r_rangoMin, r_rangoMax, r_verValoresSerie1,r_verValoresSerie2,r_verValoresSerie3);
		
		JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
			@Override
			public void attach() {
				super.attach();
				setResource("src", getSource());
			}
		};
		if (r_ancho==null)
			wrapper.setWidth("100%");
		else
			wrapper.setWidth(r_ancho);
		if (r_alto==null)
			wrapper.setHeight("100%");
		else
			wrapper.setHeight(r_alto);
		
		
		return  wrapper ; 
	}
}