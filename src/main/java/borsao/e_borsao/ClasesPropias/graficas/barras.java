package borsao.e_borsao.ClasesPropias.graficas;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;
import org.vaadin.addon.JFreeChartWrapper;

public class barras
{
	 private static CategoryDataset createDataset(String r_nombre_serie_actual, String r_nombre_serie_1, String r_nombre_serie_2, ArrayList<MapeoBarras> r_vector) 
	 {
		    DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	
		    for (int i = 0; i< r_vector.size();i++)
		    {
		    	MapeoBarras mapeo = (MapeoBarras)r_vector.get(i);
		    	
			    dataset.addValue(mapeo.getValorActual(), r_nombre_serie_actual , mapeo.getClave());
			    if (mapeo.getValorSerie1()!=null) dataset.addValue(mapeo.getValorSerie1(), r_nombre_serie_1 , mapeo.getClave());
			    if (mapeo.getValorSerie2()!=null) dataset.addValue(mapeo.getValorSerie2(), r_nombre_serie_2 , mapeo.getClave());
		    }

		    return dataset;
		  }


	private static JFreeChart createChart(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, ArrayList<MapeoBarras> r_vector,String r_nombre_serie_actual, String r_nombre_serie_1, String r_nombre_serie_2, boolean r_girados, double r_rangoMin, double r_rangoMax) {
		// create the chart...
		CategoryDataset dataset = createDataset(r_nombre_serie_actual, r_nombre_serie_1, r_nombre_serie_2, r_vector);
		
		JFreeChart chart=ChartFactory.createBarChart(
		        r_titulo,
		        r_titulo_ejex,
		        r_titulo_ejey, // Value axis
		        dataset,
		        PlotOrientation.VERTICAL,
		        true,true,false
		       );
	    
		
		CategoryPlot plot = chart.getCategoryPlot();
		plot.setBackgroundPaint(new Color(68,77,80));
		
		DecimalFormat NumberFormat = new DecimalFormat("##.##");
		ItemLabelPosition p = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,TextAnchor.BASELINE_CENTER,TextAnchor.TOP_CENTER,-20);
		
		CategoryItemRenderer renderer0 = plot.getRenderer(0);
		
			renderer0.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE5,TextAnchor.BOTTOM_RIGHT)); 
			renderer0.setSeriesItemLabelGenerator(0, new StandardCategoryItemLabelGenerator("{2}",NumberFormat));                       
			renderer0.setSeriesItemLabelsVisible(0, true);
			renderer0.setSeriesItemLabelPaint(0, new Color(255,255,255));
			renderer0.setSeriesPaint(0, new Color(229,153,5));
			renderer0.setSeriesItemLabelGenerator(1, new StandardCategoryItemLabelGenerator("{2}",NumberFormat));                       
			renderer0.setSeriesItemLabelsVisible(1, true);
			renderer0.setSeriesItemLabelPaint(1, new Color(255,255,255));
			renderer0.setSeriesPaint(1, new Color(131,158,247));
			
			renderer0.setSeriesItemLabelGenerator(2, new StandardCategoryItemLabelGenerator("{2}",NumberFormat));                       
			renderer0.setSeriesItemLabelsVisible(2, true);
			renderer0.setSeriesItemLabelPaint(2, new Color(255,255,255));
			renderer0.setSeriesPaint(2, Color.red);
			
			renderer0.setBasePositiveItemLabelPosition(p);
		
		plot.setRenderer(0,renderer0);

        ValueAxis axis = plot.getRangeAxis(0);
        axis.setLabelPaint(new Color(255,255,255));
        axis.setTickLabelPaint(new Color(255,255,255));
        
        if (r_rangoMax!=0) axis.setRange(r_rangoMin, r_rangoMax);
        
        
        CategoryAxis y = plot.getDomainAxis(0);
        y.setLabelPaint(new Color(0,0,0));
        y.setTickLabelPaint(new Color(255,255,255));
//        y.setTickMarkPaint(new Color(0,0,0));
        
//        y.setBasePositiveItemLabelPosition(new ItemLabelPosition(
//                ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER));
        
        if (r_girados)
        	y.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 4.0));
        
        chart.setBackgroundPaint(new Color(112,124,129));
        LegendTitle legend = chart.getLegend();
        Color legendBackground = new Color(0, 0, 0, 0);
        legend.setBackgroundPaint(legendBackground);
        
        TextTitle tt = chart.getTitle();
        tt.setPaint(new Color(255,255,255));
	        
		return chart;
	}

	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, String r_ancho, String r_alto, ArrayList<MapeoBarras> r_vector,String r_nombre_serie_actual, String r_nombre_serie_1, String r_nombre_serie_2, boolean r_girados ) 
	{
		
		 JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, r_vector,r_nombre_serie_actual, r_nombre_serie_1, r_nombre_serie_2, r_girados,0,0);
	    //Create chart
	    
	    
    	JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
            @Override
            public void attach() {
            	super.attach();
                setResource("src", getSource());
            }
        };
        wrapper.setWidth(r_ancho);
        wrapper.setHeight(r_alto);

		return  wrapper ; 
   }
	public static JFreeChartWrapper generarGrafico(String r_titulo, String r_titulo_ejex, String r_titulo_ejey, String r_ancho, String r_alto, ArrayList<MapeoBarras> r_vector,String r_nombre_serie_actual, String r_nombre_serie_1, String r_nombre_serie_2, boolean r_girados, double r_rangoMin, double r_rangoMax ) 
	{
		
		JFreeChart chart = createChart(r_titulo, r_titulo_ejex, r_titulo_ejey, r_vector,r_nombre_serie_actual, r_nombre_serie_1, r_nombre_serie_2, r_girados, r_rangoMin, r_rangoMax);
		//Create chart
		
		
		JFreeChartWrapper wrapper = new JFreeChartWrapper(chart) {
			@Override
			public void attach() {
				super.attach();
				setResource("src", getSource());
			}
		};
		wrapper.setWidth(r_ancho);
		wrapper.setHeight(r_alto);
		
		return  wrapper ; 
	}
}