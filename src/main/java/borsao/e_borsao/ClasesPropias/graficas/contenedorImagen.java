package borsao.e_borsao.ClasesPropias.graficas;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;

import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;

public class contenedorImagen extends CustomComponent
{

  private static final long serialVersionUID = -9072918882445768463L;

  private static final int WIDTH_POS = 0;

  private static final int HEIGHT_POS = 1;

  private static final String APPLICATION_THEME = "borsaoTheme";

  private float width;

  private float height;

  Image imagen;

  public contenedorImagen(float widthRecibo, float heightRecibo, Resource resourceRecibo)
  {
    this.setStyleName("contenedorImagen");
    this.width = widthRecibo;
    this.height = heightRecibo;

    this.init(resourceRecibo);
  }

  private void init(Resource resourceRecibo)
  {
    HorizontalLayout horizontalLayout = new HorizontalLayout();
    horizontalLayout.setMargin(false);
    horizontalLayout.setSizeFull();

    if (resourceRecibo != null)
    {
      float[] medidas = this.getFinalSize(resourceRecibo);
      imagen = new Image(null, resourceRecibo);
      imagen.setWidth(medidas[WIDTH_POS], Unit.PIXELS);
      imagen.setHeight(medidas[HEIGHT_POS], Unit.PIXELS);

      horizontalLayout.addComponent(imagen);
      horizontalLayout.setComponentAlignment(imagen, Alignment.MIDDLE_CENTER);
    }

    this.setCompositionRoot(horizontalLayout);
    this.setWidth(width + 5, Unit.PIXELS);
    this.setHeight(height + 5, Unit.PIXELS);
  }

  /**
   * 
   * @param resourceRecibo
   * @return
   */
  private float[] getFinalSize(Resource resourceRecibo)
  {
    float[] inicial = getRealSize(resourceRecibo);
    float prop_ini = inicial[0] / inicial[1]; //Relación de aspecto de la imagen
    float prop_fin = width / height; //Relación de aspecto de la ventana donde la vamos a mostrar
    float factor;
    float[] salida = new float[2];
    if (prop_ini > prop_fin) //Si la imagen es más ancha (en proporción) que la ventana
    {
      factor = inicial[WIDTH_POS] / width; //Será el valor de proporción
    }
    else
    {
      factor = inicial[HEIGHT_POS] / height; //Si es más alta (en proporción) que la ventana
    }
    if ((inicial[WIDTH_POS] > width) || (inicial[HEIGHT_POS] > height)) //Si excede los límites de la ventana
    {
      salida[WIDTH_POS] = Math.round(inicial[WIDTH_POS] / factor);
      salida[HEIGHT_POS] = Math.round(inicial[HEIGHT_POS] / factor);
      return salida;
    }
    else
    //Si no se sale de la ventana, no la estiramos, para no pixelarla.
    {
      salida[WIDTH_POS] = inicial[WIDTH_POS];
      salida[HEIGHT_POS] = inicial[HEIGHT_POS];
      return salida; //Devolvemos las dimensiones que ha de tener la imagen mostrada
    }
  }

  private float[] getRealSize(Resource resourceRecibo)
  {
    BufferedImage bi = null;

    try
    {
      if (resourceRecibo instanceof StreamResource)
        bi = ImageIO.read(((StreamResource) resourceRecibo).getStreamSource().getStream());
      else if (resourceRecibo instanceof FileResource)
      {
        ImageInfo imageInfo;
        try
        {
          imageInfo = Imaging.getImageInfo(((FileResource) resourceRecibo).getSourceFile());
          imageInfo.getWidth();
          imageInfo.getHeight();
          
          return new float[] { imageInfo.getWidth(), imageInfo.getHeight()};
        }
        catch (ImageReadException e)
        {
          e.printStackTrace();
          return null;
        }
      }
      else if (resourceRecibo instanceof ThemeResource)
      {
        String pathImageResource = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/VAADIN/themes/" + APPLICATION_THEME + "/" + ((ThemeResource) resourceRecibo).getResourceId();
        bi = ImageIO.read(new File(pathImageResource));
      }

      return new float[] { bi.getWidth(), bi.getHeight()};
    }
    catch (IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return null;
    }
  }

  public void addClickListener(ClickListener clickListener)
  {
    imagen.addClickListener(clickListener);
  }
  
  public Resource getImageSource()
  {
    return (imagen.getSource());
  }
}