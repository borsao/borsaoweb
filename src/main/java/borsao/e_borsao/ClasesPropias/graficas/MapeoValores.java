package borsao.e_borsao.ClasesPropias.graficas;

public class MapeoValores
{
	private String clave;
	private Double valorDouble;
	private Double valorDouble2;
	private Double valorDouble3;

	public Double getValorDouble2() {
		return valorDouble2;
	}

	public void setValorDouble2(Double valorDouble2) {
		this.valorDouble2 = valorDouble2;
	}

	public Double getValorDouble3() {
		return valorDouble3;
	}

	public void setValorDouble3(Double valorDouble3) {
		this.valorDouble3 = valorDouble3;
	}

	public MapeoValores()
	{
	}

	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public Double getValorDouble() {
		return valorDouble;
	}


	public void setValorDouble(Double valorDouble) {
		this.valorDouble = valorDouble;
	}


}