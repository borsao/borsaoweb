package borsao.e_borsao.ClasesPropias;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.PantallaCargasVinoMesa;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view.PantallaSituacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view.pantallaCalculoNecesidades;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.PantallaProduccionDiaria;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.pantallaControlProgramacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.pantallaControlProgramacionEnvasasdo;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaMenuCtrlT extends Window 
{
    public Button opcCRMArticulo = null;
    public Button opcSituacionEnvasadora = null;
    public Button opcProduccionDiaria = null;
    public Button opcCargasVinoMesa = null;
    public Button opcControlProgramacion = null;
    public Button opcControlProgramacionEnvasadora = null;
    public Button opcCalculoNecesidades	 = null;
    
    public Button opcControlRetrabajos = null;
    public Button opcPreparacionPedidos = null;
    public Button opcSalir = null;
    
	private VerticalLayout principal = null;
	private HorizontalLayout topLayout = null;
	private GridLayout botones = null;

	private int fila = 0;  //filas del grid (para crecer botones en vertical
	private int columna = 0;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaMenuCtrlT()
    {
		this.setCaption("Menu Especial");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		
		this.cargarPantalla();
		this.cargarBotones();

		this.setWidth(this.columna * 200 + 200 + "px");
		this.setContent(principal);
		this.cargarListeners();
    }

    public void cargarPantalla() 
    {
		principal = new VerticalLayout();
//		principal.setSizeFull();
		principal.setMargin(true);
		principal.setSpacing(true);
		
		this.topLayout = new HorizontalLayout();
    	this.topLayout.setSpacing(true);
//    	this.topLayout.setMargin(true);
    	
	    	this.opcSalir= new Button("Salir");
	    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
	
//    	this.topLayout.addComponent(this.opcSalir);
		
    	principal.addComponent(this.topLayout);
    	
    }

    private void cargarBotones()
    {
    		//columnas del grid (para crecer botones en horizontal
    	
    	if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Comercial"))
    	{
    		this.cargarCRMArticulo();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Produccion"))
    	{
    		this.cargarCRMArticulo();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Financiero"))
    	{
    		this.cargarCRMArticulo();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Laboratorio"))
    	{
    		this.cargarCRMArticulo();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Compras"))
    	{
    		this.cargarCRMArticulo();
    		this.cargarProduccionDiaria();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Almacen"))
    	{
    		this.cargarCRMArticulo();
    		this.cargarProduccionDiaria();
    	}
    	else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))
    	{
    		this.cargarCRMArticulo();
    		this.cargarCargasVinoMesa();
    		this.cargarProduccionDiaria();
    		
    		this.cargarSituacionEnvasadora();
    		this.cargarCalculoNecesidades();
    		
    		this.cargarControlProgramacion();
    		this.cargarControlProgramacionEnvasadora();

    	}

    	this.setHeight(this.fila * 100 + 100 + "px");
    	this.botones = new GridLayout(columna+1, fila+1);
    	
    	this.fila = 0;
    	this.columna = 0;
    	
    	//columna 0
    	if (this.opcCRMArticulo!=null)
    	{
    		this.botones.addComponent(this.opcCRMArticulo,columna,fila);
    		this.fila = this.fila+1;
    	}
    	if (this.opcCargasVinoMesa!=null)
		{
    		this.botones.addComponent(this.opcCargasVinoMesa, columna,fila);
    		this.fila = this.fila+1;
		}
    	if (this.opcProduccionDiaria!=null)
    	{
    		this.botones.addComponent(this.opcProduccionDiaria,columna,fila);
    		this.fila = this.fila+1;
    	}
    	if (this.opcPreparacionPedidos!=null)
		{
    		this.botones.addComponent(this.opcCargasVinoMesa,columna,fila);
    		this.fila = this.fila+1;
		}

    	//columna 1
    	this.fila=0;
    	this.columna = 1;
    	
    	if (this.opcSituacionEnvasadora!=null)
		{
    		this.botones.addComponent(this.opcSituacionEnvasadora,columna,fila);
    		this.fila = this.fila+1;
		}
    	if (this.opcCalculoNecesidades!=null)
    	{
    		this.botones.addComponent(this.opcCalculoNecesidades,columna,fila);
    		this.fila = this.fila+1;
    	}
    	
    	//columna 2
    	this.fila=0;
    	this.columna = 2;
    	
    	if (this.opcControlProgramacion!=null)
    	{
    		this.botones.addComponent(this.opcControlProgramacion,columna,fila);
    		this.fila = this.fila+1;
    	}
    	if (this.opcControlProgramacionEnvasadora!=null)
    	{
    		this.botones.addComponent(this.opcControlProgramacionEnvasadora,columna,fila);
    		this.fila = this.fila+1;
    	}
    	if (this.opcControlRetrabajos!=null)
    	{
    		this.botones.addComponent(this.opcControlRetrabajos,columna,fila);
    		this.fila = this.fila+1;
    	}

    	this.botones.setSpacing(true);
    	principal.addComponent(this.botones);

    }
    private void cargarListeners()
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			close();
    		}
    	});
    	
    }
    
	@Override
	public void close()
	{
		super.close();
	}


	// columna 0
    private void cargarCRMArticulo()
    {
    	if (this.fila<=0) this.fila=0;
    	if (this.columna<=0) this.columna=0;
    	this.opcCRMArticulo=new Button("CRM Articulo");
		this.opcCRMArticulo.setEnabled(true);
		this.opcCRMArticulo.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCRMArticulo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
    	this.opcCRMArticulo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  pantallaCRMArticulo pt = new pantallaCRMArticulo("CRM Articulo");
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }
    
    private void cargarCargasVinoMesa()
    {
    	if (this.fila<=1) this.fila=1;
    	if (this.columna<=0) this.columna=0;
    	this.opcCargasVinoMesa=new Button("Cargas Mercadona");
		this.opcCargasVinoMesa.setEnabled(true);
		this.opcCargasVinoMesa.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCargasVinoMesa.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
    	this.opcCargasVinoMesa.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  PantallaCargasVinoMesa pt = new PantallaCargasVinoMesa("Cargas Mercadona");
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }
    
	// columna 1
    private void cargarProduccionDiaria()
    {
    	if (this.fila<=2) this.fila=2;
    	if (this.columna<=0) this.columna=0;
    	this.columna = 0;

    	this.opcProduccionDiaria=new Button("Produccion Diaria");
		this.opcProduccionDiaria.setEnabled(true);
		this.opcProduccionDiaria.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcProduccionDiaria.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		
    	this.opcProduccionDiaria.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  PantallaProduccionDiaria pt = new PantallaProduccionDiaria();
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }
    
    private void cargarSituacionEnvasadora()
    {
    	if (this.fila<=0) this.fila=0;
    	if (this.columna<=1) this.columna=1;

    	this.opcSituacionEnvasadora=new Button("Situacion Envasadora");
		this.opcSituacionEnvasadora.setEnabled(true);
		this.opcSituacionEnvasadora.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcSituacionEnvasadora.addStyleName(ValoTheme.BUTTON_PRIMARY);
		
    	this.opcSituacionEnvasadora.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  PantallaSituacionEnvasadora pt = new PantallaSituacionEnvasadora();
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }
    
    private void cargarCalculoNecesidades()
    {
    	if (this.fila<=1) this.fila=1;
    	if (this.columna<=1) this.columna=1;

    	this.opcCalculoNecesidades=new Button("Situacion Embotelladora");
		this.opcCalculoNecesidades.setEnabled(true);
		this.opcCalculoNecesidades.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCalculoNecesidades.addStyleName(ValoTheme.BUTTON_PRIMARY);
		
    	this.opcCalculoNecesidades.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  pantallaCalculoNecesidades pt = new pantallaCalculoNecesidades("Situacion Embotelladora");
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }
    
    private void cargarControlProgramacion()
    {
    	if (this.fila<=0) this.fila=0;
    	if (this.columna<=2) this.columna=2;

    	this.opcControlProgramacion=new Button("Control Programacion");
		this.opcControlProgramacion.setEnabled(true);
		this.opcControlProgramacion.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcControlProgramacion.addStyleName(ValoTheme.BUTTON_DANGER);
		
    	this.opcControlProgramacion.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
          	  pantallaControlProgramacionEmbotellado pt = new pantallaControlProgramacionEmbotellado("Control Programacion");
          	  getUI().addWindow(pt);
          	  close();
    		}
    	});
    }

    private void cargarControlProgramacionEnvasadora()
    {
    	if (this.fila<=1) this.fila=1;
    	if (this.columna<=2) this.columna=2;
    	
    	this.opcControlProgramacionEnvasadora=new Button("Control Programacion Envasadora");
    	this.opcControlProgramacionEnvasadora.setEnabled(true);
    	this.opcControlProgramacionEnvasadora.addStyleName(ValoTheme.BUTTON_DANGER);
    	this.opcControlProgramacionEnvasadora.addStyleName(ValoTheme.BUTTON_TINY);
    	
    	this.opcControlProgramacionEnvasadora.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			pantallaControlProgramacionEnvasasdo pt = new pantallaControlProgramacionEnvasasdo("Control Programacion Envasadora");
    			getUI().addWindow(pt);
    			close();
    		}
    	});
    }
    
}

