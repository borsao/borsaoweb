package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.server.consultaCambiosProvocadosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.ProgramacionAux.server.consultaProgramacionServerAux;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.server.consultaSituacionEmbotelladoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionFormatoImpresion extends Window
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
		
	private ComboBox cmbPrinter= null;
	private ComboBox cmbTipo= null;
	private Label descPapel = null;
	private TextField codigo = null;
	private CheckBox chkValorado = null;
	private TextField nif = null;
	private TextField tercero = null;
	
	private String area = null;
	private Integer id = null;
	private String semana = null;
	private String titular = null;
	private String sql = null;
	private String ejercicio = null;
	private ArrayList<MapeoGlobal> vector = null;
	/*
	 * Valores devueltos
	 */
	
	public PeticionFormatoImpresion(Integer r_id, String r_area)
	{
		this.id=r_id;
		this.area = r_area;
		
		this.cargarPantalla();
	}

	public PeticionFormatoImpresion(ArrayList<MapeoGlobal> r_vector,  String r_area)
	{
		this.vector=r_vector;
		this.area = r_area;
		
		this.cargarPantalla();
	}

	public PeticionFormatoImpresion(Integer r_id, String r_area, String r_sql, String r_valor)
	{
		this.id=r_id;
		this.area = r_area;
		this.sql = r_sql;
		
		this.cargarPantalla();
		this.cmbTipo.setValue(r_valor);
		this.cmbTipo.setEnabled(false);
	}

	public PeticionFormatoImpresion(String r_area, String r_ejercicio, String r_semana)
	{
		this.area = r_area;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;
		this.cargarPantalla();
	}

	public PeticionFormatoImpresion(String r_area, String r_ejercicio, String r_semana, String r_titular)
	{
		this.area = r_area;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;
		this.titular=r_titular;
		
		this.cargarPantalla();
	}

	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);

			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
			
				this.codigo=new TextField("Artículo / NC");
				this.codigo.setWidth("150px");

				this.nif=new TextField("NIF");
				this.nif.setWidth("150px");

				this.tercero=new TextField("Origen/Destino");
				this.tercero.setWidth("350px");

				fila1.addComponent(this.codigo);
				fila1.addComponent(this.nif);
				fila1.addComponent(this.tercero);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				fila2.addComponent(cmbPrinter);

				this.cmbTipo=new ComboBox("Tipo");
				this.cargarComboTipos();
				this.cmbTipo.setInvalidAllowed(false);
				this.cmbTipo.setNewItemsAllowed(false);
				this.cmbTipo.setNullSelectionAllowed(false);
				this.cmbTipo.setRequired(true);				
				this.cmbTipo.setWidth("200px");
				fila2.addComponent(cmbTipo);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);

				this.descPapel = new Label();
				this.descPapel.setWidth("100px");				
				fila3.addComponent(this.descPapel);
				
				this.chkValorado = new CheckBox("Valorado?");
				this.chkValorado.setValue(false);
				this.chkValorado.setVisible(false);
				fila3.addComponent(this.chkValorado);

		if(this.titular!=null) controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Formatos Impresion");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();		
	}

	private void habilitarCampos()
	{
	
		this.cmbPrinter.setEnabled(true);
	}
	
	private void cargarListeners()
	{

		cmbTipo.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				establecerLabel();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if ((cmbTipo.getValue()!=null && cmbTipo.getValue().toString().length()>0) && (cmbPrinter.getValue()!=null && cmbPrinter.getValue().toString().length()>0))
				{
					String pdfGenerado = generarPdf();

			    	if(pdfGenerado!=null && pdfGenerado.length()>0)
			    	{
			    		if (cmbPrinter.getValue().toString().equals("Pantalla"))
			    		{
			    			RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
			    		}
			    		else				    			
			    		{
			    			String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
							RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
							RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
			    		}
			    	}	
			    	else
			    		Notificaciones.getInstance().mensajeError("Error en la generacion");
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos");
				}
			}});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
	}

	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}
	
	private void establecerLabel()
	{
		chkValorado.setVisible(false);
		if (cmbTipo.getValue().toString().equals("InformeProveedor"))
		{
			descPapel.setCaption("Informe No Conformidad de Proveedor");
			chkValorado.setVisible(true);
		}
		if (cmbTipo.getValue().toString().equals("Rechazo"))	descPapel.setCaption("Papel ROJO");
		if (cmbTipo.getValue().toString().equals("Resumen"))	descPapel.setCaption("");
	}
	
	private void cargarComboTipos()
	{
		switch (this.area)
		{
			case "Calidad":
			{
				this.cmbTipo.addItem("Rechazo");
				this.cmbTipo.addItem("Resumen");
				this.cmbTipo.addItem("InformeProveedor");
				break;
			}
			case "Calendario":
			{
				this.cmbTipo.addItem("Vacaciones");
				this.cmbTipo.addItem("Turnos");
				this.cmbTipo.addItem("Personal");
				this.cmbTipo.addItem("Almacen");
				break;
			}
			case "Programacion":
			{
				this.cmbTipo.addItem("Normal");
				this.cmbTipo.addItem("Laboratorio");
				break;
			}
			case "ProgramacionAux":
			{
				this.cmbTipo.addItem("Normal");
				break;
			}
			case "MovimientosSilicie":
			{
				this.cmbTipo.addItem("InventarioNC");
				this.cmbTipo.addItem("InventarioArticulo");
				this.cmbTipo.addItem("MovimientosNC");
				this.cmbTipo.addItem("MovimientosArticulo");
				break;
			}
			case "Necesidades":
			{
				this.cmbTipo.addItem("General");
				this.cmbTipo.addItem("Laboratorio");
				this.cmbTipo.addItem("Produccion");
				this.cmbTipo.addItem("Produccion Et.");
				this.cmbTipo.addItem("Planificacion");
				break;
			}
			case "CambiosPlanificacion":
			{
				this.cmbTipo.addItem("Relacion");
				this.cmbTipo.addItem("Indicador");
				break;
			}
		}
	}
	
	private String generarPdf()
	{
		String pdfGenerado = null;
		switch (area)
		{
			case "Calidad":
			{
				consultaNoConformidadesServer cps = new consultaNoConformidadesServer(CurrentUser.get());
				if (chkValorado.getValue())
				{
					pdfGenerado = cps.imprimirNoConformidad(this.id, "ProveedorValorado");
				}
				else
				{
					pdfGenerado = cps.imprimirNoConformidad(this.id, this.cmbTipo.getValue().toString());
				}
				break;
			}
			case "Calendario":
			{
				consultaCalendarioServer ccs = new consultaCalendarioServer(CurrentUser.get());
				pdfGenerado = ccs.imprimirAgenda(this.sql, this.cmbTipo.getValue().toString());
				break;
			}
			case "Programacion":
			{
		    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		    	pdfGenerado = cps.imprimirProgramacion(this.ejercicio,this.semana, this.cmbTipo.getValue().toString());
		    	break;
			}
			case "ProgramacionAux":
			{
		    	consultaProgramacionServerAux cps = new consultaProgramacionServerAux(CurrentUser.get());
		    	pdfGenerado = cps.imprimirProgramacion(this.ejercicio,this.semana, this.cmbTipo.getValue().toString());
		    	break;
			}

			case "MovimientosSilicie":
			{
				Integer id = null;
				String art = null;
				String nif = null;
				String tercero = null;
				
				if (this.codigo.getValue()!=null && this.codigo.getValue().toString().length()>0) art = this.codigo.getValue().toString();
				if (this.nif.getValue()!=null && this.nif.getValue().length()>0) nif = this.nif.getValue().toString();
				if (this.tercero.getValue()!=null && this.tercero.getValue().length()>0) tercero = this.tercero.getValue().toString();
				
				consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
				if (this.cmbTipo.getValue().toString().contains("Inventario")) id = cmss.generacionDatosTemporalExistencias(this.cmbTipo.getValue().toString(),this.ejercicio,this.semana, this.titular, art);
				pdfGenerado = cmss.imprimirGeneracion(this.ejercicio,this.semana, this.cmbTipo.getValue().toString(), this.titular, id, art, nif, tercero);
				break;
			}
			case "Necesidades":
			{
				consultaSituacionEmbotelladoServer cses = consultaSituacionEmbotelladoServer.getInstance(CurrentUser.get());
	    		pdfGenerado = cses.generarInforme(this.vector, true, this.cmbTipo.getValue().toString());

				break;
			}
			
			case "CambiosPlanificacion":
			{
				consultaCambiosProvocadosServer cps = consultaCambiosProvocadosServer.getInstance(CurrentUser.get());
	    		pdfGenerado = cps.imprimirCambiosProgramacion(new Integer(this.ejercicio), new Integer(this.semana), this.cmbTipo.getValue().toString());

				break;
			}
		}
		return pdfGenerado;
	}
}