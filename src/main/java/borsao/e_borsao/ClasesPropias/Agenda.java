package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.shared.ui.calendar.DateConstants;
import com.vaadin.ui.Calendar;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.BackwardEvent;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.DateClickEvent;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.EventClick;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.EventClickHandler;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.ForwardEvent;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.MoveEvent;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.WeekClick;
import com.vaadin.ui.components.calendar.CalendarComponentEvents.WeekClickHandler;
import com.vaadin.ui.components.calendar.event.CalendarEvent;
import com.vaadin.ui.components.calendar.handler.BasicBackwardHandler;
import com.vaadin.ui.components.calendar.handler.BasicDateClickHandler;
import com.vaadin.ui.components.calendar.handler.BasicEventMoveHandler;
import com.vaadin.ui.components.calendar.handler.BasicForwardHandler;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoCalendario;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.CalendarioView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class Agenda extends Calendar  
{

	public String identificadorCalendario = null;
//	public GridViewRefresh vista = null;
	public String identificadorAlmacen = null;
	public boolean menuCreado = false;
	public AgendaEvento eventoCopiado = null;
	public boolean isSoloLectura = true;
	private CalendarioView calendarioView = null;
	
	public Agenda(CalendarioView r_view, String r_id, String r_titulo, Integer r_ancho, Integer r_alto, String r_almacen)
	{
		this.identificadorCalendario=r_id;
		this.identificadorAlmacen=r_almacen;
		this.calendarioView = r_view;
//		this.vista = r_view;
		this.setCaption("");
    	this.setSizeFull();
    	this.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	this.setFirstVisibleHourOfDay(6);
    	this.setLastVisibleHourOfDay(22);
    	if (r_almacen!=null)
    	{
        	this.setFirstVisibleHourOfDay(7);
        	this.setLastVisibleHourOfDay(18);    		
    	}
		this.setResponsive(true);
		
//    	this.setWidthUndefined();
//    	this.setWidth(r_ancho.toString() + "px");
//    	this.setHeight(r_alto.toString() + "px");
    	
    	GregorianCalendar startDate = new GregorianCalendar();
    	startDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	startDate.setTime(this.getStartDate());

    	GregorianCalendar endDate = new GregorianCalendar();
    	endDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	endDate.setTime(this.getEndDate());    	
//    	endDate.add(java.util.Calendar.HOUR, 24);
    	
		this.eliminarEventos();

    	recuerparDatosBBDD(startDate.getTime(),endDate.getTime());
    	this.establecerModo();
    	this.eventosNavegacion();
	}
	
	private void eventosNavegacion()
	{
		if (!isSoloLectura)
		{
			this.setHandler(new BasicEventMoveHandler() {
		
				public void eventMove(MoveEvent event) 
				{
					eliminarDatosBBDD((AgendaEvento) event.getCalendarEvent());
					super.eventMove(event);
					guardarEvento((AgendaEvento) event.getCalendarEvent());
				}
			});
		}
		else
		{
			/*
			 * anula la posibilidad de mover eventos
			 */
			
			BasicEventMoveHandler bemh = null;
			this.setHandler(bemh);
		}
			
		this.setHandler(new WeekClickHandler() {
			
			@Override
			public void weekClick(WeekClick event) 
			{
				eliminarEventos();
				Calendar cal = event.getComponent();
				int semana = event.getWeek();
	    		GregorianCalendar weekstart = new GregorianCalendar();
	    			weekstart.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
	    			weekstart.set(java.util.Calendar.WEEK_OF_YEAR, semana);
	          	  	weekstart.setFirstDayOfWeek(java.util.Calendar.MONDAY);
	          	  	weekstart.set(java.util.Calendar.HOUR_OF_DAY, 7);
	          	  	weekstart.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.MONDAY);
	          
	      	  	GregorianCalendar weekend   = new GregorianCalendar();
	      	  		weekend.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
	      	  		try {
						weekend.setTime(RutinasFechas.conversionDeString(RutinasFechas.sumarDiasFecha(RutinasFechas.convertirDateToString(weekstart.getTime()),6)));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	      	  		weekend.set(java.util.Calendar.HOUR_OF_DAY, 16);
	      	  		weekend.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.SUNDAY);
		          
	  	  		cal.setStartDate(weekstart.getTime());
	  	  		cal.setEndDate(weekend.getTime());
	  	  		
				recuerparDatosBBDD(cal.getStartDate(), cal.getEndDate());
				calendarioView.agen.setCaption("");
				calendarioView.btnAnterior.setVisible(false);
				calendarioView.btnSiguiente.setVisible(false);
			}
		});
		
		this.setHandler(new BasicDateClickHandler() 
    	{
    	    public void dateClick(DateClickEvent event) 
    	    {
    	    	eliminarEventos();
    	    	Calendar cal = event.getComponent();

    	    	// Check if the current range is already one day long
    	    	long currentCalDateRange = cal.getEndDate().getTime() - cal.getStartDate().getTime();

    	    	// From one-day view, zoom out to week view
    	    	if (currentCalDateRange <= DateConstants.DAYINMILLIS) 
    	    	{
    	    		// Change the date range to the current week
    	    		GregorianCalendar weekstart = new GregorianCalendar();
    	    			weekstart.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	          	  	weekstart.setTime(event.getDate());
    	          	  	weekstart.setFirstDayOfWeek(java.util.Calendar.MONDAY);
    	          	  	weekstart.set(java.util.Calendar.HOUR_OF_DAY, 7);
    	          	  	weekstart.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.MONDAY);
	    	          
	          	  	GregorianCalendar weekend   = new GregorianCalendar();
	          	  		weekend.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
	          	  		weekend.setTime(event.getDate());
	          	  		weekend.set(java.util.Calendar.HOUR_OF_DAY, 16);
	          	  		weekend.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.SUNDAY);
	    	          
          	  		cal.setStartDate(weekstart.getTime());
          	  		cal.setEndDate(weekend.getTime());
          	  		
    	    	}
    	    	

    	    	else 
    	    	{
    	    		// Default behavior, change date range to one day
    	    		super.dateClick(event);
    	    	}
    	    	
    	    	recuerparDatosBBDD(cal.getStartDate(), cal.getEndDate());
    	    	calendarioView.agen.setCaption("");
    	    	calendarioView.btnAnterior.setVisible(false);
    	    	calendarioView.btnSiguiente.setVisible(false);
    	    }
    	  });

    	this.setHandler(new BasicBackwardHandler() {
    		  protected void setDates(BackwardEvent event, Date start, Date end) {
    			  eliminarEventos();
    			  recuerparDatosBBDD(start, end);
    		      super.setDates(event, start, end);
    		  }});
    	
    	this.setHandler(new BasicForwardHandler() {
  		  protected void setDates(ForwardEvent event, Date start, Date end) {
  			  eliminarEventos();
  			  recuerparDatosBBDD(start, end);
  		      super.setDates(event, start, end);
  		  }});
    	
		this.setHandler(new EventClickHandler() {
    	    public void eventClick(EventClick event) {
    	    	AgendaEvento ev = ((AgendaEvento) event.getCalendarEvent());
//    	    	Notificaciones.getInstance().mensajeSeguimiento(ev.getCaption());
    	    	if (ev.getArea()!=null && ev.getArea().equals("Carga"))
    	    	{
    	    		pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(new Integer(ev.getDescription()), "Lineas Pedido Venta: " + ev.getDescription());
    	    		getUI().addWindow(vt);
    	    	}
    	    	else if (ev.getArea()!=null && ev.getArea().equals("Descarga"))
    	    	{
    	    		pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(new Integer(ev.getDescription()), "Lineas Pedido Compra: " + ev.getDescription());
    	    		getUI().addWindow(vt);
    	    	}
    	    	else
    	    	{
    	    		
    	    		PantallaEventoAgenda vt = new PantallaEventoAgenda(calendarioView, isSoloLectura, ((Agenda) event.getComponent()).identificadorCalendario, ((Agenda) event.getComponent()).identificadorAlmacen, event);
    	    		getUI().addWindow(vt);
    	    	}
//    	    	vista.opcImprimir.setEnabled(true);
    	    }
    	});
		
		
    	this.addActionHandler(new Handler() {
   		 	Action[] ac = null;
   		 	@Override
   		    public Action[] getActions(Object target, Object sender) 
   		 	{
//		    	Notificaciones.getInstance().mensajeError(sender.getClass().getTypeName()); //agenda
//		    	Notificaciones.getInstance().mensajeError(target.getClass().getTypeName()); // calendardaterange
//		    	Notificaciones.getInstance().mensajeError("get Action");

   		      	if (sender.getClass().getTypeName().contains("Agenda") && !menuCreado && !isSoloLectura) 
   		      	{
   		      		ac = new Action[] { new Action("Nuevo"), new Action("Editar"), new Action("Copiar"), new Action("Pegar"), new Action("Eliminar"), new Action("Completar") };
   		      		menuCreado=true;
   		      	}
   		      	return ac;
   		    }
   		 
   		    @Override
   		    public void handleAction(Action action, Object sender, Object target) 
   		    {
//   		    	Notificaciones.getInstance().mensajeError(sender.getClass().getTypeName()); //Agenda
//   		    	Notificaciones.getInstance().mensajeError(target.getClass().getTypeName()); //AgendaEvento
//   		    	Notificaciones.getInstance().mensajeError("Handle Action");
   		    	if (target.getClass().getTypeName().contains("AgendaEvento"))
   		    	{
   		    		if (action.getCaption() == "Editar") 
   		    		{
   		    			AgendaEvento evt = (AgendaEvento) target;
   		    			Agenda age = (Agenda) sender;
   		    			if (identificadorCalendario!="Almacen" && evt.getValorAviso()!=null) 
   		    			{
   		    				PantallaEventoAgenda vt = new PantallaEventoAgenda(calendarioView, action.getCaption(), age,evt);
   		    				eBorsao.get().getUI().addWindow(vt);
   		    			}
   		    			else Notificaciones.getInstance().mensajeAdvertencia("Este evento no se puede modificar");
   		    		}
   		    		else if (action.getCaption() == "Eliminar") 
   		    		{
   		    			AgendaEvento e = (AgendaEvento) target;
   		    			if (identificadorCalendario!="Almacen" && e.getValorAviso()!=null) eliminarDatosBBDD(e); else Notificaciones.getInstance().mensajeAdvertencia("Este evento no se puede eliminar");
   		    		}
   		    		else if (action.getCaption() == "Copiar") 
   		    		{
   		    			eventoCopiado = (AgendaEvento) target;
   		    		}
   		    		else if (action.getCaption() == "Completar") 
   		    		{
   		    			AgendaEvento ev = (AgendaEvento) target;
   		    			if (identificadorCalendario!="Almacen" && ev.getValorAviso()!=null) 
   		    			{
   		    				ev.setCompletada(true);
   		    				ev.setStyleName(null);
	   		    			markAsDirty();
	   		    			String rdo = completarDatosBBDD(ev);
	   		    			if (rdo!=null)
	   		    				Notificaciones.getInstance().mensajeAdvertencia("Este evento no se puede Completar");
   		    			}
		    			else Notificaciones.getInstance().mensajeAdvertencia("Este evento no se puede Completar");
   		    		}
   		    		else if (action.getCaption() == "Nuevo") 
   		    		{
   		    			AgendaEvento evt = (AgendaEvento) target;
   		    			Agenda ag = (Agenda) sender;
   		    			PantallaEventoAgenda vt = new PantallaEventoAgenda(calendarioView, action.getCaption(), ag, evt.getStart());
   		    			eBorsao.get().getUI().addWindow(vt);
   		    		}
   		    	}
   		    	else if (target.getClass().getTypeName().contains("Date"))
   		    	{
   		    		if (action.getCaption() == "Nuevo") 
   		    		{
   		    			Date evt = (Date) target;
   		    			Agenda ag = (Agenda) sender;
   		    			PantallaEventoAgenda vt = new PantallaEventoAgenda(calendarioView, action.getCaption(), ag, evt);
   		    			eBorsao.get().getUI().addWindow(vt);
   		    		}
   		    		else if (action.getCaption() == "Pegar") 
   		    		{
   		    			if (eventoCopiado!=null)
   		    			{
	   		    			Date evt = (Date) target;
	   		    			AgendaEvento agPegar = new AgendaEvento();
	   		    			agPegar.setAlmacen(eventoCopiado.getAlmacen());
	   		    			agPegar.setArea(eventoCopiado.getArea());
	   		    			agPegar.setTipo(eventoCopiado.getTipo());
	   		    			agPegar.setEjercicioContable(eventoCopiado.getEjercicioContable());
	   		    			
	   		    			Notificaciones.getInstance().mensajeSeguimiento(eventoCopiado.getStyleName());
	   		    			
	   		    			agPegar.setStyleName(eventoCopiado.getStyleName());
	   		    			agPegar.setAviso(eventoCopiado.isAviso());
	   		    			agPegar.setCaption(eventoCopiado.getCaption());
	   		    			agPegar.setPropietario(eventoCopiado.getPropietario());
	   		    			agPegar.setDescription(eventoCopiado.getDescription());
	   		    			agPegar.setAllDay(eventoCopiado.isAllDay());
	   		    			agPegar.setCompletada(eventoCopiado.isCompletada());
	   		    			agPegar.setValorAviso(eventoCopiado.getValorAviso());
	   		    			
	   		    			agPegar.setStart(evt);	   		    			
	   		    				Long duracion = ((eventoCopiado.getEnd().getTime()-eventoCopiado.getStart().getTime())/3600000);
	   		    				GregorianCalendar end = new GregorianCalendar();
	   		    				end.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
	   		    				end.setTime(evt);
	   		    				end.add(java.util.Calendar.HOUR, duracion.intValue());
	   		    			agPegar.setEnd(end.getTime());
	   		    			MapeoCalendario mapeo = convertirEventoEnMapeo(agPegar);
	   		    			String rdo = guardarNuevosDatosBBDD(mapeo);
	   		    			if (rdo!=null)
	   		    			{
	   		    				agPegar.setId(rdo);
	   		    				addEvent(agPegar);
	   		    				markAsDirty();
	   		    			}
   		    			}
   		    			else
   		    			{
   		    				Notificaciones.getInstance().mensajeAdvertencia("No hay datos a pegar");
   		    			}
   		    		}
   		    		else if (action.getCaption() == "Eliminar") 
   		    		{
   		    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a Eliminar");
   		    		}
   		    		else if (action.getCaption() == "Copiar") 
   		    		{
   		    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a Copiar");
   		    		}
   		    		else if (action.getCaption() == "Editar") 
   		    		{
   		    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a Editar");
   		    		}
   		    		else if (action.getCaption() == "Completar") 
   		    		{
   		    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a Completar");
   		    		}
   		    	}
   		    	else
   		    	{
   		    		Notificaciones.getInstance().mensajeError(target.getClass().getTypeName());
   		    	}
   		    }
   		  });

	}
	
	public void añadirEvento(String r_propietario, String r_titulo, String r_descripcion, Date r_inicio, Integer r_duracion, boolean r_allDay, Date r_aviso, boolean r_mostrarAviso, String r_formulaAviso, String r_area, String r_almacen, boolean r_recuperar, String r_tipo, String r_ejercicioContable)
	{
		String rdo =null;
    	MapeoCalendario mapeo = new MapeoCalendario();
    	
    	AgendaEvento evento = new AgendaEvento();
    	
    	evento.setCaption(r_titulo);
    	evento.setPropietario(r_propietario);
    	evento.setAlmacen(r_almacen);
    	evento.setDescription(r_descripcion);
    	evento.setAllDay(r_allDay);
    	
    	evento.setStyleName(this.recogerEstilo(r_allDay,false,r_area));
    	if (r_propietario.contentEquals("Turnos")|| r_propietario.contentEquals("Vacaciones"))
    	{
    		evento.setArea(r_area);
    		evento.setTipo(r_tipo);
    		evento.setEjercicioContable(r_ejercicioContable);
    	}
    	else if (r_propietario.equals("Personal"))
    	{
    		evento.setPropietario(eBorsao.get().accessControl.getNombre());
    	}

    	evento.setAviso(r_aviso);
    	evento.setValorAviso(r_formulaAviso);
    	evento.setAviso(r_mostrarAviso);
    	evento.setCompletada(false);    	
    	
    	GregorianCalendar start = new GregorianCalendar();
    	start.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	start.setTime(r_inicio);
    	
    	GregorianCalendar end = new GregorianCalendar();
    	end.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	end.setTime(r_inicio);
    	end.add(java.util.Calendar.HOUR, r_duracion.intValue());
    	
    	evento.setStart(r_inicio);
    	evento.setEnd(end.getTime());
    	
    	if (r_recuperar)
    	{
	    	mapeo.setCaption(r_titulo);
	    	
	    	if (this.identificadorCalendario.equals("Personal"))
	    	{
	    		
	    		mapeo.setPropietario(eBorsao.get().accessControl.getNombre());
	    	}
	    	else
	    	{
	    		mapeo.setPropietario(r_propietario);
	    	}
	    	mapeo.setAlmacen(r_almacen);
	    	mapeo.setDescription(r_descripcion);
	    	mapeo.setAllDay(r_allDay);
	    	mapeo.setStyleName(this.recogerEstilo(r_allDay,false,r_area));
	    	mapeo.setAviso(r_aviso);
	    	mapeo.setValorAviso(r_formulaAviso);
	    	mapeo.setAviso(r_mostrarAviso);
	    	mapeo.setCompletada(false);
	    	mapeo.setStart(r_inicio);
	    	mapeo.setEnd(end.getTime());
	    	
	    	if (r_area!=null && r_area.length()>0) 
	    		mapeo.setAlmacen(r_area); 
	    	else 
	    		mapeo.setAlmacen(null);
	    	
	    	if (r_area!=null )
	    	{
	    		mapeo.setTipo(r_tipo);
	    		mapeo.setEjercicioContable(r_ejercicioContable);
	    	}
	    	if (r_recuperar) rdo = this.guardarNuevosDatosBBDD(mapeo);
	    	
	    	Notificaciones.getInstance().mensajeSeguimiento(rdo);
	    	
	    	if (rdo!=null)
	    	{
	    		evento.setId(rdo);
	    		mapeo.setId(Integer.valueOf(rdo));
	    	}
    	}
    	this.addEvent(evento);
    	this.markAsDirty();
	}

	public void añadirEvento(String r_id, String r_propietario, String r_titulo, String r_descripcion, Date r_inicio, Date r_fin, boolean r_allDay, Date r_aviso, boolean r_mostrarAviso, String r_formulaAviso, String r_area, String r_almacen, boolean r_completada, boolean r_recuperar, String r_tipo, String r_ejercicioContable)
	{
		String rdo =null;
    	MapeoCalendario mapeo = new MapeoCalendario();
    	AgendaEvento evento = new AgendaEvento();
    	
    	evento.setId(r_id);
    	evento.setCaption(r_titulo);
    	evento.setPropietario(r_propietario);
    	evento.setAlmacen(r_almacen);
    	evento.setDescription(r_descripcion);
    	evento.setAllDay(r_allDay);
    	if (!r_completada) evento.setStyleName(this.recogerEstilo(r_allDay,false,r_area));
    	evento.setAviso(r_aviso);
    	evento.setValorAviso(r_formulaAviso);
    	evento.setAviso(r_mostrarAviso);
    	evento.setCompletada(r_completada);
    	if (r_propietario.contentEquals("Turnos")|| r_propietario.contentEquals("Vacaciones"))
    	{
    		evento.setArea(r_area);
    		evento.setTipo(r_tipo);
    		evento.setEjercicioContable(r_ejercicioContable);
    	}

    	
    	GregorianCalendar inicio = new GregorianCalendar();
    	inicio.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	inicio.setTime(r_inicio);
    	evento.setStart(inicio.getTime());
    	
    	GregorianCalendar fin = new GregorianCalendar();
    	fin.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
    	fin .setTime(r_fin);
    	evento.setEnd(fin.getTime());
    	
    	if (r_recuperar)
    	{
//	    	mapeo.setId(new Integer(r_id));
	    	mapeo.setCaption(r_titulo);
	    	mapeo.setPropietario(r_propietario);
	    	mapeo.setAlmacen(r_almacen);
	    	mapeo.setDescription(r_descripcion);
	    	mapeo.setAllDay(r_allDay);

	    	if (!r_completada) mapeo.setStyleName(this.recogerEstilo(r_allDay,false,r_area));
	    	mapeo.setAviso(r_aviso);
	    	mapeo.setValorAviso(r_formulaAviso);
	    	mapeo.setAviso(r_mostrarAviso);
	    	mapeo.setCompletada(r_completada);
	    	mapeo.setStart(r_inicio);
	    	mapeo.setEnd(r_fin);
	    	if (r_area!=null && r_area.length()>0) mapeo.setAlmacen(r_area);
	    	
	    	rdo = this.guardarNuevosDatosBBDD(mapeo);
	    	if (rdo!=null)
	    	{
	    		evento.setId(rdo);
	    		mapeo.setId(Integer.valueOf(rdo));
	    	}
    	}
    	this.addEvent(evento);
    	this.markAsDirty();
	}

	public void añadirEvento(MapeoCalendario r_mapeo)
	{
    	AgendaEvento evento = new AgendaEvento();
    	
    	evento.setId(r_mapeo.getId().toString());
    	evento.setCaption(r_mapeo.getDescription().trim() + "(" + r_mapeo.getCaption() + ")");
    	evento.setPropietario(r_mapeo.getPropietario());
    	evento.setAlmacen(r_mapeo.getAlmacen());
    	evento.setDescription(r_mapeo.getCaption());
    	evento.setAllDay(r_mapeo.isAllDay());
    	evento.setStyleName(r_mapeo.getStyleName());
    	evento.setAviso(r_mapeo.getAviso());
    	evento.setValorAviso(r_mapeo.getValorAviso());
    	evento.setAviso(r_mapeo.isAviso());
    	evento.setCompletada(r_mapeo.isCompletada());
    	evento.setArea(r_mapeo.getArea());
    	evento.setStart(r_mapeo.getStart());
    	evento.setEnd(r_mapeo.getEnd());
    	evento.setEjercicioContable(r_mapeo.getEjercicioContable());
    	evento.setTipo(r_mapeo.getTipo());
    	
    	this.addEvent(evento);
    	this.markAsDirty();
	}

	public void recuerparDatosBBDD(Date r_start, Date r_end)
	{
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());
		ArrayList<MapeoCalendario> rdo = ccs.datosOpcionesGlobal(identificadorCalendario, identificadorAlmacen, r_start, r_end);				
		
		if (rdo!=null)
		{
			for (int i = 0; i<rdo.size();i++)
			{
				MapeoCalendario mapeo = (MapeoCalendario) rdo.get(i);
				if (identificadorCalendario.equals("Almacen"))
					this.añadirEvento(mapeo);
				else
					this.añadirEvento(mapeo.getId().toString(),mapeo.getPropietario(), mapeo.getCaption(), mapeo.getDescription(), mapeo.getStart(), mapeo.getEnd(), mapeo.isAllDay(), mapeo.getAviso(), mapeo.isAviso(), mapeo.getValorAviso(), mapeo.getArea(), mapeo.getAlmacen(),mapeo.isCompletada(), false, mapeo.getTipo(), mapeo.getEjercicioContable());
			}
		}
	}
	
	public void guardarEvento(AgendaEvento r_evento)
	{
		MapeoCalendario mapeo = new MapeoCalendario();
		mapeo = this.convertirEventoEnMapeo(r_evento);
		String rdo = this.guardarNuevosDatosBBDD(mapeo);
		r_evento.setId(rdo);
		mapeo.setId(new Integer(rdo));
		this.añadirEvento(mapeo);
		this.markAsDirty();
	}
	public String guardarNuevosDatosBBDD(MapeoCalendario r_mapeo)
	{
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());
		String rdo = ccs.guardarNuevo(r_mapeo);
		return rdo;
	}
	
	
	private String recogerEstilo(boolean r_allDay, boolean r_complete, String r_area)
	{
		String estilo=null;
		if (r_complete) estilo =  "";
		else if (this.identificadorCalendario.equals("Vacaciones"))
		{
			if (r_area!=null && r_area.equals("Generales"))
				estilo="cyan";
			else if (r_area!=null && r_area.equals("Embotelladora") || r_area!=null && r_area.equals("Envasadora") || r_area!=null && r_area.equals("BIB"))
				estilo = "red";
			else if (r_area!=null && r_area.equals("Limpieza"))
				estilo = "green";
			else if (r_area!=null && r_area.equals("Bodega") || r_area!=null && r_area.equals("Laboratorio"))
				estilo = "pink";
			else if (r_area!=null && r_area.equals("Almacen"))
				estilo = "yellow";
			else if (r_area!=null && r_area.equals("Tecnicos"))
				estilo = "white";
			else
				estilo = "white";
//				estilo = "white";

		}
		else if (this.identificadorCalendario.equals("Personal"))
			estilo = "white";
		else if (this.identificadorCalendario.equals("Turnos"))
		{
			if (r_area!=null && r_area.equals("Embotelladora"))
				estilo = "yellow";
			else if (r_area!=null && r_area.equals("Envasadora"))
				estilo = "green";
			else
				estilo = "cyan";
		}
		else
			estilo =  "";
		
		return estilo;
	}
	
	public void eliminarEventos()
	{
		List<CalendarEvent> evs = this.getEvents(this.getStartDate(), this.getEndDate());
		
		for (int i=0; i< evs.size(); i++)
		{
			this.removeEvent(evs.get(i));
			this.markAsDirty();
		}
	}
	
	public Integer generarEventosParaImprimir()
	{
		Integer rdo = null;
		String rdoBBDD=null;
		String tabla = null;
		String prop = null;
		MapeoCalendario mapeo = null;
		consultaCalendarioServer ccs = null;

		prop = eBorsao.get().accessControl.getNombre().replace(" ", "") ;
		tabla = this.identificadorCalendario;
		ccs = new consultaCalendarioServer(CurrentUser.get());
		
		rdoBBDD=ccs.eliminarImpresion(prop, tabla);
		
		if (rdoBBDD==null)
		{
			rdoBBDD=ccs.crearTabla(prop, tabla);
			if (rdoBBDD==null)
			{
				List<CalendarEvent> evs = this.getEvents(this.getStartDate(), this.getEndDate());
				
				for (int i=0; i< evs.size(); i++)
				{
					mapeo=this.convertirEventoEnValoresImprimir((AgendaEvento) evs.get(i));
					
					switch (mapeo.getPropietario())
					{
						case "Turnos":
						{
							rdo=ccs.guardarNuevoImprimir(rdo, mapeo, prop+tabla);
							break;
						}
						case "Vacaciones":
						{
							rdo=ccs.guardarNuevoImprimirVacaciones(rdo, mapeo, prop+tabla);
							break;
						}
						
	//					case "Personal":
	//					{
	//						rdo=ccs.guardarNuevoImprimirPersonal(rdo, mapeo);
	//						break;
	//					}
	//					case "Almacen":
	//					{
	//						rdo=ccs.guardarNuevoImprimirAlmacen(rdo, mapeo);
	//						break;
	//					}
					}
					if (rdo==null)
					{
						Notificaciones.getInstance().mensajeError("No se ha podido insertar los datos en la la tabla temporal." );
						break;
					}
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido crear la tabla temporal para almacenar los datos" );
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No se ha podido inicializar las tablas para la generacion del informe" );
		}
		
		return rdo;
	}

	public void copiarEventos()
	{
		List<CalendarEvent> evs = this.getEvents(this.getStartDate(), this.getEndDate());
		PantallaCopiaEventosAgenda vt = new PantallaCopiaEventosAgenda(this.identificadorCalendario, this, evs);
		eBorsao.get().getUI().addWindow(vt);
	}

	private void eliminarDatosBBDD(AgendaEvento r_evento)
	{
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());
		String rdo = ccs.eliminar(Integer.valueOf(r_evento.getId()));
		if (rdo==null) this.removeEvent(r_evento);
		this.markAsDirty();
	}
	
	private String completarDatosBBDD(AgendaEvento r_evento)
	{
		consultaCalendarioServer ccs = consultaCalendarioServer.getInstance(CurrentUser.get());
		String rdo = ccs.completar(Integer.valueOf(r_evento.getId()));
		return rdo;
	}

	private MapeoCalendario convertirEventoEnMapeo(AgendaEvento r_evento)
	{
		MapeoCalendario mapeo = new MapeoCalendario();
		
    	mapeo.setCaption(r_evento.getCaption());
    	mapeo.setPropietario(r_evento.getPropietario());
    	
    	if (r_evento.getPropietario().equals("Turnos"))
    		mapeo.setAlmacen(r_evento.getArea());
    	else
    		mapeo.setAlmacen(r_evento.getAlmacen());
    	
    	mapeo.setDescription(r_evento.getDescription());
    	mapeo.setAllDay(r_evento.isAllDay());
    	mapeo.setStyleName(r_evento.getStyleName());
    	mapeo.setAviso(r_evento.getAviso());
    	mapeo.setValorAviso(r_evento.getValorAviso());
    	mapeo.setAviso(r_evento.isAviso());
    	mapeo.setCompletada(r_evento.isCompletada());
    	mapeo.setArea(r_evento.getArea());
    	mapeo.setTipo(r_evento.getTipo());
    	mapeo.setEjercicioContable(r_evento.getEjercicioContable());
    	mapeo.setStart(r_evento.getStart());
    	mapeo.setEnd(r_evento.getEnd());
    	return mapeo;

	}
	
	private MapeoCalendario convertirEventoEnValoresImprimir(AgendaEvento r_evento)
	{
		MapeoCalendario mapeo = new MapeoCalendario();
		
		mapeo.setPropietario(r_evento.getPropietario());
    	mapeo.setCaption(r_evento.getCaption());
    	mapeo.setDescription(r_evento.getDescription());
		mapeo.setStart(r_evento.getStart());
		mapeo.setTurno(RutinasFechas.obtenerFranjaFecha(r_evento.getStart()));
		mapeo.setSemana(RutinasFechas.obtenerSemanaFecha(r_evento.getStart()));
		mapeo.setMes(new Integer(RutinasFechas.mesFecha(r_evento.getStart())));
		mapeo.setArea(r_evento.getArea());
    	mapeo.setTipo(r_evento.getTipo());
    	mapeo.setEjercicioContable(r_evento.getEjercicioContable());

    	return mapeo;

	}

	private void establecerModo()
	{
		if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))
		{
			isSoloLectura=false;
		}
		else if (this.identificadorCalendario.equals("Personal"))
		{
			isSoloLectura=false;
		}
		else if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Almacen")  && this.identificadorCalendario.equals("Almacen"))
		{
			isSoloLectura=false;
		}
		else if (((eBorsao.get().accessControl.getNombre().contains("David Ferrandez")) || (eBorsao.get().accessControl.getNombre().contains("ecepcion")) || (eBorsao.get().accessControl.getNombre().contains("men Candado"))) && this.identificadorCalendario.equals("Vacaciones") )
		{
			isSoloLectura=false;
		}
		else if (eBorsao.get().accessControl.getNombre().contains("Gracia") && this.identificadorCalendario.equals("Turnos"))
		{
			isSoloLectura=false;
		}
		else
			isSoloLectura=true;
	}

}


