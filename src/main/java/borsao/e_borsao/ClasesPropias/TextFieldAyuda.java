package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

public class TextFieldAyuda extends CustomComponent {

	public TextField txtTexto = null;
	public Button btnAyuda = null;
	private ventanaAyuda vtAyuda = null;
	private String titulo = null;
	private String tabla = null;
	private String campoDevolver = null;
	private String campoMostrar = null;
	private String condicionWhere = null;
	private HorizontalLayout layout = null;
	
	public TextFieldAyuda (String r_caption,String r_prefijo) {

		this.titulo=r_caption;
		
		this.establecerCondiciones(this.titulo);
		if (titulo.equals("ARTICULO VENTAS")) titulo="ARTICULO";
		if (!this.tabla.equals(""))
		{
			layout = new HorizontalLayout();
			setCompositionRoot(layout);
	
			txtTexto = new TextField();
			txtTexto.addStyleName(ValoTheme.TEXTFIELD_TINY);
			layout.addComponent(txtTexto);
			txtTexto.setCaption(r_prefijo.toUpperCase() + " " + this.titulo.toUpperCase());
			
			btnAyuda = new Button("?");
			btnAyuda.addStyleName(ValoTheme.BUTTON_TINY);
			btnAyuda.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					ayuda(tabla, campoDevolver, campoMostrar);
				}
			});
			
//			btnAyuda.addStyleName(ValoTheme.BUTTON_PRIMARY);
//			btnAyuda.addStyleName("v-button-help");
			btnAyuda.setDescription("Seleccionar " + this.titulo);
			layout.addComponent(btnAyuda);
			layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Este campo " + this.titulo + " no está vinculado con la base de datos.");
		}
	}

	
	public TextFieldAyuda (String r_caption,String r_prefijo, String r_valor) {

		this.titulo=r_caption;
		
		this.establecerCondiciones(this.titulo);
		
		if (!this.tabla.equals(""))
		{
			layout = new HorizontalLayout();
			setCompositionRoot(layout);
	
			txtTexto = new TextField();
			layout.addComponent(txtTexto);
			txtTexto.setCaption(r_prefijo.toUpperCase() + " " + this.titulo.toUpperCase());
			
			btnAyuda = new Button("?");
			btnAyuda.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					if (txtTexto.getValue()!=null)
					{
						ayudaLike(tabla, campoDevolver, campoMostrar, txtTexto.getValue());
					}
					else
					{
						Notificaciones.getInstance().mensajeInformativo("Debes introducir un valor");
					}
				}
			});
			
//			btnAyuda.addStyleName(ValoTheme.BUTTON_PRIMARY);
//			btnAyuda.addStyleName("v-button-help");
			btnAyuda.setDescription("Seleccionar " + this.titulo);
			layout.addComponent(btnAyuda);
			layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Este campo " + this.titulo + " no está vinculado con la base de datos.");
		}
	}

	public TextFieldAyuda(String r_caption,String r_prefijo, String r_valor, String r_where) {

		this.titulo=r_caption;
		this.condicionWhere=r_where;
		this.establecerCondiciones(this.titulo);
		
		if (!this.tabla.equals(""))
		{
			layout = new HorizontalLayout();
			setCompositionRoot(layout);
	
			txtTexto = new TextField();
			layout.addComponent(txtTexto);
			txtTexto.setCaption(r_prefijo.toUpperCase() + " " + this.titulo.toUpperCase());
			
			btnAyuda = new Button("?");
			btnAyuda.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					if (txtTexto.getValue()!=null)
					{
						ayudaLikeWhere(tabla, campoDevolver, campoMostrar, txtTexto.getValue());
					}
					else
					{
						Notificaciones.getInstance().mensajeInformativo("Debes introducir un valor");
					}
				}
			});
			
//			btnAyuda.addStyleName(ValoTheme.BUTTON_PRIMARY);
//			btnAyuda.addStyleName("v-button-help");
			btnAyuda.setDescription("Seleccionar " + this.titulo);
			layout.addComponent(btnAyuda);
			layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Este campo " + this.titulo + " no está vinculado con la base de datos.");
		}
	}
	
	private void ayuda(String r_tabla, String r_campoCodigo, String r_campoDescripcion)
	{
		/*
		 * Recogemos los registros a mostrar
		 * Abrimos ventana con el resultado de la consulta sobre la tabla indicada
		 * Mostramos en un grid el resultado de la consulta
		 * 
		 * Dos botones Aceptar y Cancelar para gestionar los eventos de la venta
		 * 
		 * El boton aceptar de esta ventana capturará el selectedrow para devolverme el codigo inicial
		 */
		ArrayList<MapeoAyudas> vectorRegistros = null;
		
		TextFieldAyudaServer ayudaServer = new TextFieldAyudaServer();
		
		if (this.condicionWhere!=null)
		{
			vectorRegistros=ayudaServer.ayudaTablaWhere(r_tabla, r_campoCodigo, r_campoDescripcion, this.condicionWhere);
		}
		else
		{
			vectorRegistros=ayudaServer.ayudaTabla(r_tabla, r_campoCodigo, r_campoDescripcion);
		}
		
		this.vtAyuda = new ventanaAyuda(this, vectorRegistros, "Ayuda de " + this.titulo);
		getUI().addWindow(this.vtAyuda);
	}

	private void ayudaLike(String r_tabla, String r_campoCodigo, String r_campoDescripcion, String r_valor)
	{
		/*
		 * Recogemos los registros a mostrar
		 * Abrimos ventana con el resultado de la consulta sobre la tabla indicada
		 * Mostramos en un grid el resultado de la consulta
		 * 
		 * Dos botones Aceptar y Cancelar para gestionar los eventos de la venta
		 * 
		 * El boton aceptar de esta ventana capturará el selectedrow para devolverme el codigo inicial
		 */
		ArrayList<MapeoAyudas> vectorRegistros = null;
		
		TextFieldAyudaServer ayudaServer = new TextFieldAyudaServer();
		vectorRegistros=ayudaServer.ayudaTablaLike(r_tabla, r_campoCodigo, r_campoDescripcion, r_valor);
		
		this.vtAyuda = new ventanaAyuda(this, vectorRegistros, "Ayuda de " + this.titulo);
		getUI().addWindow(this.vtAyuda);
	}

	private void ayudaLikeWhere(String r_tabla, String r_campoCodigo, String r_campoDescripcion, String r_valor)
	{
		/*
		 * Recogemos los registros a mostrar
		 * Abrimos ventana con el resultado de la consulta sobre la tabla indicada
		 * Mostramos en un grid el resultado de la consulta
		 * 
		 * Dos botones Aceptar y Cancelar para gestionar los eventos de la venta
		 * 
		 * El boton aceptar de esta ventana capturará el selectedrow para devolverme el codigo inicial
		 */
		ArrayList<MapeoAyudas> vectorRegistros = null;
		
		TextFieldAyudaServer ayudaServer = new TextFieldAyudaServer();
		vectorRegistros=ayudaServer.ayudaTablaLikeWhere(r_tabla, r_campoCodigo, r_campoDescripcion, r_valor, this.condicionWhere);
		
		this.vtAyuda = new ventanaAyuda(this, vectorRegistros, "Ayuda de " + this.titulo);
		getUI().addWindow(this.vtAyuda);
	}

	public void botonAceptarAyuda(String r_valor)
	{
		this.txtTexto.setValue(r_valor);
		getUI().removeWindow(this.vtAyuda);
		this.vtAyuda=null;
	}
	
	private void establecerCondiciones(String r_titulo)
	{
		switch (r_titulo.toUpperCase())
		{
			case "PAIS":
			{
				this.tabla="e_paises";
				this.campoDevolver="pais";
				this.campoMostrar="nombre";
				break;
			}
			case "CLIENTE":
			{
				this.tabla="e_client";
				this.campoDevolver="cliente";
				this.campoMostrar="nombre_comercial";
				break;
			}
			case "NIVEL":
			{
				this.titulo="GRUPO CLIENTE";
				this.tabla="e_cli_ea";
				this.campoDevolver="nivel";
				this.campoMostrar="nom_abreviado";
				break;
			}
			case "GREMIO":
			{
				this.tabla="e_gremio";
				this.campoDevolver="gremio";
				this.campoMostrar="descripcion";
				break;
			}
			case "FAMILIA":
			{
				this.tabla="e_famili";
				this.campoDevolver="familia";
				this.campoMostrar="descripcion";
				break;
			}
			case "GRUPO_ARTICULO":
			{
				this.titulo="GRUPO ARTICULO";
				this.tabla="e__grp_c";
				this.campoDevolver="grupo";
				this.campoMostrar="descripcion";
				break;
			}
			case "ARTICULO":
			{
				this.tabla="e_articu";
				this.campoDevolver="articulo";
				this.campoMostrar="descrip_articulo";
				break;
			}
			case "ARTICULO VENTAS":
			{
				this.tabla="e_articu";
				this.campoDevolver="articulo";
				this.campoMostrar="descrip_articulo";
				this.condicionWhere="tipo_articulo='PT'";
				break;
			}

			case "MOVIMIENTOS":
			{
				this.tabla="movimientos";
				this.campoDevolver="lote_salida";
				this.campoMostrar="";
				break;
			}
			
			case "LOTE":
			{
				this.tabla="a_ubic_0";
				this.campoDevolver="numero_lote";
				this.campoMostrar="";
				break;
			}
			
			default:
			{
				this.tabla="";
				this.campoDevolver="";
				this.campoMostrar="";
				break;				
			}
		
		}
	}
}