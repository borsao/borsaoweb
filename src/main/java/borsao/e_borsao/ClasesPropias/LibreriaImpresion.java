package borsao.e_borsao.ClasesPropias;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class LibreriaImpresion
{
	private Notificaciones serNotif=null;
	public Integer codigo = null;
	public String codigoTxt = null;
	public String codigoDateD = null;
	public String codigoDateH = null;
	public String codigoD = null;
	public String codigoH = null;
	public String rutaTxt = null;
	public Integer codigo2 = null;
	public String codigo2Txt = null;
	public String codigo3Txt = null;
	public String codigo4Txt = null;
	public String codigo5Txt = null;
	public String codigo6Txt = null;
	public ArrayList<Integer> codigoList=null;
	public String archivoDefinitivo = null;
	public String archivoPlantilla = null;
	public String archivoPlantillaDetalle = null;
	public String archivoPlantillaDetalleCompilado = null;
	public String archivoPlantillaNotas = null;
	public String archivoPlantillaNotasCompilado = null;
	public String carpeta = null;
	private String backGround = null;
	private String backGroundEtiqueta = null;
	
	private connectionManager conManager = null;
	
	public LibreriaImpresion()
	{
		this.conManager= connectionManager.getInstance();
		serNotif=Notificaciones.getInstance();
		backGround=LecturaProperties.baseImagePath + "fondoA4PedidoCompra.jpg";
		backGroundEtiqueta=LecturaProperties.baseImagePath + "fondoA4LogoBlanco.jpg";
	}
	
	public String generacionFinal()
    {
		Connection con = null;
    	JasperDesign jasperDesign=null;
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		FileInputStream file = null;

		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap<String, Object> masterParams = new HashMap<String, Object>();
    		
    		jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantilla());
    		
    		if (jasperDesign!=null)
    		{
    			
//    			JRDesignQuery query = new JRDesignQuery();
//    			query.setText("select * from prueba");    			
//    			jasperDesign.setQuery(query);
    			
    			masterReport = this.compilarReport(jasperDesign);
    			
    			masterParams.put("codigo", this.getCodigo());
    			if (this.getBackGround()!=null)
    			{
    				masterParams.put("backGround", this.getBackGround());
    			}
    			if (this.getRutaTxt()!=null && this.getRutaTxt().length()>0 )
    			{
    				masterParams.put("ruta", this.getRutaTxt());
    			}
    			
    			if (this.getArchivoPlantillaDetalle()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaDetalle());
    				if (jasperDesign!=null)
    				{
    					JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaDetalleCompilado());
    					file = new FileInputStream(this.getArchivoPlantillaDetalleCompilado());
    					
    					masterParams.put("detalle", file);
    					masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
    				}
    				else
    				{
    					serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme detalle");
    					return "Error con la generacion del subinforme detalle";
    				}
    				
    			}
    			if (this.getArchivoPlantillaNotas()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaNotas());
        			if (jasperDesign!=null)
        			{
        				JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaNotasCompilado());
        				file = new FileInputStream(this.getArchivoPlantillaNotasCompilado());
        				
        				masterParams.put("notas", file);
        				masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
        			}
        			else
        			{
        				serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme notas");
        				return "Error con la generacion del subinforme notas";
        			}
    			}
    			
    			con=this.conManager.establecerConexionInd();
    			
    			masterPrint = this.fillReport(masterReport, masterParams, con);
    			
    			if (masterPrint!=null)
    			{
    				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
    			}
    			else
    			{
    				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
    				return "Error con la generacion del informe ";
    			}
    		}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
    	catch (FileNotFoundException e) 
		{
			serNotif.mensajeInformativo("No se pudo cargar el subinforme " + e.getMessage());
			return "No se pudo cargar el subinforme " + e.getMessage();
		}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }
	
	public String generacionFinalList()
    {
		Connection con =null;
    	JasperDesign jasperDesign=null;
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		FileInputStream file = null;

		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
    		jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantilla());
    		
    		if (jasperDesign!=null)
    		{
    			masterReport = this.compilarReport(jasperDesign);
    			
    			masterParams.put("codigo", this.getCodigoList());
    			if (this.getBackGround()!=null)
    			{
    				masterParams.put("backGround", this.getBackGround());
    			}
    			
    			if (this.getArchivoPlantillaDetalle()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaDetalle());
    				if (jasperDesign!=null)
    				{
    					JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaDetalleCompilado());
    					file = new FileInputStream(this.getArchivoPlantillaDetalleCompilado());
    					
    					masterParams.put("detalle", file);
    					masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
    				}
    				else
    				{
    					serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme detalle");
    					return "Error con la generacion del subinforme detalle";
    				}
    				
    			}
    			if (this.getArchivoPlantillaNotas()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaNotas());
        			if (jasperDesign!=null)
        			{
        				JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaNotasCompilado());
        				file = new FileInputStream(this.getArchivoPlantillaNotasCompilado());
        				
        				masterParams.put("notas", file);
        				masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
        			}
        			else
        			{
        				serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme notas");
        				return "Error con la generacion del subinforme notas";
        			}
    			}
    			
    			con=this.conManager.establecerConexionInd();
    			
    			masterPrint = this.fillReport(masterReport, masterParams, con);
    			
    			if (masterPrint!=null)
    			{
    				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
    			}
    			else
    			{
    				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
    				return "Error con la generacion del informe ";
    			}
    		}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
    	catch (FileNotFoundException e) 
		{
			serNotif.mensajeInformativo("No se pudo cargar el subinforme " + e.getMessage());
			return "No se pudo cargar el subinforme " + e.getMessage();
		}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }
	
	public String generacionFinalTxt()
    {
		Connection con =null;
    	JasperDesign jasperDesign=null;
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		FileInputStream file = null;

		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
    		jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantilla());
    		
    		if (jasperDesign!=null)
    		{
    			masterReport = this.compilarReport(jasperDesign);
    			
    			masterParams.put("codigo", this.getCodigoTxt());
    			if (this.getBackGround()!=null)
    			{
    				masterParams.put("backGround", this.getBackGround());
    			}
    			
    			if (this.getArchivoPlantillaDetalle()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaDetalle());
    				if (jasperDesign!=null)
    				{
    					JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaDetalleCompilado());
    					file = new FileInputStream(this.getArchivoPlantillaDetalleCompilado());
    					
    					masterParams.put("detalle", file);
    					masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
    				}
    				else
    				{
    					serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme detalle");
    					return "Error con la generacion del subinforme detalle";
    				}
    				
    			}
    			if (this.getArchivoPlantillaNotas()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaNotas());
        			if (jasperDesign!=null)
        			{
        				JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaNotasCompilado());
        				file = new FileInputStream(this.getArchivoPlantillaNotasCompilado());
        				
        				masterParams.put("notas", file);
        				masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
        			}
        			else
        			{
        				serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme notas");
        				return "Error con la generacion del subinforme notas";
        			}
    			}
    			
    			con=this.conManager.establecerConexionInd();
    			
    			masterPrint = this.fillReport(masterReport, masterParams, con);
    			
    			if (masterPrint!=null)
    			{
    				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
    			}
    			else
    			{
    				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
    				return "Error con la generacion del informe ";
    			}
    		}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
    	catch (FileNotFoundException e) 
		{
			serNotif.mensajeInformativo("No se pudo cargar el subinforme " + e.getMessage());
			return "No se pudo cargar el subinforme " + e.getMessage();
		}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }

	public String generacionFinalBetweenDates()
    {
		Connection con =null;
    	JasperDesign jasperDesign=null;
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		FileInputStream file = null;

		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
    		jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantilla());
    		
    		if (jasperDesign!=null)
    		{
    			masterReport = this.compilarReport(jasperDesign);
    			
    			masterParams.put("desde", this.getCodigoDateD());
    			masterParams.put("hasta", this.getCodigoDateH());
    			masterParams.put("dsd", this.getCodigoD());
    			masterParams.put("hst", this.getCodigoH());
    			if (this.getBackGround()!=null)
    			{
    				masterParams.put("backGround", this.getBackGround());
    			}
    			
    			if (this.getArchivoPlantillaDetalle()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaDetalle());
    				if (jasperDesign!=null)
    				{
    					JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaDetalleCompilado());
    					file = new FileInputStream(this.getArchivoPlantillaDetalleCompilado());
    					
    					masterParams.put("detalle", file);
    					masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
    				}
    				else
    				{
    					serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme detalle");
    					return "Error con la generacion del subinforme detalle";
    				}
    				
    			}
    			if (this.getArchivoPlantillaNotas()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaNotas());
        			if (jasperDesign!=null)
        			{
        				JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaNotasCompilado());
        				file = new FileInputStream(this.getArchivoPlantillaNotasCompilado());
        				
        				masterParams.put("notas", file);
        				masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
        			}
        			else
        			{
        				serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme notas");
        				return "Error con la generacion del subinforme notas";
        			}
    			}
    			
    			con=this.conManager.establecerConexionInd();
    			
    			masterPrint = this.fillReport(masterReport, masterParams, con);
    			
    			if (masterPrint!=null)
    			{
    				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
    			}
    			else
    			{
    				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
    				return "Error con la generacion del informe ";
    			}
    		}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
    	catch (FileNotFoundException e) 
		{
			serNotif.mensajeInformativo("No se pudo cargar el subinforme " + e.getMessage());
			return "No se pudo cargar el subinforme " + e.getMessage();
		}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }

	public String generacionInformeInteger()
    {
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		Connection con =null;

		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
			File ficheroEtiquetaCompilado = new File(LecturaProperties.baseReportsPath + this.getCarpeta() + "/" + this.getArchivoPlantilla());
			
			masterReport = (JasperReport) JRLoader.loadObject(ficheroEtiquetaCompilado);
			
			masterParams.put("codigo", this.getCodigo());
			if (this.getCodigo2()!=null) masterParams.put("codigo2", this.getCodigo2());
			masterParams.put("backGround", this.getBackGroundEtiqueta());
			
			con=this.conManager.establecerConexionInd();
			
			masterPrint = this.fillReport(masterReport, masterParams, con);
			
			if (masterPrint!=null)
			{
				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
			}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }

	public String generacionInformeTxt()
    {
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		Connection con =null;
		
		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
			File ficheroEtiquetaCompilado = new File(LecturaProperties.baseReportsPath + this.getCarpeta() + "/" + this.getArchivoPlantilla());
			
			masterReport = (JasperReport) JRLoader.loadObject(ficheroEtiquetaCompilado);
			
			masterParams.put("codigo", this.getCodigoTxt());
			if (this.getCodigo2Txt()!=null) masterParams.put("codigo2", this.getCodigo2Txt());
			if (this.getCodigo3Txt()!=null) masterParams.put("codigo3", this.getCodigo3Txt());
			if (this.getCodigo4Txt()!=null) masterParams.put("codigo4", this.getCodigo4Txt());
			masterParams.put("backGround", this.getBackGroundEtiqueta());
			
			con=this.conManager.establecerConexionInd();
			
			masterPrint = this.fillReport(masterReport, masterParams, con);
			
			if (masterPrint!=null)
			{
				this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
			}
			else
			{
				serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
				return "Error con la generacion del informe ";
			}
    	}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }

	public String generacionInformeTxtXML()
    {
		Connection con =null;
		JasperDesign jasperDesign=null;
		JasperPrint masterPrint = null;
		JasperReport masterReport = null;
		FileInputStream file = null;
		
		RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + this.getArchivoDefinitivo());
    	
    	try
    	{
    		HashMap masterParams = new HashMap();
    		
    		jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantilla());
    		
    		if (jasperDesign!=null)
    		{
    			masterReport = this.compilarReport(jasperDesign);
			
				masterParams.put("codigo", this.getCodigoTxt());
				if (this.getCodigo2Txt()!=null) masterParams.put("codigo2", this.getCodigo2Txt());
				if (this.getCodigo3Txt()!=null) masterParams.put("codigo3", this.getCodigo3Txt());
				if (this.getCodigo4Txt()!=null) masterParams.put("codigo4", this.getCodigo4Txt());
				if (this.getCodigo5Txt()!=null) masterParams.put("codigo5", this.getCodigo5Txt());
				if (this.getCodigo6Txt()!=null) masterParams.put("codigo6", this.getCodigo6Txt());
				masterParams.put("backGround", this.getBackGroundEtiqueta());
				
//<<<<<<< HEAD
    			if (this.getArchivoPlantillaDetalle()!=null)
    			{
    				jasperDesign = this.cargarJrxml(this.getCarpeta(), this.getArchivoPlantillaDetalle());
    				if (jasperDesign!=null)
    				{
//    					JasperCompileManager.compileReportToFile(jasperDesign, this.getArchivoPlantillaDetalleCompilado());
    					file = new FileInputStream(this.getArchivoPlantillaDetalleCompilado());
    					
    					masterParams.put("detalle", file);
    					masterParams.put("SUBREPORT_DIR", LecturaProperties.baseReportsPath);
    				}
    				else
    				{
    					serNotif.mensajeInformativo("Hay problemas con la generacion del subinforme detalle");
    					return "Error con la generacion del subinforme detalle";
    				}
    				
    			}

				this.conManager = connectionManager.getInstance();
//				con=this.conManager.establecerConexion();
				con=this.conManager.establecerConexionInd();
//>>>>>>> pre-master
				
				masterPrint = this.fillReport(masterReport, masterParams, con);
				
				if (masterPrint!=null)
				{
					this.exportReportToPdf( masterPrint, this.getArchivoDefinitivo());
				}
				else
				{
					serNotif.mensajeInformativo("Hay problemas con la generacion del informe");
					return "Error con la generacion del informe ";
				}
    		}
    	}
    	catch (FileNotFoundException e) 
		{
			serNotif.mensajeInformativo("No se pudo cargar el subinforme " + e.getMessage());
			return "No se pudo cargar el subinforme " + e.getMessage();
		}
		catch (JRException e) 
		{
			serNotif.mensajeInformativo("Hay problemas con la generacion del informe " + e.getMessage());
			return "Hay problemas con la generacion del informe " + e.getMessage();
		}
    	catch (SQLException ex) 
		{
			serNotif.mensajeInformativo("Hay problemas con la conexion de la BBDD " + ex.getMessage());
			return "Hay problemas con la conexion de la BBDD " + ex.getMessage();
		}
    	finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
    }
    /**
     * Load the template (defined by templatePath) and return a JasperDesign object representing the template
     * @return JasperDesign
     */
	private JasperDesign cargarJrxml(String r_carpeta, String r_archivo)
    {
        JasperDesign jasperDesign=null;
        
        File ficheroPlantillaImpresion = new File(LecturaProperties.baseReportsPath + r_carpeta + "/" + r_archivo);
        
        if(ficheroPlantillaImpresion.exists())
        {
            try 
            {
                jasperDesign= JRXmlLoader.load(ficheroPlantillaImpresion);
            }
            catch (JRException e) 
            {
                serNotif.mensajeError("Error in loading the template... "+e.getMessage());
            }
        }
        else
        {
        	serNotif.mensajeError("Error, the file dont exists");
        }
        
        return(jasperDesign);
    }

    /**
     * Compile the report and generates a binary version of it
     * @param jasperDesign The report design
     * @return JasperReport
     */
    private JasperReport compilarReport(JasperDesign r_jasperDesign)
    {
        JasperReport jasperReport=null;
        try 
        {
            jasperReport= JasperCompileManager.compileReport(r_jasperDesign);
        } 
        catch (JRException e) 
        {
            serNotif.mensajeError("Error in compiling the report.... "+e.getMessage());
        }
        return(jasperReport);
    }

    /**
     * Fill the report and generates a binary version of it
     * @param jasperReport The Compiled report design
     * @return JasperPrint
     */
    private JasperPrint fillReport(JasperReport r_jasperReport, HashMap fillParameters, Connection r_conn){
        JasperPrint jasperPrint=null;        
        
        try 
        {
            jasperPrint =JasperFillManager.fillReport(r_jasperReport,fillParameters, r_conn);
        } 
        catch (JRException e) 
        {
            serNotif.mensajeError("Error in filling the report..... "+e.getMessage());
            e.printStackTrace();
        }
        return(jasperPrint);
    }

    /**
     * Prepare a JRExporter for the filled report (to HTML)
     * @param jasperPrint The jasperPrint
     * @return The HTML text
     */
    @SuppressWarnings("deprecation")
    private void exportReportToPdf(JasperPrint r_jasperPrint, String r_nombreFichero) throws JRException 
    {
//    	JasperExportManager.exportReportToPdfFile(r_jasperPrint, CurrentUser.basePdfPath + "/"+ r_nombreFichero);
    	
    	JRExporter exporter = new JRPdfExporter();
          //parameter used for the destined file.
          exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,
        		  LecturaProperties.basePdfPath + "/"+ r_nombreFichero);
          exporter.setParameter(JRExporterParameter.JASPER_PRINT, r_jasperPrint);
          //export to .pdf
          exporter.exportReport();
          exporter = null;
    }
    
	public String getArchivoDefinitivo() {
		return archivoDefinitivo;
	}
	public void setArchivoDefinitivo(String archivoDefinitivo) {
		this.archivoDefinitivo = archivoDefinitivo;
	}
	public String getArchivoPlantilla() {
		return archivoPlantilla;
	}
	public void setArchivoPlantilla(String archivoPlantilla) {
		this.archivoPlantilla = archivoPlantilla;
	}
	public String getArchivoPlantillaDetalle() {
		return archivoPlantillaDetalle;
	}
	public void setArchivoPlantillaDetalle(String archivoPlantillaDetalle) {
		this.archivoPlantillaDetalle = archivoPlantillaDetalle;
	}
	public String getArchivoPlantillaDetalleCompilado() {
		return LecturaProperties.baseReportsPath + this.getCarpeta() + "/" + archivoPlantillaDetalleCompilado;
	}
	public void setArchivoPlantillaDetalleCompilado(String archivoPlantillaDetalleCompilado) {
		this.archivoPlantillaDetalleCompilado = archivoPlantillaDetalleCompilado;
	}
	public String getArchivoPlantillaNotas() {
		return archivoPlantillaNotas;
	}
	public void setArchivoPlantillaNotas(String archivoPlantillaNotas) {
		this.archivoPlantillaNotas = archivoPlantillaNotas;
	}
	public String getArchivoPlantillaNotasCompilado() {
		return LecturaProperties.baseReportsPath + this.getCarpeta() + "/" +  archivoPlantillaNotasCompilado;
	}
	public void setArchivoPlantillaNotasCompilado(String archivoPlantillaNotasCompilado) {
		this.archivoPlantillaNotasCompilado = archivoPlantillaNotasCompilado;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getRutaTxt() {
		return rutaTxt;
	}

	public void setRutaTxt(String rutaTxt) {
		this.rutaTxt = rutaTxt;
	}

	public Integer getCodigo2() {
		return codigo2;
	}

	public void setCodigo2(Integer codigo2) {
		this.codigo2 = codigo2;
	}

	public String getCodigo2Txt() {
		return codigo2Txt;
	}

	public ArrayList<Integer> getCodigoList() {
		return codigoList;
	}

	public void setCodigoList(ArrayList<Integer> codigoList) {
		this.codigoList = codigoList;
	}

	public void setCodigo2Txt(String codigo2Txt) {
		this.codigo2Txt = codigo2Txt;
	}

	public String getCarpeta() {
		return carpeta;
	}
	public void setCarpeta(String carpeta) {
		this.carpeta = carpeta;
	}
	public String getBackGround() {
		return LecturaProperties.baseImagePath + backGround;
	}
	public void setBackGround(String backGround) {
		this.backGround = backGround;
	}

	public String getBackGroundEtiqueta() {
		return LecturaProperties.baseImagePath + backGroundEtiqueta;
	}

	public void setBackGroundEtiqueta(String backGroundEtiqueta) {
		this.backGroundEtiqueta = backGroundEtiqueta;
	}

	public String getCodigoTxt() {
		return codigoTxt;
	}

	public void setCodigoTxt(String codigoTxt) {
		this.codigoTxt = codigoTxt;
	}

	public String getCodigoDateD() {
		return codigoDateD;
	}

	public void setCodigoDateD(String codigoDateD) {
		this.codigoDateD = codigoDateD;
	}

	public String getCodigoDateH() {
		return codigoDateH;
	}

	public void setCodigoDateH(String codigoDateH) {
		this.codigoDateH = codigoDateH;
	}

	public String getCodigoD() {
		return codigoD;
	}

	public void setCodigoD(String codigoD) {
		this.codigoD = codigoD;
	}

	public String getCodigoH() {
		return codigoH;
	}

	public void setCodigoH(String codigoH) {
		this.codigoH = codigoH;
	}

	public String getCodigo3Txt() {
		return codigo3Txt;
	}

	public void setCodigo3Txt(String codigo3Txt) {
		this.codigo3Txt = codigo3Txt;
	}

	public String getCodigo4Txt() {
		return codigo4Txt;
	}

	public void setCodigo4Txt(String codigo4Txt) {
		this.codigo4Txt = codigo4Txt;
	}

	public String getCodigo5Txt() {
		return codigo5Txt;
	}

	public void setCodigo5Txt(String codigo5Txt) {
		this.codigo5Txt = codigo5Txt;
	}

	public String getCodigo6Txt() {
		return codigo6Txt;
	}

	public void setCodigo6Txt(String codigo6Txt) {
		this.codigo6Txt = codigo6Txt;
	}
}