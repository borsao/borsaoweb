package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view.MovimientosSilicieView;

public class CargaFichero extends Window  
{
    private static final long serialVersionUID = -4292553844521293140L;
    
    private Button btnBotonCVentana = null;
    private Button btnBotonAVentana = null;
    private GridViewRefresh origen =null;

    private String ruta = null;
    private String archivo = null;
    private TextField separador = null;
    private TextField fichero = null;
    private Combo encabezados = null;
    private fileReceiver receiver =null;
    
    public CargaFichero(String context, GridViewRefresh r_app, String r_ruta, String r_file)
    {
    	VerticalLayout layout = new VerticalLayout();
    	layout.setSizeFull();
    	this.origen=r_app;
    	this.setRuta(r_ruta + "/");
    	this.archivo=r_file;
    	
		this.setModal(true);
		this.setCaption("Pantalla Seleccion Archivo");
		this.setClosable(true);
		this.setDraggable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("550px");
		
//		this.comprobarTemporales();
		
        if ("basic".equals(context))
            basic(layout);
//        else if ("advanced".equals(context))
//            advanced(layout);
        else
            layout.addComponent(new Label("Invalid context: " + context));
        
        this.setContent(layout);
        this.cargarListeners();
    }

    public void basic(VerticalLayout layout) 
    {
//    	File fil = null;
//		VerticalLayout controles = new VerticalLayout();
//		controles.setMargin(true);
//		controles.setSpacing(true);
		
	    	Panel panel = new Panel("Seleccionar Archivo");
	    	
	    		HorizontalLayout panelContent = new HorizontalLayout();
	    		panelContent.setMargin(true);
	    		// Show uploaded file in this placeholder
	    			
	    			// Implement both receiver that saves upload in a file and
	    			// listener for successful upload
	        
//	    			receiver = new fileReceiver(this.getRuta()); 
	
	    			// Create the upload with a caption and set receiver later
//	    			final Upload upload = new Upload("Selecciona el archivo aqui", receiver);
//	    			upload.setButtonCaption("Cargar ");
//	    			upload.addSucceededListener(receiver);
//	    			upload.setImmediate(true);
	    			
	    			separador = new TextField("Separador de Campos");
	    			separador.setWidth("80px");
	    			separador.setValue(";");
	    			
	    			fichero = new TextField("Nombre fichero");
	    			fichero.setWidth("300px");
	    			fichero.setValue(";");

	    			encabezados = new Combo("Fila de Encabezados ");
	    			encabezados.setWidth("120px");
	    			encabezados.addItem("S");
	    			encabezados.addItem("N");
	    			encabezados.setValue("S");
	    			encabezados.setNullSelectionAllowed(false);
	    			encabezados.setNewItemsAllowed(false);
	    			
	    		// Put the components in a panel
//	    		panelContent.addComponents(upload);
	    		panelContent.addComponents(fichero);
	    		panelContent.addComponents(separador);
	    		panelContent.addComponents(encabezados);
	
	        panel.setContent(panelContent);
	
	        ((HorizontalLayout) panel.getContent()).setSpacing(true);
	        panel.setWidth("-1");        
	        
	        HorizontalLayout botonera = new HorizontalLayout();
			botonera.setMargin(true);
			botonera.setWidthUndefined();
			botonera.setHeight("100px");
			
				btnBotonAVentana = new Button("Confirmar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
			botonera.addComponent(btnBotonAVentana);
			botonera.addComponent(btnBotonCVentana);

		layout.addComponent(panel);
		layout.addComponent(botonera);
		
//        layout.addComponent(controles);
        
        layout.setSpacing(true);
        layout.setMargin(true);
        layout.setHeight("-1");
    }
    
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (archivo!=null && !archivo.equals(receiver.getFilePath()) && receiver.getFile()!=null)
				{
//					receiver.getFile().delete();
				}
				close();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				/*
				 * cogemos la ruta completa de la imagen y lo insertaremos
				 */
//				if (receiver.getFilePath()!=null) Notificaciones.getInstance().mensajeAdvertencia(receiver.getFilePath());
				
				if (archivo!=null && !archivo.equals(receiver.getFilePath())) 
				{
//					File f = new File(archivo);
//					f.delete();
				}
				ejecutarAccion(separador.getValue(), fichero.getValue(), encabezados.getValue().toString()); 
				close();
			}
		});
	}
	
	private void ejecutarAccion(String r_separador, String r_archivo, String r_encabezados)
	{
		if (this.origen instanceof MovimientosSilicieView)
		{
			((MovimientosSilicieView) origen).validar(r_separador, r_archivo, r_encabezados);
		}
	}
	
	private String getRuta() {
		return ruta;
	}
	
	private void setRuta(String ruta) {
		this.ruta = ruta;
	}
}