package borsao.e_borsao.ClasesPropias;

public class RutinasCadenas
{
	public RutinasCadenas()
	{
		
	}
	
	
	public static String reemplazar(String cadenaConvertir, String r_buscar, String r_reemplazar) throws Exception
	{
		String  cadenaConvertida=cadenaConvertir.replace(r_buscar, r_reemplazar);
		
//		String cadenaConvertida = "";
//		int con = 0;
//		
//        String cadenaBuscar = r_buscar; 
//		String cadenaReemplazar = r_reemplazar;
//        for ( con = 0 ; con < cadenaConvertir.length() ; ++con)
//        {
//        	String provisional = String.valueOf(cadenaConvertir.charAt(con)).toString(); 
//        	if ( cadenaBuscar.equals(provisional))
//        	{
//        		cadenaConvertida = cadenaConvertida + cadenaReemplazar;
//        	}
//        	else
//        	{
//        		cadenaConvertida = cadenaConvertida + provisional;		            		
//        	}
//        }        
		return(cadenaConvertida);
//		return(cadenaConvertir);
	}

	public static String conversion(String r_string)
	{
		String nombre = "";
		if (r_string==null) return nombre;
		for (int i = 0; i<r_string.length();i++)
		{
			switch ((int)r_string.charAt(i))
			{
			case 130:
				nombre = nombre + (char)233;
				break;
			case 133:
				nombre = nombre + (char)224;
				break;
			case 138:
				nombre = nombre + (char)232;
				break;
			case 141:
				nombre = nombre + (char)236;
				break;
			case 160:
				nombre = nombre + (char)225;
				break;
			case 161:
				nombre = nombre + (char)237;
				break;
			case 162:
				nombre = nombre + (char)243;
				break;
			case 163:
				nombre = nombre + (char)250;
				break;
			case 164:
				nombre = nombre + (char)241;
				break;
			case 165:
				nombre = nombre + (char)209;
				break;
			case 167:
				nombre = nombre + (char)186;
				break;				
			case 173:
				nombre = nombre + (char)161;
				break;
			default:
				nombre=nombre + r_string.charAt(i);
				
			}
		}
		return nombre;
	}
	
	public static String reemplazarPuntoMiles(String r_valor)
	{
		String cadenaDevolver= "";
		
		for (int i=0; i<r_valor.length();i++)
		{
			if (r_valor.charAt(i)=='.')
			{
				cadenaDevolver=cadenaDevolver+",";
			}
			else
			{
				cadenaDevolver=cadenaDevolver+r_valor.charAt(i);
			}
		}
		return cadenaDevolver;
	}

	public static String reemplazarComaMiles(String r_valor)
	{
		String cadenaDevolver= "";
		
		for (int i=0; i<r_valor.length();i++)
		{
			if (r_valor.charAt(i)==',')
			{
				cadenaDevolver=cadenaDevolver+".";
			}
			else
			{
				cadenaDevolver=cadenaDevolver+r_valor.charAt(i);
			}
		}
		return cadenaDevolver;
	}

	public static String quitarPuntoMiles(String r_valor)
	{
		String cadenaDevolver= "";
		
		for (int i=0; i<r_valor.length();i++)
		{
			if (r_valor.charAt(i)!='.')
			{
				cadenaDevolver=cadenaDevolver+r_valor.charAt(i);
			}
		}
		return cadenaDevolver;
	}
	
	public static String formatCerosIzquierda(String r_valor, Integer r_tamaño)
	{
		String cadenaDevolver= "";
		String cero = "0";
		
		Integer diferencia = r_tamaño - r_valor.length();
		if (diferencia>0)
			cadenaDevolver = new String(new char[diferencia]).replace("\0", cero) + r_valor;	
		else cadenaDevolver=r_valor;
		return cadenaDevolver;
	}
	
	public static String formatCerosDerecha(String r_valor, Integer r_tamaño)
	{
		String cadenaDevolver= "";
		String cero = "0";
		
		Integer diferencia = r_tamaño - r_valor.length();
		if (diferencia>0)
			cadenaDevolver = r_valor + new String(new char[diferencia]).replace("\0", cero);	
		else cadenaDevolver=r_valor;
		return cadenaDevolver;
	}
	public static String formatNuevesDerecha(String r_valor, Integer r_tamaño)
	{
		String cadenaDevolver= "";
		String cero = "9";
		
		Integer diferencia = r_tamaño - r_valor.length();
		if (diferencia>0)
			cadenaDevolver = r_valor + new String(new char[diferencia]).replace("\0", cero);	
		else cadenaDevolver=r_valor;
		return cadenaDevolver;
	}
}
