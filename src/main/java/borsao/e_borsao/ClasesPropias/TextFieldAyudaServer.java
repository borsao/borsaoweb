package borsao.e_borsao.ClasesPropias;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TextFieldAyudaServer
{
	private connectionManager conManager = null;
	private Connection con = null;	
	private Statement cs = null;
	private ResultSet rs = null;
	private MapeoAyudas mapeoAyudas = null;
	public static String nombre = null;
	
	public TextFieldAyudaServer()
	{
		this.conManager = connectionManager.getInstance();		
	}
	
	public ArrayList<MapeoAyudas> ayudaTabla(String r_tabla, String r_campo1, String r_campo2)
	{
		String sql = null;
		ArrayList<MapeoAyudas> vector = null;
		try
		{
			sql="select " + r_campo1 + " as codigo, " + r_campo2 + " as descripcion from " + r_tabla + " order by " + r_campo2;
			
			con= this.conManager.establecerConexion();
			if (con!=null)
			{
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rs = cs.executeQuery(sql);
				vector = new ArrayList<MapeoAyudas>();
					
				rs.beforeFirst();
				while(rs.next())
				{
					mapeoAyudas=new MapeoAyudas();
					mapeoAyudas.setCodigo(rs.getString("codigo"));
					mapeoAyudas.setDescripcion(rs.getString("descripcion"));
					vector.add(mapeoAyudas);
				}
				return vector;
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rs!=null)
				{
					rs.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	public ArrayList<MapeoAyudas> ayudaTablaWhere(String r_tabla, String r_campo1, String r_campo2, String r_where)
	{
		String sql = null;
		ArrayList<MapeoAyudas> vector = null;
		try
		{
			sql="select " + r_campo1 + " as codigo, " + r_campo2 + " as descripcion from " + r_tabla;
			
			if (r_where!=null && r_where.length()>0)
			{
				sql = sql + " where " + r_where;
			}
			sql = sql + " order by " + r_campo2;
			
			con= this.conManager.establecerConexion();
			if (con!=null)
			{
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rs = cs.executeQuery(sql);
				vector = new ArrayList<MapeoAyudas>();
					
				rs.beforeFirst();
				while(rs.next())
				{
					mapeoAyudas=new MapeoAyudas();
					mapeoAyudas.setCodigo(rs.getString("codigo"));
					mapeoAyudas.setDescripcion(rs.getString("descripcion"));
					vector.add(mapeoAyudas);
				}
				return vector;
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rs!=null)
				{
					rs.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public ArrayList<MapeoAyudas> ayudaTablaLike(String r_tabla, String r_campo1, String r_campo2, String r_valor)
	{
		String sql = null;
		ArrayList<MapeoAyudas> vector = null;
		try
		{
			
			sql="select " + r_campo1 + " as codigo " ;
			
			if(r_campo2.length()>0)
			{
				sql = sql + ", " + r_campo2 + " as descripcion ";
			}
			sql = sql + " from " + r_tabla + " where " + r_campo1 + " like '" + r_valor + "%' ";
			
			if (r_campo2.length()>0)
			{
				sql = sql + " order by " + r_campo2;
			}
			else
			{
				sql = sql + " order by " + r_campo1;
			}
			
			con= this.conManager.establecerConexion();
			if (con!=null)
			{
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rs = cs.executeQuery(sql);
				vector = new ArrayList<MapeoAyudas>();
					
				rs.beforeFirst();
				while(rs.next())
				{
					mapeoAyudas=new MapeoAyudas();
					mapeoAyudas.setCodigo(rs.getString("codigo"));
					if (r_campo2.length()>0)
					{
						mapeoAyudas.setDescripcion(rs.getString("descripcion"));
					}
					else
					{
						mapeoAyudas.setDescripcion(null);
					}
					vector.add(mapeoAyudas);
				}
				return vector;
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rs!=null)
				{
					rs.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public ArrayList<MapeoAyudas> ayudaTablaLikeWhere(String r_tabla, String r_campo1, String r_campo2, String r_valor, String r_where)
	{
		String sql = null;
		ArrayList<MapeoAyudas> vector = null;
		try
		{
			
			sql="select " + r_campo1 + " as codigo " ;
			
			if(r_campo2.length()>0)
			{
				sql = sql + ", " + r_campo2 + " as descripcion ";
			}
			sql = sql + " from " + r_tabla + " where " + r_campo1 + " like '" + r_valor + "%' ";
			
			if (r_where!=null && r_where.length()>0)
			{
				sql = sql + " where " + r_where;
			}
			
			if (r_campo2.length()>0)
			{
				sql = sql + " order by " + r_campo2;
			}
			else
			{
				sql = sql + " order by " + r_campo1;
			}
			
			con= this.conManager.establecerConexion();
			if (con!=null)
			{
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rs = cs.executeQuery(sql);
				vector = new ArrayList<MapeoAyudas>();
					
				rs.beforeFirst();
				while(rs.next())
				{
					mapeoAyudas=new MapeoAyudas();
					mapeoAyudas.setCodigo(rs.getString("codigo"));
					if (r_campo2.length()>0)
					{
						mapeoAyudas.setDescripcion(rs.getString("descripcion"));
					}
					else
					{
						mapeoAyudas.setDescripcion(null);
					}
					vector.add(mapeoAyudas);
				}
				return vector;
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rs!=null)
				{
					rs.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeError(ex.getMessage());
			}
		}
		return null;
	}
}
