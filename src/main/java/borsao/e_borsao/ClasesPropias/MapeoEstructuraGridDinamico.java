package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;
import java.util.HashMap;

public class MapeoEstructuraGridDinamico
{
	
	/*
	 * vectores que nos configuran los campos a filtrar, anchuras y demas
	 */
	public ArrayList<String> camposFiltrar = new ArrayList<String>();
	public HashMap<String,String> widthFiltros = new HashMap<String, String>();
	public HashMap<String,String> filtros = new HashMap<String, String>();
	
	//	public String valorSeleccionado=null;
	public HashMap<String , String> opcionesEscogidas = null;
	
	/*
	 * hash que contiene cada columna de la tabla de que tipo es
	 */
	private HashMap<String, String> hashColumnasTiposFijas = null;
	private HashMap<String, String> hashColumnasNombresFijas = null;
	/*
	 * hash que contiene como clave el valor de la columna, como valor el contenido de la base de datos
	 */
//	private ArrayList<MapeoPedidosNacional> vectorDatos=null;
	/*
	 * Array que contiene el titulo de las columnas del grid
	 */
	private ArrayList<String> vectorTitulos=null;
	private ArrayList<String> vectorTitulosHeader=null;
	/*
	 * hash que contiene para cada columna el articulo que recoge
	 */
	private HashMap<String,String> vectorColumnas=null;
	/*
	 * hash que contiene para cada columna a que agrupacion pertenece
	 */
	private HashMap<String,String> hashGamas=null;
	/*
	 * hash que contiene para cada agrupacion su nombre
	 */
	private HashMap<Integer, String> nombreAgrupaciones=null;
	private HashMap<Integer, String> nombreTotalesAgrupaciones=null;
	/*
	 * numero de agrupaciones a presentar
	 */
	private Integer totalAgrupaciones = null;
	
	
	/**
	 * MAPEO QUE CONTIENE LOS DATOS A PRESENTAR EN EL GRID IDNAMICO
	 * TANTO EN SU PARTE FIJA COMO DINAMICA
	 */
	public MapeoEstructuraGridDinamico()
	{
	}


	public ArrayList<String> getVectorTitulos() {
		return vectorTitulos;
	}


	public void setVectorTitulos(ArrayList<String> vectorTitulos) {
		this.vectorTitulos = vectorTitulos;
	}


	public ArrayList<String> getVectorTitulosHeader() {
		return vectorTitulosHeader;
	}


	public void setVectorTitulosHeader(ArrayList<String> vectorTitulosHeader) {
		this.vectorTitulosHeader = vectorTitulosHeader;
	}


	public HashMap<String, String> getVectorColumnas() {
		return vectorColumnas;
	}


	public void setVectorColumnas(HashMap<String, String> vectorColumnas) {
		this.vectorColumnas = vectorColumnas;
	}


	public HashMap<String, String> getHashGamas() {
		return hashGamas;
	}


	public void setHashGamas(HashMap<String, String> hashGamas) {
		this.hashGamas = hashGamas;
	}


	public Integer getTotalAgrupaciones() {
		return totalAgrupaciones;
	}


	public void setTotalAgrupaciones(Integer totalAgrupaciones) {
		this.totalAgrupaciones = totalAgrupaciones;
	}


	public HashMap<Integer, String> getNombreAgrupaciones() {
		return nombreAgrupaciones;
	}


	public void setNombreAgrupaciones(HashMap<Integer, String> nombreAgrupaciones) {
		this.nombreAgrupaciones = nombreAgrupaciones;
	}


	public HashMap<Integer, String> getNombreTotalesAgrupaciones() {
		return nombreTotalesAgrupaciones;
	}


	public void setNombreTotalesAgrupaciones(HashMap<Integer, String> nombreTotalesAgrupaciones) {
		this.nombreTotalesAgrupaciones = nombreTotalesAgrupaciones;
	}


	public HashMap<String, String> getHashColumnasTiposFijas() {
		return hashColumnasTiposFijas;
	}


	public void setHashColumnasTiposFijas(HashMap<String, String> hashColumnasTiposFijas) {
		this.hashColumnasTiposFijas = hashColumnasTiposFijas;
	}


	public HashMap<String, String> getHashColumnasNombresFijas() {
		return hashColumnasNombresFijas;
	}


	public void setHashColumnasNombresFijas(HashMap<String, String> hashColumnasNombresFijas) {
		this.hashColumnasNombresFijas = hashColumnasNombresFijas;
	}


}