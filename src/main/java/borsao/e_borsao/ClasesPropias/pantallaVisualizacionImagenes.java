package borsao.e_borsao.ClasesPropias;

import java.io.File;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.MouseEvents;
import com.vaadin.server.FileResource;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.graficas.contenedorImagen;

public class pantallaVisualizacionImagenes extends Ventana  
{
	private static String with = "800px"; 
	private static String heigth = "600px"; 
	private static String titulo = "VISOR IMAGENES"; 

	private GridLayout panelGlobal= null;
		private HorizontalLayout panelCabecera = null;
		private HorizontalLayout panelCentral = null;
			private HorizontalLayout panelImagen = null;
			private VerticalLayout panelDetalles = null;
		private HorizontalLayout panelPie = null;
	
	private Combo comboArchivos = null;
	private Button btnCerrar = null;
	private Label lblFechaModif = null;
	private Label lblAncho= null;
	private Label lblAlto= null;
	private Label lblRuta= null;
	private Label lblNombre= null;
	private String  ruta= null;
	
	
	public pantallaVisualizacionImagenes(String r_ruta, String r_imagenMostrar)
    {
		this.ruta = r_ruta;
		this.caracteristicas();
		this.cargarPantallaSimple();
	    this.cargarRecurso(r_imagenMostrar);
	    this.cargarListeners();
    }

	public pantallaVisualizacionImagenes(String r_ruta, String[] r_imagenesMostrar)
	{
		this.ruta = r_ruta;
		this.caracteristicas();
		this.cargarPantallaSelector(r_imagenesMostrar);
		this.cargarListeners();
		this.cargarListenersCombo();
		this.cargarCombo(r_imagenesMostrar);
	}
	
	private void caracteristicas()
	{
		this.setWidth(pantallaVisualizacionImagenes.with);
		this.setHeight(pantallaVisualizacionImagenes.heigth);
		this.setCaption(pantallaVisualizacionImagenes.titulo);
		this.setModal(true);
		this.center();
		this.setResizable(true);
		
	}
	
	private void cargarPantallaSimple()
	{
		this.panelGlobal = new GridLayout(1,1);
		this.panelGlobal.setSizeFull();
		
//		this.panelCentral = new HorizontalLayout();
//		this.panelCentral.setSizeFull();
		
		this.panelImagen = new HorizontalLayout();
		this.panelImagen.setSizeFull();
		
		this.panelPie = new HorizontalLayout();
		this.panelPie.setSizeUndefined();
		
			this.btnCerrar = new Button("Cerrar");
			this.btnCerrar.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			this.btnCerrar.setStyleName(ValoTheme.BUTTON_TINY);
		this.panelPie.addComponent(this.btnCerrar);
			
		this.panelGlobal.addComponent(this.panelImagen,0,0); 
		
		this.setContent(this.panelGlobal);
	}
	
	
	private void cargarPantallaSelector(String[] r_imagenesMostrar)
	{
		this.panelGlobal = new GridLayout(2,3);
		this.panelGlobal.setSizeFull();
		
			this.panelCabecera = new HorizontalLayout();
			this.panelCabecera.setSizeUndefined();
			
				this.comboArchivos =  new Combo("Archivos encontrados");
				this.comboArchivos.addStyleName(ValoTheme.COMBOBOX_TINY);
				this.comboArchivos.setNewItemsAllowed(false);
				this.comboArchivos.setNullSelectionAllowed(false);
				
			this.panelCabecera.addComponent(this.comboArchivos);
			this.panelCabecera.setComponentAlignment(this.comboArchivos, Alignment.TOP_LEFT);
			
				this.panelImagen = new HorizontalLayout();
				this.panelImagen.setSizeFull();
	
				this.panelDetalles = new VerticalLayout();
				this.panelDetalles.setSizeFull();
				
					this.lblRuta = new Label();
					this.lblRuta.setCaption("Ruta");
					this.lblRuta.addStyleName(ValoTheme.LABEL_TINY);

					this.lblNombre= new Label();
					this.lblNombre.setCaption("Nombre");
					this.lblNombre.addStyleName(ValoTheme.LABEL_TINY);

					this.lblFechaModif = new Label();
					this.lblFechaModif.setCaption("Fecha Ultima Modificacion");
					this.lblFechaModif.addStyleName(ValoTheme.LABEL_TINY);

					this.lblAncho = new Label();
					this.lblAncho.setCaption("Ancho");
					this.lblAncho.addStyleName(ValoTheme.LABEL_TINY);
					
					this.lblAlto= new Label();
					this.lblAlto.setCaption("Alto");
					this.lblAlto.addStyleName(ValoTheme.LABEL_TINY);
					
				this.panelDetalles.addComponent(this.lblRuta);
				this.panelDetalles.setComponentAlignment(this.lblRuta, Alignment.TOP_LEFT);
				this.panelDetalles.addComponent(this.lblNombre);
				this.panelDetalles.setComponentAlignment(this.lblNombre, Alignment.TOP_LEFT);
				this.panelDetalles.addComponent(this.lblFechaModif);
				this.panelDetalles.setComponentAlignment(this.lblFechaModif, Alignment.TOP_LEFT);
				this.panelDetalles.addComponent(this.lblAncho);
				this.panelDetalles.setComponentAlignment(this.lblAncho, Alignment.TOP_LEFT);
				this.panelDetalles.addComponent(this.lblAlto);
				this.panelDetalles.setComponentAlignment(this.lblAlto, Alignment.TOP_LEFT);
				
			this.panelPie = new HorizontalLayout();
			this.panelPie.setSizeUndefined();
			
				this.btnCerrar = new Button("Cerrar");
				this.btnCerrar.setStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.btnCerrar.setStyleName(ValoTheme.BUTTON_TINY);
				
			this.panelPie.addComponent(this.btnCerrar);
			this.panelPie.setComponentAlignment(this.btnCerrar, Alignment.BOTTOM_LEFT);
			
		this.panelGlobal.addComponent(this.panelCabecera,0,0); 
		this.panelGlobal.addComponent(this.panelImagen,0,1); 
		this.panelGlobal.addComponent(this.panelDetalles,1,1); 
		this.panelGlobal.addComponent(this.panelPie,0,2); 
		this.panelGlobal.setRowExpandRatio(0, 1);
		this.panelGlobal.setRowExpandRatio(1, 2);
		this.panelGlobal.setRowExpandRatio(2, 1);
//		this.panelGlobal.setExpandRatio(this.panelCentral, 2);
		this.setContent(this.panelGlobal);
	}

	private void cargarListenersCombo()
	{
		this.comboArchivos.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
//				cargarRecurso(LecturaProperties.rutaImagenesIntranet + "/" + comboArchivos.getValue().toString());
				cargarRecurso(ruta + "/" + comboArchivos.getValue().toString());
				cargarDetalles(comboArchivos.getValue().toString());
			}
		});
	}

	private void cargarListeners()
	{
		this.btnCerrar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
	}
	
	private void cargarDetalles(String r_imagenMostrar)
	{
		this.lblRuta.setValue(ruta);
		this.lblNombre.setValue(r_imagenMostrar);
		String fechaModif = RutinasFicheros.recuperarFechaUltimaModificacionFichero(ruta, r_imagenMostrar);
		this.lblFechaModif.setValue(fechaModif);
		float ancho  = RutinasFicheros.recuperarAnchuraFichero(ruta, r_imagenMostrar);
		this.lblAncho.setValue(String.valueOf(ancho));
		float alto = RutinasFicheros.recuperarAlturaFichero(ruta, r_imagenMostrar);
		this.lblAlto.setValue(String.valueOf(alto));
	}
	
	private void cargarRecurso(String r_imagenMostrar)
	{
		this.panelImagen.removeAllComponents();
		
    	File fl =new File(r_imagenMostrar);   	
    	if (fl.exists())
    	{
    		FileResource res = new FileResource(fl);
    		
    		final contenedorImagen contenedor = new contenedorImagen(370, 400, res);
    		contenedor.addClickListener(new MouseEvents.ClickListener()
            {
              @Override
              public void click(com.vaadin.event.MouseEvents.ClickEvent event)
              {
                if (MouseButton.LEFT.equals(event.getButton()))
                	getUI().getPage().open(contenedor.getImageSource(), "_blank", false);
              }
            });
    		this.panelImagen.addComponent(contenedor);
    		this.panelImagen.setComponentAlignment(contenedor, Alignment.MIDDLE_CENTER);
    		this.panelImagen.setHeight(String.valueOf(contenedor.getHeight()));
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeAdvertencia("No he ecnontrado la iamgen");
    	}
	}
	
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private void cargarCombo(String[] r_valores)
	{
		if (r_valores!=null && r_valores.length>0)
		{
			for (int i=0; i<r_valores.length; i++)
			{
				this.comboArchivos.addItem(r_valores[i]);
			}
			for (int i=0; i<r_valores.length; i++)
			{
				if (!r_valores[i].contains("-")) this.comboArchivos.setValue(r_valores[i]);
			}
		}		
	}
}
