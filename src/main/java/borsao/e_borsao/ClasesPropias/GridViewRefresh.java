package borsao.e_borsao.ClasesPropias;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Menu;
import borsao.e_borsao.eBorsao;

public abstract class GridViewRefresh extends CssLayout implements View 
{
	
    public HashMap<String , String> opcionesEscogidas = null;
    public VerticalLayout barAndGridLayout = null;
    public HorizontalLayout cabLayout = null;
    public HorizontalLayout topLayoutL = null;
    public HorizontalLayout topLayoutR = null;
    public HorizontalLayout topLayoutC = null;
    public Label lblTitulo = null; 
    public Label lblSeparador = null;
    public Grid grid = null;
    public SelectionListener sl = null;    
    public int intervaloRefresco=15*60*1000;
    public Button opcNuevo = null;
    public Button opcRefresh = null;
    public Button opcBuscar = null;
    public Button opcEliminar = null;
    public Button opcSalir = null;
    public Button opcImprimir = null;
    public Panel pp = null;
	public HorizontalLayout topLayout = null;
	private boolean activarSync = false;
	private boolean pantallaPeticion=false;
	private boolean soloConsulta=false;
	private boolean conFormulario = true;
    private boolean hayGrid=false;	
   
	private ChatRefreshListener cr = null;
	
    public void enter(ViewChangeEvent event)
    {
    	eBorsao.get().mainScreen.menu.verMenu(false);
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);
    	this.generarGraficos();
    }
    
	
	
    private void generarGraficos() {

        this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSpacing(false);        
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");
        this.barAndGridLayout.setResponsive(true);

    	this.topLayout = new HorizontalLayout();
    	this.cabLayout = new HorizontalLayout();
    	this.topLayoutL = new HorizontalLayout();
    	this.topLayoutL.setSpacing(true);
    	this.topLayoutR = new HorizontalLayout();
    	this.topLayoutC = new HorizontalLayout();
    	this.topLayoutC.setSpacing(true);
    	this.topLayout.setWidth("100%");
    	this.topLayout.setSpacing(true);
//    	this.cabLayout.setWidth("50%");
    	this.cabLayout.setSpacing(true);
    	
    	this.opcNuevo= new Button("Crear");
    	this.opcNuevo.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcNuevo.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcNuevo.setIcon(FontAwesome.PLUS_CIRCLE);
    	this.opcNuevo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			botonesGenerales(false);
    			newForm();
    		}
    	});

    	this.opcEliminar= new Button("Eliminar");
    	this.opcEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    	this.opcEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcEliminar.setVisible(false);
    	this.opcEliminar.setIcon(FontAwesome.MINUS_CIRCLE);
    	this.opcEliminar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			botonesGenerales(false);
    			eliminarRegistro();
    		}
    	});


    	this.opcImprimir= new Button("Imprimir");
    	this.opcImprimir.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcImprimir.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcImprimir.setIcon(FontAwesome.PRINT);
    	this.opcImprimir.setVisible(false);
    	this.opcImprimir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			botonesGenerales(false);
    			print();
    		}
    	});

    	this.opcSalir= new Button("Salir");
    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			
    			if (eBorsao.get().mainScreen.menu==null)
    			{
    				eBorsao.get().mainScreen.menu = new Menu(eBorsao.getCurrent().getNavigator());
    				Notificaciones.getInstance().mensajeSeguimiento("No hay menu. lo doy de alta");
    			}
    			
    	    	if (eBorsao.get().accessControl.getPantalla().length()>0)
    	    	{
    	    		eBorsao.getCurrent().getNavigator().navigateTo(eBorsao.get().accessControl.getPantalla());
    	    	}
    	    	else
    	    	{
    	    		eBorsao.getCurrent().getNavigator().navigateTo("");
    	    	}
    	    	
    	    	eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcBuscar= new Button("Buscar");
    	this.opcBuscar.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcBuscar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcBuscar.setIcon(FontAwesome.BINOCULARS);
    	this.opcBuscar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			botonesGenerales(false);
//    			if (grid!=null)
//    			{
//    				barAndGridLayout.removeComponent(grid);
//    				grid=null;
//    				setHayGrid(false);
//    			}
    			verForm(true);
    		}
    	});

    	this.opcRefresh= new Button("Refresh");
    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcRefresh.setIcon(FontAwesome.REFRESH);
    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			actualizarGrid();
    		}
    	});

    	this.lblTitulo= new Label();
    	this.lblTitulo.setValue("VIEW");
    	this.lblTitulo.setStyleName("labelTituloVentas");

    	this.lblSeparador= new Label();
    	this.lblSeparador.setValue("");
    	this.lblSeparador.addStyleName(ValoTheme.LABEL_TINY);

    	this.topLayoutL.addComponent(this.opcNuevo);
    	this.topLayoutL.addComponent(this.opcEliminar);
    	if (isConFormulario())
    	{
    		this.topLayoutL.addComponent(this.opcBuscar);
    	}
    	this.topLayoutL.addComponent(this.opcRefresh);
    	this.topLayoutL.addComponent(this.opcImprimir);
    	this.topLayoutL.addComponent(this.opcSalir);
//    	this.topLayoutR.addComponent(this.lblTitulo);
//    	this.topLayoutR.setComponentAlignment(this.lblTitulo, Alignment.MIDDLE_RIGHT);
    	
    	this.topLayout.addComponent(this.topLayoutL);
    	this.topLayout.addComponent(this.topLayoutC);
    	this.topLayout.addComponent(this.topLayoutR);
    	this.topLayout.setComponentAlignment(this.topLayoutL, Alignment.MIDDLE_LEFT);
    	this.topLayout.setComponentAlignment(this.topLayoutC, Alignment.MIDDLE_CENTER);
    	this.topLayout.addStyleName("top-bar");
//    	this.topLayout.addStyleName("v-panelTitulo");
//    	this.cabLayout.addStyleName("top-bar");
//    	this.cabLayout.addStyleName("v-panelTitulo");
    	this.barAndGridLayout.addComponent(this.topLayout);
    	this.barAndGridLayout.addComponent(this.lblSeparador);
    	this.barAndGridLayout.addComponent(this.cabLayout);
    	pp = new Panel();
    	pp.setSizeFull();    	
    	pp.setContent(this.barAndGridLayout);
    	
        
    	pp.addActionHandler(new Handler() {

            Action action_0 = new ShortcutAction("ctrl t", ShortcutAction.KeyCode.T, new int[] { ShortcutAction.ModifierKey.CTRL, ShortcutAction.ModifierKey.ALT });
                     
            @Override
            public void handleAction(Action action, Object sender, Object target) {

              if (action == action_0) {
                // handle action
//            	  Notificaciones.getInstance().mensajeError("HOLA");
            	  PantallaMenuCtrlT menu = new PantallaMenuCtrlT();
            	  getUI().addWindow(menu);
              }

            }
                     
            @Override
            public Action[] getActions(Object target, Object sender) {
              return new Action[] {action_0};
            }
          });

    	this.addComponent(pp);
        this.cargarPantalla();
    }
    
    
    public boolean isActivarSync() {
		return activarSync;
	}

	public void setActivarSync(boolean r_activarSync, int r_intervaloRefresco) {
		this.activarSync = r_activarSync;
		if (r_activarSync) 
		{
			this.ejecutarTimer(r_intervaloRefresco);
		}
		this.opcRefresh.setVisible(r_activarSync);
	}
	
	
	public boolean isConFormulario() {
		return conFormulario;
	}

	public void setConFormulario(boolean r_conFormulario) 
	{
		this.conFormulario = r_conFormulario;
	}
	
	public boolean isPantallaPeticion() {
		return pantallaPeticion;
	}

	public void setPantallaPeticion(boolean r_pantallaPeticion) {
		this.pantallaPeticion = r_pantallaPeticion;
		if (r_pantallaPeticion)
		{
			this.opcSalir.setVisible(false);
			this.opcBuscar.setVisible(false);
		}
	}

	public boolean isSoloConsulta() 
	{
		return soloConsulta;
	}

	public void setSoloConsulta(boolean r_soloConsulta) 
	{
		this.soloConsulta = r_soloConsulta;		
		this.opcNuevo.setVisible(!r_soloConsulta);
	}
		
    public boolean isHayGrid() {
		return hayGrid;
	}

	public void setHayGrid(boolean r_hayGrid) {
		this.hayGrid = r_hayGrid;

		if (r_hayGrid)
		{
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);

			grid.addSelectionListener(new SelectionListener() {
				
				@Override
				public void select(SelectionEvent event) {
						
					if (grid.getSelectionModel() instanceof SingleSelectionModel)
						filaSeleccionada(grid.getSelectedRow());
//					else
//						mostrarFilas(grid.getSelectedRows());
				}
			});
		}
		else
		{
		}
	}
	
    public void actualizarGrid()
    {
    	if (isHayGrid())
    	{
    		this.grid.removeAllColumns();			
    		this.barAndGridLayout.removeComponent(this.grid);
    		this.grid=null;
    	}
    	this.generarGrid(this.opcionesEscogidas);
    }
    
    private void ejecutarTimer(int r_intervaloRefresco)
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(r_intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }

	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco" + new Date());
    		actualizarGrid();
    	}
    }

	public void botonesGenerales(boolean r_activo)
	{
		this.opcBuscar.setEnabled(r_activo);
		this.opcSalir.setEnabled(r_activo);
		this.opcNuevo.setEnabled(r_activo);
		this.opcImprimir.setEnabled(r_activo);
	}
	
	public void regresarDesdeForm()
	{
		this.botonesGenerales(true);
		this.reestablecerPantalla();
		if (this.hayGrid)
		{
			this.setHayGrid(true);
			this.grid.setEnabled(true);
		}
	}

	public abstract void reestablecerPantalla();
	public abstract void generarGrid(HashMap<String , String> opcionesEscogidas);
	public abstract void cargarPantalla();
	public abstract void newForm();
	public abstract void eliminarRegistro();
	public abstract void print();
	public abstract void verForm(boolean r_busqueda);
	public abstract void destructor();
	public abstract void filaSeleccionada(Object r_fila);
	public abstract void mostrarFilas(Collection<Object> r_filas);
	public abstract void aceptarProceso(String r_accion);
	public abstract void cancelarProceso(String r_accion);
}
