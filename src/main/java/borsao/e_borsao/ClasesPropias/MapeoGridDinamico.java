package borsao.e_borsao.ClasesPropias;

import java.util.HashMap;

public class MapeoGridDinamico
{
	private HashMap<String, String> hashColumnasFijasValores = null;
	private HashMap<String, Integer> hashValores = null;

	/**
	 * MAPEO QUE CONTIENE LOS DATOS A PRESENTAR EN EL GRID IDNAMICO
	 * TANTO EN SU PARTE FIJA COMO DINAMICA
	 */
	public MapeoGridDinamico()
	{
	}

	public HashMap<String, String> getHashColumnasFijasValores() {
		return hashColumnasFijasValores;
	}

	public void setHashColumnasFijasValores(HashMap<String, String> hashColumnasFijasValores) {
		this.hashColumnasFijasValores = hashColumnasFijasValores;
	}

	public HashMap<String, Integer> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Integer> hashValores) {
		this.hashValores = hashValores;
	}



}