package borsao.e_borsao.ClasesPropias;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.vaadin.server.VaadinService;

public class LecturaProperties
{
	/*
	 * variables para establecer el entorno en el que queremos trabajar
	 */
	public static boolean pruebas=false;
	public static String cron = null;
//	static boolean local=true;

	/*
	 * variables de configuracion
	 */
	static String entorno = null;
	static String user = null;
	static String url = null;
	static String pass = null;
	static String userG = null;
	static String urlG = null;
	static String passG = null;
	static String userBiblia = null;
	static String urlBiblia = null;
	static String passBiblia = null;
	static String protocolo = null;

	static String userGC = null;
	static String urlGC = null;
	static String passGC = null;
	static String dbPathC = null;
	static String dbLibC= null;
	static String driverC = null;

	
	static String urlSSH = null;
	static String userSSH = null;
	static String passSSH = null;
	static String portSSH = null;
	static String driverT = null;
	static String hostDBT = null;
	static String portDBT = null;
	static String userDBT = null;
	static String passDBT = null;
	static String nameDBT = null;

	static String hostSQL = null;
	static String portSQL = null;
	static String urlSQL = null;
	static String BBDDSQL = null;
	static String BBDDSQLsync = null;
	static String BBDDSQLtest = null;
	static String userSQL = null;
	static String passSQL = null;
	static String driverSQL = null;

	static String dbPath = null;
	static String dbLib = null;
	static String driver = null;
	static String smtp = null;
	static String puerto = null;
	static String autentificacion = null;
	static String usuario = null;
	static String password = null;
	static String cuenta = null;
	static String destino = null;
	static String ssl = null;
	
	public static boolean avisos = false;
	public static boolean avisosCalendario = false;
	public static boolean semaforos = false;
	public static boolean automatizarProduccionSGA = false;
	public static String basePdfPath=null;
	public static String baseReportsPath=null;
	public static String baseImagePath=null;
	public static String baseImageSignature=null;
	public static String rutaImagenesIntranet=null;
	public static String rutaInventario=null;
	public static String rutaImagenesCalidad=null;
	public static String rutaLecturaCV=null;
	public static String rutaSilicie=null;
	public static String extensionArchivosImagenesIntranet=null;
	public static Integer logs = null;
	
	public LecturaProperties() 
	{
	}
	
	public static boolean cargarProperties() throws IOException
	{
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	    InputStream stream = null;
	    Properties p = null;
	    String fichero="entorno.properties";
	    String valor = null;
	    stream = classLoader.getResourceAsStream(fichero);

	    if (stream != null) 
	    {
	    	System.out.println("Cargado fichero propiedades:" + fichero);
	    	p = new Properties();
	    	
	    	p.load(stream);
	    	entorno = p.getProperty("entorno");
	    	
	    	if (entorno.equals("pruebas"))
	    	{
	    		pruebas=true;
	    		fichero="local.properties";
	    	}
	    	else if (entorno.equals("casa"))
	    	{
	    		pruebas=true;
	    		fichero="casa.properties";
	    	}
	    	else 
	    		if (entorno.equals("produccion"))
	    	{
	    		pruebas=false;
	    		fichero="e-borsao.properties";
	    	}
    		else if (entorno.equals("pre-produccion"))
	    	{
	    		pruebas=false;
	    		fichero="e-borsao.properties";
	    	}
	    	else
	    	{
	    		System.out.println("Valor de entrono erroneo. " + entorno);
		    	return false;
	    	}

	    	stream = classLoader.getResourceAsStream(fichero);
		    
		    if (stream == null) 
		    {
	    		System.out.println("Fichero de propiedades segun entorno no encontrado. " + fichero);
		    	return false;
		    }
		    else
		    {
		    	System.out.println("Cargado fichero propiedades:" + fichero);
		    	p = new Properties();
		    	p.load(stream);
		    	
		    	cron = p.getProperty("activarCron");
		    	
		    	user = p.getProperty("userbbdd");
		    	url = p.getProperty("urlbbdd");
//		    	pass = p.getProperty("passwordbbdd");
			      
		    	userG = p.getProperty("userG");
		    	urlG = p.getProperty("urlG");
//		    	passG = p.getProperty("passwordG");
		    	dbPath = p.getProperty("dbPath");
		    	dbLib = p.getProperty("dbLib");
		    	driver = p.getProperty("driver");
	
		    	userGC = p.getProperty("userGC");
		    	urlGC = p.getProperty("urlGC");
//		    	passGC = p.getProperty("passwordGC");
		    	dbPathC = p.getProperty("dbPathC");
		    	dbLibC = p.getProperty("dbLibC");
		    	driverC = p.getProperty("driverC");
	
		    	userBiblia = p.getProperty("userBiblia");
		    	urlBiblia = p.getProperty("urlBiblia");
//		    	passBiblia = p.getProperty("passwordBiblia");
	
		    	urlSSH = p.getProperty("urlSSH");
		    	userSSH = p.getProperty("userSSH");
		    	passSSH = p.getProperty("passSSH");
		    	portSSH = p.getProperty("portSSH");
		    	driverT = p.getProperty("driverDBT");
		    	userDBT = p.getProperty("userDBT");
		    	passDBT = p.getProperty("passDBT");
		    	hostDBT = p.getProperty("hostDBT");
		    	portDBT = p.getProperty("portDBT");
		    	nameDBT = p.getProperty("nameDBT");

		    	urlSQL = p.getProperty("urlSQL");
		    	BBDDSQL = p.getProperty("BBDDSQL");
		    	BBDDSQLsync = p.getProperty("BBDDSQLsync");
		    	BBDDSQLtest = p.getProperty("BBDDSQLtest");
		    	
		    	
		    	userSQL = p.getProperty("userSQL");
		    	passSQL = p.getProperty("passSQL");
		    	hostSQL = p.getProperty("hostSQL");
		    	portSQL = p.getProperty("portSQL");
		    	driverSQL = p.getProperty("driverSQL");
		    	
		    	String autProd = p.getProperty("autProd");
		    	
		    	if (autProd.contentEquals("true")) automatizarProduccionSGA=true;
		    	
		    	smtp=p.getProperty("smtp");
		    	puerto=p.getProperty("puerto");
		    	protocolo=p.getProperty("protocolo");
		    	autentificacion=p.getProperty("autentificacion");
		    	usuario=p.getProperty("usuario");
		    	ssl=p.getProperty("ssl");
//		    	password=p.getProperty("password");
		    	password="Javier-2017!!";
		    	cuenta=p.getProperty("cuenta");
		    	destino=p.getProperty("destino");
		    	
		    	
		    	valor = p.getProperty("logs").toString().trim();
		    			
		    	if (valor!=null && valor.equals("T")) //Todos
		    	{
		    		logs=0;
		    	}
		    	else if (valor.equals("W"))
		    	{
		    		logs= 1;
		    	}
		    	else if (valor.equals("D"))
	    		{
		    		logs = 2;
	    		}
		    	else if (valor.equals("E"))
	    		{
		    		logs = 3;
	    		}		    	
		    	if (p.getProperty("avisos").equals("S"))
		    	{
		    		avisos=true;
		    	}
		    	if (p.getProperty("semaforos").equals("S"))
		    	{
		    		semaforos=true;
		    	}
		    	if (p.getProperty("avisosCalendario").equals("S"))
		    	{
		    		avisosCalendario=true;
		    	}
		    	
		    	basePdfPath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/" + p.getProperty("basePdfPath") + "/"  ;
		    	baseReportsPath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/" + p.getProperty("baseReportsPath") + "/";
		    	baseImagePath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/" + p.getProperty("baseImagePath")+ "/";
		    	baseImageSignature = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/" + p.getProperty("baseImageSignature")+ "/";
		    	rutaImagenesIntranet=p.getProperty("rutaImagenesIntranet");
		    	rutaInventario=p.getProperty("rutaInventario");
		    	rutaImagenesCalidad=p.getProperty("rutaImagenesCalidad");
		    	rutaLecturaCV=p.getProperty("rutaLecturaCV");
		    	rutaSilicie=p.getProperty("rutaSilicie");
		    	extensionArchivosImagenesIntranet=p.getProperty("extensionArchivosImagenesIntranet");
		    }
		}
	    else
	    {
	    	System.out.println("No hemos localizado fichero entorno." + fichero);
	    	return false;
	    }
	    
	    System.out.println("entorno: " + entorno);
	    System.out.println("modo pruebas: " + pruebas);
	    System.out.println("usuario BBDD: " + user);
	    System.out.println("pass BBDD: " + pass);
	    System.out.println("url BBDD: " + url);
	    System.out.println("avisos activos: " + avisos);
	    System.out.println("semaforos activos: " + semaforos);
	    System.out.println("avisos calendario activos: " + avisosCalendario);
	    System.out.println("sistema log activo: " + valor);
	    
	    return true;
	}
	
	public static boolean logActivo() throws IOException
	{
		return true;
		
//		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
//	    InputStream stream = null;
//	    Properties p = null;
//	    String fichero="entorno.properties";
//	    String valor =null;
//	    
//	    stream = classLoader.getResourceAsStream(fichero);
//
//	    if (stream != null) 
//	    {
////	    	System.out.println("Cargado fichero propiedades:" + fichero);
//	    	p = new Properties();
//	    	p.load(stream);
//	    	entorno = p.getProperty("entorno");
//	    	
//	    	if (entorno.equals("pruebas"))
//	    	{
//	    		pruebas=true;
//	    		fichero="local.properties";
//	    	}
//	    	else if (entorno.equals("produccion"))
//	    	{
//	    		pruebas=false;
//	    		fichero="e-borsao.properties";
//	    	}
//    		else if (entorno.equals("pre-produccion"))
//	    	{
//	    		pruebas=false;
//	    		fichero="e-borsao.properties";
//	    	}
//	    	else
//	    	{
//	    		System.out.println("Valor de entrono erroneo. " + entorno);
//		    	return false;
//	    	}
//
//	    	stream = classLoader.getResourceAsStream(fichero);
//		    
//	    	p = new Properties();
//	    	p.load(stream);
//		    	
//	    	valor = p.getProperty("logs").toString().trim();
//			
//	    	if (valor!=null && valor.equals("T")) //Todos
//	    	{
//	    		logs=0;
//	    	}
//	    	else if (valor!=null && valor.equals("W"))
//	    	{
//	    		logs= 1;
//	    	}
//	    	else if (valor!=null &&  valor.equals("D"))
//    		{
//	    		logs = 2;
//    		}
//	    	else if (valor!=null && valor.equals("E"))
//    		{
//	    		logs = 3;
//    		}	
//		}
//	    else
//	    {
//	    	System.out.println("Buscar logs No hemos localizado fichero entorno." + fichero);
//	    	return false;
//	    }
	    
//	    if (pruebas)
//	    {
//    		user = "root";
//    		if (local)
//    		{
//    			url = "jdbc:mysql://127.0.0.1:3306/laboratorio?characterEncoding=Cp437";	    	
//    			pass = "Javier-2017!!";
//    		}
//    		else
//    		{
//    			url = "jdbc:mysql://192.168.6.6:3306/laboratorio?characterEncoding=Cp437";	    	
//    			pass = "compaq123";    			
//    		}
//    		destino="j.claveria@bodegasborsao.com";
//	    }
//	    return true;
	}
}

