package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;


public abstract class GridPropio extends Grid
{
	private boolean conFiltro = false;
	private boolean conTotales = false;
	private boolean editable = false;
	
	public ArrayList<String> camposNoFiltrar = new ArrayList<String>();
	public HashMap<String,String> widthFiltros = new HashMap<String, String>();
	public HashMap<String,String> filtros = new HashMap<String, String>();
	
	public BeanItemContainer<?> container = null; 
	public String valorSeleccionado=null;
	public HashMap<String , String> opcionesEscogidas = null;
	public ArrayList<?> vector = null;
	
	public FooterRow footer = null;
	
	public GridPropio()
	{
		if (this.isConTotales()) this.footer = this.appendFooterRow();
		setSizeFull();
		setCaptionAsHtml(true);	
		setResponsive(true);
	}

	private void activarFiltro(boolean r_filtro)
	{
		if (r_filtro)
		{
			HeaderRow filterRow = this.appendHeaderRow();
			
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				HeaderCell cell = filterRow.getCell(pid);
				
				// Have an input field to use for filter
				TextField filterField = new TextField();
				filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
				filterField.setInputPrompt("Filtro");				
				
				// Update filter When the filter input is changed
				filterField.addTextChangeListener(new TextChangeListener() {
					
					@Override
					public void textChange(TextChangeEvent event) {
						// Can't modify filters so need to replace
						getContainer().removeContainerFilters(pid);
						// (Re)create the filter if necessary
						if (!event.getText().isEmpty()) getContainer().addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
						filtros.put(pid.toString(), event.getText());
						calcularTotal();
				}});
				
				if (isFiltrable(pid.toString())) 
				{
					filterField.setWidth(obtenerAncho(pid.toString()));
					cell.setComponent(filterField);
				}
				
			}
			
		}
	}

	public void establecerFiltros(HashMap<String,String> r_filtros)
	{
		if (r_filtros!=null && this.getHeaderRowCount()>1) 
		{
			HeaderRow filterRow = this.getHeaderRow(1);
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				if (r_filtros.get(pid.toString())!=null)
				{
					HeaderCell cell = filterRow.getCell(pid);
					
					getContainer().removeContainerFilters(pid);
					// (Re)create the filter if necessary
					getContainer().addContainerFilter(new SimpleStringFilter(pid,r_filtros.get(pid.toString()), true, false));
					((TextField) cell.getComponent()).setValue(r_filtros.get(pid.toString()));
					
				}
			}
		}		
	}
	
	public HashMap<String,String> recogerFiltros()
	{
		HashMap<String,String> filtros = new HashMap<String,String>();
		
		if (this.getHeaderRowCount()>1)
		{
			HeaderRow filterRow = this.getHeaderRow(1);
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				if (isFiltrable(pid.toString()))
				{
					HeaderCell cell = filterRow.getCell(pid);
					if (cell.getComponent() instanceof TextField)
					{
						if (((TextField) cell.getComponent()).getValue()!=null && ((TextField) cell.getComponent()).getValue().length()>0) filtros.put(pid.toString(), ((TextField) cell.getComponent()).getValue());
					}
				}
				// (Re)create the filter if necessary
						
			}
		}
		return filtros;
	}
	
	private boolean isFiltrable(String r_columna)
	{
		if (camposNoFiltrar==null) return true;
		
		for (int i=0;i<camposNoFiltrar.size();i++)
		{
			if (camposNoFiltrar.get(i)!=null && camposNoFiltrar.get(i).equals(r_columna)) return false;
		}
		return true;
	}
	
	private String obtenerAncho(String r_columna)
	{
		if (!widthFiltros.isEmpty())
		{
			String rdo = widthFiltros.get(r_columna);
			if (rdo!=null && rdo.length()>0) return rdo;
		}
		return "0";
	}
	
	public void crearGrid(Class r_class)
	{
		BeanItemContainer<MapeoGlobal> container = new BeanItemContainer<MapeoGlobal>(r_class);
        setContainerDataSource(container);
        
        this.establecerTitulosColumnas();
		this.establecerOrdenPresentacionColumnas();
		this.establecerColumnasNoFiltro();
		this.asignarEstilos();
		this.cargarListeners();
	}
	
    public BeanItemContainer<MapeoGlobal> getContainer() {
		return (BeanItemContainer<MapeoGlobal>) super.getContainerDataSource();		
    }

    public void setRecords(ArrayList<?> r_mapeo) {
    	((BeanItemContainer<MapeoGlobal>) getContainer()).removeAllItems();
        ((BeanItemContainer<MapeoGlobal>) getContainer()).addAll( (Collection<MapeoGlobal>) r_mapeo);
    }

	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	

	public boolean isConFiltro() {
		return conFiltro;
	}

	public void setConFiltro(boolean r_conFiltro) 
	{
		this.conFiltro= r_conFiltro;
		this.activarFiltro(r_conFiltro);
	}
	
	public boolean isConTotales() {
		return conTotales;
	}

	public void setConTotales(boolean conTotales) {
		this.conTotales = conTotales;
	}

	public void setSeleccion(SelectionMode r_tipo)
	{
		setSelectionMode(r_tipo);
	}
	
    public void remove(MapeoGlobal r_mapeo) {
        getContainer().removeItem(r_mapeo);
    }

    public MapeoGlobal getSelectedRow() throws IllegalStateException {
        return (MapeoGlobal) super.getSelectedRow();
    }

    public void refresh(MapeoGlobal r_mapeo, MapeoGlobal r_mapeo_orig) 
    {    	
    	if (r_mapeo_orig!=null)
    	{
	        int index = this.getContainer().indexOfId(r_mapeo_orig);
	        if (index != -1) {    
		        this.getContainer().removeItem(r_mapeo_orig);
		        this.getContainer().addItemAt(index, r_mapeo);
		        if (!(this.getSelectionModel() instanceof Grid.NoSelectionModel) ) this.select(r_mapeo);
		        this.recalculateColumnWidths();		        
	        }
    	}
        else
        {
        	this.getContainer().addItem(r_mapeo);        	
        }
    	this.asignarEstilos();
    }

    public void asignarTitulo(String r_titulo)
    {
    	setCaption("<h3><b><center>" + r_titulo + "</center></b></h3>");
    }
    
    public void ejecutarAccion(String  r_accion, String r_ruta)
    {
    	
    }
    
    public abstract void calcularTotal();
	public abstract void establecerTitulosColumnas();
	public abstract void establecerOrdenPresentacionColumnas();
	public abstract void establecerColumnasNoFiltro();
	public abstract void asignarEstilos();
	public abstract void cargarListeners();
}
