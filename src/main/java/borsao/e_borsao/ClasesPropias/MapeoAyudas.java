package borsao.e_borsao.ClasesPropias;

public class MapeoAyudas
{
	public String codigo = null;
	public String descripcion = null;
	
	
	public MapeoAyudas()
	{
		
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}