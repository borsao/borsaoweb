package borsao.e_borsao.ClasesPropias;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

public class fileReceiver implements Receiver, SucceededListener  
{
	private static final long serialVersionUID = -1276759102490466761L;
	
	public File file;
	public String ruta = null;
            
	public fileReceiver(String r_ruta)
	{
		this.setRuta(r_ruta);
	}
	
	public OutputStream receiveUpload(String filename, String mimeType) 
	{
		// Create upload stream
		FileOutputStream fos = null; // Stream to write to
		try 
		{
			// Open the file for writing.
			file = new File(this.getRuta() + filename);
			fos = new FileOutputStream(file);
		} 
		catch (final java.io.FileNotFoundException e) 
		{
			Notificaciones.getInstance().mensajeError(e.getMessage());
			return null;
		}
		return fos; // Return the output stream to write to
	}

	public void uploadSucceeded(SucceededEvent event) 
	{
		// Show the uploaded file in the image viewer
		
//		if (this.viewOrigen instanceof MovimientosSilicieView) ((MovimientosSilicieView) this.viewOrigen).validar(getFilePath());
		Notificaciones.getInstance().mensajeInformativo("Carga Terminada");
	}
	
	public String getFilePath()
	{
		if ( file!=null && file.getAbsolutePath()!=null)
			return file.getAbsolutePath();
		else return null;
	}
	
	public File getFile()
	{
		if ( file!=null)
			return file;
		else return null;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
}