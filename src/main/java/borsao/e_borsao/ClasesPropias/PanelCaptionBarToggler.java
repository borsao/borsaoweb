package borsao.e_borsao.ClasesPropias;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;
import com.vaadin.ui.Panel;

public class PanelCaptionBarToggler<T extends Panel> {
    private T panel;
    private Resource cacheIcon;
    private float COLLAPSED_WIDTH_PIXELS = 400f;
    private static final int CAPTION_BAR_HEIGHT = 30; 
    
    public PanelCaptionBarToggler(T panel) {
        this.panel = panel;
        panel.addClickListener((clickEvent->{
            if(clickEvent.getRelativeY()<CAPTION_BAR_HEIGHT) {
                toggleContent();
            }
        }));
    }

  
    private void toggleContent() {
      panel.getContent().setVisible(!panel.getContent().isVisible());
        if (!panel.getContent().isVisible()) {
            cacheIcon = panel.getIcon();
            panel.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
//            panel.setWidth(COLLAPSED_WIDTH_PIXELS, Unit.PIXELS);
        } else {
            panel.setIcon(cacheIcon);
//            panel.setWidth("100%");
        }
    }
 
}