package borsao.e_borsao.ClasesPropias;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.io.FileUtils;

import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Component;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.modelo.MapeoEtiquetasUbicacion;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo.MapeoInventarioSGAGsys;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.modelo.MapeoEtiquetasRecepcion;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.modelo.MapeoMovimientosDeclaradoSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.server.consultaDeclaracionSilicieServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoInventarioSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoEntradasCosecha;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoFincasVendimia;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server.consultaFincasVendimiaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class RutinasFicheros
{
	private static String COMMA_DELIMITER=";";
	private static String PIPE_DELIMITER="|";
	
	public RutinasFicheros()
	{
		
	}
	
	
	public static boolean copiarFicheroATemporal(String r_file)
	{
		File srcFile = new File(r_file);
		File destDir = new File("c:/tmp");

		try {
			FileUtils.copyFileToDirectory(srcFile, destDir);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	/**
	 * Comprobar existe fichero
	 * @param r_archivo Recibe en un string la ruta completa del archivo
	 * @return Devuelve Verdadero si existe el fichero o falso en caso contrario
	 */
	public static boolean comprobarExisteFichero( String r_archivo)
    {
    	File file = new File ( r_archivo );
    	
    	if (file.exists())
    	{
    		file=null;
    		return true;
    	}
		else 
		{
			file=null;
			return false;
		}
    	
    }

	/**
	 * Comprobar existe fichero.
	 * Recibe tres parámetros obligatorios que son:
	 * @param r_ruta Recibe en un string la ruta donde se ubica el fichero
	 * @param r_nombre Recibe en un string el nombre del archivo sin la extensión
	 * @param r_ext Recibe en un string la extension del archivo
	 * @return Devuelve Verdadero si existe el fichero o falso en caso contrario
	 */
	public static boolean comprobarExisteFichero( String r_ruta, String r_nombre, String r_ext)
	{
		File file = null;
		if (r_ruta!=null && r_ruta.length()>0 && r_nombre!=null && r_nombre.length()>0 && r_ext!=null && r_ext.length()>0)
		{
			file =new File(r_ruta + "/" + r_nombre + "." + r_ext);
			if (file.exists())
			{
				file=null;
				return true;
			}
			else 
			{
				file=null;
				return false;
			}
		}
		else
		{
			file=null;
			return false;
		}
	}

	/**
	 * Este metodo sirve para encontrar ficheros en una ruta determinada.
	 * Recibe tres parámetros obligatorios que son:
	 * @param r_ruta Recibe en un string la ruta donde se ubica el fichero
	 * @param r_nombre StartsWith Recibe en un string el nombre o parte del nombre del archivo. 
	 * @param r_ext Recibe en un string la extension del archivo
	 * @return String[] Todos los nombres de los archivos coincidentes con los parametros recibidos
	 */
	public static String[] recuperarNombresArchivosCarpeta(String r_ruta, String r_nombre, String r_ext)
    {
		File folder = new File(r_ruta);
		FilenameFilter filtro = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {					
				if (r_nombre!=null && name.contains(r_nombre) && r_ext!=null && name.endsWith(r_ext)) return true; 
				else
					if (r_nombre==null && r_ext!=null && name.endsWith(r_ext)) return true; 
					else 
						if (r_ext==null && r_nombre!=null && name.startsWith(r_nombre)) return true; 
						else 
							if (r_ext==null && r_nombre==null) return true;
							else
								return false;
			}
		};
		String [] archivos = folder.list(filtro);
		return archivos;
    }

	public static String[] recuperarNombresCarpeta(String r_ruta)
	{
		File file = new File(r_ruta);
		String[] directories = file.list(new FilenameFilter() {
		  @Override
		  public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		  }
		});
		return directories;
	}

	/**
	 * Este metodo sirve para ver la fecha de ultima modificacion de un fichero
	 * Recibe dos parámetros obligatorios que son:
	 * @param r_ruta Recibe en un string la ruta donde se ubica el fichero
	 * @param r_nombre Recibe en un string el nombre del archivo, extension incluida. 
	 * @return String Con la fehca y hora de ultima modificacion
	 */
	public static String recuperarFechaUltimaModificacionFichero(String r_ruta, String r_archivo)
	{
		long fechaModificacion = 0;
		String fechaUltimaModificaion = null;
		
		if (RutinasFicheros.comprobarExisteFichero(r_ruta + "/" + r_archivo))
		{
			File fl =new File(r_ruta + "/" + r_archivo);
			fechaModificacion = fl.lastModified();
			
			Date d = new Date(fechaModificacion);
			fechaUltimaModificaion = RutinasFechas.convertirDateTimeToString(d);
		}
		return fechaUltimaModificaion;
	}

	/**
	 * Este metodo sirve para ver la anchura de la imagen
	 * Recibe dos parámetros obligatorios que son:
	 * @param r_ruta Recibe en un string la ruta donde se ubica el fichero
	 * @param r_nombre Recibe en un string el nombre del archivo, extension incluida. 
	 * @return float Con la anchura de la imagen
	 */
	public static float recuperarAnchuraFichero(String r_ruta, String r_archivo)
	{
		float anchura = 0;
		
		if (RutinasFicheros.comprobarExisteFichero(r_ruta + "/" + r_archivo))
		{
			File fl =new File(r_ruta + "/" + r_archivo);
			FileResource res = new FileResource(fl);
			try
			{
				if (res instanceof FileResource)
				{
					ImageInfo imageInfo;
					try
					{
						imageInfo = Imaging.getImageInfo(((FileResource) res).getSourceFile());
						anchura = imageInfo.getWidth();
					}
					catch (ImageReadException e)
					{
						e.printStackTrace();
					}
				}
			}
			catch (IOException e)
		    {
		      e.printStackTrace();
		    }
		        
		}
		return anchura;
	}

	/**
	 * Este metodo sirve para ver la altura de la imagen
	 * Recibe dos parámetros obligatorios que son:
	 * @param r_ruta Recibe en un string la ruta donde se ubica el fichero
	 * @param r_nombre Recibe en un string el nombre del archivo, extension incluida. 
	 * @return float Con la altura de la imagen
	 */
	public static float recuperarAlturaFichero(String r_ruta, String r_archivo)
	{
		float altura = 0;
		
		if (RutinasFicheros.comprobarExisteFichero(r_ruta + "/" + r_archivo))
		{
			File fl =new File(r_ruta + "/" + r_archivo);
			FileResource res = new FileResource(fl);
			try
			{
				if (res instanceof FileResource)
				{
					ImageInfo imageInfo;
					try
					{
						imageInfo = Imaging.getImageInfo(((FileResource) res).getSourceFile());
						altura = imageInfo.getHeight();
					}
					catch (ImageReadException e)
					{
						e.printStackTrace();
					}
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			
		}
		return altura;
	}

//    public static void findAllFilesInFolder(File folder) {
//    	for (File file : folder.listFiles()) {
//    		if (!file.isDirectory()) {
//    			System.out.println(file.getName());
//    			
//    		} else {
//    			findAllFilesInFolder(file);
//    		}
//    	}
//    }

	public static int imprimir(String r_archivo, String r_impresora)
	{
	
		int processOutput = 0;
		
		String cmd = "lp -d " + r_impresora + " " + r_archivo ;
		try
		{
			Runtime runtime = Runtime.getRuntime();
		    Process croppingProcess = runtime.exec(cmd);
	
		    BufferedReader input = new BufferedReader(new InputStreamReader(croppingProcess.getErrorStream()));
	
		    String line = null;
	
		    while ((line = input.readLine()) != null)
		    {
		    	Notificaciones.getInstance().mensajeSeguimiento(line);
		    }
	
		    processOutput = croppingProcess.waitFor();
		    if(processOutput == 0)
		    {
		    	Notificaciones.getInstance().mensajeSeguimiento("paso comn ok");
		    }
		    else
		    {
		    	Notificaciones.getInstance().mensajeSeguimiento("paso comn ko");
		    }	
		    Notificaciones.getInstance().mensajeSeguimiento("Exited with error code " + processOutput);	                
		    Notificaciones.getInstance().mensajeSeguimiento("comnado ejecutado: " + cmd);
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		
	    return processOutput;
	}

	public static int imprimirCuantas(String r_area, String r_archivo, String r_impresora, Integer r_cuantas, MapeoGlobal r_mapeo)
	{
		/*
		* 
		* Metodo que va a imprimir directamente en la impresora Zebra de Almacen.
		* Sólo se ejecuta cuando la impresora seleccionada es la llamada "etiquetas"
		* Una impresora de red con el driver de generica texto
		* 
		* 
		* 1.- Disponemos de unas plantillas con el diseño de la etiqueta para imprimir:
		* 
		* 		ruta:	/usr/share/jetty9/webapps/root/germark
		* 		nombre:	recepcion.prn
		* 		nombre:	ubicacion.prn
		* 
		* 		Estas plantillas se generan con el programa nicelabel y se imprime a fichero.
		* 		Antes de darlas por buenas, se editan desde linux y se eliminan ciertos caracteres
		* 		que provocan errores en la impresora, aunque no impiden imprimir.
		* 		Para que funcione todo correctamente las cadenas de texto a reemplazar tienen 
		* 		que estar con tipo de letra Avery
		* 
		* 2.- Antes de empezar el proceso ejecutamos un script para copiar el fichero plantilla
		*     en uno nuevo con el nombre modificado y sobre el que actuará el resto del proceso. 
		* 
		* 		ruta:	/usr/bin
		* 		nombre:	copiarArchivo
		* 
		* 		parametros:	$1	$2	
		* 	
		* 					$1 Ruta y nombre del archivo plantilla
		* 					$2 Nombre del archivo definitivo. 
		* 					   Se compone del codigo de articulo y la extension .prn
		* 
		* 					contenido del script
		* 
		* 						#! /bin/bash
		*						cp $1 $2
		* 		
		* 3.- Requiere de un script en el s.o. que realiza ciertos cambios en el .prn de plantilla
		*     para luego mandar el resultado modificado a la impresora mediante el comando lp
		* 
		* 		ruta:	/usr/bin
		* 		nombre:	prnReplace
		* 
		* 		parametros:	$1	$2	$3
		* 
		* 					$1	cadena de texto original a buscar
		* 					$2	cadena de texto que reemplaza la original
		* 					$3	nombre del archivo que se genera para enviar a la impresora
		* 
		* 
		* 					contenido del script
		* 
		* 						#! /bin/bash
		*						str=$2
		*						find="_"
		*						replace=" "
		*						nuevo=${str//$find/$replace}
		*						sed -i "s/$1/$nuevo/g" $3
		*
		*					Explicacion del script
		*
		* 					Al pasar cadenas de texto al script tenemos un problema con los espacios en blanco dentro de la cadena de texto,
		*					para evitarlos reemplazamos los espacios en blanco por "_"
		* 
		* 					una vez dentro del script antes de hacer el cambio deseado reemplazamos los "_" por espacios en blanco para dejar
		* 					la cadena de texto igual que la original
		* 
		*  					una vez hecho esto, enviamos la instruccion sed al fichero para poder generar el fichero con los datos definitivos
		*  
		* 4.- En el caso de querer sacar más de una etiqueta, para no enviar n trabajos de impresion metemos todas las etiquetas en un archivo
		*     prn único y este es el que enviamos a imprimir. Para ello tenemos un script que vuelca el contenido de la plantilla en el archivo
		*     creado para mandar a la impresora y sobre ese va modificando los contenidos
		* 
		* 		ruta:	/usr/bin
		* 		nombre:	prnConcatenar
		* 
		* 		parametros:	$1	$2	
		* 	
		* 					$1 Ruta y nombre del archivo plantilla
		* 					$2 Nombre del archivo definitivo. 
		* 					   Se compone del codigo de articulo y la extension .prn
		* 
		* 					contenido del script
		* 
		* 						#! /bin/bash
		*						cat $1 >> $2
		*
		*  5.- Finalizado el proceso de sustitución de informacion ejcutamos el comando
		*  
		*  		lp -d etiquetas archivo.prn
		*/
		
		String archivoPlantilla=null;
		String archivoImprimir=null;
		
		String error = null;
		String programa = null;
		String scriptCopiarArchivos = null;
		String scriptCambioDatos = null;
		String scriptCambioDatos2 = null;
		String scriptCambioDatos3 = null;
		String scriptCambioDatos5 = null;
		String scriptCambioDatos6 = null;
		String scriptCambioDatos7 = null;
		String scriptCambioDatos8 = null;
		String scriptRestauroDatosPlantilla = null;
		String scriptImpresion = null;
		
		int processOutput = 0;
		archivoPlantilla = r_archivo;
		
		try
		{
			switch (r_area.trim().toUpperCase())
			{
				case "RECEPCION":
				{
					MapeoEtiquetasRecepcion mapeo = (MapeoEtiquetasRecepcion) r_mapeo;
					programa = "prnReplace";
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + mapeo.getIdPedido().toString().trim() + ".prn";
					
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					
					scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
					scriptCambioDatos2 = programa + " xxxxxxx " + mapeo.getArticulo().toString().trim() + " " + archivoImprimir ;
					scriptCambioDatos3 = programa + " descrip13 " + RutinasCadenas.reemplazar(mapeo.getDescripcion().toString()," ","_") + " " + archivoImprimir ;
					scriptCambioDatos5 = programa + " 034000000000000000 " + mapeo.getSscc().toString().trim() + " " + archivoImprimir ;
					scriptCambioDatos6 = programa + " DDMMYY " + mapeo.getLote().toString().trim() + " " + archivoImprimir ;
					scriptCambioDatos7 = programa + " nnnnnn " + mapeo.getCajas().toString().trim() + " " + archivoImprimir ;
					scriptCambioDatos8 = programa + " 035000000000000000 " + "03500000000" + mapeo.getArticulo().trim() + " " + archivoImprimir ;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;

					processOutput= ejecutarComando(scriptCopiarArchivos);
					
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos); else error="error en el scriptCopiarArchivo";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos3);else error="error en el scriptCambioDatos2";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos5);else error="error en el scriptCambioDatos3";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos6);else error="error en el scriptCambioDatos5";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos7);else error="error en el scriptCambioDatos6";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos8);else error="error en el scriptCambioDatos7";
					if(processOutput == 0) processOutput= ejecutarComando(scriptImpresion);else error="error en el scriptCambioDatos8";
					if(processOutput == 0)
					{
						if(processOutput == 0) eliminarFichero(archivoImprimir); else Notificaciones.getInstance().mensajeSeguimiento("No se ha podido eliminar el fichero"); 
					}
					else 
					{
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					
					break;
				}
				case "UBICACION":
				{
					MapeoEtiquetasUbicacion mapeo = (MapeoEtiquetasUbicacion) r_mapeo;
					String nombre = RutinasFechas.horaActualSinSeparador();
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + nombre + ".prn";
					programa = "prnReplace";
					
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					
					scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
					String ubic=mapeo.getAlm().toString().trim() + "." + mapeo.getFil().toString().trim() + "." + mapeo.getCol().toString().trim() + "." + mapeo.getAlt().toString().trim() + "." + mapeo.getHue().toString().trim();
					scriptCambioDatos2 = programa + " MP.A.0.A.0 " + ubic + " " + archivoImprimir ;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;
					
					processOutput= ejecutarComando(scriptCopiarArchivos);
					
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos); else error="error en el scriptCopiarArchivo";
					if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
					if(processOutput == 0) processOutput= ejecutarComando(scriptImpresion);else error="error en el scriptCambioDatos2";
					if(processOutput == 0)
					{
						if(processOutput == 0) eliminarFichero(archivoImprimir); else Notificaciones.getInstance().mensajeSeguimiento("No se ha podido eliminar el fichero"); 
					}
					else 
					{
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					
					break;
				}
				default:
				{
					programa = "numCopias";
					scriptCambioDatos = programa + " 1 " + r_cuantas.toString().trim() + " " + r_archivo ;
					scriptRestauroDatosPlantilla = programa + r_cuantas.toString().trim() + " 1" + " " + r_archivo ;
					scriptImpresion = "lp -d " + r_impresora + " " + r_archivo ;
					
					processOutput= ejecutarComando(scriptCambioDatos);
					if(processOutput == 0) processOutput= ejecutarComando(scriptImpresion);else Notificaciones.getInstance().mensajeSeguimiento("error en el scriptCambioDatos");
					if(processOutput == 0)
					{
						processOutput= ejecutarComando(scriptRestauroDatosPlantilla);
					}
					else 
					{
						Notificaciones.getInstance().mensajeSeguimiento("error en el scriptImpresion");
					}
					break;
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		
	    return processOutput;
	}

	public static int imprimirCuantasUbicacion(String r_area, String r_archivo, String r_impresora, Integer r_cuantas, HashMap<Integer, MapeoGlobal> r_hash)
	{
		/*
		 * 
		 * Metodo que va a imprimir directamente en la impresora Zebra de Almacen.
		 * Sólo se ejecuta cuando la impresora seleccionada es la llamada "etiquetas"
		 * Una impresora de red con el driver de generica texto
		 * 
		 * 
		 * 1.- Disponemos de unas plantillas con el diseño de la etiqueta para imprimir:
		 * 
		 * 		ruta:	/usr/share/jetty9/webapps/root/germark
		 * 		nombre:	recepcion.prn
		 * 		nombre:	ubicacion.prn
		 * 
		 * 		Estas plantillas se generan con el programa nicelabel y se imprime a fichero.
		 * 		Antes de darlas por buenas, se editan desde linux y se eliminan ciertos caracteres
		 * 		que provocan errores en la impresora, aunque no impiden imprimir.
		 * 		Para que funcione todo correctamente las cadenas de texto a reemplazar tienen 
		 * 		que estar con tipo de letra Avery
		 * 
		 * 2.- Antes de empezar el proceso ejecutamos un script para copiar el fichero plantilla
		 *     en uno nuevo con el nombre modificado y sobre el que actuará el resto del proceso. 
		 * 
		 * 		ruta:	/usr/bin
		 * 		nombre:	copiarArchivo
		 * 
		 * 		parametros:	$1	$2	
		 * 	
		 * 					$1 Ruta y nombre del archivo plantilla
		 * 					$2 Nombre del archivo definitivo. 
		 * 					   Se compone del codigo de articulo y la extension .prn
		 * 
		 * 					contenido del script
		 * 
		 * 						#! /bin/bash
		 *						cp $1 $2
		 * 		
		 * 3.- Requiere de un script en el s.o. que realiza ciertos cambios en el .prn de plantilla
		 *     para luego mandar el resultado modificado a la impresora mediante el comando lp
		 * 
		 * 		ruta:	/usr/bin
		 * 		nombre:	prnReplace
		 * 
		 * 		parametros:	$1	$2	$3
		 * 
		 * 					$1	cadena de texto original a buscar
		 * 					$2	cadena de texto que reemplaza la original
		 * 					$3	nombre del archivo que se genera para enviar a la impresora
		 * 
		 * 
		 * 					contenido del script
		 * 
		 * 						#! /bin/bash
		 *						str=$2
		 *						find="_"
		 *						replace=" "
		 *						nuevo=${str//$find/$replace}
		 *						sed -i "s/$1/$nuevo/g" $3
		 *
		 *					Explicacion del script
		 *
		 * 					Al pasar cadenas de texto al script tenemos un problema con los espacios en blanco dentro de la cadena de texto,
		 *					para evitarlos reemplazamos los espacios en blanco por "_"
		 * 
		 * 					una vez dentro del script antes de hacer el cambio deseado reemplazamos los "_" por espacios en blanco para dejar
		 * 					la cadena de texto igual que la original
		 * 
		 *  					una vez hecho esto, enviamos la instruccion sed al fichero para poder generar el fichero con los datos definitivos
		 *  
		 * 4.- En el caso de querer sacar más de una etiqueta, para no enviar n trabajos de impresion metemos todas las etiquetas en un archivo
		 *     prn único y este es el que enviamos a imprimir. Para ello tenemos un script que vuelca el contenido de la plantilla en el archivo
		 *     creado para mandar a la impresora y sobre ese va modificando los contenidos
		 * 
		 * 		ruta:	/usr/bin
		 * 		nombre:	prnConcatenar
		 * 
		 * 		parametros:	$1	$2	
		 * 	
		 * 					$1 Ruta y nombre del archivo plantilla
		 * 					$2 Nombre del archivo definitivo. 
		 * 					   Se compone del codigo de articulo y la extension .prn
		 * 
		 * 					contenido del script
		 * 
		 * 						#! /bin/bash
		 *						cat $1 >> $2
		 *
		 *  5.- Finalizado el proceso de sustitución de informacion ejcutamos el comando
		 *  
		 *  		lp -d etiquetas archivo.prn
		 */
		String archivoPlantilla=null;
		String archivoImprimir=null;
		
		String error = null;
		String programa = null;
		String scriptCopiarArchivos = null;
		String scriptConcatenarArchivos = null;
		String scriptCambioDatos = null;
		String scriptCambioDatos2 = null;
		String scriptCambioDatos3 = null;
		String scriptCambioDatos5 = null;
		String scriptCambioDatos6 = null;
		String scriptCambioDatos7 = null;
		String scriptCambioDatos8 = null;
		String scriptCambioDatos9 = null;
		String scriptCambioDatos10 = null;
		String scriptRestauroDatosPlantilla = null;
		String scriptImpresion = null;
		
		int processOutput = 0;
		archivoPlantilla = r_archivo;
		
		try
		{
			switch (r_area.trim().toUpperCase())
			{
				case "RECEPCION":
				{
					HashMap<Integer, MapeoGlobal> hashRecibido = r_hash;
					programa = "prnReplace";
					String nombre = RutinasFechas.horaActualSinSeparador();
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + nombre + ".prn";
					scriptConcatenarArchivos = "prnConcatenar " + archivoPlantilla + " " + archivoImprimir;
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;
//					
					processOutput= ejecutarComando(scriptCopiarArchivos);
					if(processOutput == 0)
					{
						for (int i=0; i< hashRecibido.size();i++)
						{
							if (i>0)
							{
								processOutput= ejecutarComando(scriptConcatenarArchivos);
								if(processOutput != 0)
								{
									error="error en el scriptConcatenarArchivos";
									break;
								}
								
							}
							MapeoEtiquetasRecepcion mapeo = (MapeoEtiquetasRecepcion) hashRecibido.get(i);
							
							scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos2 = programa + " xxxxxxx " + mapeo.getArticulo().toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos3 = programa + " descrip13 " + RutinasCadenas.reemplazar(mapeo.getDescripcion().toString()," ","_") + " " + archivoImprimir ;
							scriptCambioDatos5 = programa + " 034000000000000000 " + mapeo.getSscc().toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos6 = programa + " DDMMYY " + mapeo.getLote().toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos7 = programa + " nnnnnn " + mapeo.getCajas().toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos8 = programa + " 03500000000000 " + mapeo.getEan().trim() + " " + archivoImprimir ;
							scriptCambioDatos9 = programa + " CODIGOBARRAS2 " + mapeo.getCodigot().trim() + " " + archivoImprimir ;
							scriptCambioDatos10 = programa + " CODIGOBARRAS1 " + mapeo.getSscc1t().trim() + " " + archivoImprimir ;
							
							processOutput= ejecutarComando(scriptCambioDatos);
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos3);else error="error en el scriptCambioDatos2";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos5);else error="error en el scriptCambioDatos3";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos6);else error="error en el scriptCambioDatos5";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos7);else error="error en el scriptCambioDatos6";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos8);else error="error en el scriptCambioDatos7";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos9);else error="error en el scriptCambioDatos8";
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos10);else error="error en el scriptCambioDatos9";
							if(processOutput != 0)
							{
								error="error en el scriptCambioDatos10";
								break;
							}
						}
						if(processOutput == 0) 
						{
							processOutput= ejecutarComando(scriptImpresion);
							if(processOutput == 0)
							{
								eliminarFichero(archivoImprimir);  
							}
							else
							{
								error="No se ha podido imprimir el fichero";
								Notificaciones.getInstance().mensajeError(error);
							}
						}
						else 
						{
							Notificaciones.getInstance().mensajeSeguimiento(error);
							Notificaciones.getInstance().mensajeError(error);
						}
						break;							
					}
					else 
					{
						error="error en el scriptCopiarArchivo";
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					break;
//					if(processOutput == 0) processOutput= ejecutarComando(scriptImpresion);else error="error en el scriptCambioDatos8";
//					if(processOutput == 0)
//					{
//						if(processOutput == 0) eliminarFichero(archivoImprimir); else Notificaciones.getInstance().mensajeSeguimiento("No se ha podido eliminar el fichero"); 
//					}
//					else 
//					{
//						Notificaciones.getInstance().mensajeSeguimiento(error);
//						Notificaciones.getInstance().mensajeError(error);
//					}
//					
//					break;
				}
				case "UBICACION":
				{
					HashMap<Integer, MapeoGlobal> hashRecibido = r_hash;
					String nombre = RutinasFechas.horaActualSinSeparador();
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + nombre + ".prn";
					programa = "prnReplace";
					
					scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
					
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					scriptConcatenarArchivos = "prnConcatenar " + archivoPlantilla + " " + archivoImprimir;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;
					processOutput= ejecutarComando(scriptCopiarArchivos);
					if(processOutput == 0)
					{
						for (int i=0; i< hashRecibido.size();i++)
						{
							if (i>0)
							{
								processOutput= ejecutarComando(scriptConcatenarArchivos);
								if(processOutput != 0)
								{
									error="error en el scriptConcatenarArchivos";
									break;
								}
							}
							
							MapeoEtiquetasUbicacion mapeo = (MapeoEtiquetasUbicacion) hashRecibido.get(i);
							String ubic = null;
							
							ubic=mapeo.getAlm().toString().trim();
							if (mapeo.getFil()!=null && mapeo.getFil().length()>0)
								ubic= ubic + "." + mapeo.getFil().toString().trim();
							if (mapeo.getCol()!=null && mapeo.getCol().length()>0)
								ubic= ubic + "." + mapeo.getCol().toString().trim();
							if (mapeo.getAlt()!=null && mapeo.getAlt().length()>0)
								ubic= ubic + "." + mapeo.getAlt().toString().trim();
							if (mapeo.getHue()!=null && mapeo.getHue().length()>0)
								ubic= ubic + "." + mapeo.getHue().toString().trim();
							
							scriptCambioDatos2 = programa + " MP.A.0.A.0 " + ubic + " " + archivoImprimir ;
							
							processOutput= ejecutarComando(scriptCambioDatos);
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
							if(processOutput != 0)
							{
								error="error en el scriptCambioDatos2";
								break;
							}
						}					
						if(processOutput == 0) 
						{
							processOutput= ejecutarComando(scriptImpresion);
							if(processOutput == 0)
							{
								eliminarFichero(archivoImprimir);  
							}
							else
							{
								error="No se ha podido imprimir el fichero";
								Notificaciones.getInstance().mensajeError(error);
							}
						}
						else 
						{
							Notificaciones.getInstance().mensajeSeguimiento(error);
							Notificaciones.getInstance().mensajeError(error);
						}
						break;
					}
					else 
					{
						error="error en el scriptCopiarArchivo";
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					break;
				}
				case "JAULONES":
				{
					HashMap<Integer, MapeoGlobal> hashRecibido = r_hash;
					String nombre = RutinasFechas.horaActualSinSeparador();
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + nombre + ".prn";
					programa = "prnReplace";
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					scriptConcatenarArchivos = "prnConcatenar " + archivoPlantilla + " " + archivoImprimir;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;
					
					processOutput= ejecutarComando(scriptCopiarArchivos);
					if(processOutput == 0)
					{
						for (int i=0; i< hashRecibido.size();i++)
						{
							if (i>0)
							{
								processOutput= ejecutarComando(scriptConcatenarArchivos);
								if(processOutput != 0)
								{
									error="error en el scriptConcatenarArchivos";
									break;
								}
							}
							
							MapeoEtiquetasUbicacion mapeo = (MapeoEtiquetasUbicacion) hashRecibido.get(i);
							
							String ubic=RutinasNumericas.formatearIntegerDigitosIzqda(new Integer(mapeo.getCol()), 4).toString().trim();
							
							scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos2 = programa + " nnnn " + ubic + " " + archivoImprimir ;
							
							processOutput= ejecutarComando(scriptCambioDatos);
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
							if(processOutput != 0)
							{
								error="error en el scriptCambioDatos2";
								break;
							}
						}					
						if(processOutput == 0) 
						{
							processOutput= ejecutarComando(scriptImpresion);
							if(processOutput == 0)
							{
								eliminarFichero(archivoImprimir);  
							}
							else
							{
								error="No se ha podido imprimir el fichero";
								Notificaciones.getInstance().mensajeError(error);
							}
						}
						else 
						{
							Notificaciones.getInstance().mensajeSeguimiento(error);
							Notificaciones.getInstance().mensajeError(error);
						}
						break;
					}
					else 
					{
						error="error en el scriptCopiarArchivo";
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					break;
				}
				case "MANUALES":
				{
					HashMap<Integer, MapeoGlobal> hashRecibido = r_hash;
					String nombre = RutinasFechas.horaActualSinSeparador();
					archivoImprimir = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/" + nombre + ".prn";
					programa = "prnReplace";
					scriptCopiarArchivos = "copiarArchivo " + archivoPlantilla + " " + archivoImprimir;
					scriptConcatenarArchivos = "prnConcatenar " + archivoPlantilla + " " + archivoImprimir;
					scriptImpresion = "lp -d " + r_impresora + " " + archivoImprimir ;
					
//					processOutput= ejecutarComando(scriptCopiarArchivos);
					if(processOutput == 0)
					{
						for (int i=0; i< hashRecibido.size();i++)
						{
							if (i>0)
							{
								processOutput= ejecutarComando(scriptConcatenarArchivos);
								if(processOutput != 0)
								{
									error="error en el scriptConcatenarArchivos";
									break;
								}
							}
							
							MapeoEtiquetasUbicacion mapeo = (MapeoEtiquetasUbicacion) hashRecibido.get(i);
							
							String ubic=RutinasCadenas.reemplazar(mapeo.getFil(), " ", "_");
							
							scriptCambioDatos = programa + " Q1 "+ "Q" + r_cuantas.toString().trim() + " " + archivoImprimir ;
							scriptCambioDatos2 = programa + " mmmm " + ubic + " " + archivoImprimir ;
							
							processOutput= ejecutarComando(scriptCambioDatos);
							if(processOutput == 0) processOutput= ejecutarComando(scriptCambioDatos2); else error="error en el scriptCambioDatos";
							if(processOutput != 0)
							{
								error="error en el scriptCambioDatos2";
								break;
							}
						}					
						if(processOutput == 0) 
						{
							processOutput= ejecutarComando(scriptImpresion);
							if(processOutput == 0)
							{
								eliminarFichero(archivoImprimir);  
							}
							else
							{
								error="No se ha podido imprimir el fichero";
								Notificaciones.getInstance().mensajeError(error);
							}
						}
						else 
						{
							Notificaciones.getInstance().mensajeSeguimiento(error);
							Notificaciones.getInstance().mensajeError(error);
						}
						break;
					}
					else 
					{
						error="error en el scriptCopiarArchivo";
						Notificaciones.getInstance().mensajeSeguimiento(error);
						Notificaciones.getInstance().mensajeError(error);
					}
					break;
				}
				default:
				{
					programa = "numCopias";
					scriptCambioDatos = programa + " 1 " + r_cuantas.toString().trim() + " " + r_archivo ;
					scriptRestauroDatosPlantilla = programa + r_cuantas.toString().trim() + " 1" + " " + r_archivo ;
					scriptImpresion = "lp -d " + r_impresora + " " + r_archivo ;
					
					processOutput= ejecutarComando(scriptCambioDatos);
					if(processOutput == 0) processOutput= ejecutarComando(scriptImpresion);else Notificaciones.getInstance().mensajeSeguimiento("error en el scriptCambioDatos");
					if(processOutput == 0)
					{
						processOutput= ejecutarComando(scriptRestauroDatosPlantilla);
					}
					else 
					{
						Notificaciones.getInstance().mensajeSeguimiento("error en el scriptImpresion");
					}
					break;
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
		
		return processOutput;
	}

	
	public static int ejecutarComando(String r_cmd) throws IOException, InterruptedException
	{
		String line = null;
		Runtime runtime =null;
		Process croppingProcess = null;
		BufferedReader input =null;
		
		int processOutput = 1;
		
		runtime = Runtime.getRuntime();
		
		croppingProcess = runtime.exec(r_cmd);
		System.out.println(r_cmd);
	    input = new BufferedReader(new InputStreamReader(croppingProcess.getErrorStream()));
	    while ((line = input.readLine()) != null)
	    {
	    	Notificaciones.getInstance().mensajeSeguimiento(line);
	    }
	    processOutput = croppingProcess.waitFor();
	    return processOutput;
	}

	public static void ejecutarComandoSinTraza(String r_cmd) throws IOException, InterruptedException
	{
		Runtime.getRuntime().exec(r_cmd);
		Runtime.getRuntime().exit(0);
	}
	
	public static void eliminarFichero( String r_archivo)
    {
    	File file = new File ( r_archivo );
    	
    	if (file.exists()) file.delete();
    	
    	file=null;
    }

    public static void abrirPdfGenerado(Component r_UI, String r_ruta, String r_pdf, boolean r_eliminarArchivo)
    {
    	Window window = null;
    	File fl =new File(r_ruta + "/" + r_pdf);
    	
    	if (fl.exists())
    	{
    		FileResource res = new FileResource(fl);
    		BrowserFrame e = new BrowserFrame();
    		
    		window = new Window();	
    		window.setSizeFull();
    		window.setWindowMode(WindowMode.MAXIMIZED);
    		window.setResizable(true);
    		window.setCaption("VISOR PDF");
    		
    		
    		e.setSizeFull();
    		e.setSource(res);
    		window.setContent(e);
    		
    		window.addCloseListener(new CloseListener() {
				
				@Override
				public void windowClose(CloseEvent e) {
					if (r_eliminarArchivo) eliminarFichero(r_ruta + "/" + r_pdf);
				}
			});
    		r_UI.getUI().addWindow(window);
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("No se ha generado el pdf solicitado");
    	}
    }

    public static void abrirImagenGenerado(Component r_UI, String r_ruta, String r_img)
    {
    	Ventana window = null;
    	
    	if (r_UI!=null)
    	{
	    	window = new pantallaVisualizacionImagenes(r_ruta, r_ruta + "/" + r_img);
	    	r_UI.getUI().addWindow(window);
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("No se ha podido mostrar la imagen");
    	}
    }

    public static void abrirImagenGenerado(Component r_UI, String[] r_archivos, String r_ruta)
    {
    	Ventana window = null;
    	
    	if (r_UI!=null)
    	{
    		window = new pantallaVisualizacionImagenes(r_ruta, r_archivos);
    		r_UI.getUI().addWindow(window);
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("No se ha podido mostrar la imagen");
    	}
    }

    public static ArrayList<MapeoMovimientosDeclaradoSilicie> leerCsvDeclaradoSilicie(String r_separador, String r_ruta, boolean r_anulados, String r_encabezados)
    {
    	ArrayList<MapeoMovimientosDeclaradoSilicie> vector = null;
    	/*
    	 * Leo el CSV de vuelta que me indique el usuario, actualizo el campo numeroAsiento 
    	 * con el que venga en el fihcero enlazando por el numero de referencia interno
    	 */
    	MapeoMovimientosDeclaradoSilicie mapeo = null;
    	vector = new ArrayList<MapeoMovimientosDeclaradoSilicie>();
    	
//    	FileResource res = null;
//    	Iterator iterator = null;
//    	File file = null;
//    	StringBuilder linea = null;
//    	
//    	Integer contador = 0;
//    	Integer i = 0;
//    	Integer filas = 0;
    	
    	/*
    	 * Pedir selector fichero al usuario
    	 */
//    	String ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) + String.valueOf(r_mes) +  ".csv";

    	String line = "";  
    	String splitBy = r_separador;  
    	try   
    	{  
    		//parsing a CSV file into BufferedReader class constructor
    		FileReader fl = new FileReader(r_ruta + ".csv");
    		BufferedReader br = new BufferedReader(fl);  
    		/*
    		 * me salto la línea de encabezados de columnas
    		 */
    		if (r_encabezados.equals("S")) br.readLine();
    		/*
    		 * procesamos lineas leidas
    		 */
    		while ((line = br.readLine()) != null)   //returns a Boolean value  
    		{  
    			String[] datos = line.split(splitBy);    // use comma as separator
    			
    			mapeo = new MapeoMovimientosDeclaradoSilicie();
    			
    			if (datos.length>3)
    			{
    				if (r_anulados)
    				{
    					mapeo.setNumeroAsiento(datos[0]);
    					mapeo.setNumeroAsientoPrevio(datos[1]);
    				}
    				else
    				{
    					mapeo.setNumeroAsiento(datos[0]);
    					mapeo.setTipoAsiento(datos[1]);
    					mapeo.setNumeroRefInterno(datos[2]);
    				}
    			
    				vector.add(mapeo);
    			}
    			else
    				break;
    		}
    		br.close();
    		fl.close();
    		line=null;
    		br=null;
    		fl=null;
    	}   
    	catch (IOException e)   
    	{  
    		e.printStackTrace();  
    	}  
    	return vector;
    }
    
    public static ArrayList<MapeoInventarioSilicie> leerCsvInventarioSilicieAeat(String r_separador, String r_ruta, String r_encabezados)
    {
    	ArrayList<MapeoInventarioSilicie> vector = null;
    	/*
    	 * Leo el CSV de vuelta que me indique el usuario, actualizo el campo numeroAsiento 
    	 * con el que venga en el fihcero enlazando por el numero de referencia interno
    	 */
    	MapeoInventarioSilicie mapeo = null;
    	vector = new ArrayList<MapeoInventarioSilicie>();
    	
//    	FileResource res = null;
//    	Iterator iterator = null;
//    	File file = null;
//    	StringBuilder linea = null;
//    	
//    	Integer contador = 0;
//    	Integer i = 0;
//    	Integer filas = 0;
    	
    	/*
    	 * Pedir selector fichero al usuario
    	 */
//    	String ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) + String.valueOf(r_mes) +  ".csv";
    	String cantidad="";
    	String line = "";  
    	String splitBy = r_separador;  
    	try   
    	{  
    		//parsing a CSV file into BufferedReader class constructor
    		FileReader fl = new FileReader(r_ruta + ".csv");
    		BufferedReader br = new BufferedReader(fl);  
    		/*
    		 * me salto la línea de encabezados de columnas
    		 */
    		if (r_encabezados.equals("S")) br.readLine();
    		/*
    		 * procesamos lineas leidas
    		 */
    		while ((line = br.readLine()) != null)   //returns a Boolean value  
    		{  
    			String[] datos = line.split(splitBy);    // use comma as separator
    			
    			mapeo = new MapeoInventarioSilicie();
    			
    			if (datos.length>3 && datos[7]!=null && datos[7].length()>0)
    			{
    				mapeo.setCae(datos[2]);
					mapeo.setCodigo(datos[7]);
					mapeo.setDescripcion(datos[9]);
					
					cantidad = RutinasCadenas.reemplazar(datos[11], ".", "");
					cantidad = RutinasCadenas.reemplazar(cantidad, ",", ".");
					
					mapeo.setFinales(new Double(cantidad));
					
    				vector.add(mapeo);
    			}
    			else
    				break;
    		}
    		br.close();
    		fl.close();
    		line=null;
    		br=null;
    		fl=null;
    	}   
    	catch (Exception e)   
    	{  
    		e.printStackTrace();  
    	}  
    	return vector;
    }

    public static ArrayList<MapeoInventarioSGA> leerCsvInventarioSGA(String r_separador, String r_ruta, String r_encabezados)
    {
    	ArrayList<MapeoInventarioSGA> vector = null;
    	/*
    	 * Leo el CSV de vuelta que me indique el usuario, actualizo el campo numeroAsiento 
    	 * con el que venga en el fihcero enlazando por el numero de referencia interno
    	 */
    	MapeoInventarioSGA mapeo = null;
    	vector = new ArrayList<MapeoInventarioSGA>();
    	
    	String cantidad="";
    	String line = "";  
    	String splitBy = r_separador;  
    	try   
    	{  
    		//parsing a CSV file into BufferedReader class constructor
    		FileReader fl = new FileReader(r_ruta);
    		BufferedReader br = new BufferedReader(fl);  
    		/*
    		 * me salto la línea de encabezados de columnas
    		 */
    		if (r_encabezados.equals("S")) br.readLine();
    		/*
    		 * procesamos lineas leidas
    		 */
    		while ((line = br.readLine()) != null)   //returns a Boolean value  
    		{  
    			String[] datos = line.split(splitBy);    // use comma as separator
    			
    			mapeo = new MapeoInventarioSGA();
    			
    			if (datos.length>3 )
    			{
    				mapeo.setSscc(datos[0]);
    				mapeo.setLote(datos[1]);
    				mapeo.setFechaCad(datos[2]);
    				mapeo.setLpn(datos[3]);
    				if (datos.length==4)
    					mapeo.setCantidad(null);
    				else
    					mapeo.setCantidad(datos[4]);
    				vector.add(mapeo);
    			}
    			else
    				break;
    		}
    		br.close();
    		fl.close();
    		line=null;
    		br=null;
    		fl=null;
    	}   
    	catch (Exception e)   
    	{  
    		e.printStackTrace();  
    	}  
    	return vector;
    }


    public static String generarCsv(ArrayList<MapeoInventarioSGAGsys> r_vector)
    {
		String ruta = eBorsao.get().prop.rutaSilicie + "rotativo" +RutinasFechas.añoActualYY()+RutinasFechas.mesActualMM()+RutinasFechas.diaActualDD()+ RutinasFechas.horaActualSinSeparador()  + ".csv";
		Iterator iterator = r_vector.iterator();
		File file = new File(ruta);
		if (file.exists())
		{
			do 
			{
				ruta = eBorsao.get().prop.rutaSilicie + "rotativo" +RutinasFechas.añoActualYY()+RutinasFechas.mesActualMM()+RutinasFechas.diaActualDD()+ RutinasFechas.horaActualSinSeparador()  + ".csv";
				file = new File(ruta);
			} while (file.exists());
		}
			
		StringBuilder linea = new StringBuilder();
		try
		{
			PrintWriter writer = new PrintWriter(file,"UTF-8");
				
			linea.append("articulo").append(COMMA_DELIMITER);
			linea.append("descripcion").append(COMMA_DELIMITER);
			linea.append("stock_Actual").append(COMMA_DELIMITER);
				
			writer.println(linea.toString());
				
			while (iterator.hasNext())
			{
				linea= new StringBuilder();
					
				MapeoInventarioSGAGsys mapeoCsv = (MapeoInventarioSGAGsys) iterator.next();
					
				//articulo
				linea.append(mapeoCsv.getArticulo()).append(COMMA_DELIMITER);
				//descripcion
				linea.append(mapeoCsv.getDescripcion()).append(COMMA_DELIMITER);
				//stock
				linea.append(mapeoCsv.getStock_actual_sga()).append(COMMA_DELIMITER);
				
				writer.println(linea.toString());
					
			}
			writer.close();
				
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		FileResource res = new FileResource(file);
		if (res!=null && res.getSourceFile()!=null && res.getSourceFile().getAbsolutePath()!=null) return res.getSourceFile().getAbsolutePath();
		else return null;
    }

    public static String generarCsv(String r_area, Integer r_ejercicio, Integer r_mes, Integer r_maxFilas, String r_parmAux)
    {
    	FileResource res = null;
    	Iterator iterator = null;
    	File file = null;
    	StringBuilder linea = null;
    	
    	Integer contador = 0;
    	Integer i = 0;
    	Integer filas = 0;
    	
    	
    	String ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) + String.valueOf(r_mes) +  ".csv";
    	
    	if (r_area.startsWith("anulacionSilicie"))
		{
    		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
    		ArrayList<MapeoMovimientosSilicie> vectorCsv = cds.datosMovimientosDeclaradosSilicieGlobal(null,true);
    		
    		if (vectorCsv!=null && !vectorCsv.isEmpty())
    		{
    			iterator = vectorCsv.iterator();
    			filas = vectorCsv.size() / r_maxFilas ;
    			
    			for (int j = 0;j<=filas; j++)
    			{
    				contador = 0;
    				file = new File(ruta);
    				if (file.exists())
    				{
    					do 
    					{
    						i+=1;
    						ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) + String.valueOf(r_mes) + "_" + i +  ".csv";
    						file = new File(ruta);
    					} while (file.exists());
    				}
    				
    				linea = new StringBuilder();
    				try
    				{
    					PrintWriter writer = new PrintWriter(file,"UTF-8");
    					
    					linea.append("Número Asiento").append(COMMA_DELIMITER);
    					linea.append("Número Referencia Interno").append(COMMA_DELIMITER);
    					linea.append("Fecha Registro Contable").append(COMMA_DELIMITER);
    					linea.append("Motivo Anulación").append(COMMA_DELIMITER);
    					linea.append("Observaciones").append(COMMA_DELIMITER);
    					
    					writer.println(linea.toString());
    					contador++;
    					
    					while (iterator.hasNext())
    					{
    						if (contador>=r_maxFilas) break;
    						linea= new StringBuilder();
    						
    						if (contador==534) 
    						{
    							System.err.println("Aqui estamos");
    						}
    						MapeoMovimientosSilicie mapeoCsv = (MapeoMovimientosSilicie) iterator.next();
    						
    						//asiento previo
    						linea.append(mapeoCsv.getNumeroAsiento()).append(COMMA_DELIMITER);
    						//referencia interna
    						if (mapeoCsv.getNumeroRefInterno()!=null && mapeoCsv.getNumeroRefInterno().length()>0) linea.append(mapeoCsv.getNumeroRefInterno().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getNumeroRefInterno()).append(COMMA_DELIMITER);
    						//fecha registro erp
    						linea.append(RutinasFechas.convertirDateToString(mapeoCsv.getFechaRegistro())).append(COMMA_DELIMITER);
    						//motivo anulacion - valor: otros motivos
    						linea.append("3").append(COMMA_DELIMITER);
    						//observaciones
    						linea.append("").append(COMMA_DELIMITER);
    						
    						writer.println(linea.toString());
    						contador++;
    						
    					}
    					writer.close();
    					
    				}
    				catch (Exception ex)
    				{
    					ex.printStackTrace();
    				}
    				res = new FileResource(file);
    			}
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a generar");
    		}
		}
    	else if (r_area.startsWith("silicie"))
    	{
    		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
    		ArrayList<MapeoMovimientosSilicie> vectorCsv = cds.datosMovimientosDeclaradosSilicieGlobal(null,false);
    		
    		if (vectorCsv!=null && !vectorCsv.isEmpty())
    		{
    			iterator = vectorCsv.iterator();
    			filas = vectorCsv.size() / r_maxFilas ;
    			
    			for (int j = 0;j<=filas; j++)
    			{
    				contador = 0;
    				file = new File(ruta);
    				if (file.exists())
    				{
    					do 
    					{
    						i+=1;
    						ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) + String.valueOf(r_mes) + "_" + i +  ".csv";
    						file = new File(ruta);
    					} while (file.exists());
    				}
    				
    				linea = new StringBuilder();
    				try
    				{
    					PrintWriter writer = new PrintWriter(file,"UTF-8");
    					
    					linea.append("Número Referencia Interno").append(COMMA_DELIMITER);
    					linea.append("Número Asiento Previo").append(COMMA_DELIMITER);
    					linea.append("Fecha Movimiento").append(COMMA_DELIMITER);
    					linea.append("Fecha Registro Contable").append(COMMA_DELIMITER);
    					linea.append("Tipo Movimiento").append(COMMA_DELIMITER);
    					linea.append("Información adicional Diferencia en Menos").append(COMMA_DELIMITER);
    					linea.append("Régimen Fiscal").append(COMMA_DELIMITER);
    					linea.append("Tipo de Operación").append(COMMA_DELIMITER);
    					linea.append("Número Operación").append(COMMA_DELIMITER);
    					linea.append("Descripción Unidad de Fabricación").append(COMMA_DELIMITER);
    					linea.append("Código Unidad de Fabricación").append(COMMA_DELIMITER);
    					linea.append("Tipo Justificante").append(COMMA_DELIMITER);
    					linea.append("Número Justificante").append(COMMA_DELIMITER);
    					linea.append("Tipo Documento Identificativo").append(COMMA_DELIMITER);
    					linea.append("Número Documento Identificativo").append(COMMA_DELIMITER);
    					linea.append("Razón Social").append(COMMA_DELIMITER);
    					linea.append("CAE/Número Seed").append(COMMA_DELIMITER);
    					linea.append("Repercusión Tipo Documento Identificativo").append(COMMA_DELIMITER);
    					linea.append("Repercusión Número Documento Identificativo").append(COMMA_DELIMITER);
    					linea.append("Repercusión Razón Social").append(COMMA_DELIMITER);
    					linea.append("Epígrafe").append(COMMA_DELIMITER);
    					linea.append("Código Epígrafe").append(COMMA_DELIMITER);
    					linea.append("Código NC").append(COMMA_DELIMITER);
    					linea.append("Clave").append(COMMA_DELIMITER);
    					linea.append("Cantidad").append(COMMA_DELIMITER);
    					linea.append("Unidad de Medida").append(COMMA_DELIMITER);
    					linea.append("Descripción de Producto").append(COMMA_DELIMITER);
    					linea.append("Referencia Producto").append(COMMA_DELIMITER);
    					linea.append("Densidad").append(COMMA_DELIMITER);
    					linea.append("Grado Alcohólico").append(COMMA_DELIMITER);
    					linea.append("Cantidad de Alcohol Puro").append(COMMA_DELIMITER);
    					linea.append("Porcentaje de Extracto").append(COMMA_DELIMITER);
    					linea.append("Kg. - Extracto").append(COMMA_DELIMITER);
    					linea.append("Grado Plato Medio").append(COMMA_DELIMITER);
    					linea.append("Grado Acético").append(COMMA_DELIMITER);
    					linea.append("Tipo de Envase").append(COMMA_DELIMITER);
    					linea.append("Capacidad de Envase").append(COMMA_DELIMITER);
    					linea.append("Número de Envases").append(COMMA_DELIMITER);
    					linea.append("Observaciones").append(COMMA_DELIMITER);
    					
    					writer.println(linea.toString());
    					contador++;
    					
    					while (iterator.hasNext())
    					{
    						if (contador>=r_maxFilas) break;
    						linea= new StringBuilder();
    						
    						if (contador==534) 
    						{
    							System.err.println("Aqui estamos");
    						}
    						MapeoMovimientosSilicie mapeoCsv = (MapeoMovimientosSilicie) iterator.next();
    						
    						//referencia interna
    						if (mapeoCsv.getNumeroRefInterno()!=null && mapeoCsv.getNumeroRefInterno().length()>0) linea.append(mapeoCsv.getNumeroRefInterno().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getNumeroRefInterno()).append(COMMA_DELIMITER);
    						//asiento previo
    						linea.append("").append(COMMA_DELIMITER);
    						//fecha movimiento
    						linea.append(RutinasFechas.convertirDateToString(mapeoCsv.getFechaMovimiento())).append(COMMA_DELIMITER);
    						//fecha registro erp
    						linea.append(RutinasFechas.convertirDateToString(mapeoCsv.getFechaRegistro())).append(COMMA_DELIMITER);
    						//codigoo movimiento
    						if (mapeoCsv.getCodigoMovimiento()!=null && mapeoCsv.getCodigoMovimiento().length()>0) linea.append(mapeoCsv.getCodigoMovimiento().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getCodigoMovimiento()).append(COMMA_DELIMITER);
    						//codigo diferencia menos
    						if (mapeoCsv.getCodigoDiferenciaMenos()!=null && mapeoCsv.getCodigoDiferenciaMenos().length()>0)
    							linea.append(mapeoCsv.getCodigoDiferenciaMenos().trim()).append(COMMA_DELIMITER);
    						else
    							linea.append("").append(COMMA_DELIMITER);
    						//regimen fiscal
    						linea.append(mapeoCsv.getCodigoRegimenFiscal()).append(COMMA_DELIMITER);
    						//tipo transformacion
    						linea.append(mapeoCsv.getCodigoOperacion()).append(COMMA_DELIMITER);
    						//numero operacion transformacion
    						if (mapeoCsv.getIdCodigoOperacion() !=null && mapeoCsv.getIdCodigoOperacion()!=0) linea.append(mapeoCsv.getIdCodigoOperacion()).append(COMMA_DELIMITER); else linea.append("").append(COMMA_DELIMITER);
    						//unidad fabricacion desripcion
    						linea.append("").append(COMMA_DELIMITER);
    						//unidad fabricacion codigo
    						linea.append("").append(COMMA_DELIMITER);
    						//tipo justificante
    						linea.append(mapeoCsv.getCodigoJustificante()).append(COMMA_DELIMITER);
    						//numero justificante
    						linea.append(mapeoCsv.getNumeroJustificante()).append(COMMA_DELIMITER);
    						//codigo documento identificacion origen destino
    						linea.append(mapeoCsv.getCodigoDocumentoOd()).append(COMMA_DELIMITER);
    						//numero documento identificacion origen destino
    						if (mapeoCsv.getNumeroDocumentoIdOD()!=null && mapeoCsv.getNumeroDocumentoIdOD().length()>0) linea.append(mapeoCsv.getNumeroDocumentoIdOD().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getNumeroDocumentoIdOD()).append(COMMA_DELIMITER);
    						//razon social origen destino
    						if (mapeoCsv.getRazonSocialOD()!=null && mapeoCsv.getRazonSocialOD().length()>0) linea.append(mapeoCsv.getRazonSocialOD().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getRazonSocialOD()).append(COMMA_DELIMITER);
    						//cae origen destino
    						if (mapeoCsv.getCaeOD()!=null && mapeoCsv.getCaeOD().length()>0) linea.append(mapeoCsv.getCaeOD().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getCaeOD()).append(COMMA_DELIMITER);
    						//codigo documento identificacion repercusion impuesto
    						linea.append("").append(COMMA_DELIMITER);
    						//numero documento identificacion repercusion impuesto
    						linea.append("").append(COMMA_DELIMITER);
    						//razon social repercusion impuesto
    						linea.append("").append(COMMA_DELIMITER);
    						//epigrafe
    						linea.append(mapeoCsv.getEpigrafe()).append(COMMA_DELIMITER);
    						//codigo epigrafe
    						linea.append(mapeoCsv.getCodigoEpigrafe()).append(COMMA_DELIMITER);
    						//codigo nomencaltura combinada
    						linea.append(mapeoCsv.getCodigoNC()).append(COMMA_DELIMITER);
    						//clave asociada al epigrafe
    						linea.append(mapeoCsv.getClave()).append(COMMA_DELIMITER);
    						//cantidad
    						linea.append(RutinasCadenas.reemplazarPuntoMiles(mapeoCsv.getLitros().toString())).append(COMMA_DELIMITER);
    						//unidad medida
    						linea.append(mapeoCsv.getUnidad_medida()).append(COMMA_DELIMITER);
    						//descripcion producto
    						if (mapeoCsv.getDescripcion()!=null && mapeoCsv.getDescripcion().length()>0) linea.append(mapeoCsv.getDescripcion().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getDescripcion()).append(COMMA_DELIMITER);
    						//referencia interna producto (caso packs)
    						if (mapeoCsv.getCodigoArticulo()!=null && mapeoCsv.getCodigoArticulo().length()>0) linea.append(mapeoCsv.getCodigoArticulo().trim()).append(COMMA_DELIMITER); else linea.append(mapeoCsv.getCodigoArticulo()).append(COMMA_DELIMITER);
    						//densidad
    						linea.append("").append(COMMA_DELIMITER);
    						//grado alcoholico
    						linea.append(RutinasCadenas.reemplazarPuntoMiles(mapeoCsv.getGrado().toString())).append(COMMA_DELIMITER);
    						//cantidad_absoluta
    						linea.append(RutinasCadenas.reemplazarPuntoMiles(mapeoCsv.getAlcoholPuro().toString())).append(COMMA_DELIMITER);
    						//extractoPorcentaje
    						linea.append("").append(COMMA_DELIMITER);
    						//extractoKg
    						linea.append("").append(COMMA_DELIMITER);
    						//gradoPlatoMedio
    						linea.append("").append(COMMA_DELIMITER);
    						//gradoAcetico
    						linea.append("").append(COMMA_DELIMITER);
    						//tipo envase
    						linea.append("").append(COMMA_DELIMITER);
    						//caàcidad envase
    						linea.append("").append(COMMA_DELIMITER);
    						//numero envases
    						linea.append("").append(COMMA_DELIMITER);
    						//observaciones
    						linea.append("").append(COMMA_DELIMITER);
    						
    						writer.println(linea.toString());
    						contador++;
    						
    					}
    					writer.close();
    					
    				}
    				catch (Exception ex)
    				{
    					ex.printStackTrace();
    				}
    				res = new FileResource(file);
    			}
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a generar");
    		}
		}
    	//
    	else if (r_area.startsWith("FincasVendimiaAlbaranes"))
		{
			ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux +  ".csv";
			
			consultaFincasVendimiaServer cfvs = consultaFincasVendimiaServer.getInstance(CurrentUser.get());
			MapeoFincasVendimia mapeo = new MapeoFincasVendimia();
			mapeo.setEjercicio(r_ejercicio);
    		ArrayList<MapeoEntradasCosecha> vectorAlb = cfvs.datosFincasVendimiaAlbGlobal(mapeo,r_parmAux);
    		
    		if (vectorAlb!=null && !vectorAlb.isEmpty())
    		{
    			iterator = vectorAlb.iterator();
				file = new File(ruta);
				if (file.exists())
				{
					do 
					{
						ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux + RutinasFechas.horaActualSinSeparador()  + ".csv";
						file = new File(ruta);
					} while (file.exists());
				}
				
				linea = new StringBuilder();
				try
				{
					PrintWriter writer = new PrintWriter(file,"UTF-8");
					
					linea.append("Bodega").append(COMMA_DELIMITER);
					linea.append("Socio").append(COMMA_DELIMITER);
					linea.append("Nombre").append(COMMA_DELIMITER);
					linea.append("Finca").append(COMMA_DELIMITER);
					linea.append("Albaran").append(COMMA_DELIMITER);
					linea.append("Variedad Alb").append(COMMA_DELIMITER);
					linea.append("Articulo").append(COMMA_DELIMITER);
					linea.append("Descripcion").append(COMMA_DELIMITER);
					linea.append("Kilos").append(COMMA_DELIMITER);
					linea.append("Variedad Fin").append(COMMA_DELIMITER);

					writer.println(linea.toString());
					
					while (iterator.hasNext())
					{
						linea= new StringBuilder();
						
						MapeoEntradasCosecha mapeoCsv = (MapeoEntradasCosecha) iterator.next();
						
						//Bodega
						if (r_parmAux.contentEquals("Borja")) 
							linea.append("080").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Pozuelo"))
							linea.append("138").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Tabuenca"))
							linea.append("141").append(COMMA_DELIMITER);
						//Socio
						linea.append(mapeoCsv.getSocio().toString()).append(COMMA_DELIMITER);
						//Nombre
						linea.append(mapeoCsv.getNombreSocio()).append(COMMA_DELIMITER);
						//Finca
						if (mapeoCsv.getFinca()!=null) linea.append(mapeoCsv.getFinca()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						//Albaran
						if (mapeoCsv.getCodigo()!=null) linea.append(mapeoCsv.getCodigo()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						//Variedad
						linea.append(mapeoCsv.getAlmacen()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getArticulo()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getNombreArticulo()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getKgs_neto_liq()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getCalidad()).append(COMMA_DELIMITER);
						
						writer.println(linea.toString());
						
					}
					writer.close();
					
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				res = new FileResource(file);
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a generar");
    		}
    	}
    	else if (r_area.startsWith("EntradaUvaDeposito"))
		{
			ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux +  ".csv";
			
			consultaFincasVendimiaServer cfvs = consultaFincasVendimiaServer.getInstance(CurrentUser.get());
			MapeoFincasVendimia mapeo = new MapeoFincasVendimia();
			mapeo.setEjercicio(r_ejercicio);
    		ArrayList<MapeoEntradasCosecha> vectorAlb = cfvs.datosEntradasUvaDeposito(mapeo,r_parmAux);
    		
    		if (vectorAlb!=null && !vectorAlb.isEmpty())
    		{
    			iterator = vectorAlb.iterator();
				file = new File(ruta);
				if (file.exists())
				{
					do 
					{
						ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux + RutinasFechas.horaActualSinSeparador()  + ".csv";
						file = new File(ruta);
					} while (file.exists());
				}
				
				linea = new StringBuilder();
				try
				{
					PrintWriter writer = new PrintWriter(file,"UTF-8");
					
					linea.append("Bodega").append(COMMA_DELIMITER);
					linea.append("Albaran").append(COMMA_DELIMITER);
					linea.append("Fecha").append(COMMA_DELIMITER);
					linea.append("Socio").append(COMMA_DELIMITER);
					linea.append("Nombre").append(COMMA_DELIMITER);
					linea.append("Articulo").append(COMMA_DELIMITER);
					linea.append("Descripcion").append(COMMA_DELIMITER);
					linea.append("Recogida").append(COMMA_DELIMITER);
					linea.append("Kilos Bruto").append(COMMA_DELIMITER);
					linea.append("Kilos Tara").append(COMMA_DELIMITER);
					linea.append("Kilos Neto").append(COMMA_DELIMITER);
					linea.append("Porcentaje").append(COMMA_DELIMITER);
					linea.append("Kilos Nto Liq.").append(COMMA_DELIMITER);
					linea.append("Finca").append(COMMA_DELIMITER);
					linea.append("Pueblo").append(COMMA_DELIMITER);
					linea.append("Grado").append(COMMA_DELIMITER);
					linea.append("Estado Sanitario").append(COMMA_DELIMITER);
					linea.append("Calidad").append(COMMA_DELIMITER);
					linea.append("Almacen").append(COMMA_DELIMITER);
					linea.append("Rendimiento").append(COMMA_DELIMITER);
					linea.append("Disponible").append(COMMA_DELIMITER);
					writer.println(linea.toString());
					
					while (iterator.hasNext())
					{
						linea= new StringBuilder();
						
						MapeoEntradasCosecha mapeoCsv = (MapeoEntradasCosecha) iterator.next();
						
						//Bodega
						if (r_parmAux.contentEquals("Borja")) 
							linea.append("080").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Pozuelo"))
							linea.append("138").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Tabuenca"))
							linea.append("141").append(COMMA_DELIMITER);
						
						if (mapeoCsv.getCodigo()!=null) linea.append(mapeoCsv.getCodigo()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER); 
						linea.append(RutinasFechas.convertirDateToString(mapeoCsv.getFecha())).append(COMMA_DELIMITER);
						//Socio
						linea.append(mapeoCsv.getSocio().toString()).append(COMMA_DELIMITER);
						//Nombre
						linea.append(mapeoCsv.getNombreSocio()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getArticulo()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getNombreArticulo()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getRecogida()).append(COMMA_DELIMITER);
						if (mapeoCsv.getKgs_bruto()!=null) linea.append(mapeoCsv.getKgs_bruto()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						if (mapeoCsv.getTara()!=null) linea.append(mapeoCsv.getTara()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						if (mapeoCsv.getKgs_neto()!=null) linea.append(mapeoCsv.getKgs_neto()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						if (mapeoCsv.getPorcentaje()!=null) linea.append(mapeoCsv.getPorcentaje()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						if (mapeoCsv.getKgs_neto_liq()!=null) linea.append(mapeoCsv.getKgs_neto_liq()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						
						//Finca
						if (mapeoCsv.getFinca()!=null) linea.append(mapeoCsv.getFinca()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getPueblo()).append(COMMA_DELIMITER);
						if (mapeoCsv.getGrado()!=null) linea.append(mapeoCsv.getGrado()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						
						linea.append(mapeoCsv.getEstadoSanitario()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getCalidad()).append(COMMA_DELIMITER);
						linea.append(mapeoCsv.getAlmacen()).append(COMMA_DELIMITER);
						if (mapeoCsv.getKgs_neto_liq_grado()!=null) linea.append(mapeoCsv.getKgs_neto_liq_grado()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						if (mapeoCsv.getDisponible()!=null) linea.append(mapeoCsv.getDisponible()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						
						//Variedad
						
						writer.println(linea.toString());
						
					}
					writer.close();
					
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				res = new FileResource(file);
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a generar");
    		}
    	}
		else if (r_area.startsWith("DGAFincasVendimia"))
		{
			ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux +  ".csv";
			
			consultaFincasVendimiaServer cfvs = consultaFincasVendimiaServer.getInstance(CurrentUser.get());
			MapeoFincasVendimia mapeo = new MapeoFincasVendimia();
			mapeo.setEjercicio(r_ejercicio);
    		ArrayList<MapeoFincasVendimia> vectorDGA = cfvs.datosFincasVendimiaDGAGlobal(mapeo,r_parmAux);
    		
    		if (vectorDGA!=null && !vectorDGA.isEmpty())
    		{
    			iterator = vectorDGA.iterator();
				file = new File(ruta);
				if (file.exists())
				{
					do 
					{
						ruta = eBorsao.get().prop.rutaSilicie + r_area + String.valueOf(r_ejercicio) +r_parmAux + RutinasFechas.horaActualSinSeparador()  + ".csv";
						file = new File(ruta);
					} while (file.exists());
				}
				
				linea = new StringBuilder();
				try
				{
					PrintWriter writer = new PrintWriter(file,"UTF-8");
					
					linea.append("Cosecha").append(COMMA_DELIMITER);
					linea.append("Bodega").append(COMMA_DELIMITER);
					linea.append("CIF/NIF").append(COMMA_DELIMITER);
					linea.append("Cosechero").append(COMMA_DELIMITER);
					linea.append("Variedad").append(COMMA_DELIMITER);
					linea.append("Provincia").append(COMMA_DELIMITER);
					linea.append("Municipio").append(COMMA_DELIMITER);
					linea.append("Agregado").append(COMMA_DELIMITER);
					linea.append("Zona").append(COMMA_DELIMITER);
					linea.append("Poligono").append(COMMA_DELIMITER);
					linea.append("Parcela").append(COMMA_DELIMITER);
					linea.append("Subparcela").append(COMMA_DELIMITER);
					linea.append("Recinto").append(COMMA_DELIMITER);
					linea.append("Destino").append(COMMA_DELIMITER);
					linea.append("KG").append(COMMA_DELIMITER);
					linea.append("Planes").append(COMMA_DELIMITER);
					linea.append("Socio").append(COMMA_DELIMITER);
					linea.append("NombreSocio").append(COMMA_DELIMITER);
					linea.append("Finca").append(COMMA_DELIMITER);
					linea.append("Superficie").append(COMMA_DELIMITER);
					linea.append("kgs Entrgados").append(COMMA_DELIMITER);
					linea.append("SuperficieParcela").append(COMMA_DELIMITER);
					
					writer.println(linea.toString());
					
					while (iterator.hasNext())
					{
						linea= new StringBuilder();
						
						MapeoFincasVendimia mapeoCsv = (MapeoFincasVendimia) iterator.next();
						
						//Cosecha
						linea.append(mapeoCsv.getEjercicio().toString()).append(COMMA_DELIMITER);
						//Bodega
						if (r_parmAux.contentEquals("Borja")) 
							linea.append("080").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Pozuelo"))
							linea.append("138").append(COMMA_DELIMITER);
						else if (r_parmAux.contentEquals("Tabuenca"))
							linea.append("141").append(COMMA_DELIMITER);
						//NIF
						linea.append(mapeoCsv.getCif().trim()).append(COMMA_DELIMITER);
						//COSECHERO
						if (mapeoCsv.getCosechero()!=null) linea.append(mapeoCsv.getCosechero().trim()).append(COMMA_DELIMITER); else linea.append(" ").append(COMMA_DELIMITER);
						//Variedad
						linea.append(mapeoCsv.getVariedad()).append(COMMA_DELIMITER);
						//Provincia
						linea.append(mapeoCsv.getProvincia()).append(COMMA_DELIMITER);
						//Municipio
						linea.append(mapeoCsv.getMunicipio().toString()).append(COMMA_DELIMITER);
						//Agregado
						linea.append("0").append(COMMA_DELIMITER);
						//Zona
						linea.append("0").append(COMMA_DELIMITER);
						//Poligono
						linea.append(mapeoCsv.getPoligono()).append(COMMA_DELIMITER);
						//Parcela
						linea.append(mapeoCsv.getParcela()).append(COMMA_DELIMITER);
						//SubParcela
						if (mapeoCsv.getSubParcela()!=null) linea.append(mapeoCsv.getSubParcela().trim()).append(COMMA_DELIMITER); else linea.append("").append((COMMA_DELIMITER));
						//Recinto
						if (mapeoCsv.getRecinto()!=null) linea.append(RutinasNumericas.formatearIntegerDigitosIzqda(new Integer(mapeoCsv.getRecinto().trim()), 4)).append(COMMA_DELIMITER); else linea.append("").append((COMMA_DELIMITER));
						//Destino
						linea.append(" " ).append(COMMA_DELIMITER);
						//KG
						linea.append(0).append(COMMA_DELIMITER);
						//Planes
						linea.append(" ").append(COMMA_DELIMITER);
						//Socio
						linea.append(mapeoCsv.getSocio().toString()).append(COMMA_DELIMITER);
						//Nombre Socio
						linea.append(mapeoCsv.getNombre().trim()).append(COMMA_DELIMITER);
						//Finca
						linea.append(mapeoCsv.getFinca().toString()).append(COMMA_DELIMITER);
						//Superficie
						linea.append(RutinasNumericas.formatearDoubleDecimales(mapeoCsv.getSuperficie(),4)).append(COMMA_DELIMITER);
						//Rendimiento
						linea.append(RutinasNumericas.formatearDoubleDecimales(mapeoCsv.getRendimiento(),4)).append(COMMA_DELIMITER);
						//superficie subparcela
						linea.append(RutinasNumericas.formatearDoubleDecimales(mapeoCsv.getSuperficieParcela(),4)).append(COMMA_DELIMITER);
						
						writer.println(linea.toString());
						
					}
					writer.close();
					
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
				}
				res = new FileResource(file);
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("No hay datos a generar");
    		}
    	}
    	if (res!=null && res.getSourceFile()!=null && res.getSourceFile().getAbsolutePath()!=null) return res.getSourceFile().getAbsolutePath();
    	else return null;
    }
    
    
    public static String generarUNLGsys(String r_bbdd, String r_tabla, String r_sql)
    {
    	FileResource res = null;
    	File file = null;
    	StringBuilder linea = null;
    	HashMap<Integer, String> valores = null;
    	String dato = null;
    	
    	String ruta = eBorsao.get().prop.rutaSilicie + r_tabla +  ".unl";

    	servidorSincronizadorBBDD ss = new servidorSincronizadorBBDD();
    	HashMap<Integer, HashMap<Integer, String>> datosFichero = ss.generarEstructuraTabla(r_bbdd, r_tabla, r_sql);

    	try
    	{
			file = new File(ruta);
			if (file.exists())
			{
				do 
				{
					file.delete();
				} while (file.exists());
				file = new File(ruta);
			}
			
	
			PrintWriter writer = new PrintWriter(file,"UTF-8");
	    	
	    	for (int registro=0;registro<=datosFichero.size()-1;registro++)
	    	{
	    		valores = datosFichero.get(registro);
	    		linea = new StringBuilder();
	    		
	    		for (int campo=0; campo<=valores.size()-1; campo++)
	    		{
	    			dato = valores.get(campo);
	    			if (dato!=null && dato.length()>0)
	    				linea.append(dato.trim()).append(PIPE_DELIMITER);
	    			else
	    				linea.append("").append(PIPE_DELIMITER);
	    		}
	    		writer.println(linea.toString());
	    	}    		
			writer.close();
			res = new FileResource(file);
    	}
    	catch (Exception ex)
    	{
    		Notificaciones.getInstance().mensajeError("Problemas al generar el fichero");
    	}
    	if (res!=null && res.getSourceFile()!=null && res.getSourceFile().getAbsolutePath()!=null) return res.getSourceFile().getAbsolutePath();
    	else return null;
    }
}

