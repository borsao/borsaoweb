package borsao.e_borsao.ClasesPropias;

import java.sql.CallableStatement;
import java.sql.Connection;

public class RutinasBaseDatos
{
	public RutinasBaseDatos()
	{
		
	}
	
	public static String eliminarTablaTemporal(String r_prop, String r_tabla)
	{
		CallableStatement callableStatement = null;
		Connection con = null;
		
		String tabla = r_tabla;
		String prop = r_prop;
		String query = "{ call comprobar_tabla_existente(?,?,?) }"; 
		 
		try
		{
			connectionManager conManager = connectionManager.getInstance();
			con = conManager.establecerConexion();
			
			callableStatement= con.prepareCall(query);
			callableStatement.setString(1, tabla);
			callableStatement.setString(2, prop);
			callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
			callableStatement.executeQuery();
			
			int rdo = callableStatement.getInt(3);
			
//			System.out.println("Acabo de comprobar: " + rdo);

			if (rdo!=0) 
			{
//				System.out.println("La tabla existe " + callableStatement.getInt(3));
				
				query = "{ call borrar_tabla_existente(?,?) }";
				callableStatement= con.prepareCall(query);
				callableStatement.setString(1, tabla);
				callableStatement.setString(2, prop);
				callableStatement.executeQuery();
				
//				System.out.println("La tabla se borra ");
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		return null;
	}
	
	public static Integer comprobarMaximoId(String r_campo, String r_tabla)
	{
		CallableStatement callableStatement = null;
		Connection con = null;
		Integer rdo = null;
		String tabla = r_tabla;
		String query = "{ call comprobar_max_id(?,?,?) }"; 
		 
		try
		{
			connectionManager conManager = connectionManager.getInstance();
			con = conManager.establecerConexion();
			
			callableStatement= con.prepareCall(query);
			callableStatement.setString(1, tabla);			
			callableStatement.setString(2, r_campo);
			callableStatement.registerOutParameter(3, java.sql.Types.INTEGER);
			callableStatement.executeQuery();
			
			rdo = callableStatement.getInt(3)+1;
			
//			System.out.println("El registro maximo es: " + rdo);

		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
			return 0;
		}
		return rdo;
	}
	public static String crearTablaTemporal(String r_prop, String r_tabla)
	{
		CallableStatement callableStatement = null;
		Connection con = null;
		
		String tabla = r_tabla;
		String prop = r_prop;
		String query = "{ call crear_tabla_temporal(?,?) }"; 
		 
		try
		{
			connectionManager conManager = connectionManager.getInstance();
			con = conManager.establecerConexion();
			
			callableStatement= con.prepareCall(query);
			callableStatement.setString(1, tabla);
			callableStatement.setString(2, prop);
			callableStatement.executeQuery();
			
//			System.out.println("La tabla se crea " + r_prop + " " + r_tabla);
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		return null;
	}
	
	
}
