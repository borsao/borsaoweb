package borsao.e_borsao.ClasesPropias;

import com.vaadin.server.Resource;
import com.vaadin.ui.Image;

import borsao.e_borsao.eBorsao;

public class Imagen extends Image
{
	public Imagen()
	{
	}
	
	public Imagen(String r_caption, Resource r_resource)
	{
		super.setSource(r_resource);
		super.setCaption(r_caption);
		this.addStyleName("logoMenu");
		this.setStyleName("logoMenu");
		
        com.vaadin.event.MouseEvents.ClickListener lis = new com.vaadin.event.MouseEvents.ClickListener() {
			
			@Override
			public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
				String url = "http://" + eBorsao.strDominio;
        		getUI().getPage().open(url, "_blank");				
			}
		};
        this.addClickListener(lis);

	}
}