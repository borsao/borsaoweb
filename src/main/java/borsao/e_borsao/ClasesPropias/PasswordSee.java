package borsao.e_borsao.ClasesPropias;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

public class PasswordSee extends CustomComponent {

	public TextField txtTexto = null;
	public PasswordField txtPass = null;
	public Button btnAyuda = null;
	private HorizontalLayout layout = null;
	
	public PasswordSee (String r_caption) {

			layout = new HorizontalLayout();
			setCompositionRoot(layout);
			
			txtTexto = new TextField();
			txtTexto.setWidth(15, Unit.EM);
			txtTexto.setIcon(FontAwesome.UNLOCK_ALT);
			txtTexto.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
			txtPass = new PasswordField();			
			txtPass.setVisible(true);
			txtPass.setWidth(15, Unit.EM);
			txtPass.setIcon(FontAwesome.UNLOCK_ALT);
			txtPass.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
			layout.addComponent(txtPass);
			
			btnAyuda = new Button();
			btnAyuda.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
			btnAyuda.addStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);
			btnAyuda.addStyleName("planos");
			btnAyuda.setIcon(FontAwesome.EYE);
			btnAyuda.setWidth(3, Unit.EM);
			btnAyuda.setTabIndex(-1);
			btnAyuda.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					if (txtPass.isVisible())
					{
						layout.removeAllComponents();
						layout.addComponent(txtTexto);
						layout.addComponent(btnAyuda);
						txtPass.setVisible(false);
						txtTexto.setVisible(true);
						txtTexto.setValue(txtPass.getValue());
						layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
					}
					else
					{
						layout.removeAllComponents();
						layout.addComponent(txtPass);
						layout.addComponent(btnAyuda);
						txtPass.setVisible(true);
						txtTexto.setVisible(false);
						txtPass.setValue(txtTexto.getValue());
						layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
					}
				}
			});
			
			layout.addComponent(btnAyuda);
			layout.setComponentAlignment(btnAyuda,Alignment.BOTTOM_LEFT);
	}
}