package borsao.e_borsao.ClasesPropias;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;


public class GridDinamico extends Grid
{

	/**
	 * 	ATRIBUTOS PUBLICOS Y PRIVADOS PARA CONSTRUIR LA CLASE
	 * 
	 */
	/*
	 * Origen de creacion del grid para poder modificar el posicionamiento en pantalla
	 */
	public MaquetadoDinamico app = null;
	private boolean hayGrid=false;
	/*
	 * contenedor de los datos que vamos a presentar. 
	 * Define la estructura de celdas, columnas, cabeceras, pies...
	 */
	private IndexedContainer container =null;
	/*
	 * Atributos que modifican el comportamiento del grid
	 */
	private boolean conTotalesColumna = false;
	private boolean mostrarTercerNivel = false;
	private boolean conFiltro = false;
	private boolean conObservaciones = false;
	
	/*
	 * vectores que nos configuran los campos a filtrar, anchuras y demas
	 */
	public ArrayList<String> camposFiltrar = new ArrayList<String>();
	public HashMap<String,String> widthFiltros = new HashMap<String, String>();
	public HashMap<String,String> filtros = new HashMap<String, String>();
	
	//	public String valorSeleccionado=null;
	public HashMap<String , String> opcionesEscogidas = null;
	
	/*
	 * hash que contiene cada columna de la tabla de que tipo es
	 */
	private ArrayList<MapeoGridDinamico> vectorEstructuras=null;
	private HashMap<String, String> hashColumnasTiposFijas = null;
	private HashMap<String, String> hashColumnasNombresFijas = null;
	/*
	 * hash que contiene como clave el valor de la columna, como valor el contenido de la base de datos
	 */
//	private ArrayList<MapeoPedidosNacional> vectorDatos=null;
	/*
	 * Array que contiene el titulo de las columnas del grid
	 */
	private ArrayList<String> vectorTitulos=null;
	private ArrayList<String> vectorTitulosHeader=null;
	/*
	 * hash que contiene para cada columna el articulo que recoge
	 */
	private HashMap<String,String> vectorColumnas=null;
	/*
	 * hash que contiene para cada columna a que agrupacion pertenece
	 */
	private HashMap<String,String> vectorAgrupaciones=null;
	/*
	 * hash que contiene para cada agrupacion su nombre
	 */
	private HashMap<Integer, String> vectorNombreAgrupaciones=null;
	private HashMap<Integer, String> vectorNombreTotalesAgrupaciones=null;
	/*
	 * numero de agrupaciones a presentar
	 */
	private Integer numeroAgrupaciones = null;
	
	/**
	 * 	CONSTRUCTORES DE LA CLASE
	 * 
	 */

	/**
	 * 
	 * @param r_app								GridViewRefresh Origen de creacion del grid para poder modificar el posicionamiento en pantalla
	 * @param r_vectorEstructuras				array
	 * @param r_mapeo							mapeo que contiene todas las carateristicas de la estrucrura del grid 
	 * 
	 * 		@param r_vectorTitulos					Array 			Contiene el titulo de las columnas del grid
	 * 		@param r_vectorTitulosHeader			Array 			Contiene el nombre visible de las columnas del grid
	 * 		@param r_vectorColumnas					hash 			contiene para cada columna el articulo que recoge
	 * 
	 * 											contiene cada columna de la tabla de que tipo es
	 * 		@param r_hashColumnasTiposFijas			hash
	 * 		@param r_hashColumnasNombresFijas		hash
	 * 
	 * 											CONTIENEN TODA LA INFORMACION PARA GENERAR LA AGRUPACION DE COLUMNAS
	 * 
	 * 		@param r_vectorAgrupaciones				hash 			contiene para cada columna a que agrupacion pertenece
	 * 		@param r_gamasTotales					Integer 		numero de agrupaciones a presentar
	 * 		@param r_nombreAgrupaciones				hash			contiene los nombres a mostrar de cada agrupacion
	 * 		@param r_nombreTotalesAgrupaciones		hash			contiene los nombres para la columna de cada total de cada agrupacion
	 * 
	 * @param r_isConTotalesColumnas			boolean			aciva la visualizacion de subtotales por agrupacion
	 */

	public GridDinamico(MaquetadoDinamico r_app, MapeoEstructuraGridDinamico r_mapeo, ArrayList<MapeoGridDinamico> r_vectorEstructuras,  boolean r_isConTotalesColumnas, boolean r_mostrarTercerNivel, boolean r_observaciones)
	{
		/*
		 * rellenamos todos los datos de origen en sus respectivas variables para su uso posterior
		 */
		app = r_app;
		this.setConTotalesColumna(r_isConTotalesColumnas);
		this.setMostrarTercerNivel(r_mostrarTercerNivel);
		this.setConObservaciones(r_observaciones);
		
		this.vectorTitulos= r_mapeo.getVectorTitulos();
		this.vectorTitulosHeader= r_mapeo.getVectorTitulosHeader();
		this.vectorColumnas = r_mapeo.getVectorColumnas();
		this.vectorAgrupaciones=r_mapeo.getHashGamas();
		this.numeroAgrupaciones = r_mapeo.getTotalAgrupaciones();
		this.vectorNombreAgrupaciones = r_mapeo.getNombreAgrupaciones();
		this.vectorNombreTotalesAgrupaciones = r_mapeo.getNombreTotalesAgrupaciones();
		this.vectorEstructuras=r_vectorEstructuras;
		this.hashColumnasTiposFijas=r_mapeo.getHashColumnasTiposFijas();
		this.hashColumnasNombresFijas=r_mapeo.getHashColumnasNombresFijas();

		/*
		 * procesamos y rellenamos el contenido de la tabla
		 */
		this.llenarRegistros();
		/*
		 * aasociamos el container generado al grid
		 */
		this.setContainerDataSource(container);
		/*
		 * Una vez generado el contenido, procesamos las columnas para agruparlas en función de la configuracion aportada.
		 * 
		 * A su vez asociamos el mismo estilo de color a cada agrupacion y sus contenidos
		 * 
		 * Finalmente procesamos las columnas para establecer su titulo en pantalla
		 */
		this.generarAgrupaciones();
		this.pintarCabeceras();
		this.procesarTitulosColumnas();
		/*
		 * indicamos el tamaño y estilo y comportamiento del grid a nivel grafico
		 */
		this.setSizeFull();
		this.addStyleName("pedidosComercial");
		this.setSelectionMode(SelectionMode.SINGLE);
		setCaptionAsHtml(true);	
		setResponsive(true);

	}

	public GridDinamico()
	{
		setSizeFull();
		setCaptionAsHtml(true);	
		setResponsive(true);
	}

	/**
	 * 	METODOS PRIVADOS DE LA CLASE
	 * 
	 */

	private void llenarRegistros()
	{
		Integer totalPedido = null;
		Integer totalCabecera = null;
		Integer totalGama = null;
		
		container = new IndexedContainer();
		
		
		/*
		 * cargamos las columnas fijas del grid.
		 * son las columnas no calculadas y comunes a toda la fila
		 */
			for (int i = 0; i<this.hashColumnasTiposFijas.size();i++)
			{
				this.agregarColumna(hashColumnasNombresFijas.get(String.valueOf(i)), hashColumnasTiposFijas.get(String.valueOf(i)));
			}
	
			/*
			 * cargo todos los alias de datos a mostrar segun la tabla de articulos de la embotelladora
			 * 
			 */
			for (int g = 1; g<=this.numeroAgrupaciones; g++)
			{
				Integer gama = null;
				
				for (int i=0;i<this.vectorTitulos.size();i++)
				{
					String columna = this.vectorTitulos.get(i);
					gama = new Integer(this.vectorAgrupaciones.get(columna));
					if (gama==g)
					{
						this.agregarColumna(this.vectorTitulos.get(i), "Integer");
					}
				}
				if (isConTotalesColumna()) this.agregarColumna("Total" + recogerNombreTotalAgrupacion(g),"Integer");
	
			}
			if (isMostrarTercerNivel()) this.agregarColumna("Total CABECERA", "Integer"); 
			this.agregarColumna("Total", "Integer");
			if (isConObservaciones()) this.agregarColumna("Observaciones", "String");
			totalPedido = new Integer(0);
			totalCabecera = new Integer(0);
			Item file =null;
			
			/*
			 * Rellenamos el contenido de las celdas, para todos los registros obtenidos		
			 */
			for (int i=0;i<this.vectorEstructuras.size();i++)
			{
				totalPedido = new Integer(0);
				Object newItemId = container.addItem();
				file = container.getItem(newItemId);
				
				HashMap<String, Integer> hasValores = new HashMap<String, Integer>();
				Integer valor = new Integer(0);
				totalGama = new Integer(0);
				MapeoGridDinamico mapeoGridDinamico = this.vectorEstructuras.get(i);
				
				for (int j = 0; j<mapeoGridDinamico.getHashColumnasFijasValores().size()-1;j++)
				{
					this.establecerColumna(file, hashColumnasNombresFijas.get(String.valueOf(j)), mapeoGridDinamico.getHashColumnasFijasValores().get(String.valueOf(j)),String.valueOf(j));
				}
	    		hasValores = mapeoGridDinamico.getHashValores();

				for (int cab=0;cab<1;cab++)
				{
					totalCabecera = new Integer(0);

		    		for (int g = 1; g<=this.numeroAgrupaciones; g++)
		    		{
		    			Integer gama = null;
		    			totalGama = 0;
		    			
		    			for (int c=0;c<this.vectorTitulos.size();c++)
		        		{
		    				String col = this.vectorTitulos.get(c);
		    				String articulo = this.vectorColumnas.get(col);
		        			gama = new Integer(this.vectorAgrupaciones.get(col));
		        			
		    				if (gama==g)
		    				{
		        			
			        			if (articulo!=null && articulo.length()>0)
			        			{
			        				valor = hasValores.get(col);
			        				
			        				articulo = this.vectorColumnas.get("c-" + col);
			        				if (articulo!=null && articulo.length()>0)
			        				{
			        					valor = valor + hasValores.get("c-" + col);
			        				}
			        			
			        				if (valor!=null)
			        				{
			        					totalGama = totalGama + valor;
			        					totalPedido = totalPedido + valor;
			        					totalCabecera = totalCabecera + valor;
			        					file.getItemProperty(col).setValue(new Integer(valor));
			        				}
			        			}
			        		}
		        		}
		    			if (isConTotalesColumna()) file.getItemProperty("Total" + recogerNombreTotalAgrupacion(g)).setValue(totalGama);
		    		}
		    		if (isMostrarTercerNivel()) file.getItemProperty("Total CABECERA").setValue(totalCabecera);
				}
				file.getItemProperty("Total").setValue(totalPedido);
				if (isConObservaciones()) file.getItemProperty("Observaciones").setValue(mapeoGridDinamico.getHashColumnasFijasValores().get("observaciones"));
			}	
	}
	
	

	private void procesarTitulosColumnas()
	{
		for (int i=0;i<this.vectorTitulosHeader.size();i++)
		{
			this.getColumn(vectorTitulos.get(i)).setHeaderCaption(vectorTitulosHeader.get(i));
		}
	}
	
	private void pintarCabeceras()
	{
		int gama = 0;
		for (int i=0;i<this.vectorTitulos.size();i++)
		{
			gama = obtenerAgrupacion(this.vectorTitulos.get(i));			
			this.getDefaultHeaderRow().getCell(this.vectorTitulos.get(i)).setStyleName(recogerEstiloAgrupacion(gama));
		}
	}
	
	private void generarAgrupaciones()
	{
		int gama = 0;
		
		HeaderCell gama1 = null;
		ArrayList<String> cols = null;
		String [] parameters;
		HeaderRow groupingHeader = this.prependHeaderRow();
		
		for (int g = 1; g<=this.numeroAgrupaciones; g++)
		{
			gama1 = null;
			cols = new ArrayList<String>();
			
			for (int i=0;i<this.vectorTitulos.size();i++)
			{
				String columna = this.vectorTitulos.get(i);
				gama = new Integer(this.vectorAgrupaciones.get(columna));
				if (gama==g)
				{
					cols.add(columna);
				}
			}
			if (isConTotalesColumna())
			{
				cols.add("Total" + recogerNombreTotalAgrupacion(g) );
			}

	        parameters = new String[cols.size()];
	        for(int i = 0; i < parameters.length; i++)
	        {
	            parameters[i] = cols.get(i);
	        }
			if (cols.size()>1)
			{
				gama1 = groupingHeader.join(parameters);
				gama1.setText(recogerNombreAgrupacion(g));
				gama1.setStyleName(recogerEstiloAgrupacion(g));
				
				if (isConTotalesColumna())
				{
					/*
					 * aregamos un boton "enmascarado" en la celda de cabcecera de la agrupacion
					 * con el objetivo de que al hacer click podamos expandir o contraer el contenido
					 * de forma que podamos ver solo el total o todo el detalle de la agrupacion 
					 */
					Button prob = new Button(gama1.getText());
					prob.setWidth("100%");
					prob.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					prob.addClickListener(new ClickListener()
					{
						public void buttonClick(ClickEvent event) {
							
							ocultarColumnas(event.getButton().getCaption());
						}
					});
					
					gama1.setComponent(prob);
				}
			}
		}
		
		if (isMostrarTercerNivel())
		{
			HeaderRow groupingHeaderSuperior = this.prependHeaderRow();
			HeaderCell cabecera = null;
			ArrayList<String> colsCabecera = null;
			String [] parametersCabecera;
			
			for (int cab=0;cab<1;cab++)
			{
				 
				colsCabecera = new ArrayList<String>();
				
				for (int g = 1; g<=this.numeroAgrupaciones; g++)
				{
					for (int i=0;i<this.vectorTitulos.size();i++)
					{
						String columna = this.vectorTitulos.get(i);
						gama = new Integer(this.vectorAgrupaciones.get(columna));
						if (gama==g)
						{
							colsCabecera.add(columna);
						}
					}
					if (isConTotalesColumna())
					{
						colsCabecera.add("Total" + recogerNombreTotalAgrupacion(g) );
					}
				}
				colsCabecera.add("Total CABECERA" );
				
				parametersCabecera = new String[colsCabecera.size()];
		        for(int i = 0; i < parametersCabecera.length; i++)
		        {
		            parametersCabecera[i] = colsCabecera.get(i);
		        }
		        cabecera = groupingHeaderSuperior.join(parametersCabecera);
				cabecera.setText("CABECERA");
				
				if (isConTotalesColumna())
				{
					/*
					 * aregamos un boton "enmascarado" en la celda de cabcecera de la agrupacion
					 * con el objetivo de que al hacer click podamos expandir o contraer el contenido
					 * de forma que podamos ver solo el total o todo el detalle de la agrupacion 
					 */
					Button prob = new Button(cabecera.getText());
					prob.setWidth("100%");
					prob.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					prob.addClickListener(new ClickListener()
					{
						public void buttonClick(ClickEvent event) {
							
							ocultarColumnas(event.getButton().getCaption());
						}
					});
					
					cabecera.setComponent(prob);
				}
			}
		}
		
		if (isConTotalesColumna()) 
		{
			for (int g = 1; g<=this.numeroAgrupaciones; g++)
			{
				vectorTitulos.add("Total" + recogerNombreTotalAgrupacion(g));
			}
		}
		if (isMostrarTercerNivel()) vectorTitulos.add("Total CABECERA");
	}	
	
	private void agregarColumna(String r_nombre, String r_clase)
	{
		switch(r_clase)
		{
			case "String":
			{
				container.addContainerProperty(r_nombre, String.class, null);
				break;
			}
			case "Integer":
			{
				container.addContainerProperty(r_nombre, Integer.class, null);
				break;
			}
			case "Date":
			{
				container.addContainerProperty(r_nombre, Date.class, null);
				break;
			}
			case "Double":
			{
				container.addContainerProperty(r_nombre, Double.class, null);
				break;
			}
		}
		
	}

	private void establecerColumna(Item r_file, String r_nombre, String r_valor, String r_posicion)
	{
		switch(hashColumnasTiposFijas.get(r_posicion))
		{
			case "String":
			{
				r_file.getItemProperty(r_nombre).setValue((String) r_valor);
				break;
			}
			case "Integer":
			{
				r_file.getItemProperty(r_nombre).setValue(new Integer(r_valor));
				break;
			}
			case "Date":
			{
				try 
				{
					if (r_valor!=null)
					{
						r_file.getItemProperty(r_nombre).setValue(RutinasFechas.convertirADate(r_valor));
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				break;
			}
			case "Double":
			{
				r_file.getItemProperty(r_nombre).setValue(new Double(r_valor));
				break;
			}
		}
		
	}

	private boolean isConTotalesColumna() {
		return conTotalesColumna;
	}

	private void setConTotalesColumna(boolean conTotalesColumna) {
		this.conTotalesColumna = conTotalesColumna;
	}
	
	private String recogerNombreAgrupacion(int r_gama)
	{
		Integer gama = new Integer(r_gama);
		String nombre = null;
		nombre = this.vectorNombreAgrupaciones.get(gama);
		return nombre;
	}
	
	private String recogerNombreTotalAgrupacion(int r_gama)
	{
		Integer gama = new Integer(r_gama);
		String nombre = null;
		nombre = this.vectorNombreTotalesAgrupaciones.get(gama);
		return nombre;
	}
	
	private void ocultarColumnas(String r_nombreGama)
	{
		/*
		 * Busco todas las columnas pertenecientes a la gama en la que estoy 
		 * haciendo click, para ocultar/mostrar en funcion de como esten en el
		 * momento de hacer click
		 */
		if (r_nombreGama.contentEquals("CABECERA"))
		{
			for (int i=0;i<vectorTitulos.size();i++)
			{
				if (!vectorTitulos.get(i).contentEquals("Total CABECERA")) getColumn(vectorTitulos.get(i)).setHidden(!getColumn(vectorTitulos.get(i)).isHidden());
			}			
		}
		else
		{
			for (int i=0;i<vectorTitulos.size();i++)
			{
				int gama = obtenerAgrupacion(vectorTitulos.get(i));
				String nombre = recogerNombreAgrupacion(gama);
				if (nombre!=null && nombre.contentEquals(r_nombreGama))
				{
					getColumn(vectorTitulos.get(i)).setHidden(!getColumn(vectorTitulos.get(i)).isHidden());
				}
			}
		}
		this.refreshAllRows();
	}
	
	private boolean isFiltrable(String r_columna)
	{
		if (camposFiltrar==null) return false;
		
		for (int i=0;i<camposFiltrar.size();i++)
		{
			if (camposFiltrar.get(i)!=null && camposFiltrar.get(i).contentEquals(r_columna)) return true;
		}
		return false;
	}
	
	private String obtenerAncho(String r_columna)
	{
		if (!widthFiltros.isEmpty())
		{
			String rdo = widthFiltros.get(r_columna);
			if (rdo!=null && rdo.length()>0) return rdo;
		}
		return "0";
	}

	private void activarFiltro(boolean r_filtro)
	{
		if (r_filtro)
		{
			HeaderRow filterRow = this.appendHeaderRow();
			
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				if (isFiltrable(pid.toString())) 
				{

					HeaderCell cell = filterRow.getCell(pid);
					
					// Have an input field to use for filter
					TextField filterField = new TextField();
					filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
					filterField.setInputPrompt("Filtro");				
					
					// Update filter When the filter input is changed
					filterField.addTextChangeListener(new TextChangeListener() {
						
						@Override
						public void textChange(TextChangeEvent event) {
							// Can't modify filters so need to replace
							container.removeContainerFilters(pid);
							// (Re)create the filter if necessary
							if (!event.getText().isEmpty()) container.addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
							filtros.put(pid.toString(), event.getText());
							app.generarTotalesGrid();
					}});
				
					filterField.setWidth(obtenerAncho(pid.toString()));
					cell.setComponent(filterField);
				}
				
			}
			
		}
	}

	/**
	 * 	METODOS PUBLICOS DE LA CLASE
	 * 
	 */
	public void establecerFiltros(HashMap<String,String> r_filtros)
	{
		if (r_filtros!=null && this.getHeaderRowCount()>2) 
		{
			HeaderRow filterRow = this.getHeaderRow(2);
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				if (r_filtros.get(pid.toString())!=null)
				{
					HeaderCell cell = filterRow.getCell(pid);
					
					container.removeContainerFilters(pid);
					// (Re)create the filter if necessary
					container.addContainerFilter(new SimpleStringFilter(pid,r_filtros.get(pid.toString()), true, false));
					((TextField) cell.getComponent()).setValue(r_filtros.get(pid.toString()));
					
				}
			}
		}		
	}
	
	public HashMap<String,String> recogerFiltros()
	{
		HashMap<String,String> filtros = new HashMap<String,String>();
		
		if (this.getHeaderRowCount()>1)
		{
			HeaderRow filterRow = this.getHeaderRow(1);
			for (Object pid: this.getContainerDataSource().getContainerPropertyIds()) 
			{
				if (isFiltrable(pid.toString()))
				{
					HeaderCell cell = filterRow.getCell(pid);
					if (cell.getComponent() instanceof TextField)
					{
						if (((TextField) cell.getComponent()).getValue()!=null && ((TextField) cell.getComponent()).getValue().length()>0) filtros.put(pid.toString(), ((TextField) cell.getComponent()).getValue());
					}
				}
				// (Re)create the filter if necessary
						
			}
		}
		return filtros;
	}
	
	public int obtenerAgrupacion(String r_columna)
	{
		int gamaObtenida=0;
		
		for (int i=0;i<this.vectorTitulos.size();i++)
		{
			String columna = this.vectorTitulos.get(i);
			if (this.vectorAgrupaciones.get(columna)!=null)
			{
				if (columna.contentEquals(r_columna)) gamaObtenida = new Integer(this.vectorAgrupaciones.get(columna));
			}
			
		}
		return gamaObtenida;
	}
	
	public String recogerEstiloAgrupacion(int r_gama)
	{
		if (r_gama==0)
		{
			return "cell-cyan";
		}
		else if (r_gama==1)
		{
			return "cell-orange";
		}
		else if (r_gama==2)
		{
			return "cell-warning";
		}
		else if (r_gama==3)
		{
			return "cell-magenta";
		}
		else if (r_gama==4)
		{
			return "cell-gray";
		}
		else if (r_gama==5)
		{
			return "cell-dgray";
		}
		else if (r_gama==6)
		{
			return "cell-yellow";
		}
		return "Rcell-pie";
	}
	
	public boolean isConFiltro() {
		return conFiltro;
	}

	public void setConFiltro(boolean r_conFiltro) 
	{
		this.conFiltro= r_conFiltro;
		this.activarFiltro(r_conFiltro);
	}
	
	public void setSeleccion(SelectionMode r_tipo)
	{
		setSelectionMode(r_tipo);
	}
	
    public void asignarTitulo(String r_titulo)
    {
    	setCaption("<h3><b><center>" + r_titulo + "</center></b></h3>");
    }
    
    public void ejecutarAccion(String  r_accion, String r_ruta)
    {
    	
    }
    
	public boolean isHayGrid() {
		return hayGrid;
	}

	public void setHayGrid(boolean hayGrid) {
		this.hayGrid = hayGrid;
	}

	public boolean isMostrarTercerNivel() {
		return mostrarTercerNivel;
	}

	public void setMostrarTercerNivel(boolean mostrarTercerNivel) {
		this.mostrarTercerNivel = mostrarTercerNivel;
	}

	private boolean isConObservaciones() {
		return conObservaciones;
	}

	private void setConObservaciones(boolean conObservaciones) {
		this.conObservaciones = conObservaciones;
	}
	
}
