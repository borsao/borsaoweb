package borsao.e_borsao.ClasesPropias;

import java.io.IOException;
import java.util.Date;

import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Notification;

public class Notificaciones  
{
	private static Notificaciones instance;
	private Notification notif = null;	
	private String titulo = null;
	private String color= null;
	private Boolean html = false;
	
	public Notificaciones()
	{
		this.titulo = "CRM Borsao";
		this.color="blue";
		this.html = true;		
	}
		
	public static Notificaciones getInstance() 
	{
		if (instance == null) 
		{
			instance = new Notificaciones();			
		}
		return instance;
	}

	public void mensajeInformativo(String mensaje)
	{	
		boolean logs;
		try 
		{
			logs = LecturaProperties.logActivo();
			this.notif= new Notification("",estiloTexto(mensaje), Notification.Type.HUMANIZED_MESSAGE, this.html);
			this.notif.setStyleName("mystyleInfo");
			this.notif.setIcon(new ThemeResource("img/info.png"));
			if (logs && LecturaProperties.logs==0) System.out.println("mensaje : " + new Date() + " : " + mensaje ) ;
			this.mostrar();
			
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void mensajeAdvertencia(String mensaje)
	{
		boolean logs;
		try 
		{
			logs = LecturaProperties.logActivo();
			this.notif= new Notification("",estiloTexto(mensaje),Notification.Type.WARNING_MESSAGE, this.html);
			this.notif.setStyleName("mystyleWarning");
			this.notif.setIcon(new ThemeResource("img/warning.png"));
			if (logs && LecturaProperties.logs<=1) System.out.println("mensaje warning: " + new Date() + " : " + mensaje ) ;
			this.mostrar();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void mensajeError(String mensaje)
	{
		boolean logs;
		try 
		{
			logs = LecturaProperties.logActivo();
			this.notif= new Notification("",estiloTexto(mensaje),Notification.Type.ERROR_MESSAGE, this.html);
			this.notif.setStyleName("mystyleError");
			this.notif.setIcon(new ThemeResource("img/error.png"));
			if (logs && LecturaProperties.logs<=3) System.out.println("mensaje error: " + new Date() + " : " + mensaje ) ;
			this.mostrar();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void mensajeSeguimiento(String mensaje)
	{
		boolean logs;
		try 
		{
			logs = LecturaProperties.logActivo();
			if (logs && LecturaProperties.logs<=2) System.out.println("mensaje debug: " + new Date() + " : " + mensaje ) ;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	public void mensajeAvisoCalendario(String mensaje)
	{
		this.notif= new Notification("",estiloTexto(mensaje),Notification.Type.ERROR_MESSAGE, this.html);
		this.notif.setStyleName("mystyleInfo");
		this.notif.setIcon(new ThemeResource("img/info.png"));
		this.mostrar();
	}
	
	private String estiloTexto(String r_mensaje)
	{
		String textoHtml = "";		
		textoHtml="<font color='" + this.color + "'>" +  this.titulo + "</font><br/>" + r_mensaje;	
		return textoHtml;
	}
	
	private void mostrar()
	{		
		this.notif.show(Page.getCurrent());
	}
}