package borsao.e_borsao.ClasesPropias;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.calendar.event.CalendarEvent;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaCopiaEventosAgenda extends Window 
{
	private String propietario = null;
	private List<CalendarEvent> eventosCopiar = null;
	private Agenda ag = null;
	
    private Button opcCopiar = null;
    private Button opcSalir = null;
    private CheckBox trasponer = null;
    private CheckBox tripleTurno = null;
    private DateField fechaInicio = null;
    
	private HorizontalLayout topLayout = null;
	private VerticalLayout principal = null;
	private HorizontalLayout panelEvento = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
	public PantallaCopiaEventosAgenda(String r_propietario, Agenda r_agenda, List<CalendarEvent> r_evs)
    {
    	this.eventosCopiar=r_evs;
    	this.propietario=r_propietario;
    	this.ag = r_agenda;
    	
		this.setCaption("Copia Eventos");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("900px");
		this.setHeight("650px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		this.cargarListeners();
		this.habilitarBotones();
    }

    public void cargarPantalla() 
    {
		principal = new VerticalLayout();
		principal.setMargin(true);
		principal.setSpacing(true);
		
		this.topLayout = new HorizontalLayout();
    	this.topLayout.setSpacing(true);
    	
	    	this.opcCopiar= new Button("Copiar");
	    	this.opcCopiar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	this.opcCopiar.setIcon(FontAwesome.COPY);
	    	this.opcCopiar.setEnabled(false);

	    	this.opcSalir= new Button("Salir");
	    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
	
    	this.topLayout.addComponent(this.opcCopiar);
    	this.topLayout.addComponent(this.opcSalir);
    	
	    	this.panelEvento = new HorizontalLayout();
	    	this.panelEvento.setSpacing(true);

	    		this.fechaInicio = new DateField("Fecha Inicio Nuevos Eventos");
		    	this.fechaInicio.setEnabled(true);

		    	this.trasponer = new CheckBox("Intercambiar horarios?");
		    	this.trasponer.setValue(this.propietario.equals("Turnos"));
		    	this.trasponer.setEnabled(this.propietario.equals("Turnos"));

		    	this.tripleTurno = new CheckBox("Semana de tres turnos?");
		    	this.tripleTurno.setValue(false);
		    	this.tripleTurno.setEnabled(this.propietario.equals("Turnos"));

			this.panelEvento.addComponent(this.fechaInicio);
			this.panelEvento.addComponent(this.trasponer);
			this.panelEvento.addComponent(this.tripleTurno);
			
    	principal.addComponent(this.topLayout);
    	principal.addComponent(this.panelEvento);
    	
    }

    private void cargarListeners()
    {

    	this.opcCopiar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			copiarDatos();
    		}
    	});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			close();
    		}
    	});
    	
    }

    private void guardarNuevoEvento(AgendaEvento r_nuevoEvento, Integer r_diasDiferencia) 
	{
		Integer diferenciaEnHoras = r_diasDiferencia;

		if (this.fechaInicio.getValue()!=null)
		{
//			System.out.println("Fecha inicio evento original: " + r_nuevoEvento.getStart().toString());
//			System.out.println("Fecha fin evento original  " + r_nuevoEvento.getEnd().toString());
//
//			System.out.println("Fecha inicio nueva: " + this.fechaInicio.getValue().toString());
			
			if (this.fechaInicio.getValue().after(r_nuevoEvento.getStart()))
			{
				
				GregorianCalendar startDate = new GregorianCalendar();
				startDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				startDate.setTime(r_nuevoEvento.getStart());
				startDate.add(java.util.Calendar.HOUR, diferenciaEnHoras);
				
				GregorianCalendar endDate = new GregorianCalendar();
				endDate.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				endDate.setTime(r_nuevoEvento.getEnd());
				endDate.add(java.util.Calendar.HOUR, diferenciaEnHoras);

//				System.out.println("Fecha inicio nuevo evento sumando horas dif: " + startDate.getTime().toString());
//				System.out.println("Fecha fin nuevo evento sumando horas dif: " + endDate.getTime().toString());

				if (this.trasponer.getValue() && !r_nuevoEvento.getArea().contentEquals("BIB"))
				{
//					System.out.println("Fecha: " + startDate.getTime().toString() + "Hora dia " + startDate.get(Calendar.HOUR_OF_DAY));
					
					if (this.tripleTurno.getValue())
					{
						if (startDate.get(Calendar.HOUR_OF_DAY)>=22 && startDate.get(Calendar.HOUR_OF_DAY)<6)
						{
							startDate.add(java.util.Calendar.HOUR, -16);
							endDate.add(java.util.Calendar.HOUR, -16);
						}
						else if (startDate.get(Calendar.HOUR_OF_DAY)>=6 && startDate.get(Calendar.HOUR_OF_DAY)<14)
						{
							startDate.add(java.util.Calendar.HOUR, 8);
							endDate.add(java.util.Calendar.HOUR, 8);
						}	
						else if (startDate.get(Calendar.HOUR_OF_DAY)>=14 && startDate.get(Calendar.HOUR_OF_DAY)<22)
						{
							startDate.add(java.util.Calendar.HOUR, 8);
							endDate.add(java.util.Calendar.HOUR, 8);
						}	
					}
					else
					{
						if (startDate.get(Calendar.HOUR_OF_DAY)>=6 && startDate.get(Calendar.HOUR_OF_DAY)<14)
						{
							startDate.add(java.util.Calendar.HOUR, 8);
							endDate.add(java.util.Calendar.HOUR, 8);
						}	
						else if (startDate.get(Calendar.HOUR_OF_DAY)>=14 && startDate.get(Calendar.HOUR_OF_DAY)<22)
						{
							startDate.add(java.util.Calendar.HOUR, -8);
							endDate.add(java.util.Calendar.HOUR, -8);
						}
						
//						System.out.println("Fecha inicio trasponiendo: " + startDate.getTime().toString());
//						System.out.println("Fecha fin trasponiendo: " + endDate.getTime().toString());

					}
				}
				

				ag.añadirEvento(null, this.propietario, r_nuevoEvento.getCaption(), r_nuevoEvento.getDescription(), startDate.getTime(), endDate.getTime(), r_nuevoEvento.isAllDay(), r_nuevoEvento.getAviso(), r_nuevoEvento.isAviso(), r_nuevoEvento.getValorAviso(), r_nuevoEvento.getArea(), r_nuevoEvento.getAlmacen(), false, true, r_nuevoEvento.getTipo(), r_nuevoEvento.getEjercicioContable());
				
			}
			else
			{
				Notificaciones.getInstance().mensajeError("La fecha de Inicio debe ser posterior a la del primer evento a copiar");
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Has de seleccionar una fecha de inicio");
		}
	}

	private void copiarDatos()
	{
		Integer diferenciaEnHoras = null;
		if (this.eventosCopiar!=null)
		{			
			AgendaEvento primerEv = (AgendaEvento) this.eventosCopiar.get(0);
			
			diferenciaEnHoras = RutinasFechas.diferenciaEnDias2(this.fechaInicio.getValue(), primerEv.getStart());
			diferenciaEnHoras = (diferenciaEnHoras + 1)*24;
//			System.out.println("Diferencia en horas: " + (diferenciaEnHoras  +1 ) * 24);
			
			for (int i=0; i< this.eventosCopiar.size(); i++)
			{
				this.guardarNuevoEvento((AgendaEvento) this.eventosCopiar.get(i), diferenciaEnHoras);
			}
		}
		close();
	}
	
	public void destructor()
	{
	}

	private void habilitarBotones()
	{
		if (eBorsao.get().accessControl.getNombre().contains("Claveria"))
		{
			this.opcCopiar.setEnabled(true);
 		}
	}

	@Override
	public void close()
	{
		super.close();
	}
}

