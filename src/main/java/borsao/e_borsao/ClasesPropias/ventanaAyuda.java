package borsao.e_borsao.ClasesPropias;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

public class ventanaAyuda extends Window
{
	
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private String valorSeleccionado = null;
	private String valorSeleccionado2 = null;
	private IndexedContainer container =null;
	
	public ventanaAyuda(TextFieldAyuda r_App, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(new TextChangeListener() {
				
				@Override
				public void textChange(TextChangeEvent event) {
					// Can't modify filters so need to replace
					container.removeContainerFilters(pid);
			   
					// (Re)create the filter if necessary
					if (!event.getText().isEmpty()) container.addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
			}});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				r_App.botonAceptarAyuda(valorSeleccionado);
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public ventanaAyuda(TextField r_App, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(change -> {
			   // Can't modify filters so need to replace
			   container.removeContainerFilters(pid);
			   
			   // (Re)create the filter if necessary
			   if (! change.getText().isEmpty())
			       container.addContainerFilter(
			           new SimpleStringFilter(pid,
			               change.getText(), true, false));
			});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				r_App.setValue(valorSeleccionado);
				close();
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public ventanaAyuda(TextField r_App, TextArea r_App2, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(change -> {
			   // Can't modify filters so need to replace
			   container.removeContainerFilters(pid);
			   
			   // (Re)create the filter if necessary
			   if (! change.getText().isEmpty())
			       container.addContainerFilter(
			           new SimpleStringFilter(pid,
			               change.getText(), true, false));
			});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				r_App.setValue(valorSeleccionado);
				r_App2.setValue(valorSeleccionado2);
				close();
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public ventanaAyuda(TextField r_App, TextField r_App2, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(change -> {
			   // Can't modify filters so need to replace
			   container.removeContainerFilters(pid);
			   
			   // (Re)create the filter if necessary
			   if (! change.getText().isEmpty())
			       container.addContainerFilter(
			           new SimpleStringFilter(pid,
			               change.getText(), true, false));
			});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				if (r_App!=null) r_App.setValue(valorSeleccionado);
				if (r_App2!=null) r_App2.setValue(valorSeleccionado2);
				close();
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public ventanaAyuda(TextField r_App, ComboBox r_App2, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(change -> {
			   // Can't modify filters so need to replace
			   container.removeContainerFilters(pid);
			   
			   // (Re)create the filter if necessary
			   if (! change.getText().isEmpty())
			       container.addContainerFilter(
			           new SimpleStringFilter(pid,
			               change.getText(), true, false));
			});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				r_App.setValue(valorSeleccionado);
				r_App2.setValue(valorSeleccionado2);
				close();
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public ventanaAyuda(TextField r_App, Label r_App2, ArrayList<MapeoAyudas> r_vector, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		
		this.llenarRegistros(r_vector);
		
		this.gridDatos.setWidth("750px");
		this.gridDatos.setHeight("350px");
		
		HeaderRow filterRow = gridDatos.appendHeaderRow();

		for (Object pid: gridDatos.getContainerDataSource()
                .getContainerPropertyIds()) {
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");
			
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(change -> {
			   // Can't modify filters so need to replace
			   container.removeContainerFilters(pid);
			   
			   // (Re)create the filter if necessary
			   if (! change.getText().isEmpty())
			       container.addContainerFilter(
			           new SimpleStringFilter(pid,
			               change.getText(), true, false));
			});
			cell.setComponent(filterField);
			}

        
		btnBotonCVentana = new Button("Cancelar");
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				if (r_App!=null) r_App.setValue(valorSeleccionado);
				if (r_App2!=null) r_App2.setValue(valorSeleccionado2);
				close();
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	private void llenarRegistros(ArrayList<MapeoAyudas> r_vector)
	{
		Iterator iterator = null;
		MapeoAyudas mapeoAyuda = null;
		
		if (r_vector!=null && !r_vector.isEmpty())
		{
			iterator = r_vector.iterator();
			
			container = new IndexedContainer();
	        container.addContainerProperty("Codigo", String.class, null);
	        container.addContainerProperty("Descripcion", String.class, null);
	        
			while (iterator.hasNext())
			{
				Object newItemId = container.addItem();
				Item file = container.getItem(newItemId);
				
				mapeoAyuda = (MapeoAyudas) iterator.next();
				
				file.getItemProperty("Codigo").setValue(RutinasCadenas.conversion(mapeoAyuda.getCodigo()));
	    		file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
	    		
	    		
			}
	
			this.gridDatos= new Grid(container);
			this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
	
			this.gridDatos.addItemClickListener(new ItemClickListener() {
				
				@Override
				public void itemClick(ItemClickEvent event) {
					valorSeleccionado =  gridDatos.getContainerDataSource().getItem(event.getItemId()).getItemProperty("Codigo").getValue().toString();
					valorSeleccionado2 =  gridDatos.getContainerDataSource().getItem(event.getItemId()).getItemProperty("Descripcion").getValue().toString();
				}
			});
		}
	}
	
//	private String seleccionarCampoFila(Object r_filaGridSeleccionada)
//	{
//		String valorSeleccionado = null;
//		return valorSeleccionado;
//	}
}