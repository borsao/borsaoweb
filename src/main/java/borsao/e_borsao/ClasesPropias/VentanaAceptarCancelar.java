package borsao.e_borsao.ClasesPropias;

import com.vaadin.navigator.View;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view.ArticulosNuevosView;

public class VentanaAceptarCancelar extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private String accionAceptar = null;
//	private String mensajeAccionAceptar = null;
	private String accionCancelar = null;
	public Label lblMensaje = null;
	private Image imgIcono = null;
	private GridViewRefresh view = null;
	private View viewApp = null;
	private Ventana win = null;
	private HorizontalLayout botonera = null;
	
	public VentanaAceptarCancelar(GridViewRefresh r_app, String r_mensaje, String r_aceptar, String r_cancelar, String r_accion_aceptar, String r_accion_cancelar)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		this.view = r_app;
		this.accionAceptar=r_accion_aceptar;
		this.accionCancelar=r_accion_cancelar;
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.imgIcono = new Image();
				this.imgIcono.setIcon(new ThemeResource("img/warning.png"));
				
				this.lblMensaje =new Label(r_mensaje);
				this.lblMensaje.setWidth("300px");
				
				fila1.addComponent(this.imgIcono);
				fila1.addComponent(this.lblMensaje);
				fila1.setComponentAlignment(this.lblMensaje, Alignment.MIDDLE_LEFT);

		controles.addComponent(fila1);
		
		botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Mensaje Advertencia");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("450px");
		this.setHeight("250px");
		this.center();
		
		btnBotonCVentana = new Button(r_cancelar);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button(r_aceptar);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}
	
	
	public VentanaAceptarCancelar(View r_app, String r_mensaje, String r_aceptar, String r_cancelar, String r_accion_aceptar, String r_accion_cancelar)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		this.viewApp = r_app;
		this.accionAceptar=r_accion_aceptar;
		this.accionCancelar=r_accion_cancelar;
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.imgIcono = new Image();
				this.imgIcono.setIcon(new ThemeResource("img/warning.png"));
				
				this.lblMensaje =new Label(r_mensaje);
				this.lblMensaje.setWidth("300px");
				
				fila1.addComponent(this.imgIcono);
				fila1.addComponent(this.lblMensaje);
				fila1.setComponentAlignment(this.lblMensaje, Alignment.MIDDLE_LEFT);

		controles.addComponent(fila1);
		
		botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Mensaje Advertencia");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("450px");
		this.setHeight("250px");
		this.center();
		
		btnBotonCVentana = new Button(r_cancelar);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button(r_aceptar);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}
	
	public VentanaAceptarCancelar(Ventana r_app, String r_mensaje, String r_aceptar, String r_cancelar, String r_accion_aceptar, String r_accion_cancelar)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		this.win = r_app;
		
		this.accionAceptar=r_accion_aceptar;
		this.accionCancelar=r_accion_cancelar;
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.imgIcono = new Image();
				this.imgIcono.setIcon(new ThemeResource("img/warning.png"));
				
				this.lblMensaje =new Label(r_mensaje);
				this.lblMensaje.setWidth("300px");
				
				fila1.addComponent(this.imgIcono);
				fila1.addComponent(this.lblMensaje);
				fila1.setComponentAlignment(this.lblMensaje, Alignment.MIDDLE_LEFT);

		controles.addComponent(fila1);
		
		botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Mensaje Advertencia");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("450px");
		this.setHeight("250px");
		this.center();
		
		btnBotonCVentana = new Button(r_cancelar);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button(r_aceptar);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}
	
//	public VentanaAceptarCancelar(GridViewRefresh r_app, String r_mensaje, String r_aceptar, String r_cancelar, String r_accion_aceptar, String r_accion_cancelar, String r_mensaje_accion_aceptar)
//	{
//		VerticalLayout principal = new VerticalLayout();
//		principal.setSizeFull();
//		
//		this.view = r_app;
//		this.accionAceptar=r_accion_aceptar;
//		this.accionCancelar=r_accion_cancelar;				
//		this.mensajeAccionAceptar = r_mensaje_accion_aceptar; 
//				
//		VerticalLayout controles = new VerticalLayout();
//		controles.setMargin(true);
//		controles.setSpacing(true);
//				
//			HorizontalLayout fila1 = new HorizontalLayout();
//			fila1.setWidthUndefined();
//			fila1.setSpacing(true);
//				
//				this.imgIcono = new Image();
//				this.imgIcono.setIcon(new ThemeResource("img/warning.png"));
//				
//				this.lblMensaje =new Label(r_mensaje);
//				this.lblMensaje.setWidth("300px");
//				
//				fila1.addComponent(this.imgIcono);
//				fila1.addComponent(this.lblMensaje);
//				fila1.setComponentAlignment(this.lblMensaje, Alignment.MIDDLE_LEFT);
//
//		controles.addComponent(fila1);
//		
//		botonera = new HorizontalLayout();
//		botonera.setMargin(true);
//		botonera.setWidthUndefined();
//		botonera.setHeight("100px");
//		
//		this.setCaption("Mensaje Advertencia");
//		this.setModal(true);
//		this.setClosable(true);
//		this.setResizable(true);
//		this.setWidth("450px");
//		this.setHeight("250px");
//		this.center();
//		
//		btnBotonCVentana = new Button(r_cancelar);
//		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
//		btnBotonAVentana = new Button(r_aceptar);
//		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
//		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
//		
//		botonera.addComponent(btnBotonAVentana);
//		botonera.addComponent(btnBotonCVentana);		
//		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
//		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
//		
//		principal.addComponent(controles);
//		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
//		this.setContent(principal);
//
//		this.cargarListeners();
//	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
//				if (mensajeAccionAceptar!=null)
//				{
//					lblMensaje.setValue(mensajeAccionAceptar);
//					lblMensaje.markAsDirty();
//				}
				if (view !=null) view.aceptarProceso(accionAceptar);
				if (win !=null) win.aceptarProceso(accionAceptar);
				if (viewApp !=null && viewApp instanceof ArticulosNuevosView) ((ArticulosNuevosView) viewApp).aceptarProceso(accionAceptar);
				close();
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				if (view !=null) view.cancelarProceso(accionCancelar);
				if (win !=null) win.cancelarProceso(accionCancelar);
				if (viewApp !=null && viewApp instanceof ArticulosNuevosView) ((ArticulosNuevosView) viewApp).cancelarProceso(accionCancelar);
				close();
			}
		});	
		
	}
}