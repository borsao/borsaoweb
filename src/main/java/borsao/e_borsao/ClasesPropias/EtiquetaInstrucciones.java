package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.Label;

public class EtiquetaInstrucciones extends Label
{
	public EtiquetaInstrucciones()
	{
		this.setStyleName("v-etiqueta");
	}
	public EtiquetaInstrucciones(String r_caption)
	{
		this.setCaption(r_caption);
		this.setStyleName("v-etiqueta");
	}
}