package borsao.e_borsao.ClasesPropias;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.jcraft.jsch.Session;

public class connectionManager
{
	private Connection con = null;
	private Connection conG = null;
	private Connection conGC = null;
	private Connection conGB = null;
	private Connection conGT = null;
	private Connection conGP = null;
	private Connection conB = null;
	private Connection conT = null;
	private Session session = null;
	private static connectionManager instance;
	
	public connectionManager()
	{
		this.cerrarConexion(this.con);
		this.cerrarConexion(this.conB);
		this.cerrarConexion(this.conG);
		this.cerrarConexion(this.conGB);
		this.cerrarConexion(this.conGT);
		this.cerrarConexion(this.conGP);
		this.cerrarConexion(this.conGC);
		this.cerrarConexion(this.conT);
	}
	
	public static connectionManager getInstance() 
	{
		if (instance == null) 
		{
			instance = new connectionManager();			
		}
		return instance;
	}
	public Connection establecerConexion() throws SQLException
	{
		String dbUrl=LecturaProperties.url;
		String user=LecturaProperties.user;
		String pass=LecturaProperties.pass;
		
		if (LecturaProperties.entorno.equals("produccion")) pass = "compaq123" ;
		if (LecturaProperties.entorno.equals("pruebas")) pass = "Javier-2017!!";
		if (LecturaProperties.entorno.equals("casa")) pass = "Codys-2011!!";
		if (LecturaProperties.entorno.equals("pre-produccion")) pass = "Javier-2017!!";
		
		try
		{
			if (this.con==null || this.con.isClosed())
			{
				Class.forName("com.mysql.jdbc.Driver");
				this.con = DriverManager.getConnection(dbUrl, user, pass);				 
			}
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		return this.con;
	}

//	public Connection establecerConexionTienda() throws SQLException
//	{
//		/*
//		 * parametros ssh
//		 */
//		
//		String host=LecturaProperties.urlSSH;
//		final int lport=new Integer(LecturaProperties.portSSH).intValue();
//		String user=LecturaProperties.userSSH;
//		String password =LecturaProperties.passSSH;
//
////        final String host = "209.97.179.2";
////        final int lport = 27514;
////        final String user = "luisg8a";
////        final String password = "FW8mtZhNDHZ27lF0argYK1vL";
//
//        /*
//         * parametros mysql
//         */
//		String rhost=LecturaProperties.hostDBT;
//		final int rport=new Integer(LecturaProperties.portDBT).intValue();
//		String dbuserName =LecturaProperties.userDBT;
//		String dbpassword =LecturaProperties.passDBT;
//		String driverName =LecturaProperties.driverT;
//
//		//        final String rhost = "127.0.0.1";
//		//        final int rport = 3306;
//		//        final String dbuserName = "rein-luisg8a";
//		//        final String dbpassword = "1GQt6rIoZkBdU2IOC6SJhvX7";
//		//        final String dburl = "jdbc:mysql://" + rhost + ":" + rport  + "/information_schema?useUnicode=true&characterEncoding=UTF-8";
//		//        final String driverName = "com.mysql.jdbc.Driver";
//         
//         try 
//         {
//        	 if (this.conT==null || this.conT.isClosed())
//	         {
//	             //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
//	             final java.util.Properties config = new java.util.Properties();
//	             config.put("StrictHostKeyChecking", "no");
//	             final JSch jsch = new JSch();
//	             session = jsch.getSession(user, host, 22);
//	//             jsch.addIdentity("c\\pemfile.pem");
//	             session.setPassword(password);
//	             session.setConfig(config);
//	             session.connect();
//	             System.out.println("Connected");
//	             final int assinged_port = session.setPortForwardingL(lport, rhost, rport);
//	             System.out.println("localhost:" + assinged_port + " -> " + rhost + ":" + rport);
//	             System.out.println("Port Forwarded");
//	
//	             final String dburl = "jdbc:mysql://localhost:" + assinged_port + "/rout-star-bicycle-tienda?useUnicode=true&characterEncoding=UTF-8";
//	
//	             //mysql database connectivity
//	             Class.forName(driverName).newInstance();
//	             conT = DriverManager.getConnection(dburl, dbuserName, dbpassword);
//	
//	             System.out.println("Database connection established");
//	             System.out.println("DONE");
//        	 }
//
//         }
//         catch (final Exception e) 
//         {
//             e.printStackTrace();
//         } 
////         finally 
////         {
////             if (conT != null && !conT.isClosed()) {
////                 System.out.println("Closing Database Connection");
////                 conT.close();
////             }
////             if (session != null && session.isConnected()) {
////                 System.out.println("Closing SSH Connection");
////                 session.disconnect();
////             }
////         }
//		return conT;
//	}
	


	public Connection establecerConexionTienda() throws SQLException
	{
		/*
		 * parametros ssh
		 */
		
//		String host=LecturaProperties.urlSSH;
//		final int lport=new Integer(LecturaProperties.portSSH).intValue();
//		String user=LecturaProperties.userSSH;
//		String password =LecturaProperties.passSSH;

//        final String host = "209.97.179.2";
//        final int lport = 27514;
//        final String user = "luisg8a";
//        final String password = "FW8mtZhNDHZ27lF0argYK1vL";

        /*
         * parametros mysql
         */
		String rhost=LecturaProperties.hostDBT;
		final int rport=new Integer(LecturaProperties.portDBT).intValue();
		String dbuserName =LecturaProperties.userDBT;
		String dbpassword =LecturaProperties.passDBT;
		String driverName =LecturaProperties.driverT;
		String bbddName =LecturaProperties.nameDBT;

		//        final String rhost = "127.0.0.1";
		//        final int rport = 3306;
		//        final String dbuserName = "rein-luisg8a";
		//        final String dbpassword = "1GQt6rIoZkBdU2IOC6SJhvX7";
		//        final String dburl = "jdbc:mysql://" + rhost + ":" + rport  + "/information_schema?useUnicode=true&characterEncoding=UTF-8";
		//        final String driverName = "com.mysql.jdbc.Driver";
         
         try 
         {
        	 if (this.conT==null || this.conT.isClosed())
	         {
	             //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
//	             final java.util.Properties config = new java.util.Properties();
//	             config.put("StrictHostKeyChecking", "no");
//	             final JSch jsch = new JSch();
//	             session = jsch.getSession(user, host, 22);
//	//             jsch.addIdentity("c\\pemfile.pem");
//	             session.setPassword(password);
//	             session.setConfig(config);
//	             session.connect();
//	             System.out.println("Connected");
//	             final int assinged_port = session.setPortForwardingL(lport, rhost, rport);
//	             System.out.println("localhost:" + assinged_port + " -> " + rhost + ":" + rport);
//	             System.out.println("Port Forwarded");
//	
	             final String dburl = "jdbc:mysql://" + rhost + ":" + rport + "/" + bbddName + "?useUnicode=true&characterEncoding=UTF-8";
	
	             //mysql database connectivity
	             Class.forName(driverName).newInstance();
	             conT = DriverManager.getConnection(dburl, dbuserName, dbpassword);
	
	             System.out.println("Database connection established");
	             System.out.println("DONE");
        	 }

         }
         catch (final Exception e) 
         {
             e.printStackTrace();
         } 
//         finally 
//         {
//             if (conT != null && !conT.isClosed()) {
//                 System.out.println("Closing Database Connection");
//                 conT.close();
//             }
//             if (session != null && session.isConnected()) {
//                 System.out.println("Closing SSH Connection");
//                 session.disconnect();
//             }
//         }
		return conT;
	}

	public Connection establecerConexionAccess(String r_bbdd) throws SQLException
	{
		// set this to a MS Access DB you have on your machine
		try
		{
			con = DriverManager.getConnection("jdbc:ucanaccess:" + LecturaProperties.rutaLecturaCV +"/" + r_bbdd, "", "");
			
//			Connection conn=DriverManager.getConnection(
//			        "jdbc:ucanaccess://V:/Javi/Planta/Calidad/calidad/statistics/20221222/NoName_20221222_075142_0102227 VInA BORGIA TINTO contra vieja tirilla.mdb");
//			java.sql.Statement s = conn.createStatement();
//			ResultSet rs = s.executeQuery("SELECT [TotalKO] FROM [GeneralCounters]");
//			while (rs.next()) {
//			    System.out.println(rs.getString(1));
//			    
//	        }
//            s.close();
//            rs.close();
//            conn.close();
        }
        catch (Exception err) 
		{
            System.out.println("ERROR: " + err);
        }
		
		return con;
	}
	
	public Connection establecerConexionBiblia() throws SQLException
	{
		String dbUrl=LecturaProperties.urlBiblia;
		String user=LecturaProperties.userBiblia;
		String pass=LecturaProperties.passBiblia;
		
		if (LecturaProperties.entorno.equals("casa")) pass = "Codys-2011!!";
		else if (!LecturaProperties.entorno.equals("pruebas")) pass = "compaq123"; 
		else pass = "Javier-2017!!";
		
		try
		{
			if (this.conB==null || this.conB.isClosed())
			{
				Class.forName("com.mysql.jdbc.Driver");
				this.conB = DriverManager.getConnection(dbUrl, user, pass);
			}
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		return this.conB;
	}

	public Connection establecerConexionGestion() throws SQLException
	{
		String urlG=LecturaProperties.urlG;
		String passG=LecturaProperties.passG;
		String userG=LecturaProperties.userG;
		String dbPath=LecturaProperties.dbPath;
		String dbLib=LecturaProperties.dbLib;
		String driver=LecturaProperties.driver;
		
		passG = "Green2012!!";
		
		this.conG=null;
		
		if (this.conG==null || this.conG.isClosed())
		{
			try {
//				System.out.println("Este es el driver cargado; " + driver);
				Class.forName(driver);				
				this.conG = DriverManager.getConnection(urlG + ";DBPATH=" + dbPath + ";DBLIB=" + dbLib, userG, passG);
			} 
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		return this.conG;
	}

	public Connection establecerConexionInd() throws SQLException
	{
		Connection con = null;
		String dbUrl=LecturaProperties.url;
		String user=LecturaProperties.user;
		String pass=LecturaProperties.pass;
		
		if (LecturaProperties.entorno.equals("produccion")) pass = "compaq123" ;
		if (LecturaProperties.entorno.equals("pruebas")) pass = "Javier-2017!!";
		if (LecturaProperties.entorno.equals("casa")) pass = "Codys-2011!!";
		if (LecturaProperties.entorno.equals("pre-produccion")) pass = "Javier-2017!!";
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(dbUrl, user, pass);				 
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		return con;
	}

	public Connection establecerConexionGestionInd() throws SQLException
	{
		Connection con = null;
		String urlG=LecturaProperties.urlG;
		String passG=LecturaProperties.passG;
		String userG=LecturaProperties.userG;
		String dbPath=LecturaProperties.dbPath;
		String dbLib=LecturaProperties.dbLib;
		String driver=LecturaProperties.driver;
		
		passG = "Green2012!!";
		
		
		try 
		{
			Class.forName(driver);				
			con = DriverManager.getConnection(urlG + ";DBPATH=" + dbPath + ";DBLIB=" + dbLib, userG, passG);
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
			
		return con;
	}

	
	public Connection establecerConexionGestionBorja() throws SQLException
	{
		
//		String url="jdbc:ctsql://81.45.138.206:10500/centro";
		String url="jdbc:ctsql://2.136.212.188:10500/centro";

		String urlG=url;
		String passG=LecturaProperties.passG;
		String userG=LecturaProperties.userG;
		String dbPath=LecturaProperties.dbPath;
		String dbLib=LecturaProperties.dbLib;
		String driver=LecturaProperties.driver;
		
		passG = "Green2012!!";
		
		this.conGB=null;
		
		if (this.conGB==null || this.conGB.isClosed())
		{
			try {
//				System.out.println("Este es el driver cargado; " + driver);
				Class.forName(driver);				
				this.conGB = DriverManager.getConnection(url + ";DBPATH=" + dbPath + ";DBLIB=" + dbLib, userG, passG);
			} 
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		return this.conGB;
	}
	
	public Connection establecerConexionGestionTabuenca() throws SQLException
	{
		
		String url="jdbc:ctsql://192.168.34.20:10500/centro";

		String urlG=url;
		String passG=LecturaProperties.passG;
		String userG=LecturaProperties.userG;
		String dbPath="/greensys/eumaster/usr";
		String dbLib="/greensys/eumaster";
		String driver=LecturaProperties.driver;
		
		passG = "Green";
		
		this.conGT=null;
		
		if (this.conGT==null || this.conGT.isClosed())
		{
			try {
//				System.out.println("Este es el driver cargado; " + driver);
				Class.forName(driver);				
				this.conGT = DriverManager.getConnection(url + ";DBPATH=" + dbPath + ";DBLIB=" + dbLib, userG, passG);
			} 
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		return this.conGT;
	}
	
	
	public Connection establecerConexionGestionPozuelo() throws SQLException
	{
		
		String url="jdbc:ctsql://192.168.34.10:10500/centro";
		String dbPath="/greensys/eumaster/usr";
		String dbLib="/greensys/eumaster";
		String urlG=url;
		String passG=LecturaProperties.passG;
		String userG=LecturaProperties.userG;
		String driver=LecturaProperties.driver;
		
		passG = "Green";
		
		this.conGP=null;
		
		if (this.conGP==null || this.conGP.isClosed())
		{
			try {
//				System.out.println("Este es el driver cargado; " + driver);
				Class.forName(driver);				
				this.conGP = DriverManager.getConnection(url + ";DBPATH=" + dbPath + ";DBLIB=" + dbLib, userG, passG);
			} 
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		return this.conGP;
	}
	
	
	public Connection establecerConexionGestionCopias() throws SQLException
	{
		String urlGC=LecturaProperties.urlGC;
		String passGC=LecturaProperties.passGC;
		String userGC=LecturaProperties.userGC;
		String dbPathC=LecturaProperties.dbPathC;
		String dbLibC=LecturaProperties.dbLibC;
		String driverC=LecturaProperties.driverC;
		
		this.conGC=null;
		
		if (this.conGC==null || this.conGC.isClosed())
		{
			try {
//				System.out.println("Este es el driver cargado; " + driver);
				Class.forName(driverC);				
				this.conGC = DriverManager.getConnection(urlGC + ";DBPATH=" + dbPathC + ";DBLIB=" + dbLibC, userGC, passGC);
			} 
			catch (ClassNotFoundException e) 
			{
				e.printStackTrace();
			}
			
		}
		return this.conGC;
	}
	
	public void cerrarConexion(Connection r_con)
	{
		try
		{
			if (r_con.getCatalog()!=null && r_con.getCatalog().contains("rout-star-bicycle-tienda"))
			{
				this.session.disconnect();
			}
			r_con.close();
		}
		catch(Exception sqlEx)
		{
		}
	}
	/**
	 * Metodo que conecta con la BBDD intermedia de SQLServer
	 * Es la BBDD que gestiona la sincronizacion entre la intranet y el SGA-MES
	 * @return
	 */
	public Connection establecerConexionSQLInd() 
	{
	    // Connect to your database.
	    // Replace server name, username, and password with your credentials
		
		/*
		 * Metodo que conecta con la BBDD Intermedia
		 */
		Connection con = null;
		
		String dbUrl=LecturaProperties.hostSQL;
		String port=LecturaProperties.portSQL;
		String user=LecturaProperties.userSQL;
		String pass=LecturaProperties.passSQL;
		String name=LecturaProperties.BBDDSQLsync;
		String driver=LecturaProperties.driverSQL;
		
		String connectionUrl =
				"jdbc:sqlserver://" + dbUrl + ":" + port + ";"
						+ "database=" + name + ";"
						+ "user=" + user + ";"
						+ "password=" + pass + ";"
						+ "encrypt=false;"
						+ "trustServerCertificate=false;"
						+ "loginTimeout=30;";
		try 
        {
			Class.forName(driver);
			con = DriverManager.getConnection(connectionUrl);
        }
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
	        // Handle any errors that may have occurred.
		catch (SQLException e) 
		{
			e.printStackTrace();
        }
		return con;
	}

	/**
	 * Metodo que conecta con la BBDD real de SQLServer
	 * Es la BBDD que gestiona el SGA-MES
	 * @return
	 */
	public Connection establecerConexionSGAInd() 
	{
		// Connect to your database.
		// Replace server name, username, and password with your credentials
		/*
		 * Metodo que conecta con la BBDD real
		 */
		Connection con = null;
		
		String dbUrl=LecturaProperties.hostSQL;
		String port=LecturaProperties.portSQL;
		String user=LecturaProperties.userSQL;
		String pass=LecturaProperties.passSQL;
		String name=LecturaProperties.BBDDSQL;
		String driver=LecturaProperties.driverSQL;
		
		String connectionUrl =
				"jdbc:sqlserver://" + dbUrl + ":" + port + ";"
						+ "database=" + name + ";"
						+ "user=" + user + ";"
						+ "password=" + pass + ";"
						+ "encrypt=false;"
						+ "trustServerCertificate=false;"
						+ "loginTimeout=30;";
		try 
		{
			Class.forName(driver);
			con = DriverManager.getConnection(connectionUrl);
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		// Handle any errors that may have occurred.
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return con;
	}

	public Connection establecerConexionSGATestInd() 
	{
		// Connect to your database.
		// Replace server name, username, and password with your credentials
		Connection con = null;
		
		String dbUrl=LecturaProperties.hostSQL;
		String port=LecturaProperties.portSQL;
		String user=LecturaProperties.userSQL;
		String pass=LecturaProperties.passSQL;
		String name=LecturaProperties.BBDDSQLtest;
		String driver=LecturaProperties.driverSQL;
		
		String connectionUrl =
				"jdbc:sqlserver://" + dbUrl + ":" + port + ";"
						+ "database=" + name + ";"
						+ "user=" + user + ";"
						+ "password=" + pass + ";"
						+ "encrypt=false;"
						+ "trustServerCertificate=false;"
						+ "loginTimeout=30;";
		try 
		{
			Class.forName(driver);
			con = DriverManager.getConnection(connectionUrl);
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		// Handle any errors that may have occurred.
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return con;
	}
}