package borsao.e_borsao.ClasesPropias;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.vaadin.server.FileResource;
import com.vaadin.ui.Image;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

public class ImageReceiver implements Receiver, SucceededListener  
{
	private static final long serialVersionUID = -1276759102490466761L;
	
	public File file;
	public Image image = null;
	public String ruta = null;
            
	public ImageReceiver(Image r_image, String r_ruta)
	{
		this.image=r_image;		
		this.setRuta(r_ruta);
	}
            
	public OutputStream receiveUpload(String filename, String mimeType) 
	{
		// Create upload stream
		FileOutputStream fos = null; // Stream to write to
		try 
		{
			// Open the file for writing.
			file = new File(this.getRuta() + filename);
			fos = new FileOutputStream(file);
		} 
		catch (final java.io.FileNotFoundException e) 
		{
			Notificaciones.getInstance().mensajeError(e.getMessage());
			return null;
		}
		return fos; // Return the output stream to write to
	}

	public void uploadSucceeded(SucceededEvent event) 
	{
		// Show the uploaded file in the image viewer
		image.setVisible(true);
		image.setSource(new FileResource(file));
		image.setAlternateText("cargada");
	}
	
	public String getFilePath()
	{
		if ( image!=null && ((FileResource) image.getSource())!=null && ((FileResource) image.getSource()).getSourceFile().getAbsolutePath()!=null)
			return ((FileResource) image.getSource()).getSourceFile().getAbsolutePath();
		else return null;
	}
	
	public File getFile()
	{
		if ( image!=null && ((FileResource) image.getSource())!=null && ((FileResource) image.getSource()).getSourceFile()!=null)
		return ((FileResource) image.getSource()).getSourceFile();
		else return null;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
}