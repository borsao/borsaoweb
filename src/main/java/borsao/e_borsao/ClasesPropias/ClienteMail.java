package borsao.e_borsao.ClasesPropias;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.AuthenticationException;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.modelo.Login.CurrentUser;



 public class ClienteMail
 {
	 private static ClienteMail instance;
	 private MimeMessage mensaje = null;
	 private String servidorSMTP;
	 private String puertoSMTP;	 	 
	 private String autentificacion;
	 private String cuentaCorreo;
	 private String usuarioCuenta;
	 private String passwordCuenta;
	 private boolean SSL;
	 private String para = null;
	 private String asunto = null;
	 private String para2 = null;
	 private String para3 = null;
	 private ArrayList<String> direccionesEnvio = null;
	 private String cuerpoMensaje = null;
	 private HashMap adjuntos = null;
	 private static String imapProtocol = null;
	 
	 public boolean enviado = false;	 
	 private static String folderName = "Elementos Enviados";
	 private static String folderName2 = "Sent Items";
	 
	 public static ClienteMail getInstance() 
	 {
		 
		 if (instance == null) 
		 {
			 instance = new ClienteMail();			
		 }
		 return instance;
	 }
 
	 public ClienteMail()
	 {
		 
	 }
	 public boolean envioMailSincro(String para, String copia, String asunto, String cuerpoMensaje) 
	 {
		 String mensaje = null;
		 boolean enviado = false;
		 this.setPara(para);
		 this.setPara2(copia);
		 this.setAsunto(asunto);
		 this.setCuerpoMensaje(cuerpoMensaje);
		 try 
		 {
			 this.sendMailSincro();
			 mensaje = "Mensaje enviado correctamente a los destinatarios: " + this.getPara();
			 if (this.getPara2()!=null && this.getPara2().length()>0) mensaje = mensaje + " y " + this.getPara2();
			 Notificaciones.getInstance().mensajeSeguimiento(mensaje);
			 enviado=true;
		 }
		 catch (Exception ex)
		 {
			 ex.printStackTrace();
			 enviado=false;
		 }
		 return enviado;
	 }

	 public boolean envioMail(String para, ArrayList<String> copia, String asunto, String cuerpoMensaje, HashMap adjuntos, boolean r_eliminarAdjunto) 
	 {
		 String mensaje = null;
		 boolean enviado = false;
		 this.setPara(para);
		 this.setAsunto(asunto);
		 this.setCuerpoMensaje(cuerpoMensaje);
		 this.setAdjuntos(adjuntos);
		 this.direccionesEnvio=copia;
		 
		 try 
		 {
			 this.sendMail();
			 if (adjuntos!=null) mensaje = "Archvo " + adjuntos.get("nombre").toString() + " enviado correctamente a los destinatarios: " + this.getPara();
			 else mensaje = "Mensaje enviado correctamente a los destinatarios: " + this.getPara();
			 if (copia!=null && !copia.isEmpty())
			 {
				 for (int i = 0; i<copia.size();i++)
				 {
					 mensaje = mensaje + " y " + copia.get(i);
				 }
			 }
			 Notificaciones.getInstance().mensajeAdvertencia(mensaje);
			 enviado=true;
		 }
		 catch (Exception ex)
		 {
			 ex.printStackTrace();
			 Notificaciones.getInstance().mensajeError("Error al enviar el mail: " + ex.getMessage());
			 enviado=false;
		 }
		 if (adjuntos!=null && r_eliminarAdjunto) RutinasFicheros.eliminarFichero(adjuntos.get("archivo").toString());
		 return enviado;
	 }

	 public boolean envioMailPrueba(String para, ArrayList<String> copia, String asunto, String cuerpoMensaje, HashMap adjuntos, boolean r_eliminarAdjunto) 
	 {
		 String mensaje = null;
		 boolean enviado = false;
		 this.setPara(para);
		 this.setAsunto(asunto);
		 this.setCuerpoMensaje(cuerpoMensaje);
		 this.setAdjuntos(adjuntos);
		 this.direccionesEnvio=copia;
		 
		 try 
		 {
			 this.sendMailPrueba();
//			 if (adjuntos!=null) mensaje = "Archvo " + adjuntos.get("nombre").toString() + " enviado correctamente a los destinatarios: " + this.getPara();
//			 mensaje = "Mensaje enviado correctamente a los destinatarios: " + this.getPara() + " por: " + CurrentUser.get();
//			 if (copia!=null && !copia.isEmpty())
//			 {
//				 for (int i = 0; i<copia.size();i++)
//				 {
//					 mensaje = mensaje + " y " + copia.get(i);
//				 }
//			 }
//			 Notificaciones.getInstance().mensajeAdvertencia(mensaje);
			 enviado=true;
		 }
		 catch (Exception ex)
		 {
			 ex.printStackTrace();
			 Notificaciones.getInstance().mensajeError("Error al enviar el mail: " + ex.getMessage());
			 enviado=false;
		 }
		 if (adjuntos!=null && r_eliminarAdjunto) RutinasFicheros.eliminarFichero(adjuntos.get("archivo").toString());
		 return enviado;
	 }

	 public boolean envioMail(String para, String copia, String copia2, String asunto, String cuerpoMensaje, HashMap adjuntos, boolean r_eliminarAdjunto) 
	 {
		 String mensaje = null;
		 boolean enviado = false;
		 this.setPara(para);
		 this.setPara2(copia);
		 this.setPara3(copia2);
		 this.setAsunto(asunto);
		 this.setCuerpoMensaje(cuerpoMensaje);
		 this.setAdjuntos(adjuntos);
		 this.direccionesEnvio=null;
		 try 
		 {
			 this.sendMail();
			 if (adjuntos!=null) mensaje = "Archvo " + adjuntos.get("nombre").toString() + " enviado correctamente a los destinatarios: " + this.getPara();
			 else mensaje = "Mensaje enviado correctamente a los destinatarios: " + this.getPara();
			 if (this.getPara2()!=null && this.getPara2().length()>0) mensaje = mensaje + " y " + this.getPara2();
			 if (this.getPara3()!=null && this.getPara3().length()>0) mensaje = mensaje + " y " + this.getPara3();
			 Notificaciones.getInstance().mensajeAdvertencia(mensaje);
			 enviado=true;
		 }
		 catch (Exception ex)
		 {
			 ex.printStackTrace();
			 Notificaciones.getInstance().mensajeError("Error al enviar el mail: " + ex.getMessage());
			 enviado=false;
		 }
		 if (adjuntos!=null && r_eliminarAdjunto) RutinasFicheros.eliminarFichero(adjuntos.get("archivo").toString());
		 return enviado;
	 }

     private void sendMail() throws AuthenticationException, MessagingException, AddressException
     {
    	 Session session = null;
    	 this.recogerParametrosConfiguracionCorreo();
         // Setup mail server
		 Properties props = System.getProperties();
		 props.put("mail.smtp.host", this.getServidorSMTP());
		 props.put("mail.smtp.port", this.getPuertoSMTP());
		 props.put("mail.smtp.auth", this.getAutentificacion());
		 props.put("mail.smtp.from", this.getCuentaCorreo());
		 
		 props.put("mail.smtp.ssl.trust", this.getServidorSMTP());
		 props.put("mail.smtp.starttls.enable", this.isSSL());
		 props.put("mail.smtp.connectiontimeout", "10000");
		 
		 String dsn = "SUCCESS,FAILURE,DELAY ORCPT=rfc822;" + this.getCuentaCorreo().trim();
		 props.put("mail.smtp.dsn.notify", dsn);
		 
		 if (this.isSSL())
		 {
			 props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			 props.put("mail.smtp.socketFactory.fallback", "false");
			 props.put("mail.smtp.socketFactory.port", this.getPuertoSMTP());
			 props.put("mail.smtp.debug", "true");
		 }
		 // Get a mail session
		 if (this.getAutentificacion().equals("true"))
		 {
			 session = Session.getInstance(props,
					 new javax.mail.Authenticator()
			 {
				 protected PasswordAuthentication getPasswordAuthentication() 
				 {
					 return new PasswordAuthentication(getUsuarioCuenta(), getPasswordCuenta());
				 }
			 });
		 }
		 else
		 {
			 session = Session.getInstance(props, null);
		 }
		 
		 session.setDebug(false);
		 this.mensaje = new MimeMessage(session);
		 
		 // Define a new mail message
		 
		 
	 // Define a new mail message
		 
		 if (this.getPara()==null || this.getPara().length()==0)
		 {
			 this.setPara(LecturaProperties.destino);
		 }
		 this.mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(this.getPara()));
		 
		 if (this.direccionesEnvio!=null && !direccionesEnvio.isEmpty())
		 {
			 for (int i = 0; i<direccionesEnvio.size();i++)
			 {
				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(direccionesEnvio.get(i)));
			 }
		 }
		 else
		 {
			 if (this.getPara2()!=null && this.getPara2().length()>0)
			 {
				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(this.getPara2()));
			 }
			 if (this.getPara3()!=null && this.getPara3().length()>0)
			 {
				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(this.getPara3()));
			 }
		 }
		 
		 this.mensaje.addHeader("Disposition-Notification-To",this.getCuentaCorreo());
		 this.mensaje.addHeader("Return-Receipt-To",this.getCuentaCorreo());
		 
		 this.mensaje.setSubject(this.getAsunto());
		 
		 Multipart multipart = new MimeMultipart();
		 
		 BodyPart messageBodyPart = new MimeBodyPart();
         String htmlText = "<H4>" + this.getCuerpoMensaje() + "</H4><img src=\"cid:image\">";
         messageBodyPart.setContent(htmlText, "text/html");
         // add it
         multipart.addBodyPart(messageBodyPart);
         
		 if (this.getAdjuntos() != null && this.getAdjuntos().size()>0)
		 {
			 adjuntos(this.getAdjuntos(), multipart);
		 }
		 
		 /*
		  * probando la firma
		  */
		 
		 if (eBorsao.get().accessControl!=null && eBorsao.get().accessControl.getFirma()!=null && eBorsao.get().accessControl.getFirma().length()>0)
		 {
			 messageBodyPart = new MimeBodyPart();
			 DataSource fds = new FileDataSource(LecturaProperties.baseImageSignature +  eBorsao.get().accessControl.getFirma());

			 messageBodyPart.setDataHandler(new DataHandler(fds));
			 messageBodyPart.setHeader("Content-ID", "<image>");
		 
			 multipart.addBodyPart(messageBodyPart);
		 }
		 // Put all message parts in the message
		 this.mensaje.setContent(multipart);
		 
		 // Send the message
		 this.mensaje.setFrom(new InternetAddress(this.getCuentaCorreo()));
		 
		 Transport transport = session.getTransport(this.getImapProtocol());
		 transport.connect();		 
		 	transport.sendMessage(this.mensaje, this.mensaje.getAllRecipients());
//		 	if ((this.getPara2()!=null && this.getPara2().length()>0) || (this.getPara3()!=null && this.getPara3().length()>0)) transport.sendMessage(this.mensaje, this.mensaje.getRecipients(Message.RecipientType.CC));
		 	transport.close();
		 
//		 Folder folder = null; 
// 	 	 Store store = session.getStore(imapProtocol);
//		 store.connect(this.getServidorSMTP(), this.getUsuarioCuenta(), this.getPasswordCuenta());
//		 try
//		 {
//			 folder = store.getFolder(folderName);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }
//		 catch (FolderNotFoundException fnf)
//		 {
//			 folder = store.getFolder(folderName2);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }	
     }
     
     private void sendMailPrueba() throws AuthenticationException, MessagingException, AddressException
     {
    	 Session session = null;
    	 this.recogerParametrosConfiguracionCorreo();
         // Setup mail server
		 Properties props = System.getProperties();
		 props.put("mail.smtp.host", this.getServidorSMTP());
		 props.put("mail.smtp.port", this.getPuertoSMTP());
		 props.put("mail.smtp.auth", this.getAutentificacion());
		 props.put("mail.smtp.from", "j.claveria@bodegasborsao.com");
		 
		 props.put("mail.smtp.ssl.trust", this.getServidorSMTP());
		 props.put("mail.smtp.starttls.enable", this.isSSL());
		 props.put("mail.smtp.connectiontimeout", "10000");
		 
		 String dsn = "SUCCESS,FAILURE,DELAY ORCPT=rfc822;" + "j.claveria@bodegasborsao.com";
		 props.put("mail.smtp.dsn.notify", dsn);
		 
		 if (this.isSSL())
		 {
			 props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			 props.put("mail.smtp.socketFactory.fallback", "false");
			 props.put("mail.smtp.socketFactory.port", this.getPuertoSMTP());
			 props.put("mail.smtp.debug", "true");
		 }
		 // Get a mail session
		 if (this.getAutentificacion().equals("true"))
		 {
			 session = Session.getInstance(props,
					 new javax.mail.Authenticator()
			 {
				 protected PasswordAuthentication getPasswordAuthentication() 
				 {
					 return new PasswordAuthentication("javier", "Javier-2017!!");
				 }
			 });
		 }
		 else
		 {
			 session = Session.getInstance(props, null);
		 }
		 
		 session.setDebug(false);
		 this.mensaje = new MimeMessage(session);
		 
		 // Define a new mail message
		 
		 
	 // Define a new mail message
		 
		 if (this.getPara()==null || this.getPara().length()==0)
		 {
			 this.setPara(LecturaProperties.destino);
		 }
		 this.mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(this.getPara()));
		 
		 if (this.direccionesEnvio!=null && !direccionesEnvio.isEmpty())
		 {
			 for (int i = 0; i<direccionesEnvio.size();i++)
			 {
				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(direccionesEnvio.get(i)));
			 }
		 }
		 else
		 {
//			 if (this.getPara2()!=null && this.getPara2().length()>0)
//			 {
//				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(this.getPara2()));
//			 }
//			 if (this.getPara3()!=null && this.getPara3().length()>0)
//			 {
//				 this.mensaje.addRecipient(Message.RecipientType.CC, new InternetAddress(this.getPara3()));
//			 }
		 }
		 
//		 this.mensaje.addHeader("Disposition-Notification-To","j.claveria@bodegasborsao.com");
//		 this.mensaje.addHeader("Return-Receipt-To","j.claveria@bodegasborsao.com");
		 
		 this.mensaje.setSubject(this.getAsunto());
		 
		 Multipart multipart = new MimeMultipart();
		 
		 BodyPart messageBodyPart = new MimeBodyPart();
         String htmlText = "<H4>" + this.getCuerpoMensaje() + "</H4><img src=\"cid:image\">";
         messageBodyPart.setContent(htmlText, "text/html");
         // add it
         multipart.addBodyPart(messageBodyPart);
         
		 if (this.getAdjuntos() != null && this.getAdjuntos().size()>0)
		 {
			 adjuntos(this.getAdjuntos(), multipart);
		 }
		 
		 /*
		  * probando la firma
		  */
		 
		 if (eBorsao.get().accessControl!=null && eBorsao.get().accessControl.getFirma()!=null && eBorsao.get().accessControl.getFirma().length()>0)
		 {
			 messageBodyPart = new MimeBodyPart();
			 DataSource fds = new FileDataSource(LecturaProperties.baseImageSignature +  eBorsao.get().accessControl.getFirma());

			 messageBodyPart.setDataHandler(new DataHandler(fds));
			 messageBodyPart.setHeader("Content-ID", "<image>");
		 
			 multipart.addBodyPart(messageBodyPart);
		 }
		 // Put all message parts in the message
		 this.mensaje.setContent(multipart);
		 
		 // Send the message
		 this.mensaje.setFrom(new InternetAddress("j.claveria@bodegasborsao.com"));
		 
		 Transport transport = session.getTransport(this.getImapProtocol());
		 transport.connect();		 
		 	transport.sendMessage(this.mensaje, this.mensaje.getAllRecipients());
//		 	if ((this.getPara2()!=null && this.getPara2().length()>0) || (this.getPara3()!=null && this.getPara3().length()>0)) transport.sendMessage(this.mensaje, this.mensaje.getRecipients(Message.RecipientType.CC));
		 	transport.close();
		 
//		 Folder folder = null; 
// 	 	 Store store = session.getStore(imapProtocol);
//		 store.connect(this.getServidorSMTP(), this.getUsuarioCuenta(), this.getPasswordCuenta());
//		 try
//		 {
//			 folder = store.getFolder(folderName);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }
//		 catch (FolderNotFoundException fnf)
//		 {
//			 folder = store.getFolder(folderName2);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }	
     }
     
     private void sendMailSincro() throws AuthenticationException, MessagingException, AddressException
     {
    	 Session session = null;
    	 this.recogerParametrosConfiguracionCorreoSincro();
    	 
         // Setup mail server
		 Properties props = System.getProperties();
		 props.put("mail.smtp.host", this.getServidorSMTP());  //en el ejemplo pone el nombre no la ip
		 props.put("mail.smtp.port", this.getPuertoSMTP()); // en el ejemplo pone el 587
		 props.put("mail.smtp.auth", this.getAutentificacion());
		 props.put("mail.smtp.from", this.getCuentaCorreo());
		 
		 props.put("mail.smtp.ssl.trust", this.getServidorSMTP());
		 props.put("mail.smtp.starttls.enable", this.isSSL());
		 props.put("mail.smtp.connectiontimeout", "10000");
		 
		 String dsn = "SUCCESS,FAILURE,DELAY ORCPT=rfc822;" + this.getCuentaCorreo().trim();
		 props.put("mail.smtp.dsn.notify", dsn);
		 
		 if (this.isSSL())
		 {
			 props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			 props.put("mail.smtp.socketFactory.fallback", "false");
			 props.put("mail.smtp.socketFactory.port", this.getPuertoSMTP());
			 props.put("mail.smtp.debug", "true");
		 }
		 
//		 // Get a mail session
		 if (this.getAutentificacion().equals("true"))
		 {
			 session = Session.getInstance(props,
					 new javax.mail.Authenticator()
			 {
				 protected PasswordAuthentication getPasswordAuthentication() 
				 {
					 return new PasswordAuthentication(getUsuarioCuenta(), getPasswordCuenta());
				 }
			 });
		 }
		 else
		 {
			 session = Session.getInstance(props, null);
		 }
		 
		 session.setDebug(false);
		 this.mensaje = new MimeMessage(session);
		 
		 // Define a new mail message
		 
		 if (this.getPara()==null && this.getPara().length()>0)
		 {
			 this.setPara(LecturaProperties.destino);
		 }
		 this.mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(this.getPara()));
		 
		 this.mensaje.addHeader("Disposition-Notification-To",this.getCuentaCorreo());
		 this.mensaje.addHeader("Return-Receipt-To",this.getCuentaCorreo());
		 
		 this.mensaje.setSubject(this.getAsunto());
		 
		 Multipart multipart = new MimeMultipart();
		 
		 BodyPart messageBodyPart = new MimeBodyPart();
         String htmlText = "<H4>" + this.getCuerpoMensaje() + "</H4><img src=\"cid:image\">";
         messageBodyPart.setContent(htmlText, "text/html");
         // add it
         multipart.addBodyPart(messageBodyPart);
         
		 /*
		  * probando la firma
		  */
		 
//		 if (eBorsao.get().accessControl!=null && eBorsao.get().accessControl.getFirma()!=null && eBorsao.get().accessControl.getFirma().length()>0)
//		 {
			 messageBodyPart = new MimeBodyPart();
			 DataSource fds = new FileDataSource(LecturaProperties.baseImageSignature +  "javierClaveria.png");

			 messageBodyPart.setDataHandler(new DataHandler(fds));
			 messageBodyPart.setHeader("Content-ID", "<image>");
		 
			 multipart.addBodyPart(messageBodyPart);
//		 }
		 // Put all message parts in the message
		 this.mensaje.setContent(multipart);
		 
		 // Send the message
		 this.mensaje.setFrom(new InternetAddress(this.getCuentaCorreo()));
		 
		 Transport transport = session.getTransport(this.getImapProtocol());
//		 	transport.connect(this.getServidorSMTP(), new Integer(this.getPuertoSMTP()) ,this.getUsuarioCuenta(), this.getPasswordCuenta());
		 transport.connect();
	 		transport.sendMessage(this.mensaje, this.mensaje.getAllRecipients());
//		 	if ((this.getPara2()!=null && this.getPara2().length()>0) || (this.getPara3()!=null && this.getPara3().length()>0)) transport.sendMessage(this.mensaje, this.mensaje.getRecipients(Message.RecipientType.CC));
		 	transport.close();
		 
		 	
/*
 * No se puede acceder a las carpetas del correo cuando el protocolo es SMTP		 	
 */
//		 Folder folder = null;
////		 URLName urlName = new URLName("smtps", this.getServidorSMTP(), 3025, null, this.getUsuarioCuenta(), this.getPasswordCuenta());
// 	 	 Store store = session.getStore(session.getProvider(imapProtocol));
//		 store.connect();
//		 try
//		 {
//			 folder = store.getFolder(folderName);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }
//		 catch (FolderNotFoundException fnf)
//		 {
//			 folder = store.getFolder(folderName2);
//			 folder.open(Folder.READ_WRITE);
//			 this.mensaje.setFlag(Flag.SEEN, true);
//			 folder.appendMessages(new Message[] {this.mensaje});
//			 store.close();
//		 }	
		 	
     }
     
     protected void adjuntos(HashMap attachments, Multipart multipart) throws MessagingException, AddressException
     {
         
         String filename = attachments.get("archivo").toString();
         MimeBodyPart attachmentBodyPart = new MimeBodyPart();
         
         //use a JAF FileDataSource as it does MIME type detection
         DataSource source = new FileDataSource(filename);
         attachmentBodyPart.setDataHandler(new DataHandler(source));
         
         //assume that the filename you want to send is the same as the
         //actual file name - could alter this to remove the file path
         attachmentBodyPart.setFileName(attachments.get("nombre").toString());
         
         //add the attachment
         multipart.addBodyPart(attachmentBodyPart);
     }
 
    private void recogerParametrosConfiguracionCorreo()
 	{    	
//			LecturaProperties lp = new LecturaProperties();
		this.setServidorSMTP(LecturaProperties.smtp);
		this.setPuertoSMTP(LecturaProperties.puerto);
		this.setAutentificacion(LecturaProperties.autentificacion);
		this.setSSL(LecturaProperties.ssl);
		this.setImapProtocol(LecturaProperties.protocolo);

		try
		{
			if (CurrentUser.get()==null || CurrentUser.get().length()==0)
			{
				this.setUsuarioCuenta(LecturaProperties.usuario);
				this.setCuentaCorreo(LecturaProperties.cuenta);
				this.setPasswordCuenta(LecturaProperties.password);
			}
			else
			{
				this.setUsuarioCuenta(eBorsao.get().accessControl.getUsuarioCuentaCorreo());
				this.setCuentaCorreo(CurrentUser.get());
				if (eBorsao.get().accessControl.getUsuarioCuentaCorreo().contains("Pedro"))
					this.setPasswordCuenta("1de2df");
				else
					this.setPasswordCuenta(eBorsao.get().accessControl.getPasswordCuenta());
			}
			if (LecturaProperties.pruebas)
			{
				this.setPara(LecturaProperties.destino);
				this.setPara2(null);
				this.setPara3(null);
			}
		}
		catch (Exception ex)
		{
			this.setUsuarioCuenta(LecturaProperties.usuario);
			this.setCuentaCorreo(LecturaProperties.cuenta);
			this.setPasswordCuenta(LecturaProperties.password);
			
			if (LecturaProperties.pruebas)
			{
				this.setPara(LecturaProperties.destino);
				this.setPara2(null);
				this.setPara3(null);
			}
		}
 	}
    
    private void recogerParametrosConfiguracionCorreoSincro()
 	{    	
//			LecturaProperties lp = new LecturaProperties();
		this.setImapProtocol(LecturaProperties.protocolo);
		this.setServidorSMTP(LecturaProperties.smtp);
		this.setPuertoSMTP(LecturaProperties.puerto);
		this.setAutentificacion(LecturaProperties.autentificacion);
		this.setCuentaCorreo(LecturaProperties.cuenta);
		this.setUsuarioCuenta(LecturaProperties.usuario);
		this.setPasswordCuenta(LecturaProperties.password);

		this.setSSL(LecturaProperties.ssl);
 	}
    
    private String getAutentificacion() 
    {
		return this.autentificacion;
	}

    private void setAutentificacion(String autentificacion) 
    {
		this.autentificacion = autentificacion.trim();
	}
    
    private void setSSL(String sslRecibo) 
    {
		if (sslRecibo.equals("True")) 
		{
			this.setSSL(true); 
		} else {
			this.setSSL(false);
		}
	}


    private String getPasswordCuenta() 
    {
		return this.passwordCuenta;
	}

    private void setPasswordCuenta(String r_passwordCuenta) 
    {
		this.passwordCuenta= r_passwordCuenta.trim();
	}

    private String getCuentaCorreo() 
    {
    	if (this.cuentaCorreo.contentEquals("a.cardiel@bodegasborsao.com"))
    	{
    		return "tecnicocalidad@bodegasborsao.com";
    	}
		return this.cuentaCorreo;
	}

    private void setCuentaCorreo(String cuentaCorreo) 
    {
		this.cuentaCorreo = cuentaCorreo.trim();
	}

    private String getPuertoSMTP() 
    {
		return this.puertoSMTP;
	}

    private void setPuertoSMTP(String puertoSMTP) 
    {
		this.puertoSMTP = puertoSMTP.trim();
	}

    private String getServidorSMTP() 
    {
		return this.servidorSMTP;
	}

    private void setServidorSMTP(String servidorSMTP) 
    {
		this.servidorSMTP = servidorSMTP.trim();
	}

    private boolean isSSL() 
    {
		return this.SSL;
	}

    private void setSSL(boolean ssl) 
    {
		this.SSL = ssl;
	}

    private HashMap getAdjuntos() 
    {
		return this.adjuntos;
	}

    private void setAdjuntos(HashMap adjuntos) 
    {
		this.adjuntos = adjuntos;
	}

    private String getAsunto() 
    {
		return this.asunto;
	}

    private void setAsunto(String asunto) 
    {
		this.asunto = asunto;
	}

    private String getCuerpoMensaje() 
    {
		return this.cuerpoMensaje;
	}

    private void setCuerpoMensaje(String cuerpoMensaje) 
    {
		this.cuerpoMensaje = cuerpoMensaje;
	}

    private String getPara() 
    {
		return this.para;
	}

    private  void setPara(String para) 
    {
		this.para = para;
	}
	 public String getPara2() {
		return para2;
	}

	public void setPara2(String para2) {
		this.para2 = para2;
	}

	/**
	 * @return the usuarioCuenta
	 */
    private String getUsuarioCuenta()
	{
		return this.usuarioCuenta;
	}

	/**
	 * @param usuarioCuenta the usuarioCuenta to set
	 */
    private void setUsuarioCuenta(String usuarioCuenta)
	{
		this.usuarioCuenta = usuarioCuenta.trim();
	}

	public String getPara3() {
		return para3;
	}

	public void setPara3(String para3) {
		this.para3 = para3;
	}

	public static String getImapProtocol() {
		return imapProtocol;
	}

	public static void setImapProtocol(String imapProtocol) {
		ClienteMail.imapProtocol = imapProtocol;
	}
 } 