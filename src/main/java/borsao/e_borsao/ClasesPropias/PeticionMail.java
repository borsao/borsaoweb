package borsao.e_borsao.ClasesPropias;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.NoConformidadesGrid;
import borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoFacturasVentas;

public class PeticionMail extends Window
{
	private OpcionesGrid App = null;
	private NoConformidadesGrid AppNC = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtMail= null;
	private TextField txtMail2= null;
	private TextField txtMail3= null;
	private TextArea txtObservaciones = null;
	private CheckBox chkAdjuntarPdf = null;
	/*
	 * Valores devueltos
	 */
	private MapeoFacturasVentas mapeoFactura = null;
	private MapeoNoConformidades mapeoNoConformidades = null;
	
	public PeticionMail(GridPropio r_App, MapeoGlobal r_mapeo, String r_titulo)
	{
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		principal.setSpacing(true);
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtMail=new TextField("Para");
				this.txtMail.setValue("");
				this.txtMail.setWidth("400px");
				this.txtMail.setRequired(true);
				fila1.addComponent(txtMail);
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtMail2=new TextField("Copia");
				this.txtMail2.setValue("");
				this.txtMail2.setWidth("400px");
				fila2.addComponent(txtMail2);
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.txtMail3=new TextField("Copia2");
				this.txtMail3.setValue("");
				this.txtMail3.setWidth("400px");
				this.txtMail3.setHeight("40px");
				fila3.addComponent(txtMail3);				
			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);				
				
				this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("400");
    			this.txtObservaciones.setHeight("200");
    			fila4.addComponent(txtObservaciones);

			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);				
				
				this.chkAdjuntarPdf= new CheckBox();
    			this.chkAdjuntarPdf.setCaption("Adjuntar PDF");
    			this.chkAdjuntarPdf.setValue(false);

    			fila5.addComponent(chkAdjuntarPdf);

		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setSpacing(true);

			
			btnBotonCVentana = new Button("Cancelar");
			btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
			btnBotonAVentana = new Button("Enviar");
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
			botonera.addComponent(btnBotonAVentana);
			botonera.addComponent(btnBotonCVentana);		
			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
			botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
	
		
			
		controles.addComponent(fila1);
		if (r_mapeo instanceof MapeoFacturasVentas)
		{
			this.App = (OpcionesGrid) r_App;
			this.mapeoFactura = (MapeoFacturasVentas) r_mapeo;
		}
		else if (r_mapeo instanceof MapeoNoConformidades)
		{
			controles.addComponent(fila2);
			controles.addComponent(fila3);
			controles.addComponent(fila4);
			controles.addComponent(fila5);

			this.AppNC = (NoConformidadesGrid) r_App;
			this.mapeoNoConformidades = (MapeoNoConformidades) r_mapeo;
		}

		controles.addComponent(botonera);
//		controles.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		if (r_mapeo instanceof MapeoFacturasVentas)
		{
			this.setHeight("250px");
		}
		else
		{
			this.setHeight("650px");
		}
		this.setWidth("475px");
		
		principal.addComponent(controles);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
	}

	private void habilitarCampos()
	{
	
		this.txtMail.setEnabled(true);
		
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					if (App != null)
					{
						mapeoFactura.setMail(txtMail.getValue());
						App.envioFactura(mapeoFactura);
					}
					else if (AppNC!=null)
					{
						mapeoNoConformidades.setCorreo(txtMail.getValue());
						mapeoNoConformidades.setCorreo2(txtMail2.getValue());
						mapeoNoConformidades.setCorreo3(txtMail3.getValue());
						mapeoNoConformidades.setObservacionCorreo(txtObservaciones.getValue());
						mapeoNoConformidades.setAdjuntarPdf(chkAdjuntarPdf.getValue());
						AppNC.envioIncidencia(mapeoNoConformidades);
					}
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.txtMail.getValue()==null || this.txtMail.getValue().toString().length()==0)
		{
			devolver = false;
			txtMail.focus();
		}
		return devolver;
	}
}