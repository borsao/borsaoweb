package borsao.e_borsao.ClasesPropias;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import borsao.e_borsao.eBorsao;

public abstract class serverBasico
{
	public static Integer obtenerSiguienteCodigoInterno(Connection parmConection, String parmTabla, String parmCampo) throws Exception
	{
       Integer codigoInterno = null;
	   PreparedStatement preparedStatement = null;
	   ResultSet resultSet = null;
	   StringBuffer cadenaSQL = new StringBuffer();
	   codigoInterno = new Integer(1);
	   try
	   {
         cadenaSQL.append(" SELECT max(" + parmTabla + "." + parmCampo + ") codigo_id ");
	     cadenaSQL.append(" FROM " +  parmTabla + " ");
	     preparedStatement = parmConection.prepareStatement(cadenaSQL.toString());      
	     resultSet = preparedStatement.executeQuery();
	     while (resultSet.next())
	     {        	         
	         codigoInterno = (new Integer(resultSet.getInt("codigo_id")+1));	             
	     }
	   }
	   catch (Exception exception)
	   {
	   }
	   finally
	   {
	     try
	     {
	       if (resultSet != null)
	       {
	         resultSet.close();
	       }
	       if (preparedStatement != null)
	       {
	         preparedStatement.close();
	       }
	     }
	     catch (SQLException sqlException)
	     {
	    	 return (codigoInterno); 
	     }
	   }
	   return (codigoInterno); 
	}
	
//	public static String obtenerValorCampoEnviandoCampoBusqueda(Connection r_conection, String r_tabla, String r_campoBusqueda, String r_campoDevolver, String r_valor) throws Exception
//	{
//       String valorDevolver = null;
//	   PreparedStatement preparedStatement = null;
//	   ResultSet resultSet = null;
//	   StringBuffer cadenaSQL = new StringBuffer();
//	   
//	   try
//	   {
//         cadenaSQL.append(" SELECT " + r_tabla + "." + r_campoDevolver + " valor ");
//	     cadenaSQL.append(" FROM " +  r_tabla + " ");
//	     cadenaSQL.append(" WHERE " +  r_tabla + "." + r_campoBusqueda + " = '" + r_valor + "' ");
//	     preparedStatement = r_conection.prepareStatement(cadenaSQL.toString());      
//	     resultSet = preparedStatement.executeQuery();
//	     while (resultSet.next())
//	     {        	         
//	         valorDevolver = resultSet.getString("valor");	             
//	     }
//	   }
//	   catch (Exception exception)
//	   {
//	   }
//	   finally
//	   {
//	     try
//	     {
//	       if (resultSet != null)
//	       {
//	         resultSet.close();
//	       }
//	       if (preparedStatement != null)
//	       {
//	         preparedStatement.close();
//	       }
//	     }
//	     catch (SQLException sqlException)
//	     {
//	    	 return (valorDevolver); 
//	     }
//	   }
//	   return (valorDevolver); 
//	}
	
	public static String eliminarRegistro(Connection con, String parmTabla, String parmCampo, Integer parmValor)
	{
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + parmTabla);            
			cadenaSQL.append(" WHERE " + parmCampo + " = ?"); 
				          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, parmValor);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();	
		}
		finally
		   {
		     try
		     {
		       if (preparedStatement != null)
		       {
		         preparedStatement.close();
		       }
		     }
		     catch (SQLException sqlException)
		     {
		     }
		   }
		return rdo;
	}
	
	public static boolean comprobarRegistro(Connection r_con, String parmTabla, String parmCampo, Integer parmValor)
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		
		try
		{
			cadenaSQL.append(" select " + parmCampo + " FROM " + parmTabla);            
			cadenaSQL.append(" WHERE " + parmCampo + " = ? "); 
				          
			preparedStatement = r_con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, parmValor);
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next())
			{        	         
		         return true;	             
			}

		}
		catch (Exception ex)
		{
		}
		return false;
	}
	
	public String semaforo()
	{
		if (!eBorsao.get().accessControl.isUsuarioAlerta()) return "0";
		return this.semaforos();
	}
	
	public abstract String semaforos();
}