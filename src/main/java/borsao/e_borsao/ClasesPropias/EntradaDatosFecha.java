package borsao.e_borsao.ClasesPropias;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.vaadin.ui.DateField;
import com.vaadin.ui.themes.ValoTheme;

public class EntradaDatosFecha extends DateField{

	public EntradaDatosFecha ()
	{
		this.setDateFormat("dd/MM/yyyy");
//		this.setStyleName("v-fecha");	
		this.addStyleName("v-datefield-textfield");
		this.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.setShowISOWeekNumbers(true);

		this.setLocale(new Locale("ES"));
	}
	
	public EntradaDatosFecha (String r_caption)
	{
		this.setCaption(r_caption);
		this.setDateFormat("dd/MM/yyyy");
		this.setStyleName("v-fecha");
		
		this.setShowISOWeekNumbers(true);
	}
	
	public void setValue(String r_date)
	{
		Date date = null;
		
		if (r_date==null || r_date.length()==0)
		{
			super.setValue(null);
		}
		else
		{
			date = this.convertirStringADate(r_date);
			this.setValue(date);
		}
	}

	public void setRangeStart(String r_date)
	{
		Date date = null;
		
		if (r_date.equals(""))
		{
			super.setRangeStart(null);
		}
		else
		{
			date = this.convertirStringADate(r_date);
			this.setRangeStart(date);
		}
	}
	
	public void setRangeEnd(String r_date)
	{
		Date date = null;
		if (r_date.equals(""))
		{
			super.setRangeEnd(null);
		}
		else
		{
			date = this.convertirStringADate(r_date);
			this.setRangeEnd(date);
		}
	}
	
	public String getFecha()
	{
		String fecha = null;
		fecha=this.convertirDateAString();		
		return fecha;
	}
	
	public Date convertirStringADate(String r_date)
	{
		Date date = null;;
		try 
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");			
			date = sdf.parse(r_date);
			return date;			
		} 
		catch (ParseException e) 
		{
			Notificaciones.getInstance().mensajeError(e.getMessage());
		}
		return null;
	}
	
	private String convertirDateAString()
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fecha = sdf.format(this.getValue());
		return fecha;			
	}

	public String convertirDateToString(Date r_date)
	{
		String fecha = null;;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fecha = sdf.format(r_date);
		return fecha;			
	}

}