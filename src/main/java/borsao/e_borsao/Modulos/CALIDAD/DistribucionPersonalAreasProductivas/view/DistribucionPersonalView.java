package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.MapeoDistribucionPersonal;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.server.consultaDistribucionPersonalServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DistribucionPersonalView extends GridViewRefresh {

	public static final String VIEW_NAME = "Distribucion Personal por Ejercicio y Area";
	private final String titulo = "Distribucion Personal por Ejercicio y Area";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = true;
	private MapeoDistribucionPersonal mapeoDistribucionPersonal=null;
	
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	
    	ArrayList<MapeoDistribucionPersonal> r_vector=null;
    	MapeoDistribucionPersonal mapeoDistribucionPersonal =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoDistribucionPersonal=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaDistribucionPersonalServer cdps = new consultaDistribucionPersonalServer(CurrentUser.get());
    	r_vector=cdps.datosOpcionesGlobal(mapeoDistribucionPersonal);
    	
    	if (r_vector!=null && r_vector.size()>0)
    	{
			grid = new OpcionesGrid(r_vector,this);
			setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    	
    	
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public DistribucionPersonalView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
//    	this.generarGrid(null);
    	
        this.form = new OpcionesForm(this);
        this.verForm(true);
        addComponent(this.form);

        this.botonesGenerales(true);
    }

    public void newForm()
    {
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarDistribucion((MapeoDistribucionPersonal) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);

    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
