package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDistribucionPersonal extends MapeoGlobal
{
	private String area;
	private String categoria;
	private Double valor;
	private String ejercicio;
	private boolean ett;
	private boolean eliminar=false;
	

	public MapeoDistribucionPersonal()
	{
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public Double getValor() {
		return valor;
	}


	public void setValor(Double valor) {
		this.valor = valor;
	}


	public String getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}


	public boolean isEtt() {
		return ett;
	}


	public void setEtt(boolean ett) {
		this.ett = ett;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}
}