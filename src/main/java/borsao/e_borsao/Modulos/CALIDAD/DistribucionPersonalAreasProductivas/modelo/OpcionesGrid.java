package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.server.consultaDistribucionPersonalServer;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.view.DistribucionPersonalView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = true;
	public boolean actualizar = false;
	private DistribucionPersonalView app = null;
	
    public OpcionesGrid(ArrayList<MapeoDistribucionPersonal> r_vector, DistribucionPersonalView r_app) 
    {
    	this.app=r_app;
        this.vector=r_vector;
		this.asignarTitulo("Distribucion Personal Areas");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		actualizar = false;
        this.crearGrid(MapeoDistribucionPersonal.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoDistribucionPersonal>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
//		this.setHeightMode(HeightMode.ROW);
//		this.setHeightByRows(30);
		this.calcularTotal();
    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	super.doEditItem();
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "categoria", "area","valor", "ett" );

    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.widthFiltros.put("ejercicio", "80");
    	this.widthFiltros.put("categoria", "210");
    	this.widthFiltros.put("area", "210");
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(120));
    	this.getColumn("ejercicio").setEditorField(getComboBox("El campo es obligatorio!", "ejercicios"));
    	this.getColumn("categoria").setHeaderCaption("Categoria");
    	this.getColumn("categoria").setWidth(new Double(250));
    	this.getColumn("categoria").setEditorField(getComboBox("El campo es obligatorio!", "categoria"));
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidth(new Double(250));
    	this.getColumn("area").setEditorField(getComboBox("El campo es obligatorio!", "area"));
    	this.getColumn("valor").setHeaderCaption("Valor");
    	this.getColumn("valor").setWidth(new Double(250));
    	this.getColumn("ett").setHeaderCaption("Ett");
    	this.getColumn("ett").setWidth(new Double(50));

    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("valor".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
        	}
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("valor");
		this.camposNoFiltrar.add("ett");
		this.camposNoFiltrar.add("eliminar");
	}

	@Override
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
//    	Indexed indexed = this.getContainerDataSource();
//        List<?> list = new ArrayList<Object>(indexed.getItemIds());
//        for(Object itemId : list)
//        {
//        	Item item = indexed.getItem(itemId);
//        	
//    		valor = (Integer) item.getItemProperty("cantidad").getValue();
//			totalU += valor.longValue();
//        }
//        
//        footer.getCell("tipo").setText("Totales ");
//		footer.getCell("cantidad").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
//		footer.getCell("cantidad").setStyleName("Rcell-pie");

	}

	private Field<?> getComboBox(String requiredErrorMsg, String r_campo) 
	{
		ComboBox comboBox = new ComboBox();
		
		if (r_campo.equals("categoria"))
		{
			comboBox.addItem("Encargado");
			comboBox.addItem("Oficial 1ª");
			comboBox.addItem("Oficial 2ª");
			comboBox.addItem("Peon");
			comboBox.addItem("Encargado Gral");
			comboBox.addItem("Administrativo 1ª");
			comboBox.addItem("Administrativo 2ª");
			
			comboBox.setRequired(true);
			comboBox.setRequiredError(requiredErrorMsg);
			comboBox.setInvalidAllowed(false);
			comboBox.setNullSelectionAllowed(false);
//			comboBox.setStyleName("blanco");
		}
		else if (r_campo.equals("area"))
		{
			comboBox.addItem("EMBOTELLADORA");
			comboBox.addItem("ENVASADORA");
			comboBox.addItem("BIB");
			comboBox.addItem("RETRABAJOS");
			
			comboBox.setRequired(true);
			comboBox.setRequiredError(requiredErrorMsg);
			comboBox.setInvalidAllowed(false);
			comboBox.setNullSelectionAllowed(false);
//			comboBox.setStyleName("blanco");
		}
		else if(r_campo.equals("ejercicios"))
		{
			ArrayList<String> ejer = null;
			RutinasEjercicios re = new RutinasEjercicios();
			
			ejer = re.cargarEjercicios();
			IndexedContainer cont = new IndexedContainer(ejer);
			comboBox.setContainerDataSource(cont);
			
			comboBox.setRequired(true);
			comboBox.setRequiredError(requiredErrorMsg);
			comboBox.setInvalidAllowed(false);
			comboBox.setNullSelectionAllowed(false);

		}
		return comboBox;
	}

}
