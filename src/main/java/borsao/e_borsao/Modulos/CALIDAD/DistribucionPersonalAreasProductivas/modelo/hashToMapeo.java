package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoDistribucionPersonal mapeo = null;
	 
	 public MapeoDistribucionPersonal convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoDistribucionPersonal();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setCategoria(r_hash.get("categoria"));
		 this.mapeo.setEjercicio(r_hash.get("ejercicio"));		 
		 if (r_hash.get("valor")!=null && r_hash.get("valor").length()>0) this.mapeo.setValor(new Double(r_hash.get("valor")));
		 return mapeo;		 
	 }
	 
	 public MapeoDistribucionPersonal convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoDistribucionPersonal();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setCategoria(r_hash.get("categoria"));
		 this.mapeo.setEjercicio(r_hash.get("ejercicio"));		 
		 if (r_hash.get("valor")!=null && r_hash.get("valor").length()>0) this.mapeo.setValor(new Double(RutinasCadenas.reemplazarComaMiles(r_hash.get("valor"))));
		 return mapeo;		 			 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoDistribucionPersonal r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("categoria", r_mapeo.getCategoria());
		 if (r_mapeo.getEjercicio()!=null)
		 {
			 this.hash.put("ejercicio", r_mapeo.getEjercicio().toString());
		 }
		 if (r_mapeo.getValor()!=null)
		 {
			 this.hash.put("valor", r_mapeo.getValor().toString());
		 }
		 return hash;		 
	 }
}