package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.MapeoDistribucionPersonal;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.server.consultaDistribucionPersonalServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoDistribucionPersonal mapeoDistribucion = null;
	 
	 private consultaDistribucionPersonalServer cdps = null;	 
	 private DistribucionPersonalView uw = null;
	 private BeanFieldGroup<MapeoDistribucionPersonal> fieldGroup;
	 
	 public OpcionesForm(DistribucionPersonalView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos();
        
        fieldGroup = new BeanFieldGroup<MapeoDistribucionPersonal>(MapeoDistribucionPersonal.class);
        fieldGroup.bindMemberFields(this);

        
        this.cargarListeners();

    }   

	private void cargarListeners()
	{
		
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoDistribucion = (MapeoDistribucionPersonal) uw.grid.getSelectedRow();
                eliminarDistribucion(mapeoDistribucion);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoDistribucion = hm.convertirHashAMapeo(uw.opcionesEscogidas);	 				
	                crearDistribucion(mapeoDistribucion);
	 			}
	 			else
	 			{
	 				MapeoDistribucionPersonal mapeoDistribucion_orig = (MapeoDistribucionPersonal) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoDistribucion= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarDistribucion(mapeoDistribucion,mapeoDistribucion_orig);
	 				hm=null;
	 			}
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos()
	{
		categoria.addItem("Encargado");
		categoria.addItem("Oficial 1ª");
		categoria.addItem("Oficial 2ª");
		categoria.addItem("Peon");
		categoria.addItem("Encargado Gral");
		categoria.addItem("Administrativo 1ª");
		categoria.addItem("Administrativo 2ª");
		
		categoria.setRequired(true);
		categoria.setInvalidAllowed(false);
		categoria.setNullSelectionAllowed(false);
		
		area.addItem("EMBOTELLADORA");
		area.addItem("ENVASADORA");
		area.addItem("BIB");
		area.addItem("RETRABAJOS");
		area.addItem("RETRABAJOSETT");
		
		area.setRequired(true);
		area.setInvalidAllowed(false);
		area.setNullSelectionAllowed(false);

	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarDistribucion(null);
		this.creacion = creacion;		
//		if (creacion) cargarNumerador();
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.categoria.getValue()!=null && this.categoria.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("categoria", this.categoria.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("categoria", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.valor.getValue()!=null && this.valor.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("valor", this.valor.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("valor", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarDistribucion(null);
	}
	
	public void editarDistribucion(MapeoDistribucionPersonal r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoDistribucionPersonal();
		r_mapeo.setEjercicio(RutinasFechas.añoActualYYYY());
		fieldGroup.setItemDataSource(new BeanItem<MapeoDistribucionPersonal>(r_mapeo));
		ejercicio.focus();
	}
	
	public void eliminarDistribucion(MapeoDistribucionPersonal r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cdps = new consultaDistribucionPersonalServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		cdps.eliminar(r_mapeo);
		
	}
	
	public void modificarDistribucion(MapeoDistribucionPersonal r_mapeo, MapeoDistribucionPersonal r_mapeo_orig)
	{
		cdps  = new consultaDistribucionPersonalServer(CurrentUser.get());
	    if (r_mapeo.getEjercicio()!=null) r_mapeo.setEjercicio(r_mapeo_orig.getEjercicio());
	    if (r_mapeo.getCategoria()!=null) r_mapeo.setCategoria(r_mapeo_orig.getCategoria());
	    if (r_mapeo.getArea()!=null) r_mapeo.setArea(r_mapeo_orig.getArea());
		cdps.guardarCambios(r_mapeo);		
		((OpcionesGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearDistribucion(MapeoDistribucionPersonal r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cdps  = new consultaDistribucionPersonalServer(CurrentUser.get());
		String rdo = cdps.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoDistribucionPersonal>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null)
			{
				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
			}
			else
			{
				uw.generarGrid(uw.opcionesEscogidas);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoDistribucionPersonal> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoDistribucion= item.getBean();
            if (this.mapeoDistribucion.getEjercicio()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
}
