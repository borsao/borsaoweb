package borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CALIDAD.DistribucionPersonalAreasProductivas.modelo.MapeoDistribucionPersonal;

public class consultaDistribucionPersonalServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDistribucionPersonalServer instance;
	
	public consultaDistribucionPersonalServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDistribucionPersonalServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDistribucionPersonalServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoDistribucionPersonal> datosOpcionesGlobal(MapeoDistribucionPersonal r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoDistribucionPersonal> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_distribucionPersonal_ej.ejercicio qcdp_eje, ");
		cadenaSQL.append(" qc_distribucionPersonal_ej.categoria qcdp_cat, ");
		cadenaSQL.append(" qc_distribucionPersonal_ej.area qcdp_are, ");
		cadenaSQL.append(" qc_distribucionPersonal_ej.ett qcdp_ett, ");
        cadenaSQL.append(" qc_distribucionPersonal_ej.valor qcdp_con ");
     	cadenaSQL.append(" FROM qc_distribucionPersonal_ej ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getCategoria()!=null && r_mapeo.getCategoria().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" categoria = '" + r_mapeo.getCategoria() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor= " + r_mapeo.getValor() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, area, categoria asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoDistribucionPersonal>();
			
			while(rsOpcion.next())
			{
				MapeoDistribucionPersonal mapeo = new MapeoDistribucionPersonal();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("qcdp_eje"));
				mapeo.setEjercicio(rsOpcion.getString("qcdp_eje"));
				mapeo.setArea(rsOpcion.getString("qcdp_are"));
				mapeo.setCategoria(rsOpcion.getString("qcdp_cat"));
				mapeo.setEtt(rsOpcion.getBoolean("qcdp_ett"));
				mapeo.setValor(rsOpcion.getDouble("qcdp_con"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return vector;
	}
	
	
	public Long recuperarValor(String r_ejercicio, String r_area, String r_categoria, boolean r_ett)
	{
		Long valor_obtenido = null;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		String ett = "0";
		try
		{
			if (r_ett) ett = "1";
			sql="select valor from qc_distribucionPersonal_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "' and categoria = '" + r_categoria + "' and ett = '" + ett + "' " ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido = rsValor.getLong("valor") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return valor_obtenido;
	}
	
	public boolean recuperarEtt(Integer r_ejercicio, String r_area, String r_categoria)
	{
		boolean valor_obtenido = false;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select ett from qc_distribucionPersonal_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "' and categoria = '" + r_categoria + "' ";
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido = rsValor.getBoolean("ett");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return valor_obtenido;
	}
	public boolean comprobarValor(String r_ejercicio, String r_area, String r_categoria,boolean r_ett)
	{
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		String ett = "0";
		try
		{
			if (r_ett) ett = "1";
			sql="select valor from qc_distribucionPersonal_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "' and categoria = '" + r_categoria + "' and ett = '" + ett + "' " ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}


	public String guardarNuevo(MapeoDistribucionPersonal r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_distribucionPersonal_ej ( ");
			cadenaSQL.append(" qc_distribucionPersonal_ej.ejercicio, ");
    		cadenaSQL.append(" qc_distribucionPersonal_ej.categoria, ");
    		cadenaSQL.append(" qc_distribucionPersonal_ej.area, ");
    		cadenaSQL.append(" qc_distribucionPersonal_ej.ett, ");
	        cadenaSQL.append(" qc_distribucionPersonal_ej.valor) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getCategoria()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCategoria());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getArea()!=null && r_mapeo.getArea().contains("ETT"))
		    	preparedStatement.setBoolean(4, true);
		    else
		    	preparedStatement.setBoolean(4, false);

		    if (r_mapeo.getValor()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getValor());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }

		    Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	
	public void guardarCambios(MapeoDistribucionPersonal r_mapeo)
	{
		Connection con = null;
		Statement cs = null;
		String ett = "0";
		String sql = null;
		try
		{
			
		    if (r_mapeo.getArea()!=null && r_mapeo.getArea().contains("ETT"))
		    	ett="1";
		    else
		    	ett="0";

			sql="update qc_distribucionPersonal_ej set valor = " + r_mapeo.getValor() + ", ett = '" + ett + "' where ejercicio = " + r_mapeo.getEjercicio() + " and area = '" + r_mapeo.getArea() + "' and categoria = '" + r_mapeo.getCategoria() + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}
	
	public void eliminar(MapeoDistribucionPersonal r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_distribucionPersonal_ej ");            
			cadenaSQL.append(" WHERE qc_distribucionPersonal_ej.ejercicio = ?");
			cadenaSQL.append(" AND qc_distribucionPersonal_ej.area = ?");
			cadenaSQL.append(" AND qc_distribucionPersonal_ej.categoria = ?");
			cadenaSQL.append(" AND qc_distribucionPersonal_ej.ett = ?");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento(cadenaSQL.toString());
			preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
			preparedStatement.setString(2, r_mapeo.getArea());
			preparedStatement.setString(3, r_mapeo.getCategoria());
			if (r_mapeo.isEtt()) preparedStatement.setString(4, "1"); else preparedStatement.setString(4, "0");
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}