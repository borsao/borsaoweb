package borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoParametrosCalidad extends MapeoGlobal
{
	private String parametro;
	private String valor;
	private String ejercicio;
	private boolean eliminar=false;
	

	public MapeoParametrosCalidad()
	{
	}


	public String getParametro() {
		return parametro;
	}


	public void setParametro(String parametro) {
		this.parametro = parametro;
	}


	public String getValor() {
		return valor;
	}


	public void setValor(String valor) {
		this.valor = valor;
	}


	public String getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

}