package borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.modelo.MapeoParametrosCalidad;

public class consultaParametrosCalidadServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaParametrosCalidadServer instance;
	
	public consultaParametrosCalidadServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaParametrosCalidadServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaParametrosCalidadServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoParametrosCalidad> datosOpcionesGlobal(MapeoParametrosCalidad r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoParametrosCalidad> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT qc_parametros_ej.idCodigo qcp_id, ");
		cadenaSQL.append(" qc_parametros_ej.ejercicio qcp_eje, ");
		cadenaSQL.append(" qc_parametros_ej.parametro qcp_par, ");
		cadenaSQL.append(" qc_parametros_ej.valor qcp_val ");
     	cadenaSQL.append(" FROM qc_parametros_ej ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + Integer.valueOf(r_mapeo.getEjercicio()));
				}
				if (r_mapeo.getParametro()!=null && r_mapeo.getParametro().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" parametro = '" + r_mapeo.getParametro() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor = '" + r_mapeo.getValor() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, parametro asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoParametrosCalidad>();
			
			while(rsOpcion.next())
			{
				MapeoParametrosCalidad mapeo = new MapeoParametrosCalidad();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("qcp_id"));
				mapeo.setEjercicio(rsOpcion.getString("qcp_eje"));
				mapeo.setValor(rsOpcion.getString("qcp_val"));
				mapeo.setParametro(rsOpcion.getString("qcp_par"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		return vector;
	}
	
	
	public String recuperarParametro(Integer r_ejercicio, String r_paramtero)
	{
		String valor_obtenido = null;
		ResultSet rsParametro = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select valor from qc_parametros_ej where ejercicio = " + r_ejercicio + " and parametro = '" + r_paramtero+ "'" ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsParametro = cs.executeQuery(sql);
			while(rsParametro.next())
			{
				valor_obtenido = rsParametro.getString("valor");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return valor_obtenido;
	}
	
	public String recuperarEjercicioCalidad(String r_paramtero)
	{
		String valor_obtenido = null;
		ResultSet rsParametro = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select max(ejercicio) prm_ej from qc_parametros_ej where parametro like '%" + r_paramtero+ "%'" ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsParametro = cs.executeQuery(sql);
			while(rsParametro.next())
			{
				valor_obtenido = rsParametro.getString("prm_ej");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return valor_obtenido;
	}
	

	public boolean comprobarParametro(String r_parametro, String r_ejercicio)
	{
		ResultSet rsParametro = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select parametro from qc_parametros_ej where parametro = '" + r_parametro + "' and ejercicio = " + r_ejercicio;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsParametro = cs.executeQuery(sql);
			while(rsParametro.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return false;
	}


	public String guardarNuevo(MapeoParametrosCalidad r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Integer sig = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_parametros_ej ( ");
			cadenaSQL.append(" qc_parametros_ej.idCodigo, ");
			cadenaSQL.append(" qc_parametros_ej.ejercicio, ");
    		cadenaSQL.append(" qc_parametros_ej.parametro, ");
    		cadenaSQL.append(" qc_parametros_ej.valor) VALUES (");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    sig = serverBasico.obtenerSiguienteCodigoInterno(con, "qc_parametros_ej", "idCodigo");
		    preparedStatement.setInt(1, sig);
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getParametro()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getParametro());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getValor()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getValor());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return sig.toString();
	}
	
	public void guardarCambios(String r_ejercicio, String r_parametro, String r_valor)
	{
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			sql="update qc_parametros_ej set valor = '" + r_valor + "' where ejercicio = " + r_ejercicio + " and parametro = '" + r_parametro + "'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}
	
	public void eliminar(MapeoParametrosCalidad r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_parametros_ej ");            
			cadenaSQL.append(" WHERE qc_parametros_ej.ejercicio = ?");
			cadenaSQL.append(" AND qc_parametros_ej.parametro = ?");
			con= this.conManager.establecerConexion();	     
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, new Integer(r_mapeo.getEjercicio()));
			preparedStatement.setString(2, r_mapeo.getParametro());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}