package borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.view;

import java.io.File;
import java.util.ArrayList;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.modelo.MapeoObservacionesProgramacion;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.server.consultaObservacionesProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.AyudaEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaObservacionesProgramacion extends Ventana
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Button btnBotonEliminar= null;
	private Button btnBotonVerPdfPadre = null;
	
	private Label lblArticulo = null;
	private Label lblDescripcionArticulo = null;
	private TextArea observaciones = null; 
	public Integer idCodigo = null;
	private Integer idProg = null;
	private String articuloProgramacion = null;
	private String operacionProgramacion = null;
	private GridPropio grid = null;
	private HorizontalLayout fila3 = null;
	private HorizontalLayout fila4 = null;
	private boolean cargarBotones = false;
	public pantallaObservacionesProgramacion(String r_articulo, Integer r_idProgramacion, String r_operacion)
	{
		this.idProg = r_idProgramacion;
		this.operacionProgramacion = r_operacion.toUpperCase();
		cargarBotones=true;
		if (this.operacionProgramacion.contentEquals("ETIQUETADO"))
		{
			this.articuloProgramacion = r_articulo + "-1";
		}
		else
		{
			this.articuloProgramacion = r_articulo;
		}
		
		this.cargarPantalla();

		this.rellenarEntradasDatos();
		
		this.habilitarCampos();
		this.cargarListeners();
	}

	public pantallaObservacionesProgramacion(String r_articulo)
	{
		this.idProg = 0;
		this.operacionProgramacion = "";
		cargarBotones=false;
		
		if (this.operacionProgramacion.contentEquals("ETIQUETADO"))
		{
			this.articuloProgramacion = r_articulo + "-1";
		}
		else
		{
			this.articuloProgramacion = r_articulo;
		}
		
		this.cargarPantalla();

		this.rellenarEntradasDatos();
		
		this.habilitarCampos();
		this.cargarListeners();
	}
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.lblArticulo=new Label();
				this.lblArticulo.setCaption("Articulo");
				this.lblArticulo.setWidth("120px");

				this.lblDescripcionArticulo=new Label();
				this.lblDescripcionArticulo.setCaption("Descripción");
				this.lblDescripcionArticulo.setWidth("250px");
				
				btnBotonVerPdfPadre = new Button();
				btnBotonVerPdfPadre.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnBotonVerPdfPadre.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonVerPdfPadre.setIcon(FontAwesome.FILE_PDF_O);
				btnBotonVerPdfPadre.setVisible(false);
				fila1.addComponent(this.lblArticulo);
				fila1.addComponent(this.lblDescripcionArticulo);
				fila1.addComponent(this.btnBotonVerPdfPadre);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.observaciones=new TextArea("Notas Calidad");
				this.observaciones.setWidth("800px");
				this.observaciones.setRequired(true);
				
				fila2.addComponent(this.observaciones);

			fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				
			fila3 = new HorizontalLayout();
			fila3.setWidth("100%");
			fila3.setSpacing(true);
			
				
			
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila4);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Observaciones Calidad del articulo " + this.articuloProgramacion + "  " + this.lblDescripcionArticulo.getValue());
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("850px");
		
		btnBotonEliminar= new Button("Eliminar");
		btnBotonEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonEliminar.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana = new Button("Guardar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		if (cargarBotones) botonera.addComponent(btnBotonAVentana);
		if (cargarBotones) botonera.addComponent(btnBotonEliminar);		
		botonera.addComponent(btnBotonCVentana);
		
		if (cargarBotones) botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		if (cargarBotones) botonera.setComponentAlignment(btnBotonEliminar, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
	}
	
	private void habilitarCampos()
	{
	
		this.observaciones.setEnabled(true);
	}
	
	private void cargarListeners()
	{
//		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath + "/calidad", mapeo.getArticulo().trim() + ".pdf", false);
		
		btnBotonVerPdfPadre.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath + "/calidad/pt", lblArticulo.getValue().trim() + ".pdf", false);
			}
		});

		btnBotonEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				consultaObservacionesProgramacionServer cops = new consultaObservacionesProgramacionServer(CurrentUser.get());
				MapeoObservacionesProgramacion mapeo = new MapeoObservacionesProgramacion();
				mapeo.setIdProgramacion(idProg);
				cops.eliminar(mapeo);
				close();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				String todoCorrecto = todoEnOrden();
				if (todoCorrecto.equals("OK"))
				{
					
					MapeoObservacionesProgramacion newMapeo = new MapeoObservacionesProgramacion();
					newMapeo.setIdProgramacion(idProg);
					newMapeo.setObservaciones(observaciones.getValue());
					
					String rdo = null;
					consultaObservacionesProgramacionServer cops = new consultaObservacionesProgramacionServer(CurrentUser.get());
					
					if (idCodigo==null)
					{
						rdo = cops.guardarNuevo(newMapeo);
						idCodigo = new Integer(rdo);
						rdo = null;
					}
					else
					{
						rdo = cops.guardarCambios(newMapeo);
					}
					if (rdo!=null)
					{
						Notificaciones.getInstance().mensajeError(rdo);
					}
					else
					{
						btnBotonCVentana.setEnabled(true);
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia(todoCorrecto);
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private String todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		String devolver = "OK";

		return devolver;
	}

	private void rellenarEntradasDatos()
	{
		consultaObservacionesProgramacionServer cops = new consultaObservacionesProgramacionServer(CurrentUser.get());
		MapeoObservacionesProgramacion mapeo = new MapeoObservacionesProgramacion();
		
		mapeo.setIdProgramacion(this.idProg);		
		mapeo =cops.datosObservacionesProgramacionGlobal(mapeo);
		
		if (this.operacionProgramacion.contentEquals("ETIQUETADO"))
		{
			this.lblArticulo.setValue(this.articuloProgramacion.substring(0, 7));
		}
		else
		{
			this.lblArticulo.setValue(this.articuloProgramacion);
		}

		
		this.lblDescripcionArticulo.setValue(cops.obtenerDescripcionArticulo(this.lblArticulo.getValue()));

		if (mapeo!= null && mapeo.getIdCodigo()!=null)
		{
			this.idCodigo = mapeo.getIdCodigo();		
			this.observaciones.setValue(mapeo.getObservaciones());
		}
		else 
		{
			this.idCodigo=null;
			this.observaciones.setValue("");
		}
		
    	File fl =new File(LecturaProperties.basePdfPath + "/calidad/pt/" + lblArticulo.getValue().trim() + ".pdf");
    	
    	if (fl.exists())
    	{
    		this.btnBotonVerPdfPadre.setVisible(true);
    	}

		this.presentarGrid();
		
	}
	
	private void presentarGrid()
	{
		ArrayList<MapeoLineasEscandallo> vector = null;
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		vector = ces.recuperarModificacionesEscandallo(this.articuloProgramacion);
		
		this.grid = new AyudaEscandalloGrid(vector,true);
		
		if (this.grid!=null && this.grid.vector!=null && !this.grid.vector.isEmpty())
		{
			this.grid.setCaption("Escandallo");
			this.grid.getColumn("fechaModificacion").setHidden(true);
			this.grid.setSelectionMode(SelectionMode.SINGLE);
			this.grid.setWidth("100%");
			this.fila3.addComponent(this.grid);
		}
	}
	public void aceptarProceso(String r_accion)
	{
	}
	
	public void cancelarProceso(String r_accion)
	{
	}
}