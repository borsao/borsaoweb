package borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoObservacionesProgramacion extends MapeoGlobal
{

	private Integer idProgramacion;
	private String observaciones;
	
	public MapeoObservacionesProgramacion()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	
}