package borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.modelo.MapeoObservacionesProgramacion;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaObservacionesProgramacionServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaObservacionesProgramacionServer instance;
	
	public consultaObservacionesProgramacionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaObservacionesProgramacionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaObservacionesProgramacionServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoObservacionesProgramacion datosObservacionesProgramacionGlobal(MapeoObservacionesProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_datos_programacion.idCodigo qc_id,");
		cadenaSQL.append(" qc_datos_programacion.idProgramacion qc_prg,");
		cadenaSQL.append(" qc_datos_programacion.observaciones qc_obs ");
		cadenaSQL.append(" from qc_datos_programacion ");
		cadenaSQL.append(" inner join prd_programacion on prd_programacion.idPrd_programacion = qc_datos_programacion.idProgramacion ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idProgramacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idCodigo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoObservacionesProgramacion mapeo = new MapeoObservacionesProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("qc_id"));
				mapeo.setIdProgramacion(rsOpcion.getInt("qc_prg"));
				mapeo.setObservaciones(rsOpcion.getString("qc_obs"));
				
				return mapeo;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public String guardarNuevo(MapeoObservacionesProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Integer id = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO qc_datos_programacion( ");
			cadenaSQL.append(" qc_datos_programacion.idCodigo,");
			cadenaSQL.append(" qc_datos_programacion.idProgramacion,");
			cadenaSQL.append(" qc_datos_programacion.observaciones ) VALUES (");
			cadenaSQL.append(" ?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    id =consultaObservacionesProgramacionServer.obtenerSiguienteCodigoInterno(con, "qc_datos_programacion", "idCodigo");
		    preparedStatement.setInt(1, id);
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }     
		return id.toString();
	}
	
	public String guardarCambios(MapeoObservacionesProgramacion r_mapeo)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE qc_datos_programacion set ");
			cadenaSQL.append(" qc_datos_programacion.observaciones =? ");
			cadenaSQL.append(" where qc_datos_programacion.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

		    if (r_mapeo.getIdCodigo()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdCodigo());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    
		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			rdo=ex.getMessage();
	    }
		return rdo;
	}
	
	public void eliminar(MapeoObservacionesProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_datos_programacion");            
			cadenaSQL.append(" WHERE qc_datos_programacion.idProgramacion = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}


	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
}