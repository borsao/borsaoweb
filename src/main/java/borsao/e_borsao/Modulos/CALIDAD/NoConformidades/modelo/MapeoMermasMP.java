package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoMermasMP extends MapeoGlobal
{
	private Integer cantidad;
	private Integer ejercicio;
	private Integer semana;
	private String articulo;
	private String descripcionarticulo;
	private String serie;
		
	public MapeoMermasMP()
	{
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getDescripcionarticulo() {
		return descripcionarticulo;
	}

	public void setDescripcionarticulo(String descripcionarticulo) {
		this.descripcionarticulo = descripcionarticulo;
	}

}