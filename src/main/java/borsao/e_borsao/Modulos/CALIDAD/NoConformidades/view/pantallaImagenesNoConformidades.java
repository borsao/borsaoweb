package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.CargaImagenes;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.contenedorImagen;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoImagenesNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaImagenesNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaImagenesNoConformidades extends Window
{
	private String ruta=null;;	
	private Button btnBotonCVentana = null;
	private Button btnBotonAimagen = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private Integer id = null;
	private Integer registro = null;
	private String titulo = null;
	private VerticalLayout principal =null;
	private HorizontalLayout panelGrid =null;
	private HorizontalLayout botonera = null;
	private ArrayList<MapeoImagenesNoConformidades> vector = null;
	private MapeoNoConformidades mapeoNoConformidades = null;
	private HorizontalLayout panelImagen = null;
	
	public pantallaImagenesNoConformidades(MapeoNoConformidades r_mapeo, String r_titulo)
	{
		this.ruta=eBorsao.get().prop.rutaImagenesCalidad;
		
		this.mapeoNoConformidades=r_mapeo;
		this.id=r_mapeo.getIdqc_incidencias();
		this.titulo="Fecha: " + RutinasFechas.convertirDateToString(this.mapeoNoConformidades.getFechaNotificacion()) + 
				" No Conformidad: " + RutinasNumericas.formatearIntegerDigitos(this.mapeoNoConformidades.getNumerador(),0) +
				" Incidencia: " + this.mapeoNoConformidades.getIncidencia();

		this.cargarPantalla();
		this.generarGrid();
		this.cargarListeners();
		
		this.principal.addComponent(this.panelGrid);
		this.principal.addComponent(this.botonera);
		this.principal.setComponentAlignment(this.panelGrid, Alignment.TOP_LEFT);
		this.principal.setComponentAlignment(this.botonera, Alignment.BOTTOM_LEFT);
		this.setContent(this.principal);

		if (this.vector==null || this.vector.size()==0)
		{
			btnBotonAimagen.setCaption("Agregar");
		}
	}

	private void cargarPantalla()
	{
		this.setCaption(this.titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(false);
		this.setWidth("1200px");
		this.setHeight("550px");
		
		this.principal = new VerticalLayout();
		this.principal.setSizeFull();
		this.principal.setMargin(true);
		
		this.panelGrid= new HorizontalLayout();
		this.panelGrid.setWidthUndefined();
		this.panelGrid.setHeight("500px");		
		this.panelGrid.setSpacing(true);
		
		this.panelImagen = new HorizontalLayout();
		this.panelImagen.setSizeFull();

		this.botonera = new HorizontalLayout();
		this.botonera.setWidthUndefined();
		this.botonera.setHeight("100px");		
		this.botonera.setSpacing(true);
		
			this.btnBotonCVentana = new Button("Cerrar");
			this.btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
			this.btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
	
			this.btnBotonAimagen = new Button("Guardar");
			this.btnBotonAimagen.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			this.btnBotonAimagen.addStyleName(ValoTheme.BUTTON_TINY);

			this.botonera.addComponent(this.btnBotonAimagen);
			this.botonera.addComponent(this.btnBotonCVentana);
			this.botonera.setComponentAlignment(this.btnBotonAimagen, Alignment.BOTTOM_LEFT);
			this.botonera.setComponentAlignment(this.btnBotonCVentana, Alignment.BOTTOM_CENTER);
	}

	private void generarGrid()
	{

		
		if (this.gridDatos!=null)
		{
			this.gridDatos.removeAllColumns();
			this.container=null;
			this.gridDatos=null;
		}
		
		this.llenarRegistros();
		
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.setEditorEnabled(false);
		this.gridDatos.setWidth("620px");
		this.gridDatos.setHeight("400px");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setFrozenColumnCount(2);

		this.panelGrid.addComponent(this.gridDatos);
		this.panelGrid.addComponent(this.panelImagen);
	}
	
	private void llenarRegistros()
	{
		Iterator iterator = null;
		MapeoImagenesNoConformidades mapeo = null;
		consultaImagenesNoConformidadesServer cps = new consultaImagenesNoConformidadesServer(CurrentUser.get());
		
		mapeo = new MapeoImagenesNoConformidades();
		mapeo.setIdqc_incidencias(this.id);
		
		this.container = new IndexedContainer();
		this.container.addContainerProperty("agregar", String.class, null);
		this.container.addContainerProperty("eliminar", String.class, null);
		this.container.addContainerProperty("idQc", Integer.class, null);
		this.container.addContainerProperty("orden", Integer.class, null);
		this.container.addContainerProperty("archivo", String.class, null);
		
		this.vector = cps.datosImagenesNoConformidadesGlobal(this.id);
		iterator =vector.iterator();
		
		while (iterator.hasNext())
		{
			Object newItemId = this.container.addItem();
			Item file = this.container.getItem(newItemId);
			
			mapeo= (MapeoImagenesNoConformidades) iterator.next();
			file.getItemProperty("agregar").setValue("");
			file.getItemProperty("eliminar").setValue("");
			file.getItemProperty("idQc").setValue(mapeo.getIdqc_incidencias());
    		file.getItemProperty("orden").setValue(mapeo.getOrden());
    		file.getItemProperty("archivo").setValue(mapeo.getArchivo());
		}

		this.gridDatos= new Grid(container);
		this.asignarEstilos();
	}

	private void regenerarGrid(ArrayList<MapeoImagenesNoConformidades> r_vector)
	{
		Iterator iterator = null;
		MapeoImagenesNoConformidades mapeo = null;
		
		iterator =r_vector.iterator();
		
		while (iterator.hasNext())
		{
			Object newItemId = this.gridDatos.getContainerDataSource().addItem();
			Item file = this.gridDatos.getContainerDataSource().getItem(newItemId);
			
			mapeo= (MapeoImagenesNoConformidades) iterator.next();
			file.getItemProperty("agregar").setValue("");
			file.getItemProperty("eliminar").setValue("");
			file.getItemProperty("idQc").setValue(mapeo.getIdqc_incidencias());
    		file.getItemProperty("orden").setValue(mapeo.getOrden());
    		file.getItemProperty("archivo").setValue(mapeo.getArchivo());
		}
		this.gridDatos.sort("orden");
//		this.gridDatos= new Grid(container);
//		this.asignarEstilos();
	}

	private void asignarEstilos()
    {
		this.asignarTooltips();
		
		this.gridDatos.getColumn("idQc").setHidden(true);
		this.gridDatos.getColumn("agregar").setHeaderCaption("");
		this.gridDatos.getColumn("agregar").setSortable(false);
		this.gridDatos.getColumn("agregar").setWidth(new Double(50));
		this.gridDatos.getColumn("eliminar").setHeaderCaption("");
		this.gridDatos.getColumn("eliminar").setSortable(false);
		this.gridDatos.getColumn("eliminar").setWidth(new Double(50));
		this.gridDatos.getColumn("orden").setHeaderCaption("Orden");
		this.gridDatos.getColumn("orden").setSortable(false);
		this.gridDatos.getColumn("orden").setWidth(new Double(100));
		this.gridDatos.getColumn("archivo").setHeaderCaption("Archivo de Imagen");
		this.gridDatos.getColumn("archivo").setSortable(false);
		this.gridDatos.getColumn("archivo").setWidth(new Double(350));
		
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "agregar".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonAgregar";
            	}
            	else if ( "eliminar".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEliminar";
            	}
            	else if ( "orden".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
    	});
    }
	private void cargarListeners()
	{
		
		this.btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				cerrar();
			}
		});

		this.btnBotonAimagen.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				if (vector==null || vector.size()==0)
				{
					MapeoImagenesNoConformidades mapeo = new MapeoImagenesNoConformidades();
					mapeo.setIdqc_incidencias(id);
					mapeo.setOrden(0);
					registro = 0;
					btnBotonAimagen.setCaption("Guardar");
					agregarImagen(mapeo);
				}
				else
				{
					guardarTodo();
				}
			}
		});
	
		this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
		{
	        public void itemClick(ItemClickEvent event) 
	        {
	        	if (event.getPropertyId()!=null)
            	{
		        	MapeoImagenesNoConformidades mapeo = (MapeoImagenesNoConformidades) vector.get(((Integer) event.getItemId())-1);
		        	
		        	if (event.getPropertyId().toString().equals("agregar"))
		        	{
		        		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
		        		{
		        			registro=mapeo.getOrden();
		        			vector.remove(mapeo);
		        			gridDatos.refreshAllRows();
		        			agregarImagen(mapeo); 
		        		}
		        	}
		        	else if (event.getPropertyId().toString().equals("eliminar"))
		        	{
		        		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
		        		{
		        			vector.remove(mapeo);
		        			if (mapeo.getArchivo()!=null)
		        			{
		        				File ficImagen = new File(mapeo.getRuta()+mapeo.getArchivo());
		        				if (ficImagen.exists())
		        				{
		        					ficImagen.delete();
		        				}
		        			}
		        			mapeo.setArchivo(null);
		        			vector.add(mapeo);
		        		}
		        		gridDatos.getContainerDataSource().removeAllItems();
		        		regenerarGrid(vector);
		        	}
		        	else
		        	{
		    			if (mapeo.getRuta()!=null)
		    			{
		    				File ficImagen = new File(mapeo.getRuta()+mapeo.getArchivo());
		    				if (ficImagen.exists()) 
		    				{
			    				FileResource res = new FileResource(ficImagen);
			    	    		contenedorImagen contenedor = new contenedorImagen(370, 400, res);
			    	    		
			    	    		panelImagen.removeAllComponents();
			    	    		panelImagen.addComponent(contenedor);
			    	    		panelImagen.setComponentAlignment(contenedor, Alignment.MIDDLE_CENTER);
			    	    		panelImagen.setHeight(String.valueOf(contenedor.getHeight()));
		    				}
		    			}
		    			else
		    			{
		    				panelImagen.removeAllComponents();
		    			}
		        	}
            	}
			}
	    });
	}

    private void asignarTooltips()
    {
    	this.gridDatos.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) 
    {
        String descriptionText = null;
        
        if ("agregar".equals(cell.getPropertyId()))
        {
        	descriptionText = "Asociar Imagen";
        }
        else if ("eliminar".equals(cell.getPropertyId()))
        {
        	descriptionText = "Asociar Imagen";
        }
        return descriptionText;
    }

	private void cerrar()
	{
		close();
	}
	
	private void guardarTodo()
	{
//		Notificaciones.getInstance().mensajeInformativo("Guardo los datos ");
		/*
		 * Guardaremos los datos en la base de datos
		 * 
		 */
		String rdo = null;
		
		consultaImagenesNoConformidadesServer cis = consultaImagenesNoConformidadesServer.getInstance(CurrentUser.get());
		cis.eliminar(this.id);
		
		rdo = cis.guardarNuevo(this.vector, this.id);
		
		if (rdo!=null)
		{
			Notificaciones.getInstance().mensajeError("No se han podido guardar los datos");
		}
		close();
	}
	
	public void ejecutarAccion(String  r_accion, String r_archivo)
    {
		if (r_accion.equals("Actualizar"))
		{
//			Notificaciones.getInstance().mensajeInformativo("Guardo el fichero: " + r_ruta);
			MapeoImagenesNoConformidades mapeo = new MapeoImagenesNoConformidades();
			mapeo.setIdqc_incidencias(this.id);
			mapeo.setArchivo(r_archivo);
			mapeo.setOrden(registro);
			this.vector.add(mapeo);
			this.gridDatos.getContainerDataSource().removeAllItems();
			regenerarGrid(this.vector);
		}
		
    }

	private void agregarImagen(MapeoImagenesNoConformidades r_mapeo)
	{
		String archivo = null;
		if (r_mapeo!=null && r_mapeo.getRuta()!=null && r_mapeo.getArchivo()!=null) archivo=r_mapeo.getRuta()+r_mapeo.getArchivo();
		
		CargaImagenes up = new CargaImagenes("basic",this, this.ruta, archivo);
		getUI().addWindow(up);		
	}
}

