package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.mermasPtView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class MermasPtGrid extends GridPropio 
{
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	private boolean editable = false;
	private boolean conFiltro = true;
	
	public Integer permisos = null;
	private mermasPtView app = null;
	private boolean conTotales = false;
	
    public MermasPtGrid(mermasPtView r_app, ArrayList<MapeoMermasPt> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("MERMAS EN EMBOTELLADO");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoMermasPt.class);
		this.setRecords(this.vector);
		
		this.setStyleName("smallgrid");
		this.setConTotales(this.conTotales);
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
//    	setColumnOrder("estado","img", "imagen", "euro", "programacion", "linea", "dia", "ejercicio", "semana", "dia","articulo", "cantidad", "tecnicoCalidad", "observaciones");
    	setColumnOrder("ejercicio", "semana", "tipo", "dia","articulo", "cantidad", "tecnicoCalidad", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("dia", "85");
    	this.widthFiltros.put("articulo", "85");
    	this.widthFiltros.put("semana", "85");
    	this.widthFiltros.put("tipo", "145");
    	
//    	this.getColumn("imagen").setHeaderCaption("");
//    	this.getColumn("imagen").setSortable(false);
//    	this.getColumn("imagen").setWidth(new Double(50));

    	this.getColumn("dia").setHeaderCaption("DIA");
    	this.getColumn("dia").setWidthUndefined();
    	this.getColumn("dia").setWidth(120);
    	this.getColumn("semana").setHeaderCaption("Semana");
    	this.getColumn("semana").setWidth(120);
    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setWidth(165);

    	this.getColumn("articulo").setHeaderCaption("CODIGO");
    	this.getColumn("articulo").setWidth(120);
    	this.getColumn("cantidad").setHeaderCaption("CANT.");
    	this.getColumn("cantidad").setWidth(120);
    	this.getColumn("tecnicoCalidad").setHeaderCaption("Técnico");
    	this.getColumn("tecnicoCalidad").setWidth(350);
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("observaciones").setWidth(550);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	
    	this.getColumn("dia").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoNoConformidades) {
                MapeoMermasPt progRow = (MapeoMermasPt)bean;
                // The actual description text is depending on the application
                if ("articulo".equals(cell.getPropertyId()))
                {
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "semana".equals(cellReference.getPropertyId()) || "cantidad".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoMermasPt mapeo = (MapeoMermasPt) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("articulo"))
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
//            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("cantidad");
		this.camposNoFiltrar.add("tecnicoCalidad");
		this.camposNoFiltrar.add("observaciones");
	}

	public void generacionPdf(MapeoMermasPt r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
    		valor = (Integer) item.getItemProperty("cantidad").getValue();
			totalU += valor.longValue();
        }
        
        footer.getCell("tipo").setText("Totales ");
		footer.getCell("cantidad").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("cantidad").setStyleName("Rcell-pie");
		

	}
}


