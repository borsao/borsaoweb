package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoMermasPt extends MapeoGlobal
{
	private Integer cantidad;
	private Integer ejercicio;
	private Integer semana;
	
	private Date dia;

	private String articulo;
	private String observaciones;
	private String tecnicoCalidad;
	private String tipo;
	
	public MapeoMermasPt()
	{
//		this.setTipo("Proceso");
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public Date getDia() {
		return dia;
	}

	public String getArticulo() {
		return articulo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public String getTecnicoCalidad() {
		return tecnicoCalidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public void setTecnicoCalidad(String tecnicoCalidad) {
		this.tecnicoCalidad = tecnicoCalidad;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo=tipo;
	}

}