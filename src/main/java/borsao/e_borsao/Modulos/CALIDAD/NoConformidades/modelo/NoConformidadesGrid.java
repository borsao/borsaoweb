package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionMail;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.NoConformidadesView;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.pantallaImagenesNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.pantallaValoracionNoConformidades;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class NoConformidadesGrid extends GridPropio 
{
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	private boolean editable = false;
	private boolean conFiltro = true;
	
	public Integer permisos = null;
	private MapeoNoConformidades mapeo=null;
	private MapeoNoConformidades mapeoOrig=null;
	private NoConformidadesView app = null;
	
    public NoConformidadesGrid(NoConformidadesView r_app , Integer r_permisos, ArrayList<MapeoNoConformidades> r_vector) 
    {
        this.permisos=r_permisos;
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("NO CONFORMIDADES EN EMBOTELLADO");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoNoConformidades.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		if (this.permisos<40)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.NONE);
		}
		else if (this.permisos<50)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.SINGLE);
		}
		else 
		{
			if (this.permisos!=99) setEditorEnabled(true);
			this.setSeleccion(SelectionMode.SINGLE);
		}
    }
    

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	
    	this.mapeoOrig = new MapeoNoConformidades();
    	this.mapeo=((MapeoNoConformidades) getEditedItemId());
    	
    	if (this.permisos<99) 
    	{
    		this.mapeoOrig.setArticulo(this.mapeo.getArticulo());
    		this.mapeoOrig.setCantidad(this.mapeo.getCantidad());
    		this.mapeoOrig.setCorrectiva(this.mapeo.getCorrectiva());
    		this.mapeoOrig.setDia(this.mapeo.getDia());
    		this.mapeoOrig.setFechaCierre(this.mapeo.getFechaCierre());
    		this.mapeoOrig.setFechaImplantacion(this.mapeo.getFechaImplantacion());
    		this.mapeoOrig.setFechaNotificacion(this.mapeo.getFechaNotificacion());
    		this.mapeoOrig.setFechaRevision(this.mapeo.getFechaRevision());
    		this.mapeoOrig.setIdParte(this.mapeo.getIdParte());
    		this.mapeoOrig.setIncidencia(this.mapeo.getIncidencia());
    		this.mapeoOrig.setInvestigacion(this.mapeo.getInvestigacion());
    		this.mapeoOrig.setNotificador(this.mapeo.getNotificador());
    		this.mapeoOrig.setNumerador(this.mapeo.getNumerador());
    		this.mapeoOrig.setObservaciones(this.mapeo.getObservaciones());
    		this.mapeoOrig.setOrigen(this.mapeo.getOrigen());
    		this.mapeoOrig.setResolucion(this.mapeo.getResolucion());
    		this.mapeoOrig.setResponsable(this.mapeo.getResponsable());
    		this.mapeoOrig.setRevisado(this.mapeo.getRevisado());
    		this.mapeoOrig.setSeguimiento(this.mapeo.getSeguimiento());
    		this.mapeoOrig.setTercero(this.mapeo.getTercero());
    		this.mapeoOrig.setDocumento(this.mapeo.getDocumento());
    		this.mapeoOrig.setLinea(this.mapeo.getLinea());
    		this.mapeoOrig.setFase(this.mapeo.getFase());
    	}
    	if (this.permisos<70) 
    	{
    		this.mapeoOrig.setArticulo(this.mapeo.getArticulo());
    		this.mapeoOrig.setCantidad(this.mapeo.getCantidad());
    		this.mapeoOrig.setDia(this.mapeo.getDia());
    		this.mapeoOrig.setFechaNotificacion(this.mapeo.getFechaNotificacion());
    		this.mapeoOrig.setIncidencia(this.mapeo.getIncidencia());
    		this.mapeoOrig.setNotificador(this.mapeo.getNotificador());
    		this.mapeoOrig.setNumerador(this.mapeo.getNumerador());
    		this.mapeoOrig.setObservaciones(this.mapeo.getObservaciones());
    		this.mapeoOrig.setOrigen(this.mapeo.getOrigen());
    		this.mapeoOrig.setDocumento(this.mapeo.getDocumento());
    		this.mapeoOrig.setLinea(this.mapeo.getLinea());
    		this.mapeoOrig.setLinea(this.mapeo.getLinea());
    		this.mapeoOrig.setFase(this.mapeo.getFase());
    	}
    	
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("estado","img", "mail", "imagen", "euro", "programacion", "linea", "dia", "numerador", "fechaNotificacion", "notificador","documento", "articulo", "cantidad", "origen", "incidencia", "investigacion", "tercero", "fechaRevision", "revisado", "resolucion", "correctiva", "fechaImplantacion", "responsable" , "seguimiento" , "fechaCierre", "observaciones", "idProgramacion","correo","correo2","correo3", "observacionCorreo", "adjuntarPdf");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("dia", "85");
    	this.widthFiltros.put("fechaNotificacion", "85");
    	this.widthFiltros.put("fechaImplantacion", "85");
    	this.widthFiltros.put("fechaCierre", "85");
    	this.widthFiltros.put("articulo", "85");
    	this.widthFiltros.put("incidencia", "225");
    	this.widthFiltros.put("numerador", "100");
    	this.widthFiltros.put("documento", "100");
    	this.widthFiltros.put("linea", "120");
    	this.widthFiltros.put("notificador", "120");
    	this.widthFiltros.put("origen", "100");    
    	this.widthFiltros.put("tercero", "100");
    	this.widthFiltros.put("cantidad", "85");
    	
    	this.getColumn("mail").setHeaderCaption("");
    	this.getColumn("mail").setSortable(false);
    	this.getColumn("mail").setWidth(new Double(50));

    	this.getColumn("imagen").setHeaderCaption("");
    	this.getColumn("imagen").setSortable(false);
    	this.getColumn("imagen").setWidth(new Double(50));

    	this.getColumn("euro").setHeaderCaption("");
    	this.getColumn("euro").setSortable(false);
    	this.getColumn("euro").setWidth(new Double(50));

    	this.getColumn("programacion").setHeaderCaption("");
    	this.getColumn("programacion").setSortable(false);
    	this.getColumn("programacion").setWidth(new Double(50));

    	this.getColumn("linea").setHeaderCaption("Linea");
    	this.getColumn("linea").setWidth(120);

    	this.getColumn("dia").setHeaderCaption("DIA");
    	this.getColumn("dia").setWidthUndefined();
    	this.getColumn("dia").setWidth(120);
    	this.getColumn("numerador").setHeaderCaption("Numero");
    	this.getColumn("numerador").setWidth(120);
    	this.getColumn("documento").setHeaderCaption("Documento");
    	this.getColumn("documento").setWidth(120);
    	this.getColumn("articulo").setHeaderCaption("CODIGO");
    	this.getColumn("articulo").setWidth(120);
    	this.getColumn("cantidad").setHeaderCaption("CANT.");
    	this.getColumn("cantidad").setWidth(120);
    	this.getColumn("fechaNotificacion").setHeaderCaption("Fecha Not.");
    	this.getColumn("fechaNotificacion").setWidth(120);
    	this.getColumn("fechaImplantacion").setHeaderCaption("Fecha Impl.");
    	this.getColumn("fechaImplantacion").setWidth(120);
    	this.getColumn("fechaCierre").setHeaderCaption("Fecha Cierre");
    	this.getColumn("fechaCierre").setWidth(120);

    	this.getColumn("notificador").setHeaderCaption("Por:");
    	this.getColumn("notificador").setWidth(150);
    	this.getColumn("origen").setHeaderCaption("Origen");
    	this.getColumn("origen").setWidth(120);
    	this.getColumn("incidencia").setHeaderCaption("Incidencia");
    	this.getColumn("incidencia").setWidth(300);
    	this.getColumn("investigacion").setHeaderCaption("Investigacion");
    	this.getColumn("investigacion").setWidth(300);
    	this.getColumn("correctiva").setHeaderCaption("Correctiva");
    	this.getColumn("correctiva").setWidth(300);
    	this.getColumn("tercero").setHeaderCaption("Tercero");
    	this.getColumn("tercero").setWidth(140);
    	this.getColumn("resolucion").setHeaderCaption("Resolucion");
    	this.getColumn("resolucion").setWidth(150);
    	this.getColumn("responsable").setHeaderCaption("Responsable");
    	this.getColumn("responsable").setWidth(150);
    	this.getColumn("seguimiento").setHeaderCaption("Seguimiento");
    	this.getColumn("seguimiento").setWidth(150);
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("observaciones").setWidth(300);
    	this.getColumn("fiveWhy").setHeaderCaption("5 PQ's");
    	this.getColumn("fiveWhy").setWidth(300);

    	this.getColumn("idqc_incidencias").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idParte").setHidden(true);
    	this.getColumn("status").setHidden(true);
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("descripcion").setHidden(true);
    	this.getColumn("img").setHidden(true);
    	this.getColumn("fase").setHidden(true);
    	this.getColumn("fechaRevision").setHidden(true);
    	this.getColumn("revisado").setHidden(true);
    	this.getColumn("idProgramacion").setHidden(true);
    	this.getColumn("programacion").setHidden(true);
    	this.getColumn("correo").setHidden(true);
    	this.getColumn("correo2").setHidden(true);
    	this.getColumn("correo3").setHidden(true);
    	this.getColumn("observacionCorreo").setHidden(true);
    	this.getColumn("adjuntarPdf").setHidden(true);
    	
    	
    	this.getColumn("dia").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaNotificacion").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaRevision").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaImplantacion").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaCierre").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoNoConformidades) {
                MapeoNoConformidades progRow = (MapeoNoConformidades)bean;
                // The actual description text is depending on the application
                if ("numerador".equals(cell.getPropertyId()))
                {
                	switch (progRow.getEstado())
                	{
                		case "C":
                		{
                			descriptionText = "Cerrada";
                			break;
                		}
                		case "R":
                		{
                			descriptionText = "Resuelta";
                			break;
                		}
                		case "I":
                		{
                			descriptionText = "Implantacion";
                			break;
                		}
                		case "P":
                		{
                			descriptionText = "Revisada";
                			break;
                		}
                		case "N":
                		{
                			descriptionText = "Nueva";
                			break;
                		}
                	}
                	
                }
                else if ("programacion".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Programación";
                }
                else if ("euro".equals(cell.getPropertyId()))
                {
                	descriptionText = "Tiempos Perdidos";
                }                
                else if ("imagen".equals(cell.getPropertyId()))
                {
                	descriptionText = "Adjunar Imagenes";
                }
                else if ("mail".equals(cell.getPropertyId()))
                {
                	descriptionText = "Enviar por mail";
                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            boolean hayImagen = false;
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strEstado=cellReference.getValue().toString();
            	}
            	if ("img".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("S")) hayImagen=false; else hayImagen=true;
            	}
            	if ("numerador".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "C":
	        			{
	        				return "Rcell-normal";
	        			}
	        			case "R":
	        			{
	        				return "Rcell-darkgreen";
	        			}
	        			case "I":
	        			{
	        				return "Rcell-green";
	        			}
	        			case "P":
	        			{
	        				return "Rcell-warning";
	        			}
	        			case "N":
	        			{
	        				return "Rcell-error";
	        			}

	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	if ( "programacion".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonProgramacion";
            	}
            	if ( "imagen".equals(cellReference.getPropertyId()))
            	{
            		if (hayImagen) return "cell-nativebuttonImagen"; return "cell-nativebuttonImagenVerde"; 
            	}
            	if ( "mail".equals(cellReference.getPropertyId()))
            	{
            		return "nativebutton"; 
            	}
            	if ( "euro".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEuro";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
        		mapeo = (MapeoNoConformidades) getEditedItemId();
		        
	        	consultaNoConformidadesServer cs = new consultaNoConformidadesServer(CurrentUser.get());
	        	
	        	actualizar=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		mapeo = (MapeoNoConformidades) getEditedItemId();
	        		mapeoOrig=(MapeoNoConformidades) getEditedItemId();
	        		
	        		consultaNoConformidadesServer cs = new consultaNoConformidadesServer(CurrentUser.get());
		        
	        		if (mapeo.getIdqc_incidencias()!=null)
		        	{
	        			if (permisos<99) 
	        	    	{
	        	    		mapeo.setArticulo(mapeoOrig.getArticulo());
	        	    		mapeo.setCantidad(mapeoOrig.getCantidad());
	        	    		mapeo.setCorrectiva(mapeoOrig.getCorrectiva());
	        	    		mapeo.setDia(mapeoOrig.getDia());
	        	    		mapeo.setFechaCierre(mapeoOrig.getFechaCierre());
	        	    		mapeo.setFechaImplantacion(mapeoOrig.getFechaImplantacion());
	        	    		mapeo.setFechaNotificacion(mapeoOrig.getFechaNotificacion());
	        	    		mapeo.setFechaRevision(mapeoOrig.getFechaRevision());
	        	    		mapeo.setIdParte(mapeoOrig.getIdParte());
	        	    		mapeo.setIncidencia(mapeoOrig.getIncidencia());
	        	    		mapeo.setInvestigacion(mapeoOrig.getInvestigacion());
	        	    		mapeo.setNotificador(mapeoOrig.getNotificador());
	        	    		mapeo.setNumerador(mapeoOrig.getNumerador());
	        	    		mapeo.setObservaciones(mapeoOrig.getObservaciones());
	        	    		mapeo.setOrigen(mapeoOrig.getOrigen());
	        	    		mapeo.setResolucion(mapeoOrig.getResolucion());
	        	    		mapeo.setResponsable(mapeoOrig.getResponsable());
	        	    		mapeo.setRevisado(mapeoOrig.getRevisado());
	        	    		mapeo.setSeguimiento(mapeoOrig.getSeguimiento());
	        	    		mapeo.setTercero(mapeoOrig.getTercero());
	        	    		mapeo.setDocumento(mapeoOrig.getDocumento());
	        	    		mapeo.setLinea(mapeoOrig.getLinea());
	        	    		mapeo.setFase(mapeoOrig.getFase());
        				}
	        			
	        	    	if (permisos<70) 
	        	    	{
	        	    		mapeo.setArticulo(mapeoOrig.getArticulo());
	        	    		mapeo.setCantidad(mapeoOrig.getCantidad());
	        	    		mapeo.setDia(mapeoOrig.getDia());
	        	    		mapeo.setFechaNotificacion(mapeoOrig.getFechaNotificacion());
	        	    		mapeo.setIncidencia(mapeoOrig.getIncidencia());
	        	    		mapeo.setNotificador(mapeoOrig.getNotificador());
	        	    		mapeo.setNumerador(mapeoOrig.getNumerador());
	        	    		mapeo.setObservaciones(mapeoOrig.getObservaciones());
	        	    		mapeo.setOrigen(mapeoOrig.getOrigen());
	        	    		mapeo.setDocumento(mapeoOrig.getDocumento());
	        	    		mapeo.setLinea(mapeoOrig.getLinea());
	        	    		mapeo.setFase(mapeoOrig.getFase());
	        	    	}
	        			
		        		cs.guardarCambios(mapeo);
		        		mapeo.setStatus(mapeo.getStatus()+1);
		        		
		        		((ArrayList<MapeoNoConformidades>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoNoConformidades>) vector).add(mapeo);

		    			refreshAllRows();
		    			scrollTo(mapeo);
		    			select(mapeo);
		        	}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoNoConformidades mapeo = (MapeoNoConformidades) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("programacion"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
            		{
            			pantallaLineasProgramaciones vt = null;
        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen No Conformidad");
        	    		getUI().addWindow(vt);
            		}
            	}
            	else if (event.getPropertyId().toString().equals("imagen"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
            		{
            			pantallaImagenesNoConformidades vt = null;
        	    		vt= new pantallaImagenesNoConformidades(mapeo, "Imagenes No Conformidad");
        	    		getUI().addWindow(vt);
            		}

            	}            	
            	else if (event.getPropertyId().toString().equals("mail"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
            		{

        	    		PeticionMail vm = null;
        	    		vm= new PeticionMail((NoConformidadesGrid) event.getSource(), mapeo, "Peticion Mail Envío Incidencia");
        	    		getUI().addWindow(vm);
            		}
            		
            	}            	
            	else if (event.getPropertyId().toString().equals("euro"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0 && (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Calidad")
            				|| (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))))
            		{
            			pantallaValoracionNoConformidades vt = null;
        	    		vt= new pantallaValoracionNoConformidades(mapeo.getLinea(), mapeo.getIdqc_incidencias());
        	    		getUI().addWindow(vt);
            		}
            	}            	
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("mail");
		this.camposNoFiltrar.add("imagen");
		this.camposNoFiltrar.add("euro");
		this.camposNoFiltrar.add("programacion");
		this.camposNoFiltrar.add("observaciones");
		this.camposNoFiltrar.add("estado");
		this.camposNoFiltrar.add("seguimiento");
		this.camposNoFiltrar.add("revisado");
		this.camposNoFiltrar.add("resolucion");
		this.camposNoFiltrar.add("correctiva");
		this.camposNoFiltrar.add("investigacion");
		this.camposNoFiltrar.add("responsable");
		this.camposNoFiltrar.add("seguimiento_ac");
		this.camposNoFiltrar.add("fiveWhy");
	}

	public void generacionPdf(MapeoNoConformidades r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	public void envioIncidencia(MapeoNoConformidades r_mapeo)
	{
		String cuerpo = null;
		
		consultaNoConformidadesServer cps = consultaNoConformidadesServer.getInstance(CurrentUser.get());
		String pdfGenerado = cps.imprimirNoConformidad(r_mapeo.getIdqc_incidencias(), "Resumen");
		
		String cuentaDestino = r_mapeo.getCorreo();
		String cuentaCopia = r_mapeo.getCorreo2();
		String cuentaCopia2 = r_mapeo.getCorreo3();
		
		String asunto = "Envío Incidencia " + r_mapeo.getIdqc_incidencias().toString();
		
		if (r_mapeo.getObservacionCorreo()==null || r_mapeo.getObservacionCorreo().length()==0)
		{
			cuerpo = "Adjunto enviamos la incidencia: " + r_mapeo.getIdqc_incidencias().toString();
		}
		else
		{
			cuerpo = r_mapeo.getObservacionCorreo();
		}
		
		
		HashMap adjuntos = null;
		
		if (r_mapeo.isAdjuntarPdf())
		{
			adjuntos = new HashMap();
			adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
			adjuntos.put("nombre", "incidencia-" + r_mapeo.getIdqc_incidencias().toString() + ".pdf");
		}
		
		ClienteMail.getInstance().envioMail(cuentaDestino, cuentaCopia, cuentaCopia2, asunto , cuerpo, adjuntos, true);

	}
	
	public String guardarCambios(MapeoNoConformidades r_mapeo, MapeoNoConformidades r_mapeoPrevio)
	{
		String rdo =null;
		consultaNoConformidadesServer cus = new consultaNoConformidadesServer(CurrentUser.get());
		if (r_mapeo!=null)
		{
			rdo = cus.guardarCambios(r_mapeo);
			if (rdo== null)
			{
				r_mapeo.setStatus(r_mapeo.getStatus()+1);
				
				((ArrayList<MapeoNoConformidades>) vector).add(r_mapeo);
				
				if (r_mapeoPrevio!=null)
				{
					rdo = cus.guardarCambios(r_mapeoPrevio);
					if (rdo==null)
					{
						r_mapeoPrevio.setStatus(r_mapeoPrevio.getStatus()+1);
						
						((ArrayList<MapeoNoConformidades>) vector).add(r_mapeoPrevio);
	
					}
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError(rdo);
			}
			
			if (!this.vector.isEmpty())
			{
				
				Indexed indexed = this.getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            this.removeAllColumns();
	            
	            this.generarGrid();
    
				this.sort("numerador");
				this.select(r_mapeo);
				this.scrollTo(r_mapeo);
			}
		}		
		return rdo;
	}
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
}


