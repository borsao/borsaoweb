package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoValoracionNoConformidades extends MapeoGlobal
{
    private Integer idqc_incidencias;
    private Integer tiempoParada;
    private Integer tiempoInterrupcion;
    private Integer tiempoRetrabajo;
    private Integer tiempoRetrabajomp;
    private Integer velocidadHabitual;
    private Integer velocidadTrabajo;
    private Integer tiempoVelocidad;
    
    private String parada;
    private String interrupcion;
    private String retrabajo;
    private String retrabajomp;    
    private String velocidad;
    

    private Double importeGlobal;
    
	public MapeoValoracionNoConformidades()
	{
	}

	public Integer getIdqc_incidencias() {
		return idqc_incidencias;
	}

	public void setIdqc_incidencias(Integer idqc_incidencias) {
		this.idqc_incidencias = idqc_incidencias;
	}

	public Integer getTiempoParada() {
		return tiempoParada;
	}

	public void setTiempoParada(Integer tiempoParada) {
		this.tiempoParada = tiempoParada;
	}

	public Integer getTiempoInterrupcion() {
		return tiempoInterrupcion;
	}

	public void setTiempoInterrupcion(Integer tiempoInterrupcion) {
		this.tiempoInterrupcion = tiempoInterrupcion;
	}

	public Integer getTiempoRetrabajo() {
		return tiempoRetrabajo;
	}

	public void setTiempoRetrabajo(Integer tiempoRetrabajo) {
		this.tiempoRetrabajo = tiempoRetrabajo;
	}

	public Integer getVelocidadHabitual() {
		return velocidadHabitual;
	}

	public void setVelocidadHabitual(Integer velocidadHabitual) {
		this.velocidadHabitual = velocidadHabitual;
	}

	public Integer getVelocidadTrabajo() {
		return velocidadTrabajo;
	}

	public void setVelocidadTrabajo(Integer velocidadTrabajo) {
		this.velocidadTrabajo = velocidadTrabajo;
	}

	public Integer getTiempoVelocidad() {
		return tiempoVelocidad;
	}

	public void setTiempoVelocidad(Integer tiempoVelocidad) {
		this.tiempoVelocidad = tiempoVelocidad;
	}

	public String getParada() {
		return parada;
	}

	public void setParada(String parada) {
		this.parada = parada;
	}

	public String getInterrupcion() {
		return interrupcion;
	}

	public void setInterrupcion(String interrupcion) {
		this.interrupcion = interrupcion;
	}

	public String getRetrabajo() {
		return retrabajo;
	}

	public void setRetrabajo(String retrabajo) {
		this.retrabajo = retrabajo;
	}

	public String getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(String velocidad) {
		this.velocidad = velocidad;
	}

	public Double getImporteGlobal() {
		return importeGlobal;
	}

	public void setImporteGlobal(Double importeGlobal) {
		this.importeGlobal = importeGlobal;
	}

	public Integer getTiempoRetrabajomp() {
		return tiempoRetrabajomp;
	}

	public void setTiempoRetrabajomp(Integer tiempoRetrabajomp) {
		this.tiempoRetrabajomp = tiempoRetrabajomp;
	}

	public String getRetrabajomp() {
		return retrabajomp;
	}

	public void setRetrabajomp(String retrabajomp) {
		this.retrabajomp = retrabajomp;
	}
}