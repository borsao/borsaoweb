package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.vaadin.haijian.ExcelExporter;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionFormatoImpresion;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.NoConformidadesGrid;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class NoConformidadesView extends GridViewRefresh {

	public static final String VIEW_NAME = "Incidencias CALIDAD";
	public consultaNoConformidadesServer cus =null;
	public boolean modificando = false;
	public ComboBox cmbEstado = null;
	public ComboBox cmbLinea = null;
	public ComboBox cmbBodega = null;
	public ComboBox cmbEjercicio = null;
	
	private final String titulo = "Incidencias CALIDAD";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private Integer permisos = 0;	
	private OpcionesForm form = null;
	private OpcionesFormSimple formSimple = null;
	private Button opcCompletar = null;
//	private Button opcImprimir = null;
	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public NoConformidadesView() 
    {
    	opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaNoConformidadesServer.getInstance(CurrentUser.get());
		
		this.permisos = new Integer(this.cus.comprobarAccesos());
		this.permisos=99;
		if (this.permisos==99) this.intervaloRefresco=0;
    	
		
    	if (this.permisos==0)
    	{
    		Notificaciones.getInstance().mensajeError("No tienes acceso a este programa");
    		this.destructor();
    	}
    	else
    	{
    		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    		
    		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    		
//    		this.opcImprimir= new Button();    	
//    		this.opcImprimir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
//    		this.opcImprimir.setIcon(FontAwesome.PRINT);

    		this.opcExcel = new ExcelExporter();    	  	
    		this.opcExcel.setDateFormat("yyyy-MM-dd");
    		this.opcExcel.setIcon(FontAwesome.FILE_EXCEL_O);
    		this.opcExcel.setStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcExcel.setStyleName(ValoTheme.BUTTON_TINY);
    		this.opcExcel.setDownloadFileName("programacion");
    		
    		this.opcCompletar= new Button("Completar");    	
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcCompletar.setIcon(FontAwesome.CHECK_CIRCLE);

    		this.cmbEstado= new ComboBox("Situacion No Conformidades");    		
    		this.cmbEstado.setNewItemsAllowed(false);
    		this.cmbEstado.setNullSelectionAllowed(false);
    		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);

    		this.cmbBodega= new ComboBox("Instalación");    		
    		this.cmbBodega.setNewItemsAllowed(false);
    		this.cmbBodega.setNullSelectionAllowed(false);
    		this.cmbBodega.addStyleName(ValoTheme.COMBOBOX_TINY);
    		
    		this.cmbLinea= new ComboBox("Linea Produccion");    		
    		this.cmbLinea.setNewItemsAllowed(false);
    		this.cmbLinea.setNullSelectionAllowed(false);
    		this.cmbLinea.addStyleName(ValoTheme.COMBOBOX_TINY);

    		this.cmbEjercicio= new ComboBox("Ejercicio");    		
    		this.cmbEjercicio.setNewItemsAllowed(false);
    		this.cmbEjercicio.setNullSelectionAllowed(false);
    		this.cmbEjercicio.addStyleName(ValoTheme.COMBOBOX_TINY);

    		this.cabLayout.addComponent(this.cmbEjercicio);
    		this.cabLayout.addComponent(this.cmbLinea);
    		this.cabLayout.addComponent(this.cmbEstado);
    		this.cabLayout.addComponent(this.cmbBodega);
//    		this.cabLayout.setComponentAlignment(this.opcImprimir,Alignment.BOTTOM_LEFT);
//    		this.cabLayout.addComponent(this.opcExcel);
//    		this.cabLayout.setComponentAlignment(this.opcExcel, Alignment.BOTTOM_LEFT);
//    		this.cabLayout.addComponent(this.opcCompletar);
//    		this.cabLayout.setComponentAlignment(this.opcCompletar,Alignment.BOTTOM_LEFT);
    		
    		this.cargarCombo();
    		this.cargarListeners();
    		this.establecerModo();
    		
    		this.generarGrid(opcionesEscogidas);
    	}
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoNoConformidades> r_vector=null;
    	MapeoNoConformidades mapeoNoConformidades =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoNoConformidades=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	mapeoNoConformidades.setEstado(this.cmbEstado.getValue().toString());
    	mapeoNoConformidades.setBodega(this.cmbBodega.getValue().toString());
    	mapeoNoConformidades.setLinea(this.cmbLinea.getValue().toString());
    	r_vector=this.cus.datosNoConformidadesGlobal(mapeoNoConformidades, cmbEjercicio.getValue().toString());
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null && (eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") || eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    		this.form.setCreacion(true);
    		this.form.setBusqueda(false);
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(false);    	
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);
    	}
    	else if (this.form!=null && (eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") || eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.form.setCreacion(true);
    		this.form.setBusqueda(false);
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(false);    	
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);
    	}
    	else if (this.formSimple==null && (!eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") && !eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.formSimple = new OpcionesFormSimple(this);
    		addComponent(this.formSimple);
    		this.formSimple.setCreacion(true);
    		this.formSimple.setBusqueda(false);
    		this.formSimple.btnGuardar.setCaption("Guardar");
    		this.formSimple.btnEliminar.setEnabled(false);    	
    		this.formSimple.addStyleName("visible");
    		this.formSimple.setEnabled(true);
    	}
    	else if (this.formSimple!=null && (!eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") && !eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.formSimple.setCreacion(true);
    		this.formSimple.setBusqueda(false);
    		this.formSimple.btnGuardar.setCaption("Guardar");
    		this.formSimple.btnEliminar.setEnabled(false);    	
    		this.formSimple.addStyleName("visible");
    		this.formSimple.setEnabled(true);
    	}
    	this.modificando=false;
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null && (eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") || eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    		this.form.setCreacion(false);
    		this.form.setBusqueda(r_busqueda);
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);

        	if (r_busqueda)
        	{
        		this.form.btnGuardar.setCaption("Buscar");
    	    	this.form.btnEliminar.setEnabled(false);
        	}
        	else
        	{
        		this.form.btnGuardar.setCaption("Guardar");
        		this.form.btnEliminar.setEnabled(true);
        	}

    	}
    	else if (this.form!=null && (eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") || eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.form.setCreacion(false);
    		this.form.setBusqueda(r_busqueda);
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);

        	if (r_busqueda)
        	{
        		this.form.btnGuardar.setCaption("Buscar");
    	    	this.form.btnEliminar.setEnabled(false);
        	}
        	else
        	{
        		this.form.btnGuardar.setCaption("Guardar");
        		this.form.btnEliminar.setEnabled(true);
        	}    		
    	}
    	else if (this.formSimple==null && (!eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") && !eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.formSimple = new OpcionesFormSimple(this);
    		addComponent(this.formSimple);
    		this.formSimple.setCreacion(false);
    		this.formSimple.setBusqueda(r_busqueda);
    		this.formSimple.addStyleName("visible");
    		this.formSimple.setEnabled(true);

        	if (r_busqueda)
        	{
        		this.formSimple.btnGuardar.setCaption("Buscar");
    	    	this.formSimple.btnEliminar.setEnabled(false);
        	}
        	else
        	{
        		this.formSimple.btnGuardar.setCaption("Guardar");
        		this.formSimple.btnEliminar.setEnabled(true);
        	}
    	}
    	else if (this.formSimple!=null && (!eBorsao.get().accessControl.getNombre().toUpperCase().contains("CLAVER") && !eBorsao.get().accessControl.getNombre().toUpperCase().contains("ARI CARM") ))
    	{
    		this.formSimple.setCreacion(false);
    		this.formSimple.setBusqueda(r_busqueda);
    		this.formSimple.addStyleName("visible");
    		this.formSimple.setEnabled(true);
    		
    		if (r_busqueda)
    		{
    			this.formSimple.btnGuardar.setCaption("Buscar");
    			this.formSimple.btnEliminar.setEnabled(false);
    		}
    		else
    		{
    			this.formSimple.btnGuardar.setCaption("Guardar");
    			this.formSimple.btnEliminar.setEnabled(true);
    		}
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((NoConformidadesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((NoConformidadesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((NoConformidadesGrid) this.grid).activadaVentanaPeticion && !((NoConformidadesGrid) this.grid).ordenando)
    	{
	    	switch (this.permisos)
	    	{
		    	case 0:
		    		break;
		    	case 10:
		    		break;
		    	case 20:
		    		break;
		    	case 30:
		    		break;
		    	case 40:
		    		this.establecerBotonAccion(r_fila);
		    		break;
		    	case 50:
		    		break;
		    	case 70:
		    		break;
		    	case 80:
		    		break;
		    	case 99:
		    		this.establecerBotonAccion(r_fila);
		    		this.modificando=true;
		    		this.verForm(false);
			    	if (this.form!=null) this.form.editarNoConformidad((MapeoNoConformidades) r_fila);
			    	else if (this.formSimple!=null) this.formSimple.editarNoConformidad((MapeoNoConformidades) r_fila);
		    		break;
	    	}
    	}    		
    }
    
    public void generarGrid(MapeoNoConformidades r_mapeo)
    {
    	ArrayList<MapeoNoConformidades> r_vector=null;
    	r_vector=this.cus.datosNoConformidadesGlobal(r_mapeo, cmbEjercicio.getValue().toString());
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoNoConformidades> r_vector)
    {
    	
    	grid = new NoConformidadesGrid(this, this.permisos,r_vector);
    	if (((NoConformidadesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((NoConformidadesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
    	
    	this.cmbEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			if (isHayGrid())
    			{			
    				grid.removeAllColumns();			
    				barAndGridLayout.removeComponent(grid);
    				grid=null;			
    				MapeoNoConformidades mapeo = new MapeoNoConformidades();
    				
    				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
    				if (cmbBodega.getValue()!=null) mapeo.setBodega(cmbBodega.getValue().toString());
    				if (cmbLinea.getValue()!=null) mapeo.setLinea(cmbLinea.getValue().toString());
    				
    				generarGrid(mapeo);
    			}
    		}
    	});
    	
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoNoConformidades mapeo = new MapeoNoConformidades();
					
					if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
					if (cmbLinea.getValue()!=null) mapeo.setLinea(cmbLinea.getValue().toString());
					if (cmbBodega.getValue()!=null) mapeo.setBodega(cmbBodega.getValue().toString());
					
					generarGrid(mapeo);
				}
			}
		});
		
		this.cmbBodega.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoNoConformidades mapeo = new MapeoNoConformidades();
					
					if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
					if (cmbLinea.getValue()!=null) mapeo.setLinea(cmbLinea.getValue().toString());
					if (cmbBodega.getValue()!=null) mapeo.setBodega(cmbBodega.getValue().toString());
					
					generarGrid(mapeo);
				}
			}
		});
		
		this.cmbLinea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoNoConformidades mapeo = new MapeoNoConformidades();
					
					if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
					if (cmbLinea.getValue()!=null) mapeo.setLinea(cmbLinea.getValue().toString());
					if (cmbBodega.getValue()!=null) mapeo.setBodega(cmbBodega.getValue().toString());
					
					generarGrid(mapeo);
				}
			}
		}); 

    	this.opcCompletar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoNoConformidades mapeo=(MapeoNoConformidades) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					if (opcCompletar.getCaption().equals("Completar"))
//					{
//	 					mapeo.setEstado("T");
//					}
//					else if (opcCompletar.getCaption().equals("Reabrir"))
//					{
//						mapeo.setEstado("A");
//					}
//					else if (opcCompletar.getCaption().equals("Aceptar"))
//					{
//						mapeo.setEstado("A");
//					}
//					else if (opcCompletar.getCaption().equals("Validar"))
//					{
//						mapeo.setEstado("A");
//					}
//					
//					cus.guardarCambios(mapeo);
					if (isHayGrid())
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;
						generarGrid(opcionesEscogidas);
					}
				}				
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
		this.opcImprimir.setVisible(true);
		this.opcImprimir.setEnabled(true);

		switch (this.permisos)
		{
			case 10:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				break;
			case 20:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(true);
				this.activarBotones(false);
				break;
			case 30:
				this.setSoloConsulta(true);
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 40:
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				this.opcCompletar.setEnabled(true);
				this.opcCompletar.setVisible(true);
				break;
			case 50:
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 70:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				break;
			case 80:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				
				this.opcImprimir.setVisible(true);
				this.opcImprimir.setEnabled(true);				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				break;				
			case 99:
				this.verBotones(true);
				this.activarBotones(true);
				this.navegacion(true);
				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				setSoloConsulta(this.soloConsulta);
				break;			
		}
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCompletar.setEnabled(r_activo);
		this.opcExcel.setEnabled(r_activo);
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCompletar.setVisible(r_visibles);
		this.opcExcel.setVisible(r_visibles);
	}
	
	private void establecerBotonAccion(Object r_fila)
	{
		this.opcCompletar.setVisible(true);

    	if (r_fila!=null)
    	{
    		this.opcCompletar.setEnabled(true);
//	    	if (((MapeoNoConformidades) r_fila).getEstado().equals("A"))
//	    	{
//	    		this.opcCompletar.setCaption("Completar");    		
//	    	}
//	    	else if (((MapeoNoConformidades) r_fila).getEstado().equals("T"))
//	    	{
//	    		this.opcCompletar.setCaption("Reabrir");    		
//	    	}
//	    	else if (((MapeoNoConformidades) r_fila).getEstado().equals("N"))
//	    	{
//	    		this.opcCompletar.setCaption("Validar");    		
//	    	}
//	    	else 
	    	{
	    		if (this.permisos==99)
	    		{
	    			this.opcCompletar.setCaption("Aceptar");
	    		}
	    		else
	    		{
	    			this.opcCompletar.setCaption("");
	    			this.opcCompletar.setEnabled(false);
	    		}
	    	}
    	}
	}

    public void print() 
    {
		((NoConformidadesGrid) grid).activadaVentanaPeticion=false;
		((NoConformidadesGrid) grid).ordenando = false;
		
		MapeoNoConformidades mapeo=(MapeoNoConformidades) grid.getSelectedRow();
		
		if (form!=null) form.cerrar();
		
		if (mapeo!=null && mapeo.getIdqc_incidencias()!=null)
		{
			PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion(mapeo.getIdqc_incidencias(), "Calidad");
			getUI().addWindow(vtPeticion);
		}
		else
		{
			Notificaciones.getInstance().mensajeInformativo("Debes seleccionar el registro a imprimir");
			this.botonesGenerales(true);
			this.opcImprimir.setEnabled(true);
		}
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		this.cmbEstado.addItem("Pendientes");
		this.cmbEstado.addItem("Revisadas");
		this.cmbEstado.addItem("Todas");
		
		this.cmbEstado.setValue("Pendientes");

		this.cmbBodega.removeAllItems();
		this.cmbBodega.addItem("Tejar");
		this.cmbBodega.addItem("Capuchinos");
		
		this.cmbBodega.setValue("Tejar");
		
		this.cmbLinea.removeAllItems();
		this.cmbLinea.addItem("EMBOTELLADORA");
		this.cmbLinea.addItem("ENVASADORA");
		this.cmbLinea.addItem("BIB");
		this.cmbLinea.addItem("Todas");		
		
		this.cmbLinea.setValue("EMBOTELLADORA");
		
		this.cargarComboEjercicio();

	}
	
	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicio.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicio.setValue(vector.get(0).toString());
	}

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
}

/*
package com.vaadin.book.examples.component;


*/