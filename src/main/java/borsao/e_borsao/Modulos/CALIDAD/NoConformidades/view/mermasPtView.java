package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoMermasPt;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MermasPtGrid;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.hashToMapeoMermasPt;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaMermasPtServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class mermasPtView extends GridViewRefresh {

	public static final String VIEW_NAME = "Mermas PT";
	public consultaMermasPtServer cmpts =null;
	public boolean modificando = false;
	public ComboBox cmbEjercicio = null;
	public ComboBox cmbSemana = null;
	
	private final String titulo = "MERMAS Producto Terminado";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private Integer permisos = 0;	
	private OpcionesFormMermasPt form = null;

    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public mermasPtView() 
    {
    	opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
		this.cmpts = consultaMermasPtServer.getInstance(CurrentUser.get());
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.cmbEjercicio= new ComboBox("Ejercicio");    		
		this.cmbEjercicio.setNewItemsAllowed(false);
		this.cmbEjercicio.setNullSelectionAllowed(false);
		this.cmbEjercicio.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cmbSemana= new ComboBox("Semana");    		
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setNullSelectionAllowed(false);
		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cabLayout.addComponent(this.cmbEjercicio);
		this.cabLayout.addComponent(this.cmbSemana);
		
		this.cargarCombo();
		this.cargarListeners();
		
		
		this.opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
		this.opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoMermasPt> r_vector=null;
    	MapeoMermasPt mapeoMermasPt =null;
    	hashToMapeoMermasPt hm = new hashToMapeoMermasPt();
    	
    	mapeoMermasPt=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	r_vector=this.cmpts.datosMermasPtGlobal(mapeoMermasPt);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesFormMermasPt(this);
    		addComponent(this.form);
    		this.form.setCreacion(true);
    		this.form.setBusqueda(false);
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(false);    	
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);
    	}
    	else 
    	{
    		this.form.setCreacion(true);
    		this.form.setBusqueda(false);
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(false);    	
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);
    	}
    	this.modificando=false;
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesFormMermasPt(this);
    		addComponent(this.form);
    		this.form.setCreacion(false);
    		this.form.setBusqueda(r_busqueda);
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);

        	if (r_busqueda)
        	{
        		this.form.btnGuardar.setCaption("Buscar");
    	    	this.form.btnEliminar.setEnabled(false);
        	}
        	else
        	{
        		this.form.btnGuardar.setCaption("Guardar");
        		this.form.btnEliminar.setEnabled(true);
        	}

    	}
    	else
    	{
    		this.form.setCreacion(false);
    		this.form.setBusqueda(r_busqueda);
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);

        	if (r_busqueda)
        	{
        		this.form.btnGuardar.setCaption("Buscar");
    	    	this.form.btnEliminar.setEnabled(false);
        	}
        	else
        	{
        		this.form.btnGuardar.setCaption("Guardar");
        		this.form.btnEliminar.setEnabled(true);
        	}    		
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((MermasPtGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((MermasPtGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((MermasPtGrid) this.grid).activadaVentanaPeticion && !((MermasPtGrid) this.grid).ordenando)
    	{
    		this.modificando=true;
    		this.verForm(false);
	    	if (this.form!=null) this.form.editarMerma((MapeoMermasPt) r_fila);
    	}    		
    }
    
    public void generarGrid(MapeoMermasPt r_mapeo)
    {
    	ArrayList<MapeoMermasPt> r_vector=null;
    	r_mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
    	r_vector=this.cmpts.datosMermasPtGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoMermasPt> r_vector)
    {
    	
    	grid = new MermasPtGrid(this, r_vector);
    	if (((MermasPtGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((MermasPtGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
    	
    	this.cmbEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			if (isHayGrid())
    			{			
    				grid.removeAllColumns();			
    				barAndGridLayout.removeComponent(grid);
    				grid=null;			
    				MapeoMermasPt mapeo = new MapeoMermasPt();
    				mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
    				mapeo.setSemana(new Integer(cmbSemana.getValue().toString()));
    				generarGrid(mapeo);
    			}
    		}
    	});
    	this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			if (isHayGrid())
    			{			
    				grid.removeAllColumns();			
    				barAndGridLayout.removeComponent(grid);
    				grid=null;			
    				MapeoMermasPt mapeo = new MapeoMermasPt();
    				mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
    				mapeo.setSemana(new Integer(cmbSemana.getValue().toString()));
    				generarGrid(mapeo);
    			}
    		}
    	});
    }
    
	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
    public void print() 
    {
    }
	
	private void cargarCombo()
	{
		this.cargarComboEjercicio();
		String semana=String.valueOf(RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY()));
		int semanas = RutinasFechas.semanasAño(RutinasFechas.añoActualYYYY());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		
		this.cmbSemana.setValue(semana);
	}
	
	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicio.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicio.setValue(vector.get(0).toString());
	}

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
}

/*
package com.vaadin.book.examples.component;


*/