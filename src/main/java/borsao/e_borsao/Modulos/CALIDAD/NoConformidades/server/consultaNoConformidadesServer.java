package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoDesgloseValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoValoracionNoConformidades;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaNoConformidadesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaNoConformidadesServer instance;
	
	public consultaNoConformidadesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaNoConformidadesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaNoConformidadesServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoNoConformidades> datosNoConformidadesGlobal(MapeoNoConformidades r_mapeo, String r_ejercicio)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoNoConformidades> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_incidencias.idqc_incidencias qc_id,");
		cadenaSQL.append(" qc_incidencias.qc_dia qc_dia,");
		cadenaSQL.append(" qc_incidencias.qc_numerador qc_num,");
		cadenaSQL.append(" qc_incidencias.qc_origen qc_ori,");
		cadenaSQL.append(" qc_incidencias.qc_tercero qc_ter,");
		cadenaSQL.append(" qc_incidencias.qc_articulo qc_art,");
		cadenaSQL.append(" qc_incidencias.qc_cantidad qc_can,");
		cadenaSQL.append(" qc_incidencias.qc_notificado_por qc_not,");
		cadenaSQL.append(" qc_incidencias.qc_incidencia qc_inc,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_notificacion qc_fe_no,");
		cadenaSQL.append(" qc_incidencias.qc_investigacion qc_inv,");
		
		cadenaSQL.append(" qc_incidencias.qc_5_why qc_fpq,");

		cadenaSQL.append(" qc_incidencias.qc_revisado_por qc_rev,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_revision qc_fe_rev,");
		cadenaSQL.append(" qc_incidencias.qc_resolucion qc_res,");
		cadenaSQL.append(" qc_incidencias.qc_correctiva qc_cor,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_implantacion qc_fe_imp,");
		cadenaSQL.append(" qc_incidencias.qc_responsable_implantacion qc_re_imp,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_cierre qc_fe_cie, ");
		cadenaSQL.append(" qc_incidencias.qc_seguimiento qc_seg, ");
		
		cadenaSQL.append(" qc_incidencias.qc_seguimiento_acc_cor qc_sac, ");
		
		cadenaSQL.append(" qc_incidencias.qc_observaciones qc_obs, ");
		cadenaSQL.append(" qc_incidencias.qc_idparte_calidad qc_id_pc, ");
		cadenaSQL.append(" qc_incidencias.qc_idprogramacion qc_id_pro, ");
		cadenaSQL.append(" qc_incidencias.qc_documento qc_doc, ");
		cadenaSQL.append(" qc_incidencias.qc_linea qc_lin, ");
		cadenaSQL.append(" qc_incidencias.qc_fase qc_fas, ");
		cadenaSQL.append(" qc_incidencias.qc_bodega qc_bod, ");
		cadenaSQL.append(" e_articu.descrip_articulo ar_des ");
		cadenaSQL.append(" from qc_incidencias ");
		cadenaSQL.append(" left outer join e_articu on e_articu.articulo = qc_incidencias.qc_articulo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdqc_incidencias()!=null && r_mapeo.getIdqc_incidencias().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idqc_incidencias = '" + r_mapeo.getIdqc_incidencias() + "' ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_dia = '" + RutinasFechas.convertirDateToString(r_mapeo.getDia()) + "' ");
				}
				if (r_mapeo.getBodega()!=null && r_mapeo.getBodega().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_bodega = '" + r_mapeo.getBodega() + "' ");
				}
				if (r_mapeo.getNumerador()!=null && r_mapeo.getNumerador().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_numerador = '" + r_mapeo.getNumerador() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_origen = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getLinea()!=null && r_mapeo.getLinea().length()>0)
				{
					if (!r_mapeo.getLinea().equals("Todas"))
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" qc_linea = '" + r_mapeo.getLinea() + "' ");
					}
				}
				if (r_mapeo.getTercero()!=null && r_mapeo.getTercero().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_tercero = '" + r_mapeo.getTercero() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_cantidad = " + r_mapeo.getCantidad() + " ");
				}

				if (r_mapeo.getNotificador()!=null && r_mapeo.getNotificador().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_notificado_por = '" + r_mapeo.getNotificador() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_documento = '" + r_mapeo.getDocumento() + "' ");
				}

				if (r_mapeo.getIncidencia()!=null && r_mapeo.getIncidencia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_incidencia = '" + r_mapeo.getIncidencia() + "' ");
				}
				if (r_mapeo.getFechaNotificacion()!=null && r_mapeo.getFechaNotificacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_fecha_notificacion = '" + r_mapeo.getFechaNotificacion() + "' ");
				}
				if (r_mapeo.getInvestigacion()!=null && r_mapeo.getInvestigacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_investigacion = '" + r_mapeo.getInvestigacion() + "' ");
				}
				if (r_mapeo.getRevisado()!=null && r_mapeo.getRevisado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_revisado_por = '" + r_mapeo.getRevisado() + "' ");
				}
				if (r_mapeo.getFechaRevision()!=null && r_mapeo.getFechaRevision().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_fecha_revision = '" + r_mapeo.getFechaRevision() + "' ");
				}
				if (r_mapeo.getResolucion()!=null && r_mapeo.getResolucion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" resolucion = '" + r_mapeo.getResolucion() + "' ");
				}
				if (r_mapeo.getCorrectiva()!=null && r_mapeo.getCorrectiva().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_correctiva = '" + r_mapeo.getCorrectiva() + "' ");
				}
				if (r_mapeo.getFechaImplantacion()!=null && r_mapeo.getFechaImplantacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_fecha_implantacion = '" + r_mapeo.getFechaImplantacion() + "' ");
				}
				if (r_mapeo.getResponsable()!=null && r_mapeo.getResponsable().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_responsable_implantacion = '" + r_mapeo.getResponsable() + "' ");
				}
				if (r_mapeo.getFechaCierre()!=null && r_mapeo.getFechaCierre().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_fecha_cierre = '" + r_mapeo.getFechaCierre() + "' ");
				}
				if (r_mapeo.getSeguimiento()!=null && r_mapeo.getSeguimiento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" seguimiento = '" + r_mapeo.getSeguimiento() + "' ");
				}
				if (r_mapeo.getFase()!=null && r_mapeo.getFase().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_fase = '" + r_mapeo.getFase() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getIdParte()!=null && r_mapeo.getIdParte().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_idparte_calidad = " + r_mapeo.getIdParte() + " ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_idprogramacion = " + r_mapeo.getIdProgramacion() + " ");
				}
				if (r_mapeo.getEstado()!=null)
				{
					if (r_mapeo.getEstado().equals("Pendientes"))
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" qc_fecha_cierre is null ");	
					}
					else if (r_mapeo.getEstado().equals("Revisadas"))
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" qc_fecha_cierre is not null ");
					}
						
				}
			}
			if (r_ejercicio!=null && r_ejercicio.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" year(qc_dia) = '" + r_ejercicio + "' ");
			}
		
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by qc_numerador asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoNoConformidades>();
			
			while(rsOpcion.next())
			{
				MapeoNoConformidades mapeoNoConformidades = new MapeoNoConformidades();
				/*
				 * recojo mapeo operarios
				 */
				mapeoNoConformidades.setIdqc_incidencias(rsOpcion.getInt("qc_id"));
				mapeoNoConformidades.setDia(RutinasFechas.conversion(rsOpcion.getDate("qc_dia")));
				mapeoNoConformidades.setNumerador(rsOpcion.getInt("qc_num"));
				mapeoNoConformidades.setOrigen(rsOpcion.getString("qc_ori"));
				mapeoNoConformidades.setLinea(rsOpcion.getString("qc_lin"));
				mapeoNoConformidades.setBodega(rsOpcion.getString("qc_bod"));
				
				mapeoNoConformidades.setFase(rsOpcion.getString("qc_fas"));
				
				mapeoNoConformidades.setTercero(rsOpcion.getString("qc_ter"));
				mapeoNoConformidades.setArticulo(rsOpcion.getString("qc_art"));
				mapeoNoConformidades.setCantidad(rsOpcion.getInt("qc_can"));
				mapeoNoConformidades.setNotificador(rsOpcion.getString("qc_not"));
				mapeoNoConformidades.setIncidencia(rsOpcion.getString("qc_inc"));
				mapeoNoConformidades.setFechaNotificacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_no")));
				mapeoNoConformidades.setInvestigacion(rsOpcion.getString("qc_inv"));
				mapeoNoConformidades.setFiveWhy(rsOpcion.getString("qc_fpq"));
				mapeoNoConformidades.setRevisado(rsOpcion.getString("qc_rev"));
				mapeoNoConformidades.setFechaRevision(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_rev")));
				mapeoNoConformidades.setResolucion(rsOpcion.getString("qc_res"));
				mapeoNoConformidades.setCorrectiva(rsOpcion.getString("qc_cor"));
				mapeoNoConformidades.setFechaImplantacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_imp")));
				mapeoNoConformidades.setResponsable(rsOpcion.getString("qc_re_imp"));
				mapeoNoConformidades.setFechaCierre(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_cie")));
				mapeoNoConformidades.setSeguimiento(rsOpcion.getString("qc_seg"));
				mapeoNoConformidades.setSeguimiento_ac(rsOpcion.getString("qc_sac"));
				mapeoNoConformidades.setObservaciones(rsOpcion.getString("qc_obs"));
				mapeoNoConformidades.setDocumento(rsOpcion.getString("qc_doc"));
				mapeoNoConformidades.setIdParte(rsOpcion.getInt("qc_id_pc"));
				mapeoNoConformidades.setIdProgramacion(rsOpcion.getInt("qc_id_pro"));
				
				mapeoNoConformidades.setDescripcion(rsOpcion.getString("ar_des"));
				
				boolean hayImg = this.hayImagen(rsOpcion.getInt("qc_id"));
				
				if (hayImg) mapeoNoConformidades.setImg("S"); else mapeoNoConformidades.setImg("N");
				
				if (mapeoNoConformidades.getFechaCierre()!=null)
					mapeoNoConformidades.setEstado("C");
				else if (mapeoNoConformidades.getFechaImplantacion()!=null)
					mapeoNoConformidades.setEstado("I");
				else if (mapeoNoConformidades.getFechaRevision()!=null)
					mapeoNoConformidades.setEstado("P");
				else 
					mapeoNoConformidades.setEstado("N");

				vector.add(mapeoNoConformidades);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public MapeoNoConformidades datosNoConformidades(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		MapeoNoConformidades mapeoNoConformidades = null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_incidencias.idqc_incidencias qc_id,");
		cadenaSQL.append(" qc_incidencias.qc_dia qc_dia,");
		cadenaSQL.append(" qc_incidencias.qc_numerador qc_num,");
		cadenaSQL.append(" qc_incidencias.qc_origen qc_ori,");
		cadenaSQL.append(" qc_incidencias.qc_tercero qc_ter,");
		cadenaSQL.append(" qc_incidencias.qc_articulo qc_art,");
		cadenaSQL.append(" qc_incidencias.qc_cantidad qc_can,");
		cadenaSQL.append(" qc_incidencias.qc_notificado_por qc_not,");
		cadenaSQL.append(" qc_incidencias.qc_incidencia qc_inc,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_notificacion qc_fe_no,");
		cadenaSQL.append(" qc_incidencias.qc_investigacion qc_inv,");
		
		cadenaSQL.append(" qc_incidencias.qc_5_why qc_fpq,");
		cadenaSQL.append(" qc_incidencias.qc_seguimiento_acc_cor qc_sac, ");
		
		cadenaSQL.append(" qc_incidencias.qc_revisado_por qc_rev,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_revision qc_fe_rev,");
		cadenaSQL.append(" qc_incidencias.qc_resolucion qc_res,");
		cadenaSQL.append(" qc_incidencias.qc_correctiva qc_cor,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_implantacion qc_fe_imp,");
		cadenaSQL.append(" qc_incidencias.qc_responsable_implantacion qc_re_imp,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_cierre qc_fe_cie, ");
		cadenaSQL.append(" qc_incidencias.qc_seguimiento qc_seg, ");
		cadenaSQL.append(" qc_incidencias.qc_observaciones qc_obs, ");
		cadenaSQL.append(" qc_incidencias.qc_idparte_calidad qc_id_pc, ");
		cadenaSQL.append(" qc_incidencias.qc_idprogramacion qc_id_pro, ");
		cadenaSQL.append(" qc_incidencias.qc_documento qc_doc, ");
		cadenaSQL.append(" qc_incidencias.qc_linea qc_lin, ");
		cadenaSQL.append(" qc_incidencias.qc_fase qc_fas, ");
		cadenaSQL.append(" qc_incidencias.qc_bodega qc_bod, ");
		cadenaSQL.append(" e_articu.descrip_articulo ar_des ");
		cadenaSQL.append(" from qc_incidencias ");
		cadenaSQL.append(" left outer join e_articu on e_articu.articulo = qc_incidencias.qc_articulo ");
		
		try
		{
			cadenaWhere = new StringBuffer();
			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			
			if (r_id !=null && r_id > 0)
			{
				cadenaSQL.append(" where idqc_incidencias = '" + r_id + "' ");
			}
			
			cadenaSQL.append(" order by qc_numerador asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoNoConformidades = new MapeoNoConformidades();
				/*
				 * recojo mapeo operarios
				 */
				mapeoNoConformidades.setIdqc_incidencias(rsOpcion.getInt("qc_id"));
				mapeoNoConformidades.setDia(RutinasFechas.conversion(rsOpcion.getDate("qc_dia")));
				mapeoNoConformidades.setNumerador(rsOpcion.getInt("qc_num"));
				mapeoNoConformidades.setOrigen(rsOpcion.getString("qc_ori"));
				mapeoNoConformidades.setLinea(rsOpcion.getString("qc_lin"));
				mapeoNoConformidades.setBodega(rsOpcion.getString("qc_bod"));
				
				mapeoNoConformidades.setFase(rsOpcion.getString("qc_fas"));
				
				mapeoNoConformidades.setTercero(rsOpcion.getString("qc_ter"));
				mapeoNoConformidades.setArticulo(rsOpcion.getString("qc_art"));
				mapeoNoConformidades.setCantidad(rsOpcion.getInt("qc_can"));
				mapeoNoConformidades.setNotificador(rsOpcion.getString("qc_not"));
				mapeoNoConformidades.setIncidencia(rsOpcion.getString("qc_inc"));
				mapeoNoConformidades.setFechaNotificacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_no")));
				mapeoNoConformidades.setInvestigacion(rsOpcion.getString("qc_inv"));
				mapeoNoConformidades.setFiveWhy(rsOpcion.getString("qc_fpq"));
				mapeoNoConformidades.setRevisado(rsOpcion.getString("qc_rev"));
				mapeoNoConformidades.setFechaRevision(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_rev")));
				mapeoNoConformidades.setResolucion(rsOpcion.getString("qc_res"));
				mapeoNoConformidades.setCorrectiva(rsOpcion.getString("qc_cor"));
				mapeoNoConformidades.setFechaImplantacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_imp")));
				mapeoNoConformidades.setResponsable(rsOpcion.getString("qc_re_imp"));
				mapeoNoConformidades.setFechaCierre(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_cie")));
				mapeoNoConformidades.setSeguimiento(rsOpcion.getString("qc_seg"));
				mapeoNoConformidades.setSeguimiento_ac(rsOpcion.getString("qc_sac"));
				mapeoNoConformidades.setObservaciones(rsOpcion.getString("qc_obs"));
				mapeoNoConformidades.setDocumento(rsOpcion.getString("qc_doc"));
				mapeoNoConformidades.setIdParte(rsOpcion.getInt("qc_id_pc"));
				mapeoNoConformidades.setIdProgramacion(rsOpcion.getInt("qc_id_pro"));
				
				mapeoNoConformidades.setDescripcion(rsOpcion.getString("ar_des"));
				
				boolean hayImg = this.hayImagen(rsOpcion.getInt("qc_id"));
				
				if (hayImg) mapeoNoConformidades.setImg("S"); else mapeoNoConformidades.setImg("N");
				
				if (mapeoNoConformidades.getFechaCierre()!=null)
					mapeoNoConformidades.setEstado("C");
				else if (mapeoNoConformidades.getFechaImplantacion()!=null)
					mapeoNoConformidades.setEstado("I");
				else if (mapeoNoConformidades.getFechaRevision()!=null)
					mapeoNoConformidades.setEstado("P");
				else 
					mapeoNoConformidades.setEstado("N");

			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeoNoConformidades;
	}
	
	public Boolean hayNoConformidades(String r_articulo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;		

		
		cadenaSQL.append(" SELECT qc_incidencias.idqc_incidencias qc_id ");
		cadenaSQL.append(" from qc_incidencias ");
		cadenaSQL.append(" where qc_fecha_cierre is null ");
		cadenaSQL.append(" and qc_articulo = '" + r_articulo + "' ");
		
		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				return true;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
	public String imprimirNoConformidad(Integer r_id,String r_area)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigo(r_id);
		libImpresion.setRutaTxt(eBorsao.get().prop.rutaImagenesCalidad);
		
		libImpresion.setArchivoDefinitivo("/NoConformidad" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		if (r_area.equals("Rechazo"))	libImpresion.setArchivoPlantilla("noConformidadesRojo.jrxml");
		if (r_area.equals("Resumen"))	libImpresion.setArchivoPlantilla("noConformidadesInforme.jrxml");
		
		libImpresion.setBackGround("/fondoA4LogoBlancoH.jpg");

		if (r_area.equals("InformeProveedor"))
		{
			libImpresion.setArchivoPlantilla("noConformidadesProveedor.jrxml");
			libImpresion.setArchivoPlantillaDetalle(null);			
			libImpresion.setArchivoPlantillaDetalleCompilado(null);

			libImpresion.setBackGround("/fondoA4LogoAzul.jpg");			
		}
		else if (r_area.contentEquals("ProveedorValorado"))
		{
			libImpresion.setArchivoPlantilla("noConformidadesProveedorValorado.jrxml");
			libImpresion.setArchivoPlantillaDetalle("impresoNoConformidad_subreport0.jrxml");
			libImpresion.setArchivoPlantillaDetalleCompilado("impresoNoConformidad_subreport0.jasper");
			libImpresion.setBackGround("/fondoA4LogoAzul.jpg");
		}
		
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("calidad");

		resultadoGeneracion = libImpresion.generacionFinal();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public String generarInformeTiempos(Date r_desde,Date r_hasta)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		try
		{
			libImpresion.setCodigoDateD(RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_desde)));
			libImpresion.setCodigoDateH(RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_hasta)));
			libImpresion.setCodigoD(RutinasFechas.convertirDateToString(r_desde));
			libImpresion.setCodigoH(RutinasFechas.convertirDateToString(r_hasta));
			
			libImpresion.setArchivoDefinitivo("/InformeTiempos" + RutinasFechas.horaActualSinSeparador() + ".pdf");
			
			libImpresion.setArchivoPlantilla("informeTiempos.jrxml");
			libImpresion.setBackGround("/fondoA4LogoBlancoH.jpg");
			libImpresion.setArchivoPlantillaDetalle(null);
			libImpresion.setArchivoPlantillaDetalleCompilado(null);
			libImpresion.setArchivoPlantillaNotas(null);
			libImpresion.setArchivoPlantillaNotasCompilado(null);
			libImpresion.setCarpeta("calidad");
	
			resultadoGeneracion = libImpresion.generacionFinalBetweenDates();
	
			if (resultadoGeneracion == null)
				resultadoGeneracion = libImpresion.getArchivoDefinitivo();
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(qc_incidencias.idqc_incidencias) qc_sig ");
     	cadenaSQL.append(" FROM qc_incidencias ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("qc_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 1;
	}


	public String guardarNuevo(MapeoNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;

		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO qc_incidencias( ");
			cadenaSQL.append(" qc_incidencias.idqc_incidencias,");
			cadenaSQL.append(" qc_incidencias.qc_dia,");
			cadenaSQL.append(" qc_incidencias.qc_numerador, ");
			cadenaSQL.append(" qc_incidencias.qc_origen, ");
			cadenaSQL.append(" qc_incidencias.qc_tercero, ");
			cadenaSQL.append(" qc_incidencias.qc_articulo, ");
			cadenaSQL.append(" qc_incidencias.qc_cantidad, ");
			cadenaSQL.append(" qc_incidencias.qc_notificado_por, ");
			cadenaSQL.append(" qc_incidencias.qc_incidencia, ");
			cadenaSQL.append(" qc_incidencias.qc_fecha_notificacion, ");
			cadenaSQL.append(" qc_incidencias.qc_investigacion, ");
			cadenaSQL.append(" qc_incidencias.qc_5_why, ");
			cadenaSQL.append(" qc_incidencias.qc_revisado_por, ");
			cadenaSQL.append(" qc_incidencias.qc_fecha_revision, ");
			cadenaSQL.append(" qc_incidencias.qc_resolucion, ");
			cadenaSQL.append(" qc_incidencias.qc_correctiva, ");
			cadenaSQL.append(" qc_incidencias.qc_fecha_implantacion, ");
			cadenaSQL.append(" qc_incidencias.qc_responsable_implantacion, ");
			cadenaSQL.append(" qc_incidencias.qc_fecha_cierre, ");
			cadenaSQL.append(" qc_incidencias.qc_seguimiento, ");
			cadenaSQL.append(" qc_incidencias.qc_seguimiento_acc_cor, ");
			cadenaSQL.append(" qc_incidencias.qc_observaciones, ");
			cadenaSQL.append(" qc_incidencias.qc_idparte_calidad, ");
			cadenaSQL.append(" qc_incidencias.qc_idprogramacion, ");
			cadenaSQL.append(" qc_incidencias.qc_documento, ");
			cadenaSQL.append(" qc_incidencias.qc_linea, ");
			cadenaSQL.append(" qc_incidencias.qc_fase, ");
			cadenaSQL.append(" qc_incidencias.qc_bodega ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
		    String dia = "";
		    
		    
		    if (r_mapeo.getDia()!=null)
		    {
		    	dia=RutinasFechas.convertirDateToString(r_mapeo.getDia());
		    }
		    else
		    {
		    	dia = RutinasFechas.convertirDateToString(new Date());
		    }
	    	preparedStatement.setString(2, RutinasFechas.convertirAFechaMysql(dia));
	    	
		    if (r_mapeo.getNumerador()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getIdqc_incidencias());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, r_mapeo.getIdqc_incidencias());
		    }
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(4, "");
		    }
		    if (r_mapeo.getTercero()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTercero());
		    }
		    else
		    {
		    	preparedStatement.setString(5, "");
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(6, "");
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    
		    if (r_mapeo.getNotificador()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getNotificador());
		    }
		    else
		    {
		    	preparedStatement.setString(8, "");
		    }
		    if (r_mapeo.getIncidencia()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getIncidencia());
		    }
		    else
		    {
		    	preparedStatement.setString(9, "");
		    }
		    if (r_mapeo.getFechaNotificacion()!=null && r_mapeo.getFechaNotificacion().toString().length()>0)
		    {
		    	preparedStatement.setString(10, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaNotificacion())));
		    }
		    else
		    {
		    	preparedStatement.setDate(10, null);
		    }
		    if (r_mapeo.getInvestigacion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getInvestigacion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, "");
		    }
		    if (r_mapeo.getFiveWhy()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getFiveWhy());
		    }
		    else
		    {
		    	preparedStatement.setString(12, "");
		    }
		    if (r_mapeo.getRevisado()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getRevisado());
		    }
		    else
		    {
		    	preparedStatement.setString(13, "");
		    }
		    if (r_mapeo.getFechaRevision()!=null && r_mapeo.getFechaRevision().toString().length()>0)
		    {
		    	preparedStatement.setString(14, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaRevision())));
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getResolucion()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getResolucion());
		    }
		    else
		    {
		    	preparedStatement.setString(15, "");
		    }
		    if (r_mapeo.getCorrectiva()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getCorrectiva());
		    }
		    else
		    {
		    	preparedStatement.setString(16, "");
		    }
		    if (r_mapeo.getFechaImplantacion()!=null && r_mapeo.getFechaImplantacion().toString().length()>0)
		    {
		    	preparedStatement.setString(17, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaImplantacion())));
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getResponsable()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(18, "");
		    }
		    if (r_mapeo.getFechaCierre()!=null && r_mapeo.getFechaCierre().toString().length()>0)
		    {
		    	preparedStatement.setString(19, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaCierre())));
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getSeguimiento()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getSeguimiento());
		    }
		    else
		    {
		    	preparedStatement.setString(20, "");
		    }
		    
		    if (r_mapeo.getSeguimiento_ac()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getSeguimiento_ac());
		    }
		    else
		    {
		    	preparedStatement.setString(21, "");
		    }
		    
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(22, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(22, "");
		    }
		    
		    preparedStatement.setInt(23, 0);
		    
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(24, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(24, 0);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(25, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(25, "");
		    }
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(26, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(26, "");
		    }
		    if (r_mapeo.getFase()!=null)
		    {
		    	preparedStatement.setString(27, r_mapeo.getFase());
		    }
		    else
		    {
		    	preparedStatement.setString(27, "");
		    }
		    if (r_mapeo.getBodega()!=null)
		    {
		    	preparedStatement.setString(28, r_mapeo.getBodega());
		    }
		    else
		    {
		    	preparedStatement.setString(28, "");
		    }
	        preparedStatement.executeUpdate();
	        
	        if (r_mapeo.getFechaCierre()!=null)
				return "C";
			else if (r_mapeo.getFechaImplantacion()!=null)
				return "I";
			else if (r_mapeo.getFechaRevision()!=null)
				return "P";
			else 
				return "N";
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public String guardarCambios(MapeoNoConformidades r_mapeo)
	{
		String rdo = null;
		Connection con = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE qc_incidencias set ");
			cadenaSQL.append(" qc_incidencias.qc_dia =?,");
			cadenaSQL.append(" qc_incidencias.qc_numerador =?,");
			cadenaSQL.append(" qc_incidencias.qc_origen =?,");
			cadenaSQL.append(" qc_incidencias.qc_tercero =?,");
			cadenaSQL.append(" qc_incidencias.qc_articulo =?,");
			cadenaSQL.append(" qc_incidencias.qc_cantidad =?,");
			cadenaSQL.append(" qc_incidencias.qc_notificado_por =?,");
			cadenaSQL.append(" qc_incidencias.qc_incidencia =?,");
			cadenaSQL.append(" qc_incidencias.qc_fecha_notificacion =?,");
			cadenaSQL.append(" qc_incidencias.qc_investigacion =?,");
			
			cadenaSQL.append(" qc_incidencias.qc_5_why =?,");

			cadenaSQL.append(" qc_incidencias.qc_revisado_por =?,");
			cadenaSQL.append(" qc_incidencias.qc_fecha_revision =?,");
			cadenaSQL.append(" qc_incidencias.qc_resolucion =?,");
			cadenaSQL.append(" qc_incidencias.qc_correctiva =?,");
			cadenaSQL.append(" qc_incidencias.qc_fecha_implantacion =?,");
			cadenaSQL.append(" qc_incidencias.qc_responsable_implantacion =?,");
			cadenaSQL.append(" qc_incidencias.qc_fecha_cierre =?,");
			cadenaSQL.append(" qc_incidencias.qc_seguimiento = ?, ");
			
			cadenaSQL.append(" qc_incidencias.qc_seguimiento_acc_cor =?, ");
			
			cadenaSQL.append(" qc_incidencias.qc_observaciones = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_idparte_calidad = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_idprogramacion = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_documento = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_linea = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_fase = ?, ");
			cadenaSQL.append(" qc_incidencias.qc_bodega = ? ");
			cadenaSQL.append(" where qc_incidencias.idqc_incidencias = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    String dia ="";
		    
		    if (r_mapeo.getDia()!=null)
		    {
		    	dia=RutinasFechas.convertirDateToString(r_mapeo.getDia());
		    }
		    else
		    {
		    	dia = RutinasFechas.convertirDateToString(new Date());
		    }
	    	
		    preparedStatement.setString(1, RutinasFechas.convertirAFechaMysql(dia));
		    
		   
		    if (r_mapeo.getNumerador()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getNumerador());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(3, "");
		    }
		    if (r_mapeo.getTercero()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTercero());
		    }
		    else
		    {
		    	preparedStatement.setString(4, "");
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, "");
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    
		    if (r_mapeo.getNotificador()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getNotificador());
		    }
		    else
		    {
		    	preparedStatement.setString(7, "");
		    }
		    if (r_mapeo.getIncidencia()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getIncidencia());
		    }
		    else
		    {
		    	preparedStatement.setString(8, "");
		    }
		    if (r_mapeo.getFechaNotificacion()!=null && r_mapeo.getFechaNotificacion().toString().length()>0)
		    {
		    	preparedStatement.setString(9, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaNotificacion())));
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getInvestigacion()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getInvestigacion());
		    }
		    else
		    {
		    	preparedStatement.setString(10, "");
		    }
		    if (r_mapeo.getFiveWhy()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getFiveWhy());
		    }
		    else
		    {
		    	preparedStatement.setString(11, "");
		    }
		    if (r_mapeo.getRevisado()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getRevisado());
		    }
		    else
		    {
		    	preparedStatement.setString(12, "");
		    }
		    if (r_mapeo.getFechaRevision()!=null && r_mapeo.getFechaRevision().toString().length()>0)
		    {
		    	preparedStatement.setString(13, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaRevision())));
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getResolucion()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getResolucion());
		    }
		    else
		    {
		    	preparedStatement.setString(14, "");
		    }
		    if (r_mapeo.getCorrectiva()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getCorrectiva());
		    }
		    else
		    {
		    	preparedStatement.setString(15, "");
		    }
		    if (r_mapeo.getFechaImplantacion()!=null && r_mapeo.getFechaImplantacion().toString().length()>0)
		    {
		    	preparedStatement.setString(16, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaImplantacion())));
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getResponsable()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(17, "");
		    }
		    if (r_mapeo.getFechaCierre()!=null && r_mapeo.getFechaCierre().toString().length()>0)
		    {
		    	preparedStatement.setString(18, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaCierre())));
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getSeguimiento()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getSeguimiento());
		    }
		    else
		    {
		    	preparedStatement.setString(19, "");
		    }
		    if (r_mapeo.getSeguimiento_ac()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getSeguimiento_ac());
		    }
		    else
		    {
		    	preparedStatement.setString(20, "");
		    }
		    
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(21, "");
		    }
		    
		    preparedStatement.setInt(22, 0);
		   
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(23, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(23, 0);
		    }
		    
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(24, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(24, "");
		    }
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(25, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(25, "");
		    }
		    if (r_mapeo.getFase()!=null)
		    {
		    	preparedStatement.setString(26, r_mapeo.getFase());
		    }
		    else
		    {
		    	preparedStatement.setString(26, "");
		    }
		    if (r_mapeo.getBodega()!=null)
		    {
		    	preparedStatement.setString(27, r_mapeo.getBodega());
		    }
		    else
		    {
		    	preparedStatement.setString(27, "");
		    }
		    preparedStatement.setInt(28, r_mapeo.getIdqc_incidencias());
		    
		    preparedStatement.executeUpdate();
		    
		    if (r_mapeo.getFechaCierre()!=null)
				return "C";
			else if (r_mapeo.getFechaImplantacion()!=null)
				return "I";
			else if (r_mapeo.getFechaRevision()!=null)
				return "P";
			else 
				return "N";
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			rdo=ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}
	
	public void eliminar(MapeoNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;
		
		try
		{
			con= this.conManager.establecerConexionInd();	          

			cadenaSQL.append(" DELETE FROM qc_incidencias ");            
			cadenaSQL.append(" WHERE qc_incidencias.idqc_incidencias = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			preparedStatement.executeUpdate();
			
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM qc_incidencias_img ");            
			cadenaSQL.append(" WHERE qc_incidencias_img.id_incidencia = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			preparedStatement.executeUpdate();
			
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM qc_incidencias_euro ");            
			cadenaSQL.append(" WHERE qc_incidencias_euro.id_incidencia = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			preparedStatement.executeUpdate();
			
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM qc_incidencias_euro_desglose ");            
			cadenaSQL.append(" WHERE qc_incidencias_euro_desglose.id_incidencia = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}

	public MapeoValoracionNoConformidades datosValoracionNoConformidadesGlobal(Integer r_id)
	{
		ResultSet rsOpcion = null;
		Connection con = null;
		Statement cs = null;		

		MapeoValoracionNoConformidades mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_incidencias_euro.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_euro.parada par,");
		cadenaSQL.append(" qc_incidencias_euro.tiempo_parada tpar,");
		cadenaSQL.append(" qc_incidencias_euro.interrupcion itr,");
		cadenaSQL.append(" qc_incidencias_euro.tiempo_interrupcion tint,");
		cadenaSQL.append(" qc_incidencias_euro.retrabajo ret,");
		cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo tret,");
		cadenaSQL.append(" qc_incidencias_euro.retrabajo_mp retmp,");
		cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo_mp tretmp,");
		cadenaSQL.append(" qc_incidencias_euro.velocidad vel,");
		cadenaSQL.append(" qc_incidencias_euro.velocidad_normal veln,");
		cadenaSQL.append(" qc_incidencias_euro.velocidad_trabajo velt,");
		cadenaSQL.append(" qc_incidencias_euro.tiempo_velocidad tvel,");
		cadenaSQL.append(" qc_incidencias_euro.precio_hora pre,");
		cadenaSQL.append(" qc_incidencias_euro.personalRetrabajo rper, ");
		cadenaSQL.append(" qc_incidencias_euro.personalRetrabajomp rmpper, ");
		cadenaSQL.append(" qc_incidencias_euro.importe imp ");
		cadenaSQL.append(" from qc_incidencias_euro ");
		cadenaSQL.append(" where qc_incidencias_euro.id_incidencia = '" + r_id + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			mapeo = new MapeoValoracionNoConformidades();

			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdqc_incidencias(rsOpcion.getInt("qc_id"));
				mapeo.setParada(rsOpcion.getString("par"));
				mapeo.setTiempoParada(rsOpcion.getInt("tpar"));
				mapeo.setInterrupcion(rsOpcion.getString("itr"));
				mapeo.setTiempoInterrupcion(rsOpcion.getInt("tint"));
				mapeo.setRetrabajo(rsOpcion.getString("ret"));
				mapeo.setTiempoRetrabajo(rsOpcion.getInt("tret"));
				mapeo.setRetrabajomp(rsOpcion.getString("retmp"));
				mapeo.setTiempoRetrabajomp(rsOpcion.getInt("tretmp"));
				mapeo.setVelocidad(rsOpcion.getString("vel"));
				mapeo.setVelocidadHabitual(rsOpcion.getInt("veln"));
				mapeo.setVelocidadTrabajo(rsOpcion.getInt("velt"));
				mapeo.setTiempoVelocidad(rsOpcion.getInt("tvel"));
				mapeo.setImporteGlobal(rsOpcion.getDouble("imp"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
		{		
			return mapeo;
		}
		else
		{
			return null;
		}
	}
	
	public ArrayList<MapeoDesgloseValoracionNoConformidades> datosDesgloseValoracionNoConformidadesGlobal(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;		

		MapeoDesgloseValoracionNoConformidades mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		ArrayList<MapeoDesgloseValoracionNoConformidades> vector = null;
		
		cadenaSQL.append(" SELECT qc_incidencias_euro_desglose.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.area des_are,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.concepto des_con,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.costeUnitario des_cos,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.unidades des_uni,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.tiempo des_time,");
		cadenaSQL.append(" qc_incidencias_euro_desglose.ett des_ett");
		cadenaSQL.append(" from qc_incidencias_euro_desglose ");
		cadenaSQL.append(" where qc_incidencias_euro_desglose.id_incidencia = '" + r_id + "' ");
		cadenaSQL.append(" order by qc_incidencias_euro_desglose.area, qc_incidencias_euro_desglose.concepto " );
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoDesgloseValoracionNoConformidades>();
			while(rsOpcion.next())
			{
				mapeo = new MapeoDesgloseValoracionNoConformidades();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdqc_incidencias(rsOpcion.getInt("qc_id"));
				mapeo.setArea(rsOpcion.getString("des_are"));
				mapeo.setConcepto(rsOpcion.getString("des_con"));
				mapeo.setEtt(rsOpcion.getString("des_ett"));
				mapeo.setCosteUnitario(rsOpcion.getDouble("des_cos"));
				mapeo.setUnidades(rsOpcion.getDouble("des_uni"));
				mapeo.setTiempo(rsOpcion.getDouble("des_time"));
				
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}

	public String guardarNuevoValoracion(MapeoValoracionNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO qc_incidencias_euro( ");
			cadenaSQL.append(" qc_incidencias_euro.id_incidencia,");
			cadenaSQL.append(" qc_incidencias_euro.parada,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_parada,");
			cadenaSQL.append(" qc_incidencias_euro.interrupcion,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_interrupcion,");
			cadenaSQL.append(" qc_incidencias_euro.retrabajo,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo,");
			cadenaSQL.append(" qc_incidencias_euro.retrabajo_mp,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo_mp,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad_normal,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad_trabajo,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_velocidad,");
			cadenaSQL.append(" qc_incidencias_euro.precio_hora,");
			cadenaSQL.append(" qc_incidencias_euro.importe,");
			cadenaSQL.append(" qc_incidencias_euro.personalRetrabajo,");
			cadenaSQL.append(" qc_incidencias_euro.personalRetrabajomp ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
		    preparedStatement.setString(2, r_mapeo.getParada());
		    preparedStatement.setInt(3, r_mapeo.getTiempoParada());
		    preparedStatement.setString(4, r_mapeo.getInterrupcion());
		    preparedStatement.setInt(5, r_mapeo.getTiempoInterrupcion());
		    preparedStatement.setString(6, r_mapeo.getRetrabajo());
		    preparedStatement.setInt(7, r_mapeo.getTiempoRetrabajo());
		    preparedStatement.setString(8, r_mapeo.getRetrabajomp());
		    preparedStatement.setInt(9, r_mapeo.getTiempoRetrabajomp());
		    preparedStatement.setString(10, r_mapeo.getVelocidad());
		    preparedStatement.setInt(11, r_mapeo.getVelocidadHabitual());
		    preparedStatement.setInt(12, r_mapeo.getVelocidadTrabajo());
		    preparedStatement.setInt(13, r_mapeo.getTiempoVelocidad());
		    
//		    if (r_mapeo.getPrecioHora()!=null)
//		    {
//		    	preparedStatement.setDouble(14, r_mapeo.getPrecioHora());
//		    }
//		    else
//		    {
		    	preparedStatement.setDouble(14, 0);
//		    }
		    if (r_mapeo.getImporteGlobal()!=null)
		    {
		    	preparedStatement.setDouble(15, r_mapeo.getImporteGlobal());
		    }
		    else
		    {
		    	preparedStatement.setDouble(15, 0);
		    }

		    preparedStatement.setInt(16, 0);
		    preparedStatement.setInt(17, 0);
	        preparedStatement.executeUpdate();
	        
//	        if (r_mapeo.getRetrabajo().equals("S"))
//	        {
//	        	/*
//	        	 * guardar registro retrabajo
//	        	 */
//	        	
//	        	String rdo = null;
//	        	rdo = this.guardarRetrabajo(r_mapeo.getIdqc_incidencias(), "PT");
//	        	if (rdo!=null)
//	        		Notificaciones.getInstance().mensajeError("Error al guardar retrabajo desde NoConformidad PT" + rdo);
//	        }
//	        if (r_mapeo.getRetrabajomp().equals("S"))
//	        {
//	        	/*
//	        	 * guardar registro retrabajo
//	        	 */
//	        	
//	        	String rdo = null;
//	        	rdo = this.guardarRetrabajo(r_mapeo.getIdqc_incidencias(), "MP");
//	        	if (rdo!=null)
//	        		Notificaciones.getInstance().mensajeError("Error al guardar retrabajo desde NoConformidad MP" + rdo);
//	        }
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarRetrabajo(Integer r_id,String r_area)
	{
		/*
		 * TODO guardar registro retrabajo
		 */
		String rdo = null;
		
    	consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());
    	consultaNoConformidadesServer cns = consultaNoConformidadesServer.getInstance(CurrentUser.get());
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
    	
    	MapeoNoConformidades mapeoNC = new MapeoNoConformidades();
    	MapeoRetrabajos mapeoRetr = new MapeoRetrabajos();
    	
    	try
    	{
    		
    		mapeoNC = cns.datosNoConformidades(r_id);
    		
    		boolean existe = crs.comprobarRetrabajos(r_id);
    		
    		if (!existe)
    		{
		    	
		    	
		    	mapeoRetr.setIdCodigo(crs.obtenerSiguiente());
		    	mapeoRetr.setOrigen("CALIDAD");
		    	mapeoRetr.setIdNoConformidad(r_id);
		    	mapeoRetr.setFecha(RutinasFechas.convertirADate(RutinasFechas.fechaActual()));
		    	
		    	if (r_area.equals("MP"))
		    	{
		    		mapeoRetr.setArticulo(mapeoNC.getArticulo());
		    		mapeoRetr.setDescripcion(cas.obtenerDescripcionArticulo(mapeoNC.getArticulo()));
		    		mapeoRetr.setUnidades(mapeoNC.getCantidad());
		    		mapeoRetr.setFactor(new Integer("0"));
		    		mapeoRetr.setCantidad(new Integer("0"));
		    	}
		    	else if (r_area.equals("PT"))
		    	{
		    		String art = null;
		    		MapeoProgramacion mapeoPrg = new MapeoProgramacion();
		    		
		    		mapeoPrg=cps.datosProgramacionGlobal(mapeoNC.getIdProgramacion());
		    		
		    		art= mapeoPrg.getArticulo(); 
		    		mapeoRetr.setArticulo(art);
		    		mapeoRetr.setDescripcion(cas.obtenerDescripcionArticulo(art));
		    		
		    		Double fac = ces.recuperarCantidadComponente(art, mapeoNC.getArticulo());
		    		
		    		if (fac != null && fac > 0)
		    		{
		    			mapeoRetr.setUnidades(new Double(mapeoNC.getCantidad()*fac).intValue());
		    			mapeoRetr.setFactor(mapeoPrg.getFactor());
		    			mapeoRetr.setCantidad(mapeoPrg.getUnidades()/mapeoPrg.getFactor());
		    		}
		    		else
		    		{
		    			mapeoRetr.setUnidades(0);
		    			mapeoRetr.setFactor(0);
		    			mapeoRetr.setCantidad(0);
		    		}		    		
		    	}
		
		    	rdo = crs.guardarNuevo(mapeoRetr);
    		}
    	}
    	catch (Exception ex)
    	{
    		serNotif.mensajeError(ex.getMessage());
    	}
    	return rdo;
	}
	
	public String guardarNuevoDesgloseValoracion(MapeoDesgloseValoracionNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO qc_incidencias_euro_desglose( ");
			cadenaSQL.append(" qc_incidencias_euro_desglose.id_incidencia,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.area,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.concepto,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.costeUnitario,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.unidades,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.ett,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.tiempo ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
		    preparedStatement.setString(2, r_mapeo.getArea());
		    preparedStatement.setString(3, r_mapeo.getConcepto());
		    if (r_mapeo.getCosteUnitario()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getCosteUnitario());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    
		    preparedStatement.setString(6, r_mapeo.getEtt());
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return null;
	}
	
	public String guardarCambiosDesgloseValoracion(MapeoDesgloseValoracionNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con=null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE qc_incidencias_euro_desglose SET ");
			cadenaSQL.append(" qc_incidencias_euro_desglose.costeUnitario=?,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.unidades=?,");
			cadenaSQL.append(" qc_incidencias_euro_desglose.tiempo=? ");
			cadenaSQL.append(" WHERE qc_incidencias_euro_desglose.id_incidencia =?");
			cadenaSQL.append(" and qc_incidencias_euro_desglose.area =?");
			cadenaSQL.append(" and qc_incidencias_euro_desglose.concepto =?");
			cadenaSQL.append(" and qc_incidencias_euro_desglose.ett=?");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getCosteUnitario()!=null)
		    {
		    	preparedStatement.setDouble(1, r_mapeo.getCosteUnitario());
		    }
		    else
		    {
		    	preparedStatement.setDouble(1, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdqc_incidencias());
		    preparedStatement.setString(5, r_mapeo.getArea());
		    preparedStatement.setString(6, r_mapeo.getConcepto());
		    preparedStatement.setString(7, r_mapeo.getEtt());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return null;
	}
	
	public String guardarCambiosValoracion(MapeoValoracionNoConformidades r_mapeo)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con=null;
		
		try
		{
			
		    cadenaSQL.append(" UPDATE qc_incidencias_euro set ");
			cadenaSQL.append(" qc_incidencias_euro.parada =?,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_parada =?,");
			cadenaSQL.append(" qc_incidencias_euro.interrupcion =?,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_interrupcion =?,");
			cadenaSQL.append(" qc_incidencias_euro.retrabajo =?,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo=?,");
			cadenaSQL.append(" qc_incidencias_euro.retrabajo_mp =?,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_retrabajo_mp=?,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad =?,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad_normal =?,");
			cadenaSQL.append(" qc_incidencias_euro.velocidad_trabajo =?,");
			cadenaSQL.append(" qc_incidencias_euro.tiempo_velocidad =?,");
			cadenaSQL.append(" qc_incidencias_euro.precio_hora =?,");
			cadenaSQL.append(" qc_incidencias_euro.importe =?, ");
			cadenaSQL.append(" qc_incidencias_euro.personalRetrabajo=?, ");
			cadenaSQL.append(" qc_incidencias_euro.personalRetrabajomp=? "); 
			cadenaSQL.append(" where qc_incidencias_euro.id_incidencia = ? ");
			
		    con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setString(1, r_mapeo.getParada());
		    preparedStatement.setInt(2, r_mapeo.getTiempoParada());
		    preparedStatement.setString(3, r_mapeo.getInterrupcion());
		    preparedStatement.setInt(4, r_mapeo.getTiempoInterrupcion());
		    preparedStatement.setString(5, r_mapeo.getRetrabajo());
		    preparedStatement.setInt(6, r_mapeo.getTiempoRetrabajo());
		    preparedStatement.setString(7, r_mapeo.getRetrabajomp());
		    preparedStatement.setInt(8, r_mapeo.getTiempoRetrabajomp());
		    preparedStatement.setString(9, r_mapeo.getVelocidad());
		    preparedStatement.setInt(10, r_mapeo.getVelocidadHabitual());
		    preparedStatement.setInt(11, r_mapeo.getVelocidadTrabajo());
		    preparedStatement.setInt(12, r_mapeo.getTiempoVelocidad());
		    
//		    if (r_mapeo.getPrecioHora()!=null)
//		    {
//		    	preparedStatement.setDouble(13, r_mapeo.getPrecioHora());
//		    }
//		    else
//		    {
		    	preparedStatement.setDouble(13, 0);
//		    }
		    if (r_mapeo.getImporteGlobal()!=null)
		    {
		    	preparedStatement.setDouble(14, r_mapeo.getImporteGlobal());
		    }
		    else
		    {
		    	preparedStatement.setDouble(14, 0);
		    }
		    preparedStatement.setInt(15, 0);
		    preparedStatement.setInt(16, 0);
		    preparedStatement.setInt(17, r_mapeo.getIdqc_incidencias());
		    
		    preparedStatement.executeUpdate();
//		 
//		    if (r_mapeo.getRetrabajo().equals("S"))
//	        {
//	        	/*
//	        	 * guardar registro retrabajo
//	        	 */
//	        	
//	        	rdo = null;
//	        	rdo = this.guardarRetrabajo(r_mapeo.getIdqc_incidencias(), "PT");
//	        	if (rdo!=null)
//	        		Notificaciones.getInstance().mensajeError("Error al guardar retrabajo desde NoConformidad PT" + rdo);
//	        }
//	        if (r_mapeo.getRetrabajomp().equals("S"))
//	        {
//	        	/*
//	        	 * guardar registro retrabajo
//	        	 */
//	        	
//	        	rdo = null;
//	        	rdo = this.guardarRetrabajo(r_mapeo.getIdqc_incidencias(), "MP");
//	        	if (rdo!=null)
//	        		Notificaciones.getInstance().mensajeError("Error al guardar retrabajo desde NoConformidad MP" + rdo);
//	        }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			rdo=ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}
	
	public void eliminarValoracion(MapeoValoracionNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con =null;
		try
		{
			cadenaSQL.append(" DELETE FROM qc_incidencias_euro ");            
			cadenaSQL.append(" WHERE qc_incidencias_euro.id_incidencia = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			preparedStatement.executeUpdate();
			MapeoDesgloseValoracionNoConformidades mapDes = new MapeoDesgloseValoracionNoConformidades();
			mapDes.setIdqc_incidencias(r_mapeo.getIdqc_incidencias());
			this.eliminarDesgloseValoracion(mapDes);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}
	
	public void eliminarDesgloseValoracion(MapeoDesgloseValoracionNoConformidades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con=null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_incidencias_euro_desglose ");            
			cadenaSQL.append(" WHERE qc_incidencias_euro_desglose.id_incidencia = ?");

			if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
			{
				cadenaSQL.append(" and qc_incidencias_euro_desglose.area =?");
				cadenaSQL.append(" and qc_incidencias_euro_desglose.concepto =?");
				cadenaSQL.append(" and qc_incidencias_euro_desglose.ett=?");
			}			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdqc_incidencias());
			if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
			{
				preparedStatement.setString(2, r_mapeo.getArea());
				preparedStatement.setString(3, r_mapeo.getConcepto());
				preparedStatement.setString(4, r_mapeo.getEtt());
			}
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}

	public Double recuperarCosteTotalIncidencia(Integer r_incidencia)
	{
		Double valorObtenido = new Double(0);
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		
		try
		{
				  sql = " select sum(qc_incidencias_euro_desglose.costeUnitario*qc_incidencias_euro_desglose.unidades*qc_incidencias_euro_desglose.tiempo) coste from qc_incidencias_euro_desglose ";
			sql = sql + " where qc_incidencias_euro_desglose.id_incidencia = " + r_incidencia;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valorObtenido = rsValor.getDouble("coste");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rsValor!=null)
				{
					rsValor.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return valorObtenido;
	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Embotelladora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos(null);
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";
			}
			else
			{
				permisos="0";
			}
		}
		return permisos;
	}

	@Override
	public String semaforos() 
	{
		ResultSet rsOpcion = null;
		Integer resultado = null;
		ArrayList<MapeoNoConformidades> vector = null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT qc_incidencias.idqc_incidencias qc_id, ");
		cadenaSQL.append(" qc_incidencias.qc_dia qc_dia,");
		cadenaSQL.append(" qc_incidencias.qc_numerador qc_num,");
		cadenaSQL.append(" qc_incidencias.qc_origen qc_ori,");
		cadenaSQL.append(" qc_incidencias.qc_tercero qc_ter,");
		cadenaSQL.append(" qc_incidencias.qc_articulo qc_art,");
		cadenaSQL.append(" qc_incidencias.qc_cantidad qc_can,");
		cadenaSQL.append(" qc_incidencias.qc_notificado_por qc_not,");
		cadenaSQL.append(" qc_incidencias.qc_incidencia qc_inc,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_notificacion qc_fe_no,");
		cadenaSQL.append(" qc_incidencias.qc_investigacion qc_inv,");
		cadenaSQL.append(" qc_incidencias.qc_revisado_por qc_rev,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_revision qc_fe_rev,");
		cadenaSQL.append(" qc_incidencias.qc_resolucion qc_res,");
		cadenaSQL.append(" qc_incidencias.qc_correctiva qc_cor,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_implantacion qc_fe_imp,");
		cadenaSQL.append(" qc_incidencias.qc_responsable_implantacion qc_re_imp,");
		cadenaSQL.append(" qc_incidencias.qc_fecha_cierre qc_fe_cie, ");
		cadenaSQL.append(" qc_incidencias.qc_seguimiento qc_seg, ");
		cadenaSQL.append(" qc_incidencias.qc_observaciones qc_obs, ");
		cadenaSQL.append(" qc_incidencias.qc_idparte_calidad qc_id_pc, ");
		cadenaSQL.append(" qc_incidencias.qc_idprogramacion qc_id_pro, ");
		cadenaSQL.append(" qc_incidencias.qc_documento qc_doc, ");
		cadenaSQL.append(" qc_incidencias.qc_fase qc_fas, ");
		cadenaSQL.append(" qc_incidencias.qc_bodega qc_bod, ");
		cadenaSQL.append(" qc_incidencias.qc_linea qc_lin ");
		cadenaSQL.append(" from qc_incidencias ");
		
		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoNoConformidades>();
			
			while(rsOpcion.next())
			{
				MapeoNoConformidades mapeoNoConformidades = new MapeoNoConformidades();
				/*
				 * recojo mapeo operarios
				 */
				mapeoNoConformidades.setFechaNotificacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_no")));
				mapeoNoConformidades.setFechaRevision(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_rev")));
				mapeoNoConformidades.setFechaImplantacion(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_imp")));
				mapeoNoConformidades.setFechaCierre(RutinasFechas.conversion(rsOpcion.getDate("qc_fe_cie")));
				
				if (mapeoNoConformidades.getFechaCierre()!=null)
				{
					if (resultado==null) resultado = 4;
				}
				if (mapeoNoConformidades.getFechaCierre()==null &&  mapeoNoConformidades.getFechaImplantacion()!=null)
				{	
					if (resultado==null || resultado >= 3 ) resultado = 2;
				}	
				if (mapeoNoConformidades.getFechaCierre()==null &&  mapeoNoConformidades.getFechaImplantacion()==null && mapeoNoConformidades.getFechaRevision()!=null)
				{	
					if (resultado==null || resultado >= 2 ) resultado = 1;
				}	
				if (mapeoNoConformidades.getFechaCierre()==null &&  mapeoNoConformidades.getFechaImplantacion()==null && mapeoNoConformidades.getFechaRevision()==null)
				{
					resultado = 0;
				}

			}
			
			if (resultado==null) resultado = 5;
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		if (resultado==4) return "0";
		if (resultado==3) return "4";
		if (resultado==2) return "3";
		if (resultado==1) return "1";
		if (resultado==0) return "2";
		return "0";
	}

	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;		

		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where articulo between '0100000' and '0109999' order by articulo asc" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}

	private boolean hayImagen(Integer r_id)
	{
		boolean hay = false;
		
		consultaImagenesNoConformidadesServer cimgs = consultaImagenesNoConformidadesServer.getInstance(CurrentUser.get());
		hay = cimgs.hayDatosImagenes(r_id);
		
		return hay;
	}
	
}


/*
SELECT `qc_incidencias`.`idqc_incidencias`,
`qc_incidencias`.`qc_dia`,
`qc_incidencias`.`qc_numerador`,
`qc_incidencias`.`qc_origen`,
`qc_incidencias`.`qc_tercero`,
`qc_incidencias`.`qc_articulo`,
`qc_incidencias`.`qc_cantidad`,
`qc_incidencias`.`qc_notificado_por`,
`qc_incidencias`.`qc_incidencia`,
`qc_incidencias`.`qc_fecha_notificacion`,
`qc_incidencias`.`qc_investigacion`,
`qc_incidencias`.`qc_revisado_por`,
`qc_incidencias`.`qc_fecha_revision`,
`qc_incidencias`.`qc_resolucion`,
`qc_incidencias`.`qc_correctiva`,
`qc_incidencias`.`qc_fecha_implantacion`,
`qc_incidencias`.`qc_responsable_implantacion`,
`qc_incidencias`.`qc_fecha_cierre`,
`qc_incidencias`.`qc_seguimiento`,
`qc_incidencias`.`qc_5_why`,
`qc_incidencias`.`qc_seguimiento_acc_cor`,
`qc_incidencias`.`qc_observaciones`,
`qc_incidencias`.`qc_idparte_calidad`,
`qc_incidencias`.`qc_idprogramacion`,
`qc_incidencias`.`qc_documento`,
`qc_incidencias`.`qc_linea`,
`qc_incidencias`.`qc_fase`,
`qc_incidencias`.`qc_real_pot`
FROM `laboratorio`.`qc_incidencias`;
*/
/*
rojo: 228
verde: 166
azul: 54

hex: #E4A636
*/

/*
<v-text-field caption="PEDIDO" _id="documento" width-full  />
<v-date-field caption="REVISION" _id="fechaRevision" width-full />
<v-text-field caption="POR" _id="revisado" width-full />
*/    		
