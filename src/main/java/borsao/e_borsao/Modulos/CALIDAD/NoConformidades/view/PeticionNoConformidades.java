package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionNoConformidades extends Window
{
	private MapeoProgramacion mapeoProgramacion = null;
	private MapeoProgramacionEnvasadora mapeoProgramacionEnvasadora = null;
	private Integer idIncidencia=null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbArticulo = null;
	private ComboBox cmbOrigen = null;
	private ComboBox cmbLinea = null;
	private ComboBox cmbPrinter = null;
	
	private CheckBox chkParada;
	private CheckBox chkInterrupcion;
	private CheckBox chkVelocidad;
	private CheckBox chkRetrabajo;
	private CheckBox chkRetrabajoMp;
	
	private TextField txtCajas = null;
	private TextField txtFecha = null;
	private TextField txtUsuario = null;
	private TextArea txtIncidencia = null;
	private TextField txtDescripcion= null;
	
	private TextField txtTiempoParada= null;
	private TextField txtTiempoInterrupcion= null;
	private TextField txtTiempoRetrabajo= null;
	private TextField txtTiempoRetrabajoMp= null;
	private TextField txtTiempoVelocidad= null;
	private TextField txtVelocidadHabitual= null;
	private TextField txtVelocidadTrabajo= null;

	public PeticionNoConformidades(MapeoProgramacion r_mapeo)
	{
		this.mapeoProgramacion = r_mapeo;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbOrigen=new ComboBox("Origen");
				this.cmbOrigen.addItem("PRODUCCION");
				this.cmbOrigen.addItem("CLIENTE");
				this.cmbOrigen.addItem("PROVEEDOR");
				this.cmbOrigen.setValue("PRODUCCION");
				this.cmbOrigen.setNewItemsAllowed(false);
				this.cmbOrigen.setNullSelectionAllowed(false);
				this.cmbOrigen.setRequired(true);
				this.cmbOrigen.setWidth("200px");
				
				this.txtFecha=new TextField("Día");
				this.txtFecha.setValue(RutinasFechas.fechaActual());
				this.txtFecha.setWidth("150px");
				
				this.cmbLinea=new ComboBox("Origen");
				this.cmbLinea.addItem("EMBOTELLADORA");
				this.cmbLinea.setValue("EMBOTELLADORA");
				
				fila1.addComponent(cmbLinea);
				fila1.addComponent(cmbOrigen);
				fila1.addComponent(txtFecha);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbArticulo=new ComboBox("Articulo");
				this.cargarCombo("Articulo");
				this.cmbArticulo.setWidth("550px");
				this.cmbArticulo.setRequired(true);
				this.cmbArticulo.setInvalidAllowed(true);
				this.cmbArticulo.setTextInputAllowed(true);
				this.cmbArticulo.setNullSelectionAllowed(false);
				
				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setWidth("400px");
				this.txtDescripcion.setRequired(true);
				
				this.txtCajas=new TextField("Cantidad ");				
				this.txtCajas.setWidth("150px");
				this.txtCajas.setRequired(true);
				
				fila2.addComponent(cmbArticulo);
				fila2.addComponent(txtCajas);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtIncidencia=new TextArea("Incidencia");
				this.txtIncidencia.setWidth("800px");
				this.txtIncidencia.setRequired(true);
				
				fila3.addComponent(txtIncidencia);

				HorizontalLayout fila4 = new HorizontalLayout();
				fila4.setWidthUndefined();
				fila4.setSpacing(true);
				
					this.chkParada = new CheckBox("Provoca Parada de Línea?", false);
					this.txtTiempoParada = new TextField("Tiempo");
					this.txtTiempoParada.setEnabled(false);
					this.txtTiempoParada.setWidth("80px");
					this.txtTiempoParada.setDescription("Tiempo en minutos");
					this.txtTiempoParada.setStyleName("rightAligned");
					
					this.chkInterrupcion= new CheckBox("Provoca Interrupciones en la Producción?", false);				
					this.txtTiempoInterrupcion  = new TextField("Tiempo");
					this.txtTiempoInterrupcion.setEnabled(false);
					this.txtTiempoInterrupcion.setWidth("80");
					this.txtTiempoInterrupcion.setDescription("Tiempo en minutos");
					this.txtTiempoInterrupcion.setStyleName("rightAligned");
					
				fila4.addComponent(this.chkParada);
				fila4.addComponent(this.txtTiempoParada);
				fila4.addComponent(this.chkInterrupcion);
				fila4.addComponent(this.txtTiempoInterrupcion);
				fila4.setComponentAlignment(this.chkParada, Alignment.MIDDLE_LEFT);
				fila4.setComponentAlignment(this.chkInterrupcion, Alignment.MIDDLE_LEFT);

				HorizontalLayout fila5 = new HorizontalLayout();
				fila5.setWidthUndefined();
				fila5.setSpacing(true);
					
					this.chkVelocidad = new CheckBox("Provoca Bajar la velocidad de la Línea?", false);
					this.txtVelocidadHabitual= new TextField("Vel. Normal");
					this.txtVelocidadHabitual.setEnabled(false);
					this.txtVelocidadHabitual.setDescription("Botellas Hora");
					this.txtVelocidadHabitual.setStyleName("rightAligned");
					this.txtVelocidadHabitual.setWidth("120px");
					this.txtVelocidadTrabajo= new TextField("Vel. Trabajo");
					this.txtVelocidadTrabajo.setEnabled(false);
					this.txtVelocidadTrabajo.setDescription("Botellas Hora");
					this.txtVelocidadTrabajo.setStyleName("rightAligned");
					this.txtVelocidadTrabajo.setWidth("120px");
					this.txtTiempoVelocidad= new TextField("Tiempo Producción");
					this.txtTiempoVelocidad.setEnabled(false);
					this.txtTiempoVelocidad.setDescription("Tiempo en Minutos");
					this.txtTiempoVelocidad.setStyleName("rightAligned");
					this.txtTiempoVelocidad.setWidth("120px");
					
				fila5.addComponent(this.chkVelocidad);
				fila5.addComponent(this.txtVelocidadHabitual);
				fila5.addComponent(this.txtVelocidadTrabajo);
				fila5.addComponent(this.txtTiempoVelocidad);
				fila5.setComponentAlignment(this.chkVelocidad, Alignment.MIDDLE_LEFT);

				HorizontalLayout fila6 = new HorizontalLayout();
				fila6.setWidthUndefined();
				fila6.setSpacing(true);

				
				this.chkRetrabajo = new CheckBox("Provoca Retrabajar el producto terminado?", false);
				this.txtTiempoRetrabajo = new TextField("Tiempo");
				this.txtTiempoRetrabajo.setEnabled(false);
				this.txtTiempoRetrabajo.setStyleName("rightAligned");
				this.txtTiempoRetrabajo.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajo.setWidth("80px");
				
				this.chkRetrabajoMp = new CheckBox("Provoca adaptar la Materia Seca?", false);
				this.txtTiempoRetrabajoMp = new TextField("Tiempo");
				this.txtTiempoRetrabajoMp.setEnabled(false);
				this.txtTiempoRetrabajoMp.setStyleName("rightAligned");
				this.txtTiempoRetrabajoMp.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajoMp.setWidth("80px");
				
			fila6.addComponent(this.chkRetrabajo);
			fila6.addComponent(this.txtTiempoRetrabajo);
			fila6.addComponent(this.chkRetrabajoMp);
			fila6.addComponent(this.txtTiempoRetrabajoMp);
			fila6.setComponentAlignment(this.chkRetrabajo, Alignment.MIDDLE_LEFT);
			fila6.setComponentAlignment(this.chkRetrabajoMp, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila7 = new HorizontalLayout();
			fila7.setWidthUndefined();
			fila7.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo("Impresora");
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				
				this.txtUsuario=new TextField("Usuario");
				this.txtUsuario.setWidth("350px");
				this.txtUsuario.setRequired(true);
				
				fila7.addComponent(txtUsuario);
				fila7.addComponent(cmbPrinter);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		controles.addComponent(fila6);
		controles.addComponent(fila7);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Incidencias Linea Embotellado          " + this.mapeoProgramacion.getArticulo() + " " + r_mapeo.getDescripcion() );
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("750px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
		this.txtCajas.focus();
	}
	
	public PeticionNoConformidades(MapeoProgramacionEnvasadora r_mapeo)
	{
		this.mapeoProgramacionEnvasadora = r_mapeo;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbOrigen=new ComboBox("Origen");
				this.cmbOrigen.addItem("PRODUCCION");
				this.cmbOrigen.addItem("CLIENTE");
				this.cmbOrigen.addItem("PROVEEDOR");
				this.cmbOrigen.setValue("PRODUCCION");
				this.cmbOrigen.setNewItemsAllowed(false);
				this.cmbOrigen.setNullSelectionAllowed(false);
				this.cmbOrigen.setRequired(true);
				this.cmbOrigen.setWidth("200px");
				
				this.txtFecha=new TextField("Día");
				this.txtFecha.setValue(RutinasFechas.fechaActual());
				this.txtFecha.setWidth("150px");
				
				this.cmbLinea=new ComboBox("Origen");
				this.cmbLinea.addItem("ENVASADORA");
				this.cmbLinea.addItem("BIB");
				this.cmbLinea.setValue("ENVASADORA");
				
				fila1.addComponent(cmbLinea);
				fila1.addComponent(cmbOrigen);
				fila1.addComponent(txtFecha);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbArticulo=new ComboBox("Articulo");
				this.cargarCombo("Articulo");
				this.cmbArticulo.setWidth("550px");
				this.cmbArticulo.setRequired(true);
				this.cmbArticulo.setInvalidAllowed(true);
				this.cmbArticulo.setTextInputAllowed(true);
				this.cmbArticulo.setNullSelectionAllowed(false);
				
				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setWidth("400px");
				this.txtDescripcion.setRequired(true);
				
				this.txtCajas=new TextField("Cantidad ");				
				this.txtCajas.setWidth("150px");
				this.txtCajas.setRequired(true);
				
				fila2.addComponent(cmbArticulo);
				fila2.addComponent(txtCajas);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtIncidencia=new TextArea("Incidencia");
				this.txtIncidencia.setWidth("800px");
				this.txtIncidencia.setRequired(true);
				
				fila3.addComponent(txtIncidencia);

				HorizontalLayout fila4 = new HorizontalLayout();
				fila4.setWidthUndefined();
				fila4.setSpacing(true);
				
					this.chkParada = new CheckBox("Provoca Parada de Línea?", false);
					this.txtTiempoParada = new TextField("Tiempo");
					this.txtTiempoParada.setEnabled(false);
					this.txtTiempoParada.setWidth("80px");
					this.txtTiempoParada.setDescription("Tiempo en minutos");
					this.txtTiempoParada.setStyleName("rightAligned");
					
					this.chkInterrupcion= new CheckBox("Provoca Interrupciones en la Producción?", false);				
					this.txtTiempoInterrupcion  = new TextField("Tiempo");
					this.txtTiempoInterrupcion.setEnabled(false);
					this.txtTiempoInterrupcion.setWidth("80");
					this.txtTiempoInterrupcion.setDescription("Tiempo en minutos");
					this.txtTiempoInterrupcion.setStyleName("rightAligned");
					
				fila4.addComponent(this.chkParada);
				fila4.addComponent(this.txtTiempoParada);
				fila4.addComponent(this.chkInterrupcion);
				fila4.addComponent(this.txtTiempoInterrupcion);
				fila4.setComponentAlignment(this.chkParada, Alignment.MIDDLE_LEFT);
				fila4.setComponentAlignment(this.chkInterrupcion, Alignment.MIDDLE_LEFT);

				HorizontalLayout fila5 = new HorizontalLayout();
				fila5.setWidthUndefined();
				fila5.setSpacing(true);
					
					this.chkVelocidad = new CheckBox("Provoca Bajar la velocidad de la Línea?", false);
					this.txtVelocidadHabitual= new TextField("Vel. Normal");
					this.txtVelocidadHabitual.setEnabled(false);
					this.txtVelocidadHabitual.setDescription("Botellas Hora");
					this.txtVelocidadHabitual.setStyleName("rightAligned");
					this.txtVelocidadHabitual.setWidth("120px");
					this.txtVelocidadTrabajo= new TextField("Vel. Trabajo");
					this.txtVelocidadTrabajo.setEnabled(false);
					this.txtVelocidadTrabajo.setDescription("Botellas Hora");
					this.txtVelocidadTrabajo.setStyleName("rightAligned");
					this.txtVelocidadTrabajo.setWidth("120px");
					this.txtTiempoVelocidad= new TextField("Tiempo Producción");
					this.txtTiempoVelocidad.setEnabled(false);
					this.txtTiempoVelocidad.setDescription("Tiempo en Minutos");
					this.txtTiempoVelocidad.setStyleName("rightAligned");
					this.txtTiempoVelocidad.setWidth("120px");
					
				fila5.addComponent(this.chkVelocidad);
				fila5.addComponent(this.txtVelocidadHabitual);
				fila5.addComponent(this.txtVelocidadTrabajo);
				fila5.addComponent(this.txtTiempoVelocidad);
				fila5.setComponentAlignment(this.chkVelocidad, Alignment.MIDDLE_LEFT);

				HorizontalLayout fila6 = new HorizontalLayout();
				fila6.setWidthUndefined();
				fila6.setSpacing(true);

				
				this.chkRetrabajo = new CheckBox("Provoca Retrabajar el producto terminado?", false);
				this.txtTiempoRetrabajo = new TextField("Tiempo");
				this.txtTiempoRetrabajo.setEnabled(false);
				this.txtTiempoRetrabajo.setStyleName("rightAligned");
				this.txtTiempoRetrabajo.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajo.setWidth("80px");
				
				this.chkRetrabajoMp = new CheckBox("Provoca adaptar la Materia Seca?", false);
				this.txtTiempoRetrabajoMp = new TextField("Tiempo");
				this.txtTiempoRetrabajoMp.setEnabled(false);
				this.txtTiempoRetrabajoMp.setStyleName("rightAligned");
				this.txtTiempoRetrabajoMp.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajoMp.setWidth("80px");
				
			fila6.addComponent(this.chkRetrabajo);
			fila6.addComponent(this.txtTiempoRetrabajo);
			fila6.addComponent(this.chkRetrabajoMp);
			fila6.addComponent(this.txtTiempoRetrabajoMp);
			fila6.setComponentAlignment(this.chkRetrabajo, Alignment.MIDDLE_LEFT);
			fila6.setComponentAlignment(this.chkRetrabajoMp, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila7 = new HorizontalLayout();
			fila7.setWidthUndefined();
			fila7.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo("Impresora");
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				
				this.txtUsuario=new TextField("Usuario");
				this.txtUsuario.setWidth("350px");
				this.txtUsuario.setRequired(true);
				
				fila7.addComponent(txtUsuario);
				fila7.addComponent(cmbPrinter);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		controles.addComponent(fila6);
		controles.addComponent(fila7);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Incidencias Linea " + this.cmbLinea.getValue().toString() + "          " + r_mapeo.getArticulo() + " " + r_mapeo.getDescripcion() );
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("750px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
		this.txtCajas.focus();
	}

	private void habilitarCampos()
	{
	
		this.cmbOrigen.setEnabled(false);
		this.txtFecha.setEnabled(false);
		
		this.cmbArticulo.setEnabled(true);
		this.cmbPrinter.setEnabled(true);
		this.txtCajas.setEnabled(true);
		this.txtIncidencia.setEnabled(true);			
		this.txtUsuario.setEnabled(true);			
	}
	
	private void cargarListeners()
	{
		chkParada.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoParada.setEnabled(chkParada.getValue());
			}
		});
		
		chkInterrupcion.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoInterrupcion.setEnabled(chkInterrupcion.getValue());
			}
		});

		chkRetrabajo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoRetrabajo.setEnabled(chkRetrabajo.getValue());
			}
		});

		chkRetrabajoMp.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoRetrabajoMp.setEnabled(chkRetrabajoMp.getValue());
			}
		});

		chkVelocidad.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtVelocidadHabitual.setEnabled(chkVelocidad.getValue());
				txtVelocidadTrabajo.setEnabled(chkVelocidad.getValue());
				txtTiempoVelocidad.setEnabled(chkVelocidad.getValue());
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				String todoCorrecto = todoEnOrden();
				if (todoCorrecto.equals("OK"))
				{
					MapeoNoConformidades newMapeo = new MapeoNoConformidades();
					
					newMapeo.setArticulo(cmbArticulo.getValue().toString().substring(0, 7));
					newMapeo.setCantidad(new Integer(txtCajas.getValue()));
					newMapeo.setDia(new Date());
					newMapeo.setOrigen(cmbOrigen.getValue().toString());
					newMapeo.setFechaNotificacion(RutinasFechas.conversionDeString(txtFecha.getValue()));
					newMapeo.setNotificador(txtUsuario.getValue().toString());
					newMapeo.setIncidencia(txtIncidencia.getValue());
					
					if (mapeoProgramacionEnvasadora!=null) newMapeo.setIdProgramacion(mapeoProgramacionEnvasadora.getIdProgramacion());
					if (mapeoProgramacion!=null) newMapeo.setIdProgramacion(mapeoProgramacion.getIdProgramacion());
					
					newMapeo.setLinea(cmbLinea.getValue().toString());
					
					consultaNoConformidadesServer cns = consultaNoConformidadesServer.getInstance(CurrentUser.get());
					if (btnBotonCVentana.getCaption().equals("Cancelar")) 
					{
						idIncidencia=cns.obtenerSiguiente();
					}
					newMapeo.setIdqc_incidencias(idIncidencia);
					
					MapeoValoracionNoConformidades newMapeoVal = new MapeoValoracionNoConformidades();
					newMapeoVal.setIdqc_incidencias(idIncidencia);
					if (chkInterrupcion.getValue())
					{
						newMapeoVal.setInterrupcion("S");
						newMapeoVal.setTiempoInterrupcion(new Integer(txtTiempoInterrupcion.getValue()));
					}
					else
					{
						newMapeoVal.setInterrupcion("N");
						newMapeoVal.setTiempoInterrupcion(0);
					}
					
					if (chkParada.getValue())
					{
						newMapeoVal.setParada("S");
						newMapeoVal.setTiempoParada(new Integer(txtTiempoParada.getValue()));
					}
					else 
					{
						newMapeoVal.setParada("N");
						newMapeoVal.setTiempoParada(0);
					}
					if (chkRetrabajo.getValue())
					{
						newMapeoVal.setRetrabajo("S");
						newMapeoVal.setTiempoRetrabajo(new Integer(txtTiempoRetrabajo.getValue()));
					}
					else 
					{
						newMapeoVal.setRetrabajo("N");
						newMapeoVal.setTiempoRetrabajo(0);
					}
					if (chkRetrabajoMp.getValue())
					{
						newMapeoVal.setRetrabajomp("S");
						newMapeoVal.setTiempoRetrabajomp(new Integer(txtTiempoRetrabajoMp.getValue()));
					}
					else 
					{
						newMapeoVal.setRetrabajomp("N");
						newMapeoVal.setTiempoRetrabajomp(0);
					}
					if (chkVelocidad.getValue())
					{
						newMapeoVal.setVelocidad("S"); 
						newMapeoVal.setTiempoVelocidad(new Integer(txtTiempoVelocidad.getValue()));
						newMapeoVal.setVelocidadHabitual(new Integer(txtVelocidadHabitual.getValue()));
						newMapeoVal.setVelocidadTrabajo(new Integer(txtVelocidadTrabajo.getValue()));
					}
					else 
					{
						newMapeoVal.setVelocidad("N");
						newMapeoVal.setTiempoVelocidad(0);
						newMapeoVal.setVelocidadHabitual(0);
						newMapeoVal.setVelocidadTrabajo(0);
					}
					
					String rdo = null;
					
					if (btnBotonCVentana.getCaption().equals("Cancelar"))
					{
						rdo = cns.guardarNuevo(newMapeo);
						if (rdo==null || rdo.length()==1 )
						{
							rdo = cns.guardarNuevoValoracion(newMapeoVal);
						}
					}
					else
					{
						rdo = cns.guardarCambios(newMapeo);
						if (rdo==null || rdo.length()==1 )
						{
							rdo = cns.guardarCambiosValoracion(newMapeoVal);
						}
					}
					
					if (rdo!=null && rdo.length()>=1 )
						Notificaciones.getInstance().mensajeError(rdo);
					else
					/*
					 * tras llamar a la generacion cierro la ventana
					 */
					{
						String pdfGenerado = cns.imprimirNoConformidad(newMapeo.getIdqc_incidencias(),"Pendiente");
						if(pdfGenerado.length()>0)
				    	{
				    		if (cmbPrinter.getValue().toString().equals("Pantalla"))
				    		{
				    			RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
				    			btnBotonCVentana.setCaption("Cerrar");
				    		}
				    		else				    			
				    		{
				    			String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
								RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
								RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
				    		}				    		
				    	}	
				    	else
				    		Notificaciones.getInstance().mensajeError("Incidencia guardada. Pero por error no se ha podido imprimir");
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia(todoCorrecto);
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
		ValueChangeListener lisArt = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbArticulo.getValue()!="")
				{
					String articulo = cmbArticulo.getValue().toString().substring(0, 7);
					consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
					txtDescripcion.setValue(cas.obtenerDescripcionArticulo(articulo));
					cmbArticulo.setValue(articulo);
					txtCajas.focus();
				}
			}
		};
		cmbArticulo.addValueChangeListener(lisArt);
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private String todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		String devolver = "OK";
		if (this.cmbOrigen.getValue()==null || this.cmbOrigen.getValue().toString().length()==0)
		{
			devolver = "Hay que rellenar el origen de la incidencia";
			cmbOrigen.focus();
		}
		else if (this.cmbArticulo.getValue()==null || this.cmbArticulo.getValue().toString().length()==0)
		{
			devolver = "Hay que rellenar el articulo que provoca la incidencia";
			cmbArticulo.focus();
		}
		else if (this.cmbPrinter.getValue()==null || this.cmbPrinter.getValue().toString().length()==0)
		{
			devolver = "No puedes dejar vacio la salida del informe";
			cmbPrinter.focus();
		}
		else if (this.txtCajas.getValue()==null || this.txtCajas.getValue().length()==0) 
		{
			devolver ="Has de rellenar la cantidad afectada por la incidencia";			
			txtCajas.focus();
		}
		else if (this.txtFecha.getValue()==null || this.txtFecha.getValue().length()==0) 
		{
			devolver ="Es obligatorio indicar la fecha de la incidencia";
			txtFecha.focus();
		}
		else if (this.txtUsuario.getValue()==null || this.txtUsuario.getValue().length()==0)
		{
			devolver ="Hay que rellenar el usuario que ha detectado la incidencia";
			txtUsuario.focus();
		}
		else if (this.chkParada.getValue() && (this.txtTiempoParada.getValue()==null || this.txtTiempoParada.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo de parada sufrido por la incidencia";
			txtTiempoParada.focus();
		}
		else if (this.chkInterrupcion.getValue() && (this.txtTiempoInterrupcion.getValue()==null || this.txtTiempoInterrupcion.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo de itnerrupciones total sufrido por la incidencia";
			txtTiempoInterrupcion.focus();
		}
		else if (this.chkRetrabajo.getValue() && (this.txtTiempoRetrabajo.getValue()==null || this.txtTiempoRetrabajo.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo retrabajado para resolver la incidencia";
			txtTiempoRetrabajo.focus();
		}
		else if (this.chkRetrabajoMp.getValue() && (this.txtTiempoRetrabajoMp.getValue()==null || this.txtTiempoRetrabajoMp.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo retrabajado la materia prima para poder utilizarla";
			txtTiempoRetrabajoMp.focus();
		}
		else if (this.chkVelocidad.getValue() && (this.txtVelocidadHabitual.getValue()==null || this.txtVelocidadHabitual.getValue().length()==0))
		{
			devolver ="Hay que indicar la velocidad habitual al que hubieramos producido sin existir la incidencia";
			txtVelocidadHabitual.focus();
		}
		else if (this.chkVelocidad.getValue() && (this.txtTiempoVelocidad.getValue()==null || this.txtTiempoVelocidad.getValue().length()==0))
		{
			devolver ="Hay que indicar cuánto tiempo hemos estado trabajando a menor velocidad de la habitual";
			txtTiempoVelocidad.focus();
		}
		else if (this.chkVelocidad.getValue() && (this.txtVelocidadTrabajo.getValue()==null || this.txtVelocidadTrabajo.getValue().length()==0))
		{
			devolver ="Hay que indicar la velocidad a la que hemos tenido que producir por causa de la incidencia";
			txtVelocidadTrabajo.focus();
		}

		return devolver;
	}

	/*
	 * Metodo que nos dirá si estamos en palet completo o en palet parcial
	 * 
	 * devuelve: 	true en caso de palet completo
	 * 				false en caso de palet parcial 
	 */

	private void cargarCombo(String r_area)
	{
		String defecto=null;
		switch (r_area)
		{
			case "Impresora":
			{	
				consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
				ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
				
				for (int i=0;i<vector.size();i++)
				{
					if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
					this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
				}
				
				this.cmbPrinter.setValue(defecto);
				break;
			}	
			case "Articulo":
			{
				consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
				ArrayList<String> componentes = null;
				
				if (this.mapeoProgramacion!=null)
				{
					componentes = ces.recuperarHijos(this.mapeoProgramacion.getArticulo());
					cmbArticulo.removeAllItems();
					cmbArticulo.addItem(this.mapeoProgramacion.getArticulo() + " " + RutinasCadenas.conversion(this.mapeoProgramacion.getDescripcion()));
					for (int i=0;i< componentes.size();i++)
					{
						cmbArticulo.addItem(componentes.get(i));
					}
					break;
				}
				else if (this.mapeoProgramacionEnvasadora!=null)
				{
					componentes = ces.recuperarHijos(this.mapeoProgramacionEnvasadora.getArticulo());
					cmbArticulo.removeAllItems();
					cmbArticulo.addItem(this.mapeoProgramacionEnvasadora.getArticulo() + " " + RutinasCadenas.conversion(this.mapeoProgramacionEnvasadora.getDescripcion()));
					for (int i=0;i< componentes.size();i++)
					{
						cmbArticulo.addItem(componentes.get(i));
					}
					break;
				} 
			}
		}
	}

}