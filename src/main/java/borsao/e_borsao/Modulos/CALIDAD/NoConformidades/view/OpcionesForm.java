package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.NoConformidadesGrid;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoNoConformidades mapeo = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaNoConformidadesServer cps = null;
	 private NoConformidadesView uw = null;
	 public MapeoNoConformidades mapeoModificado = null;
	 private BeanFieldGroup<MapeoNoConformidades> fieldGroup;
	 
	 public OpcionesForm(NoConformidadesView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoNoConformidades>(MapeoNoConformidades.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fechaNotificacion.addStyleName("v-datefield-textfield");
        this.fechaNotificacion.setDateFormat("dd/MM/yyyy");
        this.fechaNotificacion.setShowISOWeekNumbers(true);
        this.fechaNotificacion.setResolution(Resolution.DAY);
        
        this.fechaCierre.addStyleName("v-datefield-textfield");
        this.fechaCierre.setDateFormat("dd/MM/yyyy");
        this.fechaCierre.setShowISOWeekNumbers(true);
        this.fechaCierre.setResolution(Resolution.DAY);
        
        this.fechaImplantacion.addStyleName("v-datefield-textfield");
        this.fechaImplantacion.setDateFormat("dd/MM/yyyy");
        this.fechaImplantacion.setShowISOWeekNumbers(true);
        this.fechaImplantacion.setResolution(Resolution.DAY);
        
//        this.fechaRevision.addStyleName("v-datefield-textfield");
//        this.fechaRevision.setDateFormat("dd/MM/yyyy");
//        this.fechaRevision.setShowISOWeekNumbers(true);
//        this.fechaRevision.setResolution(Resolution.DAY);
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoNoConformidades) uw.grid.getSelectedRow();
                eliminarNoConformidad(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
		
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				String todoenorden = null;
	 				todoenorden=comprobarCamposObligatorios();

	 				if (todoenorden=="OK")
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearNoConformidad(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError(todoenorden);
	 				}
	 			}
	 			else
	 			{
	 				String todoenorden = null;
	 				todoenorden=comprobarCamposObligatorios();
	 				
	 				if (todoenorden=="OK")
	 				{
		 				MapeoNoConformidades mapeo_orig= (MapeoNoConformidades) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setDia(mapeo_orig.getDia());
		 				mapeo.setIdProgramacion(mapeo_orig.getIdProgramacion());
		 				mapeo.setIdParte(mapeo_orig.getIdParte());
		 				
		 				modificarNoConformidad(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError(todoenorden);
	 				}

	 			}
	 			
	 			if (((NoConformidadesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0)
				{
					cps  = new consultaNoConformidadesServer(CurrentUser.get());
					String desc = cps.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
//				else if (mapeoModificado!=null)
//				{
//					descripcion.setValue(mapeoModificado.getDescripcion());
//					factor.setValue(mapeoModificado.getFactor().toString());					
//				}
			}
		});
		
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarNoConformidad(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.numerador.getValue()!=null && this.numerador.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numerador", this.numerador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numerador", "");
		}
		if (this.fechaNotificacion.getValue()!=null && this.fechaNotificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaNotificacion", RutinasFechas.convertirDateToString(this.fechaNotificacion.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaNotificacion", "");
		}
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.bodega.getValue()!=null && this.bodega.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("bodega", this.bodega.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bodega", "");
		}
		if (this.linea.getValue()!=null && this.linea.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("linea", this.linea.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("linea", "");
		}

		if (this.tercero.getValue()!=null && this.tercero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tercero", this.tercero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tercero", "");
		}
		if (this.notificador.getValue()!=null && this.notificador.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("notificador", this.notificador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("notificador", "");
		}
//		if (this.documento.getValue()!=null && this.documento.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("documento", this.documento.getValue().toString());
//		}
//		else
//		{
//			opcionesEscogidas.put("documento", "");
//		}

		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.incidencia.getValue()!=null && this.incidencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("incidencia", this.incidencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("incidencia", "");
		}
//		if (this.fechaRevision.getValue()!=null && this.fechaRevision.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("fechaRevision", RutinasFechas.convertirDateToString(this.fechaRevision.getValue()));
//		}
//		else
//		{
//			opcionesEscogidas.put("fechaRevision", "");
//		}
//		if (this.revisado.getValue()!=null && this.revisado.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("revisado", this.revisado.getValue().toString());
//		}
//		else
//		{
//			opcionesEscogidas.put("revisado", "");
//		}
		if (this.investigacion.getValue()!=null && this.investigacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("investigacion", this.investigacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("investigacion", "");
		}
		if (this.resolucion.getValue()!=null && this.resolucion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("resolucion", this.resolucion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("resolucion", "");
		}
		if (this.correctiva.getValue()!=null && this.correctiva.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("correctiva", this.correctiva.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("correctiva", "");
		}
		if (this.responsable.getValue()!=null && this.responsable.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("responsable", this.responsable.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("responsable", "");
		}
		if (this.fechaImplantacion.getValue()!=null && this.fechaImplantacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaImplantacion", RutinasFechas.convertirDateToString(this.fechaImplantacion.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaImplantacion", "");
		}
		if (this.fechaCierre.getValue()!=null && this.fechaCierre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaCierre", RutinasFechas.convertirDateToString(this.fechaCierre.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaCierre", "");
		}
		
		if (this.seguimiento.getValue()!=null && this.seguimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("seguimiento", this.seguimiento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("seguimiento", "");
		}
		if (this.seguimiento_ac.getValue()!=null && this.seguimiento_ac.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("seguimiento_acc", this.seguimiento_ac.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("seguimiento_acc", "");
		}
		if (this.fiveWhy.getValue()!=null && this.fiveWhy.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("porques", this.fiveWhy.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("porques", "");
		}

		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		if (this.fase.getValue()!=null && this.fase.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fase", this.fase.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fase", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.numerador.getValue()!=null && this.numerador.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numerador", this.numerador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numerador", "");
		}
		if (this.fechaNotificacion.getValue()!=null && this.fechaNotificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaNotificacion", RutinasFechas.convertirDateToString(this.fechaNotificacion.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaNotificacion", "");
		}
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.linea.getValue()!=null && this.linea.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("linea", this.linea.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("linea", "");
		}
		if (this.bodega.getValue()!=null && this.bodega.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("bodega", this.bodega.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bodega", "");
		}
		
		if (this.tercero.getValue()!=null && this.tercero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tercero", this.tercero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tercero", "");
		}
		if (this.notificador.getValue()!=null && this.notificador.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("notificador", this.notificador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("notificador", "");
		}
//		if (this.documento.getValue()!=null && this.documento.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("documento", this.documento.getValue().toString());
//		}
//		else
//		{
//			opcionesEscogidas.put("documento", "");
//		}

		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.incidencia.getValue()!=null && this.incidencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("incidencia", this.incidencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("incidencia", "");
		}
//		if (this.fechaRevision.getValue()!=null && this.fechaRevision.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("fechaRevision", RutinasFechas.convertirDateToString(this.fechaRevision.getValue()));
//		}
//		else
//		{
//			opcionesEscogidas.put("fechaRevision", "");
//		}
//		if (this.revisado.getValue()!=null && this.revisado.getValue().toString().length()>0) 
//		{
//			opcionesEscogidas.put("revisado", this.revisado.getValue().toString());
//		}
//		else
//		{
//			opcionesEscogidas.put("revisado", "");
//		}
		if (this.investigacion.getValue()!=null && this.investigacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("investigacion", this.investigacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("investigacion", "");
		}
		if (this.resolucion.getValue()!=null && this.resolucion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("resolucion", this.resolucion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("resolucion", "");
		}
		if (this.correctiva.getValue()!=null && this.correctiva.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("correctiva", this.correctiva.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("correctiva", "");
		}
		if (this.responsable.getValue()!=null && this.responsable.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("responsable", this.responsable.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("responsable", "");
		}
		if (this.fechaImplantacion.getValue()!=null && this.fechaImplantacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaImplantacion", RutinasFechas.convertirDateToString(this.fechaImplantacion.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaImplantacion", "");
		}
		if (this.fechaCierre.getValue()!=null && this.fechaCierre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaCierre", RutinasFechas.convertirDateToString(this.fechaCierre.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaCierre", "");
		}
		if (this.seguimiento.getValue()!=null && this.seguimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("seguimiento", this.seguimiento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("seguimiento", "");
		}
		if (this.seguimiento_ac.getValue()!=null && this.seguimiento_ac.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("seguimiento_acc", this.seguimiento_ac.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("seguimiento_acc", "");
		}
		if (this.fiveWhy.getValue()!=null && this.fiveWhy.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("porques", this.fiveWhy.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("porques", "");
		}

		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarNoConformidad(null);
		}
	}
	
	public void editarNoConformidad(MapeoNoConformidades r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoNoConformidades();
			this.numerador.setValue("");
//			this.fechaNotificacion.setValue(null);
			
			if (!isBusqueda())
			{
				cps = consultaNoConformidadesServer.getInstance(CurrentUser.get());
				Integer numerador = cps.obtenerSiguiente();
				this.numerador.setValue(numerador.toString());
				this.fechaNotificacion.setValue(new Date());
				r_mapeo.setNumerador(numerador);
				r_mapeo.setFechaNotificacion(this.fechaNotificacion.getValue());
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoNoConformidades>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarNoConformidad(MapeoNoConformidades r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((NoConformidadesGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((NoConformidadesGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
				
			ArrayList<MapeoNoConformidades> r_vector = (ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector;
			
			Indexed indexed = ((NoConformidadesGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((NoConformidadesGrid) uw.grid).sort("numerador");
		}
	}
	
	public void modificarNoConformidad(MapeoNoConformidades r_mapeo, MapeoNoConformidades r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdqc_incidencias(r_mapeo_orig.getIdqc_incidencias());
		
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo.length()==1) r_mapeo.setEstado(rdo);
//		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdqc_incidencias()));
		
		if (!((NoConformidadesGrid) uw.grid).vector.isEmpty())
		{
			
			((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector).remove(r_mapeo_orig);
			((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector).add(r_mapeo);
			ArrayList<MapeoNoConformidades> r_vector = (ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector;
			
			Indexed indexed = ((NoConformidadesGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
//            indexed.addItemAt(list.size()+1, r_mapeo);
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			
//            uw.generarGrid(uw.opcionesEscogidas);
            uw.presentarGrid(r_vector);
			((NoConformidadesGrid) uw.grid).setRecords((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector);
////			((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);
			((NoConformidadesGrid) uw.grid).sort("numerador");
			((NoConformidadesGrid) uw.grid).scrollTo(r_mapeo);
			((NoConformidadesGrid) uw.grid).select(r_mapeo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearNoConformidad(MapeoNoConformidades r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdqc_incidencias(uw.cus.obtenerSiguiente());
		r_mapeo.setDia(new Date());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
//		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdqc_incidencias()));
		if (rdo.length()==1) r_mapeo.setEstado(rdo);
		
		if (rdo.length()==1)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoNoConformidades>(r_mapeo));
			if (((NoConformidadesGrid) uw.grid)!=null )//&& )
			{
				if (!((NoConformidadesGrid) uw.grid).vector.isEmpty())
				{
					Indexed indexed = ((NoConformidadesGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector).add(r_mapeo);
					((NoConformidadesGrid) uw.grid).setRecords((ArrayList<MapeoNoConformidades>) ((NoConformidadesGrid) uw.grid).vector);
					((NoConformidadesGrid) uw.grid).sort("numerador");
					((NoConformidadesGrid) uw.grid).scrollTo(r_mapeo);
					((NoConformidadesGrid) uw.grid).select(r_mapeo);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoNoConformidades> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdqc_incidencias()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		cps  = new consultaNoConformidadesServer(CurrentUser.get());
		vectorArticulos=cps.vector();
		this.vHelp = new ventanaAyuda(this.articulo, this.descripcion, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
//		this.articulo.setRequired(r_obliga);
//		this.numerador.setRequired(r_obliga);
//		this.origen.setRequired(r_obliga);
//		this.notificador.setRequired(r_obliga);
//		this.cantidad.setRequired(r_obliga);
//		this.fechaNotificacion.setRequired(r_obliga);
//		this.fechaNotificacion.setEnabled(r_obliga);
	}

	private String comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.numerador.getValue()==null || this.numerador.getValue().toString().length()==0) return "Rellena el numerador";
			if (this.fechaNotificacion.getValue()==null || this.fechaNotificacion.getValue().toString().length()==0) return "Rellena la fecha notificacion";
			if (this.articulo.getValue()==null || this.articulo.getValue().length()==0) return "Rellena el articulo";
			if (this.origen.getValue()==null || this.origen.getValue().toString().length()==0) return "Rellena el origen de la incidencia";
			if (this.linea.getValue()==null || this.linea.getValue().toString().length()==0) return "Rellena la linea de produccion de la incidencia";
			if (this.bodega.getValue()==null || this.bodega.getValue().toString().length()==0) return "Rellena la instalacion de la incidencia";
			if (this.notificador.getValue()==null || this.notificador.getValue().length()==0) return "Rellena quien notifica la incidencia";
//			if (this.documento.getValue()==null || this.documento.getValue().length()==0) return "Rellena el documento";
			if (this.cantidad.getValue()==null || this.cantidad.getValue().length()==0) return "Rellena la cantidad";
		}
		
		return "OK";
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
	
	private void cargarCombo()
	{
		this.origen.addItem("PRODUCCION");
		this.origen.addItem("CLIENTE");
		this.origen.addItem("PROVEEDOR");
		this.origen.addItem("INTERNA");
		this.origen.addItem("CONSUMIDOR");
		this.origen.addItem("AUD. INTERNA");
		this.origen.addItem("AUD. EXTERNA");
	
		this.origen.setNewItemsAllowed(false);
		
		this.linea.addItem("EMBOTELLADORA");
		this.linea.addItem("ENVASADORA");
		this.linea.addItem("BIB");
	
		this.linea.setNewItemsAllowed(false);


		this.bodega.addItem("Tejar");
		this.bodega.addItem("Capuchinos");
		
		this.bodega.setNewItemsAllowed(false);
	}
}
