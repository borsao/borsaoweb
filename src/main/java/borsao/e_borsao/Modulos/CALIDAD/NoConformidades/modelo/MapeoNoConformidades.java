package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoNoConformidades extends MapeoGlobal
{
    private Integer idqc_incidencias;
	private Integer numerador;
	private Integer cantidad;
	private Integer idParte;
	private Integer idProgramacion;
	private Integer status;
	
	private String bodega;
	private String origen;
	private String estado;
	private String tercero;
	private String articulo;
	private String notificador;
	private String incidencia;
	private String investigacion;
	private String fiveWhy;
	private String revisado;
	private String resolucion;
	private String correctiva;
	private String responsable;
	private String seguimiento;
	private String seguimiento_ac;
	private String observaciones;
	private String descripcion;
	private String documento;
	private String linea;
	private String correo;
	private String correo2;
	private String correo3;
	private String observacionCorreo;
	private String fase;
	
	private Date fechaNotificacion;
	private Date dia;
	private Date fechaRevision;
	private Date fechaImplantacion;
	private Date fechaCierre;
	private boolean adjuntarPdf;
	
//	private String estado;
	private String programacion="";
	private String imagen="";
	private String img="";
	private String euro="";
	private String mail="";
//	private String biblia=" ";
//	private String esc=" ";
//	private String cambios=" ";

	public MapeoNoConformidades()
	{
		this.setTercero("");
		this.setDocumento(" ");
		this.setIncidencia("");
		this.setObservaciones("");
		this.setCorrectiva("");
		this.setSeguimiento("");
		this.setSeguimiento_ac("");
		this.setFiveWhy("");
		this.setCorrectiva("");
		this.setInvestigacion("");
		this.setArticulo("");
		this.setResolucion("");
		this.setFase("");
		this.setDescripcion("");
		this.setNotificador("");
		this.setRevisado("");
		this.setResponsable("");
	}

	public Integer getIdqc_incidencias() {
		return idqc_incidencias;
	}

	public void setIdqc_incidencias(Integer idqc_incidencias) {
		this.idqc_incidencias = idqc_incidencias;
	}

	public Integer getNumerador() {
		return numerador;
	}

	public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}

	public Date getDia() {
		return dia;
	}

	public void setDia(Date dia) {
		this.dia = dia;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getTercero() {
		return tercero;
	}

	public void setTercero(String tercero) {
		this.tercero = tercero;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getNotificador() {
		return notificador;
	}

	public void setNotificador(String notificador) {
		this.notificador = notificador;
	}

	public String getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(String incidencia) {
		this.incidencia = incidencia;
	}

	public Date getFechaNotificacion() {
		return fechaNotificacion;
	}

	public void setFechaNotificacion(Date fechaNotificacion) {
		this.fechaNotificacion = fechaNotificacion;
	}

	public String getInvestigacion() {
		return investigacion;
	}

	public void setInvestigacion(String investigacion) {
		this.investigacion = investigacion;
	}

	public String getRevisado() {
		return revisado;
	}

	public void setRevisado(String revisado) {
		this.revisado = revisado;
	}

	public Date getFechaRevision() {
		return  fechaRevision;
	}

	public void setFechaRevision(Date fechaRevision) {
		this.fechaRevision = fechaRevision;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getCorrectiva() {
		return correctiva;
	}

	public void setCorrectiva(String correctiva) {
		this.correctiva = correctiva;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Date getFechaImplantacion() {
		return fechaImplantacion;
	}

	public void setFechaImplantacion(Date fechaImplantacion) {
		this.fechaImplantacion = fechaImplantacion;
	}

	public Date getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public String getSeguimiento() {
		return seguimiento;
	}

	public void setSeguimiento(String seguimiento) {
		this.seguimiento = seguimiento;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Integer getIdParte() {
		return idParte;
	}

	public void setIdParte(Integer idParte) {
		this.idParte = idParte;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getProgramacion() {
		return programacion;
	}

	public void setProgramacion(String programacion) {
		this.programacion = programacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getEuro() {
		return euro;
	}

	public void setEuro(String euro) {
		this.euro = euro;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}
	

	public String getFase() {
		return fase;
	}

	public void setFase(String fase) {
		this.fase = fase;
	}

	public String getFiveWhy() {
		return fiveWhy;
	}

	public String getSeguimiento_ac() {
		return seguimiento_ac;
	}

	public void setFiveWhy(String fiveWhy) {
		this.fiveWhy = fiveWhy;
	}

	public void setSeguimiento_ac(String seguimiento_ac) {
		this.seguimiento_ac = seguimiento_ac;
	}

	public String getBodega() {
		return bodega;
	}

	public void setBodega(String bodega) {
		this.bodega = bodega;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCorreo() {
		return correo;
	}

	public String getCorreo2() {
		return correo2;
	}

	public String getCorreo3() {
		return correo3;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public void setCorreo2(String correo2) {
		this.correo2 = correo2;
	}

	public void setCorreo3(String correo3) {
		this.correo3 = correo3;
	}

	public String getObservacionCorreo() {
		return observacionCorreo;
	}

	public void setObservacionCorreo(String observacionCorreo) {
		this.observacionCorreo = observacionCorreo;
	}

	public boolean isAdjuntarPdf() {
		return adjuntarPdf;
	}

	public void setAdjuntarPdf(boolean adjuntarPdf) {
		this.adjuntarPdf = adjuntarPdf;
	}

}