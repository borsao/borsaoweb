package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoImagenesNoConformidades;

public class consultaImagenesNoConformidadesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaImagenesNoConformidadesServer instance;
	
	public consultaImagenesNoConformidadesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaImagenesNoConformidadesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaImagenesNoConformidadesServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoImagenesNoConformidades> datosImagenesNoConformidadesGlobal(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoImagenesNoConformidades> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT qc_incidencias_img.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_img.qc_img_h1 qc_ru ");
		cadenaSQL.append(" from qc_incidencias_img ");
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		cadenaSQL.append(" UNION ALL ");
		cadenaSQL.append(" SELECT qc_incidencias_img.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_img.qc_img_h2 qc_ru ");
		cadenaSQL.append(" from qc_incidencias_img ");
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		cadenaSQL.append(" UNION ALL ");
		cadenaSQL.append(" SELECT qc_incidencias_img.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_img.qc_img_v1 qc_ru ");
		cadenaSQL.append(" from qc_incidencias_img ");
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		cadenaSQL.append(" UNION ALL ");
		cadenaSQL.append(" SELECT qc_incidencias_img.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_img.qc_img_v2 qc_ru ");
		cadenaSQL.append(" from qc_incidencias_img ");
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		cadenaSQL.append(" UNION ALL ");
		cadenaSQL.append(" SELECT qc_incidencias_img.id_incidencia qc_id,");
		cadenaSQL.append(" qc_incidencias_img.qc_img_v3 qc_ru");
		cadenaSQL.append(" from qc_incidencias_img ");
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoImagenesNoConformidades>();
			Integer orden = 0;
			
			while(rsOpcion.next())
			{
				MapeoImagenesNoConformidades mapeoImagenesNoConformidades = new MapeoImagenesNoConformidades();

				mapeoImagenesNoConformidades.setIdqc_incidencias(rsOpcion.getInt("qc_id"));
				mapeoImagenesNoConformidades.setArchivo(rsOpcion.getString("qc_ru"));
				mapeoImagenesNoConformidades.setOrden(orden);
				orden = orden + 1;

				vector.add(mapeoImagenesNoConformidades);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public boolean hayDatosImagenes(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoImagenesNoConformidades> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(qc_incidencias_img.id_incidencia) qc_id ");
		cadenaSQL.append(" from qc_incidencias_img " );
		cadenaSQL.append(" where qc_incidencias_img.id_incidencia = " + r_id);
		cadenaSQL.append(" and (qc_incidencias_img.qc_img_h1 <> '' ");
		cadenaSQL.append(" or qc_incidencias_img.qc_img_h2 <> '' ");
		cadenaSQL.append(" or qc_incidencias_img.qc_img_v1 <> '' ");
		cadenaSQL.append(" or qc_incidencias_img.qc_img_v2 <> '' ");
		cadenaSQL.append(" or qc_incidencias_img.qc_img_v3 <> '' ");
		cadenaSQL.append(" or qc_incidencias_img.qc_img_area <> '') ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("qc_id")>0) return true;				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public String guardarNuevo(ArrayList<MapeoImagenesNoConformidades> r_vector, Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		Iterator<MapeoImagenesNoConformidades> iterator = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			iterator = r_vector.iterator();
			
			cadenaSQL.append(" INSERT INTO qc_incidencias_img( ");
			cadenaSQL.append(" qc_incidencias_img.id_incidencia,");
			cadenaSQL.append(" qc_incidencias_img.qc_img_h1,");
			cadenaSQL.append(" qc_incidencias_img.qc_img_h2, ");
			cadenaSQL.append(" qc_incidencias_img.qc_img_v1, ");
			cadenaSQL.append(" qc_incidencias_img.qc_img_v2, ");
			cadenaSQL.append(" qc_incidencias_img.qc_img_v3) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_id);
		    preparedStatement.setString(2, null);
		    preparedStatement.setString(3, null);
		    preparedStatement.setString(4, null);
		    preparedStatement.setString(5, null);
		    preparedStatement.setString(6, null);
		    
		    while (iterator.hasNext())
		    {
		    	MapeoImagenesNoConformidades mapeo = (MapeoImagenesNoConformidades) iterator.next();
		    	if (mapeo.getArchivo()!=null)
		    	{
		    		preparedStatement.setString(mapeo.getOrden() + 2, mapeo.getArchivo());
		    	}
		    }
		    preparedStatement.executeUpdate();
		}		    
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoImagenesNoConformidades r_mapeo)
	{
		String rdo = null;
//		PreparedStatement preparedStatement = null;		
//		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
//			cadenaSQL.append(" UPDATE qc_incidencias_img set ");
//			cadenaSQL.append(" qc_incidencias_img.id_incidencia=?,");
//			cadenaSQL.append(" qc_incidencias_img.qc_img_h1=?,");
//			cadenaSQL.append(" qc_incidencias_img.qc_img_h2=?, ");
//			cadenaSQL.append(" qc_incidencias_img.qc_img_v1=?, ");
//			cadenaSQL.append(" qc_incidencias_img.qc_img_v2=?, ");
//			cadenaSQL.append(" qc_incidencias_img.qc_img_v3=? ");
//			cadenaSQL.append(" where qc_incidencias_img.id_incidencia=?");
//			
//		    con= this.conManager.establecerConexion();	 
//		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
//		    
		    
//		    if (r_mapeo.getImagen1()!=null)
//		    {
//		    	preparedStatement.setString(1, r_mapeo.getImagen1());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(1, null);
//		    }
//		    
//		    if (r_mapeo.getImagen2()!=null)
//		    {
//		    	preparedStatement.setString(2, r_mapeo.getImagen2());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(2, null);
//		    }
//		    
//		    if (r_mapeo.getImagen3()!=null)
//		    {
//		    	preparedStatement.setString(3, r_mapeo.getImagen3());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(3, null);
//		    }
//		    
//		    if (r_mapeo.getImagen4()!=null)
//		    {
//		    	preparedStatement.setString(4, r_mapeo.getImagen4());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(4, null);
//		    }
//		    
//		    if (r_mapeo.getImagen5()!=null)
//		    {
//		    	preparedStatement.setString(5, r_mapeo.getImagen5());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(5, null);
//		    }
//		    
//		    preparedStatement.setInt(6, r_mapeo.getIdqc_incidencias());
//		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			rdo=ex.getMessage();
	    }
		return rdo;
	}
	
	public void eliminar(Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_incidencias_img ");            
			cadenaSQL.append(" WHERE qc_incidencias_img.id_incidencia = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	@Override
	public String semaforos() 
	{
		return null;
	}

}