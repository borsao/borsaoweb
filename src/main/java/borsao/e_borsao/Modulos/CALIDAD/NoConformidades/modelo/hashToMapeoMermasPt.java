package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeoMermasPt
{
	 public HashMap<String , String> hash = null;
	 public MapeoMermasPt mapeo = null;
	 
	 public MapeoMermasPt convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoMermasPt();

		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDia(rtFecha.conversionDeString(r_hash.get("dia")));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setTecnicoCalidad(r_hash.get("tecnicoCalidad"));		 
		 this.mapeo.setTipo(r_hash.get("tipo"));
		 
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio"))));
		 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("semana"))));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoMermasPt convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoMermasPt();
		 
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDia(rtFecha.conversionDeString(r_hash.get("dia")));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setTecnicoCalidad(r_hash.get("tecnicoCalidad"));
		 this.mapeo.setTipo(r_hash.get("tipo"));
		 
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio"))));
		 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("semana"))));

		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoMermasPt r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("observaciones", r_mapeo.getObservaciones());
		 this.hash.put("tecnicoCalidad", r_mapeo.getTecnicoCalidad());
		 this.hash.put("tipo", r_mapeo.getTipo());

		 if (r_mapeo.getDia()!=null)
		 {
			 this.hash.put("dia", r_mapeo.getDia().toString());
		 }
		 else
		 {
			 this.hash.put("dia", null);
		 }

		 this.hash.put("ejercicio", String.valueOf(this.mapeo.getEjercicio()));
		 this.hash.put("semana", String.valueOf(this.mapeo.getSemana()));
		 this.hash.put("cantidad", String.valueOf(this.mapeo.getCantidad()));
		 
		 return hash;		 
	 }
}