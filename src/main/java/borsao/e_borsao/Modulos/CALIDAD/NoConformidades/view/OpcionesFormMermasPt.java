package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoMermasPt;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MermasPtGrid;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.hashToMapeoMermasPt;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaMermasPtServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesFormMermasPt extends OpcionesFormDesignMermasPt 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoMermasPt mapeo = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaMermasPtServer cps = null;
	 private mermasPtView uw = null;
	 public MapeoMermasPt mapeoModificado = null;
	 private BeanFieldGroup<MapeoMermasPt> fieldGroup;
	 
	 public OpcionesFormMermasPt(mermasPtView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoMermasPt>(MapeoMermasPt.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
        this.dia.addStyleName("v-datefield-textfield");
        this.dia.setDateFormat("dd/MM/yyyy");
        this.dia.setShowISOWeekNumbers(true);
        this.dia.setResolution(Resolution.DAY);
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoMermasPt) uw.grid.getSelectedRow();
                eliminarMerma(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
		
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				String todoenorden = null;
	 				todoenorden=comprobarCamposObligatorios();

	 				if (todoenorden=="OK")
	 				{
		 				hashToMapeoMermasPt hm = new hashToMapeoMermasPt();
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearMerma(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError(todoenorden);
	 				}
	 			}
	 			else
	 			{
	 				String todoenorden = null;
	 				todoenorden=comprobarCamposObligatorios();
	 				
	 				if (todoenorden=="OK")
	 				{
		 				MapeoMermasPt mapeo_orig= (MapeoMermasPt) uw.grid.getSelectedRow();
		 				hashToMapeoMermasPt hm = new hashToMapeoMermasPt(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setArticulo(mapeo_orig.getArticulo());
		 				mapeo.setEjercicio(mapeo_orig.getEjercicio());
		 				mapeo.setSemana(mapeo_orig.getSemana());
		 				
		 				modificarMerma(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError(todoenorden);
	 				}

	 			}
	 			
	 			if (((MermasPtGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0)
				{
					cps  = new consultaMermasPtServer(CurrentUser.get());
					String desc = cps.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
			}
		});
		
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarMerma(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.dia.getValue()!=null && this.dia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("dia", RutinasFechas.convertirDateToString(this.dia.getValue()));
		}
		else
		{
			opcionesEscogidas.put("dia", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.tecnicoCalidad.getValue()!=null && this.tecnicoCalidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tecnicoCalidad", this.tecnicoCalidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tecnicoCalidad", "");
		}
		if (this.tipo.getValue()!=null && this.tipo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tipo", this.tipo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipo", "");
		}
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.dia.getValue()!=null && this.dia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("dia", RutinasFechas.convertirDateToString(this.dia.getValue()));
		}
		else
		{
			opcionesEscogidas.put("dia", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.tecnicoCalidad.getValue()!=null && this.tecnicoCalidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tecnicoCalidad", this.tecnicoCalidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tecnicoCalidad", "");
		}
		if (this.tipo.getValue()!=null && this.tipo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tipo", this.tipo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipo", "");
		}
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarMerma(null);
		}
	}
	
	public void editarMerma(MapeoMermasPt r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			descripcion.setValue(null);
			r_mapeo=new MapeoMermasPt();
			if (!isBusqueda())
			{
				r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
				r_mapeo.setSemana(new Integer(RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY())));
				r_mapeo.setDia(new Date());
				r_mapeo.setTipo("Proceso");
			}
		}

		fieldGroup.setItemDataSource(new BeanItem<MapeoMermasPt>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarMerma(MapeoMermasPt r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((MermasPtGrid) uw.grid).remove(r_mapeo);
		uw.cmpts.eliminar(r_mapeo);
		((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((MermasPtGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
				
			ArrayList<MapeoMermasPt> r_vector = (ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector;
			
			Indexed indexed = ((MermasPtGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

		}
	}
	
	public void modificarMerma(MapeoMermasPt r_mapeo, MapeoMermasPt r_mapeo_orig)
	{
		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		String rdo = uw.cmpts.guardarCambios(r_mapeo);		
		
		if (!((MermasPtGrid) uw.grid).vector.isEmpty())
		{
			
			((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector).remove(r_mapeo_orig);
			((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector).add(r_mapeo);
			ArrayList<MapeoMermasPt> r_vector = (ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector;
			
			Indexed indexed = ((MermasPtGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			
            uw.presentarGrid(r_vector);
            
			((MermasPtGrid) uw.grid).setRecords((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector);
			((MermasPtGrid) uw.grid).scrollTo(r_mapeo);
			((MermasPtGrid) uw.grid).select(r_mapeo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearMerma(MapeoMermasPt r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setDia(new Date());
		r_mapeo.setIdCodigo(uw.cmpts.obtenerSiguiente());
		String rdo = uw.cmpts.guardarNuevo(r_mapeo);
//		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdqc_incidencias()));
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoMermasPt>(r_mapeo));
			if (((MermasPtGrid) uw.grid)!=null )//&& )
			{
				if (!((MermasPtGrid) uw.grid).vector.isEmpty())
				{
					Indexed indexed = ((MermasPtGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector).add(r_mapeo);
					((MermasPtGrid) uw.grid).setRecords((ArrayList<MapeoMermasPt>) ((MermasPtGrid) uw.grid).vector);
					((MermasPtGrid) uw.grid).scrollTo(r_mapeo);
					((MermasPtGrid) uw.grid).select(r_mapeo);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoMermasPt> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		cps  = new consultaMermasPtServer(CurrentUser.get());
		vectorArticulos=cps.vector();
		this.vHelp = new ventanaAyuda(this.articulo, this.descripcion, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

	private void establecerCamposObligatorios()
	{
	}

	private String comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0) return "Rellena el ejercicio";
			if (this.semana.getValue()==null || this.semana.getValue().toString().length()==0) return "Rellena la semana";
			if (this.dia.getValue()==null || this.dia.getValue().toString().length()==0) return "Rellena la fecha del dia";
			if (this.articulo.getValue()==null || this.articulo.getValue().length()==0) return "Rellena el articulo";
			if (this.tecnicoCalidad.getValue()==null || this.tecnicoCalidad.getValue().toString().length()==0) return "Rellena el tecnico de calidad";
			if (this.tipo.getValue()==null || this.tipo.getValue().toString().length()==0) return "Rellena el tipo de merma";
			if (this.cantidad.getValue()==null || this.cantidad.getValue().length()==0) return "Rellena la cantidad";
		}
		
		return "OK";
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
	
	private void cargarCombo()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		this.ejercicio.removeAllItems();
		this.semana.removeAllItems();
		this.tipo.removeAllItems();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.ejercicio.addItem((String) vector.get(i));
		}
		
		this.ejercicio.setValue(vector.get(0).toString());

		String semana=String.valueOf(RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY()));
		int semanas = RutinasFechas.semanasAño(RutinasFechas.añoActualYYYY());
		
		for (int i=1; i<=semanas; i++)
		{
			this.semana.addItem(String.valueOf(i));
		}

		this.tipo.addItem("Proceso");
		this.tipo.addItem("Descorche");
		this.tipo.addItem("Otros");
		
		
	}
}
