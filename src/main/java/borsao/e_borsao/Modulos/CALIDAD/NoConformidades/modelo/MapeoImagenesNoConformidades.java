package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoImagenesNoConformidades extends MapeoGlobal
{
    private Integer idqc_incidencias;
	
	private String ruta;
	private String archivo;
	private Integer orden;
	
	private String agregar="";
	private String eliminar="";

	public MapeoImagenesNoConformidades()
	{
		this.setRuta(eBorsao.get().prop.rutaImagenesCalidad);
	}

	public Integer getIdqc_incidencias() {
		return idqc_incidencias;
	}

	public void setIdqc_incidencias(Integer idqc_incidencias) {
		this.idqc_incidencias = idqc_incidencias;
	}

	public String getRuta() {
		return ruta;
	}

	private void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getAgregar() {
		return agregar;
	}

	public void setAgregar(String agregar) {
		this.agregar = agregar;
	}

	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

}