package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.server.consultaCosteMaquinaServer;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.server.consultaCostePersonalServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoDesgloseValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaValoracionNoConformidades extends Ventana
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Button btnBotonEliminar= null;
	private Button btnBotonCalcular= null;
	private Button btnBotonDesglose= null;
	
	private ComboBox cmbArticulo = null;
	private ComboBox cmbOrigen = null;
	
	private CheckBox chkParada;
	private CheckBox chkInterrupcion;
	private CheckBox chkVelocidad;
	private CheckBox chkRetrabajo;
	private CheckBox chkRetrabajoMp;
	private CheckBox chkEtt;
	private CheckBox chkEttMp;
	
	private TextField txtCajas = null;
	private TextField txtFecha = null;
	private TextField txtUsuario = null;
	private TextArea txtIncidencia = null;
	
	private TextField txtTiempoParada= null;
	private TextField txtTiempoInterrupcion= null;
	private TextField txtTiempoRetrabajo= null;
	private TextField txtTiempoRetrabajoMp= null;
	private TextField txtTiempoVelocidad= null;
	private TextField txtVelocidadHabitual= null;
	private TextField txtVelocidadTrabajo= null;
	
//	private TextField txtPersonalRetrabajo= null;
//	private TextField txtPersonalRetrabajoMp= null;
	
//	private TextField txtPrecioHora= null;
	private TextField txtImporteGlobal= null;

	private String lineaProduccion = null;
	private Integer idIncidencia = null;
	
	public pantallaValoracionNoConformidades(String r_linea, Integer r_id)
	{
		this.idIncidencia = r_id;
		this.lineaProduccion = r_linea;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbOrigen=new ComboBox("Origen");
				this.cmbOrigen.addItem("PRODUCCION");
				this.cmbOrigen.addItem("CLIENTE");
				this.cmbOrigen.addItem("PROVEEDOR");
				this.cmbOrigen.setValue("PRODUCCION");
				this.cmbOrigen.setNewItemsAllowed(false);
				this.cmbOrigen.setNullSelectionAllowed(false);
				this.cmbOrigen.setRequired(true);
				this.cmbOrigen.setWidth("200px");
				
				this.txtFecha=new TextField("Día");
				this.txtFecha.setValue(RutinasFechas.fechaActual());
				this.txtFecha.setWidth("150px");
				
				this.txtUsuario = new TextField("Usuario");
				this.txtUsuario.setWidth("250px");
				
				fila1.addComponent(cmbOrigen);
				fila1.addComponent(txtFecha);
				fila1.addComponent(txtUsuario);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbArticulo=new ComboBox("Articulo");
				this.cmbArticulo.setWidth("550px");
				this.cmbArticulo.setRequired(true);
				this.cmbArticulo.setNullSelectionAllowed(false);
				
				this.txtCajas=new TextField("Cantidad ");				
				this.txtCajas.setWidth("150px");
				this.txtCajas.setRequired(true);
				
				fila2.addComponent(cmbArticulo);
				fila2.addComponent(txtCajas);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtIncidencia=new TextArea("Incidencia");
				this.txtIncidencia.setWidth("800px");
				this.txtIncidencia.setRequired(true);
				
				fila3.addComponent(txtIncidencia);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
			
				this.chkParada = new CheckBox("Provoca Parada de Línea?", false);
				this.txtTiempoParada = new TextField("Tiempo");
				this.txtTiempoParada.setEnabled(false);
				this.txtTiempoParada.setWidth("80px");
				this.txtTiempoParada.setDescription("Tiempo en minutos");
				this.txtTiempoParada.setStyleName("rightAligned");
				this.txtTiempoParada.addStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.chkInterrupcion= new CheckBox("Provoca Interrupciones en la Producción?", false);				
				this.txtTiempoInterrupcion  = new TextField("Tiempo");
				this.txtTiempoInterrupcion.setEnabled(false);
				this.txtTiempoInterrupcion.setWidth("80");
				this.txtTiempoInterrupcion.setDescription("Tiempo en minutos");
				this.txtTiempoInterrupcion.setStyleName("rightAligned");
				this.txtTiempoInterrupcion.addStyleName(ValoTheme.TEXTFIELD_TINY);
				
			fila4.addComponent(this.chkParada);
			fila4.addComponent(this.txtTiempoParada);
			fila4.addComponent(this.chkInterrupcion);
			fila4.addComponent(this.txtTiempoInterrupcion);
			fila4.setComponentAlignment(this.chkParada, Alignment.MIDDLE_LEFT);
			fila4.setComponentAlignment(this.chkInterrupcion, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
				
				this.chkVelocidad = new CheckBox("Provoca Bajar la velocidad de la Línea?", false);
				this.txtVelocidadHabitual= new TextField("Vel. Normal (B/H)");
				this.txtVelocidadHabitual.setEnabled(false);
				this.txtVelocidadHabitual.setDescription("Botellas Hora");
				this.txtVelocidadHabitual.setStyleName("rightAligned");
				this.txtVelocidadHabitual.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtVelocidadHabitual.setWidth("120px");
				this.txtVelocidadTrabajo= new TextField("Vel. Trabajo (B/H)");
				this.txtVelocidadTrabajo.setEnabled(false);
				this.txtVelocidadTrabajo.setDescription("Botellas Hora");
				this.txtVelocidadTrabajo.setStyleName("rightAligned");
				this.txtVelocidadTrabajo.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtVelocidadTrabajo.setWidth("120px");
				this.txtTiempoVelocidad= new TextField("Tiempo Producción (H) ");
				this.txtTiempoVelocidad.setEnabled(false);
				this.txtTiempoVelocidad.setDescription("Tiempo en Horas");
				this.txtTiempoVelocidad.setStyleName("rightAligned");
				this.txtTiempoVelocidad.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtTiempoVelocidad.setWidth("120px");
				
			fila5.addComponent(this.chkVelocidad);
			fila5.addComponent(this.txtVelocidadHabitual);
			fila5.addComponent(this.txtVelocidadTrabajo);
			fila5.addComponent(this.txtTiempoVelocidad);
			fila5.setComponentAlignment(this.chkVelocidad, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila6 = new HorizontalLayout();
			fila6.setWidthUndefined();
			fila6.setSpacing(true);
				
				this.chkRetrabajo = new CheckBox("Provoca Retrabajar el producto terminado?", false);
				this.txtTiempoRetrabajo = new TextField("Tiempo");
				this.txtTiempoRetrabajo.setEnabled(false);
				this.txtTiempoRetrabajo.setStyleName("rightAligned");
				this.txtTiempoRetrabajo.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtTiempoRetrabajo.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajo.setWidth("80px");
				this.chkEtt = new CheckBox("Ett?", false);
				this.chkEtt.setEnabled(false);
				
				this.chkRetrabajoMp = new CheckBox("Provoca adaptar la Materia Seca?", false);
				this.txtTiempoRetrabajoMp = new TextField("Tiempo");
				this.txtTiempoRetrabajoMp.setEnabled(false);
				this.txtTiempoRetrabajoMp.setStyleName("rightAligned");
				this.txtTiempoRetrabajoMp.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtTiempoRetrabajoMp.setDescription("Tiempo en Minutos");
				this.txtTiempoRetrabajoMp.setWidth("80px");
				this.chkEttMp = new CheckBox("Ett?", false);
				this.chkEttMp.setEnabled(false);
				
			fila6.addComponent(this.chkRetrabajo);
			fila6.addComponent(this.txtTiempoRetrabajo);
			fila6.addComponent(this.chkEtt);
			fila6.addComponent(this.chkRetrabajoMp);
			fila6.addComponent(this.txtTiempoRetrabajoMp);
			fila6.addComponent(this.chkEttMp);
			fila6.setComponentAlignment(this.chkRetrabajo, Alignment.MIDDLE_LEFT);
			fila6.setComponentAlignment(this.chkRetrabajoMp, Alignment.MIDDLE_LEFT);
			fila6.setComponentAlignment(this.chkEtt, Alignment.MIDDLE_LEFT);
			fila6.setComponentAlignment(this.chkEttMp, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila7 = new HorizontalLayout();
			fila7.setWidthUndefined();
			fila7.setSpacing(true);
			
				this.txtImporteGlobal = new TextField("Importe Total Incidencia");
				this.txtImporteGlobal.setValue("0");
				this.txtImporteGlobal.setEnabled(true);
				this.txtImporteGlobal.setStyleName("rightAligned");
				this.txtImporteGlobal.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtImporteGlobal.setDescription("Precio Medio Coste Incidencia");
				this.txtImporteGlobal.setWidth("150px");
				this.txtImporteGlobal.setVisible(false);
				if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Calidad")
						|| (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))) this.txtImporteGlobal.setVisible(true);

				btnBotonCalcular = new Button("Calcular");
				btnBotonCalcular.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnBotonCalcular.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonCalcular.setEnabled(false);
				btnBotonCalcular.setVisible(false);
				if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Calidad")
						|| (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))) btnBotonCalcular.setVisible(true);
				
				btnBotonDesglose= new Button("Ver Desglose");
				btnBotonDesglose.addStyleName(ValoTheme.BUTTON_PRIMARY);
				btnBotonDesglose.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonDesglose.setEnabled(false);
				btnBotonDesglose.setVisible(false);
				if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Calidad")
						|| (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))) btnBotonDesglose.setVisible(true);
				
				fila7.addComponent(this.btnBotonCalcular);
			fila7.addComponent(this.txtImporteGlobal);
			fila7.addComponent(this.btnBotonDesglose);
			fila7.setComponentAlignment(this.btnBotonCalcular, Alignment.BOTTOM_LEFT);
			fila7.setComponentAlignment(this.btnBotonDesglose, Alignment.BOTTOM_LEFT);
			
//			HorizontalLayout fila8 = new HorizontalLayout();
//			fila8.setWidthUndefined();
//			fila8.setSpacing(true);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		controles.addComponent(fila6);
		controles.addComponent(fila7);
//		controles.addComponent(fila8);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Incidencias Linea " + r_linea + " Embotellado          " + this.idIncidencia );
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("850px");
		
		btnBotonEliminar= new Button("Eliminar");
		btnBotonEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonEliminar.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana = new Button("Guardar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonEliminar);		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonEliminar, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.rellenarEntradasDatos();
		this.habilitarCampos();
		this.cargarListeners();
		this.txtCajas.focus();
	}

	private void habilitarCampos()
	{
	
		this.cmbOrigen.setEnabled(false);
		this.txtFecha.setEnabled(false);
		
		this.cmbArticulo.setEnabled(true);
		this.txtCajas.setEnabled(true);
		this.txtIncidencia.setEnabled(true);			
	}
	
	private void cargarListeners()
	{
		btnBotonCalcular.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				calcular();
			}
		});
		btnBotonDesglose.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verDesglose();
			}
		});
		
		chkParada.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoParada.setEnabled(chkParada.getValue());
				btnBotonCVentana.setEnabled(false);
			}
		});
		
		chkInterrupcion.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoInterrupcion.setEnabled(chkInterrupcion.getValue());
				btnBotonCVentana.setEnabled(false);
			}
		});

		chkRetrabajo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoRetrabajo.setEnabled(chkRetrabajo.getValue());
				chkEtt.setEnabled(chkRetrabajo.getValue());
				btnBotonCVentana.setEnabled(false);
			}
		});

		chkRetrabajoMp.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtTiempoRetrabajoMp.setEnabled(chkRetrabajoMp.getValue());
				chkEttMp.setEnabled(chkRetrabajoMp.getValue());
				btnBotonCVentana.setEnabled(false);
			}
		});

		chkVelocidad.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtVelocidadHabitual.setEnabled(chkVelocidad.getValue());
				txtVelocidadTrabajo.setEnabled(chkVelocidad.getValue());
				txtTiempoVelocidad.setEnabled(chkVelocidad.getValue());
				btnBotonCVentana.setEnabled(false);
			}
		});
		btnBotonEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				consultaNoConformidadesServer cns = new consultaNoConformidadesServer(CurrentUser.get());
				MapeoValoracionNoConformidades mapeo = new MapeoValoracionNoConformidades();
				mapeo.setIdqc_incidencias(idIncidencia);
				cns.eliminarValoracion(mapeo);
				close();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				String todoCorrecto = todoEnOrden();
				if (todoCorrecto.equals("OK"))
				{
					
					MapeoValoracionNoConformidades newMapeoVal = new MapeoValoracionNoConformidades();
					newMapeoVal.setIdqc_incidencias(idIncidencia);
					
					if (chkInterrupcion.getValue())
					{
						newMapeoVal.setInterrupcion("S");
						newMapeoVal.setTiempoInterrupcion(new Integer(txtTiempoInterrupcion.getValue()));
					}
					else
					{
						newMapeoVal.setInterrupcion("N");
						newMapeoVal.setTiempoInterrupcion(0);
					}
					
					if (chkParada.getValue())
					{
						newMapeoVal.setParada("S");
						newMapeoVal.setTiempoParada(new Integer(txtTiempoParada.getValue()));
					}
					else 
					{
						newMapeoVal.setParada("N");
						newMapeoVal.setTiempoParada(0);
					}
					if (chkRetrabajo.getValue())
					{
						newMapeoVal.setRetrabajo("S");
						newMapeoVal.setTiempoRetrabajo(new Integer(txtTiempoRetrabajo.getValue()));
					}
					else 
					{
						newMapeoVal.setRetrabajo("N");
						newMapeoVal.setTiempoRetrabajo(0);
					}
					if (chkRetrabajoMp.getValue())
					{
						newMapeoVal.setRetrabajomp("S");
						newMapeoVal.setTiempoRetrabajomp(new Integer(txtTiempoRetrabajoMp.getValue()));
					}
					else 
					{
						newMapeoVal.setRetrabajomp("N");
						newMapeoVal.setTiempoRetrabajomp(0);
					}
					if (chkVelocidad.getValue())
					{
						newMapeoVal.setVelocidad("S"); 
						newMapeoVal.setTiempoVelocidad(new Integer(txtTiempoVelocidad.getValue()));
						newMapeoVal.setVelocidadHabitual(new Integer(txtVelocidadHabitual.getValue()));
						newMapeoVal.setVelocidadTrabajo(new Integer(txtVelocidadTrabajo.getValue()));
					}
					else 
					{
						newMapeoVal.setVelocidad("N");
						newMapeoVal.setTiempoVelocidad(0);
						newMapeoVal.setVelocidadHabitual(0);
						newMapeoVal.setVelocidadTrabajo(0);
					}
					
					newMapeoVal.setImporteGlobal(new Double(txtImporteGlobal.getValue()));
					
					String rdo = null;
					consultaNoConformidadesServer cns = new consultaNoConformidadesServer(CurrentUser.get());
					
					if (btnBotonCVentana.getCaption().equals("Cancelar"))
					{
						rdo = cns.guardarNuevoValoracion(newMapeoVal);
					}
					else
					{
						rdo = cns.guardarCambiosValoracion(newMapeoVal);
						calcular();
					}
					if (rdo!=null)
					{
						Notificaciones.getInstance().mensajeError(rdo);
					}
					else
					{
						btnBotonCalcular.setEnabled(true);
						btnBotonDesglose.setEnabled(true);
						btnBotonCVentana.setEnabled(true);
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia(todoCorrecto);
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private String todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		String devolver = "OK";
		if (this.cmbOrigen.getValue()==null || this.cmbOrigen.getValue().toString().length()==0)
		{
			devolver = "Hay que rellenar el origen de la incidencia";
			cmbOrigen.focus();
			return devolver;
		}
		else if (this.cmbArticulo.getValue()==null || this.cmbArticulo.getValue().toString().length()==0)
		{
			devolver = "Hay que rellenar el articulo que provoca la incidencia";
			cmbArticulo.focus();
			return devolver;
		}
		else if (this.txtCajas.getValue()==null || this.txtCajas.getValue().length()==0) 
		{
			devolver ="Has de rellenar la cantidad afectada por la incidencia";			
			txtCajas.focus();
			return devolver;
		}
		else if (this.txtFecha.getValue()==null || this.txtFecha.getValue().length()==0) 
		{
			devolver ="Es obligatorio indicar la fecha de la incidencia";
			txtFecha.focus();
			return devolver;
		}
		else if (this.txtUsuario.getValue()==null || this.txtUsuario.getValue().length()==0)
		{
			devolver ="Hay que rellenar el usuario que ha detectado la incidencia";
			txtUsuario.focus();
			return devolver;
		}
		else if (this.chkParada.getValue() && (this.txtTiempoParada.getValue()==null || this.txtTiempoParada.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo de parada sufrido por la incidencia";
			txtTiempoParada.focus();
			return devolver;
		}
		else if (this.chkInterrupcion.getValue() && (this.txtTiempoInterrupcion.getValue()==null || this.txtTiempoInterrupcion.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo de itnerrupciones total sufrido por la incidencia";
			txtTiempoInterrupcion.focus();
			return devolver;
		}
		else if (this.chkRetrabajo.getValue() && (this.txtTiempoRetrabajo.getValue()==null || this.txtTiempoRetrabajo.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo retrabajado para resolver la incidencia";
			txtTiempoRetrabajo.focus();
			return devolver;
		}
		else if (this.chkRetrabajoMp.getValue() && (this.txtTiempoRetrabajoMp.getValue()==null || this.txtTiempoRetrabajoMp.getValue().length()==0))
		{
			devolver ="Hay que indicar el tiempo retrabajado la materia prima para poder utilizarla";
			txtTiempoRetrabajoMp.focus();
			return devolver;
		}
		else if (this.chkVelocidad.getValue() && (this.txtVelocidadHabitual.getValue()==null || this.txtVelocidadHabitual.getValue().length()==0))
		{
			devolver ="Hay que indicar la velocidad habitual al que hubieramos producido sin existir la incidencia";
			txtVelocidadHabitual.focus();
			return devolver;
		}
		else if (this.chkVelocidad.getValue() && (this.txtTiempoVelocidad.getValue()==null || this.txtTiempoVelocidad.getValue().length()==0))
		{
			devolver ="Hay que indicar cuánto tiempo hemos estado trabajando a menor velocidad de la habitual";
			txtTiempoVelocidad.focus();
			return devolver;
		}
		else if (this.chkVelocidad.getValue() && (this.txtVelocidadTrabajo.getValue()==null || this.txtVelocidadTrabajo.getValue().length()==0))
		{
			devolver ="Hay que indicar la velocidad a la que hemos tenido que producir por causa de la incidencia";
			txtVelocidadTrabajo.focus();
			return devolver;
		}
		else if (this.txtImporteGlobal.getValue()==null || this.txtImporteGlobal.getValue().length()==0)
		{
			devolver ="Hay que calcular un importe ";
			txtImporteGlobal.focus();
			return devolver;
		}

		return devolver;
	}

	private void rellenarEntradasDatos()
	{
		ArrayList<MapeoNoConformidades> vectorIncidencia = null;
		consultaNoConformidadesServer cns = new consultaNoConformidadesServer(CurrentUser.get());
		MapeoNoConformidades mapeoNC = new MapeoNoConformidades();
		mapeoNC.setIdqc_incidencias(this.idIncidencia);
		vectorIncidencia =cns.datosNoConformidadesGlobal(mapeoNC,null);
		
		for (int i=0;i< vectorIncidencia.size();i++)
		{
			this.cmbArticulo.addItem(((MapeoNoConformidades) vectorIncidencia.get(i)).getArticulo() + " " + ((MapeoNoConformidades) vectorIncidencia.get(i)).getDescripcion());
			this.cmbArticulo.setValue(((MapeoNoConformidades) vectorIncidencia.get(i)).getArticulo() + " " + ((MapeoNoConformidades) vectorIncidencia.get(i)).getDescripcion());

			this.txtCajas.setValue(((MapeoNoConformidades) vectorIncidencia.get(i)).getCantidad().toString());
			this.txtIncidencia.setValue(((MapeoNoConformidades) vectorIncidencia.get(i)).getIncidencia());
			this.txtUsuario.setValue(((MapeoNoConformidades) vectorIncidencia.get(i)).getNotificador());
		}
		
		MapeoValoracionNoConformidades mapeoVal = cns.datosValoracionNoConformidadesGlobal(this.idIncidencia);
		
		if (mapeoVal!=null)
		{
			this.chkParada.setValue(mapeoVal.getParada().equals("S"));
			this.chkInterrupcion.setValue(mapeoVal.getInterrupcion().equals("S"));
			this.chkRetrabajo.setValue(mapeoVal.getRetrabajo().equals("S"));
			this.chkRetrabajoMp.setValue(mapeoVal.getRetrabajomp().equals("S"));
			this.chkVelocidad.setValue(mapeoVal.getVelocidad().equals("S"));
			
			this.txtTiempoParada.setValue(mapeoVal.getTiempoParada().toString());
			this.txtTiempoParada.setEnabled(mapeoVal.getParada().equals("S"));
			this.txtTiempoInterrupcion.setValue(mapeoVal.getTiempoInterrupcion().toString());
			this.txtTiempoInterrupcion.setEnabled(mapeoVal.getInterrupcion().equals("S"));
			this.txtTiempoVelocidad.setValue(mapeoVal.getTiempoVelocidad().toString());
			this.txtTiempoVelocidad.setEnabled(mapeoVal.getVelocidad().equals("S"));
			this.txtTiempoRetrabajo.setValue(mapeoVal.getTiempoRetrabajo().toString());
			this.txtTiempoRetrabajo.setEnabled(mapeoVal.getRetrabajo().equals("S"));
			this.txtTiempoRetrabajoMp.setValue(mapeoVal.getTiempoRetrabajomp().toString());
			this.txtTiempoRetrabajoMp.setEnabled(mapeoVal.getRetrabajomp().equals("S"));
			this.txtVelocidadHabitual.setValue(mapeoVal.getVelocidadHabitual().toString());
			this.txtVelocidadHabitual.setEnabled(mapeoVal.getVelocidad().equals("S"));
			this.txtVelocidadTrabajo.setValue(mapeoVal.getVelocidadTrabajo().toString());
			this.txtVelocidadTrabajo.setEnabled(mapeoVal.getVelocidad().equals("S"));
			
			this.txtImporteGlobal.setValue(mapeoVal.getImporteGlobal().toString());
			
			this.btnBotonCVentana.setCaption("Cerrar");
			this.btnBotonCalcular.setEnabled(true);
			this.btnBotonDesglose.setEnabled(true);
		}	
		else
		{
			this.btnBotonCVentana.setCaption("Cancelar");
			this.btnBotonCalcular.setEnabled(false);
			this.btnBotonDesglose.setEnabled(false);
		}
	}
	
	public void calcularImporteIncidencia(boolean r_regenerar)
	{
		/*
		 * Declaracion variables y accesos parte servidora bbdd
		 */
		String rdo = null;
		BigDecimal bd = null;
		consultaNoConformidadesServer cns = null;
		consultaParametrosCalidadServer cpcs = null;
		consultaCostePersonalServer cps = null;
		consultaCosteMaquinaServer cms = null;
		/*
		 * Inicializacion variables calculo
		 */
		Double importeTotal = new Double(0);
		Double tiempo = new Double(0);
		Double tiempoParada = new Double(0);
		Double tiempoReTrabajo = new Double(0);
		Double tiempoReTrabajoMP = new Double(0);
		Integer ejercicio = null;
		/*
		 * Instanaciación accesos bbdd
		 */		
	    cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
	    /*
	     * obtencion ejercicio mas proximo parametrizado en calidad
	     */
	    ejercicio = Integer.valueOf(cpcs.recuperarEjercicioCalidad("Horas"));
	    /*
	     * Si no tengo parametrizacion de calidad no calculo nada.
	     * En caso contrario valorare el coste segun lo tarificado para el ultimo ejercicio conocido más cercano al actual
	     */
	    if (ejercicio!=null && ejercicio>0)
	    {
	    	cns = consultaNoConformidadesServer.getInstance(CurrentUser.get());
	    	MapeoDesgloseValoracionNoConformidades mapeo = new MapeoDesgloseValoracionNoConformidades();
	    	mapeo.setIdqc_incidencias(this.idIncidencia);
	    	if (r_regenerar)
	    	{
	    		cns.eliminarDesgloseValoracion(mapeo);
		    	/*
		    	 * Calculo unidades tiempo
		    	 */
		    	if (chkParada.getValue())
		    	{
		    		tiempoParada = tiempoParada + new Double(txtTiempoParada.getValue())/60;
		    	}
		    	if (chkInterrupcion.getValue())
		    	{
		    		tiempoParada = tiempoParada + new Double(txtTiempoInterrupcion.getValue())/60;
		    	}
		    	if (chkVelocidad.getValue())
		    	{
		    		Double velocidadParada = new Double(txtTiempoVelocidad.getValue()) - (new Double(txtTiempoVelocidad.getValue())*new Integer(txtVelocidadTrabajo.getValue())/new Integer(txtVelocidadHabitual.getValue()));
		    		tiempoParada = tiempoParada + velocidadParada;
		    	}		
		    	if (chkRetrabajo.getValue())
		    	{
		    		tiempoReTrabajo = new Double(txtTiempoRetrabajo.getValue())/60;
		    	}
		    	if (chkRetrabajoMp.getValue())
		    	{
		    		tiempoReTrabajoMP = new Double(txtTiempoRetrabajoMp.getValue())/60;
		    	}
			    /*
			     * COSTES DIRECTOS
			     */
		    	cps = consultaCostePersonalServer.getInstance(CurrentUser.get());
		    	
		    	if (chkParada.getValue() || chkInterrupcion.getValue() || chkVelocidad.getValue())
		    	{
		    		rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, this.lineaProduccion,false,tiempoParada);
		    	}
		    	if (chkRetrabajo.getValue() && chkRetrabajoMp.getValue())
		    	{
		    		if (chkEtt.getValue() && chkEttMp.getValue())
		    		{
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOSETT",true, tiempoReTrabajo+ tiempoReTrabajoMP);
		    		}
		    		else if (!chkEtt.getValue() && !chkEttMp.getValue())
		    		{
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOS",false, tiempoReTrabajo+tiempoReTrabajoMP);
		    		}
		    		else if (chkEtt.getValue() && !chkEttMp.getValue())
		    		{
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOSETT",true, tiempoReTrabajo);
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOS",false, tiempoReTrabajoMP);
		    		}
		    		else if (!chkEtt.getValue() && chkEttMp.getValue())
		    		{
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOS",false, tiempoReTrabajo);
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOSETT",true, tiempoReTrabajoMP);
		    		}
		    	}
		    	else if (chkRetrabajo.getValue() && !chkRetrabajoMp.getValue()) 
		    	{
		    		if (chkEtt.getValue())
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOSETT",true, tiempoReTrabajo);
		    		else
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOS",false, tiempoReTrabajo);
		    	}
		    	else if (!chkRetrabajo.getValue() && chkRetrabajoMp.getValue())
		    	{
		    		if (chkEttMp.getValue())
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOSETT",true, tiempoReTrabajoMP);
		    		else
		    			rdo = cps.generarCosteHoraArea(this.idIncidencia, ejercicio, "RETRABAJOS",false, tiempoReTrabajoMP);
		    	}
			    /*
			     * COSTES INDIRECTOS	      
			     */
		    	if (chkParada.getValue() || chkInterrupcion.getValue() || chkVelocidad.getValue())
		    	{
				    cms = consultaCosteMaquinaServer.getInstance(CurrentUser.get());
				    rdo = cms.generarCosteHoraGlobalArea(this.idIncidencia, ejercicio, this.lineaProduccion, tiempoParada);
		    	}
	    	}
	    	
	    	importeTotal = cns.recuperarCosteTotalIncidencia(this.idIncidencia);

	    	bd = new BigDecimal(importeTotal);
	    	bd = bd.setScale(2, RoundingMode.HALF_EVEN);
	    	txtImporteGlobal.setValue(bd.toString());
	    }	
	    else
	    {
	    	txtImporteGlobal.setValue("0");
	    }
	}
	
	private void verDesglose()
	{
		pantallaDesgloseCostes vt = new pantallaDesgloseCostes(this, "Desglose Costes ", idIncidencia);
		getUI().addWindow(vt);
	}
	
	private void calcular()
	{
		VentanaAceptarCancelar vac = new VentanaAceptarCancelar(this, "Se calculará el coste con la parametrizacion por defecto. Se perderán los datos añadidos manualmente.", "Regenerar", "Mantener", null, null);
		getUI().addWindow(vac);
	}
	
	public void aceptarProceso(String r_accion)
	{
		calcularImporteIncidencia(true);
	}
	
	public void cancelarProceso(String r_accion)
	{
		calcularImporteIncidencia(false);
	}
}