package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoMermasPt;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaMermasPtServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionMermasPt extends Window
{
	private MapeoProgramacion mapeoProgramacion = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private static String tipo ="Proceso";
	private Label lblArticulo = null;
	
	private TextField txtArticulo = null;
	private TextField txtCantidad = null;
	private TextField txtCantidadTotal = null;
	private TextField txtTecnicoCalidad = null;
	private TextArea txtObservaciones = null;
	private EntradaDatosFecha txtFecha = null;
	

	public PeticionMermasPt(MapeoProgramacion r_mapeo)
	{
		this.mapeoProgramacion = r_mapeo;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.txtFecha=new EntradaDatosFecha("Día");
				this.txtFecha.setValue(RutinasFechas.fechaActual());
				this.txtFecha.setWidth("150px");
				
				this.txtTecnicoCalidad=new TextField("Tecnico Calidad");
				this.txtTecnicoCalidad.setWidth("400px");
				this.txtTecnicoCalidad.setRequired(true);
				this.txtTecnicoCalidad.setValue(CurrentUser.get());

				this.txtCantidadTotal=new TextField("ACUMULADO ");				
				this.txtCantidadTotal.setWidth("150px");
				this.txtCantidadTotal.setEnabled(false);
				this.txtCantidadTotal.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
				
				fila1.addComponent(txtFecha);
				fila1.addComponent(txtTecnicoCalidad);
				fila1.addComponent(txtCantidadTotal);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setRequired(true);
				this.txtArticulo.setValue(this.mapeoProgramacion.getArticulo());
				
				this.lblArticulo = new Label();
				this.lblArticulo.setValue(mapeoProgramacion.getDescripcion());
				this.lblArticulo.setWidth("400px");
				
				this.txtCantidad=new TextField("Cantidad ");				
				this.txtCantidad.setWidth("150px");
				this.txtCantidad.setRequired(true);
				this.txtCantidad.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
				
				fila2.addComponent(txtArticulo);
				fila2.addComponent(lblArticulo);
				fila2.setComponentAlignment(lblArticulo, Alignment.MIDDLE_LEFT);
				fila2.addComponent(txtCantidad);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtObservaciones=new TextArea("Observaciones");
				this.txtObservaciones.setWidth("800px");
				this.txtObservaciones.setRequired(true);
				
				fila3.addComponent(txtObservaciones);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Mermas Producto Terminado " + this.mapeoProgramacion.getArticulo() + " " + r_mapeo.getDescripcion() );
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarAcumulado();
		this.habilitarCampos();
		this.cargarListeners();
		this.txtCantidad.focus();
	}
	
	private void habilitarCampos()
	{
	
		this.txtFecha.setEnabled(false);
		this.txtCantidad.setEnabled(true);
		this.txtObservaciones.setEnabled(true);			
		this.txtArticulo.setEnabled(true);			
		this.txtTecnicoCalidad.setEnabled(true);			
	}
	
	private void cargarListeners()
	{
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				String todoCorrecto = todoEnOrden();
				if (todoCorrecto.equals("OK"))
				{
					consultaMermasPtServer cms = consultaMermasPtServer.getInstance(CurrentUser.get());
					MapeoMermasPt newMapeo = new MapeoMermasPt();
				
					newMapeo.setIdCodigo(cms.obtenerSiguiente());
					newMapeo.setArticulo(txtArticulo.getValue().toString().substring(0, 7));
					newMapeo.setCantidad(new Integer(txtCantidad.getValue()));
					newMapeo.setDia(txtFecha.getValue());
					newMapeo.setTecnicoCalidad(txtTecnicoCalidad.getValue().toString());
					newMapeo.setObservaciones(txtObservaciones.getValue());
					newMapeo.setEjercicio(mapeoProgramacion.getEjercicio());
					newMapeo.setSemana(new Integer(mapeoProgramacion.getSemana()));
					newMapeo.setTipo(tipo);
					
					String rdo = null;
					
					rdo = cms.guardarNuevo(newMapeo);
					
					if (rdo!=null && rdo.length()>=1 )
						Notificaciones.getInstance().mensajeError(rdo);
					else
						close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia(todoCorrecto);
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
		ValueChangeListener lisArt = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtArticulo.getValue()!="")
				{
					String articulo = txtArticulo.getValue().toString().substring(0, 7);
					consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
					lblArticulo.setValue(cas.obtenerDescripcionArticulo(articulo));
					txtArticulo.setValue(articulo);
					txtCantidad.focus();
				}
			}
		};
		txtArticulo.addValueChangeListener(lisArt);
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private String todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		String devolver = "OK";
		if (this.txtArticulo.getValue()==null || this.txtArticulo.getValue().toString().length()==0)
		{
			devolver = "Hay que rellenar el articulo";
			txtArticulo.focus();
		}
		else if (this.txtCantidad.getValue()==null || this.txtCantidad.getValue().length()==0) 
		{
			devolver ="Has de rellenar la cantidad de mermas.";			
			txtCantidad.focus();
		}
		else if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0) 
		{
			devolver ="Es obligatorio indicar la fecha.";
			txtFecha.focus();
		}
		else if (this.txtTecnicoCalidad.getValue()==null || this.txtTecnicoCalidad.getValue().length()==0)
		{
			devolver ="Hay que rellenar el usuario que procesa las mermas";
			txtTecnicoCalidad.focus();
		}
		return devolver;
	}

	private Integer cargarAcumulado()
	{
		Integer total = null;
		consultaMermasPtServer cms = consultaMermasPtServer.getInstance(CurrentUser.get());
		total = cms.cargarAcumulado(this.mapeoProgramacion.getEjercicio(), new Integer(this.mapeoProgramacion.getSemana()),this.mapeoProgramacion.getArticulo(),this.txtFecha.getFecha(),this.tipo);
		this.txtCantidadTotal.setValue(total.toString());
		return total;
	}
}