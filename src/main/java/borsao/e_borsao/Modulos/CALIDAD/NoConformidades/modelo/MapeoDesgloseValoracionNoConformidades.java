package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDesgloseValoracionNoConformidades extends MapeoGlobal
{
    private Integer idqc_incidencias;
    private Double costeUnitario;
    private Double unidades;
    private Double tiempo;
    
    private String area;
    private String concepto;
    private String ett;
    private boolean eliminar=false;
    
	public MapeoDesgloseValoracionNoConformidades()
	{
	}

	public Integer getIdqc_incidencias() {
		return idqc_incidencias;
	}

	public void setIdqc_incidencias(Integer idqc_incidencias) {
		this.idqc_incidencias = idqc_incidencias;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String r_area) {
		this.area = r_area;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String r_concepto) {
		this.concepto = r_concepto;
	}

	public String getEtt() {
		return ett;
	}

	public void setEtt(String r_ett) {
		this.ett = r_ett;
	}

	public Double getCosteUnitario() {
		return costeUnitario;
	}

	public void setCosteUnitario(Double r_costeUnitario) {
		this.costeUnitario = r_costeUnitario;
	}

	public Double getUnidades() {
		return unidades;
	}

	public void setUnidades(Double r_unidades) {
		this.unidades= r_unidades;
	}

	public Double getTiempo() {
		return tiempo;
	}

	public void setTiempo(Double tiempo) {
		this.tiempo = tiempo;
	}

	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}
}