package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.pantallaDesgloseCostes;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class DesgloseCostesGrid extends GridPropio 
{
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	private boolean editable = false;
	private boolean conFiltro = true;
	
	private MapeoDesgloseValoracionNoConformidades mapeo=null;
	private MapeoDesgloseValoracionNoConformidades mapeoOrig=null;
	private pantallaDesgloseCostes app = null;
	
    public DesgloseCostesGrid(pantallaDesgloseCostes r_app , ArrayList<MapeoDesgloseValoracionNoConformidades> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("Desglose Costes Incidencia " + r_app.incidenciaConsultada);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoDesgloseValoracionNoConformidades.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setEditorEnabled(true);
		this.setSeleccion(SelectionMode.SINGLE);
    }

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	
    	this.mapeoOrig = new MapeoDesgloseValoracionNoConformidades();
		this.mapeo=((MapeoDesgloseValoracionNoConformidades) getEditedItemId());
    	
		this.mapeoOrig.setIdqc_incidencias(this.mapeo.getIdqc_incidencias());
		this.mapeoOrig.setArea(this.mapeo.getArea());
		this.mapeoOrig.setConcepto(this.mapeo.getConcepto());
		this.mapeoOrig.setCosteUnitario(this.mapeo.getCosteUnitario());
		this.mapeoOrig.setUnidades(this.mapeo.getUnidades());
		this.mapeoOrig.setTiempo(this.mapeo.getTiempo());
		this.mapeoOrig.setEtt(this.mapeo.getEtt());
    	
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("area", "concepto","costeUnitario", "unidades", "tiempo", "ett", "eliminar");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("area", "85");
    	this.widthFiltros.put("concepto", "185");
    	this.widthFiltros.put("ett", "85");
    	
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidthUndefined();
    	this.getColumn("area").setWidth(140);
    	this.getColumn("concepto").setHeaderCaption("Concepto");
    	this.getColumn("concepto").setWidth(220);
    	this.getColumn("costeUnitario").setHeaderCaption("Coste Hora");
    	this.getColumn("costeUnitario").setWidth(120);
    	this.getColumn("unidades").setHeaderCaption("Participacion");
    	this.getColumn("unidades").setWidth(120);
    	this.getColumn("tiempo").setHeaderCaption("Tiempo ");
    	this.getColumn("tiempo").setWidth(120);
    	this.getColumn("ett").setHeaderCaption("ETT");
    	this.getColumn("ett").setWidth(120);

    	this.getColumn("idqc_incidencias").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoNoConformidades) {
                MapeoDesgloseValoracionNoConformidades progRow = (MapeoDesgloseValoracionNoConformidades)bean;
                // The actual description text is depending on the application
//                if ("numerador".equals(cell.getPropertyId()))
//                {
//                	switch (progRow.getEstado())
//                	{
//                		case "C":
//                		{
//                			descriptionText = "Cerrada";
//                			break;
//                		}
//                	}
//                	
//                }
//                else if ("programacion".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Programación";
//                }
//                else if ("euro".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Tiempos Perdidos";
//                }                
//                else if ("imagen".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Adjunar Imagenes";
//                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "costeUnitario".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) || "tiempo".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	actualizar=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		mapeo = (MapeoDesgloseValoracionNoConformidades) getEditedItemId();
	        		
	        		consultaNoConformidadesServer cs = new consultaNoConformidadesServer(CurrentUser.get());
		        
	        		if (mapeo.getIdqc_incidencias()!=null)
		        	{
	        			if (mapeo.isEliminar())
	        			{
	        				cs.eliminarDesgloseValoracion(mapeo);
	        				((ArrayList<MapeoDesgloseValoracionNoConformidades>) vector).remove(mapeo);
	        				remove(mapeo);
			    			refreshAllRows();
	        			}
	        			else
	        			{
		        			mapeo.setArea(mapeoOrig.getArea());
	        				mapeo.setConcepto(mapeoOrig.getConcepto());
	        				mapeo.setEtt(mapeoOrig.getEtt());
			        		cs.guardarCambiosDesgloseValoracion(mapeo);
			        		
			        		((ArrayList<MapeoDesgloseValoracionNoConformidades>) vector).remove(mapeoOrig);
			    			((ArrayList<MapeoDesgloseValoracionNoConformidades>) vector).add(mapeo);
	
			    			refreshAllRows();
			    			scrollTo(mapeo);
			    			select(mapeo);
	        			}
		        	}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoDesgloseValoracionNoConformidades mapeo = (MapeoDesgloseValoracionNoConformidades) event.getItemId();
            	
//            	if (event.getPropertyId().toString().equals("programacion"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//            	}
//            	else if (event.getPropertyId().toString().equals("imagen"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaImagenesNoConformidades vt = null;
//        	    		vt= new pantallaImagenesNoConformidades(mapeo, "Imagenes No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//
//            	}            	
//            	else if (event.getPropertyId().toString().equals("euro"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaValoracionNoConformidades vt = null;
//        	    		vt= new pantallaValoracionNoConformidades(mapeo.getIdqc_incidencias());
//        	    		getUI().addWindow(vt);
//            		}
//            	}            	
//            	else
//            	{
//            		activadaVentanaPeticion=false;
//            		ordenando = false;
//	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("costeUnitario");
		this.camposNoFiltrar.add("unidades");
		this.camposNoFiltrar.add("tiempo");
	}

	public void generacionPdf(MapeoNoConformidades r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
}


