package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.DesgloseCostesGrid;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoDesgloseValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaDesgloseCostes extends Window
{
	
	private Button btnBotonCVentana = null;
	private Button btnBotonActualizar = null;
	private GridPropio gridPadre= null;
	private VerticalLayout principal=null;
	private boolean hayGridPadre = false;
	private boolean modificando = false;
	private VerticalLayout frameCentral = null;	

	private TextField txtArea = null;
	private TextField txtConcepto = null;
	private TextField txtCosteUnitario = null;
	private TextField txtUnidades = null;
	private TextField txtTiempo = null;
	private CheckBox chkEtt=null;
	
	private consultaNoConformidadesServer cncs  = null;
	private pantallaValoracionNoConformidades app = null;
	
	public Integer incidenciaConsultada = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaDesgloseCostes(pantallaValoracionNoConformidades r_app, String r_titulo, Integer r_incidencia)
	{
		this.app=r_app;
		this.setCaption(r_titulo);
		this.incidenciaConsultada=r_incidencia;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaNoConformidadesServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("750px");
		
		this.cargarPantalla();
		this.llenarRegistros();
		this.cargarListeners();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
				btnBotonActualizar = new Button("Insertar");
				btnBotonActualizar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			
			HorizontalLayout framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				app.calcularImporteIncidencia(false);
				close();
			}
		});

		btnBotonActualizar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				actualizar();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoDesgloseValoracionNoConformidades> r_vector=null;

    	r_vector=this.cncs.datosDesgloseValoracionNoConformidadesGlobal(incidenciaConsultada);
    	
    	this.presentarGrid(r_vector);
    	this.activarEntradasDatos(true);
	}

    private void presentarGrid(ArrayList<MapeoDesgloseValoracionNoConformidades> r_vector)
    {
    	
    	gridPadre = new DesgloseCostesGrid(this, r_vector);
    	
    	if (((DesgloseCostesGrid) gridPadre).vector==null) 
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		setHayGridPadre(true);
    	}
    	
    	if (isHayGridPadre())
    	{
    		this.gridPadre.setHeight("75%");
    		this.gridPadre.setWidth("100%");
    		
    		this.frameCentral.addComponent(gridPadre);
    		this.frameCentral.setExpandRatio(gridPadre,1 );
    		
    		/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			txtArea = new TextField("Area");
    			txtArea.setWidth("100px");
    			txtArea.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			txtConcepto= new TextField("Concepto");
    			txtConcepto.setWidth("120px");
    			txtConcepto.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			chkEtt = new CheckBox("Ett");
    			
    			
    			linea1.addComponent(this.txtArea);
    			linea1.addComponent(this.txtConcepto);
    			linea1.addComponent(this.chkEtt);
    			linea1.setComponentAlignment(this.chkEtt, Alignment.MIDDLE_LEFT);
    			
			HorizontalLayout linea2 = new HorizontalLayout();
				linea2.setSpacing(true);

				txtCosteUnitario= new TextField("Coste Unitario");
				txtCosteUnitario.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtUnidades = new TextField("Unidades");
				txtUnidades.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtTiempo = new TextField("Tiempo");
				txtTiempo.addStyleName(ValoTheme.TEXTFIELD_TINY);
				
				linea2.addComponent(this.txtCosteUnitario);
				linea2.addComponent(this.txtUnidades);
				linea2.addComponent(this.txtTiempo);
				linea2.addComponent(this.btnBotonActualizar);
				linea2.setComponentAlignment(this.btnBotonActualizar, Alignment.BOTTOM_LEFT);    			
				
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		
    		this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    		this.frameCentral.setMargin(true);
    	}
    }

    private void activarEntradasDatos(boolean r_activar)
    {
    	txtArea.setEnabled(r_activar);
    	txtConcepto.setEnabled(r_activar);
    	txtCosteUnitario.setEnabled(r_activar);
    	txtUnidades.setEnabled(r_activar);
    	txtTiempo.setEnabled(r_activar);
    	txtTiempo.setEnabled(r_activar);
    	chkEtt.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	public void rellenarEntradasDatos(Integer r_reg)
	{
		/*
		 * con los datos del switch y la boca pulsada 
		 * buscaremos ne la parte servidora los datos asociados
		 * 
		 */
//		consultaNoConformidadesServer cecs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
//		MapeoDesgloseValoracionNoConformidades mapeo = new MapeoDesgloseValoracionNoConformidades();
//		
//		mapeo = cecs.datosDesgloseValoracionNoConformidadesGlobal(r_reg);
//		
//		
//		if (mapeo!=null)
//		{
//			modificando=true;
//			txtRoseta.setValue(mapeo.getRoseta());
//			txtIP.setValue(mapeo.getIp());
//			txtIdentificador.setValue(mapeo.getIdentificador());
//			txtDescripcion.setValue(mapeo.getDescripcion());
//			txtObservaciones.setValue(mapeo.getObservaciones());
//			txtUsuario.setValue(mapeo.getUser());
//			txtPasswd.setValue(mapeo.getPass());
//			txtAnyDesk.setValue(mapeo.getDireccionPublica());
//	//		txtCuenta.setValue(mapeo.getCuenta());
//		}
//		else
//		{
//			modificando=false;
//			txtRoseta.setValue("");
//			txtIP.setValue("");
//			txtIdentificador.setValue("");
//			txtDescripcion.setValue("");
//			txtObservaciones.setValue("");
//			txtUsuario.setValue("");
//			txtPasswd.setValue("");
//			txtAnyDesk.setValue("");
////			txtCuenta.setValue(mapeo.getCuenta());
//			
//			Notificaciones.getInstance().mensajeInformativo("Boca " + boca.toString() + " del switch " + r_switch + ". No hay datos asociados");
//		}
		
	}
	
	private void actualizar()
	{
		String rdo = null;
		MapeoDesgloseValoracionNoConformidades mapeo = null;
		consultaNoConformidadesServer cecs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
		
		if (todoEnOrden())
		{
			mapeo = new MapeoDesgloseValoracionNoConformidades();
			mapeo.setIdqc_incidencias(this.incidenciaConsultada);
			mapeo.setArea(txtArea.getValue());
	    	mapeo.setConcepto(txtConcepto.getValue());
	    	mapeo.setCosteUnitario(new Double(txtCosteUnitario.getValue()));
	    	mapeo.setUnidades(new Double(txtUnidades.getValue()));
	    	mapeo.setTiempo(new Double(txtTiempo.getValue()));
	    	if (chkEtt.getValue()) mapeo.setEtt("0"); else mapeo.setEtt("1");
	    	
	    	rdo = cecs.guardarNuevoDesgloseValoracion(mapeo);
	    	this.gridPadre.refresh(mapeo,null);
	    	this.gridPadre.refreshAllRows();
	    	this.limpiarEntradasDatos();
		}
	}
	
	private void limpiarEntradasDatos()
	{
		txtArea.setValue("");
		txtConcepto.setValue("");
		txtCosteUnitario.setValue("");
		txtUnidades.setValue("");
		txtTiempo.setValue("");
		chkEtt.setValue(false);
	}
    private boolean todoEnOrden()
    {
    	if (this.txtArea.getValue()==null || this.txtArea.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el area");
    		return false;
    	}
    	if ((this.txtConcepto.getValue()==null || this.txtConcepto.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el concepto ");
    		return false;
    	}
    	if ((this.txtCosteUnitario.getValue()==null || this.txtCosteUnitario.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el coste unitario");
    		return false;
    	}
    	if ((this.txtUnidades.getValue()==null || this.txtUnidades.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar las unidades ");
    		return false;
    	}
    	if ((this.txtTiempo.getValue()==null || this.txtTiempo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tiempo");
    		return false;
    	}
    	return true;
    }
    
    private void cargarCombo()
    {
    }
}