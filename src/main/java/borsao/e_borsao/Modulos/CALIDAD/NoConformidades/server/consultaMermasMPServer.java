package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoMermasMP;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaMermasMPServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaMermasMPServer instance;
	
	public consultaMermasMPServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaMermasMPServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaMermasMPServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoMermasMP> datosMermasMPGlobal(MapeoMermasMP r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoMermasMP> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_lin_control_embotellado.articulo mmp_art, "); 
		cadenaSQL.append(" sum(prd_lin_control_embotellado.mermas) mmp_mer,");
		cadenaSQL.append(" e_articu.descrip_articulo ar_des ");
		cadenaSQL.append(" FROM prd_lin_control_embotellado ");
		cadenaSQL.append(" inner join prd_control_embotellado cab on cab.idprd_control = prd_lin_control_embotellado.idprd_control ");
		cadenaSQL.append(" left outer join e_articu on e_articu.articulo = prd_lin_control_embotellado.articulo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cab.serie = '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + r_mapeo.getSemana() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}

			
			
			cadenaSQL.append(" group by prd_lin_control_embotellado.articulo");
			cadenaSQL.append(" having sum(prd_lin_control_embotellado.mermas) <> 0");
			cadenaSQL.append(" order by fecha,erticulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoMermasMP>();
			
			while(rsOpcion.next())
			{
				MapeoMermasMP mapeoMermasMP = new MapeoMermasMP();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMermasMP.setArticulo(rsOpcion.getString("mmp_art"));
				mapeoMermasMP.setCantidad(rsOpcion.getInt("mpt_mer"));
				
				vector.add(mapeoMermasMP);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	

	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}

	@Override
	public String semaforos() {
		return null;
	}
}