package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoNoConformidades mapeo = null;
	 
	 public MapeoNoConformidades convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoNoConformidades();

		 this.mapeo.setBodega(r_hash.get("bodega"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setLinea(r_hash.get("linea"));
		 this.mapeo.setTercero(r_hash.get("tercero"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setNotificador(r_hash.get("notificador"));
		 this.mapeo.setDocumento(r_hash.get("documento"));
		 this.mapeo.setIncidencia(r_hash.get("incidencia"));
		 this.mapeo.setInvestigacion(r_hash.get("investigacion"));
		 this.mapeo.setFiveWhy(r_hash.get("porques"));
		 this.mapeo.setSeguimiento_ac(r_hash.get("seguimiento_acc"));
		 this.mapeo.setRevisado(r_hash.get("revisado"));
		 this.mapeo.setResolucion(r_hash.get("resolucion"));
		 this.mapeo.setCorrectiva(r_hash.get("correctiva"));
		 this.mapeo.setResponsable(r_hash.get("responsable"));
		 this.mapeo.setSeguimiento(r_hash.get("seguimiento"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFase(r_hash.get("fase"));
		 
		 this.mapeo.setFechaNotificacion(rtFecha.conversionDeString(r_hash.get("fechaNotificacion")));
		 this.mapeo.setDia(rtFecha.conversionDeString(r_hash.get("dia")));
		 this.mapeo.setFechaRevision(rtFecha.conversionDeString(r_hash.get("fechaRevision")));
		 this.mapeo.setFechaImplantacion(rtFecha.conversionDeString(r_hash.get("fechaImplantacion")));
		 this.mapeo.setFechaCierre(rtFecha.conversionDeString(r_hash.get("fechaCierre")));
		 
		 if (r_hash.get("numerador")!=null && r_hash.get("numerador").length()>0) this.mapeo.setNumerador(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numerador"))));
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("idParte")!=null && r_hash.get("idParte").length()>0) this.mapeo.setIdParte(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idParte")));
		 if (r_hash.get("idProgramacion")!=null && r_hash.get("idProgramacion").length()>0) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idProgramacion")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoNoConformidades convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoNoConformidades();
		 this.mapeo.setBodega(r_hash.get("bodega"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setLinea(r_hash.get("linea"));
		 this.mapeo.setTercero(r_hash.get("tercero"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setNotificador(r_hash.get("notificador"));
		 this.mapeo.setDocumento(r_hash.get("documento"));
		 this.mapeo.setIncidencia(r_hash.get("incidencia"));
		 this.mapeo.setInvestigacion(r_hash.get("investigacion"));
		 this.mapeo.setFiveWhy(r_hash.get("porques"));
		 this.mapeo.setSeguimiento_ac(r_hash.get("seguimiento_acc"));
		 this.mapeo.setRevisado(r_hash.get("revisado"));
		 this.mapeo.setResolucion(r_hash.get("resolucion"));
		 this.mapeo.setCorrectiva(r_hash.get("correctiva"));
		 this.mapeo.setResponsable(r_hash.get("responsable"));
		 this.mapeo.setSeguimiento(r_hash.get("seguimiento"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFase(r_hash.get("fase"));
		 
		 this.mapeo.setFechaNotificacion(rtFecha.conversionDeString(r_hash.get("fechaNotificacion")));
		 this.mapeo.setDia(rtFecha.conversionDeString(r_hash.get("dia")));
		 this.mapeo.setFechaRevision(rtFecha.conversionDeString(r_hash.get("fechaRevision")));
		 this.mapeo.setFechaImplantacion(rtFecha.conversionDeString(r_hash.get("fechaImplantacion")));
		 this.mapeo.setFechaCierre(rtFecha.conversionDeString(r_hash.get("fechaCierre")));
		 
		 if (r_hash.get("numerador")!=null) this.mapeo.setNumerador(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numerador"))));
		 if (r_hash.get("cantidad")!=null) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("idParte")!=null) this.mapeo.setIdParte(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idParte")));
		 if (r_hash.get("idProgramacion")!=null) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idProgramacion")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoNoConformidades r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("bodega", r_mapeo.getBodega());
		 this.hash.put("origen", r_mapeo.getOrigen());
		 this.hash.put("linea", r_mapeo.getLinea());
		 this.hash.put("fase", r_mapeo.getFase());
		 this.hash.put("tercero", this.mapeo.getTercero());
		 this.hash.put("articulo", this.mapeo.getArticulo());
		 this.hash.put("notificador", this.mapeo.getNotificador());
		 this.hash.put("documento", this.mapeo.getDocumento());
		 this.hash.put("incidencia", this.mapeo.getIncidencia());
		 this.hash.put("investigacion", this.mapeo.getInvestigacion());
		 this.hash.put("porques", this.mapeo.getFiveWhy());
		 this.hash.put("seguimiento_acc", this.mapeo.getSeguimiento_ac());
		 this.hash.put("revisado", this.mapeo.getRevisado());
		 this.hash.put("resolucion", this.mapeo.getResolucion());
		 this.hash.put("correctiva", this.mapeo.getCorrectiva());
		 this.hash.put("responsable", this.mapeo.getResponsable());
		 this.hash.put("seguimiento", this.mapeo.getSeguimiento());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 
		 if (r_mapeo.getFechaNotificacion()!=null)
		 {
			 this.hash.put("fechaNotificacion", r_mapeo.getFechaNotificacion().toString());
		 }
		 else
		 {
			 this.hash.put("fechaNotificacion", null);
		 }
		 if (r_mapeo.getDia()!=null)
		 {
			 this.hash.put("dia", r_mapeo.getDia().toString());
		 }
		 else
		 {
			 this.hash.put("dia", null);
		 }
		 if (r_mapeo.getFechaRevision()!=null)
		 {
			 this.hash.put("fechaRevision", r_mapeo.getFechaRevision().toString());
		 }
		 else
		 {
			 this.hash.put("fechaRevision", null);
		 }
		 if (r_mapeo.getFechaImplantacion()!=null)
		 {
			 this.hash.put("fechaImplantacion", r_mapeo.getFechaImplantacion().toString());
		 }
		 else
		 {
			 this.hash.put("fechaImplantacion", null);
		 }
		 if (r_mapeo.getFechaCierre()!=null)
		 {
			 this.hash.put("fechaCierre", r_mapeo.getFechaCierre().toString());
		 }
		 else
		 {
			 this.hash.put("fechaCierre", null);
		 }

		 this.hash.put("numerador", String.valueOf(this.mapeo.getNumerador()));
		 this.hash.put("cantidad", String.valueOf(this.mapeo.getCantidad()));
		 if (this.mapeo.getIdParte()!=null) this.hash.put("idParte", String.valueOf(this.mapeo.getIdParte()));
		 if (this.mapeo.getIdProgramacion()!=null) this.hash.put("idProgramacion", String.valueOf(this.mapeo.getIdProgramacion()));
		 
		 return hash;		 
	 }
}