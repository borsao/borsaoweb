package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasNoConformidades extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private String articuloSeleccionado = null;
	
	public pantallaLineasNoConformidades(String r_articulo, String r_titulo)
	{
		this.articuloSeleccionado=r_articulo;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1750px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	private void llenarRegistros(SelectionMode r_selecet)
	{
		Iterator iterator = null;
		MapeoNoConformidades mapeo = null;
		ArrayList<MapeoNoConformidades> vector = null;		
		consultaNoConformidadesServer cncs = new consultaNoConformidadesServer(CurrentUser.get());
		
		mapeo = new MapeoNoConformidades();
		mapeo.setArticulo(this.articuloSeleccionado);
		
		container = new IndexedContainer();
		container.addContainerProperty("estado", String.class, null);
		container.addContainerProperty("numerador", Integer.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String .class, null);
		container.addContainerProperty("incidencia", String .class, null);
		container.addContainerProperty("investigacion", String .class, null);
			
		vector = cncs.datosNoConformidadesGlobal(mapeo,null);

		iterator = vector.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoNoConformidades) iterator.next();
			
			file.getItemProperty("estado").setValue(mapeo.getEstado());
			file.getItemProperty("numerador").setValue(mapeo.getNumerador());
			file.getItemProperty("articulo").setValue(mapeo.getArticulo());
			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
    		file.getItemProperty("incidencia").setValue(mapeo.getIncidencia());
    		file.getItemProperty("investigacion").setValue(mapeo.getInvestigacion());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(r_selecet);

		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.asignarTooltips();
		
		this.gridDatos.getColumn("estado").setHidden(true);
		
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
			String strEstado = "";
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strEstado=cellReference.getValue().toString();
            	}
            	if ("articulo".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "C":
	        			{
	        				return "Rcell-normal";
	        			}
	        			case "R":
	        			{
	        				return "Rcell-darkgreen";
	        			}
	        			case "I":
	        			{
	        				return "Rcell-green";
	        			}
	        			case "P":
	        			{
	        				return "Rcell-warning";
	        			}
	        			case "N":
	        			{
	        				return "Rcell-error";
	        			}

	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

	private void asignarTooltips()
    {
		this.gridDatos.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
	private String getCellDescription(CellReference cell) 
	{
		
		String strEstado = "";
		String descriptionText = "";
			                
		Item file = cell.getItem();
				
				
		if ("articulo".equals(cell.getPropertyId()))
        {
			if (file.getItemProperty("estado")!=null && file.getItemProperty("estado").getValue()!=null)
			{
				strEstado = file.getItemProperty("estado").getValue().toString();
			}
			
        	switch (strEstado.trim())
        	{
	        	case "C":
	    		{
	    			descriptionText = "Cerrada";
	    			break;
	    		}
	    		case "R":
	    		{
	    			descriptionText = "Resuelta";
	    			break;
	    		}
	    		case "I":
	    		{
	    			descriptionText = "Implantacion";
	    			break;
	    		}
	    		case "P":
	    		{
	    			descriptionText = "Revisada";
	    			break;
	    		}
	    		case "N":
	    		{
	    			descriptionText = "Nueva";
	    			break;
	    		}
        	}
        }
		return descriptionText;
	}

}

