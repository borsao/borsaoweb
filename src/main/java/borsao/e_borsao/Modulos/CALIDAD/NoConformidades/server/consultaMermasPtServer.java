package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoMermasPt;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaMermasPtServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaMermasPtServer instance;
	
	public consultaMermasPtServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaMermasPtServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaMermasPtServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoMermasPt> datosMermasPtGlobal(MapeoMermasPt r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoMermasPt> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_mermas_pt.idCodigo mpt_id,");
		cadenaSQL.append(" prd_mermas_pt.ejercicio mpt_eje,");
		cadenaSQL.append(" prd_mermas_pt.fecha mpt_dia,");
		cadenaSQL.append(" prd_mermas_pt.semana mpt_sem,");
		cadenaSQL.append(" prd_mermas_pt.articulo mpt_art,");
		cadenaSQL.append(" prd_mermas_pt.cantidad mpt_can,");
		cadenaSQL.append(" prd_mermas_pt.observaciones mpt_obs, ");
		cadenaSQL.append(" prd_mermas_pt.tecnicoCalidad mpt_tec,");
		cadenaSQL.append(" prd_mermas_pt.tipo mpt_tip,");
		cadenaSQL.append(" e_articu.descrip_articulo ar_des ");
		cadenaSQL.append(" from prd_mermas_pt ");
		cadenaSQL.append(" left outer join e_articu on e_articu.articulo = prd_mermas_pt.articulo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getDia()) + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_mermas_pt.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_mermas_pt.ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_mermas_pt.semana = " + r_mapeo.getSemana());
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}

				if (r_mapeo.getTecnicoCalidad()!=null && r_mapeo.getTecnicoCalidad().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tecnicoCalidad = '" + r_mapeo.getTecnicoCalidad() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipo = '" + r_mapeo.getTipo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, fecha,prd_mermas_pt.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoMermasPt>();
			
			while(rsOpcion.next())
			{
				MapeoMermasPt mapeoMermasPt = new MapeoMermasPt();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMermasPt.setIdCodigo(rsOpcion.getInt("mpt_id"));
				mapeoMermasPt.setEjercicio(rsOpcion.getInt("mpt_eje"));
				mapeoMermasPt.setDia(RutinasFechas.conversion(rsOpcion.getDate("mpt_dia")));
				mapeoMermasPt.setSemana(rsOpcion.getInt("mpt_sem"));
				mapeoMermasPt.setArticulo(rsOpcion.getString("mpt_art"));
				mapeoMermasPt.setCantidad(rsOpcion.getInt("mpt_can"));
				mapeoMermasPt.setTecnicoCalidad(rsOpcion.getString("mpt_tec"));
				mapeoMermasPt.setTipo(rsOpcion.getString("mpt_tip"));
				mapeoMermasPt.setObservaciones(rsOpcion.getString("mpt_obs"));
				
				vector.add(mapeoMermasPt);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public MapeoMermasPt datosMermasPt(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		MapeoMermasPt mapeoMermasPt = null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_mermas_pt.idCodigo mpt_id,");
		cadenaSQL.append(" prd_mermas_pt.ejercicio mpt_eje,");
		cadenaSQL.append(" prd_mermas_pt.fecha mpt_dia,");
		cadenaSQL.append(" prd_mermas_pt.semana mpt_sem,");
		cadenaSQL.append(" prd_mermas_pt.articulo mpt_art,");
		cadenaSQL.append(" prd_mermas_pt.cantidad mpt_can,");
		cadenaSQL.append(" prd_mermas_pt.observaciones mpt_obs, ");
		cadenaSQL.append(" prd_mermas_pt.tecnicoCalidad mpt_tec,");
		cadenaSQL.append(" prd_mermas_pt.tipo mpt_tip,");
		cadenaSQL.append(" e_articu.descrip_articulo ar_des ");
		cadenaSQL.append(" from prd_mermas_pt ");
		cadenaSQL.append(" left outer join e_articu on e_articu.articulo = prd_mermas_pt.articulo ");
		
		
		try
		{
			cadenaWhere = new StringBuffer();
			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			
			if (r_id !=null && r_id > 0)
			{
				cadenaSQL.append(" where idCodigo = '" + r_id + "' ");
			}
			
			cadenaSQL.append(" order by fecha,erticulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoMermasPt = new MapeoMermasPt();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMermasPt.setIdCodigo(rsOpcion.getInt("mpt_id"));
				mapeoMermasPt.setEjercicio(rsOpcion.getInt("mpt_eje"));
				mapeoMermasPt.setDia(RutinasFechas.conversion(rsOpcion.getDate("mpt_dia")));
				mapeoMermasPt.setSemana(rsOpcion.getInt("mpt_sem"));
				mapeoMermasPt.setArticulo(rsOpcion.getString("mpt_art"));
				mapeoMermasPt.setCantidad(rsOpcion.getInt("mpt_can"));
				mapeoMermasPt.setTecnicoCalidad(rsOpcion.getString("mpt_tec"));
				mapeoMermasPt.setTipo(rsOpcion.getString("mpt_tip"));
				mapeoMermasPt.setObservaciones(rsOpcion.getString("mpt_obs"));

			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeoMermasPt;
	}
	
	public String imprimirNoConformidad(Integer r_id,String r_area)
	{
		return null;		
	}

	public String generarInformeTiempos(Date r_desde,Date r_hasta)
	{
		return null;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_mermas_pt.idCodigo) mpt_sig ");
     	cadenaSQL.append(" FROM prd_mermas_pt ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("mpt_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 1;
	}

	public Integer cargarAcumulado(Integer r_ejercicio, Integer r_semana, String r_articulo, String r_fecha, String r_tipo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoMermasPt> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT sum(prd_mermas_pt.cantidad) mpt_tot ");
		cadenaSQL.append(" from prd_mermas_pt ");
		
		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_ejercicio!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" ejercicio = " + r_ejercicio);
			}
			if (r_articulo!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" articulo = '" + r_articulo + "' ");
			}
			if (r_semana!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" semana = " + r_semana);
			}

			if (r_fecha!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" fecha = '" + RutinasFechas.convertirAFechaMysql(r_fecha) + "' ");
			}

			if (r_tipo!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" tipo = '" + r_tipo + "' ");
			}

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("mpt_tot");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 0;
	}

	public String guardarNuevo(MapeoMermasPt r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;

		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_mermas_pt( ");
			cadenaSQL.append(" prd_mermas_pt.idCodigo,");
			cadenaSQL.append(" prd_mermas_pt.ejercicio,");
			cadenaSQL.append(" prd_mermas_pt.fecha, ");
			cadenaSQL.append(" prd_mermas_pt.semana, ");
			cadenaSQL.append(" prd_mermas_pt.articulo, ");
			cadenaSQL.append(" prd_mermas_pt.cantidad, ");
			cadenaSQL.append(" prd_mermas_pt.tecnicoCalidad, ");
			cadenaSQL.append(" prd_mermas_pt.tipo, ");
			cadenaSQL.append(" prd_mermas_pt.observaciones ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    String dia = "";

		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    
		    if (r_mapeo.getDia()!=null)
		    {
		    	dia=RutinasFechas.convertirDateToString(r_mapeo.getDia());
		    }
		    else
		    {
		    	dia = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(dia));

		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    
		    if (r_mapeo.getTecnicoCalidad()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getTecnicoCalidad());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
	        preparedStatement.executeUpdate();
	        
	        return null;
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public String guardarCambios(MapeoMermasPt r_mapeo)
	{
		String rdo = null;
		Connection con = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_mermas_pt set ");
			cadenaSQL.append(" prd_mermas_pt.ejercicio =?,");
			cadenaSQL.append(" prd_mermas_pt.fecha =?,");
			cadenaSQL.append(" prd_mermas_pt.semana =?,");
			cadenaSQL.append(" prd_mermas_pt.articulo =?,");
			cadenaSQL.append(" prd_mermas_pt.cantidad =?,");
			cadenaSQL.append(" prd_mermas_pt.tecnicoCalidad =?,");
			cadenaSQL.append(" prd_mermas_pt.tipo =?,");
			cadenaSQL.append(" prd_mermas_pt.observaciones =? ");
			cadenaSQL.append(" where prd_mermas_pt.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    String dia ="";
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getDia()!=null)
		    {
		    	dia=RutinasFechas.convertirDateToString(r_mapeo.getDia());
		    }
		    else
		    {
		    	dia = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(2, RutinasFechas.convertirAFechaMysql(dia));
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    
		    if (r_mapeo.getTecnicoCalidad()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTecnicoCalidad());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		    return null;
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			rdo=ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}
	
	public void eliminar(MapeoMermasPt r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;
		
		try
		{
			con= this.conManager.establecerConexionInd();	          

			cadenaSQL.append(" DELETE FROM prd_mermas_pt ");            
			cadenaSQL.append(" WHERE prd_mermas_pt.idCodigo = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			
			preparedStatement.close();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}

	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Calidad");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos(null);
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";
			}
			else
			{
				permisos="0";
			}
		}
		return permisos;
	}

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;		

		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where articulo between '0100000' and '0109999' order by articulo asc" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
	
	public Integer mermasSemanalProduccion(String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsProduccion = null;
		Integer valor = null;
		Connection con = null;	
		Statement cs = null;
		String articulo = null;

		if (r_linea.toUpperCase().equals("ENVASADO"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADO"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
		
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT sum(prd_mermas_pt.cantidad) prd_mer ");
			sql.append(" FROM prd_mermas_pt ");
			sql.append(" WHERE semana ='" + r_semana + "' " );
			sql.append(" AND ejercicio = " + r_ejercicio);
			sql.append(" AND articulo like '" + articulo + "%' ");
			sql.append(" AND tipo = 'Proceso' ");
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			
			while(rsProduccion.next())
			{
				valor = rsProduccion.getInt("prd_mer");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return valor;
	}
}