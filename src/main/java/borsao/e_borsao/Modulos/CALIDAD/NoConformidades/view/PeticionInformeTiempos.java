package borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionInformeTiempos extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbPrinter = null;
	private DateField txtDesdeFecha = null;
	private DateField txtHastaFecha = null;
	
	
	private consultaNoConformidadesServer cncs = null;
	private InformeTiemposView origen = null;
	
	public PeticionInformeTiempos(InformeTiemposView r_view)
	{
		cncs = new consultaNoConformidadesServer(CurrentUser.get());
		this.origen=r_view;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtDesdeFecha= new DateField("DesdeFecha",new Date());
				this.txtDesdeFecha.setWidth("150px");
				this.txtDesdeFecha.setDateFormat("dd/MM/yyyy");
				this.txtDesdeFecha.setRequired(true);
				this.txtHastaFecha= new DateField("HastaFecha",new Date());
				this.txtHastaFecha.setWidth("150px");
				this.txtHastaFecha.setRequired(true);
				this.txtHastaFecha.setDateFormat("dd/MM/yyyy");
				
				fila2.addComponent(this.txtDesdeFecha);
				fila2.addComponent(this.txtHastaFecha);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);
				this.cmbPrinter.setWidth("200px");
				fila3.addComponent(cmbPrinter);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
			btnBotonCVentana = new Button("Cancelar");
			btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
			btnBotonAVentana = new Button("Aceptar");
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
			botonera.addComponent(btnBotonAVentana);
			botonera.addComponent(btnBotonCVentana);		
			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
			botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		
		this.setContent(principal);

		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();

		this.cargarListeners();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				String todoCorrecto = todoEnOrden();
				
				if (todoCorrecto.equals("OK"))
				{
					String pdfGenerado = cncs.generarInformeTiempos(txtDesdeFecha.getValue(), txtHastaFecha.getValue());
					
					if(pdfGenerado.length()>0) 
					{
						if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
						{
							RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
						}
						else
						{
							String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
							RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
						    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
						}
					}
			    	else
			    		Notificaciones.getInstance().mensajeError("Error en la generacion");
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia(todoCorrecto);
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private String todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		String devolver = "OK";
		if (this.txtDesdeFecha.getValue()==null) 
		{
			devolver = "Hay que rellenar la fecha inicio del listado";
			this.txtDesdeFecha.focus();
			return devolver;
		}
		else if (this.txtHastaFecha.getValue()==null) 
		{
			devolver = "Hay que rellenar la fecha fin del listado";
			this.txtHastaFecha.focus();
			return devolver;
		}
		else if (this.txtDesdeFecha.getValue().after(this.txtHastaFecha.getValue())) 
		{
			devolver ="La fecha inicial no puede ser posterior a la final";			
			txtDesdeFecha.focus();
			return devolver;
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		this.cmbPrinter.setValue(defecto);
	}

}