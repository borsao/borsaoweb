package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.modelo.MapeoControlImpulsos;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.server.consultaImpulsosServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaImpulsos extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlImpulsos mapeoControlImpulsos = null;
	private consultaImpulsosServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;
	private Label lblCumplimentacion = null;
	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtVacio= null;
	private TextField txtCarbonico= null;
	private TextField txtPurga= null;
	private TextField txtTemperaturaLlenado= null;;
	private TextField txtImpulsosLlenado= null;;
	private TextField txtCono= null;;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app = null;
	private static String tipoControl = "global";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaImpulsos(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaImpulsosServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
			HorizontalLayout linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new Combo("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setEnabled(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

			this.txtFecha = new EntradaDatosFecha();
			this.txtFecha.setCaption("Fecha");
			this.txtFecha.setEnabled(false);
			this.txtFecha.setValue(this.mapeoProduccion.getFecha());
			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtFecha.setWidth("150px");
			
			this.lblCumplimentacion = new Label();
			this.lblCumplimentacion.setValue("Frecuencia: Cuando se cambian los parámetros de la llenadora");
//			this.lblCumplimentacion.setWidth("100%");
//			this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
			this.lblCumplimentacion.addStyleName("lblTituloCalidad");
			
			linea1.addComponent(this.txtEjercicio);
			linea1.addComponent(this.cmbSemana);
			linea1.addComponent(this.txtFecha);
			linea1.addComponent(this.lblCumplimentacion);
			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
			
		HorizontalLayout linea2 = new HorizontalLayout();
			linea2.setSpacing(true);

			this.txtHoras = new TextField();
			this.txtHoras.setCaption("Hora");
			this.txtHoras.setWidth("120px");
			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtHoras.addStyleName("rightAligned");
			
			this.txtVacio= new TextField();
			this.txtVacio.setCaption("Tiempo de Vacio");
			this.txtVacio.setWidth("120px");
			this.txtVacio.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtVacio.addStyleName("rightAligned");
			
			this.txtCarbonico= new TextField();
			this.txtCarbonico.setCaption("Tiempo de Carbonico");
			this.txtCarbonico.setWidth("120px");
			this.txtCarbonico.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtCarbonico.addStyleName("rightAligned");
			
			this.txtPurga= new TextField();
			this.txtPurga.setCaption("Tiempo de Purga");
			this.txtPurga.setWidth("120px");
			this.txtPurga.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtPurga.addStyleName("rightAligned");
			
			this.txtTemperaturaLlenado= new TextField();
			this.txtTemperaturaLlenado.setCaption("Temperatura Llenado");
			this.txtTemperaturaLlenado.setWidth("120px");
			this.txtTemperaturaLlenado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtTemperaturaLlenado.addStyleName("rightAligned");
			
			this.txtImpulsosLlenado= new TextField();
			this.txtImpulsosLlenado.setCaption("Impulsos Llenado");
			this.txtImpulsosLlenado.setWidth("120px");
			this.txtImpulsosLlenado.setValue("3");
			this.txtImpulsosLlenado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtImpulsosLlenado.addStyleName("rightAligned");
			
			this.txtCono= new TextField();
			this.txtCono.setCaption("Medida del Cono");
			this.txtCono.setWidth("120px");
			this.txtCono.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtCono.addStyleName("rightAligned");

			linea2.addComponent(this.txtHoras);
//			linea2.addComponent(this.txtVacio);
//			linea2.addComponent(this.txtCarbonico);
//			linea2.addComponent(this.txtPurga);
//			linea2.addComponent(this.txtTemperaturaLlenado);
			linea2.addComponent(this.txtImpulsosLlenado);
//			linea2.addComponent(this.txtCono);

		HorizontalLayout linea4 = new HorizontalLayout();
			linea4.setSpacing(true);
			
			this.txtRealizado= new TextField();
			this.txtRealizado.setCaption("Realizado Por");
			this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtRealizado.setWidth("180px");
			
			this.txtObservaciones= new TextArea();
			this.txtObservaciones.setCaption("Observaciones");
			this.txtObservaciones.setWidth("300");
			
			btnLimpiar= new Button("Nuevo");
			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

			btnInsertar= new Button("Guardar");
			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

			btnEliminar= new Button("Eliminar");
			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
			btnEliminar.setEnabled(false);

			linea4.addComponent(this.txtRealizado);
			linea4.addComponent(this.txtObservaciones);
			linea4.addComponent(this.btnLimpiar);
			linea4.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnInsertar);
			linea4.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnEliminar);
			linea4.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
//    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlImpulsos mapeo = new MapeoControlImpulsos();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlImpulsos.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}			
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlImpulsos mapeo = new MapeoControlImpulsos();

			if (this.txtVacio.getValue()!=null && this.txtVacio.getValue().length()>0) mapeo.setVacio(new Double(this.txtVacio.getValue())); else mapeo.setVacio(new Double(0));
			if (this.txtCarbonico.getValue()!=null && this.txtCarbonico.getValue().length()>0) mapeo.setCarbonico(new Double(this.txtCarbonico.getValue())); else mapeo.setCarbonico(new Double(0));
			if (this.txtPurga.getValue()!=null && this.txtPurga.getValue().length()>0) mapeo.setPurga(new Double(this.txtPurga.getValue())); else mapeo.setPurga(new Double(0));
			if (this.txtTemperaturaLlenado.getValue()!=null && this.txtTemperaturaLlenado.getValue().length()>0) mapeo.setTempLlenado(new Double(this.txtTemperaturaLlenado.getValue())); else mapeo.setTempLlenado(new Double(0));
			if (this.txtImpulsosLlenado.getValue()!=null && this.txtImpulsosLlenado.getValue().length()>0) mapeo.setImpLlenado(new Double(this.txtImpulsosLlenado.getValue())); else mapeo.setImpLlenado(new Double(0));
			if (this.txtCono.getValue()!=null && this.txtCono.getValue().length()>0) mapeo.setCono(new Double(this.txtCono.getValue())); else mapeo.setCono(new Double(0));
			
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlImpulsos.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlImpulsos.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlImpulsos> vector=null;

    	MapeoControlImpulsos mapeoPreop = new MapeoControlImpulsos();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlImpulsos r_mapeo)
	{
		setCreacion(false);

		this.mapeoControlImpulsos = new MapeoControlImpulsos();
		
		this.mapeoControlImpulsos.setIdCodigo(r_mapeo.getIdCodigo());
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		
		if (r_mapeo.getVacio()!=null) this.txtVacio.setValue(r_mapeo.getVacio().toString());
		if (r_mapeo.getCarbonico()!=null) this.txtCarbonico.setValue(r_mapeo.getCarbonico().toString());
		if (r_mapeo.getPurga()!=null) this.txtPurga.setValue(r_mapeo.getPurga().toString());
		if (r_mapeo.getTempLlenado()!=null) this.txtTemperaturaLlenado.setValue(r_mapeo.getTempLlenado().toString());
		if (r_mapeo.getImpLlenado()!=null) this.txtImpulsosLlenado.setValue(r_mapeo.getImpLlenado().toString());
		if (r_mapeo.getCono()!=null) this.txtCono.setValue(r_mapeo.getCono().toString());
		
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
//    	txtVerificado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtVacio.setValue("");
		this.txtCarbonico.setValue("");
		this.txtPurga.setValue("");
		this.txtTemperaturaLlenado.setValue("");
		this.txtImpulsosLlenado.setValue("3");
		this.txtCono.setValue("");
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;

    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlImpulsos) r_fila)!=null)
    	{
    		MapeoControlImpulsos mapeoPreop = (MapeoControlImpulsos) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlImpulsos> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Impulsoss Realizados");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlImpulsos> container = new BeanItemContainer<MapeoControlImpulsos>(MapeoControlImpulsos.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));

			    	getColumn("vacio").setHeaderCaption("Tiempo Vacio");
			    	getColumn("vacio").setWidth(new Double(120));
			    	getColumn("carbonico").setHeaderCaption("Tiempo Carbonico");
			    	getColumn("carbonico").setWidth(new Double(120));
			    	getColumn("purga").setHeaderCaption("Tiempo Purga");
			    	getColumn("purga").setWidth(new Double(120));
			    	getColumn("tempLlenado").setHeaderCaption("Temperatura llenar");
			    	getColumn("tempLlenado").setWidth(new Double(120));
			    	getColumn("impLlenado").setHeaderCaption("Impulsos Llenado");
			    	getColumn("impLlenado").setWidth(new Double(120));
			    	getColumn("cono").setHeaderCaption("Impulsos Llenado");
			    	getColumn("cono").setWidth(new Double(120));
			    	
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() 
				{					
					setColumnOrder("fecha", "hora", "vacio", "carbonico", "purga","tempLlenado", "impLlenado", "cono", "realizado", "verificado", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}