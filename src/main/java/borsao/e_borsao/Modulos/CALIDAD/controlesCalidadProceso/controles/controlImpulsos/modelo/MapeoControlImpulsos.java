package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlImpulsos extends MapeoGlobal
{
	private Integer idProgramacion;
	private String hora;
	private String realizado;
	private String observaciones;
	private String linea;
	private String verificado;
	
	private Double vacio;
	private Double carbonico;
	private Double purga;
	private Double tempLlenado;
	private Double impLlenado;
	private Double cono;
	
	private Date fecha =null;
	
	public MapeoControlImpulsos()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public Double getVacio() {
		return vacio;
	}

	public void setVacio(Double vacio) {
		this.vacio = vacio;
	}

	public Double getCarbonico() {
		return carbonico;
	}

	public void setCarbonico(Double carbonico) {
		this.carbonico = carbonico;
	}

	public Double getPurga() {
		return purga;
	}

	public void setPurga(Double purga) {
		this.purga = purga;
	}

	public Double getTempLlenado() {
		return tempLlenado;
	}

	public void setTempLlenado(Double tempLlenado) {
		this.tempLlenado = tempLlenado;
	}

	public Double getImpLlenado() {
		return impLlenado;
	}

	public void setImpLlenado(Double impLlenado) {
		this.impLlenado = impLlenado;
	}

	public Double getCono() {
		return cono;
	}

	public void setCono(Double cono) {
		this.cono = cono;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
}