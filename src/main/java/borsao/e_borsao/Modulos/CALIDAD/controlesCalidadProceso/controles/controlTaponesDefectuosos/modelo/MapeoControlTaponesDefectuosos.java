package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlTaponesDefectuosos.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlTaponesDefectuosos extends MapeoGlobal
{
	private Integer idProgramacion;
	private Integer cantidad;
	private Integer cabezal;
	
	private String linea;
	private String hora;
	private String realizado;
	private String verificado;
	private String observaciones;
	
	private Date fecha;
	
	public MapeoControlTaponesDefectuosos()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getCabezal() {
		return cabezal;
	}

	public void setCabezal(Integer cabezal) {
		this.cabezal = cabezal;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}