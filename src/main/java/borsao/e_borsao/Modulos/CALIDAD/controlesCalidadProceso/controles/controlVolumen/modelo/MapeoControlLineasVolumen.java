package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlLineasVolumen extends MapeoGlobal
{
	private Integer idVolumen;
	private Double media;

	private String acc; // para poner el boton
	
	private String hora;
	private String articulo;
	private String muestra1;
	private String muestra2;
	private String muestra3;
	private String muestra4;
	private String muestra5;

	public MapeoControlLineasVolumen()
	{
	}

	public Integer getIdVolumen() {
		return idVolumen;
	}

	public void setIdVolumen(Integer idVolumen) {
		this.idVolumen = idVolumen;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getMuestra1() {
		return muestra1;
	}

	public void setMuestra1(String muestra1) {
		this.muestra1 = muestra1;
	}

	public String getMuestra2() {
		return muestra2;
	}

	public void setMuestra2(String muestra2) {
		this.muestra2 = muestra2;
	}

	public String getMuestra3() {
		return muestra3;
	}

	public void setMuestra3(String muestra3) {
		this.muestra3 = muestra3;
	}

	public String getMuestra4() {
		return muestra4;
	}

	public void setMuestra4(String muestra4) {
		this.muestra4 = muestra4;
	}

	public String getMuestra5() {
		return muestra5;
	}

	public void setMuestra5(String muestra5) {
		this.muestra5 = muestra5;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

}