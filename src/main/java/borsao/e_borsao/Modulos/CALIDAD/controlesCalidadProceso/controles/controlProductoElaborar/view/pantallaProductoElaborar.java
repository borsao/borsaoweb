package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.modelo.MapeoControlProductoElaborar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.server.consultaProductoElaborarServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaProductoElaborar extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlProductoElaborar mapeoControlProducto = null;
	private consultaProductoElaborarServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	private Label lblCumplimentacion = null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtArticulo= null;
	private TextField txtDescripcion= null;
	private TextField txtLote= null;
	private TextField txtTipoOperacion= null;
	
	private TextField txtEnvase= null;
	private TextField txtMarca= null;
	private TextField txtLoteMuestra= null;
	private TextField txtRealizadoMuestra= null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "referencia";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaProductoElaborar(PantallaControlesCalidadProceso  r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaProductoElaborarServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setValue(" ");
//				this.lblCumplimentacion.setWidth("100%");
//				this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");

    			this.txtArticulo= new TextField();
    			this.txtArticulo.setCaption("Articulo a elaborar");
    			this.txtArticulo.setWidth("120px");
    			this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

    			this.txtDescripcion= new TextField();
    			this.txtDescripcion.setCaption("Descsripcion Articulo");
    			this.txtDescripcion.setWidth("500px");
    			this.txtDescripcion.addStyleName(ValoTheme.TEXTFIELD_TINY);

    			linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.txtArticulo);
    			linea2.addComponent(this.txtDescripcion);
    			
			HorizontalLayout linea3 = new HorizontalLayout();
			linea3.setSpacing(true);
    			
				this.txtLote= new TextField();
				this.txtLote.setCaption("Lote de Fabricación");
				this.txtLote.setWidth("120px");
				this.txtLote.addStyleName(ValoTheme.TEXTFIELD_TINY);
				
    			this.txtTipoOperacion= new TextField();
    			this.txtTipoOperacion.setCaption("Tipo Operacion");
    			this.txtTipoOperacion.setWidth("120px");
    			this.txtTipoOperacion.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			linea3.addComponent(this.txtLote);
    			linea3.addComponent(this.txtTipoOperacion);
    			

    			this.txtEnvase= new TextField();
    			this.txtEnvase.setCaption("Envase");
    			this.txtEnvase.setWidth("120px");
    			this.txtEnvase.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			this.txtMarca= new TextField();
    			this.txtMarca.setCaption("Marca");
    			this.txtMarca.setWidth("120px");
    			this.txtMarca.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			this.txtLoteMuestra= new TextField();
    			this.txtLoteMuestra.setCaption("Lote Muestra");
    			this.txtLoteMuestra.setWidth("120px");
    			this.txtLoteMuestra.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			this.txtRealizadoMuestra= new TextField();
    			this.txtRealizadoMuestra.setCaption("Realizado Muestra");
    			this.txtRealizadoMuestra.setWidth("120px");
    			this.txtRealizadoMuestra.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			if (this.lineaProduccion.contentEquals("Envasadora"))
    			{
    				linea3.addComponent(this.txtEnvase);
    				linea3.addComponent(this.txtMarca);
    				linea3.addComponent(this.txtLoteMuestra);
    				linea3.addComponent(this.txtRealizadoMuestra);
    			}
    			
    		HorizontalLayout linea6 = new HorizontalLayout();
    			linea6.setSpacing(true);
    			
    			this.txtRealizado= new TextField();
    			this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");
				
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

    			
    			linea6.addComponent(this.txtRealizado);
    			linea6.addComponent(this.txtObservaciones);
    			linea6.addComponent(this.btnLimpiar);
    			linea6.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea6.addComponent(this.btnInsertar);
    			linea6.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea6.addComponent(this.btnEliminar);
    			linea6.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea6);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		this.setCreacion(true);
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlProductoElaborar mapeo = new MapeoControlProductoElaborar();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlProducto.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}

	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlProductoElaborar mapeo = new MapeoControlProductoElaborar();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setArticulo(this.txtArticulo.getValue());
			mapeo.setDescripcion(this.txtDescripcion.getValue());
			mapeo.setLote(this.txtLote.getValue());

			mapeo.setEnvase(this.txtEnvase.getValue());
			mapeo.setMarca(this.txtMarca.getValue());
			mapeo.setLoteMuestra(this.txtLoteMuestra.getValue());
			mapeo.setRealizadoMuestra(this.txtRealizadoMuestra.getValue());
			
			mapeo.setTipoOperacion(this.txtTipoOperacion.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoProductoElaborar(mapeo);
				if (rdo==null)
				{
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlProducto.getIdCodigo());
				rdo = cncs.guardarCambiosProductoElaborar(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlProductoElaborar> vector=null;

    	MapeoControlProductoElaborar mapeoPreop = new MapeoControlProductoElaborar();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(),this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlProductoElaborar r_mapeo)
	{
		this.mapeoControlProducto = new MapeoControlProductoElaborar();
		this.mapeoControlProducto.setIdCodigo(r_mapeo.getIdCodigo());
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtArticulo.setValue(r_mapeo.getArticulo());
		this.txtDescripcion.setValue(r_mapeo.getDescripcion());
		this.txtLote.setValue(r_mapeo.getLote());
		this.txtEnvase.setValue(r_mapeo.getEnvase());
		this.txtMarca.setValue(r_mapeo.getMarca());
		this.txtLoteMuestra.setValue(r_mapeo.getLoteMuestra());
		this.txtRealizadoMuestra.setValue(r_mapeo.getRealizadoMuestra());
		this.txtTipoOperacion.setValue(r_mapeo.getTipoOperacion());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	txtArticulo.setEnabled(r_activar);
    	txtDescripcion.setEnabled(r_activar);
    	txtLote.setEnabled(r_activar);
    	txtEnvase.setEnabled(r_activar);
    	txtMarca.setEnabled(r_activar);
    	txtRealizadoMuestra.setEnabled(r_activar);
    	txtLoteMuestra.setEnabled(r_activar);
    	txtTipoOperacion.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.txtHoras.setValue("");
		this.txtArticulo.setValue("");
		this.txtDescripcion.setValue("");
		this.txtLote.setValue("");
		this.txtEnvase.setValue("");
		this.txtMarca.setValue("");
		this.txtRealizadoMuestra.setValue("");
		this.txtLoteMuestra.setValue("");
		this.txtTipoOperacion.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
		
		if (isCreacion())
		{
			MapeoControlProductoElaborar mapeo = new MapeoControlProductoElaborar();
			if (this.lineaProduccion.contentEquals("Embotelladora"))
			{
				consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
				MapeoProgramacion mapeoProgramacion = cps.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());

				mapeo.setArticulo(mapeoProgramacion.getArticulo());
				mapeo.setDescripcion(mapeoProgramacion.getDescripcion());
				mapeo.setTipoOperacion(mapeoProgramacion.getTipo());
				mapeo.setLote(mapeoProgramacion.getLote());
			}
			else 
			{
				consultaProgramacionEnvasadoraServer cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
				MapeoProgramacionEnvasadora mapeoEnvasadora = cpes.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());						

				mapeo.setArticulo(mapeoEnvasadora.getArticulo());
				mapeo.setDescripcion(mapeoEnvasadora.getDescripcion());
				mapeo.setTipoOperacion(mapeoEnvasadora.getTipo());
				mapeo.setLote(mapeoEnvasadora.getLote());
			}

			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);
			mapeo.setHora("");
			mapeo.setRealizado("");
			mapeo.setVerificado("");
			mapeo.setObservaciones("");
			mapeo.setEnvase("");
			mapeo.setLoteMuestra("");
			mapeo.setMarca("");
			mapeo.setRealizadoMuestra("");
			
	
			this.llenarEntradasDatos(mapeo);
		}
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	if ((this.txtArticulo.getValue()==null || this.txtArticulo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el artículo a elaborar.");
    		return false;
    	}
    	if ((this.txtLote.getValue()==null || this.txtLote.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el lote de fabricación");
    		return false;
    	}
    	if ((this.txtTipoOperacion.getValue()==null || this.txtTipoOperacion.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tipo de operacion realizada");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlProductoElaborar) r_fila)!=null)
    	{
    		MapeoControlProductoElaborar mapeoGrifo = (MapeoControlProductoElaborar) r_fila;
    		if (mapeoGrifo!=null) this.llenarEntradasDatos(mapeoGrifo);
    	}    			
    	else 
    	{
    		this.setCreacion(true);
    		this.limpiarCampos();
    	}
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlProductoElaborar> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Producto Elaborar ");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlProductoElaborar> container = new BeanItemContainer<MapeoControlProductoElaborar>(MapeoControlProductoElaborar.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));
			    	getColumn("articulo").setHeaderCaption("Articulo");
			    	getColumn("articulo").setWidth(new Double(150));
			    	getColumn("descripcion").setHeaderCaption("Descripcion");
			    	getColumn("descripcion").setWidth(new Double(420));
			    	getColumn("lote").setHeaderCaption("Lote");
			    	getColumn("lote").setWidth(new Double(150));
			    	getColumn("tipoOperacion").setHeaderCaption("Operacion");
			    	getColumn("tipoOperacion").setWidth(new Double(150));
			    	
			    	if (lineaProduccion.contentEquals("Envasadora"))
			    	{
			    		getColumn("envase").setHeaderCaption("Envase");
			    		getColumn("envase").setWidth(new Double(150));
			    		getColumn("marca").setHeaderCaption("Marca");
			    		getColumn("marca").setWidth(new Double(150));
			    		getColumn("loteMuestra").setHeaderCaption("LoteMuestra");
			    		getColumn("loteMuestra").setWidth(new Double(150));
			    		getColumn("realizadoMuestra").setHeaderCaption("RealizadoMuestra");
			    		getColumn("realizadoMuestra").setWidth(new Double(155));
			    	}
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					if (lineaProduccion.contentEquals("Envasadora"))
			    	{
						setColumnOrder("fecha", "hora", "articulo","descripcion", "lote", "tipoOperacion","envase","marca", "loteMuestra", "realizadoMuestra", "realizado", "verificado", "observaciones");
			    	}
					else
					{
						setColumnOrder("fecha", "hora", "articulo","descripcion", "lote", "tipoOperacion", "realizado", "verificado", "observaciones");
					}
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
			panelGrid.setContent(gridDatos);
			this.setCreacion(false);
		}
		else
		{
			this.setCreacion(true);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}