package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.modelo.MapeoControlProductoElaborar;

public class consultaProductoElaborarServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaProductoElaborarServer  instance;
	
	public consultaProductoElaborarServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProductoElaborarServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProductoElaborarServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlProductoElaborar datosOpcionesGlobal(MapeoControlProductoElaborar r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlProductoElaborar mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_producto.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_producto.idCodigo pro_id, ");
			cadenaSQL.append(" qc_producto.hora pro_hora, ");
			cadenaSQL.append(" qc_producto.articulo pro_art, ");
			cadenaSQL.append(" qc_producto.descripcion pro_des, ");
			cadenaSQL.append(" qc_producto.lote pro_lot, ");
			if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
				cadenaSQL.append(" qc_producto.envase pro_env, ");
				cadenaSQL.append(" qc_producto.marca pro_mar, ");
				cadenaSQL.append(" qc_producto.loteMuestra pro_lotM, ");
				cadenaSQL.append(" qc_producto.realizadoMuestra pro_reaM, ");
			}			
			cadenaSQL.append(" qc_producto.tipoOperacion pro_tip, ");
			cadenaSQL.append(" qc_producto.realizado pro_rea, ");
			cadenaSQL.append(" qc_producto.verificado pro_ver, ");
			cadenaSQL.append(" qc_producto.fecha pro_fec, ");
			cadenaSQL.append(" qc_producto.observaciones pro_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_producto ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_producto.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlProductoElaborar();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pro_id"));
					mapeo.setArticulo(rsOpcion.getString("pro_art"));
					mapeo.setDescripcion(rsOpcion.getString("pro_des"));
					mapeo.setLote(rsOpcion.getString("pro_lot"));
					if (r_mapeo.getLinea().contentEquals("Envasadora"))
					{
						mapeo.setEnvase(rsOpcion.getString("pro_env"));
						mapeo.setMarca(rsOpcion.getString("pro_mar"));
						mapeo.setLoteMuestra(rsOpcion.getString("pro_lotM"));
						mapeo.setRealizadoMuestra(rsOpcion.getString("pro_reaM"));
					}
					mapeo.setTipoOperacion(rsOpcion.getString("pro_tip"));
					mapeo.setRealizado(rsOpcion.getString("pro_rea"));
					mapeo.setVerificado(rsOpcion.getString("pro_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pro_obs"));
					mapeo.setFecha(rsOpcion.getDate("pro_fec"));
					mapeo.setHora(rsOpcion.getString("pro_hora"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return mapeo;
	}
	
	public ArrayList<MapeoControlProductoElaborar> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlProductoElaborar> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlProductoElaborar mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_producto.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_producto.idCodigo pro_id, ");
			cadenaSQL.append(" qc_producto.hora pro_hora, ");
			cadenaSQL.append(" qc_producto.articulo pro_art, ");
			cadenaSQL.append(" qc_producto.descripcion pro_des, ");
			cadenaSQL.append(" qc_producto.lote pro_lot, ");
			if (r_linea.contentEquals("Envasadora"))
			{
				cadenaSQL.append(" qc_producto.envase pro_env, ");
				cadenaSQL.append(" qc_producto.marca pro_mar, ");
				cadenaSQL.append(" qc_producto.loteMuestra pro_lotM, ");
				cadenaSQL.append(" qc_producto.realizadoMuestra pro_reaM, ");
			}			
			cadenaSQL.append(" qc_producto.tipoOperacion pro_tip, ");
			cadenaSQL.append(" qc_producto.realizado pro_rea, ");
			cadenaSQL.append(" qc_producto.verificado pro_ver, ");
			cadenaSQL.append(" qc_producto.fecha pro_fec, ");
			cadenaSQL.append(" qc_producto.observaciones pro_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_producto ");
			
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlProductoElaborar>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlProductoElaborar();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pro_id"));
					mapeo.setArticulo(rsOpcion.getString("pro_art"));
					mapeo.setDescripcion(rsOpcion.getString("pro_des"));
					mapeo.setLote(rsOpcion.getString("pro_lot"));
					if (r_linea.contentEquals("Envasadora"))
					{
						mapeo.setEnvase(rsOpcion.getString("pro_env"));
						mapeo.setMarca(rsOpcion.getString("pro_mar"));
						mapeo.setLoteMuestra(rsOpcion.getString("pro_lotM"));
						mapeo.setRealizadoMuestra(rsOpcion.getString("pro_reaM"));
					}
					mapeo.setTipoOperacion(rsOpcion.getString("pro_tip"));
					mapeo.setRealizado(rsOpcion.getString("pro_rea"));
					mapeo.setVerificado(rsOpcion.getString("pro_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pro_obs"));
					mapeo.setFecha(rsOpcion.getDate("pro_fec"));
					mapeo.setHora(rsOpcion.getString("pro_hora"));
					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	
	public String guardarNuevoProductoElaborar(MapeoControlProductoElaborar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
		}

		try
		{
			if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".articulo , ");
				cadenaSQL.append(tabla + ".descripcion , ");
				cadenaSQL.append(tabla + ".lote , ");
				cadenaSQL.append(tabla + ".envase , ");
				cadenaSQL.append(tabla + ".marca, ");
				cadenaSQL.append(tabla + ".loteMuestra , ");
				cadenaSQL.append(tabla + ".realizadoMuestra , ");
				cadenaSQL.append(tabla + ".tipoOperacion , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			}
			else
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".articulo , ");
				cadenaSQL.append(tabla + ".descripcion , ");
				cadenaSQL.append(tabla + ".lote , ");
				cadenaSQL.append(tabla + ".tipoOperacion , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");
			}		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    
		    if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
		    	if (r_mapeo.getEnvase()!=null)
		    	{
		    		preparedStatement.setString(6, r_mapeo.getEnvase());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(6, null);
		    	}
		    	if (r_mapeo.getMarca()!=null)
		    	{
		    		preparedStatement.setString(7, r_mapeo.getMarca());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(7, null);
		    	}
		    	if (r_mapeo.getLoteMuestra()!=null)
		    	{
		    		preparedStatement.setString(8, r_mapeo.getLoteMuestra());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(8, null);
		    	}
		    	if (r_mapeo.getRealizadoMuestra()!=null)
		    	{
		    		preparedStatement.setString(9, r_mapeo.getRealizadoMuestra());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(9, null);
		    	}
		    	if (r_mapeo.getTipoOperacion()!=null)
		    	{
		    		preparedStatement.setString(10, r_mapeo.getTipoOperacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(10, null);
		    	}
		    	if (r_mapeo.getRealizado()!=null)
		    	{
		    		preparedStatement.setString(11, r_mapeo.getRealizado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(11, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
		    	{
		    		preparedStatement.setString(12, r_mapeo.getVerificado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(12, null);
		    	}
		    	if (r_mapeo.getObservaciones()!=null)
		    	{
		    		preparedStatement.setString(13, r_mapeo.getObservaciones());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(13, null);
		    	}
		    	if (r_mapeo.getIdProgramacion()!=null)
		    	{
		    		preparedStatement.setInt(14, r_mapeo.getIdProgramacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setInt(14, 0);
		    	}
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(15, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(15, null);
		    	}

			}
		    else
		    {
		    	
		    	if (r_mapeo.getTipoOperacion()!=null)
		    	{
		    		preparedStatement.setString(6, r_mapeo.getTipoOperacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(6, null);
		    	}
		    	if (r_mapeo.getRealizado()!=null)
		    	{
		    		preparedStatement.setString(7, r_mapeo.getRealizado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(7, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
		    	{
		    		preparedStatement.setString(8, r_mapeo.getVerificado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(8, null);
		    	}
		    	if (r_mapeo.getObservaciones()!=null)
		    	{
		    		preparedStatement.setString(9, r_mapeo.getObservaciones());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(9, null);
		    	}
		    	if (r_mapeo.getIdProgramacion()!=null)
		    	{
		    		preparedStatement.setInt(10, r_mapeo.getIdProgramacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setInt(10, 0);
		    	}
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(11, null);
		    	}
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	
	public String guardarCambiosProductoElaborar(MapeoControlProductoElaborar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
		}
		try
		{
			if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".articulo=?, ");
				cadenaSQL.append(tabla + ".descripcion=?, ");
				cadenaSQL.append(tabla + ".lote=?, ");
				cadenaSQL.append(tabla + ".envase=?, ");
				cadenaSQL.append(tabla + ".marca=?, ");
				cadenaSQL.append(tabla + ".loteMuestra=?, ");
				cadenaSQL.append(tabla + ".realizadoMuestra=?, ");
				cadenaSQL.append(tabla + ".tipoOperacion=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
			else
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".articulo=?, ");
				cadenaSQL.append(tabla + ".descripcion=?, ");
				cadenaSQL.append(tabla + ".lote=?, ");
				cadenaSQL.append(tabla + ".tipoOperacion=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    
		    if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
		    	if (r_mapeo.getEnvase()!=null)
		    	{
		    		preparedStatement.setString(5, r_mapeo.getEnvase());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(5, null);
		    	}
		    	if (r_mapeo.getMarca()!=null)
		    	{
		    		preparedStatement.setString(6, r_mapeo.getMarca());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(6, null);
		    	}
		    	if (r_mapeo.getLoteMuestra()!=null)
		    	{
		    		preparedStatement.setString(7, r_mapeo.getLoteMuestra());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(7, null);
		    	}
		    	if (r_mapeo.getRealizadoMuestra()!=null)
		    	{
		    		preparedStatement.setString(8, r_mapeo.getRealizadoMuestra());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(8, null);
		    	}
		    	if (r_mapeo.getTipoOperacion()!=null)
		    	{
		    		preparedStatement.setString(9, r_mapeo.getTipoOperacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(9, null);
		    	}
		    	if (r_mapeo.getRealizado()!=null)
		    	{
		    		preparedStatement.setString(10, r_mapeo.getRealizado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(10, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
		    	{
		    		preparedStatement.setString(11, r_mapeo.getVerificado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(11, null);
		    	}
		    	if (r_mapeo.getObservaciones()!=null)
		    	{
		    		preparedStatement.setString(12, r_mapeo.getObservaciones());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(12, null);
		    	}
		    	if (r_mapeo.getIdProgramacion()!=null)
		    	{
		    		preparedStatement.setInt(13, r_mapeo.getIdProgramacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setInt(13, 0);
		    	}
		    	
		    	preparedStatement.setInt(14, r_mapeo.getIdCodigo());

			}
		    else
		    {
		    	
		    	if (r_mapeo.getTipoOperacion()!=null)
		    	{
		    		preparedStatement.setString(5, r_mapeo.getTipoOperacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(5, null);
		    	}
		    	if (r_mapeo.getRealizado()!=null)
		    	{
		    		preparedStatement.setString(6, r_mapeo.getRealizado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(6, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
		    	{
		    		preparedStatement.setString(7, r_mapeo.getVerificado());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(7, null);
		    	}
		    	if (r_mapeo.getObservaciones()!=null)
		    	{
		    		preparedStatement.setString(8, r_mapeo.getObservaciones());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(8, null);
		    	}
		    	if (r_mapeo.getIdProgramacion()!=null)
		    	{
		    		preparedStatement.setInt(9, r_mapeo.getIdProgramacion());
		    	}
		    	else
		    	{
		    		preparedStatement.setInt(9, 0);
		    	}
		    	
		    	preparedStatement.setInt(10, r_mapeo.getIdCodigo());
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlProductoElaborar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_producto.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_producto.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_producto.idCodigo pro_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_producto ");
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );

	     	con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pro_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pro_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_producto_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_producto_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_producto_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pro_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			cadenaSQL.append(" and qc_preop.verificado <> '' " );
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
}