package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlPCC2 extends MapeoGlobal
{
	private Integer idProgramacion;
	private String hora;
	private String manometro;
	private String controlRoturas;
	private String controlGrifos;
	private String resultado;
	private String validado;
	private String realizado;
	private String observaciones;
	private String linea;
	private String verificado;
	private Date fecha =null;
	
	private String cuello;
	private String filtro;
	private String grifo;

	
	public MapeoControlPCC2()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getManometro() {
		return manometro;
	}

	public void setManometro(String manometro) {
		this.manometro = manometro;
	}

	public String getControlRoturas() {
		return controlRoturas;
	}

	public void setControlRoturas(String controlRoturas) {
		this.controlRoturas = controlRoturas;
	}

	public String getControlGrifos() {
		return controlGrifos;
	}

	public void setControlGrifos(String controlGrifos) {
		this.controlGrifos = controlGrifos;
	}

	public String getValidado() {
		return validado;
	}

	public void setValidado(String validado) {
		this.validado = validado;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getCuello() {
		return cuello;
	}

	public void setCuello(String cuello) {
		this.cuello = cuello;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	public String getGrifo() {
		return grifo;
	}

	public void setGrifo(String grifo) {
		this.grifo = grifo;
	}
	
}