package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.modelo.MapeoControlLineasVolumenEf;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.modelo.MapeoControlVolumenEf;

public class consultaVolumenEfServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaVolumenEfServer  instance;
	
	public consultaVolumenEfServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaVolumenEfServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaVolumenEfServer (r_usuario);			
		}
		return instance;
	}

	public ArrayList<MapeoControlVolumenEf> datosOpcionesGlobal(MapeoControlVolumenEf r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoControlVolumenEf> vector = null;
		MapeoControlVolumenEf mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_volumen.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_volumen.idCodigo vol_id, ");
			cadenaSQL.append(" qc_volumen.linea vol_lin, ");
			cadenaSQL.append(" qc_volumen.lote vol_lot, ");
			cadenaSQL.append(" qc_volumen.capacidad vol_cap, ");
			cadenaSQL.append(" qc_volumen.media vol_med, ");
			cadenaSQL.append(" qc_volumen.realizado vol_rea, ");
			cadenaSQL.append(" qc_volumen.verificado vol_ver, ");
			cadenaSQL.append(" qc_volumen.fecha vol_fec, ");
			cadenaSQL.append(" qc_volumen.idProgramacion vol_idp, ");
			cadenaSQL.append(" qc_volumen.observaciones vol_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_volumen ");
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" qc_volumen.lote ='" + r_mapeo.getLote() + "' ");
					}
					if (r_mapeo.getCapacidad()!=null && r_mapeo.getCapacidad()!=0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" qc_volumen.capacidad ='" + r_mapeo.getCapacidad() + "' ");
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_volumen.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				vector = new ArrayList<MapeoControlVolumenEf>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlVolumenEf();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("vol_id"));
					mapeo.setLinea(r_mapeo.getLinea());
					mapeo.setMedia(rsOpcion.getDouble("vol_med"));
					mapeo.setLote(rsOpcion.getString("vol_lot"));
					mapeo.setCapacidad(rsOpcion.getDouble("vol_cap"));
					mapeo.setRealizado(rsOpcion.getString("vol_rea"));
					mapeo.setVerificado(rsOpcion.getString("vol_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vol_obs"));
					mapeo.setFecha(rsOpcion.getDate("vol_fec"));
					mapeo.setIdProgramacion(rsOpcion.getInt("vol_idp"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	public ArrayList<MapeoControlVolumenEf> datosOpcionesGlobal(String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlVolumenEf> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlVolumenEf mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_volumen.idCodigo vol_id, ");
			cadenaSQL.append(" qc_volumen.linea vol_lin, ");
			cadenaSQL.append(" qc_volumen.lote vol_lot, ");
			cadenaSQL.append(" qc_volumen.capacidad vol_cap, ");
			cadenaSQL.append(" qc_volumen.media vol_med, ");
			cadenaSQL.append(" qc_volumen.realizado vol_rea, ");
			cadenaSQL.append(" qc_volumen.verificado vol_ver, ");
			cadenaSQL.append(" qc_volumen.fecha vol_fec, ");
			cadenaSQL.append(" qc_volumen.observaciones vol_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_volumen ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlVolumenEf>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlVolumenEf();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("vol_id"));
					mapeo.setLinea(r_linea);
					mapeo.setMedia(rsOpcion.getDouble("vol_med"));
					mapeo.setLote(rsOpcion.getString("vol_lot"));
					mapeo.setCapacidad(rsOpcion.getDouble("vol_cap"));
					mapeo.setRealizado(rsOpcion.getString("vol_rea"));
					mapeo.setVerificado(rsOpcion.getString("vol_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vol_obs"));
					mapeo.setFecha(rsOpcion.getDate("vol_fec"));					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}

	public ArrayList<MapeoControlLineasVolumenEf> datosOpcionesGlobalLineas(Integer r_id, MapeoControlVolumenEf r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlLineasVolumenEf> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLineasVolumenEf mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			tablaLineas = "qc_lineas_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasVolumenEf.idCodigo lvol_id, ");
			cadenaSQL.append(" qc_lineasVolumenEf.hora lvol_hor, ");
			cadenaSQL.append(" qc_lineasVolumenEf.articulo lvol_art, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra1 lvol_m1, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra2 lvol_m2, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra3 lvol_m3, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra4 lvol_m4, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra5 lvol_m5, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra6 lvol_m6, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra7 lvol_m7, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra8 lvol_m8, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra9 lvol_m9, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra10 lvol_m10, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra11 lvol_m11, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra12 lvol_m12, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra13 lvol_m13, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra14 lvol_m14, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra15 lvol_m15, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra16 lvol_m16, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra17 lvol_m17, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra18 lvol_m18, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra19 lvol_m19, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra20 lvol_m20, ");
			cadenaSQL.append(" qc_lineasVolumenEf.media lvol_med, ");
			cadenaSQL.append(" qc_lineasVolumenEf.idVolumenEf lvol_vol ");
			
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasVolumenEf ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_lineasVolumenEf.idVolumenEf  = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLineasVolumenEf>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLineasVolumenEf();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lvol_id"));
					mapeo.setHora(rsOpcion.getString("lvol_hor"));
					mapeo.setArticulo(rsOpcion.getString("lvol_art"));
					mapeo.setMuestra1(rsOpcion.getString("lvol_m1"));
					mapeo.setMuestra2(rsOpcion.getString("lvol_m2"));
					mapeo.setMuestra3(rsOpcion.getString("lvol_m3"));
					mapeo.setMuestra4(rsOpcion.getString("lvol_m4"));
					mapeo.setMuestra5(rsOpcion.getString("lvol_m5"));
					mapeo.setMuestra6(rsOpcion.getString("lvol_m6"));
					mapeo.setMuestra7(rsOpcion.getString("lvol_m7"));
					mapeo.setMuestra8(rsOpcion.getString("lvol_m8"));
					mapeo.setMuestra9(rsOpcion.getString("lvol_m9"));
					mapeo.setMuestra10(rsOpcion.getString("lvol_m10"));
					mapeo.setMuestra11(rsOpcion.getString("lvol_m11"));
					mapeo.setMuestra12(rsOpcion.getString("lvol_m12"));
					mapeo.setMuestra13(rsOpcion.getString("lvol_m13"));
					mapeo.setMuestra14(rsOpcion.getString("lvol_m14"));
					mapeo.setMuestra15(rsOpcion.getString("lvol_m15"));
					mapeo.setMuestra16(rsOpcion.getString("lvol_m16"));
					mapeo.setMuestra17(rsOpcion.getString("lvol_m17"));
					mapeo.setMuestra18(rsOpcion.getString("lvol_m18"));
					mapeo.setMuestra19(rsOpcion.getString("lvol_m19"));
					mapeo.setMuestra20(rsOpcion.getString("lvol_m20"));
					mapeo.setMedia(rsOpcion.getDouble("lvol_med"));
					mapeo.setIdVolumen(rsOpcion.getInt("lvol_vol"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return vector;
	}

	public Integer muestrasLineasErroneas(Integer r_id, Double r_neto, String r_linea)
	{
		ArrayList<MapeoControlLineasVolumenEf> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLineasVolumenEf mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		Integer cuantasErroneas =0;
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			tablaLineas = "qc_lineas_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");
			
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
			
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasVolumenEf.idCodigo lvol_id, ");
			cadenaSQL.append(" qc_lineasVolumenEf.hora lvol_hor, ");
			cadenaSQL.append(" qc_lineasVolumenEf.articulo lvol_art, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra1 lvol_m1, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra2 lvol_m2, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra3 lvol_m3, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra4 lvol_m4, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra5 lvol_m5, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra6 lvol_m6, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra7 lvol_m7, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra8 lvol_m8, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra9 lvol_m9, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra10 lvol_m10, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra11 lvol_m11, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra12 lvol_m12, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra13 lvol_m13, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra14 lvol_m14, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra15 lvol_m15, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra16 lvol_m16, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra17 lvol_m17, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra18 lvol_m18, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra19 lvol_m19, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra20 lvol_m20, ");
			cadenaSQL.append(" qc_lineasVolumenEf.media lvol_med, ");
			cadenaSQL.append(" qc_lineasVolumenEf.idVolumenEf lvol_vol ");
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasVolumenEf ");
			
			try
			{
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_lineasVolumenEf.idVolumenEf  = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLineasVolumenEf>();
				
				while(rsOpcion.next())
				{
					/*
					 * recojo mapeo operarios
					 */
					r_neto = r_neto/1000;
					if (rsOpcion.getDouble("lvol_m1")!=0 && rsOpcion.getDouble("lvol_m1") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m2")!=0 && rsOpcion.getDouble("lvol_m2") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m3")!=0 && rsOpcion.getDouble("lvol_m3") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m4")!=0 && rsOpcion.getDouble("lvol_m4") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m5")!=0 && rsOpcion.getDouble("lvol_m5") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m6")!=0 && rsOpcion.getDouble("lvol_m6") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m7")!=0 && rsOpcion.getDouble("lvol_m7") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m8")!=0 && rsOpcion.getDouble("lvol_m8") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m9")!=0 && rsOpcion.getDouble("lvol_m9") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m10")!=0 && rsOpcion.getDouble("lvol_m10") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m11")!=0 && rsOpcion.getDouble("lvol_m11") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m12")!=0 && rsOpcion.getDouble("lvol_m12") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m13")!=0 && rsOpcion.getDouble("lvol_m13") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m14")!=0 && rsOpcion.getDouble("lvol_m14") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m15")!=0 && rsOpcion.getDouble("lvol_m15") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m16")!=0 && rsOpcion.getDouble("lvol_m16") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m17")!=0 && rsOpcion.getDouble("lvol_m17") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m18")!=0 && rsOpcion.getDouble("lvol_m18") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m19")!=0 && rsOpcion.getDouble("lvol_m19") < r_neto) cuantasErroneas++;
					if (rsOpcion.getDouble("lvol_m20")!=0 && rsOpcion.getDouble("lvol_m20") < r_neto) cuantasErroneas++;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return cuantasErroneas;
	}
	
	public MapeoControlLineasVolumenEf datosOpcionesLineas(Integer r_id, String r_linea)
	{
		ResultSet rsOpcion = null;		
		MapeoControlLineasVolumenEf mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tablaLineas = "qc_lineas_volumenEf_emb";
			campo= "idVolumenEf";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasVolumenEf.idVolumenEf ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tablaLineas!=null && tablaLineas.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasVolumenEf.idCodigo lvol_id, ");
			cadenaSQL.append(" qc_lineasVolumenEf.hora lvol_hor, ");
			cadenaSQL.append(" qc_lineasVolumenEf.articulo lvol_art, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra1 lvol_m1, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra2 lvol_m2, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra3 lvol_m3, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra4 lvol_m4, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra5 lvol_m5, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra6 lvol_m6, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra7 lvol_m7, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra8 lvol_m8, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra9 lvol_m9, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra10 lvol_m10, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra11 lvol_m11, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra12 lvol_m12, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra13 lvol_m13, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra14 lvol_m14, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra15 lvol_m15, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra16 lvol_m16, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra17 lvol_m17, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra18 lvol_m18, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra19 lvol_m19, ");
			cadenaSQL.append(" qc_lineasVolumenEf.muestra20 lvol_m20, ");
			cadenaSQL.append(" qc_lineasVolumenEf.media lvol_med, ");
			cadenaSQL.append(" qc_lineasVolumenEf.idVolumenEf lvol_vol ");
			
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasVolumenEf ");
			
			try
			{
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and "); else cadenaWhere.append(" where ");
					cadenaWhere.append(" qc_lineasVolumenEf.idCodigo = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLineasVolumenEf();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lvol_id"));
					mapeo.setHora(rsOpcion.getString("lvol_hor"));
					mapeo.setArticulo(rsOpcion.getString("lvol_art"));
					mapeo.setMuestra1(rsOpcion.getString("lvol_m1"));
					mapeo.setMuestra2(rsOpcion.getString("lvol_m2"));
					mapeo.setMuestra3(rsOpcion.getString("lvol_m3"));
					mapeo.setMuestra4(rsOpcion.getString("lvol_m4"));
					mapeo.setMuestra5(rsOpcion.getString("lvol_m5"));
					mapeo.setMuestra6(rsOpcion.getString("lvol_m6"));
					mapeo.setMuestra7(rsOpcion.getString("lvol_m7"));
					mapeo.setMuestra8(rsOpcion.getString("lvol_m8"));
					mapeo.setMuestra9(rsOpcion.getString("lvol_m9"));
					mapeo.setMuestra10(rsOpcion.getString("lvol_m10"));
					mapeo.setMuestra11(rsOpcion.getString("lvol_m11"));
					mapeo.setMuestra12(rsOpcion.getString("lvol_m12"));
					mapeo.setMuestra13(rsOpcion.getString("lvol_m13"));
					mapeo.setMuestra14(rsOpcion.getString("lvol_m14"));
					mapeo.setMuestra15(rsOpcion.getString("lvol_m15"));
					mapeo.setMuestra16(rsOpcion.getString("lvol_m16"));
					mapeo.setMuestra17(rsOpcion.getString("lvol_m17"));
					mapeo.setMuestra18(rsOpcion.getString("lvol_m18"));
					mapeo.setMuestra19(rsOpcion.getString("lvol_m19"));
					mapeo.setMuestra20(rsOpcion.getString("lvol_m20"));
					mapeo.setMedia(rsOpcion.getDouble("lvol_med"));
					mapeo.setIdVolumen(rsOpcion.getInt("lvol_vol"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return mapeo;
	}
	
	public String guardarNuevoVolumenEf(MapeoControlVolumenEf r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}

		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".linea , ");
			cadenaSQL.append(tabla + ".lote, ");
			cadenaSQL.append(tabla + ".capacidad, ");
			cadenaSQL.append(tabla + ".media, ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".verificado, ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion , ");
			cadenaSQL.append(tabla + ".fecha ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCapacidad()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getCapacidad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, new Double(0));
		    }
		    if (r_mapeo.getMedia()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getMedia());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, new Double(0));
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(10, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public String guardarCambiosVolumenEf(MapeoControlVolumenEf r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".linea =?, ");
			cadenaSQL.append(tabla + ".lote=?, ");
			cadenaSQL.append(tabla + ".capacidad=?, ");
			cadenaSQL.append(tabla + ".media=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".verificado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCapacidad()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getCapacidad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, new Double(0));
		    }
		    if (r_mapeo.getMedia()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getMedia());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, new Double(0));
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlVolumenEf r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		String tablaLineas = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			tablaLineas = "qc_lineas_volumenEf_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
		}

		try
		{
			con= this.conManager.establecerConexionInd();
			
			cadenaSQL.append(" DELETE FROM " + tablaLineas );            
			cadenaSQL.append(" WHERE " + tablaLineas + ".idVolumenEf = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_volumen.idCodigo vol_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_volumen ");
	     	
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("vol_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	/*
	 * METODOS LINEAS LOTES MP
	 */
	public String guardarNuevoLineasVolumenEf(String r_linea, MapeoControlLineasVolumenEf r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_volumenEf_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".hora, ");
			cadenaSQL.append(tabla + ".articulo, ");
			cadenaSQL.append(tabla + ".muestra1, ");
			cadenaSQL.append(tabla + ".muestra2, ");
			cadenaSQL.append(tabla + ".muestra3, ");
			cadenaSQL.append(tabla + ".muestra4, ");
			cadenaSQL.append(tabla + ".muestra5, ");
			cadenaSQL.append(tabla + ".muestra6, ");
			cadenaSQL.append(tabla + ".muestra7, ");
			cadenaSQL.append(tabla + ".muestra8, ");
			cadenaSQL.append(tabla + ".muestra9, ");
			cadenaSQL.append(tabla + ".muestra10, ");
			cadenaSQL.append(tabla + ".muestra11, ");
			cadenaSQL.append(tabla + ".muestra12, ");
			cadenaSQL.append(tabla + ".muestra13, ");
			cadenaSQL.append(tabla + ".muestra14, ");
			cadenaSQL.append(tabla + ".muestra15, ");
			cadenaSQL.append(tabla + ".muestra16, ");
			cadenaSQL.append(tabla + ".muestra17, ");
			cadenaSQL.append(tabla + ".muestra18, ");
			cadenaSQL.append(tabla + ".muestra19, ");
			cadenaSQL.append(tabla + ".muestra20, ");
			cadenaSQL.append(tabla + ".media, ");
			cadenaSQL.append(tabla + ".idVolumenEf ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			if (r_mapeo.getHora()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getHora());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getMuestra1()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getMuestra1());
			}
			else
			{
				preparedStatement.setString(4, "0");
			}
			if (r_mapeo.getMuestra2()!=null)
			{
				preparedStatement.setString(5, r_mapeo.getMuestra2());
			}
			else
			{
				preparedStatement.setString(5, "0");
			}
			if (r_mapeo.getMuestra3()!=null)
			{
				preparedStatement.setString(6, r_mapeo.getMuestra3());
			}
			else
			{
				preparedStatement.setString(6, "0");
			}
			if (r_mapeo.getMuestra4()!=null)
			{
				preparedStatement.setString(7, r_mapeo.getMuestra4());
			}
			else
			{
				preparedStatement.setString(7, "0");
			}
			if (r_mapeo.getMuestra5()!=null)
			{
				preparedStatement.setString(8, r_mapeo.getMuestra5());
			}
			else
			{
				preparedStatement.setString(8, "0");
			}
			if (r_mapeo.getMuestra6()!=null)
			{
				preparedStatement.setString(9, r_mapeo.getMuestra6());
			}
			else
			{
				preparedStatement.setString(9, "0");
			}
			if (r_mapeo.getMuestra7()!=null)
			{
				preparedStatement.setString(10, r_mapeo.getMuestra7());
			}
			else
			{
				preparedStatement.setString(10, "0");
			}
			if (r_mapeo.getMuestra8()!=null)
			{
				preparedStatement.setString(11, r_mapeo.getMuestra8());
			}
			else
			{
				preparedStatement.setString(11, "0");
			}
			if (r_mapeo.getMuestra9()!=null)
			{
				preparedStatement.setString(12, r_mapeo.getMuestra9());
			}
			else
			{
				preparedStatement.setString(12, "0");
			}
			if (r_mapeo.getMuestra10()!=null)
			{
				preparedStatement.setString(13, r_mapeo.getMuestra10());
			}
			else
			{
				preparedStatement.setString(13, "0");
			}
			if (r_mapeo.getMuestra11()!=null)
			{
				preparedStatement.setString(14, r_mapeo.getMuestra11());
			}
			else
			{
				preparedStatement.setString(14, "0");
			}
			if (r_mapeo.getMuestra12()!=null)
			{
				preparedStatement.setString(15, r_mapeo.getMuestra12());
			}
			else
			{
				preparedStatement.setString(15, "0");
			}
			if (r_mapeo.getMuestra13()!=null)
			{
				preparedStatement.setString(16, r_mapeo.getMuestra13());
			}
			else
			{
				preparedStatement.setString(16, "0");
			}
			if (r_mapeo.getMuestra14()!=null)
			{
				preparedStatement.setString(17, r_mapeo.getMuestra14());
			}
			else
			{
				preparedStatement.setString(17, "0");
			}
			if (r_mapeo.getMuestra15()!=null)
			{
				preparedStatement.setString(18, r_mapeo.getMuestra15());
			}
			else
			{
				preparedStatement.setString(18, "0");
			}
			if (r_mapeo.getMuestra16()!=null)
			{
				preparedStatement.setString(19, r_mapeo.getMuestra16());
			}
			else
			{
				preparedStatement.setString(19, "0");
			}
			if (r_mapeo.getMuestra17()!=null)
			{
				preparedStatement.setString(20, r_mapeo.getMuestra17());
			}
			else
			{
				preparedStatement.setString(20, "0");
			}
			if (r_mapeo.getMuestra18()!=null)
			{
				preparedStatement.setString(21, r_mapeo.getMuestra18());
			}
			else
			{
				preparedStatement.setString(21, "0");
			}
			if (r_mapeo.getMuestra19()!=null)
			{
				preparedStatement.setString(22, r_mapeo.getMuestra19());
			}
			else
			{
				preparedStatement.setString(22, "0");
			}
			if (r_mapeo.getMuestra20()!=null)
			{
				preparedStatement.setString(23, r_mapeo.getMuestra20());
			}
			else
			{
				preparedStatement.setString(23, "0");
			}

			if (r_mapeo.getMedia()!=null)
			{
				preparedStatement.setDouble(24, r_mapeo.getMedia());
			}
			else
			{
				preparedStatement.setDouble(24, new Double(0));
			}
			if (r_mapeo.getIdVolumen()!=null)
			{
				preparedStatement.setInt(25, r_mapeo.getIdVolumen());
			}
			else
			{
				preparedStatement.setInt(25, 0);
			}
			preparedStatement.executeUpdate();
			this.actualizarMediaCabecera(r_linea, r_mapeo.getIdVolumen());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String guardarCambiosLineasVolumenEf(String r_linea, MapeoControlLineasVolumenEf r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_volumenEf_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
			cadenaSQL.append(" update " + tabla + " SET " );
			cadenaSQL.append(tabla + ".hora=?, ");
			cadenaSQL.append(tabla + ".articulo=?, ");
			cadenaSQL.append(tabla + ".muestra1=?, ");
			cadenaSQL.append(tabla + ".muestra2=?, ");
			cadenaSQL.append(tabla + ".muestra3=?, ");
			cadenaSQL.append(tabla + ".muestra4=?, ");
			cadenaSQL.append(tabla + ".muestra5=?, ");
			cadenaSQL.append(tabla + ".muestra6=?, ");
			cadenaSQL.append(tabla + ".muestra7=?, ");
			cadenaSQL.append(tabla + ".muestra8=?, ");
			cadenaSQL.append(tabla + ".muestra9=?, ");
			cadenaSQL.append(tabla + ".muestra10=?, ");
			cadenaSQL.append(tabla + ".muestra11=?, ");
			cadenaSQL.append(tabla + ".muestra12=?, ");
			cadenaSQL.append(tabla + ".muestra13=?, ");
			cadenaSQL.append(tabla + ".muestra14=?, ");
			cadenaSQL.append(tabla + ".muestra15=?, ");
			cadenaSQL.append(tabla + ".muestra16=?, ");
			cadenaSQL.append(tabla + ".muestra17=?, ");
			cadenaSQL.append(tabla + ".muestra18=?, ");
			cadenaSQL.append(tabla + ".muestra19=?, ");
			cadenaSQL.append(tabla + ".muestra20=?, ");
			cadenaSQL.append(tabla + ".media=?, ");
			cadenaSQL.append(tabla + ".idVolumenEf=? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			
			if (r_mapeo.getHora()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getHora());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getMuestra1()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getMuestra1());
			}
			else
			{
				preparedStatement.setString(3, "0");
			}
			if (r_mapeo.getMuestra2()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getMuestra2());
			}
			else
			{
				preparedStatement.setString(4, "0");
			}
			if (r_mapeo.getMuestra3()!=null)
			{
				preparedStatement.setString(5, r_mapeo.getMuestra3());
			}
			else
			{
				preparedStatement.setString(5, "0");
			}
			if (r_mapeo.getMuestra4()!=null)
			{
				preparedStatement.setString(6, r_mapeo.getMuestra4());
			}
			else
			{
				preparedStatement.setString(6, "0");
			}
			if (r_mapeo.getMuestra5()!=null)
			{
				preparedStatement.setString(7, r_mapeo.getMuestra5());
			}
			else
			{
				preparedStatement.setString(7, "0");
			}
			if (r_mapeo.getMuestra6()!=null)
			{
				preparedStatement.setString(8, r_mapeo.getMuestra6());
			}
			else
			{
				preparedStatement.setString(8, "0");
			}
			if (r_mapeo.getMuestra7()!=null)
			{
				preparedStatement.setString(9, r_mapeo.getMuestra7());
			}
			else
			{
				preparedStatement.setString(9, "0");
			}
			if (r_mapeo.getMuestra8()!=null)
			{
				preparedStatement.setString(10, r_mapeo.getMuestra8());
			}
			else
			{
				preparedStatement.setString(10, "0");
			}
			if (r_mapeo.getMuestra9()!=null)
			{
				preparedStatement.setString(11, r_mapeo.getMuestra9());
			}
			else
			{
				preparedStatement.setString(11, "0");
			}
			if (r_mapeo.getMuestra10()!=null)
			{
				preparedStatement.setString(12, r_mapeo.getMuestra10());
			}
			else
			{
				preparedStatement.setString(12, "0");
			}
			if (r_mapeo.getMuestra11()!=null)
			{
				preparedStatement.setString(13, r_mapeo.getMuestra11());
			}
			else
			{
				preparedStatement.setString(13, "0");
			}
			if (r_mapeo.getMuestra12()!=null)
			{
				preparedStatement.setString(14, r_mapeo.getMuestra12());
			}
			else
			{
				preparedStatement.setString(14, "0");
			}
			if (r_mapeo.getMuestra13()!=null)
			{
				preparedStatement.setString(15, r_mapeo.getMuestra13());
			}
			else
			{
				preparedStatement.setString(15, "0");
			}
			if (r_mapeo.getMuestra14()!=null)
			{
				preparedStatement.setString(16, r_mapeo.getMuestra14());
			}
			else
			{
				preparedStatement.setString(16, "0");
			}
			if (r_mapeo.getMuestra15()!=null)
			{
				preparedStatement.setString(17, r_mapeo.getMuestra15());
			}
			else
			{
				preparedStatement.setString(17, "0");
			}
			if (r_mapeo.getMuestra16()!=null)
			{
				preparedStatement.setString(18, r_mapeo.getMuestra16());
			}
			else
			{
				preparedStatement.setString(18, "0");
			}
			if (r_mapeo.getMuestra17()!=null)
			{
				preparedStatement.setString(19, r_mapeo.getMuestra17());
			}
			else
			{
				preparedStatement.setString(19, "0");
			}
			if (r_mapeo.getMuestra18()!=null)
			{
				preparedStatement.setString(20, r_mapeo.getMuestra18());
			}
			else
			{
				preparedStatement.setString(20, "0");
			}
			if (r_mapeo.getMuestra19()!=null)
			{
				preparedStatement.setString(21, r_mapeo.getMuestra19());
			}
			else
			{
				preparedStatement.setString(21, "0");
			}
			if (r_mapeo.getMuestra20()!=null)
			{
				preparedStatement.setString(22, r_mapeo.getMuestra20());
			}
			else
			{
				preparedStatement.setString(22, "0");
			}

			if (r_mapeo.getMedia()!=null)
			{
				preparedStatement.setDouble(23, r_mapeo.getMedia());
			}
			else
			{
				preparedStatement.setDouble(23, new Double(0));
			}
			if (r_mapeo.getIdVolumen()!=null)
			{
				preparedStatement.setInt(24, r_mapeo.getIdVolumen());
			}
			else
			{
				preparedStatement.setInt(24, 0);
			}
			preparedStatement.setInt(25, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			this.actualizarMediaCabecera(r_linea, r_mapeo.getIdVolumen());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public boolean comprobarControl(Integer r_idProgramacion, String r_lote, String r_capacidad, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_volumen.idCodigo lmp_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_volumen ");
			cadenaSQL.append(" where qc_volumen.lote = '" + r_lote.trim() + "' ");
			cadenaSQL.append(" and qc_volumen.capacidad = " + new Double(RutinasCadenas.reemplazarComaMiles(r_capacidad.trim())));
			cadenaSQL.append(" and qc_volumen.linea = '" + r_linea.trim() + "' ");
			cadenaSQL.append(" and qc_volumen.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public void actualizarMediaCabecera(String r_linea, Integer r_IdVolumenEf)
	{
		/*
		 * sumo la media de las lineas
		 * cuento el total de las lineas
		 * divido y updateo la cabecera.
		 */
//		Notificaciones.getInstance().mensajeAvisoCalendario("Acuerdate de actualizar la media del lote");
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		ResultSet rsOpcion = null;		
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			tablaLineas= "qc_lineas_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_volumen.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_volumen.idProgramacion ");
		}

		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);

			Double media = new Double(0);
			Double medias = new Double(0);
			Integer cuantos = new Integer(0);
			
			cadenaSQL=new StringBuffer();
			cadenaSQL.append(" SELECT sum(qc_volumen.media) vol_med ");
	     	cadenaSQL.append(" FROM " + tablaLineas +" qc_volumen ");
	     	cadenaSQL.append(" where idVolumenEf = " + r_IdVolumenEf );
	     	
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{

				medias = rsOpcion.getDouble("vol_med");
				break;
			}
			
			rsOpcion=null;
			
			cadenaSQL=new StringBuffer();
			cadenaSQL.append(" SELECT count(qc_volumen.idCodigo) vol_tot ");
	     	cadenaSQL.append(" FROM " + tablaLineas +" qc_volumen ");
	     	cadenaSQL.append(" where idVolumenEf = " + r_IdVolumenEf );

	     	rsOpcion= cs.executeQuery(cadenaSQL.toString());

			while (rsOpcion.next())
			{

				cuantos = rsOpcion.getInt("vol_tot");
				break;
			}

			if (medias!=null && medias!=0 && cuantos!=null && cuantos !=0)
			{
				media = medias/cuantos;
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".media = ? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
				    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
				    
		    	preparedStatement.setDouble(1, media);
		    	preparedStatement.setInt(2, r_IdVolumenEf);
		    	
		    	preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_volumenEf_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_volumenEf_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_volumenEf_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}