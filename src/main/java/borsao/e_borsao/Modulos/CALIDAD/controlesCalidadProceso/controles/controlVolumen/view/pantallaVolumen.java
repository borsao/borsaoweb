package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.modelo.MapeoControlLineasVolumen;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.modelo.MapeoControlVolumen;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.server.consultaVolumenServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaVolumen extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlVolumen mapeoControlVolumen = null;
	private MapeoControlLineasVolumen mapeoControlLineasVolumen = null;
	private consultaVolumenServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout frameGridLineas = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private Panel panelGridLineas = null;
	private GridPropio gridDatos = null;
	private GridPropio gridDatosLineas = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtLote= null;
	private TextField txtCapacidad= null;
	private Label media= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean creacionLinea = false;
	private boolean hayGridPadre = false;
	public String lineaProduccion = null;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "referencia";
	private Label lblCumplimentacion = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaVolumen(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(false);
		
    	this.cncs = new consultaVolumenServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("100%");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
//			this.frameCentral.setSpacing(true);
//			this.frameCentral.setHeightUndefined();
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setCaption("Referencias *** SIN *** Simbolo de Contenido Efectivo en la contra");
				this.lblCumplimentacion.setValue("Frecuencia: 5 muestras cada 2 horas. En el caso de vinos propios de Australia 12 muestras cada hora.");
				this.lblCumplimentacion.setWidth("500px");
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtLote = new TextField();
    			this.txtLote.setCaption("Lote");
    			this.txtLote.setEnabled(false);
    			this.txtLote.setWidth("120px");
				this.txtLote.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.txtCapacidad = new TextField();
				this.txtCapacidad.setCaption("Capacidad");
				this.txtCapacidad.setEnabled(false);
				this.txtCapacidad.setWidth("120px");
				this.txtCapacidad.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.txtRealizado= new TextField();
				this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");

				this.txtObservaciones= new TextArea();
				this.txtObservaciones.setCaption("Observaciones");
				this.txtObservaciones.setWidth("300");

				this.media = new Label();
				this.media.setCaption("Media Volumen");
				this.media.setWidth("180");
				this.media.addStyleName(ValoTheme.LABEL_H4);
				
				linea2.addComponent(this.txtLote);
				linea2.addComponent(this.txtCapacidad);
    			linea2.addComponent(this.txtRealizado);
    			linea2.addComponent(this.txtObservaciones);
    			linea2.addComponent(this.media);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
				
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnLimpiar.setEnabled(true);
    			
    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);
    			
//    			linea3.addComponent(this.btnLimpiar);
//    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		this.frameGrid.setHeight("175px");

    		this.frameGridLineas = new HorizontalLayout();
    		this.frameGridLineas.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		
			this.framePie = new HorizontalLayout();
			this.framePie.setHeight("3%");
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.TOP_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
			
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
//		principal.setExpandRatio(this.frameCentral, 1);
//		principal.setExpandRatio(this.framePie, 2);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{	
			MapeoControlVolumen mapeo = new MapeoControlVolumen();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlVolumen.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}			
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlVolumen mapeo = new MapeoControlVolumen();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setLote(this.txtLote.getValue());
			mapeo.setCapacidad(new Double(RutinasCadenas.reemplazarComaMiles(this.txtCapacidad.getValue())));
			mapeo.setMedia(new Double(this.media.getValue()));
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoVolumen(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
						frameGrid.removeComponent(panelGrid);
					}
					panelGrid=null;
					if (gridDatosLineas!=null)
					{
						gridDatosLineas.removeAllColumns();
						gridDatosLineas=null;
						frameGridLineas.removeComponent(panelGridLineas);
					}
					panelGridLineas=null;
					llenarRegistros();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlVolumen.getIdCodigo());
				rdo = cncs.guardarCambiosVolumen(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					panelGridLineas=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
    	String capacidad = cas.obtenerCapacidadArticulo(this.mapeoProduccion.getArticulo());
    	ArrayList<MapeoControlVolumen> vector=null;
    	MapeoControlVolumen mapeoPreop = new MapeoControlVolumen();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	mapeoPreop.setLote(this.mapeoProduccion.getLote());
    	mapeoPreop.setCapacidad(new Double(RutinasCadenas.reemplazarComaMiles(capacidad))*1000);
    	vector = this.cncs.datosOpcionesGlobal(mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlVolumen r_mapeo)
	{
		this.mapeoControlVolumen= new MapeoControlVolumen();
		this.mapeoControlVolumen.setIdCodigo(r_mapeo.getIdCodigo());
		
		setCreacion(false);
		
		this.txtLote.setValue(r_mapeo.getLote());
		if (r_mapeo.getCapacidad()!=null)
			this.txtCapacidad.setValue(RutinasCadenas.reemplazarComaMiles(r_mapeo.getCapacidad().toString()));
		if (r_mapeo.getMedia()!=null)
			this.actualizarMedia(r_mapeo.getCapacidad().toString(), r_mapeo.getMedia().toString());
			
		
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
	private void actualizarMedia(String r_capacidad, String r_media)
	{
		Double valorMedia = new Double(RutinasCadenas.reemplazarComaMiles(r_media));
		Double capacidad = new Double(RutinasCadenas.reemplazarComaMiles(r_capacidad));
		switch (RutinasCadenas.reemplazarComaMiles(r_capacidad.trim()))
		{
			case "750":
			{
	
				/*
				 * limite inferior 745
				 * limite superior 760
				 * 
				 * fuera de estos avisar a calidad
				 */
				if (valorMedia < (capacidad - 5) || valorMedia > (capacidad + 10) )
				{
					Notificaciones.getInstance().mensajeError("Error en la media. Avisar a calidad");
				}
				break;
			}
			case "1500":
			{
				/*
				 * limite inferior 1490
				 * limite superior 1510
				 * 
				 * fuera de estos avisar a calidad
				 */
				if (valorMedia < (capacidad - 10) || valorMedia > (capacidad + 10) )
				{
					Notificaciones.getInstance().mensajeError("Error en la media. Avisar a calidad");
				}

				break;
			}
			case "375":
			{
				/*
				 * limite inferior 373
				 * limite superior 379
				 * 
				 * fuera de estos avisar a calidad
				 */
				if (valorMedia < (capacidad - 2) || valorMedia > (capacidad + 4) )
				{
					Notificaciones.getInstance().mensajeError("Error en la media. Avisar a calidad");
				}

				break;
			}
			case "3000":
			{
				/*
				 * limite inferior 2990
				 * limite superior 3010
				 * 
				 * fuera de estos avisar a calidad
				 */
				if (valorMedia < (capacidad - 10) || valorMedia > (capacidad + 10) )
				{
					Notificaciones.getInstance().mensajeError("Error en la media. Avisar a calidad");
				}

				break;
			}
		}
		
		this.media.setValue(r_media);
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
//    	txtLote.setEnabled(r_activar);
//    	txtCapacidad.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	media.setEnabled(false);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		Double cap = new Double(0);
		this.setCreacion(true);
		
		if (this.lineaProduccion.contentEquals("BIB"))
		{
			consultaProgramacionEnvasadoraServer cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			MapeoProgramacionEnvasadora mapeo = cpes.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());
			
			if (mapeo!=null && (mapeo.getLote()==null || mapeo.getLote().length()==0))
			{
				String lote = cpes.buscarLote(mapeo);
				if (lote!=null && lote.length()>0) 
					this.txtLote.setValue(lote); 
				else 
					this.txtLote.setValue("");
			}
			else if (mapeo!=null && mapeo.getLote()!=null && mapeo.getLote().length()>0)
				this.txtLote.setValue(mapeo.getLote());
			else
			{
				this.txtLote.setValue("");
			}

		}
		else
		{
			consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
			MapeoProgramacion mapeo = cps.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());
			
			if (mapeo!=null && (mapeo.getLote()==null || mapeo.getLote().length()==0))
			{
				String lote = cps.buscarLote(mapeo);
				if (lote!=null && lote.length()>0) 
					this.txtLote.setValue(lote); 
				else 
					this.txtLote.setValue("");
			}
			else if (mapeo!=null && mapeo.getLote()!=null && mapeo.getLote().length()>0)
				this.txtLote.setValue(mapeo.getLote());
			else
			{
				this.txtLote.setValue("");
			}
		}
		
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		cap = new Double(RutinasCadenas.reemplazarComaMiles(cas.obtenerCapacidadArticulo(this.mapeoProduccion.getArticulo())))*1000;
		this.txtCapacidad.setValue(cap.toString());
		
		this.media.setValue("0");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtLote.getValue()==null || this.txtLote.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el lote de control ");
    		return false;
    	}
    	if ((this.txtCapacidad.getValue()==null || this.txtCapacidad.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la capacidad del lote de control ");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlVolumen) r_fila)!=null)
    	{
    		MapeoControlVolumen mapeoVolumen = (MapeoControlVolumen) r_fila;
    		if (mapeoVolumen!=null)
    		{
    			this.llenarEntradasDatos(mapeoVolumen);
    			
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					frameCentral.removeComponent(frameGridLineas);
				}
				panelGridLineas=null;

    			this.frameGridLineas.addComponent(this.generarGridLineas(mapeoVolumen.getIdCodigo()));
    			this.frameCentral.addComponent(this.frameGridLineas);
    			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
    		}
    		mapeoVolumen.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
		}
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);
    }

    private void generarGrid(ArrayList<MapeoControlVolumen> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */

    	if (r_vector!= null && !r_vector.isEmpty())
		{
			this.frameGrid.addComponent(this.generarGridCAb(r_vector));
			this.frameCentral.addComponent(this.frameGrid);
			this.frameCentral.setExpandRatio(this.frameGrid, 2);
			this.gridDatos.select(this.gridDatos.getContainerDataSource().getIdByIndex(r_vector.size()-1));
			this.filaSeleccionada(this.gridDatos.getSelectedRow());
		}
    	else
    	{
    		this.limpiarCampos();
    	}
	}
    
    private Panel generarGridCAb(ArrayList<MapeoControlVolumen> r_vector)
    {
    	pantallaVolumen app = this;
    	panelGrid= new Panel("Control Volumen");
    	panelGrid.setSizeFull(); // Shrink to fit content
    	
    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		BeanItemContainer<MapeoControlVolumen> container = new BeanItemContainer<MapeoControlVolumen>(MapeoControlVolumen.class);
		this.gridDatos=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
				
		    	getColumn("acc").setHeaderCaption("");
		    	getColumn("acc").setSortable(true);
		    	getColumn("acc").setWidth(new Double(50));
		    	getColumn("fecha").setHeaderCaption("Fecha");
		    	getColumn("fecha").setWidth(new Double(120));
		    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
		    	getColumn("lote").setHeaderCaption("Lote");
		    	getColumn("lote").setWidth(new Double(100));
		    	getColumn("capacidad").setHeaderCaption("Capacidad");
		    	getColumn("capacidad").setWidth(new Double(100));
		    	getColumn("realizado").setHeaderCaption("Realizado");
		    	getColumn("realizado").setWidth(new Double(155));
		    	getColumn("verificado").setHeaderCaption("Verificado");
		    	getColumn("verificado").setWidth(new Double(155));
		    	getColumn("media").setHeaderCaption("Media");
		    	getColumn("media").setWidth(new Double(155));

				getColumn("idCodigo").setHidden(true);
				getColumn("idProgramacion").setHidden(true);
				getColumn("linea").setHidden(true);
				getColumn("fecha").setHidden(true);
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				setColumnOrder("acc", "fecha", "lote", "capacidad", "media", "realizado", "verificado", "observaciones");
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {

				addItemClickListener(new ItemClickEvent.ItemClickListener() 
				{
					public void itemClick(ItemClickEvent event) 
					{
						if (event.getPropertyId()!=null)
		            	{
							MapeoControlVolumen mapeoOrig = (MapeoControlVolumen) event.getItemId();
			            	MapeoControlLineasVolumen mapeoLineas = new MapeoControlLineasVolumen();
			            	mapeoLineas.setIdVolumen(mapeoOrig.getIdCodigo());
			            	mapeoLineas.setArticulo(mapeoProduccion.getArticulo());
			            	
		            		if (event.getPropertyId().toString().equals("acc"))
			            	{
			        			try
			        			{
			        				/*
			        				 * Me falta coger el nombre del articulo de materia prima correspondiente y pasarlo en 
			        				 * el campo descripcion del mapeo
			        				 * 
			        				 * asi se evitan escribirlo todas las veces
			        				 */
			        				pantallaActualizacionLineasVolumen vt = new pantallaActualizacionLineasVolumen(app, mapeoLineas, "Datos relativos al lote " + mapeoOrig.getLote().toString(), true);
			        				app.getUI().addWindow(vt);
			        			}
			        			catch (IndexOutOfBoundsException ex)
			        			{
			        				
			        			}
			            	}            	
			            	else
			            	{
			            		filaSeleccionada(event.getItemId());
			            	}
			            }
					}
				});


			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
					setCellStyleGenerator(new Grid.CellStyleGenerator() {
		            
		            @Override
		            public String getStyle(Grid.CellReference cellReference) {
		            	if ("acc".equals(cellReference.getPropertyId())) 
		            	{            		
		            		return "cell-nativebuttonAgregar";
		            	}
		            	else
		            	{
		            		return "cell-normal";
		            	}
		            }
		        });
			}
		};
		this.gridDatos.setContainerDataSource(container);

		this.gridDatos.getContainer().removeAllItems();
		this.gridDatos.getContainer().addAll(r_vector);
		
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setEditorEnabled(false);
		this.gridDatos.setConFiltro(false);
		this.gridDatos.setSeleccion(SelectionMode.SINGLE);
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
    	
		this.gridDatos.establecerTitulosColumnas();
		this.gridDatos.establecerOrdenPresentacionColumnas();
		this.gridDatos.asignarEstilos();
		this.gridDatos.cargarListeners();
		
		panelGrid.setContent(gridDatos);
    	
    	return panelGrid;
    }
    private Panel generarGridLineas(Integer r_id)
    {
    	
    	pantallaVolumen app = this;
    	MapeoControlVolumen mapeoPreop = new MapeoControlVolumen();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	ArrayList<MapeoControlLineasVolumen> vectorLineas = this.cncs.datosOpcionesGlobalLineas(r_id, mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	panelGridLineas= new Panel("Control Muestras Volumen ");
    	panelGridLineas.setSizeFull(); // Shrink to fit content

    	BeanItemContainer<MapeoControlLineasVolumen> container = new BeanItemContainer<MapeoControlLineasVolumen>(MapeoControlLineasVolumen.class);
		this.gridDatosLineas=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
				
		    	getColumn("acc").setHeaderCaption("");
		    	getColumn("acc").setSortable(true);
		    	getColumn("acc").setWidth(new Double(50));

				getColumn("hora").setHeaderCaption("Hora");
				getColumn("hora").setWidth(new Double(150));
				getColumn("articulo").setHeaderCaption("Articulo");
				getColumn("articulo").setWidth(new Double(250));
				getColumn("muestra1").setHeaderCaption("Muestra1");
				getColumn("muestra1").setWidth(new Double(140));
				getColumn("muestra2").setHeaderCaption("Muestra2");
				getColumn("muestra2").setWidth(new Double(140));
				getColumn("muestra3").setHeaderCaption("Muestra3");
				getColumn("muestra3").setWidth(new Double(140));
				getColumn("muestra4").setHeaderCaption("Muestra4");
				getColumn("muestra4").setWidth(new Double(140));
				getColumn("muestra5").setHeaderCaption("Muestra5");
				getColumn("muestra5").setWidth(new Double(140));
				getColumn("media").setHeaderCaption("Media");
				getColumn("media").setWidth(new Double(140));
				
				getColumn("idCodigo").setHidden(true);
				getColumn("idVolumen").setHidden(true);
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				setColumnOrder("acc", "hora", "articulo", "muestra1","muestra2", "muestra3", "muestra4", "muestra5", "media");
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {
				addItemClickListener(new ItemClickEvent.ItemClickListener() 
				{
					public void itemClick(ItemClickEvent event) 
					{
						if (event.getPropertyId()!=null)
		            	{
							MapeoControlLineasVolumen mapeoOrig = (MapeoControlLineasVolumen) event.getItemId();
			            	
		            		if (event.getPropertyId().toString().equals("acc"))
			            	{
			        			try
			        			{
			        				/*
			        				 * Me falta coger el nombre del articulo de materia prima correspondiente y pasarlo en 
			        				 * el campo descripcion del mapeo
			        				 * 
			        				 * asi se evitan escribirlo todas las veces
			        				 */
			        				mapeoOrig.setIdVolumen(r_id);
			        				mapeoOrig.setArticulo(mapeoProduccion.getArticulo());
			        				pantallaActualizacionLineasVolumen vt = new pantallaActualizacionLineasVolumen(app, mapeoOrig, "Datos relativos a " + mapeoOrig.getArticulo().toString() + " Hora:" + mapeoOrig.getHora(),false);
			        				app.getUI().addWindow(vt);
			        			}
			        			catch (IndexOutOfBoundsException ex)
			        			{
			        				
			        			}
			            	}            	
			            	else
			            	{
			            	}
			            }
					}
				});
				
			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
		    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
		            
		            @Override
		            public String getStyle(Grid.CellReference cellReference) {
		            	if ("acc".equals(cellReference.getPropertyId())) 
		            	{            		
		            		return "cell-nativebuttonProgramacion";
		            	}
		            	else
		            	{
		            		return "cell-normal";
		            	}
		            }
		        });

			}
		};
		this.gridDatosLineas.setContainerDataSource(container);
		
		this.gridDatosLineas.getContainer().removeAllItems();
		this.gridDatosLineas.getContainer().addAll(vectorLineas);
		
		this.gridDatosLineas.addStyleName("smallgrid");
		this.gridDatosLineas.setEditorEnabled(false);
		this.gridDatosLineas.setConFiltro(false);
		this.gridDatosLineas.setSeleccion(SelectionMode.SINGLE);
		this.gridDatosLineas.setWidth("100%");
		this.gridDatosLineas.setHeight("100%");
		
		this.gridDatosLineas.establecerTitulosColumnas();
		this.gridDatosLineas.establecerOrdenPresentacionColumnas();
		this.gridDatosLineas.cargarListeners();
		this.gridDatosLineas.asignarEstilos();
		panelGridLineas.setContent(gridDatosLineas);
		
		return panelGridLineas;
    }
    
	public void actualizarDatosLineaVolumen(MapeoControlLineasVolumen r_mapeo, boolean r_nuevo)
	{
		String rdo = null;
		creacionLinea=r_nuevo;
		consultaVolumenServer cps = consultaVolumenServer.getInstance(CurrentUser.get());
		
		if (!creacionLinea)
		{
			rdo = cps.guardarCambiosLineasVolumen(this.lineaProduccion, r_mapeo);
		}
		else
		{
			rdo = cps.guardarNuevoLineasVolumen(this.lineaProduccion, r_mapeo);
		}
		
		if (rdo != null)
		{
			Notificaciones.getInstance().mensajeError("Error al actualizar los datos de las muestras");
		}
		else
		{
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
				this.frameCentral.removeComponent(this.frameGridLineas);
			}

			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
				this.frameCentral.removeComponent(this.frameGrid);
			}
			this.llenarRegistros();
			
//			this.frameGrid.addComponent(this.generarGridCAb(r_vector));
//			this.frameCentral.setExpandRatio(this.frameGrid, 2);
//			this.frameGridLineas.addComponent(this.generarGridLineas(r_mapeo.getIdVolumen()));
//			this.frameCentral.addComponent(this.frameGridLineas);
//			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
		}
	}
}