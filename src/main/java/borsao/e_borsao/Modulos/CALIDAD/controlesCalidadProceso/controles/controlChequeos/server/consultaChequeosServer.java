package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlChequeos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlChequeos.modelo.MapeoControlChequeos;

public class consultaChequeosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaChequeosServer  instance;
	
	public consultaChequeosServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaChequeosServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaChequeosServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlChequeos datosOpcionesGlobal(MapeoControlChequeos r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlChequeos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_chequeo.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_chequeo.idCodigo chq_id, ");
			cadenaSQL.append(" qc_chequeo.hora chq_hora, ");
			cadenaSQL.append(" qc_chequeo.serie chq_ser, ");
			cadenaSQL.append(" qc_chequeo.numeroDesde chq_numd, ");
			cadenaSQL.append(" qc_chequeo.numeroHasta chq_numh, ");
			cadenaSQL.append(" qc_chequeo.realizado chq_rea, ");
			cadenaSQL.append(" qc_chequeo.verificado chq_ver, ");
			cadenaSQL.append(" qc_chequeo.fecha chq_fec, ");
			cadenaSQL.append(" qc_chequeo.observaciones chq_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_chequeo ");
			try
			{
				if (r_mapeo!=null)
				{
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_chequeo.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlChequeos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("chq_id"));
					mapeo.setSerie(rsOpcion.getString("chq_ser"));
					mapeo.setNumeroDesde(rsOpcion.getInt("chq_numd"));
					mapeo.setNumeroHasta(rsOpcion.getInt("chq_numh"));
					mapeo.setRealizado(rsOpcion.getString("chq_rea"));
					mapeo.setVerificado(rsOpcion.getString("chq_ver"));
					mapeo.setObservaciones(rsOpcion.getString("chq_obs"));
					mapeo.setFecha(rsOpcion.getDate("chq_fec"));
					mapeo.setHora(rsOpcion.getString("chq_hora"));
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlChequeos> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlChequeos> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlChequeos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_chequeo.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_chequeo.idCodigo chq_id, ");
			cadenaSQL.append(" qc_chequeo.hora chq_hora, ");
			cadenaSQL.append(" qc_chequeo.serie chq_ser, ");
			cadenaSQL.append(" qc_chequeo.numeroDesde chq_numd, ");
			cadenaSQL.append(" qc_chequeo.numeroHasta chq_numh, ");
			cadenaSQL.append(" qc_chequeo.realizado chq_rea, ");
			cadenaSQL.append(" qc_chequeo.verificado chq_ver, ");
			cadenaSQL.append(" qc_chequeo.fecha chq_fec, ");
			cadenaSQL.append(" qc_chequeo.observaciones chq_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_chequeo ");
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlChequeos>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlChequeos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("chq_id"));
					mapeo.setSerie(rsOpcion.getString("chq_ser"));
					mapeo.setNumeroDesde(rsOpcion.getInt("chq_numd"));
					mapeo.setNumeroHasta(rsOpcion.getInt("chq_numh"));
					mapeo.setRealizado(rsOpcion.getString("chq_rea"));
					mapeo.setVerificado(rsOpcion.getString("chq_ver"));
					mapeo.setObservaciones(rsOpcion.getString("chq_obs"));
					mapeo.setFecha(rsOpcion.getDate("chq_fec"));
					mapeo.setHora(rsOpcion.getString("chq_hora"));
					
					vector.add(mapeo);
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}
	
	public String guardarNuevoChequeos(MapeoControlChequeos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
		}

		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".hora, ");
			cadenaSQL.append(tabla + ".serie , ");
			cadenaSQL.append(tabla + ".numeroDesde , ");
			cadenaSQL.append(tabla + ".numeroHasta , ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".verificado, ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion , ");
			cadenaSQL.append(tabla + ".fecha ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");

			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getNumeroDesde()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getNumeroDesde());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getNumeroHasta()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getNumeroHasta());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
	    	if (r_mapeo.getRealizado()!=null)
	    	{
	    		preparedStatement.setString(6, r_mapeo.getRealizado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getVerificado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
	    	}
	    	if (r_mapeo.getObservaciones()!=null)
	    	{
	    		preparedStatement.setString(8, r_mapeo.getObservaciones());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(8, null);
	    	}
	    	if (r_mapeo.getIdProgramacion()!=null)
	    	{
	    		preparedStatement.setInt(9, r_mapeo.getIdProgramacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setInt(9, 0);
	    	}
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(10, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(10, null);
	    	}
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambiosChequeos(MapeoControlChequeos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
		}
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".hora=?, ");
			cadenaSQL.append(tabla + ".serie=?, ");
			cadenaSQL.append(tabla + ".numeroDesde=?, ");
			cadenaSQL.append(tabla + ".numeroHasta=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".verificado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");

			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

		    if (r_mapeo.getNumeroDesde()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getNumeroDesde());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getNumeroHasta()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getNumeroHasta());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
	    	if (r_mapeo.getRealizado()!=null)
	    	{
	    		preparedStatement.setString(5, r_mapeo.getRealizado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(5, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
	    	{
	    		preparedStatement.setString(6, r_mapeo.getVerificado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
	    	}
	    	if (r_mapeo.getObservaciones()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getObservaciones());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
	    	}
	    	if (r_mapeo.getIdProgramacion()!=null)
	    	{
	    		preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setInt(8, 0);
	    	}
	    	
	    	preparedStatement.setInt(9, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlChequeos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_ejercicio, String r_semana, String r_fecha)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_chequeo.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_chequeo.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_chequeo.idCodigo chq_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_chequeo ");
	     	cadenaSQL.append(" where prd_prog.ejercicio = " + r_ejercicio );
	     	cadenaSQL.append(" and prd_prog.semana = " + r_semana);
	     	cadenaSQL.append(" and qc_chequeo.linea = " + r_linea);
	     	cadenaSQL.append(" and qc_chequeo.fecha = '" + RutinasFechas.convertirAFechaMysql(r_fecha) + "' ");
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("chq_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo chq_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_chequeos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_chequeos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_chequeos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}