package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.modelo.MapeoControlVaciado;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.server.consultaVaciadoServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaVaciado extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlVaciado mapeoControlVaciado = null;
	private consultaVaciadoServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	private Label lblCumplimentacion= null;
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private CheckBox chkRevision = null;
	private CheckBox chkPerfecto= null;
	private TextField txtArticulo= null;
	private TextField txtDescripcion= null;
	private TextField txtLote= null;
	private TextField txtTipoOperacion= null;

	
	private Combo cmbZona1 = null;
	private Combo cmbZona2 = null;
	private Combo cmbZona3 = null;
	private Combo cmbZona4 = null;
	private Combo cmbZona5 = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app = null;
	private static String tipoControl = "referencia";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaVaciado(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaVaciadoServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
			HorizontalLayout linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new Combo("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setEnabled(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

			this.txtFecha = new EntradaDatosFecha();
			this.txtFecha.setCaption("Fecha");
			this.txtFecha.setEnabled(false);
			this.txtFecha.setValue(this.mapeoProduccion.getFecha());
			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtFecha.setWidth("150px");
			
			this.lblCumplimentacion = new Label();
			this.lblCumplimentacion.setValue("Frecuencia: Al comienzo y al final de cada referencia de material auxiliar.");
//			this.lblCumplimentacion.setWidth("100%");
//			this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
			this.lblCumplimentacion.addStyleName("lblTituloCalidad");

			linea1.addComponent(this.txtEjercicio);
			linea1.addComponent(this.cmbSemana);
			linea1.addComponent(this.txtFecha);
			linea1.addComponent(this.lblCumplimentacion);
			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);

		HorizontalLayout linea2 = new HorizontalLayout();
		linea2.setSpacing(true);
		
			this.txtArticulo= new TextField();
			this.txtArticulo.setCaption("Articulo a elaborar");
			this.txtArticulo.setWidth("120px");
			this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
	
			this.txtDescripcion= new TextField();
			this.txtDescripcion.setCaption("Descsripcion Articulo");
			this.txtDescripcion.setWidth("500px");
			this.txtDescripcion.addStyleName(ValoTheme.TEXTFIELD_TINY);

			this.txtLote= new TextField();
			this.txtLote.setCaption("Lote de Fabricación");
			this.txtLote.setWidth("120px");
			this.txtLote.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			this.txtTipoOperacion= new TextField();
			this.txtTipoOperacion.setCaption("Tipo Operacion");
			this.txtTipoOperacion.setWidth("120px");
			this.txtTipoOperacion.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			linea2.addComponent(this.txtArticulo);
			linea2.addComponent(this.txtDescripcion);
			linea2.addComponent(this.txtLote);
			linea2.addComponent(this.txtTipoOperacion);

		HorizontalLayout linea3 = new HorizontalLayout();
		linea3.setSpacing(true);
		
			this.chkRevision= new CheckBox();
			this.chkRevision.setCaption("Revisadas las etapas de elaboración?");
	
			this.chkPerfecto= new CheckBox();
			this.chkPerfecto.setCaption("Articulo en perfectas condiciones?");

			linea3.addComponent(this.chkRevision);
			linea3.setComponentAlignment(this.chkRevision, Alignment.BOTTOM_LEFT);
			linea3.addComponent(this.chkPerfecto);
			linea3.setComponentAlignment(this.chkPerfecto, Alignment.BOTTOM_LEFT);
			
		HorizontalLayout linea4 = new HorizontalLayout();
		linea4.setSpacing(true);

			this.txtHoras = new TextField();
			this.txtHoras.setCaption("Hora");
			this.txtHoras.setWidth("120px");
			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtHoras.addStyleName("rightAligned");
			
			this.cmbZona1= new Combo("ZONA 1");
			this.cmbZona1.setWidth("150px");
			this.cmbZona1.setEnabled(false);
			this.cmbZona1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbZona1.setNewItemsAllowed(false);
			this.cmbZona1.setNullSelectionAllowed(false);
			
			this.cmbZona2= new Combo("ZONA 2");
			this.cmbZona2.setWidth("150px");
			this.cmbZona2.setEnabled(false);
			this.cmbZona2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbZona2.setNewItemsAllowed(false);
			this.cmbZona2.setNullSelectionAllowed(false);
			
			this.cmbZona3= new Combo("ZONA 3");
			this.cmbZona3.setWidth("150px");
			this.cmbZona3.setEnabled(false);
			this.cmbZona3.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbZona3.setNewItemsAllowed(false);
			this.cmbZona3.setNullSelectionAllowed(false);
			
			this.cmbZona4= new Combo("ZONA 4");
			this.cmbZona4.setWidth("150px");
			this.cmbZona4.setEnabled(false);
			this.cmbZona4.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbZona4.setNewItemsAllowed(false);
			this.cmbZona4.setNullSelectionAllowed(false);
			
			this.cmbZona5= new Combo("ZONA 5");
			this.cmbZona5.setWidth("150px");
			this.cmbZona5.setEnabled(false);
			this.cmbZona5.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbZona5.setNewItemsAllowed(false);
			this.cmbZona5.setNullSelectionAllowed(false);


			linea4.addComponent(this.txtHoras);
			linea4.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
			linea4.addComponent(this.cmbZona1);
			linea4.addComponent(this.cmbZona2);
			linea4.addComponent(this.cmbZona3);
			linea4.addComponent(this.cmbZona4);
			linea4.addComponent(this.cmbZona5);

		HorizontalLayout linea5 = new HorizontalLayout();
			linea5.setSpacing(true);
			
			this.txtRealizado= new TextField();
			this.txtRealizado.setCaption("Realizado Por");
			this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtRealizado.setWidth("180px");
			
			this.txtObservaciones= new TextArea();
			this.txtObservaciones.setCaption("Observaciones");
			this.txtObservaciones.setWidth("300");
			
			btnLimpiar= new Button("Nuevo");
			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

			btnInsertar= new Button("Guardar");
			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

			btnEliminar= new Button("Eliminar");
			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
			btnEliminar.setEnabled(false);

			linea5.addComponent(this.txtRealizado);
			linea5.addComponent(this.txtObservaciones);
			linea5.addComponent(this.btnLimpiar);
			linea5.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea5.addComponent(this.btnInsertar);
			linea5.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea5.addComponent(this.btnEliminar);
			linea5.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		this.frameCentral.addComponent(linea5);
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlVaciado mapeo = new MapeoControlVaciado();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlVaciado.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlVaciado mapeo = new MapeoControlVaciado();

			if (chkPerfecto.getValue()) mapeo.setPerfecto("S"); else mapeo.setPerfecto("N");
			if (chkRevision.getValue()) mapeo.setRevision("S"); else mapeo.setRevision("N");
			mapeo.setArticulo(this.txtArticulo.getValue());
			mapeo.setDescripcion(this.txtDescripcion.getValue());
			mapeo.setLote(this.txtLote.getValue());
			mapeo.setTipoOperacion(this.txtTipoOperacion.getValue());

			mapeo.setEstadoZona1(this.cmbZona1.getValue().toString());
			mapeo.setEstadoZona2(this.cmbZona2.getValue().toString());
			mapeo.setEstadoZona3(this.cmbZona3.getValue().toString());
			mapeo.setEstadoZona4(this.cmbZona4.getValue().toString());
			mapeo.setEstadoZona5(this.cmbZona5.getValue().toString());
			
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlVaciado.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlVaciado.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlVaciado> vector=null;

    	MapeoControlVaciado mapeoPreop = new MapeoControlVaciado();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlVaciado r_mapeo)
	{
		setCreacion(false);

		this.mapeoControlVaciado = new MapeoControlVaciado();
		this.mapeoControlVaciado.setIdCodigo(r_mapeo.getIdCodigo());
		
		if (r_mapeo.getRevision().contentEquals("S")) chkRevision.setValue(true);
		if (r_mapeo.getPerfecto().contentEquals("S")) chkPerfecto.setValue(true);
		this.txtArticulo.setValue(r_mapeo.getArticulo());
		this.txtDescripcion.setValue(r_mapeo.getDescripcion());
		this.txtLote.setValue(r_mapeo.getLote());
		this.txtTipoOperacion.setValue(r_mapeo.getTipoOperacion());
		
		this.cmbZona1.setValue(r_mapeo.getEstadoZona1());
		this.cmbZona2.setValue(r_mapeo.getEstadoZona2());
		this.cmbZona3.setValue(r_mapeo.getEstadoZona3());
		this.cmbZona4.setValue(r_mapeo.getEstadoZona4());
		this.cmbZona5.setValue(r_mapeo.getEstadoZona5());
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	cmbZona1.setEnabled(r_activar);
    	cmbZona2.setEnabled(r_activar);
    	cmbZona3.setEnabled(r_activar);
    	cmbZona4.setEnabled(r_activar);
    	cmbZona5.setEnabled(r_activar);

    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
//    	txtVerificado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.chkRevision.setValue(false);
		this.chkPerfecto.setValue(false);
		this.txtArticulo.setValue("");
		this.txtDescripcion.setValue("");
		this.txtLote.setValue("");
		this.txtTipoOperacion.setValue("");
		
		this.cmbZona1.setValue(null);
		this.cmbZona2.setValue(null);
		this.cmbZona3.setValue(null);
		this.cmbZona4.setValue(null);
		this.cmbZona5.setValue(null);
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");

		if (this.mapeoProduccion!=null)
		{
			if (this.lineaProduccion.contentEquals("Embotelladora"))
			{
				consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
				MapeoProgramacion mapeoProgramacion = cps.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());
	
				this.txtArticulo.setValue(mapeoProgramacion.getArticulo());
				this.txtDescripcion.setValue(mapeoProgramacion.getDescripcion());
				this.txtTipoOperacion.setValue(mapeoProgramacion.getTipo());
				if (mapeoProgramacion.getLote()!=null && mapeoProgramacion.getLote().length()>0)
				{
					this.txtLote.setValue(mapeoProgramacion.getLote());
				}
				else
				{
					this.txtLote.setValue(cps.buscarLote(mapeoProgramacion));
				}
			}
			else
			{
				consultaProgramacionEnvasadoraServer cps = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
				MapeoProgramacionEnvasadora mapeoProgramacion = cps.datosProgramacionGlobal(this.mapeoProduccion.getIdProgramacion());
	
				this.txtArticulo.setValue(mapeoProgramacion.getArticulo());
				this.txtDescripcion.setValue(mapeoProgramacion.getDescripcion());
				this.txtTipoOperacion.setValue(mapeoProgramacion.getTipo());
				this.txtLote.setValue(mapeoProgramacion.getLote());
			}
		}
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	if ((this.txtArticulo.getValue()==null || this.txtArticulo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el artículo a elaborar.");
    		return false;
    	}
    	if ((this.txtLote.getValue()==null || this.txtLote.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el lote de fabricación");
    		return false;
    	}
    	if ((this.txtTipoOperacion.getValue()==null || this.txtTipoOperacion.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tipo de operacion realizada");
    		return false;
    	}
    	return true;

    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarCombosZona();
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlVaciado) r_fila)!=null)
    	{
    		MapeoControlVaciado mapeoPreop = (MapeoControlVaciado) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlVaciado> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Vaciado LINEA");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlVaciado> container = new BeanItemContainer<MapeoControlVaciado>(MapeoControlVaciado.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(80));
			    	getColumn("articulo").setHeaderCaption("Articulo");
			    	getColumn("articulo").setWidth(new Double(80));
			    	getColumn("descripcion").setHeaderCaption("Descripcion");
			    	getColumn("descripcion").setWidth(new Double(120));
			    	getColumn("lote").setHeaderCaption("Lote");
			    	getColumn("lote").setWidth(new Double(80));
			    	getColumn("tipoOperacion").setHeaderCaption("Operacion");
			    	getColumn("tipoOperacion").setWidth(new Double(100));
			    	getColumn("revision").setHeaderCaption("Rev.");
			    	getColumn("revision").setWidth(new Double(80));
			    	getColumn("perfecto").setHeaderCaption("Perf.");
			    	getColumn("perfecto").setWidth(new Double(80));
			    	
			    	getColumn("estadoZona1").setHeaderCaption("ZONA 1");
			    	getColumn("estadoZona1").setWidth(new Double(150));
			    	getColumn("estadoZona2").setHeaderCaption("ZONA 2");
			    	getColumn("estadoZona2").setWidth(new Double(150));
			    	getColumn("estadoZona3").setHeaderCaption("ZONA 3");
			    	getColumn("estadoZona3").setWidth(new Double(150));
			    	getColumn("estadoZona4").setHeaderCaption("ZONA 4");
			    	getColumn("estadoZona4").setWidth(new Double(150));
			    	getColumn("estadoZona5").setHeaderCaption("ZONA 5");
			    	getColumn("estadoZona5").setWidth(new Double(150));
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					getColumn("verificado").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() 
				{					
					setColumnOrder("fecha", "hora",  "articulo","descripcion", "lote", "tipoOperacion", "revision","perfecto", "estadoZona1", "estadoZona2", "estadoZona3","estadoZona4", "estadoZona5", "realizado", "verificado", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeUndefined();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
    
    private void cargarCombosZona()
    {
    	this.cargarComboEstados(this.cmbZona1);
    	this.cargarComboEstados(this.cmbZona2);
    	this.cargarComboEstados(this.cmbZona3);
    	this.cargarComboEstados(this.cmbZona4);
    	this.cargarComboEstados(this.cmbZona5);
    }
    private void cargarComboEstados(Combo r_combo)
    {
    	r_combo.addItem("CORRECTO");
    	r_combo.addItem("FALLO");
    	r_combo.addItem("INCORRECTO");
    	r_combo.addItem("REALIZADO REFERENCIA ANTERIOR");
    	r_combo.addItem("SIN PRODUCCION");
    }
    
}