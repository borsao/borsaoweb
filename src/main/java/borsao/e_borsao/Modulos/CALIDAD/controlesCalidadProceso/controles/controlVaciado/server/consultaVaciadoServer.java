package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.modelo.MapeoControlVaciado;

public class consultaVaciadoServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaVaciadoServer  instance;
	
	public consultaVaciadoServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaVaciadoServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaVaciadoServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlVaciado datosOpcionesGlobal(MapeoControlVaciado r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlVaciado mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_vaciado.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_vaciado.idCodigo vac_id, ");
			cadenaSQL.append(" qc_vaciado.hora vac_hora, ");
			
			cadenaSQL.append(" qc_vaciado.revision pre_rev, ");
			cadenaSQL.append(" qc_vaciado.perfecto pre_per, ");
			cadenaSQL.append(" qc_vaciado.articulo pro_art, ");
			cadenaSQL.append(" qc_vaciado.descripcion pro_des, ");
			cadenaSQL.append(" qc_vaciado.lote pro_lot, ");
			cadenaSQL.append(" qc_vaciado.tipoOperacion pro_tip, ");

			cadenaSQL.append(" qc_vaciado.zona1 vac_zo1, ");
			cadenaSQL.append(" qc_vaciado.zona2 vac_zo2, ");
			cadenaSQL.append(" qc_vaciado.zona3 vac_zo3, ");
			cadenaSQL.append(" qc_vaciado.zona4 vac_zo4, ");
			cadenaSQL.append(" qc_vaciado.zona5 vac_zo5, ");
			cadenaSQL.append(" qc_vaciado.realizado vac_rea, ");
			cadenaSQL.append(" qc_vaciado.verificado vac_ver, ");
			cadenaSQL.append(" qc_vaciado.observaciones vac_obs, ");
			cadenaSQL.append(" qc_vaciado.fecha tur_fec ");
			cadenaSQL.append(" FROM " + tabla + " qc_vaciado ");
			
			try
			{
				if (r_mapeo!=null)
				{
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_vaciado.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlVaciado();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("vac_id"));
					mapeo.setHora(rsOpcion.getString("vac_hora"));
					
					mapeo.setRevision(rsOpcion.getString("pre_rev"));
					mapeo.setPerfecto(rsOpcion.getString("pre_per"));
					mapeo.setArticulo(rsOpcion.getString("pro_art"));
					mapeo.setDescripcion(rsOpcion.getString("pro_des"));
					mapeo.setLote(rsOpcion.getString("pro_lot"));
					mapeo.setTipoOperacion(rsOpcion.getString("pro_tip"));

					mapeo.setEstadoZona1(rsOpcion.getString("vac_zo1"));
					mapeo.setEstadoZona2(rsOpcion.getString("vac_zo2"));
					mapeo.setEstadoZona3(rsOpcion.getString("vac_zo3"));
					mapeo.setEstadoZona4(rsOpcion.getString("vac_zo4"));
					mapeo.setEstadoZona5(rsOpcion.getString("vac_zo5"));
					mapeo.setRealizado(rsOpcion.getString("vac_rea"));
					mapeo.setVerificado(rsOpcion.getString("vac_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vac_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlVaciado> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlVaciado> vector = null;
		MapeoControlVaciado mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_vaciado.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_vaciado.idCodigo vac_id, ");
			cadenaSQL.append(" qc_vaciado.hora vac_hora, ");
			
			cadenaSQL.append(" qc_vaciado.revision pre_rev, ");
			cadenaSQL.append(" qc_vaciado.perfecto pre_per, ");
			cadenaSQL.append(" qc_vaciado.articulo pro_art, ");
			cadenaSQL.append(" qc_vaciado.descripcion pro_des, ");
			cadenaSQL.append(" qc_vaciado.lote pro_lot, ");
			cadenaSQL.append(" qc_vaciado.tipoOperacion pro_tip, ");
			
			cadenaSQL.append(" qc_vaciado.zona1 vac_zo1, ");
			cadenaSQL.append(" qc_vaciado.zona2 vac_zo2, ");
			cadenaSQL.append(" qc_vaciado.zona3 vac_zo3, ");
			cadenaSQL.append(" qc_vaciado.zona4 vac_zo4, ");
			cadenaSQL.append(" qc_vaciado.zona5 vac_zo5, ");
			cadenaSQL.append(" qc_vaciado.realizado vac_rea, ");
			cadenaSQL.append(" qc_vaciado.verificado vac_ver, ");
			cadenaSQL.append(" qc_vaciado.observaciones vac_obs, ");
			cadenaSQL.append(" qc_vaciado.fecha tur_fec ");
			cadenaSQL.append(" FROM " + tabla + " qc_vaciado ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}

				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlVaciado>();
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlVaciado();
					mapeo.setIdCodigo(rsOpcion.getInt("vac_id"));
					mapeo.setHora(rsOpcion.getString("vac_hora"));
					
					mapeo.setRevision(rsOpcion.getString("pre_rev"));
					mapeo.setPerfecto(rsOpcion.getString("pre_per"));
					mapeo.setArticulo(rsOpcion.getString("pro_art"));
					mapeo.setDescripcion(rsOpcion.getString("pro_des"));
					mapeo.setLote(rsOpcion.getString("pro_lot"));
					mapeo.setTipoOperacion(rsOpcion.getString("pro_tip"));

					mapeo.setEstadoZona1(rsOpcion.getString("vac_zo1"));
					mapeo.setEstadoZona2(rsOpcion.getString("vac_zo2"));
					mapeo.setEstadoZona3(rsOpcion.getString("vac_zo3"));
					mapeo.setEstadoZona4(rsOpcion.getString("vac_zo4"));
					mapeo.setEstadoZona5(rsOpcion.getString("vac_zo5"));
					mapeo.setRealizado(rsOpcion.getString("vac_rea"));
					mapeo.setVerificado(rsOpcion.getString("vac_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vac_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
					vector.add(mapeo);

				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	
	public String guardarNuevo(MapeoControlVaciado r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
		}
		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + "( ");
			
			cadenaSQL.append(tabla + ".idCodigo , ");
			cadenaSQL.append(tabla + ".hora , ");
			
			cadenaSQL.append(tabla + ".revision , ");
			cadenaSQL.append(tabla + ".perfecto , ");
			cadenaSQL.append(tabla + ".articulo , ");
			cadenaSQL.append(tabla + ".descripcion , ");
			cadenaSQL.append(tabla + ".lote , ");
			cadenaSQL.append(tabla + ".tipoOperacion , ");
			
			cadenaSQL.append(tabla + ".zona1 , ");
			cadenaSQL.append(tabla + ".zona2, ");
			cadenaSQL.append(tabla + ".zona3, ");
			cadenaSQL.append(tabla + ".zona4, ");
			cadenaSQL.append(tabla + ".zona5, ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion, ");
			cadenaSQL.append(tabla + ".fecha , ");
			cadenaSQL.append(tabla + ".verificado ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getRevision()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getRevision());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getPerfecto()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getPerfecto());
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }

		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getTipoOperacion()!=null)
	    	{
	    		preparedStatement.setString(8, r_mapeo.getTipoOperacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(8, null);
	    	}
		    
		    
		    if (r_mapeo.getEstadoZona1()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getEstadoZona1());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getEstadoZona2()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getEstadoZona2());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getEstadoZona3()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getEstadoZona3());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getEstadoZona4()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getEstadoZona4());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getEstadoZona5()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getEstadoZona5());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(16, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(16, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(17, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(17, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(18, "");
		    }
	        preparedStatement.executeUpdate();

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoControlVaciado r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
		}
		try
		{
			
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".hora =?, ");
			
			cadenaSQL.append(tabla + ".revision  =?, ");
			cadenaSQL.append(tabla + ".perfecto  =?, ");
			cadenaSQL.append(tabla + ".articulo  =?, ");
			cadenaSQL.append(tabla + ".descripcion  =?, ");
			cadenaSQL.append(tabla + ".lote  =?, ");
			cadenaSQL.append(tabla + ".tipoOperacion  =?, ");
			
			cadenaSQL.append(tabla + ".zona1 =?, ");
			cadenaSQL.append(tabla + ".zona2 =?, ");
			cadenaSQL.append(tabla + ".zona3 =?, ");
			cadenaSQL.append(tabla + ".zona4 =?, ");
			cadenaSQL.append(tabla + ".zona5 =?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =?, " );
			cadenaSQL.append(tabla + ".fecha =? " );
			
	        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getRevision()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getRevision());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

		    if (r_mapeo.getPerfecto()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getPerfecto());
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getTipoOperacion()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getTipoOperacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
	    	}
		    if (r_mapeo.getEstadoZona1()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getEstadoZona1());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getEstadoZona2()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getEstadoZona2());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getEstadoZona3()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getEstadoZona3());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getEstadoZona4()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getEstadoZona4());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getEstadoZona5()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getEstadoZona5());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(15, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(15, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(16, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(16, null);
	    	}
	    	
		    preparedStatement.setInt(17, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoControlVaciado r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
		}

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_vaciado.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_vaciado.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_vaciado.idCodigo vac_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_vaciado ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("vac_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_vaciado_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_vaciado_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_vaciado_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}