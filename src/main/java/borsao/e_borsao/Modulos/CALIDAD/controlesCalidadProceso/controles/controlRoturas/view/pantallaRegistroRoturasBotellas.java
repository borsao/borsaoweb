package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.modelo.MapeoRoturasBotellas;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.server.consultaControlRoturasServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaRegistroRoturasBotellas extends Ventana
{
	private MapeoRoturasBotellas mapeoControlPreoprativo = null;
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private consultaControlRoturasServer cncs  = null;
	
	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;

	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;
	private boolean hayGridPadre = false;

	private Label lblCumplimentacion = null;
	private Combo cmbZona = null;
	private TextField txtUbicacion = null;
	private TextField txtUnidades = null;
	private TextField txtTiempo = null;
	private TextField txtLimpiador = null;
	private TextField txtVerificador= null;

	private EntradaDatosFecha txtFecha = null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	private TextArea txtObservaciones= null;


	private static String tipoControl = "referencia";
	private PantallaControlesCalidadProceso  app = null;
	private String lineaProduccion = null;
	private boolean creacion = false;	
	

	/*
	 * Entradas datos equipo
	 */
	
	public pantallaRegistroRoturasBotellas(PantallaControlesCalidadProceso  r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaControlRoturasServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombo();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			this.frameCentral.setMargin(true);
			
    		HorizontalLayout linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
	    		this.txtEjercicio=new TextField("Ejercicio");
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
	    		this.txtEjercicio.setWidth("100px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	
	    		this.cmbSemana= new Combo("Semana");    		
	    		this.cmbSemana.setNewItemsAllowed(false);
	    		this.cmbSemana.setNullSelectionAllowed(false);
	    		this.cmbSemana.setEnabled(false);
	    		this.cmbSemana.setWidth("100px");
	    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
	
				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setValue("");
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
			linea2.setSpacing(true);
				
				this.cmbZona = new Combo("Zona");
				this.cmbZona.setNullSelectionAllowed(false);
				this.cmbZona.setNewItemsAllowed(false);
				this.cmbZona.addStyleName(ValoTheme.COMBOBOX_TINY);
				
				txtUbicacion = new TextField("Ubicación");
				txtUbicacion.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtUbicacion.setRequired(true);
				
				txtUnidades= new TextField("Unidades");
				txtUnidades.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtUnidades.setRequired(true);
			
			linea2.addComponent(this.cmbZona);
			linea2.addComponent(this.txtUbicacion);
			linea2.addComponent(this.txtUnidades);
			
    		HorizontalLayout linea3 = new HorizontalLayout();
			linea3.setSpacing(true);
			
				txtTiempo = new TextField("Tiempo (en minutos)");
				txtTiempo.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtTiempo.setRequired(true);
				txtLimpiador = new TextField("Limpiador");
				txtLimpiador.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtLimpiador.setRequired(true);
				
				txtVerificador = new TextField("Verificador");
				txtVerificador.addStyleName(ValoTheme.TEXTFIELD_TINY);
				txtVerificador.setRequired(true);
				
			linea3.addComponent(this.txtTiempo);
			linea3.addComponent(this.txtLimpiador);
			linea3.addComponent(this.txtVerificador);
			
			HorizontalLayout linea4 = new HorizontalLayout();
			linea4.setSpacing(true);
			
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

			
			linea4.addComponent(this.txtObservaciones);
			linea4.addComponent(this.btnLimpiar);
			linea4.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnInsertar);
			linea4.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnEliminar);
			linea4.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);  			
				
    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		
    		this.frameCentral.addComponent(this.frameGrid);
			
    		this.framePie = new HorizontalLayout();
			
			btnBotonCVentana = new Button("Cerrar");
			btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
			btnBotonAVentana = new Button("Guardar");
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			
			framePie.addComponent(btnBotonCVentana);		
			framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}

	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoRoturasBotellas mapeo = new MapeoRoturasBotellas();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
			
			cncs.eliminarRoturas(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}
	}
	
	
	private void guardar()
	{
		String rdo = null;
		MapeoRoturasBotellas mapeo = null;
		consultaControlRoturasServer cecs = consultaControlRoturasServer.getInstance(CurrentUser.get());
		
		if (todoEnOrden())
		{
			mapeo = new MapeoRoturasBotellas();
			
			if (cmbZona.getValue()!=null) mapeo.setZona(cmbZona.getValue().toString());
	    	mapeo.setUbicacion(txtUbicacion.getValue());
	    	mapeo.setUnidades(new Double(txtUnidades.getValue()));
	    	mapeo.setTiempo(new Integer(txtTiempo.getValue()));
	    	mapeo.setLimpiador(txtLimpiador.getValue());
	    	mapeo.setVerificador(txtVerificador.getValue());
	    	mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);
			mapeo.setFecha(txtFecha.getValue());
	    	
			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cecs.guardarRoturas(mapeo);
				if (rdo==null)
				{
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
				rdo = cecs.guardarCambiosRoturas(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}

	}
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoRoturasBotellas> vector=null;
    	
    	MapeoRoturasBotellas mapeoPreop = new MapeoRoturasBotellas();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion );
    	
    	vector=this.cncs.datosRoturasBotellas(mapeoPreop, this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());
    	
    	if (vector==null || vector.isEmpty())
    	{
    		MapeoRoturasBotellas mapeo = this.generarMapeoVacio(mapeoPreop);
    		String rdo = this.cncs.guardarRoturas(mapeo);
    		
    		if (rdo!=null)
    		{
    			Notificaciones.getInstance().mensajeError("No se pudo generar el registro");
    		}
    		else
    		{
    			vector=this.cncs.datosRoturasBotellas(mapeoPreop, this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());
    		}
    	}
    	
    	this.generarGrid(vector);
	}

	private MapeoRoturasBotellas generarMapeoVacio(MapeoRoturasBotellas r_mapeo)
	{
		r_mapeo.setTiempo(0);
		r_mapeo.setUnidades(new Double(0));
		r_mapeo.setLimpiador("OPERARIO");
		r_mapeo.setVerificador("JEFE TURNO");
		r_mapeo.setZona("EMBOTELLADORA");
		r_mapeo.setFecha(RutinasFechas.conversionDeString(this.mapeoProduccion.getFecha()));
		return r_mapeo;
	}
	
	private void llenarEntradasDatos(MapeoRoturasBotellas r_mapeo)
	{
		this.mapeoControlPreoprativo = new MapeoRoturasBotellas();
		this.mapeoControlPreoprativo.setIdCodigo(r_mapeo.getIdCodigo());
		setCreacion(false);
		
		this.cmbZona.setValue(r_mapeo.getZona());
		this.txtUbicacion.setValue(r_mapeo.getUbicacion());
		this.txtUnidades.setValue(r_mapeo.getUnidades().toString());
		this.txtTiempo.setValue(r_mapeo.getTiempo().toString());
		this.txtLimpiador.setValue(r_mapeo.getLimpiador());
		this.txtVerificador.setValue(r_mapeo.getVerificador());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}

    private void activarEntradasDatos(boolean r_activar)
    {
    	cmbZona.setEnabled(r_activar);
    	txtUbicacion.setEnabled(r_activar);
    	txtUnidades.setEnabled(r_activar);
    	txtTiempo.setEnabled(r_activar);
    	txtLimpiador.setEnabled(r_activar);
    	txtVerificador.setEnabled(r_activar);
    }
    
    private void generarGrid(ArrayList<MapeoRoturasBotellas> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Control Rotura Cristales ");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoRoturasBotellas> container = new BeanItemContainer<MapeoRoturasBotellas>(MapeoRoturasBotellas.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("zona").setHeaderCaption("Zona");
			    	getColumn("zona").setWidth(new Double(150));
			    	getColumn("ubicacion").setHeaderCaption("Ubicacion");
			    	getColumn("ubicacion").setWidth(new Double(220));
			    	getColumn("unidades").setHeaderCaption("Unidades");
			    	getColumn("unidades").setWidth(new Double(220));
			    	getColumn("tiempo").setHeaderCaption("Tiempo");
			    	getColumn("tiempo").setWidth(new Double(220));
			    	getColumn("limpiador").setHeaderCaption("Limpiador");
			    	getColumn("limpiador").setWidth(new Double(155));
			    	getColumn("verificador").setHeaderCaption("Verificador");
			    	getColumn("verificador").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));
			    	getColumn("observaciones").setHeaderCaption("Verificado");
			    	getColumn("observaciones").setWidth(new Double(350));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					setColumnOrder("fecha", "zona", "ubicacion","unidades","tiempo", "limpiador","verificador", "verificado", "observaciones");
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}

	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.cmbZona.setValue("");
		this.txtUbicacion.setValue("");
		this.txtUnidades.setValue("");
		this.txtTiempo.setValue("");
		this.txtLimpiador.setValue("");
		this.txtVerificador.setValue("");
		this.txtObservaciones.setValue("");
	}

    private boolean todoEnOrden()
    {
    	if ((this.cmbZona.getValue()==null || this.cmbZona.getValue().toString().length()==0) && (this.txtUnidades.getValue()!=null && !this.txtUnidades.getValue().contentEquals("0")))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Zona");
    		cmbZona.focus();
    		return false;
    	}
    	if ((this.txtLimpiador.getValue()==null || this.txtLimpiador.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el limpìado por");
    		txtLimpiador.focus();
    		return false;
    	}
    	if ((this.txtVerificador.getValue()==null || this.txtVerificador.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el verificado por");
    		txtVerificador.focus();
    		return false;
    	}
    	if ((this.txtUnidades.getValue()==null || this.txtUnidades.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar las unidades ");
    		txtUnidades.focus();
    		return false;
    	}
    	if ((this.txtTiempo.getValue()==null || this.txtTiempo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tiempo");
    		txtTiempo.focus();
    		return false;
    	}
    	return true;
    }
    
    private void cargarCombo()
    {
		cmbZona.addItem("Zona 1");
		cmbZona.addItem("Zona 2");
		cmbZona.addItem("Zona 3");
		cmbZona.addItem("Zona 4");
		cmbZona.addItem("Zona 5");
		cmbZona.addItem("EMBOTELLADORA");
		
		this.cargarComboSemana();
    }
    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoRoturasBotellas) r_fila)!=null)
    	{
    		MapeoRoturasBotellas mapeoGrifo = (MapeoRoturasBotellas) r_fila;
    		if (mapeoGrifo!=null) this.llenarEntradasDatos(mapeoGrifo);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

}