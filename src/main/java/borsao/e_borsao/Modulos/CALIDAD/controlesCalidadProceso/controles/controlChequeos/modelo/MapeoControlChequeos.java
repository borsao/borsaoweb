package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlChequeos.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlChequeos extends MapeoGlobal
{
	private Integer idProgramacion;
	private Integer numeroDesde;
	private Integer numeroHasta;
	
	private String linea;
	private String hora;
	private String serie;
	private String realizado;
	private String verificado;
	private String observaciones;
	
	private Date fecha;
	
	public MapeoControlChequeos()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}


	public Integer getNumeroDesde() {
		return numeroDesde;
	}

	public void setNumeroDesde(Integer numeroDesde) {
		this.numeroDesde = numeroDesde;
	}

	public Integer getNumeroHasta() {
		return numeroHasta;
	}

	public void setNumeroHasta(Integer numeroHasta) {
		this.numeroHasta = numeroHasta;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}