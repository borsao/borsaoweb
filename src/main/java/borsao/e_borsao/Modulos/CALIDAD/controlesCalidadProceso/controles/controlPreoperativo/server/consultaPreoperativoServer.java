package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.modelo.MapeoControlPreoperativo;

public class consultaPreoperativoServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPreoperativoServer  instance;
	
	public consultaPreoperativoServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPreoperativoServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPreoperativoServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlPreoperativo datosOpcionesGlobal(MapeoControlPreoperativo r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlPreoperativo mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id, ");
			cadenaSQL.append(" qc_preop.hora pre_hora, ");
			cadenaSQL.append(" qc_preop.sala_ok pre_sok, ");
			cadenaSQL.append(" qc_preop.maq_limpia pre_mli, ");
			cadenaSQL.append(" qc_preop.ausencia_cuerpos pre_aus, ");
			cadenaSQL.append(" qc_preop.maq_ok pre_mok, ");
			cadenaSQL.append(" qc_preop.linea_ok pre_lok, ");
			if (r_mapeo.getLinea().contentEquals("BIB")) cadenaSQL.append(" qc_preop.carbonico pre_car, ");
			cadenaSQL.append(" qc_preop.realizado pre_rea, ");
			cadenaSQL.append(" qc_preop.verificado pre_ver, ");
			cadenaSQL.append(" qc_preop.fecha pre_fec, ");
			cadenaSQL.append(" qc_preop.observaciones pre_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_preop.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPreoperativo();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
					mapeo.setSala_ok(rsOpcion.getString("pre_sok"));
					mapeo.setMaquina_limpia(rsOpcion.getString("pre_mli"));
					mapeo.setAusencia_cuerpos(rsOpcion.getString("pre_aus"));
					mapeo.setMaquina_ok(rsOpcion.getString("pre_mok"));
					mapeo.setLinea_ok(rsOpcion.getString("pre_lok"));
					mapeo.setRealizado(rsOpcion.getString("pre_rea"));
					mapeo.setVerificado(rsOpcion.getString("pre_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
					if (r_mapeo.getLinea().contentEquals("BIB")) mapeo.setCarbonico(rsOpcion.getString("pre_car")); else mapeo.setCarbonico(""); 
					
					mapeo.setFecha(rsOpcion.getDate("pre_fec"));
					mapeo.setHora(rsOpcion.getString("pre_hora"));
					mapeo.setIdTurno(rsOpcion.getInt("pre_idt"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return mapeo;
	}
	
	public ArrayList<MapeoControlPreoperativo> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlPreoperativo> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlPreoperativo mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id, ");
			cadenaSQL.append(" qc_preop.hora pre_hora, ");
			cadenaSQL.append(" qc_preop.sala_ok pre_sok, ");
			cadenaSQL.append(" qc_preop.maq_limpia pre_mli, ");
			cadenaSQL.append(" qc_preop.ausencia_cuerpos pre_aus, ");
			cadenaSQL.append(" qc_preop.maq_ok pre_mok, ");
			cadenaSQL.append(" qc_preop.linea_ok pre_lok, ");
			if (r_linea.contentEquals("BIB")) cadenaSQL.append(" qc_preop.carbonico pre_car, ");
			cadenaSQL.append(" qc_preop.realizado pre_rea, ");
			cadenaSQL.append(" qc_preop.verificado pre_ver, ");
			cadenaSQL.append(" qc_preop.fecha pre_fec, ");
			cadenaSQL.append(" qc_preop.observaciones pre_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlPreoperativo>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPreoperativo();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
					mapeo.setSala_ok(rsOpcion.getString("pre_sok"));
					mapeo.setMaquina_limpia(rsOpcion.getString("pre_mli"));
					mapeo.setAusencia_cuerpos(rsOpcion.getString("pre_aus"));
					mapeo.setMaquina_ok(rsOpcion.getString("pre_mok"));
					mapeo.setLinea_ok(rsOpcion.getString("pre_lok"));
					if (r_linea.contentEquals("BIB")) mapeo.setCarbonico(rsOpcion.getString("pre_car")); else mapeo.setCarbonico(""); 
					mapeo.setRealizado(rsOpcion.getString("pre_rea"));
					mapeo.setVerificado(rsOpcion.getString("pre_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
					mapeo.setFecha(rsOpcion.getDate("pre_fec"));
					mapeo.setHora(rsOpcion.getString("pre_hora"));
					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	
	public String guardarNuevoPreoperativo(MapeoControlPreoperativo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
		}

		try
		{
			
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".sala_ok , ");
				cadenaSQL.append(tabla + ".maq_limpia , ");
				cadenaSQL.append(tabla + ".ausencia_cuerpos , ");
				cadenaSQL.append(tabla + ".maq_ok , ");
				cadenaSQL.append(tabla + ".linea_ok , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idTurno , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".carbonico , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
				
			}
			else
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".sala_ok , ");
				cadenaSQL.append(tabla + ".maq_limpia , ");
				cadenaSQL.append(tabla + ".ausencia_cuerpos , ");
				cadenaSQL.append(tabla + ".maq_ok , ");
				cadenaSQL.append(tabla + ".linea_ok , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idTurno , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSala_ok()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSala_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getMaquina_limpia()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getMaquina_limpia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getAusencia_cuerpos()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getAusencia_cuerpos());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getMaquina_ok()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getMaquina_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLinea_ok()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getLinea_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    preparedStatement.setInt(11, 0);
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getLinea().contentEquals("BIB"))
		    {
		    	if (r_mapeo.getCarbonico()!=null)
			    {
			    	preparedStatement.setString(13, r_mapeo.getCarbonico());
			    }
			    else
			    {
			    	preparedStatement.setString(13, null);
			    }
		    	
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(14, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(14, null);
		    	}
		    }
		    else
		    {
			    if (r_mapeo.getFecha()!=null)
			    {
			    	preparedStatement.setString(13, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
			    }
			    else
			    {
			    	preparedStatement.setString(13, null);
			    }
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	
	public String guardarCambiosPreoperativo(MapeoControlPreoperativo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
		}
		try
		{
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".sala_ok =?, ");
				cadenaSQL.append(tabla + ".maq_limpia=?, ");
				cadenaSQL.append(tabla + ".ausencia_cuerpos =?, ");
				cadenaSQL.append(tabla + ".maq_ok =?, ");
				cadenaSQL.append(tabla + ".linea_ok =?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idTurno =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =?, ");
				cadenaSQL.append(tabla + ".carbonico =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
			else
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".sala_ok =?, ");
				cadenaSQL.append(tabla + ".maq_limpia=?, ");
				cadenaSQL.append(tabla + ".ausencia_cuerpos =?, ");
				cadenaSQL.append(tabla + ".maq_ok =?, ");
				cadenaSQL.append(tabla + ".linea_ok =?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idTurno =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getSala_ok()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getSala_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getMaquina_limpia()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getMaquina_limpia());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getAusencia_cuerpos()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getAusencia_cuerpos());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getMaquina_ok()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getMaquina_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getLinea_ok()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getLinea_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    preparedStatement.setInt(10, 0);
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    
		    if (r_mapeo.getLinea().contentEquals("BIB"))
		    {
			    if (r_mapeo.getCarbonico()!=null)
			    {
			    	preparedStatement.setString(12, r_mapeo.getCarbonico());
			    }
			    else
			    {
			    	preparedStatement.setString(12, null);
			    }
		    	
		    	preparedStatement.setInt(13, r_mapeo.getIdCodigo());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, r_mapeo.getIdCodigo());
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlPreoperativo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_preop ");
	     	
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	
	     	cadenaSQL.append(" where prd_prog." + campo + " = " + r_idProgramacion);
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pre_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }     
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
		
			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_preop_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_preop_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_preop_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
}