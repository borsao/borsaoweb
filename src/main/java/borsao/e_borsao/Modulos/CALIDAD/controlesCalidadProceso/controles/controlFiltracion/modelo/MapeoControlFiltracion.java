package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlFiltracion extends MapeoGlobal
{
	private Integer idTurno;
	private Integer idProgramacion;
	private String hora;
	private String estadoIntegridad;
	private String estadoConductividad;
	private String nitrogeno;	
	private String ph;	
	private String peracetico;	
	private String tipoLimpieza;	
	private String realizado;
	private String observaciones;
	private String linea;
	private String verificado;
	
	private Double valorPh;
	private Double valorPeracetico;
	private Double valorIntegridad;
	private Double valorConductividad;
	
	private Date fecha =null;
	
	public MapeoControlFiltracion()
	{
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstadoIntegridad() {
		return estadoIntegridad;
	}

	public void setEstadoIntegridad(String estadoIntegridad) {
		this.estadoIntegridad = estadoIntegridad;
	}

	public String getEstadoConductividad() {
		return estadoConductividad;
	}

	public void setEstadoConductividad(String estadoConductividad) {
		this.estadoConductividad = estadoConductividad;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Double getValorIntegridad() {
		return valorIntegridad;
	}

	public void setValorIntegridad(Double valorIntegridad) {
		this.valorIntegridad = valorIntegridad;
	}

	public Double getValorConductividad() {
		return valorConductividad;
	}

	public void setValorConductividad(Double valorConductividad) {
		this.valorConductividad = valorConductividad;
	}

	public String getTipoLimpieza() {
		return tipoLimpieza;
	}

	public void setTipoLimpieza(String tipoLimpieza) {
		this.tipoLimpieza = tipoLimpieza;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getNitrogeno() {
		return nitrogeno;
	}

	public void setNitrogeno(String nitrogeno) {
		this.nitrogeno = nitrogeno;
	}

	public String getPh() {
		return ph;
	}

	public void setPh(String ph) {
		this.ph = ph;
	}

	public String getPeracetico() {
		return peracetico;
	}

	public void setPeracetico(String peracetico) {
		this.peracetico = peracetico;
	}

	public Double getValorPh() {
		return valorPh;
	}

	public void setValorPh(Double valorPh) {
		this.valorPh = valorPh;
	}

	public Double getValorPeracetico() {
		return valorPeracetico;
	}

	public void setValorPeracetico(Double valorPeracetico) {
		this.valorPeracetico = valorPeracetico;
	}


	
}