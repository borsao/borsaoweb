package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlLineasPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.server.consultaPaletizarServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaPaletizar extends Ventana
{
	public MapeoControlesProcesoProductivo mapeoProduccion = null;
	public MapeoControlLineasPaletizar mapeoControlLineasPaletizar = null;
	private MapeoControlPaletizar mapeoControlPaletizar = null;
	private consultaPaletizarServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout frameGridLineas = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private Panel panelGridLineas = null;
	private GridPropio gridDatos = null;
	private GridPropio gridDatosLineas = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "referencia";

	public String lineaProduccion = null;
	public HashMap<String, String> hashCopia = null;
	private Label lblCumplimentacion = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaPaletizar(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(false);
		
    	this.cncs = new consultaPaletizarServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("100%");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
//			this.frameCentral.setSpacing(true);
//			this.frameCentral.setHeightUndefined();
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				if (this.lineaProduccion.contentEquals("Embotelladora"))
	    		{
					this.lblCumplimentacion.setValue("Frecuencia: 36 botellas cada hora ");
	    		}
				else if (this.lineaProduccion.contentEquals("Envasadora"))
	    		{
					this.lblCumplimentacion.setValue("Frecuencia: 2 Garrafas al comienzo, 2 cambio referencia, 2 tras almuerzo y 2 al final ");
	    		}
				else
				{
					this.lblCumplimentacion.setValue("Frecuencia: 4 BIB Cada 2 ");
				}
				this.lblCumplimentacion.setWidth("500px");
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");

				this.txtRealizado= new TextField();
				this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");

				this.txtObservaciones= new TextArea();
				this.txtObservaciones.setCaption("Observaciones");
				this.txtObservaciones.setWidth("300");

				linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.txtRealizado);
    			linea2.addComponent(this.txtObservaciones);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
				
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnLimpiar.setEnabled(true);
    			
    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);
    			
    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		this.frameGrid.setHeight("175px");

    		this.frameGridLineas = new HorizontalLayout();
    		this.frameGridLineas.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		
			this.framePie = new HorizontalLayout();
			this.framePie.setHeight("3%");
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.TOP_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
			
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
//		principal.setExpandRatio(this.frameCentral, 1);
//		principal.setExpandRatio(this.framePie, 2);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlPaletizar mapeo = new MapeoControlPaletizar();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlPaletizar.getIdCodigo());
			
			cncs.eliminar(mapeo);
			
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
			}
			this.hashCopia=null;
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlPaletizar mapeo = new MapeoControlPaletizar();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoPaletizar(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
						frameGrid.removeComponent(panelGrid);
					}
					panelGrid=null;
					if (gridDatosLineas!=null)
					{
						gridDatosLineas.removeAllColumns();
						gridDatosLineas=null;
						frameGridLineas.removeComponent(panelGridLineas);
					}
					panelGridLineas=null;
					llenarRegistros();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPaletizar.getIdCodigo());
				rdo = cncs.guardarCambiosPaletizar(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					panelGridLineas=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlPaletizar> vector=null;
    	MapeoControlPaletizar mapeoPreop = new MapeoControlPaletizar();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	vector = this.cncs.datosOpcionesGlobal(mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlPaletizar r_mapeo)
	{
		this.mapeoControlPaletizar= new MapeoControlPaletizar();
		this.mapeoControlPaletizar.setIdCodigo(r_mapeo.getIdCodigo());
		
		setCreacion(false);
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlPaletizar) r_fila)!=null)
    	{
    		MapeoControlPaletizar mapeoPaletizar = (MapeoControlPaletizar) r_fila;
    		if (mapeoPaletizar!=null)
    		{
    			this.llenarEntradasDatos(mapeoPaletizar);
    			
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					frameCentral.removeComponent(frameGridLineas);
				}
				panelGridLineas=null;

    			this.frameGridLineas.addComponent(this.generarGridLineas(mapeoPaletizar.getIdCodigo()));
    			this.frameCentral.addComponent(this.frameGridLineas);
    			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
    		}
    		mapeoPaletizar.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
		}
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);
    }

    private void generarGrid(ArrayList<MapeoControlPaletizar> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */

    	if (r_vector!= null && !r_vector.isEmpty())
		{
			this.frameGrid.addComponent(this.generarGridCAb(r_vector));
			this.frameCentral.addComponent(this.frameGrid);
			this.frameCentral.setExpandRatio(this.frameGrid, 2);
			this.gridDatos.select(this.gridDatos.getContainerDataSource().getIdByIndex(r_vector.size()-1));
			this.filaSeleccionada(this.gridDatos.getSelectedRow());
		}
    	else
    	{
    		this.limpiarCampos();
    	}
	}
    
    private Panel generarGridCAb(ArrayList<MapeoControlPaletizar> r_vector)
    {
    	panelGrid= new Panel("Control Antes del Paletizado");
    	panelGrid.setSizeFull(); // Shrink to fit content
    	
    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		BeanItemContainer<MapeoControlPaletizar> container = new BeanItemContainer<MapeoControlPaletizar>(MapeoControlPaletizar.class);
		this.gridDatos=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
		    	getColumn("fecha").setHeaderCaption("Fecha");
		    	getColumn("fecha").setWidth(new Double(120));
		    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
		    	getColumn("hora").setHeaderCaption("Hora");
		    	getColumn("hora").setWidth(new Double(100));
		    	getColumn("realizado").setHeaderCaption("Realizado");
		    	getColumn("realizado").setWidth(new Double(155));
		    	getColumn("verificado").setHeaderCaption("Verificado");
		    	getColumn("verificado").setWidth(new Double(155));

				getColumn("idCodigo").setHidden(true);
				getColumn("idProgramacion").setHidden(true);
				getColumn("linea").setHidden(true);
				getColumn("fecha").setHidden(true);
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				setColumnOrder("fecha", "hora", "realizado", "verificado", "observaciones");
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {
				addItemClickListener(new ItemClickEvent.ItemClickListener() 
		    	{
		            public void itemClick(ItemClickEvent event) 
		            {
		        		filaSeleccionada(event.getItemId());
		    		}
		        });

			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
			}
		};
		this.gridDatos.setContainerDataSource(container);

		this.gridDatos.getContainer().removeAllItems();
		this.gridDatos.getContainer().addAll(r_vector);
		
		this.gridDatos.addStyleName("minigrid");
		this.gridDatos.setEditorEnabled(false);
		this.gridDatos.setConFiltro(false);
		this.gridDatos.setSeleccion(SelectionMode.SINGLE);
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
    	
		this.gridDatos.establecerTitulosColumnas();
		this.gridDatos.establecerOrdenPresentacionColumnas();
		this.gridDatos.cargarListeners();
		
		panelGrid.setContent(gridDatos);
    	
    	return panelGrid;
    }
    private Panel generarGridLineas(Integer r_id)
    {
    	
    	pantallaPaletizar app = this;
    	MapeoControlPaletizar mapeoPreop = new MapeoControlPaletizar();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	ArrayList<MapeoControlLineasPaletizar> vectorLineas = this.cncs.datosOpcionesGlobalLineas(r_id, mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	panelGridLineas= new Panel("Control Cajas ");
    	panelGridLineas.setSizeFull(); // Shrink to fit content

    	BeanItemContainer<MapeoControlLineasPaletizar> container = new BeanItemContainer<MapeoControlLineasPaletizar>(MapeoControlLineasPaletizar.class);
		this.gridDatosLineas=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
				
		    	getColumn("acc").setHeaderCaption("");
		    	getColumn("acc").setSortable(true);
		    	getColumn("acc").setWidth(new Double(50));

				getColumn("material").setHeaderCaption("Control");
				getColumn("material").setWidth(new Double(250));
				getColumn("caja1").setHeaderCaption("Caja1");
				getColumn("caja1").setWidth(new Double(140));
				getColumn("caja2").setHeaderCaption("Caja2");
				getColumn("caja2").setWidth(new Double(140));
				if (!lineaProduccion.contentEquals("Envasadora"))
				{
					getColumn("caja3").setHeaderCaption("Caja3");
					getColumn("caja3").setWidth(new Double(140));
					getColumn("caja4").setHeaderCaption("Caja4");
					getColumn("caja4").setWidth(new Double(140));
				}
				if (lineaProduccion.contentEquals("Embotelladora"))
				{
					getColumn("caja5").setHeaderCaption("Caja5");
					getColumn("caja5").setWidth(new Double(140));
					getColumn("caja6").setHeaderCaption("Caja6");
					getColumn("caja6").setWidth(new Double(140));
				}
				getColumn("idCodigo").setHidden(true);
				getColumn("idPaletizar").setHidden(true);
				getColumn("acc").setHidden(true);
				if (lineaProduccion.contentEquals("Envasadora"))
				{
					getColumn("caja3").setHidden(true);
					getColumn("caja4").setHidden(true);
					getColumn("caja5").setHidden(true);
					getColumn("caja6").setHidden(true);
					
				}
				if (lineaProduccion.contentEquals("BIB"))
				{
					getColumn("caja5").setHidden(true);
					getColumn("caja6").setHidden(true);
					
				}
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				
				if (lineaProduccion.contentEquals("Embotelladora"))
				{
					setColumnOrder("acc", "material", "caja1","caja2", "caja3", "caja4", "caja5", "caja6");
				}
				else if (lineaProduccion.contentEquals("BIB"))
				{
					setColumnOrder("acc", "material", "caja1","caja2", "caja3", "caja4");
				}
				else 
				{
					setColumnOrder("acc", "material", "caja1","caja2");
				}
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {
				addItemClickListener(new ItemClickEvent.ItemClickListener() 
				{
					public void itemClick(ItemClickEvent event) 
					{
						if (event.getPropertyId()!=null)
		            	{
							mapeoControlLineasPaletizar = (MapeoControlLineasPaletizar) event.getItemId();
			            	
		            		if (!event.getPropertyId().toString().equals("material"))
			            	{
			        			try
			        			{
			        				/*
			        				 * ME falta coger el nombre del articulo de materia prima correspondiente y pasarlo en 
			        				 * el campo descripcion del mapeo
			        				 * 
			        				 * asi se evitan escribirlo todas las veces
			        				 */
			        				if (lineaProduccion.contentEquals("Envasadora"))
			        				{
			        					pantallaActualizacionLineasPaletizarEnv vt = new pantallaActualizacionLineasPaletizarEnv(app, mapeoControlLineasPaletizar.getIdPaletizar(), event.getPropertyId().toString(), "", "Datos relativos a " + event.getPropertyId().toString());
			        					app.getUI().addWindow(vt);
			        				}
			        				else 
			        				{
			        					pantallaActualizacionLineasPaletizar vt = new pantallaActualizacionLineasPaletizar(app, mapeoControlLineasPaletizar.getIdPaletizar(), event.getPropertyId().toString(), "", "Datos relativos a " + event.getPropertyId().toString());
			        					app.getUI().addWindow(vt);
			        				}
			        			}
			        			catch (IndexOutOfBoundsException ex)
			        			{
			        				
			        			}
			            	}            	
			            	else
			            	{
			            	}
			            }
					}
				});
				
			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
		    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
		            
		            @Override
		            public String getStyle(Grid.CellReference cellReference) {
		            	if ("acc".equals(cellReference.getPropertyId())) 
		            	{            		
		            		return "cell-nativebuttonProgramacion";
		            	}
		            	else
		            	{
		            		return "cell-normal";
		            	}
		            }
		        });

			}
		};
		this.gridDatosLineas.setContainerDataSource(container);
		
		this.gridDatosLineas.getContainer().removeAllItems();
		this.gridDatosLineas.getContainer().addAll(vectorLineas);
		
		
		this.gridDatosLineas.addStyleName("smallgrid");
		this.gridDatosLineas.setEditorEnabled(false);
		this.gridDatosLineas.setConFiltro(false);
		this.gridDatosLineas.setSeleccion(SelectionMode.SINGLE);
		this.gridDatosLineas.setWidth("100%");
		this.gridDatosLineas.setHeight("100%");
		
		this.gridDatosLineas.establecerTitulosColumnas();
		this.gridDatosLineas.establecerOrdenPresentacionColumnas();
		this.gridDatosLineas.cargarListeners();
		this.gridDatosLineas.asignarEstilos();
		panelGridLineas.setContent(gridDatosLineas);
		
		return panelGridLineas;
    }
    
	public void actualizarDatosLineaPaletizar(Integer r_id, HashMap<String, String> r_valores)
	{
		consultaPaletizarServer cps = consultaPaletizarServer.getInstance(CurrentUser.get());
		String rdo = cps.guardarCambiosLineasPaletizar(this.lineaProduccion,r_id, r_valores);
		
		if (rdo != null)
		{
			Notificaciones.getInstance().mensajeError("Error al actualizar los datos del control antes de paletizar");
		}
		else
		{
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
				this.frameCentral.removeComponent(this.frameGridLineas);
			}

			this.frameGridLineas.addComponent(this.generarGridLineas(mapeoControlLineasPaletizar.getIdPaletizar()));
			this.frameCentral.addComponent(this.frameGridLineas);
			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
		}
	}
}