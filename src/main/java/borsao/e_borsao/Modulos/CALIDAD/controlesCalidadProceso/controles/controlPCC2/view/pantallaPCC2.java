package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.server.consultaGrifosServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.modelo.MapeoControlPCC2;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.server.consultaPCC2Server;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaPCC2 extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlPCC2 mapeoControlPCC2 = null;
	private consultaPCC2Server cncs  = null;
	private consultaGrifosServer cncsg  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	private Label lblCumplimentacion= null;
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtPresion = null;
	private TextField txtControlRoturas = null;
//	private TextField txtControlGrifos = null;
	private Combo txtResultado = null;
	private TextField txtValidado= null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private TextField txtGrifos= null;
	private CheckBox chkCuello = null;
	private CheckBox chkFiltro= null;

	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app = null;
	private static String tipoControl = "global";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaPCC2(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaPCC2Server(CurrentUser.get());
    	this.cncsg = new consultaGrifosServer(CurrentUser.get());
    	/*
    	 * generar registros por horas la primera vez
    	 */

    	this.generarRegistros();
    	
    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
			HorizontalLayout linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new Combo("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setEnabled(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

			this.txtFecha = new EntradaDatosFecha();
			this.txtFecha.setCaption("Fecha");
			this.txtFecha.setEnabled(false);
			this.txtFecha.setValue(this.mapeoProduccion.getFecha());
			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtFecha.setWidth("150px");
			
			this.lblCumplimentacion = new Label();
			this.lblCumplimentacion.setValue("Realizado por personal embotelladora, Validado por el jefe de turno o personal de calidad.");
			this.lblCumplimentacion.addStyleName("lblTituloCalidad");

			linea1.addComponent(this.txtEjercicio);
			linea1.addComponent(this.cmbSemana);
			linea1.addComponent(this.txtFecha);
			linea1.addComponent(this.lblCumplimentacion);
			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);

		HorizontalLayout linea2 = new HorizontalLayout();
			linea2.setSpacing(true);

			this.txtHoras = new TextField();
			this.txtHoras.setCaption("Hora");
			this.txtHoras.setWidth("120px");
			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtHoras.addStyleName("rightAligned");
			
			this.txtPresion= new TextField();
			this.txtPresion.setCaption("Presión del Manómetro (2,5 - 3,5 bar)");
			this.txtPresion.setWidth("150px");
			this.txtPresion.setEnabled(false);
			this.txtPresion.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			this.txtControlRoturas= new TextField();
			this.txtControlRoturas.setCaption("Control Roturas Lavadora");
			this.txtControlRoturas.setWidth("150px");
			this.txtControlRoturas.setEnabled(false);
			this.txtControlRoturas.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			linea2.addComponent(this.txtHoras);
			linea2.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
			linea2.addComponent(this.txtPresion);
			linea2.addComponent(this.txtControlRoturas);

		HorizontalLayout linea3 = new HorizontalLayout();
		linea3.setSpacing(true);
		
			this.txtGrifos = new TextField();
			this.txtGrifos.setCaption("Grifo Nº");
			this.txtGrifos.setWidth("120px");
			this.txtGrifos.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			this.chkCuello= new CheckBox();
			this.chkCuello.setCaption("Ausencia daños cuello botella?");
	
			this.chkFiltro= new CheckBox();
			this.chkFiltro.setCaption("Ausencia Cuerops Extraños en el filtro?");
			
			this.txtResultado= new Combo();
			this.txtResultado.setCaption("Parada Presion");
			this.txtResultado.setWidth("150px");
			this.txtResultado.setEnabled(false);
			this.txtResultado.addStyleName(ValoTheme.COMBOBOX_TINY);
			this.txtResultado.setNullSelectionAllowed(false);
			this.txtResultado.setNewItemsAllowed(false);
			this.txtResultado.addItem("-");
			this.txtResultado.addItem("OK");
			this.txtResultado.addItem("NOK");
			this.txtResultado.setValue("-");
			
			linea3.addComponent(this.txtGrifos);
			linea3.addComponent(this.chkCuello);
			linea3.setComponentAlignment(this.chkCuello, Alignment.BOTTOM_LEFT);
			linea3.addComponent(this.chkFiltro);
			linea3.setComponentAlignment(this.chkFiltro, Alignment.BOTTOM_LEFT);
			linea3.addComponent(this.txtResultado);

		HorizontalLayout linea4 = new HorizontalLayout();
			linea4.setSpacing(true);
			
			this.txtRealizado= new TextField();
			this.txtRealizado.setCaption("Realizado Por");
			this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtRealizado.setWidth("180px");
			
			this.txtValidado= new TextField();
			this.txtValidado.setCaption("Validado Por");
			this.txtValidado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtValidado.setWidth("180px");
			
			this.txtObservaciones= new TextArea();
			this.txtObservaciones.setCaption("Observaciones");
			this.txtObservaciones.setWidth("300");
			
			btnLimpiar= new Button("Nuevo");
			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

			btnInsertar= new Button("Guardar");
			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

			btnEliminar= new Button("Eliminar");
			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
			btnEliminar.setEnabled(false);

			linea4.addComponent(this.txtRealizado);
			linea4.addComponent(this.txtValidado);
//			linea4.addComponent(this.txtVerificado);
			linea4.addComponent(this.txtObservaciones);
			linea4.addComponent(this.btnLimpiar);
			linea4.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnInsertar);
			linea4.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnEliminar);
			linea4.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
			MapeoControlPCC2 mapeo = new MapeoControlPCC2();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlPCC2.getIdCodigo());
			
			cncs.eliminar(mapeo);
			
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlPCC2 mapeo = new MapeoControlPCC2();

			mapeo.setManometro(this.txtPresion.getValue().toString());
			mapeo.setControlRoturas(this.txtControlRoturas.getValue().toString());
			mapeo.setResultado(this.txtResultado.getValue().toString());
			
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setValidado(this.txtValidado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setVerificado(null);
			mapeo.setLinea(lineaProduccion);
			
			if (chkFiltro.getValue()) mapeo.setFiltro("S"); else mapeo.setFiltro("N");
			if (chkCuello.getValue()) mapeo.setCuello("S"); else mapeo.setCuello("N");
			
			mapeo.setGrifo(this.txtGrifos.getValue());


			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPCC2.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPCC2.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlPCC2> vector=null;

    	MapeoControlPCC2 mapeoPreop = new MapeoControlPCC2();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlPCC2 r_mapeo)
	{
		setCreacion(false);

		this.mapeoControlPCC2 = new MapeoControlPCC2();
		
		this.mapeoControlPCC2.setIdCodigo(r_mapeo.getIdCodigo());
		this.txtPresion.setValue(r_mapeo.getManometro());
		this.txtControlRoturas.setValue(r_mapeo.getControlRoturas());
		this.txtResultado.setValue(r_mapeo.getResultado());
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtValidado.setValue(r_mapeo.getValidado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		
		setCreacion(false);
		if (r_mapeo.getFiltro().contentEquals("S")) chkFiltro.setValue(true);
		if (r_mapeo.getCuello().contentEquals("S")) chkCuello.setValue(true);
		
		this.txtGrifos.setValue(r_mapeo.getGrifo());

	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtPresion.setEnabled(r_activar);
    	txtControlRoturas.setEnabled(r_activar);
    	txtResultado.setEnabled(r_activar);
    	txtValidado.setEnabled(r_activar);
    	
    	txtGrifos.setEnabled(r_activar);
    	
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtPresion.setValue(null);
		this.txtControlRoturas.setValue(null);
		this.txtResultado.setValue("-");

		this.chkFiltro.setValue(false);
		this.chkCuello.setValue(false);
		this.txtGrifos.setValue("");

		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtValidado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
//    	if ((this.txtGrifos.getValue()==null || this.txtGrifos.getValue().toString().length()==0))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el grifo revisado.");
//    		return false;
//    	}
    	return true;

    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlPCC2) r_fila)!=null)
    	{
    		MapeoControlPCC2 mapeoPreop = (MapeoControlPCC2) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlPCC2> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("PCC2 LINEA");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlPCC2> container = new BeanItemContainer<MapeoControlPCC2>(MapeoControlPCC2.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));

			    	getColumn("manometro").setHeaderCaption("Presion");
			    	getColumn("manometro").setWidth(new Double(220));
			    	getColumn("controlRoturas").setHeaderCaption("Roturas");
			    	getColumn("controlRoturas").setWidth(new Double(220));
			    	
			    	getColumn("grifo").setHeaderCaption("Grifo");
			    	getColumn("grifo").setWidth(new Double(100));
			    	getColumn("cuello").setHeaderCaption("Cuello Botella");
			    	getColumn("cuello").setWidth(new Double(120));
			    	getColumn("filtro").setHeaderCaption("Cuerpos Filtro");
			    	getColumn("filtro").setWidth(new Double(130));

			    	getColumn("controlGrifos").setHeaderCaption("Grifos");
			    	getColumn("controlGrifos").setWidth(new Double(220));
			    	getColumn("resultado").setHeaderCaption("PArada Presion");
			    	getColumn("resultado").setWidth(new Double(220));
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("validado").setHeaderCaption("Validador");
			    	getColumn("validado").setWidth(new Double(220));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					getColumn("controlGrifos").setHidden(true);
					getColumn("verificado").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() 
				{					
					setColumnOrder("fecha", "hora", "resultado", "manometro", "controlRoturas", "grifo", "cuello", "filtro", "realizado", "validado", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
    
    private void generarRegistros()
    {
    	ArrayList<MapeoControlPCC2> vector=null;
    	String rdo = null;
    	
    	MapeoControlPCC2 mapeoPreop = new MapeoControlPCC2();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	if (vector==null || vector.isEmpty())
    	{
    		rdo = this.cncs.generarVacios(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());
    		if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo.toString());
    	}
    }
}