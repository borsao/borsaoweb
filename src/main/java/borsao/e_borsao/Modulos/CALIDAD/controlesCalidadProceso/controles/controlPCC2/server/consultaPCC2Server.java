package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.modelo.MapeoControlPCC2;

public class consultaPCC2Server extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPCC2Server  instance;
	
	public consultaPCC2Server (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPCC2Server  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPCC2Server (r_usuario);			
		}
		return instance;
	}


	public MapeoControlPCC2 datosOpcionesGlobal(MapeoControlPCC2 r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlPCC2 mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_PCC2_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_PCC2_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_pcc2.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_PCC2_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_pcc2.idCodigo vac_id, ");
			cadenaSQL.append(" qc_pcc2.hora vac_hora, ");
			cadenaSQL.append(" qc_pcc2.manometro vac_man, ");
			cadenaSQL.append(" qc_pcc2.controlRoturas vac_rot, ");
			cadenaSQL.append(" qc_pcc2.controlGrifos vac_gri, ");
			cadenaSQL.append(" qc_pcc2.resultado vac_res, ");
			cadenaSQL.append(" qc_pcc2.realizado vac_rea, ");
			cadenaSQL.append(" qc_pcc2.validado vac_val, ");
			cadenaSQL.append(" qc_pcc2.verificado vac_ver, ");
			cadenaSQL.append(" qc_pcc2.observaciones vac_obs, ");
			cadenaSQL.append(" qc_pcc2.fecha tur_fec ");
			cadenaSQL.append(" FROM " + tabla + " qc_pcc2 ");
			
			try
			{
				if (r_mapeo!=null)
				{
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_pcc2.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPCC2();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("vac_id"));
					mapeo.setHora(rsOpcion.getString("vac_hora"));
					mapeo.setManometro(rsOpcion.getString("vac_man"));
					mapeo.setControlRoturas(rsOpcion.getString("vac_rot"));
					mapeo.setControlGrifos(rsOpcion.getString("vac_gri"));
					mapeo.setResultado(rsOpcion.getString("vac_res"));
					mapeo.setValidado(rsOpcion.getString("vac_val"));
					mapeo.setRealizado(rsOpcion.getString("vac_rea"));
					mapeo.setVerificado(rsOpcion.getString("vac_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vac_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlPCC2> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlPCC2> vector = null;
		MapeoControlPCC2 mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_pcc2.idCodigo vac_id, ");
			cadenaSQL.append(" qc_pcc2.hora vac_hora, ");
			cadenaSQL.append(" qc_pcc2.manometro vac_man, ");
			cadenaSQL.append(" qc_pcc2.controlRoturas vac_rot, ");
			cadenaSQL.append(" qc_pcc2.controlGrifos vac_gri, ");
			cadenaSQL.append(" qc_pcc2.grifo pre_gri, ");
			cadenaSQL.append(" qc_pcc2.cuello pre_cue, ");
			cadenaSQL.append(" qc_pcc2.filtro pre_fil, ");
			cadenaSQL.append(" qc_pcc2.resultado vac_res, ");
			cadenaSQL.append(" qc_pcc2.realizado vac_rea, ");
			cadenaSQL.append(" qc_pcc2.validado vac_val, ");
			cadenaSQL.append(" qc_pcc2.verificado vac_ver, ");
			cadenaSQL.append(" qc_pcc2.observaciones vac_obs, ");
			cadenaSQL.append(" qc_pcc2.fecha tur_fec ");
			cadenaSQL.append(" FROM " + tabla + " qc_pcc2 ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}

				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlPCC2>();
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPCC2();
					mapeo.setIdCodigo(rsOpcion.getInt("vac_id"));
					mapeo.setHora(rsOpcion.getString("vac_hora"));
					mapeo.setManometro(rsOpcion.getString("vac_man"));
					mapeo.setControlRoturas(rsOpcion.getString("vac_rot"));
					mapeo.setControlGrifos(rsOpcion.getString("vac_gri"));
					mapeo.setGrifo(rsOpcion.getString("pre_gri"));
					mapeo.setCuello(rsOpcion.getString("pre_cue"));
					mapeo.setFiltro(rsOpcion.getString("pre_fil"));
					mapeo.setResultado(rsOpcion.getString("vac_res"));
					mapeo.setValidado(rsOpcion.getString("vac_val"));
					mapeo.setRealizado(rsOpcion.getString("vac_rea"));
					mapeo.setVerificado(rsOpcion.getString("vac_ver"));
					mapeo.setObservaciones(rsOpcion.getString("vac_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));

					vector.add(mapeo);

				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	public String generarVacios(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlPCC2 mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_pcc2.idCodigo vac_id, ");
			cadenaSQL.append(" qc_pcc2.hora vac_hora, ");
			cadenaSQL.append(" qc_pcc2.manometro vac_man, ");
			cadenaSQL.append(" qc_pcc2.controlRoturas vac_rot, ");
			cadenaSQL.append(" qc_pcc2.controlGrifos vac_gri, ");
			cadenaSQL.append(" qc_pcc2.grifo pre_gri, ");
			cadenaSQL.append(" qc_pcc2.cuello pre_cue, ");
			cadenaSQL.append(" qc_pcc2.filtro pre_fil, ");
			cadenaSQL.append(" qc_pcc2.resultado vac_res, ");
			cadenaSQL.append(" qc_pcc2.realizado vac_rea, ");
			cadenaSQL.append(" qc_pcc2.validado vac_val, ");
			cadenaSQL.append(" qc_pcc2.verificado vac_ver, ");
			cadenaSQL.append(" qc_pcc2.observaciones vac_obs, ");
			cadenaSQL.append(" qc_pcc2.fecha tur_fec ");
			cadenaSQL.append(" FROM " + tabla + " qc_pcc2 ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return null;
				}
				
				for (int i=0; i<24;i++)
				{
					mapeo = new MapeoControlPCC2();
					mapeo.setIdProgramacion(r_idProgramacion);
					mapeo.setIdCodigo(this.obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
					mapeo.setManometro("");
					mapeo.setControlGrifos("");
					mapeo.setGrifo("");
					mapeo.setCuello("");
					mapeo.setFiltro("");
					mapeo.setControlRoturas("");
					mapeo.setResultado("-");
					mapeo.setRealizado(eBorsao.get().accessControl.getNombre());
					mapeo.setValidado("");
					mapeo.setVerificado("");
					mapeo.setHora(String.valueOf(i+1)+":00:00");
					mapeo.setLinea(r_linea);
					this.guardarNuevo(mapeo);
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
				return ex.getMessage();
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return null;
	}

	
	public String guardarNuevo(MapeoControlPCC2 r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
		}
		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + "( ");
			
			cadenaSQL.append(tabla + ".idCodigo , ");
			cadenaSQL.append(tabla + ".hora , ");
			cadenaSQL.append(tabla + ".manometro , ");
			cadenaSQL.append(tabla + ".controlRoturas, ");
			cadenaSQL.append(tabla + ".controlGrifos, ");
			cadenaSQL.append(tabla + ".grifo, ");
			cadenaSQL.append(tabla + ".cuello, ");
			cadenaSQL.append(tabla + ".filtro, ");
			cadenaSQL.append(tabla + ".resultado, ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".validado , ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion, ");
			cadenaSQL.append(tabla + ".fecha , ");
			cadenaSQL.append(tabla + ".verificado ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getManometro()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getManometro());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getControlRoturas()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getControlRoturas());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getControlGrifos()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getControlGrifos());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getGrifo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getGrifo());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getCuello()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getCuello());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getFiltro()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getFiltro());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    
		    if (r_mapeo.getResultado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getResultado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getValidado()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getValidado());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(14, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(14, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
	        preparedStatement.executeUpdate();

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoControlPCC2 r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
		}
		try
		{
			
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".hora =?, ");
			cadenaSQL.append(tabla + ".manometro =?, ");
			cadenaSQL.append(tabla + ".controlRoturas =?, ");
			cadenaSQL.append(tabla + ".controlGrifos =?, ");
			cadenaSQL.append(tabla + ".grifo =?, ");
			cadenaSQL.append(tabla + ".cuello =?, ");
			cadenaSQL.append(tabla + ".filtro =?, ");
			cadenaSQL.append(tabla + ".resultado =?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".validado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =?, " );
			cadenaSQL.append(tabla + ".fecha =? " );
			
	        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getManometro()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getManometro());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getControlRoturas()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getControlRoturas());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getControlGrifos()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getControlGrifos());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getGrifo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getGrifo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCuello()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getCuello());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getFiltro()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getFiltro());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getResultado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getResultado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getValidado()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getValidado());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(13, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(13, null);
	    	}
	    	
		    preparedStatement.setInt(14, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoControlPCC2 r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
		}

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_pcc2.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_pcc2.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_pcc2.idCodigo vac_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_pcc2 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
			cadenaSQL.append(" and qc_pcc2.validado <> '' ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
				
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("vac_id"));
		        preparedStatement.executeUpdate();
		        return null;
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "No validado";
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_pcc2_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_pcc2_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_pcc2_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}