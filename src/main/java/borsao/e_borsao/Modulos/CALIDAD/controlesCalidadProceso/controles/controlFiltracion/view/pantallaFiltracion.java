package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.modelo.MapeoControlFiltracion;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.server.consultaFiltracionServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaFiltracion extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlFiltracion mapeoControlFiltracion = null;
	private consultaFiltracionServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;
	private Label lblCumplimentacion = null;
	
	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private Combo cmbPH = null;
	private Combo cmbPeracetico= null;
	private Combo cmbNitrogeno = null;
	private Combo cmbIntegridad = null;
	private Combo cmbConductividad = null;
	private Combo cmbTipo = null;
	
	private TextField txtPH = null;
	private TextField txtPeracetico = null;
	private TextField txtIntegridad = null;
	private TextField txtConductividad = null;;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app = null;
	private static String tipoControl = "global";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaFiltracion(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaFiltracionServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
			HorizontalLayout linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new Combo("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setEnabled(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

			this.txtFecha = new EntradaDatosFecha();
			this.txtFecha.setCaption("Fecha");
			this.txtFecha.setEnabled(false);
			this.txtFecha.setValue(this.mapeoProduccion.getFecha());
			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtFecha.setWidth("150px");
			
			this.lblCumplimentacion = new Label();
			this.lblCumplimentacion.setValue("Frecuencia: Al comienzo de cada jornada");
//			this.lblCumplimentacion.setWidth("100%");
//			this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
			this.lblCumplimentacion.addStyleName("lblTituloCalidad");
			
			linea1.addComponent(this.txtEjercicio);
			linea1.addComponent(this.cmbSemana);
			linea1.addComponent(this.txtFecha);
			linea1.addComponent(this.lblCumplimentacion);
			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
			
		HorizontalLayout linea2 = new HorizontalLayout();
			linea2.setSpacing(true);

			this.txtHoras = new TextField();
			this.txtHoras.setCaption("Hora");
			this.txtHoras.setWidth("120px");
			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtHoras.addStyleName("rightAligned");
			
			this.cmbIntegridad= new Combo("Test Integridad");
			this.cmbIntegridad.setWidth("150px");
			this.cmbIntegridad.setEnabled(false);
			this.cmbIntegridad.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbIntegridad.setNewItemsAllowed(false);
			this.cmbIntegridad.setNullSelectionAllowed(false);

			this.txtIntegridad= new TextField();
			this.txtIntegridad.setCaption("Valor Test Integridad \n" + "Para 0,45 >= 1,15 \n" + "Para 0,65 >= 0,55");
			this.txtIntegridad.setVisible(false);
			this.txtIntegridad.setWidth("120px");
			this.txtIntegridad.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtIntegridad.addStyleName("rightAligned");
			
			this.cmbConductividad= new Combo("Conductividad");
			this.cmbConductividad.setWidth("150px");
			this.cmbConductividad.setEnabled(false);
			this.cmbConductividad.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbConductividad.setNewItemsAllowed(false);
			this.cmbConductividad.setNullSelectionAllowed(false);

			this.txtConductividad= new TextField();
			this.txtConductividad.setCaption("Valor Conductividad");
			this.txtConductividad.setVisible(false);
			this.txtConductividad.setWidth("120px");
			this.txtConductividad.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtConductividad.addStyleName("rightAligned");

			this.cmbTipo= new Combo("Limpieza con");
			this.cmbTipo.setWidth("150px");
			this.cmbTipo.setEnabled(false);
			this.cmbTipo.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbTipo.setNewItemsAllowed(false);
			this.cmbTipo.setNullSelectionAllowed(false);

			this.cmbNitrogeno= new Combo("Introduccion N2");
			this.cmbNitrogeno.setWidth("150px");
			this.cmbNitrogeno.setEnabled(false);
			this.cmbNitrogeno.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbNitrogeno.setNewItemsAllowed(false);
			this.cmbNitrogeno.setNullSelectionAllowed(false);

			this.cmbPH= new Combo("pH");
			this.cmbPH.setWidth("150px");
			this.cmbPH.setEnabled(false);
			this.cmbPH.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbPH.setNewItemsAllowed(false);
			this.cmbPH.setNullSelectionAllowed(false);

			this.txtPH= new TextField();
			this.txtPH.setCaption("Valor Test PH");
			this.txtPH.setVisible(false);
			this.txtPH.setWidth("120px");
			this.txtPH.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtPH.addStyleName("rightAligned");
			
			this.cmbPeracetico= new Combo("Test Peracetico");
			this.cmbPeracetico.setWidth("150px");
			this.cmbPeracetico.setEnabled(false);
			this.cmbPeracetico.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.cmbPeracetico.setNewItemsAllowed(false);
			this.cmbPeracetico.setNullSelectionAllowed(false);
			
			this.txtPeracetico= new TextField();
			this.txtPeracetico.setCaption("Valor Test Peracetico");
			this.txtPeracetico.setVisible(false);
			this.txtPeracetico.setWidth("120px");
			this.txtPeracetico.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtPeracetico.addStyleName("rightAligned");

			if (lineaProduccion.contentEquals("BIB"))
			{
				linea2.addComponent(this.txtHoras);
				linea2.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
				linea2.addComponent(this.cmbNitrogeno);
				linea2.addComponent(this.cmbTipo);				
			}
			else if (lineaProduccion.contentEquals("Embotelladora"))
			{
				linea2.addComponent(this.txtHoras);
				linea2.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
//				linea2.addComponent(this.cmbIntegridad);
				linea2.addComponent(this.txtIntegridad);
				linea2.setComponentAlignment(this.txtIntegridad, Alignment.BOTTOM_LEFT);
//				linea2.addComponent(this.cmbPH);
				linea2.addComponent(this.txtPH);
				linea2.setComponentAlignment(this.txtPH, Alignment.BOTTOM_LEFT);
//				linea2.addComponent(this.cmbPeracetico);
				linea2.addComponent(this.txtPeracetico);
				linea2.setComponentAlignment(this.txtPeracetico, Alignment.BOTTOM_LEFT);
				linea2.addComponent(this.cmbTipo);				
				linea2.setComponentAlignment(this.cmbTipo, Alignment.BOTTOM_LEFT);
			}
			else
			{
				linea2.addComponent(this.txtHoras);
				linea2.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
//				linea2.addComponent(this.cmbIntegridad);
				linea2.addComponent(this.txtIntegridad);
				linea2.setComponentAlignment(this.txtIntegridad, Alignment.BOTTOM_LEFT);
//				linea2.addComponent(this.cmbConductividad);
				linea2.addComponent(this.txtConductividad);
				linea2.setComponentAlignment(this.txtConductividad, Alignment.BOTTOM_LEFT);
				linea2.addComponent(this.cmbTipo);								
				linea2.setComponentAlignment(this.cmbTipo, Alignment.BOTTOM_LEFT);
			}
			

		HorizontalLayout linea4 = new HorizontalLayout();
			linea4.setSpacing(true);
			
			this.txtRealizado= new TextField();
			this.txtRealizado.setCaption("Realizado Por");
			this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtRealizado.setWidth("180px");
			
			this.txtObservaciones= new TextArea();
			this.txtObservaciones.setCaption("Observaciones");
			this.txtObservaciones.setWidth("300");
			
			btnLimpiar= new Button("Nuevo");
			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

			btnInsertar= new Button("Guardar");
			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

			btnEliminar= new Button("Eliminar");
			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
			btnEliminar.setEnabled(false);

			linea4.addComponent(this.txtRealizado);
			linea4.addComponent(this.txtObservaciones);
			linea4.addComponent(this.btnLimpiar);
			linea4.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnInsertar);
			linea4.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnEliminar);
			linea4.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
//    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
		
		if (this.lineaProduccion.contentEquals("Envasadora"))
		{
			txtIntegridad.setVisible(true);
			txtConductividad.setVisible(true);
//			this.cmbIntegridad.addValueChangeListener(new Property.ValueChangeListener()  {
//				
//				public void valueChange(ValueChangeEvent event) {
//					if (cmbIntegridad.getValue()!=null && !cmbIntegridad.getValue().toString().contentEquals("OK"))
//					{
//					}
//					else
//					{
//						txtIntegridad.setValue("");
//						txtIntegridad.setVisible(false);
//					}
//				}
//			});
			
//			this.cmbConductividad.addValueChangeListener(new Property.ValueChangeListener()  {
//				
//				public void valueChange(ValueChangeEvent event) {
//					if (cmbConductividad.getValue()!=null && !cmbConductividad.getValue().toString().contentEquals("OK"))
//					{
//					}
//					else
//					{
//						txtConductividad.setValue("");
//						txtConductividad.setVisible(false);
//					}
//				}
//			});
		}
		
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
		
			txtPH.setVisible(true);
			txtPeracetico.setVisible(true);
			txtIntegridad.setVisible(true);

//			this.cmbPH.addValueChangeListener(new Property.ValueChangeListener()  {
//				
//				public void valueChange(ValueChangeEvent event) {
//					if (cmbPH.getValue()!=null && !cmbPH.getValue().toString().contentEquals("OK"))
//					{
//					}
//					else
//					{
//						txtPH.setValue("");
//						txtPH.setVisible(false);
//					}
//				}
//			});
			
//			this.cmbPeracetico.addValueChangeListener(new Property.ValueChangeListener()  {
//				
//				public void valueChange(ValueChangeEvent event) {
//					if (cmbPeracetico.getValue()!=null && !cmbPeracetico.getValue().toString().contentEquals("OK"))
//					{
//					}
//					else
//					{
//						txtPeracetico.setValue("");
//						txtPeracetico.setVisible(false);
//					}
//				}
//			});
			
//			this.cmbIntegridad.addValueChangeListener(new Property.ValueChangeListener()  {
//				
//				public void valueChange(ValueChangeEvent event) {
//					if (cmbIntegridad.getValue()!=null && !cmbIntegridad.getValue().toString().contentEquals("OK"))
//					{
//					}
//					else
//					{
//						txtIntegridad.setVisible(false);
//						txtIntegridad.setValue("");
//					}
//				}
//			});
		}
		

	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlFiltracion mapeo = new MapeoControlFiltracion();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlFiltracion.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlFiltracion mapeo = new MapeoControlFiltracion();

			if (this.lineaProduccion.contentEquals("Embotelladora"))
			{
//				mapeo.setPh(this.cmbPH.getValue().toString());
//				mapeo.setPeracetico(this.cmbPeracetico.getValue().toString());
//				mapeo.setEstadoIntegridad(this.cmbIntegridad.getValue().toString());
				if (this.txtPH.getValue()!=null && this.txtPH.getValue().length()>0) mapeo.setValorPh(new Double(this.txtPH.getValue())); else mapeo.setValorPh(null);
				if (this.txtPeracetico.getValue()!=null && this.txtPeracetico.getValue().length()>0) mapeo.setValorPeracetico(new Double(this.txtPeracetico.getValue())); else mapeo.setValorPeracetico(null);
				if (this.txtIntegridad.getValue()!=null && this.txtIntegridad.getValue().length()>0) mapeo.setValorIntegridad(new Double(this.txtIntegridad.getValue())); else mapeo.setValorIntegridad(null);
			}
			else if (this.lineaProduccion.contentEquals("BIB"))
			{
				mapeo.setNitrogeno(this.cmbNitrogeno.getValue().toString());
			}
			else
			{
//				mapeo.setEstadoIntegridad(this.cmbIntegridad.getValue().toString());
//				mapeo.setEstadoConductividad(this.cmbConductividad.getValue().toString());
				if (this.txtIntegridad.getValue()!=null && this.txtIntegridad.getValue().length()>0) mapeo.setValorIntegridad(new Double(this.txtIntegridad.getValue())); else mapeo.setValorIntegridad(null);
				if (this.txtConductividad.getValue()!=null && this.txtConductividad.getValue().length()>0) mapeo.setValorConductividad(new Double(this.txtConductividad.getValue())); else mapeo.setValorConductividad(null);
			}
			
			mapeo.setTipoLimpieza(this.cmbTipo.getValue().toString());
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdTurno(this.mapeoProduccion.getIdCodigo());
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlFiltracion.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlFiltracion.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlFiltracion> vector=null;

    	MapeoControlFiltracion mapeoPreop = new MapeoControlFiltracion();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlFiltracion r_mapeo)
	{
		setCreacion(false);

		this.mapeoControlFiltracion = new MapeoControlFiltracion();
		
		this.mapeoControlFiltracion.setIdCodigo(r_mapeo.getIdCodigo());
		this.cmbTipo.setValue(r_mapeo.getTipoLimpieza());
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		
		if (this.lineaProduccion.contains("Embotelladora"))
		{
//			if (r_mapeo.getPh().contentEquals("KO") && r_mapeo.getValorPh()!=null) 
			if (r_mapeo.getValorPh()!=null) 
				this.txtPH.setValue(r_mapeo.getValorPh().toString());
//			if (r_mapeo.getPeracetico().contentEquals("KO") && r_mapeo.getValorPeracetico()!=null) 
			if (r_mapeo.getValorPeracetico()!=null) 
				this.txtPeracetico.setValue(r_mapeo.getValorPeracetico().toString());
//			this.cmbPH.setValue(r_mapeo.getPh());
//			this.cmbPeracetico.setValue(r_mapeo.getPeracetico());
//			this.cmbIntegridad.setValue(r_mapeo.getEstadoIntegridad());
//			if (r_mapeo.getEstadoIntegridad().contentEquals("KO") && r_mapeo.getValorIntegridad()!=null) 
			if (r_mapeo.getValorIntegridad()!=null) 
				this.txtIntegridad.setValue(r_mapeo.getValorIntegridad().toString());
		}
		else if (this.lineaProduccion.contains("BIB"))
		{
			this.cmbNitrogeno.setValue(r_mapeo.getNitrogeno());
		}
		else
		{
//			this.cmbIntegridad.setValue(r_mapeo.getEstadoIntegridad());
//			this.cmbConductividad.setValue(r_mapeo.getEstadoConductividad());
//			if (r_mapeo.getEstadoIntegridad().contentEquals("KO") && r_mapeo.getValorIntegridad()!=null) 
			if (r_mapeo.getValorIntegridad()!=null) 
				this.txtIntegridad.setValue(r_mapeo.getValorIntegridad().toString());
//			if (r_mapeo.getEstadoConductividad().contentEquals("KO") && r_mapeo.getValorConductividad()!=null) 
			if (r_mapeo.getValorConductividad()!=null) 
				this.txtConductividad.setValue(r_mapeo.getValorConductividad().toString());
		}
		
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
//		cmbPH.setEnabled(false);
//		cmbPeracetico.setEnabled(false);
//		cmbIntegridad.setEnabled(false);		
//		cmbConductividad.setEnabled(false);
		cmbNitrogeno.setEnabled(false);
		
    	if (this.lineaProduccion.contains("Embotelladora"))
		{
//    		cmbPH.setEnabled(r_activar);
//    		cmbPeracetico.setEnabled(r_activar);
//    		cmbIntegridad.setEnabled(r_activar);
		}
    	if (this.lineaProduccion.contains("Envasadora"))
		{
//    		cmbIntegridad.setEnabled(r_activar);
//    		cmbConductividad.setEnabled(r_activar);
		}
    	if (this.lineaProduccion.contains("BIB"))
		{
    		cmbNitrogeno.setEnabled(r_activar);
		}
    	cmbTipo.setEnabled(r_activar);
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
//    	txtVerificado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		if (!this.lineaProduccion.contentEquals("Embotelladora"))
		{
//			this.cmbIntegridad.setValue(null);
//			this.cmbPH.setValue(null);
//			this.cmbPeracetico.setValue(null);
//			this.cmbConductividad.setValue(null);
		}
		
		this.cmbNitrogeno.setValue(null);
		this.cmbTipo.setValue(null);
		
		this.txtConductividad.setValue("");
		this.txtPH.setValue("");
		this.txtPeracetico.setValue("");
		this.txtIntegridad.setValue("");
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
//    	if (this.cmbPH.getValue()!=null && this.cmbPH.getValue().toString().contentEquals("KO") && (this.txtPH.getValue()==null || this.txtPH.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Embotelladora"))
//    	if ((this.txtPH.getValue()==null || this.txtPH.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Embotelladora"))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de presion del PH");
//    		return false;
//    	}
//    	if (this.cmbPeracetico.getValue()!=null && this.cmbPeracetico.getValue().toString().contentEquals("KO") && (this.txtPeracetico.getValue()==null || this.txtPeracetico.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Embotelladora"))
//    	if ((this.txtPeracetico.getValue()==null || this.txtPeracetico.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Embotelladora"))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de presion del Peracetico");
//    		return false;
//    	}
//    	if (this.cmbIntegridad.getValue()!=null && this.cmbIntegridad.getValue().toString().contentEquals("KO") && (this.txtIntegridad.getValue()==null || this.txtIntegridad.getValue().toString().length()==0))
//    	if ((this.txtIntegridad.getValue()==null || this.txtIntegridad.getValue().toString().length()==0))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor del test de Integridad");
//    		return false;
//    	}
//    	if (this.cmbConductividad.getValue()!=null && this.cmbConductividad.getValue().toString().contentEquals("KO") && (this.txtConductividad.getValue()==null || this.txtConductividad.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Envasadora"))
//    	if ((this.txtConductividad.getValue()==null || this.txtConductividad.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("Envasadora"))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de la conductividad");
//    		return false;
//    	}
//    	if (this.cmbTipo.getValue()==null || this.cmbTipo.getValue().toString().length()==0)
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor del tipo de limpieza");
//    		return false;
//    	}
//    	if ((this.cmbNitrogeno.getValue()==null || this.cmbNitrogeno.getValue().toString().length()==0) && this.lineaProduccion.contentEquals("BIB"))
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de la Introduccion de Nitrogeno");
//    		return false;
//    	}
    	return true;

    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboEstados();
    	this.cargarComboTipo();

    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlFiltracion) r_fila)!=null)
    	{
    		MapeoControlFiltracion mapeoPreop = (MapeoControlFiltracion) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlFiltracion> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Filtracion Realizados");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlFiltracion> container = new BeanItemContainer<MapeoControlFiltracion>(MapeoControlFiltracion.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));

			    	getColumn("ph").setHeaderCaption("PH");
			    	getColumn("ph").setWidth(new Double(120));
			    	getColumn("valorPh").setHeaderCaption("Valor PH");
			    	getColumn("valorPh").setWidth(new Double(120));
			    	getColumn("peracetico").setHeaderCaption("Peracetico");
			    	getColumn("peracetico").setWidth(new Double(120));
			    	getColumn("valorPeracetico").setHeaderCaption("Valor Peracetico");
			    	getColumn("valorPeracetico").setWidth(new Double(120));
			    	
			    	getColumn("nitrogeno").setHeaderCaption("Nitrogeno");
			    	getColumn("nitrogeno").setWidth(new Double(120));
			    	
			    	getColumn("estadoIntegridad").setHeaderCaption("Estado Integridad");
			    	getColumn("estadoIntegridad").setWidth(new Double(130));
			    	getColumn("valorIntegridad").setHeaderCaption("Valor Integridad");
			    	getColumn("valorIntegridad").setWidth(new Double(130));
			    	getColumn("estadoConductividad").setHeaderCaption("Estado Conductividad");
			    	getColumn("estadoConductividad").setWidth(new Double(120));
			    	getColumn("valorConductividad").setHeaderCaption("Valor Conductividad");
			    	getColumn("valorConductividad").setWidth(new Double(120));
			    	getColumn("tipoLimpieza").setHeaderCaption("Tipo Limpieza");
			    	getColumn("tipoLimpieza").setWidth(new Double(145));
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idTurno").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					
					if (lineaProduccion.contentEquals("BIB"))
					{
						getColumn("ph").setHidden(true);
						getColumn("valorPh").setHidden(true);
						getColumn("peracetico").setHidden(true);
						getColumn("valorPeracetico").setHidden(true);
						getColumn("estadoIntegridad").setHidden(true);
						getColumn("valorIntegridad").setHidden(true);
						getColumn("estadoConductividad").setHidden(true);
						getColumn("valorConductividad").setHidden(true);
					}
					else if (lineaProduccion.contentEquals("Embotelladora"))
					{
						getColumn("nitrogeno").setHidden(true);
						getColumn("valorConductividad").setHidden(true);
						getColumn("ph").setHidden(true);
						getColumn("peracetico").setHidden(true);
						getColumn("estadoConductividad").setHidden(true);
						getColumn("estadoIntegridad").setHidden(true);
						
					}
					else
					{
						getColumn("estadoConductividad").setHidden(true);
						getColumn("estadoIntegridad").setHidden(true);
						getColumn("ph").setHidden(true);
						getColumn("valorPh").setHidden(true);
						getColumn("peracetico").setHidden(true);
						getColumn("valorPeracetico").setHidden(true);
						getColumn("nitrogeno").setHidden(true);
					}
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() 
				{					
					setColumnOrder("fecha", "hora", "ph", "valorPh", "peracetico","valorPeracetico", "nitrogeno", "estadoIntegridad", "valorIntegridad", "estadoConductividad", "valorConductividad", "tipoLimpieza", "realizado", "verificado", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
    
    private void cargarComboTipo()
    {
    	this.cmbTipo.addItem("Agua");
    	this.cmbTipo.addItem("Química");
    	this.cmbTipo.addItem("Peracético");
    	this.cmbTipo.addItem("CANCELADO");
    }
    
    private void cargarComboEstados()
    {
    	this.cmbConductividad.addItem("OK");
    	this.cmbConductividad.addItem("KO");
    	
    	this.cmbIntegridad.addItem("OK");
    	this.cmbIntegridad.addItem("KO");
    	
    	this.cmbNitrogeno.addItem("OK");
    	this.cmbNitrogeno.addItem("KO");

    	this.cmbPH.addItem("OK");
    	this.cmbPH.addItem("KO");
 
    	this.cmbPeracetico.addItem("OK");
    	this.cmbPeracetico.addItem("KO");

    }
    
}