package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoControlesProcesoProductivo  extends MapeoGlobal
{
    private Integer idProgramacion;
	private Integer ejercicio;
	private String semana;
	private String fecha;
	private String dia;
	private String tipo;
	private String lote;
	private String articulo;
	private String descripcion;
	private Integer unidades;

	public MapeoControlesProcesoProductivo()
	{
//		this.setArticulo("");
//		this.setVino("");
//		this.setLote("");
//		this.setDia("");
//		this.setObservaciones("");
//		this.setDescripcion("");
//		this.setCantidad(0);
//		this.setOrden(0);
//		this.setCajas(0);
//		this.setFactor(0);
//		this.setUnidades(0);
		
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getSemana() {
		if (semana!=null && new Integer(semana)==0) semana = "";
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getArticulo() {
		if (articulo!=null) return articulo.trim();
		else return null;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = RutinasCadenas.conversion(descripcion);
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}