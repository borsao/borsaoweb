package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.view;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.modelo.MapeoControlLineasRosca;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.server.consultaRoscaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaActualizacionLineasRosca extends Window
{
	private MapeoControlLineasRosca mapeoControlLineasRosca = null;
	private pantallaRosca App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtApertura= null;
	private TextField txtCierre= null;

	private Combo cmbCuelloSranex= null;
	
	private String linea = null;
	private String cabezal = null;
	private Integer idRosca= null;

	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasRosca(Ventana r_App, Integer r_idRosca, String r_caja, String r_articulo, String r_titulo)
	{
		this.App = (pantallaRosca) r_App;
		this.linea=App.lineaProduccion;
		this.cabezal = r_caja;
		this.idRosca = r_idRosca;
		
		this.cargarPantalla();
		this.cargarCombo();

		this.habilitarCampos();
		this.cargarListeners();

		this.rellenarEntradasDatos();
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		

	}

	private void habilitarCampos()
	{
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					HashMap<String, String> valores = new HashMap<String, String>();
					valores.put("campo", cabezal);

					valores.put("01", txtApertura.getValue());
					valores.put("02", txtCierre.getValue());
					valores.put("03", cmbCuelloSranex.getValue().toString());

					App.actualizarDatosLineaRosca(idRosca, valores);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		String campoErroneo = null;
		
		if (txtApertura.getValue()==null || txtApertura.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Apertura";
		}
		if (txtCierre.getValue()==null || txtCierre.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Cierre";
		}
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		this.cargarValoresEstados(cmbCuelloSranex);
	}
	
	private void cargarValoresEstados(Combo r_combo)
    {
    	r_combo.addItem("CORRECTO");
    	r_combo.addItem("FALLO");
    	r_combo.addItem("INCORRECTO");
    	r_combo.addItem("REALIZADO REFERENCIA ANTERIOR");
    	r_combo.addItem("SIN PRODUCCION");
    }
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.txtApertura=new TextField("Torque Apertura");
				this.txtApertura.setWidth("150px");
				this.txtApertura.setRequired(true);
				this.txtApertura.setEnabled(true);
				this.txtApertura.setStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
//				this.txtApertura.setStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.txtCierre=new TextField("Torque Cierre");
				this.txtCierre.setWidth("150px");
				this.txtCierre.setRequired(true);
				this.txtCierre.setEnabled(true);
				this.txtCierre.setStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
//				this.txtCierre.setStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.cmbCuelloSranex=new Combo("Cuello / Saranex");
				this.propiedadesCombos(this.cmbCuelloSranex);

				fila2.addComponent(txtApertura);				
				fila2.addComponent(txtCierre);
				fila2.addComponent(cmbCuelloSranex);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
	}
	
	private void propiedadesCombos(Combo r_combo)
	{
		r_combo.setWidth("200px");
		r_combo.setNewItemsAllowed(false);
		r_combo.setNullSelectionAllowed(false);
	}

	private void rellenarEntradasDatos()
	{
		consultaRoscaServer cadps = consultaRoscaServer.getInstance(CurrentUser.get());
		HashMap<String, String> hash = cadps.recuperarDatosCabezal(this.idRosca,this.cabezal,this.linea);
		
		/*
		 * con el hash relleno las entradas de datos
		 */
		
		if (hash!=null && !hash.isEmpty())
		{
			this.txtApertura.setValue(hash.get("01"));
			this.txtCierre.setValue(hash.get("02"));
			this.cmbCuelloSranex.setValue(hash.get("03"));
		}
	}

}