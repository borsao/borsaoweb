package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlVacio extends MapeoGlobal
{
	private String linea;
	private Integer idProgramacion;
	private String hora;
	private Double cabezal1;
	private Double cabezal2;
	private Double cabezal3;
	private Double cabezal4;
	private Double cabezal5;
	private Double cabezal6;
	private String realizado;
	private String verificado;
	private String observaciones;
	private Date fecha;
	
	public MapeoControlVacio()
	{
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Double getCabezal1() {
		return cabezal1;
	}

	public void setCabezal1(Double cabezal1) {
		this.cabezal1 = cabezal1;
	}

	public Double getCabezal2() {
		return cabezal2;
	}

	public void setCabezal2(Double cabezal2) {
		this.cabezal2 = cabezal2;
	}

	public Double getCabezal3() {
		return cabezal3;
	}

	public void setCabezal3(Double cabezal3) {
		this.cabezal3 = cabezal3;
	}

	public Double getCabezal4() {
		return cabezal4;
	}

	public void setCabezal4(Double cabezal4) {
		this.cabezal4 = cabezal4;
	}

	public Double getCabezal5() {
		return cabezal5;
	}

	public void setCabezal5(Double cabezal5) {
		this.cabezal5 = cabezal5;
	}

	public Double getCabezal6() {
		return cabezal6;
	}

	public void setCabezal6(Double cabezal6) {
		this.cabezal6 = cabezal6;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


}