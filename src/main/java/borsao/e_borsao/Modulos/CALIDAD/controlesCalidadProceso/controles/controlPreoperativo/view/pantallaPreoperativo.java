package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.modelo.MapeoControlPreoperativo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.server.consultaPreoperativoServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaPreoperativo extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlPreoperativo mapeoControlPreoprativo = null;
	private consultaPreoperativoServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private CheckBox chkSalaOk = null;
	private CheckBox chkMaquinaLimpia = null;
	private CheckBox chkAusenciaCuerpos = null;
	private CheckBox chkMaquinaOk = null;
	private CheckBox chkLineaLimpia = null;
	private CheckBox chkCarbonico = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	private Label lblCumplimentacion= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "global";
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaPreoperativo(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaPreoperativoServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setValue("Frecuencia: Al comienzo de cada jornada y al cambio de formato.");
//				this.lblCumplimentacion.setWidth("100%");
//				this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
    			linea1.addComponent(this.txtEjercicio);
    			linea1.addComponent(this.cmbSemana);
    			linea1.addComponent(this.txtFecha);
    			linea1.addComponent(this.lblCumplimentacion);
    			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");
    			
    			this.chkSalaOk= new CheckBox();
    			this.chkSalaOk.setCaption("Sala Limpia ?");

    			this.chkMaquinaOk= new CheckBox();
    			this.chkMaquinaOk.setCaption("Maquinas en OK ?");
    			
				this.chkAusenciaCuerpos= new CheckBox();
    			this.chkAusenciaCuerpos.setCaption("Ausencia Cuerops Extraños ?");

				this.chkMaquinaLimpia= new CheckBox();
    			this.chkMaquinaLimpia.setCaption("Maquinas Limpias ?");

				this.chkLineaLimpia= new CheckBox();
    			this.chkLineaLimpia.setCaption("Linea Limpias ?");

    			this.chkCarbonico= new CheckBox();
    			this.chkCarbonico.setCaption("Manometro Carb. OK?");

    			linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.chkSalaOk);
    			linea2.setComponentAlignment(this.chkSalaOk, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkMaquinaOk);
    			linea2.setComponentAlignment(this.chkMaquinaOk, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkAusenciaCuerpos);
    			linea2.setComponentAlignment(this.chkAusenciaCuerpos, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkMaquinaLimpia);
    			linea2.setComponentAlignment(this.chkMaquinaLimpia, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkLineaLimpia);
    			linea2.setComponentAlignment(this.chkLineaLimpia, Alignment.BOTTOM_LEFT);
    			if (this.lineaProduccion.contentEquals("BIB"))
				{
    				linea2.addComponent(this.chkCarbonico);
    				linea2.setComponentAlignment(this.chkCarbonico, Alignment.BOTTOM_LEFT);
				}
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.txtRealizado= new TextField();
    			this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");
				
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnLimpiar= new Button("Nuevo");
//    			btnLimpiar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

    			
    			linea3.addComponent(this.txtRealizado);
    			linea3.addComponent(this.txtObservaciones);
    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlPreoperativo mapeo = new MapeoControlPreoperativo();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlPreoperativo mapeo = new MapeoControlPreoperativo();
			
			if (chkAusenciaCuerpos.getValue()) mapeo.setAusencia_cuerpos("S"); else mapeo.setAusencia_cuerpos("N");
			if (chkMaquinaLimpia.getValue()) mapeo.setMaquina_limpia("S"); else mapeo.setMaquina_limpia("N");
			if (chkMaquinaOk.getValue()) mapeo.setMaquina_ok("S"); else mapeo.setMaquina_ok("N");
			if (chkLineaLimpia.getValue()) mapeo.setLinea_ok("S"); else mapeo.setLinea_ok("N");
			if (chkSalaOk.getValue()) mapeo.setSala_ok("S"); else mapeo.setSala_ok("N");
			if (chkCarbonico.getValue()) mapeo.setCarbonico("S"); else mapeo.setCarbonico("N");
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoPreoperativo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
				rdo = cncs.guardarCambiosPreoperativo(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlPreoperativo> vector=null;

    	MapeoControlPreoperativo mapeoPreop = new MapeoControlPreoperativo();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlPreoperativo r_mapeo)
	{
		this.mapeoControlPreoprativo = new MapeoControlPreoperativo();
		this.mapeoControlPreoprativo.setIdCodigo(r_mapeo.getIdCodigo());
		setCreacion(false);
		if (r_mapeo.getAusencia_cuerpos().contentEquals("S")) chkAusenciaCuerpos.setValue(true);
		if (r_mapeo.getMaquina_limpia().contentEquals("S")) chkMaquinaLimpia.setValue(true);
		if (r_mapeo.getMaquina_ok().contentEquals("S")) chkMaquinaOk.setValue(true);
		if (r_mapeo.getLinea_ok().contentEquals("S")) chkLineaLimpia.setValue(true);
		if (r_mapeo.getSala_ok().contentEquals("S")) chkSalaOk.setValue(true);
		if (this.lineaProduccion.contentEquals("BIB")) if (r_mapeo.getCarbonico().contentEquals("S")) chkCarbonico.setValue(true);
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
//    	this.cmbTurno.setEnabled(r_activar);
//    	this.cmbSemana.setEnabled(r_activar);
//    	this.txtEjercicio.setEnabled(r_activar);
//    	this.txtFecha.setEnabled(r_activar);
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
//    	txtVerificado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.chkAusenciaCuerpos.setValue(false);
		this.chkSalaOk.setValue(false);
		this.chkMaquinaLimpia.setValue(false);
		this.chkMaquinaOk.setValue(false);
		this.chkLineaLimpia.setValue(false);
		this.chkCarbonico.setValue(false);
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlPreoperativo) r_fila)!=null)
    	{
    		MapeoControlPreoperativo mapeoPreop = (MapeoControlPreoperativo) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlPreoperativo> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Preoperativos Realizados");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlPreoperativo> container = new BeanItemContainer<MapeoControlPreoperativo>(MapeoControlPreoperativo.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));
			    	getColumn("sala_ok").setHeaderCaption("Sala Limpia");
			    	getColumn("sala_ok").setWidth(new Double(120));
			    	getColumn("maquina_limpia").setHeaderCaption("Maquina Limpia");
			    	getColumn("maquina_limpia").setWidth(new Double(130));
			    	getColumn("ausencia_cuerpos").setHeaderCaption("Cuerpos Extr.");
			    	getColumn("ausencia_cuerpos").setWidth(new Double(120));
			    	getColumn("maquina_ok").setHeaderCaption("Maquinas OK");
			    	getColumn("maquina_ok").setWidth(new Double(120));
			    	getColumn("linea_ok").setHeaderCaption("Linea buen Estado");
			    	getColumn("linea_ok").setWidth(new Double(145));
			    	getColumn("carbonico").setHeaderCaption("Manometro Carbonico ");
			    	getColumn("carbonico").setWidth(new Double(145));
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idTurno").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					if (!lineaProduccion.contentEquals("BIB")) getColumn("carbonico").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					setColumnOrder("fecha", "hora", "sala_ok", "maquina_limpia","ausencia_cuerpos", "maquina_ok", "linea_ok", "carbonico", "realizado", "verificado", "observaciones");
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}