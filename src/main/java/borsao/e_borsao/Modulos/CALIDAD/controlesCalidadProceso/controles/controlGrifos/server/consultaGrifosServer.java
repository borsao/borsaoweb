package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.modelo.MapeoControlGrifos;

public class consultaGrifosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaGrifosServer  instance;
	
	public consultaGrifosServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaGrifosServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaGrifosServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlGrifos datosOpcionesGlobal(MapeoControlGrifos r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlGrifos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_grifos.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_grifos.idCodigo pre_id, ");
			cadenaSQL.append(" qc_grifos.hora pre_hora, ");
			cadenaSQL.append(" qc_grifos.grifo pre_gri, ");
			cadenaSQL.append(" qc_grifos.cuello pre_cue, ");
			cadenaSQL.append(" qc_grifos.filtro pre_fil, ");
			cadenaSQL.append(" qc_grifos.realizado pre_rea, ");
			cadenaSQL.append(" qc_grifos.verificado pre_ver, ");
			cadenaSQL.append(" qc_grifos.fecha pre_fec, ");
			cadenaSQL.append(" qc_grifos.observaciones pre_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_grifos ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_grifos.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlGrifos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
					mapeo.setGrifo(rsOpcion.getString("pre_gri"));
					mapeo.setCuello(rsOpcion.getString("pre_cue"));
					mapeo.setFiltro(rsOpcion.getString("pre_fil"));
					mapeo.setRealizado(rsOpcion.getString("pre_rea"));
					mapeo.setVerificado(rsOpcion.getString("pre_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
					mapeo.setFecha(rsOpcion.getDate("pre_fec"));
					mapeo.setHora(rsOpcion.getString("pre_hora"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return mapeo;
	}
	
	public ArrayList<MapeoControlGrifos> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlGrifos> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlGrifos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_grifos.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_grifos.idCodigo pre_id, ");
			cadenaSQL.append(" qc_grifos.hora pre_hora, ");
			cadenaSQL.append(" qc_grifos.grifo pre_gri, ");
			cadenaSQL.append(" qc_grifos.cuello pre_cue, ");
			cadenaSQL.append(" qc_grifos.filtro pre_fil, ");
			cadenaSQL.append(" qc_grifos.realizado pre_rea, ");
			cadenaSQL.append(" qc_grifos.verificado pre_ver, ");
			cadenaSQL.append(" qc_grifos.fecha pre_fec, ");
			cadenaSQL.append(" qc_grifos.observaciones pre_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_grifos ");
			
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlGrifos>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlGrifos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
					mapeo.setGrifo(rsOpcion.getString("pre_gri"));
					mapeo.setCuello(rsOpcion.getString("pre_cue"));
					mapeo.setFiltro(rsOpcion.getString("pre_fil"));
					mapeo.setRealizado(rsOpcion.getString("pre_rea"));
					mapeo.setVerificado(rsOpcion.getString("pre_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
					mapeo.setFecha(rsOpcion.getDate("pre_fec"));
					mapeo.setHora(rsOpcion.getString("pre_hora"));
					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	
	public String guardarNuevoGrifos(MapeoControlGrifos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
		}

		try
		{
			
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".grifo , ");
				cadenaSQL.append(tabla + ".cuello , ");
				cadenaSQL.append(tabla + ".filtro , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
				
			}
			else
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".grifo , ");
				cadenaSQL.append(tabla + ".cuello , ");
				cadenaSQL.append(tabla + ".filtro , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getGrifo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getGrifo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCuello()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCuello());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getFiltro()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getFiltro());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(10, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	
	public String guardarCambiosGrifos(MapeoControlGrifos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
		}
		try
		{
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".grifo =?, ");
				cadenaSQL.append(tabla + ".cuello=?, ");
				cadenaSQL.append(tabla + ".filtro=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
			else
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".grifo =?, ");
				cadenaSQL.append(tabla + ".cuello=?, ");
				cadenaSQL.append(tabla + ".filtro=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getGrifo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getGrifo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCuello()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCuello());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getFiltro()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getFiltro());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlGrifos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_grifos.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_grifos.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_grifos.idCodigo pre_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_grifos ");
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pre_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_grifos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_grifos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_grifos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}