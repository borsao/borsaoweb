package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlLineasRosca extends MapeoGlobal
{
	private Integer idRosca;

	private String acc; // para poner el boton
	
	private String material;
	private String cabezal1;
	private String cabezal2;
	private String cabezal3;
	private String cabezal4;
	private String cabezal5;
	private String cabezal6;

	public MapeoControlLineasRosca()
	{
	}

	public Integer getIdRosca() {
		return idRosca;
	}

	public void setIdRosca(Integer idRosca) {
		this.idRosca = idRosca;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getCabezal1() {
		return cabezal1;
	}

	public void setCabezal1(String cabezal1) {
		this.cabezal1 = cabezal1;
	}

	public String getCabezal2() {
		return cabezal2;
	}

	public void setCabezal2(String cabezal2) {
		this.cabezal2 = cabezal2;
	}

	public String getCabezal3() {
		return cabezal3;
	}

	public void setCabezal3(String cabezal3) {
		this.cabezal3 = cabezal3;
	}

	public String getCabezal4() {
		return cabezal4;
	}

	public void setCabezal4(String cabezal4) {
		this.cabezal4 = cabezal4;
	}

	public String getCabezal5() {
		return cabezal5;
	}

	public void setCabezal5(String cabezal5) {
		this.cabezal5 = cabezal5;
	}

	public String getCabezal6() {
		return cabezal6;
	}

	public void setCabezal6(String cabezal6) {
		this.cabezal6 = cabezal6;
	}

}