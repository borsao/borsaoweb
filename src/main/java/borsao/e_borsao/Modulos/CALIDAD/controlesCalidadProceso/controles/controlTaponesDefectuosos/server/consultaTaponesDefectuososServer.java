package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlTaponesDefectuosos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlTaponesDefectuosos.modelo.MapeoControlTaponesDefectuosos;

public class consultaTaponesDefectuososServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTaponesDefectuososServer  instance;
	
	public consultaTaponesDefectuososServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTaponesDefectuososServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTaponesDefectuososServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlTaponesDefectuosos datosOpcionesGlobal(MapeoControlTaponesDefectuosos r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlTaponesDefectuosos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
//			campo= "idprd_programacion";
//			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_tapones.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_tapones.idCodigo tap_id, ");
			cadenaSQL.append(" qc_tapones.hora tap_hora, ");
			cadenaSQL.append(" qc_tapones.cantidad tap_can, ");
			cadenaSQL.append(" qc_tapones.cabezal tap_cab, ");
			cadenaSQL.append(" qc_tapones.realizado tap_rea, ");
			cadenaSQL.append(" qc_tapones.verificado tap_ver, ");
			cadenaSQL.append(" qc_tapones.fecha tap_fec, ");
			cadenaSQL.append(" qc_tapones.observaciones tap_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_tapones ");
			try
			{
				if (r_mapeo!=null)
				{
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_tapones.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlTaponesDefectuosos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("tap_id"));
					mapeo.setCantidad(rsOpcion.getInt("tap_can"));
					mapeo.setCabezal(rsOpcion.getInt("tap_cab"));
					mapeo.setRealizado(rsOpcion.getString("tap_rea"));
					mapeo.setVerificado(rsOpcion.getString("tap_ver"));
					mapeo.setObservaciones(rsOpcion.getString("tap_obs"));
					mapeo.setFecha(rsOpcion.getDate("tap_fec"));
					mapeo.setHora(rsOpcion.getString("tap_hora"));
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlTaponesDefectuosos> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlTaponesDefectuosos> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlTaponesDefectuosos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
//			campo= "idprd_programacion";
//			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_tapones.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_tapones.idCodigo tap_id, ");
			cadenaSQL.append(" qc_tapones.hora tap_hora, ");
			cadenaSQL.append(" qc_tapones.cantidad tap_can, ");
			cadenaSQL.append(" qc_tapones.cabezal tap_cab, ");
			cadenaSQL.append(" qc_tapones.realizado tap_rea, ");
			cadenaSQL.append(" qc_tapones.verificado tap_ver, ");
			cadenaSQL.append(" qc_tapones.fecha tap_fec, ");
			cadenaSQL.append(" qc_tapones.observaciones tap_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_tapones ");
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				
				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlTaponesDefectuosos>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlTaponesDefectuosos();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("tap_id"));
					mapeo.setCantidad(rsOpcion.getInt("tap_can"));
					mapeo.setCabezal(rsOpcion.getInt("tap_cab"));
					mapeo.setRealizado(rsOpcion.getString("tap_rea"));
					mapeo.setVerificado(rsOpcion.getString("tap_ver"));
					mapeo.setObservaciones(rsOpcion.getString("tap_obs"));
					mapeo.setFecha(rsOpcion.getDate("tap_fec"));
					mapeo.setHora(rsOpcion.getString("tap_hora"));
					
					vector.add(mapeo);
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}
	
	public String guardarNuevoTaponesDefectuosos(MapeoControlTaponesDefectuosos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
		}

		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".hora, ");
			cadenaSQL.append(tabla + ".cantidad , ");
			cadenaSQL.append(tabla + ".cabezal , ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".verificado, ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion , ");
			cadenaSQL.append(tabla + ".fecha ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");

			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getCabezal()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCabezal());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
	    	if (r_mapeo.getRealizado()!=null)
	    	{
	    		preparedStatement.setString(5, r_mapeo.getRealizado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(5, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
	    	{
	    		preparedStatement.setString(6, r_mapeo.getVerificado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
	    	}
	    	if (r_mapeo.getObservaciones()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getObservaciones());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
	    	}
	    	if (r_mapeo.getIdProgramacion()!=null)
	    	{
	    		preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setInt(8, 0);
	    	}
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(9, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(9, null);
	    	}
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambiosTaponesDefectuosos(MapeoControlTaponesDefectuosos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
		}
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".hora=?, ");
			cadenaSQL.append(tabla + ".cantidad=?, ");
			cadenaSQL.append(tabla + ".cabezal=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".verificado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");

			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getCabezal()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getCabezal());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	    	if (r_mapeo.getRealizado()!=null)
	    	{
	    		preparedStatement.setString(4, r_mapeo.getRealizado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(4, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
	    	{
	    		preparedStatement.setString(5, r_mapeo.getVerificado());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(5, null);
	    	}
	    	if (r_mapeo.getObservaciones()!=null)
	    	{
	    		preparedStatement.setString(6, r_mapeo.getObservaciones());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
	    	}
	    	if (r_mapeo.getIdProgramacion()!=null)
	    	{
	    		preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
	    	}
	    	else
	    	{
	    		preparedStatement.setInt(7, 0);
	    	}
	    	
	    	preparedStatement.setInt(8, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlTaponesDefectuosos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_ejercicio, String r_semana, String r_fecha)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
//			campo= "idprd_programacion";
//			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_tapones.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_tapones.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_tapones.idCodigo tap_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_tapones ");
	     	cadenaSQL.append(" where prd_prog.ejercicio = " + r_ejercicio );
	     	cadenaSQL.append(" and prd_prog.semana = " + r_semana);
	     	cadenaSQL.append(" and qc_tapones.linea = " + r_linea);
	     	cadenaSQL.append(" and qc_tapones.fecha = '" + RutinasFechas.convertirAFechaMysql(r_fecha) + "' ");
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("tap_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
//			tabla = "qc_taponesDef_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_taponesDef_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo tap_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_taponesDef_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_taponesDef_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_taponesDef_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}