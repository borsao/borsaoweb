package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.modelo.MapeoControlLineasLotesMP;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.modelo.MapeoControlLotesMP;

public class consultaLotesMPServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaLotesMPServer  instance;
	
	public consultaLotesMPServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaLotesMPServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaLotesMPServer (r_usuario);			
		}
		return instance;
	}


	public ArrayList<MapeoControlLotesMP> datosOpcionesGlobal(MapeoControlLotesMP r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoControlLotesMP> vector = null;
		MapeoControlLotesMP mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_lotesMP.idCodigo lmp_id, ");
			cadenaSQL.append(" qc_lotesMP.linea lmp_lin, ");
			cadenaSQL.append(" qc_lotesMP.hora lmp_hor, ");
			cadenaSQL.append(" qc_lotesMP.cantidad lmp_can, ");
			cadenaSQL.append(" qc_lotesMP.realizado lmp_rea, ");
			cadenaSQL.append(" qc_lotesMP.verificado lmp_ver, ");
			cadenaSQL.append(" qc_lotesMP.fecha lmp_fec, ");
			cadenaSQL.append(" qc_lotesMP.observaciones lmp_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_lotesMP ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_lotesMP.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				vector = new ArrayList<MapeoControlLotesMP>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLotesMP();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lmp_id"));
					mapeo.setLinea(r_mapeo.getLinea());
					mapeo.setHora(rsOpcion.getString("lmp_hor"));
					mapeo.setCantidad(rsOpcion.getInt("lmp_can"));
					mapeo.setRealizado(rsOpcion.getString("lmp_rea"));
					mapeo.setVerificado(rsOpcion.getString("lmp_ver"));
					mapeo.setObservaciones(rsOpcion.getString("lmp_obs"));
					mapeo.setFecha(rsOpcion.getDate("lmp_fec"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	public boolean datosSinBotellasLote(MapeoControlLotesMP r_mapeo)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_lotesMP.idCodigo lmp_id, ");
			cadenaSQL.append(" qc_lotesMP.linea lmp_lin, ");
			cadenaSQL.append(" qc_lotesMP.hora lmp_hor, ");
			cadenaSQL.append(" qc_lotesMP.cantidad lmp_can, ");
			cadenaSQL.append(" qc_lotesMP.realizado lmp_rea, ");
			cadenaSQL.append(" qc_lotesMP.verificado lmp_ver, ");
			cadenaSQL.append(" qc_lotesMP.fecha lmp_fec, ");
			cadenaSQL.append(" qc_lotesMP.observaciones lmp_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_lotesMP ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					cadenaWhere.append(" where qc_lotesMP.cantidad = 0 ");
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		
		
		return false;
	}

	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_lotesMP.idCodigo lmp_id, ");
			cadenaSQL.append(" qc_lotesMP.linea lmp_lin, ");
			cadenaSQL.append(" qc_lotesMP.hora lmp_hor, ");
			cadenaSQL.append(" qc_lotesMP.cantidad lmp_can, ");
			cadenaSQL.append(" qc_lotesMP.realizado lmp_rea, ");
			cadenaSQL.append(" qc_lotesMP.verificado lmp_ver, ");
			cadenaSQL.append(" qc_lotesMP.fecha lmp_fec, ");
			cadenaSQL.append(" qc_lotesMP.observaciones lmp_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_lotesMP ");
			cadenaSQL.append(" where qc_lotesMP.idProgramacion = " + r_idProgramacion);
			cadenaSQL.append(" and qc_lotesMP.cantidad <> '0'" );
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public ArrayList<MapeoControlLotesMP> datosOpcionesGlobal(String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlLotesMP> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLotesMP mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_lotesMP.idCodigo lmp_id, ");
			cadenaSQL.append(" qc_lotesMP.hora lmp_lin, ");
			cadenaSQL.append(" qc_lotesMP.hora lmp_hor, ");
			cadenaSQL.append(" qc_lotesMP.cantidad lmp_can, ");
			cadenaSQL.append(" qc_lotesMP.realizado lmp_rea, ");
			cadenaSQL.append(" qc_lotesMP.verificado lmp_ver, ");
			cadenaSQL.append(" qc_lotesMP.fecha lmp_fec, ");
			cadenaSQL.append(" qc_lotesMP.observaciones lmp_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_lotesMP ");
			
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLotesMP>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLotesMP();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lmp_id"));
					mapeo.setLinea(r_linea);
					mapeo.setHora(rsOpcion.getString("lmp_hor"));
					mapeo.setCantidad(rsOpcion.getInt("lmp_can"));
					mapeo.setRealizado(rsOpcion.getString("lmp_rea"));
					mapeo.setVerificado(rsOpcion.getString("lmp_ver"));
					mapeo.setObservaciones(rsOpcion.getString("lmp_obs"));
					mapeo.setFecha(rsOpcion.getDate("lmp_fec"));					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
			
		
		return vector;
	}

	public ArrayList<MapeoControlLineasLotesMP> datosOpcionesGlobalLineas(Integer r_id, MapeoControlLotesMP r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlLineasLotesMP> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLineasLotesMP mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tablaLineas = "qc_lineas_lote_mat_aux_env";
			tabla = "qc_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineaslotesMP.idLoteMatAux ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			tablaLineas = "qc_lineas_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineaslotesMP.idLoteMatAux ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			tablaLineas = "qc_lineas_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineaslotesMP.idLoteMatAux ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineaslotesMP.idCodigo llmp_id, ");
			cadenaSQL.append(" qc_lineaslotesMP.material llmp_mat, ");
			cadenaSQL.append(" qc_lineaslotesMP.descripcion llmp_des, ");
			cadenaSQL.append(" qc_lineaslotesMP.proveedor llmp_pro, ");
			cadenaSQL.append(" qc_lineaslotesMP.lote llmp_lot, ");
			cadenaSQL.append(" qc_lineaslotesMP.fecha_fabricacion llmp_ff, ");
			cadenaSQL.append(" qc_lineaslotesMP.cantidad llmp_can, ");
			cadenaSQL.append(" qc_lineaslotesMP.idLoteMatAux llmp_lmp ");
			
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineaslotesMP ");
			
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_lineaslotesMP.idLoteMatAux  = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLineasLotesMP>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLineasLotesMP();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("llmp_id"));
					mapeo.setCantidad(rsOpcion.getInt("llmp_can"));
					mapeo.setMaterial(rsOpcion.getString("llmp_mat"));
					mapeo.setDescripcion(rsOpcion.getString("llmp_des"));
					mapeo.setProveedor(rsOpcion.getString("llmp_pro"));
					mapeo.setLote(rsOpcion.getString("llmp_lot"));
					mapeo.setFechaFabricacion(rsOpcion.getDate("llmp_ff"));
					mapeo.setIdLoteMP(rsOpcion.getInt("llmp_lmp"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return vector;
	}
	
	
	public String guardarNuevoLotesMP(MapeoControlLotesMP r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
		}

		try
		{
			
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".linea , ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".cantidad , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
				
			}
			else
			{
				cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
				cadenaSQL.append(tabla + ".idCodigo, ");
				cadenaSQL.append(tabla + ".linea , ");
				cadenaSQL.append(tabla + ".hora, ");
				cadenaSQL.append(tabla + ".cantidad , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".verificado, ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idProgramacion , ");
				cadenaSQL.append(tabla + ".fecha ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }

		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(9, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
	        preparedStatement.executeUpdate();
	        this.generarLineasLoteMP(r_mapeo.getIdCodigo(), r_mapeo.getLinea());
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public String generarLineasLoteMP(Integer r_id, String r_linea)
	{
		String rdo=null;
		MapeoControlLineasLotesMP mapeo = null;
		
		
		if (r_linea.toUpperCase().contentEquals("EMBOTELLADORA"))
		{
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("01 NITROGENO");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);

			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("02 BOTELLA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("03 CORCHO");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("04 TAPON ROSCA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("05 CAPSULA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("06 ETIQUETA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("07 CONTRA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("08 TIRILLA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("09 MEDALLA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("10 CAJA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
		}
		else if (r_linea.toUpperCase().contentEquals("ENVASADORA"))
		{
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("01 GARRAFA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("02 TAPON");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("03 ETIQUETA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("04 CAJA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("05 BANDEJA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
		}
		else if (r_linea.toUpperCase().contentEquals("BIB"))
		{
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("01 NITROGENO");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("02 CARBONICO");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("03 BOLSA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("04 CAJA IND");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasLotesMP();
			mapeo.setMaterial("05 CAJA");
			mapeo.setDescripcion("");
			mapeo.setProveedor("");
			mapeo.setLote("");
			mapeo.setFechaFabricacion(null);
			mapeo.setCantidad(0);
			mapeo.setIdLoteMP(r_id);
			
			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
			
		}

		return rdo;
	}
	
	public String guardarCambiosLotesMP(MapeoControlLotesMP r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
		}
		try
		{
			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".linea =?, ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".cantidad=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
			else
			{
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".linea =?, ");
				cadenaSQL.append(tabla + ".hora=?, ");
				cadenaSQL.append(tabla + ".cantidad=?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".verificado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idProgramacion =? ");
				cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }

		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    
		    preparedStatement.setInt(8, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlLotesMP r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		String tablaLineas = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
			tablaLineas = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			tablaLineas = "qc_lineas_lote_mat_aux_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			tablaLineas = "qc_lineas_lote_mat_aux_bib";
		}

		try
		{
			con= this.conManager.establecerConexionInd();
			
			cadenaSQL.append(" DELETE FROM " + tablaLineas );            
			cadenaSQL.append(" WHERE " + tablaLineas + ".idLoteMatAux = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_lotesMP.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_lotesMP.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_lotesMP.idCodigo lmp_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_lotesMP ");
	     	
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("lmp_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	/*
	 * METODOS LINEAS LOTES MP
	 */
	
	
	

	public String guardarNuevoLineasLoteMP(String r_linea, MapeoControlLineasLotesMP r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_lote_mat_aux_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".material, ");
			cadenaSQL.append(tabla + ".descripcion, ");
			cadenaSQL.append(tabla + ".proveedor , ");
			cadenaSQL.append(tabla + ".lote , ");
			cadenaSQL.append(tabla + ".cantidad, ");
			cadenaSQL.append(tabla + ".idLoteMatAux , ");
			cadenaSQL.append(tabla + ".fecha_fabricacion ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			if (r_mapeo.getMaterial()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getMaterial());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getProveedor()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getProveedor());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getLote()!=null)
			{
				preparedStatement.setString(5, r_mapeo.getLote());
			}
			else
			{
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getCantidad()!=null)
			{
				preparedStatement.setInt(6, r_mapeo.getCantidad());
			}
			else
			{
				preparedStatement.setInt(6, 0);
			}
			if (r_mapeo.getIdLoteMP()!=null)
			{
				preparedStatement.setInt(7, r_mapeo.getIdLoteMP());
			}
			else
			{
				preparedStatement.setInt(7, 0);
			}
			if (r_mapeo.getFechaFabricacion()!=null)
			{
				preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaFabricacion())));
			}
			else
			{
				preparedStatement.setString(8, null);
			}
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String guardarCambiosLineasLoteMP(String r_linea, MapeoControlLineasLotesMP r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_lote_mat_aux_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
			
			cadenaSQL.append(" update " + tabla + " SET " );
			cadenaSQL.append(tabla + ".material=?, ");
			cadenaSQL.append(tabla + ".descripcion=?, ");
			cadenaSQL.append(tabla + ".proveedor=? , ");
			cadenaSQL.append(tabla + ".lote=? , ");
			cadenaSQL.append(tabla + ".cantidad=?, ");
			cadenaSQL.append(tabla + ".idLoteMatAux=? , ");
			cadenaSQL.append(tabla + ".fecha_fabricacion=? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			
			if (r_mapeo.getMaterial()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getMaterial());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getProveedor()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getProveedor());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getLote()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getLote());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getCantidad()!=null)
			{
				preparedStatement.setInt(5, r_mapeo.getCantidad());
			}
			else
			{
				preparedStatement.setInt(5, 0);
			}
			if (r_mapeo.getIdLoteMP()!=null)
			{
				preparedStatement.setInt(6, r_mapeo.getIdLoteMP());
			}
			else
			{
				preparedStatement.setInt(6, 0);
			}
			if (r_mapeo.getFechaFabricacion()!=null)
			{
				preparedStatement.setString(7, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaFabricacion())));
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			preparedStatement.setInt(8, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lineas_lote_mat_aux_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_lote_mat_aux_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lineas_lote_mat_aux_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
}