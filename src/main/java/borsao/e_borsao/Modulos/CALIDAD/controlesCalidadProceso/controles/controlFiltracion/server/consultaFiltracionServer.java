package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.modelo.MapeoControlFiltracion;

public class consultaFiltracionServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaFiltracionServer  instance;
	
	public consultaFiltracionServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaFiltracionServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaFiltracionServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlFiltracion datosOpcionesGlobal(MapeoControlFiltracion r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlFiltracion mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_filtro.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{

			if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.nitrogeno fil_nit, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
				
			}
			else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.integridad fil_ein, ");
				cadenaSQL.append(" qc_filtro.valor_integridad fil_vin, ");
				cadenaSQL.append(" qc_filtro.ph fil_ph, ");
				cadenaSQL.append(" qc_filtro.valor_ph fil_vph, ");
				cadenaSQL.append(" qc_filtro.peracetico fil_per, ");
				cadenaSQL.append(" qc_filtro.valor_peracetico fil_vpe, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
				
			}
			else
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.integridad fil_ein, ");
				cadenaSQL.append(" qc_filtro.valor_integridad fil_vin, ");
				cadenaSQL.append(" qc_filtro.conductividad fil_ecn, ");
				cadenaSQL.append(" qc_filtro.valor_conductividad fil_vcn, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
			}

			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_filtro.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlFiltracion();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("fil_id"));
					mapeo.setHora(rsOpcion.getString("fil_hora"));
					mapeo.setIdTurno(rsOpcion.getInt("fil_idt"));
					
					if (r_mapeo.getLinea().contentEquals("BIB"))
					{
						
						mapeo.setNitrogeno(rsOpcion.getString("fil_nit"));
					}
					else  if (r_mapeo.getLinea().contentEquals("Embotelladora"))
					{
						
						mapeo.setPh(rsOpcion.getString("fil_ph"));
						mapeo.setValorPh(rsOpcion.getDouble("fil_vph"));
						mapeo.setPeracetico(rsOpcion.getString("fil_per"));
						mapeo.setValorPeracetico(rsOpcion.getDouble("fil_vpe"));
						mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
						mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
					}
					else
					{
						mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
						mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
						mapeo.setEstadoConductividad(rsOpcion.getString("fil_ecn"));
						mapeo.setValorConductividad(rsOpcion.getDouble("fil_vcn"));
					}
					
					mapeo.setTipoLimpieza(rsOpcion.getString("fil_tip"));
					mapeo.setRealizado(rsOpcion.getString("fil_rea"));
					mapeo.setVerificado(rsOpcion.getString("fil_ver"));
					mapeo.setObservaciones(rsOpcion.getString("fil_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlFiltracion> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlFiltracion> vector = null;
		MapeoControlFiltracion mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_filtro.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}

		if (tabla!=null && tabla.length()>0)
		{
		
			if (r_linea.contentEquals("BIB"))
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.nitrogeno fil_nit, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
				
			}
			else if (r_linea.contentEquals("Embotelladora"))
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.integridad fil_ein, ");
				cadenaSQL.append(" qc_filtro.valor_integridad fil_vin, ");
				cadenaSQL.append(" qc_filtro.ph fil_ph, ");
				cadenaSQL.append(" qc_filtro.valor_ph fil_vph, ");
				cadenaSQL.append(" qc_filtro.peracetico fil_per, ");
				cadenaSQL.append(" qc_filtro.valor_peracetico fil_vpe, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
				
			}
			else
			{
				cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id, ");
				cadenaSQL.append(" qc_filtro.hora fil_hora, ");
				cadenaSQL.append(" qc_filtro.integridad fil_ein, ");
				cadenaSQL.append(" qc_filtro.valor_integridad fil_vin, ");
				cadenaSQL.append(" qc_filtro.conductividad fil_ecn, ");
				cadenaSQL.append(" qc_filtro.valor_conductividad fil_vcn, ");
				cadenaSQL.append(" qc_filtro.tipoLimpieza fil_tip, ");
				cadenaSQL.append(" qc_filtro.realizado fil_rea, ");
				cadenaSQL.append(" qc_filtro.verificado fil_ver, ");
				cadenaSQL.append(" qc_filtro.observaciones fil_obs, ");
				cadenaSQL.append(" qc_filtro.idTurno fil_idt," );
				cadenaSQL.append(" qc_filtro.fecha tur_fec ");
				
				cadenaSQL.append(" FROM " + tabla + " qc_filtro ");
			}

			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}

				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlFiltracion>();
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlFiltracion();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("fil_id"));
					mapeo.setHora(rsOpcion.getString("fil_hora"));
					mapeo.setIdTurno(rsOpcion.getInt("fil_idt"));
					
					if (r_linea.contentEquals("BIB"))
					{
						
						mapeo.setNitrogeno(rsOpcion.getString("fil_nit"));
					}
					else  if (r_linea.contentEquals("Embotelladora"))
					{
						
						mapeo.setPh(rsOpcion.getString("fil_ph"));
						mapeo.setValorPh(rsOpcion.getDouble("fil_vph"));
						mapeo.setPeracetico(rsOpcion.getString("fil_per"));
						mapeo.setValorPeracetico(rsOpcion.getDouble("fil_vpe"));
						mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
						mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
					}
					else
					{
						mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
						mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
						mapeo.setEstadoConductividad(rsOpcion.getString("fil_ecn"));
						mapeo.setValorConductividad(rsOpcion.getDouble("fil_vcn"));
					}
					
					mapeo.setTipoLimpieza(rsOpcion.getString("fil_tip"));
					mapeo.setRealizado(rsOpcion.getString("fil_rea"));
					mapeo.setVerificado(rsOpcion.getString("fil_ver"));
					mapeo.setObservaciones(rsOpcion.getString("fil_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
					vector.add(mapeo);
				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	
	public String guardarNuevo(MapeoControlFiltracion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		

		try
		{
			if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
				tabla = "qc_filtro_env";
				
				cadenaSQL.append(" INSERT INTO " + tabla + "( ");
				
				cadenaSQL.append(tabla + ".idCodigo , ");
				cadenaSQL.append(tabla + ".hora , ");
				cadenaSQL.append(tabla + ".integridad , ");
				cadenaSQL.append(tabla + ".valor_integridad , ");
				cadenaSQL.append(tabla + ".conductividad , ");
				cadenaSQL.append(tabla + ".valor_conductividad , ");
				cadenaSQL.append(tabla + ".tipoLimpieza , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idTurno , ");
				cadenaSQL.append(tabla + ".idProgramacion, ");
				cadenaSQL.append(tabla + ".fecha , ");
				cadenaSQL.append(tabla + ".verificado ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			    
			    con= this.conManager.establecerConexionInd();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
			    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getEstadoIntegridad()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getEstadoIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    if (r_mapeo.getValorIntegridad()!=null)
			    {
			    	preparedStatement.setDouble(4, r_mapeo.getValorIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(4, 0);
			    }
			    if (r_mapeo.getEstadoConductividad()!=null)
			    {
			    	preparedStatement.setString(5, r_mapeo.getEstadoConductividad());
			    }
			    else
			    {
			    	preparedStatement.setString(5, null);
			    }
			    if (r_mapeo.getValorConductividad()!=null)
			    {
			    	preparedStatement.setDouble(6, r_mapeo.getValorConductividad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(6, 99999);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(7, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(7, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(8, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(8, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(9, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(9, null);
			    }
			    preparedStatement.setInt(10, 0);
			    
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(11, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(11, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(12, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(12, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
			    {
			    	preparedStatement.setString(13, r_mapeo.getVerificado());
			    }
			    else
			    {
			    	preparedStatement.setString(13, null);
			    }
		        preparedStatement.executeUpdate();
			}
			else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				tabla = "qc_filtro_emb";
				cadenaSQL.append(" INSERT INTO " + tabla + "( ");
				
				cadenaSQL.append(tabla + ".idCodigo , ");
				cadenaSQL.append(tabla + ".hora , ");
				cadenaSQL.append(tabla + ".ph , ");
				cadenaSQL.append(tabla + ".valor_ph , ");
				cadenaSQL.append(tabla + ".peracetico, ");
				cadenaSQL.append(tabla + ".valor_peracetico, ");
				cadenaSQL.append(tabla + ".integridad , ");
				cadenaSQL.append(tabla + ".valor_integridad , ");
				cadenaSQL.append(tabla + ".tipoLimpieza , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idTurno , ");
				cadenaSQL.append(tabla + ".idProgramacion, ");
				cadenaSQL.append(tabla + ".fecha , ");
				cadenaSQL.append(tabla + ".verificado ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			    
			    con= this.conManager.establecerConexionInd();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
			    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getPh()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getPh());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    if (r_mapeo.getValorPh()!=null)
			    {
			    	preparedStatement.setDouble(4, r_mapeo.getValorPh());
			    }
			    else
			    {
			    	preparedStatement.setDouble(4, 99999);
			    }

			    if (r_mapeo.getPeracetico()!=null)
			    {
			    	preparedStatement.setString(5, r_mapeo.getPeracetico());
			    }
			    else
			    {
			    	preparedStatement.setString(5, null);
			    }
			    if (r_mapeo.getValorPeracetico()!=null)
			    {
			    	preparedStatement.setDouble(6, r_mapeo.getValorPeracetico());
			    }
			    else
			    {
			    	preparedStatement.setDouble(6, 99999);
			    }
			    if (r_mapeo.getEstadoIntegridad()!=null)
			    {
			    	preparedStatement.setString(7, r_mapeo.getEstadoIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setString(7, null);
			    }
			    if (r_mapeo.getValorIntegridad()!=null)
			    {
			    	preparedStatement.setDouble(8, r_mapeo.getValorIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(8, 0);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(9, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(9, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(10, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(10, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(11, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(11, null);
			    }
			    preparedStatement.setInt(12, 0);
			    
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(13, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(13, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(14, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(14, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
			    {
			    	preparedStatement.setString(15, r_mapeo.getVerificado());
			    }
			    else
			    {
			    	preparedStatement.setString(15, null);
			    }
		        preparedStatement.executeUpdate();
			}
			else if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				tabla = "qc_filtro_bib";
				cadenaSQL.append(" INSERT INTO " + tabla + "( ");
				
				cadenaSQL.append(tabla + ".idCodigo , ");
				cadenaSQL.append(tabla + ".hora , ");
				cadenaSQL.append(tabla + ".nitrogeno , ");
				cadenaSQL.append(tabla + ".tipoLimpieza , ");
				cadenaSQL.append(tabla + ".realizado , ");
				cadenaSQL.append(tabla + ".observaciones , ");
				cadenaSQL.append(tabla + ".idTurno , ");
				cadenaSQL.append(tabla + ".idProgramacion, ");
				cadenaSQL.append(tabla + ".fecha , ");
				cadenaSQL.append(tabla + ".verificado ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
			    
			    con= this.conManager.establecerConexionInd();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
			    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getNitrogeno()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getNitrogeno());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(4, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(4, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(5, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(5, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(6, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    preparedStatement.setInt(7, 0);
			    
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(8, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(8, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(9, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(9, null);
		    	}
		    	if (r_mapeo.getVerificado()!=null)
			    {
			    	preparedStatement.setString(10, r_mapeo.getVerificado());
			    }
			    else
			    {
			    	preparedStatement.setString(10, null);
			    }
		        preparedStatement.executeUpdate();
			}

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }      
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoControlFiltracion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		
		try
		{
			
			if (r_mapeo.getLinea().contentEquals("Envasadora"))
			{
				tabla = "qc_filtro_env";
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora =?, ");
				cadenaSQL.append(tabla + ".integridad =?, ");
				cadenaSQL.append(tabla + ".valor_integridad =?, ");
				cadenaSQL.append(tabla + ".conductividad =?, ");
				cadenaSQL.append(tabla + ".valor_conductividad =?, ");
				cadenaSQL.append(tabla + ".tipoLimpieza =?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idTurno =?, " );
				cadenaSQL.append(tabla + ".idProgramacion =?, " );
				cadenaSQL.append(tabla + ".fecha =? " );
				
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getEstadoIntegridad()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getEstadoIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getValorIntegridad()!=null)
			    {
			    	preparedStatement.setDouble(3, r_mapeo.getValorIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(3, 0);
			    }
			    if (r_mapeo.getEstadoConductividad()!=null)
			    {
			    	preparedStatement.setString(4, r_mapeo.getEstadoConductividad());
			    }
			    else
			    {
			    	preparedStatement.setString(4, null);
			    }
			    if (r_mapeo.getValorConductividad()!=null)
			    {
			    	preparedStatement.setDouble(5, r_mapeo.getValorConductividad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(5, 0);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(6, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(7, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(7, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(8, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(8, null);
			    }
			    preparedStatement.setInt(9, 0);
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(10, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(10, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(11, null);
		    	}
		    	
			    preparedStatement.setInt(12, r_mapeo.getIdCodigo());
		        preparedStatement.executeUpdate();
			}
			else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				tabla = "qc_filtro_emb";
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora =?, ");
				cadenaSQL.append(tabla + ".ph =?, ");
				cadenaSQL.append(tabla + ".valor_ph =?, ");
				cadenaSQL.append(tabla + ".peracetico =?, ");
				cadenaSQL.append(tabla + ".valor_peracetico =?, ");
				cadenaSQL.append(tabla + ".integridad =?, ");
				cadenaSQL.append(tabla + ".valor_integridad =?, ");
				cadenaSQL.append(tabla + ".tipoLimpieza =?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idTurno =?, " );
				cadenaSQL.append(tabla + ".idProgramacion =?, " );
				cadenaSQL.append(tabla + ".fecha =? " );
				
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getPh()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getPh());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getValorPh()!=null)
			    {
			    	preparedStatement.setDouble(3, r_mapeo.getValorPh());
			    }
			    else
			    {
			    	preparedStatement.setDouble(3, 0);
			    }

			    if (r_mapeo.getPeracetico()!=null)
			    {
			    	preparedStatement.setString(4, r_mapeo.getPeracetico());
			    }
			    else
			    {
			    	preparedStatement.setString(4, null);
			    }
			    if (r_mapeo.getValorPeracetico()!=null)
			    {
			    	preparedStatement.setDouble(5, r_mapeo.getValorPeracetico());
			    }
			    else
			    {
			    	preparedStatement.setDouble(5, 0);
			    }
			    if (r_mapeo.getEstadoIntegridad()!=null)
			    {
			    	preparedStatement.setString(6, r_mapeo.getEstadoIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    if (r_mapeo.getValorIntegridad()!=null)
			    {
			    	preparedStatement.setDouble(7, r_mapeo.getValorIntegridad());
			    }
			    else
			    {
			    	preparedStatement.setDouble(7, 0);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(8, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(8, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(9, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(9, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(10, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(10, null);
			    }
			    preparedStatement.setInt(11, 0);
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(12, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(12, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(13, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(13, null);
		    	}
		    	
			    preparedStatement.setInt(14, r_mapeo.getIdCodigo());
		        preparedStatement.executeUpdate();
			}
			else if (r_mapeo.getLinea().contentEquals("BIB"))
			{
				tabla = "qc_filtro_bib";
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".hora =?, ");
				cadenaSQL.append(tabla + ".nitrogeno =?, ");
				cadenaSQL.append(tabla + ".tipoLimpieza =?, ");
				cadenaSQL.append(tabla + ".realizado =?, ");
				cadenaSQL.append(tabla + ".observaciones =?, ");
				cadenaSQL.append(tabla + ".idTurno =?, " );
				cadenaSQL.append(tabla + ".idProgramacion =?, " );
				cadenaSQL.append(tabla + ".fecha =? " );
				
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getHora()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getHora());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getNitrogeno()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getNitrogeno());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getTipoLimpieza()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getTipoLimpieza());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    if (r_mapeo.getRealizado()!=null)
			    {
			    	preparedStatement.setString(4, r_mapeo.getRealizado());
			    }
			    else
			    {
			    	preparedStatement.setString(4, null);
			    }
			    if (r_mapeo.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(5, r_mapeo.getObservaciones());
			    }
			    else
			    {
			    	preparedStatement.setString(5, null);
			    }
			    preparedStatement.setInt(6, 0);
			    if (r_mapeo.getIdProgramacion()!=null)
			    {
			    	preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
			    }
			    else
			    {
			    	preparedStatement.setInt(7, 0);
			    }
		    	if (r_mapeo.getFecha()!=null)
		    	{
		    		preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    	}
		    	else
		    	{
		    		preparedStatement.setString(8, null);
		    	}
		    	
			    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
		        preparedStatement.executeUpdate();
			}

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoControlFiltracion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
		}

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_filtro.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_filtro.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_filtro.idCodigo fil_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_filtro ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());

	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("fil_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_filtro_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_filtro_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_filtro_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
}