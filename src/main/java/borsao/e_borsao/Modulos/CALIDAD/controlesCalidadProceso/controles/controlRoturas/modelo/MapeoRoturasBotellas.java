package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.modelo;

import java.sql.Time;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoRoturasBotellas extends MapeoGlobal
{
	private String linea;
    private Double unidades;
    private Integer tiempo;
    private Date fecha;

	private String zona;
    private String ubicacion;
    private String limpiador;
    private String verificador;
    
    private String observaciones;
    private String verificado;
	private Integer idProgramacion;
	
	public MapeoRoturasBotellas()
	{
	}

	public Double getUnidades() {
		return unidades;
	}

	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}

	public Integer getTiempo() {
		return tiempo;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getLimpiador() {
		return limpiador;
	}

	public void setLimpiador(String limpiador) {
		this.limpiador = limpiador;
	}

	public String getVerificador() {
		return verificador;
	}

	public void setVerificador(String verificador) {
		this.verificador = verificador;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}
	
}