package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.modelo.MapeoControlLineasRosca;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.modelo.MapeoControlRosca;

public class consultaRoscaServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaRoscaServer  instance;
	
	public consultaRoscaServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRoscaServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRoscaServer (r_usuario);			
		}
		return instance;
	}


	public ArrayList<MapeoControlRosca> datosOpcionesGlobal(MapeoControlRosca r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoControlRosca> vector = null;
		MapeoControlRosca mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_rosca.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_rosca.idCodigo ros_id, ");
			cadenaSQL.append(" qc_rosca.linea ros_lin, ");
			cadenaSQL.append(" qc_rosca.hora ros_hor, ");
			cadenaSQL.append(" qc_rosca.realizado ros_rea, ");
			cadenaSQL.append(" qc_rosca.verificado ros_ver, ");
			cadenaSQL.append(" qc_rosca.fecha ros_fec, ");
			cadenaSQL.append(" qc_rosca.observaciones ros_obs ");
			
			cadenaSQL.append(" FROM " + tabla + " qc_rosca ");
			
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_rosca.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				vector = new ArrayList<MapeoControlRosca>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlRosca();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("ros_id"));
					mapeo.setLinea(r_mapeo.getLinea());
					mapeo.setHora(rsOpcion.getString("ros_hor"));
					mapeo.setRealizado(rsOpcion.getString("ros_rea"));
					mapeo.setVerificado(rsOpcion.getString("ros_ver"));
					mapeo.setObservaciones(rsOpcion.getString("ros_obs"));
					mapeo.setFecha(rsOpcion.getDate("ros_fec"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	public ArrayList<MapeoControlRosca> datosOpcionesGlobal(String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlRosca> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlRosca mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_rosca.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_rosca.idCodigo ros_id, ");
			cadenaSQL.append(" qc_rosca.hora ros_lin, ");
			cadenaSQL.append(" qc_rosca.hora ros_hor, ");
			cadenaSQL.append(" qc_rosca.realizado ros_rea, ");
			cadenaSQL.append(" qc_rosca.verificado ros_ver, ");
			cadenaSQL.append(" qc_rosca.fecha ros_fec, ");
			cadenaSQL.append(" qc_rosca.observaciones ros_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_rosca ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlRosca>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlRosca();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("ros_id"));
					mapeo.setLinea(r_linea);
					mapeo.setHora(rsOpcion.getString("ros_hor"));
					mapeo.setRealizado(rsOpcion.getString("ros_rea"));
					mapeo.setVerificado(rsOpcion.getString("ros_ver"));
					mapeo.setObservaciones(rsOpcion.getString("ros_obs"));
					mapeo.setFecha(rsOpcion.getDate("ros_fec"));					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}

	public ArrayList<MapeoControlLineasRosca> datosOpcionesGlobalLineas(Integer r_id, MapeoControlRosca r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlLineasRosca> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLineasRosca mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasRosca.idRosca ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			tablaLineas = "qc_lineas_rosca_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasRosca.idRosca ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasRosca.idRosca ");
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasRosca.idCodigo lros_id, ");
			cadenaSQL.append(" qc_lineasRosca.material lros_mat, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal1 lros_c1, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal2 lros_c2, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal3 lros_c3, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal4 lros_c4, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal5 lros_c5, ");
			cadenaSQL.append(" qc_lineasRosca.cabezal6 lros_c6, ");
			cadenaSQL.append(" qc_lineasRosca.idRosca lros_ros ");
			
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasRosca ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_lineasRosca.idRosca  = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLineasRosca>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLineasRosca();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lros_id"));
					mapeo.setMaterial(rsOpcion.getString("lros_mat"));
					mapeo.setCabezal1(rsOpcion.getString("lros_c1"));
					mapeo.setCabezal2(rsOpcion.getString("lros_c2"));
					mapeo.setCabezal3(rsOpcion.getString("lros_c3"));
					mapeo.setCabezal4(rsOpcion.getString("lros_c4"));
					mapeo.setCabezal5(rsOpcion.getString("lros_c5"));
					mapeo.setCabezal6(rsOpcion.getString("lros_c6"));
					mapeo.setIdRosca(rsOpcion.getInt("lros_ros"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return vector;
	}
	
	
	public String guardarNuevoRosca(MapeoControlRosca r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}

		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".linea , ");
			cadenaSQL.append(tabla + ".hora, ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".verificado, ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion , ");
			cadenaSQL.append(tabla + ".fecha ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
	        preparedStatement.executeUpdate();
	        this.generarLineasRosca(r_mapeo.getIdCodigo(), r_mapeo.getLinea());
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public String generarLineasRosca(Integer r_id, String r_linea)
	{
		String rdo=null;
		MapeoControlLineasRosca mapeo = null;
		
		if (r_linea.toUpperCase().contentEquals("EMBOTELLADORA"))
		{
			mapeo = new MapeoControlLineasRosca();
			mapeo.setMaterial("01 Torque Apertura");
			mapeo.setCabezal1("");
			mapeo.setCabezal2("");
			mapeo.setCabezal3("");
			mapeo.setCabezal4("");
			mapeo.setCabezal5("");
			mapeo.setCabezal6("");
			mapeo.setIdRosca(r_id);
			
			this.guardarNuevoLineasRosca(r_linea, mapeo);

			mapeo = new MapeoControlLineasRosca();
			mapeo.setMaterial("02 Torque Cierre");
			mapeo.setCabezal1("");
			mapeo.setCabezal2("");
			mapeo.setCabezal3("");
			mapeo.setCabezal4("");
			mapeo.setCabezal5("");
			mapeo.setCabezal6("");
			mapeo.setIdRosca(r_id);
			
			this.guardarNuevoLineasRosca(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasRosca();
			mapeo.setMaterial("03 Cuello y Saranex");
			mapeo.setCabezal1("");
			mapeo.setCabezal2("");
			mapeo.setCabezal3("");
			mapeo.setCabezal4("");
			mapeo.setCabezal5("");
			mapeo.setCabezal6("");
			mapeo.setIdRosca(r_id);
			
			this.guardarNuevoLineasRosca(r_linea, mapeo);
			
		}
//		else if (r_linea.toUpperCase().contentEquals("ENVASADORA"))
//		{
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("01 GARRAFA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("02 TAPON");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("03 ETIQUETA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("04 CAJA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("05 BANDEJA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//		}
//		else if (r_linea.toUpperCase().contentEquals("BIB"))
//		{
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("01 NITROGENO");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("02 CARBONICO");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("03 BOLSA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("04 CAJA IND");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//			mapeo = new MapeoControlLineasRosca();
//			mapeo.setMaterial("05 CAJA");
//			mapeo.setDescripcion("");
//			mapeo.setProveedor("");
//			mapeo.setLote("");
//			mapeo.setFechaFabricacion(null);
//			mapeo.setCantidad(0);
//			mapeo.setIdLoteMP(r_id);
//			
//			this.guardarNuevoLineasLoteMP(r_linea, mapeo);
//			
//		}

		return rdo;
	}
	
	public String guardarCambiosRosca(MapeoControlRosca r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".linea =?, ");
			cadenaSQL.append(tabla + ".hora=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".verificado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlRosca r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		String tablaLineas = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			tablaLineas = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			tablaLineas = "qc_lineas_rosca_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			tablaLineas = "qc_lineas_lote_mat_aux_bib";
		}

		try
		{
			con= this.conManager.establecerConexionInd();
			
			cadenaSQL.append(" DELETE FROM " + tablaLineas );            
			cadenaSQL.append(" WHERE " + tablaLineas + ".idRosca = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_rosca.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rosca.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_rosca.idCodigo ros_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_rosca ");
	     	
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("ros_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	/*
	 * METODOS LINEAS LOTES MP
	 */
	
	
	

	public String guardarNuevoLineasRosca(String r_linea, MapeoControlLineasRosca r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_rosca_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".material, ");
			cadenaSQL.append(tabla + ".cabezal1, ");
			cadenaSQL.append(tabla + ".cabezal2, ");
			cadenaSQL.append(tabla + ".cabezal3, ");
			cadenaSQL.append(tabla + ".cabezal4, ");
			cadenaSQL.append(tabla + ".cabezal5, ");
			cadenaSQL.append(tabla + ".cabezal6, ");
			cadenaSQL.append(tabla + ".idRosca ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			if (r_mapeo.getMaterial()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getMaterial());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getCabezal1()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getCabezal1());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getCabezal2()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getCabezal2());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getCabezal3()!=null)
			{
				preparedStatement.setString(5, r_mapeo.getCabezal3());
			}
			else
			{
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getCabezal4()!=null)
			{
				preparedStatement.setString(6, r_mapeo.getCabezal4());
			}
			else
			{
				preparedStatement.setString(6, null);
			}
			if (r_mapeo.getCabezal5()!=null)
			{
				preparedStatement.setString(7, r_mapeo.getCabezal5());
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getCabezal6()!=null)
			{
				preparedStatement.setString(8, r_mapeo.getCabezal6());
			}
			else
			{
				preparedStatement.setString(8, null);
			}
			if (r_mapeo.getIdRosca()!=null)
			{
				preparedStatement.setInt(9, r_mapeo.getIdRosca());
			}
			else
			{
				preparedStatement.setInt(9, 0);
			}
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String guardarCambiosLineasLoteMP(String r_linea, Integer r_idRosca, HashMap<String, String> r_valores)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lineas_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_rosca_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lineas_lote_mat_aux_bib";
		}
		
		try
		{
//			cadenaSQL.append(" update " + tabla + " SET " );
//			cadenaSQL.append(tabla + ".material=?, ");
//			cadenaSQL.append(tabla + ".cabezal1=?, ");
//			cadenaSQL.append(tabla + ".cabezal2=?, ");
//			cadenaSQL.append(tabla + ".cabezal3=?, ");
//			cadenaSQL.append(tabla + ".cabezal4=?, ");
//			cadenaSQL.append(tabla + ".cabezal5=?, ");
//			cadenaSQL.append(tabla + ".cabezal6=?, ");
//			cadenaSQL.append(tabla + ".idRosca=? ");
//			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			
			con= this.conManager.establecerConexionInd();	
			
			for (int i = 1; i < r_valores.size(); i++)
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" update " + tabla + " SET " );
				cadenaSQL.append(tabla + "." + r_valores.get("campo") + " = '" + r_valores.get(RutinasNumericas.formatearIntegerDigitosIzqda(i, 2)) + "' ");
				cadenaSQL.append(" WHERE " + tabla + ".idRosca= " + r_idRosca);
				cadenaSQL.append(" and 	material like '" + RutinasNumericas.formatearIntegerDigitosIzqda(i, 2) + "%' ");
				preparedStatement = con.prepareStatement(cadenaSQL.toString());
				preparedStatement.executeUpdate();
			}
//
//			
//			con= this.conManager.establecerConexionInd();	
//			preparedStatement = con.prepareStatement(cadenaSQL.toString());
//			
//			if (r_mapeo.getMaterial()!=null)
//			{
//				preparedStatement.setString(1, r_mapeo.getMaterial());
//			}
//			else
//			{
//				preparedStatement.setString(1, null);
//			}
//			if (r_mapeo.getCabezal1()!=null)
//			{
//				preparedStatement.setString(2, r_mapeo.getCabezal1());
//			}
//			else
//			{
//				preparedStatement.setString(2, null);
//			}
//			if (r_mapeo.getCabezal2()!=null)
//			{
//				preparedStatement.setString(3, r_mapeo.getCabezal2());
//			}
//			else
//			{
//				preparedStatement.setString(3, null);
//			}
//			if (r_mapeo.getCabezal3()!=null)
//			{
//				preparedStatement.setString(4, r_mapeo.getCabezal3());
//			}
//			else
//			{
//				preparedStatement.setString(4, null);
//			}
//			if (r_mapeo.getCabezal4()!=null)
//			{
//				preparedStatement.setString(5, r_mapeo.getCabezal4());
//			}
//			else
//			{
//				preparedStatement.setString(5, null);
//			}
//			if (r_mapeo.getCabezal5()!=null)
//			{
//				preparedStatement.setString(6, r_mapeo.getCabezal5());
//			}
//			else
//			{
//				preparedStatement.setString(6, null);
//			}
//			if (r_mapeo.getCabezal6()!=null)
//			{
//				preparedStatement.setString(7, r_mapeo.getCabezal6());
//			}
//			else
//			{
//				preparedStatement.setString(7, null);
//			}
//			if (r_mapeo.getIdRosca()!=null)
//			{
//				preparedStatement.setInt(8, r_mapeo.getIdRosca());
//			}
//			else
//			{
//				preparedStatement.setInt(8, 0);
//			}
//			preparedStatement.setInt(9, r_mapeo.getIdCodigo());
//			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public boolean comprobarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
//			tabla = "qc_lote_mat_aux_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
//			tabla = "qc_lote_mat_aux_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_rosca.idCodigo lmp_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_rosca ");
			cadenaSQL.append(" where qc_rosca.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	
	public HashMap<String, String> recuperarDatosCabezal(Integer r_id, String r_campo, String r_linea)
	{
		ResultSet rsOpcion = null;
		HashMap<String, String> caja = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();

		if (r_linea.contentEquals("Envasadora"))
		{
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			tablaLineas = "qc_lineas_rosca_emb";
			campo= "idRosca";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasRosca.idRosca ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_linea.contentEquals("BIB"))
		{

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasRosca.material pal_mat, ");
			cadenaSQL.append(" qc_lineasRosca." + r_campo + " pal_val ");
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasRosca ");
			cadenaSQL.append(cadenaWhere.toString());
			cadenaSQL.append(" where qc_lineasRosca." + campo + " = " + r_id);
			cadenaSQL.append(" order by qc_lineasRosca.material ");
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				caja = new HashMap<String, String>();
				
				while(rsOpcion.next())
				{
					/*
					 * recojo mapeo operarios
					 */
					caja.put(rsOpcion.getString("pal_mat").substring(0, 2), rsOpcion.getString("pal_val"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return caja;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_rosca_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_rosca_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_rosca_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}