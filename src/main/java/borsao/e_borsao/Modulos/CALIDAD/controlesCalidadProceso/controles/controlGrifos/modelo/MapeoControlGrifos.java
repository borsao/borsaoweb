package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlGrifos extends MapeoGlobal
{
	private String linea;
	private Integer idProgramacion;
	private Integer idTurno;
	private String hora;
	private String cuello;
	private String filtro;
	private String grifo;
	private String realizado;
	private String verificado;
	private String observaciones;
	private Date fecha;
	
	public MapeoControlGrifos()
	{
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getCuello() {
		return cuello;
	}

	public void setCuello(String cuello) {
		this.cuello = cuello;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

	public String getGrifo() {
		return grifo;
	}

	public void setGrifo(String grifo) {
		this.grifo = grifo;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}