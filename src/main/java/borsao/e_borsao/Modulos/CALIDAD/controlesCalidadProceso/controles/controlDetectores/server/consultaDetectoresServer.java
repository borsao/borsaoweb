package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.modelo.MapeoControlDetectores;

public class consultaDetectoresServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDetectoresServer  instance;
	
	public consultaDetectoresServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDetectoresServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDetectoresServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlDetectores datosOpcionesGlobal(MapeoControlDetectores r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlDetectores mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_detectores.idProgramacion ");
		}
//		else if (r_mapeo.getLinea().contentEquals("BIB"))
//		{
//			tabla = "qc_detectores_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
//		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_detectores.idCodigo det_id, ");
			cadenaSQL.append(" qc_detectores.hora det_hora, ");
			cadenaSQL.append(" qc_detectores.realizado det_rea, ");
			cadenaSQL.append(" qc_detectores.verificado det_ver, ");
			cadenaSQL.append(" qc_detectores.observaciones det_obs, ");
			cadenaSQL.append(" qc_detectores.fecha tur_fec, ");
			cadenaSQL.append(" qc_detectores.presenciaRosca det_ros, ");
			cadenaSQL.append(" qc_detectores.presenciaFrontal det_fro, ");
			cadenaSQL.append(" qc_detectores.expulsorCajas det_exc, ");

			if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				cadenaSQL.append(" qc_detectores.nivel det_niv, ");
				cadenaSQL.append(" qc_detectores.presenciaCorcho det_cor, ");
				cadenaSQL.append(" qc_detectores.presenciaCapsula det_cap, ");
				cadenaSQL.append(" qc_detectores.presenciaContra det_con, ");
				cadenaSQL.append(" qc_detectores.presenciaTirilla det_tir, ");
				cadenaSQL.append(" qc_detectores.presenciaMedalla det_med, ");
				cadenaSQL.append(" qc_detectores.posicionTresPicos det_ptp, ");
				cadenaSQL.append(" qc_detectores.estadoCapsula det_eca, ");
			}			
			cadenaSQL.append(" qc_detectores.expulsorCajas det_exc ");
			cadenaSQL.append(" FROM " + tabla + " qc_detectores ");
			
			try
			{
				if (r_mapeo!=null)
				{
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_detectores.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlDetectores();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("det_id"));
					mapeo.setHora(rsOpcion.getString("det_hora"));
					mapeo.setRealizado(rsOpcion.getString("det_rea"));
					mapeo.setVerificado(rsOpcion.getString("det_ver"));
					mapeo.setObservaciones(rsOpcion.getString("det_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
					mapeo.setPresenciaRosca(rsOpcion.getString("det_ros"));
					mapeo.setPresenciaFrontal(rsOpcion.getString("det_fro"));
					mapeo.setExpulsorCajas(rsOpcion.getString("det_exc"));
					
					if (r_mapeo.getLinea().contentEquals("Embotelladora"))
					{
						mapeo.setNivel(rsOpcion.getString("det_niv"));
						mapeo.setPresenciaCorcho(rsOpcion.getString("det_cor"));
						mapeo.setPresenciaCapsula(rsOpcion.getString("det_cap"));
						mapeo.setPresenciaContra(rsOpcion.getString("det_con"));
						mapeo.setPresenciaTirilla(rsOpcion.getString("det_tir"));
						mapeo.setPresenciaMedalla(rsOpcion.getString("det_med"));
						mapeo.setPosicionTresPicos(rsOpcion.getString("det_ptp"));
						mapeo.setEstadoCapsula(rsOpcion.getString("det_eca"));
					}
				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlDetectores> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlDetectores> vector = null;
		MapeoControlDetectores mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_detectores.idProgramacion ");
		}
//		else if (r_linea.contentEquals("BIB"))
//		{
//			tabla = "qc_detectores_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
//		}

		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_detectores.idCodigo det_id, ");
			cadenaSQL.append(" qc_detectores.hora det_hora, ");
			cadenaSQL.append(" qc_detectores.realizado det_rea, ");
			cadenaSQL.append(" qc_detectores.verificado det_ver, ");
			cadenaSQL.append(" qc_detectores.observaciones det_obs, ");
			cadenaSQL.append(" qc_detectores.fecha tur_fec, ");
			cadenaSQL.append(" qc_detectores.presenciaRosca det_ros, ");
			cadenaSQL.append(" qc_detectores.presenciaFrontal det_fro, ");
			cadenaSQL.append(" qc_detectores.expulsorCajas det_exc, ");

			if (r_linea.contentEquals("Embotelladora"))
			{
				cadenaSQL.append(" qc_detectores.nivel det_niv, ");
				cadenaSQL.append(" qc_detectores.presenciaCorcho det_cor, ");
				cadenaSQL.append(" qc_detectores.presenciaCapsula det_cap, ");
				cadenaSQL.append(" qc_detectores.presenciaContra det_con, ");
				cadenaSQL.append(" qc_detectores.presenciaTirilla det_tir, ");
				cadenaSQL.append(" qc_detectores.presenciaMedalla det_med, ");
				cadenaSQL.append(" qc_detectores.posicionTresPicos det_ptp, ");
				cadenaSQL.append(" qc_detectores.estadoCapsula det_eca, ");
			}			
			cadenaSQL.append(" qc_detectores.expulsorCajas det_exc ");
			cadenaSQL.append(" FROM " + tabla + " qc_detectores ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}

				if (r_idProgramacion!=null && r_idProgramacion>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlDetectores>();
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlDetectores();
					mapeo.setIdCodigo(rsOpcion.getInt("det_id"));
					mapeo.setHora(rsOpcion.getString("det_hora"));
					mapeo.setRealizado(rsOpcion.getString("det_rea"));
					mapeo.setVerificado(rsOpcion.getString("det_ver"));
					mapeo.setObservaciones(rsOpcion.getString("det_obs"));
					mapeo.setFecha(rsOpcion.getDate("tur_fec"));
					mapeo.setPresenciaRosca(rsOpcion.getString("det_ros"));
					mapeo.setPresenciaFrontal(rsOpcion.getString("det_fro"));
					mapeo.setExpulsorCajas(rsOpcion.getString("det_exc"));
					
					if (r_linea.contentEquals("Embotelladora"))
					{
						mapeo.setNivel(rsOpcion.getString("det_niv"));
						mapeo.setPresenciaCorcho(rsOpcion.getString("det_cor"));
						mapeo.setPresenciaCapsula(rsOpcion.getString("det_cap"));
						mapeo.setPresenciaContra(rsOpcion.getString("det_con"));
						mapeo.setPresenciaTirilla(rsOpcion.getString("det_tir"));
						mapeo.setPresenciaMedalla(rsOpcion.getString("det_med"));
						mapeo.setPosicionTresPicos(rsOpcion.getString("det_ptp"));
						mapeo.setEstadoCapsula(rsOpcion.getString("det_eca"));
					}
					vector.add(mapeo);

				}
					
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	
	public String guardarNuevo(MapeoControlDetectores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
		}
//		else if (r_mapeo.getLinea().contentEquals("BIB"))
//		{
//			tabla = "qc_detectores_bib";
//		}
		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + "( ");
			
			cadenaSQL.append(tabla + ".idCodigo , ");
			cadenaSQL.append(tabla + ".hora , ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion, ");
			cadenaSQL.append(tabla + ".fecha , ");
			cadenaSQL.append(tabla + ".presenciaRosca , ");
			cadenaSQL.append(tabla + ".presenciaFrontal , ");
			cadenaSQL.append(tabla + ".expulsorCajas , ");

			if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				cadenaSQL.append(tabla + ".nivel , ");
				cadenaSQL.append(tabla + ".presenciaCorcho , ");
				cadenaSQL.append(tabla + ".presenciaCapsula , ");
				cadenaSQL.append(tabla + ".presenciaContra , ");
				cadenaSQL.append(tabla + ".presenciaTirilla , ");
				cadenaSQL.append(tabla + ".presenciaMedalla , ");
				cadenaSQL.append(tabla + ".posicionTresPicos , ");
				cadenaSQL.append(tabla + ".estadoCapsula , ");
			}

			cadenaSQL.append(tabla + ".verificado ) VALUES (");
			
			if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			}
			else
			{
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
			}
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(6, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
	    	}
	    	if (r_mapeo.getPresenciaRosca()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getPresenciaRosca());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
		    }
	    	if (r_mapeo.getPresenciaFrontal()!=null)
	    	{
	    		preparedStatement.setString(8, r_mapeo.getPresenciaFrontal());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(8, null);
	    	}
	    	if (r_mapeo.getExpulsorCajas()!=null)
	    	{
	    		preparedStatement.setString(9, r_mapeo.getExpulsorCajas());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(9, null);
	    	}
	    	
	    	int posicion = 0;
	    	if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
	    		if (r_mapeo.getNivel()!=null)
	    		{
	    			preparedStatement.setString(10, r_mapeo.getNivel());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(10, null);
	    		}	
	    		if (r_mapeo.getPresenciaCorcho()!=null)
		    	{
		    		preparedStatement.setString(11, r_mapeo.getPresenciaCorcho());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(11, null);
		    	}	
	    		if (r_mapeo.getPresenciaCapsula()!=null)
	    		{
	    			preparedStatement.setString(12, r_mapeo.getPresenciaCapsula());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(12, null);
	    		}	
	    		if (r_mapeo.getPresenciaContra()!=null)
	    		{
	    			preparedStatement.setString(13, r_mapeo.getPresenciaContra());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(13, null);
	    		}	
	    		if (r_mapeo.getPresenciaTirilla()!=null)
	    		{
	    			preparedStatement.setString(14, r_mapeo.getPresenciaTirilla());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(14, null);
	    		}	
	    		if (r_mapeo.getPresenciaMedalla()!=null)
	    		{
	    			preparedStatement.setString(15, r_mapeo.getPresenciaMedalla());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(15, null);
	    		}	
	    		if (r_mapeo.getPosicionTresPicos()!=null)
	    		{
	    			preparedStatement.setString(16, r_mapeo.getPosicionTresPicos());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(16, null);
	    		}	
	    		if (r_mapeo.getEstadoCapsula()!=null)
	    		{
	    			preparedStatement.setString(17, r_mapeo.getEstadoCapsula());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(17, null);
	    		}	
	    		posicion = 18;
			}
	    	else
	    	{
	    		posicion = 10;
	    	}
	    	if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(posicion, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(posicion, null);
		    }
	        preparedStatement.executeUpdate();

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }      
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoControlDetectores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_detectores_bib";
		}
		try
		{
			
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			
			cadenaSQL.append(tabla + ".hora =?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion=?, ");
			cadenaSQL.append(tabla + ".fecha =?, ");
			cadenaSQL.append(tabla + ".presenciaRosca =?, ");
			cadenaSQL.append(tabla + ".presenciaFrontal =?, ");

			if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
				cadenaSQL.append(tabla + ".nivel =?, ");
				cadenaSQL.append(tabla + ".presenciaCorcho =?, ");
				cadenaSQL.append(tabla + ".presenciaCapsula =?, ");
				cadenaSQL.append(tabla + ".presenciaContra =?, ");
				cadenaSQL.append(tabla + ".presenciaTirilla =?, ");
				cadenaSQL.append(tabla + ".presenciaMedalla =?, ");
				cadenaSQL.append(tabla + ".posicionTresPicos =?, ");
				cadenaSQL.append(tabla + ".estadoCapsula =?, ");
			}
			
			cadenaSQL.append(tabla + ".expulsorCajas =? ");
	        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(5, null);
	    	}
	    	if (r_mapeo.getPresenciaRosca()!=null)
	    	{
	    		preparedStatement.setString(6, r_mapeo.getPresenciaRosca());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(6, null);
		    }
	    	if (r_mapeo.getPresenciaFrontal()!=null)
	    	{
	    		preparedStatement.setString(7, r_mapeo.getPresenciaFrontal());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(7, null);
	    	}
	    	
	    	int posicion = 0;
	    	if (r_mapeo.getLinea().contentEquals("Embotelladora"))
			{
	    		if (r_mapeo.getNivel()!=null)
	    		{
	    			preparedStatement.setString(8, r_mapeo.getNivel());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(8, null);
	    		}	
	    		if (r_mapeo.getPresenciaCorcho()!=null)
		    	{
		    		preparedStatement.setString(9, r_mapeo.getPresenciaCorcho());
		    	}
		    	else
		    	{
		    		preparedStatement.setString(9, null);
		    	}	
	    		if (r_mapeo.getPresenciaCapsula()!=null)
	    		{
	    			preparedStatement.setString(10, r_mapeo.getPresenciaCapsula());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(10, null);
	    		}	
	    		if (r_mapeo.getPresenciaContra()!=null)
	    		{
	    			preparedStatement.setString(11, r_mapeo.getPresenciaContra());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(11, null);
	    		}	
	    		if (r_mapeo.getPresenciaTirilla()!=null)
	    		{
	    			preparedStatement.setString(12, r_mapeo.getPresenciaTirilla());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(12, null);
	    		}	
	    		if (r_mapeo.getPresenciaMedalla()!=null)
	    		{
	    			preparedStatement.setString(13, r_mapeo.getPresenciaMedalla());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(13, null);
	    		}	
	    		if (r_mapeo.getPosicionTresPicos()!=null)
	    		{
	    			preparedStatement.setString(14, r_mapeo.getPosicionTresPicos());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(14, null);
	    		}	
	    		if (r_mapeo.getEstadoCapsula()!=null)
	    		{
	    			preparedStatement.setString(15, r_mapeo.getEstadoCapsula());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(15, null);
	    		}	
	    		posicion = 16;
			}
	    	else
	    	{
	    		posicion = 8;
	    	}
	    	if (r_mapeo.getExpulsorCajas()!=null)
	    	{
	    		preparedStatement.setString(posicion, r_mapeo.getExpulsorCajas());
	    	}
	    	else
	    	{
	    		preparedStatement.setString(posicion, null);
	    	}

		    preparedStatement.setInt(posicion+1, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoControlDetectores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
		}
//		else if (r_mapeo.getLinea().contentEquals("BIB"))
//		{
//			tabla = "qc_detectores_bib";
//		}

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_detectores.idProgramacion ");
		}
//		else if (r_linea.contentEquals("BIB"))
//		{
//			tabla = "qc_detectores_bib";
//			campo= "idPrdProgramacion";
//			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_detectores.idProgramacion ");
//		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_detectores.idCodigo det_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_detectores ");

	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );

			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("det_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_detectores_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_detectores_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_detectores_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_detectores_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}