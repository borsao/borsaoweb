package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlLineasPaletizar extends MapeoGlobal
{
	private Integer idPaletizar;

	private String acc; // para poner el boton
	
	private String material;
	private String caja1;
	private String caja2;
	private String caja3;
	private String caja4;
	private String caja5;
	private String caja6;

	public MapeoControlLineasPaletizar()
	{
	}

	public Integer getIdPaletizar() {
		return idPaletizar;
	}

	public void setIdPaletizar(Integer idPaletizar) {
		this.idPaletizar = idPaletizar;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getCaja1() {
		return caja1;
	}

	public void setCaja1(String caja1) {
		this.caja1 = caja1;
	}

	public String getCaja2() {
		return caja2;
	}

	public void setCaja2(String caja2) {
		this.caja2 = caja2;
	}

	public String getCaja3() {
		return caja3;
	}

	public void setCaja3(String caja3) {
		this.caja3 = caja3;
	}

	public String getCaja4() {
		return caja4;
	}

	public void setCaja4(String caja4) {
		this.caja4 = caja4;
	}

	public String getCaja5() {
		return caja5;
	}

	public void setCaja5(String caja5) {
		this.caja5 = caja5;
	}

	public String getCaja6() {
		return caja6;
	}

	public void setCaja6(String caja6) {
		this.caja6 = caja6;
	}

}