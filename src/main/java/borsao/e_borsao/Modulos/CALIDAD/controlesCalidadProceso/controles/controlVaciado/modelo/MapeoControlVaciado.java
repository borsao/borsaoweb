package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlVaciado extends MapeoGlobal
{
	private Integer idProgramacion;
	private String hora;
	private String estadoZona1;
	private String estadoZona2;
	private String estadoZona3;
	private String estadoZona4;
	private String estadoZona5;
	private String realizado;
	private String observaciones;
	private String linea;
	private String verificado;
	private Date fecha =null;
	
	private String articulo;
	private String descripcion;
	private String lote;
	private String tipoOperacion;
	private String revision;
	private String perfecto;

	
	public MapeoControlVaciado()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstadoZona1() {
		return estadoZona1;
	}

	public void setEstadoZona1(String estadoZona1) {
		this.estadoZona1 = estadoZona1;
	}

	public String getEstadoZona2() {
		return estadoZona2;
	}

	public void setEstadoZona2(String estadoZona2) {
		this.estadoZona2 = estadoZona2;
	}

	public String getEstadoZona3() {
		return estadoZona3;
	}

	public void setEstadoZona3(String estadoZona3) {
		this.estadoZona3 = estadoZona3;
	}

	public String getEstadoZona4() {
		return estadoZona4;
	}

	public void setEstadoZona4(String estadoZona4) {
		this.estadoZona4 = estadoZona4;
	}

	public String getEstadoZona5() {
		return estadoZona5;
	}

	public void setEstadoZona5(String estadoZona5) {
		this.estadoZona5 = estadoZona5;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getLote() {
		return lote;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public String getRevision() {
		return revision;
	}

	public String getPerfecto() {
		return perfecto;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public void setPerfecto(String perfecto) {
		this.perfecto = perfecto;
	}
	
}