package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlLineasPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlPaletizar;

public class consultaPaletizarServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPaletizarServer  instance;
	
	public consultaPaletizarServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPaletizarServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPaletizarServer (r_usuario);			
		}
		return instance;
	}


	public ArrayList<MapeoControlPaletizar> datosOpcionesGlobal(MapeoControlPaletizar r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoControlPaletizar> vector = null;
		MapeoControlPaletizar mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_paletizar.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_paletizar.idCodigo pal_id, ");
			cadenaSQL.append(" qc_paletizar.linea pal_lin, ");
			cadenaSQL.append(" qc_paletizar.hora pal_hor, ");
			cadenaSQL.append(" qc_paletizar.realizado pal_rea, ");
			cadenaSQL.append(" qc_paletizar.verificado pal_ver, ");
			cadenaSQL.append(" qc_paletizar.fecha pal_fec, ");
			cadenaSQL.append(" qc_paletizar.observaciones pal_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_paletizar ");
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where qc_paletizar.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				vector = new ArrayList<MapeoControlPaletizar>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPaletizar();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pal_id"));
					mapeo.setLinea(r_mapeo.getLinea());
					mapeo.setHora(rsOpcion.getString("pal_hor"));
					mapeo.setRealizado(rsOpcion.getString("pal_rea"));
					mapeo.setVerificado(rsOpcion.getString("pal_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pal_obs"));
					mapeo.setFecha(rsOpcion.getDate("pal_fec"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	public ArrayList<MapeoControlPaletizar> datosOpcionesGlobal(String r_linea, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlPaletizar> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlPaletizar mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_paletizar.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_paletizar.idCodigo pal_id, ");
			cadenaSQL.append(" qc_paletizar.hora pal_lin, ");
			cadenaSQL.append(" qc_paletizar.hora pal_hor, ");
			cadenaSQL.append(" qc_paletizar.realizado pal_rea, ");
			cadenaSQL.append(" qc_paletizar.verificado pal_ver, ");
			cadenaSQL.append(" qc_paletizar.fecha pal_fec, ");
			cadenaSQL.append(" qc_paletizar.observaciones pal_obs ");
			cadenaSQL.append(" FROM " + tabla + " qc_paletizar ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
			
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlPaletizar>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlPaletizar();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("pal_id"));
					mapeo.setLinea(r_linea);
					mapeo.setHora(rsOpcion.getString("pal_hor"));
					mapeo.setRealizado(rsOpcion.getString("pal_rea"));
					mapeo.setVerificado(rsOpcion.getString("pal_ver"));
					mapeo.setObservaciones(rsOpcion.getString("pal_obs"));
					mapeo.setFecha(rsOpcion.getDate("pal_fec"));					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}

	public ArrayList<MapeoControlLineasPaletizar> datosOpcionesGlobalLineas(Integer r_id, MapeoControlPaletizar r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ArrayList<MapeoControlLineasPaletizar> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlLineasPaletizar mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tablaLineas = "qc_lineas_paletizar_env";
			tabla = "qc_paletizar_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			tablaLineas = "qc_lineas_paletizar_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			tablaLineas = "qc_lineas_paletizar_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasPaletizar.idCodigo lpal_id, ");
			cadenaSQL.append(" qc_lineasPaletizar.material lpal_mat, ");
			cadenaSQL.append(" qc_lineasPaletizar.caja1 lpal_c1, ");
			cadenaSQL.append(" qc_lineasPaletizar.caja2 lpal_c2, ");
			if (!r_mapeo.getLinea().contentEquals("Envasadora")) cadenaSQL.append(" qc_lineasPaletizar.caja3 lpal_c3, ");
			if (!r_mapeo.getLinea().contentEquals("Envasadora")) cadenaSQL.append(" qc_lineasPaletizar.caja4 lpal_c4, ");
			if (r_mapeo.getLinea().contentEquals("Embotelladora")) cadenaSQL.append(" qc_lineasPaletizar.caja5 lpal_c5, ");
			if (r_mapeo.getLinea().contentEquals("Embotelladora")) cadenaSQL.append(" qc_lineasPaletizar.caja6 lpal_c6, ");
			
			cadenaSQL.append(" qc_lineasPaletizar.idPaletizado lpal_ros ");
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasPaletizar ");
			
			try
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_id!=null && r_id>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" qc_lineasPaletizar.idPaletizado  = " + r_id);
				}
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlLineasPaletizar>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlLineasPaletizar();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("lpal_id"));
					mapeo.setMaterial(rsOpcion.getString("lpal_mat"));
					mapeo.setCaja1(rsOpcion.getString("lpal_c1"));
					mapeo.setCaja2(rsOpcion.getString("lpal_c2"));
					if (!r_mapeo.getLinea().contentEquals("Envasadora")) mapeo.setCaja3(rsOpcion.getString("lpal_c3"));
					if (!r_mapeo.getLinea().contentEquals("Envasadora")) mapeo.setCaja4(rsOpcion.getString("lpal_c4"));
					if (r_mapeo.getLinea().contentEquals("Embotelladora")) mapeo.setCaja5(rsOpcion.getString("lpal_c5"));
					if (r_mapeo.getLinea().contentEquals("Embotelladora")) mapeo.setCaja6(rsOpcion.getString("lpal_c6"));
					mapeo.setIdPaletizar(rsOpcion.getInt("lpal_ros"));
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return vector;
	}
	
	
	public String guardarNuevoPaletizar(MapeoControlPaletizar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
		}

		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".linea , ");
			cadenaSQL.append(tabla + ".hora, ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".verificado, ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion , ");
			cadenaSQL.append(tabla + ".fecha ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
	        preparedStatement.executeUpdate();
	        this.generarLineasPaletizar(r_mapeo.getIdCodigo(), r_mapeo.getLinea());
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public String generarLineasPaletizar(Integer r_id, String r_linea)
	{
		String rdo=null;
		MapeoControlLineasPaletizar mapeo = null;
		
		if (r_linea.toUpperCase().contentEquals("EMBOTELLADORA"))
		{
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("01 Botellas");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);

			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("02 Etiqueta");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("03 Contra");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("04 Tirilla");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("05 Medalla");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("06 Capsula");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("07 Rosca");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("08 Cerrado");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("09 Lote");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("10 Impresion Caja");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("11 SSCC");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setCaja5("");
			mapeo.setCaja6("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
		}
		else if (r_linea.toUpperCase().contentEquals("ENVASADORA"))
		{
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("01 Garrafas");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);

			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("02 Etiqueta");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("03 Prueba Asa");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("04 Tapon");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("05 Lote Garrafa");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("06 Lote Caja");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("07 Pesadora");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("08 SSCC");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
		}
		else if (r_linea.toUpperCase().contentEquals("BIB"))
		{
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("01 Bolsa");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);

			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("02 Cerrado");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("03 Lote");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("04 Impresion Caja");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
			mapeo = new MapeoControlLineasPaletizar();
			mapeo.setMaterial("05 SSCC");
			mapeo.setCaja1("");
			mapeo.setCaja2("");
			mapeo.setCaja3("");
			mapeo.setCaja4("");
			mapeo.setIdPaletizar(r_id);
			
			this.guardarNuevoLineasPaletizar(r_linea, mapeo);
			
		}

		return rdo;
	}
	
	public String guardarCambiosPaletizar(MapeoControlPaletizar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
		}
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".linea =?, ");
			cadenaSQL.append(tabla + ".hora=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".verificado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlPaletizar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		String tablaLineas = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
			tablaLineas = "qc_lineas_paletizar_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			tablaLineas = "qc_lineas_paletizar_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			tablaLineas = "qc_lineas_paletizar_bib";
		}

		try
		{
			con= this.conManager.establecerConexionInd();
			
			cadenaSQL.append(" DELETE FROM " + tablaLineas );            
			cadenaSQL.append(" WHERE " + tablaLineas + ".idPaletizado = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	
	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_paletizar.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_paletizar.idProgramacion ");
		}

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_paletizar.idCodigo pal_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_paletizar ");
	     	
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pal_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	/*
	 * METODOS LINEAS LOTES MP
	 */
	
	
	public HashMap<String, String> recuperarDatosCaja(Integer r_id, String r_campo, String r_linea)
	{
		ResultSet rsOpcion = null;
		HashMap<String, String> caja = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String tablaLineas = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tablaLineas = "qc_lineas_paletizar_env";
			tabla = "qc_paletizar_env";
			campo= "idPaletizado";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			tablaLineas = "qc_lineas_paletizar_emb";
			campo= "idPaletizado";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = cab.idProgramacion ");

		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			tablaLineas = "qc_lineas_paletizar_bib";
			campo= "idPaletizado";
			cadenaWhere.append(" inner join " + tabla + " cab on cab.idCodigo = qc_lineasPaletizar.idPaletizado ");
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = cab.idProgramacion ");

		}
		
		if (tabla!=null && tabla.length()>0)
		{
			
			cadenaSQL.append(" SELECT qc_lineasPaletizar.material pal_mat, ");
			cadenaSQL.append(" qc_lineasPaletizar." + r_campo + " pal_val ");
			cadenaSQL.append(" FROM " + tablaLineas + " qc_lineasPaletizar ");
			cadenaSQL.append(cadenaWhere.toString());
			cadenaSQL.append(" where qc_lineasPaletizar." + campo + " = " + r_id);
			cadenaSQL.append(" order by qc_lineasPaletizar.material ");
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());

				caja = new HashMap<String, String>();
				
				while(rsOpcion.next())
				{
					/*
					 * recojo mapeo operarios
					 */
					caja.put(rsOpcion.getString("pal_mat").substring(0, 2), rsOpcion.getString("pal_val"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		
		return caja;
	}

	public String guardarNuevoLineasPaletizar(String r_linea, MapeoControlLineasPaletizar r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lineas_paletizar_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_paletizar_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lineas_paletizar_bib";
		}
		
		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".material, ");
			cadenaSQL.append(tabla + ".caja1, ");
			cadenaSQL.append(tabla + ".caja2, ");
			if (!r_linea.contentEquals("Envasadora")) cadenaSQL.append(tabla + ".caja3, ");
			if (!r_linea.contentEquals("Envasadora")) cadenaSQL.append(tabla + ".caja4, ");
			if (r_linea.contentEquals("Embotelladora")) cadenaSQL.append(tabla + ".caja5, ");
			if (r_linea.contentEquals("Embotelladora")) cadenaSQL.append(tabla + ".caja6, ");
			cadenaSQL.append(tabla + ".idPaletizado ) VALUES (");
			
			cadenaSQL.append(" ?,?,?,?,");
			if (!r_linea.contentEquals("Envasadora")) cadenaSQL.append(" ?,?,");
			if (r_linea.contentEquals("Embotelladora")) cadenaSQL.append(" ?,?,");
			
			cadenaSQL.append(" ?)");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			if (r_mapeo.getMaterial()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getMaterial());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getCaja1()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getCaja1());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getCaja2()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getCaja2());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			if (!r_linea.contentEquals("Envasadora"))
			{
				if (r_mapeo.getCaja3()!=null)
				{
					preparedStatement.setString(5, r_mapeo.getCaja3());
				}
				else
				{
					preparedStatement.setString(5, null);
				}
				if (r_mapeo.getCaja4()!=null)
				{
					preparedStatement.setString(6, r_mapeo.getCaja4());
				}
				else
				{
					preparedStatement.setString(6, null);
				}
			}
			if (r_linea.contentEquals("Embotelladora"))
			{
				if (r_mapeo.getCaja5()!=null)
				{
					preparedStatement.setString(7, r_mapeo.getCaja5());
				}
				else
				{
					preparedStatement.setString(7, null);
				}
				if (r_mapeo.getCaja6()!=null)
				{
					preparedStatement.setString(8, r_mapeo.getCaja6());
				}
				else
				{
					preparedStatement.setString(8, null);
				}
			}
			int valorId = 5;
			if (!r_linea.contentEquals("Envasadora")) valorId = valorId +2;
			if (r_linea.contentEquals("Embotelladora")) valorId = valorId +2;
			
			if (r_mapeo.getIdPaletizar()!=null)
			{
				preparedStatement.setInt(valorId, r_mapeo.getIdPaletizar());
			}
			else
			{
				preparedStatement.setInt(valorId, 0);
			}
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String guardarCambiosLineasPaletizar(String r_linea, Integer r_idPaletizado, HashMap<String, String> r_valores)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_lineas_paletizar_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_lineas_paletizar_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_lineas_paletizar_bib";
		}
		
		try
		{
			con= this.conManager.establecerConexionInd();	
			
			for (int i = 1; i < r_valores.size(); i++)
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" update " + tabla + " SET " );
				cadenaSQL.append(tabla + "." + r_valores.get("campo") + " = '" + r_valores.get(RutinasNumericas.formatearIntegerDigitosIzqda(i, 2)) + "' ");
				cadenaSQL.append(" WHERE " + tabla + ".idPaletizado= " + r_idPaletizado);
				cadenaSQL.append(" and 	material like '" + RutinasNumericas.formatearIntegerDigitosIzqda(i, 2) + "%' ");
				preparedStatement = con.prepareStatement(cadenaSQL.toString());
				preparedStatement.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public boolean comprobarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_paletizar.idCodigo lmp_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_paletizar ");
			cadenaSQL.append(" where qc_paletizar.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_paletizar_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_paletizar_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_paletizar_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}