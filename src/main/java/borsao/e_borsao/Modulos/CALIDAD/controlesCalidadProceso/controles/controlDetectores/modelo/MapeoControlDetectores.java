package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlDetectores extends MapeoGlobal
{
	/*
	 * comunes
	 */
	private Integer idProgramacion;
	private String hora;
	private String observaciones;
	private String linea;
	private String verificado;
	private String realizado;
	private Date fecha =null;
	private String presenciaRosca;
	private String presenciaFrontal;
	private String expulsorCajas;

	/*
	 * propios embotelladora
	 */
	private String nivel;
	private String presenciaCorcho;
	private String presenciaCapsula;
	private String presenciaContra;
	private String presenciaTirilla;
	private String presenciaMedalla;
	private String posicionTresPicos;
	private String estadoCapsula;
	
	public MapeoControlDetectores()
	{
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getPresenciaCorcho() {
		return presenciaCorcho;
	}

	public void setPresenciaCorcho(String presenciaCorcho) {
		this.presenciaCorcho = presenciaCorcho;
	}

	public String getPresenciaRosca() {
		return presenciaRosca;
	}

	public void setPresenciaRosca(String presenciaRosca) {
		this.presenciaRosca = presenciaRosca;
	}

	public String getPresenciaCapsula() {
		return presenciaCapsula;
	}

	public void setPresenciaCapsula(String presenciaCapsula) {
		this.presenciaCapsula = presenciaCapsula;
	}

	public String getPresenciaFrontal() {
		return presenciaFrontal;
	}

	public void setPresenciaFrontal(String presenciaFrontal) {
		this.presenciaFrontal = presenciaFrontal;
	}

	public String getPresenciaContra() {
		return presenciaContra;
	}

	public void setPresenciaContra(String presenciaContra) {
		this.presenciaContra = presenciaContra;
	}

	public String getPresenciaTirilla() {
		return presenciaTirilla;
	}

	public void setPresenciaTirilla(String presenciaTirilla) {
		this.presenciaTirilla = presenciaTirilla;
	}

	public String getPresenciaMedalla() {
		return presenciaMedalla;
	}

	public void setPresenciaMedalla(String presenciaMedalla) {
		this.presenciaMedalla = presenciaMedalla;
	}

	public String getPosicionTresPicos() {
		return posicionTresPicos;
	}

	public void setPosicionTresPicos(String posicionTresPicos) {
		this.posicionTresPicos = posicionTresPicos;
	}

	public String getEstadoCapsula() {
		return estadoCapsula;
	}

	public void setEstadoCapsula(String estadoCapsula) {
		this.estadoCapsula = estadoCapsula;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getExpulsorCajas() {
		return expulsorCajas;
	}

	public void setExpulsorCajas(String expulsorCajas) {
		this.expulsorCajas = expulsorCajas;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	
}