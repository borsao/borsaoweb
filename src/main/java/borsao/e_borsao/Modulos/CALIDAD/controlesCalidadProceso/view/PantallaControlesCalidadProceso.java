package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlAprobacion.server.consultaAprobacionServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlAprobacion.view.pantallaAprobacion;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlChequeos.server.consultaChequeosServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlChequeos.view.pantallaChequeos;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.server.consultaDetectoresServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.view.pantallaDetectores;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.server.consultaFiltracionServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlFiltracion.view.pantallaFiltracion;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.server.consultaGrifosServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlGrifos.view.pantallaGrifos;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.server.consultaImpulsosServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.view.pantallaImpulsos;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.server.consultaLotesMPServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.view.pantallaLotesMP;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.server.consultaPCC2Server;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPCC2.view.pantallaPCC2;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.server.consultaPaletizarServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.view.pantallaPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.server.consultaPreoperativoServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPreoperativo.view.pantallaPreoperativo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.server.consultaProductoElaborarServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.view.pantallaProductoElaborar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.server.consultaRoscaServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRosca.view.pantallaRosca;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.server.consultaControlRoturasServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.view.pantallaRegistroRoturasBotellas;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlTaponesDefectuosos.server.consultaTaponesDefectuososServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlTaponesDefectuosos.view.pantallaTaponesDefectuosos;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.server.consultaVaciadoServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVaciado.view.pantallaVaciado;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.server.consultaVacioServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.view.pantallaVacio;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.server.consultaVolumenServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.view.pantallaVolumen;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.server.consultaVolumenEfServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.view.pantallaVolumenEf;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server.consultaTiposOFServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PantallaControlesCalidadProceso extends Ventana
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;

	public consultaProgramacionServer cus =null;
	public consultaProgramacionEnvasadoraServer cues =null;
	public String tipo=null;
	public Integer idGridSeleccionado=null;
	private String loteSeleccionado ="";
	private String articuloSeleccionado ="";
	public ComboBox cmbLinea = null;
	private TextField txtEjercicio= null;
	public ComboBox cmbSemana = null;
	private String semana ="";
	private Button btnProcesar = null;
	private Grid gridDatos = null;
	private boolean hayGrid=false;
	
	private TextField txtHora= null;
	private EntradaDatosFecha txtFecha= null;
	
	private controlesCalidadProcesoView origen = null;
	private GridLayout fila2 = null;
	
	private VerticalLayout controles = null;
	
	
	private Button btnPCC2= null;
	private Button btnPreoperativo= null;
	private Button btnFiltracion= null;
	private Button btnGrifos= null;
	private Button btnProducto= null;
	private Button btnAprobacion= null;
	private Button btnDetectores= null;
	private Button btnVaciado= null;
	private Button btnRosca= null;
	private Button btnVacio= null;
	private Button btnLotesMP= null;
	private Button btnVolumen= null;
	private Button btnContenido= null;
	private Button btnPaletizado= null;
	private Button btnImpulsos= null;
	private Button btnChequeos= null;
	private Button btnMermas= null;
	private PantallaControlesCalidadProceso app = null;
	
	public PantallaControlesCalidadProceso(controlesCalidadProcesoView r_view)
	{
		this.origen=r_view;
		this.app = this;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);

				this.cmbLinea= new ComboBox("Linea");    	
				this.cmbLinea.addStyleName(ValoTheme.COMBOBOX_TINY);
				this.cmbLinea.setNewItemsAllowed(false);
				this.cmbLinea.setWidth("140px");
				this.cmbLinea.setNullSelectionAllowed(false);

				this.txtEjercicio=new TextField("Ejercicio");
	    		this.txtEjercicio.setEnabled(true);
	    		this.txtEjercicio.setWidth("100px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

	    		this.cmbSemana= new ComboBox("Semana");    	
	    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
	    		this.cmbSemana.setNewItemsAllowed(false);
	    		this.cmbSemana.setWidth("100px");
	    		this.cmbSemana.setNullSelectionAllowed(false);

	    		this.btnProcesar=new Button("Procesar");
	        	this.btnProcesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	        	this.btnProcesar.addStyleName(ValoTheme.BUTTON_TINY);
	        	this.btnProcesar.setIcon(FontAwesome.COGS);
	        	
	        	this.txtFecha = new EntradaDatosFecha("Fecha");
	        	this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
//	        	this.txtFecha.setValue(new Date());

	    		fila1.addComponent(this.cmbLinea);
				fila1.addComponent(this.txtEjercicio);
				fila1.addComponent(this.cmbSemana);
				fila1.addComponent(this.txtFecha);
				fila1.addComponent(this.btnProcesar);
				fila1.setComponentAlignment(this.btnProcesar, Alignment.BOTTOM_LEFT);
				
			fila2 = new GridLayout(2,1);
			fila2.setSizeFull();
			fila2.setSpacing(true);
			fila2.setMargin(true);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setSpacing(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Controles Calidad Proceso");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("1150px");
		this.setHeight("750px");
		this.center();
		
		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cargarCombo(null);
		
		this.cmbSemana.setValue(this.semana);
		
		if (eBorsao.get().accessControl.getNombre().contains("Embotelladora"))
		{
			this.cmbLinea.setValue("Embotelladora");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("Envasadora"))
		{
			this.cmbLinea.setValue("Envasadora");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("bib"))
		{
			this.cmbLinea.setValue("BIB");
		}
		else
		{
		}
	}

	private void cargarListeners()
	{
		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					gridDatos.removeAllColumns();			
					fila2.removeAllComponents();
					gridDatos=null;
					setHayGrid(false);
				}
			}
		}); 

		btnProcesar.addClickListener(new ClickListener() 
		{
			
			@Override
			public void buttonClick(ClickEvent event)			
			{

		    	if (new Integer(txtEjercicio.getValue().toString())<2025)
		    		btnLotesMP.setVisible(true);

				if (isHayGrid())
				{			
					gridDatos.removeAllColumns();			
					fila2.removeAllComponents();
					gridDatos=null;
					setHayGrid(false);
				}

				generarGrid();
			}
		});
		
		
		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			@Override
			public void buttonClick(ClickEvent event) 
			{
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	
	

    public void generarGrid()
    {
    	
    	ArrayList<MapeoControlesProcesoProductivo> r_vector=null;
    	MapeoControlesProcesoProductivo mapeoProgramacion =null;
    	MapeoControlesProcesoProductivo mapeoProgramacionSeleccionado =null;

    	IndexedContainer container =null;
    	Iterator<MapeoControlesProcesoProductivo> iterator = null;
    	
		container = new IndexedContainer();
		container.addContainerProperty("dia", String.class, null);
		container.addContainerProperty("tipo", String.class, null);
		container.addContainerProperty("lote", String.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
		container.addContainerProperty("cantidad", Integer.class, null);
		container.addContainerProperty("codigo", Integer.class, null);

    	if (todoEnOrden()) 
    	{
    		mapeoProgramacion = new MapeoControlesProcesoProductivo();
    		mapeoProgramacion.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    		mapeoProgramacion.setSemana(this.cmbSemana.getValue().toString());
    		
    		if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
    		{
    			this.cus = new consultaProgramacionServer(CurrentUser.get());
    			r_vector=this.cus.datosProgramacionGlobalArticulos(mapeoProgramacion);
    		}
    		else if (this.cmbLinea.getValue().toString().contentEquals("Envasadora"))
    		{
    			this.cues = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
    			r_vector=this.cues.datosProgramacionGlobalArticulosEnv(mapeoProgramacion);
    		}
    		else 
    		{
    			this.cues = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
    			r_vector=this.cues.datosProgramacionGlobalArticulosBIB(mapeoProgramacion);
    		}
    		iterator = r_vector.iterator();
    		
    		while (iterator.hasNext())
    		{
    			Object newItemId = container.addItem();
    			Item file = container.getItem(newItemId);
    			
    			mapeoProgramacionSeleccionado=iterator.next();
    			
    			file.getItemProperty("dia").setValue(mapeoProgramacionSeleccionado.getDia());
    			file.getItemProperty("tipo").setValue(mapeoProgramacionSeleccionado.getTipo());
    			file.getItemProperty("lote").setValue(mapeoProgramacionSeleccionado.getLote());
    			if (mapeoProgramacionSeleccionado.getArticulo()!=null && mapeoProgramacionSeleccionado.getArticulo().length()>0) file.getItemProperty("articulo").setValue(mapeoProgramacionSeleccionado.getArticulo().substring(0,7)); else file.getItemProperty("articulo").setValue(null);
    			file.getItemProperty("descripcion").setValue(mapeoProgramacionSeleccionado.getDescripcion());
    			file.getItemProperty("cantidad").setValue(mapeoProgramacionSeleccionado.getUnidades());
    			file.getItemProperty("codigo").setValue(mapeoProgramacionSeleccionado.getIdProgramacion());
    		}
    		
    		gridDatos= new Grid(container);
    		gridDatos.setSelectionMode(SelectionMode.SINGLE);
    		gridDatos.setSizeFull();
    		gridDatos.setWidth("100%");
    		gridDatos.addStyleName("minigrid");
    		gridDatos.getColumn("codigo").setHidden(true);
    		gridDatos.addSelectionListener(new SelectionListener() {
    			
    			@Override
    			public void select(SelectionEvent event) {
    				if (gridDatos.getSelectedRow()!=null)
    				{
	    				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
	    				idGridSeleccionado= new Integer(file.getItemProperty("codigo").getValue().toString());
	    				
	    				if (file.getItemProperty("dia").getValue()!=null && file.getItemProperty("dia").getValue().toString().trim().length()>0)
	    				{
	    					tipo="global";
	    					activarControles("global"); 
	    					articuloSeleccionado= file.getItemProperty("dia").getValue().toString();
	    				}
	    				else
	    				{
	    					tipo="referencia";
	    					loteSeleccionado= file.getItemProperty("lote").getValue().toString();
	    					articuloSeleccionado= file.getItemProperty("articulo").getValue().toString();
	    					activarControles("referencia");
	    				}
    				}
    			}
    		});
    		this.asignarEstilos();
    		Panel panel = new Panel("PRODUCCIÓN ");
    		panel.setSizeUndefined(); // Shrink to fit content
//    		panel.setHeight("250px");
    		panel.setContent(gridDatos);
    		fila2.addComponent(panel,0,0);
    		
    		setHayGrid(true);
    		cargarAcciones();
    		activarControles("");
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Rellena correctamente los campos");
    	}


    	
    }

    private void cargarAcciones()
    {
    	
    	
    	Panel panel = new Panel("CONTROLES DISPONIBLES");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	GridLayout content = new GridLayout(2,9);

    	btnPCC2 = new Button("Control PCC2");
    	btnPCC2.setDescription("PCC2");
    	btnPCC2.setWidth("150px");
    	btnPCC2.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnPCC2.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnPCC2.addStyleName(ValoTheme.BUTTON_TINY);
    	btnPCC2.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
    			mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
    			mapeo.setSemana(cmbSemana.getValue().toString());
    			mapeo.setFecha(txtFecha.getFecha());
    			mapeo.setIdProgramacion(idGridSeleccionado);
    			
    			if (tipo.contentEquals("global"))
    			{
    				pantallaPCC2 vt = new pantallaPCC2(app, "Control PCC2" + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
    				getUI().addWindow(vt);
    			}
    			else
    			{
    				Notificaciones.getInstance().mensajeInformativo("Control no disponible");
    			}
    		}
    	});
    	
    	btnPreoperativo = new Button("PREOPERATIVO");
    	btnPreoperativo.setDescription("PREOPERATIVO");
    	btnPreoperativo.setWidth("150px");
    	btnPreoperativo.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnPreoperativo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnPreoperativo.addStyleName(ValoTheme.BUTTON_TINY);
    	btnPreoperativo.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);
				
				pantallaPreoperativo vt = new pantallaPreoperativo(app, "Preoperativo" + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);
			}
		});
    	
    	
    	btnFiltracion= new Button("FILTRACION");
    	btnFiltracion.setWidth("150px");
    	btnFiltracion.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnFiltracion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnFiltracion.addStyleName(ValoTheme.BUTTON_TINY);
    	btnFiltracion.setDescription("FILTRACION");
    	btnFiltracion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaFiltracion vt = new pantallaFiltracion(app, "Filtracion " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
			}
		});

    	btnGrifos= new Button("GRIFOS");
    	btnGrifos.setWidth("150px");
    	btnGrifos.setVisible(false);
    	btnGrifos.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnGrifos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnGrifos.addStyleName(ValoTheme.BUTTON_TINY);
    	btnGrifos.setDescription("GRIFOS");
    	btnGrifos.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaGrifos vt = new pantallaGrifos(app, "Posicionamiento Grifos " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
    		}
    	});


    	btnImpulsos= new Button("IMPULSOS");
    	btnImpulsos.setWidth("150px");
    	btnImpulsos.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnImpulsos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnImpulsos.addStyleName(ValoTheme.BUTTON_TINY);
    	btnImpulsos.setDescription("CONTROL IMPULSOS");
    	btnImpulsos.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaImpulsos vt = new pantallaImpulsos(app, "Control Impulsos " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);			
    		}
    	});
    	
    	
    	btnMermas= new Button("Control Roturas");
    	btnMermas.setWidth("150px");
    	btnMermas.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnMermas.addStyleName(ValoTheme.BUTTON_DANGER);
    	btnMermas.addStyleName(ValoTheme.BUTTON_TINY);
    	btnMermas.setDescription("MERMAS Y ROTURAS");
    	btnMermas.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
    			mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
    			mapeo.setSemana(cmbSemana.getValue().toString());
    			mapeo.setFecha(txtFecha.getFecha());
    			mapeo.setIdProgramacion(idGridSeleccionado);
    			
    			pantallaRegistroRoturasBotellas vt = new pantallaRegistroRoturasBotellas(app, "Control Roturas " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);			
    		}
    	});
    	
    	btnProducto= new Button("PROD. A ELABORAR");
    	btnProducto.setWidth("150px");
    	btnProducto.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnProducto.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnProducto.addStyleName(ValoTheme.BUTTON_TINY);
    	btnProducto.setDescription("PRODUCTO A ELABORAR");
    	btnProducto.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
    			mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
    			mapeo.setSemana(cmbSemana.getValue().toString());
    			mapeo.setFecha(txtFecha.getFecha());
    			mapeo.setIdProgramacion(idGridSeleccionado);
    			
    			pantallaProductoElaborar vt = new pantallaProductoElaborar(app, "Producto a Elaborar de " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
    			getUI().addWindow(vt);					
    		}
    	});

    	btnAprobacion= new Button("APROBACION ART.");
    	btnAprobacion.setWidth("150px");
    	btnAprobacion.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnAprobacion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnAprobacion.addStyleName(ValoTheme.BUTTON_TINY);
    	btnAprobacion.setDescription("APROBACION ARTICULO");
    	btnAprobacion.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaAprobacion vt = new pantallaAprobacion(app, "Aprobación Articulo " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);					
    		}
    	});

    	btnDetectores= new Button("DETECTORES");
    	btnDetectores.setWidth("150px");
    	btnDetectores.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnDetectores.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnDetectores.addStyleName(ValoTheme.BUTTON_TINY);
    	btnDetectores.setDescription("DETECTORES");
    	btnDetectores.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaDetectores vt = new pantallaDetectores(app, "CONTROL DETECTORES " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);					
    		}
    	});

    	btnVaciado= new Button("VACIADO LINEA");
    	btnVaciado.setWidth("150px");
    	btnVaciado.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnVaciado.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnVaciado.addStyleName(ValoTheme.BUTTON_TINY);
    	btnVaciado.setDescription("VACIADO LINEA");
    	btnVaciado.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaVaciado vt = new pantallaVaciado(app, "VACIADO LINEA " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
    		}
    	});
    	
    	btnRosca= new Button();
    	if (cmbLinea.getValue().toString().contentEquals("Envasadora"))
		{
    		btnRosca.setCaption("TAPONES DEF.");
		}
    	else
    	{
    		btnRosca.setCaption("ROSCA.");
    	}
    	btnRosca.setWidth("150px");
    	btnRosca.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnRosca.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnRosca.addStyleName(ValoTheme.BUTTON_TINY);
    	btnRosca.setDescription("CONTROL ROSCA");
    	btnRosca.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);
				
				if (cmbLinea.getValue().toString().contentEquals("Envasadora"))
				{
					pantallaTaponesDefectuosos vt = new pantallaTaponesDefectuosos(app, "Control de Tapones Defectuosos" + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
					getUI().addWindow(vt);									
				}
				else
				{
					pantallaRosca vt = new pantallaRosca(app, "Control de Rosca " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
					getUI().addWindow(vt);				
				}
				
    		}
    	});

    	btnVacio= new Button("VACIO");
    	btnVacio.setWidth("150px");
    	btnVacio.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnVacio.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnVacio.addStyleName(ValoTheme.BUTTON_TINY);
    	btnVacio.setDescription("CONTROL VACIO");
    	btnVacio.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaVacio vt = new pantallaVacio(app, "Control de Vacio " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
    		}
    	});

    	btnLotesMP= new Button("LOTES M.P.");
    	btnLotesMP.setWidth("150px");
    	btnLotesMP.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnLotesMP.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnLotesMP.addStyleName(ValoTheme.BUTTON_TINY);
    	btnLotesMP.setDescription("LOTES MATERIA AUXILIAR");
    	btnLotesMP.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);

				pantallaLotesMP vt = new pantallaLotesMP(app, "Control Lotes Material Auxiliar " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
    		}
    	});

    	btnVolumen= new Button("VOLUMEN");
    	btnVolumen.setWidth("150px");
    	btnVolumen.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnVolumen.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnVolumen.addStyleName(ValoTheme.BUTTON_TINY);
    	btnVolumen.setDescription("CONTROL DE VOLUMEN");
    	btnVolumen.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);
				mapeo.setLote(loteSeleccionado);
				mapeo.setArticulo(articuloSeleccionado);

				pantallaVolumen vt = new pantallaVolumen(app, "Control de Volumen " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);						
    		}
    	});

    	btnContenido= new Button("CONTENIDO EFEC.");
    	btnContenido.setWidth("150px");
    	btnContenido.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnContenido.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnContenido.addStyleName(ValoTheme.BUTTON_TINY);
    	btnContenido.setDescription("CONTENIDO EFECTIVO");
    	btnContenido.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);
				mapeo.setLote(loteSeleccionado);
				mapeo.setArticulo(articuloSeleccionado);

				pantallaVolumenEf vt = new pantallaVolumenEf(app, "Control de Volumen " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);						
    		}
    	});

    	btnPaletizado= new Button("PALETIZADO");
    	btnPaletizado.setWidth("150px");
    	btnPaletizado.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnPaletizado.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnPaletizado.addStyleName(ValoTheme.BUTTON_TINY);
    	btnPaletizado.setDescription("CONTROL PALETIZADO");
    	btnPaletizado.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				mapeo.setSemana(cmbSemana.getValue().toString());
				mapeo.setFecha(txtFecha.getFecha());
				mapeo.setIdProgramacion(idGridSeleccionado);
				mapeo.setLote(loteSeleccionado);

				pantallaPaletizar vt = new pantallaPaletizar(app, "Control Antes Paletizado " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
				getUI().addWindow(vt);				
    		}
    	});
    	
    	btnChequeos= new Button("CHEQUEOS");
    	btnChequeos.setWidth("150px");
    	btnChequeos.setIcon(FontAwesome.CHECK_SQUARE_O);
    	btnChequeos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnChequeos.addStyleName(ValoTheme.BUTTON_TINY);
    	btnChequeos.setDescription("CONTROL CHEQUEOS");
    	btnChequeos.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			MapeoControlesProcesoProductivo mapeo = new MapeoControlesProcesoProductivo();
    			mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
    			mapeo.setSemana(cmbSemana.getValue().toString());
    			mapeo.setFecha(txtFecha.getFecha());
    			mapeo.setIdProgramacion(idGridSeleccionado);
    			
    			pantallaChequeos vt = new pantallaChequeos(app, "Chequeos de " + cmbLinea.getValue().toString(), mapeo, cmbLinea.getValue().toString());
    			getUI().addWindow(vt);					
    		}
    	});
    	
    	/*
    	 * globales
    	 */
    	content.addComponent(btnPreoperativo, 0, 0);
    	
		/*
		 * referencia
		 */
//    	content.addComponent(btnProducto,0,3);
//		content.addComponent(btnAprobacion,1,3);
		content.addComponent(btnLotesMP,1,3);
    	btnLotesMP.setVisible(new Integer(this.txtEjercicio.getValue().toString())<2025);


    	if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
    	{
        	/*
        	 * globales
        	 */
    		content.addComponent(btnFiltracion,1,0);
//    		content.addComponent(btnGrifos,0,1);
    		content.addComponent(btnPCC2,0,1);
    		/*
    		 * referencia
    		 */
    		content.addComponent(btnMermas,0,4);
    		content.addComponent(btnVaciado,1,4);
        	content.addComponent(btnDetectores,0,5);
        	content.addComponent(btnRosca,1,5);
        	content.addComponent(btnVacio,0,6);
        	content.addComponent(btnVolumen,1,6);
        	content.addComponent(btnContenido,0,7);
        	content.addComponent(btnPaletizado,1,7);

    	}
    	else if (this.cmbLinea.getValue().toString().contentEquals("BIB"))
    	{
    		/*
    		 * globales
    		 */
    		content.addComponent(btnImpulsos,0,1);
    		/*
    		 * Referencia
    		 */
    		content.addComponent(btnChequeos,0,5);
    		content.addComponent(btnVaciado,1,4);
        	content.addComponent(btnPaletizado,1,5);
        	content.addComponent(btnVolumen,1,6);
    	}
    	else if (this.cmbLinea.getValue().toString().contentEquals("Envasadora"))
    	{
    		content.addComponent(btnFiltracion,1,0);
        	/*
        	 * Referencia
        	 */
//    		content.addComponent(btnLotesMP,0,4);
    		content.addComponent(btnVaciado,1,4);
    		content.addComponent(btnRosca,1,5);
        	content.addComponent(btnDetectores,0,5);
        	content.addComponent(btnPaletizado,0,6);
    	}
    	
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(true);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	fila2.addComponent(panel,1,0);
    }
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		
		if (txtEjercicio.getValue().length()==0)
		{
			devolver=false;
			this.txtEjercicio.focus();
		}
		else if (this.cmbSemana.getValue()==null || this.cmbSemana.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbSemana.focus();
		}
		else if (this.cmbLinea.getValue()==null || this.cmbLinea.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbLinea.focus();
		}
		else if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbLinea.focus();
		}
		return devolver;
	}
	
	private void cargarCombo(String r_ejercicio)
	{
		this.cmbLinea.removeAllItems();
		this.cmbLinea.addItem("Embotelladora");
		this.cmbLinea.addItem("Envasadora");
		this.cmbLinea.addItem("BIB");
		
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}

	public boolean isHayGrid() {
		return hayGrid;
	}

	public void setHayGrid(boolean hayGrid) {
		this.hayGrid = hayGrid;
	}
	
	public void activarControles(String r_tipo)
	{
		Double capacidadSeleccionada = null;
		/*
		 * CONTROLES AL INICIO DE LA JORNADA
		 */
		if (r_tipo.contentEquals("global"))
		{
			consultaPreoperativoServer cps = consultaPreoperativoServer.getInstance(CurrentUser.get());
	    	
	    	if (cps.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    	{
	    		btnPreoperativo.removeStyleName(ValoTheme.BUTTON_DANGER);
	    		btnPreoperativo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	}
	    	else
	    	{
	    		btnPreoperativo.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnPreoperativo.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
	    	btnPreoperativo.setEnabled(r_tipo.contentEquals("global"));
	    	
	    	consultaProgramacionServer cprogs = consultaProgramacionServer.getInstance(CurrentUser.get());
	    	MapeoProgramacion mapeo = new MapeoProgramacion();
	    	
	    	mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
			mapeo.setSemana(cmbSemana.getValue().toString());
			mapeo.setIdProgramacion(idGridSeleccionado);
			
			
	    	if (cprogs.buscoEmbotelladosDiaSemana(mapeo))
	    	{
				consultaFiltracionServer cfs = consultaFiltracionServer.getInstance(CurrentUser.get());
		    	
		    	if (cfs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
		    	{
		    		btnFiltracion.removeStyleName(ValoTheme.BUTTON_DANGER);
		    		btnFiltracion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    	}
		    	else
		    	{
		    		btnFiltracion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnFiltracion.addStyleName(ValoTheme.BUTTON_DANGER);
		    	}
		    	
				btnFiltracion.setEnabled(r_tipo.contentEquals("global"));
		
				if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
				{
					consultaGrifosServer cgs = consultaGrifosServer.getInstance(CurrentUser.get());
			    	
			    	if (cgs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
			    	{
			    		btnGrifos.removeStyleName(ValoTheme.BUTTON_DANGER);
			    		btnGrifos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			    	}
			    	else
			    	{
			    		btnGrifos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			    		btnGrifos.addStyleName(ValoTheme.BUTTON_DANGER);
			    	}
			    	
				}
				btnGrifos.setEnabled(r_tipo.contentEquals("global") && this.cmbLinea.getValue().toString().contentEquals("Embotelladora"));
				
				if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
				{
					consultaPCC2Server cppcs = consultaPCC2Server.getInstance(CurrentUser.get());
					if (cppcs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
					{
						btnPCC2.removeStyleName(ValoTheme.BUTTON_DANGER);
						btnPCC2.addStyleName(ValoTheme.BUTTON_FRIENDLY);
					}
					else
					{
						btnPCC2.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
						btnPCC2.addStyleName(ValoTheme.BUTTON_DANGER);
					}
					
				}
				btnPCC2.setEnabled(r_tipo.contentEquals("global")  && this.cmbLinea.getValue().toString().contentEquals("Embotelladora"));
				
	    	}
	    	else
	    	{
	    		btnFiltracion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnFiltracion.addStyleName(ValoTheme.BUTTON_DANGER);
	    		btnGrifos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnGrifos.addStyleName(ValoTheme.BUTTON_DANGER);
	    		btnPCC2.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnPCC2.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
			if (this.cmbLinea.getValue().toString().contentEquals("BIB"))
			{
				consultaImpulsosServer cims = consultaImpulsosServer.getInstance(CurrentUser.get());
				
				if (cims.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
				{
					btnImpulsos.removeStyleName(ValoTheme.BUTTON_DANGER);
					btnImpulsos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				}
				else
				{
					btnImpulsos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
					btnImpulsos.addStyleName(ValoTheme.BUTTON_DANGER);
				}
				
			}
			btnImpulsos.setEnabled(r_tipo.contentEquals("global") && this.cmbLinea.getValue().toString().contentEquals("BIB"));
			
			btnProducto.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnProducto.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnAprobacion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnAprobacion.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnVaciado.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnVaciado.addStyleName(ValoTheme.BUTTON_DANGER);
			btnDetectores.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnDetectores.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnRosca.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
        	btnRosca.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnVacio.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnVacio.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnLotesMP.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
        	btnLotesMP.addStyleName(ValoTheme.BUTTON_DANGER);
        	btnVolumen.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnVolumen.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnContenido.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnContenido.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnPaletizado.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
        	btnPaletizado.addStyleName(ValoTheme.BUTTON_DANGER);
        	btnChequeos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnChequeos.addStyleName(ValoTheme.BUTTON_DANGER);
			btnMermas.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnMermas.addStyleName(ValoTheme.BUTTON_DANGER);
			
		}
		/*
		 * CONTROLES POR REFERENCIA
		 */
		else if (r_tipo.contentEquals("referencia"))
		{
			consultaProductoElaborarServer cpes = consultaProductoElaborarServer.getInstance(CurrentUser.get());
	    	
	    	if (cpes.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    	{
	    		btnProducto.removeStyleName(ValoTheme.BUTTON_DANGER);
	    		btnProducto.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	}
	    	else
	    	{
	    		btnProducto.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnProducto.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
	    	
			btnProducto.setEnabled(r_tipo.contentEquals("referencia"));
			
			consultaAprobacionServer caas = consultaAprobacionServer.getInstance(CurrentUser.get());
	    	
	    	if (caas.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    	{
	    		btnAprobacion.removeStyleName(ValoTheme.BUTTON_DANGER);
	    		btnAprobacion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	}
	    	else
	    	{
	    		btnAprobacion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		btnAprobacion.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
	    	btnAprobacion.setEnabled(r_tipo.contentEquals("referencia"));
	    	
	    	
			if ((this.cmbLinea.getValue().toString().contentEquals("Envasadora") || this.cmbLinea.getValue().toString().contentEquals("Embotelladora")))
			{
				consultaVaciadoServer cvs = consultaVaciadoServer.getInstance(CurrentUser.get());
		    	
		    	if (cvs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
		    	{
		    		btnVaciado.removeStyleName(ValoTheme.BUTTON_DANGER);
		    		btnVaciado.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    	}
		    	else
		    	{
		    		btnVaciado.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnVaciado.addStyleName(ValoTheme.BUTTON_DANGER);
		    	}
		    	
			}
			btnVaciado.setEnabled(r_tipo.contentEquals("referencia"));
	
			if ((this.cmbLinea.getValue().toString().contentEquals("Envasadora") || this.cmbLinea.getValue().toString().contentEquals("Embotelladora")))
			{
				consultaDetectoresServer cvs = consultaDetectoresServer.getInstance(CurrentUser.get());
				
				if (cvs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
				{
					btnDetectores.removeStyleName(ValoTheme.BUTTON_DANGER);
					btnDetectores.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				}
				else
				{
					btnDetectores.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
					btnDetectores.addStyleName(ValoTheme.BUTTON_DANGER);
				}
				
			}
			btnDetectores.setEnabled(r_tipo.contentEquals("referencia") && (this.cmbLinea.getValue().toString().contentEquals("Envasadora") || this.cmbLinea.getValue().toString().contentEquals("Embotelladora")));
	    	
	    	if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
			{
	    		consultaRoscaServer cr = consultaRoscaServer.getInstance(CurrentUser.get());
		    	if (cr.comprobarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
		    	{
		    		btnRosca.removeStyleName(ValoTheme.BUTTON_DANGER);
		        	btnRosca.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	btnVacio.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnVacio.addStyleName(ValoTheme.BUTTON_DANGER);
		    	}
		    	else
		    	{
		    		btnRosca.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	btnRosca.addStyleName(ValoTheme.BUTTON_DANGER);
		        	
					consultaVacioServer cves = consultaVacioServer.getInstance(CurrentUser.get());
			    	
			    	if (cves.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
			    	{
			    		btnVacio.removeStyleName(ValoTheme.BUTTON_DANGER);
			    		btnVacio.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			    	}
			    	else
			    	{
			    		btnVacio.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			    		btnVacio.addStyleName(ValoTheme.BUTTON_DANGER);
			    	}
			    	
			    	btnVacio.setEnabled(r_tipo.contentEquals("referencia")&& this.cmbLinea.getValue().toString().contentEquals("Embotelladora"));
		    	}
			}
	    	else if (this.cmbLinea.getValue().toString().contentEquals("Envasadora"))
			{
	    		consultaTaponesDefectuososServer cr = consultaTaponesDefectuososServer.getInstance(CurrentUser.get());
		    	if (cr.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
		    	{
		    		btnRosca.removeStyleName(ValoTheme.BUTTON_DANGER);
		        	btnRosca.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    	}
		    	else
		    	{
		    		btnRosca.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	btnRosca.addStyleName(ValoTheme.BUTTON_DANGER);
		    	}
		    	
			}
	    	
	    	btnRosca.setEnabled(r_tipo.contentEquals("referencia")&& !this.cmbLinea.getValue().toString().contentEquals("BIB"));
	    	

	    	consultaLotesMPServer clmp = consultaLotesMPServer.getInstance(CurrentUser.get());
	    	
	    	if (clmp.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    	{
	    		btnLotesMP.removeStyleName(ValoTheme.BUTTON_DANGER);
	        	btnLotesMP.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	}
	    	else
	    	{
	    		btnLotesMP.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	        	btnLotesMP.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
	    	
	    	btnLotesMP.setEnabled(r_tipo.contentEquals("referencia"));
	    	
	    	
	    	
	    	if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
			{
				consultaVolumenServer cves = consultaVolumenServer.getInstance(CurrentUser.get());
				consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
				if (articuloSeleccionado!=null && articuloSeleccionado.length()>0)
					capacidadSeleccionada = new Double(RutinasCadenas.reemplazarComaMiles(cas.obtenerCapacidadArticulo(articuloSeleccionado)))*1000;
				else
					capacidadSeleccionada = new Double(0);
	
		    	if (cves.comprobarControl(idGridSeleccionado, loteSeleccionado, capacidadSeleccionada.toString(), this.cmbLinea.getValue().toString()))
		    	{
		    		btnVolumen.removeStyleName(ValoTheme.BUTTON_DANGER);
		    		btnVolumen.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnContenido.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnContenido.addStyleName(ValoTheme.BUTTON_DANGER);
		    	}	
		    	else
		    	{
		    		btnVolumen.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		btnVolumen.addStyleName(ValoTheme.BUTTON_DANGER);
		    		
		    		consultaVolumenEfServer cvs = consultaVolumenEfServer.getInstance(CurrentUser.get());
					if (articuloSeleccionado!=null && articuloSeleccionado.length()>0)
						capacidadSeleccionada = new Double(RutinasCadenas.reemplazarComaMiles(cas.obtenerCapacidadArticulo(articuloSeleccionado)))*1000;
					else
						capacidadSeleccionada = new Double(0);
		
		    		if (cvs.comprobarControl(idGridSeleccionado, loteSeleccionado, capacidadSeleccionada.toString(), this.cmbLinea.getValue().toString()))
		    		{
		    			btnContenido.removeStyleName(ValoTheme.BUTTON_DANGER);
		    			btnContenido.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		}
		    		else
		    		{
		    			btnContenido.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
		    			btnContenido.addStyleName(ValoTheme.BUTTON_DANGER);
		    		}
		    		
			    	btnContenido.setEnabled(r_tipo.contentEquals("referencia")&& this.cmbLinea.getValue().toString().contentEquals("Embotelladora"));
			    	
		    	}
		    	
			}
	    	btnVolumen.setEnabled(r_tipo.contentEquals("referencia") && (this.cmbLinea.getValue().toString().contentEquals("Embotelladora") || this.cmbLinea.getValue().toString().contentEquals("BIB")));
	    	
	    	
	    	consultaPaletizarServer cadps = consultaPaletizarServer.getInstance(CurrentUser.get());
	    	
	    	if (cadps.comprobarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    	{
	    		btnPaletizado.removeStyleName(ValoTheme.BUTTON_DANGER);
	        	btnPaletizado.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    	}
	    	else
	    	{
	    		btnPaletizado.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	        	btnPaletizado.addStyleName(ValoTheme.BUTTON_DANGER);
	    	}
	    	
	    	btnPaletizado.setEnabled(r_tipo.contentEquals("referencia"));
	    	
	    	
	    	if (this.cmbLinea.getValue().toString().contentEquals("BIB"))
	    	{
	    		consultaChequeosServer cchqps = consultaChequeosServer.getInstance(CurrentUser.get());
	    		if (cchqps.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    		{
	    			btnChequeos.removeStyleName(ValoTheme.BUTTON_DANGER);
	    			btnChequeos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		}
	    		else
	    		{
	    			btnChequeos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    			btnChequeos.addStyleName(ValoTheme.BUTTON_DANGER);
	    		}
	    	}
	    	btnChequeos.setEnabled(r_tipo.contentEquals("referencia") && this.cmbLinea.getValue().toString().contentEquals("BIB"));
	    	
	    	if (this.cmbLinea.getValue().toString().contentEquals("Embotelladora"))
	    	{
	    		consultaControlRoturasServer ccrs = consultaControlRoturasServer.getInstance(CurrentUser.get());
	    		if (ccrs.comrpboarControl(idGridSeleccionado, this.cmbLinea.getValue().toString()))
	    		{
	    			btnMermas.removeStyleName(ValoTheme.BUTTON_DANGER);
	    			btnMermas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		}
	    		else
	    		{
	    			btnMermas.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
	    			btnMermas.addStyleName(ValoTheme.BUTTON_DANGER);
	    		}
	    		
	    	}
	    	btnMermas.setEnabled(r_tipo.contentEquals("referencia") && this.cmbLinea.getValue().toString().contentEquals("Embotelladora"));
	    	
	    	btnImpulsos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnImpulsos.addStyleName(ValoTheme.BUTTON_DANGER);
			btnPCC2.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnPCC2.addStyleName(ValoTheme.BUTTON_DANGER);
			btnGrifos.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnGrifos.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnFiltracion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnFiltracion.addStyleName(ValoTheme.BUTTON_DANGER);
    		btnPreoperativo.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    		btnPreoperativo.addStyleName(ValoTheme.BUTTON_DANGER);
		}
	}

	private void asignarEstilos()
	{
    	gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() {
            String estilo = null;
            String color = null;
            boolean separador = false;
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	consultaTiposOFServer tiposOF = consultaTiposOFServer.getInstance(CurrentUser.get());
            	if ("tipo".equals(cellReference.getPropertyId())) 
            	{
            		if (cellReference.getValue()!=null) color = tiposOF.obtenerColor(cellReference.getValue().toString()); else color = "#lincun";            			
            		
                }
            	else if ("lote".equals(cellReference.getPropertyId())) {            		
            		if (cellReference.getValue()!=null) color = "#linlot" ;  else color = "#lincun" ;
            	}
        		else if ("dia".equals(cellReference.getPropertyId())) {            		
                		color = "#lincun" ; 
            	}
        		else 
        		{
            		color = "#lincun" ; 
        		}

            	estilo = "cell-" + color.substring(1, 7);
            	
            	if ( "cantidad".equals(cellReference.getPropertyId()) )
            	{
            		estilo = "Rcell-lincur"; 
            	}
            	return estilo;
            }
        });
    	
	}

	@Override
	public void aceptarProceso(String r_accion) 
	{
		switch (r_accion)
		{
			case "":
			{
				break;
			}
		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		switch (r_accion)
		{
			case "":
			{
				break;
			}
		}
	}
}