package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.view;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.modelo.MapeoControlLineasLotesMP;

public class pantallaActualizacionLineasLotes extends Window
{
	private MapeoControlLineasLotesMP mapeoControlLineasLotesMP = null;
	private pantallaLotesMP App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtMaterial = null;
	private TextField txtDescripcion= null;
	private TextField txtProveedor= null;
	private TextField txtLote= null;
	private TextField txtCantidad = null;
	private EntradaDatosFecha txtFechaFabricacion = null;
	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasLotes(Ventana r_App, MapeoControlLineasLotesMP r_mapeoLinea, String r_articulo, String r_titulo)
	{
		this.mapeoControlLineasLotesMP= r_mapeoLinea;
		this.App = (pantallaLotesMP) r_App;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
//			fila1.setCaption("Datos Turno");
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtMaterial =new TextField();
				this.txtMaterial.setValue(r_mapeoLinea.getMaterial());
				this.txtMaterial.setWidth("140px");
				this.txtMaterial.setEnabled(false);

				fila1.addComponent(txtMaterial);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.txtDescripcion =new TextField("Descripcion");
				this.txtDescripcion.setWidth("450px");
				if (r_mapeoLinea.getDescripcion()!=null) this.txtDescripcion.setValue(r_mapeoLinea.getDescripcion());
				
				fila2.addComponent(txtDescripcion);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				
				this.txtProveedor =new TextField("Proveedor");
				this.txtProveedor.setWidth("250px");
				if (r_mapeoLinea.getProveedor()!=null) this.txtProveedor.setValue(r_mapeoLinea.getProveedor());
				
				fila3.addComponent(txtProveedor);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
			
				this.txtLote=new TextField("Lote ");				
				this.txtLote.setWidth("150px");
				this.txtLote.setRequired(true);
				if (r_mapeoLinea.getLote()!=null) this.txtLote.setValue(r_mapeoLinea.getLote());

				this.txtFechaFabricacion=new EntradaDatosFecha("Fecha Fabricacion ");
				this.txtFechaFabricacion.setWidth("150px");
				this.txtFechaFabricacion.setRequired(true);
				if (r_mapeoLinea.getFechaFabricacion()!=null) this.txtFechaFabricacion.setValue(r_mapeoLinea.getFechaFabricacion());

				this.txtCantidad=new TextField("Cantidad Utilizada ");
				this.txtCantidad.setWidth("120px");
				this.txtCantidad.setRequired(true);
				if (r_mapeoLinea.getCantidad()!=null) this.txtCantidad.setValue(r_mapeoLinea.getCantidad().toString()); else this.txtCantidad.setValue("0");
				
				fila4.addComponent(txtLote);
				fila4.addComponent(txtFechaFabricacion);
				fila4.addComponent(txtCantidad);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
	}

	private void habilitarCampos()
	{
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					mapeoControlLineasLotesMP.setCantidad(new Integer(txtCantidad.getValue()));
					mapeoControlLineasLotesMP.setDescripcion(txtDescripcion.getValue());
					mapeoControlLineasLotesMP.setProveedor(txtProveedor.getValue());
					mapeoControlLineasLotesMP.setLote(txtLote.getValue());
					mapeoControlLineasLotesMP.setFechaFabricacion(txtFechaFabricacion.getValue());
					
					App.actualizarDatosLineaLote(mapeoControlLineasLotesMP);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		String campoErroneo = null;
		
		if (txtCantidad.getValue()==null || txtCantidad.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Cantidad";
		}
		if (txtProveedor.getValue()==null || txtProveedor.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Proveedor";
		}
		if (txtLote.getValue()==null || txtLote.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Lote";
		}
		if (txtDescripcion.getValue()==null || txtDescripcion.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Descripcion";
		}
		if (txtFechaFabricacion.getValue()==null || txtFechaFabricacion.getValue().toString().length()==0)
		{
			devolver=false;
			campoErroneo="Fecha Fabricación";
		}
		
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
}