package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.server;

 import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlRoturas.modelo.MapeoRoturasBotellas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaControlRoturasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private String almacen = null;
	private static consultaControlRoturasServer instance;
	
	public consultaControlRoturasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaControlRoturasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaControlRoturasServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}

	public ArrayList<MapeoRoturasBotellas> datosRoturasBotellas(MapeoRoturasBotellas r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoRoturasBotellas mapeoRoturasBotellas = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null;
		ArrayList<MapeoRoturasBotellas> vector_2 = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_roturas.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_roturas.idProgramacion ");
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_roturas.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT idCodigo rot_id, ");	        
	        cadenaSQL.append(" qc_roturas.zona rot_zon, ");
	        cadenaSQL.append(" qc_roturas.ubicacion rot_ubi, ");
	        cadenaSQL.append(" qc_roturas.unidades rot_uds, ");
	        cadenaSQL.append(" qc_roturas.tiempo rot_tpo, ");
	        cadenaSQL.append(" qc_roturas.limpiador rot_lim, ");
	        cadenaSQL.append(" qc_roturas.verificador rot_ver, ");
	        cadenaSQL.append(" qc_roturas.verificado rot_vcr, ");
	        cadenaSQL.append(" qc_roturas.observaciones rot_obs, ");
	        cadenaSQL.append(" qc_roturas.fecha rot_fec, ");
	        cadenaSQL.append(" qc_roturas.idProgramacion rot_idp ");
	        
			cadenaSQL.append(" FROM " + tabla + " qc_roturas ");
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_ejercicio!=null && r_ejercicio>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
					}
					if (r_semana!=null && r_semana.length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getFecha()!=null)
					{
						if (cadenaWhere.length()>0 && cadenaWhere.toString().contains("where")) cadenaWhere.append(" and "); else cadenaWhere.append(" where ");
						cadenaWhere.append(" qc_roturas.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
					if (r_mapeo.getZona()!=null)
					{
						if (cadenaWhere.length()>0 && cadenaWhere.toString().contains("where")) cadenaWhere.append(" and "); else cadenaWhere.append(" where ");
						cadenaWhere.append(" qc_roturas.zona = '" + r_mapeo.getZona() + "' ");
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector_2 = new ArrayList<MapeoRoturasBotellas>();
				
				while(rsOpcion.next())
				{
					mapeoRoturasBotellas = new MapeoRoturasBotellas();
					/*
					 * recojo mapeo operarios
					 */
					
					mapeoRoturasBotellas.setIdCodigo(rsOpcion.getInt("rot_id"));
					
					mapeoRoturasBotellas.setZona(rsOpcion.getString("rot_zon"));
					mapeoRoturasBotellas.setUbicacion(rsOpcion.getString("rot_ubi"));
					mapeoRoturasBotellas.setLimpiador(rsOpcion.getString("rot_lim"));
					mapeoRoturasBotellas.setVerificador(rsOpcion.getString("rot_ver"));
					mapeoRoturasBotellas.setObservaciones(rsOpcion.getString("rot_obs"));
					
					mapeoRoturasBotellas.setUnidades(rsOpcion.getDouble("rot_uds"));
					mapeoRoturasBotellas.setTiempo(rsOpcion.getInt("rot_tpo"));
					
					mapeoRoturasBotellas.setFecha(rsOpcion.getDate("rot_fec"));
					mapeoRoturasBotellas.setVerificado(rsOpcion.getString("rot_vcr"));
					mapeoRoturasBotellas.setIdProgramacion(rsOpcion.getInt("rot_idp"));
					
					vector_2.add(mapeoRoturasBotellas);
				}				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector_2;
	}

	private Integer obtenerSiguienteRoturasBotellas()
	{
		ResultSet rsOpcion = null;
		
		try
		{

			StringBuffer cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT max(idCodigo) rot_id ");
	     	cadenaSQL.append(" FROM qc_roturas_emb ");

	     	con = this.conManager.establecerConexion();			
	     	
	     	Statement cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
		
			while(rsOpcion.next())
			{
				
				return (rsOpcion.getInt("rot_id")+1);

			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String guardarRoturas(MapeoRoturasBotellas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
		}

		try
		{
			
			cadenaSQL = new StringBuffer();
		
			cadenaSQL.append(" INSERT INTO "+ tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo,");
			cadenaSQL.append(tabla + ".zona,");
			cadenaSQL.append(tabla + ".ubicacion, ");
			cadenaSQL.append(tabla + ".unidades, ");
			cadenaSQL.append(tabla + ".tiempo, ");
			cadenaSQL.append(tabla + ".limpiador, ");			
			cadenaSQL.append(tabla + ".verificador, ");			
			cadenaSQL.append(tabla + ".verificado, ");			
			cadenaSQL.append(tabla + ".observaciones, ");			
			cadenaSQL.append(tabla + ".idProgramacion, ");			
			cadenaSQL.append(tabla + ".fecha) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
	    
		    preparedStatement.setInt(1, this.obtenerSiguienteRoturasBotellas());
		    if (r_mapeo.getZona()!=null)
    		{
    			preparedStatement.setString(2, r_mapeo.getZona());
    		}
    		else
    		{
    			preparedStatement.setString(2, null);
    		}
		    if (r_mapeo.getUbicacion()!=null)
    		{
    			preparedStatement.setString(3, r_mapeo.getUbicacion());
    		}
    		else
    		{
    			preparedStatement.setString(3, null);
    		}
    		if (r_mapeo.getUnidades()!=null)
    		{
    			preparedStatement.setDouble(4, r_mapeo.getUnidades());
    		}
    		else
    		{
    			preparedStatement.setDouble(4, 0);
    		}
    		if (r_mapeo.getTiempo()!=null)
    		{
    			preparedStatement.setInt(5, r_mapeo.getTiempo());
    		}
    		else
    		{
    			preparedStatement.setInt(5, 0);
    		}
		    if (r_mapeo.getLimpiador()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getLimpiador());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVerificador()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVerificador());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(11 , null);
		    }

		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambiosRoturas(MapeoRoturasBotellas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  

		String tabla = null; 
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
		}

		try
		{
			
			cadenaSQL = new StringBuffer();
		
			cadenaSQL.append(" UPDATE " + tabla + " SET ");
			cadenaSQL.append(tabla + ".zona=?,");
			cadenaSQL.append(tabla + ".ubicacion=?, ");
			cadenaSQL.append(tabla + ".limpiador=?, ");
			cadenaSQL.append(tabla + ".verificador=?, ");
			cadenaSQL.append(tabla + ".unidades=?, ");			
			cadenaSQL.append(tabla + ".tiempo=?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".verificado=? ");
			cadenaSQL.append(" where " + tabla + ".idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
	    
		    preparedStatement.setString(1, r_mapeo.getZona());
		    preparedStatement.setString(2, r_mapeo.getUbicacion());
		    preparedStatement.setString(3, r_mapeo.getLimpiador());
		    preparedStatement.setString(4, r_mapeo.getVerificador());
    		
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
	    
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminarRoturas(MapeoRoturasBotellas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		boolean rdo = false;
		String tabla = null;
		
		if (r_mapeo.getLinea().contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
		}
		else if (r_mapeo.getLinea().contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
		}
		else if (r_mapeo.getLinea().contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
		}

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo chq_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_idProgramacion)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_roturas_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rotura.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_roturas_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_rotura.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_roturas_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_rotura.idProgramacion ");
		}
		
		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_rotura.idCodigo pro_id ");
	     	cadenaSQL.append(" FROM " + tabla +" qc_rotura ");
	     	if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
	     	cadenaSQL.append(" where prd_prog."  + campo + " = " + r_idProgramacion );

	     	con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pro_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
}
