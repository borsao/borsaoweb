package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.modelo.MapeoControlDetectores;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlDetectores.server.consultaDetectoresServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaDetectores extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlDetectores mapeoControlDetectores = null;
	private consultaDetectoresServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app = null;
	private static String tipoControl = "referencia";

	/*
	 * comunes
	 */
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private Combo cmbPresenciaRosca = null;
	private Combo cmbPresenciaFrontal = null;
	private Combo cmbExpulsorCajas= null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;

	/*
	 * embotelladora
	 */
	private Combo cmbNivel = null;
	private Combo cmbPresenciaCorcho = null;
	private Combo cmbPresenciaContra = null;
	private Combo cmbPresenciaCapsula = null;
	private Combo cmbPresenciaTirilla = null;
	private Combo cmbPresenciaMedalla = null;
	private Combo cmbPosicionTresPicos = null;
	private Combo cmbEstadaoCapsula = null;
	private Label lblCumplimentacion = null;
	
	private HorizontalLayout linea1 = null;
	private HorizontalLayout linea2 = null;
	private HorizontalLayout linea3 = null;
	private HorizontalLayout linea4 = null;
	private HorizontalLayout linea5 = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaDetectores(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaDetectoresServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
			linea1 = new HorizontalLayout();
			linea1.setSpacing(true);
			
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new Combo("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setEnabled(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

			this.txtFecha = new EntradaDatosFecha();
			this.txtFecha.setCaption("Fecha");
			this.txtFecha.setEnabled(false);
			this.txtFecha.setValue(this.mapeoProduccion.getFecha());
			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtFecha.setWidth("150px");
			
			this.lblCumplimentacion = new Label();
			this.lblCumplimentacion.addStyleName("lblTituloCalidad");
//			this.lblCumplimentacion.setWidth("100%");
//			this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);

			linea1.addComponent(this.txtEjercicio);
			linea1.addComponent(this.cmbSemana);
			linea1.addComponent(this.txtFecha);
			linea1.addComponent(this.lblCumplimentacion);
			linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
			
			if (this.lineaProduccion.contentEquals("Embotelladora"))
    		{
				this.lblCumplimentacion.setValue("Frecuencia: Al comienzo y final de cada referencia de material auxiliar ");
				
				linea2 = new HorizontalLayout();
				linea2.setSpacing(true);
		
					this.cmbNivel= new Combo("NIVEL");
					this.cmbNivel.setWidth("150px");
					this.cmbNivel.setEnabled(false);
					this.cmbNivel.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbNivel.setNewItemsAllowed(false);
					this.cmbNivel.setNullSelectionAllowed(false);
					
					this.cmbPresenciaCorcho= new Combo("Presencia Corcho");
					this.cmbPresenciaCorcho.setWidth("150px");
					this.cmbPresenciaCorcho.setEnabled(false);
					this.cmbPresenciaCorcho.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaCorcho.setNewItemsAllowed(false);
					this.cmbPresenciaCorcho.setNullSelectionAllowed(false);
					
					this.cmbPresenciaRosca= new Combo("Presencia Rosca");
					this.cmbPresenciaRosca.setWidth("150px");
					this.cmbPresenciaRosca.setEnabled(false);
					this.cmbPresenciaRosca.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaRosca.setNewItemsAllowed(false);
					this.cmbPresenciaRosca.setNullSelectionAllowed(false);
					
					this.cmbPresenciaCapsula= new Combo("Presencia Capsula");
					this.cmbPresenciaCapsula.setWidth("150px");
					this.cmbPresenciaCapsula.setEnabled(false);
					this.cmbPresenciaCapsula.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaCapsula.setNewItemsAllowed(false);
					this.cmbPresenciaCapsula.setNullSelectionAllowed(false);
					
					this.cmbPresenciaFrontal= new Combo("Presencia Frontal");
					this.cmbPresenciaFrontal.setWidth("150px");
					this.cmbPresenciaFrontal.setEnabled(false);
					this.cmbPresenciaFrontal.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaFrontal.setNewItemsAllowed(false);
					this.cmbPresenciaFrontal.setNullSelectionAllowed(false);
					
					this.cmbPresenciaContra= new Combo("Presencia Contra");
					this.cmbPresenciaContra.setWidth("150px");
					this.cmbPresenciaContra.setEnabled(false);
					this.cmbPresenciaContra.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaContra.setNewItemsAllowed(false);
					this.cmbPresenciaContra.setNullSelectionAllowed(false);
		
					linea2.addComponent(this.cmbNivel);
					linea2.addComponent(this.cmbPresenciaCorcho);
					linea2.addComponent(this.cmbPresenciaRosca);
					linea2.addComponent(this.cmbPresenciaCapsula);
					linea2.addComponent(this.cmbPresenciaFrontal);
					linea2.addComponent(this.cmbPresenciaContra);
					
				linea3 = new HorizontalLayout();
				linea3.setSpacing(true);
					
					this.cmbPresenciaTirilla= new Combo("Presencia Tirilla");
					this.cmbPresenciaTirilla.setWidth("150px");
					this.cmbPresenciaTirilla.setEnabled(false);
					this.cmbPresenciaTirilla.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaTirilla.setNewItemsAllowed(false);
					this.cmbPresenciaTirilla.setNullSelectionAllowed(false);
					
					this.cmbPresenciaMedalla= new Combo("Presencia Medalla");
					this.cmbPresenciaMedalla.setWidth("150px");
					this.cmbPresenciaMedalla.setEnabled(false);
					this.cmbPresenciaMedalla.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPresenciaMedalla.setNewItemsAllowed(false);
					this.cmbPresenciaMedalla.setNullSelectionAllowed(false);
					
					this.cmbPosicionTresPicos= new Combo("Posicion EMBOSS");
					this.cmbPosicionTresPicos.setWidth("150px");
					this.cmbPosicionTresPicos.setEnabled(false);
					this.cmbPosicionTresPicos.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbPosicionTresPicos.setNewItemsAllowed(false);
					this.cmbPosicionTresPicos.setNullSelectionAllowed(false);
					
					this.cmbEstadaoCapsula= new Combo("Cámara Visión");
					this.cmbEstadaoCapsula.setWidth("150px");
					this.cmbEstadaoCapsula.setEnabled(false);
					this.cmbEstadaoCapsula.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbEstadaoCapsula.setNewItemsAllowed(false);
					this.cmbEstadaoCapsula.setNullSelectionAllowed(false);
					
					this.cmbExpulsorCajas= new Combo("Expulsor Cajas");
					this.cmbExpulsorCajas.setWidth("150px");
					this.cmbExpulsorCajas.setEnabled(false);
					this.cmbExpulsorCajas.addStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbExpulsorCajas.setNewItemsAllowed(false);
					this.cmbExpulsorCajas.setNullSelectionAllowed(false);
					
					linea3.addComponent(this.cmbPresenciaTirilla);
					linea3.addComponent(this.cmbPresenciaMedalla);
					linea3.addComponent(this.cmbPosicionTresPicos);
					linea3.addComponent(this.cmbEstadaoCapsula);					
					linea3.addComponent(this.cmbExpulsorCajas);					
    		}
			else
			{
				this.lblCumplimentacion.setValue("Frecuencia: Al comienzo del turno y al regresar del almuerzo ");
				
				linea5 = new HorizontalLayout();
				linea5.setSpacing(true);
				
				this.cmbPresenciaRosca= new Combo("Presencia Rosca");
				this.cmbPresenciaRosca.setWidth("150px");
				this.cmbPresenciaRosca.setEnabled(false);
				this.cmbPresenciaRosca.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.cmbPresenciaRosca.setNewItemsAllowed(false);
				this.cmbPresenciaRosca.setNullSelectionAllowed(false);
				
				this.cmbPresenciaFrontal= new Combo("Presencia Frontal");
				this.cmbPresenciaFrontal.setWidth("150px");
				this.cmbPresenciaFrontal.setEnabled(false);
				this.cmbPresenciaFrontal.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.cmbPresenciaFrontal.setNewItemsAllowed(false);
				this.cmbPresenciaFrontal.setNullSelectionAllowed(false);
				
				this.cmbExpulsorCajas= new Combo("Expulsor Cajas");
				this.cmbExpulsorCajas.setWidth("150px");
				this.cmbExpulsorCajas.setEnabled(false);
				this.cmbExpulsorCajas.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.cmbExpulsorCajas.setNewItemsAllowed(false);
				this.cmbExpulsorCajas.setNullSelectionAllowed(false);
				
				linea5.addComponent(this.cmbPresenciaRosca);
				linea5.addComponent(this.cmbPresenciaFrontal);
				linea5.addComponent(this.cmbExpulsorCajas);
			}

		linea4 = new HorizontalLayout();
		linea4.setSpacing(true);
			
			this.txtHoras = new TextField();
			this.txtHoras.setCaption("Hora");
			this.txtHoras.setWidth("120px");
			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtHoras.addStyleName("rightAligned");
			
			this.txtRealizado= new TextField();
			this.txtRealizado.setCaption("Realizado Por");
			this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.txtRealizado.setWidth("180px");
			
			this.txtObservaciones= new TextArea();
			this.txtObservaciones.setCaption("Observaciones");
			this.txtObservaciones.setWidth("300");
			
			btnLimpiar= new Button("Nuevo");
			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

			btnInsertar= new Button("Guardar");
			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

			btnEliminar= new Button("Eliminar");
			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
			btnEliminar.setEnabled(false);

			linea4.addComponent(this.txtHoras);
			linea4.setComponentAlignment(this.txtHoras, Alignment.TOP_LEFT);
			linea4.addComponent(this.txtRealizado);
			linea4.addComponent(this.txtObservaciones);
			linea4.addComponent(this.btnLimpiar);
			linea4.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnInsertar);
			linea4.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
			linea4.addComponent(this.btnEliminar);
			linea4.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

		this.frameGrid = new HorizontalLayout();
		this.frameGrid.setWidth("100%");

		this.frameCentral.addComponent(linea1);
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
			this.frameCentral.addComponent(linea2);
			this.frameCentral.addComponent(linea3);
		}
		else
		{
			this.frameCentral.addComponent(linea5);
		}
		this.frameCentral.addComponent(linea4);
		this.frameCentral.addComponent(this.frameGrid);
		
		this.framePie = new HorizontalLayout();
		
			btnBotonCVentana = new Button("Cerrar");
			btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
			
			btnBotonAVentana = new Button("Guardar");
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
			btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			
			framePie.addComponent(btnBotonCVentana);		
			framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{
			MapeoControlDetectores mapeo = new MapeoControlDetectores();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlDetectores.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}

	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlDetectores mapeo = new MapeoControlDetectores();

			mapeo.setPresenciaRosca(this.cmbPresenciaRosca.getValue().toString());
			mapeo.setPresenciaFrontal(this.cmbPresenciaFrontal.getValue().toString());
			mapeo.setExpulsorCajas(this.cmbExpulsorCajas.getValue().toString());
			
			if (this.lineaProduccion.contentEquals("Embotelladora"))
			{
				mapeo.setNivel(this.cmbNivel.getValue().toString());
				mapeo.setPresenciaCorcho(this.cmbPresenciaCorcho.getValue().toString());
				mapeo.setPresenciaCapsula(this.cmbPresenciaCapsula.getValue().toString());
				mapeo.setPresenciaContra(this.cmbPresenciaContra.getValue().toString());
				mapeo.setPresenciaTirilla(this.cmbPresenciaTirilla.getValue().toString());
				mapeo.setPresenciaMedalla(this.cmbPresenciaMedalla.getValue().toString());
				mapeo.setPosicionTresPicos(this.cmbPosicionTresPicos.getValue().toString());
				mapeo.setEstadoCapsula(this.cmbEstadaoCapsula.getValue().toString());
			}

			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlDetectores.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlDetectores.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlDetectores> vector=null;

    	MapeoControlDetectores mapeoPreop = new MapeoControlDetectores();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlDetectores r_mapeo)
	{
		setCreacion(false);

		this.mapeoControlDetectores = new MapeoControlDetectores();
		
		this.mapeoControlDetectores.setIdCodigo(r_mapeo.getIdCodigo());
		
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		this.cmbPresenciaRosca.setValue(r_mapeo.getPresenciaRosca());
		this.cmbPresenciaFrontal.setValue(r_mapeo.getPresenciaFrontal());
		this.cmbExpulsorCajas.setValue(r_mapeo.getExpulsorCajas());
		
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
			this.cmbNivel.setValue(r_mapeo.getNivel());
			this.cmbPresenciaCorcho.setValue(r_mapeo.getPresenciaCorcho());
			this.cmbPresenciaCapsula.setValue(r_mapeo.getPresenciaCapsula());
			this.cmbPresenciaContra.setValue(r_mapeo.getPresenciaContra());
			this.cmbPresenciaTirilla.setValue(r_mapeo.getPresenciaTirilla());
			this.cmbPresenciaMedalla.setValue(r_mapeo.getPresenciaMedalla());
			this.cmbPosicionTresPicos.setValue(r_mapeo.getPosicionTresPicos());
			this.cmbEstadaoCapsula.setValue(r_mapeo.getEstadoCapsula());
		}
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
		this.cmbPresenciaRosca.setEnabled(r_activar);
		this.cmbPresenciaFrontal.setEnabled(r_activar);
		this.cmbExpulsorCajas.setEnabled(r_activar);
		
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
			this.cmbNivel.setEnabled(r_activar);
			this.cmbPresenciaCorcho.setEnabled(r_activar);
			this.cmbPresenciaCapsula.setEnabled(r_activar);
			this.cmbPresenciaContra.setEnabled(r_activar);
			this.cmbPresenciaTirilla.setEnabled(r_activar);
			this.cmbPresenciaMedalla.setEnabled(r_activar);
			this.cmbPosicionTresPicos.setEnabled(r_activar);
			this.cmbEstadaoCapsula.setEnabled(r_activar);
		}
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		this.txtHoras.setValue(null);
		this.txtRealizado.setValue(null);
		this.txtObservaciones.setValue(null);
		this.cmbPresenciaRosca.setValue(null);
		this.cmbPresenciaFrontal.setValue(null);
		this.cmbExpulsorCajas.setValue(null);
		
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
			this.cmbNivel.setValue(null);
			this.cmbPresenciaCorcho.setValue(null);
			this.cmbPresenciaCapsula.setValue(null);
			this.cmbPresenciaContra.setValue(null);
			this.cmbPresenciaTirilla.setValue(null);
			this.cmbPresenciaMedalla.setValue(null);
			this.cmbPosicionTresPicos.setValue(null);
			this.cmbEstadaoCapsula.setValue(null);
		}
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;

    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboEstados();
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlDetectores) r_fila)!=null)
    	{
    		MapeoControlDetectores mapeoPreop = (MapeoControlDetectores) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlDetectores> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Detectores LINEA");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlDetectores> container = new BeanItemContainer<MapeoControlDetectores>(MapeoControlDetectores.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));

			    	if (lineaProduccion.contentEquals("Embotelladora"))
			    	{
			    		getColumn("nivel").setHeaderCaption("Nivel");
			    		getColumn("nivel").setWidth(new Double(220));
			    		getColumn("presenciaCorcho").setHeaderCaption("Corcho");
			    		getColumn("presenciaCorcho").setWidth(new Double(220));
			    		getColumn("presenciaRosca").setHeaderCaption("Rosca");
			    		getColumn("presenciaRosca").setWidth(new Double(220));
			    		getColumn("presenciaCapsula").setHeaderCaption("Capsula");
			    		getColumn("presenciaCapsula").setWidth(new Double(220));
			    		getColumn("presenciaFrontal").setHeaderCaption("Frontal");
			    		getColumn("presenciaFrontal").setWidth(new Double(220));
			    		getColumn("presenciaContra").setHeaderCaption("Contra");
			    		getColumn("presenciaContra").setWidth(new Double(220));
			    		getColumn("presenciaTirilla").setHeaderCaption("Tirilla");
			    		getColumn("presenciaTirilla").setWidth(new Double(220));
			    		getColumn("presenciaMedalla").setHeaderCaption("Medalla");
			    		getColumn("presenciaMedalla").setWidth(new Double(220));
			    		getColumn("posicionTresPicos").setHeaderCaption("Tres Picos");
			    		getColumn("posicionTresPicos").setWidth(new Double(220));
			    		getColumn("estadoCapsula").setHeaderCaption("Estado Capsula");
			    		getColumn("estadoCapsula").setWidth(new Double(220));
			    		getColumn("expulsorCajas").setHeaderCaption("Expulsor Cajas");
			    		getColumn("expulsorCajas").setWidth(new Double(220));
			    	}
			    	else
			    	{
			    		getColumn("presenciaRosca").setHeaderCaption("Rosca");
			    		getColumn("presenciaRosca").setWidth(new Double(220));
			    		getColumn("presenciaFrontal").setHeaderCaption("Frontal");
			    		getColumn("presenciaFrontal").setWidth(new Double(220));
			    		getColumn("expulsorCajas").setHeaderCaption("Expulsor Cajas");
			    		getColumn("expulsorCajas").setWidth(new Double(220));
			    	}
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
					getColumn("verificado").setHidden(true);
					
					if (lineaProduccion.contentEquals("Envasadora"))
					{
						getColumn("nivel").setHidden(true);
						getColumn("presenciaCorcho").setHidden(true);
						getColumn("presenciaCapsula").setHidden(true);
						getColumn("presenciaContra").setHidden(true);
						getColumn("presenciaTirilla").setHidden(true);
						getColumn("presenciaMedalla").setHidden(true);
						getColumn("posicionTresPicos").setHidden(true);
						getColumn("estadoCapsula").setHidden(true);
					}
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() 
				{					
					setColumnOrder("fecha", "hora", "nivel", "presenciaCorcho", "presenciaRosca","presenciaCapsula", "presenciaFrontal", "presenciaContra", "presenciaTirilla", "presenciaMedalla", "posicionTresPicos", "estadoCapsula", "expulsorCajas", "realizado", "verificado", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}

    private void cargarComboEstados()
    {
    	this.cargarValoresEstados(this.cmbPresenciaRosca);
    	this.cargarValoresEstados(this.cmbPresenciaFrontal);
    	this.cargarValoresEstados(this.cmbExpulsorCajas);
    	
    	if (this.lineaProduccion.contentEquals("Embotelladora"))
    	{
    		this.cargarValoresEstados(this.cmbEstadaoCapsula);
    		this.cargarValoresEstados(this.cmbNivel);
    		this.cargarValoresEstados(this.cmbPosicionTresPicos);
    		this.cargarValoresEstados(this.cmbPresenciaCapsula);
    		this.cargarValoresEstados(this.cmbPresenciaContra);
    		this.cargarValoresEstados(this.cmbPresenciaCorcho);
    		this.cargarValoresEstados(this.cmbPresenciaMedalla);
    		this.cargarValoresEstados(this.cmbPresenciaTirilla);
    	}
    }
    private void cargarValoresEstados(Combo r_combo)
    {
    	r_combo.addItem("CORRECTO");
    	r_combo.addItem("FALLO");
    	r_combo.addItem("INCORRECTO");
    	r_combo.addItem("REALIZADO REFERENCIA ANTERIOR");
    	r_combo.addItem("SIN PRODUCCION");
    }
    
}