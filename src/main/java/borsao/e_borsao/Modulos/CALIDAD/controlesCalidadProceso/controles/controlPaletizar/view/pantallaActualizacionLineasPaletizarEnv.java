package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.view;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlLineasPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.server.consultaPaletizarServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaActualizacionLineasPaletizarEnv extends Window
{
	private MapeoControlLineasPaletizar mapeoControlLineasPaletizar = null;
	private pantallaPaletizar App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private Combo cmbCaja1= null;
	private Combo cmbCaja2= null;
	private Combo cmbCaja3= null;
	private Combo cmbCaja4= null;
	private TextField cmbCaja5= null;
	private TextField cmbCaja6= null;

	private Combo cmbCaja7= null;
	private Combo cmbCaja8= null;
	private Combo cmbCaja9= null;
	private Combo cmbCaja10= null;
	private Combo cmbCaja11= null;
	
	private String linea = null;
	private String caja = null;
	private Integer idProgramacion = null;
	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasPaletizarEnv(Ventana r_App, Integer r_idProgramacion, String r_caja, String r_articulo, String r_titulo)
	{
		this.App = (pantallaPaletizar) r_App;
		this.linea=App.lineaProduccion;
		this.caja = r_caja;
		this.idProgramacion = r_idProgramacion;
		
		this.cargarPantalla();
		this.cargarCombo();
		this.habilitarCampos();
		this.cargarListeners();
		
		this.rellenarEntradasDatos();
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
	}

	private void rellenarEntradasDatos()
	{
		consultaPaletizarServer cadps = consultaPaletizarServer.getInstance(CurrentUser.get());
		HashMap<String, String> hash = cadps.recuperarDatosCaja(this.idProgramacion,this.caja,this.linea);
		
		/*
		 * con el hash relleno las entradas de datos
		 */
		
		if (hash!=null && !hash.isEmpty())
		{
			this.cmbCaja1.setValue(hash.get("01"));
			this.cmbCaja2.setValue(hash.get("02"));
			this.cmbCaja3.setValue(hash.get("03"));
			this.cmbCaja4.setValue(hash.get("04"));
			this.cmbCaja5.setValue(hash.get("05"));
			this.cmbCaja6.setValue(hash.get("06"));
			this.cmbCaja7.setValue(hash.get("07"));
			this.cmbCaja8.setValue(hash.get("08"));
		}
	}
	private void habilitarCampos()
	{
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
						HashMap<String, String> valores = new HashMap<String, String>();
						valores.put("campo", caja);
						if (cmbCaja1.getValue()!=null) valores.put("01", cmbCaja1.getValue().toString()); else valores.put("01","");
						if (cmbCaja2.getValue()!=null) valores.put("02", cmbCaja2.getValue().toString()); else valores.put("02","");
						if (cmbCaja3.getValue()!=null) valores.put("03", cmbCaja3.getValue().toString()); else valores.put("03","");
						if (cmbCaja4.getValue()!=null) valores.put("04", cmbCaja4.getValue().toString()); else valores.put("04","");
						if (cmbCaja5.getValue()!=null) valores.put("05", cmbCaja5.getValue().toString()); else valores.put("05","");
						if (cmbCaja6.getValue()!=null) valores.put("06", cmbCaja6.getValue().toString()); else valores.put("06","");
						if (cmbCaja7.getValue()!=null) valores.put("07", cmbCaja7.getValue().toString()); else valores.put("07","");
						if (cmbCaja8.getValue()!=null) valores.put("08", cmbCaja8.getValue().toString()); else valores.put("08","");

					App.actualizarDatosLineaPaletizar(idProgramacion, valores);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		String campoErroneo = null;
		
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbCaja1=new Combo("Garrafa");
				this.propiedadesCombos(this.cmbCaja1);
				
				this.cmbCaja2=new Combo("Etiqueta");
				this.propiedadesCombos(this.cmbCaja2);

				this.cmbCaja3=new Combo("Prueba Asa");
				this.propiedadesCombos(this.cmbCaja3);
				
				fila1.addComponent(cmbCaja1);
				fila1.addComponent(cmbCaja2);
				fila1.addComponent(cmbCaja3);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.cmbCaja4=new Combo("Tapon");
				this.propiedadesCombos(this.cmbCaja4);
				
				this.cmbCaja5=new TextField("Lote Garrafa");
				this.cmbCaja5.setWidth("200px");

				this.cmbCaja6=new TextField("Lote Caja");
				this.cmbCaja6.setWidth("200px");

				fila2.addComponent(cmbCaja4);
				fila2.addComponent(cmbCaja5);
				fila2.addComponent(cmbCaja6);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				
				this.cmbCaja7=new Combo("Pesadora");
				this.propiedadesCombos(this.cmbCaja7);

				this.cmbCaja8=new Combo("SSCC");
				this.propiedadesCombos(this.cmbCaja8);
				
				fila3.addComponent(cmbCaja7);
				fila3.addComponent(cmbCaja8);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
	}
	private void cargarCombo()
	{
		this.cargarValoresEstados(cmbCaja1);
		this.cargarValoresEstados(cmbCaja2);
		this.cargarValoresEstados(cmbCaja3);
		this.cargarValoresEstados(cmbCaja4);
		this.cargarValoresEstados(cmbCaja7);
		this.cargarValoresEstados(cmbCaja8);
	}
	
	private void cargarValoresEstados(Combo r_combo)
    {
    	r_combo.addItem("CORRECTO");
    	r_combo.addItem("FALLO");
    	r_combo.addItem("INCORRECTO");
    	r_combo.addItem("REALIZADO REFERENCIA ANTERIOR");
    	r_combo.addItem("SIN PRODUCCION");
    	r_combo.addItem("");
    }

	private void propiedadesCombos(Combo r_combo)
	{
		r_combo.setWidth("200px");
		r_combo.setNewItemsAllowed(false);
		r_combo.setNullSelectionAllowed(true);
	}
}