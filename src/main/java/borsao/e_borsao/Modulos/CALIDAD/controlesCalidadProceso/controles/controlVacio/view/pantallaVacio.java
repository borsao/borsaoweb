package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.modelo.MapeoControlVacio;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVacio.server.consultaVacioServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaVacio extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlVacio mapeoControlPreoprativo = null;
	private consultaVacioServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtCabezal1 = null;
	private TextField txtCabezal2 = null;
	private TextField txtCabezal3 = null;
	private TextField txtCabezal4 = null;
	private TextField txtCabezal5 = null;
	private TextField txtCabezal6 = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "referencia";
	private Label lblCumplimentacion = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaVacio(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaVacioServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setCaption("Nº de Muestras: 6 botellas");
				this.lblCumplimentacion.setValue("Frecuencia: Al comienzo y final de cada referencia de corcho, cada 2 horas y cada cambio de lote de corcho. ");
				this.lblCumplimentacion.setWidth("500px");
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");
    			
				this.txtCabezal1= new TextField();
				this.txtCabezal1.setCaption("Cabezal 1");
				this.txtCabezal1.setVisible(true);
				this.txtCabezal1.setWidth("120px");
				this.txtCabezal1.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal1.addStyleName("rightAligned");
        		
				this.txtCabezal2= new TextField();
				this.txtCabezal2.setCaption("Cabezal 2");
				this.txtCabezal2.setVisible(true);
				this.txtCabezal2.setWidth("120px");
				this.txtCabezal2.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal2.addStyleName("rightAligned");
				
				this.txtCabezal3= new TextField();
				this.txtCabezal3.setCaption("Cabezal 3");
				this.txtCabezal3.setVisible(true);
				this.txtCabezal3.setWidth("120px");
				this.txtCabezal3.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal3.addStyleName("rightAligned");
				
				this.txtCabezal4= new TextField();
				this.txtCabezal4.setCaption("Cabezal 4");
				this.txtCabezal4.setVisible(true);
				this.txtCabezal4.setWidth("120px");
				this.txtCabezal4.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal4.addStyleName("rightAligned");
				
				this.txtCabezal5= new TextField();
				this.txtCabezal5.setCaption("Cabezal 5");
				this.txtCabezal5.setVisible(true);
				this.txtCabezal5.setWidth("120px");
				this.txtCabezal5.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal5.addStyleName("rightAligned");
				
				this.txtCabezal6= new TextField();
				this.txtCabezal6.setCaption("Cabezal 6");
				this.txtCabezal6.setVisible(true);
				this.txtCabezal6.setWidth("120px");
				this.txtCabezal6.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCabezal6.addStyleName("rightAligned");
			

    			linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.txtCabezal1);
    			linea2.addComponent(this.txtCabezal2);
    			linea2.addComponent(this.txtCabezal3);
    			linea2.addComponent(this.txtCabezal4);
    			linea2.addComponent(this.txtCabezal5);
    			linea2.addComponent(this.txtCabezal6);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.txtRealizado= new TextField();
    			this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");
				
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);
    			
    			linea3.addComponent(this.txtRealizado);
    			linea3.addComponent(this.txtObservaciones);
    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{	
			MapeoControlVacio mapeo = new MapeoControlVacio();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}			
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlVacio mapeo = new MapeoControlVacio();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setHora(this.txtHoras.getValue());
			if (this.txtCabezal1.getValue()!=null && this.txtCabezal1.getValue().length()>0) mapeo.setCabezal1(new Double(this.txtCabezal1.getValue())); else mapeo.setCabezal1(null);
			if (this.txtCabezal2.getValue()!=null && this.txtCabezal2.getValue().length()>0) mapeo.setCabezal2(new Double(this.txtCabezal2.getValue())); else mapeo.setCabezal2(null);
			if (this.txtCabezal3.getValue()!=null && this.txtCabezal3.getValue().length()>0) mapeo.setCabezal3(new Double(this.txtCabezal3.getValue())); else mapeo.setCabezal3(null);
			if (this.txtCabezal4.getValue()!=null && this.txtCabezal4.getValue().length()>0) mapeo.setCabezal4(new Double(this.txtCabezal4.getValue())); else mapeo.setCabezal4(null);
			if (this.txtCabezal5.getValue()!=null && this.txtCabezal5.getValue().length()>0) mapeo.setCabezal5(new Double(this.txtCabezal5.getValue())); else mapeo.setCabezal5(null);
			if (this.txtCabezal6.getValue()!=null && this.txtCabezal6.getValue().length()>0) mapeo.setCabezal6(new Double(this.txtCabezal6.getValue())); else mapeo.setCabezal6(null);
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoVacio(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
				rdo = cncs.guardarCambiosVacio(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlVacio> vector=null;

    	MapeoControlVacio mapeoPreop = new MapeoControlVacio();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoProduccion.getIdProgramacion(), this.lineaProduccion,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlVacio r_mapeo)
	{
		this.mapeoControlPreoprativo = new MapeoControlVacio();
		this.mapeoControlPreoprativo.setIdCodigo(r_mapeo.getIdCodigo());
		setCreacion(false);
		
		this.txtHoras.setValue(r_mapeo.getHora());
		if (r_mapeo.getCabezal1()!=null) 
			this.txtCabezal1.setValue(r_mapeo.getCabezal1().toString());
		if (r_mapeo.getCabezal2()!=null) 
			this.txtCabezal2.setValue(r_mapeo.getCabezal2().toString());
		if (r_mapeo.getCabezal3()!=null) 
			this.txtCabezal3.setValue(r_mapeo.getCabezal3().toString());
		if (r_mapeo.getCabezal4()!=null) 
			this.txtCabezal4.setValue(r_mapeo.getCabezal4().toString());
		if (r_mapeo.getCabezal5()!=null) 
			this.txtCabezal5.setValue(r_mapeo.getCabezal5().toString());
		if (r_mapeo.getCabezal6()!=null) 
			this.txtCabezal6.setValue(r_mapeo.getCabezal6().toString());
		
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    	
		this.txtCabezal1.setEnabled(r_activar);
		this.txtCabezal2.setEnabled(r_activar);
		this.txtCabezal3.setEnabled(r_activar);
		this.txtCabezal4.setEnabled(r_activar);
		this.txtCabezal5.setEnabled(r_activar);
		this.txtCabezal6.setEnabled(r_activar);

    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtCabezal1.setValue(null);
		this.txtCabezal2.setValue(null);
		this.txtCabezal3.setValue(null);
		this.txtCabezal4.setValue(null);
		this.txtCabezal5.setValue(null);
		this.txtCabezal6.setValue(null);
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }
    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlVacio) r_fila)!=null)
    	{
    		MapeoControlVacio mapeoPreop = (MapeoControlVacio) r_fila;
    		if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlVacio> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Vacios Realizados");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlVacio> container = new BeanItemContainer<MapeoControlVacio>(MapeoControlVacio.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("hora").setHeaderCaption("Hora");
			    	getColumn("hora").setWidth(new Double(100));
			    	getColumn("cabezal1").setHeaderCaption("Cabezal 1");
			    	getColumn("cabezal1").setWidth(new Double(120));
			    	getColumn("cabezal2").setHeaderCaption("Cabezal 2");
			    	getColumn("cabezal2").setWidth(new Double(120));
			    	getColumn("cabezal3").setHeaderCaption("Cabezal 3");
			    	getColumn("cabezal3").setWidth(new Double(120));
			    	getColumn("cabezal4").setHeaderCaption("Cabezal 4");
			    	getColumn("cabezal4").setWidth(new Double(120));
			    	getColumn("cabezal5").setHeaderCaption("Cabezal 5");
			    	getColumn("cabezal5").setWidth(new Double(120));
			    	getColumn("cabezal6").setHeaderCaption("Cabezal 6");
			    	getColumn("cabezal6").setWidth(new Double(120));
			    	getColumn("realizado").setHeaderCaption("Realizado");
			    	getColumn("realizado").setWidth(new Double(155));
			    	getColumn("verificado").setHeaderCaption("Verificado");
			    	getColumn("verificado").setWidth(new Double(155));

					getColumn("idCodigo").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
					getColumn("linea").setHidden(true);
					getColumn("fecha").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					setColumnOrder("fecha", "hora", "cabezal1", "cabezal2","cabezal3", "cabezal4", "cabezal5", "cabezal6", "realizado", "verificado", "observaciones");
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}