package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.modelo.MapeoControlLineasLotesMP;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.modelo.MapeoControlLotesMP;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlLotesMP.server.consultaLotesMPServer;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.view.PantallaControlesCalidadProceso;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaLotesMP extends Ventana
{
	private MapeoControlesProcesoProductivo mapeoProduccion = null;
	private MapeoControlLotesMP mapeoControlLotesMP = null;
	private MapeoControlLineasLotesMP mapeoControlLineasLotesMP = null;
	private consultaLotesMPServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout frameGridLineas = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private Panel panelGridLineas = null;
	private GridPropio gridDatos = null;
	private GridPropio gridDatosLineas = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	private TextField txtCantidad= null;
	
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private String lineaProduccion = null;
	private PantallaControlesCalidadProceso app=null;
	private static String tipoControl = "referencia";
	private Label lblCumplimentacion = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaLotesMP(PantallaControlesCalidadProceso r_app, String r_titulo, MapeoControlesProcesoProductivo r_mapeo, String r_linea)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoProduccion = r_mapeo;
		this.lineaProduccion=r_linea;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaLotesMPServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("100%");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
//			this.frameCentral.setSpacing(true);
//			this.frameCentral.setHeightUndefined();
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(true);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
				this.lblCumplimentacion = new Label();
				this.lblCumplimentacion.setValue("Frecuencia: Cada cambio de lote");
//				this.lblCumplimentacion.setWidth("100%");
//				this.lblCumplimentacion.setStyleName(ValoTheme.LABEL_COLORED);
				this.lblCumplimentacion.addStyleName("lblTituloCalidad");
				
				linea1.addComponent(this.txtEjercicio);
				linea1.addComponent(this.cmbSemana);
				linea1.addComponent(this.txtFecha);
				linea1.addComponent(this.lblCumplimentacion);
				linea1.setComponentAlignment(this.lblCumplimentacion, Alignment.MIDDLE_CENTER);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");

				this.txtCantidad = new TextField();
				this.txtCantidad.setCaption("Nº Botellas");
				this.txtCantidad.setWidth("120px");
				this.txtCantidad.setValue("0");
				this.txtCantidad.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.txtRealizado= new TextField();
				this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");

				this.txtObservaciones= new TextArea();
				this.txtObservaciones.setCaption("Observaciones");
				this.txtObservaciones.setWidth("300");

				linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.txtCantidad);
    			linea2.addComponent(this.txtRealizado);
    			linea2.addComponent(this.txtObservaciones);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
				
    			btnLimpiar= new Button("Cambio Lote");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnLimpiar.setEnabled(false);
    			
    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);
    			
    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		this.frameGrid.setHeight("175px");

    		this.frameGridLineas = new HorizontalLayout();
    		this.frameGridLineas.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		
			this.framePie = new HorizontalLayout();
			this.framePie.setHeight("3%");
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.TOP_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
			
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
//		principal.setExpandRatio(this.frameCentral, 1);
//		principal.setExpandRatio(this.framePie, 2);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		if (this.txtCantidad.getValue()==null || this.txtCantidad.getValue().length()==0 || this.txtCantidad.getValue().toString()=="0")
		{
			Notificaciones.getInstance().mensajeError("Debes indicar el número de botellas antes de pasar a otro control / lote.");
		}
		else
		{
			limpiarCampos();
		}
	}
	
	private void eliminar()
	{
		if (!this.cncs.comrpboarVerificado(this.mapeoProduccion.getIdProgramacion(), lineaProduccion))
		{	
			MapeoControlLotesMP mapeo = new MapeoControlLotesMP();
			mapeo.setLinea(lineaProduccion);
			mapeo.setIdCodigo(this.mapeoControlLotesMP.getIdCodigo());
			
			cncs.eliminar(mapeo);
			if (gridDatos!=null)
			{
				gridDatos.removeAllColumns();
				gridDatos=null;
				frameGrid.removeComponent(panelGrid);
				panelGrid=null;
			}
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
			}
			llenarRegistros();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Control Verificado. No es posible eliminar");
		}			
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlLotesMP mapeo = new MapeoControlLotesMP();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setCantidad(new Integer(this.txtCantidad.getValue()));
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
			mapeo.setLinea(lineaProduccion);

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoLotesMP(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
						frameGrid.removeComponent(panelGrid);
					}
					panelGrid=null;
					if (gridDatosLineas!=null)
					{
						gridDatosLineas.removeAllColumns();
						gridDatosLineas=null;
						frameGridLineas.removeComponent(panelGridLineas);
					}
					panelGridLineas=null;
					llenarRegistros();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlLotesMP.getIdCodigo());
				rdo = cncs.guardarCambiosLotesMP(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					panelGridLineas=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlLotesMP> vector=null;

    	MapeoControlLotesMP mapeoPreop = new MapeoControlLotesMP();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	vector = this.cncs.datosOpcionesGlobal(mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlLotesMP r_mapeo)
	{
		this.mapeoControlLotesMP= new MapeoControlLotesMP();
		this.mapeoControlLotesMP.setIdCodigo(r_mapeo.getIdCodigo());
		
		setCreacion(false);
		
		this.txtHoras.setValue(r_mapeo.getHora());
		if (r_mapeo.getCantidad()!=null) this.txtCantidad.setValue(r_mapeo.getCantidad().toString());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	txtCantidad.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		this.btnLimpiar.setEnabled(false);
		
		this.txtHoras.setValue("");
		this.txtCantidad.setValue("0");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	if ((this.txtCantidad.getValue()==null || this.txtCantidad.getValue().toString().length()==0 || this.txtCantidad.getValue().toString().contentEquals("0")) && !isCreacion())
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el número botellas.");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		this.app.activarControles(tipoControl);
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlLotesMP) r_fila)!=null)
    	{
    		MapeoControlLotesMP mapeoLotesMP = (MapeoControlLotesMP) r_fila;
    		
    		
    		if (mapeoLotesMP!=null)
    		{
    			this.llenarEntradasDatos(mapeoLotesMP);
    			
				if (gridDatosLineas!=null)
				{
					gridDatosLineas.removeAllColumns();
					gridDatosLineas=null;
					frameGridLineas.removeComponent(panelGridLineas);
					frameCentral.removeComponent(frameGridLineas);
				}
				panelGridLineas=null;

    			this.frameGridLineas.addComponent(this.generarGridLineas(mapeoLotesMP.getIdCodigo()));
    			this.frameCentral.addComponent(this.frameGridLineas);
    			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
    		}
    		if (this.txtCantidad.getValue()==null || this.txtCantidad.getValue().length()==0 || this.txtCantidad.getValue().toString().contentEquals("0"))
    		{
    			this.btnLimpiar.setEnabled(false);
    		}
    		else 
    		{
    			consultaLotesMPServer cls = consultaLotesMPServer.getInstance(CurrentUser.get());
    			mapeoLotesMP.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    			this.btnLimpiar.setEnabled(!cls.datosSinBotellasLote(mapeoLotesMP));
    		}
		}
    	else this.limpiarCampos();
    	
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlLotesMP> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */

    	if (r_vector!= null && !r_vector.isEmpty())
		{
			this.frameGrid.addComponent(this.generarGridCAb(r_vector));
			this.frameCentral.addComponent(this.frameGrid);
			this.frameCentral.setExpandRatio(this.frameGrid, 2);
			this.gridDatos.select(this.gridDatos.getContainerDataSource().getIdByIndex(r_vector.size()-1));
			this.filaSeleccionada(this.gridDatos.getSelectedRow());
		}
    	else
    	{
    		this.limpiarCampos();
    	}
		
//		this.frameCentral.setHeight("94%");
		
	}
    
    private Panel generarGridCAb(ArrayList<MapeoControlLotesMP> r_vector)
    {
    	panelGrid= new Panel("Control Lotes Material Auxiliar");
    	panelGrid.setSizeFull(); // Shrink to fit content
    	
    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		BeanItemContainer<MapeoControlLotesMP> container = new BeanItemContainer<MapeoControlLotesMP>(MapeoControlLotesMP.class);
		this.gridDatos=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
				
		    	getColumn("fecha").setHeaderCaption("Fecha");
		    	getColumn("fecha").setWidth(new Double(120));
		    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
		    	getColumn("hora").setHeaderCaption("Hora");
		    	getColumn("hora").setWidth(new Double(100));
		    	getColumn("cantidad").setHeaderCaption("Cantidad");
		    	getColumn("cantidad").setWidth(new Double(100));
		    	getColumn("realizado").setHeaderCaption("Realizado");
		    	getColumn("realizado").setWidth(new Double(155));
		    	getColumn("verificado").setHeaderCaption("Verificado");
		    	getColumn("verificado").setWidth(new Double(155));

				getColumn("idCodigo").setHidden(true);
				getColumn("idProgramacion").setHidden(true);
				getColumn("linea").setHidden(true);
				getColumn("fecha").setHidden(true);
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				
				setColumnOrder("fecha", "hora", "cantidad", "realizado", "verificado", "observaciones");
				
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {
				addItemClickListener(new ItemClickEvent.ItemClickListener() 
		    	{
		            public void itemClick(ItemClickEvent event) 
		            {
		        		filaSeleccionada(event.getItemId());
		    		}
		        });

			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
			}
		};
		this.gridDatos.setContainerDataSource(container);

		this.gridDatos.getContainer().removeAllItems();
		this.gridDatos.getContainer().addAll(r_vector);
		
		this.gridDatos.addStyleName("minigrid");
		this.gridDatos.setEditorEnabled(false);
		this.gridDatos.setConFiltro(false);
		this.gridDatos.setSeleccion(SelectionMode.SINGLE);
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
    	
		this.gridDatos.establecerTitulosColumnas();
		this.gridDatos.establecerOrdenPresentacionColumnas();
		this.gridDatos.cargarListeners();
		
		panelGrid.setContent(gridDatos);
    	
    	return panelGrid;
    }
    private Panel generarGridLineas(Integer r_id)
    {
    	
    	pantallaLotesMP app = this;
    	MapeoControlLotesMP mapeoPreop = new MapeoControlLotesMP();
    	mapeoPreop.setIdProgramacion(this.mapeoProduccion.getIdProgramacion());
    	mapeoPreop.setLinea(this.lineaProduccion);
    	ArrayList<MapeoControlLineasLotesMP> vectorLineas = this.cncs.datosOpcionesGlobalLineas(r_id, mapeoPreop,this.mapeoProduccion.getEjercicio(), this.mapeoProduccion.getSemana());

    	panelGridLineas= new Panel("Control Lineas Lotes Material Auxiliar ");
    	panelGridLineas.setSizeFull(); // Shrink to fit content

    	BeanItemContainer<MapeoControlLineasLotesMP> container = new BeanItemContainer<MapeoControlLineasLotesMP>(MapeoControlLineasLotesMP.class);
		this.gridDatosLineas=new GridPropio() {
			
			@Override
			public void establecerTitulosColumnas() {
				
		    	getColumn("acc").setHeaderCaption("");
		    	getColumn("acc").setSortable(true);
		    	getColumn("acc").setWidth(new Double(50));

				getColumn("material").setHeaderCaption("Material");
				getColumn("material").setWidth(new Double(250));
				getColumn("descripcion").setHeaderCaption("Descripcion");
				getColumn("descripcion").setWidth(new Double(250));
				getColumn("proveedor").setHeaderCaption("Proveedor");
				getColumn("proveedor").setWidth(new Double(175));
				getColumn("lote").setHeaderCaption("Lote");
				getColumn("lote").setWidth(new Double(100));
				getColumn("fechaFabricacion").setHeaderCaption("Fecha Fab.");
				getColumn("fechaFabricacion").setWidth(new Double(150));
				getColumn("cantidad").setHeaderCaption("Cantidad");
				getColumn("cantidad").setWidth(new Double(100));
				
				getColumn("idCodigo").setHidden(true);
				getColumn("idLoteMP").setHidden(true);
				getColumn("observaciones").setHidden(true);
				getColumn("verificado").setHidden(true);
				
				this.getColumn("fechaFabricacion").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			}
			
			@Override
			public void establecerOrdenPresentacionColumnas() {
				
				setColumnOrder("acc", "material", "descripcion","proveedor","lote","fechaFabricacion", "cantidad");
				
			}
			
			@Override
			public void establecerColumnasNoFiltro() {
			}
			
			@Override
			public void cargarListeners() {
				addItemClickListener(new ItemClickEvent.ItemClickListener() 
				{
					public void itemClick(ItemClickEvent event) 
					{
						if (event.getPropertyId()!=null)
		            	{
							MapeoControlLineasLotesMP mapeoOrig = (MapeoControlLineasLotesMP) event.getItemId();
			            	
		            		if (event.getPropertyId().toString().equals("acc"))
			            	{
			        			try
			        			{
			        				/*
			        				 * ME falta coger el nombre del articulo de materia prima correspondiente y pasarlo en 
			        				 * el campo descripcion del mapeo
			        				 * 
			        				 * asi se evitan escribirlo todas las veces
			        				 */
			        				if (mapeoOrig.getDescripcion()==null || mapeoOrig.getDescripcion().length()==0)
			        				{
			        					String descripcionArticulo = recuperarMateriaPrimaProgramacion(mapeoOrig.getMaterial());
			        					mapeoOrig.setDescripcion(descripcionArticulo);
			        				}
			        				pantallaActualizacionLineasLotes vt = new pantallaActualizacionLineasLotes(app, mapeoOrig, "", "Datos relativos a " + mapeoOrig.getMaterial());
			        				app.getUI().addWindow(vt);
			        			}
			        			catch (IndexOutOfBoundsException ex)
			        			{
			        				
			        			}
			            	}            	
			            	else
			            	{
			            	}
			            }
					}
				});
				
			}
			
			@Override
			public void calcularTotal() {
			}
			
			@Override
			public void asignarEstilos() {
		    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
		            
		            @Override
		            public String getStyle(Grid.CellReference cellReference) {
		            	if ("acc".equals(cellReference.getPropertyId())) 
		            	{            		
		            		return "cell-nativebuttonProgramacion";
		            	}
		            	else if ("cantidad".equals(cellReference.getPropertyId())) 
		            	{            		
		            		return "Rcell-normal";
		            	}
		            	else
		            	{
		            		return "cell-normal";
		            	}
		            }
		        });

			}
		};
		this.gridDatosLineas.setContainerDataSource(container);
		
		this.gridDatosLineas.getContainer().removeAllItems();
		this.gridDatosLineas.getContainer().addAll(vectorLineas);
		
		
		this.gridDatosLineas.addStyleName("smallgrid");
		this.gridDatosLineas.setEditorEnabled(false);
		this.gridDatosLineas.setConFiltro(false);
		this.gridDatosLineas.setSeleccion(SelectionMode.SINGLE);
		this.gridDatosLineas.setWidth("100%");
		this.gridDatosLineas.setHeight("100%");
		
		this.gridDatosLineas.establecerTitulosColumnas();
		this.gridDatosLineas.establecerOrdenPresentacionColumnas();
		this.gridDatosLineas.cargarListeners();
		this.gridDatosLineas.asignarEstilos();
		panelGridLineas.setContent(gridDatosLineas);
		
		return panelGridLineas;
    }
    
	public void actualizarDatosLineaLote(MapeoControlLineasLotesMP r_mapeo)
	{
		consultaLotesMPServer cps = consultaLotesMPServer.getInstance(CurrentUser.get());
		String rdo = cps.guardarCambiosLineasLoteMP(this.lineaProduccion, r_mapeo);
		
		if (rdo != null)
		{
			Notificaciones.getInstance().mensajeError("Error al actualizar los datos de la linea del lote");
		}
		else
		{
			if (gridDatosLineas!=null)
			{
				gridDatosLineas.removeAllColumns();
				gridDatosLineas=null;
				frameGridLineas.removeComponent(panelGridLineas);
				panelGridLineas=null;
				this.frameCentral.removeComponent(this.frameGridLineas);
			}

			this.frameGridLineas.addComponent(this.generarGridLineas(r_mapeo.getIdLoteMP()));
			this.frameCentral.addComponent(this.frameGridLineas);
			this.frameCentral.setExpandRatio(this.frameGridLineas, 1);
		}
	}

	public String recuperarMateriaPrimaProgramacion(String r_material)
	{
		String des=null; 
		String rdo = null;
		String masc = null;
		String descripcion = null;
		String articuloPadre = null;
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
		
		if (this.lineaProduccion.contentEquals("Embotelladora"))
		{
			consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
			articuloPadre = cps.recogerArticuloProgramacion(this.mapeoProduccion.getIdProgramacion());
			masc=recuperarMascara(r_material);
			if (masc!=null)
			{
				if (r_material.startsWith("06")) descripcion = "ET";
				if (r_material.startsWith("08")) descripcion = "TIRI";
				if (r_material.startsWith("09")) descripcion = "MEDAL";
				rdo = ces.recuperarComponente(articuloPadre, masc,descripcion);
			}
		}
		else if (this.lineaProduccion.contentEquals("BIB"))
		{
			consultaProgramacionEnvasadoraServer cps = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			articuloPadre = cps.recogerArticuloProgramacion(this.mapeoProduccion.getIdProgramacion());
			masc=recuperarMascara(r_material);
			if (masc!=null)
			{
				if (r_material.startsWith("05")) descripcion = "CAJA REEM";
				if (r_material.startsWith("04")) descripcion = "CAJA BIB";
				rdo = ces.recuperarComponente(articuloPadre, masc,descripcion);
			}
		}
		else if (this.lineaProduccion.contentEquals("Envasadora"))
		{
			consultaProgramacionEnvasadoraServer cps = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			articuloPadre = cps.recogerArticuloProgramacion(this.mapeoProduccion.getIdProgramacion());
			masc=recuperarMascara(r_material);
			if (masc!=null)
			{
				if (r_material.startsWith("04")) descripcion = "CAJA";
				if (r_material.startsWith("05")) descripcion = "BANDEJA";
				rdo = ces.recuperarComponente(articuloPadre, masc,descripcion);
			}
		}

		if (rdo!=null)
		{
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			des = cas.obtenerDescripcionArticulo(rdo);
		}
		else
			des = null;
		
		return des;
	}
	
	private String recuperarMascara(String r_material)
	{
		String mascara = null;
		switch (r_material)
		{
			case "01 NITROGENO":
			case "02 CARBONICO":
			{
				mascara = null;
				break;
			}
			case "01 GARRAFA":
			case "02 BOTELLA":
			case "03 BOLSA":
			{
				mascara = "0104";
				break;
			}
			case "03 CORCHO":
			{
				mascara = "0105";
				break;
			}
			case "02 TAPON":
			case "04 TAPON ROSCA":
			{
				mascara = "0105";
				break;
			}
			case "05 CAPSULA":
			{
				mascara = "0108";
				break;
			}
			case "03 ETIQUETA":
			case "06 ETIQUETA":
			{
				mascara = "0106";
				break;
			}
			case "07 CONTRA":
			{
				mascara = "0107";
				break;
			}
			case "08 TIRILLA":
			{
				mascara = "0106";
				break;
			}
			case "09 MEDALLA":
			{
				mascara = "0106";
				break;
			}
			case "04 CAJA":
			case "04 CAJA IND":
			case "05 BANDEJA":
			case "05 CAJA":
			case "10 CAJA":
			{
				mascara = "0109";
				break;
			}
			
		}
		return mascara;
	}
}