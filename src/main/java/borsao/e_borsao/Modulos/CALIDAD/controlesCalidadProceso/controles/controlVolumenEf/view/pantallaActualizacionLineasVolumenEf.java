package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.view;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.modelo.MapeoControlLineasVolumenEf;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.server.consultaVolumenEfServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaActualizacionLineasVolumenEf extends Window
{
	private MapeoControlLineasVolumenEf mapeoControlLineasVolumenEf = null;
	private pantallaVolumenEf App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtHora= null;
	private TextField txtArticulo= null;
	private TextField txtMuestra1= null;
	private TextField txtMuestra2= null;
	private TextField txtMuestra3= null;
	private TextField txtMuestra4= null;
	private TextField txtMuestra5= null;
	private TextField txtMuestra6= null;
	private TextField txtMuestra7= null;
	private TextField txtMuestra8= null;
	private TextField txtMuestra9= null;
	private TextField txtMuestra10= null;
	private TextField txtMuestra11= null;
	private TextField txtMuestra12= null;
	private TextField txtMuestra13= null;
	private TextField txtMuestra14= null;
	private TextField txtMuestra15= null;
	private TextField txtMuestra16= null;
	private TextField txtMuestra17= null;
	private TextField txtMuestra18= null;
	private TextField txtMuestra19= null;
	private TextField txtMuestra20= null;
	private Label lblMedia= null;

	private String linea = null;
	private Integer idVolumenEf= null;
	private Integer idLinea= null;
	private boolean creando = false;
	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasVolumenEf(Ventana r_App, MapeoControlLineasVolumenEf r_mapeo, String r_titulo, boolean r_creando)
	{
		this.creando=r_creando;
		this.App = (pantallaVolumenEf) r_App;
		this.linea=App.lineaProduccion;
		this.mapeoControlLineasVolumenEf = r_mapeo;
		this.idVolumenEf = r_mapeo.getIdVolumen();
		this.idLinea = r_mapeo.getIdCodigo();
		
		this.cargarPantalla();
		this.habilitarCampos();
		this.cargarListeners();
		this.rellenarEntradasDatos();
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
	}

	private void habilitarCampos()
	{
	}
	private void calcularMedia()
	{
		double muestras = 0;
		Double muestra1 = new Double(0);
		Double muestra2 = new Double(0);
		Double muestra3 = new Double(0);
		Double muestra4 = new Double(0);
		Double muestra5 = new Double(0);
		Double muestra6 = new Double(0);
		Double muestra7 = new Double(0);
		Double muestra8 = new Double(0);
		Double muestra9 = new Double(0);
		Double muestra10 = new Double(0);
		Double muestra11 = new Double(0);
		Double muestra12 = new Double(0);
		Double muestra13 = new Double(0);
		Double muestra14 = new Double(0);
		Double muestra15 = new Double(0);
		Double muestra16 = new Double(0);
		Double muestra17 = new Double(0);
		Double muestra18 = new Double(0);
		Double muestra19 = new Double(0);
		Double muestra20 = new Double(0);
		Double media = new Double(0);
		
		if (txtMuestra1.getValue()!=null && txtMuestra1.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra1.getValue()))>0)
		{
			muestra1 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra1.getValue()));
			muestras++;
		}
		if (txtMuestra2.getValue()!=null && txtMuestra2.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra2.getValue()))>0)
		{
			muestra2 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra2.getValue()));
			muestras++;
		}
		if (txtMuestra3.getValue()!=null && txtMuestra3.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra3.getValue()))>0) 
		{
			muestra3 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra3.getValue()));
			muestras++;
		}
		if (txtMuestra4.getValue()!=null && txtMuestra4.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra4.getValue()))>0)
		{
			muestra4 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra4.getValue()));
			muestras++;
		}
		if (txtMuestra5.getValue()!=null && txtMuestra5.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra5.getValue()))>0)
		{
			muestra5 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra5.getValue()));
			muestras++;
		}
		if (txtMuestra6.getValue()!=null && txtMuestra6.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra6.getValue()))>0)
		{
			muestra6 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra6.getValue()));
			muestras++;
		}
		if (txtMuestra7.getValue()!=null && txtMuestra7.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra7.getValue()))>0)
		{
			muestra7 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra7.getValue()));
			muestras++;
		}
		if (txtMuestra8.getValue()!=null && txtMuestra8.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra8.getValue()))>0)
		{
			muestra8 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra8.getValue()));
			muestras++;
		}
		if (txtMuestra9.getValue()!=null && txtMuestra9.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra9.getValue()))>0)
		{
			muestra9 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra9.getValue()));
			muestras++;
		}
		if (txtMuestra10.getValue()!=null && txtMuestra10.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra10.getValue()))>0)
		{
			muestra10 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra10.getValue()));
			muestras++;
		}
		if (txtMuestra11.getValue()!=null && txtMuestra11.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra11.getValue()))>0)
		{
			muestra11 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra11.getValue()));
			muestras++;
		}
		if (txtMuestra12.getValue()!=null && txtMuestra12.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra12.getValue()))>0)
		{
			muestra12 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra12.getValue()));
			muestras++;
		}
		if (txtMuestra13.getValue()!=null && txtMuestra13.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra13.getValue()))>0)
		{
			muestra13 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra13.getValue()));
			muestras++;
		}
		if (txtMuestra14.getValue()!=null && txtMuestra14.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra14.getValue()))>0)
		{
			muestra14 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra14.getValue()));
			muestras++;
		}
		if (txtMuestra15.getValue()!=null && txtMuestra15.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra15.getValue()))>0)
		{
			muestra15 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra15.getValue()));
			muestras++;
		}
		if (txtMuestra16.getValue()!=null && txtMuestra16.getValue().length()>0  && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra16.getValue()))>0)
		{
			muestra16 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra16.getValue()));
			muestras++;
		}
		if (txtMuestra17.getValue()!=null && txtMuestra17.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra17.getValue()))>0)
		{
			muestra17 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra17.getValue()));
			muestras++;
		}
		if (txtMuestra18.getValue()!=null && txtMuestra18.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra18.getValue()))>0)
		{
			muestra18 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra18.getValue()));
			muestras++;
		}
		if (txtMuestra19.getValue()!=null && txtMuestra19.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra19.getValue()))>0)
		{
			muestra19 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra19.getValue()));
			muestras++;
		}
		if (txtMuestra20.getValue()!=null && txtMuestra20.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra20.getValue()))>0)
		{
			muestra20 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra20.getValue()));
			muestras++;
		}
		
		media = (muestra1 + muestra2 + muestra3 + muestra4 + muestra5 + muestra6 + muestra7 + muestra8 + muestra9 + muestra10 + muestra11 + muestra12 + muestra13 + muestra14 + muestra15 + muestra16 + muestra17 + muestra18 + muestra19 + muestra20)/muestras;
		
		Double nuevaMedia = new Double(0);
		
		if (this.mapeoControlLineasVolumenEf.getMedia()!=null)
			nuevaMedia = (this.mapeoControlLineasVolumenEf.getMedia()+media)/2;
		else
			nuevaMedia = media;
		
		this.lblMedia.setValue(RutinasNumericas.formatearDoubleDecimales(new Double(RutinasCadenas.reemplazarComaMiles(nuevaMedia.toString())),4).toString());
	}
	
	private void cargarListeners()
	{
		txtMuestra1.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra1.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra2.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra2.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra3.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra3.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra4.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra4.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra5.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra5.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra6.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra6.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra7.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra7.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra8.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra8.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra9.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra9.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra10.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra10.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra11.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra11.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra12.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra12.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra13.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra13.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra14.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra14.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra15.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra15.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra16.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra16.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra17.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra17.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra18.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra18.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra19.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra19.getValue()))>0) calcularMedia();
			}
		});
		txtMuestra20.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra20.getValue()))>0) calcularMedia();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					double rango;
					double varianza =0;
					MapeoControlLineasVolumenEf mapeo = new MapeoControlLineasVolumenEf();
					
					mapeo.setHora(txtHora.getValue());
					mapeo.setArticulo(txtArticulo.getValue());
					mapeo.setMedia(new Double(RutinasCadenas.reemplazarComaMiles(lblMedia.getValue().toString())));
					
					if (txtMuestra1.getValue()!=null)
					{
						mapeo.setMuestra1(txtMuestra1.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra1()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra2.getValue()!=null && txtMuestra2.getValue().length()>0  && txtMuestra20.getValue()!="0")
					{
						mapeo.setMuestra2(txtMuestra2.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra2()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra3.getValue()!=null && txtMuestra3.getValue().length()>0)
					{
						mapeo.setMuestra3(txtMuestra3.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra3()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra4.getValue()!=null && txtMuestra4.getValue().length()>0)
					{
						mapeo.setMuestra4(txtMuestra4.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra4()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra5.getValue()!=null && txtMuestra5.getValue().length()>0)
					{
						mapeo.setMuestra5(txtMuestra5.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra5()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra6.getValue()!=null && txtMuestra6.getValue().length()>0)
					{
						mapeo.setMuestra6(txtMuestra6.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra6()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra7.getValue()!=null && txtMuestra7.getValue().length()>0)
					{
						mapeo.setMuestra7(txtMuestra7.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra7()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra8.getValue()!=null && txtMuestra8.getValue().length()>0)
					{
						mapeo.setMuestra8(txtMuestra8.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra8()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra9.getValue()!=null && txtMuestra9.getValue().length()>0)
					{
						mapeo.setMuestra9(txtMuestra9.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra9()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra10.getValue()!=null && txtMuestra10.getValue().length()>0)
					{
						mapeo.setMuestra10(txtMuestra10.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra10()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra11.getValue()!=null && txtMuestra11.getValue().length()>0)
					{
						mapeo.setMuestra11(txtMuestra11.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra11()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra12.getValue()!=null && txtMuestra12.getValue().length()>0)
					{
						mapeo.setMuestra12(txtMuestra12.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra12()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra13.getValue()!=null && txtMuestra13.getValue().length()>0)
					{
						mapeo.setMuestra13(txtMuestra13.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra13()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra14.getValue()!=null && txtMuestra14.getValue().length()>0)
					{
						mapeo.setMuestra14(txtMuestra14.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra14()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra15.getValue()!=null && txtMuestra15.getValue().length()>0)
					{
						mapeo.setMuestra15(txtMuestra15.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra15()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra16.getValue()!=null && txtMuestra16.getValue().length()>0)
					{
						mapeo.setMuestra16(txtMuestra16.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra16()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra17.getValue()!=null && txtMuestra17.getValue().length()>0)
					{
						mapeo.setMuestra17(txtMuestra17.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra17()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra18.getValue()!=null && txtMuestra18.getValue().length()>0)
					{
						mapeo.setMuestra18(txtMuestra18.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra18()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra19.getValue()!=null && txtMuestra19.getValue().length()>0)
					{
						mapeo.setMuestra19(txtMuestra19.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra19()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					if (txtMuestra20.getValue()!=null && txtMuestra20.getValue().length()>0)
					{
						mapeo.setMuestra20(txtMuestra20.getValue().toString());
						rango = Math.pow(new Double(mapeo.getMuestra20()) - mapeo.getMedia(),2f);
						varianza = varianza + rango;
					}
					
					mapeo.setIdVolumen(idVolumenEf);
					mapeo.setIdCodigo(mapeoControlLineasVolumenEf.getIdCodigo());
					
					varianza = varianza / 10f;
					App.actualizarDatosLineaVolumenEf(mapeo,creando,varianza);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = false;
		String campoErroneo = null;
		
		if (txtMuestra1.getValue()!=null || txtMuestra1.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra2.getValue()!=null || txtMuestra2.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra3.getValue()!=null || txtMuestra3.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra4.getValue()!=null || txtMuestra4.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra5.getValue()!=null || txtMuestra5.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra6.getValue()!=null || txtMuestra6.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra7.getValue()!=null || txtMuestra7.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra8.getValue()!=null || txtMuestra8.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra9.getValue()!=null || txtMuestra9.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra10.getValue()!=null || txtMuestra10.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra11.getValue()!=null || txtMuestra11.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra12.getValue()!=null || txtMuestra12.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra13.getValue()!=null || txtMuestra13.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra14.getValue()!=null || txtMuestra14.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra15.getValue()!=null || txtMuestra15.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra16.getValue()!=null || txtMuestra16.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra17.getValue()!=null || txtMuestra17.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra18.getValue()!=null || txtMuestra18.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra19.getValue()!=null || txtMuestra19.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtMuestra20.getValue()!=null || txtMuestra20.getValue().length()>0)
		{
			devolver=true;
		}
		if (txtHora.getValue()==null || txtHora.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Hora";
		}
		if (txtArticulo.getValue()==null || txtArticulo.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Articulo";
		}
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.txtHora=new TextField("Hora");
				this.txtHora.setWidth("200px");
				this.txtHora.setStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setEnabled(false);
				this.txtArticulo.setStyleName(ValoTheme.TEXTFIELD_TINY);

				this.lblMedia=new Label();
				this.lblMedia.setCaption("Media VolumenEf");
				this.lblMedia.setWidth("200px");
				this.lblMedia.setEnabled(false);
				this.lblMedia.setStyleName(ValoTheme.LABEL_H4);
				
				fila1.addComponent(this.txtHora);
				fila1.addComponent(this.txtArticulo);
				fila1.addComponent(this.lblMedia);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				
				this.txtMuestra1=new TextField("Muestra1");
				this.txtMuestra1.setWidth("100px");
				this.txtMuestra1.setStyleName(ValoTheme.TEXTFIELD_TINY);				

				this.txtMuestra2=new TextField("Muestra2");
				this.txtMuestra2.setWidth("100px");
				this.txtMuestra2.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra3=new TextField("Muestra3");
				this.txtMuestra3.setWidth("100px");
				this.txtMuestra3.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra4=new TextField("Muestra4");
				this.txtMuestra4.setWidth("100px");
				this.txtMuestra4.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra5=new TextField("Muestra5");
				this.txtMuestra5.setWidth("100px");
				this.txtMuestra5.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				fila2.addComponent(txtMuestra1);				
				fila2.addComponent(txtMuestra2);				
				fila2.addComponent(txtMuestra3);				
				fila2.addComponent(txtMuestra4);				
				fila2.addComponent(txtMuestra5);				
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				
				this.txtMuestra6=new TextField("Muestra6");
				this.txtMuestra6.setWidth("100px");
				this.txtMuestra6.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra7=new TextField("Muestra7");
				this.txtMuestra7.setWidth("100px");
				this.txtMuestra7.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra8=new TextField("Muestra8");
				this.txtMuestra8.setWidth("100px");
				this.txtMuestra8.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra9=new TextField("Muestra9");
				this.txtMuestra9.setWidth("100px");
				this.txtMuestra9.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra10=new TextField("Muestra10");
				this.txtMuestra10.setWidth("100px");
				this.txtMuestra10.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				fila3.addComponent(txtMuestra6);				
				fila3.addComponent(txtMuestra7);				
				fila3.addComponent(txtMuestra8);				
				fila3.addComponent(txtMuestra9);				
				fila3.addComponent(txtMuestra10);				
				
			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				
				this.txtMuestra11=new TextField("Muestra11");
				this.txtMuestra11.setWidth("100px");
				this.txtMuestra11.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra12=new TextField("Muestra12");
				this.txtMuestra12.setWidth("100px");
				this.txtMuestra12.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra13=new TextField("Muestra13");
				this.txtMuestra13.setWidth("100px");
				this.txtMuestra13.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra14=new TextField("Muestra14");
				this.txtMuestra14.setWidth("100px");
				this.txtMuestra14.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra15=new TextField("Muestra15");
				this.txtMuestra15.setWidth("100px");
				this.txtMuestra15.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				fila4.addComponent(txtMuestra11);				
				fila4.addComponent(txtMuestra12);				
				fila4.addComponent(txtMuestra13);				
				fila4.addComponent(txtMuestra14);				
				fila4.addComponent(txtMuestra15);		
				
			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
			
				this.txtMuestra16=new TextField("Muestra16");
				this.txtMuestra16.setWidth("100px");
				this.txtMuestra16.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra17=new TextField("Muestra17");
				this.txtMuestra17.setWidth("100px");
				this.txtMuestra17.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra18=new TextField("Muestra18");
				this.txtMuestra18.setWidth("100px");
				this.txtMuestra18.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra19=new TextField("Muestra19");
				this.txtMuestra19.setWidth("100px");
				this.txtMuestra19.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra20=new TextField("Muestra20");
				this.txtMuestra20.setWidth("100px");
				this.txtMuestra20.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				fila5.addComponent(txtMuestra16);				
				fila5.addComponent(txtMuestra17);				
				fila5.addComponent(txtMuestra18);				
				fila5.addComponent(txtMuestra19);				
				fila5.addComponent(txtMuestra20);				
				

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
	}
	
	private void rellenarEntradasDatos()
	{
		consultaVolumenEfServer cadps = consultaVolumenEfServer.getInstance(CurrentUser.get());
		
		if (!creando)
		{
			MapeoControlLineasVolumenEf mapeo = cadps.datosOpcionesLineas(idLinea, this.linea);
			if (mapeo!=null)
			{
				this.txtMuestra1.setValue(mapeo.getMuestra1());
				this.txtMuestra2.setValue(mapeo.getMuestra2());
				this.txtMuestra3.setValue(mapeo.getMuestra3());
				this.txtMuestra4.setValue(mapeo.getMuestra4());
				this.txtMuestra5.setValue(mapeo.getMuestra5());
				this.txtMuestra6.setValue(mapeo.getMuestra6());
				this.txtMuestra7.setValue(mapeo.getMuestra7());
				this.txtMuestra8.setValue(mapeo.getMuestra8());
				this.txtMuestra9.setValue(mapeo.getMuestra9());
				this.txtMuestra10.setValue(mapeo.getMuestra10());
				this.txtMuestra11.setValue(mapeo.getMuestra11());
				this.txtMuestra12.setValue(mapeo.getMuestra12());
				this.txtMuestra13.setValue(mapeo.getMuestra13());
				this.txtMuestra14.setValue(mapeo.getMuestra14());
				this.txtMuestra15.setValue(mapeo.getMuestra15());
				this.txtMuestra16.setValue(mapeo.getMuestra16());
				this.txtMuestra17.setValue(mapeo.getMuestra17());
				this.txtMuestra18.setValue(mapeo.getMuestra18());
				this.txtMuestra19.setValue(mapeo.getMuestra19());
				this.txtMuestra20.setValue(mapeo.getMuestra20());
				this.txtHora.setValue(mapeo.getHora());
				this.lblMedia.setValue(mapeo.getMedia().toString());
				this.txtArticulo.setValue(mapeo.getArticulo());
			}
		}
		else
		this.txtArticulo.setValue(this.mapeoControlLineasVolumenEf.getArticulo());
	}

}