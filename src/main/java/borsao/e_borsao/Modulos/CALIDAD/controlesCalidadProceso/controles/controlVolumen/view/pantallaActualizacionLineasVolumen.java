package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.view;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.modelo.MapeoControlLineasVolumen;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumen.server.consultaVolumenServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaActualizacionLineasVolumen extends Window
{
	private MapeoControlLineasVolumen mapeoControlLineasVolumen = null;
	private pantallaVolumen App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtHora= null;
	private TextField txtArticulo= null;
	private TextField txtMuestra1= null;
	private TextField txtMuestra2= null;
	private TextField txtMuestra3= null;
	private TextField txtMuestra4= null;
	private TextField txtMuestra5= null;
	private Label lblMedia= null;

	private String linea = null;
	private Integer idVolumen= null;
	private Integer idLinea= null;
	private boolean creando = false;
	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasVolumen(Ventana r_App, MapeoControlLineasVolumen r_mapeo, String r_titulo, boolean r_creando)
	{
		this.creando=r_creando;
		this.App = (pantallaVolumen) r_App;
		this.linea=App.lineaProduccion;
		this.mapeoControlLineasVolumen = r_mapeo;
		this.idVolumen = r_mapeo.getIdVolumen();
		this.idLinea = r_mapeo.getIdCodigo();
		
		this.cargarPantalla();
		this.habilitarCampos();
		this.cargarListeners();
		this.rellenarEntradasDatos();
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
	}

	private void habilitarCampos()
	{
	}
	private void calcularMedia()
	{
		double muestras = 0;
		Double muestra1 = new Double(0);
		Double muestra2 = new Double(0);
		Double muestra3 = new Double(0);
		Double muestra4 = new Double(0);
		Double muestra5 = new Double(0);
		Double media = new Double(0);
		
		if (txtMuestra1.getValue()!=null && txtMuestra1.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra1.getValue()))!=0)
		{
			muestra1 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra1.getValue()));
			muestras++;
		}
		if (txtMuestra2.getValue()!=null && txtMuestra2.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra2.getValue()))!=0)
		{
			muestra2 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra2.getValue()));
			muestras++;
		}
		if (txtMuestra3.getValue()!=null && txtMuestra3.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra3.getValue()))!=0) 
		{
			muestra3 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra3.getValue()));
			muestras++;
		}
		if (txtMuestra4.getValue()!=null && txtMuestra4.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra4.getValue()))!=0)
		{
			muestra4 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra4.getValue()));
			muestras++;
		}
		if (txtMuestra5.getValue()!=null && txtMuestra5.getValue().length()>0 && new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra5.getValue()))!=0)
		{
			muestra5 = new Double(RutinasCadenas.reemplazarComaMiles(txtMuestra5.getValue()));
			muestras++;
		}
		
		media = (muestra1 + muestra2 + muestra3 + muestra4 + muestra5)/muestras;
		
		this.lblMedia.setValue(media.toString());
	}
	
	private void cargarListeners()
	{
		txtMuestra1.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularMedia();
			}
		});
		txtMuestra2.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularMedia();
			}
		});
		txtMuestra3.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularMedia();
			}
		});
		txtMuestra4.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularMedia();
			}
		});
		txtMuestra5.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularMedia();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					MapeoControlLineasVolumen mapeo = new MapeoControlLineasVolumen();
					
					mapeo.setHora(txtHora.getValue());
					mapeo.setArticulo(txtArticulo.getValue());
					if (txtMuestra1.getValue()!=null) mapeo.setMuestra1(txtMuestra1.getValue().toString());
					if (txtMuestra2.getValue()!=null) mapeo.setMuestra2(txtMuestra2.getValue().toString());
					if (txtMuestra3.getValue()!=null) mapeo.setMuestra3(txtMuestra3.getValue().toString());
					if (txtMuestra4.getValue()!=null) mapeo.setMuestra4(txtMuestra4.getValue().toString());
					if (txtMuestra5.getValue()!=null) mapeo.setMuestra5(txtMuestra5.getValue().toString());
					mapeo.setMedia(new Double(lblMedia.getValue().toString()));
					mapeo.setIdVolumen(idVolumen);
					mapeo.setIdCodigo(mapeoControlLineasVolumen.getIdCodigo());
					
					App.actualizarDatosLineaVolumen(mapeo,creando);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		String campoErroneo = null;
		
		if (txtMuestra1.getValue()==null || txtMuestra1.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Muestra 1";
		}
		if (txtMuestra2.getValue()==null || txtMuestra2.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Muestra 2";
		}
		if (txtMuestra3.getValue()==null || txtMuestra3.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Muestra 3";
		}
		if (txtMuestra4.getValue()==null || txtMuestra3.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Muestra 4";
		}
		if (txtMuestra5.getValue()==null || txtMuestra5.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Muestra 5";
		}
		if (txtHora.getValue()==null || txtHora.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Hora";
		}
		if (txtArticulo.getValue()==null || txtArticulo.getValue().length()==0)
		{
			devolver=false;
			campoErroneo="Articulo";
		}
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.txtHora=new TextField("Hora");
				this.txtHora.setWidth("200px");
				this.txtHora.setStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setEnabled(false);
				this.txtArticulo.setStyleName(ValoTheme.TEXTFIELD_TINY);

				this.lblMedia=new Label();
				this.lblMedia.setCaption("Media Volumen");
				this.lblMedia.setWidth("200px");
				this.lblMedia.setEnabled(false);
				this.lblMedia.setStyleName(ValoTheme.LABEL_H4);
				
				fila1.addComponent(this.txtHora);
				fila1.addComponent(this.txtArticulo);
				fila1.addComponent(this.lblMedia);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				
				this.txtMuestra1=new TextField("Muestra1");
				this.txtMuestra1.setWidth("100px");
				this.txtMuestra1.setStyleName(ValoTheme.TEXTFIELD_TINY);				

				this.txtMuestra2=new TextField("Muestra2");
				this.txtMuestra2.setWidth("100px");
				this.txtMuestra2.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra3=new TextField("Muestra3");
				this.txtMuestra3.setWidth("100px");
				this.txtMuestra3.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra4=new TextField("Muestra4");
				this.txtMuestra4.setWidth("100px");
				this.txtMuestra4.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				this.txtMuestra5=new TextField("Muestra5");
				this.txtMuestra5.setWidth("100px");
				this.txtMuestra5.setStyleName(ValoTheme.TEXTFIELD_TINY);				
				
				fila2.addComponent(txtMuestra1);				
				fila2.addComponent(txtMuestra2);				
				fila2.addComponent(txtMuestra3);				
				fila2.addComponent(txtMuestra4);				
				fila2.addComponent(txtMuestra5);				
				

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
	}
	
	private void rellenarEntradasDatos()
	{
		consultaVolumenServer cadps = consultaVolumenServer.getInstance(CurrentUser.get());
		
		if (!creando)
		{
			MapeoControlLineasVolumen mapeo = cadps.datosOpcionesLineas(idLinea, this.linea);
			if (mapeo!=null)
			{
				this.txtMuestra1.setValue(mapeo.getMuestra1());
				this.txtMuestra2.setValue(mapeo.getMuestra2());
				this.txtMuestra3.setValue(mapeo.getMuestra3());
				this.txtMuestra4.setValue(mapeo.getMuestra4());
				this.txtMuestra5.setValue(mapeo.getMuestra5());
				this.txtHora.setValue(mapeo.getHora());
				this.lblMedia.setValue(mapeo.getMedia().toString());
				this.txtArticulo.setValue(mapeo.getArticulo());
			}
		}
		else
		this.txtArticulo.setValue(this.mapeoControlLineasVolumen.getArticulo());
	}

}