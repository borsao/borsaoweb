package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlVolumenEf.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlLineasVolumenEf extends MapeoGlobal
{
	private Integer idVolumen;
	private Double media;

	private String acc; // para poner el boton
	
	private String hora;
	private String articulo;
	private String muestra1;
	private String muestra2;
	private String muestra3;
	private String muestra4;
	private String muestra5;
	private String muestra6;
	private String muestra7;
	private String muestra8;
	private String muestra9;
	private String muestra10;
	private String muestra11;
	private String muestra12;
	private String muestra13;
	private String muestra14;
	private String muestra15;
	private String muestra16;
	private String muestra17;
	private String muestra18;
	private String muestra19;
	private String muestra20;

	public MapeoControlLineasVolumenEf()
	{
	}

	public Integer getIdVolumen() {
		return idVolumen;
	}

	public void setIdVolumen(Integer idVolumen) {
		this.idVolumen = idVolumen;
	}

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getMuestra1() {
		return muestra1;
	}

	public void setMuestra1(String muestra1) {
		this.muestra1 = muestra1;
	}

	public String getMuestra2() {
		return muestra2;
	}

	public void setMuestra2(String muestra2) {
		this.muestra2 = muestra2;
	}

	public String getMuestra3() {
		return muestra3;
	}

	public void setMuestra3(String muestra3) {
		this.muestra3 = muestra3;
	}

	public String getMuestra4() {
		return muestra4;
	}

	public void setMuestra4(String muestra4) {
		this.muestra4 = muestra4;
	}

	public String getMuestra5() {
		return muestra5;
	}

	public void setMuestra5(String muestra5) {
		this.muestra5 = muestra5;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Double getMedia() {
		return media;
	}

	public void setMedia(Double media) {
		this.media = media;
	}

	public String getMuestra6() {
		return muestra6;
	}

	public void setMuestra6(String muestra6) {
		this.muestra6 = muestra6;
	}

	public String getMuestra7() {
		return muestra7;
	}

	public void setMuestra7(String muestra7) {
		this.muestra7 = muestra7;
	}

	public String getMuestra8() {
		return muestra8;
	}

	public void setMuestra8(String muestra8) {
		this.muestra8 = muestra8;
	}

	public String getMuestra9() {
		return muestra9;
	}

	public void setMuestra9(String muestra9) {
		this.muestra9 = muestra9;
	}

	public String getMuestra10() {
		return muestra10;
	}

	public void setMuestra10(String muestra10) {
		this.muestra10 = muestra10;
	}

	public String getMuestra11() {
		return muestra11;
	}

	public void setMuestra11(String muestra11) {
		this.muestra11 = muestra11;
	}

	public String getMuestra12() {
		return muestra12;
	}

	public void setMuestra12(String muestra12) {
		this.muestra12 = muestra12;
	}

	public String getMuestra13() {
		return muestra13;
	}

	public void setMuestra13(String muestra13) {
		this.muestra13 = muestra13;
	}

	public String getMuestra14() {
		return muestra14;
	}

	public void setMuestra14(String muestra14) {
		this.muestra14 = muestra14;
	}

	public String getMuestra15() {
		return muestra15;
	}

	public void setMuestra15(String muestra15) {
		this.muestra15 = muestra15;
	}

	public String getMuestra16() {
		return muestra16;
	}

	public void setMuestra16(String muestra16) {
		this.muestra16 = muestra16;
	}

	public String getMuestra17() {
		return muestra17;
	}

	public void setMuestra17(String muestra17) {
		this.muestra17 = muestra17;
	}

	public String getMuestra18() {
		return muestra18;
	}

	public void setMuestra18(String muestra18) {
		this.muestra18 = muestra18;
	}

	public String getMuestra19() {
		return muestra19;
	}

	public void setMuestra19(String muestra19) {
		this.muestra19 = muestra19;
	}

	public String getMuestra20() {
		return muestra20;
	}

	public void setMuestra20(String muestra20) {
		this.muestra20 = muestra20;
	}

}