package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.view;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.modelo.MapeoControlLineasPaletizar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlPaletizar.server.consultaPaletizarServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaActualizacionLineasPaletizar extends Window
{
	private MapeoControlLineasPaletizar mapeoControlLineasPaletizar = null;
	private pantallaPaletizar App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Button btnBotonCopiar = null;
	private Button btnBotonPegar = null;
	
	
	private Combo cmbCaja1= null;
	private Combo cmbCaja2= null;
	private Combo cmbCaja3= null;
	private Combo cmbCaja4= null;
	private Combo cmbCaja5= null;
	private Combo cmbCaja6= null;

	private Combo cmbCaja7= null;
	private Combo cmbCaja8= null;
	private Combo cmbCaja9= null;
	private Combo cmbCaja10= null;
	private Combo cmbCaja11= null;
	
	private String linea = null;
	private String caja = null;
	private Integer idProgramacion = null;
	/*
	 * Valores devueltos
	 */
	
	public pantallaActualizacionLineasPaletizar(Ventana r_App, Integer r_idProgramacion, String r_caja, String r_articulo, String r_titulo)
	{
		this.App = (pantallaPaletizar) r_App;
		this.linea=App.lineaProduccion;
		this.caja = r_caja;
		this.idProgramacion = r_idProgramacion;
		
		this.cargarPantalla();
		this.cargarCombo();
		this.habilitarCampos();
		this.cargarListeners();
		
		this.rellenarEntradasDatos();
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
	}

	private void rellenarEntradasDatos()
	{
		consultaPaletizarServer cadps = consultaPaletizarServer.getInstance(CurrentUser.get());
		HashMap<String, String> hash = cadps.recuperarDatosCaja(this.idProgramacion,this.caja,this.linea);
		
		/*
		 * con el hash relleno las entradas de datos
		 */
		
		if (hash!=null && !hash.isEmpty())
		{
			this.cmbCaja1.setValue(hash.get("01"));
			this.cmbCaja2.setValue(hash.get("02"));
			this.cmbCaja3.setValue(hash.get("03"));
			this.cmbCaja4.setValue(hash.get("04"));
			this.cmbCaja5.setValue(hash.get("05"));
			this.cmbCaja6.setValue(hash.get("06"));
			this.cmbCaja7.setValue(hash.get("07"));
			this.cmbCaja8.setValue(hash.get("08"));
			this.cmbCaja9.setValue(hash.get("09"));
			this.cmbCaja10.setValue(hash.get("10"));
			this.cmbCaja11.setValue(hash.get("11"));
		}
	}
	private void habilitarCampos()
	{
	}
	
	private void cargarListeners()
	{

		btnBotonCopiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				accionCopiar();
			}
		});

		btnBotonPegar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				accionPegar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
						HashMap<String, String> valores = new HashMap<String, String>();
						valores.put("campo", caja);
						if (cmbCaja1.getValue()!=null) valores.put("01", cmbCaja1.getValue().toString()); else valores.put("01","");
						if (cmbCaja2.getValue()!=null) valores.put("02", cmbCaja2.getValue().toString()); else valores.put("02","");
						if (cmbCaja3.getValue()!=null) valores.put("03", cmbCaja3.getValue().toString()); else valores.put("03","");
						if (cmbCaja4.getValue()!=null) valores.put("04", cmbCaja4.getValue().toString()); else valores.put("04","");
						if (cmbCaja5.getValue()!=null) valores.put("05", cmbCaja5.getValue().toString()); else valores.put("05","");
						if (cmbCaja6.getValue()!=null) valores.put("06", cmbCaja6.getValue().toString()); else valores.put("06","");
						if (cmbCaja7.getValue()!=null) valores.put("07", cmbCaja7.getValue().toString()); else valores.put("07","");
						if (cmbCaja8.getValue()!=null) valores.put("08", cmbCaja8.getValue().toString()); else valores.put("08","");
						if (cmbCaja9.getValue()!=null) valores.put("09", cmbCaja9.getValue().toString()); else valores.put("09","");
						if (cmbCaja10.getValue()!=null) valores.put("10", cmbCaja10.getValue().toString()); else valores.put("10","");
						if (cmbCaja11.getValue()!=null) valores.put("11", cmbCaja11.getValue().toString()); else valores.put("11","");
						
					App.actualizarDatosLineaPaletizar(idProgramacion, valores);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		String campoErroneo = null;
		
//		if (txtCantidad.getValue()==null || txtCantidad.getValue().length()==0)
//		{
//			devolver=false;
//			campoErroneo="Cantidad";
//		}
//		if (txtProveedor.getValue()==null || txtProveedor.getValue().length()==0)
//		{
//			devolver=false;
//			campoErroneo="Proveedor";
//		}
//		if (txtLote.getValue()==null || txtLote.getValue().length()==0)
//		{
//			devolver=false;
//			campoErroneo="Lote";
//		}
//		if (txtDescripcion.getValue()==null || txtDescripcion.getValue().length()==0)
//		{
//			devolver=false;
//			campoErroneo="Descripcion";
//		}
//		if (txtFechaFabricacion.getValue()==null || txtFechaFabricacion.getValue().toString().length()==0)
//		{
//			devolver=false;
//			campoErroneo="Fecha Fabricación";
//		}
		
		if (!devolver)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor de " + campoErroneo + " correctamente.");
		}
		return devolver;
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbCaja1=new Combo("Botella");
				this.propiedadesCombos(this.cmbCaja1);
				
				this.cmbCaja2=new Combo("Etiqueta");
				this.propiedadesCombos(this.cmbCaja2);

				this.cmbCaja3=new Combo("Contra");
				this.propiedadesCombos(this.cmbCaja3);
				
				fila1.addComponent(cmbCaja1);
				fila1.addComponent(cmbCaja2);
				fila1.addComponent(cmbCaja3);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.cmbCaja4=new Combo("Tirilla");
				this.propiedadesCombos(this.cmbCaja4);
				
				this.cmbCaja5=new Combo("Medalla");
				this.propiedadesCombos(this.cmbCaja5);

				this.cmbCaja6=new Combo("Capsula");
				this.propiedadesCombos(this.cmbCaja6);

				fila2.addComponent(cmbCaja4);
				fila2.addComponent(cmbCaja5);
				fila2.addComponent(cmbCaja6);
				
			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				
				this.cmbCaja7=new Combo("Rosca");
				this.propiedadesCombos(this.cmbCaja7);

				this.cmbCaja8=new Combo("Cerrado");
				this.propiedadesCombos(this.cmbCaja8);
				
				this.cmbCaja9=new Combo("Lote");
				this.propiedadesCombos(this.cmbCaja9);

				fila3.addComponent(cmbCaja7);
				fila3.addComponent(cmbCaja8);
				fila3.addComponent(cmbCaja9);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				
				this.cmbCaja10=new Combo("Impresion Caja");
				this.propiedadesCombos(this.cmbCaja10);

				this.cmbCaja11=new Combo("SSCC");
				this.propiedadesCombos(this.cmbCaja11);

				fila4.addComponent(cmbCaja10);
				fila4.addComponent(cmbCaja11);
			
			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
				
				btnBotonCopiar = new Button("Copiar");
				btnBotonCopiar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnBotonCopiar.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonPegar= new Button("Pegar");
				btnBotonPegar.setEnabled(App.hashCopia!=null && !App.hashCopia.isEmpty());
				btnBotonPegar.addStyleName(ValoTheme.BUTTON_DANGER);
				btnBotonPegar.addStyleName(ValoTheme.BUTTON_TINY);
				
				fila5.addComponent(btnBotonCopiar);
				fila5.addComponent(btnBotonPegar);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.setExpandRatio(controles, 1);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
	}
	private void cargarCombo()
	{
		this.cargarValoresEstados(cmbCaja1);
		this.cargarValoresEstados(cmbCaja2);
		this.cargarValoresEstados(cmbCaja3);
		this.cargarValoresEstados(cmbCaja4);
		this.cargarValoresEstados(cmbCaja5);
		this.cargarValoresEstados(cmbCaja6);
		this.cargarValoresEstados(cmbCaja7);
		this.cargarValoresEstados(cmbCaja8);
		this.cargarValoresEstados(cmbCaja9);
		this.cargarValoresEstados(cmbCaja10);
		this.cargarValoresEstados(cmbCaja11);
	}
	
	private void cargarValoresEstados(Combo r_combo)
    {
    	r_combo.addItem("CORRECTO");
    	r_combo.addItem("FALLO");
    	r_combo.addItem("INCORRECTO");
    	r_combo.addItem("REALIZADO REFERENCIA ANTERIOR");
    	r_combo.addItem("SIN PRODUCCION");
    	r_combo.addItem("");
    }

	private void propiedadesCombos(Combo r_combo)
	{
		r_combo.setWidth("200px");
		r_combo.setNewItemsAllowed(false);
		r_combo.setNullSelectionAllowed(true);
	}
	
	private void accionCopiar()
	{
		HashMap<String, String> valores = new HashMap<String, String>();
		valores.put("campo", caja);
		if (cmbCaja1.getValue()!=null) valores.put("01", cmbCaja1.getValue().toString()); else valores.put("01","");
		if (cmbCaja2.getValue()!=null) valores.put("02", cmbCaja2.getValue().toString()); else valores.put("02","");
		if (cmbCaja3.getValue()!=null) valores.put("03", cmbCaja3.getValue().toString()); else valores.put("03","");
		if (cmbCaja4.getValue()!=null) valores.put("04", cmbCaja4.getValue().toString()); else valores.put("04","");
		if (cmbCaja5.getValue()!=null) valores.put("05", cmbCaja5.getValue().toString()); else valores.put("05","");
		if (cmbCaja6.getValue()!=null) valores.put("06", cmbCaja6.getValue().toString()); else valores.put("06","");
		if (cmbCaja7.getValue()!=null) valores.put("07", cmbCaja7.getValue().toString()); else valores.put("07","");
		if (cmbCaja8.getValue()!=null) valores.put("08", cmbCaja8.getValue().toString()); else valores.put("08","");
		if (cmbCaja9.getValue()!=null) valores.put("09", cmbCaja9.getValue().toString()); else valores.put("09","");
		if (cmbCaja10.getValue()!=null) valores.put("10", cmbCaja10.getValue().toString()); else valores.put("10","");
		if (cmbCaja11.getValue()!=null) valores.put("11", cmbCaja11.getValue().toString()); else valores.put("11","");

		App.hashCopia = valores;
		if (valores!=null && !valores.isEmpty())
		{
			btnBotonPegar.setEnabled(true);
		}
	}

	private void accionPegar()
	{
		HashMap<String, String> valores = new HashMap<String, String>();
		valores = App.hashCopia;
		
		if (App.hashCopia!=null && !App.hashCopia.isEmpty())
		{
			cmbCaja1.setValue(valores.get("01"));
			cmbCaja2.setValue(valores.get("02"));
			cmbCaja3.setValue(valores.get("03"));
			cmbCaja4.setValue(valores.get("04"));
			cmbCaja5.setValue(valores.get("05"));
			cmbCaja6.setValue(valores.get("06"));
			cmbCaja7.setValue(valores.get("07"));
			cmbCaja8.setValue(valores.get("08"));
			cmbCaja9.setValue(valores.get("09"));
			cmbCaja10.setValue(valores.get("10"));
			cmbCaja11.setValue(valores.get("11"));
		}
		
		App.hashCopia = valores;
	}
}