package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlProductoElaborar extends MapeoGlobal
{
	private String linea;
	private Integer idProgramacion;
	private String hora;
	private String articulo;
	private String descripcion;
	private String lote;
	private String tipoOperacion;
	private String realizado;
	private String verificado;
	private String observaciones;
	
	private String envase;
	private String marca;
	private String loteMuestra;
	private String realizadoMuestra;
	
	private Date fecha;
	
	public MapeoControlProductoElaborar()
	{
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getVerificado() {
		return verificado;
	}

	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getEnvase() {
		return envase;
	}

	public void setEnvase(String envase) {
		this.envase = envase;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getLoteMuestra() {
		return loteMuestra;
	}

	public void setLoteMuestra(String loteMuestra) {
		this.loteMuestra = loteMuestra;
	}

	public String getRealizadoMuestra() {
		return realizadoMuestra;
	}

	public void setRealizadoMuestra(String realizadoMuestra) {
		this.realizadoMuestra = realizadoMuestra;
	}

}