package borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlImpulsos.modelo.MapeoControlImpulsos;

public class consultaImpulsosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaImpulsosServer  instance;
	
	public consultaImpulsosServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaImpulsosServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaImpulsosServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlImpulsos datosOpcionesGlobal(MapeoControlImpulsos r_mapeo, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		MapeoControlImpulsos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		tabla = "qc_impulsos_bib";
		campo= "idPrdProgramacion";
		cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_impulsos.idProgramacion ");

		cadenaSQL.append(" SELECT qc_impulsos.idCodigo imp_id, ");
		cadenaSQL.append(" qc_impulsos.hora imp_hora, ");
		cadenaSQL.append(" qc_impulsos.vacio imp_vac, ");
		cadenaSQL.append(" qc_impulsos.carbonico imp_car, ");
		cadenaSQL.append(" qc_impulsos.purga imp_pur, ");
		cadenaSQL.append(" qc_impulsos.tempLlenado imp_tll, ");
		cadenaSQL.append(" qc_impulsos.impLlenado imp_ill, ");
		cadenaSQL.append(" qc_impulsos.cono imp_con, ");
		cadenaSQL.append(" qc_impulsos.realizado imp_rea, ");
		cadenaSQL.append(" qc_impulsos.verificado imp_ver, ");
		cadenaSQL.append(" qc_impulsos.observaciones imp_obs, ");
		cadenaSQL.append(" qc_impulsos.fecha imp_fec ");
		
		cadenaSQL.append(" FROM " + tabla + " qc_impulsos ");
			
		try
		{
			if (r_mapeo!=null)
			{
				if (r_ejercicio!=null && r_ejercicio>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
				}
				if (r_semana!=null && r_semana.length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
				}
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_prog." + campo + " = " + r_mapeo.getIdProgramacion());
				}
				if (r_mapeo.getFecha()!=null)
				{
					cadenaWhere.append(" where qc_impulsos.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlImpulsos();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("imp_id"));
				mapeo.setHora(rsOpcion.getString("imp_hora"));
				
				mapeo.setVacio(rsOpcion.getDouble("imp_vac"));
				mapeo.setCarbonico(rsOpcion.getDouble("imp_car"));
				mapeo.setPurga(rsOpcion.getDouble("imp_pur"));
				mapeo.setTempLlenado(rsOpcion.getDouble("imp_tll"));
				mapeo.setImpLlenado(rsOpcion.getDouble("imp_ill"));
				mapeo.setCono(rsOpcion.getDouble("imp_con"));
				
				mapeo.setRealizado(rsOpcion.getString("imp_rea"));
				mapeo.setVerificado(rsOpcion.getString("imp_ver"));
				mapeo.setObservaciones(rsOpcion.getString("imp_obs"));
				mapeo.setFecha(rsOpcion.getDate("imp_fec"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlImpulsos> datosOpcionesGlobal(Integer r_idProgramacion, String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlImpulsos> vector = null;
		MapeoControlImpulsos mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;

		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		tabla = "qc_impulsos_bib";
		campo= "idPrdProgramacion";
		cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_impulsos.idProgramacion ");

		cadenaSQL.append(" SELECT qc_impulsos.idCodigo imp_id, ");
		cadenaSQL.append(" qc_impulsos.hora imp_hora, ");
		cadenaSQL.append(" qc_impulsos.vacio imp_vac, ");
		cadenaSQL.append(" qc_impulsos.carbonico imp_car, ");
		cadenaSQL.append(" qc_impulsos.purga imp_pur, ");
		cadenaSQL.append(" qc_impulsos.tempLlenado imp_tll, ");
		cadenaSQL.append(" qc_impulsos.impLlenado imp_ill, ");
		cadenaSQL.append(" qc_impulsos.cono imp_con, ");
		cadenaSQL.append(" qc_impulsos.realizado imp_rea, ");
		cadenaSQL.append(" qc_impulsos.verificado imp_ver, ");
		cadenaSQL.append(" qc_impulsos.observaciones imp_obs, ");
		cadenaSQL.append(" qc_impulsos.fecha imp_fec ");
		
		cadenaSQL.append(" FROM " + tabla + " qc_impulsos ");
		
		try
		{
			if (r_ejercicio!=null && r_ejercicio>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_prog.ejercicio = " + r_ejercicio);
			}
			if (r_semana!=null && r_semana.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_prog.semana = '" + r_semana + "' ");
			}

			if (r_idProgramacion!=null && r_idProgramacion>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_prog."+ campo + " = " + r_idProgramacion );
			}
		
			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoControlImpulsos>();
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlImpulsos();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("imp_id"));
				mapeo.setHora(rsOpcion.getString("imp_hora"));
				
				mapeo.setVacio(rsOpcion.getDouble("imp_vac"));
				mapeo.setCarbonico(rsOpcion.getDouble("imp_car"));
				mapeo.setPurga(rsOpcion.getDouble("imp_pur"));
				mapeo.setTempLlenado(rsOpcion.getDouble("imp_tll"));
				mapeo.setImpLlenado(rsOpcion.getDouble("imp_ill"));
				mapeo.setCono(rsOpcion.getDouble("imp_con"));
				
				mapeo.setRealizado(rsOpcion.getString("imp_rea"));
				mapeo.setVerificado(rsOpcion.getString("imp_ver"));
				mapeo.setObservaciones(rsOpcion.getString("imp_obs"));
				mapeo.setFecha(rsOpcion.getDate("imp_fec"));
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	
	public String guardarNuevo(MapeoControlImpulsos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		

		try
		{
			tabla = "qc_impulsos_bib";
			
			cadenaSQL.append(" INSERT INTO " + tabla + "( ");
			
			cadenaSQL.append(tabla + ".idCodigo , ");
			cadenaSQL.append(tabla + ".hora , ");
			cadenaSQL.append(tabla + ".vacio , ");
			cadenaSQL.append(tabla + ".carbonico , ");
			cadenaSQL.append(tabla + ".purga , ");
			cadenaSQL.append(tabla + ".tempLlenado, ");
			cadenaSQL.append(tabla + ".impLlenado, ");
			cadenaSQL.append(tabla + ".cono , ");
			cadenaSQL.append(tabla + ".realizado , ");
			cadenaSQL.append(tabla + ".observaciones , ");
			cadenaSQL.append(tabla + ".idProgramacion, ");
			cadenaSQL.append(tabla + ".fecha , ");
			cadenaSQL.append(tabla + ".verificado ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla, "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getVacio()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getVacio());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getCarbonico()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getCarbonico());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getPurga()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getPurga());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getTempLlenado()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getTempLlenado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getImpLlenado()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getImpLlenado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getCono()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getCono());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(12, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(12, null);
	    	}
	    	if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoControlImpulsos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		
		try
		{
			
			tabla = "qc_impulsos_bib";
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".hora =?, ");
			cadenaSQL.append(tabla + ".vacio =?, ");
			cadenaSQL.append(tabla + ".carbonico =?, ");
			cadenaSQL.append(tabla + ".purga =?, ");
			cadenaSQL.append(tabla + ".tempLlenado=?, ");
			cadenaSQL.append(tabla + ".impLlenado=?, ");
			cadenaSQL.append(tabla + ".cono=?, ");
			cadenaSQL.append(tabla + ".realizado =?, ");
			cadenaSQL.append(tabla + ".observaciones =?, ");
			cadenaSQL.append(tabla + ".idProgramacion =?, " );
			cadenaSQL.append(tabla + ".fecha =? " );
			
	        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getVacio()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getVacio());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    if (r_mapeo.getCarbonico()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getCarbonico());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getPurga()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getPurga());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getTempLlenado()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getTempLlenado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getImpLlenado()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getImpLlenado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getCono()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getCono());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(11, null);
	    	}
	    	
		    preparedStatement.setInt(12, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoControlImpulsos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String tabla = null; 
		tabla = "qc_impulsos_bib";

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarVerificacion(String r_verificado, String r_linea, Integer r_ejercicio, String r_semana, String r_fecha)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		tabla = "qc_impulsos_bib";
		campo= "idPrdProgramacion";
		cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_impulsos.idProgramacion ");

		try
		{
			
			ResultSet rsOpcion = null;		
			
			cadenaSQL.append(" SELECT qc_impulsos.idCodigo imp_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_impulsos ");

	     	cadenaSQL.append(" where prd_prog.ejercicio = " + r_ejercicio );
	     	cadenaSQL.append(" and prd_prog.semana = " + r_semana);
	     	cadenaSQL.append(" and qc_impulsos.linea = " + r_linea);
	     	cadenaSQL.append(" and qc_impulsos.fecha = '" + RutinasFechas.convertirAFechaMysql(r_fecha) + "' ");
	     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE " + tabla + " set ");
				cadenaSQL.append(tabla + ".verificado =? ");
		        cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_verificado!=null)
			    {
			    	preparedStatement.setString(1, r_verificado);
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pre_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comrpboarControl(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		String tabla = null;
		StringBuffer cadenaSQL = null; 
		
		cadenaSQL = new StringBuffer();
		tabla = "qc_impulsos_bib";
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			cadenaSQL.append(" where qc_preop.idProgramacion = " + r_idProgramacion);
			
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}
	
	public boolean comrpboarVerificado(Integer r_idProgramacion, String r_linea)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;  
		StringBuffer cadenaWhere = null;  
		String tabla = null;
		String campo = null;
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		if (r_linea.contentEquals("Envasadora"))
		{
			tabla = "qc_impulsos_env";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("Embotelladora"))
		{
			tabla = "qc_impulsos_emb";
			campo= "idprd_programacion";
			cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idprd_programacion = qc_preop.idProgramacion ");
		}
		else if (r_linea.contentEquals("BIB"))
		{
			tabla = "qc_impulsos_bib";
			campo= "idPrdProgramacion";
			cadenaWhere.append(" inner join prd_programacion_envasadora prd_prog on prd_prog.idPrdProgramacion = qc_preop.idProgramacion ");
		}
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT qc_preop.idCodigo pre_id ");
			cadenaSQL.append(" FROM " + tabla + " qc_preop ");
			
			
			cadenaWhere.append(" where prd_prog." + campo + " = " + r_idProgramacion);
			cadenaWhere.append(" and length(qc_preop.verificado) <> 0 ");

			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
			try
			{
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					return true;
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return false;
	}

}