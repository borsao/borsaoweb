package borsao.e_borsao.Modulos.CALIDAD.CostePersonal.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.modelo.MapeoCostePersonal;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoDesgloseValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaCostePersonalServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCostePersonalServer instance;
	
	public consultaCostePersonalServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCostePersonalServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCostePersonalServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoCostePersonal> datosOpcionesGlobal(MapeoCostePersonal r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoCostePersonal> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_costePersonal_ej.ejercicio qcc_eje, ");
		cadenaSQL.append(" qc_costePersonal_ej.categoria qcc_cat, ");
        cadenaSQL.append(" qc_costePersonal_ej.valor qcc_val ");
     	cadenaSQL.append(" FROM qc_costePersonal_ej ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getCategoria()!=null && r_mapeo.getCategoria().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" categoria = '" + r_mapeo.getCategoria() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor= " + r_mapeo.getValor() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, categoria asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoCostePersonal>();
			
			while(rsOpcion.next())
			{
				MapeoCostePersonal mapeo = new MapeoCostePersonal();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setEjercicio(rsOpcion.getString("qcc_eje"));
				mapeo.setCategoria(rsOpcion.getString("qcc_cat"));
				mapeo.setValor(rsOpcion.getDouble("qcc_val"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Double recuperarCosteHoraGlobalArea(Integer r_ejercicio, String r_area, boolean r_esEtt)
	{
		Double valor_obtenido = new Double(0);
		
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		Double adicional = new Double(1);
		String sql = null;
		
		try
		{
			if (r_esEtt)
			{
				consultaParametrosCalidadServer cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
				String costeAdicional = cpcs.recuperarParametro(r_ejercicio, "SobreCosteETT");
				if (costeAdicional!=null && costeAdicional.length()>0) adicional = new Double(costeAdicional.replace(",", "."));
			}
			
				  sql = " select sum(qc_costePersonal_ej.valor*qc_distribucionPersonal_ej.valor*" + adicional + ") coste from qc_costePersonal_ej ";
			sql = sql + " inner join qc_distribucionPersonal_ej on qc_distribucionPersonal_ej.ejercicio = qc_costePersonal_ej.ejercicio ";
			sql = sql + " and qc_costePersonal_ej.categoria = qc_distribucionPersonal_ej.categoria " ;
			sql = sql + " where qc_distribucionPersonal_ej.ejercicio = " + r_ejercicio + " and qc_distribucionPersonal_ej.area= '" + r_area + "' and ett='1' ";
			sql = sql + " union all ";
			sql = sql + " select sum(qc_costePersonal_ej.valor*qc_distribucionPersonal_ej.valor*" + adicional + ") coste from qc_costePersonal_ej ";
			sql = sql + " inner join qc_distribucionPersonal_ej on qc_distribucionPersonal_ej.ejercicio = qc_costePersonal_ej.ejercicio ";
			sql = sql + " and qc_costePersonal_ej.categoria = qc_distribucionPersonal_ej.categoria " ;
			sql = sql + " where qc_distribucionPersonal_ej.ejercicio = " + r_ejercicio + " and qc_distribucionPersonal_ej.area= '" + r_area + "' and ett='0' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido =valor_obtenido+ rsValor.getDouble("coste");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return valor_obtenido;
	}
	
	public Double recuperarCosteDirectoHoraArea(Integer r_ejercicio, String r_area)
	{
		Double valor_obtenido = new Double(0);
		
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		Double adicional = new Double(1);
		String sql = null;
		
		try
		{
				  sql = " select valor as coste from qc_costeMaquina_ej ";
			sql = sql + " where qc_costeMaquina_ej.ejercicio = " + r_ejercicio + " and qc_costeMaquina_ej.area= '" + r_area + "' ";
			sql = sql + " and qc_costeMaquina_ej.concepto = 'Directo/Hora'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido =valor_obtenido+ rsValor.getDouble("coste");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return valor_obtenido;
	}

	public Double recuperarCosteIndirectoHoraArea(Integer r_ejercicio, String r_area)
	{
		Double valor_obtenido = new Double(0);
		
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		Double adicional = new Double(1);
		String sql = null;
		
		try
		{
			sql = " select sum(valor) as coste from qc_costeMaquina_ej ";
			sql = sql + " where qc_costeMaquina_ej.ejercicio = " + r_ejercicio + " and qc_costeMaquina_ej.area= '" + r_area + "' ";
			sql = sql + " and qc_costeMaquina_ej.concepto <> 'Directo/Hora'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido =valor_obtenido+ rsValor.getDouble("coste");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return valor_obtenido;
	}
	
	public String generarCosteHoraArea(Integer r_incidencia, Integer r_ejercicio, String r_area, boolean r_esEtt, Double r_tiempo)
	{
		MapeoDesgloseValoracionNoConformidades mapeoDesglose = null;
		consultaNoConformidadesServer cncs = null;
		
		String rdo = null;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		
		Double adicional = new Double(1);
		
		try
		{
			if (r_esEtt)
			{
				consultaParametrosCalidadServer cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
				String costeAdicional = cpcs.recuperarParametro(r_ejercicio, "SobreCosteETT");
				if (costeAdicional!=null && costeAdicional.length()>0) adicional = new Double(costeAdicional.replace(",", "."));
			}
			cncs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
			
				  sql = " select qc_costePersonal_ej.categoria cs_cat, qc_costePersonal_ej.valor*" + adicional + " cs_cos, qc_distribucionPersonal_ej.valor cs_cuan from qc_costePersonal_ej ";
			sql = sql + " inner join qc_distribucionPersonal_ej on qc_distribucionPersonal_ej.ejercicio = qc_costePersonal_ej.ejercicio ";
			sql = sql + " and qc_costePersonal_ej.categoria = qc_distribucionPersonal_ej.categoria " ;
			sql = sql + " where qc_distribucionPersonal_ej.ejercicio = " + r_ejercicio + " and qc_distribucionPersonal_ej.area= '" + r_area + "' ";
			if (r_esEtt)
			{
				sql = sql + " and ett='1' ";
			}
			else 
			{
				sql = sql + " and ett='0' ";
			}
//			else
//			{
//				sql = sql + " and ett in ('1') ";
//				sql = sql + " union all ";
//				sql = sql + " select qc_costePersonal_ej.categoria cs_cat, qc_costePersonal_ej.valor cs_cos, qc_distribucionPersonal_ej.valor cs_cuan from qc_costePersonal_ej ";
//				sql = sql + " inner join qc_distribucionPersonal_ej on qc_distribucionPersonal_ej.ejercicio = qc_costePersonal_ej.ejercicio ";
//				sql = sql + " and qc_costePersonal_ej.categoria = qc_distribucionPersonal_ej.categoria " ;
//				sql = sql + " where qc_distribucionPersonal_ej.ejercicio = " + r_ejercicio + " and qc_distribucionPersonal_ej.area= '" + r_area + "' and ett='0' ";
//			}
			
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				mapeoDesglose = new MapeoDesgloseValoracionNoConformidades();
				mapeoDesglose.setIdqc_incidencias(r_incidencia);
				mapeoDesglose.setArea(r_area);
				mapeoDesglose.setConcepto(rsValor.getString("cs_cat"));
				mapeoDesglose.setCosteUnitario(rsValor.getDouble("cs_cos"));
				mapeoDesglose.setUnidades(rsValor.getDouble("cs_cuan"));
				mapeoDesglose.setTiempo(r_tiempo);
				if (r_esEtt) mapeoDesglose.setEtt("1"); else mapeoDesglose.setEtt("0"); 
				
				rdo = cncs.guardarNuevoDesgloseValoracion(mapeoDesglose);
				if (rdo!=null)
				{
					Notificaciones.getInstance().mensajeError("Problemas en el calculo del coste: " + r_area);
					break;
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return rdo;
	}

	public Long recuperarValor(Integer r_ejercicio, String r_categoria)
	{
		Long valor_obtenido = null;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select valor from qc_costePersonal_ej where ejercicio = " + r_ejercicio + " and categoria = '" + r_categoria + "'" ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			
			while(rsValor.next())
			{
				valor_obtenido = rsValor.getLong("valor") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
		}
		return valor_obtenido;
	}
	

	public boolean comprobarValor(String r_ejercicio, String r_categoria)
	{
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select valor from qc_costePersonal_ej where ejercicio = " + r_ejercicio + " and categoria = '" + r_categoria + "'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		return false;
	}


	public String guardarNuevo(MapeoCostePersonal r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_costePersonal_ej ( ");
			cadenaSQL.append(" qc_costePersonal_ej.ejercicio, ");
    		cadenaSQL.append(" qc_costePersonal_ej.categoria, ");
	        cadenaSQL.append(" qc_costePersonal_ej.valor ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getCategoria()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCategoria());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getValor()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getValor());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	
	public void actualizarValor(String r_ejercicio, Double r_valor, String r_categoria)
	{
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			sql="update qc_costePersonal_ej set valor = " + r_valor + " where ejercicio = " + r_ejercicio + " and categoria = '" + r_categoria + "'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}
	
	public void eliminar(MapeoCostePersonal r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_costePersonal_ej ");            
			cadenaSQL.append(" WHERE qc_costePersonal_ej.ejercicio = ?");
			cadenaSQL.append(" AND qc_costePersonal_ej.categoria = ?");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
			preparedStatement.setString(2, r_mapeo.getCategoria());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}
}