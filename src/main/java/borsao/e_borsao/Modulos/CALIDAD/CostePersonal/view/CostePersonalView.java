package borsao.e_borsao.Modulos.CALIDAD.CostePersonal.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.modelo.MapeoCostePersonal;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.server.consultaCostePersonalServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class CostePersonalView extends GridViewRefresh {

	public static final String VIEW_NAME = "Coste Personal por Ejercicio";
	private final String titulo = "COSTE PERSONAL por EJERCICIO";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoCostePersonal mapeoContadores=null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoCostePersonal> r_vector=null;
    	consultaCostePersonalServer ccs = new consultaCostePersonalServer(CurrentUser.get());
    	r_vector=ccs.datosOpcionesGlobal(null);
    	
    	grid = new OpcionesGrid(r_vector);
		setHayGrid(true);
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public CostePersonalView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
    }

    public void newForm()
    {
		this.mapeoContadores=new MapeoCostePersonal();
		mapeoContadores.setEjercicio(RutinasFechas.añoActualYYYY());
		mapeoContadores.setCategoria("");
		mapeoContadores.setEliminar(false);
		mapeoContadores.setValor(new Double(0));

    	if (grid==null)
    	{
    		grid = new OpcionesGrid(null);
			grid.getContainerDataSource().addItem(mapeoContadores);    	
			grid.editItem(mapeoContadores);
    	}
    	else if (grid.getEditedItemId()==null)
    	{
			grid.getContainerDataSource().addItem(mapeoContadores);    	
			grid.editItem(mapeoContadores);
    	}
    	this.botonesGenerales(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.mapeoContadores = (MapeoCostePersonal) r_fila;
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
