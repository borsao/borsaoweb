package borsao.e_borsao.Modulos.CALIDAD.CostePersonal.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCostePersonal extends MapeoGlobal
{
	private String categoria;
	private Double valor;
	private String ejercicio;
	private boolean eliminar=false;
	

	public MapeoCostePersonal()
	{
	}



	public String getCategoria() {
		return categoria;
	}



	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public Double getValor() {
		return valor;
	}



	public void setValor(Double valor) {
		this.valor = valor;
	}



	public String getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

}