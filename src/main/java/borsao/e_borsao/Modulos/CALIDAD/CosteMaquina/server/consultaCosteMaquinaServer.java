package borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.modelo.MapeoCosteMaquina;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.modelo.MapeoDesgloseValoracionNoConformidades;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaCosteMaquinaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCosteMaquinaServer instance;
	
	public consultaCosteMaquinaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCosteMaquinaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCosteMaquinaServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoCosteMaquina> datosOpcionesGlobal(MapeoCosteMaquina r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoCosteMaquina> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_costeMaquina_ej.ejercicio qcm_eje, ");
		cadenaSQL.append(" qc_costeMaquina_ej.concepto qcm_con, ");
		cadenaSQL.append(" qc_costeMaquina_ej.area qcm_are, ");
        cadenaSQL.append(" qc_costeMaquina_ej.valor qcm_val ");
     	cadenaSQL.append(" FROM qc_costeMaquina_ej ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getConcepto()!=null && r_mapeo.getConcepto().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" concepto = '" + r_mapeo.getConcepto() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contador= " + r_mapeo.getValor() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, area asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoCosteMaquina>();
			
			while(rsOpcion.next())
			{
				MapeoCosteMaquina mapeo = new MapeoCosteMaquina();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setEjercicio(rsOpcion.getString("qcm_eje"));
				mapeo.setArea(rsOpcion.getString("qcm_are"));
				mapeo.setConcepto(rsOpcion.getString("qcm_con"));
				mapeo.setValor(rsOpcion.getDouble("qcm_val"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return vector;
	}
	
	
	public Long recuperarValor(Integer r_ejercicio, String r_area, String r_concepto)
	{
		Long valor_obtenido = null;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select valor from qc_costeMaquina_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			sql = sql + " and concepto = '" + r_concepto + "' ";
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				valor_obtenido = rsValor.getLong("valor") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return valor_obtenido;
	}
	
	
	public Double recuperarCosteHoraGlobalArea(Integer r_ejercicio, String r_area)
	{
		Double valor_obtenido = null;
		Double coste_total = new Double(0);
		Double horas_totales = null;
		
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		String prm = null;
		String area = null;
		
		try
		{
			
			switch (r_area.toUpperCase())
			{
				case "EMBOTELLADORA":
				{
					area = "Horas Embotelladora";
					break;
				}
				case "BIB":
				{
					area = "Horas BIB";
					break;
				}
				case "ENVASADORA":
				{
					area = "Horas Envasadora";
					break;
				}
			}
			
			
			
			prm = "select valor prm_val from qc_parametros_ej where parametro = '" + area + "' and ejercicio = " + r_ejercicio;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + prm.toString() + " : " + new Date());
			rsValor = cs.executeQuery(prm);
			while(rsValor.next())
			{
				horas_totales = rsValor.getDouble("prm_val");
			}
			
			if (horas_totales!=null && horas_totales>0)
			{
				sql="select valor cst_val from qc_costeMaquina_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
				
				con= this.conManager.establecerConexion();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
				rsValor = cs.executeQuery(sql);
				while(rsValor.next())
				{
					valor_obtenido = rsValor.getDouble("cst_val");
					coste_total = coste_total + new Double(valor_obtenido/ horas_totales);
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return coste_total;
	}
	
	public String generarCosteHoraGlobalArea(Integer r_incidencia, Integer r_ejercicio, String r_area, Double r_tiempo)
	{
		MapeoDesgloseValoracionNoConformidades mapeoDesglose = null;
		consultaNoConformidadesServer cncs = null;
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String rdo = null;
		String sql = null;
		String prm = null;
		String area = null;

		Double coste_total = new Double(0);
		Double horas_totales = null;

		try
		{
			cncs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
			
			switch (r_area.toUpperCase())
			{
				case "EMBOTELLADORA":
				{
					area = "Horas Embotelladora";
					break;
				}
				case "BIB":
				{
					area = "Horas BIB";
					break;
				}
				case "ENVASADORA":
				{
					area = "Horas Envasadora";
					break;
				}
			}
			prm = "select valor prm_val from qc_parametros_ej where parametro = '" + area + "' and ejercicio = " + r_ejercicio;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + prm.toString() + " : " + new Date());
			rsValor = cs.executeQuery(prm);
			while(rsValor.next())
			{
				horas_totales = rsValor.getDouble("prm_val");
			}
			
			if (horas_totales!=null && horas_totales>0)
			{
				sql="select concepto cst_con, valor cst_val from qc_costeMaquina_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "' and concepto not like '%Directo%'" ;
				
				con= this.conManager.establecerConexion();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
				rsValor = cs.executeQuery(sql);
				while(rsValor.next())
				{
					mapeoDesglose = new MapeoDesgloseValoracionNoConformidades();
					
					mapeoDesglose.setIdqc_incidencias(r_incidencia);
					mapeoDesglose.setArea("Maquina");
					mapeoDesglose.setConcepto(rsValor.getString("cst_con"));
					mapeoDesglose.setCosteUnitario(rsValor.getDouble("cst_val")/horas_totales);
					mapeoDesglose.setUnidades(new Double(1));
					mapeoDesglose.setTiempo(r_tiempo);
					mapeoDesglose.setEtt("0");
					
					rdo = cncs.guardarNuevoDesgloseValoracion(mapeoDesglose);
					
					if (rdo!=null)
					{
						Notificaciones.getInstance().mensajeError("Problemas con el calculo del coste maquina: " + r_area);
						break;
					}
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return rdo;
	}

	public boolean comprobarValor(String r_ejercicio, String r_area, String r_concepto)
	{
		ResultSet rsValor = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select valor from qc_costeMaquina_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			sql = sql + " and concepto = '" + r_concepto + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			rsValor = cs.executeQuery(sql);
			while(rsValor.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return false;
	}


	public String guardarNuevo(MapeoCosteMaquina r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_costeMaquina_ej ( ");
			cadenaSQL.append(" qc_costeMaquina_ej.ejercicio, ");
    		cadenaSQL.append(" qc_costeMaquina_ej.concepto, ");
    		cadenaSQL.append(" qc_costeMaquina_ej.area, ");
	        cadenaSQL.append(" qc_costeMaquina_ej.valor ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getConcepto()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getConcepto());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getValor()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getValor());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	
	public void actualizarValor(String r_ejercicio, Double r_valor, String r_area, String r_concepto)
	{
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			sql="update qc_costeMaquina_ej set valor = " + r_valor + " where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			sql = sql + " and concepto = '" + r_concepto + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
	}
	
	public void eliminar(MapeoCosteMaquina r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_costeMaquina_ej ");            
			cadenaSQL.append(" WHERE qc_costeMaquina_ej.ejercicio = ?");
			cadenaSQL.append(" AND qc_costeMaquina_ej.area = ?");
			cadenaSQL.append(" AND qc_costeMaquina_ej.concepto = ?");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, Integer.valueOf(r_mapeo.getEjercicio()));
			preparedStatement.setString(2, r_mapeo.getArea());
			preparedStatement.setString(3, r_mapeo.getConcepto());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}