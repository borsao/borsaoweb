package borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCosteMaquina extends MapeoGlobal
{
	private String area;
	private String concepto;
	private Double valor;
	private String ejercicio;
	private boolean eliminar=false;
	

	public MapeoCosteMaquina()
	{
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getConcepto() {
		return concepto;
	}


	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	public Double getValor() {
		return valor;
	}


	public void setValor(Double valor) {
		this.valor = valor;
	}


	public String getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

}