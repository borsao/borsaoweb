package borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.server.consultaCosteMaquinaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = true;
	public boolean actualizar = false;
	
	
    public OpcionesGrid(ArrayList<MapeoCosteMaquina> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("Coste Máquina");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		actualizar = false;
        this.crearGrid(MapeoCosteMaquina.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoCosteMaquina>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	super.doEditItem();
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "concepto", "area","valor");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "80");
    	this.widthFiltros.put("concepto", "210");
    	this.widthFiltros.put("area", "210");

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(120));
    	this.getColumn("ejercicio").setEditorField(getComboBox("El campo es obligatorio!", "ejercicios"));
    	this.getColumn("concepto").setHeaderCaption("Concepto");
    	this.getColumn("concepto").setWidth(new Double(250));    	
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidth(new Double(250));
    	this.getColumn("area").setEditorField(getComboBox("El campo es obligatorio!", "area"));
    	this.getColumn("valor").setHeaderCaption("Valor");
    	this.getColumn("valor").setWidth(new Double(250));
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("valor".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	MapeoCosteMaquina mapeo = (MapeoCosteMaquina) getEditedItemId();
	        	consultaCosteMaquinaServer cs = new consultaCosteMaquinaServer(CurrentUser.get());
	        
	        	if (mapeo.isEliminar())
	        	{
	        		cs.eliminar(mapeo);
	        		remove(mapeo);
	        	}
	        	else
	        	{
	        		if (cs.comprobarValor(mapeo.getEjercicio(), mapeo.getArea(), mapeo.getConcepto()))
		        	{
		        		cs.actualizarValor(mapeo.getEjercicio(), mapeo.getValor(),mapeo.getArea(), mapeo.getConcepto());
		        	}
		        	else
		        	{
		        		cs.guardarNuevo(mapeo);
		        	}
	        	}
        	}
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("valor");
		this.camposNoFiltrar.add("eliminar");
	}

	@Override
	public void calcularTotal() {
		
	}

	private Field<?> getComboBox(String requiredErrorMsg, String r_campo) 
	{
		ComboBox comboBox = new ComboBox();
		
		if (r_campo.equals("area"))
		{
			comboBox.addItem("EMBOTELLADORA");
			comboBox.addItem("ENVASADORA");
			comboBox.addItem("BIB");
			comboBox.addItem("RETRABAJOS");
			
			comboBox.setRequired(true);
			comboBox.setRequiredError(requiredErrorMsg);
			comboBox.setInvalidAllowed(false);
			comboBox.setNullSelectionAllowed(false);
//			comboBox.setStyleName("blanco");
		}
		else if(r_campo.equals("ejercicios"))
		{
			ArrayList<String> ejer = null;
			RutinasEjercicios re = new RutinasEjercicios();
			
			ejer = re.cargarEjercicios();
			IndexedContainer cont = new IndexedContainer(ejer);
			comboBox.setContainerDataSource(cont);
			
			comboBox.setRequired(true);
			comboBox.setRequiredError(requiredErrorMsg);
			comboBox.setInvalidAllowed(false);
			comboBox.setNullSelectionAllowed(false);

		}
		return comboBox;
	}

}
