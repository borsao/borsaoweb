package borsao.e_borsao.Modulos.GENERALES.Impresoras.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.modelo.MapeoImpresoras;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.server.consultaImpresorasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ImpresorasView extends GridViewRefresh {

	public static final String VIEW_NAME = "Impresoras";
	private final String titulo = "Impresoras";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoImpresoras mapeoImpresoras =null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoImpresoras> r_vector=null;
    	consultaImpresorasServer cus = new consultaImpresorasServer(CurrentUser.get());
    	r_vector=cus.datosOpcionesGlobal(null);
    	
    	grid = new OpcionesGrid(r_vector,this);
    	setHayGrid(true);
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ImpresorasView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
    }

    public void newForm()
    {
    	if (grid==null)
    	{
    		grid = new OpcionesGrid(null,this);
			this.mapeoImpresoras=new MapeoImpresoras();
			mapeoImpresoras.setImpresora("");
			mapeoImpresoras.setEliminar(false);
			grid.getContainerDataSource().addItem(mapeoImpresoras);    	
			grid.editItem(mapeoImpresoras);
    	}
    	else if (grid.getEditedItemId()==null)
    	{
    		this.mapeoImpresoras=new MapeoImpresoras();
			mapeoImpresoras.setImpresora("");
			mapeoImpresoras.setEliminar(false);
			grid.getContainerDataSource().addItem(mapeoImpresoras);    	
			grid.editItem(mapeoImpresoras);
    	}
    	
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.mapeoImpresoras = (MapeoImpresoras) r_fila;
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
