package borsao.e_borsao.Modulos.GENERALES.Impresoras.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoImpresoras extends MapeoGlobal
{
	private String impresora;
	private boolean eliminar=false;

	public MapeoImpresoras()
	{
	}

	public String getImpresora() {
		return impresora;
	}

	public void setImpresora(String impresora) {
		this.impresora = impresora;
	}


	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


}