package borsao.e_borsao.Modulos.GENERALES.Impresoras.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.modelo.MapeoImpresoras;

public class consultaImpresorasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaImpresorasServer instance;
	
	public consultaImpresorasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaImpresorasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaImpresorasServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoImpresoras> datosOpcionesGlobal(String r_usuario)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoImpresoras> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT impresora imp_imp ");
     	cadenaSQL.append(" FROM glb_impresoras ");
     	
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoImpresoras>();
			
			while(rsOpcion.next())
			{
				MapeoImpresoras mapeo = new MapeoImpresoras();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setImpresora(rsOpcion.getString("imp_imp"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoImpresoras r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO glb_impresoras ( ");
			cadenaSQL.append(" glb_impresoras.impresora) VALUES (");
		    cadenaSQL.append(" ?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getImpresora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getImpresora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		
		return null;
	}
	
	public void eliminar(MapeoImpresoras r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_impresoras ");            
			cadenaSQL.append(" WHERE glb_impresoras.impresora = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getImpresora());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}