package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAnalisisTurnos extends Window
{

	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    private Combo cmbTipo = null;
    
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;

    private Panel panelResumenSemana =null;
    private GridLayout centralTopSemana = null;
    
	public TextField txtEjercicio= null;

	private String ejercicio = null;
	private String semana = null;
	private String ambitoTemporal = null;

	private boolean verMenu = true;
	public boolean acceso = true;
	
	public pantallaAnalisisTurnos(String r_ejercicio, String r_semana, String r_calculo)
	{
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			this.semana = r_semana;
			this.ambitoTemporal=r_calculo;
			this.ejercicio = r_ejercicio;
			this.setCaption("Analisis Turno");
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	this.txtEjercicio.setValue(this.ejercicio);
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
    		
    		this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumenSemana = new Panel(this.getCaption().toUpperCase());
	    	this.panelResumenSemana.setSizeFull();
	    	this.panelResumenSemana.addStyleName("showpointer");

		    	this.centralTopSemana = new GridLayout(2,1);
		    	this.centralTopSemana.setSizeFull();
		    	this.centralTopSemana.setMargin(true);

	    	this.panelResumenSemana.setContent(this.centralTopSemana);
	    	
	    	this.panelResumen = new Panel("TURNO ");
	    	this.panelResumen.setSizeFull();
	    	this.panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(1,1);
			    this.centralTop2.setSizeFull();
			    this.centralTop2.setMargin(true);

	    	this.panelResumen.setContent(centralTop2);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
//    	this.barAndGridLayout.setExpandRatio(this.topLayoutL, 1);
    	this.barAndGridLayout.addComponent(this.panelResumenSemana);
    	this.barAndGridLayout.setExpandRatio(this.panelResumenSemana, 1);
//    	this.barAndGridLayout.addComponent(this.panelResumen);
//    	this.barAndGridLayout.setExpandRatio(this.panelResumen, 2);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
	private void cargarGrid()
	{
		ArrayList<Integer> vector_personas = null;
		DecimalFormat formato = new DecimalFormat("##.##");
		
		centralTopSemana.removeAllComponents();

		consultaParteProduccionServer cpps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		vector_personas = cpps.personasPorTurno(new Integer(this.txtEjercicio.getValue()), this.semana);

    	IndexedContainer container =null;
    	Iterator<Integer> iterator = null;
    	Integer cuantos = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("turno", String.class, null);
		container.addContainerProperty("personas", Integer.class, null);
			
		iterator = vector_personas.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			cuantos= (Integer) iterator.next();
			
			file.getItemProperty("turno").setValue("Turno de..");
			file.getItemProperty("personas").setValue(cuantos);
		}

		Grid gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
//		gridDatos.setWidth("100%");
		gridDatos.addStyleName("smallgrid");
		
		gridDatos.getColumn("turno").setHeaderCaption("Turnos");
		gridDatos.getColumn("turno").setWidth(250);
		gridDatos.getColumn("personas").setHeaderCaption("Operarios / Turno");
		gridDatos.getColumn("personas").setWidth(180);
		gridDatos.getColumn("personas").setRenderer(new NumberRenderer(formato));
		
		gridDatos.addSelectionListener(new SelectionListener() {
			Integer pers = null;
			@Override
			public void select(SelectionEvent event) {
				
				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
				if (file!=null && file.getItemProperty("personas").getValue()!=null)
				{
					pers= new Integer(file.getItemProperty("personas").getValue().toString());
					cargarResumen(pers);
				}
			}
		});
		
//		Double totalU = new Double(0) ;
//    	Double valor = null;
//        	
//    	FooterRow footer = gridDatos.appendFooterRow();
//        	
//    	Indexed indexed = gridDatos.getContainerDataSource();
//    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
//    	
//		totalU = new Double(0) ;
//    	for(Object itemId : list)
//    	{
//    		Item item = indexed.getItem(itemId);
//        		
//    		valor = (Double) item.getItemProperty("tiempo").getValue();
//    		if (valor!=null)
//    			totalU += valor;
//				
//    	}
//    	footer.getCell("incidencia").setText("Totales ");
//    	footer.getCell("tiempo").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
//    	footer.getCell("tiempo").setStyleName("Rcell-pie");
		
    	gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("personas".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
		
		Panel panel = new Panel(titulo);
    	panel.setSizeUndefined(); // Shrink to fit content
//    	panel.setHeight("250px");
    	panel.setContent(gridDatos);
    	
    	centralTopSemana.addComponent(panel,0,0);
    	
    }
	
    private void cargarResumen(Integer r_pers)
	{
//    	Notificaciones.getInstance().mensajeInformativo("Vamos a ver rendimiento turnos de " + r_pers + " personas");
    	boolean datosGenerados = false;
    	HashMap<String, Double> hasProd = null;
    	HashMap<String, Double> hasProdH = null;
    	Integer ejercicio = new Integer(this.txtEjercicio.getValue());
		double horasAUT = 0;
		double horasJAU = 0;
		double horasETI = 0;
		double horasETH = 0;

		double botellasAUT = 0;
		double botellasJAU = 0;
		double botellasETI = 0;
		double botellasETH = 0;

		double mediaAUT = 0;
		double mediaJAU = 0;
		double mediaETI = 0;
		double mediaETH = 0;
//		
		centralTopSemana.removeComponent(1, 0);
		consultaParteProduccionServer cpps = consultaParteProduccionServer.getInstance(CurrentUser.get());

		datosGenerados = cpps.recuperarFechasTurnos(new Integer(this.txtEjercicio.getValue()), r_pers, this.semana);
		
		if (datosGenerados)
		{
			horasAUT = cpps.recuperarHorasParte(ejercicio, r_pers, "horasAUTOMATICO",this.semana);
			horasJAU = cpps.recuperarHorasParte(ejercicio, r_pers, "horasJAULON",this.semana);
			horasETI = cpps.recuperarHorasParte(ejercicio, r_pers, "horasETIQUETADO",this.semana);
			horasETH = cpps.recuperarHorasParte(ejercicio, r_pers, "horasMANUAL",this.semana);
			
			consultaProduccionDiariaServer cpds = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
			
			hasProd = cpds.obtenerDistintasProduccionesFecha(ejercicio.toString());
			hasProdH = cpds.obtenerDistintasProduccionesHorizontalFecha(ejercicio.toString());
			
			if (hasProdH.get("HORIZONTAL")!=null) botellasETH = hasProdH.get("HORIZONTAL"); else botellasETH=new Double(0);
			if (hasProd.get("JAULON")!=null) botellasJAU = hasProd.get("JAULON"); else botellasJAU=0;
			if (hasProd.get("ETIQUETADO")!=null) botellasETI = new Double(hasProd.get("ETIQUETADO").doubleValue()-botellasETH); else botellasETI=0;
			if (hasProd.get("DIRECTA")!=null) botellasAUT = new Double(hasProd.get("DIRECTA").doubleValue()-botellasETI-botellasETH); else botellasAUT=0;
			
			mediaAUT = botellasAUT/horasAUT;
			mediaJAU = botellasJAU/horasJAU;
			mediaETI = botellasETI/horasETI;
			mediaETH = botellasETH/horasETH;
			
			IndexedContainer container =null;
	    		
			container = new IndexedContainer();
			container.addContainerProperty("produccion", String.class, null);
			container.addContainerProperty("botellas", Double.class, null);
			container.addContainerProperty("horas", Double.class, null);
			container.addContainerProperty("media", Double.class, null);
				
				Object newItemId = container.addItem();
				Item file = container.getItem(newItemId);
				
					file.getItemProperty("produccion").setValue("DIRECTA");
					file.getItemProperty("botellas").setValue(botellasAUT);
					file.getItemProperty("horas").setValue(horasAUT);
					file.getItemProperty("media").setValue(mediaAUT);

				newItemId = container.addItem();
				file = container.getItem(newItemId);
					
					file.getItemProperty("produccion").setValue("JAULON");
					file.getItemProperty("botellas").setValue(botellasJAU);
					file.getItemProperty("horas").setValue(horasJAU);
					file.getItemProperty("media").setValue(mediaJAU);
					
				newItemId = container.addItem();
				file = container.getItem(newItemId);
					
					file.getItemProperty("produccion").setValue("ETIQ");
					file.getItemProperty("botellas").setValue(botellasETI);
					file.getItemProperty("horas").setValue(horasETI);
					file.getItemProperty("media").setValue(mediaETI);

				newItemId = container.addItem();
				file = container.getItem(newItemId);
					
					file.getItemProperty("produccion").setValue("HORIZ.");
					file.getItemProperty("botellas").setValue(botellasETH);
					file.getItemProperty("horas").setValue(horasETH);
					file.getItemProperty("media").setValue(mediaETH);

			Grid gridDatos2= new Grid(container);
			gridDatos2.setSelectionMode(SelectionMode.SINGLE);
			gridDatos2.setSizeFull();
			gridDatos2.addStyleName("smallgrid");
			
			gridDatos2.getColumn("produccion").setHeaderCaption("Tipo Produccion");
			gridDatos2.getColumn("produccion").setWidth(250);
			gridDatos2.getColumn("botellas").setHeaderCaption("Botellas");
			gridDatos2.getColumn("botellas").setWidth(180);
			gridDatos2.getColumn("horas").setHeaderCaption("Horas");
			gridDatos2.getColumn("horas").setWidth(180);
			gridDatos2.getColumn("media").setHeaderCaption("Media");
			gridDatos2.getColumn("media").setWidth(180);
			
	    	gridDatos2.setCellStyleGenerator(new Grid.CellStyleGenerator() {
	            
	            @Override
	            public String getStyle(Grid.CellReference cellReference) {
	            	if ("produccion".equals(cellReference.getPropertyId())) 
	            	{            		
	            		return "cell-normal";
	            	}
	            	else
	            	{
	            		return "Rcell-normal";
	            	}
	            }
	        });
			
			Panel panel = new Panel(titulo);
	    	panel.setSizeUndefined(); // Shrink to fit content
//	    	panel.setHeight("250px");
	    	panel.setContent(gridDatos2);
	    	centralTopSemana.addComponent(gridDatos2,1,0);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Error al generar los datos de los turnos");
		}
	}
}