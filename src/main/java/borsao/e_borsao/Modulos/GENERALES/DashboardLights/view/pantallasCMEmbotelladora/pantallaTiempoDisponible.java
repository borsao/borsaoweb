package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.ClasesPropias.graficas.barras;
import borsao.e_borsao.ClasesPropias.graficas.queso;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.pantallaRelacionIncidencias;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaTiempoDisponible extends Window
{

	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    private Combo cmbTipo = null;
    
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;

    private Panel panelResumenSemana =null;
    private GridLayout centralTopSemana = null;
    
	public TextField txtEjercicio= null;

	private Double horas = null;
	private Double programadas = null;
	private Double incidencias = null;
	
	private boolean verMenu = true;
	public boolean acceso = true;
	
	public pantallaTiempoDisponible(String r_titulo, Double r_horas, Double r_programadas, Double r_incidencias)
	{
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			this.horas = r_horas;
			this.incidencias = r_incidencias;
			this.programadas = r_programadas;
			
			this.setCaption(r_titulo);
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarResumen();
			cargarResumenDisponible();
			cargarResumenIncidencias();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
    		this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumenSemana = new Panel("TIEMPO DISPONIBLE ");
	    	this.panelResumenSemana.setSizeFull();
	    	this.panelResumenSemana.addStyleName("showpointer");

		    	this.centralTopSemana = new GridLayout(3,1);
		    	this.centralTopSemana.setSizeFull();
		    	this.centralTopSemana.setMargin(true);

	    	this.panelResumenSemana.setContent(this.centralTopSemana);
	    	
	    	this.panelResumen = new Panel("CALIDAD ");
	    	this.panelResumen.setSizeFull();
	    	this.panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(1,1);
			    this.centralTop2.setSizeFull();
			    this.centralTop2.setMargin(true);

	    	this.panelResumen.setContent(centralTop2);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumenSemana);
    	this.barAndGridLayout.setExpandRatio(this.panelResumenSemana, 1);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {
    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
	private void cargarResumen()
	{
		String destacar = null;
		MapeoValores map = null;
		int numero = 0;
		
		HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
		ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
		
		if (horas!=null && horas!=0)
		{
			map = new MapeoValores();
			map.setClave("DISPONIBLE");
			map.setValorDouble(horas-programadas-incidencias);
			vector.add(map);
			hash.put(numero, new Color(229,153,5));
			numero+=1;
		}
		
		if (programadas!=null && programadas!=0)
		{
			map = new MapeoValores();
			map.setClave("PROGRAMADAS");
			map.setValorDouble(programadas);
//			destacar = map.getClave();
			vector.add(map);
			hash.put(numero, new Color(131,158,247));
			numero+=1;
		}

		if (incidencias!=null && incidencias!=0)
		{
			map = new MapeoValores();
			map.setClave("INCIDENCIAS");
			map.setValorDouble(incidencias);
			vector.add(map);
			hash.put(numero, Color.red);
		}
		
		if(!vector.isEmpty())
		{
			Panel grafico = queso.generarGrafico("Reparto Tiempos", "400px","322px", vector, destacar, hash, Color.white);
			centralTopSemana.addComponent(grafico,0,0);
		}
	}

	private void cargarResumenDisponible()
	{
    	String destacar = null;
    	MapeoValores map = null;
    	int numero = 0;
    	
    	HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	
    	if (horas!=null && horas!=0)
    	{
    		map = new MapeoValores();
	    	map.setClave("DISPONIBLE");
	    	map.setValorDouble(horas-programadas);
	    	vector.add(map);
	    	hash.put(numero, new Color(229,153,5));
	    	numero+=1;
    	}
    	
    	if (programadas!=null && programadas!=0)
    	{
	    	map = new MapeoValores();
	    	map.setClave("PROGRAMADAS");
	    	map.setValorDouble(programadas);
	    	destacar = map.getClave();
	    	vector.add(map);
	    	hash.put(numero, new Color(131,158,247));
    	}
    	
    	if(!vector.isEmpty())
    	{
    		Panel grafico = queso.generarGrafico("Tiempo Disponible", "400px","322px", vector, destacar, hash, Color.white);
    		centralTopSemana.addComponent(grafico,1,0);
    	}
	}

    private void cargarResumenIncidencias()
    {
    	String destacar = null;
    	MapeoValores map = null;
    	int numero = 0;
    	
    	HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	
    	if (horas!=null && horas!=0)
    	{
    		map = new MapeoValores();
    		map.setClave("DISPONIBLE");
    		map.setValorDouble(horas-programadas-incidencias);
    		vector.add(map);
    		hash.put(numero, new Color(229,153,5));
    		numero+=1;
    	}
    	
    	if (programadas!=null && programadas!=0)
    	{
    		map = new MapeoValores();
    		map.setClave("INCIDENCIAS");
    		map.setValorDouble(incidencias);
    		destacar = map.getClave();
    		vector.add(map);
    		hash.put(numero, Color.red);
    	}
    	
    	if(!vector.isEmpty())
    	{
    		Panel grafico = queso.generarGrafico("Tiempo Incidencias", "400px","322px", vector, destacar, hash, Color.white);
    		centralTopSemana.addComponent(grafico,2,0);
    	}
    }
}