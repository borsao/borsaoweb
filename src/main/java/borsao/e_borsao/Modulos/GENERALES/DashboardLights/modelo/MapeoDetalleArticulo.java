package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDetalleArticulo extends MapeoGlobal
{
	private Integer almacen;
	private Integer semana;
	private String articulo;
	private String descripcion;
	
	private double stock;
	private double pdte;
	private double sobraFalta;

	public MapeoDetalleArticulo()
	{
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public double getStock() {
		return stock;
	}

	public double getPdte() {
		return pdte;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setStock(double stock) {
		this.stock = stock;
	}

	public void setPdte(double pdte) {
		this.pdte = pdte;
	}

	public double getSobraFalta() {
		return sobraFalta;
	}

	public void setSobraFalta(double sobraFalta) {
		this.sobraFalta = sobraFalta;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

}