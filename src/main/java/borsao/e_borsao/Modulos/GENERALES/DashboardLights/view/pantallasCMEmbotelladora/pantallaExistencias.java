package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.util.HashMap;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEmbotelladoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaExistencias extends Window
{
	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;

    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;
    
	public TextField txtEjercicio= null;

	public boolean acceso = true;
	private boolean verMenu = true;
	private String tipoArticulo = null;
	
	public pantallaExistencias(String r_titulo, String r_tipo)
	{
		
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			this.tipoArticulo = r_tipo;
			this.setCaption(r_titulo);
			this.center();
			
	    	this.setSizeFull();
	    	this.addStyleName("crud-view");
	    	this.setResponsive(true);
	
	    	this.cargarPantalla();
			
			this.cargarListeners();
	
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setSizeFull();
			
	    	this.setContent(this.barAndGridLayout);
	    	this.setResponsive(true);
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}

	}
	public pantallaExistencias(String r_titulo, String r_ejercicio,String r_tipo)
	{
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			this.tipoArticulo = r_tipo;
			this.setCaption(r_titulo);
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			this.txtEjercicio.setValue(r_ejercicio);
			cargarResumen();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
//		    	this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
		    	
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
	    	this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumen = new Panel("EXISTENCIAS");
	    	this.panelResumen.setSizeFull();
	    	this.panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(1,1);
			    this.centralTop2.setSizeFull();
			    this.centralTop2.setMargin(true);

	    	this.panelResumen.setContent(centralTop2);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumen);
    	this.barAndGridLayout.setExpandRatio(this.panelResumen, 1);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
    private void cargarResumen()
	{
		Double media=null;
		HashMap<String, Double> hashExistencias = null;
		HashMap<String, Double> hashExistenciasAnterior = null;
		
		consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
		
		hashExistencias = cdes.recuperarExistenciasMes(new Integer(this.txtEjercicio.getValue()), this.tipoArticulo);
		hashExistenciasAnterior = cdes.recuperarExistenciasMes(new Integer(this.txtEjercicio.getValue())-1, tipoArticulo);
		
	    String tituloY = null;
	    
	    tituloY="Exis. / Mes";
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Exis.", "Meses", tituloY, hashExistencias, hashExistenciasAnterior, null, txtEjercicio.getValue(), "Año Anterior", null, null, null,0, 0, 0, true,false,false);
	    
	    centralTop2.addComponent(wrapper,0,0); 
	    
	    panelResumen.setCaption("Existencias " + txtEjercicio.getValue().toString());
	}
}