package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.addon.JFreeChartWrapper;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.view.pantallaObservacionesProgramacion;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.pantallaLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.pantallaVentas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view.pantallaNotasArticulo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.view.PantallaPrevisionEmbotellados;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view.pantallaLineasProgramacionesEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionAsignacionProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardArticuloView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Articulo";
	private final String titulo = "CUADRO MANDO ARTICULO";
	private final int intervaloRefresco = 0*60*1000;
	private ChatRefreshListener cr = null;
	private ventanaAyuda vHelp  =null;
	
	private String articuloSeleccionado = null;
	private String articuloPadre = null;
	private String descripcionArticuloSeleccionado = null;
	private String descripcionArticuloPadre = null;
	
	private CssLayout barAndGridLayout = null;    
    private VerticalLayout topLayout = null;
    private HorizontalLayout topLayoutL = null;
    private HorizontalLayout topLayoutS = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;
    

    private Panel panelResumen =null;
    private Panel panelVentas =null;
    private VerticalLayout panelVentas0 =null;
    private Panel panelVentas1 =null;
    private Panel panelVentas2 =null;
    private Panel panelArticulos =null;
    private Panel panelProduccion =null;
    private Panel panelPlanificacion =null;
    
    private GridLayout centralTop = null;
    private GridLayout centralTop2 = null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle1 = null;
    private GridLayout centralMiddle2 = null;
    private GridLayout centralBottom = null;
    private GridLayout centralBottom2 = null;
    
	public TextField txtEjercicio= null;
	public TextField txtArticulo= null;

	private Button procesar=null;
	private Button opcMenos= null;
	private Button opcMas= null;
	private Button opcArticulo = null;
	private ComboBox cmbResumen = null;
	
	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardArticulo mapeo=null;

	private HashMap<String, Double> hashMPPS = null;
	private MapeoGeneralDashboardArticulo mapeoMPPS=null;

	private boolean prevision = false;
	private String tipoArticulo = null;
	private ArrayList<MapeoArticulos> vectorPadres = null; 
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardArticuloView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		
		this.cargarListeners();
//		this.ejecutarTimer();
//		this.procesar();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
//    	setSpacing(false);
    	
    	
    	this.barAndGridLayout = new CssLayout();
//        this.barAndGridLayout.setSpacing(false);
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

//        final Navigator navigator = new Navigator(eBorsao.getCurrent(), this.barAndGridLayout);

		    	this.topLayoutL = new HorizontalLayout();
//		    	this.topLayoutL.addStyleName("v-panelTitulo");
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.txtEjercicio=new TextField("Ejercicio");
			    	this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);

			    	this.txtArticulo=new TextField("Articulo");
		    		this.txtArticulo.setEnabled(true);
		    		this.txtArticulo.setWidth("125px");
		    		this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		
		    		this.cmbResumen = new ComboBox("Resumen Ventas");
		    		this.cmbResumen.addItem("Paises");
		    		this.cmbResumen.addItem("Clientes");
		    		this.cmbResumen.setValue("Paises");
		    		this.cmbResumen.setNewItemsAllowed(false);
		    		this.cmbResumen.setWidth("125px");
		    		this.cmbResumen.setNullSelectionAllowed(false);
		        	this.cmbResumen.addStyleName(ValoTheme.COMBOBOX_TINY);		    		

		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);

		    		this.opcArticulo=new Button();
		        	this.opcArticulo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.opcArticulo.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.opcArticulo.setIcon(FontAwesome.QUESTION);

			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	if (LecturaProperties.semaforos)
		    	{
		    		this.topLayoutL.addComponent(this.opcRefresh);
		    		this.topLayoutL.setComponentAlignment(this.opcRefresh,  Alignment.BOTTOM_LEFT);
		    	}
		    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
//	    		this.topLayoutL.addComponent(this.opcMenos);
//	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.txtEjercicio);
//	    		this.topLayoutL.addComponent(this.opcMas);
//	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.txtArticulo);
	    		this.topLayoutL.addComponent(this.opcArticulo);
	    		this.topLayoutL.setComponentAlignment(this.opcArticulo,  Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.cmbResumen);
		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
//		    	this.topLayoutL.setStyleName("miPanel");
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

//	    	this.topLayoutL.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

	    	panelArticulos = new Panel("Padres del seleccionado");
		    panelArticulos.setSizeUndefined();
		    panelArticulos.setVisible(false);
		    panelArticulos.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelArticulos );

		    	this.centralTop = new GridLayout(4,1);
		    	this.centralTop.setSpacing(true);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);

		    	panelArticulos.setContent(centralTop);


	    	panelResumen = new Panel("Resumen Año ");
		    panelResumen.setSizeUndefined();
		    panelResumen.addStyleName("showpointer");
//		    new PanelCaptionBarToggler<Panel>( panelResumen );

			    this.centralTop2 = new GridLayout(3,1);
			    this.centralTop2.setWidth("100%");
			    this.centralTop2.setMargin(true);

		    	panelResumen.setContent(centralTop2);
		    	
	    	panelVentas = new Panel("Datos Ventas ");
		    panelVentas.setWidth("100%");
		    panelVentas.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelVentas );

		    	panelVentas0 = new VerticalLayout();
			    panelVentas0.setWidth("100%");

			    	panelVentas1 = new Panel("");
				    panelVentas1.setWidth("100%");
		
				    	this.centralMiddle = new GridLayout(4,1);
				    	this.centralMiddle.setWidth("100%");
				    	this.centralMiddle.setMargin(true);
				    	this.centralMiddle.setSpacing(true);
			    	panelVentas1.setContent(centralMiddle);
			    	
			    	panelVentas2 = new Panel("");
				    panelVentas2.setWidth("100%");
		
				    	this.centralMiddle1 = new GridLayout(4,1);
				    	this.centralMiddle1.setWidth("100%");
				    	this.centralMiddle1.setMargin(true);
				    	this.centralMiddle1.setSpacing(true);
			    	panelVentas2.setContent(centralMiddle1);
			    	
			    	panelVentas0.addComponent(panelVentas1);
			    	panelVentas0.addComponent(panelVentas2);
			    	
	    	panelVentas.setContent(panelVentas0);
	    	panelVentas.getContent().setVisible(false);
	    	panelVentas.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    	this.centralMiddle2 = new GridLayout(2,1);
	    	this.centralMiddle2.setSizeUndefined();
	    	this.centralMiddle2.setMargin(true);


	    	panelProduccion = new Panel("Datos Produccion ");
		    panelProduccion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelProduccion );
		    
	    	
		    	this.centralBottom = new GridLayout(4,1);
		    	this.centralBottom.setWidth("100%");
		    	this.centralBottom.setMargin(true);

		    	panelProduccion.setContent(centralBottom);
		    	panelProduccion.getContent().setVisible(false);
		    	panelProduccion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    	panelPlanificacion = new Panel("Datos Planificación");
		    panelPlanificacion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelPlanificacion );
		    
	    	
		    	this.centralBottom2 = new GridLayout(4,1);
		    	this.centralBottom2.setWidth("100%");
		    	this.centralBottom2.setMargin(true);

		    	panelPlanificacion.setContent(centralBottom2);
		    	panelPlanificacion.getContent().setVisible(false);
		    	panelPlanificacion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.centralTop);
    	this.barAndGridLayout.addComponent(this.panelResumen);
    	this.barAndGridLayout.addComponent(this.centralMiddle2);
    	this.barAndGridLayout.addComponent(this.panelVentas);
    	this.barAndGridLayout.addComponent(this.panelProduccion);
//    	this.barAndGridLayout.addComponent(this.panelPlanificacion);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
//    	setExpandRatio(this.barAndGridLayout, 1);
        
    }

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		consultaArticulosSMPTServer cps  = new consultaArticulosSMPTServer(CurrentUser.get());
		vectorArticulos=cps.vector();
		this.vHelp = new ventanaAyuda(this.txtArticulo, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

    private void cargarListeners() 
    {

    	this.txtArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				comprobarArticulo(txtArticulo.getValue());
			}
		});
    	this.opcArticulo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			ventanaMargenes();
    		}
    			
    	});
    	
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int year = new Integer(txtEjercicio.getValue()).intValue();
				year = year + 1;
				if (year > new Integer(RutinasFechas.añoActualYYYY())) year = year -1;
				txtEjercicio.setValue(String.valueOf(year));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int year = new Integer(txtEjercicio.getValue()).intValue();
				year = year - 1;
				txtEjercicio.setValue(String.valueOf(year));
			}
		});


    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});

    	LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};
//		centralLayoutT1.addLayoutClickListener(lcl);

    }
    
    private void procesar()
    {
    	comprobarArticulo(txtArticulo.getValue());
    	if (tipoArticulo!=null)
    	{
			eliminarDashBoards(true);
			
			if (txtEjercicio.getValue() == null || txtEjercicio.getValue().length()==0 || (new Integer(txtEjercicio.getValue().toString()) > new Integer(RutinasFechas.añoActualYYYY())))
			{
				Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
			}
			else
			{
				cargarDashboards();
			}
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Rellena el articulo correctamente");
    	}
    }
    
    public void eliminarDashBoards(boolean r_todos)
    {
    	if (r_todos) centralTop.removeAllComponents();
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle1.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	panelResumen.setCaption("Resumen Año");
    	panelVentas.setCaption("Datos Ventas");;
    	panelProduccion.setCaption("Datps Producción / Planificacion");
    	    	
    }
    
    public void cargarDashboards() 
    {
    	

    	/*
    	 * Si el articulo es PT cargaremos todos los datos y graficos convenientemente,
    	 * en caso contrario cargaremos un grid con los datos de los padres que tienen
    	 * el articulo escogido en su escandallo.
    	 * 
    	 * Daremos los datos básicos del stock pero ya no cargaremos nada mas
    	 * Seleccionando alguno de los padres mostrados cargaremos el resto de graficos
    	 */
    	switch (tipoArticulo)
    	{
	    	case "PT":
	    	{
	    		/*
	    		 * cargar panel resumen datos
	    		 */
	    		ejecutarPadres();
	    		break;
	    	}
	    	case "PS":
	    	{
	    		cargarAccionesHijo(true);
	    		cargarPadres();
	    		cargarResumenArticuloSeleccionado(tipoArticulo);
	    		break;
	    	}
	    	case "MP":
	    	{
	    		cargarAccionesHijo(false);
	    		cargarPadres();
	    		cargarResumenArticuloSeleccionado(tipoArticulo);
	    		break;
	    	}
	    	default:
	    	{
	    		cargarAccionesHijo(false);
	    		cargarPadres();
	    		cargarResumenArticulosPadres();
	    		break;
	    	}
    	}

    	
    	/*
    	 * Cargamos grid registros produccion
    	 */
//    	cargarRegistros(ejer);
    }

    private void cargarResumenArticuloSeleccionado(String r_tipo)
    {
    	String ejer = this.txtEjercicio.getValue();
    	this.mapeoMPPS = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticosMPPS(ejer,this.articuloSeleccionado, r_tipo);
    	this.cargarResumenMPPS(r_tipo);
    	/*
    	 * 
    	 * Por un lado cargaremos producción realizada: 
    	 * 
    	 * 	Si tipo Mp -> movimientos 99 del articulo seleccionado
    	 * 	Si tipo PS -> movimientos 49 del articulo seleccionado
    	 * 
    	 * Por otro lado, si es MP y el articulo es granel
    	 * 
    	 * 	a jaulon -> movimientos 49 de todos los PS que tengan mi articulo como hijo
    	 *  directos -> la diferencia entre la producción total y el calculado "a jaulon"
    	 *  
    	 * 
    	 *  
    	 */
    }

    private void cargarResumenArticulosPadres()
    {
    	String ejer = this.txtEjercicio.getValue();
    	this.mapeoMPPS = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticosPadres(ejer,this.vectorPadres);
    	this.cargarResumenMPPS(null);
    	/*
    	 * 
    	 * Por un lado cargaremos producción realizada: 
    	 * 
    	 * 	Si tipo Mp -> movimientos 99 del articulo seleccionado
    	 * 	Si tipo PS -> movimientos 49 del articulo seleccionado
    	 * 
    	 * Por otro lado, si es MP y el articulo es granel
    	 * 
    	 * 	a jaulon -> movimientos 49 de todos los PS que tengan mi articulo como hijo
    	 *  directos -> la diferencia entre la producción total y el calculado "a jaulon"
    	 *  
    	 * 
    	 *  
    	 */
    }

    private void ejecutarPadres()
    {
    	String ejer = this.txtEjercicio.getValue();
    	this.mapeo = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticosPT(ejer,this.articuloPadre, this.tipoArticulo);
		cargarAcciones();
		cargarResumenes();
		cargarVentas();
		cargarProduccion();
    }
    
    private void cargarPadres()
    {
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	vectorPadres = ces.recuperarPadresDesc(this.articuloSeleccionado);
    	IndexedContainer container =null;
    	Iterator<MapeoArticulos> iterator = null;
    	MapeoArticulos mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
			
		iterator = vectorPadres.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoArticulos) iterator.next();
			
			file.getItemProperty("articulo").setValue(mapeo.getArticulo().substring(0,7));
			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
		}

		Grid gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
		gridDatos.setWidth("100%");
		gridDatos.addStyleName("minigrid");
		gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
//				if (grid.getSelectionModel() instanceof SingleSelectionModel && !estoyEnCelda)
//					app.filaSeleccionada();
//				else
//					estoyEnCelda = false;
				articuloPadre= file.getItemProperty("articulo").getValue().toString();
				descripcionArticuloPadre = cas.obtenerDescripcionArticulo(articuloPadre.trim());
				tipoArticulo = cas.obtenerTipoArticulo(articuloPadre).trim();
				eliminarDashBoards(false);
				ejecutarPadres();
			}
		});

		Panel panel = new Panel("Articulos Padre del " + articuloSeleccionado );
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setContent(gridDatos);
		centralTop.addComponent(panel,0,0);
    }
    
    private void cargarResumenes()
    {
		this.cargarResumen();
		this.cargarResumenStock();
		this.cargarDatosPlanificacion();
		
		panelResumen.setCaption("Resumen Año " + txtEjercicio.getValue().toString() + " del " + articuloPadre + " " + descripcionArticuloPadre);
    }
    
    private void cargarVentas()
    {
		this.cargarResumenVentas();
		this.cargarResumenVentasAnterior();
		this.cargarResumenVentasAnterior2();
		this.cargarResumenVentasAnterior3();

    	this.evolucionVentas();
    	this.evolucionVentasAcumuladas();
    	
    	if (cmbResumen.getValue().toString().contentEquals("Clientes"))
    		this.resumenVentaClientes();
    	else
    		this.resumenVentaPaises();
    	
    	panelVentas.setCaption("Datos Ventas del " + articuloPadre + " " + descripcionArticuloPadre);
    }

    private void cargarProduccion()
    {
		this.cargarResumenProduccion();
		this.cargarResumenProduccionAnterior();
		this.cargarResumenProduccionAnterior2();
		this.cargarResumenProduccionAnterior3();
		
    	panelProduccion.setCaption("Datos Produccion del " + articuloPadre + " " + descripcionArticuloPadre);
    }
    private void cargarResumenMPPS(String r_tipo)
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblProduccion= null;
    	Label lblJaulon= null;
    	Label lblDirecta= null;
    	Label lblVenta= null;

    	Button lblValorProduccion= null;
    	Button lblValorJaulon= null;
    	Button lblValorDirecta= null;
    	Button lblValorVenta= null;
    	    	
    	Panel panel = new Panel("RESUMEN por " + this.txtArticulo.getValue() + " del " + txtEjercicio.getValue());
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion : ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccionMPPS("G",new Integer(txtEjercicio.getValue().toString()),true);
				
			}
		});

    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion,Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	if (r_tipo!=null && r_tipo.contentEquals("MP") && this.articuloSeleccionado.startsWith("0101"))
    	{
	    	lblJaulon= new Label();
	    	lblJaulon.setValue("A Jaulon : ");
	    	lblJaulon.setWidth(anchoLabelTitulo);    	
	    	lblJaulon.addStyleName("lblTitulo");
	
	    	lblValorJaulon= new Button();
	    	lblValorJaulon.setWidth(anchoLabelDatos);
	    	lblValorJaulon.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.prodJ")));
	    	lblValorJaulon.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorJaulon.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorJaulon.addStyleName("lblDatos");
	    	
	    	lblValorJaulon.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaProduccionMPPS("PS", new Integer(txtEjercicio.getValue().toString()),true);
				}
			});
	
	    	lin2.addComponent(lblJaulon);
	    	lin2.addComponent(lblValorJaulon);
	    	lin2.setComponentAlignment(lblJaulon,Alignment.MIDDLE_LEFT);
	    	lin2.setComponentAlignment(lblValorJaulon, Alignment.MIDDLE_RIGHT);
	    	
	    	lblDirecta= new Label();
	    	lblDirecta.setValue("Directa : ");
	    	lblDirecta.setWidth(anchoLabelTitulo);    	
	    	lblDirecta.addStyleName("lblTitulo");
	
	    	lblValorDirecta= new Button();
	    	lblValorDirecta.setWidth(anchoLabelDatos);
	    	lblValorDirecta.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.prodD")));
	    	lblValorDirecta.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorDirecta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorDirecta.addStyleName("lblDatos");
	    	
	    	lblValorDirecta.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaProduccionMPPS("PT", new Integer(txtEjercicio.getValue().toString()),true);
					
				}
			});
	
	    	lin3.addComponent(lblDirecta);
	    	lin3.addComponent(lblValorDirecta);
	    	lin3.setComponentAlignment(lblDirecta,Alignment.MIDDLE_LEFT);
	    	lin3.setComponentAlignment(lblValorDirecta, Alignment.MIDDLE_RIGHT);
	    	
	    	lblVenta= new Label();
	    	lblVenta.setValue("Venta : ");
	    	lblVenta.setWidth(anchoLabelTitulo);    	
	    	lblVenta.addStyleName("lblTitulo");

	    	lblValorVenta= new Button();
	    	lblValorVenta.setWidth(anchoLabelDatos);
	    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.venta")));
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorVenta.addStyleName("lblDatos");
	    	
	    	lblValorVenta.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaVentasMPPS();
					
				}
			});

	    	lin4.addComponent(lblVenta);
	    	lin4.addComponent(lblValorVenta);
	    	lin4.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
	    	lin4.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);

    	}
    	else if (r_tipo!=null && r_tipo.contentEquals("PS") && this.articuloSeleccionado.startsWith("0102"))
    	{
	    	lblJaulon= new Label();
	    	lblJaulon.setValue("Etiquetado : ");
	    	lblJaulon.setWidth(anchoLabelTitulo);    	
	    	lblJaulon.addStyleName("lblTitulo");
	
	    	lblValorJaulon= new Button();
	    	lblValorJaulon.setWidth(anchoLabelDatos);
	    	lblValorJaulon.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.prodJ")));
	    	lblValorJaulon.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorJaulon.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorJaulon.addStyleName("lblDatos");
	    	
	    	lblValorJaulon.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaProduccionMPPS("PS", new Integer(txtEjercicio.getValue().toString()),true);
				}
			});
	
	    	lin2.addComponent(lblJaulon);
	    	lin2.addComponent(lblValorJaulon);
	    	lin2.setComponentAlignment(lblJaulon,Alignment.MIDDLE_LEFT);
	    	lin2.setComponentAlignment(lblValorJaulon, Alignment.MIDDLE_RIGHT);
	    	
	    	lblVenta= new Label();
	    	lblVenta.setValue("Venta : ");
	    	lblVenta.setWidth(anchoLabelTitulo);    	
	    	lblVenta.addStyleName("lblTitulo");

	    	lblValorVenta= new Button();
	    	lblValorVenta.setWidth(anchoLabelDatos);
	    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.venta")));
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorVenta.addStyleName("lblDatos");
	    	
	    	lblValorVenta.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaVentasMPPS();
					
				}
			});

	    	lin4.addComponent(lblVenta);
	    	lin4.addComponent(lblValorVenta);
	    	lin4.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
	    	lin4.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);

    	}
    	else
    	{
    		lblVenta= new Label();
	    	lblVenta.setValue("Venta : ");
	    	lblVenta.setWidth(anchoLabelTitulo);    	
	    	lblVenta.addStyleName("lblTitulo");

	    	lblValorVenta= new Button();
	    	lblValorVenta.setWidth(anchoLabelDatos);
	    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeoMPPS.getHashValores().get("0.venta")));
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorVenta.addStyleName("lblDatos");
	    	
	    	lblValorVenta.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaVentasMPPS();
					
				}
			});

	    	lin4.addComponent(lblVenta);
	    	lin4.addComponent(lblValorVenta);
	    	lin4.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
	    	lin4.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);
    	}
    	
    	content.addComponent(lin);
    	if (r_tipo!=null) content.addComponent(lin2);
    	if (r_tipo!=null && r_tipo.contentEquals("MP")) content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop.addComponent(panel,1,0);
    }
    
    private void cargarResumen()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblPrevision= null;
    	Label lblProduccion= null;
    	Label lblPlanificacion= null;
    	Label lblVenta= null;
    	Label lblVentaMedia= null;
    	Label lblCobertura= null;

    	Button lblValorPrevision= null;
    	Button lblValorProduccion= null;
    	Button lblValorPlanificacion= null;
    	Button lblValorVenta= null;
    	Button lblValorVentaMedia= null;
    	Button lblValorCobertura= null;
    	    	
    	Panel panel = new Panel("RESUMEN ANUAL " + txtEjercicio.getValue());
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin1 = new HorizontalLayout();
    	lin1.setSpacing(true);
    	
    	lblPrevision= new Label();
    	lblPrevision.setValue("Previsión Emb : ");
    	lblPrevision.setWidth(anchoLabelTitulo);    	
    	lblPrevision.addStyleName("lblTitulo");
    	
    	lblValorPrevision= new Button();
    	lblValorPrevision.setWidth(anchoLabelDatos);
    	
    	if (this.txtEjercicio.getValue().contentEquals(RutinasFechas.añoActualYYYY()))
    	{
    		lblValorPrevision.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pree")));
    	}
    	else
    	{
    		lblValorPrevision.setCaption(RutinasNumericas.formatearDouble(new Double(0)));
    	}
    	lblValorPrevision.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPrevision.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPrevision.addStyleName("lblDatos");

    	if (!lblValorPrevision.getCaption().contentEquals("0,00"))
    	{
	    	lblValorPrevision.addClickListener(new ClickListener() {
	    		
	    		@Override
	    		public void buttonClick(ClickEvent event) {
	    			pantallaPrevisionEmbotellados();
	    			
	    		}
	    	});
    	}
    	
    	lin.addComponent(lblPrevision);
    	lin.addComponent(lblValorPrevision);
    	lin.setComponentAlignment(lblPrevision,Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorPrevision, Alignment.MIDDLE_RIGHT);

    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion : ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),false);
				
			}
		});

    	lin1.addComponent(lblProduccion);
    	lin1.addComponent(lblValorProduccion);
    	lin1.setComponentAlignment(lblProduccion,Alignment.MIDDLE_LEFT);
    	lin1.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblPlanificacion= new Label();
    	lblPlanificacion.setValue("Planificado: ");
    	lblPlanificacion.setWidth(anchoLabelTitulo);    	
    	lblPlanificacion.addStyleName("lblTitulo");

    	lblValorPlanificacion= new Button();
    	lblValorPlanificacion.setWidth(anchoLabelDatos);
    	lblValorPlanificacion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan")));
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPlanificacion.addStyleName("lblDatos");
		lblValorPlanificacion.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaProgramacion(true, new Integer(txtEjercicio.getValue().toString()));
					
				}
			});
    	lin2.addComponent(lblPlanificacion);
    	lin2.addComponent(lblValorPlanificacion);
    	lin2.setComponentAlignment(lblPlanificacion,Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorPlanificacion, Alignment.MIDDLE_RIGHT);
    	
    	lblVenta= new Label();
    	lblVenta.setValue("Vendido: ");
    	lblVenta.setWidth(anchoLabelTitulo);    	
    	lblVenta.addStyleName("lblTitulo");

    	lblValorVenta= new Button();
    	lblValorVenta.setWidth(anchoLabelDatos);
    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventas")));
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVenta.addStyleName("lblDatos");
		lblValorVenta.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString()));
				
			}
		});
    	
    	lin3.addComponent(lblVenta);
    	lin3.addComponent(lblValorVenta);
    	lin3.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);
    	
    	lblVentaMedia= new Label();
    	lblVentaMedia.setValue("VentaMedia: ");
    	lblVentaMedia.setWidth(anchoLabelTitulo);    	
    	lblVentaMedia.addStyleName("lblTitulo");

    	lblValorVentaMedia= new Button();
    	lblValorVentaMedia.setWidth(anchoLabelDatos);
    	lblValorVentaMedia.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasAvg")));
    	lblValorVentaMedia.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentaMedia.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentaMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentaMedia);
    	lin4.addComponent(lblValorVentaMedia);
    	lin4.setComponentAlignment(lblVentaMedia,Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorVentaMedia, Alignment.MIDDLE_RIGHT);
    	
    	lblCobertura= new Label();
    	lblCobertura.setValue("Cobertura (Mes): ");
    	lblCobertura.setWidth(anchoLabelTitulo);    	
    	lblCobertura.addStyleName("lblTitulo");

    	lblValorCobertura= new Button();
    	lblValorCobertura.setWidth(anchoLabelDatos);
    	
    	
    	Double ex = new Double(0);
    	Double pdte = new Double(0);
    	Double prep = new Double(0);
    	Double prog = new Double(0);
    	Double mediaMes = new Double(0);
    	
    	if (this.mapeo.getHashValores().get("0.stock")!=null) ex = this.mapeo.getHashValores().get("0.stock");
    	if (this.mapeo.getHashValores().get("0.pdteServir")!=null) pdte = this.mapeo.getHashValores().get("0.pdteServir");
    	if (this.mapeo.getHashValores().get("0.prep")!=null) prep = this.mapeo.getHashValores().get("0.prep");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.ventasAvg")!=null) mediaMes = this.mapeo.getHashValores().get("0.ventasAvg");
    	
    	Double dispo = ex-pdte+prep+prog;
    	
    	lblValorCobertura.setCaption(RutinasNumericas.formatearDouble(dispo/mediaMes));
    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorCobertura.addStyleName("lblDatosBlanco");

    	lin5.addComponent(lblCobertura);
    	lin5.addComponent(lblValorCobertura);
    	lin5.setComponentAlignment(lblCobertura,Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorCobertura, Alignment.MIDDLE_RIGHT);

    	content.addComponent(lin);
    	content.addComponent(lin1);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop2.addComponent(panel,0,0);
    }
    
    private void cargarDatosPlanificacion()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "130px";
    	
    	Label lblStockMinimo= null;
    	Label lblProduccion= null;
    	Label lblPaletizacion= null;
    	Label lblVelocidad= null;
    	Label lblFecha= null;
    	Label lblStockJaulon= null;
    	
    	Button lblValorStockMinimo= null;
    	Button lblValorProduccion= null;
    	Button lblValorPaletizacion= null;
    	Button lblValorVelocidad= null;
    	Button lblValorFecha= null;
    	Button lblValorStockJaulon= null;
    	
    	Button btnBiblia= null;
    	Button btnEscandallo= null;
    	Button btnIncidencias= null;
    	Button btnProgramar= null;
    	
    	Panel panel = new Panel("DATOS PARA PLANIFICACION");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);

    	Double ex = new Double(0);
    	Double pdte = new Double(0);
    	Double prep = new Double(0);
    	Double prog = new Double(0);
    	Double mediaMes = new Double(0);
    	Double minStock = new Double(0);
    	Double jaulon = new Double(0);
    	
    	if (this.mapeo.getHashValores().get("0.stock")!=null) ex = this.mapeo.getHashValores().get("0.stock");
    	if (this.mapeo.getHashValores().get("0.pdteServir")!=null) pdte = this.mapeo.getHashValores().get("0.pdteServir");
    	if (this.mapeo.getHashValores().get("0.prep")!=null) prep = this.mapeo.getHashValores().get("0.prep");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.ventasAvg")!=null) mediaMes = this.mapeo.getHashValores().get("0.ventasAvg");
    	if (this.mapeo.getHashValores().get("0.minStock")!=null) minStock = this.mapeo.getHashValores().get("0.minStock");
    	if (this.mapeo.getHashValores().get("0.stockJaulon")!=null) jaulon = this.mapeo.getHashValores().get("0.stockJaulon");

    	Double dispo = ex-pdte+prep+prog;
    	minStock = this.mapeo.getHashValores().get("0.minStock")*mediaMes;

    			
    	lblStockMinimo= new Label();
    	lblStockMinimo.setValue("StockMinimo (min): ");
    	lblStockMinimo.setWidth(anchoLabelTitulo);    	
    	lblStockMinimo.addStyleName("lblTitulo");

    	lblValorStockMinimo= new Button();
    	lblValorStockMinimo.setWidth(anchoLabelDatos);
    	lblValorStockMinimo.setCaption(RutinasNumericas.formatearDouble(minStock));
    	lblValorStockMinimo.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorStockMinimo.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorStockMinimo.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblStockMinimo);
    	lin.addComponent(lblValorStockMinimo);
    	lin.setComponentAlignment(lblStockMinimo,Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorStockMinimo, Alignment.MIDDLE_RIGHT);
    	    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion (min): ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.minprod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatosBlanco");
    	
    	lin2.addComponent(lblProduccion);
    	lin2.addComponent(lblValorProduccion);
    	lin2.setComponentAlignment(lblProduccion,Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblPaletizacion= new Label();
    	lblPaletizacion.setValue("Paletizacion: ");
    	lblPaletizacion.setWidth(anchoLabelTitulo);    	
    	lblPaletizacion.addStyleName("lblTitulo");

    	lblValorPaletizacion= new Button();
    	lblValorPaletizacion.setWidth(anchoLabelDatos);
    	lblValorPaletizacion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.palet")));
    	lblValorPaletizacion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPaletizacion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPaletizacion.addStyleName("lblDatosBlanco");

    	lin3.addComponent(lblPaletizacion);
    	lin3.addComponent(lblValorPaletizacion);
    	lin3.setComponentAlignment(lblPaletizacion,Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorPaletizacion, Alignment.MIDDLE_RIGHT);
    	
    	String fecha = "";    	
    	if (minStock==0 && pdte==0)
    	{
    		fecha = "s/f";
    	}
    	else if (dispo<minStock )
    	{
    		fecha = RutinasFechas.fechaActual();
    	}
    	else
    	{
    		Double dias_margen = (dispo-minStock)/mediaMes *30;
    		try 
    		{
				fecha = RutinasFechas.sumarDiasFecha(RutinasFechas.fechaActual(), dias_margen.intValue());
			} 
    		catch (Exception e) 
    		{
				e.printStackTrace();
			}
    	}
    	
    	lblFecha= new Label();
    	lblFecha.setValue("Próxima Prod.: ");
    	lblFecha.setWidth(anchoLabelTitulo);    	
    	lblFecha.addStyleName("lblTitulo");

    	lblValorFecha= new Button();
    	lblValorFecha.setWidth(anchoLabelDatos);
    	lblValorFecha.setCaption(fecha);
    	lblValorFecha.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorFecha.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorFecha.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblFecha);
    	lin4.addComponent(lblValorFecha);
    	lin4.setComponentAlignment(lblFecha,Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorFecha, Alignment.MIDDLE_RIGHT);
    	
//    	
//    	lblCobertura= new Label();
//    	lblCobertura.setValue("Cobertura (Mes): ");
//    	lblCobertura.setWidth(anchoLabelTitulo);    	
//    	lblCobertura.addStyleName("lblTitulo");
//
//    	lblValorCobertura= new Button();
//    	lblValorCobertura.setWidth(anchoLabelDatos);
//    	
//    	lblValorCobertura.setCaption(RutinasNumericas.formatearDouble(dispo/mediaMes));
//    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorCobertura.addStyleName("lblDatos");
//
//    	lin5.addComponent(lblCobertura);
//    	lin5.addComponent(lblValorCobertura);
//    	lin5.setComponentAlignment(lblCobertura,Alignment.MIDDLE_LEFT);
//    	lin5.setComponentAlignment(lblValorCobertura, Alignment.MIDDLE_RIGHT);

    	lblStockJaulon= new Label();
    	lblStockJaulon.setValue("Stock Jaulon: ");
    	lblStockJaulon.setWidth(anchoLabelTitulo);    	
    	lblStockJaulon.addStyleName("lblTitulo");

    	lblValorStockJaulon= new Button();
    	lblValorStockJaulon.setWidth(anchoLabelDatos);
    	lblValorStockJaulon.setCaption(RutinasNumericas.formatearDouble(jaulon));
    	lblValorStockJaulon.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorStockJaulon.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorStockJaulon.addStyleName("lblDatos");
    	
    	lblValorStockJaulon.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
				String codJaulon = ces.recuperarComponente(articuloPadre+"-1", "0102");
				
        		pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + codJaulon, codJaulon, null);
    			getUI().addWindow(vt);
			}
		});
    	
    	lin6.addComponent(lblStockJaulon);
    	lin6.addComponent(lblValorStockJaulon);
    	lin6.setComponentAlignment(lblStockJaulon,Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorStockJaulon, Alignment.MIDDLE_RIGHT);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop2.addComponent(panel,2,0);
    }
    
    private void cargarAcciones()
    {
    	String anchoLabelTitulo = "275px";
    	
    	Label lblBiblia= null;
    	Label lblEscandallo= null;
    	Label lblIncidencias= null;
    	Label lblProgramar= null;
    	
    	Button btnBiblia= null;
    	Button btnEscandallo= null;
    	Button btnIncidencias= null;
    	Button btnProgramar= null;
    	
    	Panel panel = new Panel("ACCIONES");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);

    	
    	lblBiblia= new Label();
    	lblBiblia.setValue("Ficha de Producto ");
    	lblBiblia.setWidth(anchoLabelTitulo);    	
    	lblBiblia.addStyleName("lblTitulo");

    	btnBiblia = new Button();
    	btnBiblia.setDescription("Ficha de Producto");
    	btnBiblia.setIcon(FontAwesome.BOOK);
    	btnBiblia.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnBiblia.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
        		String host = UI.getCurrent().getPage().getLocation().getHost().trim();
        		if (host.equals("localhost")) host = "192.168.6.6";
        		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + articuloPadre;
        		getUI().getPage().open(url, "_blank");
				
			}
		});
    	lin.addComponent(btnBiblia);
    	lin.addComponent(lblBiblia);
    	lin.setComponentAlignment(btnBiblia, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblBiblia, Alignment.MIDDLE_LEFT);

    	lblEscandallo= new Label();
    	lblEscandallo.setValue("Escandallo de Producto ");
    	lblEscandallo.setWidth(anchoLabelTitulo);    	
    	lblEscandallo.addStyleName("lblTitulo");
    	
    	btnEscandallo = new Button();
    	btnEscandallo.setIcon(FontAwesome.INDUSTRY);
    	btnEscandallo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnEscandallo.setDescription("Escandallo Articulo");
    	btnEscandallo.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaAyudaProduccion vt = new pantallaAyudaProduccion("Situación del Articulo " + articuloPadre, 0, articuloPadre);
//				pantallaNotasArticulo vt = new pantallaNotasArticulo("Notas del Articulo " + articuloPadre, articuloPadre);
				getUI().addWindow(vt);				
			}
		});
    	lin2.addComponent(btnEscandallo);
    	lin2.addComponent(lblEscandallo);
    	lin2.setComponentAlignment(btnEscandallo, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblEscandallo, Alignment.MIDDLE_LEFT);
    	
    	
    	lblIncidencias= new Label();
    	lblIncidencias.setValue("Incidencias Calidad de Producto ");
    	lblIncidencias.setWidth(anchoLabelTitulo);    	
    	lblIncidencias.addStyleName("lblTitulo");

    	btnIncidencias = new Button();
    	btnIncidencias.setDescription("Incidencias calidad");
    	btnIncidencias.setIcon(FontAwesome.EXCLAMATION_CIRCLE);
    	btnIncidencias.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
    	boolean hayObs = cps.hayObservacionesCalidad(articuloPadre);
    	if (hayObs)
    	{
    		btnIncidencias.setEnabled(true);
    		lblIncidencias.setEnabled(true);
	    	btnIncidencias.addClickListener(new ClickListener() {
	    		
	    		@Override
	    		public void buttonClick(ClickEvent event) {
	    			pantallaObservacionesProgramacion vt = new pantallaObservacionesProgramacion(articuloPadre);
	    			getUI().addWindow(vt);	  
	    		}
	    	});
    	}
    	else
    	{
    		btnIncidencias.setEnabled(false);
    		lblIncidencias.setEnabled(false);
    	}
    	lin3.addComponent(btnIncidencias);
    	lin3.addComponent(lblIncidencias);
    	lin3.setComponentAlignment(btnIncidencias, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblIncidencias, Alignment.MIDDLE_LEFT);

    	lblProgramar= new Label();
    	lblProgramar.setValue("Programar producción ");
    	lblProgramar.setWidth(anchoLabelTitulo);    	
    	lblProgramar.addStyleName("lblTitulo");
    	
    	btnProgramar= new Button();
    	btnProgramar.setDescription("Programar articulo");
    	btnProgramar.setIcon(FontAwesome.CALENDAR_CHECK_O);
    	btnProgramar.addStyleName(ValoTheme.BUTTON_DANGER);
//    	btnProgramar.addStyleName(ValoTheme.BUTTON_TINY);
    	
    	if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))
    	{
    		btnProgramar.setEnabled(true);
    		lblProgramar.setEnabled(true);
	    	btnProgramar.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					MapeoSituacionEmbotellado map = new MapeoSituacionEmbotellado();
					map.setArticulo(articuloPadre);
					map.setDescripcion(descripcionArticuloPadre);
					map.setStock_real(0);
					
					PeticionAsignacionProgramacion vt= new PeticionAsignacionProgramacion(null, map, "Programar Cantidad de " + articuloPadre);
		    		getUI().addWindow(vt);
				}
			});
    	}
    	else
    	{
    		btnProgramar.setEnabled(false);
    		lblProgramar.setEnabled(false);
    	}
    	lin4.addComponent(btnProgramar);
    	lin4.addComponent(lblProgramar);
    	lin4.setComponentAlignment(btnProgramar, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblProgramar, Alignment.MIDDLE_LEFT);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(true);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle2.addComponent(panel,0,0);
    }
    
    private void cargarAccionesHijo(boolean r_escandallo)
    {
    	String anchoLabelTitulo = "275px";
    	
    	Label lblBiblia= null;
    	Label lblEscandallo= null;
    	Label lblIncidencias= null;
    	Label lblProgramar= null;
    	
    	Button btnBiblia= null;
    	Button btnEscandallo= null;
    	Button btnIncidencias= null;
    	Button btnProgramar= null;
    	
    	Panel panel = new Panel("ACCIONES Sobre " + articuloSeleccionado);
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	
    	lblBiblia= new Label();
    	lblBiblia.setValue("Ficha de Producto ");
    	lblBiblia.setWidth(anchoLabelTitulo);    	
    	lblBiblia.addStyleName("lblTitulo");

    	btnBiblia = new Button();
    	btnBiblia.setDescription("Ficha de Producto");
    	btnBiblia.setIcon(FontAwesome.BOOK);
    	btnBiblia.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	if (!r_escandallo)
    	{
    		
    		btnBiblia.setEnabled(r_escandallo);
    	}
    	else
    	{
    		btnBiblia.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
	        		String host = UI.getCurrent().getPage().getLocation().getHost().trim();
	        		if (host.equals("localhost")) host = "192.168.6.6";
	        		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + articuloSeleccionado;
	        		getUI().getPage().open(url, "_blank");
					
				}
			});
    	}
    	lin.addComponent(btnBiblia);
    	lin.addComponent(lblBiblia);
    	lin.setComponentAlignment(btnBiblia, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblBiblia, Alignment.MIDDLE_LEFT);

    	lblEscandallo= new Label();
    	lblEscandallo.setValue("Escandallo de Producto ");
    	lblEscandallo.setWidth(anchoLabelTitulo);    	
    	lblEscandallo.addStyleName("lblTitulo");
    	
    	btnEscandallo = new Button();
    	btnEscandallo.setIcon(FontAwesome.INDUSTRY);
    	btnEscandallo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnEscandallo.setDescription("Escandallo Articulo");
    	if (!r_escandallo)
    	{
    		btnEscandallo.setEnabled(r_escandallo);
    	}
    	else
    	{
	    	btnEscandallo.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaAyudaProduccion vt = new pantallaAyudaProduccion("Situación del Articulo " + articuloSeleccionado, 0, articuloSeleccionado);
	    			getUI().addWindow(vt);				
				}
			});
    	}
    	lin2.addComponent(btnEscandallo);
    	lin2.addComponent(lblEscandallo);
    	lin2.setComponentAlignment(btnEscandallo, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblEscandallo, Alignment.MIDDLE_LEFT);
    	
    	lblIncidencias= new Label();
    	lblIncidencias.setValue("Incidencias Calidad de Producto ");
    	lblIncidencias.setWidth(anchoLabelTitulo);    	
    	lblIncidencias.addStyleName("lblTitulo");

    	btnIncidencias = new Button();
    	btnIncidencias.setDescription("Incidencias calidad");
    	btnIncidencias.setIcon(FontAwesome.EXCLAMATION_CIRCLE);
    	btnIncidencias.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	if (!r_escandallo)
    	{
    		btnIncidencias.setEnabled(r_escandallo);
    		lblIncidencias.setEnabled(r_escandallo);
    	}
    	else
    	{
        	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
        	boolean hayObs = cps.hayObservacionesCalidad(articuloSeleccionado);
        	if (hayObs)
        	{
        		btnIncidencias.setEnabled(true);
        		lblIncidencias.setEnabled(true);
    	    	btnIncidencias.addClickListener(new ClickListener() {
    	    		
    	    		@Override
    	    		public void buttonClick(ClickEvent event) {
    	    			pantallaObservacionesProgramacion vt = new pantallaObservacionesProgramacion(articuloSeleccionado);
    	    			getUI().addWindow(vt);	  
    	    		}
    	    	});
        	}
        	else
        	{
        		btnIncidencias.setEnabled(false);
        		lblIncidencias.setEnabled(false);
        	}
    	}
    	lin3.addComponent(btnIncidencias);
    	lin3.addComponent(lblIncidencias);
    	lin3.setComponentAlignment(btnIncidencias, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblIncidencias, Alignment.MIDDLE_LEFT);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(true);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop.addComponent(panel,3,0);
    }
    
    private void pantallaProduccion(Integer r_ejercicio, boolean r_porMeses)
    {
    	pantallaLineasProduccion vt = null;
    	String art = null;
    	
    	if (this.articuloPadre!=null)
    	{
    		art = this.articuloPadre;
    		vt = new pantallaLineasProduccion(null,"Embotelladora", r_ejercicio, art, r_porMeses);
    	}
    	else
    	{
    		art= this.articuloSeleccionado;
    		vt = new pantallaLineasProduccion(null,"Embotelladora", r_ejercicio, art, r_porMeses);
    	}
    	getUI().addWindow(vt);
    }

    private void pantallaProduccionMPPS(String r_tipo, Integer r_ejercicio, boolean r_porMeses)
    {
    	pantallaLineasProduccion vt = null;
    	String art = null;
    	
    	art= this.articuloSeleccionado;
    	vt = new pantallaLineasProduccion(r_tipo,"Embotelladora", r_ejercicio, art, r_porMeses);
    	getUI().addWindow(vt);
    }

    private void pantallaPrevisionEmbotellados()
    {
    	PantallaPrevisionEmbotellados vt = null;
    	String art = null;
    	
    	art= this.articuloPadre;
    	vt = new PantallaPrevisionEmbotellados(art);
    	getUI().addWindow(vt);
    }

    private void pantallaProgramacion(boolean r_global, Integer r_ejercicio)
    {
    	if (articuloPadre.startsWith("0111") || articuloPadre.startsWith("0103"))
    	{
    		pantallaLineasProgramacionesEnvasadora vt = new pantallaLineasProgramacionesEnvasadora(this.articuloPadre, r_ejercicio,false);
    		getUI().addWindow(vt);
    		
    	}
    	else
    	{
    		pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(this.articuloPadre, r_ejercicio, "Prog", r_global);
    		getUI().addWindow(vt);
    	}
    }
    private void pantallaVentas(Integer r_ejercicio)
    {
    	pantallaLineasAlbaranesVentas vt = new pantallaLineasAlbaranesVentas(r_ejercicio, this.articuloPadre);
    	getUI().addWindow(vt);
    }
    
    private void pantallaVentasMPPS()
    {
    	pantallaVentas vt = new pantallaVentas(new Integer(this.txtEjercicio.getValue().toString()), this.articuloSeleccionado);
    	getUI().addWindow(vt);
    }

    private void cargarResumenStock()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	String traerVentas = null;
    	
    	Label lblStock= null;
    	Label lblPedidos= null;
    	Label lblPreparado= null;
    	Label lblProgramado= null;
    	Label lblNecesidad= null;
    	Label lblPrevisionRestante= null;
    	
    	Button lblValorStock= null;
    	Button lblValorPedidos= null;
    	Button lblValorPreparado= null;
    	Button lblValorProgramado= null;
    	Button lblValorNecesidad= null;
    	Button lblValorPrevisionRestante= null;
    	
    	traerVentas="0";
    	Panel panel = new Panel("SITUACION ACTUAL");
    	
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	
    	Double ex = new Double(0);
    	Double pdte = new Double(0);
    	Double prep = new Double(0);
    	Double prog = new Double(0);
    	final Double st = ex+prep;
    	
    	
    	if (this.mapeo.getHashValores().get("0.stock")!=null) ex = this.mapeo.getHashValores().get("0.stock");
    	if (this.mapeo.getHashValores().get("0.pdteServir")!=null) pdte = this.mapeo.getHashValores().get("0.pdteServir");
    	if (this.mapeo.getHashValores().get("0.prep")!=null) prep = this.mapeo.getHashValores().get("0.prep");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");

    	Double dispo = ex-pdte+prep+prog;
    	
    	lblStock= new Label();
    	lblStock.setValue("Stock: ");
    	lblStock.setWidth(anchoLabelTitulo);    	
    	lblStock.addStyleName("lblTitulo");

    	lblValorStock= new Button();
    	lblValorStock.setWidth(anchoLabelDatos);
    	lblValorStock.setCaption(RutinasNumericas.formatearDouble(ex));
    	lblValorStock.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorStock.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorStock.addStyleName("lblDatos");
    	lblValorStock.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				String fecha=null;
				if (articuloPadre.startsWith("0103")) fecha = RutinasFechas.fechaActual();
        		pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + articuloPadre, articuloPadre,fecha);
    			getUI().addWindow(vt);
			}
		});
			
    	lin.addComponent(lblStock);
    	lin.addComponent(lblValorStock);
    	lin.setComponentAlignment(lblStock, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorStock, Alignment.MIDDLE_RIGHT);
    	
    	lblPedidos= new Label();
    	lblPedidos.setValue("Pdte. Servir: ");
    	lblPedidos.setWidth(anchoLabelTitulo);    	
    	lblPedidos.addStyleName("lblTitulo");

    	lblValorPedidos= new Button();
    	lblValorPedidos.setWidth(anchoLabelDatos);
    	lblValorPedidos.setCaption(RutinasNumericas.formatearDouble(pdte));
    	lblValorPedidos.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPedidos.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPedidos.addStyleName("lblDatos");
    	lblValorPedidos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(articuloPadre, (new Double(st)).intValue(), "Lineas Pedidos Venta del Articulo " + articuloPadre );
				getUI().addWindow(vt);
				
			}
		});
			
    	lin2.addComponent(lblPedidos);
    	lin2.addComponent(lblValorPedidos);
    	lin2.setComponentAlignment(lblPedidos, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorPedidos, Alignment.MIDDLE_RIGHT);
	    	
    	lblPreparado= new Label();
    	lblPreparado.setValue("Preparadas: ");
    	lblPreparado.setWidth(anchoLabelTitulo);    	
    	lblPreparado.addStyleName("lblTitulo");

    	lblValorPreparado= new Button();
    	lblValorPreparado.setWidth(anchoLabelDatos);
    	lblValorPreparado.setCaption(RutinasNumericas.formatearDouble(prep));
    	lblValorPreparado.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPreparado.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPreparado.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblPreparado);
    	lin3.addComponent(lblValorPreparado);
    	lin3.setComponentAlignment(lblPreparado, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorPreparado, Alignment.MIDDLE_RIGHT);
	    	
    	lblProgramado= new Label();
    	lblProgramado.setValue("Programadas: ");
    	lblProgramado.setWidth(anchoLabelTitulo);    	
    	lblProgramado.addStyleName("lblTitulo");

    	lblValorProgramado= new Button();
    	lblValorProgramado.setWidth(anchoLabelDatos);
    	lblValorProgramado.setCaption(RutinasNumericas.formatearDouble(prog));
    	lblValorProgramado.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProgramado.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProgramado.addStyleName("lblDatos");
    	
    	lblValorProgramado.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(false,new Integer(txtEjercicio.getValue().toString()));
				
			}
		});
    	
    	lin4.addComponent(lblProgramado);
    	lin4.addComponent(lblValorProgramado);
    	lin4.setComponentAlignment(lblProgramado, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorProgramado, Alignment.MIDDLE_RIGHT);

    	lblNecesidad= new Label();
    	lblNecesidad.setValue("Disponible: ");
    	lblNecesidad.setWidth(anchoLabelTitulo);    	
    	lblNecesidad.addStyleName("lblTitulo");

    	lblValorNecesidad= new Button();
    	lblValorNecesidad.setWidth(anchoLabelDatos);
    	lblValorNecesidad.setCaption(RutinasNumericas.formatearDouble(dispo));
    	lblValorNecesidad.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorNecesidad.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorNecesidad.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblNecesidad);
    	lin5.addComponent(lblValorNecesidad);
    	lin5.setComponentAlignment(lblNecesidad, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorNecesidad, Alignment.MIDDLE_RIGHT);
		    	
    	lblPrevisionRestante= new Label();
    	lblPrevisionRestante.setValue("Resto Previsto: ");
    	lblPrevisionRestante.setWidth(anchoLabelTitulo);    	
    	lblPrevisionRestante.addStyleName("lblTitulo");
    	
    	lblValorPrevisionRestante= new Button();
    	lblValorPrevisionRestante.setWidth(anchoLabelDatos);
    	
    	if (this.txtEjercicio.getValue().contentEquals(RutinasFechas.añoActualYYYY()))
    	{
    		lblValorPrevisionRestante.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prer")));
    	}
    	else
    	{
    		lblValorPrevisionRestante.setCaption(RutinasNumericas.formatearDouble(new Double(0)));
    	}

    	lblValorPrevisionRestante.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPrevisionRestante.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPrevisionRestante.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblPrevisionRestante);
    	lin6.addComponent(lblValorPrevisionRestante);
    	lin6.setComponentAlignment(lblPrevisionRestante, Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorPrevisionRestante, Alignment.MIDDLE_RIGHT);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralTop2.addComponent(panel,1,0);
    }
    
    
    private void cargarResumenProduccion()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Button lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Button lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Planificacion " + String.valueOf(new Integer(txtEjercicio.getValue())));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),false);
				
			}
		});
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Button();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")/new Integer(RutinasFechas.mesActualMM())));
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorMediaMes.addStyleName("lblDatos");
    	lblValorMediaMes.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),true);
				
			}
		});

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Veces Planificadas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.vecesPlan")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
    	
    	lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(true,new Integer(txtEjercicio.getValue().toString()));
				
			}
		});
    	
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);
	    	
    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Embotelladas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");

    	lblValorEmbotelladas= new Button();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.embPlan")));
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);

    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Etiquetadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");

    	lblValorEtiquetadas= new Button();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.etiPlan")));
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Retrabajadas: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");

    	lblValorRetrabajadas= new Button();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.retPlan")));
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,0,0);
    }
    
    private void cargarResumenProduccionAnterior()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Button lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Button lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Planificacion "+ String.valueOf(new Integer(txtEjercicio.getValue())-1));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-1, false);
				
			}
		});
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Button();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.prod")/12));
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorMediaMes.addStyleName("lblDatos");
			
    	lblValorMediaMes.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-1,true);
				
			}
		});

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Veces Planificadas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.vecesPlan")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
    	
    	lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(true,new Integer(txtEjercicio.getValue().toString())-1);
				
			}
		});

    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);
	    	
    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Embotelladas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");

    	lblValorEmbotelladas= new Button();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.embPlan")));
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);

    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Etiquetadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");

    	lblValorEtiquetadas= new Button();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.etiPlan")));
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Retrabajadas: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");

    	lblValorRetrabajadas= new Button();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.retPlan")));
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,1,0);
    }
    
    private void cargarResumenProduccionAnterior2()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Button lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Button lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Planificacion "+ String.valueOf(new Integer(txtEjercicio.getValue())-2));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2, false);
				
			}
		});	
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Button();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.prod")/12));
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorMediaMes.addStyleName("lblDatos");
			
    	lblValorMediaMes.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2,true);
				
			}
		});

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Veces Planificadas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.vecesPlan")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
		lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(true,new Integer(txtEjercicio.getValue().toString())-2);
				
			}
		});
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);
	    	
    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Embotelladas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");

    	lblValorEmbotelladas= new Button();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.embPlan")));
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);

    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Etiquetadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");

    	lblValorEtiquetadas= new Button();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.etiPlan")));
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Retrabajadas: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");

    	lblValorRetrabajadas= new Button();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.retPlan")));
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,2,0);
    }
    
    private void cargarResumenProduccionAnterior3()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Button lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Button lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Planificacion "+ String.valueOf(new Integer(txtEjercicio.getValue())-3));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2, false);
				
			}
		});	
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Button();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.prod")/12));
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorMediaMes.addStyleName("lblDatos");
			
    	lblValorMediaMes.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2,true);
				
			}
		});

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Veces Planificadas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.vecesPlan")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
		lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(true,new Integer(txtEjercicio.getValue().toString())-2);
				
			}
		});
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);
	    	
    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Embotelladas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");

    	lblValorEmbotelladas= new Button();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.embPlan")));
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);

    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Etiquetadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");

    	lblValorEtiquetadas= new Button();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.etiPlan")));
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Retrabajadas: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");

    	lblValorRetrabajadas= new Button();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.retPlan")));
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_LEFT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,3,0);
    }
    
    private void cargarResumenVentas()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Label lblVentasClientes = null;
    	Label lblValorVentasClientes = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN VENTAS " + txtEjercicio.getValue());

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");

    	lblValorVentasUnidades= new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventas")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString()));
				
			}
		});

    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PVM: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pvpVentas")));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
//    	lblVentasClientes= new Label();
//    	lblVentasClientes.setValue("Clientes: ");
//    	lblVentasClientes.setWidth(anchoLabelTitulo);    	
//    	lblVentasClientes.addStyleName("lblTitulo");
//
//    	lblValorVentasClientes = new Label();
//    	lblValorVentasClientes.setWidth(anchoLabelDatos);
//    	lblValorVentasClientes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.clientes")));
//    	lblValorVentasClientes.addStyleName("lblDatosBlanco");
//
//    	lin5.addComponent(lblVentasClientes);
//    	lin5.addComponent(lblValorVentasClientes);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,0,0);
    }
    
    private void cargarResumenVentasAnterior()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	Label lblVentasClientes= null;
    	Label lblValorVentasClientes = null;
    	
    	
    	Panel panel = new Panel("RESUMEN VENTAS " + String.valueOf(new Integer(txtEjercicio.getValue())-1));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.ventasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");


    	lblValorVentasUnidades= new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.ventas")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-1);
				
			}
		});

    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PVM: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.pvpVentas")));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.ventasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
//    	lblVentasClientes= new Label();
//    	lblVentasClientes.setValue("Clientes: ");
//    	lblVentasClientes.setWidth(anchoLabelTitulo);    	
//    	lblVentasClientes.addStyleName("lblTitulo");
//
//    	lblValorVentasClientes = new Label();
//    	lblValorVentasClientes.setWidth(anchoLabelDatos);
//    	lblValorVentasClientes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.clientes")));
//    	lblValorVentasClientes.addStyleName("lblDatosBlanco");
//
//    	lin5.addComponent(lblVentasClientes);
//    	lin5.addComponent(lblValorVentasClientes);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,1,0);
    }
    private void cargarResumenVentasAnterior2()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN VENTAS " + String.valueOf(new Integer(txtEjercicio.getValue())-2));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.ventasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");


    	lblValorVentasUnidades= new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.ventas")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-2);
				
			}
		});

    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PVM: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.pvpVentas")));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.ventasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,2,0);
    }
    private void cargarResumenVentasAnterior3()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN VENTAS " + String.valueOf(new Integer(txtEjercicio.getValue())-3));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.ventasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");


    	lblValorVentasUnidades= new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.ventas")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-3);
				
			}
		});
    	
    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PVM: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.pvpVentas")));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.ventasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,3,0);
    }
    
    private void evolucionVentas()
	{
		
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		HashMap<String, Double> hashVentasAnterior2 = null;
		
		consultaDashboardArticuloServer cdes = new consultaDashboardArticuloServer(CurrentUser.get());
		hashVentas = cdes.recuperarMovimientosVentasMesesEjercicio(new Integer(this.txtEjercicio.getValue()),this.articuloPadre,"");
	    hashVentasAnterior = cdes.recuperarMovimientosVentasMesesEjercicio(new Integer(this.txtEjercicio.getValue())-1,this.articuloPadre,"");
	    hashVentasAnterior2 = cdes.recuperarMovimientosVentasMesesEjercicio(new Integer(this.txtEjercicio.getValue())-2,this.articuloPadre,"");
	    String tituloY = null;
	    
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Ventas", "Meses", tituloY, hashVentas, hashVentasAnterior, hashVentasAnterior2, txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), String.valueOf(new Integer(this.txtEjercicio.getValue())-2), "510px", "250px",0,0,0);
	    
	    centralMiddle1.addComponent(wrapper,0,0);
	    
	}

    private void evolucionVentasAcumuladas()
	{
		
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		HashMap<String, Double> hashVentasAnterior2 = null;
		
		consultaDashboardArticuloServer cdes = new consultaDashboardArticuloServer(CurrentUser.get());
		hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()),this.articuloPadre,"","0");
		hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-1,this.articuloPadre,"","53");
		hashVentasAnterior2 = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-2,this.articuloPadre,"","53");
		
	    String tituloY = null;
	    
	    tituloY="Bt. / Semana";
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Ventas Ac.", "Semanas", tituloY, hashVentas, hashVentasAnterior, hashVentasAnterior2, txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), String.valueOf(new Integer(this.txtEjercicio.getValue())-2), "510px", "250px",0,0,0);
	    
	    centralMiddle1.addComponent(wrapper,1,0); 
	    
	}

	
    private void vaciarPantalla()
    {
    	centralTop=null;
        centralMiddle=null;
        centralBottom=null;
        topLayout=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }
    
    public boolean isPrevision() {
    	return prevision;
    }
    
    public void setPrevision(boolean prevision) {
    	this.prevision = prevision;
    }
    
    
    private void comprobarArticulo(String r_articulo)
    {
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	
    	if (r_articulo.startsWith("0101") || r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103"))
    	{
    		tipoArticulo = cas.obtenerTipoArticulo(r_articulo);
    		if (tipoArticulo!=null)
    		{
    			consultaNotasArticulosServer cns = consultaNotasArticulosServer.getInstance(CurrentUser.get());
    			MapeoNotasArticulos mapeoNotas = new MapeoNotasArticulos();
    			mapeoNotas.setArticulo(r_articulo);
    			if (cns.hayNotas(mapeoNotas))
				{
    				pantallaNotasArticulo vt = new pantallaNotasArticulo("Notas del Articulo " + r_articulo, r_articulo);
    				getUI().addWindow(vt);
				}

    			switch (tipoArticulo)
    			{
	    			case "PT":
	    			{
	    				this.articuloPadre=r_articulo.trim();
	    				this.articuloSeleccionado=null;
	    				this.descripcionArticuloSeleccionado = null;
	    				this.descripcionArticuloPadre = cas.obtenerDescripcionArticulo(r_articulo.trim());
	    				panelArticulos.setVisible(false);
	    				break;
	    			}
	    			case "PS":
	    			{
	    				this.articuloSeleccionado=r_articulo.trim();
	    				this.articuloPadre=null;
	    				this.descripcionArticuloSeleccionado = cas.obtenerDescripcionArticulo(r_articulo.trim());
	    				this.descripcionArticuloPadre = null;
	    				panelArticulos.setVisible(true);
	    				break;
	    			}
	    			case "MP":
	    			{
	    				panelArticulos.setVisible(true);
	    				this.articuloSeleccionado=r_articulo.trim();
	    				this.descripcionArticuloSeleccionado = cas.obtenerDescripcionArticulo(r_articulo.trim());
	    				this.articuloPadre=null;
	    				this.descripcionArticuloPadre = null;
	    				break;
	    			}
	    			default:
	    			{
	    				tipoArticulo = "";
	    				panelArticulos.setVisible(true);
	    				this.articuloSeleccionado=r_articulo.toUpperCase().trim();
	    				this.descripcionArticuloSeleccionado = null;;
	    				this.articuloPadre=null;
	    				this.descripcionArticuloPadre = null;
	    			}
    			}
    		}
    		else
    		{
    			tipoArticulo = "";
    			panelArticulos.setVisible(true);
    			this.articuloSeleccionado=r_articulo.toUpperCase().trim();
    			this.descripcionArticuloSeleccionado = null;;
    			this.articuloPadre=null;
    			this.descripcionArticuloPadre = null;
    		}
    	}
    	else if (r_articulo.length()>0)
    	{
    		tipoArticulo = "";
    		panelArticulos.setVisible(true);
    		this.articuloSeleccionado=r_articulo.toUpperCase().trim();
    		this.descripcionArticuloSeleccionado = null;;
    		this.articuloPadre=null;
    		this.descripcionArticuloPadre = null;
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Valor no válido");
    		this.txtArticulo.focus();
    		tipoArticulo=null;
    		this.articuloSeleccionado=null;
    		this.descripcionArticuloSeleccionado = null;
    		panelArticulos.setVisible(false);
    	}
    }
    
    private void resumenVentaPaises()
    {
    	consultaDashboardArticuloServer cds = new consultaDashboardArticuloServer(CurrentUser.get());
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	vector = cds.recuperarVentasPaisEjercicio(new Integer(txtEjercicio.getValue().toString()), this.articuloPadre, "");
    	
    	IndexedContainer container =null;
    	Iterator<MapeoValores> iterator = null;
    	MapeoValores mapeo = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("Pais", String.class, null);
    	container.addContainerProperty("Unidades", Double.class, null);
    	
    	iterator = vector.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoValores) iterator.next();
    		total = total + mapeo.getValorDouble();
    		
    		file.getItemProperty("Pais").setValue(mapeo.getClave());
    		file.getItemProperty("Unidades").setValue(mapeo.getValorDouble());
    	}
    	
    	Object newItemId = container.addItem();
    	Item file = container.getItem(newItemId);
    	
    	file.getItemProperty("Pais").setValue("TOTAL");
    	file.getItemProperty("Unidades").setValue(total);
    	
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Unidades")) {
    				return "Rcell-normal";
    			}
    			return null;
    		}
    	});
    	
    	Panel panel = new Panel("Venta por paises (Uds.)");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralMiddle1.addComponent(panel,2,0);
    	
    }
    private void resumenVentaClientes()
    {
    	consultaDashboardArticuloServer cds = new consultaDashboardArticuloServer(CurrentUser.get());
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	vector = cds.recuperarVentasClientesEjercicio(new Integer(txtEjercicio.getValue().toString()), this.articuloPadre, "");
    	
    	IndexedContainer container =null;
    	Iterator<MapeoValores> iterator = null;
    	MapeoValores mapeo = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("Cliente", String.class, null);
    	container.addContainerProperty("Unidades", Double.class, null);
    	
    	iterator = vector.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoValores) iterator.next();
    		total = total + mapeo.getValorDouble();
    		
    		file.getItemProperty("Cliente").setValue(mapeo.getClave());
    		file.getItemProperty("Unidades").setValue(mapeo.getValorDouble());
    	}
    	
    	Object newItemId = container.addItem();
    	Item file = container.getItem(newItemId);
    	
    	file.getItemProperty("Cliente").setValue("TOTAL");
    	file.getItemProperty("Unidades").setValue(total);
    	
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Unidades")) {
    				return "Rcell-normal";
    			}
    			return null;
    		}
    	});
    	
    	Panel panel = new Panel("Venta por cliente (Uds.)");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralMiddle1.addComponent(panel,2,0);
    	
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}


    private void ejecutarTimer()
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }	
	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco dashboard Embotelladora " + new Date());
    		refrescar();
    	}
    }


    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarListeners();
    }


}
