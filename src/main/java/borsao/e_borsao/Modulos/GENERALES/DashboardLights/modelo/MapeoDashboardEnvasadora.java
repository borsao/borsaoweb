package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDashboardEnvasadora extends MapeoGlobal
{
	private Integer ejercicio;
	private String color;
	private Integer formato;
	private String palet;
	
	private HashMap<String, Double> hashValores = null;
	private double total;

	public MapeoDashboardEnvasadora()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getFormato() {
		return formato;
	}

	public void setFormato(Integer formato) {
		this.formato = formato;
	}

	public String getPalet() {
		return palet;
	}

	public void setPalet(String palet) {
		this.palet = palet;
	}


	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	
}