package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardCompras;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardComprasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaDashboardComprasServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivoLitros = 33750;
	public static Integer produccionObjetivoBotellas = 45000;

	
	private ArrayList<MapeoProgramacionEnvasadora> vectorProgramadoMesa = null;

	public consultaDashboardComprasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardComprasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardComprasServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardCompras recuperarValoresEstadisticosMP(String r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		Double valor = null;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 * 
		 *  Tenemos que tener en cuenta el tipo de articulo
		 *  
		 *  	- cargaremos los datos del articulo que recibimos sin mas
		 *  
		 *  	datos de produccion acumulada de este ejercicio y el anterior
		 *  	datos de compra acumulada de este ejercicio y el anterior
		 *  	datos de compra acumulada en importes de este ejercicio y el anterior
		 *  	datos de proveedores, importes, precio y cantidades
		 *  	datos de venta de PT que contengan esa MP
		 *  	datos de planificacion de PT que contengan esa MP
		 *  
		 *  	datos de stocks
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod)
		 *  	- produccion total programada del periodo						(0.plan)
		 *  
		 *  	- ventas totales de PT del periodo								(0.ventas)
		 *  	- importe ventas totales  de PT del periodo						(0.ventasImp)
		 *  	- compras totales del periodo									(1.compras)
		 *  	- importe compras totales del periodo							(0.comprasImp)
		 *  	- compra media del periodo										(0.compraAvg)
		 *  	- precio compra medio del periodo								(0.pvpCompras)		   
		 *   	
		 *      - stock minimo 													(0.minStock)
		 *      - stock actual													(0.stock)
		 *      - stock pedido													(0.pdteRecibir)
		 *      - stock pedido													(0.pdteServir)
		 *      
		 *      - necesidad														(0.neeed)
		 *            
		 */
		HashMap<String, Double> hashValores = null;
		HashMap<String, Double> vectorDatosStock = null;

		MapeoGeneralDashboardCompras mapeo = null;

		hashValores = new HashMap<String, Double>();
		vectorDatosStock = this.recuperarStock(new Integer(r_ejercicio), r_articulo);

		Double produccion = this.recuperarProduccion(new Integer(r_ejercicio), r_articulo, r_tipoArticulo);
		hashValores.put("0.prod", produccion);

		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-1, r_articulo, r_tipoArticulo);
		hashValores.put("1.prod", produccion);
		
		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-2, r_articulo, r_tipoArticulo);
		hashValores.put("2.prod", produccion);

		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-3, r_articulo, r_tipoArticulo);
		hashValores.put("3.prod", produccion);

			
		Integer meses = 0;
		int diaActual = 0;
		int diaFinMes = 0;
		
		diaActual = new Integer(RutinasFechas.diaActualDD());
		diaFinMes = RutinasFechas.obtenerUltimoDiaMesActual(new Integer(RutinasFechas.añoActualYYYY()), new Integer(RutinasFechas.mesActualMM()));
		
		if (new Integer(r_ejercicio)<new Integer(RutinasFechas.añoActualYYYY()))
			meses= 12; 
		else 
		{
			if (diaActual==diaFinMes)
				meses = new Integer(RutinasFechas.mesActualMM());
			else if (diaActual<diaFinMes)
			{
				meses = new Integer(RutinasFechas.mesActualMM())-1;				
			}
			else
				meses = new Integer(RutinasFechas.mesActualMM());
		}

		Double planificadoVM = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio), "T", r_articulo));
		Double planificadoBt = new Double(this.obtenerProgramado(new Integer(r_ejercicio), "T", r_articulo));
		
		hashValores.put("0.plan", planificadoBt+planificadoVM);
		
		if (meses!=0) hashValores.put("0.avgplan", (planificadoBt+planificadoVM)/meses); else hashValores.put("0.avgplan", (planificadoBt+planificadoVM)/12);
		
		planificadoVM = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-1, "T", r_articulo));
		planificadoBt = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-1, "T", r_articulo));
		
		hashValores.put("1.plan", planificadoBt+planificadoVM);
		hashValores.put("1.avgplan", (planificadoBt+planificadoVM)/12);
		
		Double ventas = this.recuperarUnidadesVentas(new Integer(r_ejercicio),meses, r_articulo);
		if (meses==0)
			hashValores.put("0.ventas", new Double(0));
		else
			hashValores.put("0.ventas", ventas);

		if (meses!=0) hashValores.put("0.ventasAvg", ventas/meses); else hashValores.put("0.ventasAvg", new Double(0));
//		Double ventasImp = this.recuperarImporteVentas(new Integer(r_ejercicio),meses,r_articulo);
//		hashValores.put("0.ventasImp", ventasImp);
//		hashValores.put("0.pvpVentas", ventasImp/ventas);
//		
		Double ventasAnterior = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-1,12,r_articulo);
		hashValores.put("1.ventas", ventasAnterior);
		hashValores.put("1.ventasAvg", ventasAnterior/12);
//		Double ventasImpAnterior = this.recuperarImporteVentas(new Integer(r_ejercicio)-1,12,r_articulo);
//		hashValores.put("1.ventasImp", ventasImpAnterior);
//		hashValores.put("1.pvpVentas", ventasImpAnterior/ventasAnterior);
//		
		Double ventasAnterior2 = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-2,12,r_articulo);
		hashValores.put("2.ventas", ventasAnterior2);
		hashValores.put("2.ventasAvg", ventasAnterior2/12);
//		Double ventasImpAnterior2 = this.recuperarImporteVentas(new Integer(r_ejercicio)-2,12,r_articulo);
//		hashValores.put("2.ventasImp", ventasImpAnterior2);
//		hashValores.put("2.pvpVentas", ventasImpAnterior2/ventasAnterior2);
//
		Double ventasAnterior3 = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-3,12,r_articulo);
		hashValores.put("3.ventas", ventasAnterior3);
		hashValores.put("3.ventasAvg", ventasAnterior3/12);
//		Double ventasImpAnterior3 = this.recuperarImporteVentas(new Integer(r_ejercicio)-3,12,r_articulo);
//		hashValores.put("3.ventasImp", ventasImpAnterior3);
//		hashValores.put("3.pvpVentas", ventasImpAnterior3/ventasAnterior3);

		Double compras = this.recuperarUnidadesCompras(new Integer(r_ejercicio),meses, r_articulo);
		hashValores.put("0.compras", compras);
		
		if (meses!=0) hashValores.put("0.comprasAvg", compras/meses); else hashValores.put("0.comprasAvg", compras/12);
		Double comprasImp = this.recuperarImporteCompras(new Integer(r_ejercicio),meses,r_articulo);
		hashValores.put("0.comprasImp", comprasImp);
		hashValores.put("0.pvpCompras", comprasImp/compras);
		
		Double comprasAnterior = this.recuperarUnidadesCompras(new Integer(r_ejercicio)-1,12,r_articulo);
		hashValores.put("1.compras", comprasAnterior);
		hashValores.put("1.comprasAvg", comprasAnterior/12);
		Double comprasImpAnterior = this.recuperarImporteCompras(new Integer(r_ejercicio)-1,12,r_articulo);
		hashValores.put("1.comprasImp", comprasImpAnterior);
		hashValores.put("1.pvpCompras", comprasImpAnterior/comprasAnterior);
		
		Double comprasAnterior2 = this.recuperarUnidadesCompras(new Integer(r_ejercicio)-2,12,r_articulo);
		hashValores.put("2.compras", comprasAnterior2);
		hashValores.put("2.comprasAvg", comprasAnterior2/12);
		Double comprasImpAnterior2 = this.recuperarImporteCompras(new Integer(r_ejercicio)-2,12,r_articulo);
		hashValores.put("2.comprasImp", comprasImpAnterior2);
		hashValores.put("2.pvpCompras", comprasImpAnterior2/comprasAnterior2);
		
		Double comprasAnterior3 = this.recuperarUnidadesCompras(new Integer(r_ejercicio)-3,12,r_articulo);
		hashValores.put("3.compras", comprasAnterior3);
		hashValores.put("3.comprasAvg", comprasAnterior3/12);
		Double comprasImpAnterior3 = this.recuperarImporteCompras(new Integer(r_ejercicio)-3,12,r_articulo);
		hashValores.put("3.comprasImp", comprasImpAnterior3);
		hashValores.put("3.pvpCompras", comprasImpAnterior3/comprasAnterior3);
		
//		Integer cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio), r_articulo);
//		hashValores.put("0.clientes", cuantos.doubleValue());
//		cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio)-1, r_articulo);
//		hashValores.put("1.clientes", cuantos.doubleValue());
//		cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio)-2, r_articulo);
//		hashValores.put("2.clientes", cuantos.doubleValue());
//		
		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.stock");
			hashValores.put("0.stock", valor);

			valor = vectorDatosStock.get("0.minStock");
			hashValores.put("0.minStock", valor);

			valor = vectorDatosStock.get("0.pdteRecibir");
			hashValores.put("0.pdteRecibir", valor);
			
			valor = vectorDatosStock.get("0.pdteServir");
			hashValores.put("0.pdteServir", valor);

			valor = vectorDatosStock.get("0.prog");
			hashValores.put("0.prog", valor);
			
		}
		
		mapeo = new MapeoGeneralDashboardCompras();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	
	private Double recuperarProduccion(Integer r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "49";
		String articulos="";
		ArrayList<String> r_articulos=null;
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			sql = new StringBuffer();
			
			if (r_tipoArticulo.contentEquals("OA") || r_tipoArticulo.contentEquals("MP")) movimiento = "99";
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

//			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
//			r_articulos = ces.recuperarPadres(r_articulo);

			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades) total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");

//     		for (int i=0; i<r_articulos.size();i++)
//     		{
//     			if (articulos!="") articulos = articulos + ",";
//     			articulos =articulos + "'" + r_articulos.get(i).trim().substring(0, 7) + "'";
//     		}
//     		if (articulos!="") sql.append(" where articulo in (" + articulos + ") ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('"+movimiento+"') ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	
	private Double recuperarUnidadesVentas(Integer r_ejercicio, Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;
		
		try
		{
			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			String digito = "0";
			
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(unidades) total "); 	
			sql.append(" FROM a_lin_" + digito + " a ");
 			sql.append(" where a.articulo in (select padre from e__lconj where hijo = '" + r_articulo + "') ");
			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a.movimiento in ('51','53') ");
			sql.append(" and a.clave_tabla in ('A','F') ");
			sql.append(" GROUP BY 1 ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			Double factor = ces.recuperarCantidadComponente(con, r_articulo);
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total")*factor;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	private Double recuperarUnidadesPendientesServir(Integer r_ejercicio, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		
		try
		{
			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			String digito = "0";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(unidades-unidades_serv) total "); 	
			sql.append(" FROM a_vp_l_" + digito + " a ");
			sql.append(" where a.articulo in (select padre from e__lconj where hijo = '" + r_articulo + "') ");
			sql.append(" and a.movimiento in ('51','53') ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			Double factor = ces.recuperarCantidadComponente(con, r_articulo);
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total")*factor;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}

	private Double recuperarUnidadesCompras(Integer r_ejercicio, Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;
		try
		{
			
			String digito = "0";
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(unidades) total ");
			sql.append(" FROM a_lin_" + digito );
			sql.append(" where a_lin_" + digito + ".articulo = '" + r_articulo + "' ");
			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a_lin_" + digito + ".movimiento in ('01') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('E','M') ");
			
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}

	private Double recuperarImporteVentas(Integer r_ejercicio,Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;

		try
		{
			String digito = "0";
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}

			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(importe) total ");
			sql.append(" FROM a_lin_" + digito );
			

 			sql.append(" where a_lin_" + digito + ".articulo in (select padre from e__lconj where hijo = '" + r_articulo + "') ");

			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY 1 ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	private Double recuperarImporteCompras(Integer r_ejercicio,Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;
		try
		{
			String digito = "0";
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(importe) total ");
			sql.append(" FROM a_lin_" + digito );
			sql.append(" where a_lin_" + digito + ".articulo = '" + r_articulo + "' ");
			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a_lin_" + digito + ".movimiento in ('01') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('E','M') ");
			
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}

	private HashMap<String, Double>  recuperarStock(Integer r_ejercicio,String r_articulo)
	{
		
		
		HashMap<String, Double> hash = null;
		
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);

		hash = cis.recuperarDatosResumenStock(r_ejercicio, r_articulo, "1,3,4,5");

		Double programadas = new Double(this.obtenerProgramado(r_ejercicio, "X", r_articulo));
		Double programadasEnv = new Double(this.obtenerProgramadoEnvasadora(r_ejercicio, "X", r_articulo));
		Double pendienteServir = new Double(this.recuperarUnidadesPendientesServir(r_ejercicio, r_articulo));
			
		hash.put(digito+".pdteServir", pendienteServir);
		hash.put(digito+".prog", programadas+programadasEnv);

		/*
		 * pendiente de ver como caluclarlo
		 */
		hash.put("0.minStock", new Double(0));

		return hash;
	}

	private Integer obtenerProgramado(Integer r_ejercicio, String r_estado, String r_articulo)
	{
		ArrayList<MapeoProgramacion> vectorProgramado = null;
		ArrayList<String> vectorArticulos=null;
		int total = 0;
		Double factorMp=new Double(0);
		boolean etiquetados=true;
		
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
		vectorArticulos = ces.recuperarPadres(r_articulo);

		if (r_articulo.startsWith("0104") || r_articulo.startsWith("0105") || r_articulo.startsWith("0101")) etiquetados = false;
				
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		vectorProgramado = cps.datosProgramacionGlobal(vectorArticulos, r_estado, etiquetados, r_ejercicio);
		if (vectorProgramado!=null)
		{
			for (int i=0;i<vectorProgramado.size();i++)
			{
				MapeoProgramacion map = (MapeoProgramacion) vectorProgramado.get(i);
				if (factorMp==0) factorMp = ces.recuperarCantidadComponente(map.getArticulo(), r_articulo);
				if (map!=null && map.getEjercicio()!=null && map.getEjercicio().compareTo(r_ejercicio)>=0 && map.getUnidades()!=null && factorMp!=null)
				{
					total = total + new Double((map.getUnidades().doubleValue()*factorMp)).intValue();
				}
			}
		}
		return total;
	}

	private Integer obtenerProgramadoEnvasadora(Integer r_ejercicio, String r_estado, String r_articulo)
	{
		int total = 0;
		boolean etiquetados=true;
		Double factorMp=new Double(0);
		MapeoProgramacionEnvasadora map=null;
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
		ArrayList<String> vectorArticulos = ces.recuperarPadres(r_articulo);
		
		if (vectorArticulos!=null && vectorArticulos.size()>0)
		{
			if (r_articulo.startsWith("0104") || r_articulo.startsWith("0105") || r_articulo.startsWith("0101")) etiquetados = false;
			map = new MapeoProgramacionEnvasadora();
			map.setEstado(r_estado);
			consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
			vectorProgramadoMesa = cps.datosProgramacionGlobal(vectorArticulos, map, etiquetados, false);
	
			for (int i=0;i<vectorProgramadoMesa.size();i++)
			{
				map = (MapeoProgramacionEnvasadora) vectorProgramadoMesa.get(i);
				if (map.getEjercicio().compareTo(r_ejercicio)>=0)
				{
					factorMp = ces.recuperarCantidadComponente(map.getArticulo(), r_articulo);
					total = total + new Double((map.getUnidades().doubleValue()*factorMp)).intValue();
				}
			}
		}
		return total;
	}

	public HashMap<String,	Double> recuperarMovimientosVentasMesesEjercicio(Integer r_ejercicio, String r_mascara,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				Integer hastaSemana = 0;
				if (digito.contentEquals("0")) hastaSemana = new Integer(RutinasFechas.mesActualMM()); else hastaSemana = 12;
				
				for (int i = 1; i<=hastaSemana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public ArrayList<MapeoValores>  recuperarComprasProveedorEjercicio(Integer r_ejercicio, String r_mascara,String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoValores> vector = null;
		MapeoValores map = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(d.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a.nombre_comercial pro, ");
			sql.append(" SUM(unidades*" + r_campo + ") compra, ");
			sql.append(" SUM(importe) imp ");
			sql.append(" FROM a_lin_" + digito + " b, a_ce_c_" + digito + " a ");
			sql.append(" where b.clave_tabla = a.clave_tabla ");
			sql.append(" and b.documento = a.documento ");
			sql.append(" and b.serie = a.serie ");
			sql.append(" and b.codigo = a.codigo ");
			sql.append(" and b.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and b.movimiento in ('01') ");
			sql.append(" and b.clave_tabla in ('E','M') ");
			
			sql.append(" GROUP BY 1 ");
			sql.append(" order by 2 desc ");

				
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			vector = new ArrayList<MapeoValores>();
			
			while(rsMovimientos.next())
			{
				map = new MapeoValores();
				map.setClave(RutinasCadenas.conversion(rsMovimientos.getString("pro")));
				map.setValorDouble(rsMovimientos.getDouble("compra"));
				map.setValorDouble2(rsMovimientos.getDouble("imp"));
				vector.add(map);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	
	public Integer  recuperarProveedoresDisintosEjercicio(Integer r_ejercicio, String r_mascara)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer cuantos = 0;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT count(unique a.proveedor) pro ");
			sql.append(" FROM a_lin_" + digito + " b, a_ce_c_" + digito + " a ");
			sql.append(" where b.clave_tabla = a.clave_tabla ");
			sql.append(" and b.documento = a.documento ");
			sql.append(" and b.serie = a.serie ");
			sql.append(" and b.codigo = a.codigo ");
			sql.append(" and b.articulo like '" + r_mascara + "%' ");
			sql.append(" and b.movimiento in ('01') ");
			sql.append(" and b.clave_tabla in ('E','M') ");
				
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				cuantos = rsMovimientos.getInt("pro");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cuantos;
	}
	
	public HashMap<String,	Double> recuperarComprasAcumulados(Integer r_ejercicio,String  r_mascara, String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		CallableStatement statement = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			
			String digito = "0";
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			Integer hastaSemana = 0;
//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			
			if (r_semana.contentEquals("0") && digito.contentEquals("0"))
			{
				sql = " "; 
				hastaSemana=53;
			}
			else
			{
				sql= " and week(fecha_documento,3)<= '" + r_semana + "' ";
				hastaSemana=new Integer(r_semana);
			}
			
			con= this.conManager.establecerConexionInd();			
			
			
			statement = con.prepareCall("{call compraAcumuladaBotella(?,?,?,?)}");

			statement.setString(1, r_campo);
			statement.setString(2, digito);
			statement.setString(3, r_mascara);
			statement.setString(4, sql);
			boolean hadResults = statement.execute();

			while (hadResults) 
			{
				rsMovimientos = statement.getResultSet();
				hash = new HashMap<String, Double>(); 
				// process result set
				while(rsMovimientos.next())
				{
					
					for (int i = 1; i<=hastaSemana;i++)
					{
						hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
					}
				}


				hadResults = statement.getMoreResults();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos(null);
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("BIB");
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="0";
				}
			}
		}
		return permisos;
	}

}
