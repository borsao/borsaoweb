package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDashboardEmbotelladoraProduccionAnalizada extends MapeoGlobal
{
	private Integer ejercicio;
	private String familia;
	private String subfamilia;
	private String nombreFamilia;
	private String nombreSubfamilia;
	private double total;
	
	public MapeoDashboardEmbotelladoraProduccionAnalizada()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public String getFamilia() {
		return familia;
	}

	public String getSubfamilia() {
		return subfamilia;
	}

	public String getNombreFamilia() {
		return nombreFamilia;
	}

	public String getNombreSubfamilia() {
		return nombreSubfamilia;
	}

	public double getTotal() {
		return total;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public void setSubfamilia(String subfamilia) {
		this.subfamilia = subfamilia;
	}

	public void setNombreFamilia(String nombreFamilia) {
		this.nombreFamilia = nombreFamilia;
	}

	public void setNombreSubfamilia(String nombreSubfamilia) {
		this.nombreSubfamilia = nombreSubfamilia;
	}

	public void setTotal(double total) {
		this.total = total;
	}

}