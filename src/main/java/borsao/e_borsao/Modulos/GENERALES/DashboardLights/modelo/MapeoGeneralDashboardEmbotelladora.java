package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardEmbotelladora extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardEmbotelladora> vectorProduccion = null;
	private ArrayList<MapeoDashboardEmbotelladora> vectorVentas= null;
	private ArrayList<MapeoDashboardEmbotelladora> vectorVentasAnterior= null;
	
	public MapeoGeneralDashboardEmbotelladora()
	{
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardEmbotelladora> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardEmbotelladora> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardEmbotelladora> getVectorVentas() {
		return vectorVentas;
	}

	public void setVectorVentas(ArrayList<MapeoDashboardEmbotelladora> vectorVentas) {
		this.vectorVentas = vectorVentas;
	}

	public ArrayList<MapeoDashboardEmbotelladora> getVectorVentasAnterior() {
		return vectorVentasAnterior;
	}

	public void setVectorVentasAnterior(ArrayList<MapeoDashboardEmbotelladora> vectorVentasAnterior) {
		this.vectorVentasAnterior = vectorVentasAnterior;
	}

	
}