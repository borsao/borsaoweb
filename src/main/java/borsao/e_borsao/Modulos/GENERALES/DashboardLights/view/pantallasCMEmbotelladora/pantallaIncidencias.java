package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.ClasesPropias.graficas.barras;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.pantallaRelacionIncidencias;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaIncidencias extends Window
{

	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    private Combo cmbTipo = null;
    
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;

    private Panel panelResumenSemana =null;
    private GridLayout centralTopSemana = null;
    
	public TextField txtEjercicio= null;

	private String ejercicio = null;
	private String semana = null;
	private String ambitoTemporal = null;
	private String tipoIncidencias = null;
	private String lineaProduccion = null;
	private boolean verMenu = true;
	public boolean acceso = true;
	
	public pantallaIncidencias(String r_area, String r_titulo, String r_ejercicio, String r_semana, String r_tipo, String r_calculo)
	{
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			if (r_tipo!=null && r_tipo.length()>0) this.tipoIncidencias=r_tipo;
			this.semana = r_semana;
			this.ambitoTemporal=r_calculo;
			this.ejercicio = r_ejercicio;
			this.lineaProduccion = r_area;
			this.setCaption(r_titulo);
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	this.txtEjercicio.setValue(this.ejercicio);
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		
	    		this.cmbTipo = new Combo();
	    		this.cmbTipo.setNewItemsAllowed(false);
	    		this.cmbTipo.setNullSelectionAllowed(false);	    		
	    		this.cmbTipo.addItem("Calidad");
	    		this.cmbTipo.addItem("No Programadas");
	    		this.cmbTipo.addItem("Todas");
	    		this.cmbTipo.setValue("Todas");
	    		this.cmbTipo.addStyleName(ValoTheme.COMBOBOX_TINY);
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
    		
    		if (this.tipoIncidencias!=null && !this.tipoIncidencias.contains("P"))
    		{
    			this.topLayoutL.addComponent(this.cmbTipo);
    			this.topLayoutL.setComponentAlignment(this.cmbTipo,  Alignment.BOTTOM_LEFT);
    		}
	    	
    		this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumenSemana = new Panel(this.getCaption().toUpperCase());
	    	this.panelResumenSemana.setSizeFull();
	    	this.panelResumenSemana.addStyleName("showpointer");

		    	this.centralTopSemana = new GridLayout(2,1);
		    	this.centralTopSemana.setSizeFull();
		    	this.centralTopSemana.setMargin(true);

	    	this.panelResumenSemana.setContent(this.centralTopSemana);
	    	
	    	this.panelResumen = new Panel("CALIDAD ");
	    	this.panelResumen.setSizeFull();
	    	this.panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(1,1);
			    this.centralTop2.setSizeFull();
			    this.centralTop2.setMargin(true);

	    	this.panelResumen.setContent(centralTop2);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
//    	this.barAndGridLayout.setExpandRatio(this.topLayoutL, 1);
    	this.barAndGridLayout.addComponent(this.panelResumenSemana);
    	this.barAndGridLayout.setExpandRatio(this.panelResumenSemana, 1);
//    	this.barAndGridLayout.addComponent(this.panelResumen);
//    	this.barAndGridLayout.setExpandRatio(this.panelResumen, 2);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

		this.cmbTipo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				/*
				 * regeneraremos los datos con el valor seleccionado
				 */
				if (cmbTipo.getValue().toString().contains("Calidad"))
					tipoIncidencias = "C";
				else if (cmbTipo.getValue().toString().contains("Progra"))
					tipoIncidencias = "N";
				else
					tipoIncidencias = "Todas";
					
				cargarGrid();
			}
		});
		
    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
	private void cargarGrid()
	{
		ArrayList<MapeoBarras> vector_maquinas = null;
		DecimalFormat formato = new DecimalFormat("##.##");
		
		centralTopSemana.removeAllComponents();

		consultaParteProduccionServer cpps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		vector_maquinas = cpps.recuperarArrayIncidenciasMaquinas(this.lineaProduccion, new Integer(this.txtEjercicio.getValue()), this.semana, this.tipoIncidencias, this.ambitoTemporal);

    	IndexedContainer container =null;
    	Iterator<MapeoBarras> iterator = null;
    	MapeoBarras mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("incidencia", String.class, null);
		container.addContainerProperty("tiempo", Double.class, null);
			
		iterator = vector_maquinas.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoBarras) iterator.next();
			
			file.getItemProperty("incidencia").setValue(mapeo.getClave());
			file.getItemProperty("tiempo").setValue(mapeo.getValorActual());
		}

		Grid gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
//		gridDatos.setWidth("100%");
		gridDatos.addStyleName("smallgrid");
		
		gridDatos.getColumn("incidencia").setHeaderCaption("Máquina");
		gridDatos.getColumn("incidencia").setWidth(250);
		gridDatos.getColumn("tiempo").setHeaderCaption("Tiempo (Horas)");
		gridDatos.getColumn("tiempo").setWidth(180);
		gridDatos.getColumn("tiempo").setRenderer(new NumberRenderer(formato));
		
		gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
				if (file!=null && file.getItemProperty("incidencia").getValue()!=null)
				{
					String maquina= file.getItemProperty("incidencia").getValue().toString();
	
					pantallaRelacionIncidencias vt = new pantallaRelacionIncidencias(lineaProduccion, maquina, new Integer(txtEjercicio.getValue()), semana, tipoIncidencias, ambitoTemporal);
					getUI().addWindow(vt);
				}
			}
		});
		
		
		Double totalU = new Double(0) ;
    	Double valor = null;
        	
    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	
		totalU = new Double(0) ;
    	for(Object itemId : list)
    	{
    		Item item = indexed.getItem(itemId);
        		
    		valor = (Double) item.getItemProperty("tiempo").getValue();
    		if (valor!=null)
    			totalU += valor;
				
    	}
    	footer.getCell("incidencia").setText("Totales ");
    	footer.getCell("tiempo").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    	footer.getCell("tiempo").setStyleName("Rcell-pie");
		
    	gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("tiempo".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
		
		String titulo = null;
		if (this.semana!=null)
		{
			titulo = this.getCaption() + " " + this.txtEjercicio.getValue() + " acumulado " + this.ambitoTemporal + " " + this.semana; 
		}
		else
		{
			titulo = this.getCaption() + " " + this.txtEjercicio.getValue() ;
		}
		Panel panel = new Panel(titulo);
    	panel.setSizeUndefined(); // Shrink to fit content
//    	panel.setHeight("250px");
    	panel.setContent(gridDatos);
    	
    	centralTopSemana.addComponent(panel,0,0);
    	
    	this.cargarResumen();
    }
	
    private void cargarResumen()
	{
		HashMap<String, Double> hashIncidencias= null;
	    ArrayList<MapeoBarras> vector = null;
		HashMap<Integer, String> vector_maquinas = null;
		MapeoBarras mapeoGrafico = null;
		double valorMax = 0;
		
		consultaParteProduccionServer cpps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		vector_maquinas = cpps.recuperarMaquinas(this.lineaProduccion, new Integer(this.txtEjercicio.getValue()), this.semana, this.tipoIncidencias);
		hashIncidencias= cpps.recuperarIncidenciasMaquinas(this.lineaProduccion, new Integer(this.txtEjercicio.getValue()), this.semana, this.tipoIncidencias,this.ambitoTemporal);
		
		vector = new ArrayList<MapeoBarras>();
		
		for (int i = 1; i<=vector_maquinas.size();i++)
		{
			mapeoGrafico = new MapeoBarras();
			
			mapeoGrafico.setClave(vector_maquinas.get(i));
			mapeoGrafico.setValorActual(hashIncidencias.get(vector_maquinas.get(i)));
			if (valorMax < hashIncidencias.get(vector_maquinas.get(i)).doubleValue())
			{
				valorMax = hashIncidencias.get(vector_maquinas.get(i)).doubleValue();
			}
			vector.add(mapeoGrafico);
		}
		
		JFreeChartWrapper wrapper = barras.generarGrafico("Incidencias Año / Máquina (Horas)", "", "", "765px", "400px", vector,"Actual", null, null,true, 0, valorMax*1.15);
		centralTopSemana.addComponent(wrapper,1,0);
	}
}