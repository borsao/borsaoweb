package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardEnvasadora extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardEnvasadora> vectorProduccion = null;
	private ArrayList<MapeoDashboardEnvasadora> vectorVentas= null;
	private ArrayList<MapeoDashboardEnvasadora> vectorVentasAnterior= null;
	
	public MapeoGeneralDashboardEnvasadora()
	{
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardEnvasadora> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardEnvasadora> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardEnvasadora> getVectorVentas() {
		return vectorVentas;
	}

	public void setVectorVentas(ArrayList<MapeoDashboardEnvasadora> vectorVentas) {
		this.vectorVentas = vectorVentas;
	}

	public ArrayList<MapeoDashboardEnvasadora> getVectorVentasAnterior() {
		return vectorVentasAnterior;
	}

	public void setVectorVentasAnterior(ArrayList<MapeoDashboardEnvasadora> vectorVentasAnterior) {
		this.vectorVentasAnterior = vectorVentasAnterior;
	}

	
}