package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.vaadin.addon.JFreeChartWrapper;
import org.vaadin.addons.d3Gauge.GaugeStyle;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.ClasesPropias.graficas.barras;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.ClasesPropias.graficas.queso;
import borsao.e_borsao.ClasesPropias.graficas.reloj;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEnvasadora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.server.consultaHorasTrabajadasEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardEnvasadoraView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Envasadora";
	private Integer tamañoRelojes = 160;
	private final String titulo = "CUADRO MANDO VINO MESA";
	private final int intervaloRefresco = 0*60*1000;
	private ChatRefreshListener cr = null;
	
	private CssLayout barAndGridLayout = null;    
    private VerticalLayout topLayout = null;
    private HorizontalLayout topLayoutL = null;
    private HorizontalLayout topLayoutS = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;
    
    private Panel panelResumen =null;
    private Panel panelProduccion =null;
    private Panel panelVentas=null;
    private Panel panelStockResumen =null;
    
    private GridLayout centralTop = null;
    private GridLayout centralTop2 = null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle2 = null;
    
    private GridLayout centralBottom = null;
    
	public ComboBox cmbSemana = null;
	public ComboBox cmbCalculo = null;
	public ComboBox cmbVista = null;
	public TextField txtEjercicio= null;
	public String semana = null;
	private Button procesar=null;

	private Button opcMenos= null;
	private Button opcMas= null;

	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardEnvasadora mapeo=null;
	
	private boolean prevision = false;
	private Double correctorVentasEjercicio = null;
	private CheckBox prev = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardEnvasadoraView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.correctorVentasEjercicio = new Double(1);
    	
    	this.cargarPantalla();
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
		
		this.cargarListeners();
//		this.ejecutarTimer();
//		this.procesar();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
//    	setSpacing(false);
    	
    	
    	this.barAndGridLayout = new CssLayout();
//        this.barAndGridLayout.setSpacing(false);
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

//        final Navigator navigator = new Navigator(eBorsao.getCurrent(), this.barAndGridLayout);

		    	this.topLayoutL = new HorizontalLayout();
//		    	this.topLayoutL.addStyleName("v-panelTitulo");
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.cmbVista= new ComboBox("Vista");    	
			    	this.cmbVista.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbVista.setNewItemsAllowed(false);
			    	this.cmbVista.setWidth("150px");
			    	this.cmbVista.setNullSelectionAllowed(false);
			    	this.cmbVista.addItem("Anual");
			    	this.cmbVista.addItem("Acumulado");
//			    	this.cmbVista.addItem("Ultimos 6 Meses");
			    	this.cmbVista.addItem("Semanal");
			    	this.cmbVista.setValue("Semanal");

			    	this.cmbCalculo= new ComboBox("Cálculo");    	
			    	this.cmbCalculo.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbCalculo.setNewItemsAllowed(false);
			    	this.cmbCalculo.setWidth("150px");
			    	this.cmbCalculo.setNullSelectionAllowed(false);
			    	this.cmbCalculo.addItem("Litros");
//			    	this.cmbCalculo.addItem("Ultimos 6 Meses");
			    	this.cmbCalculo.addItem("Unidades");
			    	this.cmbCalculo.setValue("Litros");

			    	this.txtEjercicio=new TextField("Ejercicio");
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
//		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.cmbSemana= new ComboBox("Semana");    	
		    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		    		this.cmbSemana.setNewItemsAllowed(false);
		    		this.cmbSemana.setWidth("125px");
		    		this.cmbSemana.setNullSelectionAllowed(false);

		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		    		
		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);
		        	
			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
			    	this.prev = new CheckBox("Prevision?");
			    	this.prev.addStyleName(ValoTheme.CHECKBOX_SMALL);
			    	
		    	if (LecturaProperties.semaforos)
		    	{
//		    		this.topLayoutL.addComponent(this.opcRefresh);
//		    		this.topLayoutL.setComponentAlignment(this.opcRefresh,  Alignment.BOTTOM_LEFT);
		    	}
		    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.cmbVista);
		    	this.topLayoutL.addComponent(this.cmbCalculo);
		    	this.topLayoutL.addComponent(this.txtEjercicio);
	    		this.topLayoutL.addComponent(this.opcMenos);
	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.cmbSemana);
	    		this.topLayoutL.addComponent(this.opcMas);
	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);

		    		this.topLayoutL.addComponent(this.prev);
		    		this.topLayoutL.setComponentAlignment(this.prev,  Alignment.BOTTOM_LEFT);

		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
//		    	this.topLayoutL.setStyleName("miPanel");
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

//	    	this.topLayoutL.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

	    	panelResumen = new Panel("Resumen");
		    panelResumen.setHeightUndefined();
		    panelResumen.setWidth("100%");
		    panelResumen.addStyleName("showpointer");

		    	this.centralTop = new GridLayout(8,1);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);
		    	this.centralTop.setSpacing(true);

	    	panelResumen.setContent(centralTop);
	    	
	    	panelProduccion = new Panel("Resumen Prod. ");
		    panelProduccion.setHeightUndefined();
		    panelProduccion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelProduccion);
		    
		    	this.centralTop2 = new GridLayout(8,1);
		    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	this.centralTop2.setSpacing(true);
		    	
	    	panelProduccion.setContent(centralTop2);
	    	panelProduccion.getContent().setVisible(false);
	    	panelProduccion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    	panelVentas= new Panel("Resumen Ventas ");
		    panelVentas.setHeightUndefined();
		    panelVentas.setWidth("100%");
		    panelVentas.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelVentas );
		    
		    	this.centralMiddle = new GridLayout(3,1);
		    	this.centralMiddle.setWidth("100%");
		    	this.centralMiddle.setMargin(true);
		    	this.centralMiddle.setSpacing(true);

	    	panelVentas.setContent(centralMiddle);
	    	panelVentas.getContent().setVisible(false);
	    	panelVentas.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    		this.centralMiddle2 = new GridLayout(5,1);
		    	this.centralMiddle2.setWidth("100%");
		    	this.centralMiddle2.setMargin(true);

	    	panelStockResumen= new Panel("Resumen Stock");
		    panelStockResumen.setHeightUndefined();
		    panelStockResumen.setWidth("100%");		    
		    panelStockResumen.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelStockResumen );

		    	this.centralBottom = new GridLayout(2,1);
		    	this.centralBottom.setWidth("100%");
		    	this.centralBottom.setMargin(true);
		    	this.centralBottom.setSpacing(true);
		    	
	    	panelStockResumen.setContent(centralBottom);
	    	panelStockResumen.getContent().setVisible(false);
	    	panelStockResumen.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumen);
//    	this.barAndGridLayout.addComponent(this.centralTop);
    	this.barAndGridLayout.addComponent(this.panelProduccion);
//    	this.barAndGridLayout.addComponent(this.centralTop2);
    	this.barAndGridLayout.addComponent(this.panelVentas);
//    	this.barAndGridLayout.addComponent(this.centralMiddle);
    	this.barAndGridLayout.addComponent(this.centralMiddle2);
    	this.barAndGridLayout.addComponent(this.panelStockResumen);
//    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
//    	setExpandRatio(this.barAndGridLayout, 1);
        
    }

    private void procesar()
    {
		eliminarDashBoards();
		
//    	if ((RutinasFechas.añoActualYYYY().contentEquals(this.txtEjercicio.getValue()) && (new Integer(this.cmbSemana.getValue().toString())).intValue() > RutinasFechas.semanaActual(this.txtEjercicio.getValue()))
//    		
//    			|| ((RutinasFechas.añoActualYYYY().contentEquals(this.txtEjercicio.getValue()) && (new Integer(this.cmbSemana.getValue().toString())).intValue() == RutinasFechas.semanaActual(this.txtEjercicio.getValue()) 
//    				&& (RutinasFechas.obtenerDiaDeLaSemana(new Date())>=2 && RutinasFechas.obtenerDiaDeLaSemana(new Date())<=4)))) 
//    	{
//			setPrevision(true);
//			this.correctorVentasEjercicio=new Double(1.1);
//    	}
//    	else
//    	{
//    		setPrevision(false);
//    		this.correctorVentasEjercicio=new Double(1);
//    	}
    	
		if (cmbVista.getValue()==null || cmbSemana.getValue()==null || txtEjercicio.getValue()==null 
				|| cmbVista.getValue().toString().length()==0 || cmbSemana.getValue().toString().length()==0 || txtEjercicio.getValue().length()==0 )
		{
			Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
		}
		else
		{
			cargarDashboards();
		}
    }
    
    private void cargarListeners() 
    {
    	
    	this.prev.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				setPrevision(prev.getValue());
			}
		});
    	
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
//    			if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;			
//					if (cmbLinea.getValue()!=null && cmbLinea.getValue().toString().length()>0) 
//					{
//						generarGrid();
//					}
//				}
    		}
    			
    	});
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));

			}
		});


    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});

    	LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};
//		centralLayoutT1.addLayoutClickListener(lcl);

    }

    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
    	this.cargarListeners();
    }

    public void eliminarDashBoards()
    {
    	centralTop.removeAllComponents();
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	
    }
    public void cargarDashboards() 
    {
    	String ejer = this.txtEjercicio.getValue();
    	String sem = this.cmbSemana.getValue().toString();
    	String vista = this.cmbVista.getValue().toString();
    	String campo = this.cmbCalculo.getValue().toString();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		{
    			this.mapeo = consultaDashboardEnvasadoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;
    		}
    		case "Acumulado":
    		{
    			this.mapeo = consultaDashboardEnvasadoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;
    		}
    		case "Anual":
    		{
    			this.mapeo = consultaDashboardEnvasadoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, "0", vista,campo);
    			break;
    		}
    		default:
    		{
    			this.mapeo = consultaDashboardEnvasadoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista,campo);
    			cargarMeses(ejer, sem);
    			break;
    		}
    	}
    	/*
    	 * cargar panel resumen datos
    	 */
    	cargarResumen();
    	/*
    	 * cargamos grafico con las medias de produccion
    	 */
    	
    	if (!isPrevision()) this.pieProdTotales();
    	if (!isPrevision()) this.pieProdTotales2();
    	
    	cargarResumenVentas();
    	cargarResumenStock();
    	cargarResumenHoras();
    	
    	if (!isPrevision()) cargarRelojes();
    	
//    	tendenciaMediaActual5L();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		{
    			cargarSemanal(ejer, sem);
    			break;
    		}
    		case "Anual":
    		{
    			cargarAnual(ejer);
    			break;
    		}
    	}
    	
    	/*
    	 * Cargamos grid registros produccion
    	 */
//    	cargarRegistros(ejer);
    	
    	
    }
    
    
    private void cargarRelojes()
    {
    	/*
    	 * GENERACION DATOS GLOBAL
    	 */
    	
    	Double valorG = null;
    	Double horasG = null;
    	Double mediaG = null;
    	Panel gaugeGL = null;
    	
    	valorG = this.mapeo.getHashValores().get("0.prod.01030")+this.mapeo.getHashValores().get("0.prod.01031");
    	horasG = this.mapeo.getHashValores().get("0.horas.01030")+this.mapeo.getHashValores().get("0.horas.01031");

    	if(valorG!=null && horasG != null && horasG.doubleValue()!=0)
    	{
    		mediaG = valorG/horasG*8;

    		if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    			gaugeGL = reloj.generarReloj(  "MEDIA GLOBAL (Ltr/Tno.)",mediaG.intValue(), 50000, 75000, 35000, 49999, 0, 34999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    		else
    			gaugeGL = reloj.generarReloj(  "MEDIA GLOBAL (Grf/Tno.)",mediaG.intValue(), 10000, 15000, 7000, 9999, 0, 6999, 0, 15000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    		
    		centralTop.addComponent(gaugeGL,5,0);
    	}
    	/*
    	 * GENERACION DATOS 5L
    	 */
    	
    	Double valor = null;
    	Double horas = null;
    	Double inc = null;
    	Double media = null;
    	Double mediaInc = null;
    	Double mediaMostrar = null;
    	Panel gauge5L = null;
    	Panel gaugeInc5L = null;
    	Panel gauge2L = null;
    	Panel gaugeInc2L = null;
    	
    	valor = this.mapeo.getHashValores().get("0.prod.01030");
    	horas = this.mapeo.getHashValores().get("0.horas.01030");
    	inc = this.mapeo.getHashValores().get("0.pro.01030");
    	
    	if(valor!=null && horas != null && horas.doubleValue()!=0)
    	{
    		media = valor/horas*8;
    		mediaInc = valor/(horas-inc)*8;

    		if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    			gauge5L = reloj.generarReloj(  "MEDIA 5L (Ltr/Tno.)",media.intValue(), 50000, 75000, 35000, 49999, 0, 34999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    		else
    			gauge5L = reloj.generarReloj(  "MEDIA 5L (Grf/Tno.)",media.intValue(), 10000, 15000, 7000, 9999, 0, 6999, 0, 15000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
//    	centralLayoutT5.addComponent(gauge5L);
    		
    		
    		centralTop2.addComponent(gauge5L,1,0);
    		
    		
    		if (inc.doubleValue()!=0)
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				gaugeInc5L = reloj.generarReloj(  "MEDIA 5L (Ltr/Tno-Pr.)",mediaInc.intValue(), 50000, 75000, 35000, 49999, 0, 34999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    			else
    				gaugeInc5L = reloj.generarReloj(  "MEDIA 5L (Grf/Tno-Pr.)",mediaInc.intValue(), 10000, 15000, 7000, 9999, 0, 6999, 0, 15000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
//        	
//	    	centralLayoutT5.addComponent(gaugeInc5L);
    			centralTop2.addComponent(gaugeInc5L,2,0);	
    		}
    		
    		centralTop2.addComponent(indiceOEE5L(),3,0);
    	}
//    	else
//    	{
//    		media = new Double(0);
//    		mediaInc = new Double(0);
//    	}

    	
    	
    	/*
    	 * GENERACION DATOS 2L
    	 */
    	valor = this.mapeo.getHashValores().get("0.prod.01031");
    	horas = this.mapeo.getHashValores().get("0.horas.01031");
    	inc = this.mapeo.getHashValores().get("0.pro.01031");
    	if(valor!=null && horas != null && horas.doubleValue()!=0)
    	{
    		media = valor/horas*8;
    		mediaInc = valor/(horas-inc)*8;

    		if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    			gauge2L = reloj.generarReloj("MEDIA 2L (Ltr/Tno)",media.intValue(), 45000, 75000, 30000, 44999, 0, 29999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(),tamañoRelojes);
    		else
    			gauge2L = reloj.generarReloj("MEDIA 2L (Grf/Tno)",media.intValue(), 22500, 37500, 15000, 22499, 0, 14999, 0, 37500, GaugeStyle.STYLE_DEFAULT.toString(),tamañoRelojes);
//    	centralLayoutT6.addComponent(gauge2L);
    		
    		centralTop2.addComponent(gauge2L,5,0);
    		
    		if (inc.doubleValue()!=0)
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				gaugeInc2L = reloj.generarReloj("MEDIA 2L (Ltr/Tno-Pr.)",mediaInc.intValue(), 45000, 75000, 30000, 44999, 0, 29999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(),tamañoRelojes);
    			else
    				gaugeInc2L = reloj.generarReloj("MEDIA 2L (Grf/Tno-Pr.)",mediaInc.intValue(), 22500, 37500, 15000, 22499, 0, 14999, 0, 37500, GaugeStyle.STYLE_DEFAULT.toString(),tamañoRelojes);
    			
//	    	centralLayoutT6.addComponent(gaugeInc2L);
    			centralTop2.addComponent(gaugeInc2L,6,0);
    		}
    		
    		centralTop2.addComponent(indiceOEE2L(),7,0);
    	}

    	
//    	centralLayoutT7.addComponent(centralLayoutT5);
//    	centralLayoutT7.addComponent(centralLayoutT6);
    	
    }
    private void cargarSemanal(String r_ejercicio, String r_semana)
    {
//    	this.evolucionVentas();
//    	if (!isPrevision()) this.tendenciaAnualComparativa5y2L();
//    	if (!isPrevision()) this.ventas5L();
//    	if (!isPrevision()) this.ventas2L();
    	
//    	this.evolucionVentasAcumuladaActual5L();
//    	this.ventasAcumuladas5L();
//    	this.evolucionVentasAcumuladaActual2L();
//    	this.ventasAcumuladas2L();
    	
//    	this.coberturaStock5L();
//    	this.coberturaStock2L();
    }
    
    private void cargarAnual(String r_ejercicio)
    {
    	this.evolucionVentas();
    	this.tendenciaAnualComparativa5y2L();
    	this.ventas5L();
    	this.ventas2L();
    	
//    	this.evolucionVentasAcumuladaActual5L();
//    	this.ventasAcumuladas5L();
//    	this.evolucionVentasAcumuladaActual2L();
//    	this.ventasAcumuladas2L();
//    	this.coberturaStock5L();
//    	this.coberturaStock2L();
    	
    }
    
    private void cargarMeses(String r_ejercicio, String r_semana)
    {
    	/*
//    	 * GENERACION DATOS 5L
//    	 */
//    	Panel gauge = reloj.generarReloj("5L",42900, 50000, 75000, 35000, 49999, 0, 34999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(),150);
//    	
//    	centralLayoutT2.addComponent(gauge);
//
//    	/*
//    	 * GENERACION DATOS 2L
//    	 */
//    	Panel gauge2L = reloj.generarReloj("2L",33900, 45000, 75000, 30000, 44999, 0, 29999, 0, 75000,GaugeStyle.STYLE_DEFAULT.toString(),150);
//    	
//    	centralLayoutT3.addComponent(gauge2L);
//    	
//    	centralTop.addComponent(centralLayoutT2);
//    	centralTop.addComponent(centralLayoutT3);
//
//    	this.pieVentasTotales();
//    	this.tendenciaMedia5L();
//    	this.tendenciaMedia2L();
    	
    }
    
    private void cargarRegistros(String r_ejercicio)
    {
    	
    }

    private void cargarResumen()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";

    	Label lblProduccionTeorica = null;
    	Label lblValorProduccionTeorica= null;

    	Label lblProduccionTotal = null;
    	Label lblProduccion5L= null;
    	Label lblProduccionTinto5L= null;
    	Label lblProduccionRosa5L= null;
    	Label lblProduccion2L= null;
    	Label lblProduccionTinto2L= null;
    	Label lblProduccionRosa2L= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccion5L= null;
    	Label lblValorProduccionTinto5L= null;
    	Label lblValorProduccionRosa5L= null;
    	Label lblValorProduccion2L= null;
    	Label lblValorProduccionTinto2L= null;
    	Label lblValorProduccionRosa2L= null;
    	
    	Panel panel = new Panel("RESUMEN PRODUCCION");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PRODUCCION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin1 = new HorizontalLayout();
    	lin1.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);

    	if (isPrevision())
    	{
	    	lblProduccionTeorica = new Label();
	    	lblProduccionTeorica.setValue("Teorica: ");
	    	lblProduccionTeorica.setWidth(anchoLabelTitulo);    	
	    	lblProduccionTeorica.addStyleName("lblTitulo");
	
	    	lblValorProduccionTeorica = new Label();
	    	lblValorProduccionTeorica.setWidth(anchoLabelDatos);
	    	lblValorProduccionTeorica.setValue(RutinasNumericas.formatearDouble(((this.mapeo.getHashValores().get("0.horas.01030")-this.mapeo.getHashValores().get("0.pro.01030"))/8*58000)+((this.mapeo.getHashValores().get("0.horas.01031")-this.mapeo.getHashValores().get("0.pro.01031"))/8*48000)));
	    	lblValorProduccionTeorica.addStyleName("lblDatos");
	    	
	    	lin.addComponent(lblProduccionTeorica);
	    	lin.addComponent(lblValorProduccionTeorica);

    	}
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
    	if (isPrevision())
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010300")+this.mapeo.getHashValores().get("0.pre.010310")+this.mapeo.getHashValores().get("0.pre.010301")+this.mapeo.getHashValores().get("0.pre.010311")));    		
    	}
    	else
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0103")));
    	}
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin1.addComponent(lblProduccionTotal);
    	lin1.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccion5L = new Label();
	    	lblProduccion5L.setValue(" 5L: ");
	    	lblProduccion5L.setWidth(anchoLabelTitulo);
	    	lblProduccion5L.addStyleName("lblTituloCentrado");
	
	    	lblValorProduccion5L = new Label();
	    	lblValorProduccion5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010300")+this.mapeo.getHashValores().get("0.pre.010301")));
	    	}
	    	else
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.01030")));
	    	}
	    	lblValorProduccion5L.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblProduccion5L);
	    	lin2.addComponent(lblValorProduccion5L);
	    	
		    	lblProduccionTinto5L = new Label();
		    	lblProduccionTinto5L.setValue("Tinto 5L: ");
		    	lblProduccionTinto5L.setWidth(anchoLabelTitulo);
		    	lblProduccionTinto5L.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionTinto5L = new Label();
		    	lblValorProduccionTinto5L.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010300")));
		    	}
		    	else
		    	{
		    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010300")));
		    	}
		    	lblValorProduccionTinto5L.addStyleName("lblDatos");
		    	
		    	lblProduccionRosa5L = new Label();
		    	lblProduccionRosa5L.setValue("Rosado 5L: ");
		    	lblProduccionRosa5L.setWidth(anchoLabelTitulo);
		    	lblProduccionRosa5L.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionRosa5L = new Label();
		    	lblValorProduccionRosa5L.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010301")));
		    	}
		    	else
		    	{
		    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010301")));
		    	}
		    	lblValorProduccionRosa5L.addStyleName("lblDatos");
		    	
		    	lin3.addComponent(lblProduccionTinto5L);
		    	lin3.addComponent(lblValorProduccionTinto5L);
		    	lin4.addComponent(lblProduccionRosa5L);
		    	lin4.addComponent(lblValorProduccionRosa5L);
		
	    	lblProduccion2L = new Label();
	    	lblProduccion2L.setValue(" 2L: ");
	    	lblProduccion2L.setWidth(anchoLabelTitulo);
	    	lblProduccion2L.addStyleName("lblTituloCentrado");
	    	
	    	lblValorProduccion2L = new Label();
	    	lblValorProduccion2L.setWidth(anchoLabelDatos);
	    	
	    	if (isPrevision())
	    	{
	    		lblValorProduccion2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010310")+this.mapeo.getHashValores().get("0.pre.010311")));
	    	}
	    	else
	    	{
	    		lblValorProduccion2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.01031")));
	    	}

	    	lblValorProduccion2L.addStyleName("lblDatos");
	    	lin5.addComponent(lblProduccion2L);
	    	lin5.addComponent(lblValorProduccion2L);
	
		    	lblProduccionTinto2L = new Label();
		    	lblProduccionTinto2L.setValue("Tinto 2L: ");
		    	lblProduccionTinto2L.setWidth(anchoLabelTitulo);
		    	lblProduccionTinto2L.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionTinto2L = new Label();
		    	lblValorProduccionTinto2L.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010310")));
		    	}
		    	else
		    	{
		    		lblValorProduccionTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010310")));
		    	}
		    	lblValorProduccionTinto2L.addStyleName("lblDatos");
		    	
		    	lblProduccionRosa2L = new Label();
		    	lblProduccionRosa2L.setValue("Rosado 2L: ");
		    	lblProduccionRosa2L.setWidth(anchoLabelTitulo);
		    	lblProduccionRosa2L.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionRosa2L = new Label();
		    	lblValorProduccionRosa2L.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010311")));
		    	}
		    	else
		    	{
		    		lblValorProduccionRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010311")));
		    	}
		    	lblValorProduccionRosa2L.addStyleName("lblDatos");
		    	
		
		    	lin6.addComponent(lblProduccionTinto2L);
		    	lin6.addComponent(lblValorProduccionTinto2L);
		    	lin7.addComponent(lblProduccionRosa2L);
		    	lin7.addComponent(lblValorProduccionRosa2L);
		    	
    	if (isPrevision()) content.addComponent(lin);
    	content.addComponent(lin1);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	content.addComponent(lin7);
    	
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProduccion();
			}
		});

    	
    	centralTop.addComponent(panel,1,0);
    }
    
    private void cargarResumenStock()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	String traerVentas = null;
    	
    	Label lblStockTotal = null;
    	Label lblStock5L= null;
    	Label lblStockTinto5L= null;
    	Label lblStockRosa5L= null;
    	Label lblStock2L= null;
    	Label lblStockTinto2L= null;
    	Label lblStockRosa2L= null;

    	Label lblValorStockTotal = null;
    	Label lblValorStock5L= null;
    	Label lblValorStockTinto5L= null;
    	Label lblValorStockRosa5L= null;
    	Label lblValorStock2L= null;
    	Label lblValorStockTinto2L= null;
    	Label lblValorStockRosa2L= null;
    	
    	
    	traerVentas="0";
    	Panel panel = new Panel("RESUMEN STOCK");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION STOCK");
    		traerVentas="1";
    	}

//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);
    	
    	lblStockTotal = new Label();
    	lblStockTotal.setValue("Total: ");
    	lblStockTotal.setWidth(anchoLabelTitulo);    	
    	lblStockTotal.addStyleName("lblTitulo");

    	lblValorStockTotal = new Label();
    	lblValorStockTotal.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0103")));
    	}
    	else
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0103")+this.mapeo.getHashValores().get("0.pre.010300") + this.mapeo.getHashValores().get("0.pre.010310")+this.mapeo.getHashValores().get("0.pre.010301") + this.mapeo.getHashValores().get("0.pre.010311") -this.mapeo.getHashValores().get(traerVentas + ".venta.0103")*this.correctorVentasEjercicio));
    	}
    	lblValorStockTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblStockTotal);
    	lin.addComponent(lblValorStockTotal);
    	
	    	lblStock5L = new Label();
	    	lblStock5L.setValue(" 5L: ");
	    	lblStock5L.setWidth(anchoLabelTitulo);
	    	lblStock5L.addStyleName("lblTituloCentrado");
	
	    	lblValorStock5L = new Label();
	    	lblValorStock5L.setWidth(anchoLabelDatos);
	    	if (!isPrevision())
	    	{
	    		lblValorStock5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.01030")));
	    	}
	    	else
	    	{
	    		lblValorStock5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.01030")+this.mapeo.getHashValores().get("0.pre.010300")+this.mapeo.getHashValores().get("0.pre.010301")-this.mapeo.getHashValores().get(traerVentas + ".venta.01030")*this.correctorVentasEjercicio));
	    	}
	    	lblValorStock5L.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblStock5L);
	    	lin2.addComponent(lblValorStock5L);
	    	
		    	lblStockTinto5L = new Label();
		    	lblStockTinto5L.setValue("Tinto 5L: ");
		    	lblStockTinto5L.setWidth(anchoLabelTitulo);
		    	lblStockTinto5L.addStyleName("lblTituloDerecha");
		
		    	lblValorStockTinto5L = new Label();
		    	lblValorStockTinto5L.setWidth(anchoLabelDatos);
		    	if (!isPrevision())
		    	{
		    		lblValorStockTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010300")));
		    	}
		    	else
		    	{
		    		lblValorStockTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010300")+this.mapeo.getHashValores().get("0.pre.010300")-this.mapeo.getHashValores().get(traerVentas + ".venta.010300")*this.correctorVentasEjercicio));
		    	}
		    	lblValorStockTinto5L.addStyleName("lblDatos");
		    	
		    	lblStockRosa5L = new Label();
		    	lblStockRosa5L.setValue("Rosado 5L: ");
		    	lblStockRosa5L.setWidth(anchoLabelTitulo);
		    	lblStockRosa5L.addStyleName("lblTituloDerecha");
		
		    	lblValorStockRosa5L = new Label();
		    	lblValorStockRosa5L.setWidth(anchoLabelDatos);
		    	if (!isPrevision())
		    	{
		    		lblValorStockRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010301")));
		    	}
		    	else
		    	{
		    		lblValorStockRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010301")+this.mapeo.getHashValores().get("0.pre.010301")-this.mapeo.getHashValores().get(traerVentas + ".venta.010301")*this.correctorVentasEjercicio));
		    	}
		    	lblValorStockRosa5L.addStyleName("lblDatos");
		    	
	    		lin3.addComponent(lblStockTinto5L);
		    	lin3.addComponent(lblValorStockTinto5L);
		    	lin4.addComponent(lblStockRosa5L);
		    	lin4.addComponent(lblValorStockRosa5L);
		
	    	lblStock2L = new Label();
	    	lblStock2L.setValue(" 2L: ");
	    	lblStock2L.setWidth(anchoLabelTitulo);
	    	lblStock2L.addStyleName("lblTituloCentrado");
	    	
	    	lblValorStock2L = new Label();
	    	lblValorStock2L.setWidth(anchoLabelDatos);
	    	if (!isPrevision())
	    	{
	    		lblValorStock2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.01031")));
	    	}
	    	else
	    	{
	    		lblValorStock2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.01031")+this.mapeo.getHashValores().get("0.pre.010310")+this.mapeo.getHashValores().get("0.pre.010311")-this.mapeo.getHashValores().get(traerVentas + ".venta.01031")*this.correctorVentasEjercicio));
	    	}
	    	lblValorStock2L.addStyleName("lblDatos");
	    	lin5.addComponent(lblStock2L);
	    	lin5.addComponent(lblValorStock2L);
	
		    	lblStockTinto2L = new Label();
		    	lblStockTinto2L.setValue("Tinto 2L: ");
		    	lblStockTinto2L.setWidth(anchoLabelTitulo);
		    	lblStockTinto2L.addStyleName("lblTituloDerecha");
		
		    	lblValorStockTinto2L = new Label();
		    	lblValorStockTinto2L.setWidth(anchoLabelDatos);
		    	if (!isPrevision())
		    	{
		    		lblValorStockTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010310")));
		    	}
		    	else
		    	{
		    		lblValorStockTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010310")+this.mapeo.getHashValores().get("0.pre.010310")-this.mapeo.getHashValores().get(traerVentas + ".venta.010310")*this.correctorVentasEjercicio));
		    	}
		    	lblValorStockTinto2L.addStyleName("lblDatos");
		    	
		    	lblStockRosa2L = new Label();
		    	lblStockRosa2L.setValue("Rosado 2L: ");
		    	lblStockRosa2L.setWidth(anchoLabelTitulo);
		    	lblStockRosa2L.addStyleName("lblTituloDerecha");
		
		    	lblValorStockRosa2L = new Label();
		    	lblValorStockRosa2L.setWidth(anchoLabelDatos);
		    	if (!isPrevision())
		    	{
		    		lblValorStockRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010311")));
		    	}
		    	else
		    	{
		    		lblValorStockRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010311")+this.mapeo.getHashValores().get("0.pre.010311")-this.mapeo.getHashValores().get(traerVentas + ".venta.010311")*this.correctorVentasEjercicio));
		    	}
		    	lblValorStockRosa2L.addStyleName("lblDatos");
		
	    	lin6.addComponent(lblStockTinto2L);
	    	lin6.addComponent(lblValorStockTinto2L);
	    	lin7.addComponent(lblStockRosa2L);
	    	lin7.addComponent(lblValorStockRosa2L);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	content.addComponent(lin7);
    	
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
//    	if (isPrevision()) 
    	centralTop.addComponent(panel,3,0);
    }
    
    private void cargarResumenVentas()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblVentasTotal = null;
    	Label lblVentas5L= null;
    	Label lblVentasTinto5L= null;
    	Label lblVentasRosa5L= null;
    	Label lblVentas2L= null;
    	Label lblVentasTinto2L= null;
    	Label lblVentasRosa2L= null;

    	Label lblValorVentasTotal = null;
    	Label lblValorVentas5L= null;
    	Label lblValorVentasTinto5L= null;
    	Label lblValorVentasRosa5L= null;
    	Label lblValorVentas2L= null;
    	Label lblValorVentasTinto2L= null;
    	Label lblValorVentasRosa2L= null;
    	
    	
    	String traerVentas = "0";
    	Panel panel = new Panel("RESUMEN VENTAS");

    	if (isPrevision())
    	{
    		traerVentas="1";
    		panel.setCaption("RESUMEN VENTAS EJ. ANTERIOR" );
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);
    	
    	lblVentasTotal = new Label();
    	lblVentasTotal.setValue("Total: ");
    	lblVentasTotal.setWidth(anchoLabelTitulo);    	
    	lblVentasTotal.addStyleName("lblTitulo");

    	lblValorVentasTotal = new Label();
    	lblValorVentasTotal.setWidth(anchoLabelDatos);
    	lblValorVentasTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.0103")*correctorVentasEjercicio));
    	lblValorVentasTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblVentasTotal);
    	lin.addComponent(lblValorVentasTotal);
    	
	    	lblVentas5L = new Label();
	    	lblVentas5L.setValue(" 5L: ");
	    	lblVentas5L.setWidth(anchoLabelTitulo);
	    	lblVentas5L.addStyleName("lblTituloCentrado");
	
	    	lblValorVentas5L = new Label();
	    	lblValorVentas5L.setWidth(anchoLabelDatos);
	    	lblValorVentas5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.01030")*correctorVentasEjercicio));
	    	lblValorVentas5L.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblVentas5L);
	    	lin2.addComponent(lblValorVentas5L);
	    	
		    	lblVentasTinto5L = new Label();
		    	lblVentasTinto5L.setValue("Tinto 5L: ");
		    	lblVentasTinto5L.setWidth(anchoLabelTitulo);
		    	lblVentasTinto5L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasTinto5L = new Label();
		    	lblValorVentasTinto5L.setWidth(anchoLabelDatos);
		    	lblValorVentasTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010300")*correctorVentasEjercicio));
		    	lblValorVentasTinto5L.addStyleName("lblDatos");
		    	
		    	lblVentasRosa5L = new Label();
		    	lblVentasRosa5L.setValue("Rosado 5L: ");
		    	lblVentasRosa5L.setWidth(anchoLabelTitulo);
		    	lblVentasRosa5L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasRosa5L = new Label();
		    	lblValorVentasRosa5L.setWidth(anchoLabelDatos);
		    	lblValorVentasRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010301")*correctorVentasEjercicio));
		    	lblValorVentasRosa5L.addStyleName("lblDatos");
		    	
		    	lin3.addComponent(lblVentasTinto5L);
		    	lin3.addComponent(lblValorVentasTinto5L);
		    	lin4.addComponent(lblVentasRosa5L);
		    	lin4.addComponent(lblValorVentasRosa5L);
		
	    	lblVentas2L = new Label();
	    	lblVentas2L.setValue(" 2L: ");
	    	lblVentas2L.setWidth(anchoLabelTitulo);
	    	lblVentas2L.addStyleName("lblTituloCentrado");
	    	
	    	lblValorVentas2L = new Label();
	    	lblValorVentas2L.setWidth(anchoLabelDatos);
	    	lblValorVentas2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.01031")*correctorVentasEjercicio));
	    	lblValorVentas2L.addStyleName("lblDatos");
	    	lin5.addComponent(lblVentas2L);
	    	lin5.addComponent(lblValorVentas2L);
	
		    	lblVentasTinto2L = new Label();
		    	lblVentasTinto2L.setValue("Tinto 2L: ");
		    	lblVentasTinto2L.setWidth(anchoLabelTitulo);
		    	lblVentasTinto2L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasTinto2L = new Label();
		    	lblValorVentasTinto2L.setWidth(anchoLabelDatos);
		    	lblValorVentasTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010310")*correctorVentasEjercicio));
		    	lblValorVentasTinto2L.addStyleName("lblDatos");
		    	
		    	lblVentasRosa2L = new Label();
		    	lblVentasRosa2L.setValue("Rosado 2L: ");
		    	lblVentasRosa2L.setWidth(anchoLabelTitulo);
		    	lblVentasRosa2L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasRosa2L = new Label();
		    	lblValorVentasRosa2L.setWidth(anchoLabelDatos);
		    	lblValorVentasRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010311")*correctorVentasEjercicio));
		    	lblValorVentasRosa2L.addStyleName("lblDatos");
		    	
		
		    	lin6.addComponent(lblVentasTinto2L);
		    	lin6.addComponent(lblValorVentasTinto2L);
		    	lin7.addComponent(lblVentasRosa2L);
		    	lin7.addComponent(lblValorVentasRosa2L);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	content.addComponent(lin7);
    	
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,2,0);
    }
    
    private void cargarResumenHoras()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";

    	Label lblTurnos = null;
    	Label lblValorTurnos = null;

    	Label lblHoras5L = null;
    	Label lblValorHoras5L = null;
    	Label lblHoras2L = null;
    	Label lblValorHoras2L = null;
    	
    	Label lblInc5L = null;
    	Label lblValorInc5L = null;
    	Label lblInc2L = null;
    	Label lblValorInc2L = null;
    	
    	Label lblPro5L = null;
    	Label lblValorPro5L = null;
    	Label lblPro2L = null;
    	Label lblValorPro2L = null;
    	
    	Panel panel = new Panel("HORAS TRABAJADAS");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined();

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);

	    	lblTurnos = new Label();
	    	lblTurnos.setValue("Turnos: ");
	    	lblTurnos.setWidth(anchoLabelTitulo);
	    	lblTurnos.addStyleName("lblTitulo ");
	
	    	lblValorTurnos = new Label();
	    	lblValorTurnos.setWidth(anchoLabelDatos);
	    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble((this.mapeo.getHashValores().get("0.horas.01030")+this.mapeo.getHashValores().get("0.horas.01031"))/8));
	    	lblValorTurnos.addStyleName("lblDatos");

	    	lin.addComponent(lblTurnos);
	    	lin.addComponent(lblValorTurnos);

	    	lblHoras5L = new Label();
	    	lblHoras5L.setValue("Horas 5L: ");
	    	lblHoras5L.setWidth(anchoLabelTitulo);
	    	lblHoras5L.addStyleName("lblTitulo ");
	
	    	lblValorHoras5L = new Label();
	    	lblValorHoras5L.setWidth(anchoLabelDatos);
	    	lblValorHoras5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.01030")));
	    	lblValorHoras5L.addStyleName("lblDatos");

	    	lin2.addComponent(lblHoras5L);
	    	lin2.addComponent(lblValorHoras5L);
	    	
	    	lblInc5L = new Label();
	    	lblInc5L.setValue("Incidencias 5L: ");
	    	lblInc5L.setWidth(anchoLabelTitulo);
	    	lblInc5L.addStyleName("lblTitulo ");
	    	
	    	lblValorInc5L = new Label();
	    	lblValorInc5L.setWidth(anchoLabelDatos);
	    	lblValorInc5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.inc.01030")));
	    	lblValorInc5L.addStyleName("lblDatos");

	    	lin3.addComponent(lblInc5L);
	    	lin3.addComponent(lblValorInc5L);

	    	lblPro5L = new Label();
	    	lblPro5L.setValue("Programadas 5L: ");
	    	lblPro5L.setWidth(anchoLabelTitulo);
	    	lblPro5L.addStyleName("lblTitulo ");
	    	
	    	lblValorPro5L = new Label();
	    	lblValorPro5L.setWidth(anchoLabelDatos);
	    	lblValorPro5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pro.01030")));
	    	lblValorPro5L.addStyleName("lblDatos");
	    	
	    	lin4.addComponent(lblPro5L);
	    	lin4.addComponent(lblValorPro5L);

	    	lblHoras2L = new Label();
	    	lblHoras2L.setValue("Horas 2L: ");
	    	lblHoras2L.setWidth(anchoLabelTitulo);
	    	lblHoras2L.addStyleName("lblTitulo");
	    	
	    	lblValorHoras2L = new Label();
	    	lblValorHoras2L.setWidth(anchoLabelDatos);
	    	lblValorHoras2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.01031")));
	    	lblValorHoras2L.addStyleName("lblDatos");

	    	lin5.addComponent(lblHoras2L);
	    	lin5.addComponent(lblValorHoras2L);
	    	
	    	lblInc2L = new Label();
	    	lblInc2L.setValue("Incidencias 2L: ");
	    	lblInc2L.setWidth(anchoLabelTitulo);
	    	lblInc2L.addStyleName("lblTitulo");
	
	    	lblValorInc2L = new Label();
	    	lblValorInc2L.setWidth(anchoLabelDatos);
	    	lblValorInc2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.inc.01031")));
	    	lblValorInc2L.addStyleName("lblDatos");

	    	lin6.addComponent(lblInc2L);
	    	lin6.addComponent(lblValorInc2L);
	    	
	    	lblPro2L = new Label();
	    	lblPro2L.setValue("Programadas 2L: ");
	    	lblPro2L.setWidth(anchoLabelTitulo);
	    	lblPro2L.addStyleName("lblTitulo");
	
	    	lblValorPro2L = new Label();
	    	lblValorPro2L.setWidth(anchoLabelDatos);
	    	lblValorPro2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pro.01031")));
	    	lblValorPro2L.addStyleName("lblDatos");

	    	lin7.addComponent(lblPro2L);
	    	lin7.addComponent(lblValorPro2L);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	content.addComponent(lin7);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,0,0);
    	
    }
    
    private Panel indiceOEE5L()
    {
    	Double disponibilidad = null;
    	Double productividad = null;
    	Double calidad = null;
    	Double oeeRdo = null;
    	Label interpretacionOEE = null;
    	Panel gaugeOEE5L = null;
    	
    	Panel panelOEE = new Panel("Indice OEE 5L");
    	panelOEE.setSizeUndefined();
    	
    	Label resultado = new Label();
    	VerticalLayout contenido = new VerticalLayout();
    	
    	
		disponibilidad = obtenerDisponibilidad(this.mapeo.getHashValores().get("0.horas.01030"), this.mapeo.getHashValores().get("0.inc.01030"),this.mapeo.getHashValores().get("0.pro.01030"));
		productividad = obtenerProductividad("5L", this.mapeo.getHashValores().get("0.prod.01030"), this.mapeo.getHashValores().get("0.horas.01030"), this.mapeo.getHashValores().get("0.inc.01030"), this.mapeo.getHashValores().get("0.pro.01030"));
		calidad = obtenerCalidad (this.mapeo.getHashValores().get("0.prod.01030"), this.mapeo.getHashValores().get("0.mer.01030"));
		
		oeeRdo = disponibilidad * productividad * calidad *100;
		gaugeOEE5L = reloj.generarReloj("",oeeRdo.intValue(), 75, 100, 60, 74, 0, 59, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
		
		interpretacionOEE = obtenerInterpretacion(oeeRdo);
		
		Double teep =teep("0103", oeeRdo);
		
		contenido.addComponent(gaugeOEE5L);
		contenido.addComponent(interpretacionOEE);
		contenido.setComponentAlignment(interpretacionOEE, Alignment.MIDDLE_CENTER);
    	panelOEE.setContent(contenido);
    	
    	return panelOEE;
    }
    
    private Panel indiceOEE2L()
    {
    	Double disponibilidad = null;
    	Double productividad = null;
    	Double calidad = null;
    	Double oeeRdo = null;
    	Label interpretacionOEE = null;
    	Panel gaugeOEE5L = null;
    	
    	Panel panelOEE = new Panel("Indice OEE 2L");
    	panelOEE.setSizeUndefined();
    	Label resultado = new Label();
    	VerticalLayout contenido = new VerticalLayout();
    	
    	
		disponibilidad = obtenerDisponibilidad(this.mapeo.getHashValores().get("0.horas.01031"), this.mapeo.getHashValores().get("0.inc.01031"),this.mapeo.getHashValores().get("0.pro.01031"));
		productividad = obtenerProductividad("5L", this.mapeo.getHashValores().get("0.prod.01031"), this.mapeo.getHashValores().get("0.horas.01031"), this.mapeo.getHashValores().get("0.inc.01031"), this.mapeo.getHashValores().get("0.pro.01031"));
		calidad = obtenerCalidad (this.mapeo.getHashValores().get("0.prod.01031"), this.mapeo.getHashValores().get("0.mer.01031"));
		
		oeeRdo = disponibilidad * productividad * calidad *100;
		gaugeOEE5L = reloj.generarReloj("",oeeRdo.intValue(), 75, 100, 60, 74, 0, 59, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
		
		interpretacionOEE = obtenerInterpretacion(oeeRdo);
		
		Double teep =teep("0103", oeeRdo);
		
		contenido.addComponent(gaugeOEE5L);
		contenido.addComponent(interpretacionOEE);
		contenido.setComponentAlignment(interpretacionOEE, Alignment.MIDDLE_CENTER);
    	panelOEE.setContent(contenido);
    	
    	return panelOEE;
    }


    private Label obtenerInterpretacion(Double r_oee)    
    {
    	Label resultado = new Label();
    	resultado.setWidth("100%");
    	
    	if (r_oee < 65)
    	{
    		resultado.setValue("INADMISIBLE");
    		resultado.addStyleName("oeeMalo");
    	}
    	if (r_oee >= 65 && r_oee < 75)
    	{
    		resultado.setValue("REGULAR");
    		resultado.addStyleName("oeeRegular");
    	}
    	if (r_oee >= 75 && r_oee < 85)
    	{
    		resultado.setValue("ACEPTABLE");
    		resultado.addStyleName("oeeAceptable");
    	}
    	if (r_oee >= 85 && r_oee < 95)
    	{
    		resultado.setValue("BUENA");
    		resultado.addStyleName("oeeBuena");
    	}
    	if (r_oee > 95)
    	{
    		resultado.setValue("EXCELENTE");
    		resultado.addStyleName("oeeExcelente");
    	}
    	
    	return resultado;
    }
    
    private double teep(String r_area, Double r_oee)
    {
    	/*
    	 * teep = oee * utilizacion
    	 * 
    	 * utilizacion = tiempo planificado / tiempo total año
    	 * 
    	 */
    	Double teep = null;
    	Double utilizacion = null;
    	
    	if (r_area.contentEquals("0103"))
    	{
    		utilizacion = consultaDashboardEnvasadoraServer.horasTurno*5/(24*7);
    	}
    	else
    	{
    		utilizacion = 2*consultaDashboardEnvasadoraServer.horasTurno*5/(24*7);
    	}
    	teep = r_oee * utilizacion;
    	return teep;
    }
    
    private double obtenerProductividad(String r_area, Double r_produccion, Double r_horas, Double r_incidencias, Double r_programadas)
    {
    	Double tiempoCiclo = null;
    	Double velocidadNominal = null;
    	Double produccionNominal = null;
    	Double productividad = null;
    	Double tiempoOperativo = null;
    
    	switch (r_area)
    	{
    		case "5L":
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				velocidadNominal = new Double(consultaDashboardEnvasadoraServer.produccionObjetivo5L / 8);
    			else
    				velocidadNominal = new Double(consultaDashboardEnvasadoraServer.produccionObjetivo5G / 8);
    			break;
    		}
    		case "2L":
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				velocidadNominal = new Double(consultaDashboardEnvasadoraServer.produccionObjetivo2L / 8);
    			else
    				velocidadNominal = new Double(consultaDashboardEnvasadoraServer.produccionObjetivo2G / 8);
    			break;
    		}
    	}
    	tiempoOperativo = r_horas - r_incidencias - r_programadas;
    	
    	tiempoCiclo = new Double(1)/velocidadNominal;

//    	produccionNominal = velocidadNominal * tiempoOperativo;
    	
    	productividad = tiempoCiclo/(tiempoOperativo/r_produccion);
    	return productividad;
    }
    
    private double obtenerCalidad(Double r_produccion, Double r_mermas)
    {
    	return (r_produccion - r_mermas)/(r_produccion);
    }

    private double obtenerDisponibilidad(Double r_horas, Double r_incidencias, Double r_programadas)
    {
    	return (r_horas - r_incidencias - r_programadas)/(r_horas - r_programadas);
    }
    
    private void evolucionVentas()
	{
		
    	
    	HashMap<String, Double> hashVentas = null;
    	HashMap<String, Double> hashVentasAnterior = null;
    	String tituloY=null;
    	
    	consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
    	hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "0103",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-1, "0103",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	
    	if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Semana"; else tituloY="Grfs. / Semana";
    	JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Ventas ", "Semanas", tituloY, hashVentas, hashVentasAnterior, this.txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), "306px", "300px",0,0,0);

//		HashMap<String, Double> hashVentas = null;
//		HashMap<String, Double> hashVentasAnterior = null;
//		
//		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
//		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()),"0103",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());
//	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue())-1,"0103",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());
//	    String tituloY = null;
//	    
//	    if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Semana"; else tituloY="Grfs. / Semana";
//	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Ventas", "Semanas", tituloY, hashVentas, hashVentasAnterior, txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), "510px", "250px",0);
	    
	    if (isPrevision()) centralTop2.addComponent(wrapper,0,0); else centralTop.addComponent(wrapper,4,0);
	    
	}
    
    private void ventas5L()
    {
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103001",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());
	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103004",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());
	    
	    JFreeChartWrapper wrapper = linea.generarGrafico("Ventas Palets y 1/2 Paletas de 5L", "Semanas", "Ltrs. / Semana", hashVentas, hashVentasAnterior, "Palets", "1/2 Paletas", "510px", "300px",0,0,0);
	    
	    centralMiddle.addComponent(wrapper,1,0);
	    
    }
    
    private void ventas2L()
    {
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103100",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());
	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103104",this.cmbCalculo.getValue().toString(),this.cmbSemana.getValue().toString());

	    JFreeChartWrapper wrapper = linea.generarGrafico("Ventas Palets y 1/2 Paletas de 2L", "Semanas", "Ltrs. / Semana", hashVentas, hashVentasAnterior, "Palets", "1/2 Paletas", "510px", "300px",0,0,0);
	    
	    centralMiddle.addComponent(wrapper,2,0);
	} 


    private void tendenciaAnualComparativa5y2L()
    {
    	HashMap<String, Double> hashVentas5 = null;
    	HashMap<String, Double> hashVentas2 = null;
    	String tituloY=null;
    	
    	consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
    	hashVentas5 = cdes.recuperarVentasComparativaEjercicios(new Integer(this.txtEjercicio.getValue())-1, "01030", 5,this.cmbCalculo.getValue().toString());
    	hashVentas2 = cdes.recuperarVentasComparativaEjercicios(new Integer(this.txtEjercicio.getValue())-1, "01031", 5,this.cmbCalculo.getValue().toString());
    	
    	if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Ejercicio"; else tituloY="Grfs. / Ejercicio";
    	JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Anual por Formato", "Ejercicios", tituloY, hashVentas5, hashVentas2, "Ventas 5L", "Ventas 2L", "510px", "300px",new Integer(txtEjercicio.getValue())-6,0,0);
    	
    	centralMiddle.addComponent(wrapper,0,0);
    	
    }

    private void tendenciaMediaActual5L()
    {
    	HashMap<String, Double> hashProduccion5 = null;
    	HashMap<String, Double> hashProduccion2 = null;
    	HashMap<String, Double> hashHoras5 = null;
    	HashMap<String, Double> hashHoras2 = null;
    	HashMap<String, Double> hashMedia5 = null;
    	HashMap<String, Double> hashMedia2 = null;
    	
    	consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
    	consultaHorasTrabajadasEnvasadoraServer ches = new consultaHorasTrabajadasEnvasadoraServer(CurrentUser.get());
    	hashProduccion5 = cdes.recuperarProduccionFormatoSemana(new Integer(this.txtEjercicio.getValue()), this.cmbSemana.getValue().toString(),this.cmbCalculo.getValue().toString(),"5");
    	hashHoras5 = ches.recuperarHorasFormatoSemanas(new Integer(this.txtEjercicio.getValue()), new Integer(this.cmbSemana.getValue().toString()), "5");
    	hashMedia5 = new HashMap<String, Double>();
    	
    	Double media = new Double(0);
    	for (int i = 1; i<= new Integer(this.cmbSemana.getValue().toString());i++)
    	{
    		if (hashProduccion5.get(String.valueOf(i))==0)
    		{
    			hashMedia5.put(String.valueOf(i),media);
    		}
    		else
    		{
    			media= (hashProduccion5.get(String.valueOf(i))/hashHoras5.get(String.valueOf(i))*8);
    			hashMedia5.put(String.valueOf(i),media);
    		}
    	}

    	hashProduccion2 = cdes.recuperarProduccionFormatoSemana(new Integer(this.txtEjercicio.getValue()), this.cmbSemana.getValue().toString(),this.cmbCalculo.getValue().toString(),"2");
    	hashHoras2 = ches.recuperarHorasFormatoSemanas(new Integer(this.txtEjercicio.getValue()), new Integer(this.cmbSemana.getValue().toString()), "2");
    	hashMedia2 = new HashMap<String, Double>();
    	
    	Double media2 = new Double(0);
    	for (int i = 1; i<= new Integer(this.cmbSemana.getValue().toString());i++)
    	{
    		if (hashProduccion2.get(String.valueOf(i))==0)
    		{
    			hashMedia2.put(String.valueOf(i),media2);
    		}
    		else
    		{
    			media2= (hashProduccion2.get(String.valueOf(i))/hashHoras2.get(String.valueOf(i))*8);
    			hashMedia2.put(String.valueOf(i),media2);
    		}
    	}

    	JFreeChartWrapper wrapper = linea.generarGrafico("Media Produccion Formato", "Semanas", "Litros", hashMedia5, hashMedia2, "Media 5L", "Media 2L", "350px", "250px",0,0,0);
    	
    	centralTop2.addComponent(wrapper,7,0);
    	
    }


    private void evolucionVentasAcumuladaActual5L()
    {
    	HashMap<String, Double> hashVentas = null;
    	HashMap<String, Double> hashVentasAnterior = null;
    	String tituloY=null;
    	
    	consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
    	hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "01030",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-1, "01030",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	
    	if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Ejercicio"; else tituloY="Grfs. / Ejercicio";
    	JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Ventas 5L", "Semanas", tituloY, hashVentas, hashVentasAnterior, "Ventas 5L", "Ventas 5L Anterior", "306px", "300px",0,0,0);
    	
    	centralMiddle2.addComponent(wrapper,1,0);
    	
    }
    
    
    private void evolucionVentasAcumuladaActual2L()
    {
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		String tituloY=null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "01031",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
		hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-1, "01031",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
		
		if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Ejercicio"; else tituloY="Grfs. / Ejercicio";
		JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Ventas 2L", "Semanas", tituloY, hashVentas, hashVentasAnterior, "Ventas 2L", "Ventas 2L Anterior", "306px", "300px",0,0,0);
	    
	    centralMiddle2.addComponent(wrapper,2,0);
	    
    }

    

    private void ventasAcumuladas5L()
    {
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "0103001",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
	    hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "0103004",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
	    
	    JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Palets y 1/2 Paletas de 5L", "Semanas", "Ltrs. / Semana", hashVentas, hashVentasAnterior, "Palets", "1/2 Paletas", "306px", "300px",0,0,0);
	    
	    centralMiddle2.addComponent(wrapper,3,0);
	    
    }
    private void ventasAcumuladas2L()
    {
    	HashMap<String, Double> hashVentas = null;
    	HashMap<String, Double> hashVentasAnterior = null;
    	
    	consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
    	hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "0103100",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()), "0103104",  this.cmbCalculo.getValue().toString(), this.cmbSemana.getValue().toString());
    	
    	JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Palets y 1/2 Paletas de 2L", "Semanas", "Ltrs. / Semana", hashVentas, hashVentasAnterior, "Palets", "1/2 Paletas", "306px", "300px",0,0,0);
    	
    	centralMiddle2.addComponent(wrapper,4,0);
    	
    }
    
    private void pieProdTotales()
    {
    	String destacar = null;
    	MapeoValores map = null;
    	int numero = 0;
    	
    	HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	
    	if (this.mapeo.getHashValores().get("0.prod.010300")!=null && this.mapeo.getHashValores().get("0.prod.010300").doubleValue()!=0)
    	{
    		map = new MapeoValores();
	    	map.setClave("TINTO");
	    	map.setValorDouble(this.mapeo.getHashValores().get("0.prod.010300"));
	    	vector.add(map);
	    	destacar = map.getClave();
	    	hash.put(numero, new Color(130,0,19));
	    	numero+=1;
    	}
    	
    	if (this.mapeo.getHashValores().get("0.prod.010301")!=null && this.mapeo.getHashValores().get("0.prod.010301").doubleValue()!=0)
    	{
	    	map = new MapeoValores();
	    	map.setClave("ROSADO");
	    	map.setValorDouble(this.mapeo.getHashValores().get("0.prod.010301"));
	    	vector.add(map);
			hash.put(numero, new Color(255,176,188));
    	}
    	
    	if(!vector.isEmpty())
    	{
    		Panel grafico = queso.generarGrafico("REPARTO POR COLOR 5L", "200px","222px", vector, destacar, hash);
    		centralTop2.addComponent(grafico,0,0);
    	}

    }

    private void pieProdTotales2()
    {
    	String destacar = null;
    	MapeoValores map = null;
    	HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
    	int numero = 0;
    	
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	
    	
    	if (this.mapeo.getHashValores().get("0.prod.010310")!=null && this.mapeo.getHashValores().get("0.prod.010310").doubleValue()!=0)
    	{
    		map = new MapeoValores();
	    	map.setClave("TINTO");
	    	map.setValorDouble(this.mapeo.getHashValores().get("0.prod.010310"));
	    	vector.add(map);
	    	destacar = map.getClave();
	    	hash.put(numero, new Color(130,0,19));
	    	numero+=1;
    	}
    	
    	if (this.mapeo.getHashValores().get("0.prod.010311")!=null && this.mapeo.getHashValores().get("0.prod.010311").doubleValue()!=0)
    	{
	    	map = new MapeoValores();
	    	map.setClave("ROSADO");
	    	map.setValorDouble(this.mapeo.getHashValores().get("0.prod.010311"));
	    	vector.add(map);
	    	hash.put(numero, new Color(255,176,188));
    	}
    	
    	if (!vector.isEmpty())
    	{
    		Panel grafico = queso.generarGrafico("REPARTO POR COLOR 2L", "200px","222px", vector, destacar, hash);

    		centralTop2.addComponent(grafico,4,0);
    	}
    }

	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
			r_ejercicio = this.txtEjercicio.getValue();
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		if (RutinasFechas.semanaActual(r_ejercicio)-1 < 1 )
		{
			this.semana = "53";
			this.txtEjercicio.setValue(String.valueOf((new Integer(RutinasFechas.añoActualYYYY())-1)));
			r_ejercicio = this.txtEjercicio.getValue();
		}
		else
		{
			this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio)-1);
		}
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}

    private void ejecutarTimer()
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }
	
    private void vaciarPantalla()
    {
    	centralTop=null;
        centralMiddle=null;
        centralBottom=null;
        topLayout=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	
	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco dashboard envasadora " + new Date());
    		refrescar();
    	}
    }


	private void coberturaStock5L()
	{
		ArrayList<MapeoBarras> vector = null;
		ArrayList<MapeoArticulos> vector_articulos = null;
		MapeoBarras mapeoGrafico = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		
		vector_articulos = cas.obtenerArticulos("01030");
		
		HashMap<String,	Double> hashVentas = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue()),"01030",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashVentasAnterior = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue())-1,"01030",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashStock = cdes.recuperarStockArticulo("01030",this.cmbCalculo.getValue().toString());
		
		vector = new ArrayList<MapeoBarras>();
		
		for (int i = 0; i<vector_articulos.size();i++)
		{
			mapeoGrafico = new MapeoBarras();
			MapeoArticulos mapeo = (MapeoArticulos) vector_articulos.get(i);
			
			mapeoGrafico.setClave(mapeo.getArticulo());
			mapeoGrafico.setValorActual(hashStock.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie1(hashVentas.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie2(hashVentasAnterior.get(mapeo.getArticulo()));
			
			vector.add(mapeoGrafico);
		}
		
		
		
		JFreeChartWrapper wrapper = barras.generarGrafico("Cobertura Stock - Venta Media Semanal 5L", "Referencias 5L", "Stock vs Ventas", "765px", "400px", vector,"Actual", "Venta Media " + txtEjercicio.getValue(), "Venta Media " + String.valueOf(new Integer(txtEjercicio.getValue()).intValue()-1),false);

	    centralBottom.addComponent(wrapper,0,0);
	    
	}
	
	private void coberturaStock2L()
	{
		ArrayList<MapeoBarras> vector = null;
		ArrayList<MapeoArticulos> vector_articulos = null;
		MapeoBarras mapeoGrafico = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		
		vector_articulos = cas.obtenerArticulos("01031");
		
		HashMap<String,	Double> hashVentas = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue()),"01031",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashVentasAnterior = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue())-1,"01031",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashStock = cdes.recuperarStockArticulo("01031",this.cmbCalculo.getValue().toString());
		
		vector = new ArrayList<MapeoBarras>();
		
		for (int i = 0; i<vector_articulos.size();i++)
		{
			mapeoGrafico = new MapeoBarras();
			MapeoArticulos mapeo = (MapeoArticulos) vector_articulos.get(i);
			
			mapeoGrafico.setClave(mapeo.getArticulo());
			mapeoGrafico.setValorActual(hashStock.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie1(hashVentas.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie2(hashVentasAnterior.get(mapeo.getArticulo()));
			
			vector.add(mapeoGrafico);
		}
		
		
		
		JFreeChartWrapper wrapper = barras.generarGrafico("Cobertura Stock - Venta Media Semanal 2L", "Referencias 2L", "Stock vs. Ventas", "765px", "400px", vector, "Actual", "Venta Media " + txtEjercicio.getValue(), "Venta Media " + String.valueOf(new Integer(txtEjercicio.getValue()).intValue()-1),false);

	    centralBottom.addComponent(wrapper,1,0);
	}

    private void pantallaProduccion()
    {
    	pantallaLineasProduccion vt = new pantallaLineasProduccion("Envasadora", new Integer(this.txtEjercicio.getValue().toString()), this.cmbSemana.getValue().toString(), "produccion semana " + this.cmbSemana.getValue().toString(), this.cmbCalculo.getValue().toString());
    	getUI().addWindow(vt);
    }

	public boolean isPrevision() {
		return prevision;
	}

	public void setPrevision(boolean prevision) {
		
		if (prevision) this.correctorVentasEjercicio=new Double(1.1); else this.correctorVentasEjercicio=new Double(1);
		this.prevision = prevision;
	}
}
