package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view.pantallaLineasAlbaranesTraslados;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.modelo.MapeoAlmacenes;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardExistencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardExistencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardExistenciasServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMStock.pantallaEvolucionEjercicios;
import borsao.e_borsao.Modulos.GENERALES.Ejercicios.server.consultaEjerciciosServer;
import borsao.e_borsao.Modulos.GENERALES.Familias.modelo.MapeoFamilias;
import borsao.e_borsao.Modulos.GENERALES.Familias.server.consultaFamiliasServer;
import borsao.e_borsao.Modulos.GENERALES.SubFamilias.modelo.MapeoSubFamilias;
import borsao.e_borsao.Modulos.GENERALES.SubFamilias.server.consultaSubFamiliasServer;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view.pantallaEvolucion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardExistenciasView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Existencias";
	private final String titulo = "CUADRO MANDO EXISTENCIAS";
	
	private CssLayout barAndGridLayout = null;    

	private HorizontalLayout topLayout = null;
		private Label lblTitulo = null; 
		private Button opcSalir = null;

	private HorizontalLayout cabLayout = null;
		private Panel panelSelectores =null;
			private GridLayout selectoresDatos = null;
				private Panel panelEjercicios =null;
				private Panel panelAlmacenes =null;
				private Panel panelFamilias =null;
				private Panel panelSubFamilias =null;
				private Combo cmbMes= null;
				private Combo cmbTipoArticulo= null;
				private Combo cmbTipoArticuloPTPS= null;
				private Button procesar=null;
				private TextField aliasArticulo=null;

	private HorizontalLayout middleLayout = null;    
		private Panel panelResumen =null;
			private GridLayout centralMiddle = null;
				private Panel panelResumenFamilias =null;
			// 	Dos Relojes para mostrar datos stock familias en unidades y en importe
			
	private HorizontalLayout bottomLayout = null;    
		private Panel panelDetalleFamilias =null;
			private GridLayout centralMiddle1 = null;
				private Panel panelResumenSubfamilias =null;
				private Panel panelResumenAñadas =null;

	private HorizontalLayout footerLayout = null;    
		private Panel panelDetalleArticulos =null;
			private GridLayout centralMiddle2 = null;
				private Panel panelResumenArticulos=null;
				private Panel panelResumenArticulosAñada=null;

	private Grid gridSubFamilias = null;
	private Grid gridFamilias = null;
	private Grid gridAlmacenes = null;
	private Grid gridEjercicios = null;
	
	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardExistencias mapeo=null;
	private StringBuilder almacenesSeleccionados = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */

    public DashboardExistenciasView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
    	this.cargarCombo();
    	this.cargarListeners();
    	this.cargarSelectores();
    	
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new CssLayout();
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	        /*
	         * Zona para la selección de datos a consultar
	         * 
	         *  el boton procesar cargará el siguiente panel
	         */
	    	this.topLayout = new HorizontalLayout();
	    	this.topLayout.setSpacing(true);
	    	this.topLayout.setSizeUndefined();
	    	this.topLayout.setMargin(true);
	    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
	        	this.cmbMes = new Combo("Cálculo Existencias");
	        	this.cmbMes.setNullSelectionAllowed(false);
	        	this.cmbMes.setNewItemsAllowed(false);
	        	this.cmbMes.setRequired(true);
	        	this.cmbMes.addStyleName(ValoTheme.COMBOBOX_TINY);
	        	
	        	this.cmbTipoArticulo = new Combo("Articulos a Consultar");
	        	this.cmbTipoArticulo.setNullSelectionAllowed(false);
	        	this.cmbTipoArticulo.setNewItemsAllowed(false);
	        	this.cmbTipoArticulo.setRequired(true);
	        	this.cmbTipoArticulo.addStyleName(ValoTheme.COMBOBOX_TINY);
	        	
	        	this.cmbTipoArticuloPTPS = new Combo("Tipo Articulo ");
	        	this.cmbTipoArticuloPTPS.setNullSelectionAllowed(false);
	        	this.cmbTipoArticuloPTPS.setNewItemsAllowed(false);
	        	this.cmbTipoArticuloPTPS.setRequired(true);
	        	this.cmbTipoArticuloPTPS.addStyleName(ValoTheme.COMBOBOX_TINY);
	        	
	        	this.aliasArticulo = new TextField();
	        	this.aliasArticulo.setCaption("Alias");
	        	this.aliasArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
	        	
	        	this.procesar=new Button("Procesar");
	        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
	        	this.procesar.setIcon(FontAwesome.PRINT);
		    	
	        	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
		    	
	    	this.topLayout.addComponent(this.opcSalir);
	    	this.topLayout.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
	    	
        	this.topLayout.addComponent(this.cmbMes);
        	this.topLayout.addComponent(this.cmbTipoArticulo);
        	this.topLayout.addComponent(this.cmbTipoArticuloPTPS);
        	this.topLayout.addComponent(this.aliasArticulo);
        	this.topLayout.setComponentAlignment(this.aliasArticulo,  Alignment.BOTTOM_LEFT);
        	this.topLayout.addComponent(this.procesar);
        	this.topLayout.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
	    	this.topLayout.addComponent(this.lblTitulo);
	    	this.topLayout.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	
	    	/*
	    	 * Panel que contiene los selectores
	    	 * 
	    	 * contiene un grid layour para 5 paneles
	    	 * ejercicio, almacenes, familias, subfamilias, mes  
	    	 */
	    	this.cabLayout = new HorizontalLayout();
	    	this.cabLayout.setSpacing(true);
	    	this.cabLayout.setSizeUndefined();
	    	this.cabLayout.setWidth("100%");
	    	this.cabLayout.setMargin(true);
	    	
		    	panelSelectores = new Panel("Selección Datos");
		    	panelSelectores.setSizeUndefined();
		    	panelSelectores.setWidth("100%");
		    	panelSelectores.setVisible(true);
		    	panelSelectores.addStyleName("showpointer");
		    	new PanelCaptionBarToggler<Panel>( panelSelectores );
		    	
			    	this.selectoresDatos= new GridLayout(4,2);
			    	this.selectoresDatos.setSizeUndefined();
			    	this.selectoresDatos.setSpacing(true);
			    	this.selectoresDatos.setMargin(true);
			    	this.selectoresDatos.setWidth("100%");
			    	this.selectoresDatos.setMargin(true);
			    	
	        	this.panelSelectores.setContent(this.selectoresDatos);
        	
        	this.cabLayout.addComponent(this.panelSelectores);

	    	this.middleLayout = new HorizontalLayout();
	    	this.middleLayout.setSpacing(true);
	    	this.middleLayout.setSizeUndefined();
	    	this.middleLayout.setWidth("100%");
	    	this.middleLayout.setMargin(true);

		    	panelResumen = new Panel("Resumen Familias ");
			    panelResumen.setSizeUndefined();
			    panelResumen.setWidth("100%");
			    panelResumen.setVisible(false);
			    panelResumen.addStyleName("showpointer");
			    new PanelCaptionBarToggler<Panel>( panelResumen );
	
				    this.centralMiddle = new GridLayout(4,1);
				    this.centralMiddle.setWidth("100%");
				    this.centralMiddle.setSpacing(true);
				    this.centralMiddle.setMargin(true);
	
		    	panelResumen.setContent(centralMiddle);
		    	panelResumen.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
	    	this.middleLayout.addComponent(panelResumen);
	    	
	    	this.bottomLayout = new HorizontalLayout();
	    	this.bottomLayout.setSpacing(true); 
	    	this.bottomLayout.setSizeUndefined();
	    	this.bottomLayout.setWidth("100%");
	    	this.bottomLayout.setMargin(true);
			    	
		    	panelDetalleFamilias = new Panel("Detalle Familias");
		    	panelDetalleFamilias.setSizeFull();
			    panelDetalleFamilias.setWidth("100%");
			    panelDetalleFamilias.addStyleName("showpointer");
			    panelDetalleFamilias.setVisible(false);
			    new PanelCaptionBarToggler<Panel>( panelDetalleFamilias );
	
			    	this.centralMiddle1 = new GridLayout(2,1);
			    	this.centralMiddle1.setWidth("100%");
			    	this.centralMiddle1.setMargin(true);
			    	this.centralMiddle1.setSpacing(true);
				    	
		    	panelDetalleFamilias.setContent(centralMiddle1);
		    	panelDetalleFamilias.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
		    	this.bottomLayout.addComponent(panelDetalleFamilias);

	    	this.footerLayout = new HorizontalLayout();
	    	this.footerLayout.setSpacing(true); 
	    	this.footerLayout.setSizeUndefined();
	    	this.footerLayout.setWidth("100%");
	    	this.footerLayout.setMargin(true);
		    	
		    	panelDetalleArticulos= new Panel("Detalle Articulos");
		    	panelDetalleArticulos.setSizeFull();
		    	panelDetalleArticulos.setWidth("100%");
		    	panelDetalleArticulos.addStyleName("showpointer");
		    	panelDetalleArticulos.setVisible(false);
		    	new PanelCaptionBarToggler<Panel>( panelDetalleArticulos );
		    	
			    	this.centralMiddle2 = new GridLayout(2,1);
			    	this.centralMiddle2.setWidth("100%");
			    	this.centralMiddle2.setMargin(true);
			    	this.centralMiddle2.setSpacing(true);
		    	
		    	panelDetalleArticulos.setContent(centralMiddle2);
		    	panelDetalleArticulos.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
		    	this.footerLayout.addComponent(panelDetalleArticulos);

    	this.barAndGridLayout.addComponent(this.topLayout);
    	this.barAndGridLayout.addComponent(this.cabLayout);
    	this.barAndGridLayout.addComponent(this.middleLayout);
    	this.barAndGridLayout.addComponent(this.bottomLayout);
    	this.barAndGridLayout.addComponent(this.footerLayout);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
        
    }

    private void cargarListeners() 
    {

    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};

    }
    
    private void procesar()
    {
    	if (todoEnOrden())
    	{
    		panelSelectores.getContent().setVisible(false);
			eliminarDashBoards(true);
			cargarDashboards();
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Debes seleccionar correctamente las opciones a ejecutar");
    	}
    }
    
    public void eliminarDashBoards(boolean r_todos)
    {
    	centralMiddle.removeAllComponents();
    	centralMiddle1.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	panelResumen.setCaption("Resumen Familias");
    	panelDetalleFamilias.setCaption("Detalle Familias");
    	panelDetalleArticulos.setCaption("Detalle Articulos");
    	    	
    }
    
    private void cargarSelectores()
    {

    	/*
    	 * Cargo ejercicios
    	 */
		cargarEjercicios();
    	
		/*
		 * cargamos almacenes
		 */
		gridAlmacenes = cargarAlmacenes();
		panelAlmacenes = new Panel("Almacenes a Consultar (*) ");
		panelAlmacenes.setSizeUndefined(); 
		panelAlmacenes.setHeight("250px");
		panelAlmacenes.setContent(gridAlmacenes);
		selectoresDatos.addComponent(panelAlmacenes,1,0);

		/*
    	 * Cargamos familias
    	 */
		gridFamilias = cargarFamilias();
		panelFamilias = new Panel("familias a Consultar");
		panelFamilias.setSizeUndefined(); 
		panelFamilias.setHeight("250px");
		panelFamilias.setContent(gridFamilias);
		selectoresDatos.addComponent(panelFamilias,0,1);
    	
		/*
		 * Cargamos familias
		 */
		gridSubFamilias = cargarSubFamilias("");
		panelSubFamilias = new Panel("SubFamilias a Consultar");
		panelSubFamilias.setSizeUndefined(); 
		panelSubFamilias.setHeight("250px");
		panelSubFamilias.setContent(gridSubFamilias);
		selectoresDatos.addComponent(panelSubFamilias,1,1);
		
    	
		panelSelectores.setContent(selectoresDatos);
		
    }
    
    public void cargarDashboards() 
    {
    	/*
    	 * Con los datos seleccionados inicialmente ejecutamos la carga de stock por familias
    	 * para rellenar el primer panel
    	 * 
    	 */
    	this.mapeo = consultaDashboardExistenciasServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(RutinasFechas.añoActualYYYY(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),this.aliasArticulo.getValue().trim().trim());
    	this.cargarResumenStock();
		this.cargarResumenFamilias();
		this.cargarResumenImportesFamilias();
    }
    
    private void cargarResumenStock()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblStockPT = null;
    	Label lblValorStockPT = null;
    	Label lblStockPS = null;
    	Label lblValorStockPS = null;
    	Label lblStockOA= null;
    	Label lblValorStockOA= null;
    	
    	Panel panel = new Panel("RESUMEN STOCK ");

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	
    	lblStockPT = new Label();
    	lblStockPT.setValue("Stock PT: ");
    	lblStockPT.setWidth(anchoLabelTitulo);    	
    	lblStockPT.addStyleName("lblTitulo");

    	lblValorStockPT= new Label();
    	lblValorStockPT.setWidth(anchoLabelDatos);
    	lblValorStockPT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.PT")));
    	lblValorStockPT.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblStockPT);
    	lin.addComponent(lblValorStockPT);
    	
    	lblStockPS = new Label();
    	lblStockPS.setValue("Stock PS: ");
    	lblStockPS.setWidth(anchoLabelTitulo);    	
    	lblStockPS.addStyleName("lblTitulo");

    	lblValorStockPS= new Label();
    	lblValorStockPS.setWidth(anchoLabelDatos);
    	lblValorStockPS.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.PS")));
    	lblValorStockPS.addStyleName("lblDatosBlanco");
    	
    	lin2.addComponent(lblStockPS);
    	lin2.addComponent(lblValorStockPS);
    	
    	
    	lblStockOA = new Label();
    	lblStockOA.setValue("Stock OA: ");
    	lblStockOA.setWidth(anchoLabelTitulo);    	
    	lblStockOA.addStyleName("lblTitulo");

    	lblValorStockOA= new Label();
    	lblValorStockOA.setWidth(anchoLabelDatos);
    	lblValorStockOA.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.OA")));
    	lblValorStockOA.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblStockOA);
    	lin3.addComponent(lblValorStockOA);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,0,0);
    	panelResumen.setVisible(true);
    	
    	com.vaadin.event.MouseEvents.ClickListener listenerDisp = new com.vaadin.event.MouseEvents.ClickListener() {
			
			@Override
			public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
				pantallaEvolucionEjercicios vt = new pantallaEvolucionEjercicios("Evolucion", recogerEjercicios());
				getUI().addWindow(vt);
			}
		};
//		panelResumen.addClickListener(listenerDisp);
    }
	
    
    private void cargarResumenFamilias()
    {
    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaFamiliasServer cfs = new consultaFamiliasServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	ArrayList<String> vectorFamiliasSeleccionadas = new ArrayList<String>();
    	HashMap<String, String> hashFamilias = new HashMap<String, String>();
    	
    	/*
    	 * Traigo los datos de la base de datos que voy a representar
    	 */
    	hashFamilias = cfs.hashFamilias();
    	vectorFamiliasSeleccionadas = this.listaFamilias();
    	vector = cds.recuperarUnidadesStockFamilias(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),listaFamilias(), this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Familia", String.class, null);
    	container.addContainerProperty("Nombre", String.class, null);
    	while (iterator.hasNext())
    	{
    		MapeoDashboardExistencias mapeoExistencias = iterator.next();
    		container.addContainerProperty(mapeoExistencias.getEjercicio().toString(), Double.class, new Double(0));
    	}
    	
    	Iterator< String> iteratorFamiliasSeleccionadas = vectorFamiliasSeleccionadas.iterator();
    	
    	while (iteratorFamiliasSeleccionadas.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		iterator = null;
    		String familiaProcesar = iteratorFamiliasSeleccionadas.next();
    		String nombreFamiliaProcesar = hashFamilias.get(familiaProcesar);
    		
    		iterator = vector.iterator();
    		
    		while (iterator.hasNext())
    		{
    			MapeoDashboardExistencias mapeoExi = iterator.next();
    			if (mapeoExi.getHashValores().get(familiaProcesar)!=null)
    			{
    				file.getItemProperty("Familia").setValue(familiaProcesar); 
	    			file.getItemProperty("Nombre").setValue(nombreFamiliaProcesar); 
	    			
	    			file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(mapeoExi.getHashValores().get(familiaProcesar));

    			}
    		}
    		if (file.getItemProperty("Familia").getValue()==null)     		
			{
				container.removeItem(newItemId);
			}

    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
			
			@Override
			public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Familia") || cellReference.getPropertyId().toString().contains("Nombre")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
				cargarResumenSubFamilias(it.getItemProperty("Familia").getValue().toString(), it.getItemProperty("Nombre").getValue().toString());
			}
		});

    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
        	
    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
	    	for(Object itemId : list)
	    	{
	    		Item item = indexed.getItem(itemId);
            		
        		valor = (Double) item.getItemProperty(ej.toString()).getValue();
        		if (valor!=null)
        			totalU += valor.longValue();
    				
        	}
        	footer.getCell("Nombre").setText("Totales ");
        	footer.getCell(ej.toString()).setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
        	footer.getCell(ej.toString()).setStyleName("Rcell-pie");
        }
            
    	
    	Panel panel = new Panel("Stock por Familia (Uds.)");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
//    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralMiddle.addComponent(panel,2,0);
    	
    }
    private void cargarResumenImportesFamilias()
    {
    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaFamiliasServer cfs = new consultaFamiliasServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	ArrayList<String> vectorFamiliasSeleccionadas = new ArrayList<String>();
    	HashMap<String, String> hashFamilias = new HashMap<String, String>();
    	
    	/*
    	 * Traigo los datos de la base de datos que voy a representar
    	 */
    	hashFamilias = cfs.hashFamilias();
    	vectorFamiliasSeleccionadas = this.listaFamilias();
    	vector = cds.recuperarImportesStockFamilias(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),listaFamilias(), this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Familia", String.class, null);
    	container.addContainerProperty("Nombre", String.class, null);
    	while (iterator.hasNext())
    	{
    		MapeoDashboardExistencias mapeoExistencias = iterator.next();
    		container.addContainerProperty(mapeoExistencias.getEjercicio().toString(), Double.class, new Double(0));
    	}
    	
    	Iterator< String> iteratorFamiliasSeleccionadas = vectorFamiliasSeleccionadas.iterator();
    	
    	while (iteratorFamiliasSeleccionadas.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		iterator = null;
    		String familiaProcesar = iteratorFamiliasSeleccionadas.next();
    		String nombreFamiliaProcesar = hashFamilias.get(familiaProcesar);
    		
    		iterator = vector.iterator();
    		
    		while (iterator.hasNext())
    		{
    			MapeoDashboardExistencias mapeoExi = iterator.next();
    			if (mapeoExi.getHashValores().get(familiaProcesar)!=null)
    			{
    				file.getItemProperty("Familia").setValue(familiaProcesar); 
	    			file.getItemProperty("Nombre").setValue(nombreFamiliaProcesar);
	    			
	    			if (mapeoExi.getHashValores().get(familiaProcesar)==null) 
	    				file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(new Double(0)); 
	    			else
	    				file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(mapeoExi.getHashValores().get(familiaProcesar));

    			}
    		}
    		if (file.getItemProperty("Familia").getValue()==null)     		
			{
				container.removeItem(newItemId);
			}
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Familia") || cellReference.getPropertyId().toString().contains("Nombre")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	
    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
    	
    	FooterRow footer = gridDatos.appendFooterRow();
    	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
    		for(Object itemId : list)
    		{
    			Item item = indexed.getItem(itemId);
    			
    			valor = (Double) item.getItemProperty(ej.toString()).getValue();
    			if (valor!=null)
    				totalU += valor.longValue();
    			
    		}
    		footer.getCell("Nombre").setText("Totales ");
    		footer.getCell(ej.toString()).setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    		footer.getCell(ej.toString()).setStyleName("Rcell-pie");
    	}
    	
    	
    	Panel panel = new Panel("Stock por Familia (Imp.)");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
//    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralMiddle.addComponent(panel,3,0);
    	
    }
    
    private void cargarResumenSubFamilias(String r_familia, String r_nombre)
    {
    	
    	centralMiddle1.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	panelDetalleFamilias.setVisible(true);

    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaFamiliasServer cfs = new consultaFamiliasServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	ArrayList<String> vectorSubFamiliasSeleccionadas = new ArrayList<String>();
    	HashMap<String, String> hashSubFamilias = new HashMap<String, String>();
    	String subFamiliaProcesar = null;
    	/*
    	 * Traigo los datos de la base de datos que voy a representar
    	 */
    	hashSubFamilias = cfs.hashSubFamilias(r_familia);
    	vectorSubFamiliasSeleccionadas = this.listaSubFamilias(r_familia);
    	vector = cds.recuperarUnidadesStockFamiliasSubFamilias(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),r_familia, vectorSubFamiliasSeleccionadas, this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Familia", String.class, null);
    	container.addContainerProperty("Nombre", String.class, null);
    	container.addContainerProperty("SubFamilia", String.class, null);
    	container.addContainerProperty("Nombre Sub", String.class, null);
    	while (iterator.hasNext())
    	{
    		MapeoDashboardExistencias mapeoExistencias = iterator.next();
    		container.addContainerProperty(mapeoExistencias.getEjercicio().toString(), Double.class, new Double(0));
    	}
    	
    	Iterator< String> iteratorSubFamiliasSeleccionadas = vectorSubFamiliasSeleccionadas.iterator();
    	
    	while (iteratorSubFamiliasSeleccionadas.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		iterator = null;
    		subFamiliaProcesar = iteratorSubFamiliasSeleccionadas.next();
    		String nombreSubFamiliaProcesar = hashSubFamilias.get(subFamiliaProcesar);
    		
    		iterator = vector.iterator();
    		file.getItemProperty("Familia").setValue(r_familia); 
    		file.getItemProperty("Nombre").setValue(r_nombre); 
    		file.getItemProperty("SubFamilia").setValue(subFamiliaProcesar); 
    		file.getItemProperty("Nombre Sub").setValue(nombreSubFamiliaProcesar); 
    		
    		while (iterator.hasNext())
    		{
    			MapeoDashboardExistencias mapeoExi = iterator.next();
    			if (mapeoExi.getHashValores().get(subFamiliaProcesar)==null) 
    				container.removeItem(newItemId);
    			else
    				file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(mapeoExi.getHashValores().get(subFamiliaProcesar));	
    		}
    		
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
			
			@Override
			public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Familia") || cellReference.getPropertyId().toString().contains("Nombre") || cellReference.getPropertyId().toString().contains("SubFamilia") || cellReference.getPropertyId().toString().contains("Nombre Sub")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
		    	if ((!cmbTipoArticuloPTPS.getValue().toString().contentEquals("OA")) && (!cmbTipoArticuloPTPS.getValue().toString().contentEquals("MP")))
		    		cargarResumenSubFamiliasAñadas(it.getItemProperty("Familia").getValue().toString(), it.getItemProperty("Nombre").getValue().toString(), it.getItemProperty("SubFamilia").getValue().toString(),it.getItemProperty("Nombre Sub").getValue().toString());
		    	cargarResumenArticulos(it.getItemProperty("Familia").getValue().toString(), it.getItemProperty("SubFamilia").getValue().toString());
			}
		});

    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
        	
    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
	    	for(Object itemId : list)
	    	{
	    		Item item = indexed.getItem(itemId);
            		
        		valor = (Double) item.getItemProperty(ej.toString()).getValue();
        		totalU += valor.longValue();
    				
        	}
        	footer.getCell("Nombre Sub").setText("Totales ");
        	footer.getCell(ej.toString()).setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
        	footer.getCell(ej.toString()).setStyleName("Rcell-pie");
        }
            
    	
    	panelResumenSubfamilias = new Panel("Stock por subFamilia (Uds.)");
    	panelResumenSubfamilias.setSizeUndefined(); // Shrink to fit content
    	panelResumenSubfamilias.setHeight("250px");
    	panelResumenSubfamilias.setWidth("100%");
    	panelResumenSubfamilias.setContent(gridDatos);
    	centralMiddle1.addComponent(panelResumenSubfamilias,0,0);
    	panelDetalleFamilias.setVisible(true);
//    	
    }

    private void cargarResumenSubFamiliasAñadas(String r_familia, String r_nombreFamilia, String r_subfamilia, String r_nombreSubfamilia)
    {
    	if (panelResumenAñadas!=null) centralMiddle1.removeComponent(panelResumenAñadas);
    	centralMiddle2.removeAllComponents();
    	panelDetalleFamilias.setVisible(true);
    	
    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaFamiliasServer cfs = new consultaFamiliasServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	
    	/*
    	 * Traigo los datos de la base de datos que voy a representar
    	 */
    	vector = cds.recuperarUnidadesStockFamiliasSubFamiliasAñadas(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),r_familia, r_subfamilia, this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Familia", String.class, null);
    	container.addContainerProperty("Nombre", String.class, null);
    	container.addContainerProperty("SubFamilia", String.class, null);
    	container.addContainerProperty("Nombre Sub", String.class, null);
    	container.addContainerProperty("Añada", String.class, null);
    	container.addContainerProperty("Valor", Double.class, new Double(0));
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		MapeoDashboardExistencias mapeoExi = iterator.next();

    		file.getItemProperty("Familia").setValue(r_familia); 
    		file.getItemProperty("Nombre").setValue(r_nombreFamilia); 
    		file.getItemProperty("SubFamilia").setValue(r_subfamilia); 
    		file.getItemProperty("Nombre Sub").setValue(r_nombreSubfamilia); 
    		file.getItemProperty("Añada").setValue(mapeoExi.getAñada()); 
    		file.getItemProperty("Valor").setValue(mapeoExi.getValor()); 
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Familia") || cellReference.getPropertyId().toString().contains("Nombre") || cellReference.getPropertyId().toString().contains("SubFamilia") || cellReference.getPropertyId().toString().contains("Nombre Sub")
    					|| cellReference.getPropertyId().toString().contains("Añada") ) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	gridDatos.addSelectionListener(new SelectionListener() {
    		
    		@Override
    		public void select(SelectionEvent event) {
    			
    			Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
    			
    			cargarResumenAñada(it.getItemProperty("Familia").getValue().toString(), it.getItemProperty("SubFamilia").getValue().toString(), it.getItemProperty("Añada").getValue().toString());
    			
    		}
    	});
    	
    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
    	
    	FooterRow footer = gridDatos.appendFooterRow();
    	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
    		for(Object itemId : list)
    		{
    			Item item = indexed.getItem(itemId);
    			
    			valor = (Double) item.getItemProperty("Valor").getValue();
    			totalU += valor.longValue();
    			
    		}
    		footer.getCell("Nombre Sub").setText("Totales ");
    		footer.getCell("Valor").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    		footer.getCell("Valor").setStyleName("Rcell-pie");
    		break;
    	}
    	
    	
    	panelResumenAñadas = new Panel("Stock por subFamilia - añada (Uds.)");
    	panelResumenAñadas.setSizeUndefined(); // Shrink to fit content
    	panelResumenAñadas.setHeight("250px");
    	panelResumenAñadas.setWidth("100%");
    	panelResumenAñadas.setContent(gridDatos);
    	centralMiddle1.addComponent(panelResumenAñadas,1,0);
    	panelDetalleFamilias.setVisible(true);
//    	
    }

    
    private void cargarResumenArticulos(String r_familia, String r_subfamilia)
    {
    	boolean control = false;
    	centralMiddle2.removeAllComponents();
    	
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	
    	ArrayList<String> vectorArticulos = new ArrayList<String>();
    	HashMap<String, String> hashArticulos = new HashMap<String, String>();
    	String articuloProcesar = null;
    	/*
    	 * Traigo los datos de la base de datos que voy a representar
    	 */
    	hashArticulos = cas.hashArticulos(r_familia,r_subfamilia,this.cmbTipoArticuloPTPS.getValue().toString());
    	vectorArticulos = cas.listaArticulos(r_familia, r_subfamilia, this.cmbTipoArticuloPTPS.getValue().toString());

    	vector = cds.recuperarUnidadesStockFamiliasSubFamiliasArticulos(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),r_familia, r_subfamilia, this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Familia", String.class, null);
    	container.addContainerProperty("SubFamilia", String.class, null);
    	container.addContainerProperty("Articulo", String.class, null);
    	container.addContainerProperty("Descripcion", String.class, null);
    	while (iterator.hasNext())
    	{
    		MapeoDashboardExistencias mapeoExistencias = iterator.next();
    		container.addContainerProperty(mapeoExistencias.getEjercicio().toString(), Double.class, new Double(0));
    	}
    	
    	Iterator< String> iteratorArticulosProcesar = vectorArticulos.iterator();

    	while (iteratorArticulosProcesar.hasNext())
    	{
    		articuloProcesar = iteratorArticulosProcesar.next();
    		String nombreArticuloProcesar = hashArticulos.get(articuloProcesar);
    		
			control=false;
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		iterator = null;

    		
    		iterator = vector.iterator();
    		file.getItemProperty("Familia").setValue(r_familia); 
    		file.getItemProperty("SubFamilia").setValue(r_subfamilia); 
    		file.getItemProperty("Articulo").setValue(articuloProcesar); 
    		file.getItemProperty("Descripcion").setValue(nombreArticuloProcesar); 
    		
    		while (iterator.hasNext())
    		{
    			MapeoDashboardExistencias mapeoExi = iterator.next();
    			
    			if (mapeoExi!=null && mapeoExi.getHashValores()!=null && mapeoExi.getHashValores().get(articuloProcesar)!=null)
    			{
    				if (mapeoExi.getHashValores().get(articuloProcesar)!=0)
    				{
						file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(mapeoExi.getHashValores().get(articuloProcesar));
						control=true;
    				}
    				else
    				{
    					file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(new Double(0));//
    				}
    			}
    			else
    			{
    				file.getItemProperty(mapeoExi.getEjercicio().toString()).setValue(new Double(0));
    			}
    		}
			if (!control)
				container.removeItem(newItemId);
		}    		
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.NONE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
			
			@Override
			public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Familia") || cellReference.getPropertyId().toString().contains("SubFamilia") 
    					|| cellReference.getPropertyId().toString().contains("Articulo") || cellReference.getPropertyId().toString().contains("Descripcion")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
//    	gridDatos.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void select(SelectionEvent event) {
//				
//		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
//		    	
//		    	cargarResumenArticulos(it.getItemProperty("Familia").getValue().toString(), it.getItemProperty("subFamilia").getValue().toString());
//
//			}
//		});

    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
        	
    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
	    	for(Object itemId : list)
	    	{
	    		Item item = indexed.getItem(itemId);
            		
        		valor = (Double) item.getItemProperty(ej.toString()).getValue();
        		totalU += valor.longValue();
    				
        	}
        	footer.getCell("Descripcion").setText("Totales ");
        	footer.getCell(ej.toString()).setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
        	footer.getCell(ej.toString()).setStyleName("Rcell-pie");
        }
            
    	
    	panelResumenArticulos = new Panel("Stock por Articulo (Uds.)");
    	panelResumenArticulos.setSizeFull(); // Shrink to fit content
    	panelResumenArticulos.setHeight("250px");
    	panelResumenArticulos.setWidth("100%");
    	panelResumenArticulos.setContent(gridDatos);
    	centralMiddle2.addComponent(panelResumenArticulos,0,0);
    	panelDetalleArticulos.setVisible(true);
    	
    }
    private void cargarResumenAñada(String r_familia, String r_subfamilia, String r_añada)
    {
    	if (panelResumenArticulosAñada!=null) centralMiddle2.removeComponent(panelResumenArticulosAñada);
    	
    	consultaDashboardExistenciasServer cds = new consultaDashboardExistenciasServer(CurrentUser.get());
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	ArrayList<MapeoDashboardExistencias> vector = new ArrayList<MapeoDashboardExistencias>();
    	
    	vector = cds.recuperarUnidadesStockFamiliasSubFamiliasArticulosAñadas(recogerEjercicios(), recuperarMes(this.cmbMes.getValue().toString()), recogerAlmacenes(),recuperarTipoArticulo(this.cmbTipoArticulo.getValue().toString()),r_familia, r_subfamilia, r_añada, this.cmbTipoArticuloPTPS.getValue().toString(), this.aliasArticulo.getValue().trim());
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDashboardExistencias> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Articulo", String.class, null);
    	container.addContainerProperty("Lote", String.class, null);
    	container.addContainerProperty("Añada", String.class, null);
    	container.addContainerProperty("Valor", Double.class, new Double(0));
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		MapeoDashboardExistencias mapeoExi = iterator.next();    		
    		
    		file.getItemProperty("Articulo").setValue(mapeoExi.getArticulo()); 
    		file.getItemProperty("Lote").setValue(mapeoExi.getNumeroLote().toString()); 
    		file.getItemProperty("Añada").setValue(r_añada); 
    		file.getItemProperty("Valor").setValue(mapeoExi.getValor()); 
    	}
    	
    	Grid gridDatos= new Grid(container);
    	
    	if (this.almacenesSeleccionados.toString().contains("12"))
    	{
    		gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	}
    	else
    	{
    		gridDatos.setSelectionMode(SelectionMode.NONE);
    	}
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Articulo") || cellReference.getPropertyId().toString().contains("Añada")|| cellReference.getPropertyId().toString().contains("Lote")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	
    	ArrayList<Integer> ejer = recogerEjercicios();
    	Iterator<Integer> iterEjer = ejer.iterator();
    	
    	Long totalU = new Long(0) ;
    	Double valor = null;
    	
    	FooterRow footer = gridDatos.appendFooterRow();
    	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	Integer ej = null;
    	while (iterEjer.hasNext())
    	{
    		ej = iterEjer.next();
    		totalU = new Long(0) ;
    		for(Object itemId : list)
    		{
    			Item item = indexed.getItem(itemId);
    			
    			valor = (Double) item.getItemProperty("Valor").getValue();
    			totalU += valor.longValue();
    			
    		}
    		footer.getCell("Lote").setText("Totales ");
    		footer.getCell("Valor").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    		footer.getCell("Valor").setStyleName("Rcell-pie");
    		break;
    	}
    	
    	gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
		    	if (it!=null)
		    	{
		    		pantallaLineasAlbaranesTraslados vtt = new pantallaLineasAlbaranesTraslados("Reservas Stock",12,it.getItemProperty("Articulo").getValue().toString(),it.getItemProperty("Lote").getValue().toString(),null,true);
		    		getUI().addWindow(vtt);
		    	}
			}
		});
    	
    	panelResumenArticulosAñada = new Panel("Stock Articulo Añada y Lote (Uds.)");
    	panelResumenArticulosAñada.setSizeFull(); // Shrink to fit content
    	panelResumenArticulosAñada.setHeight("250px");
    	panelResumenArticulosAñada.setWidth("100%");
    	panelResumenArticulosAñada.setContent(gridDatos);
    	centralMiddle2.addComponent(panelResumenArticulosAñada,1,0);
    	panelDetalleArticulos.setVisible(true);
    	
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}


    private void cargarCombo()
    {
    	this.cmbMes.addItem("Iniciales");
    	this.cmbMes.addItem("Actuales");
    	this.cmbMes.addItem("Enero");
    	this.cmbMes.addItem("Febrero");
    	this.cmbMes.addItem("Marzo");
    	this.cmbMes.addItem("Abril");
    	this.cmbMes.addItem("Mayo");
    	this.cmbMes.addItem("Junio");
    	this.cmbMes.addItem("Julio");
    	this.cmbMes.addItem("Agosto");
    	this.cmbMes.addItem("Septiembre");
    	this.cmbMes.addItem("Octubre");
    	this.cmbMes.addItem("Noviembre");
    	this.cmbMes.addItem("Diciembre");
    	
    	this.cmbTipoArticulo.addItem("Embotellado");
    	this.cmbTipoArticulo.addItem("Envasado");
    	this.cmbTipoArticulo.addItem("BIB");
    	this.cmbTipoArticulo.addItem("Materia Seca");
    	this.cmbTipoArticulo.addItem("Granel");

    	this.cmbTipoArticuloPTPS.addItem("PT");
    	this.cmbTipoArticuloPTPS.addItem("PS");
    	this.cmbTipoArticuloPTPS.addItem("OA");
    	this.cmbTipoArticuloPTPS.addItem("MP");
    	
    }

    private String recuperarMes(String r_mes)
    {
    	switch (r_mes)
    	{
    		case "Iniciales":
    		{
    			return "unid_inicio";
    		}
    		case "Actuales":
    		{
    			return "existencias";
    		}
    		case "Enero":
    		{
    			return "stock_1";
    		}
    		case "Febrero":
    		{
    			return "stock_2";
    		}
    		case "Marzo":
    		{
    			return "stock_3";
    		}
    		case "Abril":
    		{
    			return "stock_4";
    		}
    		case "Mayo":
    		{
    			return "stock_5";
    		}
    		case "Junio":
    		{
    			return "stock_6";
    		}
    		case "Julio":
    		{
    			return "stock_7";
    		}
    		case "Agosto":
    		{
    			return "stock_8";
    		}
    		case "Septiembre":
    		{
    			return "stock_9";
    		}
    		case "Octubre":
    		{
    			return "stock_10";
    		}
    		case "Noviembre":
    		{
    			return "stock_11";
    		}
    		case "Diciembre":
    		{
    			return "stock_12";
    		}
    		default:
    			return"";
    	}
    }
    private String recuperarTipoArticulo(String r_tipo)
    {
    	switch (r_tipo)
    	{
    	case "Embotellado":
    	{
    		return "0102";
    	}
    	case "Envasado":
    	{
    		return "0103";
    	}
    	case "BIB":
    	{
    		return "0111";
    	}
    	case "Materia Seca":
    	{
    		return "010";
    	}
    	case "Granel":
    	{
    		return "0101";
    	}
    	default:
    		return "0102";
    	}
    }

    private boolean todoEnOrden()
    {
    	/*
    	 * Metodo que se encarga de comprobar la seleccion de datos
    	 * para ejecutar la consulta
    	 * 
    	 * comprobara que este seleccionado:
    	 * 
    	 * 	- un ejercicio al menos
    	 *  - un almacen al menos
    	 *  - un valor en combo mes
    	 */
    	
    	if (this.cmbTipoArticuloPTPS.getValue()==null || this.cmbTipoArticulo.getValue()==null || this.cmbMes.getValue()==null 
    			|| this.gridEjercicios.getSelectedRows().isEmpty() || this.gridAlmacenes.getSelectedRows().isEmpty()) return false;
    	return true;
    }
    
    
    private Grid cargarAlmacenes()
    {
    	consultaAlmacenesServer cas = new consultaAlmacenesServer(CurrentUser.get());
    	
    	ArrayList<MapeoAlmacenes> vectorAlmacenes = cas.datosAlmacenesGlobal();
    	
    	IndexedContainer container =null;
    	Iterator<MapeoAlmacenes> iterator = null;
    	MapeoAlmacenes mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("almacen", String.class, null);
		container.addContainerProperty("nombre", String.class, null);
			
		iterator = vectorAlmacenes.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoAlmacenes) iterator.next();
			
			file.getItemProperty("almacen").setValue(mapeo.getAlmacen());
			file.getItemProperty("nombre").setValue(mapeo.getDescripcion());
		}

		gridAlmacenes= new Grid(container);
		gridAlmacenes.setSelectionMode(SelectionMode.MULTI);
		gridAlmacenes.setSizeFull();
		gridAlmacenes.setWidth("100%");
		gridAlmacenes.addStyleName("minigrid");
		
		gridAlmacenes.getColumn("almacen").setWidth(new Double(100));
		gridAlmacenes.getColumn("nombre").setWidth(new Double(220));
//		gridDatos.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void select(SelectionEvent event) {
//				
//				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
////				if (grid.getSelectionModel() instanceof SingleSelectionModel && !estoyEnCelda)
////					app.filaSeleccionada();
////				else
////					estoyEnCelda = false;
//				articuloPadre= file.getItemProperty("articulo").getValue().toString();
//				descripcionArticuloPadre = cas.obtenerDescripcionArticulo(articuloPadre.trim());
//				tipoArticulo = cas.obtenerTipoArticulo(articuloPadre).trim();
//				eliminarDashBoards(false);
//				ejecutarPadres();
//			}
//		});

		
		return gridAlmacenes;
    }

    private Grid cargarFamilias()
    {
    	consultaFamiliasServer cas = new consultaFamiliasServer(CurrentUser.get());
    	
    	ArrayList<MapeoFamilias> vectorFamilias = cas.datosFamiliasGlobal();
    	
    	IndexedContainer container =null;
    	Iterator<MapeoFamilias> iterator = null;
    	MapeoFamilias mapeo = null;
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("familia", String.class, null);
    	container.addContainerProperty("nombre", String.class, null);
    	
    	iterator = vectorFamilias.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoFamilias) iterator.next();
    		
    		file.getItemProperty("familia").setValue(mapeo.getFamilia());
    		file.getItemProperty("nombre").setValue(mapeo.getDescripcion());
    	}
    	
    	gridFamilias= new Grid(container);
    	gridFamilias.setSelectionMode(SelectionMode.MULTI);
    	gridFamilias.setSizeFull();
    	gridFamilias.setWidth("100%");
    	gridFamilias.addStyleName("minigrid");
		gridFamilias.getColumn("familia").setWidth(new Double(100));
		gridFamilias.getColumn("nombre").setWidth(new Double(220));
    	
		gridFamilias.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
				String familia = recogerFamilias();
				cargarSubFamilias(familia);
			}
		});

    	return gridFamilias;
    }
    
    private Grid cargarSubFamilias(String r_familia)
    {
    	if (this.gridSubFamilias!=null)
    	{
    		this.gridSubFamilias.removeAllColumns();
    		this.gridSubFamilias = null;
    	}
    	panelSubFamilias = null;
    	selectoresDatos.removeComponent(1,1);
    	
    	
    	consultaSubFamiliasServer cas = new consultaSubFamiliasServer(CurrentUser.get());
    	ArrayList<MapeoSubFamilias> vectorSubFamilias = cas.datosSubFamiliasGlobal(r_familia);
    	
    	IndexedContainer container =null;
    	Iterator<MapeoSubFamilias> iterator = null;
    	MapeoSubFamilias mapeo = null;
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("subfamilia", String.class, null);
    	container.addContainerProperty("nombre", String.class, null);
    	container.addContainerProperty("familia", String.class, null);
    	
    	iterator = vectorSubFamilias.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoSubFamilias) iterator.next();
    		
    		file.getItemProperty("subfamilia").setValue(mapeo.getSubFamilia());
    		file.getItemProperty("nombre").setValue(mapeo.getDescripcion());
    		file.getItemProperty("familia").setValue(mapeo.getFamilia());
    	}
    	
    	gridSubFamilias= new Grid(container);
    	gridSubFamilias.setSelectionMode(SelectionMode.MULTI);
    	gridSubFamilias.getColumn("familia").setHidden(true);
    	gridSubFamilias.setSizeFull();
    	gridSubFamilias.setWidth("100%");
    	gridSubFamilias.addStyleName("minigrid");
    	
    	if (r_familia.length()>0)
    	{
    		panelSubFamilias = new Panel("SubFamilias a Consultar");
    		panelSubFamilias.setSizeUndefined(); 
    		panelSubFamilias.setHeight("250px");
    		panelSubFamilias.setContent(gridSubFamilias);
    		selectoresDatos.addComponent(panelSubFamilias,1,1);
    	}

		return gridSubFamilias;
    }
    
    private void cargarEjercicios()
    {
    	consultaEjerciciosServer cas = new consultaEjerciciosServer(CurrentUser.get());
    	
    	ArrayList<String> vectorEjercicios = cas.obtenerEjercicios();
    	
    	IndexedContainer container =null;
    	Iterator<String> iterator = null;
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("ejercicio", String.class, null);
    	
    	iterator = vectorEjercicios.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		String ejer= (String) iterator.next();
    		
    		file.getItemProperty("ejercicio").setValue(ejer);
    	}
    	
    	gridEjercicios= new Grid(container);
    	gridEjercicios.getColumn("ejercicio").setWidth(new Double(100));
    	gridEjercicios.setSelectionMode(SelectionMode.MULTI);
    	gridEjercicios.setSizeFull();
    	gridEjercicios.setWidth("100%");
    	gridEjercicios.addStyleName("minigrid");
    	
		panelEjercicios = new Panel("Ejercicios a Consultar (*) ");
		panelEjercicios.setSizeUndefined(); 
		panelEjercicios.setHeight("250px");
		panelEjercicios.setContent(gridEjercicios);
		selectoresDatos.addComponent(panelEjercicios,0,0);

    }

    private void vaciarPantalla()
    {
        centralMiddle=null;
        centralMiddle1=null;
        topLayout=null;
        barAndGridLayout=null;
        
        this.removeAllComponents();
    }    

    private String recogerAlmacenes()
    {
		Iterator it = null;
		this.almacenesSeleccionados = new StringBuilder();
		
		it = this.gridAlmacenes.getSelectedRows().iterator();
		
		while (it.hasNext())
		{			
			Integer valor = (Integer) it.next();
			Item file = gridAlmacenes.getContainerDataSource().getItem(valor);
			if (this.almacenesSeleccionados.toString().length()!=0) this.almacenesSeleccionados.append(",");
			this.almacenesSeleccionados.append("'" + file.getItemProperty("almacen").getValue().toString() + "'");
		}
		return this.almacenesSeleccionados.toString();
		
		
    }
    private String recogerFamilias()
    {
    	StringBuilder familiasSeleccionados = null;
    	
    	Iterator it = null;
    	familiasSeleccionados = new StringBuilder();
    	
    	it = this.gridFamilias.getSelectedRows().iterator();
    	
    	while (it.hasNext())
    	{			
    		Integer valor = (Integer) it.next();
    		Item file = gridFamilias.getContainerDataSource().getItem(valor);
    		if (familiasSeleccionados.toString().length()!=0) familiasSeleccionados.append(",");
    		familiasSeleccionados.append("'" + file.getItemProperty("familia").getValue().toString() + "'");
    	}
    	return familiasSeleccionados.toString();
    	
    	
    }

    private ArrayList<String> listaFamilias()
    {
    	ArrayList<String> familiasSelecc =null;
    	Iterator it = null;
    	
    	it = this.gridFamilias.getSelectedRows().iterator();
    	familiasSelecc = new ArrayList<String>();

    	if (it.hasNext())
    	{
	    	
	    	while (it.hasNext())
	    	{			
	    		Integer valor = (Integer) it.next();
	    		Item file = gridFamilias.getContainerDataSource().getItem(valor);
	    		familiasSelecc.add(file.getItemProperty("familia").getValue().toString());
	    	}
    	}
    	else
    	{
    		it = gridFamilias.getContainerDataSource().getItemIds().iterator();	
    		while (it.hasNext())
	    	{			
	    		Integer valor = (Integer) it.next();
	    		Item file = gridFamilias.getContainerDataSource().getItem(valor);
	    		familiasSelecc.add(file.getItemProperty("familia").getValue().toString());
	    	}
    	}
    	return familiasSelecc;
    	
    }

    private ArrayList<String> listaSubFamilias(String r_familias )
    {
    	ArrayList<String> familiasSelecc =null;
    	Iterator it = null;
    	
    	
    	it = this.gridSubFamilias.getSelectedRows().iterator();
    	familiasSelecc = new ArrayList<String>();
    	
    	if (it.hasNext())
    	{
	    	while (it.hasNext())
	    	{			
	    		Integer valor = (Integer) it.next();
	    		Item file = gridSubFamilias.getContainerDataSource().getItem(valor);
	    		if (!familiasSelecc.contains(file.getItemProperty("subfamilia").getValue().toString()))
	    		{
	    			familiasSelecc.add(file.getItemProperty("subfamilia").getValue().toString());
	    		}
	    	}
    	}
    	else
    	{
    		it = gridSubFamilias.getContainerDataSource().getItemIds().iterator();
    		while (it.hasNext())
    		{
    			Integer valor = (Integer) it.next();
	    		Item file = gridSubFamilias.getContainerDataSource().getItem(valor);
	    		if (file.getItemProperty("familia").getValue().toString().contentEquals(r_familias))
	    		{
		    		if (!familiasSelecc.contains(file.getItemProperty("subfamilia").getValue().toString()))
		    		{
		    			familiasSelecc.add(file.getItemProperty("subfamilia").getValue().toString());
		    		}
	    		}
    		}
    	}
    	return familiasSelecc;
    	
    }

    private ArrayList<Integer> recogerEjercicios()
    {
//    	StringBuilder ejerciciosSeleccionados = null;
    	ArrayList<Integer> ejerciciosVector = null;
    	
    	Iterator it = null;
//    	ejerciciosSeleccionados = new StringBuilder();
    	
    	it = this.gridEjercicios.getSelectedRows().iterator();
    	ejerciciosVector = new ArrayList<Integer>();
    	
    	while (it.hasNext())
    	{	
    		Integer valor = (Integer) it.next();
    		Item file = gridEjercicios.getContainerDataSource().getItem(valor);
    		ejerciciosVector.add(new Integer(file.getItemProperty("ejercicio").getValue().toString()));
    		
//    		if (ejerciciosSeleccionados.toString().length()!=0) ejerciciosSeleccionadosSeleccionados.append(",");
//    		familiasSeleccionados.append("'" + file.getItemProperty("familia").getValue().toString() + "'");
    	}
    	return ejerciciosVector;
    	
    	
    }
}
