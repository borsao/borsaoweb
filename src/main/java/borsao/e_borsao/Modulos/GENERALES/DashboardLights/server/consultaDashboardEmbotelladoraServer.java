package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladoraProduccionAnalizada;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEmbotelladora;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server.consultaHorasTrabajadasEmbotelladoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardEmbotelladoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDashboardEmbotelladoraServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivoLitros = 33750;
	public static Integer produccionObjetivoBotellas = 45000;
	public static String area = "EMBOTELLADO";
	private String ambitoTemporal = null; 
	private String semana = null; 
	private String ejercicio = null; 
	
	public consultaDashboardEmbotelladoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardEmbotelladoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardEmbotelladoraServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardEmbotelladora recuperarValoresEstadisticos(String r_ejercicio, String r_semana, String r_vista,String r_campo)
	{
		Double valor = null;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 *  
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod.0102)
		 *      - total produccion prevista por formato del periodo				(0.pre.0102)
		 *  	- produccion total programada del periodo						(0.plan.0102)
		 *  
		 *  	- mermas total del periodo										(0.mer.0102)
		 *   
		 *   	- produccion del periodo										(0.prod.0102PT) 
		 *   																	(0.prod.0102SE)
		 *   																	(0.prod.0102ET)

		 *   	- produccion planificada del periodo							(0.plan.0102PT) 
		 *   																	(0.plan.0102SE)
		 *   																	(0.plan.0102ET)

   		 *   	- mermas del periodo											(0.mer.0102PT) 
		 *   																	(0.mer.0102SE)
		 *   																	(0.mer.0102ET)
		 *   

		 *   	- produccion por formato del periodo							(0.prod.0102MediaPaleta) 
		 *   																	(0.prod.0102CajaHorizontal)
		 *   																	(0.prod.0102Troncoconica)
		 *   																	(0.prod.0102Resto)
		 *   
		 *      - total horas trabajadas por formato del periodo				(0.horas.0102Rresto)
		 *      																(0.horas.0102CajaHorizontal)
		 *      																(0.horas.0102Troncoconica)

   		 *      - total horas incidencias por formato del periodo				(0.inc.0102)
		 *      																(0.inc.0102Troncoconica)
		 *      																(0.inc.0102CajaHorizontal)
      
   		 *      - total horas incidencias programadas por formato del periodo	(0.pro.0102)
		 *      																(0.pro.0102Troncoconica)
		 *      																(0.pro.0102CajaHorizontal)
		 *      
		 *      - media produccion del periodo por formato						(0.media.0102)
		 *      																(0.media.0102Troncoconica)
		 *      																(0.media.0102CajaHorizontal)
		 *      
		 *      - venta total del periodo										(0.venta.0102)
		 *      																(0.venta.0102Troncoconica)
		 *      																(0.venta.0102CajaHorizontal)
		 *      
		 *      - ventas por formato del periodo para el ejercicio anterior		(1.venta.0102)
		 *      																(1.venta.0102Troncoconica)
		 *      																(1.venta.0102CajaHorizontal)
		 *      
		 *      - venta media por referencia									(0.media.0102)
		 *      
		 *      - stock productos actual										(0.stock.0102)
		 *      
		 *      - stock por formato del periodo									(0.stock.0102PT)
		 *      																(0.stock.0102SE)
		 *      
		 */
		ResultSet rsOpcion = null;		
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardEmbotelladora mapeo = null;
		ArrayList<MapeoDashboardEmbotelladora> vectorDatosProduccion = null;
		ArrayList<MapeoDashboardEmbotelladora> vectorDatosPlanificacion = null;
		ArrayList<MapeoDashboardEmbotelladora> vectorDatosVenta = null;
		ArrayList<MapeoDashboardEmbotelladora> vectorDatosVentaAnterior = null;
		HashMap<String, Double> vectorDatosStock = null;

		this.ambitoTemporal=r_vista;
		if (ambitoTemporal.contentEquals("Anual"))
		{
			r_semana = "0";
			semana = "0";
		}
		if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
		
		hashValores = new HashMap<String, Double>();

		vectorDatosProduccion = this.recuperarMovimientosProduccion(new Integer(r_ejercicio),r_semana,r_campo);
		vectorDatosPlanificacion = this.recuperarMovimientosPlanificacion(new Integer(r_ejercicio),r_semana, r_campo);
		vectorDatosVenta = this.recuperarMovimientosVentas(new Integer(r_ejercicio),r_campo, r_semana);
		vectorDatosVentaAnterior = this.recuperarMovimientosVentas(new Integer(r_ejercicio)-1,r_campo, r_semana);
		vectorDatosStock = this.recuperarStock(new Integer(r_ejercicio),r_campo);
		
		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.stock.0102");
			hashValores.put("0.stock.0102", valor);
			
			valor = vectorDatosStock.get("0.stock.0102PT");
			hashValores.put("0.stock.0102PT", valor);

			valor = vectorDatosStock.get("0.stock.0102SE");
			hashValores.put("0.stock.0102SE", valor);
			
		}
		if (vectorDatosPlanificacion!=null && !vectorDatosPlanificacion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0102");
			hashValores.put("0.plan.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0102PT");
			hashValores.put("0.plan.0102PT", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0102SE");
			hashValores.put("0.plan.0102SE", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0102ET");
			hashValores.put("0.plan.0102ET", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0102RT");
			hashValores.put("0.plan.0102RT", valor);
		}
		
		if (vectorDatosProduccion!=null && !vectorDatosProduccion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion , r_semana, "0.pre.0102");
			hashValores.put("0.pre.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102");
			hashValores.put("0.prod.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102PT");
			hashValores.put("0.prod.0102PT", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102SE");
			hashValores.put("0.prod.0102SE", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102ET");
			hashValores.put("0.prod.0102ET", valor);

			valor = this.recuperarMovimientosProduccionHorizontal(new Integer(r_ejercicio),r_semana,r_campo); 
			hashValores.put("0.prod.0102CajaHorizontal", valor);
			
/*
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102MediaPaleta");
			hashValores.put("0.prod.0102MediaPaleta", valor);

			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102Troncoconica");
			hashValores.put("0.prod.0102Troncoconica", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102Resto");
			hashValores.put("0.prod.0102Resto", valor);
*/
		}
		if (vectorDatosVenta!=null && !vectorDatosVenta.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.0102");
			hashValores.put("0.venta.0102", valor);
			
/*			
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.0102Troncoconica");
			hashValores.put("0.venta.0102Troncoconica", valor);
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.0102CajaHorizontal");
			hashValores.put("0.venta.0102CajaHorizontal", valor);
*/
			
		}
		
		if (vectorDatosVentaAnterior!=null && !vectorDatosVentaAnterior.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.0102");
			hashValores.put("1.venta.0102", valor);
			
/*			
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "1.venta.0102Troncoconica");
			hashValores.put("1.venta.0102Troncoconica", valor);
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "1.venta.0102CajaHorizontal");
			hashValores.put("1.venta.0102CajaHorizontal", valor);
*/
		}
		
		if ((vectorDatosStock==null || vectorDatosStock.isEmpty()) && (vectorDatosVenta==null || vectorDatosVenta.isEmpty()) && (vectorDatosVenta==null && vectorDatosVenta.isEmpty()) && (vectorDatosProduccion==null || !vectorDatosProduccion.isEmpty()))
		{
			Notificaciones.getInstance().mensajeAdvertencia("No se han encontrado registros referidos al periodo indicado.");
		}
		else
		{
			consultaHorasTrabajadasEmbotelladoraServer ches = new consultaHorasTrabajadasEmbotelladoraServer(CurrentUser.get());
			
			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
			hashValores.put("0.horas.0102", valor);

			valor = ches.recuperarHorasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasReales.0102", valor);

			valor = ches.recuperarHorasEmbotelladoReales("horasAutomatico",new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasAutomatico.0102", valor);

			valor = ches.recuperarHorasEmbotelladoReales("horasJaulon",new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasJaulon.0102", valor);

			valor = ches.recuperarHorasEmbotelladoReales("horasEtiquetado",new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasEtiquetado.0102", valor);
			
			valor = ches.recuperarHorasEmbotelladoReales("horasHorizontal",new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasHorizontal.0102", valor);
//			
//			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), "Troncoconica");
//			hashValores.put("0.horas.0102Troncoconica", valor);
//
//			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), "CajaHorizontal");
//			hashValores.put("0.horas.0102CajaHorizontal", valor);
//			
			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.inc.0102", valor);

			valor = ches.recuperarIncidenciasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.incReales.0102", valor);
			
//			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana), "Troncoconica");
//			hashValores.put("0.inc.0102Troncoconica", valor);
//			
//			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana), "CajaHorizontal");
//			hashValores.put("0.inc.0102Ca", valor);
//
			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.pro.0102", valor);

			valor = ches.recuperarProgramadasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.proReales.0102", valor);
			
//			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana), "Troncoconica");
//			hashValores.put("0.pro.0102Troncoconica", valor);
//			
//			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana), "CajaHorizontal");
//			hashValores.put("0.pro.0102CajaHorizontal", valor);
//
			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.mer.0102", valor);

//			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana), 5);
//			hashValores.put("0.mer.0102SE", valor);
//
//			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana), 2);
//			hashValores.put("0.mer.0102ET", valor);

		}
		
		mapeo = new MapeoGeneralDashboardEmbotelladora();
		mapeo.setVectorProduccion(vectorDatosProduccion);
		mapeo.setVectorVentas(vectorDatosVenta);
		mapeo.setVectorVentasAnterior(vectorDatosVentaAnterior);
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	

	private Double recuperaraDatoSolicitado(ArrayList<MapeoDashboardEmbotelladora> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.plan.0102": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("TT");
				}				
				break;
			}
			case "0.plan.0102PT": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("PT");
				}				
				break;
			}
			case "0.plan.0102ET": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("ET");
				}				
				break;
			}
			case "0.plan.0102RT": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("RT");
				}				
				break;
			}
			case "0.plan.0102SE": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("SE");
				}				
				break;
			}
			
			case "0.prod.0102": // produccion total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==49)
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.0102PT":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PT"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor - mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor - hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor - hash.get(j);
							}
						}
					}

				}
				break;
			}
			case "0.prod.0102ET": //etiquetado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}

				break;
			}
			case "0.prod.0102SE": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.0102": // venta total
			case "1.venta.0102":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						valor = valor + mapeo.getTotal();
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private ArrayList<MapeoDashboardEmbotelladora> recuperarMovimientosProduccion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
//			sql.append(" e_articu.marca as formato, " );
			sql.append(" e_articu.tipo_articulo as tipo, " );  // PS o PT
			sql.append(" a_lin_" + digito + ".movimiento as mov, "); //49 o 99
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			*/
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0102%' ");
			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(fecha_documento,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(fecha_documento) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" and a_lin_" + digito + ".movimiento in ('49','99') ");
			sql.append(" GROUP BY  e_articu.tipo_articulo,a_lin_" + digito + ".movimiento ");
			sql.append(" order by  e_articu.tipo_articulo, a_lin_" + digito + ".movimiento ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladora>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladora mapeo = new MapeoDashboardEmbotelladora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	private Double recuperarMovimientosProduccionHorizontal(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			
			sql = new StringBuffer();
			
			sql.append(" SELECT sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			
			sql.append(" and e_articu.tipo_articulo = 'PT' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '0109%' and (descripcion like '%HORIZ%' or descripcion like '%STUCH%')) ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("uds");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return new Double(0);
	}

	private ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> recuperarMovimientosProduccionHorizontalFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT e_articu.familia as Familia, ");
			sql.append(" e_famili.descripcion as nombreFamilia, ");
			sql.append(" e_articu.sub_familia as Subfamilia, ");
			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
			sql.append(" sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito);
			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
			sql.append(" inner join e_famili on e_famili.familia = e_articu.familia ");
			sql.append(" inner join e_subfam on e_subfam.familia = e_articu.familia ");
			sql.append(" and e_subfam.sub_familia = e_articu.sub_familia ");
			
			sql.append(" where e_articu.tipo_articulo = 'PT' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and e_articu.articulo in (select padre from e__lconj where (hijo like '0109%' and (descripcion like '%HORIZ%' or descripcion like '%STUCH%')) ");
			sql.append(" or (hijo like '0106%' and descripcion like '%ENVOLV%')) ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" group by 1,2,3,4 ") ;
			sql.append(" order by 1,2,3,4 ") ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = new MapeoDashboardEmbotelladoraProduccionAnalizada();
				mapeo.setFamilia(rsMovimientos.getString("Familia"));
				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
				mapeo.setTotal(rsMovimientos.getDouble("uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	private ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> recuperarMovimientosVentasFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a_lin_" + digito + ".familia as Familia, ");
			sql.append(" e_famili.descripcion as nombreFamilia, ");
			sql.append(" a_lin_" + digito + ".sub_familia as Subfamilia, ");
			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
			sql.append(" sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito);
			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
			sql.append(" inner join e_famili on e_famili.familia = a_lin_" + digito + ".familia ");
			sql.append(" inner join e_subfam on e_subfam.familia = a_lin_" + digito + ".familia ");
			sql.append(" and e_subfam.sub_familia = a_lin_" + digito + ".sub_familia ");
			
			sql.append(" where e_articu.tipo_articulo = 'PT' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') "); //49 o 99
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" group by 1,2,3,4 ") ;
			sql.append(" order by 1,2,3,4 ") ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = new MapeoDashboardEmbotelladoraProduccionAnalizada();
				mapeo.setFamilia(rsMovimientos.getString("Familia"));
				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
				mapeo.setTotal(rsMovimientos.getDouble("uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	private ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> recuperarMovimientosProduccionJaulonFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT e_articu.familia as Familia, ");
			sql.append(" e_famili.descripcion as nombreFamilia, ");
			sql.append(" e_articu.sub_familia as Subfamilia, ");
			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
			sql.append(" sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito);
			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
			sql.append(" inner join e_famili on e_famili.familia = e_articu.familia ");
			sql.append(" inner join e_subfam on e_subfam.familia = e_articu.familia ");
			sql.append(" and e_subfam.sub_familia = e_articu.sub_familia ");
			
			sql.append(" where e_articu.tipo_articulo = 'PS' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" group by 1,2,3,4 ") ;
			sql.append(" order by 1,2,3,4 ") ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = new MapeoDashboardEmbotelladoraProduccionAnalizada();
				mapeo.setFamilia(rsMovimientos.getString("Familia"));
				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
				mapeo.setTotal(rsMovimientos.getDouble("uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	private ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> recuperarMovimientosProduccionDirectaFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT e_articu.familia as Familia, ");
			sql.append(" e_famili.descripcion as nombreFamilia, ");
			sql.append(" e_articu.sub_familia as Subfamilia, ");
			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
			sql.append(" sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito);
			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
			sql.append(" inner join e_famili on e_famili.familia = e_articu.familia ");
			sql.append(" inner join e_subfam on e_subfam.familia = e_articu.familia ");
			sql.append(" and e_subfam.sub_familia = e_articu.sub_familia ");
			
			sql.append(" where e_articu.tipo_articulo = 'PT' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" group by 1,2,3,4 ") ;
			sql.append(" order by 1,2,3,4 ") ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = new MapeoDashboardEmbotelladoraProduccionAnalizada();
				mapeo.setFamilia(rsMovimientos.getString("Familia"));
				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
				mapeo.setTotal(rsMovimientos.getDouble("uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	private ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> recuperarMovimientosProduccionEtiquetadoFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT e_articu.familia as Familia, ");
			sql.append(" e_famili.descripcion as nombreFamilia, ");
			sql.append(" e_articu.sub_familia as Subfamilia, ");
			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
			sql.append(" sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito);
			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
			sql.append(" inner join e_famili on e_famili.familia = e_articu.familia ");
			sql.append(" inner join e_subfam on e_subfam.familia = e_articu.familia ");
			sql.append(" and e_subfam.sub_familia = e_articu.sub_familia ");
			
			sql.append(" where e_articu.tipo_articulo = 'PS' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and a_lin_" + digito + ".movimiento = '99' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" group by 1,2,3,4 ") ;
			sql.append(" order by 1,2,3,4 ") ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = new MapeoDashboardEmbotelladoraProduccionAnalizada();
				mapeo.setFamilia(rsMovimientos.getString("Familia"));
				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
				mapeo.setTotal(rsMovimientos.getDouble("uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private ArrayList<MapeoDashboardEmbotelladora> recuperarMovimientosPlanificacion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double total = new Double(0);
		int desde = 1;
		int hasta = 53;
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT ejercicio as ejercicio, ");
			sql.append(" prd_programacion.tipoOperacion as ope, " );  // PS o PT
			sql.append(" e_articu.tipo_articulo as tipo, " );  // PS o PT
			sql.append(" SUM( unidades*" + r_campo + " ) as total ");

			sql.append(" FROM prd_programacion ");
			sql.append(" inner join e_articu on e_articu.articulo COLLATE latin1_spanish_ci = prd_programacion.articulo COLLATE latin1_spanish_ci and e_articu.articulo like '0102%' ");
			sql.append(" where ejercicio = " + r_ejercicio);
			if (this.ambitoTemporal.contentEquals("Acumulado")) sql.append(" and semana <= '" + r_semana + "' ");
			else if (this.ambitoTemporal.contentEquals("Mensual"))
			{
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				sql.append(" and semana between " + desde + " and " + hasta);
			}
			else if (!r_semana.contentEquals("0")) sql.append(" and semana = '" + r_semana + "' ");
			sql.append(" GROUP BY  ejercicio, prd_programacion.tipoOperacion, e_articu.tipo_articulo ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladora>();
			HashMap<String, Double> hash = new HashMap<String, Double>();
			MapeoDashboardEmbotelladora mapeo = new MapeoDashboardEmbotelladora();
			
			while(rsMovimientos.next())
			{
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));

				switch (rsMovimientos.getString("ope"))
				{
					case "EMBOTELLADO":
					{
						if (rsMovimientos.getString("tipo").contentEquals("PS")) 
						{
							hash.put("SE", rsMovimientos.getDouble("total"));
						}
						else 
						{
							hash.put("PT", rsMovimientos.getDouble("total")); 							
						}
						total=total+rsMovimientos.getDouble("total");
						break;
					}
					case "ETIQUETADO":
					{
						hash.put("ET", rsMovimientos.getDouble("total"));
						total=total+rsMovimientos.getDouble("total");
						break;
					}
					case "RETRABAJO":
					{
						hash.put("RT", rsMovimientos.getDouble("total"));
						break;
					}
				}
			}
			
			hash.put("TT", total);
			mapeo.setHashValores(hash);
			vector.add(mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private ArrayList<MapeoDashboardEmbotelladora> recuperarMovimientosVentas(Integer r_ejercicio,String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			if (ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = 'PT' ");
			sql.append(" and e_articu.articulo like '0102%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			if (this.ambitoTemporal.contentEquals("Acumulado"))
			{
				sql.append(" and week(fecha_documento,3) <= " + r_semana);
			}
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladora>();
			
			int tope = 0;
			
			if (ambitoTemporal.contentEquals("Semanal"))
			{
				tope = 53;
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				tope = 12;
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				tope = 4;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				tope = 2;
			}
			else if (ambitoTemporal.contentEquals("Acumulado"))
			{
				tope = -1;
			}
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladora mapeo = new MapeoDashboardEmbotelladora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
//				mapeo.setFormato(rsMovimientos.getInt("formato"));
//				mapeo.setColor(rsMovimientos.getString("color"));
//				mapeo.setPalet(rsMovimientos.getString("palet"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public HashMap<String, Double>  recuperarExistenciasMes(Integer r_ejercicio,String r_tipo)
	{
		HashMap<String, Double> hash = null;
		hash = new HashMap<String, Double>();
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		hash=cis.recuperarExistenciasMes(r_ejercicio, r_tipo, "'1','2','4','6','10','11','12','20','30'");
		return hash;
	}
	
	private HashMap<String, Double>  recuperarStock(Integer r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double valorTotal = null;
		Double ValorFormatoPT = null;
		Double ValorFormatoPS = null;
		Integer formatoOld = null;
		
		try
		{
			
			valorTotal = new Double(0);
			ValorFormatoPT = new Double(0);
			ValorFormatoPS = new Double(0);
			formatoOld = new Integer(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.tipo_articulo as tipo, " );
			sql.append(" SUM( existencias*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and a_exis_" + digito + ".almacen in (1,2,4,6,10,11,12,20,30) ");
			sql.append(" GROUP BY e_articu.tipo_articulo");
			sql.append(" order by e_articu.tipo_articulo desc ");
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				
				valorTotal = valorTotal + rsMovimientos.getDouble("Total");
				
				if (rsMovimientos.getString("tipo").contentEquals("PT"))
				{
					ValorFormatoPT = ValorFormatoPT + rsMovimientos.getDouble("Total");
					
				}
				else if (rsMovimientos.getString("tipo").contentEquals("PS"))
				{
					ValorFormatoPS = ValorFormatoPS + rsMovimientos.getDouble("Total");
				}
			}
				
			hash.put("0.stock.0102PT", ValorFormatoPT);
			hash.put("0.stock.0102SE", ValorFormatoPS);
			hash.put("0.stock.0102", valorTotal);
				
//				for (int i = 1; i<=53;i++)
//				{
//					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
//				}
//				
//				mapeo.setHashValores(hash);
//				mapeo.setTotal(rsMovimientos.getDouble("Total"));
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarMovimientosVentasEjercicio(Integer r_ejercicio, String r_mascara,String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
			 
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				Integer hastaSemana = 0;
				
				if (digito.contentEquals("0"))
				{
					if (r_semana ==null) 
						hastaSemana = new Integer(RutinasFechas.semanaActual(String.valueOf(r_ejercicio))); 
					else 
						hastaSemana=new Integer(r_semana);
				}
				else hastaSemana = 53;

				for (int i = 1; i<=hastaSemana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	

	
	
	public HashMap<String,	Double> recuperarVentasComparativaEjercicios(Integer r_ejercicio, String r_mascara, int r_cuantos, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			hash = new HashMap<String, Double>();
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			for (int i = 0; i<r_cuantos; i ++)
			{
				if (r_ejercicio - i == ejercicioActual) digito="0";
				if (r_ejercicio - i < ejercicioActual) digito=String.valueOf(r_ejercicio-i);
			
				sql = new StringBuffer();
			
				sql.append(" SELECT year(fecha_documento) as ejercicio, ");
				sql.append(" SUM( unidades*" + r_campo + " ) AS 'total' ");
			  
				sql.append(" FROM a_lin_" + digito + ", e_articu ");
				sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
				sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
				sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
				sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
				sql.append(" GROUP BY ejercicio ");

				
				if (i==0) con= this.conManager.establecerConexion();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rsMovimientos = cs.executeQuery(sql.toString());
			
			
				while(rsMovimientos.next())
				{
					
					hash.put(String.valueOf(r_ejercicio-i), rsMovimientos.getDouble("total"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarVentasArticuloEjercicio(Integer r_ejercicio, String r_mascara, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			int semana = 53;
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual)
			{
				digito="0";
				semana = new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			}
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a_lin_" + digito + ".articulo as articulo, ");
			sql.append(" SUM( unidades*" + r_campo + "/ " + semana + ") AS 'total' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY e_articu.articulo");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				
				hash.put(rsMovimientos.getString("articulo").trim(), rsMovimientos.getDouble("total"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarStockArticulo(String r_mascara, String r_campo)
	{
		HashMap<String, Double> hash =null;
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		hash = cis.recuperarStockArticulo(r_mascara, r_campo, "1,2,4,6,10,11,12,20,30");
		return hash;
	}

	public HashMap<String,	Double> recuperarVentasAcumulados(Integer r_ejercicio,String  r_mascara, String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		CallableStatement statement = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			
			String digito = "0";
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			Integer hastaSemana = 0;
//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			
			if ((r_semana==null || r_semana.contentEquals("0")) && digito.contentEquals("0"))
			{
				sql = " "; 
				hastaSemana=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString()));
			}
			else if (this.ambitoTemporal.contentEquals("Acumulado"))
			{
				hastaSemana=new Integer(r_semana);
				sql= " and week(fecha_documento,3)<= '" + hastaSemana + "' ";
			}
			else
			{
				hastaSemana=53;
				sql= " and week(fecha_documento,3)<= '" + hastaSemana + "' ";
			}
			
			con= this.conManager.establecerConexion();			
			
			
			statement = con.prepareCall("{call ventaAcumuladaBotella(?,?,?,?)}");

			statement.setString(1, r_campo);
			statement.setString(2, digito);
			statement.setString(3, r_mascara);
			statement.setString(4, sql);
			boolean hadResults = statement.execute();

			while (hadResults) 
			{
				rsMovimientos = statement.getResultSet();
				hash = new HashMap<String, Double>(); 
				// process result set
				while(rsMovimientos.next())
				{
					
					for (int i = 1; i<=hastaSemana;i++)
					{
						hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
					}
				}


				hadResults = statement.getMoreResults();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}

	public HashMap<String, Double> recuperarProductividad(Integer r_ejercicio)
	{
		HashMap<String, Double> productividadObtenida = null;
		HashMap<String, Double> hashHoras = null;
		HashMap<String, Double> hashIncidencias = null;
		HashMap<String, Double> hashProgramadas= null;
    	Double velocidadNominal = null;
    	Double productividad = null;
    	Double tiempoOperativo = null;
    	Double horas = null;
    	Double incidencias = null;
    	Double programadas= null;
    	String semana = null;
    	int desde = 1;
    	int hasta = 53;
    	
    	consultaHorasTrabajadasEmbotelladoraServer ches = consultaHorasTrabajadasEmbotelladoraServer.getInstance(CurrentUser.get());
    	hashHoras = ches.recuperarHorasRealesAño(r_ejercicio);
    	hashIncidencias = ches.recuperarIncidenciasRealesAño(r_ejercicio);
    	hashProgramadas = ches.recuperarProgramadasRealesAño(r_ejercicio);
    	
		productividadObtenida = new HashMap<String, Double>();
		
		if (this.ambitoTemporal.contentEquals("Semanal") || this.ambitoTemporal.contentEquals("Acumulado")||this.ambitoTemporal.contentEquals("Anual"))
		{
			desde=1;
			hasta=53;
		}
		else if (this.ambitoTemporal.contentEquals("Menusal"))
		{
			try{
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(r_ejercicio.intValue(), Integer.valueOf(semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + semana + "/" + r_ejercicio));
			}
			catch (Exception ex)
			{
				
			}
		}
		
		for (int i = desde; i<=hasta;i++)
		{
			semana = String.valueOf(i);

			if (hashHoras.get(semana)!=null)
				horas = hashHoras.get(semana);
			else
				horas=new Double(0);
			if (hashIncidencias.get(semana)!=null)
				incidencias = hashIncidencias.get(semana);
			else
				incidencias=new Double(0);
			
			if (hashProgramadas.get(semana)!=null)
				programadas = hashProgramadas.get(semana);
			else
				programadas=new Double(0);
			
			tiempoOperativo = horas - incidencias - programadas;

			if (tiempoOperativo!=null && tiempoOperativo!=0)
			{
				velocidadNominal = this.obtenerTiempoDeCiclo(r_ejercicio.toString(), semana.toString(), "Semanal", false);
				productividad = (velocidadNominal / tiempoOperativo)*100;
				productividadObtenida.put(semana.toString(), productividad);
			}
			else
				productividadObtenida.put(semana.toString(), new Double(0));
		}
		return productividadObtenida;
	}

	public HashMap<String, Double> recuperarProductividadMedia(HashMap<String, Double> r_hash)
	{
		HashMap<String, Double> productividadObtenida = null;
		String semana=null;
		Integer cuantos = 0;
		Double prod = new Double(0);
		Double media = null;
		productividadObtenida = new HashMap<String, Double>();
		int desde = 1;
		int hasta = 1;
		
		if (this.ambitoTemporal.contentEquals("Semanal") || this.ambitoTemporal.contentEquals("Acumulado")||this.ambitoTemporal.contentEquals("Anual"))
		{
			desde=1;
			hasta=53;
		}
		else if (this.ambitoTemporal.contentEquals("Menusal"))
		{
			try
			{
				
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + semana + "/" + ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(ejercicio).intValue(), Integer.valueOf(semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + semana + "/" + ejercicio));

			}
			catch (Exception ex)
			{
				
			}
		}
		
		for (int i = desde; i<=r_hash.size();i++)
		{
			semana = String.valueOf(i);
			
			if (r_hash.get(semana)!=null && r_hash.get(semana)!=0)
			{
				cuantos++;
				prod = prod + r_hash.get(semana); 
			}
		}
		media = prod / cuantos;
		
		for (int i = desde; i<=hasta;i++)
		{
			productividadObtenida.put(String.valueOf(i), media);
		}
		return productividadObtenida;
	}
	
	public Double obtenerTiempoDeCiclo(String r_ejercicio,String r_semana,String r_vista, boolean r_prevision)
	{
		Double velocidadNominal = new Double(0);
		
		HashMap<Integer, String> hashArticulos = null;
		HashMap<String, Double> hashVelocidades = null;
		HashMap<String, Double> hashProducciones = null;
		
		consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
		consultaProduccionDiariaServer cpds = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
		
		hashVelocidades = cps.obtenerHashVelocidades(r_ejercicio, r_semana, r_vista, r_prevision);
		
		if  (!r_prevision)
			hashProducciones = cpds.obtenerHashProducciones(this.area, r_ejercicio, r_semana, r_vista);
		else
			hashProducciones = cps.obtenerHashProducciones(r_ejercicio, r_semana, r_vista);
		
		if  (!r_prevision)
			hashArticulos = cpds.obtenerHashArticulos(this.area, r_ejercicio, r_semana, r_vista);
		else
			hashArticulos = cps.obtenerHashArticulos(r_ejercicio, r_semana, r_vista);
		
		for (int i = 0 ; i< hashArticulos.size(); i++)
		{
			if (hashVelocidades.get(hashArticulos.get(i))!=null && hashProducciones.get(hashArticulos.get(i))!=null)
			{
				System.out.println("Articulo: " + hashArticulos.get(i));
				System.out.println("Velocidad: " + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10)).toString()));
				System.out.println("Produccion: " + hashProducciones.get(hashArticulos.get(i)).toString());
				System.out.println("ciclo: " + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10))*hashProducciones.get(hashArticulos.get(i))));
				velocidadNominal = velocidadNominal + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10))*hashProducciones.get(hashArticulos.get(i)));
				System.out.println("ciclo acumulado: " + velocidadNominal.toString());
			}
		}
		
		return velocidadNominal;
	}
	
	public ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> produccionAnalizada(String r_produccionAnalizar,String r_ejercicio, String r_semana, String r_ambitoTemporal, String r_calculo)
	{
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector2 = null;
		
		switch (r_produccionAnalizar.toUpperCase())
		{
			case "DIRECTAS":
			{
				vector = this.recuperarMovimientosProduccionDirectaFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				vector2 = this.recuperarMovimientosProduccionEtiquetadoFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				for (int i = 0; i<vector.size();i++)
				{
					MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = vector.get(i);
					for (int j = 0; j<vector2.size();j++)
					{
						MapeoDashboardEmbotelladoraProduccionAnalizada mapeo2 = vector2.get(j);
						if (mapeo.getFamilia().toString().contentEquals(mapeo2.getFamilia().toString())  && mapeo.getSubfamilia().toString().contentEquals(mapeo2.getSubfamilia().toString()))
						{
							double nuevoValor = mapeo.getTotal()-mapeo2.getTotal();
							mapeo.setTotal(nuevoValor);
							vector.set(i, mapeo);
						}
					}
				}
				break;
			}
			case "JAULON":
			{
				vector = this.recuperarMovimientosProduccionJaulonFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				break;
			}
			case "ETIQUETADO":
			{
				vector = this.recuperarMovimientosProduccionEtiquetadoFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				vector2 = this.recuperarMovimientosProduccionHorizontalFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				
				for (int i = 0; i<vector.size();i++)
				{
					MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = vector.get(i);
					for (int j = 0; j<vector2.size();j++)
					{
						MapeoDashboardEmbotelladoraProduccionAnalizada mapeo2 = vector2.get(j);
						if (mapeo.getFamilia().toString().contentEquals(mapeo2.getFamilia().toString())  && mapeo.getSubfamilia().toString().contentEquals(mapeo2.getSubfamilia().toString()))
						{
							double nuevoValor = mapeo.getTotal()-mapeo2.getTotal();
							mapeo.setTotal(nuevoValor);
							vector.set(i, mapeo);
						}
					}
				}
				break;
			}
			case "HORIZONTAL":
			{
				vector = this.recuperarMovimientosProduccionHorizontalFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				break;
			}
			case "VENTAS":
			{
				vector = this.recuperarMovimientosVentasFamSubFam(new Integer(r_ejercicio), r_semana, r_calculo);
				break;
			}
		}
		
		
		return vector;
	}
}
