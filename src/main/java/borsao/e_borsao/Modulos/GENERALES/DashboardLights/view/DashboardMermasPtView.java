package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.vaadin.addons.d3Gauge.GaugeStyle;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.reloj;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.server.consultaCostePersonalServer;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardMermasPt;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardMermasPtServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaCalidad;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardMermasPtView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Mermas Producción";
	private final String titulo = "Mermas PT";
    private Label lblTitulo = null; 
    
    private CssLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private GridLayout centralBottom = null;
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;
    private Panel panelDesglose =null;
    private GridLayout centralMiddle = null;
    private Panel panelCostes =null;
    private GridLayout centralMiddle2 = null;
    
	public ComboBox cmbSemana = null;
	public ComboBox cmbVista = null;
	public TextField txtEjercicio= null;
	public String semana = null;

	private Button procesar=null;
	private Button opcMenos= null;
	private Button opcMas= null;
	private Button opcSalir = null;
	
	private MapeoGeneralDashboardMermasPt mapeo=null;
	private Integer tamañoRelojes = 160;	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */

    public DashboardMermasPtView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
    	this.cargarComboVista();
    	this.cargarCombo(null);
		this.cargarListeners();
	}

    private void cargarPantalla() 
    {
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new CssLayout();
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

		    	this.topLayoutL = new HorizontalLayout();
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.cmbVista= new ComboBox("Vista");    	
			    	this.cmbVista.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbVista.setNewItemsAllowed(false);
			    	this.cmbVista.setWidth("150px");
			    	this.cmbVista.setNullSelectionAllowed(false);

			    	this.txtEjercicio=new TextField("Ejercicio");
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.cmbSemana= new ComboBox("Semana");    	
		    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		    		this.cmbSemana.setNewItemsAllowed(false);
		    		this.cmbSemana.setWidth("125px");
		    		this.cmbSemana.setNullSelectionAllowed(false);

		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		    		
		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);
		        	
			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.cmbVista);
		    	this.topLayoutL.addComponent(this.txtEjercicio);
	    		this.topLayoutL.addComponent(this.opcMenos);
	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.cmbSemana);
	    		this.topLayoutL.addComponent(this.opcMas);
	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

		    panelResumen = new Panel("Resumen Semana");
		    panelResumen.setSizeUndefined();
		    panelResumen.setWidth("100%");
		    panelResumen.setVisible(false);
		    panelResumen.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelResumen );

		    	this.centralTop2 = new GridLayout(5,1);
		    	this.centralTop2.setSpacing(true);
		    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	
		    	panelResumen.setContent(centralTop2);

		    panelDesglose= new Panel("Desglose Mermas");
		    panelDesglose.setSizeUndefined();
		    panelDesglose.setWidth("100%");
		    panelDesglose.setVisible(false);
		    panelDesglose.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelDesglose );
		    
			    this.centralMiddle2 = new GridLayout(5,1);
			    this.centralMiddle2.setWidth("100%");
			    this.centralMiddle2.setMargin(true);
	
			    panelDesglose.setContent(centralMiddle2);

		    panelCostes = new Panel("Costes");
		    panelCostes.setSizeUndefined();
		    panelCostes.setWidth("100%");
		    panelCostes.setVisible(false);
		    panelCostes.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelCostes );

		    	this.centralMiddle = new GridLayout(3,2);
		    	this.centralMiddle.setWidth("100%");
		    	this.centralMiddle.setMargin(true);

		    	panelCostes.setContent(centralMiddle);

	    	this.centralBottom = new GridLayout(2,1);
	    	this.centralBottom.setWidth("100%");
	    	this.centralBottom.setMargin(true);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(panelResumen);
    	this.barAndGridLayout.addComponent(panelDesglose);
    	this.barAndGridLayout.addComponent(panelCostes);
    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
    }

    private void cargarListeners() 
    {
    	this.cmbSemana.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
		    	if (cmbSemana.getValue()!=null)
				semana = cmbSemana.getValue().toString();
			}
		});

    	this.cmbVista.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
		    	cargarCombo(null);
		    	cmbSemana.setValue(semana);

			}
		});
    	
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
				}
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));
			}
		});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});
    }

    private void procesar()
    {
		eliminarDashBoards();
		
		if (cmbVista.getValue()==null || (!cmbVista.getValue().toString().contentEquals("Anual") && cmbSemana.getValue()==null) || txtEjercicio.getValue()==null 
				|| cmbVista.getValue().toString().length()==0 || (!cmbVista.getValue().toString().contentEquals("Anual") && cmbSemana.getValue().toString().length()==0) || txtEjercicio.getValue().length()==0 )
		{
			Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
		}
		else
		{
			cargarDashboards();
		}
    }

    public void eliminarDashBoards()
    {
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    }
    
    public void cargarDashboards() 
    {
    	String sem =null;
    	String ejer =null;
    	String vista=null;
    	String campo =null;
    	
    	if (this.txtEjercicio.getValue()!=null) ejer = this.txtEjercicio.getValue();
    	if (this.cmbSemana.getValue()!=null) sem = this.cmbSemana.getValue().toString();
    	if (this.cmbVista.getValue()!=null) vista = this.cmbVista.getValue().toString();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		case "Trimestral":
    		case "Mensual":
    		case "Semestral":
    		{
    			this.mapeo = consultaDashboardMermasPtServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;
    		}
    		case "Acumulado":
    		{
    			this.mapeo = consultaDashboardMermasPtServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;   			
    		}
    		case "Anual":
    		{
    			this.mapeo = consultaDashboardMermasPtServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, "0", vista,campo);
    			break;
    		}
    		default:
    		{
    			this.mapeo = consultaDashboardMermasPtServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista,campo);
    			break;
    		}
    	}
    	/*
    	 * cargar panel resumen datos
    	 */
    	cargarResumen();
    	cargarDesgloseMermas();
    	cargarCostesMermas();
    }
    
    private void cargarResumen()
    {
    	Panel panel = cargoPanelProduccion();
    	centralTop2.addComponent(panel,0,0);
    	
    	Panel panelm = cargoPanelMermas();
    	centralTop2.addComponent(panelm,1,0);
    	
    	Panel reloj = cargoKPICalidad();
		centralTop2.addComponent(reloj,2,0);

    	this.panelResumen.setVisible(true);
    	this.panelResumen.getContent().setVisible(true);

    }
    
    private void cargarDesgloseMermas()
    {
    	
    	Panel panel2 = cargoPanelMermasPT();
    	centralMiddle2.addComponent(panel2,0,0);

    	Panel panel1 = cargoPanelMermasMP();
    	centralMiddle2.addComponent(panel1,1,0);
    	
//    	cargarDesgloseMermasProduccion();
//    	cargarResumenHorasPlanificadas();
    	
    	this.panelDesglose.setVisible(true);
    	this.panelDesglose.getContent().setVisible(false);
    }

    private void cargarCostesMermas()
    {
    	
    	panelCostes.setVisible(false);
    	panelCostes.getContent().setVisible(false);

    }
    
    private Panel cargoPanelMermasMP()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "120px";

    	Label lblMermas = null;
    	Label lblMermas1 = null;

    	Label lblMP0104= null;
    	Label lblValorMP0104= null;
    	Label lblMP0105= null;
    	Label lblValorMP0105= null;
    	Label lblMP0106= null;
    	Label lblValorMP0106= null;
    	Label lblMP0107= null;
    	Label lblValorMP0107= null;
    	Label lblMP0108= null;
    	Label lblValorMP0108= null;
    	Label lblMP0109= null;
    	Label lblValorMP0109= null;
    	
    	Panel panel1 = new Panel("RESUMEN MERMAS MP");
    	panel1.setSizeUndefined(); // Shrink to fit content

	    	// Create the content
	    	VerticalLayout content1 = new VerticalLayout();

		    	HorizontalLayout linMP = new HorizontalLayout();
		    	linMP.setSpacing(true);
		    	
			    	lblMermas= new Label();
			    	lblMermas.setValue("TIPO ");
			    	lblMermas.setWidth(anchoLabelTitulo);
			    	lblMermas.addStyleName("lblTituloDerecha");
			
			    	lblMermas1 = new Label();
			    	lblMermas1.setValue("Unidades");
			    	lblMermas1.setWidth(anchoLabelTitulo);
			    	lblMermas1.addStyleName("lblTituloDerecha");

	    		linMP.addComponent(lblMermas);
	    		linMP.addComponent(lblMermas1);
	    		
		    	HorizontalLayout linMP04 = new HorizontalLayout();
		    	linMP04.setSpacing(true);
		    	
			    	lblMP0104 = new Label();
			    	lblMP0104.setValue(" Botellas ");
			    	lblMP0104.setWidth(anchoLabelTitulo);
			    	lblMP0104.addStyleName("lblTituloDerecha");
			
			    	lblValorMP0104 = new Label();
			    	lblValorMP0104.setWidth(anchoLabelDatos);
			    	lblValorMP0104.addStyleName(ValoTheme.LABEL_TINY);
			    	lblValorMP0104.addStyleName("lblDatos");
			    	lblValorMP0104.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0104")));

	    		linMP04.addComponent(lblMP0104);
	    		linMP04.addComponent(lblValorMP0104);

		    	HorizontalLayout linMP05 = new HorizontalLayout();
		    	linMP05.setSpacing(true);
		    	
			    	lblMP0105 = new Label();
			    	lblMP0105.setValue(" Corcho/Tapones ");
			    	lblMP0105.setWidth(anchoLabelTitulo);
			    	lblMP0105.addStyleName("lblTituloDerecha");
			
			    	lblValorMP0105 = new Label();
			    	lblValorMP0105.setWidth(anchoLabelDatos);
			    	lblValorMP0105.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorMP0105.addStyleName("lblDatos");
			    	lblValorMP0105.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0105")));
	
	    		linMP05.addComponent(lblMP0105);
	    		linMP05.addComponent(lblValorMP0105);
	    		
		    	HorizontalLayout linMP06 = new HorizontalLayout();
		    	linMP06.setSpacing(true);
		    	
			    	lblMP0106 = new Label();
			    	lblMP0106.setValue(" Frontales ");
			    	lblMP0106.setWidth(anchoLabelTitulo);
			    	lblMP0106.addStyleName("lblTituloDerecha");
			    	
			    	lblValorMP0106 = new Label();
			    	lblValorMP0106.setWidth(anchoLabelDatos);
			    	lblValorMP0106.addStyleName(ValoTheme.LABEL_TINY);
			    	lblValorMP0106.addStyleName("lblDatos");
			    	lblValorMP0106.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0106")));
		    	
		    	linMP06.addComponent(lblMP0106);
		    	linMP06.addComponent(lblValorMP0106);
		    	
		    	HorizontalLayout linMP07 = new HorizontalLayout();
		    	linMP07.setSpacing(true);
		    	
			    	lblMP0107 = new Label();
			    	lblMP0107.setValue(" Contras ");
			    	lblMP0107.setWidth(anchoLabelTitulo);
			    	lblMP0107.addStyleName("lblTituloDerecha");
			    	
			    	lblValorMP0107 = new Label();
			    	lblValorMP0107.setWidth(anchoLabelDatos);
			    	lblValorMP0107.addStyleName(ValoTheme.LABEL_TINY);
			    	lblValorMP0107.addStyleName("lblDatos");
			    	lblValorMP0107.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0107")));
		    	
		    	linMP07.addComponent(lblMP0107);
		    	linMP07.addComponent(lblValorMP0107);
	    	
		    	HorizontalLayout linMP08 = new HorizontalLayout();
		    	linMP08.setSpacing(true);
			    	
			    	lblMP0108 = new Label();
			    	lblMP0108.setValue(" Capsulas ");
			    	lblMP0108.setWidth(anchoLabelTitulo);
			    	lblMP0108.addStyleName("lblTituloDerecha");
			    	
			    	lblValorMP0108 = new Label();
			    	lblValorMP0108.setWidth(anchoLabelDatos);
			    	lblValorMP0108.addStyleName(ValoTheme.LABEL_TINY);
			    	lblValorMP0108.addStyleName("lblDatos");
			    	lblValorMP0108.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0108")));
		    	
		    	linMP08.addComponent(lblMP0108);
		    	linMP08.addComponent(lblValorMP0108);
	    	
		    	HorizontalLayout linMP09 = new HorizontalLayout();
		    	linMP09.setSpacing(true);
		    	
			    	lblMP0109 = new Label();
			    	lblMP0109.setValue(" Cajas/Bandejas ");
			    	lblMP0109.setWidth(anchoLabelTitulo);
			    	lblMP0109.addStyleName("lblTituloDerecha");
			    	
			    	lblValorMP0109 = new Label();
			    	lblValorMP0109.setWidth(anchoLabelDatos);
			    	lblValorMP0109.addStyleName(ValoTheme.LABEL_TINY);
			    	lblValorMP0109.addStyleName("lblDatos");
			    	lblValorMP0109.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0109")));
		    	
		    	linMP09.addComponent(lblMP0109);
		    	linMP09.addComponent(lblValorMP0109);
    	
	    	content1.addComponent(linMP);
	    	content1.addComponent(linMP04);
	    	content1.addComponent(linMP05);
	    	content1.addComponent(linMP06);
	    	content1.addComponent(linMP07);
	    	content1.addComponent(linMP08);
	    	content1.addComponent(linMP09);
	    	
	    	content1.setSizeUndefined(); // Shrink to fit
	    	content1.setSpacing(false);
	    	content1.setMargin(true);
    	panel1.setContent(content1);
	    	
    	return panel1;
    }
    
    private Panel cargoPanelMermas()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "120px";

    	Label lblMermas = null;
    	Label lblMermas1 = null;
    	Label lblMermas3 = null;
    	Label lblMermas4 = null;

    	Label lblMPT= null;
    	Button lblValorMPT= null;
    	Label lblPorcMPT= null;
    	Label lblEurMPT= null;

    	Label lblMMP= null;
    	Button lblValorMMP= null;
    	
    	Panel panelm = new Panel("RESUMEN MERMAS");
    	panelm.setSizeUndefined();

	    	// Create the content
	    	VerticalLayout contentm = new VerticalLayout();
	
	    	HorizontalLayout linM = new HorizontalLayout();
	    	linM.setSpacing(true);
		    	lblMermas= new Label();
		    	lblMermas.setValue("TIPO ");
		    	lblMermas.setWidth(anchoLabelTitulo);
		    	lblMermas.addStyleName("lblTituloDerecha");
		
		    	lblMermas1 = new Label();
		    	lblMermas1.setValue("Unidades");
		    	lblMermas1.setWidth(anchoLabelTitulo);
		    	lblMermas1.addStyleName("lblTituloDerecha");

		    	lblMermas3 = new Label();
		    	lblMermas3.setValue("%./Prod");
		    	lblMermas3.setWidth(anchoLabelTitulo);
		    	lblMermas3.addStyleName("lblTituloDerecha");

		    	lblMermas4 = new Label();
		    	lblMermas4.setValue("€./mer");
		    	lblMermas4.setWidth(anchoLabelTitulo);
		    	lblMermas4.addStyleName("lblTituloDerecha");

		    	linM.addComponent(lblMermas);
		    	linM.addComponent(lblMermas1);
		    	linM.addComponent(lblMermas3);
		    	linM.addComponent(lblMermas4);
	
	    	HorizontalLayout linM1= new HorizontalLayout();
	    	linM1.setSpacing(true);
		    	
			    	lblMPT = new Label();
			    	lblMPT.setValue(" Pt ");
			    	lblMPT.setWidth(anchoLabelTitulo);
			    	lblMPT.addStyleName("lblTituloDerecha");
			
			    	lblValorMPT = new Button();
			    	lblValorMPT.setWidth(anchoLabelDatos);
			    	lblValorMPT.addStyleName(ValoTheme.BUTTON_TINY);
			    	lblValorMPT.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorMPT.addStyleName("lblDatos");
			    	lblValorMPT.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0102")));

			    	lblValorMPT.addClickListener(new ClickListener() 
			    	{
						
						@Override
						public void buttonClick(ClickEvent event) {
						}
					});

			    	lblPorcMPT = new Label();
			    	lblPorcMPT.setWidth(anchoLabelDatos);
			    	lblPorcMPT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0102") / this.mapeo.getHashValores().get("0.prod.0102")*100));
			    	lblPorcMPT.addStyleName("lblDatos");

			    	lblEurMPT = new Label();
			    	lblEurMPT.setWidth(anchoLabelDatos);
			    	lblEurMPT.setValue(RutinasNumericas.formatearDouble(obtenerCosteMermaPt()));
			    	lblEurMPT.addStyleName("lblDatos");

			    	linM1.addComponent(lblMPT);
			    	linM1.addComponent(lblValorMPT);
			    	linM1.addComponent(lblPorcMPT);
			    	linM1.addComponent(lblEurMPT);
			    	
		    	HorizontalLayout linM2= new HorizontalLayout();
		    	linM2.setSpacing(true);
		    	
			    	lblMMP = new Label();
			    	lblMMP.setValue(" MP ");
			    	lblMMP.setWidth(anchoLabelTitulo);
			    	lblMMP.addStyleName("lblTituloDerecha");
			    	
			    	lblValorMMP = new Button();
			    	lblValorMMP.setWidth(anchoLabelDatos);
			    	lblValorMMP.addStyleName(ValoTheme.BUTTON_TINY);
			    	lblValorMMP.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorMMP.addStyleName("lblDatos");
			    	lblValorMMP.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.01")));
			    	
			    	linM2.addComponent(lblMMP);
			    	linM2.addComponent(lblValorMMP);
			    	
		    	contentm.addComponent(linM);
		    	contentm.addComponent(linM1);
		    	contentm.addComponent(linM2);
		    	
	    	contentm.setSizeUndefined(); // Shrink to fit
	    	contentm.setSpacing(false);
	    	contentm.setMargin(true);
	    	panelm.setContent(contentm);
	    	
	    	return panelm;

    }
    private Panel cargoPanelProduccion()
    {
    	
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "120px";

    	Label lblTurnos = null;
    	Button lblValorTurnos = null;
    	Label lblProduccionTotal = null;
    	Label lblProduccionBT= null;
    	Label lblProduccionSE= null;
    	Label lblProduccionET= null;
    	
    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccionBT= null;
    	Label lblValorProduccionSE= null;
    	Label lblValorProduccionET= null;

    	Panel panel = new Panel("RESUMEN PRODUCCION");
    	panel.setSizeUndefined();

	    	// Create the content
	    	VerticalLayout content = new VerticalLayout();
	
		    	HorizontalLayout lin = new HorizontalLayout();
		    	lin.setSpacing(true);
			    	lblTurnos = new Label();
			    	lblTurnos.setValue("Turnos: ");
			    	lblTurnos.setWidth(anchoLabelTitulo);
			    	lblTurnos.addStyleName("lblTitulo ");
			
			    	lblValorTurnos = new Button();
			    	lblValorTurnos.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0102")/8));
	
			    	lblValorTurnos.setWidth(anchoLabelDatos);
			    	lblValorTurnos.addStyleName(ValoTheme.BUTTON_TINY);
			    	lblValorTurnos.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorTurnos.addStyleName("lblDatos");
			    	lblValorTurnos.addClickListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
	//						String valorSemana = null;
	//						if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
	//							valorSemana = cmbSemana.getValue().toString();
	//						else
	//							valorSemana = "";
							
	//						pantallaAnalisisTurnos vtT = new pantallaAnalisisTurnos(txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString());
	//						getUI().addWindow(vtT);
						}
					});
	
		
		    	lin.addComponent(lblTurnos);
		    	lin.addComponent(lblValorTurnos);
		
		    	HorizontalLayout lin1= new HorizontalLayout();
		    	lin1.setSpacing(true);
		    	HorizontalLayout lin2 = new HorizontalLayout();
		    	lin2.setSpacing(true);
		    	HorizontalLayout lin3 = new HorizontalLayout();
		    	lin3.setSpacing(true);
		    	HorizontalLayout lin4 = new HorizontalLayout();
		    	lin4.setSpacing(true);
			    	
			    	lblProduccionTotal = new Label();
			    	lblProduccionTotal.setValue("Producción: ");
			    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
			    	lblProduccionTotal.addStyleName("lblTitulo");
		
			    	lblValorProduccionTotal = new Label();
			    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
			    	lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102")));
			    	lblValorProduccionTotal.addStyleName("lblDatos");
			    	
		    	lin1.addComponent(lblProduccionTotal);
		    	lin1.addComponent(lblValorProduccionTotal);
			    	
			    	lblProduccionBT = new Label();
			    	lblProduccionBT.setValue(" Bt: ");
			    	lblProduccionBT.setWidth(anchoLabelTitulo);
			    	lblProduccionBT.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionBT = new Label();
			    	lblValorProduccionBT.setWidth(anchoLabelDatos);
			    	lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102PT")));
			    	lblValorProduccionBT.addStyleName("lblDatos");
			    	
		    	lin2.addComponent(lblProduccionBT);
		    	lin2.addComponent(lblValorProduccionBT);
				    	
			    	lblProduccionSE = new Label();
			    	lblProduccionSE.setValue("A Jaulon: ");
			    	lblProduccionSE.setWidth(anchoLabelTitulo);
			    	lblProduccionSE.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionSE = new Label();
			    	lblValorProduccionSE.setWidth(anchoLabelDatos);
			    	lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102SE")));
			    	lblValorProduccionSE.addStyleName("lblDatos");
			    	
		    	lin3.addComponent(lblProduccionSE);
		    	lin3.addComponent(lblValorProduccionSE);
	
		    		lblProduccionET = new Label();
			    	lblProduccionET.setValue("Etiquetado: ");
			    	lblProduccionET.setWidth(anchoLabelTitulo);
			    	lblProduccionET.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionET= new Label();
			    	lblValorProduccionET.setWidth(anchoLabelDatos);
			    	lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102ET")));
			    	lblValorProduccionET.addStyleName("lblDatos");
				    	
		    	lin4.addComponent(lblProduccionET);
		    	lin4.addComponent(lblValorProduccionET);
	    							    				    	
	    	content.addComponent(lin);
	    	content.addComponent(lin1);
	    	content.addComponent(lin2);
	    	content.addComponent(lin3);
	    	content.addComponent(lin4);
	    	content.setSizeUndefined(); // Shrink to fit
	    	content.setSpacing(false);
	    	content.setMargin(true);
		panel.setContent(content);
		
		return panel;
    }    
    
    private Panel cargoPanelMermasPT()
    {
    	String sem =null;
    	Integer ejer =null;
    	String vista=null;
    	String campo =null;
    	
    	if (this.txtEjercicio.getValue()!=null) ejer = new Integer(this.txtEjercicio.getValue());
    	if (this.cmbSemana.getValue()!=null) sem = this.cmbSemana.getValue().toString();
    	if (this.cmbVista.getValue()!=null) vista = this.cmbVista.getValue().toString();

    	consultaDashboardMermasPtServer cmpts = consultaDashboardMermasPtServer.getInstance(CurrentUser.get());
    	ArrayList<MapeoArticulos> vectorGraneles = cmpts.recuperarMermasPtPorGranel(ejer, sem, vista);
    	
    	IndexedContainer container =null;
    	Iterator<MapeoArticulos> iterator = null;
    	MapeoArticulos mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
		container.addContainerProperty("litros", Double.class, null);
			
		iterator = vectorGraneles.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoArticulos) iterator.next();
			
			file.getItemProperty("articulo").setValue(mapeo.getArticulo().substring(0,7));
			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
			file.getItemProperty("litros").setValue(mapeo.getCuantos());
		}

		Grid gridDatos= new Grid();
		gridDatos.setContainerDataSource(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
		gridDatos.setWidth("100%");
		gridDatos.addStyleName("minigrid");

		calcularTotalesGrid(gridDatos);
		asignarEstilos(gridDatos);
		
		Panel panel = new Panel("DESGLOSE GRANELES - PT");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setContent(gridDatos);
		
    	return panel;
    }
    
    private void cargarComboVista()
    {
    	this.cmbVista.removeAllItems();
    	this.cmbVista.addItem("Semanal");
    	this.cmbVista.addItem("Mensual");
    	this.cmbVista.addItem("Anual");
    	this.cmbVista.addItem("Acumulado");
    	this.cmbVista.setValue("Semanal");
    }
    
	private void cargarCombo(String r_ejercicio)
	{
		switch (cmbVista.getValue().toString())
		{
			case "Anual":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setCaption("");
				this.cmbSemana.setEnabled(false);				
				break;
			}
			case "Semestral":
			{
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Semestre");
				this.cmbSemana.removeAllItems();
				for (int i=1; i<=2; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}

				break;
			}
			case "Trimestral":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Trimestre");
				for (int i=1; i<=4; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}
				break;
			}
			case "Mensual":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Mes");
				for (int i=1; i<=12; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}
				break;
			}
			case "Semanal":
			case "Acumulado":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Semana");
				if (r_ejercicio==null)
				{
					this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
					r_ejercicio = this.txtEjercicio.getValue();
				}
				else
				{
					this.cmbSemana.removeAllItems();
				}

				if (RutinasFechas.semanaActual(r_ejercicio)-1 < 1 )
				{
					this.semana = "53";
					this.txtEjercicio.setValue(String.valueOf((new Integer(RutinasFechas.añoActualYYYY())-1)));
					r_ejercicio = this.txtEjercicio.getValue();
				}
				else
				{
					this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio)-1);
				}
				int semanas = RutinasFechas.semanasAño(r_ejercicio);
				
				for (int i=1; i<=semanas; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}

				break;
			}
		}
	}

    private void vaciarPantalla()
    {
        centralMiddle=null;
        centralBottom=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }

    private void asignarEstilos(Grid r_grid)
    {
    	r_grid.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("litros".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	return "cell-normal";
            }
    	});
    }
    
    private void calcularTotalesGrid(Grid r_grid)
    {
    	Indexed indexed=null;
    	List<?> list = null;
    	Double total = new Double(0);
    	
    	r_grid.appendFooterRow();    	
    	indexed = r_grid.getContainerDataSource();
        list = new ArrayList<Object>(indexed.getItemIds());
        
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	Double valor = (Double) item.getItemProperty("litros").getValue();
			total += valor.doubleValue();
        }
        
        r_grid.getFooterRow(0).getCell("articulo").setText("Totales ");
        r_grid.getFooterRow(0).getCell("litros").setText(RutinasNumericas.formatearDouble(total));
        
		r_grid.getFooterRow(0).getCell("litros").setStyleName("Rcell-pie");

    }
 
    private Panel cargoKPICalidad()
    {
		String calidad = obtenerCalidad (this.mapeo.getHashValores().get("0.prod.0102"), this.mapeo.getHashValores().get("0.mer.0102"));
		Panel gaugeC = reloj.generarReloj("CALIDAD " + calidad,(new Double(RutinasCadenas.reemplazarComaMiles(calidad)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
		com.vaadin.event.MouseEvents.ClickListener listenerQ = new com.vaadin.event.MouseEvents.ClickListener() {
			
			@Override
			public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
				pantallaCalidad vtQ = new pantallaCalidad("Calidad", txtEjercicio.getValue().toString());
				getUI().addWindow(vtQ);
			}
		};
		gaugeC.addClickListener(listenerQ);
		return gaugeC;
    }
 
    private Double obtenerCosteMermaPt()
    {
    	Double coste = null;
    	Double horas = null;
    	Double horasTotales = null;
    	String valorHorasTotales = null;
    	Double produccion = null;
    	Double mermas = null;
    	Double costeDirecto = null;
    	Double costeIndirecto = null;
    	
    	horas = this.mapeo.getHashValores().get("0.horasReales.0102");
    	produccion = this.mapeo.getHashValores().get("0.prod.0102");
    	mermas = this.mapeo.getHashValores().get("0.mer.0102");
    	
    	consultaParametrosCalidadServer cpcs = new consultaParametrosCalidadServer(CurrentUser.get());
    	consultaCostePersonalServer ccs = new consultaCostePersonalServer(CurrentUser.get());
    	costeDirecto = ccs.recuperarCosteHoraGlobalArea(new Integer(this.txtEjercicio.getValue()), "EMBOTELLADORA",false);
    	costeIndirecto = ccs.recuperarCosteIndirectoHoraArea(new Integer(this.txtEjercicio.getValue()), "EMBOTELLADORA");
    	valorHorasTotales = cpcs.recuperarParametro(new Integer(this.txtEjercicio.getValue()), "Horas Embotelladora");
    	
    	if (valorHorasTotales!=null)
    		horasTotales = new Double(valorHorasTotales);
    	else
    		horasTotales = new Double(1);
    	
    	coste = horas*(costeDirecto+(costeIndirecto/horasTotales))/produccion*mermas;
    	
    	return coste;
    }
    
    private String obtenerCalidad(Double r_produccion, Double r_mermas)
    {
    	Double valor = (r_produccion - r_mermas)/(r_produccion );
    			
    	return (RutinasNumericas.formatearDoubleDecimales(new Double(valor*100),2));
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
}