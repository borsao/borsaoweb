package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.Date;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaLineasAnadaErroneas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaLineasStocksErroneos;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.NoConformidadesView;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.PedidosComprasView;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.server.consultaArticulosIntranetServer;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.view.ArticulosIntranetView;
import borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.view.FacturasVentasNacionalView;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.server.consultaFacturasVentasServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.view.VentasView;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.server.consultaStockExternosServer;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.view.consultaStockExternosView;
import borsao.e_borsao.Modulos.FINANCIERO.consultaApuntes.server.consultaApuntesServer;
import borsao.e_borsao.Modulos.FINANCIERO.consultaSaldos.server.consultaSaldosServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server.consultaRecepcionEMCSServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.MapeoUsuarioAlerta;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.server.consultaUsuarioAlertaServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.pantallaMermasErroneas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.server.consultaSituacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view.SituacionEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.consultaSituacionEmbotelladoView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardLightsView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoardGeneral";
	private final String titulo = "SITUACION GENERAL";
	private final int intervaloRefresco = 15*60*1000;
	private ChatRefreshListener cr = null;
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayout = null;
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;
    
    private HorizontalLayout centralTop = null;
    private VerticalLayout centralLayoutT1 = null;
    private VerticalLayout centralLayoutT2 = null;
    private GridLayout centralLayoutT3 = null;
    private HorizontalLayout centralBottom = null;
    private VerticalLayout centralLayoutB1 = null;
    private VerticalLayout centralLayoutB3 = null;
    private GridLayout centralLayoutB2 = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public DashboardLightsView() 
    {
    	
    	if (LecturaProperties.semaforos && eBorsao.get().accessControl.isVistaSemaforo())
    	{
			this.cargarPantalla();
			this.cargarListeners();
			this.cargarSemaforos();
			this.ejecutarTimer();
    	}
    }

    private void cargarPantalla() 
    {
    	this.setSizeFull();
//    	this.addStyleName("crud-view");    	
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSpacing(true);
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayout = new HorizontalLayout();
	    	this.topLayout.setWidth("100%");
	    	this.topLayout.setSpacing(true);
	    	
		    	this.topLayoutL = new HorizontalLayout();
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.addStyleName("v-panelTitulo");
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	if (LecturaProperties.semaforos) this.topLayoutL.addComponent(this.opcRefresh);
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.addComponent(this.lblTitulo);

	    	this.topLayout.addComponent(this.topLayoutL);
	    	this.topLayout.setComponentAlignment(this.topLayoutL, Alignment.MIDDLE_LEFT);
	    	this.topLayout.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

	    	this.centralTop = new HorizontalLayout();
	    	this.centralTop.setWidth("100%");
	    	this.centralTop.setHeight("50%");
	    	this.centralTop.setSpacing(true);
	    	this.centralTop.setMargin(true);
	    	
		    	this.centralLayoutT1= new VerticalLayout();
		    	this.centralLayoutT1.setCaption("Compras");
//		    	this.centralLayoutT1.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutT1.setWidth("25%");
		    	this.centralLayoutT1.setHeight("100%");
		    	this.centralLayoutT1.setSpacing(true);
		    	this.centralLayoutT1.setMargin(true);
		    	
		    	this.centralLayoutT2= new VerticalLayout();
		    	this.centralLayoutT2.setCaption("Produccion");
//		    	this.centralLayoutT2.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutT2.setWidth("25%");
		    	this.centralLayoutT2.setHeight("100%");
		    	this.centralLayoutT2.setSpacing(true);
		    	this.centralLayoutT2.setMargin(true);

		    	this.centralLayoutT3= new GridLayout(2,4);
		    	this.centralLayoutT3.setCaption("Almacen");
//		    	this.centralLayoutT3.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutT3.setWidth("25%");
		    	this.centralLayoutT3.setHeight("100%");
		    	this.centralLayoutT3.setSpacing(true);
		    	this.centralLayoutT3.setMargin(true);


	    	
	    	this.centralBottom= new HorizontalLayout();
	    	this.centralBottom.setWidth("100%");
	    	this.centralBottom.setHeight("50%");
	    	this.centralBottom.setSpacing(true);
	    	this.centralBottom.setMargin(true);
	    	
		    	this.centralLayoutB1= new VerticalLayout();
		    	this.centralLayoutB1.setCaption("Financiero");
//		    	this.centralLayoutB1.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutB1.setWidth("25%");
		    	this.centralLayoutB1.setHeight("100%");
		    	this.centralLayoutB1.setSpacing(true);
		    	this.centralLayoutB1.setMargin(true);
		    	
		    	this.centralLayoutB2= new GridLayout(2,4);
		    	this.centralLayoutB2.setCaption("Ventas");
		    	this.centralLayoutB2.setWidth("25%");
//		    	this.centralLayoutB2.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutB2.setHeight("100%");
		    	this.centralLayoutB2.setSpacing(true);
		    	this.centralLayoutB2.setMargin(true);
	    	
		    	this.centralLayoutB3= new VerticalLayout();
		    	this.centralLayoutB3.setCaption("Calidad");
//		    	this.centralLayoutB3.addStyleName("v-panelTituloSemaforo");
		    	this.centralLayoutB3.setWidth("25%");
		    	this.centralLayoutB3.setHeight("100%");
		    	this.centralLayoutB3.setSpacing(true);
		    	this.centralLayoutB3.setMargin(true);

    	this.barAndGridLayout.addComponent(this.topLayout);
    	this.barAndGridLayout.addComponent(this.centralTop);
    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
        
    }

    private void vaciarPantalla()
    {
    	centralTop=null;
        centralLayoutT1 = null;
        centralLayoutT2 = null;
        centralLayoutT3 = null;
        centralLayoutB1 = null;
        centralLayoutB2 = null;
        centralLayoutB3 = null;
        centralBottom=null;
        topLayout=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }
    
    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarListeners();
    	this.cargarSemaforos();
    }
    
    private void cargarSemaforos() 
    {
    	consultaUsuarioAlertaServer cuas =consultaUsuarioAlertaServer.getInstance(CurrentUser.get());
    	MapeoUsuarioAlerta mapeo = new MapeoUsuarioAlerta();
    	mapeo.setUsuario(CurrentUser.get());

    	ArrayList<MapeoUsuarioAlerta> vector = cuas.datosUsuarioAlertaGlobal(mapeo);
    	for (int i = 0; i<vector.size();i++)
    	{
    		MapeoUsuarioAlerta map = (MapeoUsuarioAlerta) vector.get(i); 
    		
    		switch (map.getDepartamento().toUpperCase())
    		{
    			case "COMPRAS":
    			{
    				this.cargarSemaforosCompras(centralLayoutT1);
    				this.centralTop.addComponent(this.centralLayoutT1);
    				break;
    			}
    			case "PRODUCCION":
    			{
    				this.cargarSemaforosProduccion(centralLayoutT2);
    				this.centralTop.addComponent(this.centralLayoutT2);
    				break;
    			}    			
    			case "ALMACEN":
    			{
    				this.cargarSemaforosAlmacen(centralLayoutT3);
    				this.centralTop.addComponent(this.centralLayoutT3);
    				break;
    			}
    			case "FINANCIERO":
    			{
    				this.cargarSemaforosFinanciero(this.centralLayoutB1);
    				this.centralBottom.addComponent(this.centralLayoutB1);
    				break;
    			}
    			case "CALIDAD":
    			{
    				this.cargarSemaforosCalidad(this.centralLayoutB3);
    				this.centralBottom.addComponent(this.centralLayoutB3);
    				break;
    			}
    			case "COMERCIAL":
    			{
    				this.cargarSemaforosVentas(this.centralLayoutB2);
    				this.centralBottom.addComponent(this.centralLayoutB2);
    				break;
    			}
    		}
    	}
    }

    private void cargarSemaforosCompras(VerticalLayout r_layout)
    {
    	
    	Button buttonSemaforo=null;
    	
    	String error = "0";
    	
    	consultaArticulosIntranetServer cis = consultaArticulosIntranetServer.getInstance(CurrentUser.get());
    	error=cis.semaforos();
    	
    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Intranet Actualizada?"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
                getUI().getNavigator().navigateTo(ArticulosIntranetView.VIEW_NAME);
    		}
    	});

    	r_layout.addComponent(buttonSemaforo);
    	
    	Label lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	error = "0";
    	consultaPedidosComprasServer cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
    	error=cps.semaforo();
    	
    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Pedidos sin Enviar?"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
                getUI().getNavigator().navigateTo(PedidosComprasView.VIEW_NAME);
    		}
    	});

    	r_layout.addComponent(buttonSemaforo);
    	
    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	error = "0";
    	cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
    	error=cps.semaforoPlazos();
    	
    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Pedidos Vencidos sin Entregar?"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
        		pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra("Lineas Pedidos Compra vencidos sin entregar");
        		getUI().addWindow(vt);
    		}
    	});

    	r_layout.addComponent(buttonSemaforo);

    }

    private void cargarSemaforosFinanciero(VerticalLayout r_layout)
    {
    	Button buttonSemaforo=null;

    	String error = "0";
    	
    	consultaApuntesServer cas = consultaApuntesServer.getInstance(CurrentUser.get());
    	error=cas.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Cuadre Diario Contable "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
		        	eBorsao.get().mainScreen.menu.verMenu(false);
		        	Notificaciones.getInstance().mensajeInformativo("Descuadre de asientos ejercicio actual. Compruebalo en GREENsys");
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo);
    	
    	Label lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	
    	//TODO Facturas sin presentar en el SII
    	
    	error = "0";
    	
    	consultaSaldosServer css = consultaSaldosServer.getInstance(CurrentUser.get());
    	error=css.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Cuadre Saldos "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
		        	eBorsao.get().mainScreen.menu.verMenu(false);
		        	Notificaciones.getInstance().mensajeInformativo("Descuadre de balance a grado 1 en el ejercicio actual. Compruebalo en GREENsys");
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo);
    
    }
    
    
    private void cargarSemaforosCalidad(VerticalLayout r_layout)
    {
    	Button buttonSemaforo=null;

    	String error = "5";
    	
    	consultaNoConformidadesServer cncs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
    	error=cncs.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "No Conformidades a Revisar"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("5"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
		        	eBorsao.get().mainScreen.menu.verMenu(false);
		        	getUI().getNavigator().navigateTo(NoConformidadesView.VIEW_NAME);
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo);
    	
    	Label lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);
    	
    	consultaRecepcionEMCSServer crs = consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
    	error=crs.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Albaranes Entrada sin EMCS asignado"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
		        	eBorsao.get().mainScreen.menu.verMenu(false);
		        	getUI().getNavigator().navigateTo(AsignacionEntradasEMCSView.VIEW_NAME);
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);
    	
    	consultaSeguimientoEMCSServer css = consultaSeguimientoEMCSServer.getInstance(CurrentUser.get());
    	error=css.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Albaranes Salida sin EMCS asignado"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
		        	eBorsao.get().mainScreen.menu.verMenu(false);
		        	getUI().getNavigator().navigateTo(AsignacionSalidasEMCSView.VIEW_NAME);
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo);

    }

    private void cargarSemaforosVentas(GridLayout r_layout)
    {
    	Button buttonSemaforo=null;
    	Label lblSeparador = null;
    	String error = "0";

    	if (eBorsao.get().getAccessControl().getDepartamento().toUpperCase().equals("INFORMATICA"))
    	{
    	
	    	consultaVentasServer cvs = new consultaVentasServer(CurrentUser.get());
	    	error=cvs.semaforos();
	
	    	buttonSemaforo = new Button("Ver >>");
	    	buttonSemaforo.setCaption(this.calcularColor(error, "Evolucion Ventas € "));
	    	buttonSemaforo.setCaptionAsHtml(true);
	    	if (!error.equals("0"))
	    	{
				buttonSemaforo.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(ClickEvent event) {
		            	eBorsao.get().mainScreen.menu.verMenu(false);
		                getUI().getNavigator().navigateTo(VentasView.VIEW_NAME);
					}
				});
	    	}
	    	
	    	r_layout.addComponent(buttonSemaforo,0,0);
	    	
//	    	lblSeparador= new Label();
//	    	lblSeparador.setValue("");
//	    	lblSeparador.setStyleName("labelTituloVentas");
//	    	r_layout.addComponent(lblSeparador);
//	
//	    	lblSeparador= new Label();
//	    	lblSeparador.setValue("");
//	    	lblSeparador.setStyleName("labelTituloVentas");
//	    	r_layout.addComponent(lblSeparador);
    	
    	}    	
    	error = "0";
    	
    	consultaFacturasVentasServer cfs = new consultaFacturasVentasServer(CurrentUser.get());
    	error=cfs.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Facturas Nacional Pendientes Enviar"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
	            	eBorsao.get().mainScreen.menu.verMenu(false);
	                getUI().getNavigator().navigateTo(FacturasVentasNacionalView.VIEW_NAME);
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo,0,1);
    	
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
//
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
    	
    	error = "0";
    	consultaPedidosVentasServer cps = new consultaPedidosVentasServer(CurrentUser.get());
    	error=cps.semaforos();


    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Pedidos sin PALLET  "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
	            	eBorsao.get().mainScreen.menu.verMenu(false);
	            	pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas("Pedidos Sin Pallet");
	        		getUI().addWindow(vt);    		
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo,0,2);
    	
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
//
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
    	
    	error = "0";
    	consultaStockExternosServer cse = new consultaStockExternosServer(CurrentUser.get());
    	error=cse.semaforos();
    	
    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Stock Almacenes Externos "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
	            	eBorsao.get().mainScreen.menu.verMenu(false);
	                getUI().getNavigator().navigateTo(consultaStockExternosView.VIEW_NAME);	
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo,0,3);
    	
    	
    	error = "0";
    	error=cse.semaforosPedidosMalCumplimentados();
    	
    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Pedidos Externos"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	if (!error.equals("0"))
    	{
			buttonSemaforo.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
	            	eBorsao.get().mainScreen.menu.verMenu(false);
	            	pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas("Pedidos Externos Erroneos");
	        		getUI().addWindow(vt); 
				}
			});
    	}
    	
    	r_layout.addComponent(buttonSemaforo,1,0);
    	
//    	
//    	error = "0";
//    	error=cvs.semaforosPVM();
//
//    	buttonSemaforo = new Button("Ver >>");
//    	buttonSemaforo.setCaption(this.calcularColor(error, "Evolucion Ventas PVM "));
//    	buttonSemaforo.setCaptionAsHtml(true);
//    	if (!error.equals("0"))
//    	{
//			buttonSemaforo.addClickListener(new ClickListener() {
//				@Override
//				public void buttonClick(ClickEvent event) {
//	            	eBorsao.get().mainScreen.menu.verMenu(false);
//	                getUI().getNavigator().navigateTo(VentasView.VIEW_NAME);
//				}
//			});
//    	}
//    	
//    	r_layout.addComponent(buttonSemaforo);
    
    }

    private void cargarSemaforosAlmacen(GridLayout r_layout)
    {
    	Button buttonSemaforo=null;

    	String error = "0";
    	
    	consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
    	consultaControlEmbotelladoServer cces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    	
    	error=cis.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Stocks Negativos "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) 
    		{
            	eBorsao.get().mainScreen.menu.verMenu(false);
            	pantallaLineasStocksErroneos vt = new pantallaLineasStocksErroneos("Articulos con stock negativo","Negativos");
        		getUI().addWindow(vt);    		
    		}
    	});

    	r_layout.addComponent(buttonSemaforo,0,0);
    	
//    	Label lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
//
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);

    	error = "0";
    	
    	error=cis.semaforosLotes();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Existencias - Lotes "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
            	pantallaLineasStocksErroneos vt = new pantallaLineasStocksErroneos("Articulos descuadrados entre existencias y lotes ","Lotes");
        		getUI().addWindow(vt);    		
    		}
    	});

    	r_layout.addComponent(buttonSemaforo,0,1);

//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
//
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);

    	error = "0";
    	
    	error=cis.semaforosAñada();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Añadas erróneas"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
            	pantallaLineasAnadaErroneas vt = new pantallaLineasAnadaErroneas("Articulos con Añada Erronea ");
        		getUI().addWindow(vt);    		
    		}
    	});

    	r_layout.addComponent(buttonSemaforo,0,2);

//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
//
//    	lblSeparador= new Label();
//    	lblSeparador.setValue("");
//    	lblSeparador.setStyleName("labelTituloVentas");
//    	r_layout.addComponent(lblSeparador);
    	
    	error = "0";
    	
    	error=cces.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Mermas sin procesar"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
            	pantallaMermasErroneas vt = new pantallaMermasErroneas("Mermas sin procesar anteriores a " + RutinasFechas.fechaActual());
        		getUI().addWindow(vt);    		
    		}
    	});

    	r_layout.addComponent(buttonSemaforo,0,3);
    	
    	error = "0";
    	
    	error=cis.semaforosLotesNegativos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Lotes Negativos"));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
            	pantallaLineasStocksErroneos vt = new pantallaLineasStocksErroneos("Lotes con existencias Negativas ","LotesNeg");
        		getUI().addWindow(vt);    		
    		}
    	});

    	r_layout.addComponent(buttonSemaforo,1,0);
    }
    
    private void cargarSemaforosProduccion(VerticalLayout r_layout)
    {
    	Button buttonSemaforo=null;

    	String error = "0";
    	
    	consultaSituacionEnvasadoraServer cses = consultaSituacionEnvasadoraServer.getInstance(CurrentUser.get());
    	error=cses.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Situacion Envasadora "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
                getUI().getNavigator().navigateTo(SituacionEnvasadoraView.VIEW_NAME);
    		}
    	});

    	r_layout.addComponent(buttonSemaforo);
    	
    	Label lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	lblSeparador= new Label();
    	lblSeparador.setValue("");
    	lblSeparador.setStyleName("labelTituloVentas");
    	r_layout.addComponent(lblSeparador);

    	error = "0";
    	
    	consultaAyudaProduccionServer caps = consultaAyudaProduccionServer.getInstance(CurrentUser.get());
    	error=caps.semaforos();

    	buttonSemaforo = new Button("Ver >>");
    	buttonSemaforo.setCaption(this.calcularColor(error, "Situacion Embotelladora "));
    	buttonSemaforo.setCaptionAsHtml(true);
    	buttonSemaforo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
            	eBorsao.get().mainScreen.menu.verMenu(false);
                getUI().getNavigator().navigateTo(consultaSituacionEmbotelladoView.VIEW_NAME);
    		}
    	});

    	r_layout.addComponent(buttonSemaforo);

    }

    private void cargarListeners() 
    {
    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});

    }

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	public void enter(ViewChangeEvent event) 
	{
		this.cargarPantalla();
		this.cargarListeners();
		this.cargarSemaforos();
		if (LecturaProperties.semaforos) this.ejecutarTimer();
	}

    private String calcularColor(String r_valor, String r_valorMostrar)
    {
    	String iconCode =null;
    	String color = null;
    	String icono = null;
    	int codigoIcono = 0;
    	
    			
    	if (r_valor=="0")
    	{
    		color="#2dd085";
        	icono = FontAwesome.CHECK_SQUARE.getFontFamily();
        	codigoIcono =FontAwesome.CHECK_SQUARE.getCodepoint();
    	}
    	else if (r_valor=="2")
    	{
    		color = "#cf050a";
        	icono = FontAwesome.EXCLAMATION_CIRCLE.getFontFamily();
        	codigoIcono =FontAwesome.EXCLAMATION_CIRCLE.getCodepoint();
    	}
    	else if (r_valor=="1")
    	{
    		color = "yellow";    		
        	icono = FontAwesome.CIRCLE.getFontFamily();
        	codigoIcono =FontAwesome.CIRCLE.getCodepoint();
    	}
    	else if (r_valor=="3")
    	{
    		color = "white";    		
        	icono = FontAwesome.CHECK_SQUARE.getFontFamily();
        	codigoIcono =FontAwesome.CHECK_SQUARE.getCodepoint();
    	}
    	else if (r_valor=="4")
    	{
    		color = "black";    		
        	icono = FontAwesome.CHECK_SQUARE.getFontFamily();
        	codigoIcono =FontAwesome.CHECK_SQUARE.getCodepoint();
    	}
    	
		iconCode = "<span class=\"v-icon\" style=\"font-family: "
                + icono + ";color:" + color
                + "\">&#x"
                + Integer.toHexString(codigoIcono)
                + ";</span>";

		return (r_valorMostrar) + " " + iconCode;
    }
    
    private void ejecutarTimer()
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }
	
	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco semaforos" + new Date());
    		refrescar();
    	}
    }

}
