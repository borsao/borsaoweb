package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.util.HashMap;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEmbotelladoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaProductividad extends Window
{

	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;

    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;
    
	public TextField txtEjercicio= null;
	public String  ejercicio= null;

	private boolean verMenu = true;
	public boolean acceso = true;
	
	public pantallaProductividad(String r_titulo)
	{
		
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			this.setCaption(r_titulo);
			this.center();
			
	    	this.setSizeFull();
	    	this.addStyleName("crud-view");
	    	this.setResponsive(true);
	
	    	this.cargarPantalla();
			
			this.cargarListeners();
	
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setSizeFull();
			
	    	this.setContent(this.barAndGridLayout);
	    	this.setResponsive(true);
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}

	}
	public pantallaProductividad(String r_titulo, String r_ejercicio)
	{
		acceso = this.obtenerPermisos();
		ejercicio = r_ejercicio;
		if (acceso)
		{
			verMenu = false;
			this.setCaption(r_titulo);
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarResumen();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	if (ejercicio!=null && ejercicio.length()>0)
		    		this.txtEjercicio.setValue(ejercicio);
		    	else
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
		    	
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
	    	this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumen = new Panel("CALIDAD ");
	    	this.panelResumen.setSizeFull();
	    	this.panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(1,1);
			    this.centralTop2.setSizeFull();
			    this.centralTop2.setMargin(true);

	    	this.panelResumen.setContent(centralTop2);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumen);
    	this.barAndGridLayout.setExpandRatio(this.panelResumen, 1);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
    private void cargarResumen()
	{
		Double media = null;
		HashMap<String, Double> hashProductividad= null;
		HashMap<String, Double> hashProductividadMedia = null;

		consultaDashboardEmbotelladoraServer cdes = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get());
		hashProductividad= cdes.recuperarProductividad(new Integer(this.txtEjercicio.getValue()));
		hashProductividadMedia = cdes.recuperarProductividadMedia(hashProductividad);
		
	    String tituloY = null;
	    
	    tituloY="Productividad. / Semana";
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Productividad.", "Semanas", tituloY, hashProductividad, hashProductividadMedia, null, txtEjercicio.getValue(), "Media", null, null, null,0,55,120,true, false, false);
	    
	    centralTop2.addComponent(wrapper,0,0);
	    
	    for (int i =0; i<hashProductividadMedia.size();i++)
	    {
	    	if (hashProductividadMedia.get(String.valueOf(i))!=null)
	    	{
	    		media = hashProductividadMedia.get(String.valueOf(i));
	    		break;
	    	}
	    }
	    if (media!=null)
	    {
	    	panelResumen.setCaption("Productvidad Año " + txtEjercicio.getValue().toString() + " Con un valor medio de: " + RutinasNumericas.formatearDouble(media));
	    }
	    else
	    {
	    	panelResumen.setCaption("Productividad Año " + txtEjercicio.getValue().toString());
	    }
	}
}