package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardMermasPt;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardMermasPt;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server.consultaHorasTrabajadasEmbotelladoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardMermasPtServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDashboardMermasPtServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivoLitros = 33750;
	public static Integer produccionObjetivoBotellas = 45000;
	private String ambitoTemporal = null; 
	private String semana = null; 
	private String ejercicio = null; 
	
	public consultaDashboardMermasPtServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardMermasPtServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardMermasPtServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardMermasPt recuperarValoresEstadisticos(String r_ejercicio, String r_semana, String r_vista,String r_campo)
	{
		Double valor = null;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 *  
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod.0102)
		 *      - total produccion prevista por formato del periodo				(0.pre.0102)
		 *  
		 *  	- mermas total del periodo										(0.mer.0102)
		 *   
		 *   	- produccion del periodo										(0.prod.0102PT) 
		 *   																	(0.prod.0102SE)
		 *   																	(0.prod.0102ET)
		 *   
   		 *   	- mermas del periodo											(0.mer.0102PT) 
		 *   
		 */
		ResultSet rsOpcion = null;		
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardMermasPt mapeo = null;
		ArrayList<MapeoDashboardMermasPt> vectorDatosProduccion = null;
		ArrayList<MapeoDashboardMermasPt> vectorDatosMermas = null;

		this.ambitoTemporal=r_vista;
		if (ambitoTemporal.contentEquals("Anual"))
		{
			r_semana = "0";
			semana = "0";
		}
		r_campo="1";
		
		hashValores = new HashMap<String, Double>();

		vectorDatosProduccion = this.recuperarMovimientosProduccion(new Integer(r_ejercicio),r_semana,r_campo);
		vectorDatosMermas = this.recuperarMovimientosMermasMP(new Integer(r_ejercicio),r_semana,r_campo);
		
		
		if (vectorDatosMermas!=null && !vectorDatosMermas.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.01");
			hashValores.put("0.mer.01", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0104");
			hashValores.put("0.mer.0104", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0105");
			hashValores.put("0.mer.0105", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0106");
			hashValores.put("0.mer.0106", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0107");
			hashValores.put("0.mer.0107", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0108");
			hashValores.put("0.mer.0108", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosMermas , r_semana, "0.mer.0109");
			hashValores.put("0.mer.0109", valor);
			
		}
		if (vectorDatosProduccion!=null && !vectorDatosProduccion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion , r_semana, "0.pre.0102");
			hashValores.put("0.pre.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102");
			hashValores.put("0.prod.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102PT");
			hashValores.put("0.prod.0102PT", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102SE");
			hashValores.put("0.prod.0102SE", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102ET");
			hashValores.put("0.prod.0102ET", valor);

			valor = this.recuperarMovimientosProduccionHorizontal(new Integer(r_ejercicio),r_semana,r_campo); 
			hashValores.put("0.prod.0102CajaHorizontal", valor);
			
		}
		if ((vectorDatosMermas==null || vectorDatosMermas.isEmpty()) && (vectorDatosProduccion==null || vectorDatosProduccion.isEmpty()))
		{
			Notificaciones.getInstance().mensajeAdvertencia("No se han encontrado registros referidos al periodo indicado.");
		}
		else
		{
			consultaHorasTrabajadasEmbotelladoraServer ches = new consultaHorasTrabajadasEmbotelladoraServer(CurrentUser.get());
		
			valor = ches.recuperarHorasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasReales.0102", valor);

			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.mer.0102", valor);
		}
		
		mapeo = new MapeoGeneralDashboardMermasPt();
		mapeo.setVectorProduccion(vectorDatosProduccion);
		mapeo.setVectorMermas(vectorDatosMermas);
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	

	private Double recuperaraDatoSolicitado(ArrayList<MapeoDashboardMermasPt> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.mer.01": // merma total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						for (int j = 1; j<=53;i++)
						{
							valor = valor + hash.get(j);
						}
					}
				}
				break;
			}
			case "0.mer.0101":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0101"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			
			case "0.mer.0104":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0104"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.mer.0105":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0105"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			
			case "0.mer.0106":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0106"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			

			case "0.mer.0107":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0107"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			

			case "0.mer.0108":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0108"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			

			case "0.mer.0109":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("0109"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}			

			case "0.prod.0102": // produccion total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==49)
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}

				}
				break;
			}
			case "0.prod.0102PT":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PT"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor - mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor - hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor - hash.get(j);
							}
						}
					}

				}
				break;
			}
			case "0.prod.0102ET": //etiquetado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}

				break;
			}
			case "0.prod.0102SE": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.0102": // venta total
			case "1.venta.0102":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardMermasPt mapeo = (MapeoDashboardMermasPt) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						valor = valor + mapeo.getTotal();
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private ArrayList<MapeoDashboardMermasPt> recuperarMovimientosProduccion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardMermasPt> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
//			sql.append(" e_articu.marca as formato, " );
			sql.append(" e_articu.tipo_articulo as tipo, " );  // PS o PT
			sql.append(" a_lin_" + digito + ".movimiento as mov, "); //49 o 99
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			*/
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0102%' ");
			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(fecha_documento,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(fecha_documento) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" and a_lin_" + digito + ".movimiento in ('49','99') ");
			sql.append(" GROUP BY  e_articu.tipo_articulo,a_lin_" + digito + ".movimiento ");
			sql.append(" order by  e_articu.tipo_articulo, a_lin_" + digito + ".movimiento ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardMermasPt>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardMermasPt mapeo = new MapeoDashboardMermasPt();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoArticulos> recuperarMermasPtPorGranel(Integer r_ejercicio, String r_semana, String r_vista)
	{
		ArrayList<MapeoArticulos> vectorGraneles = null;
		/*
		 * recuperar los articulos y cantidades para mi ejercicio, semana y acumulado
		 * de la tabla prd_mermas_pt
		 * agruparemos los datos por granel calculando los litros y obteniendo el precio medio
		 */
		consultaArticulosServer cas = null;
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			sql = new StringBuffer();
			sql.append(" SELECT c.hijo as articulo, ");
			
			/* convertimos a litros multiplicando por el factor*/
			sql.append(" sum(a.cantidad * replace(b.marca, ',','.')) as qt ");
			
			sql.append(" FROM prd_mermas_pt as a ");
			sql.append(" inner join e_articu as b on b.articulo = a.articulo ");
			sql.append(" inner join e__lconj as c on c.padre = a.articulo and c.hijo like '0101%' ");
			sql.append(" where a.ejercicio = " + r_ejercicio);
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and a.semana = '" + r_semana + "' ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a.dia) = '" + r_semana + "' ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1"))
				{
					sql.append(" and month(a.dia) in (1,2,3) ");
				}
				else if (r_semana.contentEquals("2"))
				{
					sql.append(" and month(a.dia) in (4,5,6) ");
				}
				else if (r_semana.contentEquals("3"))
				{
					sql.append(" and month(a.dia) in (7,8,9) ");
				}
				else
					sql.append(" and month(a.dia) in (10,11,12) ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1"))
				{
					sql.append(" and month(a.dia) in (1,2,3,4,5,6) ");
				}
				else 
				{
					sql.append(" and month(a.dia) in (7,8,9,10,11,12) ");
				}
			}

			sql.append(" group by c.hijo");
			sql.append(" order by c.hijo");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vectorGraneles = new ArrayList<MapeoArticulos>();
			
			while(rsMovimientos.next())
			{
				MapeoArticulos mapeo = new MapeoArticulos();
				mapeo.setArticulo(rsMovimientos.getString("articulo"));
				mapeo.setDescripcion(cas.obtenerDescripcionArticulo(rsMovimientos.getString("articulo")));
				mapeo.setCuantos(rsMovimientos.getDouble("qt"));
				
				vectorGraneles.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vectorGraneles;
	}
	
	private ArrayList<MapeoDashboardMermasPt> recuperarMovimientosMermasMP(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardMermasPt> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(cab.fecha) as ejercicio, ");
			sql.append(" substr(lin.articulo,1,4) as articuloMP, ");
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			 */
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '1' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '2' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '3' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '4' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '5' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '6' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '7' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '8' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '9' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '10' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '11' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '12' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '13' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '14' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '15' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '16' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '16', ");
				
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '17' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '18' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '19' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '20' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '21' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '22' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '23' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '24' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '25' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '26' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '27' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '28' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '29' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '30' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '31' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '32' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '33' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '34' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '34', ");
				
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '35' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '36' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '37' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '38' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '39' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '40' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '41' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '42' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '43' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '44' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '44', ");
				
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '45' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '46' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '47' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '48' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '49' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '50' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '51' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '52' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(cab.fecha,3) WHEN '53' THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 1 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 2 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 3 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 4 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 5 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 6 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 7 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 8 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 9 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 10 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 11 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) = 12 THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (1,2,3) THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (4,5,6) THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (7,8,9) THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (10,11,12) THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (1,2,3,4,5,6)  THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(cab.fecha) in (7,8,9,10,11,12) THEN lin.mermas*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( lin.mermas*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM prd_lin_control_embotellado as lin");
			sql.append(" inner join laboratorio.prd_control_embotellado as cab on lin.idprd_control=cab.idprd_control ");
			sql.append(" where cab.serie = 'EMBOTELLADO' ");
			sql.append(" and year(cab.fecha) = " + r_ejercicio );

			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(cab.fecha,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(cab.fecha) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(cab.fecha) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(cab.fecha) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(cab.fecha) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(cab.fecha) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(cab.fecha) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(cab.fecha) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" GROUP BY  substr(lin.articulo,1,4) ");
			sql.append(" order by  substr(lin.articulo,1,4) ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardMermasPt>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardMermasPt mapeo = new MapeoDashboardMermasPt();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
//				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("articuloMP"));
				
				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	private Double recuperarMovimientosProduccionHorizontal(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardMermasPt> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			
			sql = new StringBuffer();
			
			sql.append(" SELECT sum(unidades*" + r_campo + ") as uds ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			
			sql.append(" and e_articu.tipo_articulo = 'PT' " );  // PS o PT
			sql.append(" and e_articu.articulo like '0102%' ");
			sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '0109%' and (descripcion like '%HORIZ%' or descripcion like '%STUCH%')) ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' "); //49 o 99
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Acumulado") )
			{
				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("uds");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return new Double(0);
	}
}
