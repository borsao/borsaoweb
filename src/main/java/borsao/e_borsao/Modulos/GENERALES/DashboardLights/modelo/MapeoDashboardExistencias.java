package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDashboardExistencias extends MapeoGlobal
{
	private Integer ejercicio;
	private String añada;
	private String articulo;
	private String numeroLote;
	
	private HashMap<String, Double> hashValores = null;
	private double valor;
	private double totalPT;
	private double totalPS;

	public MapeoDashboardExistencias()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public double getTotalPT() {
		return totalPT;
	}

	public void setTotalPT(double r_total) {
		this.totalPT = r_total;
	}

	public double getTotalPS() {
		return totalPS;
	}
	
	public void setTotalPS(double r_total) {
		this.totalPS = r_total;
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public String getAñada() {
		return añada;
	}

	public void setAñada(String añada) {
		this.añada = añada;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(String  numeroLote) {
		this.numeroLote = numeroLote;
	}
}