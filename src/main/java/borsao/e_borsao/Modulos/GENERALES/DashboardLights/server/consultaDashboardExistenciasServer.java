package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardExistencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardExistencias;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;

public class consultaDashboardExistenciasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaDashboardExistenciasServer instance;

	
	private ArrayList<MapeoProgramacionEnvasadora> vectorProgramadoMesa = null;

	public consultaDashboardExistenciasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardExistenciasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardExistenciasServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardExistencias recuperarValoresEstadisticos(String r_ejercicio, String r_mes, String r_almacenes, String r_tipo, String r_alias)
	{
		Double valor = null;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 * 
		 *  Claves:
		 *  
		 *  	- stock PT														(0.pt)
		 *  	- stock PS														(0.ps)
		 *            
		 */
		HashMap<String, Double> hashValores = null;
		HashMap<String, Double> vectorDatosStock = null;

		MapeoGeneralDashboardExistencias mapeo = null;

		hashValores = new HashMap<String, Double>();
		vectorDatosStock = this.recuperarStock(r_ejercicio, r_mes, r_almacenes, r_tipo, r_alias);

		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.PT");
			hashValores.put("0.PT", valor);

			valor = vectorDatosStock.get("0.PS");
			hashValores.put("0.PS", valor);

			valor = vectorDatosStock.get("0.OA");
			hashValores.put("0.OA", valor);

			valor = vectorDatosStock.get("0.MP");
			hashValores.put("0.MP", valor);
		}
		
		mapeo = new MapeoGeneralDashboardExistencias();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	

	private HashMap<String, Double>  recuperarStock(String r_ejercicio, String r_mes, String r_almacenes, String r_tipo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double actual = null;
		Double pendiente = null;
		Double preparado = null;
		
		try
		{
			
			actual = new Double(0);
			pendiente = new Double(0);
			preparado = new Double(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (new Integer(r_ejercicio) == ejercicioActual) digito="0";
			if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(ejercicioActual-new Integer(r_ejercicio));
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.tipo_articulo tipo, ");
			sql.append(" SUM("+ r_mes + ") stock  ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and a_exis_" + digito + ".almacen in (" + r_almacenes + ") ");
			if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
			sql.append(" and e_articu.articulo like '" + r_tipo + "%' ");
			sql.append(" group by e_articu.tipo_articulo ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			System.out.println(sql.toString());
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				hash.put("0."+rsMovimientos.getString("tipo"), rsMovimientos.getDouble("stock"));
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	public ArrayList<MapeoDashboardExistencias>  recuperarUnidadesStockFamilias(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, ArrayList<String> r_familias, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
			
				sql.append(" SELECT  ");
				sql.append(" e_articu.familia familia, ");
				sql.append(" SUM("+ r_mesCalculo + ") stock  ");
				sql.append(" FROM a_exis_" + digito + " ");
				sql.append(" inner join e_articu on e_articu.articulo = a_exis_" + digito + ".articulo ");
				sql.append(" and e_articu.articulo like '" + r_tipo + "%' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" inner join e_almac on e_almac.almacen = a_exis_" + digito + ".almacen ");
				sql.append(" and e_almac.almacen in (" + r_almacenes + ") ");
				sql.append(" group by e_articu.familia ");
				sql.append(" having SUM("+ r_mesCalculo + ")<>0 ");
				sql.append(" order by e_articu.familia ");
			
				rsMovimientos = cs.executeQuery(sql.toString());
				
				hash = new HashMap<String, Double>();

				mapeoExistencias = new MapeoDashboardExistencias();
				mapeoExistencias.setEjercicio(ejercicioCalculo);
			
				while(rsMovimientos.next())
				{
					if (r_familias.contains(rsMovimientos.getString("familia"))) hash.put(rsMovimientos.getString("familia"), rsMovimientos.getDouble("stock"));
				}
				mapeoExistencias.setHashValores(hash);
				
				vectorExistencias.add(mapeoExistencias);
				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}
	public ArrayList<MapeoDashboardExistencias>  recuperarImportesStockFamilias(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, ArrayList<String> r_familias, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			
			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
				
				sql.append(" SELECT  ");
				sql.append(" e_articu.familia familia, ");
				sql.append(" SUM("+ r_mesCalculo + "*pre_medio_coste) stock  ");
				sql.append(" FROM a_exis_" + digito + " ");
				sql.append(" inner join e_articu on e_articu.articulo = a_exis_" + digito + ".articulo ");
				sql.append(" and e_articu.articulo like '" + r_tipo + "%' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" inner join e_almac on e_almac.almacen = a_exis_" + digito + ".almacen ");
				sql.append(" and e_almac.almacen in (" + r_almacenes + ") ");
				sql.append(" group by e_articu.familia ");
				sql.append(" having SUM("+ r_mesCalculo + "*pre_medio_coste) <>0 ");
				sql.append(" order by e_articu.familia ");
				
				rsMovimientos = cs.executeQuery(sql.toString());
				
				hash = new HashMap<String, Double>();
				
				mapeoExistencias = new MapeoDashboardExistencias();
				mapeoExistencias.setEjercicio(ejercicioCalculo);
				
				while(rsMovimientos.next())
				{
					if (r_familias.contains(rsMovimientos.getString("familia"))) hash.put(rsMovimientos.getString("familia"), rsMovimientos.getDouble("stock"));
				}
				mapeoExistencias.setHashValores(hash);
				
				vectorExistencias.add(mapeoExistencias);
				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}

	public ArrayList<MapeoDashboardExistencias>  recuperarUnidadesStockFamiliasSubFamilias(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, String r_familia, ArrayList<String> r_subfamilias, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
			
				sql.append(" SELECT  ");
				sql.append(" e_articu.sub_familia sub_familia, ");
				sql.append(" SUM("+ r_mesCalculo + ") stock  ");
				sql.append(" FROM a_exis_" + digito + " ");
				sql.append(" inner join e_articu on e_articu.articulo = a_exis_" + digito + ".articulo ");
				sql.append(" and e_articu.articulo like '" + r_tipo + "%' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				sql.append(" and e_articu.familia = '" + r_familia + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" inner join e_almac on e_almac.almacen = a_exis_" + digito + ".almacen ");
				sql.append(" and e_almac.almacen in (" + r_almacenes + ") ");
				sql.append(" group by e_articu.sub_familia ");
				sql.append(" having SUM("+ r_mesCalculo + ")<>0 ");
				sql.append(" order by e_articu.sub_familia ");
			
				rsMovimientos = cs.executeQuery(sql.toString());
				
				hash = new HashMap<String, Double>();

				mapeoExistencias = new MapeoDashboardExistencias();
				mapeoExistencias.setEjercicio(ejercicioCalculo);
			
				while(rsMovimientos.next())
				{
					if (r_subfamilias!=null && r_subfamilias.contains(rsMovimientos.getString("sub_familia"))) hash.put(rsMovimientos.getString("sub_familia"), rsMovimientos.getDouble("stock"));
					if (r_subfamilias==null || r_subfamilias.isEmpty()) hash.put(rsMovimientos.getString("sub_familia"), rsMovimientos.getDouble("stock"));
				}
				mapeoExistencias.setHashValores(hash);
				
				vectorExistencias.add(mapeoExistencias);
				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}

	public ArrayList<MapeoDashboardExistencias>  recuperarUnidadesStockFamiliasSubFamiliasAñadas(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, String r_familia, String r_subfamilia, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionGestionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			
			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
				
				sql.append(" SELECT  ");
				sql.append(" a_ubic_"+ digito + ".ubicacion ej, ");
				sql.append(" SUM(existencias) stock  ");
				sql.append(" FROM a_ubic_" + digito + " ");
				sql.append(" where articulo in (select articulo from e_articu ");
				sql.append(" where e_articu.articulo matches '" + r_tipo + "*' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				sql.append(" and e_articu.familia = '" + r_familia + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" and e_articu.sub_familia = '" + r_subfamilia + "') ");
				sql.append(" and almacen in (select almacen from e_almac ");
				sql.append(" where e_almac.almacen in (" + r_almacenes + ")) ");
				sql.append(" group by ubicacion ");
				sql.append(" having SUM(existencias)<>0 ");
				sql.append(" order by ubicacion ");
				
				rsMovimientos = cs.executeQuery(sql.toString());
				
				while(rsMovimientos.next())
				{
					mapeoExistencias = new MapeoDashboardExistencias();
					mapeoExistencias.setEjercicio(ejercicioCalculo);
					mapeoExistencias.setAñada(rsMovimientos.getString("ej"));
					mapeoExistencias.setValor(rsMovimientos.getDouble("stock"));
					vectorExistencias.add(mapeoExistencias);
				}
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}
	
	public ArrayList<MapeoDashboardExistencias>  recuperarUnidadesStockFamiliasSubFamiliasArticulos(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, String r_familia, String r_subfamilia, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			
			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
				
				sql.append(" SELECT  ");
				sql.append(" e_articu.articulo articulo, ");
				sql.append(" SUM("+ r_mesCalculo + ") stock  ");
				sql.append(" FROM a_exis_" + digito + " ");
				sql.append(" inner join e_articu on e_articu.articulo = a_exis_" + digito + ".articulo ");
				sql.append(" and e_articu.articulo like '" + r_tipo + "%' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				sql.append(" and e_articu.familia = '" + r_familia + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" and e_articu.sub_familia = '" + r_subfamilia + "' ");
				sql.append(" inner join e_almac on e_almac.almacen = a_exis_" + digito + ".almacen ");
				sql.append(" and e_almac.almacen in (" + r_almacenes + ") ");
				sql.append(" group by e_articu.articulo ");
				sql.append(" having SUM("+ r_mesCalculo + ") <> 0 ");
				sql.append(" order by e_articu.articulo ");
				
				rsMovimientos = cs.executeQuery(sql.toString());
				
				hash = new HashMap<String, Double>();
				
				mapeoExistencias = new MapeoDashboardExistencias();
				mapeoExistencias.setEjercicio(ejercicioCalculo);
				
				while(rsMovimientos.next())
				{
					hash.put(rsMovimientos.getString("articulo").trim(), rsMovimientos.getDouble("stock"));
				}
				mapeoExistencias.setHashValores(hash);
				
				vectorExistencias.add(mapeoExistencias);
				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}
	public ArrayList<MapeoDashboardExistencias>  recuperarUnidadesStockFamiliasSubFamiliasArticulosAñadas(ArrayList<Integer> r_vectorEjercicios, String r_mesCalculo, String r_almacenes, String r_tipo, String r_familia, String r_subfamilia, String r_añada, String r_tipoArticulo, String r_alias)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardExistencias> vectorExistencias = null; 
		MapeoDashboardExistencias mapeoExistencias = null; 
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejercicioCalculo = null;
		Iterator<Integer> itEjer = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			vectorExistencias = new ArrayList<MapeoDashboardExistencias>();
			
			con= this.conManager.establecerConexionGestionInd(); 			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			
			itEjer = r_vectorEjercicios.iterator();
			
			while (itEjer.hasNext())
			{
				ejercicioCalculo = itEjer.next();
				
				if ( ejercicioCalculo == ejercicioActual) digito="0";
				if (ejercicioCalculo < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicioCalculo);
				
				sql = new StringBuffer();
				
				sql.append(" SELECT  ");
				sql.append(" a_ubic_"+ digito + ".articulo articulo, ");
				sql.append(" a_ubic_"+ digito + ".numero_lote lote, ");
				sql.append(" SUM(existencias) stock  ");
				sql.append(" FROM a_ubic_" + digito + " ");
				sql.append(" where articulo in (select articulo from e_articu ");
				sql.append(" where e_articu.articulo matches '" + r_tipo + "*' ");
				sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
				if (r_alias!=null && r_alias.length()>0) sql.append(" and e_articu.descrip_articulo like '%" + r_alias + "%' ");
				sql.append(" and e_articu.familia = '" + r_familia + "' ");
				sql.append(" and e_articu.sub_familia = '" + r_subfamilia + "') ");
				sql.append(" and almacen in (select almacen from e_almac ");
				sql.append(" where e_almac.almacen in (" + r_almacenes + ")) ");
				sql.append(" and ubicacion = '" + r_añada + "' ");
				sql.append(" group by articulo, numero_lote ");
				sql.append(" having SUM(existencias)<>0 ");
				sql.append(" order by articulo, numero_lote ");
				
				rsMovimientos = cs.executeQuery(sql.toString());
				
				while(rsMovimientos.next())
				{
					mapeoExistencias = new MapeoDashboardExistencias();
					mapeoExistencias.setEjercicio(ejercicioCalculo);
					mapeoExistencias.setArticulo(rsMovimientos.getString("articulo"));
					if (rsMovimientos.getString("lote")!=null) mapeoExistencias.setNumeroLote(rsMovimientos.getString("lote"));
					mapeoExistencias.setValor(rsMovimientos.getDouble("stock"));
					vectorExistencias.add(mapeoExistencias);
				}
				break;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorExistencias;
	}
}
