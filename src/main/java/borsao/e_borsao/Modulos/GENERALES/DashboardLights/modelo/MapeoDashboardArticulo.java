package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDashboardArticulo extends MapeoGlobal
{
	private Integer ejercicio;
	
	private HashMap<String, Double> hashValores = null;
	private double total;

	public MapeoDashboardArticulo()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}
}