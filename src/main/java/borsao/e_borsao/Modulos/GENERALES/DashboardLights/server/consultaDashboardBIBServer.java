package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardBIB;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDetalleArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardBIB;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.server.consultaHorasTrabajadasBIBServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server.consultaHorasTrabajadasEmbotelladoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardBIBServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDashboardBIBServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivoLitros = 6000;
	public static Integer produccionObjetivoBotellas = 2000;
	public static String area = "BIB";
	private static String almacenes = "(1,2,4,6,10,11,12,20,30)";
	private String ambitoTemporal = null; 
	private String semana = null; 
	private String ejercicio = null; 
	
	public consultaDashboardBIBServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardBIBServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardBIBServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardBIB recuperarValoresEstadisticos(String r_ejercicio, String r_semana, String r_vista,String r_campo)
	{
		Double valor = null;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 *  
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod.0111)
		 *      - total produccion prevista por formato del periodo				(0.pre.0111)
		 *  	- produccion total programada del periodo						(0.plan.0111)
		 *  
		 *  	- mermas total del periodo										(0.mer.0111)
		 *   
		 *   	- produccion del periodo										(0.prod.010) 
		 *   	- produccion del periodo										(0.prod.011) 
		 *   																	(0.prod.020)
		 *   																	(0.prod.0130)

		 *   	- produccion planificada del periodo							(0.plan.010) 
		 *   	- produccion planificada del periodo							(0.plan.011) 
		 *   																	(0.plan.020)
		 *   																	(0.plan.030)

   		 *   	- mermas del periodo											(0.mer.010) 
   		 *   	- mermas del periodo											(0.mer.010) 
		 *   																	(0.mer.020)
		 *   																	(0.mer.030)
		 *   

		 *   	- produccion por formato del periodo							(0.prod.0102MediaPaleta) 
		 *   																	(0.prod.0102CajaHorizontal)
		 *   																	(0.prod.0102Troncoconica)
		 *   																	(0.prod.0102Resto)
		 *   
		 *      - total horas trabajadas por formato del periodo				(0.horas.0102Rresto)
		 *      																(0.horas.0102CajaHorizontal)
		 *      																(0.horas.0102Troncoconica)

   		 *      - total horas incidencias por formato del periodo				(0.inc.0102)
		 *      																(0.inc.0102Troncoconica)
		 *      																(0.inc.0102CajaHorizontal)
      
   		 *      - total horas incidencias programadas por formato del periodo	(0.pro.0102)
		 *      																(0.pro.0102Troncoconica)
		 *      																(0.pro.0102CajaHorizontal)
		 *      
		 *      - media produccion del periodo por formato						(0.media.0102)
		 *      																(0.media.0102Troncoconica)
		 *      																(0.media.0102CajaHorizontal)
		 *      
		 *      - venta total del periodo										(0.venta.0102)
		 *      																(0.venta.0102Troncoconica)
		 *      																(0.venta.0102CajaHorizontal)
		 *      
		 *      - ventas por formato del periodo para el ejercicio anterior		(1.venta.0102)
		 *      																(1.venta.0102Troncoconica)
		 *      																(1.venta.0102CajaHorizontal)
		 *      
		 *      - venta media por referencia									(0.media.0111)
		 *      
		 *      - stock productos actual										(0.stock.0111)
		 *      
		 *      - stock por formato del periodo									(0.stock.010)
		 *      - stock por formato del periodo									(0.stock.011)
		 *      																(0.stock.020)
		 *      																(0.stock.030)
		 *      
		 */
		ResultSet rsOpcion = null;		
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardBIB mapeo = null;
		ArrayList<MapeoDashboardBIB> vectorDatosProduccion = null;
		ArrayList<MapeoDashboardBIB> vectorDatosPlanificacion = null;
		ArrayList<MapeoDashboardBIB> vectorDatosVenta = null;
		ArrayList<MapeoDashboardBIB> vectorDatosVentaAnterior = null;
		HashMap<String, Double> vectorDatosStock = null;
		HashMap<String, Double> vectorDatosStockPdteServ = null;
		HashMap<String, Double> vectorDatosPlazoPdteServ = null;

		this.ambitoTemporal=r_vista;
		if (ambitoTemporal.contentEquals("Anual"))
		{
			r_semana = "0";
			semana = "0";
		}
		if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
		
		hashValores = new HashMap<String, Double>();

		vectorDatosProduccion = this.recuperarMovimientosProduccion(new Integer(r_ejercicio),r_semana,r_campo);
		vectorDatosPlanificacion = this.recuperarMovimientosPlanificacion(new Integer(r_ejercicio),r_semana, r_campo);
		vectorDatosVenta = this.recuperarMovimientosVentas(new Integer(r_ejercicio),r_campo, r_semana);
		vectorDatosVentaAnterior = this.recuperarMovimientosVentas(new Integer(r_ejercicio)-1,r_campo, r_semana);
		vectorDatosStock = this.recuperarStock(new Integer(r_ejercicio),r_campo);
		vectorDatosStockPdteServ = this.recuperarPdteServ(new Integer(r_ejercicio),r_campo);
		vectorDatosPlazoPdteServ = this.recuperarPlazoPdteServ(new Integer(r_ejercicio),r_campo);
		
		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.stock.0111");
			hashValores.put("0.stock.0111", valor);
			
			valor = vectorDatosStock.get("0.stock.010");
			hashValores.put("0.stock.010", valor);

			valor = vectorDatosStock.get("0.stock.011");
			hashValores.put("0.stock.011", valor);

			valor = vectorDatosStock.get("0.stock.020");
			hashValores.put("0.stock.020", valor);

			valor = vectorDatosStock.get("0.stock.030");
			hashValores.put("0.stock.030", valor);
			
		}
		if (vectorDatosStockPdteServ!=null && !vectorDatosStockPdteServ.isEmpty())
		{
			valor = vectorDatosStockPdteServ.get("0.stockPdteServ.0111");
			hashValores.put("0.stockPdteServ.0111", valor);
			
			valor = vectorDatosStockPdteServ.get("0.stockPdteServ.010");
			hashValores.put("0.stockPdteServ.010", valor);
			
			valor = vectorDatosStockPdteServ.get("0.stockPdteServ.011");
			hashValores.put("0.stockPdteServ.011", valor);
			
			valor = vectorDatosStockPdteServ.get("0.stockPdteServ.020");
			hashValores.put("0.stockPdteServ.020", valor);
			
			valor = vectorDatosStockPdteServ.get("0.stockPdteServ.030");
			hashValores.put("0.stockPdteServ.030", valor);
			
		}

		if (vectorDatosPlazoPdteServ!=null && !vectorDatosPlazoPdteServ.isEmpty())
		{
			valor = vectorDatosPlazoPdteServ.get("0.plazoPdteServ.010");
			hashValores.put("0.plazoPdteServ.010", valor);
			
			valor = vectorDatosPlazoPdteServ.get("0.plazoPdteServ.011");
			hashValores.put("0.plazoPdteServ.011", valor);
			
			valor = vectorDatosPlazoPdteServ.get("0.plazoPdteServ.020");
			hashValores.put("0.plazoPdteServ.020", valor);
			
			valor = vectorDatosPlazoPdteServ.get("0.plazoPdteServ.030");
			hashValores.put("0.plazoPdteServ.030", valor);
			
		}
		if (vectorDatosPlanificacion!=null && !vectorDatosPlanificacion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.0111");
			hashValores.put("0.plan.0111", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.010");
			hashValores.put("0.plan.010", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.011");
			hashValores.put("0.plan.011", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.020");
			hashValores.put("0.plan.020", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosPlanificacion, r_semana, "0.plan.030");
			hashValores.put("0.plan.030", valor);
		}
		
		if (vectorDatosProduccion!=null && !vectorDatosProduccion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion , r_semana, "0.prod.0111");
			hashValores.put("0.prod.0111", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.010");
			hashValores.put("0.prod.010", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.011");
			hashValores.put("0.prod.011", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.020");
			hashValores.put("0.prod.020", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.030");
			hashValores.put("0.prod.030", valor);
		}
		else
		{
			valor = new Double(0);
			hashValores.put("0.prod.0111", valor);
			hashValores.put("0.prod.010", valor);
			hashValores.put("0.prod.011", valor);
			hashValores.put("0.prod.020", valor);
			hashValores.put("0.prod.030", valor);
		}
		
		if (vectorDatosVenta!=null && !vectorDatosVenta.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.0111");
			hashValores.put("0.venta.0111", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.010");
			hashValores.put("0.venta.010", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.011");
			hashValores.put("0.venta.011", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.020");
			hashValores.put("0.venta.020", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.030");
			hashValores.put("0.venta.030", valor);
		}
		
		if (vectorDatosVentaAnterior!=null && !vectorDatosVentaAnterior.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.0102");
			hashValores.put("1.venta.0102", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.010");
			hashValores.put("1.venta.010", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.011");
			hashValores.put("1.venta.011", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.020");
			hashValores.put("1.venta.020", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.030");
			hashValores.put("1.venta.030", valor);
		}
		
		if ((vectorDatosStock==null || vectorDatosStock.isEmpty()) && (vectorDatosVenta==null || vectorDatosVenta.isEmpty()) && (vectorDatosVenta==null && vectorDatosVenta.isEmpty()) && (vectorDatosProduccion==null || !vectorDatosProduccion.isEmpty()))
		{
			Notificaciones.getInstance().mensajeAdvertencia("No se han encontrado registros referidos al periodo indicado.");
		}
		else
		{
			consultaHorasTrabajadasBIBServer ches = new consultaHorasTrabajadasBIBServer(CurrentUser.get());
			
			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
			hashValores.put("0.horas.0111", valor);

			valor = ches.recuperarHorasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.horasReales.0111", valor);

			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.inc.0111", valor);

			valor = ches.recuperarIncidenciasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.incReales.0111", valor);
			
			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.pro.0111", valor);

			valor = ches.recuperarProgramadasReales(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.proReales.0111", valor);

			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana),this.ambitoTemporal);
			hashValores.put("0.mer.0111", valor);
		}
		
		mapeo = new MapeoGeneralDashboardBIB();
		mapeo.setVectorProduccion(vectorDatosProduccion);
		mapeo.setVectorVentas(vectorDatosVenta);
		mapeo.setVectorVentasAnterior(vectorDatosVentaAnterior);
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	

	private Double recuperaraDatoSolicitado(ArrayList<MapeoDashboardBIB> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.plan.0111": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("TT");
				}				
				break;
			}
			case "0.plan.010": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("010");
				}				
				break;
			}
			case "0.plan.011": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("011");
				}				
				break;
			}
			case "0.plan.020": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("020");
				}				
				break;
			}
			case "0.plan.030": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("030");
				}				
				break;
			}
			case "0.prod.0111": // produccion total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==49)
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.010":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("010"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.011":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("011"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.020": //etiquetado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("020"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}

				break;
			}
			case "0.prod.030": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("030"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.0111": // venta total
			case "1.venta.0111":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						valor = valor + mapeo.getTotal();
					}
				}
				break;
			}
			case "0.venta.010": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("010"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.011": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("011"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.020": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("020"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.030": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("030"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private ArrayList<MapeoDashboardBIB> recuperarMovimientosProduccion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardBIB> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
//			sql.append(" e_articu.marca as formato, " );
			sql.append(" e_articu.sub_familia as tipo, " );  // PS o PT
			sql.append(" a_lin_" + digito + ".movimiento as mov, "); //49 o 99
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			*/
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0111%' ");
			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(fecha_documento,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(fecha_documento) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" and a_lin_" + digito + ".movimiento in ('49') ");
			sql.append(" GROUP BY  e_articu.sub_familia,a_lin_" + digito + ".movimiento ");
			sql.append(" order by  e_articu.sub_familia, a_lin_" + digito + ".movimiento ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardBIB>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardBIB mapeo = new MapeoDashboardBIB();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

//	private ArrayList<MapeoDashboardBIBProduccionAnalizada> recuperarMovimientosVentasFamSubFam(Integer r_ejercicio, String r_semana, String r_campo)
//	{
//		ResultSet rsMovimientos = null;
//		ArrayList<MapeoDashboardBIBProduccionAnalizada> vector = null;
//		Connection con = null;
//		Statement cs = null;
//		StringBuffer sql = null;
//		
//		try
//		{
//			String digito = "0";
//			Integer semanaActual = 0;
//			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
//			
//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
//			if (r_ejercicio == ejercicioActual) digito="0";
//			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
//			
//			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
//			
//			sql = new StringBuffer();
//			
//			sql.append(" SELECT a_lin_" + digito + ".familia as Familia, ");
//			sql.append(" e_famili.descripcion as nombreFamilia, ");
//			sql.append(" a_lin_" + digito + ".sub_familia as Subfamilia, ");
//			sql.append(" e_subfam.descripcion as nombreSubfamilia, ");
//			sql.append(" sum(unidades*" + r_campo + ") as uds ");
//			sql.append(" FROM a_lin_" + digito);
//			sql.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
//			sql.append(" inner join e_famili on e_famili.familia = a_lin_" + digito + ".familia ");
//			sql.append(" inner join e_subfam on e_subfam.familia = a_lin_" + digito + ".familia ");
//			sql.append(" and e_subfam.sub_familia = a_lin_" + digito + ".sub_familia ");
//			
//			sql.append(" where e_articu.tipo_articulo = 'PT' " );  // PS o PT
//			sql.append(" and e_articu.articulo like '0102%' ");
//			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') "); //49 o 99
//			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
//			
//			if (this.ambitoTemporal.contentEquals("Semanal"))
//			{
//				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)= " + r_semana);
//			}
//			else if (ambitoTemporal.contentEquals("Mensual"))
//			{
//				sql.append(" and month(a_lin_" + digito + ".fecha_documento)= " + r_semana);
//			}
//			else if (ambitoTemporal.contentEquals("Acumulado") )
//			{
//				sql.append(" and week(a_lin_" + digito + ".fecha_documento,3)<= " + r_semana);
//			}
//			else if (ambitoTemporal.contentEquals("Trimestral"))
//			{
//				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
//				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
//				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
//				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
//			}
//			else if (ambitoTemporal.contentEquals("Semestral"))
//			{
//				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
//				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
//			}
//			sql.append(" group by 1,2,3,4 ") ;
//			sql.append(" order by 1,2,3,4 ") ;
//			
//			con= this.conManager.establecerConexion();			
//			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
//			rsMovimientos = cs.executeQuery(sql.toString());
//			
//			vector = new ArrayList<MapeoDashboardBIBProduccionAnalizada>();
//			
//			while(rsMovimientos.next())
//			{
//				MapeoDashboardBIBProduccionAnalizada mapeo = new MapeoDashboardBIBProduccionAnalizada();
//				mapeo.setFamilia(rsMovimientos.getString("Familia"));
//				mapeo.setNombreFamilia(rsMovimientos.getString("nombreFamilia"));
//				mapeo.setSubfamilia(rsMovimientos.getString("Subfamilia"));
//				mapeo.setNombreSubfamilia(rsMovimientos.getString("nombreSubfamilia"));
//				mapeo.setTotal(rsMovimientos.getDouble("uds"));
//				
//				vector.add(mapeo);
//			}
//		}
//		catch (Exception ex)
//		{
//			serNotif.mensajeError(ex.getMessage());			
//		}
//		return vector;
//	}

	private ArrayList<MapeoDashboardBIB> recuperarMovimientosPlanificacion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardBIB> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double total = new Double(0);
		int desde = 1;
		int hasta = 53;
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT ejercicio as ejercicio, ");
			sql.append(" prd_programacion.tipoOperacion as ope, " );  // PS o PT
			sql.append(" e_articu.sub_familia as tipo, " );  // PS o PT
			sql.append(" SUM( unidades*" + r_campo + " ) as total ");

			sql.append(" FROM prd_programacion_envasadora as prd_programacion ");
			sql.append(" inner join e_articu on e_articu.articulo COLLATE latin1_spanish_ci = prd_programacion.articulo COLLATE latin1_spanish_ci and e_articu.articulo like '0111%' ");
			sql.append(" where ejercicio = " + r_ejercicio);
			if (this.ambitoTemporal.contentEquals("Acumulado")) sql.append(" and semana <= '" + r_semana + "' ");
			else if (this.ambitoTemporal.contentEquals("Mensual"))
			{
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				sql.append(" and semana between " + desde + " and " + hasta);
			}
			else if (!r_semana.contentEquals("0")) sql.append(" and semana = '" + r_semana + "' ");
			sql.append(" GROUP BY  ejercicio, prd_programacion.tipoOperacion, e_articu.sub_familia ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardBIB>();
			HashMap<String, Double> hash = new HashMap<String, Double>();
			MapeoDashboardBIB mapeo = new MapeoDashboardBIB();
			
			hash.put("010", new Double(0));
			hash.put("011", new Double(0));
			hash.put("020", new Double(0));
			hash.put("030", new Double(0));

			while(rsMovimientos.next())
			{
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				
				switch (rsMovimientos.getString("ope"))
				{
					case "EMBOTELLADO":
					{
						if (rsMovimientos.getString("tipo").startsWith("010")) 
						{
							hash.put("010", rsMovimientos.getDouble("total")); 							
						}
						else if (rsMovimientos.getString("tipo").startsWith("011")) 
						{
							hash.put("011", rsMovimientos.getDouble("total")); 							
						}
						else if (rsMovimientos.getString("tipo").startsWith("020")) 
						{
							hash.put("020", rsMovimientos.getDouble("total")); 							
						}
						else if (rsMovimientos.getString("tipo").startsWith("030")) 
						{
							hash.put("030", rsMovimientos.getDouble("total")); 							
						}
						total=total+rsMovimientos.getDouble("total");
						break;
					}
				}
			}
			
			hash.put("TT", total);
			mapeo.setHashValores(hash);
			vector.add(mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private ArrayList<MapeoDashboardBIB> recuperarMovimientosVentas(Integer r_ejercicio,String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardBIB> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" e_articu.sub_familia as tipo, " );  // PS o PT
			if (ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = 'PT' ");
			sql.append(" and e_articu.articulo like '011%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			if (this.ambitoTemporal.contentEquals("Acumulado"))
			{
				sql.append(" and week(fecha_documento,3) <= " + r_semana);
			}
			sql.append(" GROUP BY e_articu.sub_familia ");
			sql.append(" order BY e_articu.sub_familia ");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardBIB>();
			
			
			while(rsMovimientos.next())
			{
				MapeoDashboardBIB mapeo = new MapeoDashboardBIB();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				
				int tope = 0;
	
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado"))
				{
					tope = -1;
				}
			
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public HashMap<String, Double>  recuperarExistenciasMes(Integer r_ejercicio,String r_tipo)
	{
		HashMap<String, Double> hash = null;
		hash = new HashMap<String, Double>();
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		hash=cis.recuperarExistenciasMes(r_ejercicio, r_tipo, "'1','2','4','6','10','11','12','20','30'");
		return hash;
	}
	
	private HashMap<String, Double>  recuperarStock(Integer r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double valorTotal = null;
		Double ValorFormatoTinto = null;
		Double ValorFormatoTintoU = null;
		Double ValorFormatoBlanco = null;
		Double ValorFormatoRosado = null;
		Integer formatoOld = null;
		
		try
		{
			
			valorTotal = new Double(0);
			ValorFormatoTinto = new Double(0);
			ValorFormatoTintoU= new Double(0);
			ValorFormatoBlanco= new Double(0);
			ValorFormatoRosado= new Double(0);
			formatoOld = new Integer(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.sub_familia as tipo, " );
			sql.append(" SUM( existencias*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0111%' ");
			sql.append(" and a_exis_" + digito + ".almacen in " + this.almacenes );
			sql.append(" GROUP BY e_articu.sub_familia");
			sql.append(" order by e_articu.sub_familia desc ");
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				
				valorTotal = valorTotal + rsMovimientos.getDouble("Total");
				
				if (rsMovimientos.getString("tipo").contentEquals("010"))
				{
					ValorFormatoTinto = ValorFormatoTinto + rsMovimientos.getDouble("Total");
					
				}
				else if (rsMovimientos.getString("tipo").contentEquals("011"))
				{
					ValorFormatoTintoU = ValorFormatoTintoU + rsMovimientos.getDouble("Total");
				}
				else if (rsMovimientos.getString("tipo").contentEquals("020"))
				{
					ValorFormatoRosado = ValorFormatoRosado + rsMovimientos.getDouble("Total");
				}
				else if (rsMovimientos.getString("tipo").contentEquals("030"))
				{
					ValorFormatoBlanco = ValorFormatoBlanco + rsMovimientos.getDouble("Total");
				}
			}
				
			hash.put("0.stock.010", ValorFormatoTinto);
			hash.put("0.stock.011", ValorFormatoTintoU);
			hash.put("0.stock.020", ValorFormatoRosado);
			hash.put("0.stock.030", ValorFormatoBlanco);
			hash.put("0.stock.0111", valorTotal);
				
//				for (int i = 1; i<=53;i++)
//				{
//					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
//				}
//				
//				mapeo.setHashValores(hash);
//				mapeo.setTotal(rsMovimientos.getDouble("Total"));
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}

	private HashMap<String, Double>  recuperarPdteServ(Integer r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double valorTotal = null;
		Double ValorFormatoTinto = null;
		Double ValorFormatoTintoU = null;
		Double ValorFormatoBlanco = null;
		Double ValorFormatoRosado = null;
		Integer formatoOld = null;
		
		try
		{
			
			valorTotal = new Double(0);
			ValorFormatoTinto = new Double(0);
			ValorFormatoTintoU= new Double(0);
			ValorFormatoBlanco= new Double(0);
			ValorFormatoRosado= new Double(0);
			formatoOld = new Integer(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.sub_familia as tipo, " );
			sql.append(" SUM( pdte_servir *" + r_campo + " ) AS Total ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0111%' ");
			sql.append(" and a_exis_" + digito + ".almacen in "  + this.almacenes);
			sql.append(" GROUP BY e_articu.sub_familia");
			sql.append(" order by e_articu.sub_familia desc ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				
				valorTotal = valorTotal + rsMovimientos.getDouble("Total");
				
				if (rsMovimientos.getString("tipo").contentEquals("010"))
				{
					ValorFormatoTinto = ValorFormatoTinto + rsMovimientos.getDouble("Total");
					
				}
				else if (rsMovimientos.getString("tipo").contentEquals("011"))
				{
					ValorFormatoTintoU = ValorFormatoTintoU + rsMovimientos.getDouble("Total");
				}
				else if (rsMovimientos.getString("tipo").contentEquals("020"))
				{
					ValorFormatoRosado = ValorFormatoRosado + rsMovimientos.getDouble("Total");
				}
				else if (rsMovimientos.getString("tipo").contentEquals("030"))
				{
					ValorFormatoBlanco = ValorFormatoBlanco + rsMovimientos.getDouble("Total");
				}
			}
			
			hash.put("0.stockPdteServ.010", ValorFormatoTinto);
			hash.put("0.stockPdteServ.011", ValorFormatoTintoU);
			hash.put("0.stockPdteServ.020", ValorFormatoRosado);
			hash.put("0.stockPdteServ.030", ValorFormatoBlanco);
			hash.put("0.stockPdteServ.0111", valorTotal);
			
//				for (int i = 1; i<=53;i++)
//				{
//					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
//				}
//				
//				mapeo.setHashValores(hash);
//				mapeo.setTotal(rsMovimientos.getDouble("Total"));
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}

	private HashMap<String, Double>  recuperarPlazoPdteServ(Integer r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double valorTotal = null;
		Double ValorFormatoTinto = null;
		Double ValorFormatoTintoU = null;
		Double ValorFormatoBlanco = null;
		Double ValorFormatoRosado = null;
		Integer formatoOld = null;
		
		try
		{
			
			valorTotal = new Double(0);
			ValorFormatoTinto = new Double(0);
			ValorFormatoTintoU= new Double(0);
			ValorFormatoBlanco= new Double(0);
			ValorFormatoRosado= new Double(0);
			formatoOld = new Integer(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.sub_familia as tipo, " );
			sql.append(" max(plazo_entrega) AS plazo ");
			sql.append(" FROM a_vp_l_" + digito + ", e_articu ");
			sql.append(" where a_vp_l_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo matches '0111*' ");
			sql.append(" and a_vp_l_" + digito + ".almacen in " + this.almacenes);
			sql.append(" GROUP BY e_articu.sub_familia");
			sql.append(" order by e_articu.sub_familia desc ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				
				if (rsMovimientos.getString("tipo").contentEquals("010"))
				{
					ValorFormatoTinto = new Double(RutinasFechas.obtenerSemanaFecha(rsMovimientos.getDate("plazo")));
				}
				else if (rsMovimientos.getString("tipo").contentEquals("011"))
				{
					ValorFormatoTintoU = new Double(RutinasFechas.obtenerSemanaFecha(rsMovimientos.getDate("plazo")));
				}
				else if (rsMovimientos.getString("tipo").contentEquals("020"))
				{
					ValorFormatoRosado = new Double(RutinasFechas.obtenerSemanaFecha(rsMovimientos.getDate("plazo")));
				}
				else if (rsMovimientos.getString("tipo").contentEquals("030"))
				{
					ValorFormatoBlanco = new Double(RutinasFechas.obtenerSemanaFecha(rsMovimientos.getDate("plazo")));
				}
			}
			
			hash.put("0.plazoPdteServ.010", ValorFormatoTinto);
			hash.put("0.plazoPdteServ.011", ValorFormatoTintoU);
			hash.put("0.plazoPdteServ.020", ValorFormatoRosado);
			hash.put("0.plazoPdteServ.030", ValorFormatoBlanco);
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarMovimientosVentasEjercicio(Integer r_ejercicio, String r_mascara,String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
			 
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				Integer hastaSemana = 0;
				
				if (digito.contentEquals("0"))
				{
					if (r_semana ==null) 
						hastaSemana = new Integer(RutinasFechas.semanaActual(String.valueOf(r_ejercicio))); 
					else 
						hastaSemana=new Integer(r_semana);
				}
				else hastaSemana = 53;

				for (int i = 1; i<=hastaSemana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	

	
	
	public HashMap<String,	Double> recuperarVentasComparativaEjercicios(Integer r_ejercicio, String r_mascara, int r_cuantos, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			hash = new HashMap<String, Double>();
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			for (int i = 0; i<r_cuantos; i ++)
			{
				if (r_ejercicio - i == ejercicioActual) digito="0";
				if (r_ejercicio - i < ejercicioActual) digito=String.valueOf(r_ejercicio-i);
			
				sql = new StringBuffer();
			
				sql.append(" SELECT year(fecha_documento) as ejercicio, ");
				sql.append(" SUM( unidades*" + r_campo + " ) AS 'total' ");
			  
				sql.append(" FROM a_lin_" + digito + ", e_articu ");
				sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
				sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
				sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
				sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
				sql.append(" GROUP BY ejercicio ");

				
				if (i==0) con= this.conManager.establecerConexion();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rsMovimientos = cs.executeQuery(sql.toString());
			
			
				while(rsMovimientos.next())
				{
					
					hash.put(String.valueOf(r_ejercicio-i), rsMovimientos.getDouble("total"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarVentasArticuloEjercicio(Integer r_ejercicio, String r_mascara, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			int semana = 53;
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual)
			{
				digito="0";
				semana = new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			}
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a_lin_" + digito + ".articulo as articulo, ");
			sql.append(" SUM( unidades*" + r_campo + "/ " + semana + ") AS 'total' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY e_articu.articulo");

				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				
				hash.put(rsMovimientos.getString("articulo").trim(), rsMovimientos.getDouble("total"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarStockArticulo(String r_mascara, String r_campo)
	{
		HashMap<String, Double> hash =null;
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		hash = cis.recuperarStockArticulo(r_mascara, r_campo, this.almacenes);
		return hash;
	}

	public HashMap<String,	Double> recuperarVentasAcumulados(Integer r_ejercicio,String  r_mascara, String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		CallableStatement statement = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			
			String digito = "0";
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			Integer hastaSemana = 0;
//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			
			if ((r_semana==null || r_semana.contentEquals("0")) && digito.contentEquals("0"))
			{
				sql = " "; 
				hastaSemana=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString()));
			}
			else if (this.ambitoTemporal.contentEquals("Acumulado"))
			{
				hastaSemana=new Integer(r_semana);
				sql= " and week(fecha_documento,3)<= '" + hastaSemana + "' ";
			}
			else
			{
				hastaSemana=53;
				sql= " and week(fecha_documento,3)<= '" + hastaSemana + "' ";
			}
			
			con= this.conManager.establecerConexion();			
			
			
			statement = con.prepareCall("{call ventaAcumuladaBotella(?,?,?,?)}");

			statement.setString(1, r_campo);
			statement.setString(2, digito);
			statement.setString(3, r_mascara);
			statement.setString(4, sql);
			boolean hadResults = statement.execute();

			while (hadResults) 
			{
				rsMovimientos = statement.getResultSet();
				hash = new HashMap<String, Double>(); 
				// process result set
				while(rsMovimientos.next())
				{
					
					for (int i = 1; i<=hastaSemana;i++)
					{
						hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
					}
				}


				hadResults = statement.getMoreResults();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}

	public HashMap<String, Double> recuperarProductividad(Integer r_ejercicio)
	{
		HashMap<String, Double> productividadObtenida = null;
		HashMap<String, Double> hashHoras = null;
		HashMap<String, Double> hashIncidencias = null;
		HashMap<String, Double> hashProgramadas= null;
    	Double velocidadNominal = null;
    	Double productividad = null;
    	Double tiempoOperativo = null;
    	Double horas = null;
    	Double incidencias = null;
    	Double programadas= null;
    	String semana = null;
    	int desde = 1;
    	int hasta = 53;
    	
    	consultaHorasTrabajadasEmbotelladoraServer ches = consultaHorasTrabajadasEmbotelladoraServer.getInstance(CurrentUser.get());
    	hashHoras = ches.recuperarHorasRealesAño(r_ejercicio);
    	hashIncidencias = ches.recuperarIncidenciasRealesAño(r_ejercicio);
    	hashProgramadas = ches.recuperarProgramadasRealesAño(r_ejercicio);
    	
		productividadObtenida = new HashMap<String, Double>();
		
		if (this.ambitoTemporal.contentEquals("Semanal") || this.ambitoTemporal.contentEquals("Acumulado")||this.ambitoTemporal.contentEquals("Anual"))
		{
			desde=1;
			hasta=53;
		}
		else if (this.ambitoTemporal.contentEquals("Menusal"))
		{
			try{
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(r_ejercicio.intValue(), Integer.valueOf(semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + semana + "/" + r_ejercicio));
			}
			catch (Exception ex)
			{
				
			}
		}
		
		for (int i = desde; i<=hasta;i++)
		{
			semana = String.valueOf(i);

			if (hashHoras.get(semana)!=null)
				horas = hashHoras.get(semana);
			else
				horas=new Double(0);
			if (hashIncidencias.get(semana)!=null)
				incidencias = hashIncidencias.get(semana);
			else
				incidencias=new Double(0);
			
			if (hashProgramadas.get(semana)!=null)
				programadas = hashProgramadas.get(semana);
			else
				programadas=new Double(0);
			
			tiempoOperativo = horas - incidencias - programadas;

			if (tiempoOperativo!=null && tiempoOperativo!=0)
			{
				velocidadNominal = this.obtenerTiempoDeCiclo(r_ejercicio.toString(), semana.toString(), "Semanal", false);
				productividad = (velocidadNominal / tiempoOperativo)*100;
				productividadObtenida.put(semana.toString(), productividad);
			}
			else
				productividadObtenida.put(semana.toString(), new Double(0));
		}
		return productividadObtenida;
	}

	public HashMap<String, Double> recuperarProductividadMedia(HashMap<String, Double> r_hash)
	{
		HashMap<String, Double> productividadObtenida = null;
		String semana=null;
		Integer cuantos = 0;
		Double prod = new Double(0);
		Double media = null;
		productividadObtenida = new HashMap<String, Double>();
		int desde = 1;
		int hasta = 1;
		
		if (this.ambitoTemporal.contentEquals("Semanal") || this.ambitoTemporal.contentEquals("Acumulado")||this.ambitoTemporal.contentEquals("Anual"))
		{
			desde=1;
			hasta=53;
		}
		else if (this.ambitoTemporal.contentEquals("Menusal"))
		{
			try
			{
				
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + semana + "/" + ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(ejercicio).intValue(), Integer.valueOf(semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + semana + "/" + ejercicio));

			}
			catch (Exception ex)
			{
				
			}
		}
		
		for (int i = desde; i<=r_hash.size();i++)
		{
			semana = String.valueOf(i);
			
			if (r_hash.get(semana)!=null && r_hash.get(semana)!=0)
			{
				cuantos++;
				prod = prod + r_hash.get(semana); 
			}
		}
		media = prod / cuantos;
		
		for (int i = desde; i<=hasta;i++)
		{
			productividadObtenida.put(String.valueOf(i), media);
		}
		return productividadObtenida;
	}
	
	public Double obtenerTiempoDeCiclo(String r_ejercicio,String r_semana,String r_vista, boolean r_prevision)
	{
		Double velocidadNominal = new Double(0);
		
		HashMap<Integer, String> hashArticulos = null;
		HashMap<String, Double> hashVelocidades = null;
		HashMap<String, Double> hashProducciones = null;
		
		consultaProgramacionEnvasadoraServer cps = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
		consultaProduccionDiariaServer cpds = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
		
		hashVelocidades = cps.obtenerHashVelocidades(r_ejercicio, r_semana, r_vista, r_prevision);
		
		if  (!r_prevision)
			hashProducciones = cpds.obtenerHashProducciones(this.area, r_ejercicio, r_semana, r_vista);
		else
			hashProducciones = cps.obtenerHashProducciones(r_ejercicio, r_semana, r_vista);
		
		if  (!r_prevision)
			hashArticulos = cpds.obtenerHashArticulos(this.area,r_ejercicio, r_semana, r_vista);
		else
			hashArticulos = cps.obtenerHashArticulos(r_ejercicio, r_semana, r_vista);
		
		for (int i = 0 ; i< hashArticulos.size(); i++)
		{
			if (hashVelocidades.get(hashArticulos.get(i))!=null && hashProducciones.get(hashArticulos.get(i))!=null)
			{
				System.out.println("Articulo: " + hashArticulos.get(i));
				System.out.println("Velocidad: " + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10)).toString()));
				System.out.println("Produccion: " + hashProducciones.get(hashArticulos.get(i)).toString());
				System.out.println("ciclo: " + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10))*hashProducciones.get(hashArticulos.get(i))));
				velocidadNominal = velocidadNominal + (RutinasNumericas.formatearDoubleDecimalesDouble(hashVelocidades.get(hashArticulos.get(i)),new Integer(10))*hashProducciones.get(hashArticulos.get(i)));
				System.out.println("ciclo acumulado: " + velocidadNominal.toString());
			}
		}
		
		return velocidadNominal;
	}

	public ArrayList<MapeoDetalleArticulo>  recuperarDetalleArticulo(String r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDetalleArticulo> vector = null;
		MapeoDetalleArticulo mapeo = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (new Integer(r_ejercicio) == ejercicioActual) digito="0";
			if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(ejercicioActual-new Integer(r_ejercicio));
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" e_articu.sub_familia as tipo, " );
			sql.append(" e_articu.articulo as articulo, " );
			sql.append(" a_exis_"+digito+ ".almacen as almacen, " );
			sql.append(" SUM( existencias *" + r_campo + " ) AS stock, ");
			sql.append(" SUM( pdte_servir *" + r_campo + " ) AS pdte ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0111%' ");
			sql.append(" and a_exis_" + digito + ".almacen in " + this.almacenes);
			sql.append(" GROUP BY e_articu.sub_familia, e_articu.articulo, a_exis_"+digito + ".almacen");
			sql.append(" having SUM( existencias *" + r_campo + " ) <>0 ");
//			sql.append(" and SUM( pdte_servir *" + r_campo + " ) <> 0 ");
			
			sql.append(" order by 1,2,3 ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			vector = new ArrayList<MapeoDetalleArticulo>();
			
			while(rsMovimientos.next())
			{
				mapeo = new MapeoDetalleArticulo();
				
				mapeo.setArticulo(rsMovimientos.getString("articulo").trim());
				mapeo.setDescripcion(cas.obtenerDescripcionArticulo(rsMovimientos.getString("articulo").trim()));
				mapeo.setAlmacen(rsMovimientos.getInt("almacen"));
				mapeo.setStock(rsMovimientos.getDouble("stock"));
				mapeo.setPdte(rsMovimientos.getDouble("pdte"));
				
				vector.add(mapeo);
			}				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}	

	public ArrayList<MapeoDetalleArticulo>  recuperarDetallePendienteArticulo(String r_ejercicio,String r_campo, String r_articulo, String r_almacen, Double r_stock)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDetalleArticulo> vector = null;
		MapeoDetalleArticulo mapeo = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double stock = null;
		HashMap<Integer, Double> hashSemanas=null;
		HashMap<Integer, Double> hashUnidades=null;
		int semanaVenta = 0;
		int semanaProgramacion = 0;
		int contadorProgramaciones = 0;
		
		try
		{
			
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			consultaProgramacionEnvasadoraServer cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			HashMap<Integer, HashMap<Integer, Double>> programaciones = cpes.obtenerHashProgramacion(r_ejercicio, r_articulo, r_campo);
			if (programaciones!=null && programaciones.size()>0)
			{
				hashSemanas = programaciones.get(0);
				hashUnidades = programaciones.get(1);
			}
			
			if (new Integer(r_ejercicio) == ejercicioActual) digito="0";
			if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(ejercicioActual-new Integer(r_ejercicio));
			if (r_campo.contentEquals("Litros")) r_campo = "e_articu.marca"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT e_articu.sub_familia as tipo, " );
			sql.append(" e_articu.articulo as artic, " );
			sql.append(" a_vp_l_"+digito+ ".almacen as alm, " );
			sql.append(" a_vp_l_"+digito+ ".plazo_entrega as plazo, " );
			sql.append(" SUM( (a_vp_l_" + digito + ".unidades - a_vp_l_" + digito + ".unidades_serv) * " + r_campo + " ) as pdte ");
			sql.append(" FROM a_vp_l_" + digito + ", e_articu ");
			sql.append(" where a_vp_l_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			sql.append(" and a_vp_l_" + digito + ".almacen = " + r_almacen);
			sql.append(" and a_vp_l_" + digito + ".cumplimentacion is null ");
			sql.append(" GROUP BY 1,2,3,4 ");
			sql.append(" having SUM( (a_vp_l_" + digito + ".unidades - a_vp_l_" + digito + ".unidades_serv) * " + r_campo + " ) <> 0 ");
			sql.append(" order by 1,2,3,4 ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			vector = new ArrayList<MapeoDetalleArticulo>();
			stock=r_stock;
			
			while(rsMovimientos.next())
			{
				mapeo = new MapeoDetalleArticulo();
				mapeo.setArticulo(rsMovimientos.getString("artic").trim());	
				mapeo.setDescripcion(cas.obtenerDescripcionArticulo(rsMovimientos.getString("artic").trim()));
				mapeo.setAlmacen(rsMovimientos.getInt("alm"));
				semanaVenta = RutinasFechas.obtenerSemanaFecha(rsMovimientos.getDate("plazo"));
				
				if (hashSemanas!=null && hashSemanas.size()>0)
				{
					for (contadorProgramaciones=0;contadorProgramaciones<hashSemanas.size();contadorProgramaciones++)
					{
						semanaProgramacion = hashSemanas.get(contadorProgramaciones).intValue();
						
						if (semanaProgramacion <= semanaVenta)
						{
							mapeo.setDescripcion("PROGRAMADOS");
							mapeo.setSemana(semanaProgramacion);
							
							mapeo.setPdte(hashUnidades.get(contadorProgramaciones));
							stock=stock + hashUnidades.get(contadorProgramaciones);
							mapeo.setSobraFalta(stock);
							vector.add(mapeo);
							mapeo = new MapeoDetalleArticulo();
							mapeo.setArticulo(rsMovimientos.getString("artic").trim());				
							mapeo.setAlmacen(rsMovimientos.getInt("alm"));
							hashSemanas.remove(contadorProgramaciones);
							hashUnidades.remove(contadorProgramaciones);
						}
						else if (semanaProgramacion == semanaVenta)
						{
							
						}
					}
					stock=stock - rsMovimientos.getDouble("pdte");
					mapeo.setDescripcion(cas.obtenerDescripcionArticulo(rsMovimientos.getString("artic").trim()));
					mapeo.setSemana(semanaVenta);				
					mapeo.setPdte(rsMovimientos.getDouble("pdte"));
					mapeo.setSobraFalta(stock);						
				}
				else
				{
				
					stock=stock - rsMovimientos.getDouble("pdte");
					mapeo.setSemana(semanaVenta);				
					mapeo.setPdte(rsMovimientos.getDouble("pdte"));
					mapeo.setSobraFalta(stock);
				}
				vector.add(mapeo);
			}	
			if (hashSemanas!=null && hashSemanas.size()>0)
			{
				for (contadorProgramaciones=0;contadorProgramaciones<hashSemanas.size();contadorProgramaciones++)
				{
					semanaProgramacion = hashSemanas.get(contadorProgramaciones).intValue();
					mapeo = new MapeoDetalleArticulo();
					mapeo.setArticulo(r_articulo.trim());	
					mapeo.setDescripcion("PROGRAMADOS");
					mapeo.setSemana(semanaProgramacion);
					mapeo.setAlmacen(1);
					
					mapeo.setPdte(hashUnidades.get(contadorProgramaciones));
					stock=stock + hashUnidades.get(contadorProgramaciones);
					mapeo.setSobraFalta(stock);
					vector.add(mapeo);
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}	
	
}
