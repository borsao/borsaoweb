package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardArticulo extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardArticulo> vectorProduccion = null;
	private ArrayList<MapeoDashboardArticulo> vectorVentas= null;
	private ArrayList<MapeoDashboardArticulo> vectorVentasAnterior= null;
	
	public MapeoGeneralDashboardArticulo()
	{
	}
 
	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardArticulo> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardArticulo> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardArticulo> getVectorVentas() {
		return vectorVentas;
	}

	public void setVectorVentas(ArrayList<MapeoDashboardArticulo> vectorVentas) {
		this.vectorVentas = vectorVentas;
	}

	public ArrayList<MapeoDashboardArticulo> getVectorVentasAnterior() {
		return vectorVentasAnterior;
	}

	public void setVectorVentasAnterior(ArrayList<MapeoDashboardArticulo> vectorVentasAnterior) {
		this.vectorVentasAnterior = vectorVentasAnterior;
	}

	
}