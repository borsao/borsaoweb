package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEnvasadora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.server.consultaHorasTrabajadasEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardEnvasadoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaDashboardEnvasadoraServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivo5L = 65000;
	public static Integer produccionObjetivo2L = 45000;
	public static Integer produccionObjetivo5G = 13000;
	public static Integer produccionObjetivo2G = 22500;
	private String vistaDatos = null;
	
	public consultaDashboardEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardEnvasadora recuperarValoresEstadisticos(String r_ejercicio, String r_semana, String r_vista,String r_campo)
	{
		Double valor = null;
		vistaDatos=r_vista;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 *  
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod.0103)
		 *  
		 *  	- mermas total del periodo										(0.mer.0103)
		 *   
		 *   	- produccion por formato del periodo							(0.prod.01030) 
		 *   																	(0.prod.01031)

   		 *   	- mermas por formato del periodo								(0.mer.01030) 
		 *   																	(0.mer.01031)

		 *   	- produccion por formato y color del periodo					(0.prod.010300) 
		 *   																	(0.prod.010301)
		 *   																	(0.prod.010310)
		 *   																	(0.prod.010311)
		 *   
		 *      - total horas trabajadas por formato del periodo				(0.horas.01030)
		 *      																(0.horas.01031)

   		 *      - total horas incidencias por formato del periodo				(0.inc.01030)
		 *      																(0.inc.01031)
   		 *      - total horas incidencias programadas por formato del periodo	(0.pro.01030)
		 *      																(0.pro.01031)
   		 *      - total produccion prevista por formato del periodo				(0.pre.01030)
		 *      																(0.pre.01031)

		 *      - media produccion del periodo por formato						(0.media.01030)
		 *      																(0.media.01031)
		 *      
		 *      - venta total del periodo										(0.venta.0103)
		 *      
		 *      - ventas por formato del periodo								(0.venta.01030)
		 *      																(0.venta.01031)
		 *      
		 *      - ventas por formato del periodo para el ejercicio anterior		(1.venta.01030)
		 *      																(1.venta.01031)
		 *      
		 *      - venta media por referencia									(0.media.0103)
		 *      
		 *      - stock productos actual										(0.stock.0103)
		 *      
		 *      - stock por formato del periodo									(0.stock.01030)
		 *      																(0.stock.01031)
		 *      
		 *      - stock por formato y color del periodo 						(0.stock.010300)
		 *      																(0.stock.010301)
		 *      																(0.stock.010310)
		 *      																(0.stock.010311)
		 */
		ResultSet rsOpcion = null;		
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardEnvasadora mapeo = null;
		ArrayList<MapeoDashboardEnvasadora> vectorDatosProduccion = null;
		ArrayList<MapeoDashboardEnvasadora> vectorDatosVenta = null;
		ArrayList<MapeoDashboardEnvasadora> vectorDatosVentaAnterior = null;
		HashMap<String, Double> vectorDatosStock = null;

		if (r_vista.contentEquals("Anual")) r_semana = "0";
		if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
		
		hashValores = new HashMap<String, Double>();

		vectorDatosProduccion = this.recuperarMovimientosProduccion(new Integer(r_ejercicio),r_semana,r_campo);
		vectorDatosVenta = this.recuperarMovimientosVentas(new Integer(r_ejercicio),r_campo,r_vista,r_semana);
		vectorDatosVentaAnterior = this.recuperarMovimientosVentas(new Integer(r_ejercicio)-1,r_campo,r_vista,r_semana);
		vectorDatosStock = this.recuperarStock(new Integer(r_ejercicio),r_campo);
		
		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.stock.0103");
			hashValores.put("0.stock.0103", valor);
			
			valor = vectorDatosStock.get("0.stock.01030");
			hashValores.put("0.stock.01030", valor);

			valor = vectorDatosStock.get("0.stock.01031");
			hashValores.put("0.stock.01031", valor);
			
			valor = vectorDatosStock.get("0.stock.010300");
			hashValores.put("0.stock.010300", valor);
			
			valor = vectorDatosStock.get("0.stock.010301");
			hashValores.put("0.stock.010301", valor);
			
			valor = vectorDatosStock.get("0.stock.010310");
			hashValores.put("0.stock.010310", valor);
			
			valor = vectorDatosStock.get("0.stock.010311");
			hashValores.put("0.stock.010311", valor);
			
		}
		
		if (vectorDatosProduccion!=null && !vectorDatosProduccion.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0103");
			hashValores.put("0.prod.0103", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.01030");
			hashValores.put("0.prod.01030", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.01031");
			hashValores.put("0.prod.01031", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.010300");
			hashValores.put("0.prod.010300", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.010301");
			hashValores.put("0.prod.010301", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.010310");
			hashValores.put("0.prod.010310", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.010311");
			hashValores.put("0.prod.010311", valor);
		}
		if (vectorDatosVenta!=null && !vectorDatosVenta.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.0103");
			hashValores.put("0.venta.0103", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.01030");
			hashValores.put("0.venta.01030", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.01031");
			hashValores.put("0.venta.01031", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.010300");
			hashValores.put("0.venta.010300", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.010301");
			hashValores.put("0.venta.010301", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.010310");
			hashValores.put("0.venta.010310", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVenta, r_semana, "0.venta.010311");
			hashValores.put("0.venta.010311", valor);

		}
		if (vectorDatosVentaAnterior!=null && !vectorDatosVentaAnterior.isEmpty())
		{
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.0103");
			hashValores.put("1.venta.0103", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.01030");
			hashValores.put("1.venta.01030", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.01031");
			hashValores.put("1.venta.01031", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.010300");
			hashValores.put("1.venta.010300", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.010301");
			hashValores.put("1.venta.010301", valor);
			
			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.010310");
			hashValores.put("1.venta.010310", valor);

			valor = this.recuperaraDatoSolicitado(vectorDatosVentaAnterior, r_semana, "1.venta.010311");
			hashValores.put("1.venta.010311", valor);


		}
		if ((vectorDatosStock==null || vectorDatosStock.isEmpty()) && (vectorDatosVenta==null || vectorDatosVenta.isEmpty()) && (vectorDatosVenta==null && vectorDatosVenta.isEmpty()) && (vectorDatosProduccion==null || !vectorDatosProduccion.isEmpty()))
		{
			Notificaciones.getInstance().mensajeAdvertencia("No se han encontrado registros referidos al periodo indicado.");
		}
		else
		{
			consultaHorasTrabajadasEnvasadoraServer ches = new consultaHorasTrabajadasEnvasadoraServer(CurrentUser.get());
			
			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), 5, vistaDatos);
			hashValores.put("0.horas.01030", valor);
			
			valor = ches.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), 2, vistaDatos);
			hashValores.put("0.horas.01031", valor);

			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana), 5, vistaDatos);
			hashValores.put("0.inc.01030", valor);
			
			valor = ches.recuperarIncidencias(new Integer(r_ejercicio), new Integer(r_semana), 2, vistaDatos);
			hashValores.put("0.inc.01031", valor);
			
			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana), 5, vistaDatos);
			hashValores.put("0.pro.01030", valor);
			
			valor = ches.recuperarProgramadas(new Integer(r_ejercicio), new Integer(r_semana), 2, vistaDatos);
			hashValores.put("0.pro.01031", valor);

			valor = ches.recuperarProduccion5L(new Integer(r_ejercicio), new Integer(r_semana), 5, vistaDatos);
			hashValores.put("0.pre.010300", valor);
			
			valor = ches.recuperarProduccion2L(new Integer(r_ejercicio), new Integer(r_semana), 2, vistaDatos);
			hashValores.put("0.pre.010310", valor);
			
			valor = ches.recuperarProduccion5LR(new Integer(r_ejercicio), new Integer(r_semana), 5, vistaDatos);
			hashValores.put("0.pre.010301", valor);
			
			valor = ches.recuperarProduccion2LR(new Integer(r_ejercicio), new Integer(r_semana), 2, vistaDatos);
			hashValores.put("0.pre.010311", valor);

			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana), null);
			hashValores.put("0.mer.0103", valor);

			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana), 5);
			hashValores.put("0.mer.01030", valor);

			valor = ches.recuperarMermas(new Integer(r_ejercicio), new Integer(r_semana), 2);
			hashValores.put("0.mer.01031", valor);

		}
		
		mapeo = new MapeoGeneralDashboardEnvasadora();
		mapeo.setVectorProduccion(vectorDatosProduccion);
		mapeo.setVectorVentas(vectorDatosVenta);
		mapeo.setVectorVentasAnterior(vectorDatosVentaAnterior);
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	

	private Double recuperaraDatoSolicitado(ArrayList<MapeoDashboardEnvasadora> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.prod.0103":
			case "0.venta.0103":
			case "1.venta.0103":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					
					if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual")) 
						valor = valor + mapeo.getTotal();					
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
						/*
						 * meses
						 */
						valor = valor + hash.get(r_semana);
				}
				break;
			}
			case "0.venta.01030":
			case "1.venta.01030":
			case "0.prod.01030":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==5)
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.01031":
			case "1.venta.01031":
			case "0.prod.01031":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==2)
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010300":
			case "1.venta.010300":
			case "0.prod.010300":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==5 && color.toUpperCase().contentEquals("TINTO"))
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010301":
			case "1.venta.010301":
			case "0.prod.010301":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==5 && color.toUpperCase().contentEquals("ROSADO"))
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010310":
			case "1.venta.010310":
			case "0.prod.010310":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==2 && color.toUpperCase().contentEquals("TINTO"))
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010311":
			case "1.venta.010311":
			case "0.prod.010311":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==2 && color.toUpperCase().contentEquals("ROSADO"))
					{
						if (vistaDatos.contentEquals("Acumulado") || vistaDatos.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private ArrayList<MapeoDashboardEnvasadora> recuperarMovimientosProduccion(Integer r_ejercicio, String r_semana, String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEnvasadora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		int semana=0;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" e_articu.marca as formato, " );
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			if (!vistaDatos.contentEquals("Acumulado"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			  
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0103%' ");
			
			if (vistaDatos.contentEquals("Acumulado"))
			{
				sql.append(" and week(fecha_documento,3) <= " + r_semana);
			}
			else
				if (r_semana.contentEquals("0") && digito.contentEquals("0")) sql.append(" and week(fecha_documento,3)<= '" + semanaActual + "' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento = '49' ");
			sql.append(" GROUP BY e_articu.marca, e_articu.grosor,  palet ");
			sql.append(" order by e_articu.marca desc, e_articu.grosor, palet desc ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEnvasadora>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEnvasadora mapeo = new MapeoDashboardEnvasadora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("formato"));
				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setPalet(rsMovimientos.getString("palet"));

				if (vistaDatos.contentEquals("Anual"))
					semana = 53;
				else if (vistaDatos.contentEquals("Semanal"))
					semana = new Integer(r_semana);
				
				for (int i = 1; i<=semana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}

				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	private ArrayList<MapeoDashboardEnvasadora> recuperarMovimientosVentas(Integer r_ejercicio,String r_campo, String r_vista, String r_semana)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEnvasadora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		int semana = 0;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" e_articu.marca as formato, " );
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			
			if (!r_vista.contentEquals("Acumulado"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			  
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0103%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			if (r_vista.contentEquals("Acumulado"))
			{
				sql.append(" and week(fecha_documento,3) <= " + r_semana);
			}

			sql.append(" GROUP BY e_articu.marca, e_articu.grosor,  palet ");
			sql.append(" order by e_articu.marca desc, e_articu.grosor, palet desc ");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEnvasadora>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEnvasadora mapeo = new MapeoDashboardEnvasadora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("formato"));
				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setPalet(rsMovimientos.getString("palet"));

				if (r_vista.contentEquals("Anual"))
					semana = 53;
				else 
					if (r_vista.contentEquals("Semanal"))
					semana = new Integer(r_semana);
				
				for (int i = 1; i<=semana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	private HashMap<String, Double>  recuperarStock(Integer r_ejercicio,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double valorTotal = null;
		Double ValorFormato5 = null;
		Double ValorFormato2 = null;
		Integer formatoOld = null;
		
		try
		{
			
			valorTotal = new Double(0);
			ValorFormato5 = new Double(0);
			ValorFormato2 = new Double(0);
			formatoOld = new Integer(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" e_articu.marca as formato, " );
			sql.append(" SUM( existencias*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0103%' ");
			sql.append(" and a_exis_" + digito + ".almacen in (1,4) ");
			sql.append(" GROUP BY e_articu.marca, e_articu.grosor");
			sql.append(" order by e_articu.marca desc, e_articu.grosor desc ");
				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				
				valorTotal = valorTotal + rsMovimientos.getDouble("Total");
				
				if (rsMovimientos.getInt("formato")==5)
				{
					ValorFormato5 = ValorFormato5 + rsMovimientos.getDouble("Total");
					
					if (rsMovimientos.getString("color").contentEquals("ROSADO"))
					{
						hash.put("0.stock.010301", rsMovimientos.getDouble("Total"));
					}
					else if (rsMovimientos.getString("color").contentEquals("TINTO"))
					{
						hash.put("0.stock.010300", rsMovimientos.getDouble("Total"));
					}
				}
				else if (rsMovimientos.getInt("formato")==2)
				{
					ValorFormato2 = ValorFormato2 + rsMovimientos.getDouble("Total");
					
					if (rsMovimientos.getString("color").contentEquals("ROSADO"))
					{
						hash.put("0.stock.010311", rsMovimientos.getDouble("Total"));
					}
					else if (rsMovimientos.getString("color").contentEquals("TINTO"))
					{
						hash.put("0.stock.010310", rsMovimientos.getDouble("Total"));
					}
				}
			}
				
			hash.put("0.stock.01030", ValorFormato5);
			hash.put("0.stock.01031", ValorFormato2);
			hash.put("0.stock.0103", valorTotal);
				
//				for (int i = 1; i<=53;i++)
//				{
//					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
//				}
//				
//				mapeo.setHashValores(hash);
//				mapeo.setTotal(rsMovimientos.getDouble("Total"));
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarMovimientosVentasEjercicio(Integer r_ejercicio, String r_mascara,String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
			 
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				Integer hastaSemana = 0;
				if (digito.contentEquals("0")) hastaSemana = new Integer(r_semana); else hastaSemana = 53;
				
				for (int i = 1; i<=hastaSemana;i++)

				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	

	
	
	public HashMap<String,	Double> recuperarVentasComparativaEjercicios(Integer r_ejercicio, String r_mascara, int r_cuantos, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			hash = new HashMap<String, Double>();
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
			
			for (int i = 0; i<r_cuantos; i ++)
			{
				if (r_ejercicio - i == ejercicioActual) digito="0";
				if (r_ejercicio - i < ejercicioActual) digito=String.valueOf(r_ejercicio-i);
			
				sql = new StringBuffer();
			
				sql.append(" SELECT year(fecha_documento) as ejercicio, ");
				sql.append(" SUM( unidades*" + r_campo + " ) AS 'total' ");
			  
				sql.append(" FROM a_lin_" + digito + ", e_articu ");
				sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
				sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
				sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
				sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
				sql.append(" GROUP BY ejercicio ");

				
				if (i==0) con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				rsMovimientos = cs.executeQuery(sql.toString());
			
			
				while(rsMovimientos.next())
				{
					
					hash.put(String.valueOf(r_ejercicio-i), rsMovimientos.getDouble("total"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarVentasArticuloEjercicio(Integer r_ejercicio, String r_mascara, String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			int semana = 53;
			if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual)
			{
				digito="0";
				semana = new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			}
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a_lin_" + digito + ".articulo as articulo, ");
			sql.append(" SUM( unidades*" + r_campo + "/ " + semana + ") AS 'total' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY e_articu.articulo");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				
				hash.put(rsMovimientos.getString("articulo").trim(), rsMovimientos.getDouble("total"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarStockArticulo(String r_mascara, String r_campo)
	{
		HashMap<String, Double> hash =null;

		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		hash=cis.recuperarStockArticulo(r_mascara, r_campo, "1,4");
		return hash;
	}

	public HashMap<String,	Double> recuperarVentasAcumulados(Integer r_ejercicio,String  r_mascara, String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		CallableStatement statement = null;
		Connection con = null;
		String sql = null;
		
		try
		{
			
			String digito = "0";
			
			if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			Integer semanaActual = 0;
			if (r_semana.contentEquals("0") && digito.contentEquals("0"))
			{
				sql = " "; 
				semanaActual=53;
			}
			else
			{
				sql= " and week(fecha_documento,3)<= '" + r_semana + "' ";
				semanaActual=new Integer(r_semana);
			}

//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			
//			if (r_semana.contentEquals("0") && digito.contentEquals("0")) sql= " and week(fecha_documento,3)<= '" + semanaActual + "' "; else sql = " ";
			
			con= this.conManager.establecerConexionInd();			
			
			
			if (r_mascara.length()==5) statement = con.prepareCall("{call ventaAcumuladaFormato(?,?,?,?)}");
			else if (r_mascara.length()>5) statement = con.prepareCall("{call ventaAcumulada(?,?,?,?)}");
			else statement = con.prepareCall("{call ventaAcumuladaGlobal(?,?,?,?)}");

			statement.setString(1, r_campo);
			statement.setString(2, digito);
			statement.setString(3, r_mascara);
			statement.setString(4, sql);
			
			boolean hadResults = statement.execute();

			while (hadResults) 
			{
				rsMovimientos = statement.getResultSet();
				hash = new HashMap<String, Double>(); 
				// process result set
				while(rsMovimientos.next())
				{
					
					for (int i = 1; i<=semanaActual;i++)
					{
						hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
					}
				}


				hadResults = statement.getMoreResults();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (statement!=null)
				{
					statement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}

	public HashMap<String, Double> recuperarProduccionFormatoSemana(Integer r_ejercicio, String r_semana, String r_campo, String r_formato)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			if (r_campo.contentEquals("Litros")) r_campo = "marca"; else r_campo="1";
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1); else semanaActual=53;
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
			 
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
			  
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
			sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.marca ='" + r_formato + "' " );
			sql.append(" and e_articu.articulo like '0103%' ");
			if (r_semana.contentEquals("0") && digito.contentEquals("0")) sql.append(" and week(fecha_documento,3)<= '" + semanaActual + "' ");
			sql.append(" and a_lin_" + digito + ".movimiento = '49' ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				for (int i = 1; i<=semanaActual;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}

}
