package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.vaadin.addon.JFreeChartWrapper;
import org.vaadin.addons.d3Gauge.GaugeStyle;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.ClasesPropias.graficas.barras;
import borsao.e_borsao.ClasesPropias.graficas.reloj;
import borsao.e_borsao.ClasesPropias.graficas.stackedBarras;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDetalleArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardBIB;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardBIBServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEnvasadoraServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaCalidad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaDisponibilidad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaIncidencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaProductividad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaTiempoDisponible;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view.pantallaLineasProgramacionesEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardBIBView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard BIB";
	private Integer tamañoRelojes = 160;
	private final String titulo = "CUADRO MANDO BIB";
	private final int intervaloRefresco = 0*60*1000;
	private ChatRefreshListener cr = null;
	
	private CssLayout barAndGridLayout = null;    
    private VerticalLayout topLayout = null;
    private HorizontalLayout topLayoutL = null;
    private HorizontalLayout topLayoutS = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;
    
    private Panel panelResumen =null;
    private Panel panelProduccion =null;
    private Panel panelOEE=null;
    private Panel panelStockResumen =null;
    private Panel panelDetalleArticulo =null;
    
    private GridLayout centralTop = null;
    private GridLayout centralTop2 = null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle2 = null;
    private GridLayout centralDetalleArticulo = null;
    
    private GridLayout centralBottom = null;
    
	public ComboBox cmbSemana = null;
	public ComboBox cmbCalculo = null;
	public ComboBox cmbVista = null;
	public TextField txtEjercicio= null;
	public String semana = null;
	private Button procesar=null;

	private Button opcMenos= null;
	private Button opcMas= null;

	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardBIB mapeo=null;
	
	private boolean prevision = false;
	private Double correctorVentasEjercicio = null;
	private Double disponibilidad = null;
	private Double productividad = null;
	private Double calidad = null;
	private CheckBox prev = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardBIBView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.correctorVentasEjercicio = new Double(1);
    	
    	this.cargarPantalla();
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
		
		this.cargarListeners();
//		this.ejecutarTimer();
//		this.procesar();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
//    	setSpacing(false);
    	
    	
    	this.barAndGridLayout = new CssLayout();
//        this.barAndGridLayout.setSpacing(false);
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

//        final Navigator navigator = new Navigator(eBorsao.getCurrent(), this.barAndGridLayout);

		    	this.topLayoutL = new HorizontalLayout();
//		    	this.topLayoutL.addStyleName("v-panelTitulo");
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.cmbVista= new ComboBox("Vista");    	
			    	this.cmbVista.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbVista.setNewItemsAllowed(false);
			    	this.cmbVista.setWidth("150px");
			    	this.cmbVista.setNullSelectionAllowed(false);
			    	this.cmbVista.addItem("Anual");
			    	this.cmbVista.addItem("Mensual");
			    	this.cmbVista.addItem("Acumulado");
//			    	this.cmbVista.addItem("Ultimos 6 Meses");
			    	this.cmbVista.addItem("Semanal");
			    	this.cmbVista.setValue("Semanal");

			    	this.cmbCalculo= new ComboBox("Cálculo");    	
			    	this.cmbCalculo.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbCalculo.setNewItemsAllowed(false);
			    	this.cmbCalculo.setWidth("150px");
			    	this.cmbCalculo.setNullSelectionAllowed(false);
			    	this.cmbCalculo.addItem("Litros");
//			    	this.cmbCalculo.addItem("Ultimos 6 Meses");
			    	this.cmbCalculo.addItem("Unidades");
			    	this.cmbCalculo.setValue("Litros");

			    	this.txtEjercicio=new TextField("Ejercicio");
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
//		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.cmbSemana= new ComboBox("Semana");    	
		    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		    		this.cmbSemana.setNewItemsAllowed(false);
		    		this.cmbSemana.setWidth("125px");
		    		this.cmbSemana.setNullSelectionAllowed(false);

		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		    		
		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);
		        	
			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
			    	this.prev = new CheckBox("Prevision?");
			    	this.prev.addStyleName(ValoTheme.CHECKBOX_SMALL);
			    	
		    	if (LecturaProperties.semaforos)
		    	{
//		    		this.topLayoutL.addComponent(this.opcRefresh);
//		    		this.topLayoutL.setComponentAlignment(this.opcRefresh,  Alignment.BOTTOM_LEFT);
		    	}
		    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.cmbVista);
		    	this.topLayoutL.addComponent(this.cmbCalculo);
		    	this.topLayoutL.addComponent(this.txtEjercicio);
	    		this.topLayoutL.addComponent(this.opcMenos);
	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.cmbSemana);
	    		this.topLayoutL.addComponent(this.opcMas);
	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);

		    		this.topLayoutL.addComponent(this.prev);
		    		this.topLayoutL.setComponentAlignment(this.prev,  Alignment.BOTTOM_LEFT);

		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
//		    	this.topLayoutL.setStyleName("miPanel");
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

//	    	this.topLayoutL.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

	    	panelDetalleArticulo = new Panel("Detalle Articulo");
	    	panelDetalleArticulo.setHeightUndefined();
	    	panelDetalleArticulo.setWidth("100%");
	    	panelDetalleArticulo.addStyleName("showpointer");
	    	new PanelCaptionBarToggler<Panel>( panelDetalleArticulo );
		    	
		    	this.centralDetalleArticulo = new GridLayout(2,1);
		    	this.centralDetalleArticulo.setWidth("100%");
		    	this.centralDetalleArticulo.setMargin(true);
		    	this.centralDetalleArticulo.setSpacing(true);
		    	
	    	panelDetalleArticulo.setContent(centralDetalleArticulo);
	    	panelDetalleArticulo.getContent().setVisible(false);
	    	panelDetalleArticulo.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
	    	
	    	panelResumen = new Panel("Resumen");
		    panelResumen.setHeightUndefined();
		    panelResumen.setWidth("100%");
		    panelResumen.addStyleName("showpointer");

		    	this.centralTop = new GridLayout(6,1);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);
		    	this.centralTop.setSpacing(true);

	    	panelResumen.setContent(centralTop);
	    	
	    	panelProduccion = new Panel("Resumen Prod. ");
		    panelProduccion.setHeightUndefined();
		    panelProduccion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelProduccion);
		    
		    	this.centralTop2 = new GridLayout(8,2);
		    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	this.centralTop2.setSpacing(true);
		    	
	    	panelProduccion.setContent(centralTop2);
	    	panelProduccion.getContent().setVisible(false);
	    	panelProduccion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    	panelOEE= new Panel("Resumen OEE ");
		    panelOEE.setHeightUndefined();
		    panelOEE.setWidth("100%");
		    panelOEE.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelOEE );
		    
		    	this.centralMiddle = new GridLayout(5,1);
		    	this.centralMiddle.setWidth("100%");
		    	this.centralMiddle.setMargin(true);
		    	this.centralMiddle.setSpacing(true);

	    	panelOEE.setContent(centralMiddle);
	    	panelOEE.getContent().setVisible(false);
	    	panelOEE.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    		this.centralMiddle2 = new GridLayout(5,1);
		    	this.centralMiddle2.setWidth("100%");
		    	this.centralMiddle2.setMargin(true);

	    	panelStockResumen= new Panel("Resumen Stock");
		    panelStockResumen.setHeightUndefined();
		    panelStockResumen.setWidth("100%");		    
		    panelStockResumen.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelStockResumen );

		    	this.centralBottom = new GridLayout(2,1);
		    	this.centralBottom.setWidth("100%");
		    	this.centralBottom.setMargin(true);
		    	this.centralBottom.setSpacing(true);
		    	
	    	panelStockResumen.setContent(centralBottom);
	    	panelStockResumen.getContent().setVisible(false);
	    	panelStockResumen.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumen);
    	this.barAndGridLayout.addComponent(this.panelDetalleArticulo);
    	this.barAndGridLayout.addComponent(this.panelProduccion);
    	this.barAndGridLayout.addComponent(this.panelOEE);
    	
    	
//    	this.barAndGridLayout.addComponent(this.centralMiddle);
//    	this.barAndGridLayout.addComponent(this.centralMiddle2);
//    	this.barAndGridLayout.addComponent(this.panelStockResumen);
//    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
//    	setExpandRatio(this.barAndGridLayout, 1);
        
    }

    private void procesar()
    {
		eliminarDashBoards();
		
		if (cmbVista.getValue()==null || cmbSemana.getValue()==null || txtEjercicio.getValue()==null 
				|| cmbVista.getValue().toString().length()==0 || cmbSemana.getValue().toString().length()==0 || txtEjercicio.getValue().length()==0 )
		{
			Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
		}
		else
		{
			cargarDashboards();
		}
    }
    
    private void cargarListeners() 
    {
    	
    	this.prev.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				setPrevision(prev.getValue());
			}
		});
    	
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});

    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));

			}
		});


    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});

    	LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};
//		centralLayoutT1.addLayoutClickListener(lcl);

    }

    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
    	this.cargarListeners();
    }

    public void eliminarDashBoards()
    {
    	centralTop.removeAllComponents();
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
//    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	centralDetalleArticulo.removeAllComponents();
    	
    }
    public void cargarDashboards() 
    {
    	String ejer = this.txtEjercicio.getValue();
    	String sem = this.cmbSemana.getValue().toString();
    	String vista = this.cmbVista.getValue().toString();
    	String campo = this.cmbCalculo.getValue().toString();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		{
    			this.mapeo = consultaDashboardBIBServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;
    		}
    		case "Anual":
    		{
    			this.mapeo = consultaDashboardBIBServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, "0", vista,campo);
    			break;
    		}
    		default:
    		{
    			this.mapeo = consultaDashboardBIBServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista,campo);
    			cargarMeses(ejer, sem);
    			break;
    		}
    	}
    	/*
    	 * cargar panel resumen datos
    	 */
    	cargarResumenSemanal(ejer);
    	cargarResumenArticulo(ejer, campo);
    	cargarResumenProduccion();
    	cargarResumenOEE();

    	/*
    	 * cargamos grafico con las medias de produccion
    	 */
    	
    	
    	
//    	tendenciaMediaActual5L();
//    	
//    	switch (vista)
//    	{
//    		case "Semanal":
//    		{
//    			cargarSemanal(ejer, sem);
//    			break;
//    		}
//    		case "Anual":
//    		{
//    			cargarAnual(ejer);
//    			break;
//    		}
//    	}
//    	
    	/*
    	 * Cargamos grid registros produccion
    	 */
//    	cargarRegistros(ejer);
    	
    	
    }
    
    private void cargarResumenOEE()
    {
    	cargarRelojesOEE();
    	centralMiddle.addComponent(indiceOEE5L(),0,0);
    	
    	this.panelOEE.setVisible(true);
    	this.panelOEE.getContent().setVisible(false);
    }
    private void cargarResumenSemanal(String r_ejer)
    {
    	cargarResumenHoras();
    	cargarResumen();
    	cargarResumenVentas();
    	cargarResumenStock();
    	cargarResumenStockPdteServ(r_ejer);
    	
    	if (!isPrevision()) cargarRelojes();
    }
    
    
    private void cargarResumenProduccion()
    {
    	
    	cargarResumenHorasPlanificadas();
    	cargarResumenPLanificacion();
    	cargarResumenProd();
    	if (!isPrevision())
    	{
    		cargarResumenHorasReales();
//    		cargarResumenProduccionReal();
    	}
    	
    	/*
    	 * cargar relojes botellas hora planificadas vs botellas hora reales
    	 */
    	/*
    	 * cargar graficos barras enfrentando paros programados vs paros reales
    	 */
    	
    	this.cargarGraficoIncidencias();
    	this.cargarGraficoProgramadas();
    	this.cargarGraficoProduccion();
    	this.cargarGraficoBotellasHora();
    	
    	this.panelProduccion.setVisible(true);
    	this.panelProduccion.getContent().setVisible(false);
    }
    
    
    private void cargarResumenHorasPlanificadas()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblTurnos = null;
    	Label lblValorTurnos = null;
    	
    	Label lblHoras = null;
    	Label lblValorHoras = null;
    	
    	Label lblInc = null;
    	Label lblValorInc = null;
    	
    	Label lblPro = null;
    	Label lblValorPro = null;
    	
    	Label lblDisp = null;
    	Label lblValorDisp = null;
    	
    	
    	Panel panel = new Panel("HORAS PLANIFICADAS");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue("Turnos: ");
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblTitulo ");
    	
    	lblValorTurnos = new Label();
    	lblValorTurnos.setWidth(anchoLabelDatos);
    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0111")/8));
    	lblValorTurnos.addStyleName("lblDatos");
    	
    	lin.addComponent(lblTurnos);
    	lin.addComponent(lblValorTurnos);
    	
    	lblHoras = new Label();
    	lblHoras.setValue("Horas: ");
    	lblHoras.setWidth(anchoLabelTitulo);
    	lblHoras.addStyleName("lblTitulo ");
    	
    	lblValorHoras = new Label();
    	lblValorHoras.setWidth(anchoLabelDatos);
    	lblValorHoras.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0111")));
    	lblValorHoras.addStyleName("lblDatos");
    	
    	lin2.addComponent(lblHoras);
    	lin2.addComponent(lblValorHoras);
    	
    	lblInc = new Label();
    	lblInc.setValue("Incidencias: ");
    	lblInc.setWidth(anchoLabelTitulo);
    	lblInc.addStyleName("lblTitulo ");
    	
    	lblValorInc = new Label();
    	lblValorInc.setWidth(anchoLabelDatos);
    	lblValorInc.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.inc.0111")));
    	lblValorInc.addStyleName("lblDatos");
    	
    	lin3.addComponent(lblInc);
    	lin3.addComponent(lblValorInc);
    	
    	lblPro = new Label();
    	lblPro.setValue("Programadas: ");
    	lblPro.setWidth(anchoLabelTitulo);
    	lblPro.addStyleName("lblTitulo ");
    	
    	lblValorPro = new Label();
    	lblValorPro.setWidth(anchoLabelDatos);
    	lblValorPro.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pro.0111")));
    	lblValorPro.addStyleName("lblDatos");
    	
    	lin4.addComponent(lblPro);
    	lin4.addComponent(lblValorPro);
    	
    	Double operativo = this.mapeo.getHashValores().get("0.horas.0111")-this.mapeo.getHashValores().get("0.inc.0111")-this.mapeo.getHashValores().get("0.pro.0111");
    	lblDisp = new Label();
    	lblDisp.setValue("Operativo: ");
    	lblDisp.setWidth(anchoLabelTitulo);
    	lblDisp.addStyleName("lblTitulo ");
    	
    	lblValorDisp = new Label();
    	lblValorDisp.setWidth(anchoLabelDatos);
    	lblValorDisp.setValue(RutinasNumericas.formatearDoubleDecimales(operativo,5));
    	lblValorDisp.addStyleName("lblDatos");
    	
    	lin5.addComponent(lblDisp);
    	lin5.addComponent(lblValorDisp);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop2.addComponent(panel,0,0);
    	
    }

    private void cargarResumenHorasReales()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblTurnos = null;
    	Label lblValorTurnos = null;
    	
    	Label lblHoras = null;
    	Button lblValorHoras = null;
    	
    	Label lblInc = null;
    	Button lblValorInc = null;
    	
    	Label lblPro = null;
    	Button lblValorPro = null;

    	Label lblDisp = null;
    	Button lblValorDisp = null;
    	
    	Panel panel = new Panel("HORAS REALES");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue("Turnos: ");
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblTitulo ");
    	
    	lblValorTurnos = new Label();
    	lblValorTurnos.setWidth(anchoLabelDatos);
    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0111")/8));
    	lblValorTurnos.addStyleName("lblDatos");
    	
    	lin.addComponent(lblTurnos);
    	lin.addComponent(lblValorTurnos);
    	
    	lblHoras = new Label();
    	lblHoras.setValue("Horas: ");
    	lblHoras.setWidth(anchoLabelTitulo);
    	lblHoras.addStyleName("lblTitulo ");
    	
    	lblValorHoras = new Button();
    	lblValorHoras.setWidth(anchoLabelDatos);
    	lblValorHoras.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0111")));
    	lblValorHoras.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorHoras.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorHoras.addStyleName("lblDatos");
    	lblValorHoras.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaTiempoDisponible vtD = new pantallaTiempoDisponible("Tiempo Disponible", mapeo.getHashValores().get("0.horasReales.0111"), mapeo.getHashValores().get("0.proReales.0111"),mapeo.getHashValores().get("0.incReales.0111"));
				getUI().addWindow(vtD);
			}
		});
    	
    	lin2.addComponent(lblHoras);
    	lin2.addComponent(lblValorHoras);
    	
    	lblInc = new Label();
    	lblInc.setValue("Incidencias: ");
    	lblInc.setWidth(anchoLabelTitulo);
    	lblInc.addStyleName("lblTitulo ");
    	
    	lblValorInc = new Button();
    	lblValorInc.setWidth(anchoLabelDatos);
    	lblValorInc.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.incReales.0111")));
    	lblValorInc.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorInc.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorInc.addStyleName("lblDatos");
    	lblValorInc.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				String semana = null;
				if (cmbVista.getValue().toString().contains("Anual")) semana = null;
				else if (cmbSemana.getValue()!=null) semana = cmbSemana.getValue().toString();
				pantallaIncidencias vtI = new pantallaIncidencias("BIB", "Incidencias", txtEjercicio.getValue().toString(), semana, "Todas", cmbVista.getValue().toString());
				getUI().addWindow(vtI);
			}
		});
    	
    	lin3.addComponent(lblInc);
    	lin3.addComponent(lblValorInc);
    	
    	lblPro = new Label();
    	lblPro.setValue("Programadas: ");
    	lblPro.setWidth(anchoLabelTitulo);
    	lblPro.addStyleName("lblTitulo ");
    	
    	lblValorPro = new Button();
    	lblValorPro.setWidth(anchoLabelDatos);
    	lblValorPro.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.proReales.0111")));
    	lblValorPro.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPro.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPro.addStyleName("lblDatos");
    	lblValorPro.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				String semana = null;
				if (cmbVista.getValue().toString().contains("Anual")) semana = null;
				else if (cmbSemana.getValue()!=null) semana = cmbSemana.getValue().toString();
				pantallaIncidencias vtI = new pantallaIncidencias("BIB", "Programadas", txtEjercicio.getValue().toString(), semana, "P",cmbVista.getValue().toString());
				getUI().addWindow(vtI);
			}
		});
    	
    	lin4.addComponent(lblPro);
    	lin4.addComponent(lblValorPro);
    	
    	Double operativo = this.mapeo.getHashValores().get("0.horasReales.0111")-this.mapeo.getHashValores().get("0.incReales.0111")-this.mapeo.getHashValores().get("0.proReales.0111");
    	lblDisp = new Label();
    	lblDisp.setValue("Operativo: ");
    	lblDisp.setWidth(anchoLabelTitulo);
    	lblDisp.addStyleName("lblTitulo ");
    	
    	lblValorDisp = new Button();
    	lblValorDisp.setWidth(anchoLabelDatos);
    	lblValorDisp.setCaption(RutinasNumericas.formatearDoubleDecimales(operativo,5));
    	lblValorDisp.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorDisp.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorDisp.addStyleName("lblDatos");
    	
    	lin5.addComponent(lblDisp);
    	lin5.addComponent(lblValorDisp);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop2.addComponent(panel,1,0);
    	
    }
    
    private void cargarResumenPLanificacion()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblProduccionTotal = null;
    	Label lblProduccionBlanco= null;
    	Label lblProduccionRosado= null;
    	Label lblProduccionTinto= null;
    	Label lblProduccionTintoU= null;
    	Label lblCiclo= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccionBlanco= null;
    	Label lblValorProduccionRosado= null;
    	Label lblValorProduccionTinto= null;
    	Label lblValorProduccionTintoU= null;
    	Label lblValorCiclo= null;
    	
    	Panel panel = new Panel("PRODUCCION PLANIFICADA");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PLANIFICACION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);

    	lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0111")));
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblProduccionTotal);
    	lin.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccionBlanco = new Label();
	    	lblProduccionBlanco.setValue(" Blanco: ");
	    	lblProduccionBlanco.setWidth(anchoLabelTitulo);
	    	lblProduccionBlanco.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionBlanco = new Label();
	    	lblValorProduccionBlanco.setWidth(anchoLabelDatos);
	    	lblValorProduccionBlanco.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.030")));
	    	lblValorProduccionBlanco.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblProduccionBlanco);
	    	lin2.addComponent(lblValorProduccionBlanco);
	    	
	    	lblProduccionRosado = new Label();
	    	lblProduccionRosado.setValue("Rosado: ");
	    	lblProduccionRosado.setWidth(anchoLabelTitulo);
	    	lblProduccionRosado.addStyleName("lblTituloDerecha");
		
	    	lblValorProduccionRosado = new Label();
	    	lblValorProduccionRosado.setWidth(anchoLabelDatos);
	    	lblValorProduccionRosado.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.020")));
	    	lblValorProduccionRosado.addStyleName("lblDatos");
		    	
	    	lin3.addComponent(lblProduccionRosado);
	    	lin3.addComponent(lblValorProduccionRosado);

	    	lblProduccionTinto = new Label();
	    	lblProduccionTinto.setValue("Tinto: ");
	    	lblProduccionTinto.setWidth(anchoLabelTitulo);
	    	lblProduccionTinto.addStyleName("lblTituloDerecha");
	    	
	    	lblValorProduccionTinto= new Label();
	    	lblValorProduccionTinto.setWidth(anchoLabelDatos);
	    	lblValorProduccionTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.010")));
	    	lblValorProduccionTinto.addStyleName("lblDatos");
		    	
	    	lin4.addComponent(lblProduccionTinto);
	    	lin4.addComponent(lblValorProduccionTinto);
	    	
	    	lblProduccionTintoU = new Label();
	    	lblProduccionTintoU.setValue("Tinto Usa: ");
	    	lblProduccionTintoU.setWidth(anchoLabelTitulo);
	    	lblProduccionTintoU.addStyleName("lblTituloDerecha");
	    	
	    	lblValorProduccionTintoU= new Label();
	    	lblValorProduccionTintoU.setWidth(anchoLabelDatos);
	    	lblValorProduccionTintoU.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.011")));
	    	lblValorProduccionTintoU.addStyleName("lblDatos");
		    	
	    	lin5.addComponent(lblProduccionTintoU);
	    	lin5.addComponent(lblValorProduccionTintoU);
		
	    	consultaDashboardBIBServer cses = consultaDashboardBIBServer.getInstance(CurrentUser.get());
			if (this.cmbSemana.getValue()!=null) semana = this.cmbSemana.getValue().toString();
			
//	    	Double velocidadNominal = cses.obtenerTiempoDeCiclo(this.txtEjercicio.getValue(),semana,this.cmbVista.getValue().toString(), true);
//	    	
//	    	lblCiclo = new Label();
//	    	lblCiclo.setValue("Tiempo Ciclo: ");
//	    	lblCiclo.setWidth(anchoLabelTitulo);
//	    	lblCiclo.addStyleName("lblTituloDerecha");
//	    	
//	    	lblValorCiclo= new Label();
//	    	lblValorCiclo.setWidth(anchoLabelDatos);
//	    	lblValorCiclo.setValue(RutinasNumericas.formatearDoubleDecimales(velocidadNominal,5));
//	    	lblValorCiclo.addStyleName("lblDatos");
//	    	
//	    	lin6.addComponent(lblCiclo);
//	    	lin6.addComponent(lblValorCiclo);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
//    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProgramacion();
			}
		});
    	centralTop2.addComponent(panel,2,0);
    }
    
    private void cargarRelojes()
    {
    	/*
    	 * GENERACION DATOS GLOBAL
    	 */
    	
    	Double valorG = null;
    	Double horasG = null;
    	Double mediaG = null;
    	Panel gaugeGL = null;
    	
    	valorG = this.mapeo.getHashValores().get("0.prod.0111");
    	horasG = this.mapeo.getHashValores().get("0.horasReales.0111");

    	if(valorG!=null && horasG != null && horasG.doubleValue()!=0)
    	{
    		mediaG = valorG/horasG*8;

    		if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    			gaugeGL = reloj.generarReloj(  "MEDIA GLOBAL (Ltr/Tno.)",mediaG.intValue(), 5000, 7500, 3500, 4999, 0, 3499, 0, 7500, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    		else
    			gaugeGL = reloj.generarReloj(  "MEDIA GLOBAL (Cj/Tno.)",mediaG.intValue(), 1500, 2125, 1000, 1499, 0, 999, 0, 2500, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    		
    		centralTop.addComponent(gaugeGL,5,0);
    	}
    }
    
    private void cargarMeses(String r_ejercicio, String r_semana)
    {
    	/*
//    	 * GENERACION DATOS 5L
//    	 */
//    	Panel gauge = reloj.generarReloj("5L",42900, 50000, 75000, 35000, 49999, 0, 34999, 0, 75000, GaugeStyle.STYLE_DEFAULT.toString(),150);
//    	
//    	centralLayoutT2.addComponent(gauge);
//
//    	/*
//    	 * GENERACION DATOS 2L
//    	 */
//    	Panel gauge2L = reloj.generarReloj("2L",33900, 45000, 75000, 30000, 44999, 0, 29999, 0, 75000,GaugeStyle.STYLE_DEFAULT.toString(),150);
//    	
//    	centralLayoutT3.addComponent(gauge2L);
//    	
//    	centralTop.addComponent(centralLayoutT2);
//    	centralTop.addComponent(centralLayoutT3);
//
//    	this.pieVentasTotales();
//    	this.tendenciaMedia5L();
//    	this.tendenciaMedia2L();
    	
    }
    
    private void cargarRegistros(String r_ejercicio)
    {
    	
    }

    private void cargarResumen()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";

    	Label lblProduccionTeorica = null;
    	Label lblValorProduccionTeorica= null;

    	Label lblProduccionTotal = null;
    	Label lblProduccion5L= null;
    	Label lblProduccionTinto5L= null;
    	Label lblProduccionRosa5L= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccion5L= null;
    	Label lblValorProduccionTinto5L= null;
    	Label lblValorProduccionRosa5L= null;
    	
    	Panel panel = new Panel("RESUMEN PRODUCCION");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PRODUCCION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin1 = new HorizontalLayout();
    	lin1.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);

    	if (isPrevision())
    	{
	    	lblProduccionTeorica = new Label();
	    	lblProduccionTeorica.setValue("Teorica: ");
	    	lblProduccionTeorica.setWidth(anchoLabelTitulo);    	
	    	lblProduccionTeorica.addStyleName("lblTitulo");
	
	    	lblValorProduccionTeorica = new Label();
	    	lblValorProduccionTeorica.setWidth(anchoLabelDatos);
	    	lblValorProduccionTeorica.setValue(RutinasNumericas.formatearDouble(((this.mapeo.getHashValores().get("0.horas.0111")-this.mapeo.getHashValores().get("0.pro.01030"))/8*58000)+((this.mapeo.getHashValores().get("0.horas.01031")-this.mapeo.getHashValores().get("0.pro.01031"))/8*48000)));
	    	lblValorProduccionTeorica.addStyleName("lblDatos");
	    	
	    	lin.addComponent(lblProduccionTeorica);
	    	lin.addComponent(lblValorProduccionTeorica);

    	}
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
    	if (isPrevision())
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.0111")));    		
    	}
    	else
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0111")));
    	}
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin1.addComponent(lblProduccionTotal);
    	lin1.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccion5L = new Label();
	    	lblProduccion5L.setValue(" Blanco: ");
	    	lblProduccion5L.setWidth(anchoLabelTitulo);
	    	lblProduccion5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccion5L = new Label();
	    	lblValorProduccion5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.030")));
	    	}
	    	else
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.030")));
	    	}
	    	lblValorProduccion5L.addStyleName("lblDatos");
	    	
    	lin2.addComponent(lblProduccion5L);
    	lin2.addComponent(lblValorProduccion5L);
    	
	    	lblProduccionTinto5L = new Label();
	    	lblProduccionTinto5L.setValue("Rosado: ");
	    	lblProduccionTinto5L.setWidth(anchoLabelTitulo);
	    	lblProduccionTinto5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionTinto5L = new Label();
	    	lblValorProduccionTinto5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.020")));
	    	}
	    	else
	    	{
	    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.020")));
	    	}
	    	lblValorProduccionTinto5L.addStyleName("lblDatos");
		    	
	    	lblProduccionRosa5L = new Label();
	    	lblProduccionRosa5L.setValue("Tinto: ");
	    	lblProduccionRosa5L.setWidth(anchoLabelTitulo);
	    	lblProduccionRosa5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionRosa5L = new Label();
	    	lblValorProduccionRosa5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010") + this.mapeo.getHashValores().get("0.pre.011")));
	    	}
	    	else
	    	{
	    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010") + this.mapeo.getHashValores().get("0.prod.011")));
	    	}
	    	lblValorProduccionRosa5L.addStyleName("lblDatos");
	    	
    	lin3.addComponent(lblProduccionTinto5L);
    	lin3.addComponent(lblValorProduccionTinto5L);
    	lin4.addComponent(lblProduccionRosa5L);
    	lin4.addComponent(lblValorProduccionRosa5L);
		
    	
    	if (isPrevision()) content.addComponent(lin);
    	content.addComponent(lin1);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProduccion();
			}
		});

    	
    	centralTop.addComponent(panel,1,0);
    }
    
    private void cargarResumenProd()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";

    	Label lblProduccionTeorica = null;
    	Label lblValorProduccionTeorica= null;

    	Label lblProduccionTotal = null;
    	Label lblProduccion5L= null;
    	Label lblProduccionTinto5L= null;
    	Label lblProduccionRosa5L= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccion5L= null;
    	Label lblValorProduccionTinto5L= null;
    	Label lblValorProduccionRosa5L= null;
    	
    	Panel panel2 = new Panel("RESUMEN PRODUCCION");
    	
    	if (isPrevision())
    	{
    		panel2.setCaption("PREVISION PRODUCCION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel2.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin1 = new HorizontalLayout();
    	lin1.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);

    	if (isPrevision())
    	{
	    	lblProduccionTeorica = new Label();
	    	lblProduccionTeorica.setValue("Teorica: ");
	    	lblProduccionTeorica.setWidth(anchoLabelTitulo);    	
	    	lblProduccionTeorica.addStyleName("lblTitulo");
	
	    	lblValorProduccionTeorica = new Label();
	    	lblValorProduccionTeorica.setWidth(anchoLabelDatos);
	    	lblValorProduccionTeorica.setValue(RutinasNumericas.formatearDouble(((this.mapeo.getHashValores().get("0.horas.0111")-this.mapeo.getHashValores().get("0.pro.01030"))/8*58000)+((this.mapeo.getHashValores().get("0.horas.01031")-this.mapeo.getHashValores().get("0.pro.01031"))/8*48000)));
	    	lblValorProduccionTeorica.addStyleName("lblDatos");
	    	
	    	lin.addComponent(lblProduccionTeorica);
	    	lin.addComponent(lblValorProduccionTeorica);

    	}
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
    	if (isPrevision())
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.0111")));    		
    	}
    	else
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0111")));
    	}
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin1.addComponent(lblProduccionTotal);
    	lin1.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccion5L = new Label();
	    	lblProduccion5L.setValue(" Blanco: ");
	    	lblProduccion5L.setWidth(anchoLabelTitulo);
	    	lblProduccion5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccion5L = new Label();
	    	lblValorProduccion5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.020")));
	    	}
	    	else
	    	{
	    		lblValorProduccion5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.020")));
	    	}
	    	lblValorProduccion5L.addStyleName("lblDatos");
	    	
    	lin2.addComponent(lblProduccion5L);
    	lin2.addComponent(lblValorProduccion5L);
    	
	    	lblProduccionTinto5L = new Label();
	    	lblProduccionTinto5L.setValue("Rosado: ");
	    	lblProduccionTinto5L.setWidth(anchoLabelTitulo);
	    	lblProduccionTinto5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionTinto5L = new Label();
	    	lblValorProduccionTinto5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.030")));
	    	}
	    	else
	    	{
	    		lblValorProduccionTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.030")));
	    	}
	    	lblValorProduccionTinto5L.addStyleName("lblDatos");
		    	
	    	lblProduccionRosa5L = new Label();
	    	lblProduccionRosa5L.setValue("Tinto: ");
	    	lblProduccionRosa5L.setWidth(anchoLabelTitulo);
	    	lblProduccionRosa5L.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionRosa5L = new Label();
	    	lblValorProduccionRosa5L.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pre.010") + this.mapeo.getHashValores().get("0.pre.011")));
	    	}
	    	else
	    	{
	    		lblValorProduccionRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.010") + this.mapeo.getHashValores().get("0.prod.011")));
	    	}
	    	lblValorProduccionRosa5L.addStyleName("lblDatos");
	    	
    	lin3.addComponent(lblProduccionTinto5L);
    	lin3.addComponent(lblValorProduccionTinto5L);
    	lin4.addComponent(lblProduccionRosa5L);
    	lin4.addComponent(lblValorProduccionRosa5L);
		
    	
    	if (isPrevision()) content.addComponent(lin);
    	content.addComponent(lin1);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel2.setContent(content);
    	
    	
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProduccion();
			}
		});

    	
    	centralTop2.addComponent(panel2,3,0);
    }
    
    private void cargarResumenStock()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	String traerVentas = null;
    	
    	Label lblStockTotal = null;
    	Label lblStock5L= null;
    	Label lblStockTinto5L= null;
    	Label lblStockRosa5L= null;

    	Label lblValorStockTotal = null;
    	Label lblValorStock5L= null;
    	Label lblValorStockTinto5L= null;
    	Label lblValorStockRosa5L= null;
    	
    	traerVentas="0";
    	Panel panel = new Panel("RESUMEN STOCK");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION STOCK");
    		traerVentas="1";
    	}

//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);
    	
    	lblStockTotal = new Label();
    	lblStockTotal.setValue("Total: ");
    	lblStockTotal.setWidth(anchoLabelTitulo);    	
    	lblStockTotal.addStyleName("lblTitulo");

    	lblValorStockTotal = new Label();
    	lblValorStockTotal.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0111")));
    	}
    	else
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0111")+this.mapeo.getHashValores().get("0.pre.0111")  -this.mapeo.getHashValores().get(traerVentas + ".venta.0111")*this.correctorVentasEjercicio));
    	}
    	lblValorStockTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblStockTotal);
    	lin.addComponent(lblValorStockTotal);
    	
	    	lblStock5L = new Label();
	    	lblStock5L.setValue(" Blanco: ");
	    	lblStock5L.setWidth(anchoLabelTitulo);
	    	lblStock5L.addStyleName("lblTituloDerecha");
	
	    	lblValorStock5L = new Label();
	    	lblValorStock5L.setWidth(anchoLabelDatos);
	    	if (!isPrevision())
	    	{
	    		lblValorStock5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.030")));
	    	}
	    	else
	    	{
	    		lblValorStock5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.030")+this.mapeo.getHashValores().get("0.pre.030")-this.mapeo.getHashValores().get(traerVentas + ".venta.01030")*this.correctorVentasEjercicio));
	    	}
	    	lblValorStock5L.addStyleName("lblDatos");
	    	
    	lin2.addComponent(lblStock5L);
    	lin2.addComponent(lblValorStock5L);
	    	
	    	lblStockTinto5L = new Label();
	    	lblStockTinto5L.setValue("Rosado: ");
	    	lblStockTinto5L.setWidth(anchoLabelTitulo);
	    	lblStockTinto5L.addStyleName("lblTituloDerecha");
	
	    	lblValorStockTinto5L = new Label();
	    	lblValorStockTinto5L.setWidth(anchoLabelDatos);
	    	if (!isPrevision())
	    	{
	    		lblValorStockTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.020")));
	    	}
	    	else
	    	{
	    		lblValorStockTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.020")+this.mapeo.getHashValores().get("0.pre.020")-this.mapeo.getHashValores().get(traerVentas + ".venta.010300")*this.correctorVentasEjercicio));
	    	}
	    	lblValorStockTinto5L.addStyleName("lblDatos");
	    	
	    	lblStockRosa5L = new Label();
	    	lblStockRosa5L.setValue("Tinto: ");
	    	lblStockRosa5L.setWidth(anchoLabelTitulo);
	    	lblStockRosa5L.addStyleName("lblTituloDerecha");
	
	    	lblValorStockRosa5L = new Label();
	    	lblValorStockRosa5L.setWidth(anchoLabelDatos);
	    	if (!isPrevision())
	    	{
	    		lblValorStockRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010") + this.mapeo.getHashValores().get("0.stock.011")));
	    	}
	    	else
	    	{
	    		lblValorStockRosa5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.010")+this.mapeo.getHashValores().get("0.pre.010301")-this.mapeo.getHashValores().get(traerVentas + ".venta.010301")*this.correctorVentasEjercicio));
	    	}
	    	lblValorStockRosa5L.addStyleName("lblDatos");
	    	
    		lin3.addComponent(lblStockTinto5L);
	    	lin3.addComponent(lblValorStockTinto5L);

	    	lin4.addComponent(lblStockRosa5L);
	    	lin4.addComponent(lblValorStockRosa5L);
		
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
//    	if (isPrevision()) 
    	centralTop.addComponent(panel,3,0);
    }

    private void cargarResumenStockPdteServ(String r_ejer)
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	String traerVentas = null;
    	
    	Label lblTipo = null;
    	Label lblCantidad= null;
    	Label lblSemana= null;

    	Label lblStockTotal = null;
    	Label lblStockBlanco= null;
    	Label lblStockTinto= null;
    	Label lblStockRosado= null;
    	
    	Label lblValorStockTotal = null;
    	Label lblValorStockBlanco= null;
    	Label lblValorStockTinto= null;
    	Label lblValorStockRosado= null;

    	Label lblSemanaStockBlanco= null;
    	Label lblSemanaStockTinto= null;
    	Label lblSemanaStockRosado= null;
    	
    	traerVentas="0";
    	
    	Integer ejercicio= new Integer(r_ejer);
    	Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
    	String digito = "";
    	
    	if (ejercicio == ejercicioActual) digito="0";
		if (ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-ejercicio);
		
    	Panel panel = new Panel("PENDIENTE SERVIR");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PdteServ");
    		traerVentas="1";
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	HorizontalLayout lin0 = new HorizontalLayout();
    	lin0.setSpacing(true);
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblTipo = new Label();
    	lblTipo.setValue("Tipo ");
    	lblTipo.setWidth(anchoLabelTitulo);    	
    	lblTipo.addStyleName("lblTituloCentrado");

    	lblCantidad = new Label();
    	lblCantidad.setValue("Cantidad ");
    	lblCantidad.setWidth(anchoLabelTitulo);    	
    	lblCantidad.addStyleName("lblTituloCentrado");
    	
    	lblSemana = new Label();
    	lblSemana.setValue("Semana ");
    	lblSemana.setWidth(anchoLabelTitulo);    	
    	lblSemana.addStyleName("lblTituloCentrado");
    	
    	lin0.addComponent(lblTipo);
    	lin0.addComponent(lblCantidad);
    	lin0.addComponent(lblSemana);
    	
    	lblStockTotal = new Label();
    	lblStockTotal.setValue("Total: ");
    	lblStockTotal.setWidth(anchoLabelTitulo);    	
    	lblStockTotal.addStyleName("lblTitulo");
    	
    	lblValorStockTotal = new Label();
    	lblValorStockTotal.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.0111")));
    	}
    	else
    	{
    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.0111")+this.mapeo.getHashValores().get("0.pre.0111")  -this.mapeo.getHashValores().get(traerVentas + ".venta.0111")*this.correctorVentasEjercicio));
    	}
    	lblValorStockTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblStockTotal);
    	lin.addComponent(lblValorStockTotal);
    	
    	lblStockBlanco = new Label();
    	lblStockBlanco.setValue(" Blanco: ");
    	lblStockBlanco.setWidth(anchoLabelTitulo);
    	lblStockBlanco.addStyleName("lblTituloDerecha");
    	
    	lblValorStockBlanco = new Label();
    	lblValorStockBlanco.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockBlanco.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.030")));
    	}
    	else
    	{
    		lblValorStockBlanco.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.030")+this.mapeo.getHashValores().get("0.pre.030")-this.mapeo.getHashValores().get(traerVentas + ".venta.01030")*this.correctorVentasEjercicio));
    	}
    	lblValorStockBlanco.addStyleName("lblDatos");

    	lblSemanaStockBlanco = new Label();
    	lblSemanaStockBlanco.setWidth(anchoLabelDatos);
    	lblSemanaStockBlanco.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plazoPdteServ.030")));
    	lblSemanaStockBlanco.addStyleName("lblDatos");
    	
    	lin2.addComponent(lblStockBlanco);
    	lin2.addComponent(lblValorStockBlanco);
    	lin2.addComponent(lblSemanaStockBlanco);
    	
    	lblStockRosado = new Label();
    	lblStockRosado.setValue("Rosado: ");
    	lblStockRosado.setWidth(anchoLabelTitulo);
    	lblStockRosado.addStyleName("lblTituloDerecha");
    	
    	lblValorStockRosado = new Label();
    	lblValorStockRosado.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockRosado.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.020")));
    	}
    	else
    	{
    		lblValorStockRosado.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.020")+this.mapeo.getHashValores().get("0.pre.020")-this.mapeo.getHashValores().get(traerVentas + ".venta.010300")*this.correctorVentasEjercicio));
    	}
    	lblValorStockRosado.addStyleName("lblDatos");
    	
    	lblSemanaStockRosado = new Label();
    	lblSemanaStockRosado.setWidth(anchoLabelDatos);
    	lblSemanaStockRosado.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plazoPdteServ.020")));
    	lblSemanaStockRosado.addStyleName("lblDatos");

    	lblStockTinto = new Label();
    	lblStockTinto.setValue("Tinto: ");
    	lblStockTinto.setWidth(anchoLabelTitulo);
    	lblStockTinto.addStyleName("lblTituloDerecha");
    	
    	lblValorStockTinto = new Label();
    	lblValorStockTinto.setWidth(anchoLabelDatos);
    	if (!isPrevision())
    	{
    		lblValorStockTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.010") + this.mapeo.getHashValores().get("0.stockPdteServ.011")));
    	}
    	else
    	{
    		lblValorStockTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stockPdteServ.010")+this.mapeo.getHashValores().get("0.pre.010301")-this.mapeo.getHashValores().get(traerVentas + ".venta.010301")*this.correctorVentasEjercicio));
    	}
    	lblValorStockTinto.addStyleName("lblDatos");
    	
    	lblSemanaStockTinto= new Label();
    	lblSemanaStockTinto.setWidth(anchoLabelDatos);
    	
    	if (this.mapeo.getHashValores().get("0.plazoPdteServ.010").compareTo(this.mapeo.getHashValores().get("0.plazoPdteServ.011"))>0)
    		lblSemanaStockTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plazoPdteServ.011")));
    	else
    		lblSemanaStockTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plazoPdteServ.010")));
    	lblSemanaStockTinto.addStyleName("lblDatos");

    	
    	lin3.addComponent(lblStockRosado);
    	lin3.addComponent(lblValorStockRosado);
    	lin3.addComponent(lblSemanaStockRosado);
    	
    	lin4.addComponent(lblStockTinto);
    	lin4.addComponent(lblValorStockTinto);
    	lin4.addComponent(lblSemanaStockTinto);
    	
    	content.addComponent(lin0);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin);
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
//    	if (isPrevision()) 
    	centralTop.addComponent(panel,4,0);
    }
    
    private void cargarResumenVentas()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblVentasTotal = null;
    	Label lblVentas5L= null;
    	Label lblVentasTinto5L= null;
    	Label lblVentasRosa5L= null;
    	Label lblVentas2L= null;
    	Label lblVentasTinto2L= null;
    	Label lblVentasRosa2L= null;

    	Label lblValorVentasTotal = null;
    	Label lblValorVentas5L= null;
    	Label lblValorVentasTinto5L= null;
    	Label lblValorVentasRosa5L= null;
    	Label lblValorVentas2L= null;
    	Label lblValorVentasTinto2L= null;
    	Label lblValorVentasRosa2L= null;
    	
    	
    	String traerVentas = "0";
    	Panel panel = new Panel("RESUMEN VENTAS");

    	if (isPrevision())
    	{
    		traerVentas="1";
    		panel.setCaption("RESUMEN VENTAS EJ. ANTERIOR" );
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	HorizontalLayout lin7 = new HorizontalLayout();
    	lin7.setSpacing(true);
    	
    	lblVentasTotal = new Label();
    	lblVentasTotal.setValue("Total: ");
    	lblVentasTotal.setWidth(anchoLabelTitulo);    	
    	lblVentasTotal.addStyleName("lblTitulo");

    	lblValorVentasTotal = new Label();
    	lblValorVentasTotal.setWidth(anchoLabelDatos);
    	lblValorVentasTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.0111")*correctorVentasEjercicio));
    	lblValorVentasTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblVentasTotal);
    	lin.addComponent(lblValorVentasTotal);
    	
    	lblVentas5L = new Label();
    	lblVentas5L.setValue(" Blanco: ");
    	lblVentas5L.setWidth(anchoLabelTitulo);
    	lblVentas5L.addStyleName("lblTituloDerecha");

    	lblValorVentas5L = new Label();
    	lblValorVentas5L.setWidth(anchoLabelDatos);
    	lblValorVentas5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.030")*correctorVentasEjercicio));
    	lblValorVentas5L.addStyleName("lblDatos");
    	
    	lin2.addComponent(lblVentas5L);
    	lin2.addComponent(lblValorVentas5L);
	    	
		    	lblVentasTinto5L = new Label();
		    	lblVentasTinto5L.setValue("Rosado: ");
		    	lblVentasTinto5L.setWidth(anchoLabelTitulo);
		    	lblVentasTinto5L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasTinto5L = new Label();
		    	lblValorVentasTinto5L.setWidth(anchoLabelDatos);
		    	lblValorVentasTinto5L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.020")*correctorVentasEjercicio));
		    	lblValorVentasTinto5L.addStyleName("lblDatos");
		    	
		    	lblVentasRosa5L = new Label();
		    	lblVentasRosa5L.setValue("Tinto: ");
		    	lblVentasRosa5L.setWidth(anchoLabelTitulo);
		    	lblVentasRosa5L.addStyleName("lblTituloDerecha");
		
		    	lblValorVentasRosa5L = new Label();
		    	lblValorVentasRosa5L.setWidth(anchoLabelDatos);
		    	lblValorVentasRosa5L.setValue(RutinasNumericas.formatearDouble((this.mapeo.getHashValores().get(traerVentas + ".venta.010")+this.mapeo.getHashValores().get(traerVentas + ".venta.011"))*correctorVentasEjercicio));
		    	lblValorVentasRosa5L.addStyleName("lblDatos");
		    	
		    	lin3.addComponent(lblVentasTinto5L);
		    	lin3.addComponent(lblValorVentasTinto5L);
		    	lin4.addComponent(lblVentasRosa5L);
		    	lin4.addComponent(lblValorVentasRosa5L);
//		
//	    	lblVentas2L = new Label();
//	    	lblVentas2L.setValue(" 2L: ");
//	    	lblVentas2L.setWidth(anchoLabelTitulo);
//	    	lblVentas2L.addStyleName("lblTituloCentrado");
//	    	
//	    	lblValorVentas2L = new Label();
//	    	lblValorVentas2L.setWidth(anchoLabelDatos);
//	    	lblValorVentas2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.01031")*correctorVentasEjercicio));
//	    	lblValorVentas2L.addStyleName("lblDatos");
//	    	lin5.addComponent(lblVentas2L);
//	    	lin5.addComponent(lblValorVentas2L);
//	
//		    	lblVentasTinto2L = new Label();
//		    	lblVentasTinto2L.setValue("Tinto 2L: ");
//		    	lblVentasTinto2L.setWidth(anchoLabelTitulo);
//		    	lblVentasTinto2L.addStyleName("lblTituloDerecha");
//		
//		    	lblValorVentasTinto2L = new Label();
//		    	lblValorVentasTinto2L.setWidth(anchoLabelDatos);
//		    	lblValorVentasTinto2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010310")*correctorVentasEjercicio));
//		    	lblValorVentasTinto2L.addStyleName("lblDatos");
//		    	
//		    	lblVentasRosa2L = new Label();
//		    	lblVentasRosa2L.setValue("Rosado 2L: ");
//		    	lblVentasRosa2L.setWidth(anchoLabelTitulo);
//		    	lblVentasRosa2L.addStyleName("lblTituloDerecha");
//		
//		    	lblValorVentasRosa2L = new Label();
//		    	lblValorVentasRosa2L.setWidth(anchoLabelDatos);
//		    	lblValorVentasRosa2L.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.010311")*correctorVentasEjercicio));
//		    	lblValorVentasRosa2L.addStyleName("lblDatos");
//		    	
//		
//		    	lin6.addComponent(lblVentasTinto2L);
//		    	lin6.addComponent(lblValorVentasTinto2L);
//		    	lin7.addComponent(lblVentasRosa2L);
//		    	lin7.addComponent(lblValorVentasRosa2L);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
//    	content.addComponent(lin6);
//    	content.addComponent(lin7);
    	
//    	content.addComponent(new TextField("Organization"));
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,2,0);
    }
    
    private void cargarResumenHoras()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";

    	Label lblTurnos = null;
    	Label lblValorTurnos = null;

    	Label lblHorasBlanco = null;
    	Label lblValorHorasBlanco = null;
    	Label lblHorasRosado = null;
    	Label lblValorHorasRosado = null;
    	Label lblHorasTinto = null;
    	Label lblValorHorasTinto= null;
    	
    	Panel panel = new Panel("HORAS SEMANALES");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
    	panel.setSizeUndefined();

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);

    		lblTurnos = new Label();
	    	lblTurnos.setValue("Turnos: ");
	    	lblTurnos.setWidth(anchoLabelTitulo);
	    	lblTurnos.addStyleName("lblTitulo ");
	
	    	lblValorTurnos = new Label();
	    	lblValorTurnos.setWidth(anchoLabelDatos);
	    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble((this.mapeo.getHashValores().get("0.horasReales.0111"))/8));
	    	lblValorTurnos.addStyleName("lblDatos");

	    	lin.addComponent(lblTurnos);
	    	lin.addComponent(lblValorTurnos);

	    	lblHorasBlanco = new Label();
	    	lblHorasBlanco.setValue("Horas: ");
	    	lblHorasBlanco.setWidth(anchoLabelTitulo);
	    	lblHorasBlanco.addStyleName("lblTitulo ");
	
	    	lblValorHorasBlanco = new Label();
	    	lblValorHorasBlanco.setWidth(anchoLabelDatos);
	    	lblValorHorasBlanco.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0111")));
	    	lblValorHorasBlanco.addStyleName("lblDatos");

	    	lin2.addComponent(lblHorasBlanco);
	    	lin2.addComponent(lblValorHorasBlanco);
	    	
	    	lblHorasRosado = new Label();
	    	lblHorasRosado .setValue("Horas Rosado: ");
	    	lblHorasRosado .setWidth(anchoLabelTitulo);
	    	lblHorasRosado .addStyleName("lblTitulo ");
	    	
	    	lblValorHorasRosado  = new Label();
	    	lblValorHorasRosado .setWidth(anchoLabelDatos);
	    	lblValorHorasRosado .setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.020")));
	    	lblValorHorasRosado .addStyleName("lblDatos");

	    	lin3.addComponent(lblHorasRosado );
	    	lin3.addComponent(lblValorHorasRosado );

	    	lblHorasTinto = new Label();
	    	lblHorasTinto.setValue("Horas Tinto: ");
	    	lblHorasTinto.setWidth(anchoLabelTitulo);
	    	lblHorasTinto.addStyleName("lblTitulo ");
	    	
	    	lblValorHorasTinto = new Label();
	    	lblValorHorasTinto.setWidth(anchoLabelDatos);
	    	lblValorHorasTinto.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0111")));
	    	lblValorHorasTinto.addStyleName("lblDatos");
	    	
	    	lin4.addComponent(lblHorasTinto);
	    	lin4.addComponent(lblValorHorasTinto);

    	content.addComponent(lin);
    	content.addComponent(lin2);
//    	content.addComponent(lin3);
//    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,0,0);
    	
    }
    
    private double obtenerProductividad(String r_area, Double r_produccion, Double r_horas, Double r_incidencias, Double r_programadas, boolean r_prevision)
    {
    	Double velocidadNominal = null;
    	Double productividad = null;
    	Double tiempoOperativo = null;
    	String semana = null;
    	switch (r_area)
    	{
    		case "BIB":
    		{
    			consultaDashboardBIBServer cbibes = consultaDashboardBIBServer.getInstance(CurrentUser.get());
    			if (this.cmbSemana.getValue()!=null) semana = this.cmbSemana.getValue().toString();
    			velocidadNominal = cbibes.obtenerTiempoDeCiclo(this.txtEjercicio.getValue(),semana,this.cmbVista.getValue().toString(), r_prevision);
    			break;
    		}
    	}
    	tiempoOperativo = r_horas - r_incidencias - r_programadas;
    	productividad = velocidadNominal / tiempoOperativo;
    	
    	return productividad;
    }
    
    private double obtenerCalidad(Double r_produccion, Double r_mermas)
    {
    	return (r_produccion - r_mermas)/(r_produccion);
    }

    private double obtenerDisponibilidad(Double r_horas, Double r_incidencias, Double r_programadas)
    {
    	return (r_horas - r_incidencias - r_programadas)/(r_horas - r_programadas);
    }
    
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
			r_ejercicio = this.txtEjercicio.getValue();
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		if (RutinasFechas.semanaActual(r_ejercicio)-1 < 1 )
		{
			this.semana = "53";
			this.txtEjercicio.setValue(String.valueOf((new Integer(RutinasFechas.añoActualYYYY())-1)));
			r_ejercicio = this.txtEjercicio.getValue();
		}
		else
		{
			this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio)-1);
		}
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}

    private void ejecutarTimer()
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }
	
    private void vaciarPantalla()
    {
    	centralTop=null;
        centralMiddle=null;
        centralBottom=null;
        topLayout=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	
	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco dashboard envasadora " + new Date());
    		refrescar();
    	}
    }


	private void coberturaStock5L()
	{
		ArrayList<MapeoBarras> vector = null;
		ArrayList<MapeoArticulos> vector_articulos = null;
		MapeoBarras mapeoGrafico = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		
		vector_articulos = cas.obtenerArticulos("01030");
		
		HashMap<String,	Double> hashVentas = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue()),"01030",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashVentasAnterior = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue())-1,"01030",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashStock = cdes.recuperarStockArticulo("01030",this.cmbCalculo.getValue().toString());
		
		vector = new ArrayList<MapeoBarras>();
		
		for (int i = 0; i<vector_articulos.size();i++)
		{
			mapeoGrafico = new MapeoBarras();
			MapeoArticulos mapeo = (MapeoArticulos) vector_articulos.get(i);
			
			mapeoGrafico.setClave(mapeo.getArticulo());
			mapeoGrafico.setValorActual(hashStock.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie1(hashVentas.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie2(hashVentasAnterior.get(mapeo.getArticulo()));
			
			vector.add(mapeoGrafico);
		}
		
		
		
		JFreeChartWrapper wrapper = barras.generarGrafico("Cobertura Stock - Venta Media Semanal 5L", "Referencias 5L", "Stock vs Ventas", "765px", "400px", vector,"Actual", "Venta Media " + txtEjercicio.getValue(), "Venta Media " + String.valueOf(new Integer(txtEjercicio.getValue()).intValue()-1),false);

	    centralBottom.addComponent(wrapper,0,0);
	    
	}
	
	private void coberturaStock2L()
	{
		ArrayList<MapeoBarras> vector = null;
		ArrayList<MapeoArticulos> vector_articulos = null;
		MapeoBarras mapeoGrafico = null;
		
		consultaDashboardEnvasadoraServer cdes = new consultaDashboardEnvasadoraServer(CurrentUser.get());
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		
		vector_articulos = cas.obtenerArticulos("01031");
		
		HashMap<String,	Double> hashVentas = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue()),"01031",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashVentasAnterior = cdes.recuperarVentasArticuloEjercicio(new Integer(this.txtEjercicio.getValue())-1,"01031",this.cmbCalculo.getValue().toString());
		HashMap<String,	Double> hashStock = cdes.recuperarStockArticulo("01031",this.cmbCalculo.getValue().toString());
		
		vector = new ArrayList<MapeoBarras>();
		
		for (int i = 0; i<vector_articulos.size();i++)
		{
			mapeoGrafico = new MapeoBarras();
			MapeoArticulos mapeo = (MapeoArticulos) vector_articulos.get(i);
			
			mapeoGrafico.setClave(mapeo.getArticulo());
			mapeoGrafico.setValorActual(hashStock.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie1(hashVentas.get(mapeo.getArticulo()));
			mapeoGrafico.setValorSerie2(hashVentasAnterior.get(mapeo.getArticulo()));
			
			vector.add(mapeoGrafico);
		}
		
		
		
		JFreeChartWrapper wrapper = barras.generarGrafico("Cobertura Stock - Venta Media Semanal 2L", "Referencias 2L", "Stock vs. Ventas", "765px", "400px", vector, "Actual", "Venta Media " + txtEjercicio.getValue(), "Venta Media " + String.valueOf(new Integer(txtEjercicio.getValue()).intValue()-1),false);

	    centralBottom.addComponent(wrapper,1,0);
	}

    private void pantallaProduccion()
    {
    	pantallaLineasProduccion vt = new pantallaLineasProduccion("BIB", new Integer(this.txtEjercicio.getValue().toString()), this.cmbSemana.getValue().toString(), "produccion semana " + this.cmbSemana.getValue().toString(), this.cmbCalculo.getValue().toString());
    	getUI().addWindow(vt);
    }

	public boolean isPrevision() {
		return prevision;
	}

	public void setPrevision(boolean prevision) {
		
		if (prevision) this.correctorVentasEjercicio=new Double(1.1); else this.correctorVentasEjercicio=new Double(1);
		this.prevision = prevision;
	}
	
    private void pantallaProgramacion()
    {
    	if (this.cmbSemana.getValue()!=null)
    	{
    		pantallaLineasProgramacionesEnvasadora vt = new pantallaLineasProgramacionesEnvasadora(new Integer(this.txtEjercicio.getValue().toString()), this.cmbSemana.getValue().toString(), "programacion semana " + this.cmbSemana.getValue().toString());
    		getUI().addWindow(vt);
    	}
    }

    private void cargarGraficoIncidencias()
    {
	    ArrayList<MapeoBarras> vector = null;
		MapeoBarras mapeoGrafico = null;
		Double max = new Double(0);
		vector = new ArrayList<MapeoBarras>();
		
			mapeoGrafico = new MapeoBarras();
			mapeoGrafico.setClave("reales");
			mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.incReales.0111"));
			vector.add(mapeoGrafico);

			mapeoGrafico = new MapeoBarras();
			mapeoGrafico.setClave("planificadas");
			mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.inc.0111"));
			vector.add(mapeoGrafico);
		
		max = this.mapeo.getHashValores().get("0.incReales.0111") + this.mapeo.getHashValores().get("0.inc.0111");
		JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Incidencias", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.incReales.0111")<=this.mapeo.getHashValores().get("0.inc.0111"));

		centralTop2.addComponent(wrapper,0,1);
    }

    private void cargarGraficoProgramadas()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("reales");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.proReales.0111"));
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificadas");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.pro.0111"));
    	vector.add(mapeoGrafico);
    	
    	max = this.mapeo.getHashValores().get("0.proReales.0111") + this.mapeo.getHashValores().get("0.pro.0111");
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Programadas", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.proReales.0111")<=this.mapeo.getHashValores().get("0.pro.0111"));
    	
    	centralTop2.addComponent(wrapper,1,1);
    }

    private void cargarGraficoProduccion()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("real");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.prod.0111"));
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificada");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.plan.0111"));
    	vector.add(mapeoGrafico);
    	
    	max = this.mapeo.getHashValores().get("0.prod.0111") + this.mapeo.getHashValores().get("0.plan.0111");
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Producción", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.prod.0111")>=this.mapeo.getHashValores().get("0.plan.0111"));
    	
    	centralTop2.addComponent(wrapper,2,1);
    }
    
    private void cargarGraficoBotellasHora()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	Double mediaPlan = new Double(0);
    	Double mediaReal = new Double(0);
    	
		mediaReal = this.mapeo.getHashValores().get("0.prod.0111")/(this.mapeo.getHashValores().get("0.horasReales.0111")-this.mapeo.getHashValores().get("0.proReales.0111"));
		mediaPlan = this.mapeo.getHashValores().get("0.plan.0111")/(this.mapeo.getHashValores().get("0.horas.0111")-this.mapeo.getHashValores().get("0.pro.0111"));
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("real");
    	mapeoGrafico.setValorActual(mediaReal*8);
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificada");
    	mapeoGrafico.setValorActual(mediaPlan*8);
    	vector.add(mapeoGrafico);
    	
    	max = mediaReal*8 + mediaPlan*8;
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Litros / Turno", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, mediaReal>=mediaPlan);
    	
    	centralTop2.addComponent(wrapper,3,1);
    }
    
    private void cargarRelojesOEE()
    {
    	
    	/*
    	 * GENERACION DATOS 5L
    	 */
    	Double valor = null;
    	Double horas = null;
    	Double inc = null;
    	Double maq = null;
//    	Double media = null;
//    	Double mediaInc = null;
    	Double mediaMostrar = null;
    	
    	Panel gaugeGlbBt = null;
    	
    	if (!isPrevision())
    	{
    		valor = this.mapeo.getHashValores().get("0.prod.0111");
    		horas = this.mapeo.getHashValores().get("0.horasReales.0111");
    		inc = this.mapeo.getHashValores().get("0.proReales.0111");
    		maq = this.mapeo.getHashValores().get("0.incReales.0111");
    	}
    	else
    	{
    		valor = this.mapeo.getHashValores().get("0.plan.0111");
    		horas = this.mapeo.getHashValores().get("0.horas.0111");
    		inc = this.mapeo.getHashValores().get("0.pro.0111");
    		maq = this.mapeo.getHashValores().get("0.inc.0111");
    	}
    	
    	if(valor!=null && horas != null && horas.doubleValue()!=0)
    	{
//    		media = valor/horas;
//    		mediaInc = valor/(horas-inc);
    		mediaMostrar = valor/(horas-inc-maq);

    		if (mediaMostrar.doubleValue()!=0)
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				gaugeGlbBt = reloj.generarReloj(  "MEDIA OPERATIVA ",mediaMostrar.intValue(), 500, 1000, 350, 499, 0, 350, 0, 1000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    			else
    				gaugeGlbBt = reloj.generarReloj(  "MEDIA OPERATIVA ",mediaMostrar.intValue(), 250, 350, 125, 250, 0, 125, 0, 350, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);

    			centralMiddle.addComponent(gaugeGlbBt,4,0);	
    		}
    	}
    }
    
    private Panel indiceOEE5L()
    {
    	Double valor = null;
    	Double horas = null;
    	Double inc = null;
    	Double maq = null;

    	Double oeeRdo = null;
    	Label interpretacionOEE = null;
    	Panel gaugeOEE = null;
    	Panel gaugeD = null;
    	Panel gaugeP = null;
    	Panel gaugeC = null;
    	
    	Panel panelOEE = new Panel("Indice OEE");
    	panelOEE.setSizeUndefined();
    	
    	VerticalLayout contenido = new VerticalLayout();
    	
    	if (!isPrevision())
    	{
    		valor = this.mapeo.getHashValores().get("0.prod.0111");
    		horas = this.mapeo.getHashValores().get("0.horasReales.0111");
    		inc = this.mapeo.getHashValores().get("0.incReales.0111");
    		maq = this.mapeo.getHashValores().get("0.proReales.0111");
    	}
    	else
    	{
    		valor = this.mapeo.getHashValores().get("0.plan.0111");
    		horas = this.mapeo.getHashValores().get("0.horas.0111");
    		inc = this.mapeo.getHashValores().get("0.inc.0111");
    		maq = this.mapeo.getHashValores().get("0.pro.0111");
    	}

    	
		disponibilidad = obtenerDisponibilidad(horas, maq,inc);
		
			gaugeD = reloj.generarReloj("DISPONIBILIDAD",((new Double(disponibilidad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerDisp = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaDisponibilidad vt = new pantallaDisponibilidad("Disponibilidad", txtEjercicio.getValue().toString());
					getUI().addWindow(vt);
				}
			};
			gaugeD.addClickListener(listenerDisp);
			centralMiddle.addComponent(gaugeD,1,0);
			
		
		productividad = obtenerProductividad("BIB", valor, horas, maq, inc, isPrevision());
			gaugeP = reloj.generarReloj("PRODUCTIVIDAD",((new Double(productividad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerP = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaProductividad vtP = new pantallaProductividad("Productividad", txtEjercicio.getValue().toString());
					getUI().addWindow(vtP);
				}
			};
			gaugeP.addClickListener(listenerP);
			centralMiddle.addComponent(gaugeP,2,0);

		calidad = obtenerCalidad (valor, this.mapeo.getHashValores().get("0.mer.0111"));
			gaugeC = reloj.generarReloj("CALIDAD",((new Double(calidad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerQ = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaCalidad vtQ = new pantallaCalidad("Calidad", txtEjercicio.getValue().toString());
					getUI().addWindow(vtQ);
				}
			};
			gaugeC.addClickListener(listenerQ);
			centralMiddle.addComponent(gaugeC,3,0);
			
		oeeRdo = disponibilidad * productividad * calidad *100;
		gaugeOEE = reloj.generarReloj("",oeeRdo.intValue(), 75, 100, 60, 74, 0, 59, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
		
		interpretacionOEE = obtenerInterpretacion(oeeRdo);
		
//		Double teep =teep("0102", oeeRdo);
		
		contenido.addComponent(gaugeOEE);
		contenido.addComponent(interpretacionOEE);
		contenido.setComponentAlignment(interpretacionOEE, Alignment.MIDDLE_CENTER);
    	panelOEE.setContent(contenido);
    	
    	return panelOEE;
    }
    
    private Label obtenerInterpretacion(Double r_oee)    
    {
    	Label resultado = new Label();
    	resultado.setWidth("100%");
    	
    	if (r_oee < 65)
    	{
    		resultado.setValue("INADMISIBLE");
    		resultado.addStyleName("oeeMalo");
    	}
    	if (r_oee >= 65 && r_oee < 75)
    	{
    		resultado.setValue("REGULAR");
    		resultado.addStyleName("oeeRegular");
    	}
    	if (r_oee >= 75 && r_oee < 85)
    	{
    		resultado.setValue("ACEPTABLE");
    		resultado.addStyleName("oeeAceptable");
    	}
    	if (r_oee >= 85 && r_oee < 95)
    	{
    		resultado.setValue("BUENA");
    		resultado.addStyleName("oeeBuena");
    	}
    	if (r_oee > 95)
    	{
    		resultado.setValue("EXCELENTE");
    		resultado.addStyleName("oeeExcelente");
    	}
    	
    	return resultado;
    }
    
    private void cargarResumenArticulo(String r_ejer, String r_campo)
    {
    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardBIBServer cds = new consultaDashboardBIBServer(CurrentUser.get());
    	ArrayList<MapeoDetalleArticulo> vector = new ArrayList<MapeoDetalleArticulo>();
    	vector = cds.recuperarDetalleArticulo(r_ejer, r_campo);
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDetalleArticulo> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Articulo", String.class, null);
    	container.addContainerProperty("Descripcion", String.class, null);
    	container.addContainerProperty("Almacen", Integer.class, null);
    	container.addContainerProperty("Stock", Double.class, null);
    	container.addContainerProperty("Pdte. Servir", Double.class, null);
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
			MapeoDetalleArticulo mapeoDetalle = iterator.next();
			file.getItemProperty("Articulo").setValue(mapeoDetalle.getArticulo().trim()); 
			file.getItemProperty("Descripcion").setValue(mapeoDetalle.getDescripcion()); 
			file.getItemProperty("Almacen").setValue(mapeoDetalle.getAlmacen()); 
			file.getItemProperty("Stock").setValue(mapeoDetalle.getStock()); 
			file.getItemProperty("Pdte. Servir").setValue(mapeoDetalle.getPdte()); 
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	gridDatos.setFrozenColumnCount(2);
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
			
			@Override
			public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Articulo") || cellReference.getPropertyId().toString().contains("Descripcion")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
    	gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
				cargarDetalleSemanas(r_ejer, r_campo, it.getItemProperty("Articulo").getValue().toString(), it.getItemProperty("Almacen").getValue().toString(), new Double(it.getItemProperty("Stock").getValue().toString()) );
			}
		});

    	Long totalU = new Long(0) ;
    	Double valor = null;
    	Long totalE = new Long(0) ;
    	Double valorE = null;

    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	
    	for(Object itemId : list)
    	{
    		Item item = indexed.getItem(itemId);
        		
    		valor = (Double) item.getItemProperty("Pdte. Servir").getValue();
    		valorE = (Double) item.getItemProperty("Stock").getValue();
    		if (valor!=null)
    			totalU += valor.longValue();
    		if (valorE!=null)
    			totalE += valorE.longValue();
				
    	}
    	footer.getCell("Descripcion").setText("Totales ");
    	footer.getCell("Stock").setText(RutinasNumericas.formatearDouble(totalE.doubleValue()));
    	footer.getCell("Stock").setStyleName("Rcell-pie");
    	footer.getCell("Pdte. Servir").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    	footer.getCell("Pdte. Servir").setStyleName("Rcell-pie");
            
    	
    	Panel panel = new Panel("Detalle Articulo");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("450px");
    	panel.setWidth("650px");
    	panel.setContent(gridDatos);
    	centralDetalleArticulo.addComponent(panel,0,0);
    	
    }
    
    private void cargarDetalleSemanas(String r_ejer, String r_campo, String r_articulo, String r_almacen, Double r_stock)
    {
    	centralDetalleArticulo.removeComponent(1,0);
    	
    	/*
    	 * inicializo conexiones resto programas
    	 */
    	consultaDashboardBIBServer cds = new consultaDashboardBIBServer(CurrentUser.get());
    	ArrayList<MapeoDetalleArticulo> vector = new ArrayList<MapeoDetalleArticulo>();
    	vector = cds.recuperarDetallePendienteArticulo(r_ejer, r_campo, r_articulo, r_almacen, r_stock);
    	/*
    	 * genero la tabla y proceso todos los datos
    	 */
    	IndexedContainer container =null;
    	Iterator<MapeoDetalleArticulo> iterator = null;
    	Double total = new Double(0);
    	
    	container = new IndexedContainer();
    	iterator = vector.iterator();
    	/*
    	 * Recorro los valores para presentar las columnas de la tabla
    	 */
    	container.addContainerProperty("Articulo", String.class, null);
    	container.addContainerProperty("Descripcion", String.class, null);
    	container.addContainerProperty("Almacen", Integer.class, null);
    	container.addContainerProperty("Semana", Integer.class, null);
    	container.addContainerProperty("Pdte. Servir", Double.class, null);
    	container.addContainerProperty("Resto", Double.class, null);
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
			MapeoDetalleArticulo mapeoDetalle = iterator.next();
			file.getItemProperty("Articulo").setValue(mapeoDetalle.getArticulo().trim()); 
			file.getItemProperty("Descripcion").setValue(mapeoDetalle.getDescripcion()); 
			file.getItemProperty("Almacen").setValue(mapeoDetalle.getAlmacen()); 
			file.getItemProperty("Semana").setValue(mapeoDetalle.getSemana()); 
			file.getItemProperty("Pdte. Servir").setValue(mapeoDetalle.getPdte()); 
			file.getItemProperty("Resto").setValue(mapeoDetalle.getSobraFalta()); 
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	gridDatos.setFrozenColumnCount(2);
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
			
			@Override
			public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Articulo") || cellReference.getPropertyId().toString().contains("Descripcion")) {
    				return "cell-normal";
    			}
    			else
    			{
    				return "Rcell-normal";
    			}
    		}
    	});
//    	gridDatos.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void select(SelectionEvent event) {
//				
//		    	Item it = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
////				cargarDetalleSemanas(it.getItemProperty("Articulo").getValue().toString(), it.getItemProperty("Almacen").getValue().toString(), it.getItemProperty("Stock").getValue().toString() );
//			}
//		});

    	Long totalU = new Long(0) ;
    	Double valor = null;
    	FooterRow footer = gridDatos.appendFooterRow();
        	
    	Indexed indexed = gridDatos.getContainerDataSource();
    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
    	
    	for(Object itemId : list)
    	{
    		Item item = indexed.getItem(itemId);
        		
    		valor = (Double) item.getItemProperty("Pdte. Servir").getValue();
    		if (valor!=null)
    			totalU += valor.longValue();
				
    	}
    	footer.getCell("Descripcion").setText("Totales ");
    	footer.getCell("Pdte. Servir").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
    	footer.getCell("Pdte. Servir").setStyleName("Rcell-pie");
            
    	
    	Panel panel = new Panel("Detalle Pendiente Servir");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("450px");
    	panel.setWidth("650px");
    	panel.setContent(gridDatos);
    	centralDetalleArticulo.addComponent(panel,1,0);
    	
    }
}
