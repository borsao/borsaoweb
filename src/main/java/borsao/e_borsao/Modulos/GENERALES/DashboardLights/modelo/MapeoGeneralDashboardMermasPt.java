package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardMermasPt extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardMermasPt> vectorProduccion = null;
	private ArrayList<MapeoDashboardMermasPt> vectorMermas = null;
	
	public ArrayList<MapeoDashboardMermasPt> getVectorMermas() {
		return vectorMermas;
	}

	public void setVectorMermas(ArrayList<MapeoDashboardMermasPt> vectorMermas) {
		this.vectorMermas = vectorMermas;
	}

	public MapeoGeneralDashboardMermasPt()
	{
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardMermasPt> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardMermasPt> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}
	
}