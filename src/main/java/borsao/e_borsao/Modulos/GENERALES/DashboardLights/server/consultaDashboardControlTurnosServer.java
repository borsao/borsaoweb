package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardBIB;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladoraProduccionAnalizada;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEnvasadora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEmbotelladora;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.server.consultaHorasTrabajadasBIBServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.server.consultaHorasTrabajadasEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server.consultaHorasTrabajadasEmbotelladoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardControlTurnosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDashboardControlTurnosServer instance;
	private String ambitoTemporal = null; 
	private String semana = null; 
	private String ejercicio = null; 
	
	public consultaDashboardControlTurnosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardControlTurnosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardControlTurnosServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardEmbotelladora recuperarValoresEstadisticosEmbotelladora(String r_ejercicio, String r_semana, String r_vista)
	{
		Double valor = null;
		this.semana = r_semana;
		this.ejercicio = r_ejercicio;

		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardEmbotelladora mapeo = null;
		
		hashValores = new HashMap<String, Double>();
		
		this.ambitoTemporal=r_vista;
		if (ambitoTemporal.contentEquals("Anual"))
		{
			r_semana = "0";
			semana = "0";
		}

		consultaHorasTrabajadasEmbotelladoraServer ches = consultaHorasTrabajadasEmbotelladoraServer.getInstance(CurrentUser.get());
		consultaHorasTrabajadasEnvasadoraServer chvs = consultaHorasTrabajadasEnvasadoraServer.getInstance(CurrentUser.get());
		consultaHorasTrabajadasBIBServer chbs = consultaHorasTrabajadasBIBServer.getInstance(CurrentUser.get());
		consultaProduccionDiariaServer cpds = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
		
		ArrayList<MapeoDashboardEmbotelladora> vectorDatosProduccion=null;
		ArrayList<MapeoDashboardEnvasadora> vectorDatosProduccionEnvasadora=null;
		ArrayList<MapeoDashboardBIB> vectorDatosProduccionBib = null;
		vectorDatosProduccionEnvasadora = cpds.recuperarMovimientosProduccionEnvasado(new Integer(r_ejercicio),r_semana,"marca", r_vista);
		vectorDatosProduccion = cpds.recuperarMovimientosProduccionEmbotellado(new Integer(r_ejercicio),r_semana,"1",r_vista);
		vectorDatosProduccionBib = cpds.recuperarMovimientosProduccionBib(new Integer(r_ejercicio),r_semana,"marca", r_vista);
		
		valor = this.recuperaraDatoSolicitado(vectorDatosProduccion, r_semana, "0.prod.0102");
		hashValores.put("0.prod.0102", valor);
		
		valor = this.recuperaraDatoSolicitadoEnvasadora(vectorDatosProduccionEnvasadora, r_semana, "0.prod.0103");
		hashValores.put("0.prod.0103", valor);
		
		valor = this.recuperaraDatoSolicitadoBib(vectorDatosProduccionBib , r_semana, "0.prod.0111");
		hashValores.put("0.prod.0111", valor);
		
		valor = ches.recuperarHorasReales(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
		hashValores.put("0.horasEmbotellado.0102", valor);

		valor = ches.recuperarFueraLineaReales(new Integer(r_ejercicio), new Integer(r_semana));	
		hashValores.put("0.horasEmbotellado.RT", valor);
		
		valor = ches.recuperarHorasEtt(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
		hashValores.put("0.horasEmbotellado.Ett", valor);

		valor = chvs.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), 5, this.ambitoTemporal);
		hashValores.put("0.horasEnvasado.5L", valor);
		
		valor = chvs.recuperarHoras(new Integer(r_ejercicio), new Integer(r_semana), 2, this.ambitoTemporal);
		hashValores.put("0.horasEnvasado.2L", valor);

		valor = chvs.recuperarHorasEtt(new Integer(r_ejercicio), new Integer(r_semana), null, this.ambitoTemporal);
		hashValores.put("0.horasEnvasado.Ett", valor);

		valor = chbs.recuperarHorasReales(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
		hashValores.put("0.horasBib.0111", valor);

		valor = chbs.recuperarHorasEtt(new Integer(r_ejercicio), new Integer(r_semana), this.ambitoTemporal);	
		hashValores.put("0.horasBib.Ett", valor);

		mapeo = new MapeoGeneralDashboardEmbotelladora();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	
	private Double recuperaraDatoSolicitado(ArrayList<MapeoDashboardEmbotelladora> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.plan.0102": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("TT");
				}				
				break;
			}
			case "0.plan.0102PT": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("PT");
				}				
				break;
			}
			case "0.plan.0102ET": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("ET");
				}				
				break;
			}
			case "0.plan.0102RT": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("RT");
				}				
				break;
			}
			case "0.plan.0102SE": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("SE");
				}				
				break;
			}
			
			case "0.prod.0102": // produccion total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==49)
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.0102PT":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PT"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor - mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor - hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor - hash.get(j);
							}
						}
					}

				}
				break;
			}
			case "0.prod.0102ET": //etiquetado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==99 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}

				break;
			}
			case "0.prod.0102SE": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("PS"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.0102": // venta total
			case "1.venta.0102":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEmbotelladora mapeo = (MapeoDashboardEmbotelladora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						valor = valor + mapeo.getTotal();
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private Double recuperaraDatoSolicitadoEnvasadora(ArrayList<MapeoDashboardEnvasadora> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.prod.0103":
			case "0.venta.0103":
			case "1.venta.0103":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					
					if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual")) 
						valor = valor + mapeo.getTotal();					
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
						/*
						 * meses
						 */
						valor = valor + hash.get(r_semana);
				}
				break;
			}
			case "0.venta.01030":
			case "1.venta.01030":
			case "0.prod.01030":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==5)
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.01031":
			case "1.venta.01031":
			case "0.prod.01031":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==2)
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010300":
			case "1.venta.010300":
			case "0.prod.010300":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==5 && color.toUpperCase().contentEquals("TINTO"))
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010301":
			case "1.venta.010301":
			case "0.prod.010301":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==5 && color.toUpperCase().contentEquals("ROSADO"))
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010310":
			case "1.venta.010310":
			case "0.prod.010310":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==2 && color.toUpperCase().contentEquals("TINTO"))
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
			case "0.venta.010311":
			case "1.venta.010311":
			case "0.prod.010311":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardEnvasadora mapeo = (MapeoDashboardEnvasadora) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String color = mapeo.getColor();
					
					if (formato==2 && color.toUpperCase().contentEquals("ROSADO"))
					{
						if (this.ambitoTemporal.contentEquals("Acumulado") || this.ambitoTemporal.contentEquals("Anual"))
							valor = valor + mapeo.getTotal(); 
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
							/*
							 * meses
							 */
							valor = valor + hash.get(r_semana);
					}
				}
				break;
			}
		}
		return valor;
	}
	
	private Double recuperaraDatoSolicitadoBib(ArrayList<MapeoDashboardBIB> r_vector, String r_semana, String r_clave)
	{
		Double valor = new Double(0);
		Integer sem = new Integer(r_semana);
		
		switch (r_clave.trim())
		{
			case "0.plan.0111": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("TT");
				}				
				break;
			}
			case "0.plan.010": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("010");
				}				
				break;
			}
			case "0.plan.011": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("011");
				}				
				break;
			}
			case "0.plan.020": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("020");
				}				
				break;
			}
			case "0.plan.030": // produccion total
			{
				if (r_vector.size()>0)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(0);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					valor=hash.get("030");
				}				
				break;
			}
			case "0.prod.0111": // produccion total
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (formato==49)
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.010":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("010"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.011":  //embotellado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("011"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}
				break;
			}
			case "0.prod.020": //etiquetado
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("020"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}
					}
				}

				break;
			}
			case "0.prod.030": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (formato==49 && tipo.contentEquals("030"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.0111": // venta total
			case "1.venta.0111":
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					
					if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
					else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
					else
					{
						/*
						 * meses
						 */
						valor = valor + mapeo.getTotal();
					}
				}
				break;
			}
			case "0.venta.010": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("010"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.011": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("011"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.020": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("020"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
			case "0.venta.030": //jaulon
			{
				for (int i = 0; i< r_vector.size(); i++)
				{
					MapeoDashboardBIB mapeo = (MapeoDashboardBIB) r_vector.get(i);
					HashMap<String, Double> hash = new HashMap<String, Double>();
					hash = mapeo.getHashValores();
					Integer formato = mapeo.getFormato();
					String tipo = mapeo.getTipo();
					
					if (tipo.contentEquals("030"))
					{
						if (sem==0 || !ambitoTemporal.contentEquals("Semanal")) valor = valor + mapeo.getTotal();
						else if (sem > 0 && sem <=53 ) valor = valor + hash.get(r_semana);
						else
						{
							/*
							 * meses
							 */
							for (int j = 1; j<=53;i++)
							{
								valor = valor + hash.get(j);
							}
						}							
					}
				}
				break;
			}
		}
		return valor;
	}
}
