package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardExistencias extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardExistencias> vectorStock = null;
	
	public MapeoGeneralDashboardExistencias()
	{
	}
 
	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardExistencias> getVectorStock() {
		return vectorStock;
	}

	public void setVectorStock(ArrayList<MapeoDashboardExistencias> vectorStock) {
		this.vectorStock = vectorStock;
	}
}