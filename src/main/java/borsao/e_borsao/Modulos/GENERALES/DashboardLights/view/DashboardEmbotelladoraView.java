package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.HashMap;

import org.vaadin.addon.JFreeChartWrapper;
import org.vaadin.addons.d3Gauge.GaugeStyle;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.ClasesPropias.graficas.reloj;
import borsao.e_borsao.ClasesPropias.graficas.stackedBarras;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEmbotelladoraServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaAnalisisProduccion;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaAnalisisTurnos;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaCalidad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaDisponibilidad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaExistencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaIncidencias;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaProductividad;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora.pantallaTiempoDisponible;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardEmbotelladoraView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Embotelladora";
	private Integer tamañoRelojes = 160;
	private final String titulo = "CUADRO MANDO EMBOTELLADO";
	
	private CssLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    
    private GridLayout centralBottom = null;
    
	public ComboBox cmbSemana = null;
	public ComboBox cmbCalculo = null;
	public ComboBox cmbVista = null;
	public TextField txtEjercicio= null;
	public String semana = null;
	private Button procesar=null;

	private Button opcMenos= null;
	private Button opcMas= null;
	
	private Panel panelResumen =null;
		private GridLayout centralTop2 = null;
	private Panel panelProduccion =null;
		private GridLayout centralTop = null;
	private Panel panelVentas =null;
		private GridLayout centralMiddle = null;
	private Panel panelOEE =null;
		private GridLayout centralMiddle2 = null;
//	private Panel panelCalidad =null;
//	private Panel panelMantenimiento =null;
//	
//	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardEmbotelladora mapeo=null;
	
	private boolean prevision = false;
	private Double correctorVentasEjercicio = null;
	
	private Double disponibilidad = null;
	private Double productividad = null;
	private Double calidad = null;
	private CheckBox prev = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardEmbotelladoraView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.correctorVentasEjercicio = new Double(1);
    	
    	this.cargarPantalla();
    	this.cargarCombo(null);
		
		this.cargarListeners();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
//    	setSpacing(false);
    	
    	
    	this.barAndGridLayout = new CssLayout();
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

//        final Navigator navigator = new Navigator(eBorsao.getCurrent(), this.barAndGridLayout);

		    	this.topLayoutL = new HorizontalLayout();
//		    	this.topLayoutL.addStyleName("v-panelTitulo");
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.cmbVista= new ComboBox("Vista");    	
			    	this.cmbVista.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbVista.setNewItemsAllowed(false);
			    	this.cmbVista.setWidth("150px");
			    	this.cmbVista.setNullSelectionAllowed(false);
			    	this.cmbVista.addItem("Semanal");
			    	this.cmbVista.addItem("Mensual");
//			    	this.cmbVista.addItem("Trimestral");
//			    	this.cmbVista.addItem("Semestral");
			    	this.cmbVista.addItem("Anual");
			    	this.cmbVista.addItem("Acumulado");
//			    	this.cmbVista.addItem("Ultimos 6 Meses");			    	
			    	this.cmbVista.setValue("Semanal");

			    	this.cmbCalculo= new ComboBox("Cálculo");    	
			    	this.cmbCalculo.addStyleName(ValoTheme.COMBOBOX_TINY);
			    	this.cmbCalculo.setNewItemsAllowed(false);
			    	this.cmbCalculo.setWidth("150px");
			    	this.cmbCalculo.setNullSelectionAllowed(false);
			    	this.cmbCalculo.addItem("Litros");
//			    	this.cmbCalculo.addItem("Ultimos 6 Meses");
			    	this.cmbCalculo.addItem("Unidades");
			    	this.cmbCalculo.setValue("Unidades");

			    	this.txtEjercicio=new TextField("Ejercicio");
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.cmbSemana= new ComboBox("Semana");    	
		    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		    		this.cmbSemana.setNewItemsAllowed(false);
		    		this.cmbSemana.setWidth("125px");
		    		this.cmbSemana.setNullSelectionAllowed(false);

		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		    		
		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);
		        	
			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	this.prev = new CheckBox("Prevision?");
		    	this.prev.addStyleName(ValoTheme.CHECKBOX_SMALL);

		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.cmbVista);
		    	this.topLayoutL.addComponent(this.cmbCalculo);
		    	this.topLayoutL.addComponent(this.txtEjercicio);
	    		this.topLayoutL.addComponent(this.opcMenos);
	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.cmbSemana);
	    		this.topLayoutL.addComponent(this.opcMas);
	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.prev);
	    		this.topLayoutL.setComponentAlignment(this.prev,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
//		    	this.topLayoutL.setStyleName("miPanel");
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

//	    	this.topLayoutL.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

		    panelResumen = new Panel("Resumen Semana");
		    panelResumen.setSizeUndefined();
		    panelResumen.setWidth("100%");
		    panelResumen.setVisible(false);
		    panelResumen.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelResumen );

		    	this.centralTop2 = new GridLayout(5,1);
		    	this.centralTop2.setSpacing(true);
		    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	
		    	panelResumen.setContent(centralTop2);

		    panelProduccion = new Panel("Planificación - Producción");
		    panelProduccion.setSizeUndefined();
		    panelProduccion.setWidth("100%");
		    panelProduccion.setVisible(false);
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelProduccion );
		    
		    	this.centralTop = new GridLayout(5,2);
		    	this.centralTop.setSpacing(true);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);
	
		    	panelProduccion.setContent(centralTop);

		    panelVentas = new Panel("Ventas");
		    panelVentas.setSizeUndefined();
		    panelVentas.setWidth("100%");
		    panelVentas.setVisible(false);
		    panelVentas.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelVentas );

		    	this.centralMiddle = new GridLayout(3,2);
		    	this.centralMiddle.setWidth("100%");
		    	this.centralMiddle.setMargin(true);

		    	panelVentas.setContent(centralMiddle);

	    	panelOEE = new Panel("OEE");
	    	panelOEE.setSizeUndefined();
	    	panelOEE.setWidth("100%");
	    	panelOEE.setVisible(false);
	    	panelOEE.addStyleName("showpointer");
	    	new PanelCaptionBarToggler<Panel>( panelOEE );
		    	
		    	this.centralMiddle2 = new GridLayout(5,1);
		    	this.centralMiddle2.setWidth("100%");
		    	this.centralMiddle2.setMargin(true);
		    	
		    	panelOEE.setContent(centralMiddle2);
		    	

	    	this.centralBottom = new GridLayout(2,1);
	    	this.centralBottom.setWidth("100%");
	    	this.centralBottom.setMargin(true);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(panelResumen);
    	this.barAndGridLayout.addComponent(panelProduccion);
    	this.barAndGridLayout.addComponent(panelOEE);
//    	this.barAndGridLayout.addComponent(panelVentas);
    	
    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
        
    }

    private void procesar()
    {
		eliminarDashBoards();
		
		if (cmbVista.getValue()==null || (!cmbVista.getValue().toString().contentEquals("Anual") && cmbSemana.getValue()==null) || txtEjercicio.getValue()==null 
				|| cmbVista.getValue().toString().length()==0 || (!cmbVista.getValue().toString().contentEquals("Anual") && cmbSemana.getValue().toString().length()==0) || txtEjercicio.getValue().length()==0 )
		{
			Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
		}
		else
		{
			cargarDashboards();
		}
    }
    
    private void cargarListeners() 
    {
    	this.prev.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				setPrevision(prev.getValue());
			}
		});

    	this.cmbSemana.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
		    	if (cmbSemana.getValue()!=null)
				semana = cmbSemana.getValue().toString();
			}
		});

    	this.cmbVista.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
		    	cargarCombo(null);
		    	cmbSemana.setValue(semana);

			}
		});
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));

			}
		});


    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});
    }

    public void eliminarDashBoards()
    {
    	centralTop.removeAllComponents();
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	
    }
    public void cargarDashboards() 
    {
    	String sem =null;
    	String ejer =null;
    	String vista=null;
    	String campo =null;
    	
    	if (this.txtEjercicio.getValue()!=null) ejer = this.txtEjercicio.getValue();
    	if (this.cmbSemana.getValue()!=null) sem = this.cmbSemana.getValue().toString();
    	if (this.cmbVista.getValue()!=null) vista = this.cmbVista.getValue().toString();
    	if (this.cmbCalculo.getValue()!=null) campo = this.cmbCalculo.getValue().toString();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		case "Trimestral":
    		case "Mensual":
    		case "Semestral":
    		{
    			this.mapeo = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;
    		}
    		case "Acumulado":
    		{
    			this.mapeo = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista, campo);
    			break;   			
    		}
    		case "Anual":
    		{
    			this.mapeo = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, "0", vista,campo);
    			break;
    		}
    		default:
    		{
    			this.mapeo = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos(ejer, sem, vista,campo);
    			cargarMeses(ejer, sem);
    			break;
    		}
    	}
    	/*
    	 * cargar panel resumen datos
    	 */
    	cargarResumenSemanal();
    	cargarResumenProduccion();
    	cargarResumenOEE();
//    	cargarResumenVentas(vista,ejer, sem);
    	
    	
    	/*
    	 * Cargamos grid registros produccion
    	 */
//    	cargarRegistros(ejer);
    	
    	
    }
    
    private void cargarResumenVentas(String vista, String ejer, String sem)
    {
    	this.cargarVentas();
    	
    	switch (vista)
    	{
    		case "Semanal":
    		{
    			cargarSemanal(ejer, sem);
    			break;
    		}
    		case "Anual":
    		{
    			cargarAnual(ejer);
    			break;
    		}
    	}
    	panelVentas.setVisible(true);
    	panelVentas.getContent().setVisible(false);

    }
    
    private void cargarResumenProduccion()
    {
    	
    	cargarResumenHorasPlanificadas();
    	cargarResumenPLanificacion();
    	
    	if (!isPrevision())
    	{
    		cargarResumenHoras();
    		cargarResumen();
    	}
    	
    	/*
    	 * cargar relojes botellas hora planificadas vs botellas hora reales
    	 */
    	/*
    	 * cargar graficos barras enfrentando paros programados vs paros reales
    	 */
    	
    	this.cargarGraficoIncidencias();
    	this.cargarGraficoProgramadas();
    	this.cargarGraficoProduccion();
    	this.cargarGraficoBotellasHora();
    	
    	this.panelProduccion.setVisible(true);
    	this.panelProduccion.getContent().setVisible(false);
    }

    private void cargarResumenOEE()
    {
    	cargarRelojes();
    	centralMiddle2.addComponent(indiceOEE5L(),0,0);
    	
    	this.panelOEE.setVisible(true);
    	this.panelOEE.getContent().setVisible(false);
    }
    private void cargarRelojes()
    {
    	
    	/*
    	 * GENERACION DATOS 5L
    	 */
    	Double valor = null;
    	Double horas = null;
    	Double inc = null;
    	Double maq = null;
//    	Double media = null;
//    	Double mediaInc = null;
    	Double mediaMostrar = null;
    	
    	Panel gaugeGlbBt = null;
    	
    	if (!isPrevision())
    	{
    		valor = this.mapeo.getHashValores().get("0.prod.0102");
    		horas = this.mapeo.getHashValores().get("0.horasReales.0102");
    		inc = this.mapeo.getHashValores().get("0.proReales.0102");
    		maq = this.mapeo.getHashValores().get("0.incReales.0102");
    	}
    	else
    	{
    		valor = this.mapeo.getHashValores().get("0.plan.0102");
    		horas = this.mapeo.getHashValores().get("0.horas.0102");
    		inc = this.mapeo.getHashValores().get("0.pro.0102");
    		maq = this.mapeo.getHashValores().get("0.inc.0102");
    	}
    	
    	if(valor!=null && horas != null && horas.doubleValue()!=0)
    	{
//    		media = valor/horas;
//    		mediaInc = valor/(horas-inc);
    		mediaMostrar = valor/(horas-inc-maq);

    		if (mediaMostrar.doubleValue()!=0)
    		{
    			if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
    				gaugeGlbBt = reloj.generarReloj(  "MEDIA OPERATIVA ",mediaMostrar.intValue(), 4875, 6000, 2625, 4874, 0, 2624, 0, 6000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
    			else
    				gaugeGlbBt = reloj.generarReloj(  "MEDIA OPERATIVA ",mediaMostrar.intValue(), 6500, 8000, 3500, 6499, 0, 3499, 0, 8000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);

    			centralMiddle2.addComponent(gaugeGlbBt,4,0);	
    		}
    	}
    }
    
    private void cargarSemanal(String r_ejercicio, String r_semana)
    {
    	this.evolucionVentas();
    	this.evolucionVentasAcumuladas();
//    	this.coberturaStock();
//    	this.coberturaStock2L();
    }
    
    private void cargarAnual(String r_ejercicio)
    {
    	this.evolucionVentas();
    	this.tendenciaAnualComparativaBT();
    	this.evolucionVentasAcumuladas();
    }
    
    private void cargarMeses(String r_ejercicio, String r_semana)
    {
    }
    
    private void cargarResumenSemanal()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "120px";
//    	String anchoPanel = "300px";

    	Label lblTurnos = null;
    	Button lblValorTurnos = null;
    	Label lblProduccionTotal = null;
    	Label lblProduccionBT= null;
    	Label lblProduccionSE= null;
    	Label lblProduccionET= null;
    	
    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccionBT= null;
    	Label lblValorProduccionSE= null;
    	Label lblValorProduccionET= null;

    	Label lblHoras = null;
    	Label lblHoras1 = null;
    	Label lblHoras2 = null;
    	Label lblHoras3 = null;
    	Label lblValorHoras= null;
    	Label lblBT= null;
    	Label lblSE= null;
    	Label lblET= null;
    	Label lblETH = null;
    	
    	Button lblValorBT= null;
    	Button lblValorSE= null;
    	Button lblValorET= null;
    	Button lblValorETH = null;

    	Label lblHorasBT= null;
    	Label lblHorasSE= null;
    	Label lblHorasET= null;
    	Label lblHorasETH = null;
    	
    	Label lblMediaBT= null;
    	Label lblMediaSE= null;
    	Label lblMediaET= null;
    	Label lblMediaETH = null;
    	
    	Label lblStockTotal = null;
    	Label lblStockPT= null;
    	Label lblStockSE= null;

    	Label lblValorStockTotal = null;
    	Button lblValorStockPT= null;
    	Button lblValorStockSE= null;


    	Double valor = null;
    	Double horas = null;
    	Double media = null;
    	Panel gaugeBt = null;
    	
    	Panel panel = new Panel("RESUMEN PRODUCCION");
    	
    	if (isPrevision())
    	{
    		panel.setCaption(" PRODUCCION PLANIFICADA ");
    	}
    	else
    	{
    		panel.setCaption(" RESUMEN PRODUCCION ");
    	}
    	panel.setSizeUndefined();

	    	// Create the content
	    	VerticalLayout content = new VerticalLayout();
	
	    	HorizontalLayout lin = new HorizontalLayout();
	    	lin.setSpacing(true);
		    	lblTurnos = new Label();
		    	lblTurnos.setValue("Turnos: ");
		    	lblTurnos.setWidth(anchoLabelTitulo);
		    	lblTurnos.addStyleName("lblTitulo ");
		
		    	lblValorTurnos = new Button();
		    	if (isPrevision())
		    		lblValorTurnos.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0102")/8));
		    	else
		    		lblValorTurnos.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0102")/8));

		    	lblValorTurnos.setWidth(anchoLabelDatos);
		    	lblValorTurnos.addStyleName(ValoTheme.BUTTON_TINY);
		    	lblValorTurnos.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		    	lblValorTurnos.addStyleName("lblDatos");
		    	lblValorTurnos.addClickListener(new ClickListener() {
					
					@Override
					public void buttonClick(ClickEvent event) {
						String valorSemana = null;
						if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
							valorSemana = cmbSemana.getValue().toString();
						else
							valorSemana = "";
						
						pantallaAnalisisTurnos vtT = new pantallaAnalisisTurnos(txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString());
						getUI().addWindow(vtT);
					}
				});

	
		    	lin.addComponent(lblTurnos);
		    	lin.addComponent(lblValorTurnos);
	
	    	HorizontalLayout lin1= new HorizontalLayout();
	    	lin1.setSpacing(true);
	    	HorizontalLayout lin2 = new HorizontalLayout();
	    	lin2.setSpacing(true);
	    	HorizontalLayout lin3 = new HorizontalLayout();
	    	lin3.setSpacing(true);
	    	HorizontalLayout lin4 = new HorizontalLayout();
	    	lin4.setSpacing(true);
		    	
		    	lblProduccionTotal = new Label();
		    	lblProduccionTotal.setValue("Producción: ");
		    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
		    	lblProduccionTotal.addStyleName("lblTitulo");
	
		    	lblValorProduccionTotal = new Label();
		    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102")));    		
		    	}
		    	else
		    	{
		    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102")));
		    	}
		    	lblValorProduccionTotal.addStyleName("lblDatos");
		    	
		    	lin1.addComponent(lblProduccionTotal);
		    	lin1.addComponent(lblValorProduccionTotal);
		    	
			    	lblProduccionBT = new Label();
			    	lblProduccionBT.setValue(" Bt: ");
			    	lblProduccionBT.setWidth(anchoLabelTitulo);
			    	lblProduccionBT.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionBT = new Label();
			    	lblValorProduccionBT.setWidth(anchoLabelDatos);
			    	if (isPrevision())
			    	{
			    		lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102PT")));
			    	}
			    	else
			    	{
			    		lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102PT")));
			    	}
			    	lblValorProduccionBT.addStyleName("lblDatos");
			    	
			    	lin2.addComponent(lblProduccionBT);
			    	lin2.addComponent(lblValorProduccionBT);
			    	
			    	lblProduccionSE = new Label();
			    	lblProduccionSE.setValue("A Jaulon: ");
			    	lblProduccionSE.setWidth(anchoLabelTitulo);
			    	lblProduccionSE.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionSE = new Label();
			    	lblValorProduccionSE.setWidth(anchoLabelDatos);
			    	if (isPrevision())
			    	{
			    		lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102SE")));
			    	}
			    	else
			    	{
			    		lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102SE")));
			    	}
			    	lblValorProduccionSE.addStyleName("lblDatos");
			    	
			    	lblProduccionET = new Label();
			    	lblProduccionET.setValue("Etiquetado: ");
			    	lblProduccionET.setWidth(anchoLabelTitulo);
			    	lblProduccionET.addStyleName("lblTituloDerecha");
			
			    	lblValorProduccionET= new Label();
			    	lblValorProduccionET.setWidth(anchoLabelDatos);
			    	if (isPrevision())
			    	{
			    		lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102ET")));
			    	}
			    	else
			    	{
			    		lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102ET")));
			    	}
			    	lblValorProduccionET.addStyleName("lblDatos");
			    	
			    	lin3.addComponent(lblProduccionSE);
			    	lin3.addComponent(lblValorProduccionSE);
			    	lin4.addComponent(lblProduccionET);
			    	lin4.addComponent(lblValorProduccionET);
			
	    							    				    	
		    	content.addComponent(lin);
		    	content.addComponent(lin1);
		    	content.addComponent(lin2);
		    	content.addComponent(lin3);
		    	content.addComponent(lin4);
		    	
	    	content.setSizeUndefined(); // Shrink to fit
	    	content.setSpacing(false);
	    	content.setMargin(true);
	    	panel.setContent(content);
	    	
    	
    	centralTop2.addComponent(panel,0,0);
    	
    		/*
    		 * RESUMEN MEDIAS
    		 */
	    	if (!isPrevision())
	    	{
	
		    	Panel panelm = new Panel("RESUMEN MEDIAS PROD");
		    	panelm.setSizeUndefined();
		
			    	// Create the content
			    	VerticalLayout contentm = new VerticalLayout();
			
			    	HorizontalLayout linM = new HorizontalLayout();
			    	linM.setSpacing(true);
				    	lblHoras= new Label();
				    	lblHoras.setValue("TIPO ");
				    	lblHoras.setWidth(anchoLabelTitulo);
				    	lblHoras.addStyleName("lblTituloDerecha");
				
				    	lblHoras1 = new Label();
				    	lblHoras1.setValue("Botellas");
				    	lblHoras1.setWidth(anchoLabelTitulo);
				    	lblHoras1.addStyleName("lblTituloDerecha");

				    	lblHoras2 = new Label();
				    	lblHoras2.setValue("Horas");
				    	lblHoras2.setWidth(anchoLabelTitulo);
				    	lblHoras2.addStyleName("lblTituloDerecha");

				    	lblHoras3 = new Label();
				    	lblHoras3.setValue("Bt./Hora");
				    	lblHoras3.setWidth(anchoLabelTitulo);
				    	lblHoras3.addStyleName("lblTituloDerecha");

				    	linM.addComponent(lblHoras);
				    	linM.addComponent(lblHoras1);
				    	linM.addComponent(lblHoras2);
				    	linM.addComponent(lblHoras3);
			
			    	HorizontalLayout linM1= new HorizontalLayout();
			    	lin1.setSpacing(true);
			    	HorizontalLayout linM2 = new HorizontalLayout();
			    	lin2.setSpacing(true);
			    	HorizontalLayout linM3 = new HorizontalLayout();
			    	lin3.setSpacing(true);
			    	HorizontalLayout linM4 = new HorizontalLayout();
			    	lin4.setSpacing(true);
				    	
					    	lblBT = new Label();
					    	lblBT.setValue(" Directas ");
					    	lblBT.setWidth(anchoLabelTitulo);
					    	lblBT.addStyleName("lblTituloDerecha");
					
					    	lblValorBT = new Button();
					    	lblValorBT.setWidth(anchoLabelDatos);
					    	lblValorBT.addStyleName(ValoTheme.BUTTON_TINY);
					    	lblValorBT.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					    	lblValorBT.addStyleName("lblDatos");
					    	lblValorBT.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102PT")));

					    	lblValorBT.addClickListener(new ClickListener() 
					    	{
								
								@Override
								public void buttonClick(ClickEvent event) {
									String valorSemana = null;
									if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
										valorSemana = cmbSemana.getValue().toString();
									else
										valorSemana = "";
									
									pantallaAnalisisProduccion vtT = new pantallaAnalisisProduccion("Directas", txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString(),cmbCalculo.getValue().toString());
									getUI().addWindow(vtT);
								}
							});

					    	lblHorasBT = new Label();
					    	lblHorasBT.setWidth(anchoLabelDatos);
					    	lblHorasBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasAutomatico.0102")));
					    	lblHorasBT.addStyleName("lblDatos");
		
					    	lblMediaBT = new Label();
					    	lblMediaBT.setWidth(anchoLabelDatos);
					    	if (this.mapeo.getHashValores().get("0.horasAutomatico.0102")!=0)
					    		lblMediaBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102PT")/this.mapeo.getHashValores().get("0.horasAutomatico.0102")));
					    	else
					    		lblMediaBT.setValue(RutinasNumericas.formatearDouble(new Double(0)));
					    	
					    	lblMediaBT.addStyleName("lblDatos");

					    	lblSE = new Label();
					    	lblSE.setValue("A Jaulon ");
					    	lblSE.setWidth(anchoLabelTitulo);
					    	lblSE.addStyleName("lblTituloDerecha");
					
					    	lblValorSE = new Button();
					    	lblValorSE.setWidth(anchoLabelDatos);
					    	lblValorSE.addStyleName(ValoTheme.BUTTON_TINY);
					    	lblValorSE.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					    	lblValorSE.addStyleName("lblDatos");
					    	lblValorSE.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102SE")));

					    	lblValorSE.addClickListener(new ClickListener() 
					    	{
								
								@Override
								public void buttonClick(ClickEvent event) {
									String valorSemana = null;
									if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
										valorSemana = cmbSemana.getValue().toString();
									else
										valorSemana = "";
									
									pantallaAnalisisProduccion vtT = new pantallaAnalisisProduccion("Jaulon", txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString(), cmbCalculo.getValue().toString());
									getUI().addWindow(vtT);
								}
							});

					    	lblHorasSE = new Label();
					    	lblHorasSE.setWidth(anchoLabelDatos);
					    	lblHorasSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasJaulon.0102")));
					    	lblHorasSE.addStyleName("lblDatos");

					    	lblMediaSE = new Label();
					    	lblMediaSE.setWidth(anchoLabelDatos);
					    	if (this.mapeo.getHashValores().get("0.horasJaulon.0102")!=0)
					    		lblMediaSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102SE")/this.mapeo.getHashValores().get("0.horasJaulon.0102")));
					    	else
					    		lblMediaSE.setValue(RutinasNumericas.formatearDouble(new Double(0)));
					    	
					    	lblMediaSE.addStyleName("lblDatos");

					    	lblET = new Label();
					    	lblET.setValue("Etiquetadas ");
					    	lblET.setWidth(anchoLabelTitulo);
					    	lblET.addStyleName("lblTituloDerecha");
					
					    	lblValorET= new Button();
					    	lblValorET.setWidth(anchoLabelDatos);
					    	lblValorET.addStyleName(ValoTheme.BUTTON_TINY);
					    	lblValorET.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					    	lblValorET.addStyleName("lblDatos");
					    	lblValorET.setCaption(RutinasNumericas.formatearDouble(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102ET")-this.mapeo.getHashValores().get("0.prod.0102CajaHorizontal"))));

					    	lblValorET.addClickListener(new ClickListener() 
					    	{
								
								@Override
								public void buttonClick(ClickEvent event) {
									String valorSemana = null;
									if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
										valorSemana = cmbSemana.getValue().toString();
									else
										valorSemana = "";
									
									pantallaAnalisisProduccion vtT = new pantallaAnalisisProduccion("Etiquetado", txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString(), cmbCalculo.getValue().toString());
									getUI().addWindow(vtT);
								}
							});

					    	lblMediaET = new Label();
					    	lblMediaET.setWidth(anchoLabelDatos);
					    	if (this.mapeo.getHashValores().get("0.horasEtiquetado.0102")!=0)
					    		lblMediaET.setValue(RutinasNumericas.formatearDouble((this.mapeo.getHashValores().get("0.prod.0102ET")-this.mapeo.getHashValores().get("0.prod.0102CajaHorizontal"))/this.mapeo.getHashValores().get("0.horasEtiquetado.0102")));
					    	else
					    		lblMediaET.setValue(RutinasNumericas.formatearDouble(new Double(0)));
					    	
					    	lblMediaET.addStyleName("lblDatos");
					    	
					    	lblHorasET = new Label();
					    	lblHorasET.setWidth(anchoLabelDatos);
					    	lblHorasET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasEtiquetado.0102")));
					    	lblHorasET.addStyleName("lblDatos");

					    	lblETH = new Label();
					    	lblETH.setValue("Et. Horizontal ");
					    	lblETH.setWidth(anchoLabelTitulo);    	
					    	lblETH.addStyleName("lblTituloDerecha");
				
					    	lblValorETH = new Button();
					    	lblValorETH.setWidth(anchoLabelDatos);
					    	lblValorETH.setWidth(anchoLabelDatos);
					    	lblValorETH.addStyleName(ValoTheme.BUTTON_TINY);
					    	lblValorETH.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					    	lblValorETH.addStyleName("lblDatos");
					    	lblValorETH.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102CajaHorizontal")));

					    	lblValorETH.addClickListener(new ClickListener() 
					    	{
								
								@Override
								public void buttonClick(ClickEvent event) {
									String valorSemana = null;
									if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
										valorSemana = cmbSemana.getValue().toString();
									else
										valorSemana = "";
									
									pantallaAnalisisProduccion vtT = new pantallaAnalisisProduccion("Horizontal", txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString(), cmbCalculo.getValue().toString());
									getUI().addWindow(vtT);
								}
							});
					    	
					    	lblHorasETH = new Label();
					    	lblHorasETH.setWidth(anchoLabelDatos);
					    	lblHorasETH.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasHorizontal.0102")));
					    	lblHorasETH.addStyleName("lblDatos");
					    	
					    	lblMediaETH = new Label();
					    	lblMediaETH.setWidth(anchoLabelDatos);
					    	if (this.mapeo.getHashValores().get("0.horasHorizontal.0102")!=0)
					    		lblMediaETH.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102CajaHorizontal")/this.mapeo.getHashValores().get("0.horasHorizontal.0102")));
					    	else
					    		lblMediaETH.setValue(RutinasNumericas.formatearDouble(new Double(0)));
					    		
					    	lblMediaETH.addStyleName("lblDatos");
					    	
					    	linM1.addComponent(lblBT);
					    	linM1.addComponent(lblValorBT);
					    	linM1.addComponent(lblHorasBT);
					    	linM1.addComponent(lblMediaBT);
					    	linM2.addComponent(lblSE);
					    	linM2.addComponent(lblValorSE);
					    	linM2.addComponent(lblHorasSE);
					    	linM2.addComponent(lblMediaSE);
					    	linM3.addComponent(lblET);
					    	linM3.addComponent(lblValorET);
					    	linM3.addComponent(lblHorasET);
					    	linM3.addComponent(lblMediaET);
					    	linM4.addComponent(lblETH);
					    	linM4.addComponent(lblValorETH);
					    	linM4.addComponent(lblHorasETH);
					    	linM4.addComponent(lblMediaETH);
			    							    				    	
				    	contentm.addComponent(linM);
				    	contentm.addComponent(linM1);
				    	contentm.addComponent(linM2);
				    	contentm.addComponent(linM3);
				    	contentm.addComponent(linM4);
				    	
			    	contentm.setSizeUndefined(); // Shrink to fit
			    	contentm.setSpacing(false);
			    	contentm.setMargin(true);
			    	panelm.setContent(contentm);
		    	
		    	centralTop2.addComponent(panelm,1,0);
	    	}
    		/*
    		 * RESUMEN VENTAS
    		 */

    		String traerVentas = "0";
    		Panel panel1 = new Panel("RESUMEN VENTAS");
	    
	    	if (isPrevision())
	    	{
	    		traerVentas="1";
	    		panel1.setCaption("RESUMEN VENTAS EJ. ANTERIOR" );
	    	}
	    	
	    	panel1.setSizeUndefined(); // Shrink to fit content

	    	// Create the content
	    	VerticalLayout content1 = new VerticalLayout();

	    	HorizontalLayout lin8 = new HorizontalLayout();
	    	lin8.setSpacing(true);
	    	
	    	Label lblVentasTotal = new Label();
	    	lblVentasTotal.setValue("Total: ");
	    	lblVentasTotal.setWidth(anchoLabelTitulo);    	
	    	lblVentasTotal.addStyleName("lblTitulo");

	    	Button lblValorVentasTotal = new Button();
	    	
	    	lblValorVentasTotal.setWidth(anchoLabelDatos);
	    	lblValorVentasTotal.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorVentasTotal.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorVentasTotal.addStyleName("lblDatos");
	    	lblValorVentasTotal.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.0102")*correctorVentasEjercicio));

	    	lblValorVentasTotal.addClickListener(new ClickListener() 
	    	{
				
				@Override
				public void buttonClick(ClickEvent event) {
					String valorSemana = null;
					if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0)
						valorSemana = cmbSemana.getValue().toString();
					else
						valorSemana = "";
					
					pantallaAnalisisProduccion vtT = new pantallaAnalisisProduccion(txtEjercicio.getValue().toString(), valorSemana, cmbVista.getValue().toString(),cmbCalculo.getValue().toString());
					getUI().addWindow(vtT);
				}
			});

	    	
	    	lin8.addComponent(lblVentasTotal);
	    	lin8.addComponent(lblValorVentasTotal);
	    	
	    	content1.addComponent(lin8);
	    	
	    	content1.setSizeUndefined(); // Shrink to fit
	    	content1.setSpacing(false);
	    	content1.setMargin(true);
	    	panel1.setContent(content1);
	    	
	    	centralTop2.addComponent(panel1,2,0);
	    	
	    	
	    	/*
	    	 * RESUMEN STOCK
	    	 */
	    	
	    	traerVentas="0";
	    	

	    	Panel panel2 = new Panel("RESUMEN STOCK");
	    	panel2.setSizeUndefined();
//	    	panel2.setWidth(anchoPanel);
	    	
	    	VerticalLayout content2 = new VerticalLayout();
	    	
	    	HorizontalLayout lin5 = new HorizontalLayout();
	    	lin5.setSpacing(true);
	    	HorizontalLayout lin6 = new HorizontalLayout();
	    	lin6.setSpacing(true);
	    	HorizontalLayout lin7 = new HorizontalLayout();
	    	lin7.setSpacing(true);
	    	
		    	lblStockTotal = new Label();
		    	lblStockTotal.setValue("Total Stock: ");
		    	lblStockTotal.setWidth(anchoLabelTitulo);    	
		    	lblStockTotal.addStyleName("lblTitulo");
	
		    	lblValorStockTotal = new Label();
		    	lblValorStockTotal.setWidth(anchoLabelDatos);
		    	if (!isPrevision())
		    	{
		    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102")));
		    	}
		    	else
		    	{
		    		lblValorStockTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102")+this.mapeo.getHashValores().get("0.plan.0102")-this.mapeo.getHashValores().get(traerVentas + ".venta.0102")*this.correctorVentasEjercicio));
		    	}
		    	lblValorStockTotal.addStyleName("lblDatos");
		    	
		    	lin5.addComponent(lblStockTotal);
		    	lin5.addComponent(lblValorStockTotal);
		    	
			    	lblStockPT = new Label();
			    	lblStockPT.setValue("PT:");
			    	lblStockPT.setWidth(anchoLabelTitulo);
			    	lblStockPT.addStyleName("lblTitulo");
			
			    	lblValorStockPT = new Button();
			    	lblValorStockPT.setWidth(anchoLabelDatos);
			    	if (!isPrevision())
			    	{
			    		lblValorStockPT.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102PT")));
			    	}
			    	else
			    	{
			    		lblValorStockPT.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102PT")+this.mapeo.getHashValores().get("0.plan.0102PT")+this.mapeo.getHashValores().get("0.plan.0102ET")-this.mapeo.getHashValores().get("1.venta.0102")*this.correctorVentasEjercicio));
			    	}
			    	lblValorStockPT.addStyleName(ValoTheme.BUTTON_TINY);
			    	lblValorStockPT.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorStockPT.addStyleName("lblDatos");
			    	lblValorStockPT.addClickListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
							pantallaExistencias vtE = new pantallaExistencias("Existencias", txtEjercicio.getValue(), "PT");
							getUI().addWindow(vtE);
						}
					});

			    	lin6.addComponent(lblStockPT);
			    	lin6.addComponent(lblValorStockPT);
				    	
			    	lblStockSE = new Label();
			    	lblStockSE.setValue("SE:");
			    	lblStockSE.setWidth(anchoLabelTitulo);
			    	lblStockSE.addStyleName("lblTitulo");
			
			    	lblValorStockSE = new Button();
			    	lblValorStockSE.setWidth(anchoLabelDatos);
			    	if (!isPrevision())
			    	{
			    		lblValorStockSE.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102SE")));
			    	}
			    	else
			    	{
			    		if (this.mapeo.getHashValores().get("0.plan.0102SE")==null)
			    		{
			    			lblValorStockSE.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102SE")));			    			
			    		}
			    		else
			    		{
			    			lblValorStockSE.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.stock.0102SE")+this.mapeo.getHashValores().get("0.plan.0102SE")));
			    		}
			    	}
			    	lblValorStockSE.addStyleName(ValoTheme.BUTTON_TINY);
			    	lblValorStockSE.addStyleName(ValoTheme.BUTTON_BORDERLESS);
			    	lblValorStockSE.addStyleName("lblDatos");
			    	lblValorStockSE.addClickListener(new ClickListener() {
						
						@Override
						public void buttonClick(ClickEvent event) {
							pantallaExistencias vtE = new pantallaExistencias("Existencias", txtEjercicio.getValue(), "PS");
							getUI().addWindow(vtE);
						}
					});
			    	lin7.addComponent(lblStockSE);
			    	lin7.addComponent(lblValorStockSE);
						    	
		    	content2.addComponent(lin5);
		    	content2.addComponent(lin6);
		    	content2.addComponent(lin7);
		    	
		    	content2.setSizeUndefined(); // Shrink to fit
		    	content2.setSpacing(false);
		    	content2.setMargin(true);
		    	panel2.setContent(content2);
	    	centralTop2.addComponent(panel2,3,0);

	    	/*
	    	 * media bruta
	    	 */
	    	
	    	if (isPrevision())
	    	{
	    		valor = this.mapeo.getHashValores().get("0.plan.0102");
	    		horas = this.mapeo.getHashValores().get("0.horas.0102");
	    	}
	    	else
	    	{
	    		valor = this.mapeo.getHashValores().get("0.prod.0102");
	    		horas = this.mapeo.getHashValores().get("0.horasReales.0102");
	    	}
	    	
	    	if(valor!=null && horas != null && horas.doubleValue()!=0)
	    	{
	    		media = valor/horas;
	
	    		if (this.cmbCalculo.getValue().toString().contentEquals("Litros"))
	    			gaugeBt = reloj.generarReloj(  "MEDIA BRUTA",media.intValue(), 4875, 6000, 2625, 4874, 0, 2624, 0, 6000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
	    		else
	    			gaugeBt = reloj.generarReloj(  "MEDIA BRUTA ",media.intValue(), 6500, 8000, 3500, 6499, 0, 3499, 0, 8000, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
	    		
	    		centralTop2.addComponent(gaugeBt,4,0);
	    	}
	    	
    	this.panelResumen.setVisible(true);
    	this.panelResumen.getContent().setVisible(true);

    }
    private void cargarResumen()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblProduccionTotal = null;
    	Label lblProduccionBT= null;
    	Label lblProduccionSE= null;
    	Label lblProduccionET= null;
    	Label lblProduccionMermas= null;
    	Label lblCiclo= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccionBT= null;
    	Label lblValorProduccionSE= null;
    	Label lblValorProduccionET= null;
    	Button lblValorProduccionMermas= null;
    	Label lblValorCiclo= null;
    	
    	Panel panel = new Panel("PRODUCCION REAL");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PRODUCCION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);
    	if (isPrevision())
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102")));    		
    	}
    	else
    	{
    		lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102")));
    	}
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblProduccionTotal);
    	lin.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccionBT = new Label();
	    	lblProduccionBT.setValue(" Bt: ");
	    	lblProduccionBT.setWidth(anchoLabelTitulo);
	    	lblProduccionBT.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionBT = new Label();
	    	lblValorProduccionBT.setWidth(anchoLabelDatos);
	    	if (isPrevision())
	    	{
	    		lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102PT")));
	    	}
	    	else
	    	{
	    		lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102PT")));
	    	}
	    	lblValorProduccionBT.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblProduccionBT);
	    	lin2.addComponent(lblValorProduccionBT);
	    	
		    	lblProduccionSE = new Label();
		    	lblProduccionSE.setValue("A Jaulon: ");
		    	lblProduccionSE.setWidth(anchoLabelTitulo);
		    	lblProduccionSE.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionSE = new Label();
		    	lblValorProduccionSE.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102SE")));
		    	}
		    	else
		    	{
		    		lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102SE")));
		    	}
		    	lblValorProduccionSE.addStyleName("lblDatos");
		    	
		    	lblProduccionET = new Label();
		    	lblProduccionET.setValue("Etiquetado: ");
		    	lblProduccionET.setWidth(anchoLabelTitulo);
		    	lblProduccionET.addStyleName("lblTituloDerecha");
		
		    	lblValorProduccionET= new Label();
		    	lblValorProduccionET.setWidth(anchoLabelDatos);
		    	if (isPrevision())
		    	{
		    		lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102ET")));
		    	}
		    	else
		    	{
		    		lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod.0102ET")));
		    	}
		    	lblValorProduccionET.addStyleName("lblDatos");
		    	
		    	lin3.addComponent(lblProduccionSE);
		    	lin3.addComponent(lblValorProduccionSE);
		    	lin4.addComponent(lblProduccionET);
		    	lin4.addComponent(lblValorProduccionET);

		    lblProduccionMermas = new Label();
		    
	    	
	    	lblProduccionMermas.setValue("Mermas: ");
	    	lblProduccionMermas.setWidth(anchoLabelTitulo);    	
	    	lblProduccionMermas.addStyleName("lblTituloDerecha");

	    	lblValorProduccionMermas = new Button();
	    	lblValorProduccionMermas.setWidth(anchoLabelDatos);
	    	lblValorProduccionMermas.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorProduccionMermas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorProduccionMermas.addStyleName("lblDatos");
	    	lblValorProduccionMermas.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaCRMermasPT vtM = new pantallaCRMermasPT("Mermas Semana ", new Integer(cmbSemana.getValue().toString()), new Integer(txtEjercicio.getValue()), cmbVista.getValue().toString());
					getUI().addWindow(vtM);
				}
			});
	    	if (isPrevision())
	    	{
	    		lblValorProduccionMermas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0102")));    		
	    	}
	    	else
	    	{
	    		lblValorProduccionMermas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.mer.0102")));
	    	}
	    	
	    	
		    	lin5.addComponent(lblProduccionMermas);
		    	lin5.addComponent(lblValorProduccionMermas);

	    	consultaDashboardEmbotelladoraServer cses = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get());
			if (this.cmbSemana.getValue()!=null) semana = this.cmbSemana.getValue().toString();
			
	    	Double velocidadNominal = cses.obtenerTiempoDeCiclo(this.txtEjercicio.getValue(),semana,this.cmbVista.getValue().toString(), false);
	    	
	    	lblCiclo = new Label();
	    	lblCiclo.setValue("Tiempo Ciclo: ");
	    	lblCiclo.setWidth(anchoLabelTitulo);
	    	lblCiclo.addStyleName("lblTituloDerecha");
	    	
	    	lblValorCiclo= new Label();
	    	lblValorCiclo.setWidth(anchoLabelDatos);
	    	lblValorCiclo.setValue(RutinasNumericas.formatearDoubleDecimales(velocidadNominal,5));
	    	lblValorCiclo.addStyleName("lblDatos");
	    	
	    	lin6.addComponent(lblCiclo);
	    	lin6.addComponent(lblValorCiclo);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProduccion();
			}
		});
    	
    	centralTop.addComponent(panel,3,0);
    	
    	
    }
    
    private void cargarResumenPLanificacion()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblProduccionTotal = null;
    	Label lblProduccionBT= null;
    	Label lblProduccionSE= null;
    	Label lblProduccionET= null;
    	Label lblProduccionRT= null;
    	Label lblCiclo= null;

    	Label lblValorProduccionTotal = null;
    	Label lblValorProduccionBT= null;
    	Label lblValorProduccionSE= null;
    	Label lblValorProduccionET= null;
    	Label lblValorProduccionRT= null;
    	Label lblValorCiclo= null;
    	
    	Panel panel = new Panel("PRODUCCION PLANIFICADA");
    	
    	if (isPrevision())
    	{
    		panel.setCaption("PREVISION PLANIFICACION");
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblProduccionTotal = new Label();
    	lblProduccionTotal.setValue("Total: ");
    	lblProduccionTotal.setWidth(anchoLabelTitulo);    	
    	lblProduccionTotal.addStyleName("lblTitulo");

    	lblValorProduccionTotal = new Label();
    	lblValorProduccionTotal.setWidth(anchoLabelDatos);

    	lblValorProduccionTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102")));
    	lblValorProduccionTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblProduccionTotal);
    	lin.addComponent(lblValorProduccionTotal);
    	
	    	lblProduccionBT = new Label();
	    	lblProduccionBT.setValue(" Bt: ");
	    	lblProduccionBT.setWidth(anchoLabelTitulo);
	    	lblProduccionBT.addStyleName("lblTituloDerecha");
	
	    	lblValorProduccionBT = new Label();
	    	lblValorProduccionBT.setWidth(anchoLabelDatos);
	    	lblValorProduccionBT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102PT")));
	    	lblValorProduccionBT.addStyleName("lblDatos");
	    	
	    	lin2.addComponent(lblProduccionBT);
	    	lin2.addComponent(lblValorProduccionBT);
	    	
	    	lblProduccionSE = new Label();
	    	lblProduccionSE.setValue("A Jaulon: ");
	    	lblProduccionSE.setWidth(anchoLabelTitulo);
	    	lblProduccionSE.addStyleName("lblTituloDerecha");
		
	    	lblValorProduccionSE = new Label();
	    	lblValorProduccionSE.setWidth(anchoLabelDatos);
	    	lblValorProduccionSE.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102SE")));
	    	lblValorProduccionSE.addStyleName("lblDatos");
		    	
	    	lin3.addComponent(lblProduccionSE);
	    	lin3.addComponent(lblValorProduccionSE);

	    	lblProduccionET = new Label();
	    	lblProduccionET.setValue("Etiquetado: ");
	    	lblProduccionET.setWidth(anchoLabelTitulo);
	    	lblProduccionET.addStyleName("lblTituloDerecha");
	    	
	    	lblValorProduccionET= new Label();
	    	lblValorProduccionET.setWidth(anchoLabelDatos);
	    	lblValorProduccionET.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102ET")));
	    	lblValorProduccionET.addStyleName("lblDatos");
		    	
	    	lin4.addComponent(lblProduccionET);
	    	lin4.addComponent(lblValorProduccionET);
	    	
	    	lblProduccionRT = new Label();
	    	lblProduccionRT.setValue("Retrabajos: ");
	    	lblProduccionRT.setWidth(anchoLabelTitulo);
	    	lblProduccionRT.addStyleName("lblTituloDerecha");
	    	
	    	lblValorProduccionRT= new Label();
	    	lblValorProduccionRT.setWidth(anchoLabelDatos);
	    	lblValorProduccionRT.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan.0102RT")));
	    	lblValorProduccionRT.addStyleName("lblDatos");
		    	
	    	lin5.addComponent(lblProduccionRT);
	    	lin5.addComponent(lblValorProduccionRT);
		
	    	consultaDashboardEmbotelladoraServer cses = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get());
			if (this.cmbSemana.getValue()!=null) semana = this.cmbSemana.getValue().toString();
			
	    	Double velocidadNominal = cses.obtenerTiempoDeCiclo(this.txtEjercicio.getValue(),semana,this.cmbVista.getValue().toString(), true);
	    	
	    	lblCiclo = new Label();
	    	lblCiclo.setValue("Tiempo Ciclo: ");
	    	lblCiclo.setWidth(anchoLabelTitulo);
	    	lblCiclo.addStyleName("lblTituloDerecha");
	    	
	    	lblValorCiclo= new Label();
	    	lblValorCiclo.setWidth(anchoLabelDatos);
	    	lblValorCiclo.setValue(RutinasNumericas.formatearDoubleDecimales(velocidadNominal,5));
	    	lblValorCiclo.addStyleName("lblDatos");
	    	
	    	lin6.addComponent(lblCiclo);
	    	lin6.addComponent(lblValorCiclo);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	content.addLayoutClickListener(new LayoutClickListener() {
			
			public void layoutClick(LayoutClickEvent event) {
				pantallaProgramacion();
			}
		});
    	centralTop.addComponent(panel,2,0);
    }
    
    private void pantallaProduccion()
    {
    	if (this.cmbSemana.getValue()!=null)
    	{
    		String calc = this.cmbCalculo.getValue().toString();
    		pantallaLineasProduccion vt = new pantallaLineasProduccion("Embotelladora", new Integer(this.txtEjercicio.getValue().toString()), this.cmbSemana.getValue().toString(), "produccion semana " + this.cmbSemana.getValue().toString(), calc);
    		getUI().addWindow(vt);
    	}
    }

    private void pantallaProgramacion()
    {
    	if (this.cmbSemana.getValue()!=null)
    	{
    		pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(new Integer(this.txtEjercicio.getValue().toString()), this.cmbSemana.getValue().toString(), "programacion semana " + this.cmbSemana.getValue().toString());
    		getUI().addWindow(vt);
    	}
    }

    private void cargarVentas()
    {
    	String anchoLabelTitulo = "100px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblVentasTotal = null;
    	Label lblValorVentasTotal = null;
    	
    	
    	String traerVentas = "0";
    	Panel panel = new Panel("RESUMEN VENTAS");

    	if (isPrevision())
    	{
    		traerVentas="1";
    		panel.setCaption("RESUMEN VENTAS EJ. ANTERIOR" );
    	}
    	
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	
    	lblVentasTotal = new Label();
    	lblVentasTotal.setValue("Total: ");
    	lblVentasTotal.setWidth(anchoLabelTitulo);    	
    	lblVentasTotal.addStyleName("lblTitulo");

    	lblValorVentasTotal = new Label();
    	lblValorVentasTotal.setWidth(anchoLabelDatos);
    	lblValorVentasTotal.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get(traerVentas + ".venta.0102")*correctorVentasEjercicio));
    	lblValorVentasTotal.addStyleName("lblDatos");
    	
    	lin.addComponent(lblVentasTotal);
    	lin.addComponent(lblValorVentasTotal);
    	
    	content.addComponent(lin);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,0,0);
    }
    
    private void cargarResumenHoras()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";

    	Label lblTurnos = null;
    	Label lblValorTurnos = null;
    	
    	Label lblHoras = null;
    	Button lblValorHoras = null;
    	
    	Label lblInc = null;
    	Button lblValorInc = null;
    	
    	Label lblPro = null;
    	Button lblValorPro = null;

    	Label lblDisp = null;
    	Button lblValorDisp = null;
    	
    	Panel panel = new Panel("HORAS TRABAJADAS");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined();

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);

	    	lblTurnos = new Label();
	    	lblTurnos.setValue("Turnos: ");
	    	lblTurnos.setWidth(anchoLabelTitulo);
	    	lblTurnos.addStyleName("lblTitulo ");
	
	    	lblValorTurnos = new Label();
	    	lblValorTurnos.setWidth(anchoLabelDatos);
	    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0102")/8));
	    	lblValorTurnos.addStyleName("lblDatos");

	    	lin.addComponent(lblTurnos);
	    	lin.addComponent(lblValorTurnos);

    		lblHoras = new Label();
	    	lblHoras.setValue("Horas: ");
	    	lblHoras.setWidth(anchoLabelTitulo);
	    	lblHoras.addStyleName("lblTitulo ");
	
	    	lblValorHoras = new Button();
	    	lblValorHoras.setWidth(anchoLabelDatos);
	    	lblValorHoras.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasReales.0102")));
	    	lblValorHoras.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorHoras.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorHoras.addStyleName("lblDatos");
	    	lblValorHoras.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaTiempoDisponible vtD = new pantallaTiempoDisponible("Tiempo Disponible", mapeo.getHashValores().get("0.horasReales.0102"), mapeo.getHashValores().get("0.proReales.0102"),mapeo.getHashValores().get("0.incReales.0102"));
					getUI().addWindow(vtD);
				}
			});

	    	lin2.addComponent(lblHoras);
	    	lin2.addComponent(lblValorHoras);
	    	
	    	lblInc = new Label();
	    	lblInc.setValue("Incidencias: ");
	    	lblInc.setWidth(anchoLabelTitulo);
	    	lblInc.addStyleName("lblTitulo ");
	    	
	    	lblValorInc = new Button();
	    	lblValorInc.setWidth(anchoLabelDatos);
	    	lblValorInc.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.incReales.0102")));
	    	lblValorInc.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorInc.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorInc.addStyleName("lblDatos");
	    	lblValorInc.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					String semana = null;
					if (cmbVista.getValue().toString().contains("Anual")) semana = null;
					else if (cmbSemana.getValue()!=null) semana = cmbSemana.getValue().toString();
					pantallaIncidencias vtI = new pantallaIncidencias("EMBOTELLADO", "Incidencias", txtEjercicio.getValue().toString(), semana, "Todas", cmbVista.getValue().toString());
					getUI().addWindow(vtI);
				}
			});
	    	
	    	lin3.addComponent(lblInc);
	    	lin3.addComponent(lblValorInc);

	    	lblPro = new Label();
	    	lblPro.setValue("Programadas: ");
	    	lblPro.setWidth(anchoLabelTitulo);
	    	lblPro.addStyleName("lblTitulo ");
	    	
	    	lblValorPro = new Button();
	    	lblValorPro.setWidth(anchoLabelDatos);
	    	lblValorPro.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.proReales.0102")));
	    	lblValorPro.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorPro.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorPro.addStyleName("lblDatos");
	    	lblValorPro.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					String semana = null;
					if (cmbVista.getValue().toString().contains("Anual")) semana = null;
					else if (cmbSemana.getValue()!=null) semana = cmbSemana.getValue().toString();
					pantallaIncidencias vtI = new pantallaIncidencias("EMBOTELLADO", "Programadas", txtEjercicio.getValue().toString(), semana, "P",cmbVista.getValue().toString());
					getUI().addWindow(vtI);
				}
			});
	    	
	    	lin4.addComponent(lblPro);
	    	lin4.addComponent(lblValorPro);

	    	lblDisp = new Label();
	    	lblDisp.setValue("Operativo: ");
	    	lblDisp.setWidth(anchoLabelTitulo);
	    	lblDisp.addStyleName("lblTitulo ");
	    	
	    	Double operativo = this.mapeo.getHashValores().get("0.horasReales.0102")-this.mapeo.getHashValores().get("0.incReales.0102")-this.mapeo.getHashValores().get("0.proReales.0102");
	    	
	    	lblValorDisp = new Button();
	    	lblValorDisp.setWidth(anchoLabelDatos);
	    	lblValorDisp.setCaption(RutinasNumericas.formatearDoubleDecimales(operativo,5));
	    	lblValorDisp.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorDisp.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorDisp.addStyleName("lblDatos");
	    	
	    	lin5.addComponent(lblDisp);
	    	lin5.addComponent(lblValorDisp);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,1,0);
    	
    }
    private void cargarResumenHorasPlanificadas()
    {
    	String anchoLabelTitulo = "125px";
    	String anchoLabelDatos = "100px";
    	
    	Label lblTurnos = null;
    	Label lblValorTurnos = null;
    	
    	Label lblHoras = null;
    	Label lblValorHoras = null;
    	
    	Label lblInc = null;
    	Label lblValorInc = null;
    	
    	Label lblPro = null;
    	Label lblValorPro = null;
    	
    	Label lblDisp = null;
    	Label lblValorDisp = null;
    	
    	
    	Panel panel = new Panel("HORAS PLANIFICADAS");
    	if (isPrevision()) panel.setCaption("PREVISION HORAS / TURNOS");
//    	panel.addStyleName("mypanelexample");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue("Turnos: ");
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblTitulo ");
    	
    	lblValorTurnos = new Label();
    	lblValorTurnos.setWidth(anchoLabelDatos);
    	lblValorTurnos.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0102")/8));
    	lblValorTurnos.addStyleName("lblDatos");
    	
    	lin.addComponent(lblTurnos);
    	lin.addComponent(lblValorTurnos);
    	
    	lblHoras = new Label();
    	lblHoras.setValue("Horas: ");
    	lblHoras.setWidth(anchoLabelTitulo);
    	lblHoras.addStyleName("lblTitulo ");
    	
    	lblValorHoras = new Label();
    	lblValorHoras.setWidth(anchoLabelDatos);
    	lblValorHoras.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horas.0102")));
    	lblValorHoras.addStyleName("lblDatos");
    	
    	lin2.addComponent(lblHoras);
    	lin2.addComponent(lblValorHoras);
    	
    	lblInc = new Label();
    	lblInc.setValue("Incidencias: ");
    	lblInc.setWidth(anchoLabelTitulo);
    	lblInc.addStyleName("lblTitulo ");
    	
    	lblValorInc = new Label();
    	lblValorInc.setWidth(anchoLabelDatos);
    	lblValorInc.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.inc.0102")));
    	lblValorInc.addStyleName("lblDatos");
    	
    	lin3.addComponent(lblInc);
    	lin3.addComponent(lblValorInc);
    	
    	lblPro = new Label();
    	lblPro.setValue("Programadas: ");
    	lblPro.setWidth(anchoLabelTitulo);
    	lblPro.addStyleName("lblTitulo ");
    	
    	lblValorPro = new Label();
    	lblValorPro.setWidth(anchoLabelDatos);
    	lblValorPro.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pro.0102")));
    	lblValorPro.addStyleName("lblDatos");
    	
    	lin4.addComponent(lblPro);
    	lin4.addComponent(lblValorPro);
    	
    	Double operativo = this.mapeo.getHashValores().get("0.horas.0102")-this.mapeo.getHashValores().get("0.inc.0102")-this.mapeo.getHashValores().get("0.pro.0102");
    	lblDisp = new Label();
    	lblDisp.setValue("Operativo: ");
    	lblDisp.setWidth(anchoLabelTitulo);
    	lblDisp.addStyleName("lblTitulo ");
    	
    	lblValorDisp = new Label();
    	lblValorDisp.setWidth(anchoLabelDatos);
    	lblValorDisp.setValue(RutinasNumericas.formatearDoubleDecimales(operativo,5));
    	lblValorDisp.addStyleName("lblDatos");
    	
    	lin5.addComponent(lblDisp);
    	lin5.addComponent(lblValorDisp);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	
    	centralTop.addComponent(panel,0,0);
    	
    }
    
    private Panel indiceOEE5L()
    {
    	Double valor = null;
    	Double horas = null;
    	Double inc = null;
    	Double maq = null;

    	Double oeeRdo = null;
    	Label interpretacionOEE = null;
    	Panel gaugeOEE = null;
    	Panel gaugeD = null;
    	Panel gaugeP = null;
    	Panel gaugeC = null;
    	
    	Panel panelOEE = new Panel("Indice OEE");
    	panelOEE.setSizeUndefined();
    	
    	VerticalLayout contenido = new VerticalLayout();
    	
    	if (!isPrevision())
    	{
    		valor = this.mapeo.getHashValores().get("0.prod.0102");
    		horas = this.mapeo.getHashValores().get("0.horasReales.0102");
    		inc = this.mapeo.getHashValores().get("0.proReales.0102");
    		maq = this.mapeo.getHashValores().get("0.incReales.0102");
    	}
    	else
    	{
    		valor = this.mapeo.getHashValores().get("0.plan.0102");
    		horas = this.mapeo.getHashValores().get("0.horas.0102");
    		inc = this.mapeo.getHashValores().get("0.pro.0102");
    		maq = this.mapeo.getHashValores().get("0.inc.0102");
    	}

    	
		disponibilidad = obtenerDisponibilidad(horas, maq,inc);
		
			gaugeD = reloj.generarReloj("DISPONIBILIDAD",((new Double(disponibilidad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerDisp = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaDisponibilidad vt = new pantallaDisponibilidad("Disponibilidad", txtEjercicio.getValue().toString());
					getUI().addWindow(vt);
				}
			};
			gaugeD.addClickListener(listenerDisp);
			centralMiddle2.addComponent(gaugeD,1,0);
			
		
		productividad = obtenerProductividad("BT", valor, horas, maq, inc, isPrevision());
			gaugeP = reloj.generarReloj("PRODUCTIVIDAD",((new Double(productividad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerP = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaProductividad vtP = new pantallaProductividad("Productividad", txtEjercicio.getValue().toString());
					getUI().addWindow(vtP);
				}
			};
			gaugeP.addClickListener(listenerP);
			centralMiddle2.addComponent(gaugeP,2,0);

		calidad = obtenerCalidad (valor, this.mapeo.getHashValores().get("0.mer.0102"));
			gaugeC = reloj.generarReloj("CALIDAD",((new Double(calidad*100)).intValue()),92, 100, 85, 91, 0, 84, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
			com.vaadin.event.MouseEvents.ClickListener listenerQ = new com.vaadin.event.MouseEvents.ClickListener() {
				
				@Override
				public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
					pantallaCalidad vtQ = new pantallaCalidad("Calidad", txtEjercicio.getValue().toString());
					getUI().addWindow(vtQ);
				}
			};
			gaugeC.addClickListener(listenerQ);
			centralMiddle2.addComponent(gaugeC,3,0);
			
		oeeRdo = disponibilidad * productividad * calidad *100;
		gaugeOEE = reloj.generarReloj("",oeeRdo.intValue(), 75, 100, 60, 74, 0, 59, 0, 100, GaugeStyle.STYLE_DEFAULT.toString(), tamañoRelojes);
		
		interpretacionOEE = obtenerInterpretacion(oeeRdo);
		
//		Double teep =teep("0102", oeeRdo);
		
		contenido.addComponent(gaugeOEE);
		contenido.addComponent(interpretacionOEE);
		contenido.setComponentAlignment(interpretacionOEE, Alignment.MIDDLE_CENTER);
    	panelOEE.setContent(contenido);
    	
    	return panelOEE;
    }
    
    private Label obtenerInterpretacion(Double r_oee)    
    {
    	Label resultado = new Label();
    	resultado.setWidth("100%");
    	
    	if (r_oee < 65)
    	{
    		resultado.setValue("INADMISIBLE");
    		resultado.addStyleName("oeeMalo");
    	}
    	if (r_oee >= 65 && r_oee < 75)
    	{
    		resultado.setValue("REGULAR");
    		resultado.addStyleName("oeeRegular");
    	}
    	if (r_oee >= 75 && r_oee < 85)
    	{
    		resultado.setValue("ACEPTABLE");
    		resultado.addStyleName("oeeAceptable");
    	}
    	if (r_oee >= 85 && r_oee < 95)
    	{
    		resultado.setValue("BUENA");
    		resultado.addStyleName("oeeBuena");
    	}
    	if (r_oee > 95)
    	{
    		resultado.setValue("EXCELENTE");
    		resultado.addStyleName("oeeExcelente");
    	}
    	
    	return resultado;
    }
    
//    private double teep(String r_area, Double r_oee)
//    {
//    	/*
//    	 * teep = oee * utilizacion
//    	 * 
//    	 * utilizacion = tiempo planificado / tiempo total año
//    	 * 
//    	 */
//    	Double teep = null;
//    	Double utilizacion = null;
//    	
//    	if (r_area.contentEquals("0102"))
//    	{
//    		utilizacion = consultaDashboardEmbotelladoraServer.horasTurno*5/(24*7);
//    	}
//    	teep = r_oee * utilizacion;
//    	return teep;
//    }
    
    private double obtenerProductividad(String r_area, Double r_produccion, Double r_horas, Double r_incidencias, Double r_programadas, boolean r_prevision)
    {
    	Double velocidadNominal = null;
    	Double productividad = null;
    	Double tiempoOperativo = null;
    	String semana = null;
    	switch (r_area)
    	{
    		case "BT":
    		{
    			consultaDashboardEmbotelladoraServer cses = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get());
    			if (this.cmbSemana.getValue()!=null) semana = this.cmbSemana.getValue().toString();
    			velocidadNominal = cses.obtenerTiempoDeCiclo(this.txtEjercicio.getValue(),semana,this.cmbVista.getValue().toString(), r_prevision);
    			break;
    		}
    	}
    	tiempoOperativo = r_horas - r_incidencias - r_programadas;
    	productividad = velocidadNominal / tiempoOperativo;
    	
    	return productividad;
    }
    
    private double obtenerCalidad(Double r_produccion, Double r_mermas)
    {
    	return (r_produccion - r_mermas)/(r_produccion);
    }

    private double obtenerDisponibilidad(Double r_horas, Double r_incidencias, Double r_programadas)
    {
    	return (r_horas - r_incidencias - r_programadas)/(r_horas - r_programadas);
    }
    
    private void evolucionVentas()
	{
		String semana = null;
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		
		consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
		if (this.cmbSemana.getValue()!=null)
		{
			semana = this.cmbSemana.getValue().toString();
		}
		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()),"0102",this.cmbCalculo.getValue().toString(), semana);
	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue())-1,"0102",this.cmbCalculo.getValue().toString(), semana);
	    String tituloY = null;
	    
	    if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Semana"; else tituloY="Bt. / Semana";
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Ventas", "Semanas", tituloY, hashVentas, hashVentasAnterior, txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), "510px", "250px",0, 0, 0);
	    
	    if (isPrevision()) centralMiddle.addComponent(wrapper,0,1); else centralMiddle.addComponent(wrapper,0,1);
	    
	}


//    private void ventasBT()
//    {
//		HashMap<String, Double> hashVentas = null;
//		HashMap<String, Double> hashVentasAnterior = null;
//		
//		consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
//		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0102",this.cmbCalculo.getValue().toString());
//	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0102",this.cmbCalculo.getValue().toString());
//	    
//	    JFreeChartWrapper wrapper = linea.generarGrafico("Ventas", "Semanas", "Ltrs. / Semana", hashVentas, null, "Palets", null, "510px", "300px",0);
//	    
//	    centralMiddle.addComponent(wrapper,1,0);
//	    
//    }
//    
//    private void ventas2L()
//    {
//		HashMap<String, Double> hashVentas = null;
//		HashMap<String, Double> hashVentasAnterior = null;
//		
//		consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
//		hashVentas = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103100",this.cmbCalculo.getValue().toString());
//	    hashVentasAnterior = cdes.recuperarMovimientosVentasEjercicio(new Integer(this.txtEjercicio.getValue()), "0103104",this.cmbCalculo.getValue().toString());
//
//	    JFreeChartWrapper wrapper = linea.generarGrafico("Ventas Palets y 1/2 Paletas de 2L", "Semanas", "Ltrs. / Semana", hashVentas, hashVentasAnterior, "Palets", "1/2 Paletas", "510px", "300px",0);
//	    
//	    centralMiddle.addComponent(wrapper,2,0);
//	} 


    private void tendenciaAnualComparativaBT()
    {
    	HashMap<String, Double> hashVentasBT = null;
    	String tituloY=null;
    	
    	consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
    	hashVentasBT = cdes.recuperarVentasComparativaEjercicios(new Integer(this.txtEjercicio.getValue())-1, "0102", 5,this.cmbCalculo.getValue().toString());
    	
    	if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Ejercicio"; else tituloY="Bts. / Ejercicio";
    	JFreeChartWrapper wrapper = linea.generarGrafico("Tendencia Anual Ventas", "Ejercicios", tituloY, hashVentasBT, null, "Ventas ", null, "510px", "250px",new Integer(txtEjercicio.getValue())-6,0,0);
    	
    	centralMiddle.addComponent(wrapper,1,0);
    }

    private void evolucionVentasAcumuladas()
	{
		String semana = null;
		HashMap<String, Double> hashVentas = null;
		HashMap<String, Double> hashVentasAnterior = null;
		
		consultaDashboardEmbotelladoraServer cdes = new consultaDashboardEmbotelladoraServer(CurrentUser.get());
		if (this.cmbSemana.getValue()!=null)
		{
			semana =this.cmbSemana.getValue().toString();
		}
		hashVentas = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue()),"0102",this.cmbCalculo.getValue().toString(),semana);
		hashVentasAnterior = cdes.recuperarVentasAcumulados(new Integer(this.txtEjercicio.getValue())-1,"0102",this.cmbCalculo.getValue().toString(),semana);
	    String tituloY = null;
	    
	    if (this.cmbCalculo.getValue().toString().contentEquals("Litros")) tituloY = "Ltrs. / Semana"; else tituloY="Bt. / Semana";
	    JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Ventas Ac.", "Semanas", tituloY, hashVentas, hashVentasAnterior, txtEjercicio.getValue(), String.valueOf(new Integer(this.txtEjercicio.getValue())-1), "510px", "250px",0,0,0);
	    
	    centralMiddle.addComponent(wrapper,1,1); 
	}

	private void cargarCombo(String r_ejercicio)
	{
		
		switch (cmbVista.getValue().toString())
		{
			case "Anual":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setCaption("");
				this.cmbSemana.setEnabled(false);				
				break;
			}
			case "Semestral":
			{
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Semestre");
				this.cmbSemana.removeAllItems();
				for (int i=1; i<=2; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}

				break;
			}
			case "Trimestral":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Trimestre");
				for (int i=1; i<=4; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}
				break;
			}
			case "Mensual":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Mes");
				for (int i=1; i<=12; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}
				break;
			}
			case "Semanal":
			case "Acumulado":
			{
				this.cmbSemana.removeAllItems();
				this.cmbSemana.setEnabled(true);
				this.cmbSemana.setCaption("Semana");
				if (r_ejercicio==null)
				{
					this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
					r_ejercicio = this.txtEjercicio.getValue();
				}
				else
				{
					this.cmbSemana.removeAllItems();
				}

				if (RutinasFechas.semanaActual(r_ejercicio)-1 < 1 )
				{
					this.semana = "53";
					this.txtEjercicio.setValue(String.valueOf((new Integer(RutinasFechas.añoActualYYYY())-1)));
					r_ejercicio = this.txtEjercicio.getValue();
				}
				else
				{
					this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio)-1);
				}
				int semanas = RutinasFechas.semanasAño(r_ejercicio);
				
				for (int i=1; i<=semanas; i++)
				{
					this.cmbSemana.addItem(String.valueOf(i));
				}

				break;
			}
		}
	}

    private void vaciarPantalla()
    {
    	centralTop=null;
        centralMiddle=null;
        centralBottom=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	public boolean isPrevision() {
		return prevision;
	}

	public void setPrevision(boolean prevision) {
		
		if (prevision) this.correctorVentasEjercicio=new Double(1.1); else this.correctorVentasEjercicio=new Double(1);
		this.prevision = prevision;
	}
	
    private void cargarGraficoIncidencias()
    {
	    ArrayList<MapeoBarras> vector = null;
		MapeoBarras mapeoGrafico = null;
		Double max = new Double(0);
		vector = new ArrayList<MapeoBarras>();
		
			mapeoGrafico = new MapeoBarras();
			mapeoGrafico.setClave("reales");
			mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.incReales.0102"));
			vector.add(mapeoGrafico);

			mapeoGrafico = new MapeoBarras();
			mapeoGrafico.setClave("planificadas");
			mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.inc.0102"));
			vector.add(mapeoGrafico);
		
		max = this.mapeo.getHashValores().get("0.incReales.0102") + this.mapeo.getHashValores().get("0.inc.0102");
		JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Incidencias", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.incReales.0102")<=this.mapeo.getHashValores().get("0.inc.0102"));

		centralTop.addComponent(wrapper,0,1);
    }

    private void cargarGraficoProgramadas()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("reales");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.proReales.0102"));
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificadas");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.pro.0102"));
    	vector.add(mapeoGrafico);
    	
    	max = this.mapeo.getHashValores().get("0.proReales.0102") + this.mapeo.getHashValores().get("0.pro.0102");
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Programadas", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.proReales.0102")<=this.mapeo.getHashValores().get("0.pro.0102"));
    	
    	centralTop.addComponent(wrapper,1,1);
    }

    private void cargarGraficoProduccion()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("real");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.prod.0102"));
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificada");
    	mapeoGrafico.setValorActual(this.mapeo.getHashValores().get("0.plan.0102"));
    	vector.add(mapeoGrafico);
    	
    	max = this.mapeo.getHashValores().get("0.prod.0102") + this.mapeo.getHashValores().get("0.plan.0102");
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Producción", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, this.mapeo.getHashValores().get("0.prod.0102")>=this.mapeo.getHashValores().get("0.plan.0102"));
    	
    	centralTop.addComponent(wrapper,2,1);
    }
    
    private void cargarGraficoBotellasHora()
    {
    	ArrayList<MapeoBarras> vector = null;
    	MapeoBarras mapeoGrafico = null;
    	Double max = new Double(0);
    	vector = new ArrayList<MapeoBarras>();
    	Double mediaPlan = new Double(0);
    	Double mediaReal = new Double(0);
    	
		mediaReal = this.mapeo.getHashValores().get("0.prod.0102")/(this.mapeo.getHashValores().get("0.horasReales.0102")-this.mapeo.getHashValores().get("0.proReales.0102"));
		mediaPlan = this.mapeo.getHashValores().get("0.plan.0102")/(this.mapeo.getHashValores().get("0.horas.0102")-this.mapeo.getHashValores().get("0.pro.0102"));
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("real");
    	mapeoGrafico.setValorActual(mediaReal);
    	vector.add(mapeoGrafico);
    	
    	mapeoGrafico = new MapeoBarras();
    	mapeoGrafico.setClave("planificada");
    	mapeoGrafico.setValorActual(mediaPlan);
    	vector.add(mapeoGrafico);
    	
    	max = mediaReal + mediaPlan;
    	JFreeChartWrapper wrapper = stackedBarras.generarGrafico("Botellas / Hora", "", "", "250px", "300px", vector,"", null, null,true, 0, max *2, mediaReal>=mediaPlan);
    	
    	centralTop.addComponent(wrapper,3,1);
    }
}
