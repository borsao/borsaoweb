package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.Date;

import javax.print.DocFlavor;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CALIDAD.CosteMaquina.server.consultaCosteMaquinaServer;
import borsao.e_borsao.Modulos.CALIDAD.CostePersonal.server.consultaCostePersonalServer;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardControlTurnosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardControlTurnosView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Control Turnos";
	private final String titulo = "Control Turnos";
    private Label lblTitulo = null; 
    
    private CssLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private GridLayout centralBottom = null;
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;
    private Panel panelDesglose =null;
    private Panel panelCostes =null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle2 = null;
    
	public TextField txtEjercicio= null;

	private Button procesar=null;
	private Button opcSalir = null;
	
	private MapeoGeneralDashboardEmbotelladora mapeo=null;
	
	private static Integer horasJornada = 8;
	
	private Integer diasAnio = null;
	private Integer diasFds = null;
	private Integer diasFestivos = null;
	
	private Integer diasRevisionE = null;
	private Integer diasLaborablesE = null;
	private Integer turnosLaborablesE = null;
	private Integer turnosJornadaE = null;

	private Integer diasRevisionB = null;
	private Integer diasLaborablesB = null;
	private Integer turnosLaborablesB = null;
	private Integer turnosJornadaB = null;
	
	private Integer diasRevisionV = null;
	private Integer diasLaborablesV = null;
	private Integer turnosLaborablesV = null;
	private Integer turnosJornadaV = null;
	
	
	private boolean EMBEnBIB = false;
	private boolean ettEnBIB = false;
	
	private boolean EMBEnVM = false;
	private boolean ettEnVM = false;
	

	/*
     * METODOS PROPIOS PERO GENERICOS
     */

    public DashboardControlTurnosView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();
	}

    private void cargarPantalla() 
    {
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new CssLayout();
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

		    	this.topLayoutL = new HorizontalLayout();
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.txtEjercicio=new TextField("Ejercicio");
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);
		        	
			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.txtEjercicio);
		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

		    panelResumen = new Panel("Datos Partida");
		    panelResumen.setSizeUndefined();
		    panelResumen.setWidth("100%");
		    panelResumen.setVisible(false);
		    panelResumen.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelResumen );

		    	this.centralTop2 = new GridLayout(3,1);
		    	this.centralTop2.setSpacing(true);
		    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	
		    	panelResumen.setContent(centralTop2);

		    panelDesglose= new Panel("Desglose Turnos");
		    panelDesglose.setSizeUndefined();
		    panelDesglose.setWidth("100%");
		    panelDesglose.setVisible(false);
		    panelDesglose.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelDesglose );
		    
			    this.centralMiddle2 = new GridLayout(3,1);
			    this.centralMiddle2.setWidth("100%");
			    this.centralMiddle2.setMargin(true);
	
			    panelDesglose.setContent(centralMiddle2);

		    panelCostes = new Panel("Costes");
		    panelCostes.setSizeUndefined();
		    panelCostes.setWidth("100%");
		    panelCostes.setVisible(false);
		    panelCostes.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelCostes );

		    	this.centralMiddle = new GridLayout(3,2);
		    	this.centralMiddle.setWidth("100%");
		    	this.centralMiddle.setMargin(true);

		    	panelCostes.setContent(centralMiddle);

	    	this.centralBottom = new GridLayout(2,1);
	    	this.centralBottom.setWidth("100%");
	    	this.centralBottom.setMargin(true);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(panelResumen);
    	this.barAndGridLayout.addComponent(panelDesglose);
    	this.barAndGridLayout.addComponent(panelCostes);
    	this.barAndGridLayout.addComponent(this.centralBottom);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
    }

    private void cargarListeners() 
    {
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});
    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});
    }

    private void procesar()
    {
		eliminarDashBoards();
		
		if (txtEjercicio.getValue()==null || txtEjercicio.getValue().length()==0 )
		{
			Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
		}
		else
		{
			cargarDashboards();
		}
    }

    public void eliminarDashBoards()
    {
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    }
    
    public void cargarDashboards() 
    {
    	String ejer =null;
    	String sem =null;
    	String vista=null;
    	String campo =null;
    	
    	if (this.txtEjercicio.getValue()!=null) ejer = this.txtEjercicio.getValue();
    	sem="0";
    	vista="Anual";
    	
    	this.mapeo = consultaDashboardControlTurnosServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticosEmbotelladora(ejer, sem, vista);
    	
    	/*
    	 * cargar panel resumen datos
    	 */
    	cargarResumen();
    	cargarDesgloseTurnos();
    	cargarCostes();
    }
    
    private void cargarResumen()
    {
    	Panel panel = cargoPanelPartida();
    	centralTop2.addComponent(panel,0,0);
    	
    	Panel panelEmbotelladora = cargoPanelDatosPartidaLinea();
    	centralTop2.addComponent(panelEmbotelladora,1,0);
    	
    	this.panelResumen.setVisible(true);
//    	this.panelResumen.setSizeFull();
    	this.panelResumen.setWidth("1500");
    	this.panelResumen.getContent().setVisible(true);

    }
    
    private void cargarDesgloseTurnos()
    {
    	
    	Panel panelEmbotellado= cargoPanelConsumoEmbotelladora();
    	centralMiddle2.addComponent(panelEmbotellado,0,0);
//
    	Panel panelEnvasado = cargoPanelConsumoEnvasadora();
    	centralMiddle2.addComponent(panelEnvasado,1,0);
//    	
    	Panel panelBib = cargoPanelConsumoBib();
    	centralMiddle2.addComponent(panelBib,2,0);
//    	
    	this.panelDesglose.setVisible(true);
    	this.panelDesglose.getContent().setVisible(false);

    }

    private void cargarCostes()
    {
    	Panel panel = cargoPanelCostesProduccion();
    	centralMiddle.addComponent(panel,0,0);
    	
    	this.panelCostes.setVisible(true);
    	this.panelCostes.setWidth("1500");
    	this.panelCostes.getContent().setVisible(true);

    }

    private Panel cargoPanelPartida()
    {
    	
    	String anchoLabelTitulo = "130px";
    	String anchoLabelDatos = "150px";

    	Label lblDias = null;
    	Label lblValorDias= null;
    	
    	Label lblDiasFds = null;
    	Label lblValorDiasFds= null;
    	
    	Label lblDiasFestivos= null;
    	Label lblValorDiasFestivos= null;

    	Panel panel = new Panel("Datos Anuales Partida");
    	panel.setSizeUndefined();

	    	// Create the content
	    	VerticalLayout content = new VerticalLayout();
	
		    	HorizontalLayout lin = new HorizontalLayout();
		    	lin.setSpacing(true);
			    	lblDias= new Label();
			    	lblDias.setValue("Dias: ");
			    	lblDias.setWidth(anchoLabelTitulo);
			    	lblDias.addStyleName("lblTitulo ");
			
			    	lblValorDias= new Label();
			    	lblValorDias.setWidth(anchoLabelDatos);
			    	lblValorDias.addStyleName("lblDatos");
			    	
			  		Date d = RutinasFechas.conversionDeString("01/01/" + this.txtEjercicio.getValue());
			  		diasAnio = RutinasFechas.obtenerDiasDelAño(d);
			    	lblValorDias.setValue(diasAnio.toString());
	
		    	lin.addComponent(lblDias);
		    	lin.addComponent(lblValorDias);
		
		    	HorizontalLayout lin1= new HorizontalLayout();
		    	lin1.setSpacing(true);
			    	
			    	lblDiasFds = new Label();
			    	lblDiasFds.setValue("Días Fds.: ");
			    	lblDiasFds.setWidth(anchoLabelTitulo);    	
			    	lblDiasFds.addStyleName("lblTitulo");
		
			    	lblValorDiasFds = new Label();
			    	lblValorDiasFds.setWidth(anchoLabelDatos);
			    	lblValorDiasFds.addStyleName("lblDatos");
			    	
			    	lblValorDiasFds.setValue(this.calcularDiasFinDeSemana());
			    	
		    	lin1.addComponent(lblDiasFds);
		    	lin1.addComponent(lblValorDiasFds);

		    	HorizontalLayout lin2 = new HorizontalLayout();
		    	lin2.setSpacing(true);

			    	lblDiasFestivos = new Label();
			    	lblDiasFestivos.setValue(" Días Festivos: ");
			    	lblDiasFestivos.setWidth(anchoLabelTitulo);
			    	lblDiasFestivos.addStyleName("lblTitulo");
			
			    	lblValorDiasFestivos = new Label();
			    	lblValorDiasFestivos.setWidth(anchoLabelDatos);
			    	
			    	diasFestivos= new Integer(this.obtenerParametro("Festivos"));
			    	lblValorDiasFestivos.setValue(this.obtenerParametro("Festivos"));
			    	lblValorDiasFestivos.addStyleName("lblDatos");
			    	
		    	lin2.addComponent(lblDiasFestivos);
		    	lin2.addComponent(lblValorDiasFestivos);
				    	
	    	content.addComponent(lin);
	    	content.addComponent(lin1);
	    	content.addComponent(lin2);

	    	content.setSizeUndefined(); // Shrink to fit
	    	content.setSpacing(false);
	    	content.setMargin(true);
		panel.setContent(content);
		
		return panel;
    }    
    
    private Panel cargoPanelDatosPartidaLinea()
    {
    	String anchoLabelTitulo = "150px";
    	String anchoLabelDatos = "150px";
    	
    	Label lblLinea = null;
    	Label lblValorLinea1 = null;
    	Label lblValorLinea2 = null;
    	Label lblValorLinea3 = null;

    	Label lblTurnosJornada = null;
    	Label lblValorTurnosJornada1= null;
    	Label lblValorTurnosJornada2= null;
    	Label lblValorTurnosJornada3= null;
    	
    	Label lblDiasRevision = null;
    	Label lblValorDiasRevision1= null;
    	Label lblValorDiasRevision2= null;
    	Label lblValorDiasRevision3= null;

    	Label lblDiasLaborables = null;
    	Label lblValorDiasLaborables1= null;
    	Label lblValorDiasLaborables2= null;
    	Label lblValorDiasLaborables3= null;

    	Label lblHorasDisponibles = null;
    	Label lblValorHorasDisaponibles1 = null;
    	Label lblValorHorasDisaponibles2 = null;
    	Label lblValorHorasDisaponibles3 = null;

    	Label lblTurnosDisponibles = null;
    	Label lblValorTurnosDisaponibles1 = null;
    	Label lblValorTurnosDisaponibles2 = null;
    	Label lblValorTurnosDisaponibles3 = null;

    	Panel panelm = new Panel("DATOS LINEAS PRODUCCION");
    	panelm.setSizeUndefined();

	    	// Create the content
	    	VerticalLayout contentm = new VerticalLayout();
	
	    	HorizontalLayout linM = new HorizontalLayout();
	    	linM.setSpacing(true);
	    	
		    	lblLinea = new Label();
		    	lblLinea.setValue("Línea");
		    	lblLinea.setWidth(anchoLabelTitulo);
		    	lblLinea.addStyleName("lblTituloDerecha");
		    	
		    	lblDiasRevision = new Label();
		    	lblDiasRevision.setValue("Dias Revisión");
		    	lblDiasRevision.setWidth(anchoLabelTitulo);
		    	lblDiasRevision.addStyleName("lblTituloDerecha");
		    	
		    	lblDiasLaborables = new Label();
		    	lblDiasLaborables.setValue("Días Laborables:");
		    	lblDiasLaborables.setWidth(anchoLabelTitulo);
		    	lblDiasLaborables.addStyleName("lblTituloDerecha");

		    	lblTurnosJornada= new Label();
		    	lblTurnosJornada.setValue("Turnos/Jornada");
		    	lblTurnosJornada.setWidth(anchoLabelTitulo);
		    	lblTurnosJornada.addStyleName("lblTituloDerecha");

		    	lblTurnosDisponibles = new Label();
		    	lblTurnosDisponibles.setValue("Turnos Disponibles:");
		    	lblTurnosDisponibles.setWidth(anchoLabelTitulo);
		    	lblTurnosDisponibles.addStyleName("lblTituloDerecha");
		    	
		    	lblHorasDisponibles = new Label();
		    	lblHorasDisponibles.setValue("Horas Disponibles:");
		    	lblHorasDisponibles.setWidth(anchoLabelTitulo);
		    	lblHorasDisponibles.addStyleName("lblTituloDerecha");

		    	linM.addComponent(lblLinea);
		    	linM.addComponent(lblDiasRevision);
		    	linM.addComponent(lblDiasLaborables);
		    	linM.addComponent(lblTurnosJornada);
		    	linM.addComponent(lblTurnosDisponibles);
		    	linM.addComponent(lblHorasDisponibles);
	
	    	HorizontalLayout linM1= new HorizontalLayout();
	    	linM1.setSpacing(true);
		    	
			    	lblValorLinea1 = new Label();
			    	lblValorLinea1.setValue("EMBOTELLADORA");
			    	lblValorLinea1.setWidth(anchoLabelTitulo);
			    	lblValorLinea1.addStyleName("lblTituloDerecha");
			
			    	lblValorDiasRevision1 = new Label();
			    	lblValorDiasRevision1.setWidth(anchoLabelDatos);
			    	diasRevisionE = new Integer(this.obtenerParametro("Dias Revision Embotelladora"));			    	
			    	lblValorDiasRevision1.setValue(this.obtenerParametro("Dias Revision Embotelladora"));
			    	lblValorDiasRevision1.addStyleName("lblDatos");

			    	lblValorDiasLaborables1 = new Label();
			    	lblValorDiasLaborables1.setWidth(anchoLabelDatos);
			    	diasLaborablesE=diasAnio-diasFds-diasFestivos-diasRevisionE;
			    	lblValorDiasLaborables1.setValue(diasLaborablesE.toString());
			    	lblValorDiasLaborables1.addStyleName("lblDatos");

			    	lblValorTurnosJornada1 = new Label();
			    	lblValorTurnosJornada1.setWidth(anchoLabelDatos);
			    	turnosJornadaE=new Integer(this.obtenerParametro("Turnos Dia Embotelladora"));
			    	lblValorTurnosJornada1.setValue(this.obtenerParametro("Turnos Dia Embotelladora"));
			    	lblValorTurnosJornada1.addStyleName("lblDatos");

			    	lblValorTurnosDisaponibles1= new Label();
			    	turnosLaborablesE = turnosJornadaE * diasLaborablesE;
			    	lblValorTurnosDisaponibles1.setValue(turnosLaborablesE.toString());
			    	lblValorTurnosDisaponibles1.setWidth(anchoLabelTitulo);
			    	lblValorTurnosDisaponibles1.addStyleName("lblDatos");

			    	lblValorHorasDisaponibles1= new Label();
			    	lblValorHorasDisaponibles1.setValue(new Integer(turnosLaborablesE*8).toString());
			    	lblValorHorasDisaponibles1.setWidth(anchoLabelTitulo);
			    	lblValorHorasDisaponibles1.addStyleName("lblDatos");
			    	
			    	linM1.addComponent(lblValorLinea1);
			    	linM1.addComponent(lblValorDiasRevision1);
			    	linM1.addComponent(lblValorDiasLaborables1);
			    	linM1.addComponent(lblValorTurnosJornada1);
			    	linM1.addComponent(lblValorTurnosDisaponibles1);
			    	linM1.addComponent(lblValorHorasDisaponibles1);
			    	
		    	HorizontalLayout linM2= new HorizontalLayout();
		    	linM2.setSpacing(true);
		    	
			    	lblValorLinea2 = new Label();
			    	lblValorLinea2.setValue("BIB");
			    	lblValorLinea2.setWidth(anchoLabelTitulo);
			    	lblValorLinea2.addStyleName("lblTituloDerecha");
			
			    	lblValorDiasRevision2 = new Label();
			    	lblValorDiasRevision2.setWidth(anchoLabelDatos);
			    	diasRevisionB=0;
			    	lblValorDiasRevision2.setValue(diasRevisionB.toString());
			    	lblValorDiasRevision2.addStyleName("lblDatos");
	
			    	lblValorDiasLaborables2 = new Label();
			    	lblValorDiasLaborables2.setWidth(anchoLabelDatos);
			    	diasLaborablesB=0;
			    	lblValorDiasLaborables2.setValue(diasLaborablesB.toString());
			    	lblValorDiasLaborables2.addStyleName("lblDatos");
	
			    	lblValorTurnosJornada2 = new Label();
			    	lblValorTurnosJornada2.setWidth(anchoLabelDatos);
			    	turnosJornadaB=0;
			    	lblValorTurnosJornada2.setValue(turnosJornadaB.toString());
			    	lblValorTurnosJornada2.addStyleName("lblDatos");
	
			    	lblValorTurnosDisaponibles2= new Label();
			    	turnosLaborablesB=0;
			    	lblValorTurnosDisaponibles2.setValue(turnosJornadaB.toString());
			    	lblValorTurnosDisaponibles2.setWidth(anchoLabelTitulo);
			    	lblValorTurnosDisaponibles2.addStyleName("lblDatos");
	
			    	lblValorHorasDisaponibles2= new Label();
			    	lblValorHorasDisaponibles2.setValue(new Integer(turnosLaborablesB*horasJornada).toString());
			    	lblValorHorasDisaponibles2.setWidth(anchoLabelTitulo);
			    	lblValorHorasDisaponibles2.addStyleName("lblDatos");
			    	
			    	linM2.addComponent(lblValorLinea2);
			    	linM2.addComponent(lblValorDiasRevision2);
			    	linM2.addComponent(lblValorDiasLaborables2);
			    	linM2.addComponent(lblValorTurnosJornada2);
			    	linM2.addComponent(lblValorTurnosDisaponibles2);
			    	linM2.addComponent(lblValorHorasDisaponibles2);

		    	HorizontalLayout linM3= new HorizontalLayout();
		    	linM3.setSpacing(true);
				    	
			    	lblValorLinea3 = new Label();
			    	lblValorLinea3.setValue("ENVASADORA");
			    	lblValorLinea3.setWidth(anchoLabelTitulo);
			    	lblValorLinea3.addStyleName("lblTituloDerecha");
			
			    	lblValorDiasRevision3 = new Label();
			    	lblValorDiasRevision3.setWidth(anchoLabelDatos);
			    	diasRevisionV = new Integer(this.obtenerParametro("Dias Revision Envasadora"));			    	
			    	lblValorDiasRevision3.setValue(diasRevisionV.toString());
			    	lblValorDiasRevision3.addStyleName("lblDatos");

			    	lblValorDiasLaborables3 = new Label();
			    	lblValorDiasLaborables3.setWidth(anchoLabelDatos);
			    	diasLaborablesV=diasAnio-diasFds-diasFestivos-diasRevisionV;
			    	lblValorDiasLaborables3.setValue(diasLaborablesV.toString());
			    	lblValorDiasLaborables3.addStyleName("lblDatos");

			    	lblValorTurnosJornada3 = new Label();
			    	lblValorTurnosJornada3.setWidth(anchoLabelDatos);
			    	turnosJornadaV=new Integer(this.obtenerParametro("Turnos Dia Envasadora"));
			    	lblValorTurnosJornada3.setValue(turnosJornadaV.toString());
			    	lblValorTurnosJornada3.addStyleName("lblDatos");

			    	lblValorTurnosDisaponibles3= new Label();
			    	turnosLaborablesV = turnosJornadaV * diasLaborablesV;
			    	lblValorTurnosDisaponibles3.setValue(turnosLaborablesV.toString());
			    	lblValorTurnosDisaponibles3.setWidth(anchoLabelTitulo);
			    	lblValorTurnosDisaponibles3.addStyleName("lblDatos");

			    	lblValorHorasDisaponibles3= new Label();
			    	lblValorHorasDisaponibles3.setValue(new Integer(turnosLaborablesV*horasJornada).toString());
			    	lblValorHorasDisaponibles3.setWidth(anchoLabelTitulo);
			    	lblValorHorasDisaponibles3.addStyleName("lblDatos");
				    	
		    	linM3.addComponent(lblValorLinea3);
		    	linM3.addComponent(lblValorDiasRevision3);
		    	linM3.addComponent(lblValorDiasLaborables3);
		    	linM3.addComponent(lblValorTurnosJornada3);
		    	linM3.addComponent(lblValorTurnosDisaponibles3);
		    	linM3.addComponent(lblValorHorasDisaponibles3);
					    	
		    	contentm.addComponent(linM);
		    	contentm.addComponent(linM1);
		    	contentm.addComponent(linM2);
		    	contentm.addComponent(linM3);
		    	
	    	contentm.setSizeUndefined(); // Shrink to fit
	    	contentm.setSpacing(false);
	    	contentm.setMargin(true);
	    	panelm.setContent(contentm);
	    	
	    	return panelm;

    }

    private Panel cargoPanelConsumoEmbotelladora()
    {
    	
    	String anchoLabelTitulo = "130px";
    	String anchoLabelDatos = "150px";
    	
    	Double resto = new Double(0);
    	Label lblTurnosEmbotellado = null;
    	Label lblValorTurnosEmbotellado= null;
    	
    	Label lblTurnosBib = null;
    	Label lblValorTurnosBib= null;
    	
    	Label lblTurnosEnvasado= null;
    	Label lblValorTurnosEnvasado= null;

    	Label lblTurnosRetrabajos= null;
    	Label lblValorTurnosRetrabajos= null;
    	
    	Label lblValorRestoB= null;
    	Label lblValorRestoB1= null;
    	Label lblValorRestoB2= null;
    	Label lblValorRestoB3= null;
    	
    	Panel panel = new Panel("Turnos Embotelladora");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	resto = turnosLaborablesE.doubleValue();
    	
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	
	    	lblTurnosEmbotellado= new Label();
	    	lblTurnosEmbotellado.setValue("A Embotellado: ");
	    	lblTurnosEmbotellado.setWidth(anchoLabelTitulo);
	    	lblTurnosEmbotellado.addStyleName("lblTitulo ");
	    	
	    	lblValorTurnosEmbotellado= new Label();
	    	lblValorTurnosEmbotellado.setWidth(anchoLabelDatos);
	    	lblValorTurnosEmbotellado.addStyleName("lblDatos");
	    	lblValorTurnosEmbotellado.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.horasEmbotellado.0102")/horasJornada));

	    	lblValorRestoB= new Label();
	    	lblValorRestoB.setWidth(anchoLabelDatos);
	    	lblValorRestoB.addStyleName("lblDatos");
	    	resto = resto - (this.mapeo.getHashValores().get("0.horasEmbotellado.0102")/horasJornada);
	    	lblValorRestoB.setValue(RutinasNumericas.formatearDouble(resto));
    	
    	lin.addComponent(lblTurnosEmbotellado);
    	lin.addComponent(lblValorTurnosEmbotellado);
    	lin.addComponent(lblValorRestoB);
    	
    	HorizontalLayout lin1= new HorizontalLayout();
    	lin1.setSpacing(true);
    	
	    	lblTurnosBib = new Label();
	    	lblTurnosBib.setValue("A BIB.: ");
	    	lblTurnosBib.setWidth(anchoLabelTitulo);    	
	    	lblTurnosBib.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosBib = new Label();
	    	lblValorTurnosBib.setWidth(anchoLabelDatos);
	    	lblValorTurnosBib.addStyleName("lblDatos");    	
	    	Double turnosBibEtt = this.mapeo.getHashValores().get("0.horasBib.Ett")/horasJornada;
	    	lblValorTurnosBib.setValue(RutinasNumericas.formatearDouble((this.mapeo.getHashValores().get("0.horasBib.0111")/horasJornada)-turnosBibEtt));
    	
	    	lblValorRestoB1= new Label();
	    	lblValorRestoB1.setWidth(anchoLabelDatos);
	    	lblValorRestoB1.addStyleName("lblDatos");
	    	resto = resto - ((this.mapeo.getHashValores().get("0.horasBib.0111")/horasJornada)-turnosBibEtt);
	    	lblValorRestoB1.setValue(RutinasNumericas.formatearDouble(resto));

    	lin1.addComponent(lblTurnosBib);
    	lin1.addComponent(lblValorTurnosBib);
    	lin1.addComponent(lblValorRestoB1);
    	
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	
	    	lblTurnosRetrabajos = new Label();
	    	lblTurnosRetrabajos.setValue("A Retrabajos: ");
	    	lblTurnosRetrabajos.setWidth(anchoLabelTitulo);
	    	lblTurnosRetrabajos.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosRetrabajos = new Label();
	    	lblValorTurnosRetrabajos.setWidth(anchoLabelDatos);
//	    	Double turnosRetrabajos = new Double(2);
	    	Double turnosRetrabajos = this.mapeo.getHashValores().get("0.horasEmbotellado.RT")/horasJornada;
	    	
	    	lblValorTurnosRetrabajos.setValue(RutinasNumericas.formatearDouble(turnosRetrabajos));
	    	lblValorTurnosRetrabajos.addStyleName("lblDatos");

	    	lblValorRestoB2= new Label();
	    	lblValorRestoB2.setWidth(anchoLabelDatos);
	    	lblValorRestoB2.addStyleName("lblDatos");
	    	resto = resto - turnosRetrabajos;
	    	lblValorRestoB2.setValue(RutinasNumericas.formatearDouble(resto));

    	lin2.addComponent(lblTurnosRetrabajos);
    	lin2.addComponent(lblValorTurnosRetrabajos);
    	lin2.addComponent(lblValorRestoB2);

    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	
	    	lblTurnosEnvasado = new Label();
	    	lblTurnosEnvasado.setValue("A Vino Mesa: ");
	    	lblTurnosEnvasado.setWidth(anchoLabelTitulo);
	    	lblTurnosEnvasado.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosEnvasado = new Label();
	    	lblValorTurnosEnvasado.setWidth(anchoLabelDatos);
	    	Double turnosMesaCalculados = resto;
	    	lblValorTurnosEnvasado.setValue(RutinasNumericas.formatearDouble(turnosMesaCalculados));
	    	lblValorTurnosEnvasado.addStyleName("lblDatos");

	    	lblValorRestoB3= new Label();
	    	lblValorRestoB3.setWidth(anchoLabelDatos);
	    	lblValorRestoB3.addStyleName("lblDatos");
	    	resto = resto - turnosMesaCalculados;
	    	lblValorRestoB3.setValue(RutinasNumericas.formatearDouble(resto));

    	lin3.addComponent(lblTurnosEnvasado);
    	lin3.addComponent(lblValorTurnosEnvasado);
    	lin3.addComponent(lblValorRestoB3);
    	
    	content.addComponent(lin);
    	content.addComponent(lin1);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	return panel;
    }    
    
    private Panel cargoPanelConsumoEnvasadora()
    {
    	
    	String anchoLabelTitulo = "130px";
    	String anchoLabelDatos = "150px";
    	Double resto = new Double(0);
    	
    	Label lblTurnosEmbotellado = null;
    	Label lblValorTurnosEmbotellado= null;
    	
    	Label lblTurnosEnvasado= null;
    	Label lblValorTurnosEnvasado= null;

    	Label lblTurnosRetrabajos= null;
    	Label lblValorTurnosRetrabajos= null;

    	Label lblTurnosEtt= null;
    	Label lblValorTurnosEtt= null;
    	
    	Label lblValorRestoV= null;
    	Label lblValorRestoV1= null;
    	Label lblValorRestoV2= null;
    	Label lblValorRestoV3= null;

    	Panel panel = new Panel("Turnos Envasadora");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	resto = turnosLaborablesV.doubleValue();
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	
	    	lblTurnosEnvasado = new Label();
	    	lblTurnosEnvasado.setValue("Vino Mesa: ");
	    	lblTurnosEnvasado.setWidth(anchoLabelTitulo);
	    	lblTurnosEnvasado.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosEnvasado = new Label();
	    	lblValorTurnosEnvasado.setWidth(anchoLabelDatos);
	    	Double turnosMesaCalculados = (this.mapeo.getHashValores().get("0.horasEnvasado.5L") + this.mapeo.getHashValores().get("0.horasEnvasado.2L")) / horasJornada;
	    	lblValorTurnosEnvasado.setValue(RutinasNumericas.formatearDouble(turnosMesaCalculados));
	    	lblValorTurnosEnvasado.addStyleName("lblDatos");
	    	
	    	lblValorRestoV= new Label();
	    	lblValorRestoV.setWidth(anchoLabelDatos);
	    	lblValorRestoV.addStyleName("lblDatos");
	    	resto = resto - turnosMesaCalculados;
	    	lblValorRestoV.setValue(RutinasNumericas.formatearDouble(resto));
    	
    	lin.addComponent(lblTurnosEnvasado);
    	lin.addComponent(lblValorTurnosEnvasado);
    	lin.addComponent(lblValorRestoV);
	    	

    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	
	    	lblTurnosRetrabajos = new Label();
	    	lblTurnosRetrabajos.setValue("A Retrabajos: ");
	    	lblTurnosRetrabajos.setWidth(anchoLabelTitulo);
	    	lblTurnosRetrabajos.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosRetrabajos = new Label();
	    	lblValorTurnosRetrabajos.setWidth(anchoLabelDatos);
	    	Double turnosMesaRetrabajos = new Double(0);
	    	lblValorTurnosRetrabajos.setValue(RutinasNumericas.formatearDouble(turnosMesaRetrabajos));
	    	lblValorTurnosRetrabajos.addStyleName("lblDatos");
    	
	    	lblValorRestoV1= new Label();
	    	lblValorRestoV1.setWidth(anchoLabelDatos);
	    	lblValorRestoV1.addStyleName("lblDatos");
	    	resto = resto - turnosMesaRetrabajos;
	    	lblValorRestoV1.setValue(RutinasNumericas.formatearDouble(resto));
    	
    	lin2.addComponent(lblTurnosRetrabajos);
    	lin2.addComponent(lblValorTurnosRetrabajos);
    	lin2.addComponent(lblValorRestoV1);

    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
			lblTurnosEtt= new Label();
	    	lblTurnosEtt.setValue("De Ett: ");
	    	lblTurnosEtt.setWidth(anchoLabelTitulo);
	    	lblTurnosEtt.addStyleName("lblTitulo ");
	    	
	    	lblValorTurnosEtt= new Label();
	    	lblValorTurnosEtt.setWidth(anchoLabelDatos);
	    	lblValorTurnosEtt.addStyleName("lblDatos");
	    	Double turnosMesaRecibidosEtt = this.mapeo.getHashValores().get("0.horasEnvasado.Ett")/horasJornada;
	    	lblValorTurnosEtt.setValue(RutinasNumericas.formatearDouble(turnosMesaRecibidosEtt));
		
	    	lblValorRestoV3= new Label();
	    	lblValorRestoV3.setWidth(anchoLabelDatos);
	    	lblValorRestoV3.addStyleName("lblDatos");
	    	resto = resto + turnosMesaRecibidosEtt;
	    	lblValorRestoV3.setValue(RutinasNumericas.formatearDouble(resto));
    	
		lin4.addComponent(lblTurnosEtt);
		lin4.addComponent(lblValorTurnosEtt);
		lin4.addComponent(lblValorRestoV3);
		
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	
			lblTurnosEmbotellado= new Label();
	    	lblTurnosEmbotellado.setValue("De Embotellado: ");
	    	lblTurnosEmbotellado.setWidth(anchoLabelTitulo);
	    	lblTurnosEmbotellado.addStyleName("lblTitulo ");
	    	
	    	lblValorTurnosEmbotellado= new Label();
	    	lblValorTurnosEmbotellado.setWidth(anchoLabelDatos);
	    	lblValorTurnosEmbotellado.addStyleName("lblDatos");
	    	Double turnosMesaRecibidosEmbotellado = turnosMesaCalculados - turnosLaborablesV - turnosMesaRetrabajos - turnosMesaRecibidosEtt;
	    	lblValorTurnosEmbotellado.setValue(RutinasNumericas.formatearDouble(turnosMesaRecibidosEmbotellado));
		
	    	lblValorRestoV2= new Label();
	    	lblValorRestoV2.setWidth(anchoLabelDatos);
	    	lblValorRestoV2.addStyleName("lblDatos");
	    	resto = resto + turnosMesaRecibidosEmbotellado ;
	    	lblValorRestoV2.setValue(RutinasNumericas.formatearDouble(resto));
    	
		lin3.addComponent(lblTurnosEmbotellado);
		lin3.addComponent(lblValorTurnosEmbotellado);
		lin3.addComponent(lblValorRestoV2);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin4);
    	content.addComponent(lin3);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	return panel;
    }    
    
    private Panel cargoPanelConsumoBib()
    {
    	
    	String anchoLabelTitulo = "130px";
    	String anchoLabelDatos = "150px";
    	
    	Label lblTurnosBib = null;
    	Label lblValorTurnosBib= null;
    	
    	Label lblTurnosETT= null;
    	Label lblValorTurnosETT= null;

    	Label lblTurnosEmbotellado= null;
    	Label lblValorTurnosEmbotellado= null;

    	Panel panel = new Panel("Turnos BIB");
    	panel.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout content = new VerticalLayout();
    	
    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	
	    	lblTurnosBib = new Label();
	    	lblTurnosBib.setValue("BIB: ");
	    	lblTurnosBib.setWidth(anchoLabelTitulo);
	    	lblTurnosBib.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosBib = new Label();
	    	lblValorTurnosBib.setWidth(anchoLabelDatos);
	    	Double turnosBibCalculados = (this.mapeo.getHashValores().get("0.horasBib.0111")) / horasJornada;
	    	lblValorTurnosBib.setValue(RutinasNumericas.formatearDouble(turnosBibCalculados));
	    	lblValorTurnosBib.addStyleName("lblDatos");
	    	
		lin.addComponent(lblTurnosBib);
		lin.addComponent(lblValorTurnosBib);

    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	
	    	lblTurnosETT = new Label();
	    	lblTurnosETT.setValue("De ETT: ");
	    	lblTurnosETT.setWidth(anchoLabelTitulo);
	    	lblTurnosETT.addStyleName("lblTitulo");
	    	
	    	lblValorTurnosETT = new Label();
	    	lblValorTurnosETT.setWidth(anchoLabelDatos);
	    	Double turnosBibEtt = this.mapeo.getHashValores().get("0.horasBib.Ett")/horasJornada;
	    	lblValorTurnosETT.setValue(RutinasNumericas.formatearDouble(turnosBibEtt));
	    	lblValorTurnosETT.addStyleName("lblDatos");
    	
    	lin2.addComponent(lblTurnosETT);
    	lin2.addComponent(lblValorTurnosETT);

    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	
			lblTurnosEmbotellado= new Label();
	    	lblTurnosEmbotellado.setValue("De Embotellado: ");
	    	lblTurnosEmbotellado.setWidth(anchoLabelTitulo);
	    	lblTurnosEmbotellado.addStyleName("lblTitulo ");
	    	
	    	lblValorTurnosEmbotellado= new Label();
	    	lblValorTurnosEmbotellado.setWidth(anchoLabelDatos);
	    	lblValorTurnosEmbotellado.addStyleName("lblDatos");
	    	Double turnosBibRecibidosEmbotellado = turnosBibCalculados - turnosBibEtt ;
	    	lblValorTurnosEmbotellado.setValue(RutinasNumericas.formatearDouble(turnosBibRecibidosEmbotellado));
		
		lin3.addComponent(lblTurnosEmbotellado);
		lin3.addComponent(lblValorTurnosEmbotellado);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	return panel;
    }    

    private HorizontalLayout agregarLinea(String r_linea, Double r_coste, Double r_turnos, Double r_produccion)
    {
    	String anchoLabelTitulo = "150px";

    	Label lblLinea = null;
    	Label lblCosteHora = null;
    	Label lblTurnos = null;
    	Label lblTotalHoras = null;
    	Label lblCosteAnual = null;
    	Label lblProduccionAnual = null;
    	Label lblCosteUnidad = null;
    	
    	HorizontalLayout linea = new HorizontalLayout();
    	linea.setSpacing(true);
    	
    	lblLinea = new Label();
    	lblLinea.setValue(r_linea);
    	lblLinea.setWidth(anchoLabelTitulo);
    	lblLinea.addStyleName("lblTituloDerecha");
    	
    	lblCosteHora = new Label();
    	lblCosteHora.setValue(RutinasNumericas.formatearDouble(r_coste));
    	lblCosteHora.setWidth(anchoLabelTitulo);
    	lblCosteHora.addStyleName("lblDatos");
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue(RutinasNumericas.formatearDouble(r_turnos));
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblDatos");

    	Double horasAño = (r_turnos) * 8;
    	
	    	lblTotalHoras= new Label();
	    	lblTotalHoras.setValue(horasAño.toString());
	    	lblTotalHoras.setWidth(anchoLabelTitulo);
	    	lblTotalHoras.addStyleName("lblDatos");
	    	
    	Double costeAnual = horasAño * (r_coste);
	    
	    	lblCosteAnual = new Label();
	    	if (costeAnual!=0)
	    		lblCosteAnual.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(costeAnual.toString())));
	    	else
	    		lblCosteAnual.setValue("");
	    	lblCosteAnual.setWidth(anchoLabelTitulo);
	    	lblCosteAnual.addStyleName("lblDatos");
    	
    	lblProduccionAnual = new Label();
    	if (r_produccion!=0)
    		lblProduccionAnual.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(r_produccion.toString())));
    	else
    		lblProduccionAnual.setValue("");
    	lblProduccionAnual.setWidth(anchoLabelTitulo);
    	lblProduccionAnual.addStyleName("lblDatos");

    	Double costeUd = new Double(costeAnual / r_produccion);
    	String costeUnitario = RutinasNumericas.formatearDoubleDecimales(costeUd, 6);
    	
	    	lblCosteUnidad = new Label();
	    	if (r_produccion!=0)
	    		lblCosteUnidad.setValue(RutinasCadenas.reemplazarPuntoMiles(costeUnitario));
	    	else
	    		lblCosteUnidad.setValue("");
	    	lblCosteUnidad.setWidth(anchoLabelTitulo);
	    	lblCosteUnidad.addStyleName("lblDatos");
    	
    	linea.addComponent(lblLinea);
    	linea.addComponent(lblCosteHora);
    	linea.addComponent(lblTurnos);
    	linea.addComponent(lblTotalHoras);
    	linea.addComponent(lblCosteAnual);
    	linea.addComponent(lblProduccionAnual);
    	linea.addComponent(lblCosteUnidad);
    	
    	return linea;
    }
    private HorizontalLayout agregarLineaTotales(String r_linea, Double r_turnos, Double r_horas, Double r_coste, Double r_produccion)
    {
    	String anchoLabelTitulo = "150px";
    	
    	Label lblLinea = null;
    	Label lblCosteHora = null;
    	Label lblTurnos = null;
    	Label lblTotalHoras = null;
    	Label lblCosteAnual = null;
    	Label lblProduccionAnual = null;
    	Label lblCosteUnidad = null;
    	
    	HorizontalLayout linea = new HorizontalLayout();
    	linea.setSpacing(true);
    	
    	lblLinea = new Label();
    	lblLinea.setValue("");
    	lblLinea.setWidth(anchoLabelTitulo);
    	lblLinea.addStyleName("lblTituloDerecha");
    	
    	lblCosteHora = new Label();
    	lblCosteHora.setValue(r_linea);
    	lblCosteHora.setWidth(anchoLabelTitulo);
    	lblCosteHora.addStyleName("lblTituloDerecha");
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(r_turnos.toString())));
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblTituloDerecha");
    	
    	lblTotalHoras = new Label();
    	lblTotalHoras.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(r_horas.toString())));
    	lblTotalHoras.setWidth(anchoLabelTitulo);
    	lblTotalHoras.addStyleName("lblTituloDerecha");

    	lblCosteAnual = new Label();    	
    	lblCosteAnual.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(r_coste.toString())));
    	lblCosteAnual.setWidth(anchoLabelTitulo);
    	lblCosteAnual.addStyleName("lblTituloDerecha");
    	
    	lblProduccionAnual = new Label();
    	lblProduccionAnual.setValue(RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(r_produccion.toString())));
    	lblProduccionAnual.setWidth(anchoLabelTitulo);
    	lblProduccionAnual.addStyleName("lblTituloDerecha");
    	
    	Double costeUd = new Double(r_coste / r_produccion);
    	String costeUnitario = RutinasNumericas.formatearDoubleDecimales(costeUd, 6);
    	
    	lblCosteUnidad = new Label();
    	lblCosteUnidad.setValue(RutinasCadenas.reemplazarPuntoMiles(costeUnitario));
    	lblCosteUnidad.setWidth(anchoLabelTitulo);
    	lblCosteUnidad.addStyleName("lblTituloDerecha");
    	
    	linea.addComponent(lblLinea);
    	linea.addComponent(lblCosteHora);
    	linea.addComponent(lblTurnos);
    	linea.addComponent(lblTotalHoras);
    	linea.addComponent(lblCosteAnual);
    	linea.addComponent(lblProduccionAnual);
    	linea.addComponent(lblCosteUnidad);
    	
    	return linea;
    }
    private Panel cargoPanelCostesProduccion()
    {
    	String anchoLabelTitulo = "150px";
    	String anchoLabelDatos = "150px";
    	String area = null;
    	Double turnos = null;
    	Double horas = null;
    	Double coste = null;
    	Double costeTotal = null;
    	Double horasTotal = null;
    	Double turnosTotal = null;
    	Double produccion = null;

    	Label lblLinea = null;
    	Label lblCosteHora = null;
    	Label lblTurnos = null;
    	Label lblTotalHoras = null;
    	Label lblCosteAnual = null;
    	Label lblProduccionAnual = null;
    	Label lblCosteUnidad = null;
    	
    	Panel panelm = new Panel("COSTE PRODUCCION");
    	panelm.setSizeUndefined();
    	
    	// Create the content
    	VerticalLayout contentm = new VerticalLayout();
    	
    	HorizontalLayout linM= new HorizontalLayout();
    	linM.setSpacing(true);

    	lblLinea = new Label();
    	lblLinea.setValue("Línea");
    	lblLinea.setWidth(anchoLabelTitulo);
    	lblLinea.addStyleName("lblTituloDerecha");
    	
    	lblCosteHora = new Label();
    	lblCosteHora.setValue("Coste Hora");
    	lblCosteHora.setWidth(anchoLabelTitulo);
    	lblCosteHora.addStyleName("lblTituloDerecha");
    	
    	lblTurnos = new Label();
    	lblTurnos.setValue("Turnos:");
    	lblTurnos.setWidth(anchoLabelTitulo);
    	lblTurnos.addStyleName("lblTituloDerecha");

    	lblTotalHoras= new Label();
    	lblTotalHoras.setValue("Horas Año");
    	lblTotalHoras.setWidth(anchoLabelTitulo);
    	lblTotalHoras.addStyleName("lblTituloDerecha");

    	lblCosteAnual = new Label();
    	lblCosteAnual.setValue("Coste Anual");
    	lblCosteAnual.setWidth(anchoLabelTitulo);
    	lblCosteAnual.addStyleName("lblTituloDerecha");
    	
    	lblProduccionAnual = new Label();
    	lblProduccionAnual.setValue("Produccion Anual");
    	lblProduccionAnual.setWidth(anchoLabelTitulo);
    	lblProduccionAnual.addStyleName("lblTituloDerecha");

    	lblCosteUnidad = new Label();
    	lblCosteUnidad.setValue("Coste Unitario");
    	lblCosteUnidad.setWidth(anchoLabelTitulo);
    	lblCosteUnidad.addStyleName("lblTituloDerecha");

    	linM.addComponent(lblLinea);
    	linM.addComponent(lblCosteHora);
    	linM.addComponent(lblTurnos);
    	linM.addComponent(lblTotalHoras);
    	linM.addComponent(lblCosteAnual);
    	linM.addComponent(lblProduccionAnual);
    	linM.addComponent(lblCosteUnidad);

    	contentm.addComponent(linM);
    	
    	// EMBOTELLADORA
    	costeTotal = new Double(0);
    	horasTotal = new Double(0);
    	turnosTotal = new Double(0);
    	area = "EMBOTELLADORA";
    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble(this.mapeo.getHashValores().get("0.horasEmbotellado.0102")/horasJornada,2);
    	horas = this.mapeo.getHashValores().get("0.horasEmbotellado.0102");
    	coste = RutinasNumericas.formatearDoubleDecimalesDouble(this.calculoCosteHoraProduccion("EMBOTELLADORA", "EMBOTELLADORA", horas.intValue(), false),2);
    	produccion = this.mapeo.getHashValores().get("0.prod.0102");//
    	
    	HorizontalLayout linea = agregarLinea(area,coste,turnos,new Double(0));
    	costeTotal = turnos*8*coste;
    	turnosTotal = turnosTotal + turnos;
    	horasTotal = horasTotal + horas;
    	
    	contentm.addComponent(linea);

    	area = "Total ";
    	linea = agregarLineaTotales(area,turnosTotal, horasTotal, costeTotal,produccion);
    	contentm.addComponent(linea);
    	
    	// ENVASADORA
    	
    	double turnosV = 0;
    	double turnosVR = 0;
    	double turnosVE = 0;
    	double turnosVB = 0;
    	double turnosVT = 0;
    	
    	costeTotal = new Double(0);
    	horasTotal = new Double(0);
    	turnosTotal = new Double(0);
    	area = "ENVASADORA";
    	turnosVT = RutinasNumericas.formatearDoubleDecimalesDouble((this.mapeo.getHashValores().get("0.horasEnvasado.5L") + this.mapeo.getHashValores().get("0.horasEnvasado.2L")) / horasJornada,2);
    	
    	if (new Double(turnosVT).compareTo(new Double(turnosJornadaV))>0)
    	{
    		horas = new Double(turnosLaborablesV*8);
    		turnos = new Double(turnosLaborablesV);
    	}
    	else
    	{
    		turnos = turnos - new Double(0) - this.mapeo.getHashValores().get("0.horasEnvasado.Ett");
    		horas = (this.mapeo.getHashValores().get("0.horasEnvasado.5L") + this.mapeo.getHashValores().get("0.horasEnvasado.2L") - this.mapeo.getHashValores().get("0.horasEnvasado.Ett"));
    	}
    	
//    	horas = RutinasNumericas.formatearDoubleDecimalesDouble(horas,2);
//    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble(turnos,2);    		
    	coste = this.calculoCosteHoraProduccion("ENVASADORA", "ENVASADORA", horas.intValue(), false);
    	produccion = new Double(0);
    	
    	linea = agregarLinea(area,coste,turnos,produccion);
    	costeTotal = costeTotal + turnos*8*coste;
    	turnosTotal = turnosTotal + turnos;
    	horasTotal = horasTotal + horas;
    	
    	contentm.addComponent(linea);
    	turnosVE = RutinasNumericas.formatearDoubleDecimalesDouble(this.mapeo.getHashValores().get("0.horasEnvasado.Ett")/horasJornada,2);
    	
    	if (turnosVE!=0)
    	{
	    	area = "ENVASADORA Ett";
	    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble(this.mapeo.getHashValores().get("0.horasEnvasado.Ett")/horasJornada,2);
	    	horas = horas + RutinasNumericas.formatearDoubleDecimalesDouble(this.mapeo.getHashValores().get("0.horasEnvasado.Ett"),2);
	    	coste = (this.calculoCosteHoraProduccion("ENVASADORA", "ENVASADORA", horas.intValue(), true));
	    	
	    	linea = agregarLinea(area,coste,turnos,produccion);
	    	contentm.addComponent(linea);
	    	
	    	costeTotal = costeTotal + turnos*8*coste;
	    	turnosTotal = turnosTotal + turnos;

    	}
    	turnosVB =turnosVT - turnosLaborablesV - turnosVR - turnosVE; 
    	if (turnosVB>0)
    	{
	    	area = "ENVASADORA Emb";
	    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble(turnosVB,2);
	    	horas = horas + (turnos*8);
	    	coste = (this.calculoCosteHoraProduccion("EMBOTELLADORA", "ENVASADORA", horas.intValue(), false));
	    	
	    	linea = agregarLinea(area,coste,turnos,produccion);
	    	contentm.addComponent(linea);
	    	
	    	costeTotal = costeTotal + turnos*8*coste;
	    	turnosTotal = turnosTotal + turnos;

    	}
    		
    	area = "Total ";
    	produccion = this.mapeo.getHashValores().get("0.prod.0103");
    	linea = agregarLineaTotales(area,turnosTotal, turnosTotal *8 ,costeTotal,produccion);
    	contentm.addComponent(linea);

    	
    	// BIB
    	costeTotal = new Double(0);
    	horasTotal = new Double(0);
    	turnosTotal = new Double(0);
    	area = "BIB";
    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble((this.mapeo.getHashValores().get("0.horasBib.0111") / horasJornada - this.mapeo.getHashValores().get("0.horasBib.Ett")/horasJornada),2);
    	horas = this.mapeo.getHashValores().get("0.horasBib.0111")+this.mapeo.getHashValores().get("0.horasBib.Ett");
    	coste = RutinasNumericas.formatearDoubleDecimalesDouble(this.calculoCosteHoraProduccion("BIB","BIB", horas.intValue(), false),2);
    	produccion = new Double(0);

    	linea = agregarLinea(area,coste,turnos,produccion);
    	costeTotal = turnos*8*coste;
    	turnosTotal = turnosTotal + turnos;
    	horasTotal = horas;

    	contentm.addComponent(linea);

    	// BIB-ett
    	area = "BIB Ett";
    	turnos = RutinasNumericas.formatearDoubleDecimalesDouble(this.mapeo.getHashValores().get("0.horasBib.Ett")/horasJornada,2);
    	horas = this.mapeo.getHashValores().get("0.horasBib.Ett")+this.mapeo.getHashValores().get("0.horasBib.0111");
    	coste = RutinasNumericas.formatearDoubleDecimalesDouble(this.calculoCosteHoraProduccion("BIB","BIB", horas.intValue(), true),2);
    	produccion = new Double(0);

    	linea = agregarLinea(area,coste,turnos,produccion);
    	costeTotal = costeTotal + (turnos*8*coste);    	
    	turnosTotal = turnosTotal + turnos;
    	horasTotal = horas;

    	contentm.addComponent(linea);
    	
    	// Total BIB
    	area = "Total ";
    	produccion = this.mapeo.getHashValores().get("0.prod.0111");
    	
    	linea = agregarLineaTotales(area,turnosTotal, turnosTotal * 8 , costeTotal,produccion);
    	contentm.addComponent(linea);
    	
    	contentm.setSizeUndefined(); // Shrink to fit
    	contentm.setSpacing(false);
    	contentm.setMargin(true);
    	panelm.setContent(contentm);
    	
    	return panelm;
    	
    }
    
    private void vaciarPantalla()
    {
        centralMiddle=null;
        centralBottom=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }

	private String calcularDiasFinDeSemana()
	{
		String diasTotales = null;
		
		Date d = RutinasFechas.conversionDeString("01/01/" + this.txtEjercicio.getValue());
		
		int semanasAnio = RutinasFechas.semanasAño(RutinasFechas.añoFecha(d));
		int diasFinSemana = 2 * semanasAnio;
		
  		if (RutinasFechas.esBisiesto(RutinasFechas.añoFecha(d)))
  		{
//  			System.out.println("El dia 1 es... " + RutinasFechas.obtenerDiaDeLaSemana(d));
//  			System.out.println("El dia 1 es... " + RutinasFechas.obtenerDiaDeLaSemanaLetra(d));
  			if (RutinasFechas.obtenerDiaDeLaSemana(d)==7 || RutinasFechas.obtenerDiaDeLaSemana(d)==6)
  			{
  				diasFinSemana = diasFinSemana + 2;
//  				System.out.println("Dias Fin de semana " + diasFinSemana);
  			}
  			else if (RutinasFechas.obtenerDiaDeLaSemana(d)==8)
  			{
  				diasFinSemana = diasFinSemana + 1;
//  				System.out.println("Dias Fin de semana " + diasFinSemana);
  			}
  			Date df = RutinasFechas.conversionDeString("31/12/" + this.txtEjercicio.getValue());
//  			System.out.println("El dia 31 es... " + RutinasFechas.obtenerDiaDeLaSemana(df));
//  			System.out.println("El dia 31 es... " + RutinasFechas.obtenerDiaDeLaSemanaLetra(df));
  			
  			if (RutinasFechas.obtenerDiaDeLaSemana(df)==7)
  			{
  				diasFinSemana = diasFinSemana -1;
//  				System.out.println("Dias Fin de semana " + diasFinSemana);
  			}
  			
  			else if (RutinasFechas.obtenerDiaDeLaSemana(df)==6)
  			{
  				diasFinSemana = diasFinSemana -2;
//  				System.out.println("Dias Fin de semana " + diasFinSemana);
  			}
  		}
  		diasFds=diasFinSemana;
  		diasTotales = new Integer(diasFinSemana).toString();
		return diasTotales;
		
	}
	
	private String obtenerParametro(String r_valorBuscado)
	{
		consultaParametrosCalidadServer cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
		String valor = cpcs.recuperarParametro(new Integer(this.txtEjercicio.getValue()), r_valorBuscado);
		if (valor == null) valor = "0";
		return valor;
	}
	
	private Long obtenerParametro(String r_area, String r_valorBuscado)
	{
		consultaCosteMaquinaServer cms = consultaCosteMaquinaServer.getInstance(CurrentUser.get());
		Long valor = cms.recuperarValor(new Integer(this.txtEjercicio.getValue()), r_area, r_valorBuscado);
		if (valor == null) valor = new Long(0);
		return valor;
	}

	private Double calculoCosteHoraProduccion(String r_areaD, String r_areaI, Integer r_horas, boolean r_esEtt)
	{
		Integer ejercicio = new Integer(this.txtEjercicio.getValue());
		Double costeHora = new Double(0);
		Double costeDirecto = new Double(0);
		Double costeIndirecto = new Double(0);
		
		consultaCostePersonalServer cps = consultaCostePersonalServer.getInstance(CurrentUser.get());
		
		costeIndirecto = cps.recuperarCosteIndirectoHoraArea(ejercicio, r_areaI);
		costeDirecto = cps.recuperarCosteHoraGlobalArea(ejercicio, r_areaD, r_esEtt);
		
		costeHora = costeIndirecto/r_horas+costeDirecto;
		
		return costeHora;
	}

	private Double calculoCosteProduccion(Double costeHora, Integer r_horas, Integer r_produccion)
	{
		Double costeProduccion = new Double(0);

		costeProduccion = new Double(r_produccion / (r_horas * costeHora));
		
		return costeProduccion;
	}
	
	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	

}