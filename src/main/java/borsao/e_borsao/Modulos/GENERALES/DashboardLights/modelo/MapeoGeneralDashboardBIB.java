package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardBIB extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardBIB> vectorProduccion = null;
	private ArrayList<MapeoDashboardBIB> vectorVentas= null;
	private ArrayList<MapeoDashboardBIB> vectorVentasAnterior= null;
	
	public MapeoGeneralDashboardBIB()
	{
	}

	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardBIB> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardBIB> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardBIB> getVectorVentas() {
		return vectorVentas;
	}

	public void setVectorVentas(ArrayList<MapeoDashboardBIB> vectorVentas) {
		this.vectorVentas = vectorVentas;
	}

	public ArrayList<MapeoDashboardBIB> getVectorVentasAnterior() {
		return vectorVentasAnterior;
	}

	public void setVectorVentasAnterior(ArrayList<MapeoDashboardBIB> vectorVentasAnterior) {
		this.vectorVentasAnterior = vectorVentasAnterior;
	}

	
}