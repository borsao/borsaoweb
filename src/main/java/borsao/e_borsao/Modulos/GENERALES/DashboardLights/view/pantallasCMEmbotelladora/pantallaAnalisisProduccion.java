package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallasCMEmbotelladora;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.vaadin.haijian.ExcelExporter;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladoraProduccionAnalizada;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardEmbotelladoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAnalisisProduccion extends Window
{

	private String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    private Combo cmbTipo = null;
    
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;
    private Grid gridDatos = null;
    private ExcelExporter excelExporter = null;
    private Table tabla = null;
    
    private Panel panelResumenSemana =null;
    private VerticalLayout centralTopSemana = null;
    
	public TextField txtEjercicio= null;

	private String ejercicio = null;
	private String semana = null;
	private String ambitoTemporal = null;
	private String produccionAnalizar = null;
	private String calculoProduccion = null;

	private boolean verMenu = true;
	public boolean acceso = true;
	
	public pantallaAnalisisProduccion(String r_tipoAnalizar, String r_ejercicio, String r_semana, String r_calculo, String r_campo)
	{
		
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			this.produccionAnalizar = r_tipoAnalizar;
			this.calculoProduccion = r_campo;
			this.titulo= "Análisis producción " + r_tipoAnalizar;
			this.semana = r_semana;
			this.ambitoTemporal=r_calculo;
			this.ejercicio = r_ejercicio;
			this.setCaption("Analisis Producción");
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	public pantallaAnalisisProduccion(String r_ejercicio, String r_semana, String r_calculo, String r_campo)
	{
		
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			verMenu = false;
			this.produccionAnalizar = "VENTAS";
			this.calculoProduccion = r_campo;
			this.titulo= "Análisis Ventas ";
			this.semana = r_semana;
			this.ambitoTemporal=r_calculo;
			this.ejercicio = r_ejercicio;
			this.setCaption("Analisis Producción");
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
			
			this.cargarListeners();
			cargarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		return true;
//		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
//		
//		if (permisos!=null)
//		{
//			Integer per = new Integer(permisos);
//			if (per>=80)
//			{
//				return true;
//			}
//			else 
//				return false;
//		}
//		else
//			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	this.txtEjercicio.setValue(this.ejercicio);
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		
	    		excelExporter = new ExcelExporter();
	    		
	    		excelExporter.setDateFormat("yyyy-MM-dd");
	    		excelExporter.addStyleName(ValoTheme.BUTTON_FRIENDLY);
	    		excelExporter.addStyleName(ValoTheme.BUTTON_TINY);
	    		excelExporter.setIcon(FontAwesome.DOWNLOAD);
	    		excelExporter.setCaption("Exportar");
	    		excelExporter.setDownloadFileName( txtEjercicio.getValue() + " " + titulo );

	    		this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("H1");
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
    		this.topLayoutL.addComponent(this.excelExporter);
    		this.topLayoutL.setComponentAlignment(this.excelExporter,  Alignment.BOTTOM_LEFT);
    		
//    		this.topLayoutL.addComponent(this.lblTitulo);
//	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumenSemana = new Panel(this.titulo);
	    	this.panelResumenSemana.setSizeFull();
	    	this.panelResumenSemana.addStyleName("showpointer");

		    	this.centralTopSemana = new VerticalLayout();
		    	this.centralTopSemana.setSizeFull();
		    	this.centralTopSemana.setMargin(true);

	    	this.panelResumenSemana.setContent(this.centralTopSemana);


		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
//    	this.barAndGridLayout.setExpandRatio(this.topLayoutL, 1);
    	this.barAndGridLayout.addComponent(this.panelResumenSemana);
    	this.barAndGridLayout.setExpandRatio(this.panelResumenSemana, 1);
    	
//    	this.barAndGridLayout.addComponent(this.panelResumen);
//    	this.barAndGridLayout.setExpandRatio(this.panelResumen, 2);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

		this.excelExporter.addClickListener(new ClickListener() {
	        @Override
	        public void buttonClick(ClickEvent event) {
	        	tabla = generarTablaExcel();
	    		excelExporter.setTableToBeExported(tabla);
	        }
		});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(verMenu);    			
    			close();
    		}
    	});
    }
    
	private void cargarGrid()
	{
		ArrayList<MapeoDashboardEmbotelladoraProduccionAnalizada> vector = null;
//		DecimalFormat formato = new DecimalFormat("###.###,##");
		Iterator<MapeoDashboardEmbotelladoraProduccionAnalizada> iterator = null;
		
		centralTopSemana.removeAllComponents();

		consultaDashboardEmbotelladoraServer cdes = consultaDashboardEmbotelladoraServer.getInstance(CurrentUser.get());
		vector = cdes.produccionAnalizada(this.produccionAnalizar,this.ejercicio, this.semana, this.ambitoTemporal, this.calculoProduccion);

		if (vector!=null && !vector.isEmpty())
		{
	    	IndexedContainer container =null;
	    		
			container = new IndexedContainer();
			container.addContainerProperty("familia", String.class, null);
			container.addContainerProperty("nombreFamilia", String.class, null);
			container.addContainerProperty("subfamilia", String.class, null);
			container.addContainerProperty("nombreSubfamilia", String.class, null);
			container.addContainerProperty("unidades", Double.class, null);
				
		    iterator = vector.iterator();
			
			while (iterator.hasNext())
			{
				MapeoDashboardEmbotelladoraProduccionAnalizada mapeo = iterator.next();
				
				Object newItemId = container.addItem();
				Item file = container.getItem(newItemId);
				
				file.getItemProperty("familia").setValue(mapeo.getFamilia());
				file.getItemProperty("nombreFamilia").setValue(mapeo.getNombreFamilia());
				file.getItemProperty("subfamilia").setValue(mapeo.getSubfamilia());
				file.getItemProperty("nombreSubfamilia").setValue(mapeo.getNombreSubfamilia());
				file.getItemProperty("unidades").setValue(mapeo.getTotal());
			}
	
			gridDatos= new Grid(container);
			gridDatos.setSelectionMode(SelectionMode.NONE);
			gridDatos.setSizeFull();

			gridDatos.addStyleName("smallgrid");
			
			gridDatos.getColumn("familia").setHeaderCaption("Familia");
			gridDatos.getColumn("familia").setWidth(150);
			gridDatos.getColumn("nombreFamilia").setHeaderCaption("Nombre Familia");
			gridDatos.getColumn("nombreFamilia").setWidth(250);
			gridDatos.getColumn("subfamilia").setHeaderCaption("Subfamilia");
			gridDatos.getColumn("subfamilia").setWidth(150);
			gridDatos.getColumn("nombreSubfamilia").setHeaderCaption("Nombre Subfamilia");
			gridDatos.getColumn("nombreSubfamilia").setWidth(250);
			gridDatos.getColumn("unidades").setHeaderCaption("Botellas Producidas");
			gridDatos.getColumn("unidades").setWidth(180);
//			gridDatos.getColumn("unidades").setRenderer(new NumberRenderer());
			
			Double totalU = new Double(0) ;
	    	Double valor = null;
	        	
	    	FooterRow footer = gridDatos.appendFooterRow();
	        	
	    	Indexed indexed = gridDatos.getContainerDataSource();
	    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
	    	
			totalU = new Double(0) ;
	    	for(Object itemId : list)
	    	{
	    		Item item = indexed.getItem(itemId);
	        		
	    		valor = (Double) item.getItemProperty("unidades").getValue();
	    		if (valor!=null)
	    			totalU += valor;
					
	    	}
	    	footer.getCell("nombreSubfamilia").setText("Totales ");
	    	footer.getCell("unidades").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
	    	footer.getCell("unidades").setStyleName("Rcell-pie");
			
	    	gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() {
	            
	            @Override
	            public String getStyle(Grid.CellReference cellReference) {
	            	if ("unidades".equals(cellReference.getPropertyId())) 
	            	{            		
	            		return "Rcell-normal";
	            	}
	            	else
	            	{
	            		return "cell-normal";
	            	}
	            }
	        });
			
			Panel panel = new Panel();
	    	panel.setSizeFull(); // Shrink to fit content
	//    	panel.setHeight("250px");
	    	panel.setContent(gridDatos);
	    	
	    	centralTopSemana.addComponent(panel);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No hay datos para mostrar");
		}
    	
    }
	
	private Table generarTablaExcel()
	{
		Table table = new Table();
		ArrayList<Object> cols = new ArrayList<Object>();

		for (int i=0;i<this.gridDatos.getColumns().size();i++)
		{
			Column c = this.gridDatos.getColumns().get(i);
			
			if (!c.isHidden())
			{
				cols.add(c.getPropertyId());
			}
		}
		
		table.setContainerDataSource(this.gridDatos.getContainerDataSource());
		//new Object[]{"ejercicio", "fechaDocumento", "documento", "idCodigoVenta", "pais", "cliente", "nombreCliente", "articulo","descripcion","unidades", "lote"}
		table.setVisibleColumns(cols.toArray());
		return table;
	}
}