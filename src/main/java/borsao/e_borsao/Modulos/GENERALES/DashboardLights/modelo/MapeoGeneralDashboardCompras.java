package borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardCompras extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardCompras> vectorProduccion = null;
	private ArrayList<MapeoDashboardCompras> vectorCompras= null;
	private ArrayList<MapeoDashboardCompras> vectorComprasAnterior= null;
	
	public MapeoGeneralDashboardCompras()
	{
	}
 
	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardCompras> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardCompras> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardCompras> getVectorCompras() {
		return vectorCompras;
	}

	public void setVectorCompras(ArrayList<MapeoDashboardCompras> vectorCompras) {
		this.vectorCompras = vectorCompras;
	}

	public ArrayList<MapeoDashboardCompras> getVectorComprasAnterior() {
		return vectorComprasAnterior;
	}

	public void setVectorComprasAnterior(ArrayList<MapeoDashboardCompras> vectorComprasAnterior) {
		this.vectorComprasAnterior = vectorComprasAnterior;
	}

	
}