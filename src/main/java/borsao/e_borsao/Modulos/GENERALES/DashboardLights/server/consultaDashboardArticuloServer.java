package borsao.e_borsao.Modulos.GENERALES.DashboardLights.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardArticulo;
import borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.server.consultaPrevisionEmbotelladosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDashboardArticuloServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaDashboardArticuloServer instance;
	public static Double horasTurno = 8.0;
	public static Integer produccionObjetivoLitros = 33750;
	public static Integer produccionObjetivoBotellas = 45000;

	
	private ArrayList<MapeoProgramacionEnvasadora> vectorProgramadoMesa = null;

	public consultaDashboardArticuloServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDashboardArticuloServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDashboardArticuloServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardArticulo recuperarValoresEstadisticosPT(String r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		Double valor = null;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 * 
		 *  Tenemos que tener en cuenta el tipo de articulo
		 *  
		 *  	- cargaremos los datos del articulo que recibimos sin mas
		 *  
		 *  	datos de produccion acumulada de este ejercicio y el anterior
		 *  	datos de planificacion acumulada de este ejercicio y el anterior
		 *  	datos de venta acumulada de este ejercicio y el anterior
		 *  	datos de venta acumulada en importes de este ejercicio y el anterior
		 *  	datos de paises y clientes compradores del articulo
		 *  
		 *  	datos de paletizacion y de stocks
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod)
		 *  	- produccion total programada del periodo						(0.plan)
		 *  	- produccion total del periodo									(1.prod)
		 *  	- produccion total programada del periodo						(1.plan)
		 *  
		 *  	- ventas totales del periodo									(0.ventas)
		 *  	- importe ventas totales del periodo							(0.ventasImp)
		 *  	- ventas totales del periodo									(1.ventas)
		 *  	- importe ventas totales del periodo							(1.ventasImp)
		 *  	- venta media del periodo										(0.ventasAvg)
		 *  	- preciov venta medio del periodo								(0.pvpVentas)		   
		 *  	- venta media del periodo anterior								(1.ventasAvg)
		 *  	- preciov venta medio del periodo anterior						(1.pvpVentas)
		 *   	
		 *   	- paletizacion													(0.palet) 
		 *   	- produccion minima 											(0.minProd) 

		 *      - stock minimo 													(0.minStock)
		 *      - stock actual													(0.stock)
		 *      - stock pedido													(0.pdteServir)
		 *      - stock preparado												(0.prep)      
		 *      
		 *      - necesidad														(0.neeed)
		 *            
		 */
		HashMap<String, Double> hashValores = null;
		HashMap<String, Double> vectorDatosStock = null;

		MapeoGeneralDashboardArticulo mapeo = null;
		consultaPrevisionEmbotelladosServer cpes =null;
		
		hashValores = new HashMap<String, Double>();
		vectorDatosStock = this.recuperarStock(new Integer(r_ejercicio), r_articulo);

		cpes = new consultaPrevisionEmbotelladosServer(CurrentUser.get());
		Integer prevision = cpes.devolverPrevisionArticulo(r_articulo, r_ejercicio);
		hashValores.put("0.pree", new Double(prevision));
		prevision = cpes.devolverRestoPrevisionArticulo(r_articulo, r_ejercicio);
		hashValores.put("0.prer", new Double(prevision));
		
		Double produccion = this.recuperarProduccion(new Integer(r_ejercicio), r_articulo, r_tipoArticulo);
		hashValores.put("0.prod", produccion);

		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-1, r_articulo, r_tipoArticulo);
		hashValores.put("1.prod", produccion);
		
		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-2, r_articulo, r_tipoArticulo);
		hashValores.put("2.prod", produccion);

		produccion = this.recuperarProduccion(new Integer(r_ejercicio)-3, r_articulo, r_tipoArticulo);
		hashValores.put("3.prod", produccion);

		Double palet = this.recuperarPalet(r_articulo);
		hashValores.put("0.palet", palet);
		hashValores.put("0.minprod", palet*5);

		if (r_articulo.startsWith("0111") || r_articulo.startsWith("0103"))
		{
			Double planificado = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio), "X", r_articulo, null));
			hashValores.put("0.plan", planificado);
			Double vecesPlanificado = new Double(this.obtenerVecesProgramadoMesa(new Integer(r_ejercicio), "T", r_articulo));
			hashValores.put("0.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramadoMesa(new Integer(r_ejercicio)-1, "T", r_articulo));
			hashValores.put("1.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramadoMesa(new Integer(r_ejercicio)-2, "T", r_articulo));
			hashValores.put("2.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramadoMesa(new Integer(r_ejercicio)-3, "T", r_articulo));
			hashValores.put("3.vecesPlan", vecesPlanificado);
			Double embotelladas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio), "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("0.embPlan", embotelladas);
			embotelladas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-1, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("1.embPlan", embotelladas);
			embotelladas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-2, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("2.embPlan", embotelladas);
			embotelladas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-3, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("3.embPlan", embotelladas);

			hashValores.put("0.etiPlan", new Double(0));
			hashValores.put("1.etiPlan", new Double(0));
			hashValores.put("2.etiPlan", new Double(0));
			hashValores.put("3.etiPlan", new Double(0));
			Double retrabajadas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio), "T", r_articulo,"RETRABAJO"));
			hashValores.put("0.retPlan", retrabajadas);
			retrabajadas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-1, "T", r_articulo,"RETRABAJO"));
			hashValores.put("1.retPlan", new Double(0));
			retrabajadas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-2 , "T", r_articulo,"RETRABAJO"));
			hashValores.put("2.retPlan", new Double(0));
			retrabajadas = new Double(this.obtenerProgramadoEnvasadora(new Integer(r_ejercicio)-3 , "T", r_articulo,"RETRABAJO"));
			hashValores.put("3.retPlan", new Double(0));

		}
		else
		{
			Double planificado = new Double(this.obtenerProgramado(new Integer(r_ejercicio), "X", r_articulo, null));
			hashValores.put("0.plan", planificado);
			Double vecesPlanificado = new Double(this.obtenerVecesProgramado(new Integer(r_ejercicio), "T", r_articulo));
			hashValores.put("0.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramado(new Integer(r_ejercicio)-1, "T", r_articulo));
			hashValores.put("1.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramado(new Integer(r_ejercicio)-2, "T", r_articulo));
			hashValores.put("2.vecesPlan", vecesPlanificado);
			vecesPlanificado = new Double(this.obtenerVecesProgramado(new Integer(r_ejercicio)-3, "T", r_articulo));
			hashValores.put("3.vecesPlan", vecesPlanificado);

			Double embotelladas = new Double(this.obtenerProgramado(new Integer(r_ejercicio), "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("0.embPlan", embotelladas);
			
			Double etiquetadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio), "T", r_articulo,"ETIQUETADO"));
			hashValores.put("0.etiPlan", etiquetadas);
			
			Double retrabajadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio), "T", r_articulo,"RETRABAJO"));
			hashValores.put("0.retPlan", retrabajadas);
			
			embotelladas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-1, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("1.embPlan", embotelladas);
			
			etiquetadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-1, "T", r_articulo,"ETIQUETADO"));
			hashValores.put("1.etiPlan", etiquetadas);
			
			retrabajadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-1, "T", r_articulo,"RETRABAJO"));
			hashValores.put("1.retPlan", retrabajadas);
			
			embotelladas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-2, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("2.embPlan", embotelladas);
			
			etiquetadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-2, "T", r_articulo,"ETIQUETADO"));
			hashValores.put("2.etiPlan", etiquetadas);
			
			retrabajadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-2, "T", r_articulo,"RETRABAJO"));
			hashValores.put("2.retPlan", retrabajadas);
			
			embotelladas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-3, "T", r_articulo,"EMBOTELLADO"));
			hashValores.put("3.embPlan", embotelladas);
			
			etiquetadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-3, "T", r_articulo,"ETIQUETADO"));
			hashValores.put("3.etiPlan", etiquetadas);
			
			retrabajadas = new Double(this.obtenerProgramado(new Integer(r_ejercicio)-3, "T", r_articulo,"RETRABAJO"));
			hashValores.put("3.retPlan", retrabajadas);

		}

		
		Integer meses = 0;
		int diaActual = 0;
		int diaFinMes = 0;
		
		diaActual = new Integer(RutinasFechas.diaActualDD());
		diaFinMes = RutinasFechas.obtenerUltimoDiaMesActual(new Integer(RutinasFechas.añoActualYYYY()), new Integer(RutinasFechas.mesActualMM()));
		
		if (new Integer(r_ejercicio)<new Integer(RutinasFechas.añoActualYYYY()))
			meses= 12; 
		else 
		{
			if (diaActual==diaFinMes)
				meses = new Integer(RutinasFechas.mesActualMM());
			else if (diaActual<diaFinMes)
				meses = new Integer(RutinasFechas.mesActualMM())-1;
			else
				meses = new Integer(RutinasFechas.mesActualMM());
		}
		
		Double ventas = this.recuperarUnidadesVentas(new Integer(r_ejercicio),meses, r_articulo);
		hashValores.put("0.ventas", ventas);

		if (meses!=0) hashValores.put("0.ventasAvg", ventas/meses); else hashValores.put("0.ventasAvg", ventas/12);
		Double ventasImp = this.recuperarImporteVentas(new Integer(r_ejercicio),meses,r_articulo);
		hashValores.put("0.ventasImp", ventasImp);
		hashValores.put("0.pvpVentas", ventasImp/ventas);
		
		Double ventasAnterior = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-1,12,r_articulo);
		hashValores.put("1.ventas", ventasAnterior);
		hashValores.put("1.ventasAvg", ventasAnterior/12);
		Double ventasImpAnterior = this.recuperarImporteVentas(new Integer(r_ejercicio)-1,12,r_articulo);
		hashValores.put("1.ventasImp", ventasImpAnterior);
		hashValores.put("1.pvpVentas", ventasImpAnterior/ventasAnterior);
		
		Double ventasAnterior2 = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-2,12,r_articulo);
		hashValores.put("2.ventas", ventasAnterior2);
		hashValores.put("2.ventasAvg", ventasAnterior2/12);
		Double ventasImpAnterior2 = this.recuperarImporteVentas(new Integer(r_ejercicio)-2,12,r_articulo);
		hashValores.put("2.ventasImp", ventasImpAnterior2);
		hashValores.put("2.pvpVentas", ventasImpAnterior2/ventasAnterior2);

		Double ventasAnterior3 = this.recuperarUnidadesVentas(new Integer(r_ejercicio)-3,12,r_articulo);
		hashValores.put("3.ventas", ventasAnterior3);
		hashValores.put("3.ventasAvg", ventasAnterior3/12);
		Double ventasImpAnterior3 = this.recuperarImporteVentas(new Integer(r_ejercicio)-3,12,r_articulo);
		hashValores.put("3.ventasImp", ventasImpAnterior3);
		hashValores.put("3.pvpVentas", ventasImpAnterior3/ventasAnterior3);
		
//		Integer cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio), r_articulo);
//		hashValores.put("0.clientes", cuantos.doubleValue());
//		cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio)-1, r_articulo);
//		hashValores.put("1.clientes", cuantos.doubleValue());
//		cuantos = recuperarClientesDisintosEjercicio(new Integer(r_ejercicio)-2, r_articulo);
//		hashValores.put("2.clientes", cuantos.doubleValue());
//		
		if (vectorDatosStock!=null && !vectorDatosStock.isEmpty())
		{
			valor = vectorDatosStock.get("0.stock");
			hashValores.put("0.stock", valor);

			valor = vectorDatosStock.get("0.minStock");
			hashValores.put("0.minStock", valor);

			valor = vectorDatosStock.get("0.pdteServir");
			hashValores.put("0.pdteServir", valor);
			
			valor = vectorDatosStock.get("0.prep");
			hashValores.put("0.prep", valor);

			valor = vectorDatosStock.get("0.prog");
			hashValores.put("0.prog", valor);
			
			valor = vectorDatosStock.get("0.stockJaulon");
			hashValores.put("0.stockJaulon", valor);

		}
		
		mapeo = new MapeoGeneralDashboardArticulo();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}
	
	
	public MapeoGeneralDashboardArticulo recuperarValoresEstadisticosMPPS(String r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		Double valor = null;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 * 
		 *  Tenemos que tener en cuenta el tipo de articulo
		 *  
		 *  	- cargaremos los datos del articulo que recibimos sin mas
		 *  
		 *  	datos de produccion acumulada de este ejercicio y el anterior
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod)
		 *  	- produccion jaulon del periodo									(0.prodJ)
		 *  	- produccion directa del periodo								(0.prodD)
		 *            
		 */
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardArticulo mapeo = null;

		hashValores = new HashMap<String, Double>();
		Double produccionDirecta = null;
		Double produccion = this.recuperarProduccionMPPS(new Integer(r_ejercicio), r_articulo, r_tipoArticulo);
		hashValores.put("0.prod", produccion);
		produccionDirecta = produccion;
		
		if (r_tipoArticulo.contentEquals("MP") && r_articulo.startsWith("0101"))
		{			
			produccion = this.recuperarProduccionJaulon(new Integer(r_ejercicio), r_articulo, "PS");
			hashValores.put("0.prodJ", produccion);
			produccionDirecta=produccionDirecta-produccion;
			hashValores.put("0.prodD", produccionDirecta);
			produccion = this.recuperarVentasPadres(new Integer(r_ejercicio), r_articulo, "PT");
			hashValores.put("0.venta", produccion);
		}
		if (r_tipoArticulo.contentEquals("PS") && r_articulo.startsWith("0102"))
		{			
			produccion = this.recuperarProduccionMPPS(new Integer(r_ejercicio), r_articulo, "MP");
			hashValores.put("0.prodJ", produccion);
			produccion = this.recuperarVentasPadres(new Integer(r_ejercicio), r_articulo, "PT");
			hashValores.put("0.venta", produccion);
		}
		mapeo = new MapeoGeneralDashboardArticulo();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}

	public MapeoGeneralDashboardArticulo recuperarValoresEstadisticosPadres(String r_ejercicio, ArrayList<MapeoArticulos> r_articulos)
	{
		Double valor = null;
		/*
		 * Ira consultando los datos necesarios a representar en la view en un hash.
		 * El hash contiene:
		 * 	- String con la clave
		 *  - Double con el valor para dicha clave
		 * 
		 *  Tenemos que tener en cuenta el tipo de articulo
		 *  
		 *  	- cargaremos los datos del articulo que recibimos sin mas
		 *  
		 *  	datos de produccion acumulada de este ejercicio y el anterior
		 *  
		 *  Claves:
		 *  
		 *  	- produccion total del periodo									(0.prod)
		 *  	- produccion jaulon del periodo									(0.prodJ)
		 *  	- produccion directa del periodo								(0.prodD)
		 *            
		 */
		HashMap<String, Double> hashValores = null;
		MapeoGeneralDashboardArticulo mapeo = null;
		Iterator<MapeoArticulos> iter = null;
		
		hashValores = new HashMap<String, Double>();
		Double produccion = null;
		
		produccion = new Double(0);
		produccion = this.recuperarProduccionPadresTotal(new Integer(r_ejercicio), r_articulos,"PT");
		hashValores.put("0.prod", produccion);
		
		produccion = new Double(0);
		
		produccion = this.recuperarVentasPadresTotal(new Integer(r_ejercicio), r_articulos, "PT");
		hashValores.put("0.venta", produccion);
		
		mapeo = new MapeoGeneralDashboardArticulo();
		mapeo.setHashValores(hashValores);
		return mapeo;
	}

	private Double recuperarPalet(String r_articulo)
	{
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());	    
	    Integer palet = ces.obtenerCantidadPaletEmbotelladora(r_articulo);	    
	    return palet.doubleValue();
	}
	
	private Double recuperarProduccion(Integer r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "49";
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_tipoArticulo.contentEquals("MP")) movimiento = "99";
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades) total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			sql.append(" and a_lin_" + digito + ".movimiento in ('"+movimiento+"') ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	
	private Double recuperarProduccionMPPS(Integer r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "49";
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_tipoArticulo.contentEquals("MP")) movimiento = "99";
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades) total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			sql.append(" and a_lin_" + digito + ".movimiento in ('"+movimiento+"') ");
			sql.append(" and a_lin_" + digito + ".almacen =4 ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	
	private Double recuperarProduccionJaulon(Integer r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "49";
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=((Integer) (ejercicioActual-r_ejercicio)).toString();

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades*e_articu.marca) total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
			sql.append(" and a_lin_" + digito + ".articulo in (select padre from e__lconj where hijo = '"+r_articulo+"') ");
			sql.append(" and a_lin_" + digito + ".movimiento in ('"+movimiento+"') ");
			sql.append(" and a_lin_" + digito + ".almacen = 4  ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	
	private Double recuperarVentasPadres(Integer r_ejercicio, String r_articulo, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "'51','53'";
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=((Integer) (ejercicioActual-r_ejercicio)).toString();

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades*e_articu.marca) total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
			
			sql.append(" and a_lin_" + digito + ".articulo in (select padre[1,7] from e__lconj where hijo = '"+r_articulo+"') ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ("+movimiento+") ");
//			sql.append(" and a_lin_" + digito + ".almacen in (1,4)  ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}
	private Double recuperarVentasPadresTotal(Integer r_ejercicio, ArrayList<MapeoArticulos> r_articulos, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "'51','53'";
		Iterator<MapeoArticulos> iter = null;
		String cadArt = "' '";
		Double total = new Double(0);
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=((Integer) (ejercicioActual-r_ejercicio)).toString();
			
			iter = r_articulos.iterator();
			
			while (iter.hasNext())
			{
				MapeoArticulos map = iter.next();
				cadArt = cadArt + ",'" + map.getArticulo().trim() + "'";
			}
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades) total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
			sql.append(" and a_lin_" + digito + ".articulo in ("+cadArt+") ");
			sql.append(" and a_lin_" + digito + ".movimiento in ("+movimiento+") ");
			sql.append(" and a_lin_" + digito + ".almacen in (1,4)  ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				total = total + rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return total;
	}

	private Double recuperarProduccionPadresTotal(Integer r_ejercicio, ArrayList<MapeoArticulos> r_articulos, String r_tipoArticulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		String movimiento = "'49'";
		Iterator<MapeoArticulos> iter = null;
		String cadArt = "' '";
		Double total = new Double(0);
		
		try
		{
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=((Integer) (ejercicioActual-r_ejercicio)).toString();
			
			iter = r_articulos.iterator();
			
			while (iter.hasNext())
			{
				MapeoArticulos map = iter.next();
				cadArt = cadArt + ",'" + map.getArticulo().trim() + "'";
			}
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento)  ejercicio, ");
			sql.append(" SUM(unidades) total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.tipo_articulo = '" + r_tipoArticulo + "' ");
			sql.append(" and a_lin_" + digito + ".articulo in ("+cadArt+") ");
			sql.append(" and a_lin_" + digito + ".movimiento in ("+movimiento+") ");
			sql.append(" and a_lin_" + digito + ".almacen in (1,4)  ");
			sql.append(" GROUP BY 1 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				total = total + rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return total;
	}

	private Double recuperarUnidadesVentas(Integer r_ejercicio, Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;
		try
		{
			
			String digito = "0";
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}

			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(unidades) total ");
			sql.append(" FROM a_lin_" + digito );
			sql.append(" where a_lin_" + digito + ".articulo = '" + r_articulo + "' ");
			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY 1 ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}

	private Double recuperarImporteVentas(Integer r_ejercicio,Integer r_mes, String r_articulo)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer ejer = r_ejercicio;
		Integer mes = r_mes;
		try
		{
			String digito = "0";
			if (r_mes == 0)
			{
				ejer = r_ejercicio - 1;
				mes = 12;
			}

			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (ejer == ejercicioActual) digito="0";
			if (ejer < ejercicioActual) digito=String.valueOf(ejer);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) ejercicio, ");
			sql.append(" SUM(importe) total ");
			sql.append(" FROM a_lin_" + digito );
			sql.append(" where a_lin_" + digito + ".articulo = '" + r_articulo + "' ");
			sql.append(" and month(fecha_documento) <= " + mes );
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY 1 ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				return rsMovimientos.getDouble("total");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return new Double(0);
	}

	private HashMap<String, Double>  recuperarStock(Integer r_ejercicio,String r_articulo)
	{
		HashMap<String, Double> hash = null;
		
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);

		hash = cis.recuperarDatosResumenStock(r_ejercicio, r_articulo, "1,4,10,20,30");

		
		if (r_articulo.startsWith("0111")||r_articulo.startsWith("0103")) 
			hash.put(digito+".prog", new Double(this.obtenerProgramadoEnvasadora(r_ejercicio, "A", r_articulo, null))); 
		else hash.put(digito+".prog", new Double(this.obtenerProgramado(r_ejercicio, "X", r_articulo, null)));

		return hash;
	}

	private Integer obtenerProgramado(Integer r_ejercicio, String r_estado, String r_articulo, String r_operacion)
	{
		ArrayList<MapeoProgramacion> vectorProgramado = null;
		MapeoProgramacion mapeo = null;
		int total = 0;
		
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		mapeo = new MapeoProgramacion();
		mapeo.setEstado(r_estado);
		mapeo.setArticulo(r_articulo);
		mapeo.setEjercicio(r_ejercicio);
		mapeo.setTipo(r_operacion);

		vectorProgramado = cps.datosProgramacionGlobal(mapeo);
		
		for (int i=0;i<vectorProgramado.size();i++)
		{
			MapeoProgramacion map = (MapeoProgramacion) vectorProgramado.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				total = total +  map.getUnidades();
			}
		}
		mapeo.setEjercicio(r_ejercicio+1);
		vectorProgramado = cps.datosProgramacionGlobal(mapeo);
		for (int i=0;i<vectorProgramado.size();i++)
		{
			MapeoProgramacion map = (MapeoProgramacion) vectorProgramado.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				total = total +  map.getUnidades();
			}
		}
		return total;
	}

	private Integer obtenerVecesProgramado(Integer r_ejercicio, String r_estado, String r_articulo)
	{
		Integer cuantos = null;
		MapeoProgramacion mapeo = null;
		
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		mapeo = new MapeoProgramacion();
		mapeo.setEstado(r_estado);
		mapeo.setArticulo(r_articulo);
		mapeo.setEjercicio(r_ejercicio);

		cuantos = cps.cuantosProgramacionGlobal(mapeo);
		
		return cuantos;
	}

	private Integer obtenerVecesProgramadoMesa(Integer r_ejercicio, String r_estado, String r_articulo)
	{
		ArrayList<MapeoProgramacionEnvasadora> vectorProgramado = null;
		MapeoProgramacionEnvasadora mapeo = null;
		
		consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		mapeo = new MapeoProgramacionEnvasadora();
		
		mapeo.setEstado(r_estado);
		mapeo.setArticulo(r_articulo);
		mapeo.setEjercicio(r_ejercicio);

		vectorProgramado = cps.datosProgramacionGlobal(mapeo);
		
		return vectorProgramado.size();
	}

	private Integer obtenerProgramadoEnvasadora(Integer r_ejercicio, String r_estado, String r_articulo,  String r_operacion)
	{
		MapeoProgramacionEnvasadora mapeo = null;
		int total = 0;
		consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		mapeo = new MapeoProgramacionEnvasadora();
		mapeo.setEstado(r_estado);
		mapeo.setArticulo(r_articulo);
		mapeo.setEjercicio(r_ejercicio);
		mapeo.setTipo(r_operacion);
		
		vectorProgramadoMesa = cps.datosProgramacionGlobal(mapeo);

		for (int i=0;i<vectorProgramadoMesa.size();i++)
		{
			MapeoProgramacionEnvasadora map = (MapeoProgramacionEnvasadora) vectorProgramadoMesa.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				total = total + map.getUnidades();
			}
		}
		return total;
	}

	public HashMap<String,	Double> recuperarMovimientosVentasMesesEjercicio(Integer r_ejercicio, String r_mascara,String r_campo)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
			sql.append(" SUM( CASE month(fecha_documento) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12' ");
			  
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento in ('51','53') ");
			sql.append(" and a_lin_" + digito + ".clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY ejercicio ");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				Integer hastaSemana = 0;
				if (digito.contentEquals("0")) hastaSemana = new Integer(RutinasFechas.mesActualMM()); else hastaSemana = 12;
				
				for (int i = 1; i<=hastaSemana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public ArrayList<MapeoValores>  recuperarVentasPaisEjercicio(Integer r_ejercicio, String r_mascara,String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoValores> vector = null;
		MapeoValores map = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(d.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT c.nombre pai, ");
			sql.append(" SUM(unidades*" + r_campo + ") venta ");
			sql.append(" FROM a_lin_" + digito + " b, a_va_c_" + digito + " a, e_paises c , e_articu d ");
			sql.append(" where b.clave_tabla = a.clave_tabla ");
			sql.append(" and b.documento = a.documento ");
			sql.append(" and b.serie = a.serie ");
			sql.append(" and b.codigo = a.codigo ");
			sql.append(" and a.pais = c.pais ");
			sql.append(" and b.articulo = d.articulo ");
			sql.append(" and b.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and b.movimiento in ('51','53') ");
			sql.append(" and b.clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY 1 ");
			sql.append(" order by 2 desc ");

				
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			vector = new ArrayList<MapeoValores>();
			
			while(rsMovimientos.next())
			{
				map = new MapeoValores();
				map.setClave(RutinasCadenas.conversion(rsMovimientos.getString("pai")));
				map.setValorDouble(rsMovimientos.getDouble("venta"));
				
				vector.add(map);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoValores>  recuperarVentasClientesEjercicio(Integer r_ejercicio, String r_mascara,String r_campo)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoValores> vector = null;
		MapeoValores map = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual - r_ejercicio);
			if (r_campo.contentEquals("Litros")) r_campo = "replace(d.marca, ',','.')"; else r_campo="1";
			
			sql = new StringBuffer();
			
			sql.append(" SELECT c.nombre_fiscal cli, ");
			sql.append(" SUM(unidades*" + r_campo + ") venta ");
			sql.append(" FROM a_lin_" + digito + " b, a_va_c_" + digito + " a, e_client c , e_articu d ");
			sql.append(" where b.clave_tabla = a.clave_tabla ");
			sql.append(" and b.documento = a.documento ");
			sql.append(" and b.serie = a.serie ");
			sql.append(" and b.codigo = a.codigo ");
			sql.append(" and a.cliente_factura = c.cliente ");
			sql.append(" and b.articulo = d.articulo ");
			sql.append(" and b.articulo like '" + r_mascara + "%' ");
			
			sql.append(" and b.movimiento in ('51','53') ");
			sql.append(" and b.clave_tabla in ('A','F') ");
			
			sql.append(" GROUP BY 1 ");
			sql.append(" order by 2 desc ");

				
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			vector = new ArrayList<MapeoValores>();
			
			while(rsMovimientos.next())
			{
				map = new MapeoValores();
				map.setClave(RutinasCadenas.conversion(rsMovimientos.getString("cli")));
				map.setValorDouble(rsMovimientos.getDouble("venta"));
				
				vector.add(map);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public Integer  recuperarClientesDisintosEjercicio(Integer r_ejercicio, String r_mascara)
	{
		ResultSet rsMovimientos = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Integer cuantos = 0;
		
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT count(unique a.cliente) cli ");
			sql.append(" FROM a_lin_" + digito + " b, a_va_c_" + digito + " a ");
			sql.append(" where b.clave_tabla = a.clave_tabla ");
			sql.append(" and b.documento = a.documento ");
			sql.append(" and b.serie = a.serie ");
			sql.append(" and b.codigo = a.codigo ");
			sql.append(" and b.articulo like '" + r_mascara + "%' ");
			sql.append(" and b.movimiento in ('51','53') ");
			sql.append(" and b.clave_tabla in ('A','F') ");
				
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			while(rsMovimientos.next())
			{
				cuantos = rsMovimientos.getInt("cli");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cuantos;
	}
	
	public HashMap<String,	Double> recuperarVentasAcumulados(Integer r_ejercicio,String  r_mascara, String r_campo, String r_semana)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		CallableStatement statement = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			
			String digito = "0";
			
			if (r_campo.contentEquals("Litros")) r_campo = "replace(e_articu.marca, ',','.')"; else r_campo="1";
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			Integer hastaSemana = 0;
//			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			
			if (r_semana.contentEquals("0") && digito.contentEquals("0"))
			{
				sql = " "; 
				hastaSemana=53;
			}
			else
			{
				sql= " and week(fecha_documento,3)<= '" + r_semana + "' ";
				hastaSemana=new Integer(r_semana);
			}
			
			con= this.conManager.establecerConexionInd();			
			
			
			statement = con.prepareCall("{call ventaAcumuladaBotella(?,?,?,?)}");

			statement.setString(1, r_campo);
			statement.setString(2, digito);
			statement.setString(3, r_mascara);
			statement.setString(4, sql);
			boolean hadResults = statement.execute();

			while (hadResults) 
			{
				rsMovimientos = statement.getResultSet();
				hash = new HashMap<String, Double>(); 
				// process result set
				while(rsMovimientos.next())
				{
					
					for (int i = 1; i<=hastaSemana;i++)
					{
						hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
					}
				}


				hadResults = statement.getMoreResults();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos(null);
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("BIB");
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="0";
				}
			}
		}
		return permisos;
	}
}
