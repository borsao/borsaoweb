package borsao.e_borsao.Modulos.GENERALES.DashboardLights.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.view.pantallaLineasAlbaranesCompra;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.pantallaLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoGeneralDashboardCompras;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardComprasServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view.pantallaLineasProgramacionesEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardComprasView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Compras";
	private final String titulo = "CUADRO MANDO COMPRAS";
	private final int intervaloRefresco = 0*60*1000;
	private ChatRefreshListener cr = null;
	private ventanaAyuda vHelp  =null;
	
	private String articuloSeleccionado = null;
	private String descripcionArticuloSeleccionado = null;
	private String articuloPadre = null;
	private String descripcionArticuloPadre = null;
	
	private CssLayout barAndGridLayout = null;    
    private VerticalLayout topLayout = null;
    private HorizontalLayout topLayoutL = null;
    private HorizontalLayout topLayoutS = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;

    private Panel panelResumen =null;
    private Panel panelCompras =null;
    private VerticalLayout panelCompras0 =null;
    private Panel panelCompras1 =null;
    private Panel panelCompras2 =null;
    private Panel panelArticulos =null;
    private Panel panelProduccion =null;
//    private Panel panelPlanificacion =null;
    
    private GridLayout centralTop = null;
    private GridLayout centralTop2 = null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle1 = null;
    private GridLayout centralMiddle2 = null;
    private GridLayout centralBottom = null;
    private GridLayout centralBottom2 = null;
    
	public TextField txtEjercicio= null;
	public TextField txtArticulo= null;

	private Button procesar=null;
	private Button opcMenos= null;
	private Button opcMas= null;
	private Button opcArticulo = null;
	
	private HashMap<String, Double> hash = null;
	private MapeoGeneralDashboardCompras mapeo=null;

	private HashMap<String, Double> hashMP = null;
	private MapeoGeneralDashboardCompras mapeoMP=null;

	private boolean prevision = false;
	private String tipoArticulo = null;
	private ArrayList<MapeoArticulos> vectorPadres = null; 
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardComprasView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		
		this.cargarListeners();
//		this.ejecutarTimer();
//		this.procesar();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
//    	setSpacing(false);
    	
    	
    	this.barAndGridLayout = new CssLayout();
//        this.barAndGridLayout.setSpacing(false);
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

//        final Navigator navigator = new Navigator(eBorsao.getCurrent(), this.barAndGridLayout);

		    	this.topLayoutL = new HorizontalLayout();
//		    	this.topLayoutL.addStyleName("v-panelTitulo");
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.txtEjercicio=new TextField("Ejercicio");
			    	this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		this.txtEjercicio.setEnabled(true);
		    		this.txtEjercicio.setWidth("125px");
		    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		    		
		    		this.opcMas= new Button();    	
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		    		
		    		this.opcMenos= new Button();    	
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);

			    	this.txtArticulo=new TextField("Articulo");
		    		this.txtArticulo.setEnabled(true);
		    		this.txtArticulo.setWidth("125px");
		    		this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

		    		this.procesar=new Button("Procesar");
		        	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.procesar.setIcon(FontAwesome.PRINT);

		    		this.opcArticulo=new Button();
		        	this.opcArticulo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		        	this.opcArticulo.addStyleName(ValoTheme.BUTTON_TINY);
		        	this.opcArticulo.setIcon(FontAwesome.QUESTION);

			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	if (LecturaProperties.semaforos)
		    	{
		    		this.topLayoutL.addComponent(this.opcRefresh);
		    		this.topLayoutL.setComponentAlignment(this.opcRefresh,  Alignment.BOTTOM_LEFT);
		    	}
		    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
//	    		this.topLayoutL.addComponent(this.opcMenos);
//	    		this.topLayoutL.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.txtEjercicio);
//	    		this.topLayoutL.addComponent(this.opcMas);
//	    		this.topLayoutL.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);
	    		this.topLayoutL.addComponent(this.txtArticulo);
	    		this.topLayoutL.addComponent(this.opcArticulo);
	    		this.topLayoutL.setComponentAlignment(this.opcArticulo,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.procesar);
		    	this.topLayoutL.setComponentAlignment(this.procesar,  Alignment.BOTTOM_LEFT);
//		    	this.topLayoutL.setStyleName("miPanel");
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

//	    	this.topLayoutL.addStyleName("top-bar");
//	    	this.topLayout.setHeight("200px");
//	    	this.topLayout.addStyleName("v-panelTitulo");

	    	panelArticulos = new Panel("Materia Prima seleccionada");
		    panelArticulos.setSizeUndefined();
		    panelArticulos.setVisible(false);
		    panelArticulos.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelArticulos );

		    	this.centralTop = new GridLayout(4,1);
		    	this.centralTop.setSpacing(true);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);

		    	panelArticulos.setContent(centralTop);


	    	panelResumen = new Panel("Resumen Año ");
		    panelResumen.setSizeUndefined();
		    panelResumen.addStyleName("showpointer");
//		    new PanelCaptionBarToggler<Panel>( panelResumen );

			    this.centralTop2 = new GridLayout(5,1);
			    this.centralTop2.setWidth("100%");
			    this.centralTop2.setMargin(true);

		    	panelResumen.setContent(centralTop2);
		    	
	    	panelCompras = new Panel("Datos Compras ");
		    panelCompras.setWidth("100%");
		    panelCompras.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelCompras );

		    	panelCompras0 = new VerticalLayout();
			    panelCompras0.setWidth("100%");

			    	panelCompras1 = new Panel("");
				    panelCompras1.setWidth("100%");
		
				    	this.centralMiddle = new GridLayout(4,1);
				    	this.centralMiddle.setWidth("100%");
				    	this.centralMiddle.setMargin(true);
				    	this.centralMiddle.setSpacing(true);
			    	panelCompras1.setContent(centralMiddle);
			    	
			    	panelCompras2 = new Panel("");
				    panelCompras2.setWidth("100%");
		
				    	this.centralMiddle1 = new GridLayout(4,1);
				    	this.centralMiddle1.setWidth("100%");
				    	this.centralMiddle1.setMargin(true);
				    	this.centralMiddle1.setSpacing(true);
			    	panelCompras2.setContent(centralMiddle1);
			    	
			    	panelCompras0.addComponent(panelCompras1);
			    	panelCompras0.addComponent(panelCompras2);
			    	
	    	panelCompras.setContent(panelCompras0);
	    	panelCompras.getContent().setVisible(false);
	    	panelCompras.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

	    	this.centralMiddle2 = new GridLayout(2,1);
	    	this.centralMiddle2.setSizeUndefined();
	    	this.centralMiddle2.setMargin(true);


	    	panelProduccion = new Panel("Datos Produccion ");
		    panelProduccion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelProduccion );
		    
	    	
		    	this.centralBottom = new GridLayout(4,1);
		    	this.centralBottom.setWidth("100%");
		    	this.centralBottom.setMargin(true);

		    	panelProduccion.setContent(centralBottom);
		    	panelProduccion.getContent().setVisible(false);
		    	panelProduccion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
//
//	    	panelPlanificacion = new Panel("Datos Planificación");
//		    panelPlanificacion.setWidth("100%");
//		    panelProduccion.addStyleName("showpointer");
//		    new PanelCaptionBarToggler<Panel>( panelPlanificacion );
//		    
//	    	
//		    	this.centralBottom2 = new GridLayout(4,1);
//		    	this.centralBottom2.setWidth("100%");
//		    	this.centralBottom2.setMargin(true);
//
//		    	panelPlanificacion.setContent(centralBottom2);
//		    	panelPlanificacion.getContent().setVisible(false);
//		    	panelPlanificacion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.centralTop);
    	this.barAndGridLayout.addComponent(this.panelResumen);
    	this.barAndGridLayout.addComponent(this.centralMiddle2);
    	this.barAndGridLayout.addComponent(this.panelCompras);
    	this.barAndGridLayout.addComponent(this.panelProduccion);
//    	this.barAndGridLayout.addComponent(this.panelPlanificacion);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
//    	setExpandRatio(this.barAndGridLayout, 1);
        
    }

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		consultaArticulosSMPTServer cps  = new consultaArticulosSMPTServer(CurrentUser.get());
		vectorArticulos=cps.vectorMP();
		this.vHelp = new ventanaAyuda(this.txtArticulo, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

    private void cargarListeners() 
    {

    	this.txtArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				comprobarArticulo(txtArticulo.getValue());
			}
		});
    	this.opcArticulo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			ventanaMargenes();
    		}
    			
    	});
    	
    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			procesar();
    		}
    			
    	});
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int year = new Integer(txtEjercicio.getValue()).intValue();
				year = year + 1;
				if (year > new Integer(RutinasFechas.añoActualYYYY())) year = year -1;
				txtEjercicio.setValue(String.valueOf(year));
			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int year = new Integer(txtEjercicio.getValue()).intValue();
				year = year - 1;
				txtEjercicio.setValue(String.valueOf(year));
			}
		});


    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});

    	LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};
//		centralLayoutT1.addLayoutClickListener(lcl);

    }
    
    private void procesar()
    {
    	comprobarArticulo(txtArticulo.getValue());
    	if (tipoArticulo!=null)
    	{
			eliminarDashBoards(true);
			
			if (txtEjercicio.getValue() == null || txtEjercicio.getValue().length()==0 || (new Integer(txtEjercicio.getValue().toString()) > new Integer(RutinasFechas.añoActualYYYY())))
			{
				Notificaciones.getInstance().mensajeError("Rellena los selectores correctamente");
			}
			else
			{
				cargarDashboards();
			}
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Rellena el articulo correctamente");
    	}
    }
    
    public void eliminarDashBoards(boolean r_todos)
    {
    	if (r_todos) centralTop.removeAllComponents();
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle1.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	panelResumen.setCaption("Resumen Año");
    	panelCompras.setCaption("Datos Compras");;
//    	panelProduccion.setCaption("Datps Producción / Planificacion");
    	    	
    }
    
    public void cargarDashboards() 
    {
    	

    	/*
    	 * Si el articulo es PT cargaremos todos los datos y graficos convenientemente,
    	 * en caso contrario cargaremos un grid con los datos de los padres que tienen
    	 * el articulo escogido en su escandallo.
    	 * 
    	 * Daremos los datos básicos del stock pero ya no cargaremos nada mas
    	 * Seleccionando alguno de los padres mostrados cargaremos el resto de graficos
    	 */
    	switch (tipoArticulo)
    	{
	    	case "OA":
	    	{
	    		/*
	    		 * cargar panel resumen datos
	    		 */
	    		ejecutarPadres();
	    		break;
	    	}
	    	case "MP":
	    	{
	    		/*
	    		 * cargar panel resumen datos
	    		 */
	    		ejecutarPadres();
	    		break;
	    	}
	    	default:
	    	{
//	    		cargarAccionesHijo(false);
	    		cargarPadres();
	    		break;
	    	}
    	}
    }
    
    
    private void cargarAcciones()
    {
    	String anchoLabelTitulo = "275px";
    	
    	Label lblBiblia= null;
    	Button btnBiblia= null;
    	
    	Panel panel = new Panel("ACCIONES");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);

    	
    	lblBiblia= new Label();
    	lblBiblia.setValue("Detalle Producto ");
    	lblBiblia.setWidth(anchoLabelTitulo);    	
    	lblBiblia.addStyleName("lblTitulo");

    	btnBiblia = new Button();
    	btnBiblia.setDescription("Detalle de Producto");
    	btnBiblia.setIcon(FontAwesome.BOOK);
    	btnBiblia.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	btnBiblia.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				String ext = null;
		    			
				if (articuloPadre.trim().startsWith("0104") || articuloPadre.trim().startsWith("0109") || articuloPadre.trim().startsWith("0108")) ext="pdf";
    			else ext = "jpg";
				
				if (ext.contentEquals("pdf"))
				{
					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.rutaImagenesIntranet, articuloPadre+"."+ext, false);
				}
				else
				{
					String [] archivos = RutinasFicheros.recuperarNombresArchivosCarpeta(LecturaProperties.rutaImagenesIntranet, articuloPadre, ext);
					RutinasFicheros.abrirImagenGenerado(getUI(), archivos,LecturaProperties.rutaImagenesIntranet);
				}
			}
		});
    	
    	lin.addComponent(btnBiblia);
    	lin.addComponent(lblBiblia);
    	lin.setComponentAlignment(btnBiblia, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblBiblia, Alignment.MIDDLE_LEFT);

    	content.addComponent(lin);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(true);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop2.addComponent(panel,4,0);
    }
    
    private void ejecutarPadres()
    {
    	String ejer = this.txtEjercicio.getValue();
    	this.mapeo = consultaDashboardComprasServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticosMP(ejer,this.articuloPadre, this.tipoArticulo);
    	cargarAcciones();
    	cargarResumenes();
    	cargarCompras();
    	cargarProduccion();
    }

    private void cargarPadres()
    {
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	vectorPadres = cas.obtenerArticulosDesc(this.articuloSeleccionado);
    	IndexedContainer container =null;
    	Iterator<MapeoArticulos> iterator = null;
    	MapeoArticulos mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
			
		iterator = vectorPadres.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoArticulos) iterator.next();
			
			file.getItemProperty("articulo").setValue(mapeo.getArticulo().substring(0,7));
			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
		}

		Grid gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
		gridDatos.setWidth("100%");
		gridDatos.addStyleName("minigrid");
		gridDatos.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
				
				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
//				if (grid.getSelectionModel() instanceof SingleSelectionModel && !estoyEnCelda)
//					app.filaSeleccionada();
//				else
//					estoyEnCelda = false;
				articuloPadre= file.getItemProperty("articulo").getValue().toString();
				descripcionArticuloPadre = cas.obtenerDescripcionArticulo(articuloPadre.trim());
				tipoArticulo = cas.obtenerTipoArticulo(articuloPadre).trim();
				eliminarDashBoards(false);
				ejecutarPadres();
			}
		});

		Panel panel = new Panel("Articulos " + articuloSeleccionado );
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setContent(gridDatos);
		centralTop.addComponent(panel,0,0);
    }
    private void cargarPT()
    {
//    	consultaAlbaranesVentasServer cavs = new consultaAlbaranesVentasServer(CurrentUser.get());
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	vectorPadres = ces.recuperarMapeoPadres(this.articuloPadre);
//    	HashMap<String, Double> hasVentas = cavs.datosOpcionesVentasPadres(new Integer(this.txtEjercicio.getValue()), this.articuloPadre);
    	IndexedContainer container =null;
    	Iterator<MapeoArticulos> iterator = null;
    	MapeoArticulos mapeo = null;
    	DashboardComprasView app = this;
    	container = new IndexedContainer();
    	container.addContainerProperty("articulo", String.class, null);
    	container.addContainerProperty("descripcion", String.class, null);
//    	container.addContainerProperty("ventas", Double.class, null);
    	
    	iterator = vectorPadres.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoArticulos) iterator.next();
    		
    		file.getItemProperty("articulo").setValue(mapeo.getArticulo().substring(0,7));
    		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
//    		file.getItemProperty("ventas").setValue(hasVentas.get(mapeo.getArticulo().substring(0,7)));
    	}
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("minigrid");
//    	gridDatos.getColumn("ventas").setHeaderCaption("Unidades Vendidas");
//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
//			
//			@Override
//			public String getStyle(Grid.CellReference cell) {
//				if ("ventas".equals(cell.getPropertyId()))
//				{
//					return "Rcell-normal";
//				}
//				else
//					return null;
//			}
//		});
    	gridDatos.addSelectionListener(new SelectionListener() {
    		
    		@Override
    		public void select(SelectionEvent event) {
    			
    			Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
    			String articuloConsultado= file.getItemProperty("articulo").getValue().toString();
    			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo",articuloConsultado);
    			app.getUI().addWindow(vt);
    		}
    	});
//    	gridDatos.sort("ventas", SortDirection.DESCENDING);
    	
    	Panel panel = new Panel("Articulos Escandallo del " + articuloPadre);
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("225px");
    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralTop2.addComponent(panel,3,0);
    }
    
    private void cargarResumenes()
    {
		this.cargarResumen();
		this.cargarResumenStock();
		this.cargarPT();
//		this.cargarDatosPlanificacion();
		
		panelResumen.setCaption("Resumen Año " + txtEjercicio.getValue().toString() + " del " + articuloPadre + " " + descripcionArticuloPadre);
    }
    
    private void cargarCompras()
    {
		this.cargarResumenCompras();
		this.cargarResumenComprasAnterior();
		this.cargarResumenComprasAnterior2();
		this.cargarResumenComprasAnterior3();

//    	this.evolucionVentas();
//    	this.evolucionVentasAcumuladas();
    	this.resumenCompraProveedores(0);
    	this.resumenCompraProveedores(1);
    	this.resumenCompraProveedores(2);
    	this.resumenCompraProveedores(3);
    	panelCompras.setCaption("Datos Compra del " + articuloPadre + " " + descripcionArticuloPadre);
    }

    private void cargarProduccion()
    {
		this.cargarResumenProduccion();
		this.cargarResumenProduccionAnterior();
		this.cargarResumenProduccionAnterior2();
		this.cargarResumenProduccionAnterior3();
		
    	panelProduccion.setCaption("Datos Produccion del " + articuloPadre + " " + descripcionArticuloPadre);
    }
    private void cargarResumen()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblProduccion= null;
    	Label lblPlanificacion= null;
    	Label lblVenta= null;
    	Label lblVentaMedia= null;
    	Label lblCobertura= null;

    	Button lblValorProduccion= null;
    	Button lblValorPlanificacion= null;
    	Button lblValorVenta= null;
    	Button lblValorVentaMedia= null;
    	Button lblValorCobertura= null;
    	    	
    	Panel panel = new Panel("RESUMEN ANUAL " + txtEjercicio.getValue());
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion : ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),false);
				
			}
		});

    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion,Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblPlanificacion= new Label();
    	lblPlanificacion.setValue("Planificado: ");
    	lblPlanificacion.setWidth(anchoLabelTitulo);    	
    	lblPlanificacion.addStyleName("lblTitulo");

    	lblValorPlanificacion= new Button();
    	lblValorPlanificacion.setWidth(anchoLabelDatos);
    	lblValorPlanificacion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan")));
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPlanificacion.addStyleName("lblDatos");
		lblValorPlanificacion.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					pantallaProgramacion("T",true, new Integer(txtEjercicio.getValue().toString()));
					
				}
			});
    	lin2.addComponent(lblPlanificacion);
    	lin2.addComponent(lblValorPlanificacion);
    	lin2.setComponentAlignment(lblPlanificacion,Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorPlanificacion, Alignment.MIDDLE_RIGHT);
    	
    	lblVenta= new Label();
    	lblVenta.setValue("Comprado: ");
    	lblVenta.setWidth(anchoLabelTitulo);    	
    	lblVenta.addStyleName("lblTitulo");

    	lblValorVenta= new Button();
    	lblValorVenta.setWidth(anchoLabelDatos);
    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.compras")));
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVenta.addStyleName("lblDatos");
		lblValorVenta.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCompras(new Integer(txtEjercicio.getValue().toString()));
			}
		});
    	
    	lin3.addComponent(lblVenta);
    	lin3.addComponent(lblValorVenta);
    	lin3.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);
    	
    	lblVentaMedia= new Label();
    	lblVentaMedia.setValue("Compra Media: ");
    	lblVentaMedia.setWidth(anchoLabelTitulo);    	
    	lblVentaMedia.addStyleName("lblTitulo");

    	lblValorVentaMedia= new Button();
    	lblValorVentaMedia.setWidth(anchoLabelDatos);
    	lblValorVentaMedia.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.comprasAvg")));
    	lblValorVentaMedia.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentaMedia.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentaMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentaMedia);
    	lin4.addComponent(lblValorVentaMedia);
    	lin4.setComponentAlignment(lblVentaMedia,Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorVentaMedia, Alignment.MIDDLE_RIGHT);
    	
    	lblCobertura= new Label();
    	lblCobertura.setValue("Cobertura (Mes): ");
    	lblCobertura.setWidth(anchoLabelTitulo);    	
    	lblCobertura.addStyleName("lblTitulo");

    	lblValorCobertura= new Button();
    	lblValorCobertura.setWidth(anchoLabelDatos);
    	
    	
    	Double ex = new Double(0);
    	Double pdte = new Double(0);
    	Double prog = new Double(0);
    	Double mediaMes = new Double(0);
    	
    	if (this.mapeo.getHashValores().get("0.stock")!=null) ex = this.mapeo.getHashValores().get("0.stock");
    	if (this.mapeo.getHashValores().get("0.pdteRecibir")!=null) pdte = this.mapeo.getHashValores().get("0.pdteRecibir");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");
    	if (this.mapeo.getHashValores().get("0.comprasAvg")!=null) mediaMes = this.mapeo.getHashValores().get("0.comprasAvg");
    	
    	Double dispo = ex+pdte-prog;
    	
    	lblValorCobertura.setCaption(RutinasNumericas.formatearDouble(dispo/mediaMes));
    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorCobertura.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorCobertura.addStyleName("lblDatosBlanco");

    	lin5.addComponent(lblCobertura);
    	lin5.addComponent(lblValorCobertura);
    	lin5.setComponentAlignment(lblCobertura,Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorCobertura, Alignment.MIDDLE_RIGHT);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop2.addComponent(panel,0,0);
    }
    
    private void pantallaProduccion(Integer r_ejercicio, boolean r_porMeses)
    {
    	pantallaLineasProduccion vt = null;
    	String art = null;
    	String area = null;
    	
    	if (this.articuloPadre!=null)
    	{
    		art = this.articuloPadre;
    	}
    	else
    	{
    		art= this.articuloSeleccionado;
    	}
    	consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
    	ArrayList<String> padres = ces.recuperarPadres(art);
    	if (padres!=null && padres.size()>0)
    	{
    		for (int i=0;i<padres.size();i++)
    		{
    			String pad = padres.get(i);
    			if (pad.trim().startsWith("0102")) area="Embotelladora";
    			else area = "Envasadora";
    		}
    		vt = new pantallaLineasProduccion(null,area, r_ejercicio, art, r_porMeses);
    		getUI().addWindow(vt);
    	}
    }

    private void pantallaProgramacion(String r_estado, boolean r_global, Integer r_ejercicio)
    {
    	String area = null;
    	
    	consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
    	ArrayList<String> padres = ces.recuperarPadres(this.articuloPadre);
    	if (padres!=null && padres.size()>0)
    	{
    		for (int i=0;i<padres.size();i++)
    		{
    			String pad = padres.get(i);
    			if (pad.trim().startsWith("0102")) area="Embotelladora";
    			else area = "Envasadora";
    		}
    	}
    	if (area.contentEquals("Envasadora"))
    	{
    		pantallaLineasProgramacionesEnvasadora vt = new pantallaLineasProgramacionesEnvasadora(padres,r_estado, r_ejercicio,false);
    		getUI().addWindow(vt);
    	}
    	else
    	{
    		pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(r_estado, padres, "Programaciones del articulo escandallo de " + this.articuloPadre , this.articuloPadre, null,new Integer(txtEjercicio.getValue().toString()));
    		getUI().addWindow(vt);
    	}
    }
    private void pantallaCompras(Integer r_ejercicio)
    {
    	pantallaLineasAlbaranesCompra vt = new pantallaLineasAlbaranesCompra(r_ejercicio, this.articuloPadre);
    	getUI().addWindow(vt);
    }

    private void pantallaVentas(Integer r_ejercicio)
    {
    	pantallaLineasAlbaranesVentas vt = new pantallaLineasAlbaranesVentas(r_ejercicio, this.articuloPadre, false);
    	getUI().addWindow(vt);
    }
    
    private void cargarResumenStock()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	String traerVentas = null;
    	
    	Label lblStock= null;
    	Label lblPedidos= null;
    	Label lblPreparado= null;
    	Label lblProgramado= null;
    	Label lblNecesidad= null;
    	
    	Button lblValorStock= null;
    	Button lblValorPedidos= null;
    	Button lblValorPreparado= null;
    	Button lblValorProgramado= null;
    	Button lblValorNecesidad= null;
    	
    	traerVentas="0";
    	Panel panel = new Panel("SITUACION ACTUAL");
    	
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	
    	Double ex = new Double(0);
    	Double pdte = new Double(0);
    	Double pdteServir = new Double(0);
    	Double prep = new Double(0);
    	Double prog = new Double(0);
    	final Double st = ex+prep;
    	
    	
    	if (this.mapeo.getHashValores().get("0.stock")!=null) ex = this.mapeo.getHashValores().get("0.stock");
    	if (this.mapeo.getHashValores().get("0.pdteRecibir")!=null) pdte = this.mapeo.getHashValores().get("0.pdteRecibir");
    	if (this.mapeo.getHashValores().get("0.pdteServir")!=null) pdteServir = this.mapeo.getHashValores().get("0.pdteServir");
    	if (this.mapeo.getHashValores().get("0.prog")!=null) prog = this.mapeo.getHashValores().get("0.prog");

    	Double dispo = ex+pdte+prep-prog;
    	
    	lblStock= new Label();
    	lblStock.setValue("Stock: ");
    	lblStock.setWidth(anchoLabelTitulo);    	
    	lblStock.addStyleName("lblTitulo");

    	lblValorStock= new Button();
    	lblValorStock.setWidth(anchoLabelDatos);
    	lblValorStock.setCaption(RutinasNumericas.formatearDouble(ex));
    	lblValorStock.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorStock.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorStock.addStyleName("lblDatos");
    	lblValorStock.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
        		pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + articuloPadre, articuloPadre,null);
    			getUI().addWindow(vt);
			}
		});
			
    	lin.addComponent(lblStock);
    	lin.addComponent(lblValorStock);
    	lin.setComponentAlignment(lblStock, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorStock, Alignment.MIDDLE_RIGHT);
    	
    	lblPedidos= new Label();
    	lblPedidos.setValue("Pdte. Recibir: ");
    	lblPedidos.setWidth(anchoLabelTitulo);    	
    	lblPedidos.addStyleName("lblTitulo");

    	lblValorPedidos= new Button();
    	lblValorPedidos.setWidth(anchoLabelDatos);
    	lblValorPedidos.setCaption(RutinasNumericas.formatearDouble(pdte));
    	lblValorPedidos.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPedidos.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPedidos.addStyleName("lblDatos");
    	lblValorPedidos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(articuloPadre, (new Double(st)).intValue(), "Lineas Pedidos Compra del Articulo " + articuloPadre );
				getUI().addWindow(vt);
				
			}
		});
			
    	lin2.addComponent(lblPedidos);
    	lin2.addComponent(lblValorPedidos);
    	lin2.setComponentAlignment(lblPedidos, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorPedidos, Alignment.MIDDLE_RIGHT);
	    	
    	lblPreparado= new Label();
    	lblPreparado.setValue("Pdte. Servir:");
    	lblPreparado.setWidth(anchoLabelTitulo);    	
    	lblPreparado.addStyleName("lblTitulo");

    	lblValorPreparado= new Button();
    	lblValorPreparado.setWidth(anchoLabelDatos);
    	lblValorPreparado.setCaption(RutinasNumericas.formatearDouble(pdteServir));
    	lblValorPreparado.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPreparado.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPreparado.addStyleName("lblDatos");
    	lblValorPreparado.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
				pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(articuloPadre, "Lineas Pedidos Venta del Articulo " + articuloPadre,true );
				getUI().addWindow(vt);
    			
    		}
    	});
    	
    	lin3.addComponent(lblPreparado);
    	lin3.addComponent(lblValorPreparado);
    	lin3.setComponentAlignment(lblPreparado, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorPreparado, Alignment.MIDDLE_RIGHT);
	    	
    	lblProgramado= new Label();
    	lblProgramado.setValue("Programadas: ");
    	lblProgramado.setWidth(anchoLabelTitulo);    	
    	lblProgramado.addStyleName("lblTitulo");

    	lblValorProgramado= new Button();
    	lblValorProgramado.setWidth(anchoLabelDatos);
    	lblValorProgramado.setCaption(RutinasNumericas.formatearDouble(prog));
    	lblValorProgramado.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProgramado.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProgramado.addStyleName("lblDatos");
    	
    	lblValorProgramado.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProgramacion(null,false,new Integer(txtEjercicio.getValue().toString()));
				
			}
		});
    	
    	lin4.addComponent(lblProgramado);
    	lin4.addComponent(lblValorProgramado);
    	lin4.setComponentAlignment(lblProgramado, Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorProgramado, Alignment.MIDDLE_RIGHT);

    	lblNecesidad= new Label();
    	lblNecesidad.setValue("Disponible: ");
    	lblNecesidad.setWidth(anchoLabelTitulo);    	
    	lblNecesidad.addStyleName("lblTitulo");

    	lblValorNecesidad= new Button();
    	lblValorNecesidad.setWidth(anchoLabelDatos);
    	lblValorNecesidad.setCaption(RutinasNumericas.formatearDouble(dispo));
    	lblValorNecesidad.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorNecesidad.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorNecesidad.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblNecesidad);
    	lin5.addComponent(lblValorNecesidad);
    	lin5.setComponentAlignment(lblNecesidad, Alignment.MIDDLE_LEFT);
    	lin5.setComponentAlignment(lblValorNecesidad, Alignment.MIDDLE_RIGHT);
		    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin4);
    	content.addComponent(lin3);
    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralTop2.addComponent(panel,1,0);
    }
    
    
    private void cargarResumenProduccion()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Label lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Label lblValorEmbotelladas= null;
    	Label lblValorEtiquetadas= null;
    	Label lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Ventas " + String.valueOf(new Integer(txtEjercicio.getValue())));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),true);
				
			}
		});
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Label();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")/new Integer(RutinasFechas.mesActualMM())));
    	lblValorMediaMes.addStyleName("lblDatosBlanco");

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Ventas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventas")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
    	
    	lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString()));
			}
		});

    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);

    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Media Ventas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");
    	
    	lblValorEmbotelladas= new Label();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasAvg")));
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");

    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_RIGHT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);

    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Planificadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");
    	
    	lblValorEtiquetadas= new Label();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.plan")));
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_RIGHT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Media Mes: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");
    	
    	lblValorRetrabajadas= new Label();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.avgplan")));
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_RIGHT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,0,0);
    }
    
    private void cargarResumenProduccionAnterior()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Label lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Label lblValorEmbotelladas= null;
    	Label lblValorEtiquetadas= null;
    	Label lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Ventas "+ String.valueOf(new Integer(txtEjercicio.getValue())-1));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-1, false);
				
			}
		});
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Label();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.prod")/12));
    	lblValorMediaMes.addStyleName("lblDatosBlanco");
			
    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Ventas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.ventas")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
    	
    	lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-1);
			}
		});
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);

    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Media Ventas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");
    	
    	lblValorEmbotelladas= new Label();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.ventasAvg")));
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");

    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_RIGHT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);
	    	
    	lblEtiquetadas= new Label();
    	lblEtiquetadas.setValue("Planificadas: ");
    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
    	lblEtiquetadas.addStyleName("lblTitulo");
    	
    	lblValorEtiquetadas= new Label();
    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
    	lblValorEtiquetadas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.plan")));
    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
    	
    	lin5.addComponent(lblEtiquetadas);
    	lin5.addComponent(lblValorEtiquetadas);
    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_RIGHT);
    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
    	
    	lblRetrabajadas= new Label();
    	lblRetrabajadas.setValue("Media Mes: ");
    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
    	lblRetrabajadas.addStyleName("lblTitulo");
    	
    	lblValorRetrabajadas= new Label();
    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
    	lblValorRetrabajadas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.avgplan")));
    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
    	
    	lin6.addComponent(lblRetrabajadas);
    	lin6.addComponent(lblValorRetrabajadas);
    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_RIGHT);
    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	content.addComponent(lin5);
    	content.addComponent(lin6);

    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,1,0);
    }
    
    private void cargarResumenProduccionAnterior2()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Label lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Label lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Ventas "+ String.valueOf(new Integer(txtEjercicio.getValue())-2));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2, false);
				
			}
		});	
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Label();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.prod")/12));
    	lblValorMediaMes.addStyleName("lblDatosBlanco");
			
    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Ventas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.ventas")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
		lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-2);
			}
		});
		
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);

    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Media Ventas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");
    	
    	lblValorEmbotelladas= new Label();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.ventasAvg")));
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");

    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_RIGHT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);

    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,2,0);
    }
    
    private void cargarResumenProduccionAnterior3()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "140px";
    	
    	Label lblProduccion= null;
    	Label lblMediaMes= null;
    	Label lblVecesPlanificadas= null;
    	Label lblEmbotelladas= null;
    	Label lblEtiquetadas= null;
    	Label lblRetrabajadas= null;
    	
    	Button lblValorProduccion= null;
    	Label lblValorMediaMes= null;
    	Button lblValorVecesPlanificadas= null;
    	Label lblValorEmbotelladas= null;
    	Button lblValorEtiquetadas= null;
    	Button lblValorRetrabajadas= null;
    	
    	Panel panel = new Panel("Resumen Produccion / Ventas "+ String.valueOf(new Integer(txtEjercicio.getValue())-3));
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	HorizontalLayout lin6 = new HorizontalLayout();
    	lin6.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Produccion: ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.prod")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatos");
    	lblValorProduccion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaProduccion(new Integer(txtEjercicio.getValue().toString())-2, false);
				
			}
		});	
    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblMediaMes= new Label();
    	lblMediaMes.setValue("Media Mes: ");
    	lblMediaMes.setWidth(anchoLabelTitulo);    	
    	lblMediaMes.addStyleName("lblTitulo");

    	lblValorMediaMes= new Label();
    	lblValorMediaMes.setWidth(anchoLabelDatos);
    	lblValorMediaMes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.prod")/12));
    	lblValorMediaMes.addStyleName("lblDatosBlanco");

    	lin2.addComponent(lblMediaMes);
    	lin2.addComponent(lblValorMediaMes);
    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
	    	
    	lblVecesPlanificadas= new Label();
    	lblVecesPlanificadas.setValue("Ventas: ");
    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
    	lblVecesPlanificadas.addStyleName("lblTitulo");

    	lblValorVecesPlanificadas= new Button();
    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.ventas")));
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVecesPlanificadas.addStyleName("lblDatos");
		lblValorVecesPlanificadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaVentas(new Integer(txtEjercicio.getValue().toString())-3);
			}
		});
		
    	lin3.addComponent(lblVecesPlanificadas);
    	lin3.addComponent(lblValorVecesPlanificadas);
    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);

    	lblEmbotelladas= new Label();
    	lblEmbotelladas.setValue("Media Ventas: ");
    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
    	lblEmbotelladas.addStyleName("lblTitulo");
    	
    	lblValorEmbotelladas= new Label();
    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
    	lblValorEmbotelladas.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.ventasAvg")));
    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");

    	lin4.addComponent(lblEmbotelladas);
    	lin4.addComponent(lblValorEmbotelladas);
    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_RIGHT);
    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);
	    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);

    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
		centralBottom.addComponent(panel,3,0);
    }
    
    private void cargarResumenCompras()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Label lblVentasClientes = null;
    	Label lblValorVentasClientes = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN COMPRAS " + txtEjercicio.getValue());

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.comprasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");

    	lblValorVentasUnidades = new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.compras")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCompras(new Integer(txtEjercicio.getValue().toString()));
				
			}
		});
    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PMC: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDoubleDecimales(this.mapeo.getHashValores().get("0.pvpCompras"),4));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.comprasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,0,0);
    }
    
    private void cargarResumenComprasAnterior()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	Label lblVentasClientes= null;
    	Label lblValorVentasClientes = null;
    	
    	
    	Panel panel = new Panel("RESUMEN COMPRAS " + String.valueOf(new Integer(txtEjercicio.getValue())-1));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	HorizontalLayout lin5 = new HorizontalLayout();
    	lin5.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.comprasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");

    	lblValorVentasUnidades = new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.compras")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCompras(new Integer(txtEjercicio.getValue().toString())-1);
				
			}
		});
    	
    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PMC: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDoubleDecimales(this.mapeo.getHashValores().get("1.pvpCompras"),4));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("1.comprasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
//    	content.addComponent(lin5);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,1,0);
    }
    private void cargarResumenComprasAnterior2()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN COMPRAS " + String.valueOf(new Integer(txtEjercicio.getValue())-2));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.comprasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");

    	lblValorVentasUnidades = new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.compras")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCompras(new Integer(txtEjercicio.getValue().toString())-2);
				
			}
		});    	
    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PMC: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDoubleDecimales(this.mapeo.getHashValores().get("2.pvpCompras"),4));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("2.comprasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,2,0);
    }
    private void cargarResumenComprasAnterior3()
    {
    	String anchoLabelTitulo = "120px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblVentasImporte = null;
    	Label lblValorVentasImporte= null;
    	Label lblVentasUnidades = null;
    	Button lblValorVentasUnidades = null;
    	Label lblVentasPVM = null;
    	Label lblValorVentasPVM = null;
    	Label lblVentasMedia = null;
    	Label lblValorVentasMedia = null;
    	
    	
    	Panel panel = new Panel("RESUMEN COMPRAS " + String.valueOf(new Integer(txtEjercicio.getValue())-3));

    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblVentasImporte= new Label();
    	lblVentasImporte.setValue("Importe: ");
    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
    	lblVentasImporte.addStyleName("lblTitulo");

    	lblValorVentasImporte = new Label();
    	lblValorVentasImporte.setWidth(anchoLabelDatos);
    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.comprasImp")));
    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
    	
    	lin.addComponent(lblVentasImporte);
    	lin.addComponent(lblValorVentasImporte);
    	
    	lblVentasUnidades= new Label();
    	lblVentasUnidades.setValue("Unidades: ");
    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
    	lblVentasUnidades.addStyleName("lblTitulo");

    	lblValorVentasUnidades = new Button();
    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
    	lblValorVentasUnidades.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.compras")));
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVentasUnidades.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVentasUnidades.addStyleName("lblDatos");
    	lblValorVentasUnidades.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCompras(new Integer(txtEjercicio.getValue().toString())-3);
				
			}
		});
    	
    	lin2.addComponent(lblVentasUnidades);
    	lin2.addComponent(lblValorVentasUnidades);
    	
    	lblVentasPVM= new Label();
    	lblVentasPVM.setValue("PMC: ");
    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
    	lblVentasPVM.addStyleName("lblTitulo");

    	lblValorVentasPVM = new Label();
    	lblValorVentasPVM.setWidth(anchoLabelDatos);
    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDoubleDecimales(this.mapeo.getHashValores().get("3.pvpCompras"),4));
    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
    	
    	lin3.addComponent(lblVentasPVM);
    	lin3.addComponent(lblValorVentasPVM);
    	
    	lblVentasMedia= new Label();
    	lblVentasMedia.setValue("Media Mes: ");
    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
    	lblVentasMedia.addStyleName("lblTitulo");

    	lblValorVentasMedia = new Label();
    	lblValorVentasMedia.setWidth(anchoLabelDatos);
    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("3.comprasAvg")));
    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
    	
    	lin4.addComponent(lblVentasMedia);
    	lin4.addComponent(lblValorVentasMedia);
    	
    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralMiddle.addComponent(panel,3,0);
    }
    
    private void vaciarPantalla()
    {
    	centralTop=null;
        centralMiddle=null;
        centralBottom=null;
        topLayout=null;
        topLayoutL=null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }
    
    private void comprobarArticulo(String r_articulo)
    {
    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    	
    	if (r_articulo!=null)
    	{
    		tipoArticulo = cas.obtenerTipoArticulo(r_articulo);
    		if (tipoArticulo!=null)
    		{
    			switch (tipoArticulo)
    			{
	    			case "OA":
	    			{
	    				panelArticulos.setVisible(true);
	    				this.articuloSeleccionado=null;
	    				this.articuloPadre=r_articulo.trim();
	    				this.descripcionArticuloPadre = cas.obtenerDescripcionArticulo(r_articulo.trim());
	    				this.descripcionArticuloSeleccionado = null;
	    				break;
	    			}
	    			case "MP":
	    			{
	    				panelArticulos.setVisible(true);
	    				this.articuloSeleccionado=null;
	    				this.articuloPadre=r_articulo.trim();
	    				this.descripcionArticuloPadre = cas.obtenerDescripcionArticulo(r_articulo.trim());
	    				this.descripcionArticuloSeleccionado = null;
	    				break;
	    			}
	    			default:
	    			{
	    	    		Notificaciones.getInstance().mensajeError("Valor no válido");
	    	    		this.txtArticulo.focus();
	    	    		tipoArticulo=null;
	    	    		this.articuloSeleccionado=null;
	    	    		panelArticulos.setVisible(false);
	    	    		break;
	    			}
    			}
    		}
    		else
    		{
    			panelArticulos.setVisible(true);
				this.articuloSeleccionado=r_articulo.trim();
				this.descripcionArticuloSeleccionado = cas.obtenerDescripcionArticulo(r_articulo.trim());
				this.articuloPadre=null;
				this.descripcionArticuloPadre = null;
			}
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Valor no válido");
    		this.txtArticulo.focus();
    		tipoArticulo=null;
    		this.articuloSeleccionado=null;
    		panelArticulos.setVisible(false);
    	}
    }
    
    private void resumenCompraProveedores(int r_ejercicio)
    {
    	consultaDashboardComprasServer cds = new consultaDashboardComprasServer(CurrentUser.get());
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	vector = cds.recuperarComprasProveedorEjercicio(new Integer(txtEjercicio.getValue().toString())-r_ejercicio, this.articuloPadre, "");
    	
    	IndexedContainer container =null;
    	Iterator<MapeoValores> iterator = null;
    	MapeoValores mapeo = null;
    	Double total = new Double(0);
    	Double totalImp = new Double(0);
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("Proveedor", String.class, null);
    	container.addContainerProperty("Unidades", Double.class, null);
    	container.addContainerProperty("Importe", Double.class, null);
    	container.addContainerProperty("PMC", Double.class, null);
    	
    	iterator = vector.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoValores) iterator.next();
    		total = total + mapeo.getValorDouble();
    		totalImp = totalImp + mapeo.getValorDouble2();
    		
    		file.getItemProperty("Proveedor").setValue(mapeo.getClave());
    		file.getItemProperty("Unidades").setValue(mapeo.getValorDouble());
    		file.getItemProperty("Importe").setValue(mapeo.getValorDouble2());
    		file.getItemProperty("PMC").setValue(new Double(RutinasCadenas.reemplazarComaMiles(RutinasNumericas.formatearDoubleDecimales(mapeo.getValorDouble2()/mapeo.getValorDouble(),4))));
    	}
    	
    	Object newItemId = container.addItem();
    	Item file = container.getItem(newItemId);
    	
    	file.getItemProperty("Proveedor").setValue("TOTAL");
    	file.getItemProperty("Unidades").setValue(total);
    	file.getItemProperty("Importe").setValue(totalImp);
    	file.getItemProperty("PMC").setValue(totalImp/total);
    	
    	
    	Grid gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.SINGLE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Unidades")||cellReference.getPropertyId().toString().contains("Importe")||cellReference.getPropertyId().toString().contains("PMC")) {
    				return "Rcell-normal";
    			}
    			return null;
    		}
    	});
    	
    	Panel panel = new Panel("Compra por proveedor (Uds./Imp./PMC)");
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setHeight("250px");
    	panel.setWidth("350px");
    	panel.setContent(gridDatos);
    	centralMiddle1.addComponent(panel,r_ejercicio,0);
    }

	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}


    private void ejecutarTimer()
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }	
	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("referesco dashboard Embotelladora " + new Date());
    		refrescar();
    	}
    }


    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarListeners();
    }


}
