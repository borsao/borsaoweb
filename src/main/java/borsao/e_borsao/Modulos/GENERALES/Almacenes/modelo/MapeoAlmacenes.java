package borsao.e_borsao.Modulos.GENERALES.Almacenes.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAlmacenes extends MapeoGlobal
{
	private String almacen;
	private String descripcion;

	public MapeoAlmacenes()
	{
		this.setAlmacen("");
		this.setDescripcion("");
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
}