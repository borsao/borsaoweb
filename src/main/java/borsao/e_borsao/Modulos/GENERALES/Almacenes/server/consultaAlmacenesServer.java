package borsao.e_borsao.Modulos.GENERALES.Almacenes.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.modelo.MapeoAlmacenes;

public class consultaAlmacenesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAlmacenesServer instance;
	public static String restoAlmacenes="1,8,10,20,30";
	public static String capuchinos="1,8";
	
	public consultaAlmacenesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlmacenesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlmacenesServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionAlmacen(String r_almacen)
	{
		String rdo = null;
		ResultSet rsAlmacenes = null;
		Statement cs = null;
		String sql = null;
		Connection con = null;
		try
		{
			sql="select almacen, nombre from e_almac where almacen ='" + r_almacen + "'" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				rdo=rsAlmacenes.getString("nombre");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public boolean obtenerAlmacenExterno(String r_almacen)
	{
		boolean rdo = false;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select apartado from e_almac where almacen ='" + r_almacen + "'" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				if (rsAlmacenes.getString("apartado")!=null && rsAlmacenes.getString("apartado").trim().length()>0) rdo = true;								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public ArrayList<String> obtenerAlmacenes()
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		String usu = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			rdo = new ArrayList<String>();
			
			if (eBorsao.get().accessControl.getCodigoGestion().trim().length()>7)
				usu = eBorsao.get().accessControl.getCodigoGestion().trim().substring(0, 7);
			else
				usu = eBorsao.get().accessControl.getCodigoGestion().trim();
			
			sql="select distinct almacen from e_almac where almacen in (select almacen from e_doc_al where usuario = '" + usu + "' and perm_cons='S')" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				rdo.add(rsAlmacenes.getString("almacen"));								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}

	public ArrayList<MapeoAlmacenes> datosAlmacenesGlobal()
	{
		ArrayList<MapeoAlmacenes> rdo = null;
		ResultSet rsAlmacenes = null;
		String usu = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			rdo = new ArrayList<MapeoAlmacenes>();
			
			if (eBorsao.get().accessControl.getCodigoGestion().trim().length()>7)
				usu = eBorsao.get().accessControl.getCodigoGestion().trim().substring(0, 7);
			else
				usu = eBorsao.get().accessControl.getCodigoGestion().trim();
			
			sql="select distinct almacen, nombre from e_almac where almacen in (select almacen from e_doc_al where usuario = '" + usu + "' and perm_cons='S')" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				MapeoAlmacenes mapeo = new MapeoAlmacenes();
				mapeo.setAlmacen(rsAlmacenes.getString("almacen"));
				mapeo.setDescripcion(rsAlmacenes.getString("nombre"));
				
				rdo.add(mapeo);								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}
	
	public ArrayList<String> obtenerCAEAlmacenes()
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select distinct encargado from e_almac where (encargado is not null and encargado <> '' and pais = '000')" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				rdo.add(rsAlmacenes.getString("encargado").trim());								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String obtenerCAEAlmacen(String r_almacen)
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Statement cs = null;
		Connection con = null;
		String sql = null;
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select encargado from e_almac where almacen = " + r_almacen ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				return rsAlmacenes.getString("encargado");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String obtenerNombreAlmacenPorCAE(String r_cae)
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select nombre from e_almac where encargado = '" + r_cae + "' ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				return rsAlmacenes.getString("nombre").toUpperCase();								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String obtenerAlmacenPorCAE(String r_cae)
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		if (r_cae==null || r_cae.length()==0) return null;
		
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select almacen from e_almac where encargado = '" + r_cae + "' ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				return rsAlmacenes.getString("almacen").toUpperCase();								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String obtenerAlmacenPorCAEDestino(String r_cae)
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		if (r_cae==null || r_cae.length()==0) return null;
		
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select almacen from e_almac where encargado like '" + r_cae + "%' and (apartado is not null or apartado <> '') ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				return rsAlmacenes.getString("almacen").toUpperCase();								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String obtenerAlmacenPorCAE2(String r_cae)
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		if (r_cae==null || r_cae.length()==0) return null;
		
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select almacen from e_almac where encargado = '" + r_cae + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				return rsAlmacenes.getString("almacen").toUpperCase();								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}


	public ArrayList<String> obtenerAlmacenesExternos()
	{
		ArrayList<String> rdo = null;
		ResultSet rsAlmacenes = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select distinct almacen from e_almac where apartado is not null" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsAlmacenes= cs.executeQuery(sql);
			while(rsAlmacenes.next())
			{
				rdo.add(rsAlmacenes.getString("almacen"));								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAlmacenes!=null)
				{
					rsAlmacenes.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String obtenerAlmacenesCalculoPendienteServir()
	{

		String rdo = null;
		rdo = "(1,2,4,10,20,30)";
		return rdo;
	}
}
