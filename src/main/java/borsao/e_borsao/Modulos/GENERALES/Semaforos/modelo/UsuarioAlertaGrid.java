package borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class UsuarioAlertaGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public UsuarioAlertaGrid(ArrayList<MapeoUsuarioAlerta> r_vector) {
        
        this.vector=r_vector;
		this.asignarTitulo("Usuarios - Semaforos");

		if (this.vector==null ||  this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoUsuarioAlerta.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("usuario", "departamento");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("usuario").setHeaderCaption("User");
    	this.getColumn("departamento").setHeaderCaption("Semaforo");
    	this.getColumn("idCodigo").setHidden(true);
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void asignarEstilos() 
	{
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}
