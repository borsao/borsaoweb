package borsao.e_borsao.Modulos.GENERALES.Semaforos.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.MapeoUsuarioAlerta;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.UsuarioAlertaGrid;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.server.consultaUsuarioAlertaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class UsuarioAlertaView extends GridViewRefresh {

	public static final String VIEW_NAME = "UsuarioAlerta";
	private final String titulo = "USUARIOS ALERTAS";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoUsuarioAlerta> r_vector=null;
    	MapeoUsuarioAlerta MapeoUsuarioAlerta =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	MapeoUsuarioAlerta=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaUsuarioAlertaServer cus = new consultaUsuarioAlertaServer(CurrentUser.get());
    	r_vector=cus.datosUsuarioAlertaGlobal(MapeoUsuarioAlerta);
    	
    	grid = new UsuarioAlertaGrid(r_vector);
    	if (((UsuarioAlertaGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public UsuarioAlertaView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
    }

    public void newForm()
    {    	
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarUsuarioAlerta((MapeoUsuarioAlerta) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }
    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
