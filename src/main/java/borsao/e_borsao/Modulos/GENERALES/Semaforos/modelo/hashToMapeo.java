package borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoUsuarioAlerta mapeo = null;
	 
	 public MapeoUsuarioAlerta convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoUsuarioAlerta();
		 this.mapeo.setUsuario(r_hash.get("usuario"));
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoUsuarioAlerta convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoUsuarioAlerta();
		 this.mapeo.setUsuario(r_hash.get("usuario"));
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoUsuarioAlerta r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("usuario", r_mapeo.getUsuario());
		 this.hash.put("departamento", this.mapeo.getDepartamento());
		 return hash;		 
	 }
}