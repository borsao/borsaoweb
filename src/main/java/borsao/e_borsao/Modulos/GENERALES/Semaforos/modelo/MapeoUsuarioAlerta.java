package borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoUsuarioAlerta  extends MapeoGlobal
{
	private String usuario;
	private String departamento;

	public MapeoUsuarioAlerta()
	{
		this.setUsuario("");
		this.setDepartamento("");
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String r_usuario) {
		this.usuario = r_usuario;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String r_departamento) {
		this.departamento = r_departamento;
	}
}