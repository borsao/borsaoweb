package borsao.e_borsao.Modulos.GENERALES.Semaforos.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.MapeoUsuarioAlerta;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.UsuarioAlertaGrid;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.server.consultaUsuarioAlertaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoUsuarioAlerta mapeoUsuarioAlerta = null;
	 
	 private consultaUsuarioAlertaServer cus = null;	 
	 private UsuarioAlertaView uw = null;
	 private BeanFieldGroup<MapeoUsuarioAlerta> fieldGroup;
	 
	 public OpcionesForm(UsuarioAlertaView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.departamento,"Departamento");
        
        fieldGroup = new BeanFieldGroup<MapeoUsuarioAlerta>(MapeoUsuarioAlerta.class);
        fieldGroup.bindMemberFields(this);

        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{
	}

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoUsuarioAlerta = (MapeoUsuarioAlerta) uw.grid.getSelectedRow();
                eliminarUsuarioAlerta(mapeoUsuarioAlerta);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoUsuarioAlerta = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearUsuarioAlerta(mapeoUsuarioAlerta);
	 			}
	 			else
	 			{
	 				MapeoUsuarioAlerta mapeoUsuarioAlerta_orig = (MapeoUsuarioAlerta) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoUsuarioAlerta= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarUsuarioAlerta(mapeoUsuarioAlerta,mapeoUsuarioAlerta_orig);
	 				hm=null;
	 			}
	 			uw.setHayGrid(true);
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		switch (r_nombre)
		{
			case "Departamento":
			{
				r_combo.addItem("Informatica");
				r_combo.addItem("Laboratorio");
				r_combo.addItem("Comercial");
				r_combo.addItem("Calidad");
				r_combo.addItem("Compras");
				r_combo.addItem("Produccion");
				r_combo.addItem("Almacen");
				r_combo.addItem("Financiero");
				break;
			}
		}
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarUsuarioAlerta(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.departamento.getValue()!=null && this.departamento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("departamento", this.departamento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("departamento", "");
		}
		
		if (this.usuario.getValue()!=null && this.usuario.getValue().length()>0) 
		{
			opcionesEscogidas.put("usuario", this.usuario.getValue());
		}
		else
		{
			opcionesEscogidas.put("usuario", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarUsuarioAlerta(null);
	}
	
	public void editarUsuarioAlerta(MapeoUsuarioAlerta r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoUsuarioAlerta();
		fieldGroup.setItemDataSource(new BeanItem<MapeoUsuarioAlerta>(r_mapeo));
		usuario.focus();
	}
	
	public void eliminarUsuarioAlerta(MapeoUsuarioAlerta r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaUsuarioAlertaServer(CurrentUser.get());
		((UsuarioAlertaGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarUsuarioAlerta(MapeoUsuarioAlerta r_mapeo, MapeoUsuarioAlerta r_mapeo_orig)
	{
		cus  = new consultaUsuarioAlertaServer(CurrentUser.get());
		cus.guardarCambios(r_mapeo);		
		((UsuarioAlertaGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearUsuarioAlerta(MapeoUsuarioAlerta r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaUsuarioAlertaServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoUsuarioAlerta>(r_mapeo));
			((UsuarioAlertaGrid) uw.grid).refresh(r_mapeo,null);
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoUsuarioAlerta> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoUsuarioAlerta= item.getBean();
            if (this.mapeoUsuarioAlerta.getUsuario()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

}
