package borsao.e_borsao.Modulos.GENERALES.Semaforos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Semaforos.modelo.MapeoUsuarioAlerta;

public class consultaUsuarioAlertaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaUsuarioAlertaServer instance;
	
	public consultaUsuarioAlertaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaUsuarioAlertaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaUsuarioAlertaServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoUsuarioAlerta> datosUsuarioAlertaGlobal(MapeoUsuarioAlerta r_mapeo)
	{
		ResultSet rsUsuarios = null;		
		ArrayList<MapeoUsuarioAlerta> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT glb_user_alert.usuario usu_usu, ");
	    cadenaSQL.append(" glb_user_alert.departamento usu_dep ");
     	cadenaSQL.append(" FROM glb_user_alert ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getUsuario()!=null && r_mapeo.getUsuario().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" usuario = '" + r_mapeo.getUsuario() + "' ");
				}
				if (r_mapeo.getDepartamento()!=null && r_mapeo.getDepartamento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" departamento = '" + r_mapeo.getDepartamento() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by usuario asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsUsuarios.TYPE_SCROLL_SENSITIVE,rsUsuarios.CONCUR_UPDATABLE);
			rsUsuarios= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoUsuarioAlerta>();
			
			while(rsUsuarios.next())
			{
				MapeoUsuarioAlerta mapeo = new MapeoUsuarioAlerta();
				/*
				 * recojo mapeo analiticas
				 */
				mapeo.setUsuario(rsUsuarios.getString("usu_usu"));
				mapeo.setDepartamento(rsUsuarios.getString("usu_dep"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoUsuarioAlerta r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO glb_user_alert ( ");
    		cadenaSQL.append(" glb_user_alert.usuario, ");
		    cadenaSQL.append(" glb_user_alert.departamento ) VALUES (");
		    cadenaSQL.append(" ?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getUsuario()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getUsuario());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDepartamento()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDepartamento());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoUsuarioAlerta r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE glb_user_alert set ");
		    cadenaSQL.append(" glb_user_alert.departamento=? ");
		    cadenaSQL.append(" WHERE glb_user_alert.usuario = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getDepartamento());
		    preparedStatement.setString(2, r_mapeo.getUsuario());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoUsuarioAlerta r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_user_alert ");            
			cadenaSQL.append(" WHERE glb_user_alert.usuario = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getUsuario());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}
