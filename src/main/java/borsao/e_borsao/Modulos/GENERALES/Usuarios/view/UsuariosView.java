package borsao.e_borsao.Modulos.GENERALES.Usuarios.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.MapeoUsuarios;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.UsuariosGrid;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.server.consultaUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class UsuariosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Usuarios";
	private final String titulo = "USUARIOS INTRANET";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoUsuarios> r_vector=null;
    	MapeoUsuarios MapeoUsuarios =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	MapeoUsuarios=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaUsuariosServer cus = new consultaUsuariosServer(CurrentUser.get());
    	r_vector=cus.datosUsuariosGlobal(MapeoUsuarios);
    	
    	grid = new UsuariosGrid(this, r_vector);
    	if (((UsuariosGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public UsuariosView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
    }

    public void newForm()
    {    	
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarUsuario((MapeoUsuarios) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }
    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
