package borsao.e_borsao.Modulos.GENERALES.Usuarios.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CRM.vistas.modelo.MapeoVistas;
import borsao.e_borsao.Modulos.CRM.vistas.server.consultaVistasServer;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.MapeoUsuarios;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.UsuariosGrid;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.server.consultaUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoUsuarios mapeoUsuarios = null;
	 
	 private consultaUsuariosServer cus = null;	 
	 private UsuariosView uw = null;
	 private BeanFieldGroup<MapeoUsuarios> fieldGroup;
	 
	 public OpcionesForm(UsuariosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.vista,"Vista");
        this.cargarCombos(this.departamento,"Departamento");
        
        fieldGroup = new BeanFieldGroup<MapeoUsuarios>(MapeoUsuarios.class);
        fieldGroup.bindMemberFields(this);

        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{
		firma.setValidationVisible(false);
		class MyValidator implements Validator {
            private static final long serialVersionUID = -8281962473854901819L;

            @Override
            public void validate(Object value)
                    throws InvalidValueException {
            	boolean existe = RutinasFicheros.comprobarExisteFichero(LecturaProperties.baseImageSignature + firma.getValue());
            	
            	if (!existe)
            	{
	            	throw new InvalidValueException("El fichero no existe en el directorio de firmas. Contacta con el administrador.");
            	}
            }
        }
		firma.addValidator(new MyValidator());
		
		firma.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				firma.setValidationVisible(true);
			}
		});
	}

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoUsuarios = (MapeoUsuarios) uw.grid.getSelectedRow();
                eliminarUsuario(mapeoUsuarios);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoUsuarios = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearUsuario(mapeoUsuarios);
	 			}
	 			else
	 			{
	 				MapeoUsuarios mapeoUsuarios_orig = (MapeoUsuarios) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoUsuarios= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarUsuario(mapeoUsuarios,mapeoUsuarios_orig);
	 				hm=null;
	 			}
	 			uw.setHayGrid(true);
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		switch (r_nombre)
		{
			case "Vista":
			{
				ArrayList<MapeoVistas> vector = new ArrayList<MapeoVistas>();
				consultaVistasServer cvs = consultaVistasServer.getInstance(CurrentUser.get());
				vector = cvs.datosGlobal(null);
				
				for (int i =0 ; i < vector.size(); i++)
				{
					MapeoVistas mapeoVistas = (MapeoVistas) vector.get(i);
					r_combo.addItem(mapeoVistas.getNombre());
				}
				vector=null;
				cvs=null;
				break;
			}
			case "Departamento":
			{
				r_combo.addItem("Informatica");
				r_combo.addItem("Laboratorio");
				r_combo.addItem("Calidad");
				r_combo.addItem("Comercial");				
				r_combo.addItem("Compras");
				r_combo.addItem("Produccion");
				r_combo.addItem("Almacen");
				r_combo.addItem("Financiero");
				r_combo.addItem("Gerencia");
				r_combo.setNullSelectionAllowed(false);
				r_combo.setNewItemsAllowed(false);
				break;
			}
		}
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarUsuario(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.vista.getValue()!=null && this.vista.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("vista", this.vista.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("vista", "");
		}
		
		if (this.firma.getValue()!=null && this.firma.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("firma", this.firma.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("firma", "");
		}
		
		if (this.departamento.getValue()!=null && this.departamento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("departamento", this.departamento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("departamento", "");
		}
		
		if (this.usuario.getValue()!=null && this.usuario.getValue().length()>0) 
		{
			opcionesEscogidas.put("usuario", this.usuario.getValue());
		}
		else
		{
			opcionesEscogidas.put("usuario", "");
		}
		if (this.usuarioCorreo.getValue()!=null && this.usuarioCorreo.getValue().length()>0) 
		{
			opcionesEscogidas.put("usuarioCorreo", this.usuarioCorreo.getValue());
		}
		else
		{
			opcionesEscogidas.put("usuarioCorreo", "");
		}

		if (this.codigoGestion.getValue()!=null && this.codigoGestion.getValue().length()>0) 
		{
			opcionesEscogidas.put("codigoGestion", this.codigoGestion.getValue());
		}
		else
		{
			opcionesEscogidas.put("codigoGestion", "");
		}

		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.password.getValue()!=null && this.password.getValue().length()>0) 
		{
			opcionesEscogidas.put("password", this.password.getValue());
		}
		else
		{
			opcionesEscogidas.put("password", "");
		}
		if (this.viewDefecto.getValue()!=null && this.viewDefecto.getValue().length()>0) 
		{
			opcionesEscogidas.put("viewDefecto", this.viewDefecto.getValue());
		}
		else
		{
			opcionesEscogidas.put("viewDefecto", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarUsuario(null);
	}
	
	public void editarUsuario(MapeoUsuarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoUsuarios();
		fieldGroup.setItemDataSource(new BeanItem<MapeoUsuarios>(r_mapeo));
		usuario.focus();
	}
	
	public void eliminarUsuario(MapeoUsuarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaUsuariosServer(CurrentUser.get());
		((UsuariosGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarUsuario(MapeoUsuarios r_mapeo, MapeoUsuarios r_mapeo_orig)
	{
		cus  = new consultaUsuariosServer(CurrentUser.get());
		cus.guardarCambios(r_mapeo);		
		((UsuariosGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearUsuario(MapeoUsuarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaUsuariosServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoUsuarios>(r_mapeo));
			((UsuariosGrid) uw.grid).refresh(r_mapeo,null);
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoUsuarios> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoUsuarios= item.getBean();
            if (this.mapeoUsuarios.getUsuario()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

}
