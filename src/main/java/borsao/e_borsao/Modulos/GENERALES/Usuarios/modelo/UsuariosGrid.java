package borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.view.UsuariosView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class UsuariosGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private UsuariosView app = null;
    public UsuariosGrid(UsuariosView r_app, ArrayList<MapeoUsuarios> r_vector) {
        
        this.vector=r_vector;
		this.asignarTitulo("Usuarios");
		this.app=r_app;

		if (this.vector==null ||  this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoUsuarios.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setConTotales(true);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("usuario", "nombre", "password","vista","viewDefecto","departamento","usuarioCorreo","firma", "codigoGestion");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("usuario").setHeaderCaption("User");
    	this.getColumn("nombre").setHeaderCaption("Nombre");
    	this.getColumn("password").setHeaderCaption("Password");
    	this.getColumn("vista").setHeaderCaption("Vista");    	
    	this.getColumn("viewDefecto").setHeaderCaption("Pantalla Inicio");
    	this.getColumn("departamento").setHeaderCaption("Departamento");
    	this.getColumn("usuarioCorreo").setHeaderCaption("Usuario Correo");
    	this.getColumn("firma").setHeaderCaption("Fichero Firma");
    	this.getColumn("codigoGestion").setHeaderCaption("Codigo Cliente");
    	this.getColumn("idCodigo").setHidden(true);
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void asignarEstilos() 
	{
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
//    	Indexed indexed = this.getContainerDataSource();
//        List<?> list = new ArrayList<Object>(indexed.getItemIds());
//        for(Object itemId : list)
//        {
//        	Item item = indexed.getItem(itemId);
//        	
//        	valor = (Integer) item.getItemProperty("stock_actual").getValue();
//        	totalU += valor.longValue();
//        }
//        
//        footer.getCell("articulo").setText("Totales ");
//		footer.getCell("stock_actual").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
//		footer.getCell("stock_actual").setStyleName("Rcell-pie");
		
	}
}
