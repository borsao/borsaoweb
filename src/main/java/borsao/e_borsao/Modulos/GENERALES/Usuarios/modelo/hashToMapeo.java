package borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoUsuarios mapeo = null;
	 
	 public MapeoUsuarios convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoUsuarios();
		 this.mapeo.setVista(r_hash.get("vista"));
		 this.mapeo.setFirma(r_hash.get("firma"));
		 this.mapeo.setViewDefecto(r_hash.get("viewDefecto"));
		 this.mapeo.setUsuario(r_hash.get("usuario"));
		 this.mapeo.setUsuarioCorreo(r_hash.get("usuarioCorreo"));
		 this.mapeo.setCodigoGestion(r_hash.get("codigoGestion"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 this.mapeo.setPassword(r_hash.get("password"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoUsuarios convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoUsuarios();
		 this.mapeo.setVista(r_hash.get("vista"));
		 this.mapeo.setFirma(r_hash.get("firma"));
		 this.mapeo.setViewDefecto(r_hash.get("viewDefecto"));
		 this.mapeo.setUsuarioCorreo(r_hash.get("usuarioCorreo"));
		 this.mapeo.setCodigoGestion(r_hash.get("codigoGestion"));
		 this.mapeo.setUsuario(r_hash.get("usuario"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 this.mapeo.setPassword(r_hash.get("password"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoUsuarios r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("vista", r_mapeo.getVista());
		 this.hash.put("firma", r_mapeo.getFirma());		 
		 this.hash.put("usuario", r_mapeo.getUsuario());
		 this.hash.put("usuarioCorreo", r_mapeo.getUsuarioCorreo());
		 this.hash.put("codigoGestion", r_mapeo.getCodigoGestion());
		 this.hash.put("nombre", this.mapeo.getNombre());
		 this.hash.put("departamento", this.mapeo.getDepartamento());
		 this.hash.put("viewDefecto", this.mapeo.getViewDefecto());
		 this.hash.put("password", this.mapeo.getpassword());
		 return hash;		 
	 }
}