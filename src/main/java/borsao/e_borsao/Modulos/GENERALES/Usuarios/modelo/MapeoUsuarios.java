package borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo;

import javax.validation.constraints.Size;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoUsuarios  extends MapeoGlobal
{
    @Size(min = 2, message = "Product name must have at least two characters")
    private String productName = "";
    
	private String usuario;
	private String nombre;
	private String firma;
	private String password;
	private String vista;
	private String viewDefecto;
	private String departamento;
	private String usuarioCorreo;
	private String codigoGestion;

	public MapeoUsuarios()
	{
		this.setUsuario("");
		this.setUsuarioCorreo("");
		this.setNombre("");
		this.setPassword("");
		this.setDepartamento("");
		this.setViewDefecto("");
		this.setVista("");
		this.setCodigoGestion("");
		this.setFirma("");
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String r_usuario) {
		this.usuario = r_usuario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String r_nombre) {
		this.nombre = r_nombre;
	}

	public String getpassword() {
		return password;
	}

	public void setPassword(String r_password) {
		this.password= r_password;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String r_departamento) {
		this.departamento = r_departamento;
	}

	public String getVista() {
		return vista;
	}

	public void setVista(String r_vista) {
		this.vista = r_vista;
	}

	public String getViewDefecto() {
		return viewDefecto;
	}

	public void setViewDefecto(String r_view) {
		this.viewDefecto= r_view;
	}

	public String getUsuarioCorreo() {
		return usuarioCorreo;
	}

	public void setUsuarioCorreo(String usuarioCorreo) {
		this.usuarioCorreo = usuarioCorreo;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String getCodigoGestion() {
		return codigoGestion;
	}

	public void setCodigoGestion(String codigoGestion) {
		this.codigoGestion = codigoGestion;
	}
}