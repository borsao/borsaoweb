package borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.modelo.MapeoImpresoras;
import borsao.e_borsao.Modulos.GENERALES.Impresoras.server.consultaImpresorasServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.view.ImpresorasUsuariosView;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.modelo.MapeoUsuarios;
import borsao.e_borsao.Modulos.GENERALES.Usuarios.server.consultaUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	private ImpresorasUsuariosView av = null;
	private boolean editable = true;
	private boolean conFiltro = false;
	
	
    public OpcionesGrid(ArrayList<MapeoImpresorasUsuarios> r_vector, ImpresorasUsuariosView r_view) 
    {
    	this.av=r_view;
        this.vector=r_vector;
		this.asignarTitulo("Impresoras Usuarios");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
        this.crearGrid(MapeoImpresorasUsuarios.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoImpresorasUsuarios>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());    	
    	super.doEditItem();
    	av.botonesGenerales(true);
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	     av.botonesGenerales(true);
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("impresora","usuario","defecto");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("impresora").setHeaderCaption("Impresora");
    	this.getColumn("usuario").setHeaderCaption("Usuario");
    	this.getColumn("defecto").setHeaderCaption("Por Defecto");
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    	this.getColumn("impresora").setEditorField(
				getComboBox("La impresora es obligatoria!", "impresora"));
    	this.getColumn("usuario").setEditorField(
				getComboBox("El usuario es obligatorio!", "usuario"));
    	this.getColumn("defecto").setEditorField( 
				getComboBox("El valor pod defecto es obligatorio!", "defecto"));

    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "cell-normal";
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	MapeoImpresorasUsuarios mapeo = (MapeoImpresorasUsuarios) getEditedItemId();
	        	consultaImpresorasUsuariosServer cs = new consultaImpresorasUsuariosServer(CurrentUser.get());
	        
	        	if (mapeo.isEliminar())
	        	{
	        		cs.eliminar(mapeo);
	        		remove(mapeo);
	        	}
	        	else
	        	{
	        		cs.guardarNuevo(mapeo);
	        	}
	        }
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}
	
	private Field<?> getComboBox(String requiredErrorMsg, String r_nombre) {
		ComboBox comboBox = new ComboBox();
		comboBox.setNullSelectionAllowed(true);
		ArrayList<String> valores = new ArrayList<String>();
		
		switch (r_nombre)
		{
			case "impresora":
			{
				consultaImpresorasServer cis = new consultaImpresorasServer(CurrentUser.get());
				ArrayList<MapeoImpresoras> vector = cis.datosOpcionesGlobal(null);
				for (int i =0;i<vector.size();i++)
				{
					valores.add(((MapeoImpresoras) vector.get(i)).getImpresora());
				}
				break;
			}
			case "usuario":
			{
				consultaUsuariosServer cus = new consultaUsuariosServer(CurrentUser.get());
				ArrayList<MapeoUsuarios> vector = cus.datosUsuariosGlobal(null);
				for (int i =0;i<vector.size();i++)
				{
					valores.add(((MapeoUsuarios) vector.get(i)).getUsuario());
				}
				break;
			}
			case "defecto":
			{
				valores.add("S");
				valores.add("N");
				break;
			}
		}
		IndexedContainer container = new IndexedContainer(valores);
		comboBox.setContainerDataSource(container);
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
		
		return comboBox;
	}

	@Override
	public void calcularTotal() {
		
	}

}
