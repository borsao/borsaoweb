package borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;

public class consultaImpresorasUsuariosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaImpresorasUsuariosServer instance;
	
	public consultaImpresorasUsuariosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaImpresorasUsuariosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaImpresorasUsuariosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoImpresorasUsuarios> datosOpcionesGlobal()
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoImpresorasUsuarios> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		MapeoImpresorasUsuarios mapeo = null;
		
		cadenaSQL.append(" SELECT impresora imp_imp, ");
		cadenaSQL.append(" usuario imp_usu, ");
		cadenaSQL.append(" defecto imp_def ");
     	cadenaSQL.append(" FROM glb_impresoras_usuarios ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoImpresorasUsuarios>();
			while(rsOpcion.next())
			{
				mapeo = new MapeoImpresorasUsuarios();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setImpresora(rsOpcion.getString("imp_imp"));
				mapeo.setUsuario(rsOpcion.getString("imp_usu"));
				mapeo.setDefecto(rsOpcion.getString("imp_def"));
				vector.add(mapeo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoImpresorasUsuarios> datosOpcionesGlobalUsuario(String r_usuario)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoImpresorasUsuarios> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		MapeoImpresorasUsuarios mapeo = null;
		
		cadenaSQL.append(" SELECT impresora imp_imp, ");
		cadenaSQL.append(" usuario imp_usu, ");
		cadenaSQL.append(" defecto imp_def ");
     	cadenaSQL.append(" FROM glb_impresoras_usuarios ");
     	cadenaSQL.append(" WHERE usuario = '" + r_usuario + "' ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoImpresorasUsuarios>();
//			mapeo = new MapeoImpresorasUsuarios();
			
//			mapeo.setImpresora("Pantalla");
//			mapeo.setUsuario(r_usuario);
//			vector.add(mapeo);	
			if (!rsOpcion.first())
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" SELECT impresora imp_imp ");
		     	cadenaSQL.append(" FROM glb_impresoras");
		     	
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

				while(rsOpcion.next())
				{
					mapeo = new MapeoImpresorasUsuarios();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setImpresora(rsOpcion.getString("imp_imp"));
					mapeo.setUsuario(r_usuario);
					if (rsOpcion.getString("imp_imp").toUpperCase().equals("PANTALLA"))
						mapeo.setDefecto("S");
					else
						mapeo.setDefecto("N");
					vector.add(mapeo);				
				}
			}
			else
			{
				rsOpcion.beforeFirst();
				while(rsOpcion.next())
				{
					mapeo = new MapeoImpresorasUsuarios();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setImpresora(rsOpcion.getString("imp_imp"));
					mapeo.setUsuario(rsOpcion.getString("imp_usu"));
					mapeo.setDefecto(rsOpcion.getString("imp_def"));
					vector.add(mapeo);				
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoImpresorasUsuarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;  
		
		try
		{
			
	        if (r_mapeo.getDefecto().equals("S"))
	        {
	        	cadenaSQL = new StringBuffer();
				cadenaSQL.append(" UPDATE glb_impresoras_usuarios SET ");
				cadenaSQL.append(" glb_impresoras_usuarios.defecto='N' ");
		        cadenaSQL.append(" WHERE glb_impresoras_usuarios.usuario = ? ");
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    preparedStatement.setString(1, r_mapeo.getUsuario());
			    preparedStatement.executeUpdate();    	
	        }
	        
	        cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO glb_impresoras_usuarios ( ");
			cadenaSQL.append(" glb_impresoras_usuarios.impresora, ");
			cadenaSQL.append(" glb_impresoras_usuarios.usuario, ");
	        cadenaSQL.append(" glb_impresoras_usuarios.defecto) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    if (con==null) con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getImpresora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getImpresora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getUsuario()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getUsuario());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDefecto()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDefecto());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		
		return null;
	}
	
	public void eliminar(MapeoImpresorasUsuarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_impresoras_usuarios ");            
			cadenaSQL.append(" WHERE glb_impresoras_usuarios.impresora = ?");
			cadenaSQL.append(" and glb_impresoras_usuarios.usuario = ?");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getImpresora());
			preparedStatement.setString(2, r_mapeo.getUsuario());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}