package borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoImpresorasUsuarios extends MapeoGlobal
{
	private String impresora;
	private String usuario;
	private String defecto;
	private boolean eliminar=false;

	public MapeoImpresorasUsuarios()
	{
	}

	public String getImpresora() {
		return impresora;
	}

	public void setImpresora(String impresora) {
		this.impresora = impresora;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

	public String getDefecto() {
		return defecto;
	}

	public void setDefecto(String defecto) {
		this.defecto = defecto;
	}


}