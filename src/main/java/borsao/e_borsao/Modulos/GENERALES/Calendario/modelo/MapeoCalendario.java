package borsao.e_borsao.Modulos.GENERALES.Calendario.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCalendario extends MapeoGlobal
{
	private String caption = null;
	private String almacen = null;
	private Integer id = null;
	private Integer semana = null;
	private Integer mes = null;
	private String area = null;
	private String description= null;
	private String propietario= null;
	private String styleName= null;
	private String valorAviso= null;
	private String tipo= null;
	private String ejercicioContable= null;
	private String turno= null;
	private Date start = null;
	private Date end = null;
	private Date aviso = null;
	private boolean isAllDay;
	private boolean isAviso;
	private boolean isCompletada;

	public MapeoCalendario()
	{
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStyleName() {
		return styleName;
	}

	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}

	public String getValorAviso() {
		return valorAviso;
	}

	public void setValorAviso(String valorAviso) {
		this.valorAviso = valorAviso;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Date getAviso() {
		return aviso;
	}

	public void setAviso(Date aviso) {
		this.aviso = aviso;
	}

	public boolean isAllDay() {
		return isAllDay;
	}

	public void setAllDay(boolean isAllDay) {
		this.isAllDay = isAllDay;
	}

	public boolean isAviso() {
		return isAviso;
	}

	public void setAviso(boolean isAviso) {
		this.isAviso = isAviso;
	}

	public boolean isCompletada() {
		return isCompletada;
	}

	public void setCompletada(boolean isCompletada) {
		this.isCompletada = isCompletada;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public String getTipo() {
		return tipo;
	}

	public String getEjercicioContable() {
		return ejercicioContable;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setEjercicioContable(String ejercicioContable) {
		this.ejercicioContable = ejercicioContable;
	}
}