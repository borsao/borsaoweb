package borsao.e_borsao.Modulos.GENERALES.Calendario.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.CalendarioVacacionesGrid;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoGridVacaciones;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class VacacionesView extends GridViewRefresh {

	private final String titulo = "control VACACIONES";
	public static final String VIEW_NAME = "Control Vacaciones";

	public consultaCalendarioServer cus =null;
	public VacacionesView app = null;
	public boolean conTotales = true;
	public boolean modificando = false;
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private Integer permisos = 0;	
	private final boolean soloConsulta = false;
	private final boolean autoSincronizacion = false;
	private HashMap<String,String> filtrosRec = null;	

	public TextField txtEjercicio = null;
	public Label lblDiasVacaciones = null;
	public ComboBox cmbEstado = null;
	public ComboBox cmbArea = null;
	public String diasVacacionesEjercicio = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public VacacionesView() 
    {
    	this.cus= consultaCalendarioServer.getInstance(CurrentUser.get());
    	this.opcionesEscogidas = new HashMap<String , String>();
    	
    }

    public void cargarPantalla() 
    {
		this.permisos = new Integer(this.cus.comprobarAccesos());
		this.permisos=99;
		if (this.permisos==99) this.intervaloRefresco=0;
    	
    	if (this.permisos==0)
    	{
    		Notificaciones.getInstance().mensajeError("No tienes acceso a este programa");
    		this.destructor();
    	}
    	else
    	{
    		this.txtEjercicio = new TextField("Ejercicio");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
    		
    		this.lblDiasVacaciones = new Label();
    		this.lblDiasVacaciones.addStyleName(ValoTheme.LABEL_TINY);
    		this.lblDiasVacaciones.setCaption("Dias Vacaciones del Ejercicio");
    		
    		this.cmbEstado= new ComboBox("Consulta Calendario");    		
    		this.cmbEstado.setNewItemsAllowed(false);
    		this.cmbEstado.setNullSelectionAllowed(false);
    		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);

    		this.cmbArea= new ComboBox("Departamento");    		
    		this.cmbArea.setNewItemsAllowed(false);
    		this.cmbArea.setNullSelectionAllowed(false);
    		this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);
    		
    		this.cabLayout.addComponent(this.txtEjercicio);
    		this.cabLayout.addComponent(this.cmbEstado);
    		this.cabLayout.addComponent(this.cmbArea);
    		this.cabLayout.addComponent(this.lblDiasVacaciones);

    		this.cargarCombo();
    		this.cargarListeners();
    		this.establecerModo();
    	
			consultaParametrosCalidadServer cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
			this.diasVacacionesEjercicio = cpcs.recuperarParametro(new Integer(this.txtEjercicio.getValue()), "Vacaciones");
			this.lblDiasVacaciones.setValue(this.diasVacacionesEjercicio);
			this.generarGrid(opcionesEscogidas);
			app = this;
			
	    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
	    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);

    	}

    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoGridVacaciones> r_vector=null;
    	MapeoGridVacaciones mapeoVacaciones =null;
    	
    	mapeoVacaciones = new MapeoGridVacaciones();
    	mapeoVacaciones.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
    	mapeoVacaciones.setAlmacen(this.cmbArea.getValue().toString());
    	mapeoVacaciones.setPropietario(this.cmbEstado.getValue().toString());
    	if (this.permisos==99)
    	{
    		mapeoVacaciones.setCaption(null);
    	}
    	r_vector=this.cus.datosOpcionesGlobal(mapeoVacaciones,this.diasVacacionesEjercicio);
    	
    	this.presentarGrid(r_vector);
    	((CalendarioVacacionesGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    }
    
    public void verForm(boolean r_busqueda)
    {
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((CalendarioVacacionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((CalendarioVacacionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((CalendarioVacacionesGrid) this.grid).activadaVentanaPeticion && !((CalendarioVacacionesGrid) this.grid).ordenando)
    	{
    	}    		
    }
    
    public void generarGrid(MapeoGridVacaciones r_mapeo)
    {
    	ArrayList<MapeoGridVacaciones> r_vector=null;
    	r_vector=this.cus.datosOpcionesGlobal(r_mapeo,this.diasVacacionesEjercicio);
    	this.presentarGrid(r_vector);
    	((CalendarioVacacionesGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoGridVacaciones> r_vector)
    {
    	grid = new CalendarioVacacionesGrid(this, this.permisos,r_vector);
    	if (((CalendarioVacacionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((CalendarioVacacionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((CalendarioVacacionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoGridVacaciones mapeo = new MapeoGridVacaciones();
					
					if (txtEjercicio.getValue()!=null)
						mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
					else
						mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
					
					if (cmbEstado.getValue()!=null) 
					{
						mapeo.setPropietario(cmbEstado.getValue().toString());
					}
					if (cmbArea.getValue()!=null)
					{
						mapeo.setAlmacen(cmbArea.getValue().toString());
					}
					generarGrid(mapeo);
				}
			}
		}); 

		this.txtEjercicio.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((CalendarioVacacionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoGridVacaciones mapeo = new MapeoGridVacaciones();
					
					if (txtEjercicio.getValue()!=null)
						mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
					else
						mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
					
					if (cmbEstado.getValue()!=null) 
					{
						mapeo.setPropietario(cmbEstado.getValue().toString());
					}
					if (cmbArea.getValue()!=null)
					{
						mapeo.setAlmacen(cmbArea.getValue().toString());
					}
					generarGrid(mapeo);
				}
			}
		});
		
		this.cmbArea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((CalendarioVacacionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoGridVacaciones mapeo = new MapeoGridVacaciones();
					if (txtEjercicio.getValue()!=null)
						mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
					else
						mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
					
					if (cmbEstado.getValue()!=null) 
					{
						mapeo.setPropietario(cmbEstado.getValue().toString());
					}
					if (cmbArea.getValue()!=null)
					{
						mapeo.setAlmacen(cmbArea.getValue().toString());
					}
					generarGrid(mapeo);
				}
			}
		}); 
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
		this.setSoloConsulta(true);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	private void establecerBotonAccion(Object r_fila)
	{
	}

	public void imprimirVacaciones()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		this.cmbEstado.addItem("Vacaciones");
		this.cmbEstado.setValue("Vacaciones");
		
		this.cmbArea.removeAllItems();
		this.cmbArea.addItem("Generales");
		this.cmbArea.addItem("Embotelladora");
		this.cmbArea.addItem("Envasadora");
		this.cmbArea.addItem("BIB");
		this.cmbArea.addItem("Limpieza");
		this.cmbArea.addItem("Laboratorio");
		this.cmbArea.addItem("Tecnicos");
		this.cmbArea.addItem("Almacen");
		this.cmbArea.addItem("Bodega");
		this.cmbArea.addItem("Todas");
		this.cmbArea.setValue("Todas");
		

	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((CalendarioVacacionesGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

