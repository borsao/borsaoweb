package borsao.e_borsao.Modulos.GENERALES.Calendario.view;

import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Agenda;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PantallaEventoAgenda;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class CalendarioView extends GridViewRefresh {

	public static final String VIEW_NAME = "Agenda";
	private final String titulo = "Calendario";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private VerticalLayout panelCalendario = null;
	private HorizontalLayout lay  = null;
	private HorizontalLayout panelControlesCalendario = null;
	private HorizontalLayout layh = null;
	public Combo tipoCalendario=null;
	private Combo almacen=null;
	private String almacenDefecto=null;
    public Button opcMes = null;
    public Button opcLaboral = null;
    public Button opc24 = null;
    public Button copiarEventos = null;
	public Agenda agen = null;
	private Panel panelAgenda = null;
	public Button btnAnterior = null;
	public Button btnSiguiente = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public CalendarioView() 
    {
    	setConFormulario(this.conFormulario);
    	setHayGrid(false);
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());			
		this.almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());

    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
    	
    	this.opcImprimir.setEnabled(true);
    	this.opcImprimir.setVisible(true);
    	
		this.panelCalendario = new VerticalLayout();
		this.panelCalendario.setMargin(false);
		this.panelCalendario.setSpacing(true);

			this.panelControlesCalendario = new HorizontalLayout();
			this.panelControlesCalendario.setMargin(true);
			this.panelControlesCalendario.setSpacing(true);

				this.tipoCalendario = new Combo("Tipo Calendario");
				this.tipoCalendario.addStyleName(ValoTheme.COMBOBOX_TINY);
				this.tipoCalendario.setNewItemsAllowed(false);
				this.tipoCalendario.setNullSelectionAllowed(false);
				this.cargarCombo();
				
				this.almacen= new Combo("Almacen");
				this.almacen.addStyleName(ValoTheme.COMBOBOX_TINY);
				this.almacen.setNewItemsAllowed(false);
				this.almacen.setNullSelectionAllowed(false);
				this.cargarComboAlmacen();
			
				this.opcMes= new Button("Mes");
				this.opcMes.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.opcMes.addStyleName(ValoTheme.BUTTON_TINY);
				this.opcMes.setIcon(FontAwesome.CALENDAR);
				
				this.opcLaboral= new Button("Laborables");
				this.opcLaboral.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.opcLaboral.addStyleName(ValoTheme.BUTTON_TINY);
				this.opcLaboral.setIcon(FontAwesome.CALENDAR_TIMES_O);
				
				this.opc24= new Button("24H");
				this.opc24.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.opc24.addStyleName(ValoTheme.BUTTON_TINY);
				this.opc24.setIcon(FontAwesome.INDUSTRY);

				this.copiarEventos= new Button("Copiar");
				this.copiarEventos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.copiarEventos.addStyleName(ValoTheme.BUTTON_TINY);
				this.copiarEventos.setIcon(FontAwesome.COPY);
				this.copiarEventos.setEnabled(false);

			this.panelControlesCalendario.addComponent(this.tipoCalendario);
			this.panelControlesCalendario.addComponent(this.almacen);
	    	this.panelControlesCalendario.addComponent(this.opcMes);
			this.panelControlesCalendario.addComponent(this.opcLaboral);
			this.panelControlesCalendario.addComponent(this.opc24);
			this.panelControlesCalendario.addComponent(this.copiarEventos);
			this.panelControlesCalendario.setComponentAlignment(this.tipoCalendario, Alignment.TOP_LEFT);
			this.panelControlesCalendario.setComponentAlignment(this.almacen, Alignment.TOP_LEFT);
			this.panelControlesCalendario.setComponentAlignment(this.opcMes, Alignment.BOTTOM_RIGHT);
			this.panelControlesCalendario.setComponentAlignment(this.opcLaboral, Alignment.BOTTOM_RIGHT);
			this.panelControlesCalendario.setComponentAlignment(this.opc24, Alignment.BOTTOM_RIGHT);
			this.panelControlesCalendario.setComponentAlignment(this.copiarEventos, Alignment.BOTTOM_RIGHT);
			
			
		this.panelCalendario.addComponent(panelControlesCalendario);
			
		grid=null;
		
		panelAgenda = new Panel("Calendario " + tipoCalendario.getValue().toString());
		panelAgenda.setSizeFull();
			lay = new HorizontalLayout();
//			lay.setHeight("100%");
//			lay.setSizeFull();
			lay.setSpacing(true);
			
				btnAnterior = new Button("<");
				btnAnterior.addStyleName(ValoTheme.BUTTON_PRIMARY);
				btnAnterior.addStyleName(ValoTheme.BUTTON_SMALL);
				btnAnterior.addStyleName(ValoTheme.BUTTON_TINY);
				btnAnterior.setVisible(false);
				btnAnterior.addClickListener(new ClickListener() {
				
					@Override
					public void buttonClick(ClickEvent event) {
						mesAnterior();
					}
				});

				btnSiguiente = new Button(">");
				btnSiguiente.addStyleName(ValoTheme.BUTTON_PRIMARY);
				btnSiguiente.addStyleName(ValoTheme.BUTTON_SMALL);
				btnSiguiente.addStyleName(ValoTheme.BUTTON_TINY);
				btnSiguiente.setVisible(false);
				btnSiguiente.addClickListener(new ClickListener() {
					
					@Override
					public void buttonClick(ClickEvent event) {
						mesSiguiente();
					}
				});
			
				lay.addComponent(btnAnterior);
				lay.addComponent(btnSiguiente);

//		lay.addComponent(layh);
		lay.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");
		panelAgenda.setContent(lay);
		
		if (tipoCalendario.getValue().toString().equals("Almacen"))
		{
			cargarAgenda(almacen.getValue().toString());
			this.opc24.setVisible(false);
		}
		else
		{
			cargarAgenda(null);
		}
		if (agen!=null)
		{
			this.opcNuevo.setEnabled(!agen.isSoloLectura);
		}
		
    	this.cabLayout.addComponent(this.panelCalendario);
    	this.cabLayout.setComponentAlignment(this.panelCalendario, Alignment.TOP_LEFT);
    	barAndGridLayout.addComponent(panelAgenda);
    	barAndGridLayout.setExpandRatio(panelAgenda, 2);
    	
		barAndGridLayout.setWidth("100%");
		barAndGridLayout.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");

    	this.cargarListeners();
    }

    private void cargarAgenda(String r_valor)
    {
    	if (agen!=null)
    	{
    		lay.removeComponent(agen);
    		agen=null;
    	}
    	agen = new Agenda(this, tipoCalendario.getValue().toString(),"Calendario " + tipoCalendario.getValue().toString(),400,400, r_valor);
    	lay.addComponent(agen);
    	agen.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");
    	lay.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");
		if (agen!=null)
		{
			this.opcNuevo.setEnabled(!agen.isSoloLectura);
		}

    }
    
    private void mesAnterior()
    {
    	String mes = RutinasFechas.mesFecha(agen.getStartDate());
    	String ano = RutinasFechas.añoFecha(agen.getStartDate());
    	
    	Integer nuevoMes = new Integer(mes) -1;
    	Integer nuevoAno = new Integer(ano);
    	
    	if (nuevoMes<1)
    	{
    		nuevoMes = 12;
    		nuevoAno = nuevoAno - 1;
    	}
    	GregorianCalendar start = new GregorianCalendar();
    	start.setTime(RutinasFechas.conversionDeString("01/" + nuevoMes.toString() + "/" + ano));
    	
    	GregorianCalendar end = new GregorianCalendar();
    	end.setTime(RutinasFechas.conversionDeString("01/" + nuevoMes.toString() + "/" + ano));
    	
    	end.add(java.util.Calendar.HOUR, 720);
		
    	agen.eliminarEventos();
		agen.setStartDate(start.getTime());
		agen.setEndDate(end.getTime());
		agen.recuerparDatosBBDD(agen.getStartDate(), agen.getEndDate());
		agen.setCaption(RutinasFechas.obtenerMesLetra(nuevoMes));

    }

    private void mesSiguiente()
    {
    	String mes = RutinasFechas.mesFecha(agen.getStartDate());
    	String ano = RutinasFechas.añoFecha(agen.getStartDate());
    	
    	Integer nuevoMes = new Integer(mes) +1;
    	Integer nuevoAno = new Integer(ano);
    	
    	if (nuevoMes>12)
    	{
    		nuevoMes = 1;
    		nuevoAno = nuevoAno+ 1;
    	}
    	
    	GregorianCalendar start = new GregorianCalendar();
    	start.setTime(RutinasFechas.conversionDeString("01/" + nuevoMes.toString() + "/" + nuevoAno.toString()));
    	
    	GregorianCalendar end = new GregorianCalendar();
    	end.setTime(RutinasFechas.conversionDeString("01/" + nuevoMes.toString() + "/" + nuevoAno.toString()));
    	
    	end.add(java.util.Calendar.HOUR, 720);
    	
    	agen.eliminarEventos();
    	agen.setStartDate(start.getTime());
    	agen.setEndDate(end.getTime());
    	agen.recuerparDatosBBDD(agen.getStartDate(), agen.getEndDate());
    	agen.setCaption(RutinasFechas.obtenerMesLetra(nuevoMes));
    	
    }
    
    
    private void cargarListeners()
    {
    	this.tipoCalendario.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
//				if (agen!=null)
//				{
//					barAndGridLayout.removeComponent(agen);
//					agen=null;
//				}
				if (tipoCalendario.getValue().toString().equals("Almacen"))
				{
					almacen.setVisible(true);
					
					cargarAgenda(almacen.getValue().toString());
					opc24.setVisible(false);
				}
				else 
				{
					almacen.setVisible(false);
					cargarAgenda(null);
					opc24.setVisible(true);
				}
				
				panelAgenda.setCaption("Calendario " + tipoCalendario.getValue().toString());
//				if (!tipoCalendario.getValue().toString().contentEquals("Vacaciones"))
//				{
//					opcNuevo.setEnabled(!agen.isSoloLectura);
//				}
				if (tipoCalendario.getValue().toString().equals("Turnos")) copiarEventos.setEnabled(!agen.isSoloLectura);
				
//		    	barAndGridLayout.setExpandRatio(agen, 2);
		    	
				barAndGridLayout.setWidth("100%");
				barAndGridLayout.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");			

			}
		});
    	
    	this.almacen.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				cargarAgenda(almacen.getValue().toString());
				barAndGridLayout.setWidth("100%");
				barAndGridLayout.setHeight((getHeight()-cabLayout.getHeight()-lblSeparador.getHeight()-topLayout.getHeight()-10)+"%");		
			}
		});
    	this.opcMes.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			
    	    	String mes = RutinasFechas.mesFecha(agen.getStartDate());
    	    	String ano = RutinasFechas.añoFecha(agen.getStartDate());
    	    	
    	    	GregorianCalendar start = new GregorianCalendar();
    	    	start.setTime(RutinasFechas.conversionDeString("01/" + mes + "/" + ano));
    	    	
    	    	GregorianCalendar end = new GregorianCalendar();
    	    	end.setTime(RutinasFechas.conversionDeString("01/" + mes + "/" + ano));
    	    	
    	    	end.add(java.util.Calendar.HOUR, 720);
    			
    	    	agen.eliminarEventos();
    			agen.setStartDate(start.getTime());
    			agen.setEndDate(end.getTime());
    			agen.recuerparDatosBBDD(agen.getStartDate(), agen.getEndDate());
    			agen.setCaption(RutinasFechas.obtenerMesLetra(new Integer(mes)));
    			
    			btnAnterior.setVisible(true);
    			btnSiguiente.setVisible(true);
    		}
    	});
    	
    	this.opcLaboral.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (agen.getLastVisibleDayOfWeek()==7)
    			{
    				agen.setFirstVisibleDayOfWeek(1);
    				agen.setLastVisibleDayOfWeek(5);
    				opcLaboral.setCaption("Todos");
    			}
    			else if (agen.getLastVisibleDayOfWeek()==5)
    			{
    				agen.setFirstVisibleDayOfWeek(1);
    				agen.setLastVisibleDayOfWeek(7);
    				opcLaboral.setCaption("Laborables");
    			}
    		}
    	});

    	this.opc24.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(agen.getFirstVisibleHourOfDay()));
    			Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(agen.getLastVisibleHourOfDay()));
    			
    			if (agen.getFirstVisibleHourOfDay()==0)
    			{
    				agen.setFirstVisibleHourOfDay(6);
    				agen.setLastVisibleHourOfDay(22);
    				opc24.setCaption("24H");
    			}
    			else if (agen.getFirstVisibleHourOfDay()==6)
    			{
    				agen.setFirstVisibleHourOfDay(0);
    				agen.setLastVisibleHourOfDay(23);
    				opc24.setCaption("16H");
    			}
    		}
    	});
    	
    	this.copiarEventos.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) 
    		{
    			agen.copiarEventos();
    			opcImprimir.setEnabled(true);
    		}
    	});
    }
    
    public void newForm()
    {
    	if (agen!=null && !agen.isSoloLectura)
    	{
    		PantallaEventoAgenda vt = new PantallaEventoAgenda(this);
    		getUI().addWindow(vt);
    	}
    	else
    	{
    		this.botonesGenerales(true);
    	}
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    @Override
    public void reestablecerPantalla() 
    {
    	
    }

    public void print() 
    {
    	Integer rdo = null;
    	String sql = null;
    	String tabla = eBorsao.get().accessControl.getNombre().replace(" ", "") + tipoCalendario.getValue().toString(); 
    	/*
    	 * Generamos tabla temporal con los datos a imprimir
    	 * Todos identificados con un id que le pasamos a la pantella de impresion
    	 */
    	rdo=this.agen.generarEventosParaImprimir();
    	
    	if (rdo!=null && rdo!=0)
    	{
    		Notificaciones.getInstance().mensajeError("Datos generados. Actualiza la excel.");
//    		if (tipoCalendario.getValue().toString().equals("Turnos")) sql = "select " + tabla + ".* from " + tabla + " where idImpresion = " + rdo + " order by semana, area, inicio,turno ";
//    		if (tipoCalendario.getValue().toString().equals("Vacaciones")) sql = "select " + tabla + ".* from " + tabla + " where idImpresion = " + rdo + " order by mes, area, evento ";
////    		    			Notificaciones.getInstance().mensajeSeguimiento("El sql que meto al listado es: " + sql );
//    		PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion(rdo,"Calendario",sql,tipoCalendario.getValue().toString());
//    		getUI().addWindow(vtPeticion);
	
    		this.botonesGenerales(true);
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("No se han podido generar los datos a imprimir");
    		this.botonesGenerales(true);
    	}
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}
	
	private void cargarCombo()
	{
		this.tipoCalendario.addItem("Vacaciones");
		this.tipoCalendario.addItem("Turnos");
		this.tipoCalendario.addItem("Almacen");
		
		if (eBorsao.get().accessControl.getNombre().contains("Embotelladora") || eBorsao.get().accessControl.getNombre().contains("bib") || eBorsao.get().accessControl.getNombre().contains("Envasadora"))
			this.tipoCalendario.setValue("Turnos");
		else if (eBorsao.get().accessControl.getNombre().contains("lmacen"))
		{
			this.tipoCalendario.setValue("Almacen");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("laveria"))
		{
			this.tipoCalendario.addItem("Personal");
			this.tipoCalendario.setValue("Personal");
		}
		else
		{
			this.tipoCalendario.setValue("Almacen");
		}
		
			
	}
	
	private void cargarComboAlmacen()
	{
		this.almacen.addItem("Capuchinos");
		this.almacen.addItem("Tejar");
		this.almacen.addItem("Productos Vendimia");
		this.almacen.addItem("Reservas Stock");
		
		if (this.almacenDefecto.equals("1"))
			this.almacen.setValue("Capuchinos");
		else if (this.almacenDefecto.equals("4"))
			this.almacen.setValue("Tejar");
		else
			this.almacen.setValue("Capuchinos");

		if (this.tipoCalendario.getValue().equals("Almacen")) this.almacen.setVisible(true); else this.almacen.setVisible(false);
			
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
