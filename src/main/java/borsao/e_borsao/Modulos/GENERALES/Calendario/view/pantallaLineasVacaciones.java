package borsao.e_borsao.Modulos.GENERALES.Calendario.view;

import java.util.ArrayList;

import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.CALIDAD.ParametrosCalidad.server.consultaParametrosCalidadServer;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.CalendarioGrid;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoCalendario;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoGridVacaciones;
import borsao.e_borsao.Modulos.GENERALES.Calendario.server.consultaCalendarioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasVacaciones extends Window
{
	public consultaCalendarioServer ccs =null;
	
	public VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	private Grid grid = null;
	
	private final String titulo = "Detalle Vacaciones Solicitadas / disfrutadas ";
	private boolean hayGrid=false;
	private MapeoGridVacaciones mapeo = null;
	private String diasVacacionesEjercicio = null;
	private String ejercicio = null;
	private VacacionesView app = null;
	
	private Label lblDiasTotales = null;  
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public pantallaLineasVacaciones(VacacionesView r_app, MapeoGridVacaciones r_mapeo) 
    {
		this.setCaption(titulo);
		this.center();
		
		this.app = r_app;
		this.mapeo = r_mapeo;
		this.ejercicio = r_app.txtEjercicio.getValue().toString();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();
		
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);
    	
    }

    public void cargarPantalla() 
    {
    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);

    		this.lblDiasTotales = new Label();
    		this.lblDiasTotales.setCaption("Dias Vacaciones Ejercicio " + this.app.txtEjercicio.getValue().toString());
    		this.lblDiasTotales.setValue(this.app.diasVacacionesEjercicio + " días laborables ");
    		this.lblDiasTotales.addStyleName(ValoTheme.LABEL_H4);
    		
    	this.cabLayout.addComponent(this.lblDiasTotales);
		this.barAndGridLayout.addComponent(this.cabLayout);
		
		this.generarGrid();
    }

    public void generarGrid()
    {
    	ArrayList<MapeoGridVacaciones> r_vector=null;
    	ccs = new consultaCalendarioServer(CurrentUser.get());

		consultaParametrosCalidadServer cpcs = consultaParametrosCalidadServer.getInstance(CurrentUser.get());
		this.diasVacacionesEjercicio = cpcs.recuperarParametro(new Integer(this.ejercicio), "Vacaciones");

    	MapeoCalendario mapeoCalendario = new MapeoCalendario();
    	mapeoCalendario.setPropietario(this.mapeo.getPropietario());
    	mapeoCalendario.setAlmacen(this.mapeo.getAlmacen());
    	mapeoCalendario.setDescription(this.mapeo.getCaption());
    	mapeoCalendario.setEjercicioContable(this.app.txtEjercicio.getValue().toString());
    	
    	r_vector=this.ccs.datosDetalleVacaciones(mapeoCalendario, this.app.diasVacacionesEjercicio);
    	this.presentarGrid(r_vector);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoGridVacaciones> r_vector)
    {
    	
    	grid = new CalendarioGrid(this, r_vector);
    	if (((CalendarioGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else if (((CalendarioGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {
    }
    
    
	private boolean isHayGrid() {
		return hayGrid;
	}

	private void setHayGrid(boolean r_hayGrid) {
		this.hayGrid = r_hayGrid;
		if (r_hayGrid)
		{
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);

			grid.addSelectionListener(new SelectionListener() {
				
				@Override
				public void select(SelectionEvent event) {
						
//					if (grid.getSelectionModel() instanceof SingleSelectionModel)
//						filaSeleccionada(grid.getSelectedRow());
//					else
//						mostrarFilas(grid.getSelectedRows());
				}
			});
		}
		else
		{
		}
	}
}
