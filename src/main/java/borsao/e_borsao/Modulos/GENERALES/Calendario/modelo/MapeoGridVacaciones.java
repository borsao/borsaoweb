package borsao.e_borsao.Modulos.GENERALES.Calendario.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGridVacaciones extends MapeoGlobal
{
	private String caption = null;
	private String almacen = null;
	private String tipo = null;
	private String EjercicioContable= null;
	private String propietario= null;
	private Date start = null;
	private Date end = null;
	private Integer cuantos = null;
	private Integer quedan = null;
	private String bar;
	private Integer ejercicio = null;
	
	public MapeoGridVacaciones() {
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Integer getCuantos() {
		return cuantos;
	}

	public void setCuantos(Integer cuantos) {
		this.cuantos = cuantos;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getQuedan() {
		return quedan;
	}

	public void setQuedan(Integer quedan) {
		this.quedan = quedan;
	}

	public String getTipo() {
		return tipo;
	}

	public String getEjercicioContable() {
		return EjercicioContable;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setEjercicioContable(String ejercicioContable) {
		EjercicioContable = ejercicioContable;
	}


}