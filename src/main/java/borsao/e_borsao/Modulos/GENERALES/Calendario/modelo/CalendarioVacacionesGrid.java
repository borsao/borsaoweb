package borsao.e_borsao.Modulos.GENERALES.Calendario.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.VacacionesView;
import borsao.e_borsao.Modulos.GENERALES.Calendario.view.pantallaLineasVacaciones;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class CalendarioVacacionesGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	private boolean editable = false;
	private boolean conFiltro = true;
	
	public Integer permisos = null;
	private VacacionesView app = null;
	
    public CalendarioVacacionesGrid(VacacionesView r_app , Integer r_permisos, ArrayList<MapeoGridVacaciones> r_vector) 
    {
        this.permisos=r_permisos;
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("CONTROL VACACIONES");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoGridVacaciones.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setStyleName("smallgrid");

		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "almacen", "caption", "start", "end", "cuantos", "bar", "quedan", "tipo", "ejercicioContable");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("almacen", "210");
    	this.widthFiltros.put("ejercicioContable", "80");
    	this.widthFiltros.put("tipo", "150");
    	this.widthFiltros.put("caption", "210");

    	this.getColumn("bar").setHeaderCaption("");
    	this.getColumn("bar").setSortable(false);
    	this.getColumn("bar").setWidth(new Double(40));

    	this.getColumn("start").setHeaderCaption("INICIO");
    	this.getColumn("start").setWidthUndefined();
    	this.getColumn("start").setWidth(120);
    	this.getColumn("end").setHeaderCaption("FIN");
    	this.getColumn("end").setWidthUndefined();
    	this.getColumn("end").setWidth(120);
    	this.getColumn("almacen").setHeaderCaption("Area");
    	this.getColumn("almacen").setWidthUndefined();
    	this.getColumn("almacen").setWidth(250);
    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setWidthUndefined();
    	this.getColumn("tipo").setWidth(250);
    	this.getColumn("caption").setHeaderCaption("Trabajador");
    	this.getColumn("caption").setWidth(250);
    	this.getColumn("cuantos").setHeaderCaption("Dias Cogidos");
    	this.getColumn("cuantos").setWidth(120);
    	this.getColumn("quedan").setHeaderCaption("Dias Quedan");
    	this.getColumn("quedan").setWidth(120);
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(120);
    	this.getColumn("ejercicioContable").setHeaderCaption("Ejercicio Contable");
    	this.getColumn("ejercicioContable").setWidth(120);
    	
    	this.getColumn("start").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("end").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("cuantos").setRenderer((Renderer) new NumberRenderer());
    	this.getColumn("quedan").setRenderer((Renderer) new NumberRenderer());
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("propietario").setHidden(true);
    	this.getColumn("start").setHidden(true);
    	this.getColumn("end").setHidden(true);
    }
    
    private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoRetrabajos) {
                MapeoRetrabajos progRow = (MapeoRetrabajos)bean;
                // The actual description text is depending on the application
                if ("bar".equals(cell.getPropertyId()))
                {
                	descriptionText = "Detalle de Días";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("start".equals(cellReference.getPropertyId())|| "end".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-green";
            	}
            	else if ( "cuantos".equals(cellReference.getPropertyId()) || "quedan".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else if ( "bar".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoGridVacaciones mapeo = (MapeoGridVacaciones) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("bar"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
        			pantallaLineasVacaciones vt = null;
    	    		vt= new pantallaLineasVacaciones(app, mapeo);
    	    		getUI().addWindow(vt);
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("start");
		this.camposNoFiltrar.add("end");
		this.camposNoFiltrar.add("bar");
		this.camposNoFiltrar.add("cuantos");
		this.camposNoFiltrar.add("quedan");
	}

	public void generacionPdf(MapeoRetrabajos r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
    	Integer diasTrabajador=null;
    	Integer diasTrabajadorQuedan=null;
    	Integer valor = 0;
    	Integer valorQuedan = 0;

    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	if (item.getItemProperty("cuantos").getValue()!=null)
        	{
        		diasTrabajador = new Integer(item.getItemProperty("cuantos").getValue().toString());
        		valor=valor+diasTrabajador;
        	}
        	if (item.getItemProperty("quedan").getValue()!=null)
        	{
        		diasTrabajadorQuedan = new Integer(item.getItemProperty("quedan").getValue().toString());
        		valorQuedan=valorQuedan+diasTrabajadorQuedan;
        	}
        }
        
        footer.getCell("caption").setText("Totales ");
		footer.getCell("cuantos").setText(RutinasNumericas.formatearDouble(valor.toString()));
		footer.getCell("quedan").setText(RutinasNumericas.formatearDouble(valorQuedan.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		footer.getCell("cuantos").setStyleName("Rcell-pie");
		footer.getCell("quedan").setStyleName("Rcell-pie");
		

	}
}


