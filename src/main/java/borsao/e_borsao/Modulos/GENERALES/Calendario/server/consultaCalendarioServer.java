package borsao.e_borsao.Modulos.GENERALES.Calendario.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasBaseDatos;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Avisos.modelo.MapeoAvisos;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoCalendario;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoGridVacaciones;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaCalendarioServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCalendarioServer instance;
	private static int min = 60;
	public consultaCalendarioServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCalendarioServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCalendarioServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoCalendario> datosOpcionesGlobal(String r_propietario,String r_almacen,  Date r_inicio, Date r_fin)
	{
		ResultSet rsOpcion = null;		
		String plazoAnterior = "";
		ArrayList<MapeoCalendario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT idEvento ag_id, ");
		cadenaSQL.append(" idPropietario ag_pro, ");
		cadenaSQL.append(" evento ag_eve, ");
		cadenaSQL.append(" inicio ag_ini, ");
		cadenaSQL.append(" fin ag_fin, ");
		cadenaSQL.append(" descripcion ag_des, ");
		cadenaSQL.append(" estilo ag_sty, ");
		cadenaSQL.append(" diaria ag_dia, ");
		cadenaSQL.append(" aviso ag_avi, ");
		cadenaSQL.append(" formulaAviso ag_for, ");
		cadenaSQL.append(" fechaAviso ag_fec, ");
		cadenaSQL.append(" completada ag_com, ");
		cadenaSQL.append(" almacen ag_alm, ");
		cadenaSQL.append(" tipo ag_tip, ");
		cadenaSQL.append(" ejercicioContable ag_eje ");
     	cadenaSQL.append(" FROM agenda ");
     	
     	if (r_propietario=="Personal")
     	{
     		cadenaSQL.append(" WHERE idPropietario = '" + eBorsao.get().accessControl.getNombre() + "'");
     	}
     	else if (r_propietario=="Almacen")
     	{
     		cadenaSQL.append(" WHERE idPropietario = '" + r_propietario + "' AND almacen = '" + r_almacen + "' " );
     	}
     	else 
     	{
     		cadenaSQL.append(" WHERE idPropietario = '" + r_propietario + "'");
     	}
     	
		try
		{
			cadenaSQL.append(" AND (inicio >= '" + RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_inicio)) + "' ");
			cadenaSQL.append(" AND fin < '" + RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_fin) ) + "')");

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoCalendario>();
			
			while(rsOpcion.next())
			{
				MapeoCalendario mapeo = new MapeoCalendario();
				/*
				 * recojo mapeo calendario
				 */
				mapeo.setId(rsOpcion.getInt("ag_id"));
				mapeo.setPropietario(rsOpcion.getString("ag_pro"));
		    	mapeo.setCaption(rsOpcion.getString("ag_eve"));
		    	mapeo.setAlmacen(rsOpcion.getString("ag_alm"));
		    	if (rsOpcion.getString("ag_pro").contentEquals("Turnos") || rsOpcion.getString("ag_pro").contentEquals("Vacaciones"))
    			{
		    		mapeo.setArea(rsOpcion.getString("ag_alm"));
		    		mapeo.setEjercicioContable(rsOpcion.getString("ag_eje"));
		    		mapeo.setTipo(rsOpcion.getString("ag_tip"));
    			}
		    	mapeo.setDescription(RutinasCadenas.conversion(rsOpcion.getString("ag_des")));
		    	mapeo.setAllDay(rsOpcion.getString("ag_dia")=="S");
		    	mapeo.setStyleName(rsOpcion.getString("ag_sty"));
		    	mapeo.setAviso(rsOpcion.getString("ag_avi")=="S");
		    	mapeo.setValorAviso(rsOpcion.getString("ag_for"));
		    	mapeo.setAviso(rsOpcion.getDate("ag_fec"));
		    	mapeo.setCompletada(rsOpcion.getString("ag_com").contentEquals("S"));
		    	mapeo.setStart(rsOpcion.getTimestamp("ag_ini"));
		    	mapeo.setEnd(rsOpcion.getTimestamp("ag_fin"));
				vector.add(mapeo);				
			}
			
			if (r_propietario=="Almacen")
	     	{
				cadenaSQL = new StringBuffer();
				
	    		cadenaSQL.append(" SELECT unique b.rowid ag_id, ");
	    		cadenaSQL.append(" 'Almacen' ag_pro, ");
	    		cadenaSQL.append(" a.codigo ag_eve, ");
	    		cadenaSQL.append(" a.plazo_entrega ag_ini, ");
	    		cadenaSQL.append(" a.plazo_entrega ag_fin, ");
	    		cadenaSQL.append(" b.nombre_comercial ag_des, ");
	    		cadenaSQL.append(" '' ag_sty, ");
	    		cadenaSQL.append(" 'N' ag_dia, ");
	    		cadenaSQL.append(" 'N' ag_avi, ");
	    		cadenaSQL.append(" '' ag_for, ");
	    		cadenaSQL.append(" '' ag_fec, ");
	    		cadenaSQL.append(" a.cumplimentacion ag_com, ");
	         	if (r_almacen=="Capuchinos")
	         	{
	         		cadenaSQL.append(" 'Capuchinos' ag_alm,  " );
	         	}
	         	else if (r_almacen=="Tejar")
	         	{
	         		cadenaSQL.append(" 'Tejar' ag_alm,  " );
	         	}
	         	else 
	         	{
	         		cadenaSQL.append(" 'Productos Vendimia' ag_alm,  " );
	         	}
	    		cadenaSQL.append(" '' ag_tip, ");
	    		cadenaSQL.append(" '' ag_eje ");
	         	cadenaSQL.append(" FROM A_CC_l_0 a, a_cc_c_0 b");
	         	cadenaSQL.append(" WHERE a.clave_tabla = b.clave_tabla " );
	         	cadenaSQL.append(" and a.documento = b.documento " );
	         	cadenaSQL.append(" and a.serie = b.serie" );
	         	cadenaSQL.append(" and a.codigo = b.codigo " );

	         	if (r_almacen=="Capuchinos")
	         	{
	         		cadenaSQL.append(" and a.almacen = 1 " );
	         	}
	         	else if (r_almacen=="Tejar")
	         	{
	         		cadenaSQL.append(" and a.almacen = 4 " );
	         	}
	         	else if (r_almacen=="Reservas Stock")
	         	{
	         		cadenaSQL.append(" and a.almacen = 12 " );
	         	}
	         	else 
	         	{
	         		cadenaSQL.append(" and a.almacen = 5 " );
	         	}
	         	
	         	
				cadenaSQL.append(" AND (a.plazo_entrega >= '" + RutinasFechas.convertirDateToString(r_inicio) + "' ");
				cadenaSQL.append(" AND a.plazo_entrega <= '" + RutinasFechas.convertirDateToString(r_fin) + "')");
				cadenaSQL.append("  ORDER BY 4");
				rsOpcion=null;
				con= this.conManager.establecerConexionGestion();			
				cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				int minutos = this.min;				
				GregorianCalendar gcS = new GregorianCalendar();
				gcS.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				GregorianCalendar gcE = new GregorianCalendar();
				gcE.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				
				while(rsOpcion.next())
				{
					MapeoCalendario mapeo = new MapeoCalendario();
					/*
					 * recojo mapeo calendario
					 */
					if (!rsOpcion.getString("ag_ini").equals(plazoAnterior)) minutos = this.min;
					mapeo.setId(rsOpcion.getInt("ag_id"));
					mapeo.setPropietario(rsOpcion.getString("ag_pro"));
			    	mapeo.setCaption(rsOpcion.getString("ag_eve"));
			    	mapeo.setAlmacen(rsOpcion.getString("ag_alm"));
			    	mapeo.setDescription(RutinasCadenas.conversion(rsOpcion.getString("ag_des")));
			    	mapeo.setAllDay(false);
			    	mapeo.setStyleName("yellow");
			    	mapeo.setCompletada(false);
			    	
			    	gcS.setTime(rsOpcion.getDate("ag_ini"));
			    	gcE.setTime(rsOpcion.getDate("ag_fin"));
			    	
			    	if (rsOpcion.getString("ag_com")!=null)
			    	{
			    		mapeo.setStyleName(null);
			    		mapeo.setCompletada(true);
				    	gcS.setTime(rsOpcion.getDate("ag_com"));
				    	gcE.setTime(rsOpcion.getDate("ag_com"));
			    	}
			    	mapeo.setArea("Descarga");
			    	mapeo.setAviso(false);
			    	mapeo.setValorAviso(null);
			    	mapeo.setAviso(null);
			    	
			    	gcS.add(java.util.Calendar.MINUTE, 390 + minutos);
			    	gcE.add(java.util.Calendar.MINUTE, 450 + minutos);
			    	
			    	mapeo.setStart(gcS.getTime());
			    	mapeo.setEnd(gcE.getTime());
			    	
					vector.add(mapeo);				

					minutos = minutos + this.min;
//					if (rsOpcion.getString("ag_com")!=null)
//						plazoAnterior = rsOpcion.getString("ag_com");
//					else
						plazoAnterior = rsOpcion.getString("ag_ini");

				}

				cadenaSQL = new StringBuffer();
				
	    		cadenaSQL.append(" SELECT rowid ag_id, ");
	    		cadenaSQL.append(" 'Almacen' ag_pro, ");
	    		cadenaSQL.append(" codigo ag_eve, ");
	    		cadenaSQL.append(" plazo_entrega ag_ini, ");
	    		cadenaSQL.append(" plazo_entrega ag_fin, ");
	    		cadenaSQL.append(" nombre_comercial ag_des, ");
	    		cadenaSQL.append(" '' ag_sty, ");
	    		cadenaSQL.append(" 'N' ag_dia, ");
	    		cadenaSQL.append(" 'N' ag_avi, ");
	    		cadenaSQL.append(" '' ag_for, ");
	    		cadenaSQL.append(" '' ag_fec, ");
	    		cadenaSQL.append(" finalidad ag_fdd, ");
	    		cadenaSQL.append(" cumplimentacion ag_com, ");
	    		if (r_almacen=="Capuchinos")
	         	{
	         		cadenaSQL.append(" 'Capuchinos' ag_alm,  " );
	         	}
	         	else if (r_almacen=="Tejar")
	         	{
	         		cadenaSQL.append(" 'Tejar' ag_alm,  " );
	         	}
	         	else 
	         	{
	         		cadenaSQL.append(" 'Productos Vendimia' ag_alm,  " );
	         	}
	    		cadenaSQL.append(" '' ag_tip, ");
	    		cadenaSQL.append(" '' ag_eje ");
	         	cadenaSQL.append(" FROM A_vP_C_0 ");

	        	if (r_almacen=="Capuchinos")
	         	{
	         		cadenaSQL.append(" WHERE almacen = 1 " );
	         	}
	         	else if (r_almacen=="Tejar")
	         	{
	         		cadenaSQL.append(" WHERE almacen = 4 " );
	         	}
	         	else if (r_almacen=="Reservas Stock")
	         	{
	         		cadenaSQL.append(" WHERE almacen = 12 " );
	         	}
	         	else 
	         	{
	         		cadenaSQL.append(" WHERE almacen = 5 " );
	         	}
	         	
				cadenaSQL.append(" AND (plazo_entrega >= '" + RutinasFechas.convertirDateToString(r_inicio) + "' ");
				cadenaSQL.append(" AND plazo_entrega <= '" + RutinasFechas.convertirDateToString(r_fin) + "')");
				cadenaSQL.append("  ORDER BY 4");
				rsOpcion=null;
				con= this.conManager.establecerConexionGestion();			
				cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				minutos = this.min;
				plazoAnterior = "";
				while(rsOpcion.next())
				{
					MapeoCalendario mapeo = new MapeoCalendario();
					/*
					 * recojo mapeo calendario
					 */
					if (!rsOpcion.getString("ag_ini").equals(plazoAnterior)) minutos = this.min;
					mapeo.setId(rsOpcion.getInt("ag_id"));
					mapeo.setPropietario(rsOpcion.getString("ag_pro"));
			    	mapeo.setCaption(rsOpcion.getString("ag_eve"));
			    	mapeo.setAlmacen(rsOpcion.getString("ag_alm"));
			    	mapeo.setDescription(RutinasCadenas.conversion(rsOpcion.getString("ag_des")));
			    	mapeo.setAllDay(false);
			    	mapeo.setCompletada(false);
			    	mapeo.setArea("Carga");
			    	gcS.setTime(rsOpcion.getDate("ag_ini"));
			    	gcE.setTime(rsOpcion.getDate("ag_fin"));
			    	
			    	if (rsOpcion.getString("ag_fdd")!=null && (rsOpcion.getString("ag_fdd").toUpperCase().trim().equals("OK") || rsOpcion.getString("ag_fdd").trim().toUpperCase().equals("FAB")))
			    	{//
			    		mapeo.setStyleName("green");
			    	}
			    	else if (rsOpcion.getString("ag_fdd")!=null && rsOpcion.getString("ag_fdd").toUpperCase().trim().equals("PDTE"))
			    	{
			    		mapeo.setStyleName("red");
			    	}
			    	else
			    	{
			    		mapeo.setStyleName("red");
			    	}	
			    	if (rsOpcion.getString("ag_com")!=null)
			    	{
			    		mapeo.setStyleName(null);
			    		mapeo.setCompletada(true);
				    	gcS.setTime(rsOpcion.getDate("ag_com"));
				    	gcE.setTime(rsOpcion.getDate("ag_com"));
			    	}
			    	mapeo.setAviso(false);
			    	mapeo.setValorAviso(null);
			    	mapeo.setAviso(null);
			    	
			    	gcS.add(java.util.Calendar.MINUTE, 390 + minutos);
			    	gcE.add(java.util.Calendar.MINUTE, 450 + minutos);
			    	
			    	mapeo.setStart(gcS.getTime());
			    	mapeo.setEnd(gcE.getTime());
			    	
					vector.add(mapeo);
					minutos = minutos + this.min;
//					if (rsOpcion.getString("ag_com")!=null)
//						plazoAnterior = rsOpcion.getString("ag_com");
//					else
						plazoAnterior = rsOpcion.getString("ag_ini");
				}

	     	}

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoGridVacaciones> datosOpcionesGlobal(MapeoGridVacaciones r_mapeo, String r_valorDias)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGridVacaciones> vector = null;
		MapeoGridVacaciones mapeo = null; 
		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT idPropietario ag_pro, ");
		cadenaSQL.append(" evento ag_eve, ");
		cadenaSQL.append(" almacen ag_alm, ");
		cadenaSQL.append(" tipo ag_tip, ");
		cadenaSQL.append(" ejercicioContable ag_eje, ");
		cadenaSQL.append(" count(idEvento) ag_cuantos ");
     	cadenaSQL.append(" FROM agenda ");
     	cadenaSQL.append(" WHERE idPropietario = 'Vacaciones' ");
     	cadenaSQL.append(" and DAYOFWEEK(inicio) IN (2,3,4,5,6) ");
     	
     	if (r_mapeo.getCaption()!=null) cadenaSQL.append(" AND evento like '%" + eBorsao.get().accessControl.getNombre() + "%' ");
     	if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen()!="Todas") cadenaSQL.append(" AND almacen like '%" + r_mapeo.getAlmacen() + "%' ");
     	if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0) cadenaSQL.append(" AND ejercicioContable = " + r_mapeo.getEjercicio());

     	try
     	{
     		cadenaSQL.append(" group by 1,2,3,4,5 ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
			vector = new ArrayList<MapeoGridVacaciones>();

			while(rsOpcion.next())
			{
				mapeo = new MapeoGridVacaciones();
				/*
				 * recojo mapeo calendario
				 */
				mapeo.setPropietario(rsOpcion.getString("ag_pro"));
		    	mapeo.setCaption(rsOpcion.getString("ag_eve"));
		    	mapeo.setAlmacen(rsOpcion.getString("ag_alm"));		    	
		    	mapeo.setEjercicioContable(rsOpcion.getString("ag_eje"));
		    	mapeo.setTipo(rsOpcion.getString("ag_tip"));
		    	mapeo.setCuantos(rsOpcion.getInt("ag_cuantos"));
		    	if (rsOpcion.getString("ag_tip").contentEquals("Vacaciones")) mapeo.setQuedan(new Integer(r_valorDias) - rsOpcion.getInt("ag_cuantos"));
		    	else mapeo.setQuedan(0);
				vector.add(mapeo);				
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoGridVacaciones> datosDetalleVacaciones(MapeoCalendario r_mapeo, String r_diasTotales)
	{
		boolean isVacaciones=true;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGridVacaciones> vector = null;
		MapeoGridVacaciones mapeo = null; 
		Date dia = null;
		Integer cuantos= null;
		Integer diasTotales = new Integer(r_diasTotales);
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT idEvento ag_id, ");
		cadenaSQL.append(" idPropietario ag_pro, ");
		cadenaSQL.append(" evento ag_eve, ");
		cadenaSQL.append(" inicio ag_ini, ");
		cadenaSQL.append(" fin ag_fin, ");
		cadenaSQL.append(" almacen ag_alm, ");
		cadenaSQL.append(" tipo ag_tip, ");
		cadenaSQL.append(" ejercicioContable ag_eje ");
		cadenaSQL.append(" FROM agenda ");
		
		cadenaSQL.append(" WHERE idPropietario = 'Vacaciones' ");
		cadenaSQL.append(" and DAYOFWEEK(inicio) IN (2,3,4,5,6) ");
		if (r_mapeo.getEjercicioContable()!=null) cadenaSQL.append(" AND ejercicioContable = " + r_mapeo.getEjercicioContable() + " ");
		if (r_mapeo.getDescription()!=null) cadenaSQL.append(" AND evento like '%" + r_mapeo.getDescription() + "%' ");
		if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen()!="Todas") cadenaSQL.append(" AND almacen like '%" + r_mapeo.getAlmacen() + "%' ");
		cadenaSQL.append(" order by inicio ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoGridVacaciones>();
			cuantos = 0;
			
			while(rsOpcion.next())
			{
				if ((dia==null) || RutinasFechas.restarFechas(dia, rsOpcion.getDate("ag_ini"))>1)
				{
					if (mapeo!=null)
					{
						if (isVacaciones) mapeo.setQuedan(diasTotales-cuantos); else mapeo.setQuedan(0);
						mapeo.setCuantos(cuantos);
						vector.add(mapeo);
						if (isVacaciones) diasTotales=diasTotales-cuantos;
						cuantos=0;
					}
					
					isVacaciones = rsOpcion.getString("ag_tip").contentEquals("Vacaciones");
					mapeo = new MapeoGridVacaciones();
					/*
					 * recojo mapeo calendario
					 */
					mapeo.setPropietario(rsOpcion.getString("ag_pro"));
					mapeo.setCaption(rsOpcion.getString("ag_eve"));
					mapeo.setAlmacen(rsOpcion.getString("ag_alm"));
					if (rsOpcion.getString("ag_pro").contentEquals("Turnos") || rsOpcion.getString("ag_pro").contentEquals("Vacaciones"))
					{
						mapeo.setAlmacen(rsOpcion.getString("ag_alm"));
					}
					mapeo.setTipo(rsOpcion.getString("ag_tip"));
					mapeo.setEjercicioContable(rsOpcion.getString("ag_eje"));
					mapeo.setStart(rsOpcion.getTimestamp("ag_ini"));
					mapeo.setEnd(rsOpcion.getTimestamp("ag_fin"));
//					if (rsOpcion.getString("ag_tip").contentEquals("Vacaciones"))
					{
						cuantos++;
					}
				}
				else
				{
					mapeo.setEnd(rsOpcion.getTimestamp("ag_fin"));
//					if (rsOpcion.getString("ag_tip").contentEquals("Vacaciones"))
					{
						cuantos++;
					}
				}
				dia = rsOpcion.getDate("ag_ini");
			}
			if (mapeo!=null)
			{
				if (isVacaciones) mapeo.setQuedan(diasTotales-cuantos); else mapeo.setQuedan(0);
				mapeo.setCuantos(cuantos);
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoCalendario r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Integer idCodigo =null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO agenda ( ");
			cadenaSQL.append(" agenda.idEvento, ");
	        cadenaSQL.append(" agenda.idPropietario, ");
	        cadenaSQL.append(" agenda.evento, ");
	        cadenaSQL.append(" agenda.inicio, ");
	        cadenaSQL.append(" agenda.fin, ");
	        cadenaSQL.append(" agenda.descripcion, ");
	        cadenaSQL.append(" agenda.estilo, ");
	        cadenaSQL.append(" agenda.diaria, ");
	        cadenaSQL.append(" agenda.aviso, ");
	        cadenaSQL.append(" agenda.formulaAviso, ");
	        cadenaSQL.append(" agenda.fechaAviso, ");
	        cadenaSQL.append(" agenda.completada, ");
	        cadenaSQL.append(" agenda.almacen, ");
	        cadenaSQL.append(" agenda.tipo, ");
	        cadenaSQL.append(" agenda.ejercicioContable) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    idCodigo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idCodigo);
		    
		    if (r_mapeo.getPropietario()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getPropietario());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCaption()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCaption());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getStart()!=null)
		    {
		    	preparedStatement.setString(4, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getStart())));
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getEnd()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getEnd())));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDescription()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDescription());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getStyleName()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getStyleName());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.isAllDay())
		    {
		    	preparedStatement.setString(8, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(8, "N");
		    }
		    if (r_mapeo.isAviso())
		    {
		    	preparedStatement.setString(9, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(9, "N");
		    }
		    if (r_mapeo.getValorAviso()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getValorAviso());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getAviso()!=null)
		    {
		    	preparedStatement.setString(11, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getAviso())));
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.isCompletada())
		    {
		    	preparedStatement.setString(12, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(12, "N");
		    }
		    if (r_mapeo.getAlmacen()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getAlmacen());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getEjercicioContable()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getEjercicioContable());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		
		return idCodigo.toString();
	}

	public Integer guardarNuevoImprimir(Integer r_id, MapeoCalendario r_mapeo, String r_tabla)
	{
		PreparedStatement preparedStatement = null;
		Integer idCodigo =null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO " + r_tabla + " ( ");
			cadenaSQL.append(" idImpresion, ");
	        cadenaSQL.append(" evento, ");
	        cadenaSQL.append(" inicio, ");
	        cadenaSQL.append(" turno, ");
	        cadenaSQL.append(" semana, ");
	        cadenaSQL.append(" mes, ");
	        cadenaSQL.append(" area) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?) ");

		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_id ==null) idCodigo = this.obtenerSiguienteImpresion(r_tabla); else idCodigo=r_id;
		    
		    preparedStatement.setInt(1, idCodigo);
		    
		    if (r_mapeo.getCaption()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCaption());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getStart()!=null)
		    {
		    	preparedStatement.setString(3, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getStart())));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTurno());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getMes()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getMes());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return null;
	    }        
		
		return idCodigo;
	}

	public Integer guardarNuevoImprimirVacaciones(Integer r_id, MapeoCalendario r_mapeo, String r_tabla)
	{
		PreparedStatement preparedStatement = null;
		Statement cs = null;
		Connection con = null;
		ResultSet rsOpcion = null;
		
		Integer idCodigo =null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" SELECT evento ag_eve ");
	     	cadenaSQL.append(" FROM " + r_tabla );
	     	cadenaSQL.append(" WHERE idImpresion = " + r_id);
	     	cadenaSQL.append(" and evento = '" + r_mapeo.getCaption() + "'");
	     	
	     	
	     	con= this.conManager.establecerConexion();			
	     	cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
	     	rsOpcion= cs.executeQuery(cadenaSQL.toString());
	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

	     	if (rsOpcion.next())
	     	{
	     		cadenaSQL = new StringBuffer();
	     		
	     		cadenaSQL.append(" UPDATE " + r_tabla + " set ");
	     		cadenaSQL.append(" dia" + new Integer(RutinasFechas.diaFecha(r_mapeo.getStart())) + " = 'X'");
	     		cadenaSQL.append(" WHERE idImpresion = " + r_id);
	     		cadenaSQL.append(" and evento = '" + r_mapeo.getCaption() + "' ");
	     		
	     		Notificaciones.getInstance().mensajeSeguimiento(cadenaSQL.toString());
	     		con= this.conManager.establecerConexion();	
	     		preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
	     		
	     		preparedStatement.executeUpdate();
	     		
	     		idCodigo=r_id;
	     	}
	     	else
	     	{
	     		
	     		cadenaSQL = new StringBuffer();
	     		
	     		cadenaSQL.append(" INSERT INTO " + r_tabla + " ( ");
	     		cadenaSQL.append(" idImpresion, ");
	     		cadenaSQL.append(" ejercicio, ");
	     		cadenaSQL.append(" evento, ");
	     		cadenaSQL.append(" inicio, ");	        
	     		cadenaSQL.append(" semana, ");
	     		cadenaSQL.append(" mes, ");
	     		cadenaSQL.append(" area, ");
	     		cadenaSQL.append(" dia" + new Integer(RutinasFechas.diaFecha(r_mapeo.getStart())));
	     		
	     		cadenaSQL.append(" ) VALUES (");
	     		cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
	     		
	     		Notificaciones.getInstance().mensajeSeguimiento(cadenaSQL.toString());
	     		
	     		if (con==null) con= this.conManager.establecerConexion();	
	     		preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
	     		
	     		if (r_id ==null) idCodigo = this.obtenerSiguienteImpresion(r_tabla); else idCodigo=r_id;
	     		
	     		preparedStatement.setInt(1, idCodigo);
	     		
	     		if (r_mapeo.getStart()!=null)
	     		{
	     			preparedStatement.setString(2, RutinasFechas.añoFecha(r_mapeo.getStart()));
	     		}
	     		else
	     		{
	     			preparedStatement.setString(2, null);
	     		}
	     		if (r_mapeo.getCaption()!=null)
	     		{
	     			preparedStatement.setString(3, r_mapeo.getCaption());
	     		}
	     		else
	     		{
	     			preparedStatement.setString(3, null);
	     		}
	     		if (r_mapeo.getStart()!=null)
	     		{
	     			preparedStatement.setString(4, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getStart())));
	     		}
	     		else
	     		{
	     			preparedStatement.setString(4, null);
	     		}
	     		if (r_mapeo.getSemana()!=null)
	     		{
	     			preparedStatement.setInt(5, r_mapeo.getSemana());
	     		}
	     		else
	     		{
	     			preparedStatement.setInt(5, 0);
	     		}
	     		if (r_mapeo.getMes()!=null)
	     		{
	     			preparedStatement.setInt(6, r_mapeo.getMes());
	     		}
	     		else
	     		{
	     			preparedStatement.setInt(6, 0);
	     		}		    
	     		if (r_mapeo.getArea()!=null)
	     		{
	     			preparedStatement.setString(7, r_mapeo.getArea());
	     		}
	     		else
	     		{
	     			preparedStatement.setString(7, null);
	     		}
	     		preparedStatement.setString(8, "X");
	     		
	     		preparedStatement.executeUpdate();
	     	}
	     	
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return null;
	    }        
		
		return idCodigo;
	}

	public String guardarCambios(MapeoCalendario r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE agenda set ");
		    cadenaSQL.append(" agenda.idPropietario =?,");
		    cadenaSQL.append(" agenda.evento =?,");
		    cadenaSQL.append(" agenda.inicio =?,");
		    cadenaSQL.append(" agenda.fin =?,");
		    cadenaSQL.append(" agenda.descripcion =?,");
			cadenaSQL.append(" agenda.estilo =?,");
			cadenaSQL.append(" agenda.diaria =?,");
			cadenaSQL.append(" agenda.aviso =?,");
			cadenaSQL.append(" agenda.formulaAviso = ?, ");
			cadenaSQL.append(" agenda.fechaAviso = ?, ");
			cadenaSQL.append(" agenda.completada = ?, ");
			cadenaSQL.append(" agenda.almacen = ?, ");
			cadenaSQL.append(" agenda.tipo = ?, ");
			cadenaSQL.append(" agenda.ejercicioContable = ? ");
			cadenaSQL.append(" where agenda.idEvento = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getPropietario()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getPropietario());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCaption()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCaption());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getStart()!=null)
		    {
		    	preparedStatement.setString(3, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getStart())));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getEnd()!=null)
		    {
		    	preparedStatement.setString(4, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getEnd())));
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDescription()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDescription());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getStyleName()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getStyleName());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }

		    if (r_mapeo.isAllDay())
		    {
		    	preparedStatement.setString(7, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(7, "N");
		    }
		    if (r_mapeo.isAviso())
		    {
		    	preparedStatement.setString(8, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(8, "N");
		    }
		    if (r_mapeo.getValorAviso()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getValorAviso());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getAviso()!=null)
		    {
		    	preparedStatement.setString(10, RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(r_mapeo.getAviso())));
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.isCompletada())
		    {
		    	preparedStatement.setString(11, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(11, "N");
		    }
		    if (r_mapeo.getAlmacen()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getAlmacen());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getEjercicioContable()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getEjercicioContable());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    preparedStatement.setInt(15, r_mapeo.getId());
		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	public String eliminar(Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM agenda ");            
			cadenaSQL.append(" WHERE agenda.idEvento = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Error al eliminar";
		}
		return null;
	}

	public String completar(Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" update agenda ");
			cadenaSQL.append(" set completada = 'S' ");
			cadenaSQL.append(" WHERE agenda.idEvento = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Error al completar";
		}
		return null;
	}

	public String eliminarImpresion(String r_prop, String r_tabla)
	{
		try
		{
			RutinasBaseDatos.eliminarTablaTemporal(r_prop, r_tabla);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Error al eliminar";
		}
		return null;
	}

	public String crearTabla(String r_prop, String r_tabla)
	{
		try
		{
			RutinasBaseDatos.crearTablaTemporal(r_prop, r_tabla);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Error al eliminar";
		}
		return null;
	}

	public String imprimirAgenda(String r_id,String r_area)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_id);

		libImpresion.setArchivoDefinitivo("/Agenda" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		if (r_area.equals("Vacaciones"))	libImpresion.setArchivoPlantilla("agendaVacaciones.jrxml");
		if (r_area.equals("Turnos"))	libImpresion.setArchivoPlantilla("agendaTurnos.jrxml");
		if (r_area.equals("Personal"))	libImpresion.setArchivoPlantilla("agendaPersonal.jrxml");
		if (r_area.equals("Almacen"))	libImpresion.setArchivoPlantilla("agendaAlmacen.jrxml");
		libImpresion.setBackGround("/fondoA4LogoBlancoH.jpg");

		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("calendario");

		resultadoGeneracion = libImpresion.generacionFinalTxt();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}


	private Integer obtenerSiguiente()
	{
		Integer valor = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(idEvento) ag_id ");
     	cadenaSQL.append(" FROM agenda ");
     	
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				valor = rsOpcion.getInt("ag_id") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		
		return valor;
		
	}
	
	private Integer obtenerSiguienteImpresion(String r_tabla)
	{
		Integer valor = null;
		try
		{
			valor = RutinasBaseDatos.comprobarMaximoId("idImpresion", r_tabla);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		return valor;
		
	}

	public ArrayList<MapeoAvisos> comprobarAvisosCalendario(String r_usuario)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoAvisos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT evento avi_avi, ");
		cadenaSQL.append(" descripcion avi_des ");
     	cadenaSQL.append(" FROM agenda ");
     	

		try
		{
			
			GregorianCalendar avisoCalculado = new GregorianCalendar();
			avisoCalculado.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
			avisoCalculado.setTime(new Date());
			avisoCalculado.add(java.util.Calendar.MINUTE, -5);
			
			

			if (r_usuario!=null)
			{
				cadenaSQL.append(" WHERE idPropietario = '" + r_usuario + "' ");
				cadenaSQL.append(" and fechaAviso <= '" + RutinasFechas.convertirAFechaHoraMysql(RutinasFechas.convertirDateTimeToString(avisoCalculado.getTime())) + "' ");
				cadenaSQL.append(" and completada = 'N'");
			}
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoAvisos>();
			
			while(rsOpcion.next())
			{
				MapeoAvisos mapeo = new MapeoAvisos();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setDescripcion(rsOpcion.getString("avi_avi"));
				mapeo.setDestino(rsOpcion.getString("avi_des"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos(null);
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos="99";
		}
		if (eBorsao.get().accessControl.getNombre().contains("CARMEN")) permisos = "99";
		return permisos;
	}

}