package borsao.e_borsao.Modulos.GENERALES.Clientes.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;

public class consultaClientesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaClientesServer instance;
	
	public consultaClientesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaClientesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaClientesServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerNombreCliente(Integer r_cliente)
	{
		String rdo = null;
		ResultSet rsClientes = null;
		
		String sql = null;
		try
		{
			sql="select cliente, nombre_comercial from e_client where cliente =" + r_cliente  ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsClientes= cs.executeQuery(sql);
			while(rsClientes.next())
			{
				rdo=rsClientes.getString("nombre_comercial");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return rdo;
	}	
	
	public boolean comprobarAccesoCliente(Integer r_cliente)
	{
		String rdo = null;
		ResultSet rsClientes = null;
		
		String sql = null;
		try
		{
			sql="select cliente, nombre_comercial from e_client where cliente =" + r_cliente  ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsClientes= cs.executeQuery(sql);
			while(rsClientes.next())
			{
				return true;								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}	
	public boolean comprobarCliente(String r_cliente)
	{
		String rdo = null;
		ResultSet rsClientes = null;
		
		String sql = null;
		try
		{
			sql="select cliente, nombre_comercial from e_client where cliente =" + new Integer(r_cliente.trim()) ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsClientes= cs.executeQuery(sql);
			while(rsClientes.next())
			{
				return true;								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}	
	
	public boolean chequearClientesEMCS()
	{
		boolean rdo = false;
		ResultSet rsClientes = null;
		String cli = null;
		String sql = null;
		try
		{
			sql="select cliente, nombre_comercial from e_client where estadistico_b3 is null or estadistico_b3 = '' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsClientes= cs.executeQuery(sql);
			
			while(rsClientes.next())
			{
				if (rsClientes.getString("cliente")!=null) rdo=true; 
				Notificaciones.getInstance().mensajeSeguimiento("Cliente erroneo " + rsClientes.getString("cliente"));
				if (cli==null) cli = "";
				cli = cli + "-" + rsClientes.getString("cliente");
			}
			if (rdo) Notificaciones.getInstance().mensajeError("Clientes Erroneos: " + cli); 
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return rdo;
	}
	
	public String recuperarDatosAlmacen(Integer r_cliente, String r_campo)
	{
		String rdo = null;
		
		ResultSet rsClientes = null;
		String cli = null;
		String sql = null;
		String tipoDato = null;
		try
		{
			if (r_campo.contentEquals("Nombre")) tipoDato="Nl"; 
			if (r_campo.contentEquals("Numero")) tipoDato="NL"; 
			sql="select texto from e_cli_nt where cliente = " + r_cliente.toString() + " and tipo_dato = BINARY '" + tipoDato + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsClientes= cs.executeQuery(sql);
			
			while(rsClientes.next())
			{
				rdo =  rsClientes.getString("texto");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			rdo = null;
		}

		return rdo;
	}
}
