package borsao.e_borsao.Modulos.GENERALES.Ejercicios.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEjercicios extends MapeoGlobal
{
	private String ejercicio;

	public MapeoEjercicios()
	{
		this.setEjercicio("");
	}

	public String getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}

		
}