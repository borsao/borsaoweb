package borsao.e_borsao.Modulos.GENERALES.Ejercicios.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.modelo.MapeoAlmacenes;

public class consultaEjerciciosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaEjerciciosServer instance;
	
	public consultaEjerciciosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEjerciciosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEjerciciosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<String> obtenerEjercicios()
	{
		Integer ejer = null;
		ArrayList<String> rdo = null;
		ResultSet rsEjercicios = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			rdo = new ArrayList<String>();
			
			sql="select nom_ejercicio from a_ejerci " ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsEjercicios= cs.executeQuery(sql);
			while(rsEjercicios.next())
			{
				ejer = new Integer(rsEjercicios.getString("nom_ejercicio"));
				rdo.add(rsEjercicios.getString("nom_ejercicio"));								
			}
			for (int i = 1; i < 6; i++)
			{
				rdo.add(String.valueOf(ejer - i));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsEjercicios!=null)
				{
					rsEjercicios.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}
}
