package borsao.e_borsao.Modulos.GENERALES.SubFamilias.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoSubFamilias extends MapeoGlobal
{
	private String familia;
	private String subFamilia;
	private String descripcion;

	public MapeoSubFamilias()
	{
		this.setFamilia("");
		this.setSubFamilia("");
		this.setDescripcion("");
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSubFamilia() {
		return subFamilia;
	}

	public void setSubFamilia(String subFamilia) {
		this.subFamilia = subFamilia;
	}

		
}