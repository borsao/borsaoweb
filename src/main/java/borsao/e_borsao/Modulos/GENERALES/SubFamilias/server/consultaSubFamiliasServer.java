package borsao.e_borsao.Modulos.GENERALES.SubFamilias.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Familias.modelo.MapeoFamilias;
import borsao.e_borsao.Modulos.GENERALES.SubFamilias.modelo.MapeoSubFamilias;

public class consultaSubFamiliasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaSubFamiliasServer instance;
	
	public consultaSubFamiliasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSubFamiliasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSubFamiliasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoSubFamilias> datosSubFamiliasGlobal(String r_familia)
	{
		ArrayList<MapeoSubFamilias> rdo = null;
		ResultSet rsFamilias = null;
		String usu = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			rdo = new ArrayList<MapeoSubFamilias>();
			
			if (r_familia.length()!=0)
				sql="select distinct familia, sub_familia, descripcion from e_subfam where familia in ("+r_familia+") " ;
			else
				sql="select distinct familia, sub_familia, descripcion from e_subfam ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsFamilias= cs.executeQuery(sql);
			while(rsFamilias.next())
			{
				MapeoSubFamilias mapeo = new MapeoSubFamilias();
				mapeo.setSubFamilia(rsFamilias.getString("sub_familia"));
				mapeo.setFamilia(rsFamilias.getString("familia"));
				mapeo.setDescripcion(rsFamilias.getString("descripcion"));
				
				rdo.add(mapeo);								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsFamilias!=null)
				{
					rsFamilias.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}
}
