package borsao.e_borsao.Modulos.GENERALES.Contadores.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Contadores.modelo.MapeoContadores;

public class consultaContadoresServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaContadoresServer instance;
	
	public consultaContadoresServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaContadoresServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaContadoresServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoContadores> datosOpcionesGlobal(MapeoContadores r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoContadores> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT glb_contadores.area con_are, ");
        cadenaSQL.append(" glb_contadores.contador con_con ");
     	cadenaSQL.append(" FROM glb_contadores ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getContador()!=null && r_mapeo.getContador().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contador= " + r_mapeo.getContador() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by area asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoContadores>();
			
			while(rsOpcion.next())
			{
				MapeoContadores mapeo = new MapeoContadores();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setArea(rsOpcion.getString("con_are"));
				mapeo.setContador(rsOpcion.getInt("con_con"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public Integer recuperarContador(String r_area)
	{
		Integer contador_obtenido = null;
		ResultSet rsContador = null;
		
		String sql = null;
		try
		{
			sql="select contador from glb_contadores where area = '" + r_area + "'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsContador = cs.executeQuery(sql);
			while(rsContador.next())
			{
				contador_obtenido = rsContador.getInt("contador") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsContador!=null)
				{
					rsContador.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return contador_obtenido;
	}
	
	

	public String guardarNuevo(MapeoContadores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO glb_contadores ( ");
    		cadenaSQL.append(" glb_contadores.area, ");
	        cadenaSQL.append(" glb_contadores.contador ) VALUES (");
		    cadenaSQL.append(" ?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getContador()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getContador());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	
	public void actualizarContador(Integer r_contador, String r_area)
	{
			
		String sql = null;
		try
		{
			sql="update glb_contadores set contador = " + r_contador + " where area = '" + r_area + "'" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public void eliminar(MapeoContadores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_contadores ");            
			cadenaSQL.append(" WHERE glb_contadores.area = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArea());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}