package borsao.e_borsao.Modulos.GENERALES.Contadores.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.Modulos.GENERALES.Contadores.view.ContadoresView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private MapeoContadores mapeoOrig = null;
	private MapeoContadores mapeo = null;
	private ContadoresView app = null;
	
    public OpcionesGrid(ContadoresView r_app, ArrayList<MapeoContadores> r_vector) 
    {
        this.vector=r_vector;
        this.app = r_app;
		this.asignarTitulo("Contadores");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		actualizar = false;
        this.crearGrid(MapeoContadores.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoContadores>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.mapeoOrig = new MapeoContadores();
    	if (mapeo!=null)
    	{
	    	this.mapeoOrig.setArea(mapeo.getArea());
	    	this.mapeoOrig.setContador(mapeo.getContador());
	    	this.mapeoOrig.setIdCodigo(mapeo.getIdCodigo());
    	}
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	super.doEditItem();
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("area","contador");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidth(new Double(250));
    	this.getColumn("contador").setHeaderCaption("Valor");
    	this.getColumn("contador").setWidth(new Double(100));
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    }

    public void asignarEstilos()
    {
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("contador".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        	mapeo = (MapeoContadores) getEditedItemId();
	        	if (mapeo.getIdCodigo()!=null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        	}
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	MapeoContadores mapeo = (MapeoContadores) getEditedItemId();
	        	consultaContadoresServer cs = new consultaContadoresServer(CurrentUser.get());
	        
	        	if (mapeo.isEliminar())
	        	{
	        		cs.eliminar(mapeo);
	        		remove(mapeo);
	        	}
	        	else
	        	{
	        		if (actualizar)
		        	{
		        		cs.actualizarContador(mapeo.getContador(),mapeo.getArea());
		        	}
		        	else
		        	{
		        		cs.guardarNuevo(mapeo);
		        		refreshAllRows();
		        		app.botonesGenerales(true);
		        	}
	        	}
        	}
	        
		});
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() 
	{	
	}
	
}
