package borsao.e_borsao.Modulos.GENERALES.Contadores.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoContadores extends MapeoGlobal
{
	private String area;
	private Integer contador;
	private boolean eliminar=false;
	

	public MapeoContadores()
	{
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public Integer getContador() {
		return contador;
	}



	public void setContador(Integer contador) {
		this.contador = contador;
	}



	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


}