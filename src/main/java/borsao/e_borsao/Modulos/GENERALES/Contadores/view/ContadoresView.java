package borsao.e_borsao.Modulos.GENERALES.Contadores.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.GENERALES.Contadores.modelo.MapeoContadores;
import borsao.e_borsao.Modulos.GENERALES.Contadores.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ContadoresView extends GridViewRefresh {

	public static final String VIEW_NAME = "Contadores";
	private final String titulo = "CONTADORES";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoContadores mapeoContadores=null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoContadores> r_vector=null;
    	consultaContadoresServer ccs = new consultaContadoresServer(CurrentUser.get());
    	r_vector=ccs.datosOpcionesGlobal(null);
    	
    	grid = new OpcionesGrid(this, r_vector);
		setHayGrid(true);
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ContadoresView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
    }

    public void newForm()
    {
    	if (grid==null)
    	{
    		grid = new OpcionesGrid(this, null);
			this.mapeoContadores=new MapeoContadores();
			mapeoContadores.setArea("");
			mapeoContadores.setEliminar(false);
			mapeoContadores.setContador(0);
			grid.getContainerDataSource().addItem(mapeoContadores);    	
			grid.editItem(mapeoContadores);
    	}
    	else if (grid.getEditedItemId()==null)
    	{
    		this.mapeoContadores=new MapeoContadores();
			mapeoContadores.setArea("");
			mapeoContadores.setEliminar(false);
			mapeoContadores.setContador(0);
			grid.getContainerDataSource().addItem(mapeoContadores);    	
			grid.editItem(mapeoContadores);
    	}
    	
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.mapeoContadores = (MapeoContadores) r_fila;
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
