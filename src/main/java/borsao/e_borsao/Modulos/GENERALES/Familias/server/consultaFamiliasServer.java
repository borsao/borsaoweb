package borsao.e_borsao.Modulos.GENERALES.Familias.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Familias.modelo.MapeoFamilias;

public class consultaFamiliasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaFamiliasServer instance;
	
	public consultaFamiliasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaFamiliasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaFamiliasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoFamilias> datosFamiliasGlobal()
	{
		ArrayList<MapeoFamilias> rdo = null;
		ResultSet rsFamilias = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			rdo = new ArrayList<MapeoFamilias>();
			
			sql="select distinct familia, descripcion from e_famili " ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsFamilias= cs.executeQuery(sql);
			while(rsFamilias.next())
			{
				MapeoFamilias mapeo = new MapeoFamilias();
				mapeo.setFamilia(rsFamilias.getString("familia"));
				mapeo.setDescripcion(rsFamilias.getString("descripcion"));
				
				rdo.add(mapeo);								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsFamilias!=null)
				{
					rsFamilias.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}
	
	public HashMap<String, String> hashFamilias()
	{
		HashMap<String, String> rdo = null;
		ResultSet rsFamilias = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select distinct familia, descripcion from e_famili " ;
			
			rdo = new HashMap<String, String>();
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsFamilias= cs.executeQuery(sql);
			while(rsFamilias.next())
			{
				rdo.put(rsFamilias.getString("familia"),rsFamilias.getString("descripcion"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsFamilias!=null)
				{
					rsFamilias.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}

	public HashMap<String, String> hashSubFamilias(String r_familia)
	{
		HashMap<String, String> rdo = null;
		ResultSet rsFamilias = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select distinct sub_familia, descripcion from e_subfam where familia = '" + r_familia + "' " ;
			
			rdo = new HashMap<String, String>();
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsFamilias= cs.executeQuery(sql);
			while(rsFamilias.next())
			{
				rdo.put(rsFamilias.getString("sub_familia"),RutinasCadenas.conversion(rsFamilias.getString("descripcion")));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsFamilias!=null)
				{
					rsFamilias.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}
}
