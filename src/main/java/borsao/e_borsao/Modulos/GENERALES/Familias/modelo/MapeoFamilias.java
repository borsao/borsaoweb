package borsao.e_borsao.Modulos.GENERALES.Familias.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoFamilias extends MapeoGlobal
{
	private String familia;
	private String descripcion;

	public MapeoFamilias()
	{
		this.setFamilia("");
		this.setDescripcion("");
	}

	public String getFamilia() {
		return familia;
	}

	public void setFamilia(String familia) {
		this.familia = familia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

		
}