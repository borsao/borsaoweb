package borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo.MapeoContadoresEjercicio;

public class consultaContadoresEjercicioServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaContadoresEjercicioServer instance;
	
	public consultaContadoresEjercicioServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaContadoresEjercicioServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaContadoresEjercicioServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoContadoresEjercicio> datosOpcionesGlobal(MapeoContadoresEjercicio r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoContadoresEjercicio> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT glb_contadores_ej.ejercicio con_eje, ");
		cadenaSQL.append(" glb_contadores_ej.origen con_ori, ");
		cadenaSQL.append(" glb_contadores_ej.area con_are, ");
        cadenaSQL.append(" glb_contadores_ej.contador con_con ");
     	cadenaSQL.append(" FROM glb_contadores_ej ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" origen = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getContador()!=null && r_mapeo.getContador().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contador= " + r_mapeo.getContador() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, area asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoContadoresEjercicio>();
			
			while(rsOpcion.next())
			{
				MapeoContadoresEjercicio mapeo = new MapeoContadoresEjercicio();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("con_eje"));
				mapeo.setEjercicio(rsOpcion.getInt("con_eje"));
				mapeo.setArea(rsOpcion.getString("con_are"));
				mapeo.setOrigen(rsOpcion.getString("con_ori"));
				mapeo.setContador(rsOpcion.getDouble("con_con"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}
	
	
	public Long recuperarContador(Integer r_ejercicio, String r_area, String r_origen)
	{
		Long contador_obtenido = null;
		ResultSet rsContador = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select contador from glb_contadores_ej where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			if(r_origen!=null && r_origen.length()>0) sql = sql + " and origen = '" + r_origen + "' ";
				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsContador = cs.executeQuery(sql);
			while(rsContador.next())
			{
				contador_obtenido = rsContador.getLong("contador") + 1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsContador!=null)
				{
					rsContador.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return contador_obtenido;
	}
	

	public boolean comprobarContador(Long r_contador, Integer r_ejercicio, String r_area, String r_origen)
	{
		ResultSet rsContador = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select contador from glb_contadores_ej where contador = " + r_contador + " and ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			if(r_origen!=null && r_origen.length()>0) sql = sql + " and origen = '" + r_origen + "' ";
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsContador = cs.executeQuery(sql);
			while(rsContador.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsContador!=null)
				{
					rsContador.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}


	public String guardarNuevo(MapeoContadoresEjercicio r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con=null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO glb_contadores_ej ( ");
			cadenaSQL.append(" glb_contadores_ej.ejercicio, ");
    		cadenaSQL.append(" glb_contadores_ej.origen, ");
    		cadenaSQL.append(" glb_contadores_ej.area, ");
	        cadenaSQL.append(" glb_contadores_ej.contador ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getContador()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getContador());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	
	public void actualizarContador(Integer r_ejercicio, Double r_contador, String r_area, String r_origen)
	{
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			sql="update glb_contadores_ej set contador = " + r_contador + " where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			if(r_origen!=null && r_origen.length()>0) sql = sql + " and origen = '" + r_origen + "' ";
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public void actualizarContadorTransaccional(Connection r_con, Integer r_ejercicio, Double r_contador, String r_area, String r_origen)
	{
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			sql="update glb_contadores_ej set contador = " + r_contador + " where ejercicio = " + r_ejercicio + " and area = '" + r_area + "'" ;
			if(r_origen!=null && r_origen.length()>0) sql = sql + " and origen = '" + r_origen + "' ";
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			cs.execute(sql);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
	}

	public void eliminar(MapeoContadoresEjercicio r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con  = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_contadores_ej ");            
			cadenaSQL.append(" WHERE glb_contadores_ej.ejercicio = ?");
			cadenaSQL.append(" AND glb_contadores_ej.area = ?");
			if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0) cadenaSQL.append(" AND glb_contadores_ej.origen = ?");
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getEjercicio());
			preparedStatement.setString(2, r_mapeo.getArea());
			if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0) preparedStatement.setString(3, r_mapeo.getOrigen());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}
}