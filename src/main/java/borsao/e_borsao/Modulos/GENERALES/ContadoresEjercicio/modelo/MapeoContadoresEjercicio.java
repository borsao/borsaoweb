package borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoContadoresEjercicio extends MapeoGlobal
{
	private String area;
	private String origen;
	private Double contador;
	private Integer ejercicio;
	private boolean eliminar=false;
	

	public MapeoContadoresEjercicio()
	{
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public Double getContador() {
		return contador;
	}



	public void setContador(Double contador) {
		this.contador = contador;
	}



	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}



	public Integer getEjercicio() {
		return ejercicio;
	}



	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}



	public String getOrigen() {
		return origen;
	}



	public void setOrigen(String origen) {
		this.origen = origen;
	}


}