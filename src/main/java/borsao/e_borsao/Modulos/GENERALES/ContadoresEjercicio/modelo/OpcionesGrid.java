package borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.view.ContadoresEjercicioView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = true;
	public boolean actualizar = false;
	private boolean conTotales = false;
	private ContadoresEjercicioView app = null;
	
    public OpcionesGrid(ArrayList<MapeoContadoresEjercicio> r_vector, ContadoresEjercicioView r_app) 
    {
    	this.app=r_app;
        this.vector=r_vector;
		this.asignarTitulo("Contadores");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		actualizar = false;
        this.crearGrid(MapeoContadoresEjercicio.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoContadoresEjercicio>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	super.doEditItem();
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "origen", "area","contador");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "85");
    	this.widthFiltros.put("origen", "145");
    	this.widthFiltros.put("area", "145");
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(120));
    	this.getColumn("origen").setHeaderCaption("Origen");
    	this.getColumn("origen").setWidth(new Double(250));    	
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidth(new Double(250));
    	this.getColumn("contador").setHeaderCaption("Valor");
    	this.getColumn("contador").setWidth(new Double(250));
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("contador".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	MapeoContadoresEjercicio mapeo = (MapeoContadoresEjercicio) getEditedItemId();
	        	consultaContadoresEjercicioServer cs = new consultaContadoresEjercicioServer(CurrentUser.get());
	        
	        	if (mapeo.isEliminar())
	        	{
	        		cs.eliminar(mapeo);
	        		remove(mapeo);
	        	}
	        	else
	        	{
	        		if (mapeo.getIdCodigo()!=null)
		        	{
		        		cs.actualizarContador(mapeo.getEjercicio(), mapeo.getContador(),mapeo.getArea(), mapeo.getOrigen());
		        	}
		        	else
		        	{
		        		cs.guardarNuevo(mapeo);
		        		mapeo.setIdCodigo(mapeo.getEjercicio());
		        	}
	        	}
        	}
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("contador");
		this.camposNoFiltrar.add("eliminar");
	}

	@Override
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
//    	Indexed indexed = this.getContainerDataSource();
//        List<?> list = new ArrayList<Object>(indexed.getItemIds());
//        for(Object itemId : list)
//        {
//        	Item item = indexed.getItem(itemId);
//        	
//    		valor = (Integer) item.getItemProperty("cantidad").getValue();
//			totalU += valor.longValue();
//        }
//        
//        footer.getCell("tipo").setText("Totales ");
//		footer.getCell("cantidad").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
//		footer.getCell("cantidad").setStyleName("Rcell-pie");
		
	}
	
}
