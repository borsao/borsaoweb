package borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Item;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo.MapeoContadoresEjercicio;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ContadoresEjercicioView extends GridViewRefresh {

	public static final String VIEW_NAME = "Contadores por Ejercicio";
	private final String titulo = "CONTADORES por EJERCICIO";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoContadoresEjercicio mapeoContadores=null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoContadoresEjercicio> r_vector=null;
    	consultaContadoresEjercicioServer ccs = new consultaContadoresEjercicioServer(CurrentUser.get());
    	r_vector=ccs.datosOpcionesGlobal(null);
    	
    	grid = new OpcionesGrid(r_vector,this);
		setHayGrid(true);
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ContadoresEjercicioView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
    }

    public void newForm()
    {
		this.mapeoContadores=new MapeoContadoresEjercicio();
		this.mapeoContadores.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
		this.mapeoContadores.setArea("");
		this.mapeoContadores.setEliminar(false);
		this.mapeoContadores.setContador(new Double(0));

    	if (grid==null)
    	{
    		grid = new OpcionesGrid(null,this);
			grid.getContainerDataSource().addItem(this.mapeoContadores);    	
			grid.editItem(this.mapeoContadores);
    	}
    	else if (grid.getEditedItemId()==null)
    	{
    		grid.getContainerDataSource().addItem(this.mapeoContadores);
			grid.scrollTo(this.mapeoContadores);
    	}
    	this.botonesGenerales(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.mapeoContadores = (MapeoContadoresEjercicio) r_fila;
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
