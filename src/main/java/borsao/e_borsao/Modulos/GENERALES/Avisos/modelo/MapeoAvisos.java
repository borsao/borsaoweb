package borsao.e_borsao.Modulos.GENERALES.Avisos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAvisos extends MapeoGlobal
{
	private String descripcion;
	private String destino;
	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	private boolean eliminar=false;

	public MapeoAvisos()
	{
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


}