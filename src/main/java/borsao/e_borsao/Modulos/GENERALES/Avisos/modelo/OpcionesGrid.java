package borsao.e_borsao.Modulos.GENERALES.Avisos.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Avisos.server.consultaAvisosServer;
import borsao.e_borsao.Modulos.GENERALES.Avisos.view.AvisosView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	private AvisosView av = null;
	private boolean editable = true;
	private boolean conFiltro = false;
	
	
    public OpcionesGrid(ArrayList<MapeoAvisos> r_vector, AvisosView r_view) 
    {
    	this.av=r_view;
        this.vector=r_vector;
		this.asignarTitulo("Avisos Usuarios");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
        this.crearGrid(MapeoAvisos.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoAvisos>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());    	
    	super.doEditItem();
    	av.botonesGenerales(true);
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	     av.botonesGenerales(true);
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("descripcion","destino");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("descripcion").setHeaderCaption("Aviso");
    	this.getColumn("destino").setHeaderCaption("Destinatarios");
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("eliminar").setWidth(new Double(100));
    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "cell-normal";
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	MapeoAvisos mapeo = (MapeoAvisos) getEditedItemId();
	        	consultaAvisosServer cs = new consultaAvisosServer(CurrentUser.get());
	        
	        	if (mapeo.isEliminar())
	        	{
	        		cs.eliminar(mapeo);
	        		remove(mapeo);
	        	}
	        	else
	        	{
	        		cs.guardarNuevo(mapeo);
	        	}
	        }
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}
