package borsao.e_borsao.Modulos.GENERALES.Avisos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Avisos.modelo.MapeoAvisos;

public class consultaAvisosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaAvisosServer instance;
	
	public consultaAvisosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAvisosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAvisosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoAvisos> datosOpcionesGlobal(String r_usuario)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoAvisos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT aviso avi_avi, ");
		cadenaSQL.append(" destinatario avi_des ");
     	cadenaSQL.append(" FROM glb_avisos ");
     	
     	if (r_usuario!=null)
     	{
     		cadenaSQL.append(" WHERE destinatario = 'TODOS' ");
     		cadenaSQL.append(" or destinatario = '" + r_usuario + "' ");
     	}

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoAvisos>();
			
			while(rsOpcion.next())
			{
				MapeoAvisos mapeo = new MapeoAvisos();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setDescripcion(rsOpcion.getString("avi_avi"));
				mapeo.setDestino(rsOpcion.getString("avi_des"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoAvisos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO glb_avisos ( ");
			cadenaSQL.append(" glb_avisos.aviso, ");
	        cadenaSQL.append(" glb_avisos.destinatario) VALUES (");
		    cadenaSQL.append(" ?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDestino()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		
		return null;
	}
	
	public void eliminar(MapeoAvisos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM glb_avisos ");            
			cadenaSQL.append(" WHERE glb_avisos.aviso = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getDescripcion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}