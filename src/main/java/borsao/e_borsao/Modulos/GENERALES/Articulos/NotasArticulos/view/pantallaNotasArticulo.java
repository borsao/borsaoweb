package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaNotasArticulo extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridPadre= null;
	private VerticalLayout principal=null;
	private boolean hayGridPadre = false;
	private VerticalLayout frameCentral = null;	
	private String articuloConsultado = null;
	private ArrayList<MapeoNotasArticulos> vectorNotas= null;
	
	public pantallaNotasArticulo(String r_titulo, String r_articulo)
	{
		
		this.setCaption(r_titulo);
		this.articuloConsultado=r_articulo;
		this.vectorNotas= null;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);

	}

	public pantallaNotasArticulo(String r_titulo, ArrayList<MapeoNotasArticulos> r_vector)
	{
		this.articuloConsultado=null;
		this.vectorNotas=r_vector;
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
		
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

				HorizontalLayout frameCabecera = new HorizontalLayout();
				frameCabecera.setHeight("1%");
				
				this.frameCentral = new VerticalLayout();
				this.frameCentral.setHeight("650px");
				HorizontalLayout framePie = new HorizontalLayout();
		
					btnBotonCVentana = new Button("Cerrar");
					btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
					framePie.addComponent(btnBotonCVentana);		
					framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	ArrayList<MapeoNotasArticulos> r_vector=null;
    	
    	if (this.articuloConsultado!=null)
    	{
    		consultaNotasArticulosServer ces = new consultaNotasArticulosServer(CurrentUser.get());
    		MapeoNotasArticulos mapeo = new MapeoNotasArticulos();
    		mapeo.setArticulo(this.articuloConsultado.trim());
    	
    		r_vector=ces.datosOpcionesGlobal(mapeo);
    		this.presentarGrid(r_vector);
    	}
    	else
    	{
    		this.presentarGrid(this.vectorNotas);
    	}

	}

    private void presentarGrid(ArrayList<MapeoNotasArticulos> r_vector)
    {
    	
    	gridPadre = new OpcionesGrid(null, r_vector);
    	
    	if (((OpcionesGrid) gridPadre).vector==null)
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		setHayGridPadre(true);
    	}
    	
    	if (isHayGridPadre())
    	{
    		
			this.gridPadre.setSizeFull();
			this.frameCentral.addComponent(gridPadre);
			
			this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    	}
    }

	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

}