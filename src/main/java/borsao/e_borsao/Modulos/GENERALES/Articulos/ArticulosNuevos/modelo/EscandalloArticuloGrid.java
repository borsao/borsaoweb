package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view.ArticulosNuevosView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EscandalloArticuloGrid extends GridPropio {
	
	private ArticulosNuevosView app = null;
	public ArrayList<MapeoArticulosEscandallo>  vector = null;

	public EscandalloArticuloGrid(ArticulosNuevosView r_App, ArrayList<MapeoArticulosEscandallo>  r_vector) 
    {
    	this.app = r_App;
    	this.vector = r_vector;
		this.crearGrid(MapeoArticulosEscandallo.class);
		this.setRecords(vector);

		this.setSelectionMode(SelectionMode.SINGLE);
		this.setEditorEnabled(false);
		if (this.app.descripcionArticuloPadreSeleccionado!=null && this.app.descripcionArticuloPadreSeleccionado.length()>0)
			this.setCaption(this.app.descripcionArticuloPadreSeleccionado.toUpperCase());
		else if (this.app.articuloPadreSeleccionado!=null && this.app.articuloPadreSeleccionado.length()>0)
			this.setCaption(this.app.articuloPadreSeleccionado.toUpperCase());
		else if (this.app.articuloSeleccionado!=null && this.app.articuloSeleccionado.length()>0)
			this.setCaption(this.app.articuloSeleccionado.toUpperCase());
		else if (this.app.descripcionArticuloSeleccionado!=null && this.app.descripcionArticuloSeleccionado.length()>0)
			this.setCaption(this.app.descripcionArticuloSeleccionado.toUpperCase());
		else
			this.setCaption("ARTICULO NUEVO");
		this.setSizeFull();
		this.addStyleName("smallgrid");
		this.setFrozenColumnCount(2);
    }

	@Override
	public void establecerOrdenPresentacionColumnas() 
	{
		setColumnOrder("modificacion", "modificar", "estado", "porc", "tarea", "codigoArticulo", "descripcionArticulo", "notasProd", "notasPlan");
	}
	
	public void cargarListeners() 
	{
    	
		this.addSelectionListener(new SelectionListener() {

			@Override
			public void select(SelectionEvent event) {

				Item file = getContainerDataSource().getItem(getSelectedRow());
				
				if (file instanceof BeanItem<?>) 
                {
                     Object bean = ((BeanItem<?>)file).getBean();
                     if (bean instanceof MapeoArticulosEscandallo) 
                     {
                         MapeoArticulosEscandallo artRow = (MapeoArticulosEscandallo)bean;
                         
                         app.filaSeleccionadaEscandallo(artRow);
                     }
                }
			}
		});
	}
	
	public void asignarEstilos()
	{
		
		this.setCellDescriptionGenerator(new Grid.CellDescriptionGenerator() {

			String completado = "";
			@Override
			public String getDescription(CellReference cell) {
				String descriptionText = null;

				if ("estado".equals(cell.getPropertyId())) {
					if (cell.getValue() != null && cell.getValue().toString().contentEquals("6")) {
						completado = "999";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("5")) {
						completado = "100";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("4")) {
						completado = "80";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("3")) {
						completado = "60";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("2")) {
						completado = "40";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("1")) {
						completado = "20";
					} else {
						completado = "0";
					}

				}
				if ("porc".equals(cell.getPropertyId())) {
					if (completado.contentEquals("100")) {
						descriptionText = "Tarea Completada";
					} else if (completado.contentEquals("80")) {
						descriptionText = "Falta Pedido";
					} else if (completado.contentEquals("60")) {
						descriptionText = "Esperar OK Cliente ";
					} else if (completado.contentEquals("40")) {
						descriptionText = "Esperar BOCETO Cliente ";
					} else if (completado.contentEquals("20")) {
						descriptionText = "Esperar Ok Especificaciones";
					} else {
						descriptionText = "No Aplica";
					}
				}
				return descriptionText;
			}
		});

		this.setCellStyleGenerator(new Grid.CellStyleGenerator() {
			String completado = "";
			boolean modificacion = false;
			
			public String getStyle(Grid.CellReference cell) {
				if ("modificar".equals(cell.getPropertyId())) {
					if (cell.getValue().toString().contentEquals("true"))
						modificacion=true;
					else
						modificacion = false;
				}
				if ("estado".equals(cell.getPropertyId())) {
					if (cell.getValue() != null && cell.getValue().toString().contentEquals("6")) {
						completado = "999";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("5")) {
						completado = "100";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("4")) {
						completado = "80";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("3")) {
						completado = "60";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("2")) {
						completado = "40";
					} else if (cell.getValue() != null && cell.getValue().toString().contentEquals("1")) {
						completado = "20";
					} else {
						completado = "0";
					}
				}

				if ("porc".equals(cell.getPropertyId())) {
					if (completado.contentEquals("100")) {
						return "cell-dgreen";
					} else if (completado.contentEquals("80")) {
						return "cell-lime";
					} else if (completado.contentEquals("60")) {
						return "cell-yellow";
					} else if (completado.contentEquals("40")) {
						return "cell-orange";
					} else if (completado.contentEquals("20")) {
						return "cell-pink";
					} else {
						return "Ccell-normal";
					}
				} else if ("codigoArticulo".equals(cell.getPropertyId())||"descripcionArticulo".equals(cell.getPropertyId())||"tarea".equals(cell.getPropertyId())) {
					if (modificacion) {
						return "cell-modificada";
					}
					else
					{						
						if (completado.contentEquals("999")) {
							return "cell-disabled";
						}
						else
							return "cell-normal";
					}
				} else {
					if (completado.contentEquals("999")) {
						return "cell-disabled";
					}
					else
						return "cell-normal";
				}
			}
		});
	}

	@Override
	public void calcularTotal() 
	{
		
	}

	@Override
	public void establecerTitulosColumnas() 
	{
		this.getColumn("porc").setWidth(80);
		this.getColumn("porc").setHeaderCaption("");
		
		this.getColumn("tarea").setWidth(120);
		
		this.getColumn("codigoArticulo").setWidth(140);
		this.getColumn("codigoArticulo").setEditable(false);
		this.getColumn("descripcionArticulo").setWidth(320);
		this.getColumn("descripcionArticulo").setEditable(false);
		this.getColumn("notasProd").setWidth(220);
		this.getColumn("notasPlan").setWidth(220);
		
		this.getColumn("estado").setHidden(true);
		this.getColumn("modificacion").setHidden(true);
		this.getColumn("modificar").setHidden(true);
		this.getColumn("idCodigo").setHidden(true);
		this.getColumn("idArticuloNuevo").setHidden(true);
		this.getColumn("idTareaEsc").setHidden(true);
		this.getColumn("validacion").setHidden(true);
		this.getColumn("mapeoTareasEscandallo").setHidden(true);
		this.getColumn("mapeoEstadoTareasEscandallo").setHidden(true);
		this.getColumn("estadoDes").setHidden(true);
	}

	@Override
	public void establecerColumnasNoFiltro()
	{
		
	}
}
