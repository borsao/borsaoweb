package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoNotasArticulos mapeoNotasArticulos= null;
	 
	 private consultaNotasArticulosServer ser = null;	 
	 private NotasArticulosView uw = null;
	 private BeanFieldGroup<MapeoNotasArticulos> fieldGroup;
	 
	 public OpcionesForm(NotasArticulosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoNotasArticulos>(MapeoNotasArticulos.class);
        fieldGroup.bindMemberFields(this);
        
        this.caducidad.addStyleName("v-datefield-textfield");
        this.caducidad.setDateFormat("dd/MM/yyyy");
        this.caducidad.setShowISOWeekNumbers(true);
        this.caducidad.setResolution(Resolution.DAY);

        this.descripcion.setCaptionAsHtml(true);
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeoNotasArticulos = (MapeoNotasArticulos) uw.grid.getSelectedRow();
                eliminarOpcion(mapeoNotasArticulos);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoNotasArticulos = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOpcion(mapeoNotasArticulos);
	 			}
	 			else
	 			{
	 				MapeoNotasArticulos mapeo_orig= (MapeoNotasArticulos) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo();
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoNotasArticulos = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarNotaArticulo(mapeoNotasArticulos,mapeo_orig);
	 				hm=null;
	 			}
	 			
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0 )
				{
					ser = new consultaNotasArticulosServer(CurrentUser.get());
					String desc = ser.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
			}
		});
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.articulo.getValue()!=null && this.articulo.getValue().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue());
			opcionesEscogidas.put("descripcion", this.descripcion.getValue());
			opcionesEscogidas.put("notas", this.notas.getValue());
			if (this.caducidad.getValue()!=null) opcionesEscogidas.put("caducidad", RutinasFechas.convertirDateToString(this.caducidad.getValue()));
			
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
			opcionesEscogidas.put("descripcion", "");
			opcionesEscogidas.put("notas", "");
			opcionesEscogidas.put("caducidad", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoNotasArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoNotasArticulos();
		fieldGroup.setItemDataSource(new BeanItem<MapeoNotasArticulos>(r_mapeo));
		articulo.focus();
	}
	
	public void eliminarOpcion(MapeoNotasArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaNotasArticulosServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		ser.eliminar(r_mapeo);
		
	}
	
	public void crearOpcion(MapeoNotasArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaNotasArticulosServer(CurrentUser.get());
		String rdo = ser.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoNotasArticulos>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null)
			{
				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();	
		}
	}
	
	public void modificarNotaArticulo(MapeoNotasArticulos r_mapeo, MapeoNotasArticulos r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			consultaNotasArticulosServer cus  = new consultaNotasArticulosServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((OpcionesGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}

    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoNotasArticulos> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoNotasArticulos= item.getBean();
            if (this.mapeoNotasArticulos.getArticulo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
    
    private boolean todoEnOrden()
    {
    	if (this.articulo.getValue()==null || this.articulo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el articulo");
    		return false;
    	}
    	if (this.notas.getValue()==null || this.notas.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar las notas");
    		return false;
    	}
    	return true;
    }

}
