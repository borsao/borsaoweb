package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.MapeoEstadoTareasProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.modelo.MapeoTareasProtocolo;

public class MapeoArticulosProtocolo extends MapeoGlobal
{
	private Integer validacion;
	private String notas;
	private Integer idTareaPro;
	private Integer idArticuloNuevo;
	private MapeoTareasProtocolo mapeoTareasProtocolo = null;
	private MapeoEstadoTareasProtocolo mapeoEstadoTareasProtocolo = null;
	
	public MapeoArticulosProtocolo()
	{
		this.setNotas("");
	}

	public Integer getValidacion() {
		return validacion;
	}

	public void setValidacion(Integer validacion) {
		this.validacion = validacion;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public Integer getIdTareaPro() {
		return idTareaPro;
	}

	public void setIdTareaPro(Integer idTareaPro) {
		this.idTareaPro = idTareaPro;
	}

	public Integer getIdArticuloNuevo() {
		return idArticuloNuevo;
	}

	public void setIdArticuloNuevo(Integer idArticuloNuevo) {
		this.idArticuloNuevo = idArticuloNuevo;
	}

	public MapeoTareasProtocolo getMapeoTareasProtocolo() {
		return mapeoTareasProtocolo;
	}

	public void setMapeoTareasProtocolo(MapeoTareasProtocolo mapeoTareasProtocolo) {
		this.mapeoTareasProtocolo = mapeoTareasProtocolo;
	}

	public MapeoEstadoTareasProtocolo getMapeoEstadoTareasProtocolo() {
		return mapeoEstadoTareasProtocolo;
	}

	public void setMapeoEstadoTareasProtocolo(MapeoEstadoTareasProtocolo mapeoEstadoTareasProtocolo) {
		this.mapeoEstadoTareasProtocolo = mapeoEstadoTareasProtocolo;
	}
}