package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosEscandallo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server.consultaArticulosNuevosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.MapeoEstadoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.server.consultaEstadoTareasEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.server.consultaTareasEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoArticulosEscandallo mapeoArticulosEscandallo= null;
	 
	 private consultaArticulosNuevosServer ser = null;
	 private consultaTareasEscandalloServer serEsc = null;
	 private ArticulosNuevosView uw = null;
	 private BeanFieldGroup<MapeoArticulosEscandallo> fieldGroup;
	 
	 public OpcionesForm(ArticulosNuevosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoArticulosEscandallo>(MapeoArticulosEscandallo.class);
        fieldGroup.bindMemberFields(this);
        
        this.descripcionArticulo.setCaptionAsHtml(true);
        this.cargarListeners();
        this.cargarCombo();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.gridEscandallo!=null)
		 			{			
		 				uw.gridEscandallo.removeAllColumns();
		 				uw.abajo.removeComponent(uw.gridEscandallo);
		 				uw.gridEscandallo = null;
		 			}
		 			uw.cargarArticuloEscandallo();
		 			btnGuardar.setCaption("Guardar");
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
//	 				hashToMapeo hm = new hashToMapeo(); 
//	 				uw.opcionesEscogidas  = rellenarHashOpciones();
//	 				mapeoArticulosEscandallo = hm.convertirHashAMapeo(uw.opcionesEscogidas);
//	                crearOpcion(mapeoArticulosEscandallo);
	 			}
	 			else
	 			{
	 				MapeoArticulosEscandallo mapeo_orig= (MapeoArticulosEscandallo) uw.gridEscandallo.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo();
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoArticulosEscandallo = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarTareaEscandallo(mapeoArticulosEscandallo,mapeo_orig);
	 				hm=null;
	 			}
	 			
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	cerrar();
            }
        });
		
		this.codigoArticulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (codigoArticulo.getValue()!=null && codigoArticulo.getValue().length()>0 )
				{
					if (codigoArticulo.getValue().length()>40)
					{
						Notificaciones.getInstance().mensajeError("Demasiados caracteres. Ajusta a 40");
						btnGuardar.setEnabled(false);
						codigoArticulo.focus();
					}
					else
					{
						btnGuardar.setEnabled(true);
						consultaArticulosServer serArt = new consultaArticulosServer(CurrentUser.get());
						String desc = serArt.obtenerDescripcionArticulo(codigoArticulo.getValue());
						descripcionArticulo.setValue(desc);
					}
				}
			}
		});
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		if (creacion) editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
//		if (this.codigoArticulo.getValue()!=null && this.codigoArticulo.getValue().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.codigoArticulo.getValue());
			opcionesEscogidas.put("descripcion", this.descripcionArticulo.getValue());
			opcionesEscogidas.put("notasPlan", this.notasPlan.getValue());
			opcionesEscogidas.put("notasProd", this.notasProd.getValue());
			if (this.modificar.getValue()) opcionesEscogidas.put("modificar", "S"); else opcionesEscogidas.put("modificar", "N"); 
		}
//		else
//		{
//			opcionesEscogidas.put("articulo", "");
//			opcionesEscogidas.put("descripcion", "");
//			opcionesEscogidas.put("notasPlan", "");
//			opcionesEscogidas.put("notasProd", "");
//			opcionesEscogidas.put("modificar", "");
//		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoArticulosEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoArticulosEscandallo();
		fieldGroup.setItemDataSource(new BeanItem<MapeoArticulosEscandallo>(r_mapeo));
		codigoArticulo.focus();
	}
	
	public void eliminarOpcion(MapeoArticulosEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
	}
	
	public void crearOpcion(MapeoArticulosEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
	}
	
	public void modificarTareaEscandallo(MapeoArticulosEscandallo r_mapeo, MapeoArticulosEscandallo r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			consultaArticulosNuevosServer cus  = new consultaArticulosNuevosServer(CurrentUser.get());
			consultaEstadoTareasEscandalloServer cetes = consultaEstadoTareasEscandalloServer.getInstance(CurrentUser.get());
			
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			r_mapeo.setIdArticuloNuevo(r_mapeo_orig.getIdArticuloNuevo());
			r_mapeo.setIdTareaEsc(r_mapeo_orig.getIdTareaEsc());
			r_mapeo.setEstado(cetes.obtenerIdEstadoTareaEscandallo(this.estadoDes.getValue().toString()).toString());
			cus.guardarCambiosEscandallo(r_mapeo);		
			uw.cargarArticuloEscandallo();
			uw.cerrarForm();
		}
	}

    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoArticulosEscandallo> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoArticulosEscandallo= item.getBean();
            if (this.mapeoArticulosEscandallo.getIdCodigo()!=null) canRemoveProduct = true;
        }
    }
    public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
        setEnabled(false);        
        uw.cerrarForm();
	}
    
    private boolean todoEnOrden()
    {
    	Iterator<MapeoArticulosEscandallo> iter = null;
    	
    	if (!this.estadoDes.getValue().toString().contains("No Aplica"))
    	{
    		
	    	if (this.codigoArticulo.getValue()==null || this.codigoArticulo.getValue().toString().length()==0 )
	    	{
	    		Notificaciones.getInstance().mensajeError("Debes Rellenar el articulo");
	    		return false;
	    	}
	    	else 
	    	{
//	    		iter = ((EscandalloArticuloGrid) uw.gridEscandallo).vector.iterator();
//	    		
//	    		while (iter.hasNext())
//	    		{
//	    			MapeoArticulosEscandallo map = new MapeoArticulosEscandallo();
//	    			map = iter.next();
//	    			if (map.getCodigoArticulo().contentEquals(this.codigoArticulo.getValue()))
//	    			{
//	    				Notificaciones.getInstance().mensajeError("El articulo ya existe dentro del escandallo");
//	    				return false;
//	    			}
//	    		}
	    		return true;
	    	}
    	}
    	return true;
    }

    private void cargarCombo()
    {
    	Iterator<MapeoEstadoTareasEscandallo> iterator = null;
    	consultaEstadoTareasEscandalloServer cets = consultaEstadoTareasEscandalloServer.getInstance(CurrentUser.get());
    	ArrayList<MapeoEstadoTareasEscandallo> vector = cets.datosEstadoTareasEscandalloGlobal(null);
    	
    	iterator = vector.iterator();
    	
    	while (iterator.hasNext())
    	{
    		MapeoEstadoTareasEscandallo mapeo = iterator.next();
    		this.estadoDes.addItem(mapeo.getDescripcion());
    	}
    	
    }
}
