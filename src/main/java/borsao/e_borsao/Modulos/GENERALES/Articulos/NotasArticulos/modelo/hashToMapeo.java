package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoNotasArticulos mapeo = null;
	 
	 public MapeoNotasArticulos convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoNotasArticulos();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setNotas(r_hash.get("notas"));
		 try {
			 if (r_hash.get("caducidad")!=null && r_hash.get("caducidad").length()>0)
			 {
				 this.mapeo.setCaducidad(RutinasFechas.convertirADate(r_hash.get("caducidad")));
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return mapeo;		 
	 }
	 
	 public MapeoNotasArticulos convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoNotasArticulos();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setNotas(r_hash.get("notas"));
		 try {
			 if (r_hash.get("caducidad")!=null && r_hash.get("caducidad").length()>0)
			 {
				 this.mapeo.setCaducidad(RutinasFechas.convertirADate(r_hash.get("caducidad")));
			 }

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoNotasArticulos r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("notas", this.mapeo.getNotas());
		 if (this.mapeo.getCaducidad()!=null) this.hash.put("caducidad", RutinasFechas.convertirDateToString(this.mapeo.getCaducidad()));
		 
		 return hash;		 
	 }
}