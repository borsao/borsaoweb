package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoArticulosNuevos extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer version;
	private Date fechaAlta;
	private Date fechaFabricacion;
	private String eliminar;
	private InputStream imagen;
	private String img;
	private String imgSN;
	private String porcEsc;
	private String porcProt;
	private String activoSN;
	private String modificacionSN;
	private String mod;
	
	public MapeoArticulosNuevos()
	{
		this.setArticulo("");
		this.setDescripcion("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getPorcEsc() {
		return porcEsc;
	}

	public void setPorcEsc(String porcEsc) {
		this.porcEsc = porcEsc;
	}

	public String getPorcProt() {
		return porcProt;
	}

	public void setPorcProt(String porcProt) {
		this.porcProt = porcProt;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getFechaAlta() {
		if (fechaAlta!= null)  return RutinasFechas.convertirDateToString(fechaAlta); else return null;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getFechaFabricacion() {
		if (fechaFabricacion!= null) return RutinasFechas.convertirDateToString(fechaFabricacion); else return null;
	}

	public void setFechaFabricacion(Date fechaFabricacion) {
		this.fechaFabricacion = fechaFabricacion;
	}

	public String getEliminar() {
		return eliminar;
	}

	public void setEliminar(String eliminar) {
		this.eliminar = eliminar;
	}

	public String getActivoSN() {
		return activoSN;
	}

	public void setActivoSN(String activoSN) {
		this.activoSN = activoSN;
	}

	public String getModificacionSN() {
		return modificacionSN;
	}

	public void setModificacionSN(String modificacionSN) {
		this.modificacionSN = modificacionSN;
	}

	public String getMod() {
		return modificacionSN;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public InputStream getImagen() {
		return imagen;
	}

	public void setImagen(InputStream imagen) {
		this.imagen = imagen;
	}

	public String getImgSN() {
		return imgSN;
	}

	public void setImgSN(String imgSN) {
		this.imgSN = imgSN;
	}
}