package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.MapeoEstadoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.MapeoTareasEscandallo;

public class MapeoArticulosEscandallo extends MapeoGlobal
{
	private Integer validacion;
	private String descripcionArticulo;
	private String codigoArticulo;
	private String notasPlan;
	private String notasProd;
	private String tarea;
	private Integer idTareaEsc;
	private Integer idArticuloNuevo;
	private MapeoTareasEscandallo mapeoTareasEscandallo= null;
	private MapeoEstadoTareasEscandallo mapeoEstadoTareasEscandallo= null;
	private String estado = null;
	private String estadoDes = null;
	private String porc = null;
	private String modificacion = null;
	private boolean modificar;
	public MapeoArticulosEscandallo()
	{
		this.setNotasProd("");
		this.setNotasPlan("");
	}

	public Integer getValidacion() {
		return validacion;
	}

	public void setValidacion(Integer validacion) {
		this.validacion = validacion;
	}

	public String getNotasPlan() {
		return notasPlan;
	}

	public void setNotasPlan(String notasPlan) {
		this.notasPlan = notasPlan;
	}

	public String getNotasProd() {
		return notasProd;
	}

	public void setNotasProd(String notasProd) {
		this.notasProd = notasProd;
	}

	public Integer getIdTareaEsc() {
		return idTareaEsc;
	}

	public void setIdTareaEsc(Integer idTareaEsc) {
		this.idTareaEsc = idTareaEsc;
	}

	public Integer getIdArticuloNuevo() {
		return idArticuloNuevo;
	}

	public void setIdArticuloNuevo(Integer idArticuloNuevo) {
		this.idArticuloNuevo = idArticuloNuevo;
	}

	public MapeoTareasEscandallo getMapeoTareasEscandallo() {
		return mapeoTareasEscandallo;
	}

	public void setMapeoTareasEscandallo(MapeoTareasEscandallo mapeoTareasEscandallo) {
		this.mapeoTareasEscandallo = mapeoTareasEscandallo;
	}

	public MapeoEstadoTareasEscandallo getMapeoEstadoTareasEscandallo() {
		return mapeoEstadoTareasEscandallo;
	}

	public void setMapeoEstadoTareasEscandallo(MapeoEstadoTareasEscandallo mapeoEstadoTareasEscandallo) {
		this.mapeoEstadoTareasEscandallo = mapeoEstadoTareasEscandallo;
	}

	public String getCodigoArticulo() {
		return codigoArticulo;
	}

	public void setCodigoArticulo(String codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}

	public String getDescripcionArticulo() {
		return descripcionArticulo;
	}

	public void setDescripcionArticulo(String descripcionArticulo) {
		this.descripcionArticulo = descripcionArticulo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getPorc() {
		if (porc.contentEquals("999")) return porc = "";
		return porc + "% ";
	}

	public void setPorc(String porc) {
		this.porc = porc;
	}

	public String getTarea() {
		if (mapeoTareasEscandallo!=null)
			return mapeoTareasEscandallo.getTarea();
		else
			return "";
	}

	public void setTarea(String tarea) {
		this.tarea = tarea;
	}
	
	public String getEstadoDes() {
		return estadoDes;
	}

	public void setEstadoDes(String estadoDes) {
		this.estadoDes = estadoDes;
	}

	public String getModificacion() {
		return modificacion;
	}

	public void setModificacion(String modificacion) {
		this.modificacion = modificacion;
	}

	public boolean isModificar() {
		return modificar;
	}

	public void setModificar(boolean modificar) {
		this.modificar = modificar;
	}

}