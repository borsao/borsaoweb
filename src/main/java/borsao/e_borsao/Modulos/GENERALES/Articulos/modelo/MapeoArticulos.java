package borsao.e_borsao.Modulos.GENERALES.Articulos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoArticulos extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String observaciones;
	private Double cuantos;

	public MapeoArticulos()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setObservaciones("");
		this.setCuantos(new Double(1));
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getCuantos() {
		return cuantos;
	}

	public void setCuantos(Double cuantos) {
		this.cuantos = cuantos;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	
}