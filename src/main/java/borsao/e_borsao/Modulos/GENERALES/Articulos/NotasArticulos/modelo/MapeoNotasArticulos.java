package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoNotasArticulos extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String notas;
	private Date caducidad;

	public MapeoNotasArticulos()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setNotas("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNotas() {
		return notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public Date getCaducidad() {
			return caducidad;
	}

	public void setCaducidad(Date caducidad) {
		this.caducidad = caducidad;
	}


	
}