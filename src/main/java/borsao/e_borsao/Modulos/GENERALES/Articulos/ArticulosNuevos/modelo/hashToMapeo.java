package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.MapeoEstadoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.MapeoTareasEscandallo;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulosEscandallo mapeo = null;
	 
	 public MapeoArticulosEscandallo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEscandallo();
		 this.mapeo.setCodigoArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcionArticulo(r_hash.get("descripcion"));
		 this.mapeo.setNotasPlan(r_hash.get("notasPlan"));
		 this.mapeo.setNotasProd(r_hash.get("notasProd"));
		 if (r_hash.get("modificar").contentEquals("S")) this.mapeo.setModificar(true); else this.mapeo.setModificar(false);
		 return mapeo;		 
	 }
	 
	 public MapeoArticulosEscandallo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEscandallo();
		 this.mapeo.setCodigoArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcionArticulo(r_hash.get("descripcion"));
		 this.mapeo.setNotasPlan(r_hash.get("notasPlan"));
		 this.mapeo.setNotasProd(r_hash.get("notasProd"));
		 if (r_hash.get("modificar").contentEquals("S")) this.mapeo.setModificar(true); else this.mapeo.setModificar(false);
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulosEscandallo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getCodigoArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcionArticulo());
		 this.hash.put("notasPlan", this.mapeo.getNotasPlan());
		 this.hash.put("notasProd", this.mapeo.getNotasProd());
		 if (this.mapeo.isModificar()) this.hash.put("modificar", "S"); else this.hash.put("modificar", "N"); 
		 return hash;		 
	 }
}