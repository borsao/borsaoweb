package borsao.e_borsao.Modulos.GENERALES.Articulos.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;

public class consultaArticulosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosServer instance;
	
	public consultaArticulosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select articulo, descrip_articulo from e_articu where articulo ='" + r_articulo + "'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo=RutinasCadenas.conversion(rsArticulos.getString("descrip_articulo"));								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}	

	public HashMap<String, String> obtenerDescripcionesArticulos (Connection r_conexion)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		HashMap<String, String> hashDesc = null;
		String sql = null;
		try
		{
			sql="select articulo, descrip_articulo from e_articu where articulo in (select articulo from e__ccs where cultivo = 1) ";
			
			cs = r_conexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			hashDesc = new HashMap<String, String>();
			while(rsArticulos.next())
			{
				hashDesc.put(rsArticulos.getString("articulo"), rsArticulos.getString("descrip_articulo"));								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashDesc;
	}	

	public Integer obtenerVelocidadArticulo(String r_articulo)
	{
		Integer rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select vl_caract_4 from e_articu where articulo ='" + r_articulo + "'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo=rsArticulos.getInt("vl_caract_4");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public ArrayList<MapeoArticulos> obtenerArticulos(String r_mascara)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		MapeoArticulos mapeo = null;
		ArrayList<MapeoArticulos> vector = null;
		
		String sql = null;
		try
		{
			sql="select articulo from e_articu where articulo like '" + r_mascara + "%'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			
			vector = new ArrayList<MapeoArticulos>();
			
			while(rsArticulos.next())
			{
				mapeo = new MapeoArticulos();
				mapeo.setArticulo(rsArticulos.getString("articulo").trim());
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}	
	public ArrayList<MapeoArticulos> obtenerArticulosDesc(String r_mascara)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;
		
		MapeoArticulos mapeo = null;
		ArrayList<MapeoArticulos> vector = null;
		
		String sql = null;
		try
		{
			sql="select articulo from e_articu where descrip_articulo like '%" + r_mascara + "%'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			
			vector = new ArrayList<MapeoArticulos>();
			
			while(rsArticulos.next())
			{
				mapeo = new MapeoArticulos();
				mapeo.setArticulo(rsArticulos.getString("articulo").trim());
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return vector;
	}	
	
	public Integer obtenerCaducidadArticulo(String r_articulo)
	{
		Integer rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select articulo, num_lista from e_articu where articulo ='" + r_articulo + "'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo=rsArticulos.getInt("num_lista");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}	

	public String obtenerAlmacenArticulo(String r_articulo)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select articulo, talla_formato from e_articu where articulo ='" + r_articulo + "'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo=RutinasCadenas.conversion(rsArticulos.getString("talla_formato"));								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}	
	
	public String obtenerTipoArticulo(String r_articulo)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select tipo_articulo from e_articu where articulo ='" + r_articulo + "'" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo=rsArticulos.getString("tipo_articulo");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}	

	public String obtenerEan(String r_articulo, String r_prefijo)
	{
		String ean_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select cod_barra_art from e_art_cb where articulo = '" + r_articulo.trim() + "' and cod_barra_art matches '" + r_prefijo + "*'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				ean_obtenido = rsArticulos.getString("cod_barra_art");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return ean_obtenido;
	}
	
	public String obtenerCantidadEan(String r_articulo, String r_prefijo)
	{
		String cantidadObtenida = null;
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select unidades from e_art_cb where articulo = '" + r_articulo.trim() + "' and cod_barra_art matches '" + r_prefijo + "*'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				cantidadObtenida = rsArticulos.getString("unidades");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return cantidadObtenida;
	}

	public String obtenerArticuloEan(String r_ean)
	{
		String articulo=null;
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select articulo from e_art_cb where cod_barra_art = '" + r_ean.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				articulo = rsArticulos.getString("articulo");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return articulo;
	}
	public String obtenerCantidadEan(Connection r_con, String r_articulo, String r_prefijo)
	{
		String cantidadObtenida = null;
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		String bbdd = null;
		try
		{
			try
			{
				bbdd = r_con.getCatalog();
			}
			catch (Exception ex)
			{
				bbdd = "greensys";
			}
			
			if (bbdd.contains("laboratorio"))
			{
				sql="select unidades from e_art_cb where articulo = '" + r_articulo.trim() + "' and cod_barra_art like '" + r_prefijo + "%'";
			}
			else
				sql="select unidades from e_art_cb where articulo = '" + r_articulo.trim() + "' and cod_barra_art matches '" + r_prefijo + "*'";
			
			cs = r_con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				cantidadObtenida = rsArticulos.getString("unidades");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return cantidadObtenida;
	}
	
	public ArrayList<String> obtenerCantidadesEan(String r_articulo)
	{
		ArrayList<String> cantidadObtenida = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select max(unidades) unidades from e_art_cb where articulo = '" + r_articulo.trim() + "' ";
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			cantidadObtenida = new ArrayList<String>();
			
			while(rsArticulos.next())
			{
				cantidadObtenida.add(rsArticulos.getString("unidades"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return cantidadObtenida;
	}

	public boolean obtenerArticulosSinFactor()
	{
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		String art = null;
		boolean rdo = false;
		try
		{
			sql="select articulo from e_articu where cod_desarrollo = 'N' and (articulo matches '0101*' or articulo matches '0102*' or articulo matches '0103*' or articulo matches '0111*') and (marca = '' or marca is null or marca = '0') ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("articulo")!=null) rdo=true; 
				Notificaciones.getInstance().mensajeSeguimiento("Sin Volumen " + rsArticulos.getString("articulo"));
				if (art==null) art = "";
				art = art + "-" + rsArticulos.getString("articulo");
			}
			if (rdo) Notificaciones.getInstance().mensajeError("Articulos sin Volumen: " + art); 
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public String obtenerFactorArticulo(String r_articulo)
	{
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;

		try
		{
			sql="select modelo from e_articu where cod_desarrollo = 'N' and articulo = '" + r_articulo.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("modelo")!=null) return rsArticulos.getString("modelo");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String obtenerCapacidadArticulo(String r_articulo)
	{
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		
		try
		{
			sql="select marca from e_articu where cod_desarrollo = 'N' and articulo = '" + r_articulo.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("marca")!=null) return rsArticulos.getString("marca");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}

	public boolean obtenerArticulosSinNC()
	{
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		String art = null;
		boolean rdo = false;
		try
		{
			sql="select articulo from e_articu where cod_desarrollo = 'N' and (articulo matches '0101*' or articulo matches '0102*' or articulo matches '0103*' or articulo matches '0111*') and (vl_caract_2 = '' or vl_caract_2 is null) ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("articulo")!=null) rdo=true; 
				Notificaciones.getInstance().mensajeSeguimiento("Sin NC " + rsArticulos.getString("articulo"));
				if (art==null) art = "";
				art = art + "-" + rsArticulos.getString("articulo");
			}
			if (rdo) Notificaciones.getInstance().mensajeError("Articulos sin NC: " + art); 
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public boolean obtenerArticulosSinGrado()
	{
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		String art = null;
		boolean rdo = false;
		
		try
		{
			sql="select articulo from e_articu where cod_desarrollo = 'N' and (articulo matches '0101*' or articulo matches '0102*' or articulo matches '0103*' or articulo matches '0111*') and (precio_compra2<= 0) ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			if (rsArticulos!=null)
			{
				while(rsArticulos.next())
				{
					if (rsArticulos.getString("articulo")!=null) rdo=true; 
					Notificaciones.getInstance().mensajeSeguimiento("Sin grado " + rsArticulos.getString("articulo"));
					if (art==null) art = "";
					art = art + "-" + rsArticulos.getString("articulo");
				}
				if (rdo) Notificaciones.getInstance().mensajeError("Articulos sin GRADO: " + art); 
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				return rdo;
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public Double obtenerArticulosGrado(String r_articulo)
	{
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		Double rdo = new Double(0);
		
		try
		{
			sql="select precio_compra2 from e_articu where cod_desarrollo = 'N' and articulo = '" + r_articulo.trim() + "' ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("precio_compra2")!=null) rdo=rsArticulos.getDouble("precio_compra2"); 
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				return rdo;
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String obtenerArticulosNC(String r_articulo)
	{
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		String rdo = null;
		
		try
		{
			sql="select vl_caract_2 from e_articu where articulo = '" + r_articulo.trim() + "' ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			while(rsArticulos.next())
			{
				if (rsArticulos.getString("vl_caract_2")!=null) rdo=rsArticulos.getString("vl_caract_2"); 
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				return rdo;
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu order by articulo asc" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public HashMap<String, String> hashArticulos(String r_familia, String r_subfamilia, String r_tipoArticulo)
	{
		HashMap<String, String> rdo = null;
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select distinct articulo, descrip_articulo from e_articu where familia = '" + r_familia + "' and sub_familia = '" + r_subfamilia + "' and tipo_articulo = '" + r_tipoArticulo.trim() + "' "  ;
			
			rdo = new HashMap<String, String>();
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsArticulos = cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo.put(rsArticulos.getString("articulo").trim(),RutinasCadenas.conversion(rsArticulos.getString("descrip_articulo").trim()));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}

	public ArrayList<String> listaArticulos(String r_familia, String r_subfamilia, String r_tipoArticulo)
	{
		ArrayList<String> rdo = null;
		ResultSet rsArticulos = null;
		String sql = null;
		Connection con = null;
		Statement cs = null;
		try
		{
			sql="select distinct articulo from e_articu where familia = '" + r_familia + "' and sub_familia = '" + r_subfamilia + "' and tipo_articulo = '" + r_tipoArticulo.trim() + "' "  ;
			
			rdo = new ArrayList<String>();
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsArticulos = cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo.add(rsArticulos.getString("articulo").trim());
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return rdo;
	}

	public boolean comprobarArticulo(String r_articulo)
	{
		String rdo = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			sql="select articulo, descrip_articulo from e_articu where articulo ='" + r_articulo.trim() + "' " ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				return true;								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}	
	
}
