package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosNuevos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server.consultaArticulosNuevosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaCreacionArticulo extends Window
{

	private final String titulo = "Articulo Nuevo";
	private ventanaAyuda vHelp  =null;	
	
	private VerticalLayout barAndGridLayout = null;    
    private VerticalLayout topLayout = null;
    private VerticalLayout centerLayout = null;
    private HorizontalLayout pieLayout = null;
    private Label lblTitulo = null; 
    
	private TextField txtArticulo= null;
	private TextField txtDescripcionArticulo= null;
	private CheckBox chkCopiar = null;
	private CheckBox chkNuevo = null;
	private TextField txtVersion = null;
	private Combo cmbVersionCopia = null;
	private Button opcImagen = null;
	private Button opcAceptar = null;
	private Button opcSalir = null;
	private ArticulosNuevosView app = null;
	private String articuloSeleccionado=null; 
//	private MapeoAGeneralDashboardArticulo mapeo=null;

	private boolean traerEscandalloGreensys = false;
	
	public pantallaCreacionArticulo(ArticulosNuevosView r_app, String r_titulo)
	{
		
		this.app = r_app;
		this.setCaption(r_titulo);
		this.center();
		
    	this.setHeight("650px");
    	this.setWidth("450px");
    	this.addStyleName("crud-view");

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);

	}

	private void cargarPantalla()
	{
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
//        this.barAndGridLayout.setSpacing(false);
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");

		    	this.topLayout = new VerticalLayout();
		    	this.topLayout.setSpacing(true);
		    	this.topLayout.setSizeUndefined();
		    	this.topLayout.setMargin(true);

			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
//			    	this.lblTitulo.setStyleName("labelTituloVentas");

		    	this.topLayout.addComponent(this.lblTitulo);
		    	this.topLayout.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);
		    	
		    	this.centerLayout = new VerticalLayout();
		    	this.centerLayout.setSpacing(true);
		    	this.centerLayout.setSizeUndefined();
		    	this.centerLayout.setMargin(true);
		    	
			    	this.txtArticulo=new TextField("Articulo");
			    	this.txtArticulo.setEnabled(true);
			    	this.txtArticulo.setWidth("325px");
			    	this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

			    	this.txtDescripcionArticulo=new TextField("Descripcion Articulo");
			    	this.txtDescripcionArticulo.setEnabled(true);
			    	this.txtDescripcionArticulo.setWidth("325px");
			    	this.txtDescripcionArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
			    	
			    	this.chkCopiar = new CheckBox("Copiar?");
			    	this.chkCopiar.setEnabled(false);
			    	
			    	this.txtVersion = new TextField("Version");
			    	this.txtVersion.setEnabled(false);

			    	this.cmbVersionCopia = new Combo("Version a Copiar");
			    	this.cmbVersionCopia.setEnabled(false);

			    	this.chkNuevo= new CheckBox("Articulo Nuevo?");
			    	this.chkNuevo.setEnabled(true);

		    	this.centerLayout.addComponent(this.txtArticulo);		   
		    	this.centerLayout.addComponent(this.txtDescripcionArticulo);
		    	this.centerLayout.addComponent(this.txtVersion);
		    	this.centerLayout.addComponent(this.chkNuevo);
		    	this.centerLayout.addComponent(this.chkCopiar);
		    	this.centerLayout.addComponent(this.cmbVersionCopia);
		    	
		    	this.pieLayout = new HorizontalLayout();
		    	this.pieLayout.setSpacing(true);
		    	this.pieLayout.setSizeUndefined();
		    	this.pieLayout.setMargin(true);

			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcAceptar= new Button("Aceptar");
			    	this.opcAceptar.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcAceptar.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcAceptar.setIcon(FontAwesome.REFRESH);


		    	this.pieLayout.addComponent(this.opcAceptar);
		    	this.pieLayout.setComponentAlignment(this.opcAceptar,  Alignment.BOTTOM_LEFT);
		    	this.pieLayout.addComponent(this.opcSalir);
		    	this.pieLayout.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);

    	this.barAndGridLayout.addComponent(this.topLayout);
    	this.barAndGridLayout.addComponent(this.centerLayout);
    	this.barAndGridLayout.addComponent(this.pieLayout);
    	
	}
	
	
    private void cargarListeners() 
    {

    	this.chkCopiar.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtVersion.setVisible(true);
				cmbVersionCopia.setVisible(true);
			}
		});
    	
    	this.txtArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				comprobarArticulo("A", txtArticulo.getValue());
			}
		});

    	this.txtDescripcionArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (txtArticulo.getValue()==null) comprobarArticulo("D", txtDescripcionArticulo.getValue());
			}
		});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);    			
    			cerrar();
    		}
    	});

    	this.opcAceptar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			crear();
    		}
    	});
    }

    private void crear()
    {
    	if ((txtArticulo!=null && txtArticulo.getValue().length()>0) || (txtDescripcionArticulo!=null && txtDescripcionArticulo.getValue().length()>0))
    	{
    		MapeoArticulosNuevos mapeo = rellenarMapeo();
    		consultaArticulosNuevosServer cans = new consultaArticulosNuevosServer(CurrentUser.get());
    		String rdo = cans.guardarNuevo(mapeo);
    		
    		if (rdo==null)
    		{
    			if (isTraerEscandalloGreensys())
    			{
    				/*
    				 * Crearemos los registros de tareas escandallo, asociando el escandallo que tengamos en GREENsys
    				 * 
    				 * Pasaremos como parametro el articulo
    				 */
    				rdo = cans.guardarNuevoEscandallo(mapeo.getIdCodigo(),this.txtArticulo.getValue());
    			}
    			else
    			{
    				rdo = cans.guardarNuevoEscandallo(mapeo.getIdCodigo(),null);
    			}
    			
    			rdo = cans.guardarNuevoProtocolo(mapeo.getIdCodigo());
    			
    			if (this.txtArticulo.getValue()!=null && this.txtArticulo.getValue().length()>0) this.app.aceptar(this.txtArticulo.getValue()); else this.app.aceptar(this.txtDescripcionArticulo.getValue());
    			this.close();
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeError("Error al guardar los datos.");
    		}
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeError("Rellena el articulo correctamente");
    	}
    }

    private void cerrar()
    {
    	this.app.cerrar();
    	close();
    }

    private MapeoArticulosNuevos rellenarMapeo()
    {
    	MapeoArticulosNuevos mapeo = new MapeoArticulosNuevos();
    	if (this.txtArticulo.getValue()!=null) mapeo.setArticulo(this.txtArticulo.getValue().trim());
    	if (this.txtDescripcionArticulo.getValue()!=null) mapeo.setDescripcion(this.txtDescripcionArticulo.getValue());
    	if (this.txtVersion.isEnabled()) mapeo.setVersion(new Integer(this.txtVersion.getValue().toString()));
    	if (this.chkNuevo.getValue()) mapeo.setModificacionSN("N"); else mapeo.setModificacionSN("S");
    	mapeo.setFechaAlta(RutinasFechas.conversionDeString(RutinasFechas.fechaActual()));
    	
    	return mapeo;
    }
    
//    private void cargarPadres()
//    {
//    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
//    	consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
//    	ArrayList<MapeoArticulos> vectorPadres = ces.recuperarPadresDesc(this.articuloSeleccionado);
//    	IndexedContainer container =null;
//    	Iterator<MapeoArticulos> iterator = null;
//    	MapeoArticulos mapeo = null;
//    		
//		container = new IndexedContainer();
//		container.addContainerProperty("articulo", String.class, null);
//		container.addContainerProperty("descripcion", String.class, null);
//			
//		iterator = vectorPadres.iterator();
//        
//		while (iterator.hasNext())
//		{
//			Object newItemId = container.addItem();
//			Item file = container.getItem(newItemId);
//			
//			mapeo= (MapeoArticulos) iterator.next();
//			
//			file.getItemProperty("articulo").setValue(mapeo.getArticulo().substring(0,7));
//			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
//		}
//
//		Grid gridDatos= new Grid(container);
//		gridDatos.setSelectionMode(SelectionMode.SINGLE);
//		gridDatos.setSizeFull();
//		gridDatos.setWidth("100%");
//		gridDatos.addStyleName("minigrid");
//		gridDatos.addSelectionListener(new SelectionListener() {
//			
//			@Override
//			public void select(SelectionEvent event) {
//				
//				Item file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
////				if (grid.getSelectionModel() instanceof SingleSelectionModel && !estoyEnCelda)
////					app.filaSeleccionada();
////				else
////					estoyEnCelda = false;
//				articuloPadre= file.getItemProperty("articulo").getValue().toString();
//				descripcionArticuloPadre = cas.obtenerDescripcionArticulo(articuloPadre.trim());
//				eliminarDashBoards(false);
//				ejecutarPadres();
//			}
//		});
//
//		Panel panel = new Panel("Articulos Padre del " + articuloSeleccionado );
//    	panel.setSizeUndefined(); // Shrink to fit content
//    	panel.setHeight("250px");
//    	panel.setContent(gridDatos);
//		centralTop.addComponent(panel,0,0);
//    }
    
//    private void vaciarPantalla()
//    {
//    	centralTop=null;
//        centralMiddle=null;
//        centralBottom=null;
//        topLayout=null;
//        topLayoutL=null;
//    	
//        barAndGridLayout=null;
//        this.setContent(null);
//    }
    
    private void comprobarArticulo(String r_area, String r_articulo)
    {
    	MapeoArticulosNuevos mapeo =new MapeoArticulosNuevos();
    	boolean existe = false;
    	int maxVersion = 1;
    	/*
    	 * Comprobamos el articulo contra la tabla de articulos nuevos
    	 * 
    	 * 		si existe devolvemos datos y preguntamos si crear nueva version o no
    	 * 		si no existe lo buscamos
    	 * 			si existe traemos los datos del articulo (y si está marcado el copiar, traemos escandallo
    	 * 			si no existe le dejamos pasar con lo que haya puesto
    	 */
    	consultaArticulosNuevosServer caNs = new consultaArticulosNuevosServer(CurrentUser.get());

    	switch (r_area)
    	{
    		case "A":
    		{
    			mapeo.setArticulo(r_articulo.trim());
    			break;
    		}
    		case "D":
    		{
    			mapeo.setDescripcion(r_articulo.trim());
    			break;
    		}
    	}
    	ArrayList<MapeoArticulosNuevos> vector = caNs.recuperarArticuloVersion(mapeo);
    	
    	if (vector!=null && vector.size()>0) existe = true;
    	else
    	{
    		if (r_area.contentEquals("A"))
    		{
    			consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
    			String desc = cas.obtenerDescripcionArticulo(r_articulo.trim());
    			
    			if (desc!=null && desc.length()>0)
    			{
    				txtDescripcionArticulo.setValue(desc.trim());
    				this.setTraerEscandalloGreensys(true);
    			}
    			else
    			{
    				this.setTraerEscandalloGreensys(false);
    			}
    		}
    		
    		/*
    		 * Cuando hayamos llegado aqui indicando Articulo en el area ...
    		 * 
    		 * Como no existe en articulos nuevos buscamos si existe en greensys
    		 * 
    		 * en caso de que esté, lo daremos de alta trayendo su escandallo, 
    		 * en caso de que no esté lo creamos de cero 
    		 */
    	}
    	/*
    	 * Si existe habilitaremos la opcion de copiar
    	 * al habilitar copiar habilitaremos poder escoger la version origen
    	 */
    	if (existe)
    	{
    		this.txtVersion.setValue(String.valueOf(vector.size()+1));
    		
    		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
			String desc = cas.obtenerDescripcionArticulo(r_articulo.trim());
			txtDescripcionArticulo.setValue(desc.trim());
    		cmbVersionCopia.removeAllItems();
    		for (int i=0; i<vector.size();i++)
    		{
    			cmbVersionCopia.addItem(i+1);
    		}
    	}
    	else
    	{
    		txtVersion.setValue("1");
    	}
		chkCopiar.setEnabled(existe);
		txtVersion.setEnabled(existe);
		cmbVersionCopia.setEnabled(existe);
    }

	private boolean isTraerEscandalloGreensys() {
		
		if (txtArticulo!=null && txtArticulo.getValue().length()>0)
		{
			consultaEscandalloServer cas = consultaEscandalloServer.getInstance(CurrentUser.get());
			ArrayList<String> vector = cas.recuperarHijos(txtArticulo.getValue());
			
			if (vector!=null && vector.size()>0)
			{
				return true;
			}
			else 
				return false;
		}
		else
		return false;
	}

	private void setTraerEscandalloGreensys(boolean traerEscandalloGreensys) {
		this.traerEscandalloGreensys = traerEscandalloGreensys;
	}

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		consultaArticulosSMPTServer cps  = new consultaArticulosSMPTServer(CurrentUser.get());
		vectorArticulos=cps.vector();
		this.vHelp = new ventanaAyuda(this.txtArticulo, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}
}