package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view.NotasArticulosView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	private GridViewRefresh app = null;
	
    public OpcionesGrid(NotasArticulosView r_app, ArrayList<MapeoNotasArticulos> r_vector) 
    {
    	this.app = r_app;
        
        this.vector=r_vector;
		this.asignarTitulo("Notas Articulos");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoNotasArticulos.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo","descripcion", "notas", "caducidad");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "300");
    	this.widthFiltros.put("notas", "300");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("notas").setHeaderCaption("Notas");
    	this.getColumn("caducidad").setHeaderCaption("Caducidad");
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("caducidad").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    }
    
    /*
     * METODOS PUBLICOS
     */
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "cell-normal";
            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
		this.camposNoFiltrar.add("caducidad");
	}

	@Override
	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		if (this.footer==null) this.footer=this.appendFooterRow();
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setWidth("100%");
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		}

	}

}
