package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import java.io.InputStream;
import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.CargaImagenes;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server.consultaArticulosNuevosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view.ArticulosNuevosView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ArticulosNuevosGrid extends GridPropio {
	
	private ArticulosNuevosView app = null;
	private consultaArticulosNuevosServer cans = null;
	private MapeoArticulosNuevos mapeo = null;
	private MapeoArticulosNuevos mapeoOrig = null;
	private boolean actualizar = false;
	private ArrayList<MapeoArticulosNuevos> vector =null;
	
	public ArticulosNuevosGrid(String r_prueba, ArticulosNuevosView r_App, ArrayList<MapeoArticulosNuevos> r_vector)
	{
		this.app = r_App;
		this.vector = r_vector;
		this.crearGrid(MapeoArticulosNuevos.class);
		this.setRecords(this.vector);
		
		
		this.setSelectionMode(SelectionMode.SINGLE);
		this.setCaption("ARTICULOS NUEVOS");
		this.setSizeFull();
		this.setConFiltro(true);
		this.setWidth("100%");
		this.addStyleName("smallgrid");

	}

	public void cargarListeners() 
	{
		this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
    		boolean hayImagen=false;
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoArticulosNuevos artRow = null;
            	
            	Item file = getContainerDataSource().getItem(event.getItemId());
				if (file instanceof BeanItem<?>) 
                {
                     Object bean = ((BeanItem<?>)file).getBean();
                     if (bean instanceof MapeoArticulosNuevos) 
                     {
                         artRow = (MapeoArticulosNuevos)bean;    
                         mapeo = (MapeoArticulosNuevos)bean;
                         mapeoOrig = (MapeoArticulosNuevos)bean;
                         
                     }
                }

				if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("eliminar"))
            	{
            		app.idRegistro = (Integer) file.getItemProperty("idCodigo").getValue();
            		String art = (String) file.getItemProperty("articulo").getValue();
            		String des = (String) file.getItemProperty("descripcion").getValue();
            		Integer ver = (Integer) file.getItemProperty("version").getValue();

            		app.articuloPadreSeleccionado="";
            		app.articuloSeleccionado="";
            		app.descripcionArticuloPadreSeleccionado="";
            		app.descripcionArticuloSeleccionado="";

            		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(app, "Seguro que quieres borrar el articulo: " + art + " " + des + " version " + ver + " ? ", "Eliminar", "Cancelar", "aceptarEliminar", "cancelarEliminar");
            		app.getUI().addWindow(vt);
            		
	            }
            	else if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("imgSN"))
            	{
            		if (mapeo.getImagen()!=null)
            		{
            			verImagen();
            		}
            		else
            		{
            			agregarImagen();
            		}
            	}
            	else
            	{
            		app.filaSeleccionada(artRow);
            	}
    		}
        });
		
//		this.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void select(SelectionEvent event) {
//
//				Item file = getContainerDataSource().getItem(getSelectedRow());
//				
//				if (file instanceof BeanItem<?>) 
//                {
//                     Object bean = ((BeanItem<?>)file).getBean();
//                     if (bean instanceof MapeoArticulosNuevos) 
//                     {
//                         MapeoArticulosNuevos artRow = (MapeoArticulosNuevos)bean;                         
//                         app.filaSeleccionada(artRow);
//                     }
//                }
//			}
//		});
//		
	}
	
	@Override
	public void calcularTotal() {
		
	}



	@Override
	public void establecerTitulosColumnas() {
		
		this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "320");
    	this.widthFiltros.put("activoSN", "40");
    	this.widthFiltros.put("modificacionSN", "40");
    	
		this.getColumn("fechaAlta").setHeaderCaption("Fecha Alta");
		this.getColumn("fechaAlta").setWidth(140);
		
		this.getColumn("fechaFabricacion").setHeaderCaption("Fabricacion");
		this.getColumn("fechaFabricacion").setWidth(140);
		
		this.getColumn("articulo").setHeaderCaption("Articulo");
		this.getColumn("articulo").setWidth(120);
		
		this.getColumn("descripcion").setHeaderCaption("Nombre");
		this.getColumn("descripcion").setWidth(400);
		
		this.getColumn("version").setHeaderCaption("Version");
		this.getColumn("version").setWidth(120);
		
		this.getColumn("modificacionSN").setHeaderCaption("Modificación");
		this.getColumn("modificacionSN").setWidth(120);
		
		this.getColumn("porcEsc").setHeaderCaption("% Esc");
		this.getColumn("porcEsc").setWidth(120);
		
		this.getColumn("porcProt").setHeaderCaption("% Prot");
		this.getColumn("porcProt").setWidth(110);

		this.getColumn("activoSN").setHeaderCaption("Activo");
		this.getColumn("activoSN").setWidth(120);
		
		this.getColumn("imgSN").setWidth(40);
		this.getColumn("imgSN").setHeaderCaption("");
		
		this.getColumn("eliminar").setWidth(40);
		this.getColumn("eliminar").setHeaderCaption("");
		
		this.getColumn("idCodigo").setHidden(true);
		this.getColumn("mod").setHidden(true);
		this.getColumn("imagen").setHidden(true);
		this.getColumn("img").setHidden(true);

	}



	@Override
	public void establecerOrdenPresentacionColumnas() {
		setColumnOrder("img", "imgSN", "mod", "articulo", "descripcion", "version", "fechaAlta", "fechaFabricacion", "modificacionSN", "porcEsc", "porcProt",  "activoSN", "eliminar");
	}



	@Override
	public void establecerColumnasNoFiltro() {
		this.camposNoFiltrar.add("fechaAlta");
		this.camposNoFiltrar.add("fechaFabricacion");
		this.camposNoFiltrar.add("version");
		this.camposNoFiltrar.add("eliminar");
		this.camposNoFiltrar.add("img");
		this.camposNoFiltrar.add("imgSN");
		this.camposNoFiltrar.add("imagen");
		this.camposNoFiltrar.add("porcProt");
		this.camposNoFiltrar.add("porcEsc");
	}



	private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoArticulosNuevos) {
                // The actual description text is depending on the application
            	MapeoArticulosNuevos progRow = (MapeoArticulosNuevos)bean;
                if ("eliminar".equals(cell.getPropertyId())) {
					descriptionText = "Eliminar registro";
				}
                else if ("articulo".equals(cell.getPropertyId())) {
					if (progRow.getModificacionSN().contentEquals("S")) descriptionText = "Articulo en modificación"; else descriptionText = "Articulo nuevo";
				}
                else if ("imgSN".equals(cell.getPropertyId())) {
                	descriptionText = "Asociaar Imagen";
				}

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();

    	this.setCellStyleGenerator(new Grid.CellStyleGenerator() {
    		
    		boolean modificado = false;
    		boolean hayImagen = false;
    		public String getStyle(Grid.CellReference cellReference) {
    			if ("mod".equals(cellReference.getPropertyId())) 
    			{
    				if (cellReference.getValue().toString().contentEquals("S")) modificado = true; else  modificado = false;
    			}
    			if ("img".equals(cellReference.getPropertyId())) 
    			{
    				if (cellReference.getValue().toString().contentEquals("S")) hayImagen = true; else  hayImagen= false;
    			}

    			if ("eliminar".equals(cellReference.getPropertyId())) 
    			{
    				return "cell-nativebuttonDelete";
    			}
    			else if ("imgSN".equals(cellReference.getPropertyId()))
		     	{
		     		if (hayImagen)
		     		{
		     			return "cell-nativebuttonImagenVerde";
		     		}
		     		else
		     			return "cell-nativebuttonImagen"; 

    			} else if ("porcEsc".equals(cellReference.getPropertyId())|| "porcProt".equals(cellReference.getPropertyId())) {
    				
    				String valor = cellReference.getValue().toString().replace("%", "");
    				
					if (new Integer(valor)==100) {
						return "cell-dgreen";
					} else if (new Integer(valor)>=80) {
						return "cell-lime";
					} else if (new Integer(valor)>=60) {
						return "cell-yellow";
					} else if (new Integer(valor)>=40) {
						return "cell-orange";
					} else if (new Integer(valor)>=20) {
						return "cell-pink";
					} else if (new Integer(valor)>0) {
						return "cell-lpink";
					} else {
						return "cell-error";
					}
            	}else if ("articulo".equals(cellReference.getPropertyId())) {
    				if (!modificado)
						return "cell-green";
					else
						return "cell-warning";
				} else {
						return "cell-normal";
				}
    		}
    	});
	}	

    private void verImagen()
    {
    	String archivo = null;
    	String ruta = null;
    	
    	CargaImagenes up = new CargaImagenes("basic",this,mapeo.getImagen());
    	getUI().addWindow(up);		
    }

    private void agregarImagen()
    {
    	String archivo = null;
    	String ruta = eBorsao.get().prop.baseImagePath;
    	
    	CargaImagenes up = new CargaImagenes("basic",this, ruta, archivo);
    	getUI().addWindow(up);		
    }
    
    public void recuperarImagen(InputStream r_is)
    {
    	if (r_is!=null)
    	{
    		mapeo.setImagen(r_is);
    		mapeo.setImg("S");
    		cans = new consultaArticulosNuevosServer(CurrentUser.get());
    		String rdo = cans.guardarCambios(mapeo);
    		if (rdo!=null)
    		{
    			/*
    			 * si falla al guardar anulamos la carga de la imagen
    			 */
        		mapeo.setImagen(null);
        		mapeo.setImg("N");    			
    		}
    	}
    	else
		{
    		mapeo.setImagen(null);
    		mapeo.setImg("N");
    		
    		cans = new consultaArticulosNuevosServer(CurrentUser.get());
    		String rdo = cans.guardarCambios(mapeo);
		}
    	
    	(this.vector).remove(mapeoOrig);
    	(this.vector).add(mapeo);
		refreshAllRows();
		scrollTo(mapeo);
		select(mapeo);

    }
}
