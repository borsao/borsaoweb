package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.PantallaMenuCtrlT;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.ArticulosNuevosGrid;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.EscandalloArticuloGrid;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosEscandallo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosNuevos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosProtocolo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.ProtocoloArticuloGrid;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server.consultaArticulosNuevosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ArticulosNuevosView extends GridViewRefresh{

	public static final String VIEW_NAME = "Articulos Nuevos";
	public String titulo = "";

	public View app = null;
	public Integer idRegistro = null;
	public OpcionesForm form = null;
	public boolean modificando = false;
	
	private VerticalLayout barAndGridLayout = null;

	public Label lblTitulo = null;
	private Button opcNuevo = null;
	private Button opcSalir = null;

	public Panel izquierda = null;
	private Panel derecha = null;

	private Panel panelArticulos = null;
	private  VerticalLayout verticalPanel=null; 
	public VerticalLayout abajo = null;

	public GridPropio gridArticulos = null;
	public Grid gridEscandallo = null;
	public Grid gridProtocolo = null;

	private String tipoArticulo = null;

	
	public String articuloSeleccionado = null;
	public String descripcionArticuloSeleccionado = null;
	public String articuloPadreSeleccionado = null;
	public String descripcionArticuloPadreSeleccionado = null;
	private boolean existe = false;
	private CheckBox chkTodos = null;
	
	/*
	 * METODOS PROPIOS PERO GENERICOS
	 */

	public ArticulosNuevosView() {
	}

	public void enter(ViewChangeEvent event) {
		app=this;
		this.setSizeFull();
		this.addStyleName("crud-view");
		this.setResponsive(true);

		this.cargarPantalla();
		this.cargarListeners();
		this.cargarPadres();
	}

	public void cargarPantalla() {

		setSizeFull();
		addStyleName("scrollable");

		this.barAndGridLayout = new VerticalLayout();
		this.barAndGridLayout.setStyleName("crud-main-layout");
		this.barAndGridLayout.setSizeUndefined();
		this.barAndGridLayout.setWidth("100%");

		this.topLayoutL = new HorizontalLayout();
		this.topLayoutL.setSpacing(true);
    	this.topLayoutL.setSizeUndefined();
    	this.topLayoutL.setMargin(true);
    	
			this.opcSalir = new Button("Salir");
			this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			this.opcSalir.setIcon(FontAwesome.POWER_OFF);
	
			this.opcNuevo = new Button("Nuevo");
			this.opcNuevo.addStyleName(ValoTheme.BUTTON_PRIMARY);
			this.opcNuevo.addStyleName(ValoTheme.BUTTON_TINY);
			this.opcNuevo.setIcon(FontAwesome.PLUS_CIRCLE);
	
			if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master")
					|| eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento())
							.equals("Compras")) {
				this.topLayoutL.addComponent(this.opcNuevo);
				this.topLayoutL.setComponentAlignment(this.opcNuevo, Alignment.BOTTOM_LEFT);
			}
		this.topLayoutL.addComponent(this.opcSalir);
		this.topLayoutL.setComponentAlignment(this.opcSalir, Alignment.BOTTOM_LEFT);

    	panelArticulos = new Panel("Lista Araticulos");
	    panelArticulos.setSizeUndefined();
	    panelArticulos.setVisible(true);
	    panelArticulos.addStyleName("showpointer");
	    panelArticulos.setWidth("100%");
	    new PanelCaptionBarToggler<Panel>( panelArticulos );

		this.izquierda = new Panel("Ecandallo");
		this.izquierda.setSizeFull();
		this.izquierda.setWidth("100%");
		this.izquierda.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		new PanelCaptionBarToggler<Panel>(this.izquierda);

		this.derecha = new Panel("Protocolo");
		this.derecha.setSizeFull();
		this.derecha.setWidth("100%");
		this.derecha.setHeight("100%");
		this.derecha.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		new PanelCaptionBarToggler<Panel>(this.derecha);

		this.cabLayout = new HorizontalLayout();
		
		this.barAndGridLayout.addComponent(this.topLayoutL);
		this.barAndGridLayout.addComponent(this.panelArticulos);
		this.barAndGridLayout.addComponent(this.izquierda);
		this.barAndGridLayout.addComponent(this.derecha);

		Panel pp = new Panel();
		pp.setSizeFull();
		pp.setContent(this.barAndGridLayout);

		pp.addActionHandler(new Handler() {

			Action action_0 = new ShortcutAction("ctrl t", ShortcutAction.KeyCode.T,
					new int[] { ShortcutAction.ModifierKey.CTRL, ShortcutAction.ModifierKey.ALT });

			@Override
			public void handleAction(Action action, Object sender, Object target) {

				if (action == action_0) {
					PantallaMenuCtrlT menu = new PantallaMenuCtrlT();
					getUI().addWindow(menu);
				}

			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[] { action_0 };
			}
		});

		this.addComponent(pp);
		this.setResponsive(true);
	}

	private void cargarListeners() {

		this.opcSalir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				eBorsao.get().mainScreen.menu.verMenu(true);
				destructor();
			}
		});

		this.opcNuevo.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				pantallaCreacionArticuloNuevo();
			}
		});

//		LayoutClickListener lcl = new LayoutClickListener() {
//
//			@Override
//			public void layoutClick(LayoutClickEvent event) {
//				Notificaciones.getInstance()
//						.mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
//			}
//		};
		// centralLayoutT1.addLayoutClickListener(lcl);

	}

	private void pantallaCreacionArticuloNuevo() {
		/*
		 * Nos servira para mostrar la pantalla de creacion de articulos nuevos
		 * Al cerrar la pantalla refrescará la lista de articulos para agregarlo
		 * y poder trabajar con el
		 */
		pantallaCreacionArticulo pt = new pantallaCreacionArticulo(this,"Creacion Articulos");
		getUI().addWindow(pt);
	}
	

	public void eliminarDashBoards(boolean r_todos) {

		if (gridEscandallo != null) {
			gridEscandallo.removeAllColumns();
			gridEscandallo = null;
		}

		if (gridProtocolo != null) {
			gridProtocolo.removeAllColumns();
			gridProtocolo = null;
		}

		if (r_todos) {
			gridArticulos = null;
//			panelArticulos = null;
		}
	}

	public void ejecutarPadres() {
		if (panelArticulos!=null) panelArticulos.getContent().setVisible(!panelArticulos.getContent().isVisible());
		
		cargarArticuloEscandallo();
		cargarArticuloProtocolo();
	}

	public void cargarArticuloEscandallo() 
	{
		
		/*
		 * Con el articulo padre buscare su escandallo
		 */
		
		/*
		 * POr si existe el grid lo anulo, para regenerarlo
		 */
		if (this.gridEscandallo!=null)
		{
			this.gridEscandallo.removeAllColumns();
			this.gridEscandallo= null;
		}
		
		/*
		 * si existe el articulo seleccionado o no generaremos de cero o recuperaremos datos de greensys
		 */
		consultaArticulosNuevosServer cans = consultaArticulosNuevosServer.getInstance(CurrentUser.get());
		MapeoArticulosEscandallo mapeoArticulosEscandallo = new MapeoArticulosEscandallo();
		mapeoArticulosEscandallo.setIdArticuloNuevo(idRegistro);
		
		ArrayList<MapeoArticulosEscandallo> vectorTareasEscandallo = cans.datosArticulosEscandalloGlobal(mapeoArticulosEscandallo);
			
		gridEscandallo = new EscandalloArticuloGrid(this, vectorTareasEscandallo);
		
		this.izquierda.setContent(gridEscandallo);

	}

	public void cargarArticuloProtocolo() 
	{
		ArrayList<MapeoArticulosProtocolo> vector = null;
		
		if (gridProtocolo!=null)
		{
			gridProtocolo.removeAllColumns();
			gridProtocolo = null;
		}
		
		consultaArticulosNuevosServer cans = consultaArticulosNuevosServer.getInstance(CurrentUser.get());
		MapeoArticulosProtocolo mapeoArticulosProtocolo = new MapeoArticulosProtocolo();
		mapeoArticulosProtocolo.setIdArticuloNuevo(idRegistro);

		vector = cans.datosArticulosProtocoloGlobal(mapeoArticulosProtocolo);
		
		gridProtocolo = new ProtocoloArticuloGrid(this, vector);
		
		this.derecha.setContent(gridProtocolo);
	}

	private void cargarPadres() 
	{
		this.chkTodos = null;

		this.verticalPanel = new VerticalLayout();
		this.verticalPanel.setSizeFull();
		this.verticalPanel.setSpacing(true);

		this.chkTodos = new CheckBox("Ver todos");
		
		this.chkTodos.addValueChangeListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				regenerarGridArticulos();
			}
		});
		
		this.cargarGridArticulos();
		this.verticalPanel.addComponent(this.chkTodos);
		this.verticalPanel.addComponent(this.gridArticulos);
		this.panelArticulos.setContent(this.verticalPanel);
	}

	private void regenerarGridArticulos()
	{
		if (this.gridArticulos!=null)
		{
			this.gridArticulos.removeAllColumns();
			if (this.verticalPanel!=null) this.verticalPanel.removeComponent(gridArticulos);
			this.gridArticulos=null;
		}
		this.cargarGridArticulos();
		this.verticalPanel.addComponent(this.gridArticulos);
		this.panelArticulos.setContent(this.verticalPanel);

	}
	private void cargarGridArticulos() 
	{
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		consultaArticulosNuevosServer cans = new consultaArticulosNuevosServer(CurrentUser.get());
		MapeoArticulosNuevos mapeo = new MapeoArticulosNuevos();
		
		if (!this.chkTodos.getValue())
		{
			//cargar solo los articulos nuevos en curso 
			mapeo.setActivoSN("S");
		}
		ArrayList<MapeoArticulosNuevos> vectorPadres = cans.datosArticulosNuevosGlobal(mapeo);
		
		gridArticulos = new ArticulosNuevosGrid("", this, vectorPadres);
		
	}

	private void vaciarPantalla() {
		izquierda = null;
		derecha = null;
		topLayout = null;

		barAndGridLayout = null;
		this.removeAllComponents();
	}

	private void comprobarArticulo(String r_articulo) {
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());

		if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103")) {
			tipoArticulo = cas.obtenerTipoArticulo(r_articulo);
			if (tipoArticulo != null) {
				switch (tipoArticulo) {
				case "PT": {
					this.articuloPadreSeleccionado = r_articulo.trim();
					this.articuloSeleccionado = null;
					this.descripcionArticuloSeleccionado = null;
					this.descripcionArticuloPadreSeleccionado = cas.obtenerDescripcionArticulo(r_articulo.trim());

					/*
					 * Comprobaremos si el articulo existe ya en el
					 * mantenimiento de articulos nuevos o no
					 */
					break;
				}
				case "PS": {
					this.articuloSeleccionado = r_articulo.trim();
					this.articuloPadreSeleccionado = null;
					this.descripcionArticuloSeleccionado = cas.obtenerDescripcionArticulo(r_articulo.trim());
					this.descripcionArticuloPadreSeleccionado = null;
					break;
				}
				default: {
					tipoArticulo = "";
					this.articuloSeleccionado = r_articulo.toUpperCase().trim();
					this.descripcionArticuloSeleccionado = null;
					this.articuloPadreSeleccionado = null;
					this.descripcionArticuloPadreSeleccionado = null;
					break;
				}
				}
			} else {
				tipoArticulo = "";
				this.articuloSeleccionado = r_articulo.toUpperCase().trim();
				this.descripcionArticuloSeleccionado = null;
				;
				this.articuloPadreSeleccionado = null;
				this.descripcionArticuloPadreSeleccionado = null;
			}
		} else if (r_articulo.length() > 0) {
			tipoArticulo = "";
			this.articuloSeleccionado = r_articulo.toUpperCase().trim();
			this.descripcionArticuloSeleccionado = null;
			this.articuloPadreSeleccionado = null;
			this.descripcionArticuloPadreSeleccionado = null;
		} else {
			Notificaciones.getInstance().mensajeError("Valor no válido");
			tipoArticulo = null;
			this.articuloSeleccionado = null;
			this.descripcionArticuloSeleccionado = null;
		}
	}

	private boolean isExiste() {
		return existe;
	}

	private void setExiste(boolean existe) {
		this.existe = existe;
	}

	/*
	 * METODOS RELATIVOS A LOS GRID
	 */

	public void filaSeleccionada (MapeoArticulosNuevos r_fila)
	{
		this.idRegistro=r_fila.getIdCodigo();
		this.articuloPadreSeleccionado=r_fila.getArticulo();
		this.descripcionArticuloPadreSeleccionado=r_fila.getDescripcion();
		if (panelArticulos!=null) panelArticulos.getContent().setVisible(true);
		ejecutarPadres();
		
	}
	
	
	public void filaSeleccionadaEscandallo(MapeoArticulosEscandallo r_fila) {
		// this.form.editarProgramacion((MapeoProgramacion) r_fila);
//		Notificaciones.getInstance().mensajeInformativo("Escandallo - Fila Seleccionada");

		if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master") || eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Compras"))
		{
    		this.modificando=true;
    		this.verForm(false);
	    	this.form.editarOpcion((MapeoArticulosEscandallo) r_fila);
		}	
	}

	public void filaSeleccionadaProtocolo(Object r_fila) {
		// this.form.editarProgramacion((MapeoProgramacion) r_fila);
		Notificaciones.getInstance().mensajeInformativo("Protocolo - Fila Seleccionada");
	}

	public void aceptarProceso(String r_accion)
	{
		if (r_accion.contentEquals("aceptarEliminar"))
		{
    		/*
    		 * llamaremos a eliminar el registro y sus asociados.
    		 */
    		consultaArticulosNuevosServer cans = new consultaArticulosNuevosServer(CurrentUser.get());
    		MapeoArticulosNuevos mapeo = new MapeoArticulosNuevos();
    		mapeo.setIdCodigo(idRegistro);
    		String rdo = cans.eliminar(mapeo);
    		this.eliminarDashBoards(true);
    		if (rdo==null)
    		{
        		/*
        		 * actualizamos pantalla
        		 */
        		cargarPadres();
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeAdvertencia("Error al Eliminar datos ");
    		}
		}
	}
	
	public void cancelarProceso(String r_accion)
	{
		if (r_accion.contentEquals("cancelarEliminar"))
		{
//			cargarPadres();
		}
	}
	
	public void aceptar(String r_articulo)
	{
		this.comprobarArticulo(r_articulo);
		this.eliminarDashBoards(true);
		this.cargarPadres();
	}
	
	public void cerrar()
	{
		Notificaciones.getInstance().mensajeInformativo("Cerrada pantalla creacion sin guardar");
	}
	
	public void destructor() {
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void reestablecerPantalla() {
		
	}

	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}

	@Override
	public void newForm() {
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.form.setCreacion(true);
    	this.modificando=false;
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
		
	}

	@Override
	public void eliminarRegistro() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void verForm(boolean r_busqueda) {
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		if (this.gridProtocolo!=null) this.gridProtocolo.setVisible(false);
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    	}
		
	}

	@Override
	public void filaSeleccionada(Object r_fila) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		// TODO Auto-generated method stub
		
	}

	public void cerrarForm()
	{
		if (this.gridProtocolo!=null) this.gridProtocolo.setVisible(true);
		this.regenerarGridArticulos();
	}
}
