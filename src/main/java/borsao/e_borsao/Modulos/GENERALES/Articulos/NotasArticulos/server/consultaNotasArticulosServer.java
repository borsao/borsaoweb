package borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaNotasArticulosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaNotasArticulosServer instance;
	
	public consultaNotasArticulosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaNotasArticulosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaNotasArticulosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoNotasArticulos> datosOpcionesGlobal(MapeoNotasArticulos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoNotasArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_notas_art.articulo not_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo not_des, ");
		cadenaSQL.append(" prd_notas_art.notas not_not, ");
		cadenaSQL.append(" prd_notas_art.caducidad not_cad ");		
     	cadenaSQL.append(" FROM prd_notas_art ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_notas_art.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_notas_art.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_notas_art.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoNotasArticulos>();
			
			while(rsOpcion.next())
			{
				MapeoNotasArticulos mapeoNotasArticulos= new MapeoNotasArticulos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoNotasArticulos.setArticulo(rsOpcion.getString("not_art"));
				mapeoNotasArticulos.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("not_des")));
				mapeoNotasArticulos.setNotas(rsOpcion.getString("not_not"));
				mapeoNotasArticulos.setCaducidad(rsOpcion.getDate("not_cad"));
				vector.add(mapeoNotasArticulos);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public boolean hayNotas(MapeoNotasArticulos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoNotasArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_notas_art.articulo not_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo not_des, ");
		cadenaSQL.append(" prd_notas_art.notas not_not, ");
		cadenaSQL.append(" prd_notas_art.caducidad not_cad ");		
     	cadenaSQL.append(" FROM prd_notas_art ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_notas_art.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_notas_art.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}

			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" (prd_notas_art.caducidad is null or prd_notas_art.caducidad = '' or prd_notas_art.caducidad >='" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "') ");

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_notas_art.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;			
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public String verNotas(String r_articulo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoNotasArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_notas_art.articulo not_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo not_des, ");
		cadenaSQL.append(" prd_notas_art.notas not_not, ");
		cadenaSQL.append(" prd_notas_art.caducidad not_cad ");		
     	cadenaSQL.append(" FROM prd_notas_art ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_notas_art.articulo ");

		try
		{
			cadenaWhere = new StringBuffer();
				
			cadenaWhere.append(" prd_notas_art.articulo = '" + r_articulo + "' ");

			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" (prd_notas_art.caducidad is null or prd_notas_art.caducidad = '' or caducidad >='" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "') ");

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_notas_art.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getString("not_not");			
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public ArrayList<MapeoNotasArticulos> verNotas(StringBuffer r_cadenaArticulos)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;
		
		ArrayList<MapeoNotasArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaSQL = new StringBuffer();

		
		cadenaSQL.append(" SELECT distinct prd_notas_art.articulo not_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo not_des, ");
		cadenaSQL.append(" prd_notas_art.notas not_not, ");
		cadenaSQL.append(" prd_notas_art.caducidad not_cad ");		
		cadenaSQL.append(" FROM prd_notas_art ");
		cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_notas_art.articulo ");
		
		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_cadenaArticulos.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_notas_art.articulo in " + r_cadenaArticulos.toString() + " ");
			}

			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" (prd_notas_art.caducidad is null or prd_notas_art.caducidad = '' or caducidad >='" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "') ");
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_notas_art.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		
			vector = new ArrayList<MapeoNotasArticulos>();
			while(rsOpcion.next())
			{
				MapeoNotasArticulos map = new MapeoNotasArticulos();
				map.setArticulo(rsOpcion.getString("not_art"));
				map.setDescripcion(rsOpcion.getString("not_des"));
				map.setNotas(rsOpcion.getString("not_not"));
				
				vector.add(map);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	
	public String guardarNuevo(MapeoNotasArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_notas_art ( ");
			cadenaSQL.append(" prd_notas_art.articulo, ");
			cadenaSQL.append(" prd_notas_art.notas, ");
    		cadenaSQL.append(" prd_notas_art.caducidad) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getNotas()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getNotas());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCaducidad()!=null)
		    {
		    	preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getCaducidad())));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoNotasArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_notas_art  SET ");
			cadenaSQL.append(" prd_notas_art.notas =?, ");
    		cadenaSQL.append(" prd_notas_art.caducidad=? ");
    		cadenaSQL.append(" WHERE prd_notas_art.articulo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getNotas()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getNotas());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCaducidad()!=null)
		    {
		    	preparedStatement.setString(2, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getCaducidad())));
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoNotasArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_notas_art ");            
			cadenaSQL.append(" WHERE prd_notas_art.articulo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}
}