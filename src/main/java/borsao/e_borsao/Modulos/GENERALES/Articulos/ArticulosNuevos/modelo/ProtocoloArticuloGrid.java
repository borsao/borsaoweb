package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server.consultaArticulosNuevosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.view.ArticulosNuevosView;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.MapeoEstadoTareasProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.server.consultaEstadoTareasProtocoloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ProtocoloArticuloGrid extends Grid {
	
	private ArticulosNuevosView app = null;
	private consultaArticulosNuevosServer cans = null;
	private MapeoArticulosProtocolo mapeo = null;
	private MapeoArticulosProtocolo mapeoOrig = null;
	private boolean actualizar = false;
	private ArrayList<MapeoArticulosProtocolo> vector = null;

	public ProtocoloArticuloGrid(ArticulosNuevosView r_App, ArrayList<MapeoArticulosProtocolo> r_vector) 
    {
    
    	this.app = r_App; 
    	
    	IndexedContainer container = null;
		Iterator<MapeoArticulosProtocolo> iterator = null;
		
		
		cans = consultaArticulosNuevosServer.getInstance(CurrentUser.get());
		MapeoArticulosProtocolo mapeoArticulosProtocolo = new MapeoArticulosProtocolo();
		

		container = new IndexedContainer();

		container.addContainerProperty("idCodigo", Integer.class, null);
		container.addContainerProperty("mail", String.class, null);
		container.addContainerProperty("estado", String.class, null);
		container.addContainerProperty("enviar", String.class, null);
		container.addContainerProperty("porc", String.class, null);
		container.addContainerProperty("responsable", String.class, null);
		container.addContainerProperty("tarea", String.class, null);
		container.addContainerProperty("notas", String.class, null);
		container.addContainerProperty("idTareaPro", Integer.class, null);
		container.addContainerProperty("idArticuloNuevo", Integer.class, null);

		mapeoArticulosProtocolo.setIdArticuloNuevo(this.app.idRegistro);
		vector = cans.datosArticulosProtocoloGlobal(mapeoArticulosProtocolo);
			
		iterator = vector.iterator();

		while (iterator.hasNext()) {
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);

			mapeoArticulosProtocolo = (MapeoArticulosProtocolo) iterator.next();

			file.getItemProperty("idCodigo").setValue(mapeoArticulosProtocolo.getIdCodigo());
			file.getItemProperty("mail").setValue(mapeoArticulosProtocolo.getMapeoTareasProtocolo().getMail());
			if (mapeoArticulosProtocolo.getMapeoEstadoTareasProtocolo().getPorcentaje().toString().contentEquals("999")) file.getItemProperty("estado").setValue(""); else file.getItemProperty("estado").setValue(mapeoArticulosProtocolo.getMapeoEstadoTareasProtocolo().getPorcentaje().toString());
			file.getItemProperty("idTareaPro").setValue(mapeoArticulosProtocolo.getIdTareaPro());
			file.getItemProperty("idArticuloNuevo").setValue(mapeoArticulosProtocolo.getIdArticuloNuevo());
			
			file.getItemProperty("enviar").setValue(null);
			if (mapeoArticulosProtocolo.getMapeoEstadoTareasProtocolo().getPorcentaje().toString().contentEquals("999")) file.getItemProperty("porc").setValue(""); else file.getItemProperty("porc").setValue(mapeoArticulosProtocolo.getMapeoEstadoTareasProtocolo().getPorcentaje().toString()+ "% ");
			file.getItemProperty("responsable").setValue(mapeoArticulosProtocolo.getMapeoTareasProtocolo().getResponsable());
			file.getItemProperty("tarea").setValue(mapeoArticulosProtocolo.getMapeoTareasProtocolo().getTarea());
			file.getItemProperty("notas").setValue(mapeoArticulosProtocolo.getNotas());
		}

		this.setContainerDataSource(container);
		this.setSelectionMode(SelectionMode.SINGLE);
		if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master") || eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Compras"))
		{
			this.setEditorEnabled(true);
		}
		this.setCaption("PROTOCOLO PROYECTOS NUEVOS");
		this.setSizeFull();
		this.setHeight("100%");
		this.addStyleName("smallgrid");
		this.setFrozenColumnCount(2);
		
		
		this.cargarListeners();
		this.asignarEstilos();

    }
    
    

	public void cargarListeners() 
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	Item item = getContainerDataSource().getItem(getEditedItemId());
	        	mapeoOrig = new MapeoArticulosProtocolo();
	        	mapeoOrig.setIdCodigo(new Integer(item.getItemProperty("idCodigo").getValue().toString()));
	        	mapeoOrig.setIdTareaPro(new Integer(item.getItemProperty("idTareaPro").getValue().toString()));
	        	mapeoOrig.setIdArticuloNuevo(new Integer(item.getItemProperty("idArticuloNuevo").getValue().toString()));
	        	
    			System.out.println("Estado - " + item.getItemProperty("estado").getValue().toString());
//    			System.out.println("Porc - " + item.getItemProperty("porc").getValue().toString());
		        
		        
	        	actualizar=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		Item item = getContainerDataSource().getItem(getEditedItemId());
	        		mapeo = new MapeoArticulosProtocolo();
	        		
		        	mapeo.setIdCodigo(mapeoOrig.getIdCodigo());
		        	mapeo.setIdTareaPro(mapeoOrig.getIdTareaPro());
		        	mapeo.setIdArticuloNuevo(mapeoOrig.getIdArticuloNuevo());
		        	mapeo.setNotas(null);
		        	
	        		if (mapeo.getIdCodigo()!=null)
		        	{
        				/*
        				 * en funcion del campo porc
        				 * 
        				 * 	obtener idEstado y almacenar en validacion
        				 * de paso rellenar el mapeoestadortareasprotocolo
        				 * 
        				 * llamar a guardar cambios tareas protocolo articulos nuevos
        				 */
        				
	        			consultaEstadoTareasProtocoloServer cetps = consultaEstadoTareasProtocoloServer.getInstance(CurrentUser.get());
	        			String estado = item.getItemProperty("porc").getValue().toString();
	        			
	        			Integer idEstadoTareaProtocolo = cetps.obenerIdEstadoTareasProtocolo(estado);
	        			mapeo.setValidacion(idEstadoTareaProtocolo);
	        			
	        			System.out.println("Estado - " + item.getItemProperty("estado").getValue().toString());
	        			System.out.println("Porc - " + item.getItemProperty("porc").getValue().toString());
//        				mapeo.setMapeoEstadoTareasProtocolo();
//        				
		        		String rdo = cans.guardarCambiosProtocolo(mapeo);
		        		
		        		if (rdo==null)
		        		{
		        			app.cargarArticuloProtocolo();
		        			app.cerrarForm();

//			        		((ArrayList<MapeoArticulosProtocolo>) vector).remove(mapeoOrig);
//			    			((ArrayList<MapeoArticulosProtocolo>) vector).add(mapeo);
	
		        		}
		        	}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		if (event.getPropertyId().toString().equals("enviar"))
            		{
    	        		Item item = getContainerDataSource().getItem(event.getItemId());
    	        		
    	        		String mail = item.getItemProperty("mail").getValue().toString();
    	        		String tareaRecordar = item.getItemProperty("tarea").getValue().toString();
    	        		String mensaje = "Tienes pendiente la tarea: " + tareaRecordar + " del proceso de articulos nuevos, referente al " + app.descripcionArticuloPadreSeleccionado + " ";

    	        		ClienteMail.getInstance().envioMailSincro(mail, null, "Proceso Articulos nuevos. Tarea Pendiente" , mensaje + new java.util.Date().toString());
    	        		
            		}
            	}
            }
    	});
    	
		this.addSelectionListener(new SelectionListener() {

			@Override
			public void select(SelectionEvent event) {

				Item file = getContainerDataSource().getItem(getSelectedRow());
				
//				app.filaSeleccionadaProtocolo(file);
			}
		});
		
	}
	
	private void asignarEstilos()
	{
		this.getColumn("porc").setWidth(100);
		this.getColumn("porc").setHeaderCaption("");
		this.getColumn("porc").setEditorField(getComboBox("El campo es obligatorio!"));
		
		this.getColumn("enviar").setWidth(50);
		this.getColumn("enviar").setHeaderCaption("");

		this.getColumn("idCodigo").setHidden(true);
		this.getColumn("mail").setHidden(true);
		this.getColumn("estado").setHidden(true);
		this.getColumn("idTareaPro").setHidden(true);
		this.getColumn("idArticuloNuevo").setHidden(true);
		this.getColumn("notas").setHidden(true);

		this.setCellDescriptionGenerator(new Grid.CellDescriptionGenerator() 
		{
			int completado = 0;

			@Override
			public String getDescription(CellReference cell) 
			{

				String descriptionText = null;
				if ("estado".equals(cell.getPropertyId())) 
				{
					if (cell.getValue() != null && cell.getValue().toString().length()>0) completado = new Integer(cell.getValue().toString().replace("%", "")); else completado =999;
				}
				
				if ("porc".equals(cell.getPropertyId())) {
					if (completado==0)
						descriptionText = "No Iniciada";
					else if (completado==999)
						descriptionText = "No Aplica";
					else descriptionText = "Competado";
				}
				if ("enviar".equals(cell.getPropertyId())) {
					descriptionText = "Enviar recordatorio al responsable";
				}

				return descriptionText;
			}
		});

		this.setCellStyleGenerator(new Grid.CellStyleGenerator() {
			boolean enviar = true;
			int completado = 0;

			public String getStyle(Grid.CellReference cellReference) {
				if ("mail".equals(cellReference.getPropertyId())) {
					if (cellReference.getValue() != null && cellReference.getValue().toString().length() > 0) {
						enviar = true;
					} else {
						enviar = false;
					}
				}

				if ("estado".equals(cellReference.getPropertyId())) {
					
					if (cellReference.getValue() != null && cellReference.getValue().toString().length()>0) completado = new Integer(cellReference.getValue().toString().replace("%", "")); else completado =999;
				}

				if ("enviar".equals(cellReference.getPropertyId())) 
				{
					if (enviar) 
					{
						return "nativebutton";
					} 
					else 
					{
						return "nativebuttonNo";
					}
				} 
				else if ("porc".equals(cellReference.getPropertyId())) 
				{
					if (completado==100) 
					{
						return "cell-green";
					}
					else if (completado == 999)
					{
						return "cell-normal";
					}
					else 
					{
						return "cell-error";
					}
				} 
				else 
				{
					return "cell-normal";
				}
			}
		});
	}

	
	private Field<?> getComboBox(String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();
		consultaEstadoTareasProtocoloServer cetps = consultaEstadoTareasProtocoloServer.getInstance(CurrentUser.get());
		ArrayList<MapeoEstadoTareasProtocolo> vec = cetps.datosEstadoTareasProtocoloGlobal(null);
		Iterator<MapeoEstadoTareasProtocolo> it = null;
		
		it = vec.iterator();
		
		for (int i = 0; i<vec.size();i++)
		{
			MapeoEstadoTareasProtocolo map = it.next();
			comboBox.addItem(map.getDescripcion());
		}

//		comboBox.addItem("No Aplica");
//		comboBox.addItem("No Iniciado");
//		comboBox.addItem("Completado");
		
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
//		comboBox.setStyleName("blanco");
		comboBox.setWidth("150");
		return comboBox;

	}

}
