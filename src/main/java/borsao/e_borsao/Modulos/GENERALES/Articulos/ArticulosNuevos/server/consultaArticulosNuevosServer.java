package borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.server;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosEscandallo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosNuevos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.ArticulosNuevos.modelo.MapeoArticulosProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.MapeoEstadoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.server.consultaEstadoTareasEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.MapeoEstadoTareasProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.server.consultaEstadoTareasProtocoloServer;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.MapeoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.server.consultaTareasEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.modelo.MapeoTareasProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.server.consultaTareasProtocoloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosNuevosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosNuevosServer instance;
	
	public consultaArticulosNuevosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosNuevosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosNuevosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoArticulosNuevos> datosArticulosNuevosGlobal(MapeoArticulosNuevos r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoArticulosNuevos> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoArticulosNuevos mapeoArticulosNuevos =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_articulos_nuevos.idCodigo an_id,");
		cadenaSQL.append(" prd_articulos_nuevos.codigo an_ar,");
		cadenaSQL.append(" prd_articulos_nuevos.descripcion an_des,");
		cadenaSQL.append(" prd_articulos_nuevos.version an_ver,");
		cadenaSQL.append(" prd_articulos_nuevos.fecha_alta an_fea,");
		cadenaSQL.append(" prd_articulos_nuevos.activo_sn an_act,");
		cadenaSQL.append(" prd_articulos_nuevos.modificacion_sn an_mod,");
		cadenaSQL.append(" prd_articulos_nuevos.imagen an_img,");
		cadenaSQL.append(" prd_articulos_nuevos.fecha_fabricacion an_fef ");
		cadenaSQL.append(" from prd_articulos_nuevos ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo= '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion like '%" + r_mapeo.getDescripcion() + "%' ");
				}
				if (r_mapeo.getVersion()!=null && r_mapeo.getVersion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" version = " + r_mapeo.getVersion() + " ");
				}
				if (r_mapeo.getActivoSN()!=null && r_mapeo.getActivoSN().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" activo_sn = '" + r_mapeo.getActivoSN() + "' ");
				}
				if (r_mapeo.getModificacionSN()!=null && r_mapeo.getModificacionSN().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" modificacion_sn = '" + r_mapeo.getModificacionSN() + "' ");
				}
				if (r_mapeo.getFechaFabricacion()!=null && !RutinasFechas.añoFecha(RutinasFechas.conversionDeString(r_mapeo.getFechaFabricacion())).contentEquals("9999") )
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha_fabricacion = " + RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaFabricacion()));
				}
				else
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha_fabricacion is null ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by codigo, version ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosNuevos>();
			
			while(rsOpcion.next())
			{
				mapeoArticulosNuevos = new MapeoArticulosNuevos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosNuevos.setIdCodigo(rsOpcion.getInt("an_id"));
				mapeoArticulosNuevos.setArticulo(rsOpcion.getString("an_ar"));
				mapeoArticulosNuevos.setDescripcion(rsOpcion.getString("an_des"));
				mapeoArticulosNuevos.setActivoSN(rsOpcion.getString("an_act"));
				mapeoArticulosNuevos.setModificacionSN(rsOpcion.getString("an_mod"));
				mapeoArticulosNuevos.setVersion(rsOpcion.getInt("an_ver"));
				mapeoArticulosNuevos.setFechaAlta(rsOpcion.getDate("an_fea"));
				mapeoArticulosNuevos.setFechaFabricacion(rsOpcion.getDate("an_fef"));
				mapeoArticulosNuevos.setPorcEsc(this.recuperarPorcentajeEscandallo(rsOpcion.getInt("an_id"))+"%");
				mapeoArticulosNuevos.setPorcProt(this.recuperarPorcentajeProtocolo(rsOpcion.getInt("an_id"))+"%");
				
				if (rsOpcion.getBlob("an_img")!=null)
				{
					Blob bytesImagen = rsOpcion.getBlob("an_img");
					InputStream is = bytesImagen.getBinaryStream();
					
					mapeoArticulosNuevos.setImagen(is);
					mapeoArticulosNuevos.setImg("S"); 
				}
				else
				{
					mapeoArticulosNuevos.setImagen(null);
					mapeoArticulosNuevos.setImg("N");
				}
				
				
				vector.add(mapeoArticulosNuevos);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoArticulosNuevos> recuperarArticuloVersion(MapeoArticulosNuevos r_mapeo)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere =null;
		MapeoArticulosNuevos mapeoArticulosNuevos =null;
		ArrayList<MapeoArticulosNuevos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_articulos_nuevos.idCodigo an_id,");
		cadenaSQL.append(" prd_articulos_nuevos.codigo an_ar,");
		cadenaSQL.append(" prd_articulos_nuevos.descripcion an_des,");
		cadenaSQL.append(" prd_articulos_nuevos.fecha_alta an_fea,");
		cadenaSQL.append(" prd_articulos_nuevos.fecha_fabricacion an_fef,");
		cadenaSQL.append(" prd_articulos_nuevos.version an_ver");
		cadenaSQL.append(" from prd_articulos_nuevos ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo= '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" group by idCodigo, codigo, descripcion, fecha_alta, fecha_fabricacion, version ");
			cadenaSQL.append(" order by codigo, version ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			vector = new ArrayList<MapeoArticulosNuevos>();
			while(rsOpcion.next())
			{
				mapeoArticulosNuevos = new MapeoArticulosNuevos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosNuevos.setIdCodigo(rsOpcion.getInt("an_id"));
				mapeoArticulosNuevos.setArticulo(rsOpcion.getString("an_ar"));
				mapeoArticulosNuevos.setDescripcion(rsOpcion.getString("an_des"));
				mapeoArticulosNuevos.setVersion(rsOpcion.getInt("an_ver"));
				mapeoArticulosNuevos.setFechaAlta(rsOpcion.getDate("an_fea"));
				mapeoArticulosNuevos.setFechaFabricacion(rsOpcion.getDate("an_fef"));
				vector.add(mapeoArticulosNuevos);	
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public String imprimirTarea(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_articulos_nuevos.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_articulos_nuevos ");
		try
		{
			
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	private Integer obtenerSiguienteProtocolo()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_protocolo_an.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_tareas_protocolo_an ");
		try
		{
			
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	
	private Integer obtenerSiguienteEscandallo()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_escandallo_an.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_tareas_escandallo_an ");
		try
		{
			
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public Integer obtenerSiguienteVersion(MapeoArticulosNuevos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_articulos_nuevos.version) an_ver_sig");
     	cadenaSQL.append(" FROM prd_articulos_nuevos ");
		try
		{
     	
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo= '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion like '%" + r_mapeo.getDescripcion() + "%' ");
				}
			}

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("an_ver_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public String guardarNuevo(MapeoArticulosNuevos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String rdo = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_articulos_nuevos( ");
			cadenaSQL.append(" prd_articulos_nuevos.idCodigo,");			
			cadenaSQL.append(" prd_articulos_nuevos.codigo, ");
			cadenaSQL.append(" prd_articulos_nuevos.descripcion, ");
			cadenaSQL.append(" prd_articulos_nuevos.fecha_alta, ");
			cadenaSQL.append(" prd_articulos_nuevos.fecha_fabricacion, ");
			cadenaSQL.append(" prd_articulos_nuevos.modificacion_sn, ");
			cadenaSQL.append(" prd_articulos_nuevos.version ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguiente());
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getFechaAlta()!=null)
		    {
		    	preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaAlta()));
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getFechaFabricacion()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaFabricacion()));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getModificacionSN()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getModificacionSN());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVersion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getVersion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 1);
		    }
	        preparedStatement.executeUpdate();
	        
	        if (r_mapeo.getVersion()!=null && r_mapeo.getVersion()>1)
	        {
	        	r_mapeo.setVersion(r_mapeo.getVersion()-1);
	        	rdo = this.caducarVersionAnterior(r_mapeo);
	        }
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }
		return rdo;
	}
	
	private String caducarVersionAnterior(MapeoArticulosNuevos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE prd_articulos_nuevos ");
			cadenaSQL.append(" SET prd_articulos_nuevos.activo_sn = ? ");
			cadenaSQL.append(" WHERE prd_articulos_nuevos.codigo = ? ");
			cadenaSQL.append(" AND prd_articulos_nuevos.version = ? ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N");
		    preparedStatement.setString(2, r_mapeo.getArticulo());
		    preparedStatement.setInt(3, r_mapeo.getVersion());
		    
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	
	public String guardarNuevoProtocolo(Integer r_idArticuloNuevo)
	{
		PreparedStatement preparedStatement = null;
		Iterator<MapeoTareasProtocolo> iterator = null;
		MapeoTareasProtocolo mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{

			consultaEstadoTareasProtocoloServer cetps = consultaEstadoTareasProtocoloServer.getInstance(CurrentUser.get());
			Integer idEstadoTareaProtocolo = cetps.obenerIdEstadoTareasProtocolo("No Iniciado");
			
			consultaTareasProtocoloServer ctes = consultaTareasProtocoloServer.getInstance(CurrentUser.get());
			ArrayList<MapeoTareasProtocolo> vectorTareasProtocolo = ctes.datosTareasProtocoloGlobal(null);

			iterator = vectorTareasProtocolo.iterator();

			while (iterator.hasNext()) 
			{
				mapeo = (MapeoTareasProtocolo) iterator.next();
				
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" INSERT INTO prd_tareas_protocolo_an( ");
				cadenaSQL.append(" prd_tareas_protocolo_an.idCodigo,");			
				cadenaSQL.append(" prd_tareas_protocolo_an.validacion, ");
				cadenaSQL.append(" prd_tareas_protocolo_an.notas, ");
				cadenaSQL.append(" prd_tareas_protocolo_an.idTareaPro, ");
				cadenaSQL.append(" prd_tareas_protocolo_an.idArticuloNuevo ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?) ");
				
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    preparedStatement.setInt(1, this.obtenerSiguienteProtocolo());
			    preparedStatement.setInt(2, idEstadoTareaProtocolo);
			    preparedStatement.setString(3, null);
			    preparedStatement.setInt(4, mapeo.getIdCodigo());
			    preparedStatement.setInt(5, r_idArticuloNuevo);
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarNuevoEscandallo(Integer r_idArticuloNuevo, String r_articuloGreensys)
	{
		PreparedStatement preparedStatement = null;
		Iterator<MapeoTareasEscandallo> iterator = null;
		MapeoTareasEscandallo mapeo = null;
		StringBuffer cadenaSQL = null;
		
		ArrayList<MapeoLineasEscandallo> vectorGreensys =null;
		ArrayList<MapeoTareasEscandallo> vectorTareasEscandallo = null;

		Integer idEstadoTareaEscandallo = null;
		String articulo =null;
		String descripcion =null;
		try
		{

			consultaTareasEscandalloServer ctes = consultaTareasEscandalloServer.getInstance(CurrentUser.get());
			consultaEstadoTareasEscandalloServer cetps = consultaEstadoTareasEscandalloServer.getInstance(CurrentUser.get());
			vectorTareasEscandallo = ctes.datosTareasEscandalloGlobal(null);
			
			if (r_articuloGreensys!=null)
			{
				idEstadoTareaEscandallo = cetps.obtenerIdEstadoTareaEscandallo("No Aplica");
				
				consultaEscandalloServer cas = consultaEscandalloServer.getInstance(CurrentUser.get());
				vectorGreensys = cas.recuperarLineasEscandallo(r_articuloGreensys);
			}
			else
			{
				idEstadoTareaEscandallo = cetps.obtenerIdEstadoTareaEscandallo("Esperar Ok Especificaciones");
			}
			

			cadenaSQL = new StringBuffer();  
			iterator = vectorTareasEscandallo.iterator();

			while (iterator.hasNext()) 
			{
				mapeo = (MapeoTareasEscandallo) iterator.next();
				
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" INSERT INTO prd_tareas_escandallo_an( ");
				cadenaSQL.append(" prd_tareas_escandallo_an.idCodigo,");
				cadenaSQL.append(" prd_tareas_escandallo_an.codigo,");
				cadenaSQL.append(" prd_tareas_escandallo_an.descripcion,");
				cadenaSQL.append(" prd_tareas_escandallo_an.estado, ");
				cadenaSQL.append(" prd_tareas_escandallo_an.notas_prod, ");
				cadenaSQL.append(" prd_tareas_escandallo_an.notas_plan, ");
				cadenaSQL.append(" prd_tareas_escandallo_an.idTareaEsc, ");
				cadenaSQL.append(" prd_tareas_escandallo_an.idArticuloNuevo ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
				
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    preparedStatement.setInt(1, this.obtenerSiguienteEscandallo());
			    if (r_articuloGreensys==null)
			    {
			    	preparedStatement.setString(2, null);//codigo articulo
			    	preparedStatement.setString(3, null);//descripcion
			    }
			    else
			    {
			    	if (vectorGreensys!=null && vectorGreensys.size()>0)
			    	{
				    	String[] rdo = recuperarArticulo(mapeo.getIdCodigo(), vectorGreensys);
				    	
				    	if (rdo!=null)
				    	{
					    	preparedStatement.setString(2, rdo[0]);//codigo articulo
					    	preparedStatement.setString(3, rdo[1]);//descripcion
				    	}
				    	else
				    	{
					    	preparedStatement.setString(2, null);//codigo articulo
					    	preparedStatement.setString(3, null);//descripcion			    						    		
				    	}
			    	}
			    	else
			    	{
				    	preparedStatement.setString(2, null);//codigo articulo
				    	preparedStatement.setString(3, null);//descripcion			    		
			    	}
			    }
			    preparedStatement.setInt(4, idEstadoTareaEscandallo);
			    preparedStatement.setString(5, "");//notas produccion
			    preparedStatement.setString(6, "");//notas planificacion
			    preparedStatement.setInt(7, mapeo.getIdCodigo());
			    preparedStatement.setInt(8, r_idArticuloNuevo);
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	private String[] recuperarArticulo(Integer r_id, ArrayList<MapeoLineasEscandallo> r_vector)
	{
		String[] rdo = null;
		String valor =null;
		MapeoLineasEscandallo mapeo = null;
		
		for (int i = 0; i<r_vector.size();i++)
		{
			mapeo = r_vector.get(i);
			
			if (r_id==1) //granel
			{
				if (mapeo.getArticulo().startsWith("0101"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
			}
			else if (r_id==2) //botella
			{
				if (mapeo.getArticulo().startsWith("0104"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
			}
			else if (r_id==3) //etiqueta
			{
				if (mapeo.getArticulo().startsWith("0106"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
				
			}
			else if (r_id==4) //contra
			{
				if (mapeo.getArticulo().startsWith("0107"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
				
			}
			else if (r_id==5) // tapon - corcho
			{
				if (mapeo.getArticulo().startsWith("0105")) 
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
				
			}
			else if (r_id==6) //capsula
			{
				if (mapeo.getArticulo().startsWith("0108"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
				
			}
			else if (r_id==7) // caja
			{
				if (mapeo.getArticulo().startsWith("0109"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
				
			}
			else if (r_id==8) // palet
			{
				if (mapeo.getArticulo().startsWith("0119"))
				{
					valor= mapeo.getArticulo().trim() + "@" + mapeo.getDescripcion();
					break;
				}
			}
			else if (r_id==9)
			{
				valor = "@CREAR CODIGO, ESCANDALLO Y OBS. INTRANET";
				break;
			}
		}		
		
		if (valor!=null) rdo=valor.split("@"); else rdo = null;
		return rdo;
	}
	public String guardarCambiosProtocolo(MapeoArticulosProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Iterator<MapeoTareasProtocolo> iterator = null;
		MapeoTareasProtocolo mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" UPDATE prd_tareas_protocolo_an SET ");
			cadenaSQL.append(" prd_tareas_protocolo_an.validacion = ? ");
			cadenaSQL.append(" where prd_tareas_protocolo_an.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getValidacion());
		    preparedStatement.setInt(2, r_mapeo.getIdCodigo());

		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarCambiosEscandallo(MapeoArticulosEscandallo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Iterator<MapeoTareasEscandallo> iterator = null;
		MapeoTareasEscandallo mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" UPDATE prd_tareas_escandallo_an SET ");
			cadenaSQL.append(" prd_tareas_escandallo_an.notas_plan = ?, ");
			cadenaSQL.append(" prd_tareas_escandallo_an.notas_prod = ?, ");
			cadenaSQL.append(" prd_tareas_escandallo_an.codigo= ?, ");
			cadenaSQL.append(" prd_tareas_escandallo_an.descripcion= ?, ");
			cadenaSQL.append(" prd_tareas_escandallo_an.estado = ?, ");
			cadenaSQL.append(" prd_tareas_escandallo_an.modificar = ? ");
			cadenaSQL.append(" where prd_tareas_escandallo_an.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getNotasPlan());
		    preparedStatement.setString(2, r_mapeo.getNotasProd());
		    preparedStatement.setString(3, r_mapeo.getCodigoArticulo());
		    preparedStatement.setString(4, r_mapeo.getDescripcionArticulo());
		    preparedStatement.setString(5, r_mapeo.getEstado());
		    if (r_mapeo.isModificar()) preparedStatement.setString(6, "S"); else preparedStatement.setString(6, "N"); 
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());

		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarCambios(MapeoArticulosNuevos r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_articulos_nuevos set ");
		    cadenaSQL.append(" prd_articulos_nuevos.codigo =?,");
		    cadenaSQL.append(" prd_articulos_nuevos.descripcion =?,");		    
		    cadenaSQL.append(" prd_articulos_nuevos.fecha_fabricacion =?, ");
		    cadenaSQL.append(" prd_articulos_nuevos.imagen = ?");
			cadenaSQL.append(" where prd_articulos_nuevos.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getFechaFabricacion()!=null)
		    {
		    	preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaFabricacion()));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getImagen()!=null)
		    {
		    	preparedStatement.setBlob(4, r_mapeo.getImagen());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public String eliminar(MapeoArticulosNuevos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		try
		{
			/*
			 * borrar sus tareas protocolo
			 */
			
			cadenaSQL.append(" DELETE FROM prd_tareas_protocolo_an ");            
			cadenaSQL.append(" WHERE prd_tareas_protocolo_an.idArticuloNuevo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();

			/*
			 * borrar sus tareas escandallo
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_tareas_escandallo_an ");            
			cadenaSQL.append(" WHERE prd_tareas_escandallo_an.idArticuloNuevo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();

			/*
			 * borrar definicion articulo nuevo
			 */			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_articulos_nuevos ");            
			cadenaSQL.append(" WHERE prd_articulos_nuevos.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			rdo = ex.getMessage();
		}
		return rdo;
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoArticulosNuevos r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/TareasEscandallo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("TareasEscandallo.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("TareasEscandallo");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
	
	
	public ArrayList<MapeoArticulosProtocolo> datosArticulosProtocoloGlobal(MapeoArticulosProtocolo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoArticulosProtocolo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoArticulosProtocolo mapeoArticulosProtocolo =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_tareas_protocolo_an.idCodigo ap_id,");
		cadenaSQL.append(" prd_tareas_protocolo_an.notas ap_no,");
		cadenaSQL.append(" prd_tareas_protocolo_an.idTareaPro ap_idt,");
		cadenaSQL.append(" prd_tareas_protocolo_an.idArticuloNuevo ap_ida,");
		cadenaSQL.append(" prd_estados_protocolo.descripcion ep_des,");
		cadenaSQL.append(" prd_estados_protocolo.porc ep_porc,");
		cadenaSQL.append(" prd_tareas_protocolo.responsable tp_res,");
		cadenaSQL.append(" prd_tareas_protocolo.mail tp_mail,");
		cadenaSQL.append(" prd_tareas_protocolo.tarea tp_tarea ");
		
		cadenaSQL.append(" from prd_tareas_protocolo_an ");
		cadenaSQL.append(" inner join prd_estados_protocolo on prd_estados_protocolo.idCodigo =  prd_tareas_protocolo_an.validacion ");
		cadenaSQL.append(" inner join prd_tareas_protocolo on prd_tareas_protocolo.idCodigo =  prd_tareas_protocolo_an.idTareaPro ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdArticuloNuevo()!=null && r_mapeo.getIdArticuloNuevo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idArticuloNuevo = " + r_mapeo.getIdArticuloNuevo());
				}

			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_tareas_protocolo.orden asc ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosProtocolo>();
			
			while(rsOpcion.next())
			{
				mapeoArticulosProtocolo = new MapeoArticulosProtocolo();
				MapeoEstadoTareasProtocolo mapeoEstadoTareasProtocolo = new MapeoEstadoTareasProtocolo();
				MapeoTareasProtocolo mapeoTareasProtocolo = new MapeoTareasProtocolo();
				
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosProtocolo.setIdCodigo(rsOpcion.getInt("ap_id"));
				mapeoArticulosProtocolo.setNotas(rsOpcion.getString("ap_no"));
				mapeoArticulosProtocolo.setIdArticuloNuevo(rsOpcion.getInt("ap_ida"));
				mapeoArticulosProtocolo.setIdTareaPro(rsOpcion.getInt("ap_idt"));
				
				mapeoEstadoTareasProtocolo.setDescripcion(rsOpcion.getString("ep_des"));
				mapeoEstadoTareasProtocolo.setPorcentaje(rsOpcion.getInt("ep_porc"));
				
				mapeoTareasProtocolo.setResponsable(rsOpcion.getString("tp_res"));
				mapeoTareasProtocolo.setMail(rsOpcion.getString("tp_mail"));
				mapeoTareasProtocolo.setTarea(rsOpcion.getString("tp_tarea"));
				
				mapeoArticulosProtocolo.setMapeoEstadoTareasProtocolo(mapeoEstadoTareasProtocolo);
				mapeoArticulosProtocolo.setMapeoTareasProtocolo(mapeoTareasProtocolo);
				
				vector.add(mapeoArticulosProtocolo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoArticulosEscandallo> datosArticulosEscandalloGlobal(MapeoArticulosEscandallo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoArticulosEscandallo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoArticulosEscandallo mapeoArticulosEscandallo =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_tareas_escandallo_an.idCodigo ap_id,");
		cadenaSQL.append(" prd_tareas_escandallo_an.codigo ap_cod,");
		cadenaSQL.append(" prd_tareas_escandallo_an.descripcion ap_des,");
		cadenaSQL.append(" prd_tareas_escandallo_an.notas_prod ap_nopr,");
		cadenaSQL.append(" prd_tareas_escandallo_an.notas_plan ap_nopl,");
		cadenaSQL.append(" prd_tareas_escandallo_an.idTareaEsc ap_idt,");
		cadenaSQL.append(" prd_tareas_escandallo_an.estado ap_es,");
		cadenaSQL.append(" prd_tareas_escandallo_an.modificar ap_mdf,");
		cadenaSQL.append(" prd_tareas_escandallo_an.idArticuloNuevo ap_ida,");
		cadenaSQL.append(" prd_estados_escandallo.descripcion et_des,");
		cadenaSQL.append(" prd_estados_escandallo.porc et_porc,");
		cadenaSQL.append(" prd_tareas_escandallo.tarea te_tarea, ");
		cadenaSQL.append(" prd_articulos_nuevos.modificacion_sn ar_mod ");
		
		
		cadenaSQL.append(" from prd_tareas_escandallo_an ");
		cadenaSQL.append(" inner join prd_articulos_nuevos on prd_articulos_nuevos.idCodigo =  prd_tareas_escandallo_an.idArticuloNuevo ");
		cadenaSQL.append(" inner join prd_estados_escandallo on prd_estados_escandallo.idCodigo =  prd_tareas_escandallo_an.estado ");
		cadenaSQL.append(" inner join prd_tareas_escandallo on prd_tareas_escandallo.idCodigo =  prd_tareas_escandallo_an.idTareaEsc ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdArticuloNuevo()!=null && r_mapeo.getIdArticuloNuevo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idArticuloNuevo = " + r_mapeo.getIdArticuloNuevo());
				}

			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_tareas_escandallo.orden asc ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosEscandallo>();
			
			while(rsOpcion.next())
			{
				mapeoArticulosEscandallo = new MapeoArticulosEscandallo();
				MapeoEstadoTareasEscandallo mapeoEstadoTareasEscandallo = new MapeoEstadoTareasEscandallo();
				MapeoTareasEscandallo mapeoTareasEscandallo = new MapeoTareasEscandallo();
				
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosEscandallo.setIdCodigo(rsOpcion.getInt("ap_id"));
				mapeoArticulosEscandallo.setNotasPlan(rsOpcion.getString("ap_nopl"));
				mapeoArticulosEscandallo.setNotasProd(rsOpcion.getString("ap_nopr"));
				mapeoArticulosEscandallo.setEstado(rsOpcion.getString("ap_es"));
				mapeoArticulosEscandallo.setCodigoArticulo(rsOpcion.getString("ap_cod"));
				mapeoArticulosEscandallo.setDescripcionArticulo(rsOpcion.getString("ap_des"));
				mapeoArticulosEscandallo.setIdArticuloNuevo(rsOpcion.getInt("ap_ida"));
				mapeoArticulosEscandallo.setIdTareaEsc(rsOpcion.getInt("ap_idt"));
				mapeoArticulosEscandallo.setEstadoDes(rsOpcion.getString("et_des"));
				mapeoArticulosEscandallo.setPorc(rsOpcion.getString("et_porc"));
				if (rsOpcion.getString("ap_mdf").contentEquals("S")) mapeoArticulosEscandallo.setModificar(true); else mapeoArticulosEscandallo.setModificar(false);
				mapeoEstadoTareasEscandallo.setDescripcion(rsOpcion.getString("et_des"));
				mapeoEstadoTareasEscandallo.setPorcentaje(rsOpcion.getInt("et_porc"));
				mapeoArticulosEscandallo.setModificacion(rsOpcion.getString("ar_mod"));
				mapeoTareasEscandallo.setTarea(rsOpcion.getString("te_tarea"));
				
				mapeoArticulosEscandallo.setMapeoEstadoTareasEscandallo(mapeoEstadoTareasEscandallo);
				mapeoArticulosEscandallo.setMapeoTareasEscandallo(mapeoTareasEscandallo);
				
				vector.add(mapeoArticulosEscandallo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private String recuperarPorcentajeEscandallo(Integer r_idCodigoArticuloNuevo)
	{
		Iterator<MapeoArticulosEscandallo> iter = null;
		MapeoArticulosEscandallo mapeo = new MapeoArticulosEscandallo();
		mapeo.setIdArticuloNuevo(r_idCodigoArticuloNuevo);
		ArrayList<MapeoArticulosEscandallo> vector = this.datosArticulosEscandalloGlobal(mapeo);
		iter = vector.iterator();
		Integer porc = 0;
		Integer cuantos = 0;
		Integer porcentajeFinal = 0;
		
		while (iter.hasNext())
		{
			MapeoArticulosEscandallo map = new MapeoArticulosEscandallo();
			map = iter.next();
			if (map.getMapeoEstadoTareasEscandallo().getPorcentaje()!=null)
			{
				cuantos = cuantos + 1;
				if (map.getMapeoEstadoTareasEscandallo().getPorcentaje()!=999) porc = porc + new Integer(map.getMapeoEstadoTareasEscandallo().getPorcentaje());
			}
		}
		
		if (porc>0 && cuantos >0) porcentajeFinal=porc/cuantos; else porcentajeFinal=0;
		return porcentajeFinal.toString();
	}
	
	private String recuperarPorcentajeProtocolo(Integer r_idCodigoArticuloNuevo)
	{
		Iterator<MapeoArticulosProtocolo> iter = null;
		MapeoArticulosProtocolo mapeo = new MapeoArticulosProtocolo();
		mapeo.setIdArticuloNuevo(r_idCodigoArticuloNuevo);
		ArrayList<MapeoArticulosProtocolo> vector = this.datosArticulosProtocoloGlobal(mapeo);
		iter = vector.iterator();
		Integer porc = 0;
		Integer cuantos = 0;
		Integer porcentajeFinal = 0;
		
		while (iter.hasNext())
		{
			MapeoArticulosProtocolo map = new MapeoArticulosProtocolo();
			map = iter.next();
			if (map.getMapeoEstadoTareasProtocolo().getPorcentaje()!=null && map.getMapeoEstadoTareasProtocolo().getPorcentaje()!=999)
			{
				cuantos = cuantos + 1;
				porc = porc + new Integer(map.getMapeoEstadoTareasProtocolo().getPorcentaje());
			}
		}
		
		if (porc>0 && cuantos >0) porcentajeFinal=porc/cuantos; else porcentajeFinal=0;
		return porcentajeFinal.toString();
	}

}