package borsao.e_borsao.Modulos.GENERALES.proveedores.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;

public class consultaProveedoresServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaProveedoresServer instance;
	
	public consultaProveedoresServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProveedoresServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProveedoresServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerNombreProveedor(Integer r_proveedor)
	{
		String rdo = null;
		ResultSet rsProveedores = null;
		
		String sql = null;
		try
		{
			sql="select proveedor, nombre_comercial from e_provee where proveedor =" + r_proveedor ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsProveedores= cs.executeQuery(sql);
			while(rsProveedores.next())
			{
				rdo=rsProveedores.getString("nombre_comercial");								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return rdo;
	}	
	
	public boolean comprobarProveedor(String r_proveedor)
	{
		String rdo = null;
		ResultSet rsProveedores = null;
		
		String sql = null;
		try
		{
			sql="select proveedor, nombre_comercial from e_provee where proveedor =" +  new Integer(r_proveedor.trim()) ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsProveedores= cs.executeQuery(sql);
			while(rsProveedores.next())
			{
				return true;								
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}	
	
}
