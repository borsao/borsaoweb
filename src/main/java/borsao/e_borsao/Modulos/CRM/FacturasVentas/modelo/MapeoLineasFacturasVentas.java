package borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo;

import java.util.Date;

public class MapeoLineasFacturasVentas
{
	private Integer idCodigo;
	private String clave_tabla;
	private String documento;
	private String serie;
	private Integer idCodigoFactura;
	private Integer ejercicio;
	private Integer posicion;
	private Integer linea;
	private String articulo;
	private String descripcion;
	private Double cantidad;
	private Double unidades;
	private Integer bultos;
	private Integer etiquetas;
	private String ubicacion;

	private Double precio;
	private Double descuento;
	private Double importe;
	private String moneda;
	private Double importeLocal;
    private Date fechaDocumento;
	private Integer cliente;
	private String nombreCliente;
	private String referencia;

	public MapeoLineasFacturasVentas()
	{
	}


	public Integer getIdCodigoFactura() {
		return idCodigoFactura;
	}


	public void setIdCodigoFactura(Integer idCodigoFactura) {
		this.idCodigoFactura = idCodigoFactura;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}



	public Integer getPosicion() {
		return posicion;
	}


	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}


	public Integer getLinea() {
		return linea;
	}


	public void setLinea(Integer linea) {
		this.linea = linea;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Double getCantidad() {
		return cantidad;
	}


	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}


	public Double getUnidades() {
		return unidades;
	}


	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public Double getDescuento() {
		return descuento;
	}


	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}


	public Double getImporte() {
		return importe;
	}


	public void setImporte(Double importe) {
		this.importe = importe;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public Double getImporteLocal() {
		return importeLocal;
	}


	public void setImporteLocal(Double importeLocal) {
		this.importeLocal = importeLocal;
	}


	public Integer getIdCodigo() {
		return idCodigo;
	}


	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}


	
	public Date getFechaDocumento() {
		return fechaDocumento;
	}


	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}


	public String getClave_tabla() {
		return clave_tabla;
	}


	public void setClave_tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public Integer getCliente() {
		return cliente;
	}


	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}


	public String getNombreCliente() {
		return nombreCliente;
	}


	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}


	public Integer getBultos() {
		return bultos;
	}


	public void setBultos(Integer bultos) {
		this.bultos = bultos;
	}


	public Integer getEtiquetas() {
		return etiquetas;
	}


	public void setEtiquetas(Integer etiquetas) {
		this.etiquetas = etiquetas;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	public String getReferencia() {
		return referencia;
	}


	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

}