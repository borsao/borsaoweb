package borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo;

public class MapeoResultadosFacturasVentas
{
	private Integer idCodigo;
	private String clave_Tabla;
	private String documento;
	private String serie;
	private Integer idCodigoFactura;
	private Double importeBruto;
	private Double importeBrutoLocal;
	
	private Double baseDto1;
	private Double baseDto2;
	private Double porcentajeDto1;
	private Double porcentajeDto2;
	private Double importeDto1;
	private Double importeDto2;
	
	private Double base;
	private Double cuota;
	private Double porcentaje;
	private Double base2;
	private Double cuota2;
	private Double porcentaje2;
	private Double baseRE;
	private Double cuotaRE;
	private Double porcentajeRE;
	
	private Double totalFactura;
	private Double totalFacturaLocal;


	public MapeoResultadosFacturasVentas()
	{
		this.setImporteBruto(new Double(0));
		this.setImporteBrutoLocal(new Double(0));
		
		this.setBaseDto1(new Double(0));
		this.setPorcentajeDto1(new Double(0));
		this.setImporteDto1(new Double(0));
		this.setBaseDto2(new Double(0));
		this.setPorcentajeDto2(new Double(0));
		this.setImporteDto2(new Double(0));

		this.setBase(new Double(0));
		this.setPorcentaje(new Double(0));
		this.setCuota(new Double(0));
		this.setBaseRE(new Double(0));
		this.setPorcentajeRE(new Double(0));
		this.setCuotaRE(new Double(0));

		this.setBase2(new Double(0));
		this.setPorcentaje2(new Double(0));
		this.setCuota2(new Double(0));
		this.setTotalFactura(new Double(0));
		this.setTotalFacturaLocal(new Double(0));
	}

	public Integer getIdCodigo() {
		return idCodigo;
	}

	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}

	public String getClave_Tabla() {
		return clave_Tabla;
	}

	public void setClave_Tabla(String clave_Tabla) {
		this.clave_Tabla = clave_Tabla;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getIdCodigoFactura() {
		return idCodigoFactura;
	}

	public void setIdCodigoFactura(Integer idCodigoFactura) {
		this.idCodigoFactura = idCodigoFactura;
	}

	public Double getImporteBruto() {
		return importeBruto;
	}

	public void setImporteBruto(Double importeBruto) {
		this.importeBruto = importeBruto;
	}

	public Double getImporteBrutoLocal() {
		return importeBrutoLocal;
	}

	public void setImporteBrutoLocal(Double importeBrutoLocal) {
		this.importeBrutoLocal = importeBrutoLocal;
	}

	public Double getBase() {
		return base;
	}

	public void setBase(Double base) {
		this.base = base;
	}

	public Double getCuota() {
		return cuota;
	}

	public void setCuota(Double cuota) {
		this.cuota = cuota;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Double getTotalFactura() {
		return totalFactura;
	}

	public void setTotalFactura(Double totalFactura) {
		this.totalFactura = totalFactura;
	}

	public Double getTotalFacturaLocal() {
		return totalFacturaLocal;
	}

	public void setTotalFacturaLocal(Double totalFacturaLocal) {
		this.totalFacturaLocal = totalFacturaLocal;
	}

	public Double getBase2() {
		return base2;
	}

	public void setBase2(Double base2) {
		this.base2 = base2;
	}

	public Double getCuota2() {
		return cuota2;
	}

	public void setCuota2(Double cuota2) {
		this.cuota2 = cuota2;
	}

	public Double getPorcentaje2() {
		return porcentaje2;
	}

	public void setPorcentaje2(Double porcentaje2) {
		this.porcentaje2 = porcentaje2;
	}

	public Double getBaseDto1() {
		return baseDto1;
	}

	public void setBaseDto1(Double baseDto1) {
		this.baseDto1 = baseDto1;
	}

	public Double getBaseDto2() {
		return baseDto2;
	}

	public void setBaseDto2(Double baseDto2) {
		this.baseDto2 = baseDto2;
	}

	public Double getPorcentajeDto1() {
		return porcentajeDto1;
	}

	public void setPorcentajeDto1(Double porcentajeDto1) {
		this.porcentajeDto1 = porcentajeDto1;
	}

	public Double getPorcentajeDto2() {
		return porcentajeDto2;
	}

	public void setPorcentajeDto2(Double porcentajeDto2) {
		this.porcentajeDto2 = porcentajeDto2;
	}

	public Double getImporteDto1() {
		return importeDto1;
	}

	public void setImporteDto1(Double importeDto1) {
		this.importeDto1 = importeDto1;
	}

	public Double getImporteDto2() {
		return importeDto2;
	}

	public void setImporteDto2(Double importeDto2) {
		this.importeDto2 = importeDto2;
	}

	public Double getBaseRE() {
		return baseRE;
	}

	public void setBaseRE(Double baseRE) {
		this.baseRE = baseRE;
	}

	public Double getCuotaRE() {
		return cuotaRE;
	}

	public void setCuotaRE(Double cuotaRE) {
		this.cuotaRE = cuotaRE;
	}

	public Double getPorcentajeRE() {
		return porcentajeRE;
	}

	public void setPorcentajeRE(Double porcentajeRE) {
		this.porcentajeRE = porcentajeRE;
	}

}