package borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo;

public class MapeoNotasFacturasVentas
{
	private Integer idCodigo;
    private Integer idCodigoVenta;
	private String clave_tabla;
	private String documento;
	private String serie;
	private Integer numeroLinea;
	private String observaciones;

	public MapeoNotasFacturasVentas()
	{
	}


	public Integer getIdCodigoVenta() {
		return idCodigoVenta;
	}


	public void setIdCodigoVenta(Integer idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}


	public String getClave_Tabla() {
		return clave_tabla;
	}


	public void setClave_Tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public Integer getIdCodigo() {
		return idCodigo;
	}


	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}


	public Integer getNumeroLinea() {
		return numeroLinea;
	}


	public void setNumeroLinea(Integer numeroLinea) {
		this.numeroLinea = numeroLinea;
	}



}