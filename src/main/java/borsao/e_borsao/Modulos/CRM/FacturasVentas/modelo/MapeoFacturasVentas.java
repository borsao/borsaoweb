package borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoFacturasVentas extends MapeoGlobal
{
    private Integer idCodigoFactura;
	private String clave_tabla;
	private String tipoFactura;
	private String documento;
	private String referencia_cliente;
	private String serie;
	private String mail;
	private String mail2;
	private String mail3;
	private String nombre;
	private Integer ejercicio;
	private String enviar;
	private Date fecha;
	private String boton = " ";
	private String pdf = " ";
	private String marcar = " ";
	private String moneda;
	private String enviado = null;
	
	private Integer cliente;
	private String nif;
	private String direccion;
	private String cp;
	private String poblacion;
	private String provincia;
	private String pais;
	private String fax;
	private Date plazoEntrega;
	private Date vencimiento;
	private int diasVencimiento;
	private int diasPago1;
	private int diasPago2;
	private int diasPago3;
	private String formaPago;
	private String nombreBanco;
	private String iban;
	private String swift;

	public MapeoFacturasVentas()
	{
		this.setEnviar("Enviar");
	}


	public Integer getIdCodigoFactura() {
		return idCodigoFactura;
	}


	public void setIdCodigoFactura(Integer idCodigoFactura) {
		this.idCodigoFactura = idCodigoFactura;
	}

	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getEnviar() {
		return enviar;
	}


	public void setEnviar(String enviar) {
		this.enviar = enviar;
	}


	public String getFecha() {
		return RutinasFechas.convertirDateToString(fecha);
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
		
	}



	public String getMail2() {
		return mail2;
	}


	public void setMail2(String mail) {
		this.mail2 = mail;
	}


	public String getPdf() {
		return pdf;
	}


	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getBoton() {
		return boton;
	}


	public void setBoton(String boton) {
		this.boton = boton;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCp() {
		return cp;
	}


	public void setCp(String cp) {
		this.cp = cp;
	}


	public String getPoblacion() {
		return poblacion;
	}


	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getPlazoEntrega() {
		if (plazoEntrega!=null)
		return RutinasFechas.convertirDateToString(plazoEntrega);
		else return null;
	}


	public void setPlazoEntrega(Date plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}


	public String getFormaPago() {
		return formaPago;
	}


	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getEnviado() {
		return enviado;
	}


	public void setEnviado(String enviado) {
		this.enviado = enviado;
	}


	public String getMail3() {
		return mail3;
	}


	public void setMail3(String mail3) {
		this.mail3 = mail3;
	}


	public Integer getCliente() {
		return cliente;
	}


	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}


	public String getNombreBanco() {
		return nombreBanco;
	}


	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}


	public String getIban() {
		return iban;
	}


	public void setIban(String iban) {
		this.iban = iban;
	}


	public String getSwift() {
		return swift;
	}


	public void setSwift(String swift) {
		this.swift = swift;
	}


	public String getClave_tabla() {
		return clave_tabla;
	}


	public void setClave_tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getTipoFactura() {
		return tipoFactura;
	}


	public void setTipoFactura(String tipoFactura) {
		this.tipoFactura = tipoFactura;
	}


	public String getReferencia_cliente() {
		return referencia_cliente;
	}


	public void setReferencia_cliente(String referencia_cliente) {
		this.referencia_cliente = referencia_cliente;
	}


	public String getNif() {
		return nif;
	}


	public void setNif(String nif) {
		this.nif = nif;
	}


	public Date getVencimiento() {
		return vencimiento;
	}


	public void setVencimiento(Date vencimiento) {
		this.vencimiento = vencimiento;
	}


	public int getDiasVencimiento() {
		return diasVencimiento;
	}


	public void setDiasVencimiento(int diasVencimiento) {
		this.diasVencimiento = diasVencimiento;
	}


	public int getDiasPago1() {
		return diasPago1;
	}


	public void setDiasPago1(int diasPago1) {
		this.diasPago1 = diasPago1;
	}


	public int getDiasPago2() {
		return diasPago2;
	}


	public void setDiasPago2(int diasPago2) {
		this.diasPago2 = diasPago2;
	}


	public int getDiasPago3() {
		return diasPago3;
	}


	public void setDiasPago3(int diasPago3) {
		this.diasPago3 = diasPago3;
	}


	public String getMarcar() {
		return marcar;
	}


	public void setMarcar(String marcar) {
		this.marcar = marcar;
	}



}