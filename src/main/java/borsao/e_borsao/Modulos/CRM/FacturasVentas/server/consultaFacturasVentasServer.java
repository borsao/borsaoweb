package borsao.e_borsao.Modulos.CRM.FacturasVentas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoFacturasVentas;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoLineasFacturasVentas;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoNotasFacturasVentas;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoResultadosFacturasVentas;

public class consultaFacturasVentasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaFacturasVentasServer instance;
	
	private String tablaCabecera = null;
	private String tablaLineas = null;
	private String tablaPies = null;
	
	public consultaFacturasVentasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaFacturasVentasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaFacturasVentasServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoFacturasVentas> datosOpcionesGlobal(String r_documento, String r_cliente, boolean r_pendientes, String r_ejercicio)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoFacturasVentas> vector = null;
		boolean accesoCliente = false;
		Integer cliente = null; 
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		this.establecerTablas(r_ejercicio);
		
		cadenaSQL.append(" SELECT distinct a.clave_tabla cc_cl, ");
        cadenaSQL.append(" a.documento_factura cc_dc, ");
        cadenaSQL.append(" a.serie_factura cc_sr, ");
        cadenaSQL.append(" a.num_factura cc_cd, ");	    
        cadenaSQL.append(" a.fecha_factura cc_fec, ");
        cadenaSQL.append(" a.ref_cliente cc_ref, ");
        cadenaSQL.append(" a.ejercicio cc_ej, ");
        cadenaSQL.append(" a.cliente_factura cc_cli, ");
        cadenaSQL.append(" a.cl_listada_ruta cc_se, ");        
        cadenaSQL.append(" a.estadistico_b1 cc_tip, ");
        
        if (r_documento!=null && r_documento.equals("W"))
        {
	        cadenaSQL.append(" a.cif cc_cif, ");
	        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" a.direccion cc_dir, ");
	        cadenaSQL.append(" a.poblacion cc_pob, ");
	        cadenaSQL.append(" a.cod_postal cc_cp, ");
        }
        else
        {
	        cadenaSQL.append(" e.cif cc_cif, ");
	        cadenaSQL.append(" e.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" e.direccion cc_dir, ");
	        cadenaSQL.append(" e.poblacion cc_pob, ");
	        cadenaSQL.append(" e.cod_postal cc_cp, ");        	
        }
        /*
         * sgregar los datos de contacto del albaran
         */
        cadenaSQL.append(" e.fax cc_fax, ");
        cadenaSQL.append(" e.dia_pago1 cc_d1, ");
        cadenaSQL.append(" e.dia_pago2 cc_d2, ");
        cadenaSQL.append(" e.dia_pago3 cc_d3, ");

        cadenaSQL.append(" f.texto cl_mai, ");
        cadenaSQL.append(" g.texto cl_mai2, ");        
        cadenaSQL.append(" h.texto cl_mai3, ");
        
//        cadenaSQL.append(" a.fecha_documento cc_pe, ");
        cadenaSQL.append(" c.nombre cc_pa, ");
        cadenaSQL.append(" d.nombre cc_pv, ");
        cadenaSQL.append(" m.nemotecnico div_nem, ");
        
        cadenaSQL.append(" b.primer_vto cc_vto, ");
        cadenaSQL.append(" b.descripcion cc_fp ");
        
     	cadenaSQL.append(" FROM " + this.tablaCabecera );
     	cadenaSQL.append(" a, e_frmpag b, e_paises c, e_provin d, e_client e, e_divisa m, outer e_cli_nt f, outer e_cli_nt g, outer e_cli_nt h ");
     	cadenaSQL.append(" where e.cliente = a.cliente_factura and a.estadistico_b1 <> 'N' ");
     	cadenaSQL.append(" and c.pais = a.pais and m.numero=a.moneda ");
     	if (r_documento!=null && r_documento.equals("W")) cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia "); else cadenaSQL.append(" and d.pais = a.pais and d.provincia = e.provincia ");
     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
     	cadenaSQL.append(" and f.cliente = e.cliente and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
     	cadenaSQL.append(" and g.cliente = e.cliente and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
     	cadenaSQL.append(" and h.cliente = e.cliente and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
     	cadenaSQL.append(" and a.num_factura is not null ");
     	
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" and a.documento_factura = '" + r_documento + "'");
     	}
     	if (r_cliente!=null)
     	{
     		cadenaSQL.append(" and a.cliente_factura = '" + r_cliente + "'");
     	}
     	else
     	{
     		if (eBorsao.get().accessControl.isAccesoCliente()) cadenaSQL.append(" and a.cliente_factura = '" + eBorsao.get().accessControl.getCodigoGestion().toString() + "'");
     	}
     	
     	if (r_pendientes)
     	{
     		cadenaSQL.append(" and a.cl_listada_ruta = 'N'" );
     	}
     	
		try
		{
			cadenaSQL.append(" order by 4 desc ");
			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoFacturasVentas>();
			
			while(rsOpcion.next())
			{
				MapeoFacturasVentas mapeoFacturasVentas = new MapeoFacturasVentas();
				/*
				 * recojo mapeo operarios
				 */
				
				/*
				 * TODO: Modificar los datos de contacto en caso de que el cliente factura sea el mismo que el del albaran
				 * 		respetar el del albaran
				 */
				mapeoFacturasVentas.setEjercicio(rsOpcion.getInt("cc_ej"));
				mapeoFacturasVentas.setClave_tabla(rsOpcion.getString("cc_cl"));
				mapeoFacturasVentas.setTipoFactura(String.valueOf(rsOpcion.getString("cc_tip").charAt(0)));				
				mapeoFacturasVentas.setDocumento(rsOpcion.getString("cc_dc"));
				mapeoFacturasVentas.setReferencia_cliente(rsOpcion.getString("cc_ref"));
				mapeoFacturasVentas.setNif(rsOpcion.getString("cc_cif"));
				mapeoFacturasVentas.setSerie(rsOpcion.getString("cc_sr"));
				mapeoFacturasVentas.setIdCodigoFactura(rsOpcion.getInt("cc_cd"));
				mapeoFacturasVentas.setNombre(RutinasCadenas.conversion(rsOpcion.getString("cc_nc")));
				mapeoFacturasVentas.setMail(RutinasCadenas.conversion(rsOpcion.getString("cl_mai")));
				mapeoFacturasVentas.setFecha(rsOpcion.getDate("cc_fec"));
				mapeoFacturasVentas.setMail2(RutinasCadenas.conversion(rsOpcion.getString("cl_mai2")));
				mapeoFacturasVentas.setMail3(RutinasCadenas.conversion(rsOpcion.getString("cl_mai3")));
				mapeoFacturasVentas.setMoneda(RutinasCadenas.conversion(rsOpcion.getString("div_nem").substring(0, 3)));
				
				if ((rsOpcion.getString("cl_mai") == null || rsOpcion.getString("cl_mai").length()==0) && (rsOpcion.getString("cl_mai2")==null || rsOpcion.getString("cl_mai2").length()==0) && (rsOpcion.getString("cl_mai3")==null || rsOpcion.getString("cl_mai3").length()==0)) 
				{
					mapeoFacturasVentas.setEnviar("--------");
				}
				if (rsOpcion.getString("cc_tip").equals("O"))
				{
					mapeoFacturasVentas.setEnviar("Ordinario");
				}
				if (rsOpcion.getString("cc_tip").equals("M") && rsOpcion.getInt("cc_cli")==6662 )
				{
					mapeoFacturasVentas.setEnviar("");
				}
				if (rsOpcion.getString("cc_se") == null || rsOpcion.getString("cc_se").length()==0)
				{
					mapeoFacturasVentas.setEnviado("");
				}
				else
				{
					mapeoFacturasVentas.setEnviado(rsOpcion.getString("cc_se").trim().toUpperCase());
				}
				mapeoFacturasVentas.setCliente(rsOpcion.getInt("cc_cli"));
				mapeoFacturasVentas.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("cc_dir")));
				mapeoFacturasVentas.setPoblacion(RutinasCadenas.conversion(rsOpcion.getString("cc_pob")));
				mapeoFacturasVentas.setCp(rsOpcion.getString("cc_cp"));
//				mapeoFacturasVentas.setPlazoEntrega(rsOpcion.getDate("cc_pe"));
				mapeoFacturasVentas.setFax(rsOpcion.getString("cc_fax"));
				mapeoFacturasVentas.setPais(RutinasCadenas.conversion(rsOpcion.getString("cc_pa")));
				mapeoFacturasVentas.setProvincia(RutinasCadenas.conversion(rsOpcion.getString("cc_pv")));
				mapeoFacturasVentas.setFormaPago(RutinasCadenas.conversion(rsOpcion.getString("cc_fp")));
				mapeoFacturasVentas.setDiasVencimiento(rsOpcion.getInt("cc_vto"));
				mapeoFacturasVentas.setDiasPago1(rsOpcion.getInt("cc_d1"));
				mapeoFacturasVentas.setDiasPago2(rsOpcion.getInt("cc_d2"));
				mapeoFacturasVentas.setDiasPago3(rsOpcion.getInt("cc_d3"));
				vector.add(mapeoFacturasVentas);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	public boolean traerFacturaGreensysMysql(MapeoFacturasVentas r_mapeo)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		this.establecerTablas(r_mapeo.getEjercicio().toString());
		
		cadenaSQL.append(" SELECT idCodigo cc_id, ");
		cadenaSQL.append(" clave_tabla cc_clt, ");
        cadenaSQL.append(" documento cc_doc, ");
        cadenaSQL.append(" serie cc_ser, ");
        cadenaSQL.append(" codigo cc_cod, ");	    
        cadenaSQL.append(" ejercicio cc_eje ");
     	cadenaSQL.append(" FROM a_va_c_0 " );
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie = '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoFactura()!=null && r_mapeo.getIdCodigoFactura().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoFactura() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, clave_tabla, documento, serie, codigo desc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			String cadenaSQLBorrado = null;
			Statement cd = null;
			
			while(rsOpcion.next())
			{
				r_mapeo.setIdCodigo(rsOpcion.getInt("cc_id"));
				conDelete = this.conManager.establecerConexion();
				
				cd = conDelete.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				/*
				 * 1.- lo borro
				 */
				cadenaSQLBorrado = " delete from a_va_p_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				cadenaSQLBorrado = " delete from a_va_l_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				cadenaSQLBorrado = " delete from a_va_n_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				cadenaSQLBorrado = " delete from a_va_c_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				conDelete=null;
				cd=null;
				
			}
			/*
			 * inserto de nuevo la factura
			 */
			insertado = this.insertarCabecera(r_mapeo);
			/*
			 * devuelvo verdadero
			 */
			return insertado;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return false;
	}
	
	public boolean insertarCabecera(MapeoFacturasVentas r_mapeo)
	{
		/*
		 * Codigo del cliente
		 * Nombre comercial del cliente
		 * Direccion del cliente 
		 * Codigo postal del cliente
		 * Poblacion del cliente 
		 * Provincia del cliente
		 * Num. Fax del cliente
		 * mail1
		 * mail2
		 * Fecha de factura
		 * Numero de factura
		 * Tipo Factura
		 * Descripcion de forma de pago
		 * Plazo de entrega del factura
		 * Datos Bancarios
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		boolean insertado = true;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" INSERT INTO a_va_c_0 ( ");
    		cadenaSQL.append(" clave_tabla, ");
	        cadenaSQL.append(" documento, ");
	        cadenaSQL.append(" serie, ");
	        cadenaSQL.append(" codigo, ");
	        cadenaSQL.append(" tipo, ");
	        cadenaSQL.append(" fecha_documento, ");
	        cadenaSQL.append(" ejercicio, ");	       
	        cadenaSQL.append(" cliente, ");
	        cadenaSQL.append(" nombre_comercial, ");
	        cadenaSQL.append(" mail1, ");
	        cadenaSQL.append(" mail2, ");
	        cadenaSQL.append(" mail3, ");
	        cadenaSQL.append(" direccion, ");
	        cadenaSQL.append(" poblacion, ");
	        cadenaSQL.append(" cod_postal, ");
	        cadenaSQL.append(" pais, ");
	        cadenaSQL.append(" provincia, ");
	        cadenaSQL.append(" plazo_entrega, ");
	        cadenaSQL.append(" fpago_aplazado, ");
	        cadenaSQL.append(" fax, ");
	        cadenaSQL.append(" nif, ");
	        cadenaSQL.append(" vencimiento, ");
	        cadenaSQL.append(" primer_vto, ");
	        cadenaSQL.append(" dias1, ");
	        cadenaSQL.append(" dias2, ");
	        cadenaSQL.append(" dias3, ");
	        cadenaSQL.append(" moneda, ");
	        cadenaSQL.append(" idCodigo ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguiente());
		    
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdCodigoFactura());
		    
		    if (r_mapeo.getTipoFactura()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTipoFactura());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(6, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    preparedStatement.setInt(7, r_mapeo.getEjercicio());
		    preparedStatement.setInt(8, r_mapeo.getCliente());
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(9, RutinasCadenas.conversion(r_mapeo.getNombre()));
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getMail()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getMail());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getMail2()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getMail2());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getMail3()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getMail3());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }		    
		    if (r_mapeo.getDireccion()!=null)
		    {
		    	preparedStatement.setString(13, RutinasCadenas.conversion(r_mapeo.getDireccion()));
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getPoblacion()!=null)
		    {
		    	preparedStatement.setString(14, RutinasCadenas.conversion(r_mapeo.getPoblacion()));
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getCp()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getCp());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getPais()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getPais());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getProvincia()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getProvincia());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getPlazoEntrega()!=null)
		    {
		    	preparedStatement.setString(18, RutinasFechas.convertirAFechaMysql(r_mapeo.getPlazoEntrega()));
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getFormaPago()!=null)
		    {
		    	preparedStatement.setString(19, RutinasCadenas.conversion(r_mapeo.getFormaPago()));
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getFax()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getFax());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(21, null);
		    }
		    preparedStatement.setString(22, calculaVencimiento(r_mapeo.getFecha(), r_mapeo.getDiasVencimiento(),r_mapeo.getDiasPago1(), r_mapeo.getDiasPago2(), r_mapeo.getDiasPago3()));
		    
		    preparedStatement.setInt(23, r_mapeo.getDiasVencimiento());
		    preparedStatement.setInt(24, r_mapeo.getDiasPago1());
		    preparedStatement.setInt(25, r_mapeo.getDiasPago2());
		    preparedStatement.setInt(26, r_mapeo.getDiasPago3());
		    preparedStatement.setString(27, r_mapeo.getMoneda());
		    
		    preparedStatement.setInt(28, r_mapeo.getIdCodigo());

	        preparedStatement.executeUpdate();
	        
			
	        /*
	         * insertamos las lineas tras el exito de la cabecera
	         */
	        insertado=this.insertarLineas(r_mapeo);
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }        
		return insertado;
	}
	
	
	public boolean insertarLineas(MapeoFacturasVentas r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaWhere =null;
		Boolean insertado = true;
		MapeoLineasFacturasVentas mapeoLineas = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		if (r_mapeo!=null && !r_mapeo.getDocumento().trim().contentEquals("W"))
		{
		
			cadenaSQL.append(" SELECT clave_tabla cl_clt, ");
	        cadenaSQL.append(" documento cl_doc, ");
	        cadenaSQL.append(" serie cl_ser, ");
	        cadenaSQL.append(" codigo cl_cod, ");
	        cadenaSQL.append(" fecha_documento cl_fec, ");
	        cadenaSQL.append(" ejercicio cl_eje, ");
	        cadenaSQL.append(" a.articulo cl_ar, ");
	        cadenaSQL.append(" e_cli_ar.articulo_par cl_arp, ");
	        cadenaSQL.append(" posicion cl_pos, ");
	        cadenaSQL.append(" num_linea cl_lin, ");
	        cadenaSQL.append(" a.descrip_articulo cl_des, ");
	        cadenaSQL.append(" e_cli_ar.descrip_articulo cl_desp, ");
	        cadenaSQL.append(" cantidad cl_can, ");
	        cadenaSQL.append(" unidades cl_ud, ");
	        cadenaSQL.append(" bultos cl_bul, ");
	        cadenaSQL.append(" etiquetas cl_et, ");
	        cadenaSQL.append(" ubicacion cl_ub, ");
	        cadenaSQL.append(" precio cl_pre, ");
	        cadenaSQL.append(" dto_unico cl_dto, ");
	        cadenaSQL.append(" importe cl_imd, ");
	        cadenaSQL.append(" importe_local cl_iml, ");
	        cadenaSQL.append(" e_divisa.nemotecnico cl_div ");
	        
	     	cadenaSQL.append(" FROM " + this.tablaLineas + " a, e_divisa, outer e_cli_ar ");
	     	cadenaSQL.append(" where a.receptor_emisor=e_cli_ar.cliente ");
	     	cadenaSQL.append(" and a.articulo =e_cli_ar.articulo ");
	     	cadenaSQL.append(" and a.moneda =e_divisa.numero ");
			try
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento_factura = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie_factura = '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoFactura()!=null && r_mapeo.getIdCodigoFactura().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" num_factura = " + r_mapeo.getIdCodigoFactura() + " ");
				}
				
				
				if (cadenaWhere!=null && cadenaWhere.length()>0)
				{
					cadenaSQL.append(" and ");
					cadenaSQL.append(cadenaWhere.toString() );
				}
				
				cadenaSQL.append(" order by clave_tabla, documento, serie, codigo ,posicion");
				
				con= this.conManager.establecerConexionGestion();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					/*
					 * relleno el mapeo de la linea procesada para insertarla en mi tabla
					 */
					mapeoLineas = new MapeoLineasFacturasVentas();
					mapeoLineas.setIdCodigo(r_mapeo.getIdCodigo());
					mapeoLineas.setClave_tabla(rsOpcion.getString("cl_clt"));
					mapeoLineas.setDocumento(rsOpcion.getString("cl_doc"));
					mapeoLineas.setReferencia(r_mapeo.getReferencia_cliente());
					mapeoLineas.setFechaDocumento(rsOpcion.getDate("cl_fec"));
					mapeoLineas.setSerie(rsOpcion.getString("cl_ser"));
					mapeoLineas.setIdCodigoFactura(rsOpcion.getInt("cl_cod"));
					mapeoLineas.setEjercicio(r_mapeo.getEjercicio());
									
					mapeoLineas.setBultos(rsOpcion.getInt("cl_bul"));
					mapeoLineas.setEtiquetas(rsOpcion.getInt("cl_et"));
					mapeoLineas.setUbicacion(rsOpcion.getString("cl_ub"));
					
					if (rsOpcion.getString("cl_arp")!=null && rsOpcion.getString("cl_arp").trim().length()>0)
					{
						mapeoLineas.setArticulo(rsOpcion.getString("cl_arp"));
					}
					else
					{
						mapeoLineas.setArticulo(rsOpcion.getString("cl_ar"));
					}
					if (rsOpcion.getString("cl_desp")!=null && rsOpcion.getString("cl_desp").trim().length()>0)
					{
						mapeoLineas.setDescripcion(rsOpcion.getString("cl_desp"));
					}
					else
					{
						mapeoLineas.setDescripcion(rsOpcion.getString("cl_des"));
					}
					mapeoLineas.setCantidad(rsOpcion.getDouble("cl_can"));
					mapeoLineas.setUnidades(rsOpcion.getDouble("cl_ud"));
					mapeoLineas.setPrecio(rsOpcion.getDouble("cl_pre"));
					if (rsOpcion.getString("cl_dto")!=null && rsOpcion.getString("cl_dto").length()>0)
					{
						mapeoLineas.setDescuento(rsOpcion.getDouble("cl_dto"));
					}
					else
					{
						mapeoLineas.setDescuento(new Double(0));
					}
					mapeoLineas.setImporte(rsOpcion.getDouble("cl_imd"));
					mapeoLineas.setImporteLocal(rsOpcion.getDouble("cl_iml"));
					
					mapeoLineas.setMoneda(rsOpcion.getString("cl_div").substring(0, 3));
					r_mapeo.setMoneda(rsOpcion.getString("cl_div").substring(0, 3));
					
					mapeoLineas.setPosicion(rsOpcion.getInt("cl_pos"));
					mapeoLineas.setLinea(rsOpcion.getInt("cl_lin"));
					this.guardoLineasMysql(mapeoLineas);
					
				}
				
				insertado= this.insertarNotas(r_mapeo);
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
				insertado= false;
			}		
		}
		else if (r_mapeo!=null && r_mapeo.getDocumento().trim().contentEquals("W"))
		{
			try
			{

				cadenaSQL.append(" SELECT b.id as lin_lin, ");  
				cadenaSQL.append(" b.sku as lin_art, "); 
				cadenaSQL.append(" b.title as lin_des, "); 
				cadenaSQL.append(" b.quantity as lin_can, ");
				cadenaSQL.append(" b.price as lin_pre, ");
				cadenaSQL.append(" b.total as lin_imp  ");
				
//				cadenaSQL.append(" SELECT b.order_item_id as lin_lin, ");  
//				cadenaSQL.append(" c.sku as lin_art, "); 
//				cadenaSQL.append(" a.order_item_name as lin_des, "); 
//				cadenaSQL.append(" d.meta_value as lin_can, ");
//				cadenaSQL.append(" d.meta_value as lin_ud,  ");
//				cadenaSQL.append(" e.meta_value/d.meta_value as lin_pre, ");
//				cadenaSQL.append(" e.meta_value as lin_imp  ");
				
//				cadenaSQL.append(" b.tax_amount as lin_iva, ");
//				cadenaSQL.append(" b.shipping_amount as lin_por,  ");
//				cadenaSQL.append(" b.shipping_tax_amount as lin_ivp, ");
//				cadenaSQL.append(" b.product_gross_revenue as lin_imi ");

				
				cadenaSQL.append(" FROM orders as a ");
				cadenaSQL.append(" inner join order_items as b on b.order_id = a.id ");
				
				
//				cadenaSQL.append(" FROM `db54eilguucl8l`.u87_woocommerce_order_items as a ");
//				cadenaSQL.append(" inner join `db54eilguucl8l`.u87_woocommerce_order_itemmeta as b on b.order_item_id = a.order_item_id and b.meta_key = '_product_id' ");
//		     	cadenaSQL.append(" inner join `db54eilguucl8l`.u87_woocommerce_order_itemmeta as b on b.order_item_id = a.order_item_id and b.meta_key = '_product_id' ");
//		     	cadenaSQL.append(" inner join `db54eilguucl8l`.u87_woocommerce_order_itemmeta as d on d.order_item_id = a.order_item_id and d.meta_key = '_qty' ");
//		     	cadenaSQL.append(" inner join `db54eilguucl8l`.u87_woocommerce_order_itemmeta as e on e.order_item_id = a.order_item_id and e.meta_key = '_line_total' ");
//		     	cadenaSQL.append(" inner join `db54eilguucl8l`.u87_wc_product_meta_lookup as c on b.meta_value = c.product_id ");
//				cadenaSQL.append(" where a.order_item_type='line_item' and a.order_id = '" + r_mapeo.getReferencia_cliente().trim() + "' ");
		     	
		     	cadenaSQL.append(" where a.order_number = '" + r_mapeo.getReferencia_cliente().trim() + "' ");
		     	
//		     	cadenaSQL.append(" union all ");
//
//				cadenaSQL.append(" SELECT f.order_item_id as lin_lin, ");  
//				cadenaSQL.append(" 'PORTE' as lin_art, "); 
//				cadenaSQL.append(" 'ENVIO' as lin_des, "); 
//				cadenaSQL.append(" 1 as lin_can, ");
//				cadenaSQL.append(" 1 as lin_ud,  ");
//				cadenaSQL.append(" f.meta_value as lin_pre, ");
//				cadenaSQL.append(" f.meta_value as lin_imp  ");
//				cadenaSQL.append(" FROM `db54eilguucl8l`.u87_woocommerce_order_items as a ");
//				cadenaSQL.append(" inner join `db54eilguucl8l`.u87_woocommerce_order_itemmeta as f on f.order_item_id = a.order_item_id and f.meta_key = 'cost' ");
//				cadenaSQL.append(" where a.order_item_type='shipping' and a.order_id = '" + r_mapeo.getReferencia_cliente().trim() + "' ");

				cadenaSQL.append(" order by 1");
				
				con= this.conManager.establecerConexionTienda();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
//				Double porte = new Double(0);
//				Integer num_linea = new Integer(0);
				
				while(rsOpcion.next())
				{
					/*
					 * relleno el mapeo de la linea procesada para insertarla en mi tabla
					 */
					mapeoLineas = new MapeoLineasFacturasVentas();
					
					mapeoLineas.setIdCodigo(r_mapeo.getIdCodigo());
					mapeoLineas.setReferencia(r_mapeo.getReferencia_cliente());
					mapeoLineas.setEjercicio(r_mapeo.getEjercicio());
					mapeoLineas.setClave_tabla(r_mapeo.getClave_tabla());
					
					mapeoLineas.setDocumento(r_mapeo.getDocumento());
					mapeoLineas.setFechaDocumento(RutinasFechas.convertirADate(r_mapeo.getFecha()));
					mapeoLineas.setSerie(r_mapeo.getSerie());
					mapeoLineas.setIdCodigoFactura(r_mapeo.getIdCodigoFactura());
									
					mapeoLineas.setBultos(new Integer(0));
					mapeoLineas.setEtiquetas(new Integer(0));
					mapeoLineas.setUbicacion(null);
					mapeoLineas.setDescuento(new Double(0));
					mapeoLineas.setMoneda("EUR");
					r_mapeo.setMoneda("EUR");
					
						mapeoLineas.setArticulo(rsOpcion.getString("lin_art"));
						mapeoLineas.setDescripcion(rsOpcion.getString("lin_des"));
						mapeoLineas.setCantidad(rsOpcion.getDouble("lin_can"));
						mapeoLineas.setUnidades(rsOpcion.getDouble("lin_can"));
						mapeoLineas.setPrecio(rsOpcion.getDouble("lin_pre"));
						mapeoLineas.setImporte(rsOpcion.getDouble("lin_imp"));
						mapeoLineas.setImporteLocal(rsOpcion.getDouble("lin_imp"));
						mapeoLineas.setPosicion(rsOpcion.getInt("lin_lin"));
						mapeoLineas.setLinea(rsOpcion.getInt("lin_lin"));
					
					this.guardoLineasMysql(mapeoLineas);
//					porte += rsOpcion.getDouble("lin_por");
//					num_linea = rsOpcion.getInt("lin_lin");
				}
				/*
				 * inserto la linea de porte fijo				 * 
				 */
				
//				num_linea = num_linea + 1;
//				mapeoLineas = new MapeoLineasFacturasVentas();
//				
//				mapeoLineas.setIdCodigo(r_mapeo.getIdCodigo());
//				mapeoLineas.setReferencia(r_mapeo.getReferencia_cliente());
//				mapeoLineas.setEjercicio(r_mapeo.getEjercicio());
//				mapeoLineas.setClave_tabla(r_mapeo.getClave_tabla());
//				
//				mapeoLineas.setDocumento(r_mapeo.getDocumento());
//				mapeoLineas.setFechaDocumento(RutinasFechas.convertirADate(r_mapeo.getFecha()));
//				mapeoLineas.setSerie(r_mapeo.getSerie());
//				mapeoLineas.setIdCodigoFactura(r_mapeo.getIdCodigoFactura());
//								
//				mapeoLineas.setBultos(new Integer(0));
//				mapeoLineas.setEtiquetas(new Integer(0));
//				mapeoLineas.setUbicacion(null);
//				mapeoLineas.setDescuento(new Double(0));
//				mapeoLineas.setMoneda("EUR");
//				r_mapeo.setMoneda("EUR");
//				
//					mapeoLineas.setArticulo("PORTE");
//					mapeoLineas.setDescripcion("ENVIO");
//					mapeoLineas.setCantidad(new Double(1));
//					mapeoLineas.setUnidades(new Double(1));
//					mapeoLineas.setPrecio(porte);
//					mapeoLineas.setImporte(porte);
//					mapeoLineas.setImporteLocal(porte);
//					mapeoLineas.setPosicion(num_linea);
//					mapeoLineas.setLinea(num_linea);
//				
//				this.guardoLineasMysql(mapeoLineas);

				this.conManager.cerrarConexion(con);
				
				insertado= this.insertarNotas(r_mapeo);
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
				insertado= false;
			}	
		}
		else
		{
			insertado = false;
		}
		return insertado;
	}
	

	private void guardoLineasMysql(MapeoLineasFacturasVentas r_mapeo) throws SQLException
	{
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		/*
		 * Inserto los datos en el mysql
		 */
		cadenaSQL= new StringBuffer();
		cadenaSQL.append(" INSERT INTO a_va_l_0 ( ");
		cadenaSQL.append(" clave_tabla, ");
        cadenaSQL.append(" documento, ");
        cadenaSQL.append(" serie, ");
        cadenaSQL.append(" codigo, ");
        cadenaSQL.append(" ejercicio, ");
        
        cadenaSQL.append(" articulo, ");
        	cadenaSQL.append(" posicion, ");
        	cadenaSQL.append(" num_linea, ");
        cadenaSQL.append(" descrip_articulo, ");
        cadenaSQL.append(" cantidad, ");
        cadenaSQL.append(" bultos, ");
        cadenaSQL.append(" etiquetas, ");
        cadenaSQL.append(" ubicacion, ");
        cadenaSQL.append(" unidades, ");
        cadenaSQL.append(" precio, ");
        cadenaSQL.append(" dto_unico, ");
        cadenaSQL.append(" importe, ");
        cadenaSQL.append(" importe_local, ");
        cadenaSQL.append(" moneda, ");
        cadenaSQL.append(" fecha_documento, ");
        cadenaSQL.append(" referencia, ");
        cadenaSQL.append(" idCodigo ) VALUES (");
	    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
	    
	   
	    conInsert= this.conManager.establecerConexion();	
	    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
	    
	    if (r_mapeo.getClave_tabla()!=null)
	    {
	    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
	    }
	    else
	    {
	    	preparedStatement.setString(1, null);
	    }
	    if (r_mapeo.getDocumento()!=null)
	    {
	    	preparedStatement.setString(2, r_mapeo.getDocumento());
	    }
	    else
	    {
	    	preparedStatement.setString(2, null);
	    }
	    if (r_mapeo.getSerie()!=null)
	    {
	    	preparedStatement.setString(3, r_mapeo.getSerie());
	    }
	    else
	    {
	    	preparedStatement.setString(3, null);
	    }
	    preparedStatement.setInt(4, r_mapeo.getIdCodigoFactura());
	    preparedStatement.setInt(5, r_mapeo.getEjercicio());
	    if (r_mapeo.getArticulo()!=null)
	    {
	    	preparedStatement.setString(6, r_mapeo.getArticulo());
	    }
	    else
	    {
	    	preparedStatement.setString(6, null);
	    }
	    preparedStatement.setInt(7, r_mapeo.getPosicion());
	    preparedStatement.setInt(8, r_mapeo.getLinea());

	    if (r_mapeo.getDescripcion()!=null)
	    {
	    	preparedStatement.setString(9, RutinasCadenas.conversion(r_mapeo.getDescripcion()));
	    }
	    else
	    {
	    	preparedStatement.setString(9, null);
	    }
	    preparedStatement.setDouble(10, r_mapeo.getCantidad());
	    
	    preparedStatement.setDouble(11, r_mapeo.getBultos());
	    preparedStatement.setDouble(12, r_mapeo.getEtiquetas());
	    preparedStatement.setString(13, r_mapeo.getUbicacion());
	    
	    
	    preparedStatement.setDouble(14, r_mapeo.getUnidades());
	    preparedStatement.setDouble(15, r_mapeo.getPrecio());
	    preparedStatement.setDouble(16, r_mapeo.getDescuento());
	    preparedStatement.setDouble(17, r_mapeo.getImporte());
	    preparedStatement.setDouble(18, r_mapeo.getImporteLocal());
	    preparedStatement.setString(19, r_mapeo.getMoneda());
	    preparedStatement.setString(20, RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento()));
	    preparedStatement.setString(21, r_mapeo.getReferencia());
	    preparedStatement.setInt(22, r_mapeo.getIdCodigo());

        preparedStatement.executeUpdate();
        
	}
	private String recuperarFormato(Integer r_cliente, String r_area)
	{
		String formato=null;
		
    	if (r_area.trim().equals("N") || r_area.trim().equals("W"))
    	{
    		formato ="impresoFacturaNacional.jrxml";
    	}
    	else if (r_area.trim().equals("E"))
    	{
    		formato = this.recuperarNotasFormatoImpresion(r_cliente);
    		if (formato==null) formato = "impresoFacturaExport.jrxml";
    	}
		return formato;
	}
	
	public String recuperarNotasFormatoImpresion(Integer r_cliente)
	{
		ResultSet rsOpcion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT num_linea cn_lin, ");
		cadenaSQL.append(" tipo_dato cn_td, ");
        cadenaSQL.append(" texto cn_obs ");
        
     	cadenaSQL.append(" FROM e_cli_nt ");
     	cadenaSQL.append(" where e_cli_nt.cliente = " + r_cliente);
     	cadenaSQL.append(" and e_cli_nt.tipo_dato IN ('FF')" );
     	cadenaSQL.append(" order by tipo_dato, num_linea ");
     	
		try
		{

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				if (rsOpcion.getString("cn_obs")!=null && rsOpcion.getString("cn_obs").length()>0)
				{
					return rsOpcion.getString("cn_obs").trim().concat(".jrxml");
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return null;
	}
	public boolean insertarNotas(MapeoFacturasVentas r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		Boolean insertado = true;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT num_linea cn_lin, ");
		cadenaSQL.append(" tipo_dato cn_td, ");
        cadenaSQL.append(" texto cn_obs ");
        
     	cadenaSQL.append(" FROM e_cli_nt ");
     	cadenaSQL.append(" where e_cli_nt.cliente = " + r_mapeo.getCliente());
     	cadenaSQL.append(" and e_cli_nt.tipo_dato IN ('IF')" );
     	cadenaSQL.append(" order by tipo_dato, num_linea ");
     	
		try
		{

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				MapeoNotasFacturasVentas mapeoNotas = new MapeoNotasFacturasVentas();
				mapeoNotas.setIdCodigo(r_mapeo.getIdCodigo());
				mapeoNotas.setClave_Tabla(r_mapeo.getClave_tabla());
				mapeoNotas.setDocumento(r_mapeo.getDocumento());
				mapeoNotas.setSerie(r_mapeo.getSerie());
				mapeoNotas.setIdCodigoVenta(r_mapeo.getIdCodigoFactura());
								
				mapeoNotas.setNumeroLinea(rsOpcion.getInt("cn_lin"));
				
				if (rsOpcion.getString("cn_obs")!=null && rsOpcion.getString("cn_obs").length()>0)
				{
					mapeoNotas.setObservaciones(rsOpcion.getString("cn_obs"));
				}
				else
				{
					mapeoNotas.setObservaciones(null);
				}

				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_va_n_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");
		        
		        cadenaSQL.append(" num_linea, ");
		        cadenaSQL.append(" observaciones, ");
		        cadenaSQL.append(" idCodigo ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?) ");
			    
			    conInsert= this.conManager.establecerConexion();	
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoFactura());
			    preparedStatement.setInt(5, mapeoNotas.getNumeroLinea());

			    if (mapeoNotas.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(6, RutinasCadenas.conversion(mapeoNotas.getObservaciones()));
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    preparedStatement.setInt(7, r_mapeo.getIdCodigo());

		        preparedStatement.executeUpdate();
		        
			}
			
			insertado= this.insertarPies(r_mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		
		return insertado;
	}
	public boolean insertarPies(MapeoFacturasVentas r_mapeo)
	{
		/*
		 * Acumulado de importes de detalle en divisa (bruto)
		 * Nemotecnico de la moneda
		 * Notas de proveedor: solo pedidos
		 * 
		 */
		int contador = 0;
		MapeoResultadosFacturasVentas mapeoResultados=null;
		
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT clave_tabla cp_clt, ");
        cadenaSQL.append(" documento cp_doc, ");
        cadenaSQL.append(" serie cp_ser, ");
        cadenaSQL.append(" codigo cp_cod, ");
        cadenaSQL.append(" porimp cp_poi, ");
        cadenaSQL.append(" orden_porimp cp_opi, ");
        cadenaSQL.append(" base cp_bas, ");
        cadenaSQL.append(" porcentaje cp_por, ");
        cadenaSQL.append(" importe cp_imd, ");
        cadenaSQL.append(" importe_local cp_iml ");
        
     	cadenaSQL.append(" FROM " + this.tablaPies + " ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
//				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
//				{
//					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
//					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
//				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento_factura = '" + r_mapeo.getDocumento().trim() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie_factura = '" + r_mapeo.getSerie().trim() + "' ");
				}
				if (r_mapeo.getIdCodigoFactura()!=null && r_mapeo.getIdCodigoFactura().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" num_factura = " + r_mapeo.getIdCodigoFactura() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			mapeoResultados = new MapeoResultadosFacturasVentas();
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				mapeoResultados.setIdCodigo(r_mapeo.getIdCodigo());
				mapeoResultados.setClave_Tabla(r_mapeo.getClave_tabla());
				mapeoResultados.setDocumento(r_mapeo.getDocumento());
				mapeoResultados.setSerie(r_mapeo.getSerie());
				mapeoResultados.setIdCodigoFactura(r_mapeo.getIdCodigoFactura());
				
				switch (rsOpcion.getString("cp_poi"))
				{
					case "P":
					{
						switch (rsOpcion.getInt("cp_opi"))
						{
							case 1:
							{
								mapeoResultados.setBaseDto1(rsOpcion.getDouble("cp_bas"));
								mapeoResultados.setPorcentajeDto1(rsOpcion.getDouble("cp_por"));
								mapeoResultados.setImporteDto1(rsOpcion.getDouble("cp_iml"));
								break;
							}
							case 2:
							{
								mapeoResultados.setBaseDto2(rsOpcion.getDouble("cp_bas"));
								mapeoResultados.setPorcentajeDto2(rsOpcion.getDouble("cp_por"));
								mapeoResultados.setImporteDto2(rsOpcion.getDouble("cp_iml"));
								break;
							}
						}
						break;
					}
					case "T":
					{
						switch (rsOpcion.getInt("cp_opi"))
						{
							case 4:
							{
								mapeoResultados.setImporteBruto(rsOpcion.getDouble("cp_imd"));
								mapeoResultados.setImporteBrutoLocal(rsOpcion.getDouble("cp_iml"));
								break;
							}
							case 5:
							{
								mapeoResultados.setTotalFactura(rsOpcion.getDouble("cp_imd"));
								mapeoResultados.setTotalFacturaLocal(rsOpcion.getDouble("cp_iml"));
								break;
							}
						}
						break;
					}
					case "V":
					{
						contador++;
						switch (contador)
						{
							case 1:
							{
								mapeoResultados.setBase(rsOpcion.getDouble("cp_bas"));
								mapeoResultados.setPorcentaje(rsOpcion.getDouble("cp_por"));
								mapeoResultados.setCuota(rsOpcion.getDouble("cp_iml"));
								break;
							}
							case 2:
							{
								mapeoResultados.setBase2(rsOpcion.getDouble("cp_bas"));
								mapeoResultados.setPorcentaje2(rsOpcion.getDouble("cp_por"));
								mapeoResultados.setCuota2(rsOpcion.getDouble("cp_iml"));
								break;
							}
						}
						break;
					}
					case "R":
					{
						switch (rsOpcion.getInt("cp_opi"))
						{
							case 1:
							{
								mapeoResultados.setBaseRE(rsOpcion.getDouble("cp_bas"));
								mapeoResultados.setPorcentajeRE(rsOpcion.getDouble("cp_por"));
								mapeoResultados.setCuotaRE(rsOpcion.getDouble("cp_iml"));
								break;
							}
//							case 2:
//							{
//								mapeoResultados.setBase2(rsOpcion.getDouble("cp_bas"));
//								mapeoResultados.setPorcentaje2(rsOpcion.getDouble("cp_por"));
//								mapeoResultados.setCuota2(rsOpcion.getDouble("cp_iml"));
//								break;
//							}
						}
						break;
					}
				}
			}
			
			if (mapeoResultados.getIdCodigoFactura()!=null)
			{
				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_va_p_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");	        
		        cadenaSQL.append(" importeBruto, ");
		        cadenaSQL.append(" importeBrutoLocal, ");
		        
		        cadenaSQL.append(" baseDto1, ");
        		cadenaSQL.append(" porcentajeDto1, ");
				cadenaSQL.append(" importeDto1, ");
				cadenaSQL.append(" baseDto2, ");
				cadenaSQL.append(" porcentajeDto2, ");
				cadenaSQL.append(" importeDto2, ");
				
		        cadenaSQL.append(" base, ");
		        cadenaSQL.append(" porcentaje, ");
		        cadenaSQL.append(" cuota, ");
		        cadenaSQL.append(" base2, ");
		        cadenaSQL.append(" porcentaje2, ");
		        cadenaSQL.append(" cuota2, ");
		        
		        cadenaSQL.append(" baseRE, ");
		        cadenaSQL.append(" porcentajeRE, ");
		        cadenaSQL.append(" cuotaRE, ");
		        
		        cadenaSQL.append(" totalFactura, ");
		        cadenaSQL.append(" totalFacturaLocal, ");
		        cadenaSQL.append(" idCodigo ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			    
			    conInsert= this.conManager.establecerConexion();	
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoFactura());
			    
		    	preparedStatement.setDouble(5, mapeoResultados.getImporteBruto());			    
			    preparedStatement.setDouble(6, mapeoResultados.getImporteBrutoLocal());
			    
			    preparedStatement.setDouble(7, mapeoResultados.getBaseDto1());
			    preparedStatement.setDouble(8, mapeoResultados.getPorcentajeDto1());
			    preparedStatement.setDouble(9, mapeoResultados.getImporteDto1());
			    preparedStatement.setDouble(10, mapeoResultados.getBaseDto2());
			    preparedStatement.setDouble(11, mapeoResultados.getPorcentajeDto2());
			    preparedStatement.setDouble(12, mapeoResultados.getImporteDto2());
			    
			    preparedStatement.setDouble(13, mapeoResultados.getBase());
			    preparedStatement.setDouble(14, mapeoResultados.getPorcentaje());
			    preparedStatement.setDouble(15, mapeoResultados.getCuota());
			    preparedStatement.setDouble(16, mapeoResultados.getBase2());
			    preparedStatement.setDouble(17, mapeoResultados.getPorcentaje2());
			    preparedStatement.setDouble(18, mapeoResultados.getCuota2());
			    
			    preparedStatement.setDouble(19, mapeoResultados.getBaseRE());
			    preparedStatement.setDouble(20, mapeoResultados.getPorcentajeRE());
			    preparedStatement.setDouble(21, mapeoResultados.getCuotaRE());
			    
			    preparedStatement.setDouble(22, mapeoResultados.getTotalFactura());
			    preparedStatement.setDouble(23, mapeoResultados.getTotalFacturaLocal());		    
			    preparedStatement.setInt(24, r_mapeo.getIdCodigo());
	
		        preparedStatement.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return false;
		}		
		
		return true;
	}
	
	public String generarInforme(Integer codigoInterno, Integer numeroDocumento, String r_area, Integer r_cliente)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(codigoInterno);
    	libImpresion.setArchivoDefinitivo("/factura-" + numeroDocumento.toString() + RutinasFechas.horaActualSinSeparador()+ ".pdf");
    	
    	String strArchivoPlantilla = this.recuperarFormato(r_cliente, r_area);
    	
    	libImpresion.setArchivoPlantilla(strArchivoPlantilla);
    	libImpresion.setArchivoPlantillaDetalle("impresoFacturaNacional_subreport0.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("impresoFacturaNacional_subreport0.jasper");
    	libImpresion.setArchivoPlantillaNotas("impresoFactura_notas.jrxml");
    	libImpresion.setArchivoPlantillaNotasCompilado("impresoFactura_notas.jasper");
    	libImpresion.setCarpeta("facturasVenta");
    	libImpresion.setBackGround("/fondoA4FacturaVentaNacional.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(a_va_c_0.idCodigo) opc_sig ");
     	cadenaSQL.append(" FROM a_va_c_0 ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("opc_sig")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public boolean actualizarCabeceraGreensys(MapeoFacturasVentas r_mapeo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		RutinasEjercicios rej = new RutinasEjercicios();
		
		Integer digito = rej.cargarDigitoEjercicio(new Integer(r_mapeo.getEjercicio()));
		String tabla = "a_va_c_" + digito;
		
		try
		{
	        
			cadenaSQL.append(" UPDATE " + tabla + " set ");
    		cadenaSQL.append(" cl_listada_ruta = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento_factura = ? ");
	        cadenaSQL.append(" and serie_factura = ? ");
	        cadenaSQL.append(" and num_factura = ? ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getEnviado());
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigoFactura());

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		return true;
	}
	
	public String semaforos()
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL =null;
		String errores = "0";
		
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento_factura cc_dc, ");
	        cadenaSQL.append(" a.serie_factura cc_sr, ");
	        cadenaSQL.append(" a.num_factura cc_cd, ");	    
	        cadenaSQL.append(" a.fecha_factura cc_fec, ");
	        cadenaSQL.append(" a.ref_cliente cc_ref, ");
	        cadenaSQL.append(" a.cif cc_cif, ");
	        cadenaSQL.append(" a.ejercicio cc_ej, ");
	        cadenaSQL.append(" a.cliente cc_cli, ");
	        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" a.cl_listada_ruta cc_se, ");        
	        cadenaSQL.append(" f.texto cl_mai, ");
	        cadenaSQL.append(" g.texto cl_mai2, ");        
	        cadenaSQL.append(" h.texto cl_mai3, ");
	        cadenaSQL.append(" a.direccion cc_dir, ");
	        cadenaSQL.append(" a.poblacion cc_pob, ");
	        cadenaSQL.append(" a.cod_postal cc_cp, ");
	        cadenaSQL.append(" a.fecha_documento cc_pe, ");
	        cadenaSQL.append(" e.fax cc_fax, ");
	        cadenaSQL.append(" e.estadistico_b1 cc_tip, ");
	        cadenaSQL.append(" c.nombre cc_pa, ");
	        cadenaSQL.append(" d.nombre cc_pv, ");
	        cadenaSQL.append(" b.primer_vto cc_vto, ");
	        cadenaSQL.append(" e.dia_pago1 cc_d1, ");
	        cadenaSQL.append(" e.dia_pago2 cc_d2, ");
	        cadenaSQL.append(" e.dia_pago3 cc_d3, ");
	        cadenaSQL.append(" m.nemotecnico div_nem, ");
	        cadenaSQL.append(" b.descripcion cc_fp ");
	        
	     	cadenaSQL.append(" FROM a_va_c_0 a, e_frmpag b, e_paises c, e_provin d, e_client e, e_divisa m, outer e_cli_nt f, outer e_cli_nt g, outer e_cli_nt h ");
	     	cadenaSQL.append(" where e.cliente = a.cliente and e.estadistico_b1 <> 'N' ");
	     	cadenaSQL.append(" and c.pais = a.pais and m.numero=a.moneda ");
	     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
	     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
	     	cadenaSQL.append(" and f.cliente = a.cliente and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
	     	cadenaSQL.append(" and g.cliente = a.cliente and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
	     	cadenaSQL.append(" and h.cliente = a.cliente and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
	     	cadenaSQL.append(" and a.num_factura is not null ");
	     	cadenaSQL.append(" and a.documento_factura = 'N'" );
	     	cadenaSQL.append(" and a.cl_listada_ruta = 'N'" );
	     	
	     	cadenaSQL.append(" UNION " );
	     	
	     	cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento_factura cc_dc, ");
	        cadenaSQL.append(" a.serie_factura cc_sr, ");
	        cadenaSQL.append(" a.num_factura cc_cd, ");	    
	        cadenaSQL.append(" a.fecha_factura cc_fec, ");
	        cadenaSQL.append(" a.ref_cliente cc_ref, ");
	        cadenaSQL.append(" a.cif cc_cif, ");
	        cadenaSQL.append(" a.ejercicio cc_ej, ");
	        cadenaSQL.append(" a.cliente cc_cli, ");
	        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" a.cl_listada_ruta cc_se, ");        
	        cadenaSQL.append(" f.texto cl_mai, ");
	        cadenaSQL.append(" g.texto cl_mai2, ");        
	        cadenaSQL.append(" h.texto cl_mai3, ");
	        cadenaSQL.append(" a.direccion cc_dir, ");
	        cadenaSQL.append(" a.poblacion cc_pob, ");
	        cadenaSQL.append(" a.cod_postal cc_cp, ");
	        cadenaSQL.append(" a.fecha_documento cc_pe, ");
	        cadenaSQL.append(" e.fax cc_fax, ");
	        cadenaSQL.append(" e.estadistico_b1 cc_tip, ");
	        cadenaSQL.append(" c.nombre cc_pa, ");
	        cadenaSQL.append(" d.nombre cc_pv, ");
	        cadenaSQL.append(" b.primer_vto cc_vto, ");
	        cadenaSQL.append(" e.dia_pago1 cc_d1, ");
	        cadenaSQL.append(" e.dia_pago2 cc_d2, ");
	        cadenaSQL.append(" e.dia_pago3 cc_d3, ");
	        cadenaSQL.append(" m.nemotecnico div_nem, ");
	        cadenaSQL.append(" b.descripcion cc_fp ");
	        
	     	cadenaSQL.append(" FROM a_va_c_1 a, e_frmpag b, e_paises c, e_provin d, e_client e, e_divisa m, outer e_cli_nt f, outer e_cli_nt g, outer e_cli_nt h ");
	     	cadenaSQL.append(" where e.cliente = a.cliente and e.estadistico_b1 <> 'N' ");
	     	cadenaSQL.append(" and c.pais = a.pais and m.numero=a.moneda ");
	     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
	     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
	     	cadenaSQL.append(" and f.cliente = a.cliente and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
	     	cadenaSQL.append(" and g.cliente = a.cliente and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
	     	cadenaSQL.append(" and h.cliente = a.cliente and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
	     	cadenaSQL.append(" and a.num_factura is not null ");
	     	cadenaSQL.append(" and a.documento_factura = 'N'" );
	     	cadenaSQL.append(" and a.cl_listada_ruta = 'N'" );
	     	
			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				errores="2";
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return errores;
	}
	
	
	private String calculaVencimiento(String r_fechaFactura,Integer r_diasVencimiento, Integer r_dias1, Integer r_dias2, Integer r_dias3)
	{
		String vencimiento = null;
		
		/*
		 * calculo priemr vencimiento, que es el unico que se imprime
		 * 
		 * 1.- A la fecha factura le sumo los dias que han de pasar hasta el primer vencimiento
		 * 2.- Del dia resultante comparo con los dias de pago para echar el vencimiento al dia de pago correspondiente
		 *     Si el primer dia de pago es distinto de 0 no sigo mirando
		 */
		
		String fechaVencimiento = null;
		Integer diaFactura= null;
		Integer mesFactura = null;
		Integer yearFactura = null;

		Integer diaVencimiento = null;
		Integer mesVencimiento = null;
		Integer yearVencimiento = null;
		
		r_diasVencimiento = r_diasVencimiento + this.calcularCorrctor(r_fechaFactura, r_diasVencimiento);
		
		try 
		{
			diaFactura = new Integer(RutinasFechas.diaFecha(RutinasFechas.conversionDeString(r_fechaFactura)));
			mesFactura = new Integer(RutinasFechas.mesFecha(RutinasFechas.conversionDeString(r_fechaFactura)));
			yearFactura = new Integer(RutinasFechas.añoFecha(RutinasFechas.conversionDeString(r_fechaFactura)));
			
			if (diaFactura==31)
			{
				r_fechaFactura = RutinasFechas.restarDiasFecha(r_fechaFactura, 1);
				fechaVencimiento = RutinasFechas.sumarDiasFecha(r_fechaFactura, r_diasVencimiento);
				diaVencimiento = new Integer(RutinasFechas.diaFecha(RutinasFechas.conversionDeString(fechaVencimiento)));
			}
			else if (diaFactura==28 && mesFactura==2)
			{
				fechaVencimiento = RutinasFechas.sumarDiasFecha(r_fechaFactura, r_diasVencimiento);
				diaVencimiento = 30;
			}
			else if (diaFactura==29 && mesFactura==2)
			{
				fechaVencimiento = RutinasFechas.sumarDiasFecha(r_fechaFactura, r_diasVencimiento);
				diaVencimiento = 30;
			}
			else
			{
				fechaVencimiento = RutinasFechas.sumarDiasFecha(r_fechaFactura, r_diasVencimiento);
				diaVencimiento = new Integer(RutinasFechas.diaFecha(RutinasFechas.conversionDeString(fechaVencimiento)));
			}
			
			mesVencimiento = new Integer(RutinasFechas.mesFecha(RutinasFechas.conversionDeString(fechaVencimiento)));
			yearVencimiento = new Integer(RutinasFechas.añoFecha(RutinasFechas.conversionDeString(fechaVencimiento)));
			
			if (r_dias1==0)
			{
				vencimiento = fechaVencimiento;
			}
			else
			{
				if (diaVencimiento<=r_dias1)
				{
					diaVencimiento = r_dias1;
				}
				else 
				{
					if (r_dias2==0)
					{
						diaVencimiento = r_dias1;
						mesVencimiento = mesVencimiento + 1;
						if (mesVencimiento>12)
						{
							yearVencimiento = yearVencimiento+1;
							mesVencimiento = 1;
						}
					}
					else
					{
						if (diaVencimiento<=r_dias2)
						{
							diaVencimiento = r_dias2;
						}
						else 
						{
							if (r_dias3==0)
							{
								diaVencimiento = r_dias1;
								mesVencimiento = mesVencimiento + 1;
								if (mesVencimiento>12)
								{
									yearVencimiento = yearVencimiento+1;
									mesVencimiento = 1;
								}
							}
							else
							{
								if (diaVencimiento<=r_dias3)
								{
									diaVencimiento = r_dias3;
								}
								else
								{
									diaVencimiento = r_dias1;
									mesVencimiento = mesVencimiento + 1;
									if (mesVencimiento>12)
									{
										yearVencimiento = yearVencimiento+1;
										mesVencimiento = 1;
									}
								}
							}
						}
					}
				}
				vencimiento = String.valueOf(diaVencimiento) + "/" + String.valueOf(mesVencimiento) + "/" + String.valueOf(yearVencimiento);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return vencimiento;
	}
	
	private void establecerTablas(String r_ejercicio)
	{
		RutinasEjercicios rej = new RutinasEjercicios();
				
		Integer digito = rej.cargarDigitoEjercicio(new Integer(r_ejercicio));
		this.tablaCabecera = "a_va_c_" + digito;
		this.tablaLineas = "a_lin_" + digito;
		this.tablaPies = "a_va_p_" + digito;
	}
	
	private Integer calcularCorrctor(String r_fechaFactura,Integer r_diasVencimiento)
	{
		Integer corrector= null;
		Integer meses = null;
		Integer mesFactura=null;
		
		mesFactura = new Integer(RutinasFechas.mesFecha(RutinasFechas.conversionDeString(r_fechaFactura)));
		meses = r_diasVencimiento/30;
		corrector = 0;
		
		if (meses>0)
		{
			for (int i=0;i<meses;i++)
			{
				if (mesFactura == 1 || mesFactura==3 || mesFactura==5 || mesFactura==7 || mesFactura ==8 || mesFactura==10 || mesFactura==12)
				{
					corrector = corrector+1;
				}
				else if (mesFactura==2)
				{
					corrector = corrector-2;
				}
				mesFactura = mesFactura + 1;
			}
		}
		else
		{
			corrector = 0;
		}
		return corrector;
	}
}
