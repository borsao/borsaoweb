package borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.vaadin.haijian.ExcelExporter;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.modelo.MapeoStockExternos;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.server.consultaStockExternosServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaStockExternosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Inventario Externos ";
	private final String titulo = "Inventario Externos ";
	private final int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean generado = false;
	public ComboBox cmbAlmacen = null;
	public HashMap<String , String> opcionesEscogidas;
	public ExcelExporter excelExporter=null;
	private Table tabla = null;
	private consultaStockExternosServer ccs  = null;
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaStockExternosView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	opcionesEscogidas = new HashMap<String , String>();
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.cmbAlmacen= new ComboBox("Almacen"); 
		this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbAlmacen.setNewItemsAllowed(false);
		this.cmbAlmacen.setNullSelectionAllowed(false);

		this.cabLayout.addComponent(this.cmbAlmacen);
		
		excelExporter = new ExcelExporter();
	  	
		excelExporter.setDateFormat("yyyy-MM-dd");
		excelExporter.setStyleName(ValoTheme.BUTTON_PRIMARY);
		excelExporter.setStyleName(ValoTheme.BUTTON_TINY);
		excelExporter.setIcon(FontAwesome.DOWNLOAD);
		excelExporter.setCaption("Export to Excel");
		excelExporter.setDownloadFileName( "Stock Almacenes Externos");

		excelExporter.addClickListener(new ClickListener() {
	        @Override
	        public void buttonClick(ClickEvent event) {
	        	tabla = generarTablaExcel();
	    		excelExporter.setTableToBeExported(tabla);
	        }
		});

		topLayoutR.addComponent(excelExporter);
		topLayoutR.setComponentAlignment(excelExporter, Alignment.MIDDLE_RIGHT);

		
		this.cargarCombo();
		this.cargarListeners();

		
		
		opcionesEscogidas.put("almacen", this.cmbAlmacen.getValue().toString());
		this.generarGrid(opcionesEscogidas);

//    	this.barAndGridLayout.removeComponent(this.cabLayout);
//    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoStockExternos> r_vector=null;
    	this.ccs = new consultaStockExternosServer(CurrentUser.get());
    	boolean almacenExterno = false;
    	
    	almacenExterno=this.ccs.almacenExterno(opcionesEscogidas.get("almacen"));
//    	
    	Integer cli = null;
    	String e_cli=null;
    	try {
    		cli = new Integer(eBorsao.get().accessControl.getCodigoGestion());
    		e_cli=eBorsao.get().accessControl.getCodigoGestion();
    	}
    	catch (Exception ex)
    	{
    		cli = null;
    		e_cli=null;
    	}
    	r_vector=this.ccs.datosOpcionesGlobal(opcionesEscogidas.get("almacen"), e_cli, almacenExterno, eBorsao.get().accessControl.getDepartamento());
    	
    	grid = new OpcionesGrid(this,r_vector,almacenExterno);
    	
    	((OpcionesGrid) grid).asignarTitulo("Stock en " + this.ccs.obtenerDescripcionAlmacen(opcionesEscogidas.get("almacen")));
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    	}
    }
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }

    private void cargarListeners()
    {
		this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					opcionesEscogidas=null;
					
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().length()>0) 
					{
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("almacen", cmbAlmacen.getValue().toString());
					}
					
					generarGrid(opcionesEscogidas);
				}
			}
		}); 
    }
    

    private void cargarCombo()
    {
    	ArrayList<String> arrAlmacenes =null;
    	consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
    	
    	this.cmbAlmacen.removeAllItems();
    	
    	arrAlmacenes= cas.obtenerAlmacenes();
    	
    	for (int i = 0; i<arrAlmacenes.size();i++)
    	{
    		this.cmbAlmacen.addItem(arrAlmacenes.get(i));
    	}
    	
    	this.cmbAlmacen.setValue("20");
    }
    
    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(false);
    }
    public void print() {
    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;

    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}
	
	private Table generarTablaExcel()
	{
		Table table = new Table();
		table.setContainerDataSource(this.grid.getContainerDataSource());
		table.setVisibleColumns(new Object[]{"fechaPedido", "pedido", "pedidoCliente", "articulo","descripcion","stock_pedido", "cajas", "stock_actual","cajasActual", "precio", "importe"});
		return table;
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
