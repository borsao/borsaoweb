package borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.modelo.MapeoStockExternos;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaStockExternosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Connection conMysql = null;
	private Statement cs = null;
	private static consultaStockExternosServer instance;
	private ArrayList<MapeoLineasAlbaranesTraslado> vectorTotales = null;	
	
	public consultaStockExternosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaStockExternosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaStockExternosServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionAlmacen(String r_almacen)
	{
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		return cas.obtenerDescripcionAlmacen(r_almacen);
	}
	
	public boolean almacenExterno(String r_almacen)
	{
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		return cas.obtenerAlmacenExterno(r_almacen);
	}

	private String obtenerNombreCliente(Integer r_cliente)
	{
		consultaClientesServer ccs = consultaClientesServer.getInstance(CurrentUser.get());
		return ccs.obtenerNombreCliente(r_cliente);
	}

	/*
	 * JAVI
	 */
	
	public ArrayList<MapeoStockExternos> datosOpcionesGlobal(String r_almacen, String r_codigo, boolean almacenExterno, String depart)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoStockExternos> vector = null;
		
		consultaAlbaranesTrasladoServer cas = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		
		StringBuffer cadenaSQL = new StringBuffer();
				
		try
		{
			/*
			 *  
			 * 
			 */
			this.vectorTotales=cas.datosOpcionesEnviosTotales(new Integer(r_almacen));
			
			if ((depart==null || depart.length()==0) && (r_codigo==null || r_codigo.length()==0)) return null;
			
			cadenaSQL.append(" SELECT a.articulo pe_art, ");
	        cadenaSQL.append(" a.descrip_articulo pe_des, ");
	        cadenaSQL.append(" a.almacen pe_alm, ");
	        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
	        cadenaSQL.append(" a.precio pe_pre, ");
	        cadenaSQL.append(" a.bultos pe_caj, ");
	        cadenaSQL.append(" a.unidades pe_upe, ");
	        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
	        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot, ");
	        cadenaSQL.append(" a.fecha_documento pe_fec, ");
	        cadenaSQL.append(" a.codigo pe_ped, ");
	        cadenaSQL.append(" b.ref_cliente pe_ref ");
	        
	     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
	     	cadenaSQL.append(" where a.codigo = b.codigo ");	     	
	     	if (almacenExterno) cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_0 where almacen_destino = " + r_almacen + ") " );
	     	cadenaSQL.append(" and a.almacen = " + r_almacen );
	     	if (r_codigo!=null && r_codigo.length()>0)  cadenaSQL.append(" and b.cliente = " + r_codigo);
	     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
	     	cadenaSQL.append(" and a.cumplimentacion is null and a.unidades - a.unidades_serv <> 0 " );
	     	
	     	cadenaSQL.append(" UNION " );
	     	
	     	cadenaSQL.append(" SELECT a.articulo pe_art, ");
	        cadenaSQL.append(" a.descrip_articulo pe_des, ");
	        cadenaSQL.append(" a.almacen pe_alm, ");
	        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
	        cadenaSQL.append(" a.precio pe_pre, ");
	        cadenaSQL.append(" a.bultos pe_caj, ");
	        cadenaSQL.append(" a.unidades pe_upe, ");
	        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
	        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot, ");
	        cadenaSQL.append(" a.fecha_documento pe_fec, ");
	        cadenaSQL.append(" a.codigo pe_ped, ");
	        cadenaSQL.append(" b.ref_cliente pe_ref ");
	        
	     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
	     	cadenaSQL.append(" where a.codigo = b.codigo ");	     	
	     	if (almacenExterno) cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_1 where almacen_destino = " + r_almacen + ") " );
	     	cadenaSQL.append(" and a.almacen = " + r_almacen );
	     	if (r_codigo!=null && r_codigo.length()>0) cadenaSQL.append(" and b.cliente = " + r_codigo);
	     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
	     	cadenaSQL.append(" and a.cumplimentacion is null and a.unidades - a.unidades_serv <> 0 " );
//	     	cadenaSQL.append(" order by 1 ");

	     	this.con= this.conManager.establecerConexionGestion();			
	     	this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoStockExternos>();
			
			while(rsOpcion.next())
			{
				MapeoStockExternos mapeoStockExternos = new MapeoStockExternos();
				/*
				 * recojo mapeo operarios
				 */
				
				//Habria que recoger el formato de caja (6/12). Así calculamos las cajas 
				//Habria que recoger el lote del articulo
				
				mapeoStockExternos.setArticulo(rsOpcion.getString("pe_art"));
				mapeoStockExternos.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pe_des")));
				mapeoStockExternos.setStock_pedido(rsOpcion.getInt("pe_upe"));
				mapeoStockExternos.setStock_actual(rsOpcion.getInt("pe_uds"));
				mapeoStockExternos.setAlmacen(rsOpcion.getInt("pe_alm"));
				mapeoStockExternos.setCajas(rsOpcion.getInt("pe_caj"));
				mapeoStockExternos.setCajasActual(rsOpcion.getInt("pe_uds")/(rsOpcion.getInt("pe_upe")/rsOpcion.getInt("pe_caj")));
				mapeoStockExternos.setPrecio(rsOpcion.getDouble("pe_pre"));
				if (RutinasCadenas.conversion(rsOpcion.getString("pe_des")).contains("LOS DOS"))
				{
					mapeoStockExternos.setImporte(rsOpcion.getDouble("pe_imp_bot"));
				}
				else
				{
					mapeoStockExternos.setImporte(rsOpcion.getDouble("pe_imp_caj"));
				}
				mapeoStockExternos.setFechaPedido(RutinasFechas.convertirADate(rsOpcion.getString("pe_fec")));
				mapeoStockExternos.setPedido(rsOpcion.getInt("pe_ped"));
				mapeoStockExternos.setPedidoCliente(rsOpcion.getString("pe_ref"));
				
				mapeoStockExternos.setCorrecto(this.buscarEnvioCorrecto(rsOpcion.getString("pe_ref"), rsOpcion.getString("pe_art"), rsOpcion.getInt("pe_upe")));
				
				vector.add(mapeoStockExternos);				
			}
			

		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	private boolean buscarEnvioCorrecto(String r_ref, String r_articulo, Integer r_uds)
	{
		boolean rdo = true;
		MapeoLineasAlbaranesTraslado mapeo = null;
		
		for (int i=0;i<this.vectorTotales.size();i++)
		{
			mapeo=this.vectorTotales.get(i);
			
			if (r_ref!=null && r_articulo!=null && mapeo.getArticulo()!=null && mapeo.getReferencia()!=null && mapeo.getArticulo().trim().equals(r_articulo.trim()) && mapeo.getReferencia().trim().equals(r_ref.trim()))
			{
				if (!mapeo.getUnidades().equals(r_uds)) 
				{
					rdo=false;
				}
			}
		}
		return rdo;
				
	}
	

	
	
	public String generarInforme(ArrayList<MapeoStockExternos> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

		if (regenerar)
		{
			
//			this.eliminar();
			
			for (int i=0; i< r_vector.size();i++)
			{
				MapeoStockExternos mapeo = (MapeoStockExternos) r_vector.get(i);
//				this.guardarNuevo(mapeo);	
			}
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/inventarioExternos" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("inventarioExternos.jrxml");
    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado("inventarioExternos.jasper");
    	libImpresion.setCarpeta("existencias");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}

	@Override
	public String semaforos() 
	{
		ResultSet rsOpcion = null;		
		
		consultaAlbaranesTrasladoServer cts = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		ArrayList<String> almacenes = cas.obtenerAlmacenesExternos();
		
		
		for (int i=0; i< almacenes.size();i++)
		{
			this.vectorTotales=cts.datosOpcionesEnviosTotales(new Integer(almacenes.get(i)));
			
			StringBuffer cadenaSQL = new StringBuffer();
			try
			{
				cadenaSQL.append(" SELECT a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot, ");
		        cadenaSQL.append(" a.fecha_documento pe_fec, ");
		        cadenaSQL.append(" a.codigo pe_ped, ");
		        cadenaSQL.append(" b.ref_cliente pe_ref ");
		        
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");	     	
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_0 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is null and a.unidades - a.unidades_serv <> 0 " );
		     	
		     	cadenaSQL.append(" union ");
		     	
		     	cadenaSQL.append(" SELECT a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot, ");
		        cadenaSQL.append(" a.fecha_documento pe_fec, ");
		        cadenaSQL.append(" a.codigo pe_ped, ");
		        cadenaSQL.append(" b.ref_cliente pe_ref ");
		        
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");	     	
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_1 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is null and a.unidades - a.unidades_serv <> 0 " );
		     	
				con= this.conManager.establecerConexionGestion();			
				cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					if (!this.buscarEnvioCorrecto(rsOpcion.getString("pe_ref"), rsOpcion.getString("pe_art"), rsOpcion.getInt("pe_upe")))
					{
						return "2";
					}
				}
			}
			catch (Exception e) 
			{
				System.out.println(e.getMessage());
			}
		}
		return "0";
	}
	
	public ArrayList<MapeoLineasPedidosVentas> PedidosMalCumplimentados()
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = null;
		ArrayList<MapeoLineasPedidosVentas> vector = null;
		MapeoLineasPedidosVentas mapeo = null;
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		ArrayList<String> almacenes = cas.obtenerAlmacenesExternos();
		
		
		for (int i=0; i< almacenes.size();i++)
		{
			cadenaSQL = new StringBuffer();
			try
			{
				cadenaSQL.append(" SELECT b.codigo pe_cod, ");
				cadenaSQL.append(" b.ref_cliente pe_ref, ");
				cadenaSQL.append(" year(a.fecha_documento) pe_eje, ");
				cadenaSQL.append(" a.fecha_documento pe_fec, ");
				cadenaSQL.append(" a.num_linea pe_lin, ");
				cadenaSQL.append(" a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot ");
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");
		     	cadenaSQL.append(" and (a.observacion is null or a.observacion <> 'OK') ");	     	
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_0 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is not null and a.unidades - a.unidades_serv <> 0 " );
		     	
		     	cadenaSQL.append(" UNION ");
		     	
		     	cadenaSQL.append(" SELECT b.codigo pe_cod, ");
				cadenaSQL.append(" b.ref_cliente pe_ref, ");
				cadenaSQL.append(" year(a.fecha_documento) pe_eje, ");
				cadenaSQL.append(" a.fecha_documento pe_fec, ");
				cadenaSQL.append(" a.num_linea pe_lin, ");
				cadenaSQL.append(" a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot ");
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");
		     	cadenaSQL.append(" and (a.observacion is null or a.observacion <> 'OK') ");
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_1 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is not null and a.unidades - a.unidades_serv <> 0 " );
		     	
				if (con==null) con= this.conManager.establecerConexionGestion();			
				if (cs == null) cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				vector = new ArrayList<MapeoLineasPedidosVentas>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoLineasPedidosVentas();
					mapeo.setEjercicio(rsOpcion.getInt("pe_eje"));
					mapeo.setNum_linea(rsOpcion.getInt("pe_lin"));
					mapeo.setFechaDocumento(rsOpcion.getDate("pe_fec"));
					mapeo.setArticulo(rsOpcion.getString("pe_art"));
					mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pe_des")));
					mapeo.setIdCodigoVenta(rsOpcion.getInt("pe_cod"));
					mapeo.setUnidades(rsOpcion.getInt("pe_upe"));
					mapeo.setUnidades_pdte(rsOpcion.getInt("pe_uds"));

					vector.add(mapeo);
				}	
			}
				
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public String semaforosPedidosMalCumplimentados()
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = null;
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		ArrayList<String> almacenes = cas.obtenerAlmacenesExternos();
		
		
		for (int i=0; i< almacenes.size();i++)
		{
			cadenaSQL = new StringBuffer();
			try
			{
				cadenaSQL.append(" SELECT b.codigo pe_cod, ");
				cadenaSQL.append(" b.ref_cliente pe_ref, ");
				cadenaSQL.append(" a.fecha_documento pe_fec, ");
				cadenaSQL.append(" a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot ");
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");
		     	cadenaSQL.append(" and (a.observacion is null or a.observacion <> 'OK') ");
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_0 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is not null and a.unidades - a.unidades_serv <> 0 " );
		     	
		     	cadenaSQL.append(" UNION ");
		     	
		     	cadenaSQL.append(" SELECT b.codigo pe_cod, ");
				cadenaSQL.append(" b.ref_cliente pe_ref, ");
				cadenaSQL.append(" a.fecha_documento pe_fec, ");
				cadenaSQL.append(" a.articulo pe_art, ");
		        cadenaSQL.append(" a.descrip_articulo pe_des, ");
		        cadenaSQL.append(" a.almacen pe_alm, ");
		        cadenaSQL.append(" a.unidades - a.unidades_serv pe_uds, ");
		        cadenaSQL.append(" a.precio pe_pre, ");
		        cadenaSQL.append(" a.bultos pe_caj, ");
		        cadenaSQL.append(" a.unidades pe_upe, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv)/a.etiquetas * a.precio pe_imp_caj, ");
		        cadenaSQL.append(" (a.unidades - a.unidades_serv) * a.precio pe_imp_bot ");
		     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
		     	cadenaSQL.append(" where a.codigo = b.codigo ");	     	
		     	cadenaSQL.append(" and (a.observacion is null or a.observacion <> 'OK') ");
		     	cadenaSQL.append(" and b.ref_cliente in (select apartado from a_tr_c_1 where almacen_destino = " + almacenes.get(i) + ") " );
		     	cadenaSQL.append(" and a.almacen = " + almacenes.get(i) );
		     	cadenaSQL.append(" and ( a.articulo matches '0102*' or a.articulo matches '0111*' ) " );
		     	cadenaSQL.append(" and a.cumplimentacion is not null and a.unidades - a.unidades_serv <> 0 " );
		     	
				if (con==null) con= this.conManager.establecerConexionGestion();			
				if (cs == null) cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				while(rsOpcion.next())
				{
					Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("pe_cod"));
					Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("pe_alm"));
					Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("pe_art"));
					return "2";				
				}
			}
				
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
				ex.printStackTrace();
			}
		}
		return "0";
	}

}
