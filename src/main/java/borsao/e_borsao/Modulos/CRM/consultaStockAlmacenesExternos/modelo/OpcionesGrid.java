package borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view.pantallaLineasAlbaranesTraslados;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.server.consultaStockExternosServer;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.view.consultaStockExternosView;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;
	private consultaStockExternosView padre =null;
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean conTotales=true;
	
	public boolean externoSN=false;
	
    public OpcionesGrid(consultaStockExternosView app, ArrayList<MapeoStockExternos> r_vector, boolean r_externo) 
    {
    	this.externoSN=r_externo;
        this.padre=app;
        this.vector=r_vector;
        
		this.asignarTitulo("Stock Almacenes Externos ");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("stock_actual");
    	this.camposNoFiltrar.add("stock_pedido");
    	this.camposNoFiltrar.add("precio");
    	this.camposNoFiltrar.add("importe");
    	this.camposNoFiltrar.add("cajas");
    	this.camposNoFiltrar.add("cajasActual");
    	this.camposNoFiltrar.add("traslados");
    	this.camposNoFiltrar.add("correcto");    	
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
    	this.setConTotales(this.conTotales);
		this.crearGrid(MapeoStockExternos.class);
		this.setRecords(this.vector);
//		this.setStyleName("programacion");
		
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		
		this.calcularTotal();
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("correcto", "traslados", "albaranes", "fechaPedido", "pedido", "pedidoCliente", "articulo","descripcion","stock_pedido", "cajas", "stock_actual","cajasActual", "precio", "importe");
    }
    
    public void establecerTitulosColumnas()
    {

    	this.widthFiltros.put("fechaPedido", "100");
    	this.widthFiltros.put("articulo", "100");
    	this.widthFiltros.put("descripcion", "225");
    	this.widthFiltros.put("pedido", "90");
    	this.widthFiltros.put("pedidoCliente", "100");

    	this.getColumn("correcto").setHeaderCaption("");
    	this.getColumn("correcto").setSortable(false);
    	this.getColumn("correcto").setWidth(new Double(40));
    	this.getColumn("traslados").setHeaderCaption("");
    	this.getColumn("traslados").setSortable(false);
    	this.getColumn("traslados").setWidth(new Double(40));
    	this.getColumn("albaranes").setHeaderCaption("");
    	this.getColumn("albaranes").setSortable(false);
    	this.getColumn("albaranes").setWidth(new Double(40));

    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("cliente").setHidden(true);
    	this.getColumn("nombre").setHidden(true);
    	this.getColumn("fechaSalida").setHidden(true);
    	this.getColumn("correcto").setHidden(true);
    	this.getColumn("albaranes").setHidden(true);
    	
    	this.getColumn("fechaPedido").setHeaderCaption("Fecha Pedido");
    	this.getColumn("fechaPedido").setWidth(new Double(140));
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(140));
    	this.getColumn("pedido").setHeaderCaption("Pedido");
    	this.getColumn("pedido").setWidth(new Double(120));
    	this.getColumn("pedidoCliente").setHeaderCaption("Ref. Cliente");
    	this.getColumn("pedidoCliente").setWidth(new Double(140));

    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
//    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_pedido").setHeaderCaption("Pedidas");
    	this.getColumn("stock_pedido").setWidth(new Double(125));
    	
    	this.getColumn("cajas").setHeaderCaption("Cajas");
    	this.getColumn("cajas").setWidth(new Double(90));

    	this.getColumn("stock_actual").setHeaderCaption("Disponible");
    	this.getColumn("stock_actual").setWidth(new Double(125));
    	this.getColumn("cajasActual").setHeaderCaption("Cajas Disp.");
    	this.getColumn("cajasActual").setWidth(new Double(90));

    	this.getColumn("precio").setHeaderCaption("Precio");
    	this.getColumn("precio").setWidth(new Double(90));
    	this.getColumn("importe").setHeaderCaption("Importe");
    	this.getColumn("importe").setWidth(new Double(125));
	}
	
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoStockExternos mapeo = (MapeoStockExternos) event.getItemId();
            		
	            	if (event.getPropertyId().toString().equals("albaranes"))
	            	{
//	            		activadaVentanaPeticion=true;
//	            		pantallaConsultaLineasVentas vt = null;
//						vt = new pantallaConsultaLineasVentas("Salidas del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getArticulo());
//	            		getUI().addWindow(vt);
	            	}
	            	else if (event.getPropertyId().toString().equals("traslados"))
	            	{
//	            		activadaVentanaPeticion=true;
	            		pantallaLineasAlbaranesTraslados vt = null;
	            		vt = new pantallaLineasAlbaranesTraslados("Envíos del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getAlmacen(), mapeo.getArticulo(),mapeo.getPedidoCliente());
	            		getUI().addWindow(vt);	            			
	            	}
	            	else
	            	{
//	            		activadaVentanaPeticion=false;
	            	}
	            }
    		}
        });
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoProgramacion) {
                MapeoProgramacion progRow = (MapeoProgramacion)bean;
                // The actual description text is depending on the application
                if ("traslados".equals(cell.getPropertyId()))
                {
                	descriptionText = "Lineas Envio";
                }
                else if ("albaranes".equals(cell.getPropertyId()))
                {
                	descriptionText = "Lineas facturadas";
                }
            }
        }
        return descriptionText;
    }
    
    public void asignarEstilos()
    {
    	asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String error = "";
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	String estilo = "cell-normal";
            	
            	
            	if ("correcto".equals(cellReference.getPropertyId()))
    			{
            		if ((Boolean) cellReference.getValue()==false)
            		{
            			error="cell-error";
            		}
    			}
            	if (externoSN) 
            	{
	            	if ( "stock_pedido".equals(cellReference.getPropertyId()) || "cajas".equals(cellReference.getPropertyId()) || "precio".equals(cellReference.getPropertyId()) || "pedido".equals(cellReference.getPropertyId()))
	            	{
	            		if (error=="cell-error" && "stock_pedido".equals(cellReference.getPropertyId())) 
	            		{
	            			estilo = "R" + error;
	            			error = "";
	            		}
	            		else
	            		{
	            			estilo = "R" + estilo;
	            		}
	            	}
	            	else if ("importe".equals(cellReference.getPropertyId()))
	            	{
	            		estilo = "Rcell-warning";
	            	}
	            	else if ("stock_actual".equals(cellReference.getPropertyId()) || "cajasActual".equals(cellReference.getPropertyId()))
	            	{
	            		if (cellReference.getValue()==null)
	            		{
	            			estilo = "R" + estilo;	
	            		}
	            		else
	            		{
	            			estilo = "cell-green";
	            		}
	            	}
	            	else if ("articulo".equals(cellReference.getPropertyId()) || "descripcion".equals(cellReference.getPropertyId()))
	            	{
	            		if (cellReference.getValue()!=null && cellReference.getValue().toString().contains("TOTAL"))
	            		{
	            			estilo = "R" + estilo;	
	            		}
	            	}
            	}
            	else
            	{
            		if ( "stock_actual".equals(cellReference.getPropertyId()) || "cajasActual".equals(cellReference.getPropertyId()) || "stock_pedido".equals(cellReference.getPropertyId()) || "cajas".equals(cellReference.getPropertyId()) || "precio".equals(cellReference.getPropertyId()) || "pedido".equals(cellReference.getPropertyId()) || "importe".equals(cellReference.getPropertyId()))
	            	{
	        			estilo = "R" + estilo;
	            	}
	            	else if ("articulo".equals(cellReference.getPropertyId()) || "descripcion".equals(cellReference.getPropertyId()))
	            	{
	            		if (cellReference.getValue()!=null && cellReference.getValue().toString().contains("TOTAL"))
	            		{
	            			estilo = "R" + estilo;	
	            		}
	            	}
            	}
            	if ( "traslados".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonTraslado";
            	}
            	if ( "albaranes".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonUp";
            	}
            	return estilo;	
            }
        });
    }
    
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaStockExternosServer ccs = consultaStockExternosServer.getInstance(CurrentUser.get());
    	pdfGenerado = ccs.generarInforme((ArrayList<MapeoStockExternos>) this.vector, regenerar);
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		
		pdfGenerado = null;

    }

    public void calcularTotal()
    {
    	
    	Long total = new Long(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
			Double q1Value = (Double) item
					.getItemProperty("importe").getValue();
			total += q1Value.longValue();

        }
        footer.getCell("articulo").setText("Total Valorado");
		footer.getCell("importe").setText(RutinasNumericas.formatearDouble(total.toString()));
		this.padre.barAndGridLayout.setWidth("100%");
		this.padre.barAndGridLayout.setHeight((this.padre.getHeight()-this.padre.cabLayout.getHeight()-this.padre.lblSeparador.getHeight()-this.padre.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("importe").setStyleName("Rcell-pie");
		
    }

}

