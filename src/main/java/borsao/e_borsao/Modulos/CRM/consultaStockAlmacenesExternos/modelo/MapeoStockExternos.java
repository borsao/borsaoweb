package borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoStockExternos extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer stock_pedido;
	private Integer stock_actual;
	private Double precio;
	private Integer cajas;
	private Integer cajasActual;
	private Double importe;
	private Integer almacen;
	private Integer cliente;
	private String nombre;
	private Integer pedido;
	private String pedidoCliente;
	private Date fechaPedido;
	private Date fechaSalida;
	private String traslados=" ";
	private String albaranes=" ";
	private boolean correcto;


	public MapeoStockExternos()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getStock_actual() {
		return stock_actual;
	}

	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getCliente() {
		return cliente;
	}

	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}

	public Integer getPedido() {
		return pedido;
	}

	public void setPedido(Integer pedido) {
		this.pedido = pedido;
	}

	public String getFechaPedido() {
		if (fechaPedido!=null) return RutinasFechas.convertirDateToString(fechaPedido);
		return null;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Date getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPedidoCliente() {
		return pedidoCliente;
	}

	public void setPedidoCliente(String pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}

	public Integer getStock_pedido() {
		return stock_pedido;
	}

	public void setStock_pedido(Integer stock_pedido) {
		this.stock_pedido = stock_pedido;
	}

	public Integer getCajas() {
		return cajas;
	}

	public void setCajas(Integer cajas) {
		this.cajas = cajas;
	}

	public Integer getCajasActual() {
		return cajasActual;
	}

	public void setCajasActual(Integer cajasActual) {
		this.cajasActual = cajasActual;
	}

	public String getTraslados() {
		return traslados;
	}

	public void setTraslados(String traslados) {
		this.traslados = traslados;
	}

	public String getAlbaranes() {
		return albaranes;
	}

	public void setAlbaranes(String albaranes) {
		this.albaranes = albaranes;
	}

	public boolean isCorrecto() {
		return correcto;
	}

	public void setCorrecto(boolean correcto) {
		this.correcto = correcto;
	}
}