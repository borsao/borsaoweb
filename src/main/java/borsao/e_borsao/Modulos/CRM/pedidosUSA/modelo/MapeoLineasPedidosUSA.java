package borsao.e_borsao.Modulos.CRM.pedidosUSA.modelo;

import java.util.Date;

import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosVentas;

public class MapeoLineasPedidosUSA
{
	private String documento;
	private Integer idCodigoVenta;
	
    private String estatusLinea;
    private String estatusCantidad;
    private Date fechaEstatusCantidad;
    private Date fechaEstatusLinea;
    private MapeoLineasPedidosVentas mapeoLineasPedidosVentas = null;
    
	public MapeoLineasPedidosUSA()
	{
	}

	public MapeoLineasPedidosVentas getMapeoLineasPedidosVentas() {
		return mapeoLineasPedidosVentas;
	}

	public void setMapeoLineasPedidosVentas(MapeoLineasPedidosVentas mapeoLineasPedidosVentas) {
		this.mapeoLineasPedidosVentas = mapeoLineasPedidosVentas;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public void setIdCodigoVenta(Integer idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}

	public String getEstatusLinea() {
		return estatusLinea;
	}

	public void setEstatusLinea(String estatusLinea) {
		this.estatusLinea = estatusLinea;
	}

	public String getEstatusCantidad() {
		return estatusCantidad;
	}

	public void setEstatusCantidad(String estatusCantidad) {
		this.estatusCantidad = estatusCantidad;
	}

	public Date getFechaEstatusCantidad() {
		return fechaEstatusCantidad;
	}

	public void setFechaEstatusCantidad(Date fechaEstatusCantidad) {
		this.fechaEstatusCantidad = fechaEstatusCantidad;
	}

	public Date getFechaEstatusLinea() {
		return fechaEstatusLinea;
	}

	public void setFechaEstatusLinea(Date fechaEstatusLinea) {
		this.fechaEstatusLinea = fechaEstatusLinea;
	}

	
}