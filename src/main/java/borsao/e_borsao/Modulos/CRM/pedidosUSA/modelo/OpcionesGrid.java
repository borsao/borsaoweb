package borsao.e_borsao.Modulos.CRM.pedidosUSA.modelo;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private IndexedContainer container =null;
	
    public OpcionesGrid(ArrayList<MapeoLineasPedidosUSA> r_vector) 
    {
        
        this.vector=r_vector;
        
		this.asignarTitulo("Pedidos USA");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.setFrozenColumnCount(6);
		
		this.llenarRegistros((ArrayList<MapeoLineasPedidosUSA>) this.vector);
		
    }
    
	private void llenarRegistros(ArrayList<MapeoLineasPedidosUSA> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosUSA mapeoAyuda = null;
		iterator = r_vector.iterator();
		/*
		 * voltear mapeo para articulos por lineas
		 */
		container = new IndexedContainer();
		container.addContainerProperty("Cliente", String.class, null);
		container.addContainerProperty("Nombre", String.class, null);
		container.addContainerProperty("Referencia", String.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Plazo Entrega", String.class, null);
		container.addContainerProperty("Unidades", Integer.class, null);
		container.addContainerProperty("Servidas", Integer.class, null);
		container.addContainerProperty("Pendientes", Integer.class, null);
		container.addContainerProperty("Stock", Integer.class, null);
        container.addContainerProperty("Ejercicio", Integer.class, null);
        container.addContainerProperty("Documento", String.class, null);
        container.addContainerProperty("Codigo", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosUSA) iterator.next();
			
    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getMapeoLineasPedidosVentas().getNombreCliente()));
    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getMapeoLineasPedidosVentas().getDocumento()));
    		file.getItemProperty("Referencia").setValue(RutinasCadenas.conversion(mapeoAyuda.getMapeoLineasPedidosVentas().getReferencia()));
    		
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getMapeoLineasPedidosVentas().getFechaDocumento()));
    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getMapeoLineasPedidosVentas().getPlazoEntrega()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getIdCodigoVenta().toString());
    		file.getItemProperty("Cliente").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getCliente().toString());
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getUnidades());
    		file.getItemProperty("Servidas").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getUnidades_serv());
    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getUnidades_pdte());
    		file.getItemProperty("Stock").setValue(mapeoAyuda.getMapeoLineasPedidosVentas().getStock_queda());    		
		}

		this.setContainerDataSource(container);
		this.setSelectionMode(SelectionMode.SINGLE);

		this.asignarEstilos();
	}
    
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            }
        });
    }
    
    public void asignarEstilos()
    {
//    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
//    	{
//    		String estado = null;
//    		String programado = null;
//    		
//            public String getStyle(Grid.CellReference cellReference) 
//            {
//            	if ("programado".equals(cellReference.getPropertyId()))
//            	{
//            		if (cellReference.getValue().toString().equals("SI"))
//            			programado="SI";
//            		else
//            			programado = "NO";
//            	}
//            	if ("negativo".equals(cellReference.getPropertyId()))
//            	{
//            		if (cellReference.getValue().toString().equals("SI"))
//            			estado="Error";
//            		else if (cellReference.getValue().toString().equals("NO"))
//            			estado = "Advertencia";
//            		else
//            			estado = "Ok";
//            	}
//            	
//            	if ("stock_actual".equals(cellReference.getPropertyId()) || "stock_real".equals(cellReference.getPropertyId()) || "pdte_servir".equals(cellReference.getPropertyId()) || "stock_Jaulon".equals(cellReference.getPropertyId())
//            			|| "deposito".equals(cellReference.getPropertyId()) || "reservado".equals(cellReference.getPropertyId()) || "prepedido".equals(cellReference.getPropertyId())) 
//            	{
//            		if ("stock_real".equals(cellReference.getPropertyId()) && estado.equals("Error"))
//            		{
//            			if (programado.equals("SI"))
//            			{            				
//            				return "Rcell-7b0065P";
//            			}
//            			else
//            			{            				
//            				return "Rcell-error";
//            			}
//            		}
//        			else if ("stock_real".equals(cellReference.getPropertyId()) && estado.equals("Advertencia"))
//        			{
//            			if (programado.equals("SI"))
//            			{            				
//            				return "Rcell-07fff8P";
//            			}
//            			else
//            			{
//            				return "Rcell-normal";
//            			}
//        			}
//        			else if ("pdte_servir".equals(cellReference.getPropertyId()) || "stock_actual".equals(cellReference.getPropertyId()))
//        			{
//        				return "Rcell-pointer";
//        			}
//        			else 
//        			{
//        				return "Rcell-normal";
//        			}
//            	}
//            	else if ("mail".equals(cellReference.getPropertyId()))
//            	{
//            		return "nativebutton";
//            	}
//            	else if ( "ver".equals(cellReference.getPropertyId()))
//            	{
//            		return "nativebuttonBars";
//            	}
//            	else
//            	{
//    				return "cell-normal";
//    			}
//            }
//        });
    }
    
	@Override
	public void establecerTitulosColumnas() {
		
	}

	@Override
	public void establecerOrdenPresentacionColumnas() {
		
	}

	@Override
	public void calcularTotal() {
		
	}
}
