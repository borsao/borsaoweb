package borsao.e_borsao.Modulos.CRM.pedidosUSA.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.modelo.MapeoLineasPedidosUSA;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.server.consultaPedidosUSAServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PedidosUSAView extends GridViewRefresh {

	public static final String VIEW_NAME = "Pedidos USA";
	private final String titulo = "PEDIDOS U.S.A.";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoLineasPedidosUSA> r_vector=null;
//    	hashToMapeo hm = new hashToMapeo();
    	
//    	MapeoPedidosCompras=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaPedidosUSAServer cse = new consultaPedidosUSAServer(CurrentUser.get());
    	MapeoLineasPedidosUSA mapeoUSA = new MapeoLineasPedidosUSA();
    	mapeoUSA.setEstatusLinea("pendientes");
    	
    	r_vector=cse.datosOpcionesGlobal(mapeoUSA);
    	
    	grid = new OpcionesGrid(r_vector);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PedidosUSAView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	this.generarGrid(null);
    	this.barAndGridLayout.removeComponent(this.cabLayout);
    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    @Override
    public void reestablecerPantalla() {
    }

    public void print() 
    {
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	
	/*
	 * TODO: Codigo mulyline cell grid
	 * 
	 * Grid<Item> gridItem = new Grid<>();
gridItem.setRowHeight(50.0);
Column configuration

gridItem.addComponentColumn(item -> {
            Label label = new Label();
            label.setValue(item.getText());
            label.setWidthUndefined();
            label.setStyleName(ValoTheme.LAYOUT_HORIZONTAL_WRAPPING);
            return label;
        })
        .setCaption("Item")
        .setWidth(380.0);
	 */
}
