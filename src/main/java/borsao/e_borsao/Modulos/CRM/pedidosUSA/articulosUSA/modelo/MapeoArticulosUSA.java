package borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoArticulosUSA extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer posicion;

	public MapeoArticulosUSA()
	{
		this.setArticulo("");
		this.setDescripcion("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	
}