package borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulosUSA mapeo = null;
	 
	 public MapeoArticulosUSA convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosUSA();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 if (r_hash.get("posicion")!="")
		 {
			 this.mapeo.setPosicion(new Integer(r_hash.get("posicion")));
		 }
		 else
		 {
			 this.mapeo.setPosicion(null); 
		 }
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 return mapeo;		 
	 }
	 
	 public MapeoArticulosUSA convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosUSA();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setPosicion(new Integer(r_hash.get("posicion")));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulosUSA r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 if (r_mapeo.getPosicion()!=null)
		 {
			 this.hash.put("posicion", r_mapeo.getPosicion().toString());
		 }
		 else
		 {
			 this.hash.put("posicion", null);
		 }
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 return hash;		 
	 }
}