package borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.view;

 import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo.MapeoArticulosUSA;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.server.consultaArticulosUSAServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoArticulosUSA mapeoArticulosUSA = null;
	 
	 private consultaArticulosUSAServer ser = null;	 
	 private OpcionesUSAView uw = null;
	 private BeanFieldGroup<MapeoArticulosUSA> fieldGroup;
	 
	 public OpcionesForm(OpcionesUSAView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoArticulosUSA>(MapeoArticulosUSA.class);
        fieldGroup.bindMemberFields(this);

        this.descripcion.setCaptionAsHtml(true);
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoArticulosUSA = (MapeoArticulosUSA) uw.grid.getSelectedRow();
                eliminarOpcion(mapeoArticulosUSA);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoArticulosUSA = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOpcion(mapeoArticulosUSA);
	 			}
	 			else
	 			{
	 			}
	 			
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0 )
				{
					ser = new consultaArticulosUSAServer(CurrentUser.get());
					String desc = ser.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
			}
		});
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.articulo.getValue()!=null && this.articulo.getValue().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue());
			opcionesEscogidas.put("descripcion", this.descripcion.getValue());
			if (this.posicion.getValue()!="null") 
			{
				opcionesEscogidas.put("posicion", this.posicion.getValue());
			}
			else
			{
				opcionesEscogidas.put("posicion", "");	
			}
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
			opcionesEscogidas.put("descripcion", "");
			opcionesEscogidas.put("posicion", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoArticulosUSA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoArticulosUSA();
		fieldGroup.setItemDataSource(new BeanItem<MapeoArticulosUSA>(r_mapeo));
		articulo.focus();
	}
	
	public void eliminarOpcion(MapeoArticulosUSA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaArticulosUSAServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		ser.eliminar(r_mapeo);
		
	}
	
	public void crearOpcion(MapeoArticulosUSA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaArticulosUSAServer(CurrentUser.get());
		String rdo = ser.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoArticulosUSA>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null)
			{
				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();	
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoArticulosUSA> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoArticulosUSA= item.getBean();
            if (this.mapeoArticulosUSA.getArticulo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
}

