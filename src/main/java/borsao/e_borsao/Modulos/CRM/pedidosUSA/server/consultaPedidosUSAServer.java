package borsao.e_borsao.Modulos.CRM.pedidosUSA.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.modelo.MapeoLineasPedidosUSA;

public class consultaPedidosUSAServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPedidosUSAServer instance;
	
	
	public consultaPedidosUSAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPedidosUSAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPedidosUSAServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoLineasPedidosUSA> datosOpcionesGlobal(MapeoLineasPedidosUSA r_mapeo)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasPedidosUSA> vector = null;
		MapeoLineasPedidosUSA mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");        
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" a.ref_cliente det_ref, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte ");
     	cadenaSQL.append(" FROM a_vp_l_0 p, e_client c, a_vp_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
     	cadenaSQL.append(" AND a.pais = '001");
     	
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestion();			
				this.cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosUSA>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosUSA();
				 
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}
}