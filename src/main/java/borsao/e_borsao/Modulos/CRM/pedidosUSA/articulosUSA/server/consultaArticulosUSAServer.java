package borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CRM.pedidosUSA.articulosUSA.modelo.MapeoArticulosUSA;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosUSAServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosUSAServer instance;
	
	public consultaArticulosUSAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosUSAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosUSAServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoArticulosUSA> datosOpcionesGlobal(MapeoArticulosUSA r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulosUSA> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_ptusa.articulo usa_art, ");
		cadenaSQL.append(" prd_ptusa.posicion usa_pos, ");
		cadenaSQL.append(" e_articu.descrip_articulo usa_des ");
     	cadenaSQL.append(" FROM prd_ptusa ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_ptusa.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptusa.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptusa.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosUSA>();
			
			while(rsOpcion.next())
			{
				MapeoArticulosUSA mapeoArticulos = new MapeoArticulosUSA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulos.setArticulo(rsOpcion.getString("usa_art"));
				mapeoArticulos.setPosicion(rsOpcion.getInt("usa_pos"));
				mapeoArticulos.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("usa_des")));
				vector.add(mapeoArticulos);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoArticulosUSA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_ptusa ( ");
			cadenaSQL.append(" prd_ptusa.posicion, ");
    		cadenaSQL.append(" prd_ptusa.articulo) VALUES (");
		    cadenaSQL.append(" ?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getPosicion()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getPosicion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoArticulos r_mapeo)
	{
	}
	
	public void eliminar(MapeoArticulosUSA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_ptusa ");            
			cadenaSQL.append(" WHERE prd_ptusa.articulo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}
}