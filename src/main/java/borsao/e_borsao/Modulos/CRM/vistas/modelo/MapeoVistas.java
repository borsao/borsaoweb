package borsao.e_borsao.Modulos.CRM.vistas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoVistas extends MapeoGlobal
{
    private Integer idVista;
	private String nombre;
	private String descripcion;

	public MapeoVistas()
	{
		this.setNombre("");
	}

	public Integer getIdVista() {
		return idVista;
	}

	public void setIdVista(Integer idVista) {
		this.idVista = idVista;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}