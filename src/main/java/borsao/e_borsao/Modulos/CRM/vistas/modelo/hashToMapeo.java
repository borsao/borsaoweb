package borsao.e_borsao.Modulos.CRM.vistas.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoVistas mapeo = null;
	 
	 public MapeoVistas convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoVistas();
		 this.mapeo.setDescripcion(null);
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdVista(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public MapeoVistas convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoVistas();
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdVista(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoVistas r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 this.hash.put("nombre", this.mapeo.getNombre());
		 this.hash.put("id", this.mapeo.getIdVista().toString());
		 return hash;		 
	 }
}