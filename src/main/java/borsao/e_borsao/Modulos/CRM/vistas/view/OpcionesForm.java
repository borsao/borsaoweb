package borsao.e_borsao.Modulos.CRM.vistas.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.CRM.vistas.modelo.MapeoVistas;
import borsao.e_borsao.Modulos.CRM.vistas.modelo.VistasGrid;
import borsao.e_borsao.Modulos.CRM.vistas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CRM.vistas.server.consultaVistasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoVistas mapeoVistas = null;	 
	 private consultaVistasServer cus = null;	 
	 private VistasView uw = null;
	 private BeanFieldGroup<MapeoVistas> fieldGroup;
	 
	 public OpcionesForm(VistasView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoVistas>(MapeoVistas.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeoVistas = (MapeoVistas) uw.grid.getSelectedRow();
                eliminarVista(mapeoVistas);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoVistas = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearVista(mapeoVistas);
	 			}
	 			else
	 			{
	 				MapeoVistas mapeoVistas_orig = (MapeoVistas) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoVistas= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modifcarVista(mapeoVistas,mapeoVistas_orig);
	 				hm=null;
	 			}
	 			if (((VistasGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarVista(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarVista(null);
	}
	
	public void editarVista(MapeoVistas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoVistas();
		fieldGroup.setItemDataSource(new BeanItem<MapeoVistas>(r_mapeo));		
		nombre.focus();
	}
	
	public void eliminarVista(MapeoVistas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaVistasServer(CurrentUser.get());
		((VistasGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modifcarVista(MapeoVistas r_mapeo, MapeoVistas r_mapeo_orig)
	{
		cus  = new consultaVistasServer(CurrentUser.get());
		r_mapeo.setIdVista(r_mapeo_orig.getIdVista());
		cus.guardarCambios(r_mapeo);		
		((VistasGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearVista(MapeoVistas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaVistasServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoVistas>(r_mapeo));
			if (((VistasGrid) uw.grid)!=null)
				((VistasGrid) uw.grid).refresh(r_mapeo,null);
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoVistas> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoVistas= item.getBean();
            if (this.mapeoVistas.getIdVista()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

}
