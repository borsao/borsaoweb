package borsao.e_borsao.Modulos.CRM.vistas.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class VistasGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;

    public VistasGrid(ArrayList<MapeoVistas> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("VISTAS CRM");
		this.generarGrid();
    }

    private void generarGrid()
    {
    	this.crearGrid(MapeoVistas.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("nombre", "descripcion");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombre").setHeaderCaption("Vista");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("idVista").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    

    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	return "cell-normal";
            }
        });
    	
    }

	public void establecerColumnasNoFiltro() 
	{
	}
	
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}

}
