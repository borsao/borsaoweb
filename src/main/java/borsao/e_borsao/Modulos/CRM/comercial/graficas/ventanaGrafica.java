package borsao.e_borsao.Modulos.CRM.comercial.graficas;

import java.sql.ResultSet;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.vaadin.addon.JFreeChartWrapper;
import org.vaadin.addons.d3Gauge.Gauge;
import org.vaadin.addons.d3Gauge.GaugeConfig;
import org.vaadin.addons.d3Gauge.GaugeStyle;
import org.vaadin.addons.d3Gauge.Zone;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import borsao.e_borsao.ClasesPropias.Notificaciones;

public class ventanaGrafica extends Window
{	
	private JFreeChart grafico = null;
	private Integer ejercicio = null;
	private Integer cuantos = null;
	
	public ventanaGrafica(ResultSet r_registros, Integer r_cuantos, Integer r_ejercicio, String campo1, String campo2, String campo3) 
	{
		/*
		 * incializamos datos ejercicio y comparativos
		 */
		this.ejercicio = r_ejercicio;
		this.cuantos=r_cuantos;
	}
	
	public ventanaGrafica(Object[] r_fila, Integer r_cuantos, Integer r_ejercicio) 
	{
		/*
		 * incializamos datos ejercicio y comparativos
		 */
		this.ejercicio = r_ejercicio;
		this.cuantos=r_cuantos;
		/*
		 * calculamos el grafico que queremos
		 * 
		 * TODO: En funcion de lo seleccionamos generaremos un grafico u otro
		 */
			this.grafico= this.XYLineas(r_fila);
        /*
         * Creamos el objeto que se presenta en la ventana final
         */
        this.crearPantalla();
	}
	

	public ventanaGrafica() 
	{
		/*
		 * incializamos datos ejercicio y comparativos
		 */
		/*
		 * calculamos el grafico que queremos
		 * 
		 * TODO: En funcion de lo seleccionamos generaremos un grafico u otro
		 */
        /*
         * Creamos el objeto que se presenta en la ventana final
         */
        this.gauge();
	}
	
	private JFreeChart XYLineas(Object[] r_fila)
	{
		
		XYPlot pl = null;
		NumberAxis ejeY = null;
		DateAxis ejeX = null;
		NumberFormat formatX = null;
		NumberFormat formatY = null;
		XYItemLabelGenerator generator  = null;
		XYLineAndShapeRenderer renderer= null; 
		XYSeriesCollection dataset = null;
		XYSeries seriesUd = null;
		XYSeries seriesIm = null;
		Calendar cal = null;
		Double minX=null;
		Double maxX=null;
		String titulo = null;

		
		/*
		 * Creamos las series de datos
		 */
		long ejer =0;
		int veces = 0;        
		cal = Calendar.getInstance(new Locale("es", "ES"));
		
		
		if (r_fila[0].toString().equals("Todos")) 
        {
        	titulo = "Global";
        }
        else
        {
        	titulo =r_fila[0].toString();
        }

        seriesIm = new XYSeries("IMPORTES");
        for (int i=2;i<(this.cuantos+1) * 3;i=i+3)
        {
        	ejer = cal.getTimeInMillis();
        	if (i==2) 
    		{
        		maxX=new Double(r_fila[i].toString())*1.5;
    		}
        	
        	cal.set(this.ejercicio - veces ,1,1,0,0,0);
        	ejer = cal.getTimeInMillis();

        	seriesIm.add(ejer, new Double(r_fila[i].toString()));
        	veces=veces+1;

        
        }
        
        seriesUd = new XYSeries("CANTIDADES");
        veces = 0;        
        for (int i=1;i<(this.cuantos+1) * 3;i=i+3)
        {
        	cal.set(this.ejercicio - veces,1,1,0,0,0);
            ejer = cal.getTimeInMillis();

            seriesUd.add(ejer, new Double(r_fila[i].toString()));
        	veces=veces+1;
        	minX=new Double(r_fila[i].toString())*.50;
        }
        
        /*
         * asignamos las series calculadas al dataSet correspondiente
         */
        dataset = new XYSeriesCollection();
        dataset.addSeries(seriesIm);
        dataset.addSeries(seriesUd);
        
        /*
         * Creamos los ejes para la grafica y les damos el formato de datos requerido
         */
        ejeY = new NumberAxis("€/Ud");
        ejeY.setRange(minX, maxX);
        
        ejeX = new  DateAxis("Ejercicios");
        ejeX.setDateFormatOverride(new SimpleDateFormat("yyyy"));
        ejeX.setTickUnit(new DateTickUnit(DateTickUnitType.YEAR, 1));

        /*
         * Damos formato a los resumenes de datos de dentro del gráfico
         * y generamos el modelo de renderizacion 
         */
        formatX = NumberFormat.getNumberInstance();
        formatX.setMaximumFractionDigits(2);
        
        formatY = NumberFormat.getNumberInstance();
        formatY.setMaximumFractionDigits(0);
        
        generator  = new StandardXYItemLabelGenerator("{2}", formatY, formatX);        
      
        renderer = new XYLineAndShapeRenderer();
        renderer.setBaseItemLabelGenerator(generator);
        renderer.setBaseItemLabelsVisible(true);
        
        
        
        /*
         * creamos el núcleo del grafico que engloba
         * 
         * 	el dataset
         * 	los ejes
         * 	el renderizado
         */
        pl = new XYPlot(dataset, ejeX, ejeY, renderer);
        
        /*
         * Creamos el grafico con el nucleo anterior y lo titulamos y lo devolvemos
         */
        this.grafico = new JFreeChart(pl);
        this.grafico.setTitle("Evolución Ventas " + titulo);
        
        return this.grafico;
	}
	
	
	
	private JFreeChart Barras(Object[] r_fila)
	{
		
        return this.grafico;
	}
	

	private void crearPantalla()
	{
		VerticalLayout layout = null;
		JFreeChartWrapper wrapper = null;
		/*
		 * definimos el layout principal que alojará el grafico
		 */
		layout = new VerticalLayout(); 
		
		/*
		 * titulamos la ventana
		 */
		this.setCaption("Borsao");

		/*
		 * generamos el objeto a posicionar en la ventana
		 */
        wrapper = new JFreeChartWrapper(this.grafico) {
            @Override
            public void attach() {
            	super.attach();
                setResource("src", getSource());
            }
        };

        /*
         * posicionamos la pantalla y su diseño
         */
        layout.addComponent(wrapper);
        layout.setComponentAlignment(wrapper, Alignment.MIDDLE_CENTER);
        layout.setSizeFull();
        this.setContent(layout);
        this.center();
        this.setModal(true);
        this.setClosable(true);
        this.setResizable(true);
        this.setWidth("1000px");
        this.setHeight("600px"); 


	}
	
	private void gauge()
	{

		VerticalLayout layout = new VerticalLayout(); 
		
		/*
		 * titulamos la ventana
		 */
		this.setCaption("Borsao");

		/*
		 * Iniciamos la configuracion personalizada del reloj
		 */
		GaugeConfig config = new GaugeConfig();
		/*
		 * Estilo visual del reloj
		 */
		config.setStyle(GaugeStyle.STYLE_DEFAULT.toString());
		
		/*
		 * configuramos los rangos de valor indicativos del semaforo
		 */
		List<Zone> greenZones = new ArrayList<Zone>();
		List<Zone> yellowZones = new ArrayList<Zone>();
		List<Zone> redZones = new ArrayList<Zone>();
		
		Zone red = new Zone(0, 2000);		
		redZones.add(red);
		
		Zone yellow = new Zone(2001, 4200);
		yellowZones.add(yellow);
		
		Zone green = new Zone(4201, 9000);
		greenZones.add(green);
		
		config.setRedZones(redZones);
		config.setYellowZones(yellowZones);
		config.setGreenZones(greenZones);
		

		/*
		 * valores máximo y minimo del reloj
		 */
		config.setMin(0);
		config.setMax(9000);
		
		
		/*
		 * valores de tamaño de las rayas indicadoras del reloj
		 */
		config.setMinorTicks(5);
		config.setMajorTicks(5);
		/*
		 * movimiento de la aguja al generar los datos, suele usarse para relojes que tardan en cargar
		 */
        config.setTransitionDuration(1500);
        /*
         * muestra titulo valor actual, en ingles, current
         */
        config.setShowCurrentLabel(true);
//        config.setTrackAvg(true);
//        config.setTrackMax(true);
//        config.setTrackMin(true);
		
        
        /*
         * Implementacion del reloj
         * Recibe:
         * 		Titulo
         * 		valor actual
         * 		tamaño del gráfico
         * 		sesion de configuracion/personalizacion del reloj
         */
		Gauge gauge = new Gauge("Bt/Hr.",3518,200,config);
		/*
         * posicionamos la pantalla y su diseño
         */
        layout.addComponent(gauge);
        layout.setComponentAlignment(gauge, Alignment.MIDDLE_CENTER);
        layout.setSizeFull();
        LayoutClickListener lcl = new LayoutClickListener() {
			
			@Override
			public void layoutClick(LayoutClickEvent event) {
				Notificaciones.getInstance().mensajeInformativo("Aqui navegaremos al grid de datos que justifican el valor");
			}
		};
        layout.addLayoutClickListener(lcl);
        this.setContent(layout);
        this.center();
        this.setModal(true);
        this.setClosable(true);
        this.setResizable(true);
        this.setWidth("1000px");
        this.setHeight("600px"); 
        
		
		layout.addComponent(gauge);
	}
}