package borsao.e_borsao.Modulos.CRM.comercial.Consultas.view;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.haijian.ExcelExporter;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.MapeoVentas;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.VentasGrid;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.consultaVentasContainer;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class VentasView extends GridViewRefresh {

	public static final String VIEW_NAME = "Ventas";
	private final String titulo = "ESTADISTICAS VENTAS";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;

    public ExcelExporter excelExporter=null;
    public Button pdf=null;
//    public Button pdfExporter=null;
    private OpcionesForm form;
    private Table tabla = null;

    public Integer ejercicioInicial = null;
    public Integer comparativos = null;
    public String desdeFecha = null;
    public String hastaFecha = null;
    public String nivel1 = null;
    public String nivel2 = null;
    public String nivel3 = null;
    
    public VentasView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_ver)
    {
    	this.form.addStyleName("visible");
    	this.form.setEnabled(r_ver);
    }
    
    public void generarGrid(HashMap<String , String> r_opcionesEscogidas)
    {
    	grid = new VentasGrid(r_opcionesEscogidas,isHayGrid());
    	setHayGrid(true);
      
    	if (isHayGrid())
    	{
    	  
    		pdf = new Button();
    		pdf.setCaption("PDF");
    		pdf.setStyleName(ValoTheme.BUTTON_PRIMARY);
    		pdf.setStyleName(ValoTheme.BUTTON_TINY);
    		pdf.setIcon(FontAwesome.FILE_PDF_O);
    		pdf.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					expandirNodosPdf(true);
					
				}
			});
    		topLayoutR.addComponent(pdf);
    		
    		excelExporter = new ExcelExporter();
  	
    		excelExporter.setDateFormat("yyyy-MM-dd");
    		excelExporter.addStyleName(ValoTheme.BUTTON_PRIMARY);
    		excelExporter.addStyleName(ValoTheme.BUTTON_TINY);
    		excelExporter.setIcon(FontAwesome.DOWNLOAD);
    		excelExporter.setCaption("Export to Excel");
    		excelExporter.setDownloadFileName("EstadisticasVenta");

    		excelExporter.addClickListener(new ClickListener() {
		        @Override
		        public void buttonClick(ClickEvent event) {
		        	tabla= generarTablaExcel(r_opcionesEscogidas);
		    		excelExporter.setTableToBeExported(tabla);
		        }
    		});

    		topLayoutR.addComponent(excelExporter);
    		topLayoutR.setComponentAlignment(excelExporter, Alignment.MIDDLE_RIGHT);
    		topLayoutR.setSpacing(true);
//    		pdfExporter = new PdfExporter(tabla);
//    	  	pdfExporter.setStyleName(ValoTheme.BUTTON_PRIMARY);
//    	  	pdfExporter.setCaption("Export to PDF");
//    	  	pdfExporter.setIcon(FontAwesome.FILE_PDF_O);
//    	  	
//    	  	pdfExporter.addClickListener(new ClickListener() {
//                @Override
//                public void buttonClick(ClickEvent event) {
//                	new PdfFileBuilder(tabla);
//                }
//            });

//    		topLayoutR.addComponent(pdfExporter);
//    		topLayoutR.setComponentAlignment(pdfExporter, Alignment.MIDDLE_RIGHT);


      }
      barAndGridLayout.addComponent(grid);
      barAndGridLayout.setExpandRatio(grid, 1);
      barAndGridLayout.setMargin(true);
    }
    
	private Table generarTablaExcel(HashMap<String , String> r_opcionesEscogidas)
	{
		consultaVentasContainer cont=null; 
		Table table = new Table();

		consultaVentasContainer container = (consultaVentasContainer) this.grid.getContainerDataSource();
		container= expandirNodos(container, true);
		
//		try {
//			cont = container.clone();
//		} catch (CloneNotSupportedException e) {
////			e.printStackTrace();
//		}

		
		Iterator<Column> it = this.grid.getColumns().iterator();
        
        while (it.hasNext()) 
        {
        	Column col = (Column)it.next();
        	
			if (col.getPropertyId().toString().contains("Diff"))
        	{
//        		this.grid.removeColumn(col.getPropertyId());
//				col.setHidden(true);
				container.removeContainerProperty(col.getPropertyId());
        	}
        }
        
        
        table.setContainerDataSource(container);
		return table;
	}
	
	private consultaVentasContainer expandirNodos(consultaVentasContainer container, boolean r_expandir)
	{
		for (Object item: container.getItemIds().toArray()) 
		{
			//abre todos
			item.toString();
			if (container.isCollapsed(item) && container.hasChildren(item)) container.setCollapsed(item, !r_expandir);
			if (container.getChildren(item)!=null)
			{
				for (Object item2: container.getChildren(item).toArray())
				{
					//abre primer nivel
					if (container.isCollapsed(item2) && container.hasChildren(item2)) container.setCollapsed(item2, !r_expandir);
					if (container.getChildren(item2)!=null)
					{
						for (Object item3: container.getChildren(item2).toArray())
						{
							//abre segundo nivel
							if (container.isCollapsed(item3) && container.hasChildren(item3)) container.setCollapsed(item3, !r_expandir);
							if (container.getChildren(item3)!=null)
							{
								for (Object item4: container.getChildren(item3).toArray())
								{
									//abre tercer nivel
									if (container.isCollapsed(item4) && container.hasChildren(item4)) container.setCollapsed(item4, !r_expandir);		
								}
							}
						}
					}
				}
			}			
		}
		return container;
	}
	
	private void expandirNodosPdf(boolean r_expandir)
	{
 		Integer nivel = null;
 		Integer ejercicio = null; 		
 		String codigo = null;
 		String codigoNivel1 = null;
 		String codigoNivel2 = null;
 		String codigoNivel3 = null;
 		String descr = null;
 		Double uds = null;
 		Double imp = null;
 		Double pvm = null;
 		MapeoVentas mv = null;

 		Object[] allProjects = null;       
		
 		consultaVentasContainer r_container = (consultaVentasContainer) this.grid.getContainerDataSource();
 		consultaVentasServer cvs = new consultaVentasServer(CurrentUser.get());
 		
 		for (Object item: r_container.getItemIds().toArray()) 
		{
 			if (!r_container.isCollapsed(item) && r_container.hasChildren(item)) r_container.setCollapsed(item, true);
		}
 		
 		Integer id = cvs.obtenerSiguienteId();
 		int salto = 0;
 		
 		for (Object item: r_container.getItemIds().toArray()) 
		{
			//abre todos
		 	allProjects = (Object[]) item;
		 	
		 	codigo = "";
		 	descr = allProjects[0].toString();
		 	
		 	for (int i = 0; i<= comparativos; i++)
		 	{
			 	ejercicio = ejercicioInicial-i;				 	 
			 	uds = new Double(allProjects[(3*i)+salto+1].toString());
			 	imp = new Double(allProjects[(3*i)+salto+2].toString());
			 	pvm = new Double(allProjects[(3*i)+salto+3].toString());
			 	nivel=0;
			 	rellenarMapeo(id, nivel, ejercicio,"",descr,uds,imp,pvm);
			 	// insert en la tabla de ventas año a año
			 	salto = salto + 4;
		 	}
			
			if (r_container.isCollapsed(item) && r_container.hasChildren(item)) r_container.setCollapsed(item, !r_expandir);
			if (r_container.getChildren(item)!=null)
			{
				codigoNivel1 = "Todos";
				
				for (Object item2: r_container.getChildren(item).toArray())
				{
					allProjects = (Object[]) item2;
					salto=0;
					
			 		descr = allProjects[0].toString();
			 		codigoNivel2=allProjects[0].toString();
				 	for (int i = 0; i<= comparativos; i++)
				 	{
					 	ejercicio = ejercicioInicial-i;				 	 
					 	uds = new Double(allProjects[(3*i)+salto+1].toString());
					 	imp = new Double(allProjects[(3*i)+salto+2].toString());
					 	pvm = new Double(allProjects[(3*i)+salto+3].toString());
					 	nivel=1;
					 	rellenarMapeo(id, nivel, ejercicio,"",descr,uds,imp,pvm);
					 	// insert en la tabla de ventas año a año 
					 	salto=salto + 4;
				 	}

					//abre primer nivel
					if (r_container.isCollapsed(item2) && r_container.hasChildren(item2)) r_container.setCollapsed(item2, !r_expandir);
					if (r_container.getChildren(item2)!=null)
					{
						for (Object item3: r_container.getChildren(item2).toArray())
						{
							allProjects = (Object[]) item3;
							salto=0;
					 		descr = allProjects[0].toString();
					 		codigoNivel3 = allProjects[0].toString();
					 		
					 		for (int i = 0; i<= comparativos; i++)
						 	{
							 	ejercicio = ejercicioInicial-i;				 	 
							 	uds = new Double(allProjects[(3*i)+salto+1].toString());
							 	imp = new Double(allProjects[(3*i)+salto+2].toString());
							 	pvm = new Double(allProjects[(3*i)+salto+3].toString());
							 	nivel=2;
							 	rellenarMapeo(id, nivel, ejercicio,"",descr,uds,imp,pvm);
							 	// insert en la tabla de ventas año a año 
							 	salto=salto + 4;
						 	}
							//abre segundo nivel
							if (r_container.isCollapsed(item3) && r_container.hasChildren(item3)) r_container.setCollapsed(item3, !r_expandir);
							if (r_container.getChildren(item3)!=null)
							{
								for (Object item4: r_container.getChildren(item3).toArray())
								{
									allProjects = (Object[]) item4;
									salto=0;
							 		descr = allProjects[0].toString();
							 		
							 		for (int i = 0; i<= comparativos; i++)
								 	{
									 	ejercicio = ejercicioInicial-i;				 	 
									 	uds = new Double(allProjects[(3*i)+salto+1].toString());
									 	imp = new Double(allProjects[(3*i)+salto+2].toString());
									 	pvm = new Double(allProjects[(3*i)+salto+3].toString());
									 	nivel=3;
									 	rellenarMapeo(id, nivel, ejercicio,codigoNivel3,descr,uds,imp,pvm);
									 	// insert en la tabla de ventas año a año
									 	salto=salto + 4;
								 	}

									//abre tercer nivel
									
									if (r_container.isCollapsed(item4) && r_container.hasChildren(item4)) r_container.setCollapsed(item4, !r_expandir);		
								}
							}
						}
					}
				}
			}			
		}
		this.imprimirInforme(id);
		cvs.eliminarGeneracion(id);
	}


	
	@Override
	public void filaSeleccionada(Object r_fila) {
		
	}

    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
    {
    	eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
    	eBorsao.getCurrent().getNavigator().addView(VIEW_NAME, this.getClass());
    }

	@Override
	public void eliminarRegistro() {
		
	}
	
	private void rellenarMapeo(Integer r_id, Integer r_nivel, Integer r_ejercicio, String r_codigo, String r_des, Double r_uds, Double r_imp, Double r_pvm)
	{
		MapeoVentas mv = new MapeoVentas();
		consultaVentasServer cvs = new consultaVentasServer(CurrentUser.get());
		mv.setIdCodigo(r_id);
		mv.setNivel(r_nivel);
		mv.setEjercicio(r_ejercicio);
		mv.setCodigo(r_codigo);
		mv.setDescripcion(r_des);
		mv.setUnidades(r_uds);
		mv.setImporte(r_imp);
		mv.setMedio(r_pvm);
		
		mv.setDesdeFecha(this.desdeFecha);
		mv.setHastaFecha(this.hastaFecha);
		mv.setNivel1(this.nivel1);
		mv.setNivel2(this.nivel2);
		mv.setNivel3(this.nivel3);
		
		cvs.generarPdf(mv);
	}
	
	private void imprimirInforme(Integer r_id)
	{
		String pdfGenerado = null;
		consultaVentasServer cvs = new consultaVentasServer(CurrentUser.get());
		pdfGenerado = cvs.imrpimirPdf(r_id);
		
		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		
		pdfGenerado = null;
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
