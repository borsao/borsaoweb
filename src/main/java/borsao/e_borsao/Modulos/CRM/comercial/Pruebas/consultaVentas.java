package borsao.e_borsao.Modulos.CRM.comercial.Pruebas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.treegrid.TreeGrid;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.Page.BrowserWindowResizeEvent;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.EuroConverter;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.consultaVentasContainer;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;
import borsao.e_borsao.Modulos.CRM.comercial.graficas.ventanaGrafica;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaVentas extends CssLayout implements View
{
	/*
	 * Atributos publicos
	 */
	 public static final String NAME = "Pruebas";

	 public HashMap<String , String> opcionesEscogidas = null;
	 public consultaVentasContainer container =null;
	 public boolean hayGrid = false;
	 // layout que contendra la tabla
	 public VerticalLayout layoutTabla = null;
	 /*
	  * Atributos privados
	  */
	 private ventanaRangos vt  =null;	 
	 // gestion de notificaciones al usuario
	 private Notificaciones serNotif = Notificaciones.getInstance();
	 // contenedor opciones escogidas por el usuario
	 private HashMap<String, String> hashRangos = null;
	 // recoge el primer ejercicio cargado en el combo
	 private Integer primerEjercicio = null;
	 
	 // layout principal aplicacion
	 private GridLayout layoutHor = null;
	 //layout que organiza el contenido del panel de peticion
	 private GridLayout layout =null;
	 
	 // paneles que organizan la pantalla
	 private Panel panelTitulo= null;
	 private Panel panelPeticion = null;
//	 private Panel panelTabla= null;
	 
	 // etiquetas de titulo de los paneles
	 private Label lblTituloPeticion=null; 
	 private Label lblTitulo=null;

	 // controles dentro del panel de peticion
	 private Button btnGenerar = null;
	 private Button btnRangos= null;
	 private Combo cmbEjercicios = null;
	 private Combo cmbCuantos= null;
 	 private Combo cmbCampo1= null;
 	 private Combo cmbCampo2= null;
 	 private Combo cmbCampo3= null;
 	 private Combo cmbFactor= null;
 	 private Combo cmbAñada= null;
 	 private EntradaDatosFecha desdeFecha = null;
 	 private EntradaDatosFecha hastaFecha = null;
	 
 	 // contenedor de los datos generados
	 private TreeGrid grid = null; 
	 
	 
	 /*
	  * CONSTRUCTOR DE LA CLASE
	  */
	 public consultaVentas() {
			 
			
     }

	 @Override
	 public void enter(ViewChangeEvent event) {
		 
		 /*
		  * Establezco el titulo de la ventama del navegador
		  */
		 WebBrowser browser = (WebBrowser) Page.getCurrent().getWebBrowser();
		 /*
		  * COMPONENTES GRAFICOS 
		  */
		 /* Inicializamos los paneles contenedores*/

		 /*
		  * Evento para detectar el cambio de tamaño del navegador
		  */
		 this.layoutHor = new GridLayout(3,2);
		 this.panelPeticion = new Panel();
		 this.panelTitulo = new Panel();
		 this.layoutTabla = new VerticalLayout();
//		 this.panelTabla= new Panel();
		 
		 /* carga dinamica de controles en cada panel, valorres inciales, listeners, etc.*/
		 this.cargarControles(browser.getScreenWidth()-150, browser.getScreenHeight()-50,false);
		 this.cargarListeners();
		 this.inicializarRangos();
		 
		/*asignacion del panel principal a la pagina del navegador */
		 addComponent(this.layoutHor);
		 addStyleName("crud-view");
		 
		 Page.getCurrent().addBrowserWindowResizeListener(new BrowserWindowResizeListener() {
	        public void browserWindowResized(BrowserWindowResizeEvent event) {
	            cargarControles(event.getWidth(),event.getHeight(),true);
	            
	        }
		 });	
		 
//			ventanaGrafica vtG  =null;			
//			vtG = new ventanaGrafica();
//			getUI().addWindow(vtG);	

	}
	 
	/*
	 * Metodo llamado desde la ventana de peticion de rangos al aceptar
	 * 
	 * Con los datos indicados en la pantalla genera las opciones pedidas por el usuario
	 * y cambia el caption del boton rangos para agregar un asterisco indicando que tenemos
	 * filtros aplicados.
	 */
	public void establecerRangos(HashMap<String, String> r_hash, boolean r_cambios)
	{
		this.hashRangos = r_hash;
		if (r_cambios)
		{
			this.btnRangos.setCaption("Rangos (*)");
		}
		else
		{
			this.btnRangos.setCaption("Rangos");
		}
		this.generarHashOpciones();
		getUI().removeWindow(this.vt);
		this.vt=null;
	}
	
	/*
	 * Carga de eventos en los controles que lo necesitan
	 * 
	 * Combo Ejercicios. Al modificar el valor cambiamos el contenido del combo comparativos y de los selectores desde/hasta fecha
	 * Combo Campo1, 2 y 3. Al modificar los valores comprobaremos que no coincidan entre ellos e inicializamos los rangos si los hubiera
	 * 
	 * Boton LogOut. Detectamos la pulsación para abandonar la sesion
	 * Boton Generar. Con los rangos y tramos solicitados generamos la consulta
	 * Boton Rangos. LLama a la ventana de filtros
	 */
	private void cargarListeners()
	{
		this.cmbCampo1.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
				if (cmbCampo1.getValue()!=null)
            	{
            		cmbCampo2.setEnabled(true);
            		cmbCampo3.setEnabled(false);
            		cmbCampo2.clear();
					cmbCampo3.clear();
					inicializarRangos();
            	}
				else if (cmbCampo1.getValue()!=null && cmbCampo2.getValue()!=null && !cmbCampo2.getValue().equals("") && !cmbCampo2.getValue().equals(cmbCampo1.getValue()) )
            	{
            		cmbCampo3.setEnabled(true);
					cmbCampo3.clear();	
					inicializarRangos();
            	}
				else if (cmbCampo1.getValue()!=null && cmbCampo2.getValue()!=null && cmbCampo3.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo1.getValue()) || cmbCampo3.getValue().equals(cmbCampo1.getValue()) || cmbCampo3.getValue().equals(cmbCampo2.getValue()) ))
            	{
            		cmbCampo2.clear();
					cmbCampo3.clear();
					inicializarRangos();
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
            	}
            	else
            	{
            		cmbCampo2.setEnabled(false);
        			cmbCampo3.setEnabled(false);
        			inicializarRangos();
            	}
			}
	    });
		this.cmbCampo2.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
				if (cmbCampo2.getValue()!=null && !cmbCampo2.getValue().equals("") && cmbCampo3.getValue()==null && !cmbCampo2.getValue().equals(cmbCampo1.getValue()))
            	{
            		cmbCampo3.setEnabled(true);
					cmbCampo3.clear();
					inicializarRangos();
            	}
				else if (cmbCampo2.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo1.getValue()) || (cmbCampo3.getValue()!=null && cmbCampo3.getValue().equals(cmbCampo2.getValue()))))
            	{
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
					cmbCampo2.clear();
					inicializarRangos();
            	}
            	else
            	{	            		
        			cmbCampo3.setEnabled(false);
        			cmbCampo3.clear();
        			inicializarRangos();
            	}
			}
	    });
		this.cmbCampo3.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
	    		if (cmbCampo3.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo3.getValue()) || (cmbCampo3.getValue().equals(cmbCampo1.getValue()))))
            	{
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
					cmbCampo3.clear();
					inicializarRangos();
            	}
			}
	    });
		this.btnGenerar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{
//	 			eBorsao.getCurrent().getNavigator().navigateTo("");
//	 			eBorsao.getCurrent().getNavigator().removeView(consultaVentas.NAME);
//	 			eBorsao.getCurrent().getNavigator().addView(consultaVentas.NAME, consultaVentas.class);
//	 			eBorsao.getCurrent().doRefresh(null);
	 			if (hayGrid)
	 			{			
	 				grid.removeAllColumns();			
	 				layoutTabla.removeComponent(grid);
	 				grid=null;			
	 			}
	 			cargarGrid();
	 			hayGrid=true;
			}
 		});	
		this.btnRangos.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{
	 			ventanaRangos();
			}
 		});
        
		this.cmbEjercicios.addValueChangeListener(new Property.ValueChangeListener() 
		{
			public void valueChange(ValueChangeEvent event) 
			{
			
				int cuantos = cmbEjercicios.size();
				int primero = primerEjercicio.intValue();
				int estoy = new Integer(cmbEjercicios.getValue().toString()).intValue();
				
				cmbCuantos.removeAllItems();
				
				for (int j = 0; j<cuantos-(primero-estoy);j++)
				{
					cmbCuantos.addItem(String.valueOf(j));
				}
				cmbCuantos.setValue("0");
				
				desdeFecha.setRangeStart("");
				desdeFecha.setRangeEnd("");
				hastaFecha.setRangeStart("");
				hastaFecha.setRangeEnd("");

				String dateInString = "01/01/" + cmbEjercicios.getValue();
    			dateInString = "31/12/" + cmbEjercicios.getValue();
    			desdeFecha.setRangeStart(dateInString);
    			desdeFecha.setValue(dateInString);
    			hastaFecha.setRangeStart(dateInString);
    			desdeFecha.setRangeEnd(dateInString);
    			hastaFecha.setRangeEnd(dateInString);
    			hastaFecha.setValue(dateInString);

			}
		});		
	}
	
	/*
	 * CArgamos toda la parte grafica
	 */
	private void cargarControles(int r_width, int r_height, boolean existenSN)
	{
		String anchura=String.valueOf(r_width);;
		String altura = String.valueOf(r_height);
		
		double multiplicadorAnchoFrameIzquierdo = 0.15;
		double multiplicadorAnchoFrameSuperiorCentral = 0.90;
		double multiplicadorAnchoFrameCentral= 0.75;
		double multiplicadorAltoFrameSuperior = 0.10;
		double multiplicadorAltoFrameInferior = 0.80;
		
		if (eBorsao.get().accessControl.getDispositivo().toLowerCase().equals("ipad"))
        {
			multiplicadorAnchoFrameIzquierdo = 0.25;
			multiplicadorAnchoFrameSuperiorCentral = 0.85;
			multiplicadorAnchoFrameCentral= 0.65;
        }
		/*
		 * Si están cargados los controles los eliminamos para poder
		 * volver a crearlos adecuandolos a los nuevos tamaños del navegador
		 */

        if (!existenSN) 
        {
        	this.lblTitulo = new Label("ESTADISTICAS BODEGAS BORSAO, S.A.");//, ContentMode.HTML);
        	this.layout = new GridLayout(2,10);
        	this.lblTituloPeticion = new Label("Opciones");
        	this.cmbEjercicios = new Combo("Ejercicio");
//        	this.cmbEjercicios.addStyleName(Valotheme.com);
        	this.cmbCuantos = new Combo("Nº Comparativos");
        	this.desdeFecha = new EntradaDatosFecha("Desde Fecha");
        	this.hastaFecha = new EntradaDatosFecha("Hasta Fecha");
        	this.cmbCampo1 = new Combo("Agrupación Estadística");
        	this.cmbCampo2 = new Combo();
        	this.cmbCampo3 = new Combo();
        	this.cmbFactor = new Combo();
        	this.cmbAñada = new Combo();

        	this.btnGenerar = new Button("Generar");
	        this.btnGenerar.setDisableOnClick(true);
	        this.btnRangos = new Button("Rangos");
//	        this.layoutTabla = new VerticalLayout();
        }
        
        setSizeFull();
        /*
         * establezco el layout principal 
         */
        this.layoutHor.setSizeUndefined();
        
        /*
         * panel para el titulo de la web
         */
        anchura = String.valueOf(r_width*multiplicadorAnchoFrameSuperiorCentral);
        altura = String.valueOf(r_height*multiplicadorAltoFrameSuperior);
        this.panelTitulo.setHeight(altura);
        this.panelTitulo.setWidth(anchura);
        this.panelTitulo.addStyleName("v-panelTitulo");
        
        	/* contenido del panel de titulo */
        	this.lblTitulo.setWidth("90%");
        	this.lblTitulo.setHeight("90%");
        	this.lblTitulo.addStyleName("labelTituloVentas");
        	
        	this.panelTitulo.setContent(this.lblTitulo);        	
        	
	        
        if (!existenSN)
        {
			
			this.cargarEjercicios();        	
			
			this.cargarCombos(this.cmbCampo1);
			this.cargarCombos(this.cmbCampo2);
			this.cargarCombos(this.cmbCampo3);
			this.cargarCombosFactor(this.cmbFactor);
			this.cargarCombosSN(this.cmbAñada);
        }
	        
        /*
         * panel para los datos a consultar
         */	        
        anchura = String.valueOf(r_width*multiplicadorAnchoFrameIzquierdo);
        altura = String.valueOf(r_height*multiplicadorAltoFrameInferior);
        
        this.panelPeticion.setHeight(altura);
        this.panelPeticion.setWidth(anchura);
        this.panelPeticion.addStyleName("v-panelPeticion");		 

	        /* contenido del panel de datos */		 
	        this.layout.setWidthUndefined();
	        this.layout.setHeightUndefined();
	        this.layout.setResponsive(true);
	        this.layout.setMargin(false);
	        this.layout.setSpacing(true);
	        
	        this.lblTituloPeticion.setStyleName("myTheme labelPanel");
	        
	        this.btnRangos.setWidth("100px");
	        this.btnGenerar.setWidth("100px");

			this.cmbEjercicios.setWidth("100px");
			this.cmbEjercicios.setNullSelectionAllowed(false);
			
			this.cmbCuantos.setWidth("100px");
			this.cmbCuantos.setNullSelectionAllowed(false);
			
			String dateInString = "01/01/" + this.cmbEjercicios.getValue();
			this.desdeFecha.setRangeStart(dateInString);
			this.desdeFecha.setValue(dateInString);
			this.hastaFecha.setRangeStart(dateInString);
			dateInString = "31/12/" + this.cmbEjercicios.getValue();
			this.desdeFecha.setRangeEnd(dateInString);
			this.hastaFecha.setRangeEnd(dateInString);
			this.hastaFecha.setValue(dateInString);
			
			this.cmbCampo1.setValue("Pais");
			this.cmbCampo1.setNullSelectionAllowed(false);
			this.cmbCampo1.setRequired(true);
			this.cmbCampo1.setWidth("150px");

			this.cmbCampo2.setValue("");
			this.cmbCampo2.setEnabled(true);
			this.cmbCampo2.setNullSelectionAllowed(true);
			this.cmbCampo2.setWidth("150px");
			
			this.cmbCampo3.setValue("");
			this.cmbCampo3.setEnabled(false);
			this.cmbCampo3.setNullSelectionAllowed(true);
			this.cmbCampo3.setWidth("150px");

			this.cmbFactor.setCaption("Factor");
			this.cmbAñada.setCaption("Añada");
			
			this.panelPeticion.setContent(this.layout);
		/*
         * panel para los resultados de la consulta
         */    				
			  
		anchura = String.valueOf(r_width*multiplicadorAnchoFrameCentral);
        altura = String.valueOf(r_height*multiplicadorAltoFrameInferior);
//        this.panelTabla.setSizeFull();
//        this.panelTabla.setHeight(altura);
//        this.panelTabla.setWidth(anchura);
//        this.panelTabla.setStyleName("crud-main-layout");
        
			/* contenido del panel de tabla*/
        
        	this.layoutTabla.setSpacing(true);
//        	this.layoutTabla.setSizeFull();
        	this.layoutTabla.setWidth(String.valueOf(r_width*multiplicadorAnchoFrameCentral*0.9));
        	this.layoutTabla.setHeight(String.valueOf(r_height*multiplicadorAltoFrameInferior*0.8));
//        	this.layoutTabla.setStyleName("scrollable");
	        /*
	         * Agrego el layout al panel
	         */
//	        this.panelTabla.setContent(this.layoutTabla);
        /*
         * agrego los paneles al grid 
         */
	        
        if (!existenSN) 
        {
	        
        	this.layout.addComponent(this.lblTituloPeticion,0,0,1,0);	        
        	this.layout.addComponent(this.cmbEjercicios,0,1); 
        	this.layout.addComponent(this.cmbCuantos,1,1);
        	this.layout.addComponent(this.cmbFactor,0,2,0,2);
        	this.layout.addComponent(this.cmbAñada,1,2,1,2);
        	this.layout.addComponent(this.desdeFecha,0,3,1,3);
        	this.layout.addComponent(this.hastaFecha,0,4,1,4);	        
        	this.layout.addComponent(this.cmbCampo1,0,5,1,5);
        	this.layout.addComponent(this.cmbCampo2,0,6,1,6);
        	this.layout.addComponent(this.cmbCampo3,0,7,1,7);
        	this.layout.addComponent(this.btnRangos,0,8,0,8);
        	this.layout.addComponent(this.btnGenerar,1,8,1,8);
        	
        	this.layoutHor.addComponent(this.panelTitulo,0,0,2,0);
        	this.layoutHor.addComponent(this.panelPeticion,0,1);		 
        	this.layoutHor.addComponent(this.layoutTabla,1,1,2,1);
        }
        
        this.layout.setComponentAlignment(this.btnGenerar,Alignment.BOTTOM_CENTER);
        this.layout.setComponentAlignment(this.btnRangos,Alignment.BOTTOM_CENTER);

        if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master")) 
    	{
        	this.cmbAñada.setEnabled(true);
    	}
        else
        {
        	this.cmbAñada.setEnabled(false);
        }
	}

	/*
	 * Una vez generada la consulta de los datos, rellenamos el grid con el resultado
	 */
	private void cargarGrid()
	{
		
		/*
		 * Obtenemos las opciones seleccionadas por el usuario
		 */
		
		this.generarHashOpciones();

		
		/*
		 * codigo para cargar el jtree-grid
		 */

		this.grid = new TreeGrid();
		this.grid.setCaption("<h2><b>Consulta Ventas</b></h2>");
		this.grid.setCaptionAsHtml(true);
//		this.grid.setSizeFull();
        this.grid.setWidth("95%");
        this.grid.setHeight(String.valueOf(this.layoutTabla.getHeight()*0.75));
        
        container = new consultaVentasContainer(this.opcionesEscogidas,this.hayGrid);
        
        this.grid.setContainerDataSource(container);
        this.grid.setSelectionMode(Grid.SelectionMode.SINGLE);        
        Iterator<Column> it = this.grid.getColumns().iterator();
        
        while (it.hasNext()) 
        {
        	Column col = (Column)it.next();
        	
        	if (!(col.getPropertyId().equals(container.getColumnaPrincipal())))
        	{
//        		col.setConverter(new EuroConverter());
        	}
        	col.setWidthUndefined();
        }

        this.grid.setEditorEnabled(false);
        this.grid.setFrozenColumnCount(1);
        this.grid.getColumn(consultaVentasContainer.getColumnaPrincipal()).setEditable(false);
        this.grid.getColumn(consultaVentasContainer.getColumnaPrincipal()).setHidden(false);
        this.grid.setHierarchyColumn(consultaVentasContainer.getColumnaPrincipal());
        this.grid.setColumnOrder(consultaVentasContainer.getColumnaPrincipal());
        this.grid.setColumnResizeMode(ColumnResizeMode.ANIMATED);
//        this.grid.addStyleName(ValoTheme.TABLE_COMPACT);
        this.grid.setColumnReorderingAllowed(false);
		this.grid.setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
                if ("Descripcion".equals(cellReference.getPropertyId())) {
                    // when the current cell is number such as age, align text to right                	
                    return "leftAligned";
                } else {
                    // otherwise, align text to left
                	return "rightAligned";
                }
            }
        });
		
		this.grid.addItemClickListener(new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
						
				if (event.getPropertyId()!=null && ((Object[]) event.getItemId())[0].equals("Todos") && cmbCuantos.getValue().toString()!="0" &&  CurrentUser.get().contains("clave"))
				{
					/*
					 * Grafica de tendencia general comparando ejercicios
					 */
					ventanaGraficaTendencia((Object[]) event.getItemId());
				}
			}
		});

		this.layoutTabla.addComponent(this.grid);
        this.btnGenerar.setEnabled(true);
	}
	
	private void cargarEjercicios()
	{		
		consultaVentasServer cv=null;
		Iterator<String> iterator =null;
		ArrayList<String> ej =null;
		int veces = 0;
		
		this.cmbCuantos.removeAllItems();
		this.cmbEjercicios.removeAllItems();
		
		if (cv==null)
		{
			cv = new consultaVentasServer(eBorsao.get().accessControl.getNombre());
		}
		
		ej = cv.cargarEjercicios();
		iterator = ej.iterator();
		
		if (ej!=null)
		{
			while (iterator.hasNext())
			{
				iterator.next();
				this.cmbCuantos.addItem(String.valueOf(veces));
				this.cmbEjercicios.addItem(String.valueOf(ej.get(veces)));
				veces=veces+1;
			}		
			this.cmbEjercicios.setValue(ej.get(0));
			this.primerEjercicio= new Integer(ej.get(0).toString());
			this.cmbCuantos.setValue("0");
		}
	}
	
	private void generarHashOpciones()
	{
		this.opcionesEscogidas = new HashMap<String, String>();
		/*
		 * Guardo las opciones escogidas por el usuario 
		 * 
		 */
		this.opcionesEscogidas.put("Usuario", eBorsao.get().accessControl.getNombre());
		this.opcionesEscogidas.put("Ejercicio", this.cmbEjercicios.getValue().toString());
		this.opcionesEscogidas.put("Comparativos", this.cmbCuantos.getValue().toString());
		this.opcionesEscogidas.put("Desde", this.desdeFecha.getFecha());
		this.opcionesEscogidas.put("HastaFecha", this.hastaFecha.getFecha());
		this.opcionesEscogidas.put("cod_campo1", this.camposAgrupacion(this.cmbCampo1.getValue().toString()));
		this.opcionesEscogidas.put("des_campo1", this.descriptoresAgrupacion(this.cmbCampo1.getValue().toString()));
		
		this.opcionesEscogidas.put("factor", this.cmbFactor.getValue().toString());
		this.opcionesEscogidas.put("anada", this.cmbAñada.getValue().toString());
		this.opcionesEscogidas.put("campo1dsd", "0");
		this.opcionesEscogidas.put("campo1hst", "999999999");
		this.opcionesEscogidas.put("campo2dsd", null);
		this.opcionesEscogidas.put("campo2hst", null);
		this.opcionesEscogidas.put("campo3dsd", null);
		this.opcionesEscogidas.put("campo3hst", null);
		
		if (this.cmbCampo2.getValue()!=null && !this.cmbCampo2.getValue().equals(""))
		{
			opcionesEscogidas.put("cod_campo2", this.camposAgrupacion(this.cmbCampo2.getValue().toString()));
			opcionesEscogidas.put("des_campo2", this.descriptoresAgrupacion(this.cmbCampo2.getValue().toString()));
			opcionesEscogidas.put("campo2dsd", "0");
			opcionesEscogidas.put("campo2hst", "999999999");
		}
		if (this.cmbCampo3.getValue()!=null && !this.cmbCampo3.getValue().equals(""))
		{	
			opcionesEscogidas.put("cod_campo3", this.camposAgrupacion(this.cmbCampo3.getValue().toString()));
			opcionesEscogidas.put("des_campo3", this.descriptoresAgrupacion(this.cmbCampo3.getValue().toString()));
			opcionesEscogidas.put("campo3dsd", "0");
			opcionesEscogidas.put("campo3hst", "999999999");
		}
		
		if (this.hashRangos!=null)
		{
			if (this.hashRangos.get("campo1dsd")!=null)
			{
				this.opcionesEscogidas.put("campo1dsd", this.hashRangos.get("campo1dsd"));
				this.opcionesEscogidas.put("campo1hst", this.hashRangos.get("campo1hst"));
			}
			if (this.cmbCampo2.getValue()!=null && !this.cmbCampo2.getValue().equals(""))
			{
				if (this.hashRangos.get("campo2dsd")!=null)
				{
					this.opcionesEscogidas.put("campo2dsd", this.hashRangos.get("campo2dsd"));
					this.opcionesEscogidas.put("campo2hst", this.hashRangos.get("campo2hst"));
				}
				
			}
			if (this.cmbCampo3.getValue()!=null && !this.cmbCampo3.getValue().equals(""))
			{
				if (this.hashRangos.get("campo3dsd")!=null)
				{
					this.opcionesEscogidas.put("campo3dsd", this.hashRangos.get("campo3dsd"));
					this.opcionesEscogidas.put("campo3hst", this.hashRangos.get("campo3hst"));
				}
				
			}
		}
	}

	private String camposAgrupacion(String r_opcion)
	{
		String cadenaDevolver=null;
		switch (r_opcion.toLowerCase()) 
		{
	        case "pais":
			{
				cadenaDevolver="cod_pais";
				break;
			}
	        case "cliente":
			{
				cadenaDevolver="cod_cliente";
				break;
			}
	        case "articulo":
			{
				cadenaDevolver="articulo";
				break;
			}
	        case "gremio":
			{
				cadenaDevolver="cod_gremio";
				break;
			}
	        case "familia":
			{
				cadenaDevolver="cod_familia";
				break;
			}
	        case "grupo cliente":
			{
				cadenaDevolver="cod_nivel";
				break;
			}
	        case "grupo articulo":
			{
				cadenaDevolver="cod_grupo_articulo";
				break;
			}
		}
		return cadenaDevolver;
	}
	
	private String descriptoresAgrupacion(String r_opcion)
	{
		String cadenaDevolver=null;
		switch (r_opcion.toLowerCase()) 
		{
	        case "pais":
			{
				cadenaDevolver="des_pais";
				break;
			}
	        case "cliente":
			{
				cadenaDevolver="des_cliente";
				break;
			}
	        case "articulo":
			{
				cadenaDevolver="descripcion";
				break;
			}
	        case "gremio":
			{
				cadenaDevolver="gremio";
				break;
			}
	        case "familia":
			{
				cadenaDevolver="familia";
				break;
			}
	        case "grupo cliente":
			{
				cadenaDevolver="des_grupo_cliente";
				break;
			}
	        case "grupo articulo":
			{
				cadenaDevolver="des_grupo_articulo";
				break;
			}
		}
		return cadenaDevolver;
	}
	
	private void cargarCombos(ComboBox r_combo)
	{
		r_combo.addItem("Pais");
		r_combo.addItem("Cliente");
		r_combo.addItem("Articulo");
		r_combo.addItem("Familia");
		r_combo.addItem("Gremio");
		r_combo.addItem("Grupo Cliente");
		r_combo.addItem("Grupo Articulo");
	}
	private void cargarCombosSN(ComboBox r_combo)
	{
		r_combo.addItem("Si");
		r_combo.addItem("No");
		r_combo.setValue("No");
		r_combo.setNullSelectionAllowed(false);
		r_combo.setWidth("75px");
	}

	private void cargarCombosFactor(ComboBox r_combo)
	{
		r_combo.addItem("Si");
		r_combo.addItem("No");
		r_combo.addItem("Cajas");
		r_combo.setValue("Si");
		r_combo.setNullSelectionAllowed(false);
		r_combo.setWidth("100px");
	}

	private void ventanaRangos()
	{
		this.generarHashOpciones();
		this.vt = new ventanaRangos(this,this.opcionesEscogidas);
		getUI().addWindow(this.vt);	
	}

	private void ventanaGraficaTendencia(Object[] r_fila)
	{
		ventanaGrafica vtG  =null;			
		vtG = new ventanaGrafica(r_fila, new Integer(this.cmbCuantos.getValue().toString()), new Integer(this.cmbEjercicios.getValue().toString()));
		getUI().addWindow(vtG);	
	}

//	private void ventanaGraficaBarras(Object[] r_fila)
//	{
//		ventanaGrafica vtG  =null;			
//		vtG = new ventanaGrafica(r_fila, new Integer(this.cmbCuantos.getValue().toString()), new Integer(this.cmbEjercicios.getValue().toString()),this.opcionesEscogidas.get("cod_campo1"),this.opcionesEscogidas.get("cod_campo2"),this.opcionesEscogidas.get("cod_campo3"));
//		getUI().addWindow(vtG);	
//	}

	private void inicializarRangos()
	{
		this.hashRangos=new HashMap<String, String>();
		this.hashRangos.put("campo1dsd", "0");
		this.hashRangos.put("campo1hst", "999999999");
		this.hashRangos.put("campo2dsd", "0");
		this.hashRangos.put("campo2hst", "999999999");
		this.hashRangos.put("campo3dsd", "0");
		this.hashRangos.put("campo3hst", "999999999");
		this.btnRangos.setCaption("Rangos");
	}
	
	
}



/*
 *  
*/