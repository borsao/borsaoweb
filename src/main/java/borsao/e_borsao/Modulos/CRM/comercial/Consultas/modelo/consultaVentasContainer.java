package borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.vaadin.treegrid.container.Measurable;

import com.vaadin.data.Collapsible;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.FontAwesome;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

import borsao.e_borsao.Modulos.CRM.comercial.Pruebas.consultaVentasItemSorter;


public class consultaVentasContainer extends HierarchicalContainer implements Collapsible, Cloneable, Measurable {

	
    static HashMap<String, String> opcionesSeleccionadas = null;    
    consultaVentasDataSource cvds = null;
    public ResultSet rsGenerado = null;
    
    public consultaVentasContainer(HashMap<String, String> r_hashOpciones, boolean r_eliminar) 
    {
    	
    	if (r_eliminar)
    	{
    		removeChildrenRecursively(cvds.getRoot());
    		cvds=null;
    	}
    	
    	this.opcionesSeleccionadas= r_hashOpciones;    	
    	cvds = new consultaVentasDataSource(this.opcionesSeleccionadas, r_eliminar);
    	this.rsGenerado=cvds.rs;
    	
    	/*
    	 * Cargo titulos columnas GRID
    	 */
    	
    	addContainerProperty("Descripcion",String.class,"");
    	if (opcionesSeleccionadas.get("anada")=="Si")
    	{
    		addContainerProperty("Añada",String.class,"");
    	}
    	
    	int i = (new Integer(opcionesSeleccionadas.get("Comparativos"))).intValue();
    	Integer ejercicio = new Integer(opcionesSeleccionadas.get("Ejercicio"));
    	String campo = "Ud";
    	if (opcionesSeleccionadas.get("factor").equals("Cajas"))
    	{
    		campo="Cj";    				
    	}
    	for (int veces=0;veces <= i;veces++)
    	{
    		addContainerProperty(ejercicio - veces + " " + campo, Double.class, 0);    		
    		addContainerProperty(ejercicio - veces + " €", Double.class, 0);
    		addContainerProperty(ejercicio - veces + " PVM", Double.class, 0);
    		if (veces>=0 && veces < i)
    		{
        		addContainerProperty("Diff " + (ejercicio - veces) + " " + campo, String.class, "");
        		addContainerProperty("Diff " + (ejercicio - veces) + " €", String.class, "");        		
    		}
    	}
    	
        for (Object[] r : cvds.getRoot()) {
            addItem(r);
        }

        setItemSorter(new consultaVentasItemSorter());
    }

    private Object addItem(Object[] values) {
        Item item = addItem((Object) values);
        setProperties(item, values);
        return values;
    }

    private Object addChild(Object[] values, Object parentId) {
        Item item = addItemAfter(parentId, values);
        setProperties(item, values);
        setParent(values, parentId);
        return values;
    }

    private void setProperties(Item item, Object[] values) {
    	
    	int columnas = 0;
    	
    	item.getItemProperty("Descripcion").setValue(values[columnas]);
    	columnas=columnas +1;
    	
    	if (opcionesSeleccionadas.get("anada")=="Si")
    	{
    		item.getItemProperty("Añada").setValue(values[columnas]);
    		columnas=columnas +1;
    	}
    	
    	int i = (new Integer(opcionesSeleccionadas.get("Comparativos"))).intValue();
    	Integer ejercicio = new Integer(opcionesSeleccionadas.get("Ejercicio"));
    	
    	try    	
    	{
    		String campo = "Ud";
        	if (opcionesSeleccionadas.get("factor").equals("Cajas"))
        	{
        		campo="Cj";    				
        	}
	    	for (int veces=0;veces <= i;veces++)
	    	{
	        	
	    		item.getItemProperty(ejercicio - veces + " " + campo).setValue((Double) values[columnas]);
	    		columnas=columnas +1;
	    		item.getItemProperty(ejercicio - veces + " €").setValue((Double) values[columnas]);
	    		columnas=columnas +1;
	    		item.getItemProperty(ejercicio - veces + " PVM").setValue(values[columnas]);
	    		columnas=columnas +1;    		

	    		/*
	    		 * Agregaremos columnas para las diferencias
	    		 * Devolveremos la diferencia en formato texto para poder concatenar el icono	    		
	    		 */
	    		if (veces>=0 && veces <i)
	    		{
	    			
	    			String resultado = calcularColor((Double) values[columnas],(Double) values[columnas+1]);
	    			
	    			item.getItemProperty("Diff " + (ejercicio - veces) + " " + campo).setValue(resultado);	    			
		    		columnas=columnas +2;
		    		
		    		
		    		resultado = calcularColor((Double) values[columnas],(Double) values[columnas+1]);
	    			item.getItemProperty("Diff " + (ejercicio - veces) + " €").setValue(resultado);
	    			columnas=columnas+2;
	    		}
	    		
	    	}
    	}
    	catch (NumberFormatException nfEx)
    	{
    		nfEx.getMessage();
    	}
    }

    @Override
    public void setCollapsed(Object itemId, boolean collapsed) {
        expandedNodes.put(itemId, !collapsed);

        if (collapsed) {
            // remove children
            removeChildrenRecursively(itemId);
        } else {
            // lazy load children
            addChildren(itemId);
        }
    }

    private void addChildren(Object itemId) {
        for (Object[] child : cvds.getChildren(itemId)) {
            Object childId = addChild(child, itemId);
            if (Boolean.TRUE.equals(expandedNodes.get(childId))) {
                addChildren(childId);
            }
        }
    }

    private boolean removeChildrenRecursively(Object itemId) {
        boolean success = true;
        Collection<?> children2 = getChildren(itemId);
        if (children2 != null) {
            Object[] array = children2.toArray();
            for (int i = 0; i < array.length; i++) {
                boolean removeItemRecursively = removeItemRecursively(
                        this, array[i]);
                if (!removeItemRecursively) {
                    success = false;
                }
            }
        }
        return success;

    }

    @Override
    public boolean hasChildren(Object itemId) {
        return !cvds.isLeaf(itemId);
    }

    private Map<Object, Boolean> expandedNodes = new HashMap<>();

    @Override
    public boolean isCollapsed(Object itemId) {
        return !Boolean.TRUE.equals(expandedNodes.get(itemId));
    }

    @Override
    public int getDepth(Object itemId) {
        int depth = 0;
        while (!isRoot(itemId)) {
            depth ++;
            itemId = getParent(itemId);
        }
        return depth;
    }
    
    public static String getColumnaPrincipal()
    {
    	return "Descripcion";   	
    }
    
    
    private String calcularColor(Double r_valor, Double r_valorMostrar)
    {
    	String iconCode =null;
    	String color = null;
    	
    	if (r_valor>5)
    	{
    		/*
    		 * Subida de ventas por encima del 5%
    		 */
    		color="#2dd085";
    	}
    	else if (r_valor < -5)
    	{
    		/*
    		 * Bajada de ventas superior al 5%
    		 */
    		color = "#cf050a";
    		
    	}
    	else 
    	{
    		/*
    		 * Bajada o Subida en un rango de mas menos 5% 
    		 */
    		color = "#ffc66e";
    	}
    	
		iconCode = "<span class=\"v-icon\" style=\"font-family: "
                + FontAwesome.CIRCLE.getFontFamily() + ";color:" + color
                + "\">&#x"
                + Integer.toHexString(FontAwesome.CIRCLE.getCodepoint())
                + ";</span>";

		return RutinasNumericas.formatearDouble(r_valorMostrar) + " " + iconCode;
    }
    
    public consultaVentasContainer clone() throws CloneNotSupportedException{
    	consultaVentasContainer clonado = (consultaVentasContainer) super.clone();
        return clonado;
   }
}