package borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo;

public class MapeoVentas
{
	
    private Integer ejercicio;
    private Integer	orden;
    private Integer idCodigo;
	private String codigo;
	private String descripcion;
	private Integer nivel; 
	private Double unidades;
	private Double importe;
	private Double medio;
	
	private String desdeFecha;
	private String hastaFecha;
	private String nivel1;
	private String nivel2;
	private String nivel3;
	
	public MapeoVentas()
	{
		this.setNivel1("");
		this.setNivel2("");
		this.setNivel3("");
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getIdCodigo() {
		return idCodigo;
	}

	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getUnidades() {
		return unidades;
	}

	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Double getMedio() {
		return medio;
	}

	public void setMedio(Double medio) {
		this.medio = medio;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public String getDesdeFecha() {
		return desdeFecha;
	}

	public void setDesdeFecha(String desdeFecha) {
		this.desdeFecha = desdeFecha;
	}

	public String getHastaFecha() {
		return hastaFecha;
	}

	public void setHastaFecha(String hastaFecha) {
		this.hastaFecha = hastaFecha;
	}

	public String getNivel1() {
		return nivel1;
	}

	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}

	public String getNivel2() {
		return nivel2;
	}

	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}

	public String getNivel3() {
		return nivel3;
	}

	public void setNivel3(String nivel3) {
		this.nivel3 = nivel3;
	}


}