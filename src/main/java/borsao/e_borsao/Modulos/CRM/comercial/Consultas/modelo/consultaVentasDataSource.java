package borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;

public class consultaVentasDataSource {

	static HashMap<String, String> opcionesUsuario = null;
	private static consultaVentasServer cvs = null;
    private static List<Object[]> rootNodes = null;
    private static Map<Object, List<Object[]>> children = new HashMap<>();
    private static Set<Object[]> leaves = new HashSet<>();
    public ResultSet rs = null;

    public consultaVentasDataSource(HashMap<String, String> r_hashOpciones, boolean r_eliminar) {
    	
    	if (r_eliminar)
    	{
    		this.rootNodes=null;
    	}
    	this.rootNodes = new ArrayList<>();
    	this.setOpciones(r_hashOpciones);
        rs = populateWithRandomHierarchicalData();
        
    }

    static void setOpciones(HashMap<String, String> r_hashOpciones)
    {
    	opcionesUsuario=r_hashOpciones;
    }
    
    static List<Object[]> getRoot() {
        return rootNodes;
    }

    static List<Object[]> getChildren(Object parent) {
        return children.get(parent);
    }

    static boolean isLeaf(Object itemId) {
        return leaves.contains(itemId);
    }

    private static ResultSet populateWithRandomHierarchicalData() {
        ResultSet datosVentas = null;
        ResultSet datosVentasC2 = null;
        ResultSet datosVentasC3 = null;
        ResultSet rs = null;
        Integer r_ejercicio= null;
        
        int columnas=0;

        Object[] allProjects = new Object[]{"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};        
        
        Notificaciones.getInstance().mensajeSeguimiento("Empezamos" + new Date());
        cvs=new consultaVentasServer(opcionesUsuario.get("Usuario").toString());

        datosVentas = cvs.datosVentasGlobal(opcionesUsuario,null,null,null);
        Notificaciones.getInstance().mensajeSeguimiento("Global " + new Date());
        if (datosVentas!=null)
        {
    		try
    		{
    			datosVentas.beforeFirst();
				while(datosVentas.next())
				{
					allProjects[columnas]="Todos";
					columnas=columnas +1;
					if (opcionesUsuario.get("anada")=="Si")
					{
						allProjects[columnas]="Todas";
						columnas=columnas +1;
					}
					
					int i = (new Integer(opcionesUsuario.get("Comparativos"))).intValue();
					r_ejercicio=new Integer(opcionesUsuario.get("Ejercicio"));
					
					for (int veces=0;veces <= i;veces++)
					{
						int digito = r_ejercicio-veces;
						allProjects[columnas]=datosVentas.getDouble("ud" + digito);
						columnas=columnas +1;
						allProjects[columnas]=datosVentas.getDouble("imp" + digito);
						columnas=columnas +1;
						allProjects[columnas]=datosVentas.getDouble("pvm" + digito);
						columnas=columnas +1;
						if (veces>=0 && veces <i)
						{
							int digitoAnterior = r_ejercicio-veces - 1;
							allProjects[columnas]=calcularPorcentajeEvolucion(datosVentas.getDouble("ud" + digito),datosVentas.getDouble("ud" + digitoAnterior));
							columnas=columnas +1;
							allProjects[columnas]=datosVentas.getDouble("ud" + digito)-datosVentas.getDouble("ud" + digitoAnterior);
							columnas=columnas +1;
							allProjects[columnas]=calcularPorcentajeEvolucion(datosVentas.getDouble("imp" + digito),datosVentas.getDouble("imp" + digitoAnterior));
							columnas=columnas +1;							
							allProjects[columnas]=datosVentas.getDouble("imp" + digito)-datosVentas.getDouble("imp" + digitoAnterior);
							columnas=columnas +1;							
						}
					}
				}
				
				datosVentas = cvs.datosVentasGlobal(opcionesUsuario,opcionesUsuario.get("cod_campo1"),null,null);
				Notificaciones.getInstance().mensajeSeguimiento("Campo 1 " + new Date());
				if (datosVentas!=null)
		        {
					if (opcionesUsuario.get("cod_campo2")!=null && opcionesUsuario.get("cod_campo2").toString()!="")
					{
						datosVentasC2 = cvs.datosVentasGlobal(opcionesUsuario,opcionesUsuario.get("cod_campo1"),opcionesUsuario.get("cod_campo2"),null);
						Notificaciones.getInstance().mensajeSeguimiento("Campo 2 " + new Date());
					}
					if (opcionesUsuario.get("cod_campo3")!=null && opcionesUsuario.get("cod_campo3").toString()!="")
					{
						datosVentasC3 = cvs.datosVentasGlobal(opcionesUsuario,opcionesUsuario.get("cod_campo1"),opcionesUsuario.get("cod_campo2"),opcionesUsuario.get("cod_campo3"));
						Notificaciones.getInstance().mensajeSeguimiento("Campo 3 " + new Date());
					}
					datosVentas.beforeFirst();
					while(datosVentas.next())
					{
						final Object[] Campo1 = new  Object[]{"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};   
						columnas=0;
						Campo1[columnas]=datosVentas.getString("cod_campo1") + " " + RutinasCadenas.conversion(datosVentas.getString("campo1").trim());
						columnas=columnas +1;
						if (opcionesUsuario.get("anada")=="Si")
						{
							Campo1[columnas]=datosVentas.getString("anada");
							columnas=columnas +1;
						}
						
						int i = (new Integer(opcionesUsuario.get("Comparativos"))).intValue();
						r_ejercicio=new Integer(opcionesUsuario.get("Ejercicio"));
						for (int veces=0;veces <= i;veces++)
						{
							int digito = r_ejercicio-veces;
							Campo1[columnas]=datosVentas.getDouble("ud" + digito);
							columnas=columnas +1;
							Campo1[columnas]=datosVentas.getDouble("imp" + digito);
							columnas=columnas +1;
							Campo1[columnas]=datosVentas.getDouble("pvm" + digito);
							columnas=columnas +1;
							if (veces>=0 && veces <i)
							{
								int digitoAnterior = r_ejercicio-veces - 1;
								
								Campo1[columnas]=calcularPorcentajeEvolucion(datosVentas.getDouble("ud" + digito),datosVentas.getDouble("ud" + digitoAnterior));
								columnas=columnas +1;
								Campo1[columnas]=datosVentas.getDouble("ud" + digito)-datosVentas.getDouble("ud" + digitoAnterior);
								columnas=columnas +1;
								Campo1[columnas]=calcularPorcentajeEvolucion(datosVentas.getDouble("imp" + digito),datosVentas.getDouble("imp" + digitoAnterior));
								columnas=columnas +1;							
								Campo1[columnas]=datosVentas.getDouble("imp" + digito)-datosVentas.getDouble("imp" + digitoAnterior);
								columnas=columnas +1;							
							}
						}
						if (opcionesUsuario.get("cod_campo2")==null || opcionesUsuario.get("cod_campo2").toString()=="")
						{
							leaves.add(Campo1);
						}
						addChild(allProjects, Campo1);
						
						if (opcionesUsuario.get("cod_campo2")!=null && opcionesUsuario.get("cod_campo2").toString()!="")
						{
							if (datosVentasC2!=null)
							{
								datosVentasC2.beforeFirst();
								while(datosVentasC2.next())
								{
									if (datosVentas.getString("campo1").equals(datosVentasC2.getString("campo1")))
									{
										Object[] Campo2 = new  Object[]{"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};   
										columnas=0;
										Campo2[columnas]=datosVentasC2.getString("cod_campo2") + " " + RutinasCadenas.conversion(datosVentasC2.getString("campo2").trim());
										columnas=columnas + 1;
										if (opcionesUsuario.get("anada")=="Si")
										{
											Campo2[columnas]=datosVentasC2.getString("anada");
											columnas=columnas + 1;
										}
										
										int j = (new Integer(opcionesUsuario.get("Comparativos"))).intValue();
										r_ejercicio=new Integer(opcionesUsuario.get("Ejercicio"));
										for (int vecesC2=0;vecesC2 <= j;vecesC2++)
										{
											int digito = r_ejercicio-vecesC2;
											Campo2[columnas]=datosVentasC2.getDouble("ud" + digito);
											columnas=columnas +1;
											Campo2[columnas]=datosVentasC2.getDouble("imp" + digito);
											columnas=columnas +1;
											Campo2[columnas]=datosVentasC2.getDouble("pvm" + digito);
											columnas=columnas +1;
											if (vecesC2>=0 && vecesC2 <i)
											{
												int digitoAnterior = r_ejercicio-vecesC2 - 1;
												Campo2[columnas]=calcularPorcentajeEvolucion(datosVentasC2.getDouble("ud" + digito),datosVentasC2.getDouble("ud" + digitoAnterior));
												columnas=columnas +1;
												Campo2[columnas]=datosVentasC2.getDouble("ud" + digito)-datosVentasC2.getDouble("ud" + digitoAnterior);
												columnas=columnas +1;
												Campo2[columnas]=calcularPorcentajeEvolucion(datosVentasC2.getDouble("imp" + digito),datosVentasC2.getDouble("imp" + digitoAnterior));
												columnas=columnas +1;							
												Campo2[columnas]=datosVentasC2.getDouble("imp" + digito)-datosVentasC2.getDouble("imp" + digitoAnterior);
												columnas=columnas +1;							
											}
										}
										if (opcionesUsuario.get("cod_campo3")==null || opcionesUsuario.get("cod_campo3").toString()=="")
										{
											leaves.add(Campo2);									
										}
										addChild(Campo1, Campo2);
										
										if (opcionesUsuario.get("cod_campo3")!=null && opcionesUsuario.get("cod_campo3").toString()!="")
										{
											if (datosVentasC3!=null)
											{
												datosVentasC3.beforeFirst();
												while(datosVentasC3.next())
												{
													if (datosVentas.getString("campo1").equals(datosVentasC2.getString("campo1")) && datosVentas.getString("campo1").equals(datosVentasC3.getString("campo1")) 
															&& datosVentasC2.getString("campo1").equals(datosVentasC3.getString("campo1")) && datosVentasC2.getString("campo2").equals(datosVentasC3.getString("campo2")))
													{
														Object[] Campo3 = new  Object[]{"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};   
														columnas=0;
														Campo3[columnas]=datosVentasC3.getString("cod_campo3") + " " + RutinasCadenas.conversion(datosVentasC3.getString("campo3"));
														columnas=columnas + 1;
														if (opcionesUsuario.get("anada")=="Si")
														{
															Campo3[columnas]=datosVentasC3.getString("anada");
															columnas=columnas + 1;
														}
														
														int k = (new Integer(opcionesUsuario.get("Comparativos"))).intValue();
														r_ejercicio=new Integer(opcionesUsuario.get("Ejercicio"));
														for (int vecesC3=0;vecesC3 <= k;vecesC3++)
														{
															int digito = r_ejercicio-vecesC3;
															Campo3[columnas]=datosVentasC3.getDouble("ud" + digito);
															columnas=columnas +1;
															Campo3[columnas]=datosVentasC3.getDouble("imp" + digito);
															columnas=columnas +1;
															Campo3[columnas]=datosVentasC3.getDouble("pvm" + digito);
															columnas=columnas +1;
															if (vecesC3>=0 && vecesC3 <i)
															{
																int digitoAnterior = r_ejercicio-vecesC3 - 1;
																Campo3[columnas]=calcularPorcentajeEvolucion(datosVentasC3.getDouble("ud" + digito),datosVentasC3.getDouble("ud" + digitoAnterior));
																columnas=columnas +1;
																Campo3[columnas]=datosVentasC3.getDouble("ud" + digito)-datosVentasC3.getDouble("ud" + digitoAnterior);
																columnas=columnas +1;
																Campo3[columnas]=calcularPorcentajeEvolucion(datosVentasC3.getDouble("imp" + digito),datosVentasC3.getDouble("imp" + digitoAnterior));
																columnas=columnas +1;								
																Campo3[columnas]=datosVentasC3.getDouble("imp" + digito)-datosVentasC3.getDouble("imp" + digitoAnterior);
																columnas=columnas +1;								
															}
														}
														leaves.add(Campo3);
														addChild(Campo2, Campo3);
													}
												}
											}
										}
									}
								}
							}
						}
					}
					
					if (opcionesUsuario.get("cod_campo3")!=null && opcionesUsuario.get("cod_campo3").toString()!="")
					{
						if (datosVentasC3!=null)
						{
							rs= datosVentasC3;
						}
					}
					else if (opcionesUsuario.get("cod_campo2")!=null && opcionesUsuario.get("cod_campo2").toString()!="")
					{
						if (datosVentasC2!=null)
						{
							rs = datosVentasC2;
						}
					}
					else 
					{
						if (datosVentas!=null)
						{
							rs = datosVentas;
						}
					}				
		        }
    		}
    		catch (SQLException sqlEx)
    		{
    			sqlEx.printStackTrace();
    		}
    		        
        }
        rootNodes.add(allProjects);
        Notificaciones.getInstance().mensajeSeguimiento("Terminamos" + new Date());
        return rs;
    }

    private static void addChild(Object parent, Object[] child) {
        if (!children.containsKey(parent)) {
            children.put(parent, new ArrayList<Object[]>());
        }
        children.get(parent).add(child);
    }
    

	private static Double calcularPorcentajeEvolucion(Double r_valor1, Double r_valor2)
	{
		Double resultado=null;
		
		if (r_valor1!=0 && r_valor2==0)
		{
			return new Double(100);
		}
		else if (r_valor1==0 && r_valor2!=0)
		{
			return new Double(-100);
		}
		else if (r_valor1==0 && r_valor2==0)
		{
			return new Double(0);
		}
		else
		{
			resultado = (r_valor1/r_valor2-1)*100;
		}
		return resultado;
		
	}
}