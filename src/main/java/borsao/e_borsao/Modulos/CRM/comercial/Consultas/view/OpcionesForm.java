package borsao.e_borsao.Modulos.CRM.comercial.Consultas.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign  {
	
	 public HashMap<String , String> opcionesEscogidas = null;
	 private HashMap<String, String> hashRangos = null;
	 private Integer primerEjercicio = null;
	 private Notificaciones serNotif = Notificaciones.getInstance();
	 private ventanaMargenes vt  =null;	 
	 private VentasView vw = null;
	 
	 public OpcionesForm(VentasView r_vw) {
        super();
        vw=r_vw;
        addStyleName("mytheme product-form");
        
        this.cargarEjercicios();        	
		
		this.cargarCombos(this.cmbCampo1);
		this.cargarCombos(this.cmbCampo2);
		this.cargarCombos(this.cmbCampo3);
		this.cargarCombosFactor(this.cmbFactor);
		this.cargarCombosSN(this.cmbAnada);
		this.cmbAnada.setCaption("Añada");
		this.cargarListeners();
		
		this.desdeFecha.addStyleName("v-datefield-textfield");
		this.hastaFecha.addStyleName("v-datefield-textfield");
		
		this.desdeFecha.setDateFormat("dd/MM/yyyy");
		this.hastaFecha.setDateFormat("dd/MM/yyyy");
		this.cmbCampo1.setValue("Pais");
		this.btnGenerar.setDisableOnClick(true);
		
		if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master")) 
    	{
			this.cmbAnada.setEnabled(true);
    	}
		else
		{
			this.cmbAnada.setEnabled(false);
		}
    }   

    private void cargarListeners()
	{
    	desdeFecha.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				desdeFecha.setValue(RutinasFechas.conversion(desdeFecha.getValue()));					
			}
		});
    	
    	hastaFecha.addValueChangeListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				hastaFecha.setValue(RutinasFechas.conversion(hastaFecha.getValue()));
			}
		});
    	
		this.cmbCampo1.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
				if (cmbCampo1.getValue()!=null)
            	{
            		cmbCampo2.setEnabled(true);
            		cmbCampo3.setEnabled(false);
            		cmbCampo2.clear();
					cmbCampo3.clear();
					inicializarRangos();
            	}
				else if (cmbCampo1.getValue()!=null && cmbCampo2.getValue()!=null && !cmbCampo2.getValue().equals("") && !cmbCampo2.getValue().equals(cmbCampo1.getValue()) )
            	{
            		cmbCampo3.setEnabled(true);
					cmbCampo3.clear();	
					inicializarRangos();
            	}
				else if (cmbCampo1.getValue()!=null && cmbCampo2.getValue()!=null && cmbCampo3.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo1.getValue()) || cmbCampo3.getValue().equals(cmbCampo1.getValue()) || cmbCampo3.getValue().equals(cmbCampo2.getValue()) ))
            	{
            		cmbCampo2.clear();
					cmbCampo3.clear();
					inicializarRangos();
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
            	}
            	else
            	{
            		cmbCampo2.setEnabled(false);
        			cmbCampo3.setEnabled(false);
        			inicializarRangos();
            	}
			}
	    });
		this.cmbCampo2.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
				if (cmbCampo2.getValue()!=null && !cmbCampo2.getValue().equals("") && cmbCampo3.getValue()==null && !cmbCampo2.getValue().equals(cmbCampo1.getValue()))
            	{
            		cmbCampo3.setEnabled(true);
					cmbCampo3.clear();
					inicializarRangos();
            	}
				else if (cmbCampo2.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo1.getValue()) || (cmbCampo3.getValue()!=null && cmbCampo3.getValue().equals(cmbCampo2.getValue()))))
            	{
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
					cmbCampo2.clear();
					inicializarRangos();
            	}
            	else
            	{	            		
        			cmbCampo3.setEnabled(false);
        			cmbCampo3.clear();
        			inicializarRangos();
            	}
			}
	    });
		this.cmbCampo3.addValueChangeListener(new Property.ValueChangeListener() 
		{
	    	public void valueChange(ValueChangeEvent event) 
	    	{
	    		if (cmbCampo3.getValue()!=null && (cmbCampo2.getValue().equals(cmbCampo3.getValue()) || (cmbCampo3.getValue().equals(cmbCampo1.getValue()))))
            	{
					serNotif.mensajeAdvertencia("No puedes seleccionar el mismo criterio más de una vez");
					cmbCampo3.clear();
					inicializarRangos();
            	}
			}
	    });
		this.btnGenerar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a ventas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			generarHashOpciones();
	 			activarDesactivarControles(false);
	 			if (vw.excelExporter!=null)
 				{
 					vw.topLayoutR.removeComponent(vw.excelExporter);
 					vw.topLayoutR.removeComponent(vw.pdf);
 				}
//	 			if (vw.pdfExporter!=null)
// 				{
// 					vw.topLayoutR.removeComponent(vw.pdfExporter);
// 				}
	 			if (vw.isHayGrid())
	 			{			
	 				vw.grid.removeAllColumns();			
	 				vw.barAndGridLayout.removeComponent(vw.grid);
	 				vw.grid=null;			
	 			}
	 			vw.ejercicioInicial=new Integer(cmbEjercicios.getValue().toString());
	 			vw.comparativos = new Integer(cmbCuantos.getValue().toString());
	 			vw.desdeFecha = RutinasFechas.convertirDateToString(desdeFecha.getValue());
	 			vw.hastaFecha = RutinasFechas.convertirDateToString(hastaFecha.getValue());
	 			vw.nivel1 = cmbCampo1.getValue().toString();
	 			if (cmbCampo2.getValue() != null) vw.nivel2 = cmbCampo2.getValue().toString();
	 			if (cmbCampo3.getValue() != null) vw.nivel3 = cmbCampo3.getValue().toString();
	 			
	 			vw.generarGrid(opcionesEscogidas);
	 			vw.setHayGrid(true);
	 			vw.botonesGenerales(true);
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
		this.btnRangos.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{
	 			ventanaMargenes();
			}
 		});
        
        btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
             setEnabled(false);
             vw.botonesGenerales(true);
            }
        });


		this.cmbEjercicios.addValueChangeListener(new Property.ValueChangeListener() 
		{
			public void valueChange(ValueChangeEvent event) 
			{
			
				int cuantos = cmbEjercicios.size();
				int primero = primerEjercicio.intValue();
				int estoy = new Integer(cmbEjercicios.getValue().toString()).intValue();
				
				cmbCuantos.removeAllItems();
				
				for (int j = 0; j<cuantos-(primero-estoy);j++)
				{
					cmbCuantos.addItem(String.valueOf(j));
				}
				cmbCuantos.setValue("0");
				
				EntradaDatosFecha fecha = new EntradaDatosFecha();
				
				desdeFecha.setRangeStart(null);
				desdeFecha.setRangeEnd(null);
				hastaFecha.setRangeStart(null);
				hastaFecha.setRangeEnd(null);

				String dateInString = "01/01/" + cmbEjercicios.getValue();
				desdeFecha.setRangeStart(fecha.convertirStringADate(dateInString));
				desdeFecha.setValue(fecha.convertirStringADate(dateInString));
				hastaFecha.setRangeStart(fecha.convertirStringADate(dateInString));

				dateInString = "31/12/" + cmbEjercicios.getValue();    			
    			desdeFecha.setRangeEnd(fecha.convertirStringADate(dateInString));
    			hastaFecha.setRangeEnd(fecha.convertirStringADate(dateInString));
    			hastaFecha.setValue(fecha.convertirStringADate(dateInString));

			}
		});		
	}
    
	private void inicializarRangos()
	{
		this.hashRangos=new HashMap<String, String>();
		this.hashRangos.put("campo1dsd", "0");
		this.hashRangos.put("campo1hst", "999999999");
		this.hashRangos.put("campo2dsd", "0");
		this.hashRangos.put("campo2hst", "999999999");
		this.hashRangos.put("campo3dsd", "0");
		this.hashRangos.put("campo3hst", "999999999");
		this.btnRangos.setCaption("Rangos");
	}


	
	private void cargarCombos(ComboBox r_combo)
	{
		r_combo.addItem("Pais");
		r_combo.addItem("Cliente");
		r_combo.addItem("Articulo");
		r_combo.addItem("Familia");
		r_combo.addItem("Gremio");
		r_combo.addItem("Grupo Cliente");
		r_combo.addItem("Grupo Articulo");
	}
	private void cargarCombosSN(ComboBox r_combo)
	{
		r_combo.addItem("Si");
		r_combo.addItem("No");
		r_combo.setValue("No");
		r_combo.setNullSelectionAllowed(false);
		r_combo.setWidth("75px");
	}

	private void cargarCombosFactor(ComboBox r_combo)
	{
		r_combo.addItem("Si");
		r_combo.addItem("No");
		r_combo.addItem("Cajas");
		r_combo.setValue("Si");
		r_combo.setNullSelectionAllowed(false);
		r_combo.setWidth("100px");
	}
	private void cargarEjercicios()
	{		
		consultaVentasServer cv=null;
		Iterator<String> iterator =null;
		ArrayList<String> ej =null;
		int veces = 0;
		
		this.cmbCuantos.removeAllItems();
		this.cmbEjercicios.removeAllItems();
		
		if (cv==null)
		{
			cv = new consultaVentasServer(eBorsao.get().accessControl.getNombre());
		}
		
		ej = cv.cargarEjercicios();
		iterator = ej.iterator();
		
		if (ej!=null)
		{
			while (iterator.hasNext())
			{
				iterator.next();
				this.cmbCuantos.addItem(String.valueOf(veces));
				this.cmbEjercicios.addItem(String.valueOf(ej.get(veces)));
				veces=veces+1;
			}		
			this.cmbEjercicios.setValue(ej.get(0));
			this.primerEjercicio= new Integer(ej.get(0).toString());
			this.cmbCuantos.setValue("0");
		}
		
		EntradaDatosFecha fecha = new EntradaDatosFecha();
		
		String dateInString = "01/01/" + cmbEjercicios.getValue();
		desdeFecha.setRangeStart(fecha.convertirStringADate(dateInString));
		desdeFecha.setValue(fecha.convertirStringADate(dateInString));
		hastaFecha.setRangeStart(fecha.convertirStringADate(dateInString));

		dateInString = "31/12/" + cmbEjercicios.getValue();    			
		desdeFecha.setRangeEnd(fecha.convertirStringADate(dateInString));
		hastaFecha.setRangeEnd(fecha.convertirStringADate(dateInString));
		hastaFecha.setValue(fecha.convertirStringADate(dateInString));
	}
	
	private void ventanaMargenes()
	{
		this.generarHashOpciones();
		this.vt = new ventanaMargenes(this,this.opcionesEscogidas);
		getUI().addWindow(this.vt);	
	}
	
	private void generarHashOpciones()
	{
		this.opcionesEscogidas = new HashMap<String, String>();
		/*
		 * Guardo las opciones escogidas por el usuario 
		 * 
		 */
		this.opcionesEscogidas.put("Usuario", eBorsao.get().accessControl.getNombre());
		this.opcionesEscogidas.put("Ejercicio", this.cmbEjercicios.getValue().toString());
		this.opcionesEscogidas.put("Comparativos", this.cmbCuantos.getValue().toString());
		EntradaDatosFecha fecha = new EntradaDatosFecha();
		this.opcionesEscogidas.put("Desde", fecha.convertirDateToString(this.desdeFecha.getValue()));
		this.opcionesEscogidas.put("HastaFecha", fecha.convertirDateToString(this.hastaFecha.getValue()));
		this.opcionesEscogidas.put("cod_campo1", this.camposAgrupacion(this.cmbCampo1.getValue().toString()));
		this.opcionesEscogidas.put("des_campo1", this.descriptoresAgrupacion(this.cmbCampo1.getValue().toString()));
		
		this.opcionesEscogidas.put("factor", this.cmbFactor.getValue().toString());
		this.opcionesEscogidas.put("anada", this.cmbAnada.getValue().toString());
		this.opcionesEscogidas.put("campo1dsd", "0");
		this.opcionesEscogidas.put("campo1hst", "999999999");
		this.opcionesEscogidas.put("campo2dsd", null);
		this.opcionesEscogidas.put("campo2hst", null);
		this.opcionesEscogidas.put("campo3dsd", null);	
		this.opcionesEscogidas.put("campo3hst", null);
		
		if (this.cmbCampo2.getValue()!=null && !this.cmbCampo2.getValue().equals(""))
		{
			opcionesEscogidas.put("cod_campo2", this.camposAgrupacion(this.cmbCampo2.getValue().toString()));
			opcionesEscogidas.put("des_campo2", this.descriptoresAgrupacion(this.cmbCampo2.getValue().toString()));
			opcionesEscogidas.put("campo2dsd", "0");
			opcionesEscogidas.put("campo2hst", "999999999");
		}
		if (this.cmbCampo3.getValue()!=null && !this.cmbCampo3.getValue().equals(""))
		{	
			opcionesEscogidas.put("cod_campo3", this.camposAgrupacion(this.cmbCampo3.getValue().toString()));
			opcionesEscogidas.put("des_campo3", this.descriptoresAgrupacion(this.cmbCampo3.getValue().toString()));
			opcionesEscogidas.put("campo3dsd", "0");
			opcionesEscogidas.put("campo3hst", "999999999");
		}
		
		if (this.hashRangos!=null)
		{
			if (this.hashRangos.get("campo1dsd")!=null)
			{
				this.opcionesEscogidas.put("campo1dsd", this.hashRangos.get("campo1dsd"));
				this.opcionesEscogidas.put("campo1hst", this.hashRangos.get("campo1hst"));
			}
			if (this.cmbCampo2.getValue()!=null && !this.cmbCampo2.getValue().equals(""))
			{
				if (this.hashRangos.get("campo2dsd")!=null)
				{
					this.opcionesEscogidas.put("campo2dsd", this.hashRangos.get("campo2dsd"));
					this.opcionesEscogidas.put("campo2hst", this.hashRangos.get("campo2hst"));
				}
				
			}
			if (this.cmbCampo3.getValue()!=null && !this.cmbCampo3.getValue().equals(""))
			{
				if (this.hashRangos.get("campo3dsd")!=null)
				{
					this.opcionesEscogidas.put("campo3dsd", this.hashRangos.get("campo3dsd"));
					this.opcionesEscogidas.put("campo3hst", this.hashRangos.get("campo3hst"));
				}
				
			}
		}
	}
	
	private String camposAgrupacion(String r_opcion)
	{
		String cadenaDevolver=null;
		switch (r_opcion.toLowerCase()) 
		{
	        case "pais":
			{
				cadenaDevolver="cod_pais";
				break;
			}
	        case "cliente":
			{
				cadenaDevolver="cod_cliente";
				break;
			}
	        case "articulo":
			{
				cadenaDevolver="articulo";
				break;
			}
	        case "gremio":
			{
				cadenaDevolver="cod_gremio";
				break;
			}
	        case "familia":
			{
				cadenaDevolver="cod_familia";
				break;
			}
	        case "grupo cliente":
			{
				cadenaDevolver="cod_nivel";
				break;
			}
	        case "grupo articulo":
			{
				cadenaDevolver="cod_grupo_articulo";
				break;
			}
		}
		return cadenaDevolver;
	}
	
	private String descriptoresAgrupacion(String r_opcion)
	{
		String cadenaDevolver=null;
		switch (r_opcion.toLowerCase()) 
		{
	        case "pais":
			{
				cadenaDevolver="des_pais";
				break;
			}
	        case "cliente":
			{
				cadenaDevolver="des_cliente";
				break;
			}
	        case "articulo":
			{
				cadenaDevolver="descripcion";
				break;
			}
	        case "gremio":
			{
				cadenaDevolver="gremio";
				break;
			}
	        case "familia":
			{
				cadenaDevolver="familia";
				break;
			}
	        case "grupo cliente":
			{
				cadenaDevolver="des_grupo_cliente";
				break;
			}
	        case "grupo articulo":
			{
				cadenaDevolver="des_grupo_articulo";
				break;
			}
		}
		return cadenaDevolver;
	}
	
	public void establecerRangos(HashMap<String, String> r_hash, boolean r_cambios)
	{
		this.hashRangos = r_hash;
		if (r_cambios)
		{
			this.btnRangos.setCaption("Rangos (*)");
		}
		else
		{
			this.btnRangos.setCaption("Rangos");
		}
		this.generarHashOpciones();
		getUI().removeWindow(this.vt);
		this.vt=null;
	}

	private void activarDesactivarControles(boolean r_activoono)
	{
		btnGenerar.setEnabled(r_activoono);
		btnRangos.setEnabled(r_activoono);
		btnCancel.setEnabled(r_activoono);
		cmbAnada.setEnabled(r_activoono);
		cmbFactor.setEnabled(r_activoono);
		cmbCuantos.setEnabled(r_activoono);
		cmbEjercicios.setEnabled(r_activoono);
		desdeFecha.setEnabled(r_activoono);
		hastaFecha.setEnabled(r_activoono);
	}
}
