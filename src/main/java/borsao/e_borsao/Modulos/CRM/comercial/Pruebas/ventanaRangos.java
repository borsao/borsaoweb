package borsao.e_borsao.Modulos.CRM.comercial.Pruebas;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class ventanaRangos extends Window{
	
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private TextField campo1dsd=null;
	private TextField campo1hst=null;
	private TextField campo2dsd=null;
	private TextField campo2hst=null;
	private TextField campo3dsd=null;
	private TextField campo3hst=null;
	
	private HashMap<String, String> hashOpciones = null;
	
	public ventanaRangos(consultaVentas r_App, HashMap<String, String> r_hash) 
	{
		this.hashOpciones= r_hash;
		
        final GridLayout layout = new GridLayout(3,6);
        
        this.setCaption("Escoge los rangos necesarios");
        
        Label lblTitulo = new Label();        
        layout.addComponent(lblTitulo, 0, 0, 2, 0);
        
        Label lblRangosDsd = new Label();        
        lblRangosDsd.setCaption("Desde Codigo (por defecto 0)");
        layout.addComponent(lblRangosDsd, 1, 1, 1, 1);
        Label lblRangosHst = new Label();        
        lblRangosHst.setCaption("Hasta Codigo (por defecto 999999999)");
        layout.addComponent(lblRangosHst, 2, 1, 2, 1);

        
        Label campo1 = new Label();
        campo1.addStyleName("myTheme labelRango");
        Label campo2 = new Label();
        campo2.addStyleName("myTheme labelRango");
        Label campo3 = new Label();
        campo3.addStyleName("myTheme labelRango");
        campo1dsd = new TextField();
        campo1hst = new TextField();
        if (this.hashOpciones.get("cod_campo1").substring(0,4).toUpperCase().equals("COD_"))
        {
        	campo1.setCaption(this.hashOpciones.get("cod_campo1").substring(4).toUpperCase());
        }
        else
        {
        	campo1.setCaption(this.hashOpciones.get("cod_campo1").toUpperCase());
        }
        campo1dsd.setValue(this.hashOpciones.get("campo1dsd"));
        campo1hst.setValue(this.hashOpciones.get("campo1hst"));
        
        layout.addComponent(campo1, 0, 2, 0, 2);
        layout.addComponent(campo1dsd, 1, 2, 1, 2);
        layout.addComponent(campo1hst, 2, 2, 2, 2);
        layout.setComponentAlignment(campo1dsd, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(campo1hst, Alignment.MIDDLE_CENTER);
        
        if (this.hashOpciones.get("des_campo2")!=null)
        {
        	campo2dsd = new TextField();
        	campo2hst = new TextField();

            if (this.hashOpciones.get("cod_campo2").substring(0,4).toUpperCase().equals("COD_"))
            {
            	campo2.setCaption(this.hashOpciones.get("cod_campo2").substring(4).toUpperCase());
            }
            else
            {
            	campo2.setCaption(this.hashOpciones.get("cod_campo2").toUpperCase());
            }


            campo2dsd.setValue(this.hashOpciones.get("campo2dsd"));
            campo2hst.setValue(this.hashOpciones.get("campo2hst"));
            
            layout.addComponent(campo2, 0, 3, 0, 3);
            layout.addComponent(campo2dsd, 1, 3, 1, 3);
            layout.addComponent(campo2hst, 2, 3, 2, 3);
            layout.setComponentAlignment(campo2dsd, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(campo2hst, Alignment.MIDDLE_CENTER);
        }
        if (this.hashOpciones.get("des_campo3")!=null)
        {
        	campo3dsd = new TextField();
        	campo3hst = new TextField();
        	
            if (this.hashOpciones.get("cod_campo3").substring(0,4).toUpperCase().equals("COD_"))
            {
            	campo3.setCaption(this.hashOpciones.get("cod_campo3").substring(4).toUpperCase());
            }
            else
            {
            	campo3.setCaption(this.hashOpciones.get("cod_campo3").toUpperCase());
            }

            
        	
        	campo3dsd.setValue(this.hashOpciones.get("campo3dsd"));
            campo3hst.setValue(this.hashOpciones.get("campo3hst"));
            
            layout.addComponent(campo3, 0, 4, 0, 4);
            layout.addComponent(campo3dsd, 1, 4, 1, 4);
            layout.addComponent(campo3hst, 2, 4, 2, 4);
            layout.setComponentAlignment(campo3dsd, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(campo3hst, Alignment.MIDDLE_CENTER);
        }

        
		btnBotonCVentana = new Button("Cancelar");
		btnBotonAVentana = new Button("Aceptar");
    
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				boolean hayCambios = false;
				hashOpciones.put("campo1dsd", campo1dsd.getValue());
				hashOpciones.put("campo1hst", campo1hst.getValue());
				
				if (!campo1dsd.getValue().equals("0") || !campo1hst.getValue().equals("999999999") )
				{
					hayCambios=true;
				}
				
				if (hashOpciones.get("des_campo2")!=null)
		        {
					hashOpciones.put("campo2dsd", campo2dsd.getValue());
					hashOpciones.put("campo2hst", campo2hst.getValue());
					if (!campo2dsd.getValue().equals("0") || !campo2hst.getValue().equals("999999999") )
					{
						hayCambios=true;
					}
		        }
				if (hashOpciones.get("des_campo3")!=null)
		        {
					hashOpciones.put("campo3dsd", campo3dsd.getValue());
					hashOpciones.put("campo3hst", campo3hst.getValue());
					if (!campo3dsd.getValue().equals("0") || !campo3hst.getValue().equals("999999999") )
					{
						hayCambios=true;
					}
		        }

				r_App.establecerRangos(hashOpciones, hayCambios);
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
    
		layout.addComponent(btnBotonAVentana,1,5,1,5);
		layout.setComponentAlignment(btnBotonAVentana, Alignment.MIDDLE_CENTER);
		layout.addComponent(btnBotonCVentana,2,5,2,5);
		layout.setComponentAlignment(btnBotonCVentana, Alignment.MIDDLE_CENTER);
		layout.setSizeFull();
		center();
	    setModal(true);
	    setClosable(false);
	    setResizable(true);
	    setWidth("750px");
	    setHeight("450px");
		setContent(layout);

	}  
}