package borsao.e_borsao.Modulos.CRM.comercial.Consultas.view;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.TextFieldAyuda;

public class ventanaMargenes extends Window{
	
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private TextFieldAyuda  campo1dsd=null;
	private TextFieldAyuda  campo1hst=null;
	private TextFieldAyuda  campo2dsd=null;
	private TextFieldAyuda  campo2hst=null;
	private TextFieldAyuda  campo3dsd=null;
	private TextFieldAyuda  campo3hst=null;
	
	private HashMap<String, String> hashOpciones = null;
	
	public ventanaMargenes(OpcionesForm r_App, HashMap<String, String> r_hash) 
	{
		this.hashOpciones= r_hash;
		
        final GridLayout layout = new GridLayout(2,6);
        
        this.setCaption("Escoge los rangos necesarios");
        
        Label lblTitulo = new Label();        
        layout.addComponent(lblTitulo, 0, 0, 1, 0);
        
        Label lblRangosDsd = new Label();        
        lblRangosDsd.setCaption("Desde Codigo (por defecto 0)");
        layout.addComponent(lblRangosDsd, 0, 1, 0, 1);
        Label lblRangosHst = new Label();        
        lblRangosHst.setCaption("Hasta Codigo (por defecto 999999999)");
        layout.addComponent(lblRangosHst, 1, 1, 1, 1);

        
        String titulo= new String();
       
        if (this.hashOpciones.get("cod_campo1").substring(0,4).toUpperCase().equals("COD_"))
        {
        	titulo=this.hashOpciones.get("cod_campo1").substring(4).toUpperCase();
        }
        else
        {
        	titulo=this.hashOpciones.get("cod_campo1").toUpperCase();
        }
        if (titulo.equals("ARTICULO")) titulo="ARTICULO VENTAS";
        campo1dsd = new TextFieldAyuda(titulo,"Desde");
        campo1hst = new TextFieldAyuda(titulo,"Hasta");
        campo1dsd.txtTexto.setValue(this.hashOpciones.get("campo1dsd"));
        campo1hst.txtTexto.setValue(this.hashOpciones.get("campo1hst"));
        
        layout.addComponent(campo1dsd, 0, 2, 0, 2);
        layout.addComponent(campo1hst, 1, 2, 1, 2);
        layout.setComponentAlignment(campo1dsd, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(campo1hst, Alignment.MIDDLE_CENTER);
        
        if (this.hashOpciones.get("des_campo2")!=null)
        {
            if (this.hashOpciones.get("cod_campo2").substring(0,4).toUpperCase().equals("COD_"))
            {
            	titulo=this.hashOpciones.get("cod_campo2").substring(4).toUpperCase();
            }
            else
            {
            	titulo=this.hashOpciones.get("cod_campo2").toUpperCase();
            }
            if (titulo.equals("ARTICULO")) titulo="ARTICULO VENTAS";
            campo2dsd = new TextFieldAyuda(titulo,"desde");
            campo2hst = new TextFieldAyuda(titulo,"hasta");
            
            campo2dsd.txtTexto.setValue(this.hashOpciones.get("campo2dsd"));
            campo2hst.txtTexto.setValue(this.hashOpciones.get("campo2hst"));
            
            layout.addComponent(campo2dsd, 0, 3, 0, 3);
            layout.addComponent(campo2hst, 1, 3, 1, 3);
            layout.setComponentAlignment(campo2dsd, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(campo2hst, Alignment.MIDDLE_CENTER);
        }
        if (this.hashOpciones.get("des_campo3")!=null)
        {
        	
            if (this.hashOpciones.get("cod_campo3").substring(0,4).toUpperCase().equals("COD_"))
            {
            	titulo=this.hashOpciones.get("cod_campo3").substring(4).toUpperCase();
            }
            else
            {
            	titulo=this.hashOpciones.get("cod_campo3").toUpperCase();
            }
            if (titulo.equals("ARTICULO")) titulo="ARTICULO VENTAS";
            campo3dsd = new TextFieldAyuda(titulo,"desde");
            campo3hst = new TextFieldAyuda(titulo,"hasta");
            
        	campo3dsd.txtTexto.setValue(this.hashOpciones.get("campo3dsd"));
            campo3hst.txtTexto.setValue(this.hashOpciones.get("campo3hst"));
            
            layout.addComponent(campo3dsd, 0, 4, 0, 4);
            layout.addComponent(campo3hst, 1, 4, 1, 4);
            layout.setComponentAlignment(campo3dsd, Alignment.MIDDLE_CENTER);
            layout.setComponentAlignment(campo3hst, Alignment.MIDDLE_CENTER);
        }

        
		btnBotonCVentana = new Button("Cancelar");
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
    
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				boolean hayCambios = false;
				hashOpciones.put("campo1dsd", campo1dsd.txtTexto.getValue());
				hashOpciones.put("campo1hst", campo1hst.txtTexto.getValue());
				
				if (!campo1dsd.txtTexto.getValue().equals("0") || !campo1hst.txtTexto.getValue().equals("999999999") )
				{
					hayCambios=true;
				}
				
				if (hashOpciones.get("des_campo2")!=null)
		        {
					hashOpciones.put("campo2dsd", campo2dsd.txtTexto.getValue());
					hashOpciones.put("campo2hst", campo2hst.txtTexto.getValue());
					if (!campo2dsd.txtTexto.getValue().equals("0") || !campo2hst.txtTexto.getValue().equals("999999999") )
					{
						hayCambios=true;
					}
		        }
				if (hashOpciones.get("des_campo3")!=null)
		        {
					hashOpciones.put("campo3dsd", campo3dsd.txtTexto.getValue());
					hashOpciones.put("campo3hst", campo3hst.txtTexto.getValue());
					if (!campo3dsd.txtTexto.getValue().equals("0") || !campo3hst.txtTexto.getValue().equals("999999999") )
					{
						hayCambios=true;
					}
		        }

				r_App.establecerRangos(hashOpciones, hayCambios);
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
    
		layout.addComponent(btnBotonAVentana,0,5,0,5);
		layout.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		layout.addComponent(btnBotonCVentana,1,5,1,5);
		layout.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		layout.setMargin(true);
		layout.setSizeFull();
		center();
	    setModal(true);
	    setClosable(false);	    
	    setResizable(true);
	    setWidth("750px");
	    setHeight("450px");
		setContent(layout);

	}  
}