package borsao.e_borsao.Modulos.CRM.comercial.Consultas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.MapeoVentas;

public class consultaVentasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaVentasServer instance;
	
	public consultaVentasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public ArrayList<String> cargarEjercicios()
	{
		ArrayList<String> ejer = null;
		RutinasEjercicios re = new RutinasEjercicios();
		
		ejer = re.cargarEjercicios();		

		return ejer;
	}
	
	
	public ResultSet datosVentasGlobal(HashMap<String, String> r_opciones, String n_campo1, String n_campo2, String n_campo3)
	{
		ResultSet rsVentas = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		
		
		StringBuffer sql = new StringBuffer();
		
		Integer r_ejercicio= new Integer(r_opciones.get("Ejercicio"));
		int comparativos = (new Integer(r_opciones.get("Comparativos"))).intValue();
		String desdeFecha = r_opciones.get("Desde");
		String hastaFecha = r_opciones.get("HastaFecha");
		
		
		sql.append("select ");
		if (n_campo1!=null && n_campo1!="")
		{
			if (r_opciones.get("des_campo1")!=null)
			{
				sql.append(r_opciones.get("des_campo1") + " as campo1, ");
				sql.append(r_opciones.get("cod_campo1") + " as cod_campo1, ");
			}
			else
			{
				sql.append(n_campo1 + " as campo1, ");
			}
		}
		if (n_campo2!=null && n_campo2!="")
		{
			if (r_opciones.get("des_campo2")!=null)
			{
				sql.append(r_opciones.get("des_campo2") + " as campo2, ");
				sql.append(r_opciones.get("cod_campo2") + " as cod_campo2, ");
			}
			else
			{
				sql.append(n_campo2 + " as campo2, ");
			}
		}
		if (n_campo3!=null && n_campo3!="")
		{
			if (r_opciones.get("des_campo3")!=null)
			{
				sql.append(r_opciones.get("des_campo3") + " as campo3, ");
				sql.append(r_opciones.get("cod_campo3") + " as cod_campo3, ");
			}
			else
			{
				sql.append(n_campo3 + " as campo3, ");
			}
		}
		if (n_campo1!=null && n_campo1!="" && r_opciones.get("anada")!=null && r_opciones.get("anada").equals("Si"))
		{
			sql.append(" anada as anada, " );
		}	
		String campo="ud";
		if (r_opciones.get("factor").equals("Si"))
		{		
			campo="uf";
		}
		else if (r_opciones.get("factor").equals("Cajas"))
		{
			campo="cj";		
		}
		
		for (int i = 0; i<=comparativos ;i++)
		{
			int digito = r_ejercicio - i ;
			sql.append(" sum(" + campo + digito +") as ud" + digito + ", sum(imp" + digito +") as imp" + digito + ", sum(imp" + digito +")/sum(" + campo + digito +") as pvm" + digito);
			if (i != comparativos) sql.append(", " );
		}
		
		sql.append(" from " + eBorsao.get().accessControl.getVista() );
		
		Notificaciones.getInstance().mensajeSeguimiento("JAVI " + eBorsao.get().accessControl.getVista() );

		try
		{
			String condicionWhere = "";
			for (int i=0;i<=comparativos;i++)
			{
				fechaInicio=String.valueOf(r_ejercicio-i) + "-" + desdeFecha.substring(3, 5) + "-" + desdeFecha.substring(0, 2) ;
				fechaFin=String.valueOf(r_ejercicio-i) + "-" + hastaFecha.substring(3, 5) + "-" + hastaFecha.substring(0, 2) ;

				if (i==0)
				{
					condicionWhere = " where ((fechaFactura between '" + fechaInicio + "' and '" + fechaFin + "') ";
				}
				else
				{
					condicionWhere = condicionWhere + " or (fechaFactura between '" + fechaInicio + "' and '" + fechaFin + "') ";
				}
			}
			condicionWhere = condicionWhere + ") ";
			
			if (r_opciones.get("cod_campo3")!=null)
			{
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo1") + " between '" + r_opciones.get("campo1dsd") + "'  and '" + r_opciones.get("campo1hst") + "' ";
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo2") + " between '" + r_opciones.get("campo2dsd") + "'  and '" + r_opciones.get("campo2hst") + "' ";
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo3") + " between '" + r_opciones.get("campo3dsd") + "'  and '" + r_opciones.get("campo3hst") + "' ";				
			}
			else if (r_opciones.get("cod_campo2")!=null)
			{
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo1") + " between '" + r_opciones.get("campo1dsd") + "'  and '" + r_opciones.get("campo1hst") + "' ";
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo2") + " between '" + r_opciones.get("campo2dsd") + "'  and '" + r_opciones.get("campo2hst") + "' ";				
			}
			else if (r_opciones.get("cod_campo1")!=null)
			{
				condicionWhere = condicionWhere + " and " + r_opciones.get("cod_campo1") + " between '" + r_opciones.get("campo1dsd") + "'  and '" + r_opciones.get("campo1hst") + "' ";
			}
			
			String condicionGroupBy = null;
			String condicionOrderBy = null;	
			if (n_campo3!=null && n_campo3!="")
			{
				if (r_opciones.get("anada").equals("Si"))
				{
					condicionGroupBy= " group by 1,2,3,4,5,6,7 ";
					condicionOrderBy= " order by 9 asc ";
				}
				else
				{
					condicionGroupBy= " group by 1,2,3,4,5,6 ";
					condicionOrderBy= " order by 8 asc ";
				}
			}
			else if (n_campo2!=null && n_campo2!="")
			{
				if (r_opciones.get("anada").equals("Si"))
				{
					condicionGroupBy= " group by 1,2,3,4,5 ";
					condicionOrderBy= " order by 7 asc ";
				}
				else
				{
					condicionGroupBy= " group by 1,2,3,4 ";
					condicionOrderBy= " order by 6 asc ";
				}
			}
			else if (n_campo1!=null && n_campo1!="")
			{				
				
				if (r_opciones.get("anada").equals("Si"))
				{
					condicionGroupBy= " group by 1,2,3 ";
					condicionOrderBy= " order by 5 asc ";
				}
				else
				{
					condicionGroupBy= " group by 1,2 ";
					condicionOrderBy= " order by 4 asc ";
				}
			}
			
			sql.append(condicionWhere);
			if (condicionGroupBy!=null)
			{
				sql.append(condicionGroupBy);				
			}
			if (condicionOrderBy!=null)
			{
				sql.append(condicionOrderBy);				
			}
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				return rsVentas;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	@Override
	public String semaforos() {
		ResultSet rsVentas = null;
		
		Integer ejercicio =new Integer(RutinasFechas.añoActualYYYY());
		
		Double ventasActuales = null;
		Double ventasAnteriores = null;
		
		StringBuffer sql = new StringBuffer();
		
		try
		{
			sql.append(" SELECT sum(imp" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_0 ");
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "'");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasActuales=rsVentas.getDouble("vta_vta");
			}
				
			sql = new StringBuffer();
			
			ejercicio = ejercicio -1;
			
			sql.append(" SELECT sum(imp" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_" + ejercicio.toString());
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 365)) + "'");

			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasAnteriores=rsVentas.getDouble("vta_vta");
			}

			Double resultado = (ventasActuales/ventasAnteriores-1)*100;
			
			if (resultado>5)
			{
				return "0";
			}
			else if (resultado<-5)
			{
				return "2";
			}
			else
			{
				return "1";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return "0";
	}
	public String semaforosPVM() {
		ResultSet rsVentas = null;
		
		Integer ejercicio =new Integer(RutinasFechas.añoActualYYYY());
		
		Double ventasActuales = null;
		Double ventasAnteriores = null;
		
		StringBuffer sql = new StringBuffer();
		
		try
		{
			sql.append(" SELECT sum(imp" + ejercicio.toString() + ") / sum(uf" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_0 ");
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "'");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasActuales=rsVentas.getDouble("vta_vta");
			}
				
			sql = new StringBuffer();
			
			ejercicio = ejercicio -1;
			
			sql.append(" SELECT sum(imp" + ejercicio.toString() + ") / sum(uf" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_" + ejercicio.toString());
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 365)) + "'");

			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasAnteriores=rsVentas.getDouble("vta_vta");
			}

			Double resultado = (ventasActuales/ventasAnteriores-1)*100;
			
			if (resultado>5)
			{
				return "0";
			}
			else if (resultado<-5)
			{
				return "2";
			}
			else
			{
				return "1";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return "0";
	}

	public String semaforosUd() {
		ResultSet rsVentas = null;
		
		Integer ejercicio =new Integer(RutinasFechas.añoActualYYYY());
		
		Double ventasActuales = null;
		Double ventasAnteriores = null;
		
		StringBuffer sql = new StringBuffer();
		
		try
		{
			sql.append(" SELECT sum(uf" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_0 ");
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()) + "'");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasActuales=rsVentas.getDouble("vta_vta");
			}
				
			sql = new StringBuffer();
			
			ejercicio = ejercicio -1;
			
			sql.append(" SELECT sum(uf" + ejercicio.toString() + ") vta_vta");
			sql.append(" from vi_ventas_general_" + ejercicio.toString());
			sql.append(" where fechaFactura <= '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 365)) + "'");

			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				ventasAnteriores=rsVentas.getDouble("vta_vta");
			}

			Double resultado = (ventasActuales/ventasAnteriores-1)*100;
			
			if (resultado>5)
			{
				return "0";
			}
			else if (resultado<-5)
			{
				return "2";
			}
			else
			{
				return "1";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return "0";
	}


	public void generarPdf(MapeoVentas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuilder insertCampos= null;
		
		try{
			
			insertCampos = new StringBuilder();
			insertCampos.append("insert into crm_ventas( idCodigo, nivel, orden, ejercicio, codigo, descripcion, unidades, importe, medio, desdeFecha, hastaFecha, nivel1, nivel2, nivel3 ");			
			insertCampos.append(") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexion();	
			preparedStatement = con.prepareStatement(insertCampos.toString()); 
			
			if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(1, 0);
			}

			if (r_mapeo.getNivel()!=null)
			{
				preparedStatement.setInt(2, r_mapeo.getNivel());
			}
			else
			{
				preparedStatement.setInt(2, 0);
			}
			preparedStatement.setInt(3, this.obtenerSiguienteOrdenId(r_mapeo.getIdCodigo()));
			
			if (r_mapeo.getEjercicio()!=null)
			{
				preparedStatement.setInt(4, r_mapeo.getEjercicio());
			}
			else
			{
				preparedStatement.setInt(4, 0);
			}
			if (r_mapeo.getCodigo()!=null)
			{
				preparedStatement.setString(5, r_mapeo.getCodigo());
			}
			else
			{
				preparedStatement.setString(5, null);
			}		
			if (r_mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(6, r_mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(6, null);
			}
			
			if (r_mapeo.getUnidades()!=null)
			{
				preparedStatement.setDouble(7, r_mapeo.getUnidades());
			}
			else
			{
				preparedStatement.setDouble(7, 0);
			}
			if (r_mapeo.getImporte()!=null)
			{
				preparedStatement.setDouble(8, r_mapeo.getImporte());
			}
			else
			{
				preparedStatement.setDouble(8, 0);
			}
			if (r_mapeo.getMedio()!=null)
			{
				preparedStatement.setDouble(9, r_mapeo.getMedio());
			}
			else
			{
				preparedStatement.setDouble(9, 0);
			}
			preparedStatement.setString(10, r_mapeo.getDesdeFecha());
			preparedStatement.setString(11, r_mapeo.getHastaFecha());
			preparedStatement.setString(12, r_mapeo.getNivel1());
			preparedStatement.setString(13, r_mapeo.getNivel2());
			preparedStatement.setString(14, r_mapeo.getNivel3());
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	//sum(imp" + digito +")/sum(" + campo + digito +")
	
	public void eliminarGeneracion(Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM crm_ventas ");            
			cadenaSQL.append(" WHERE crm_ventas.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}

	}

	public Integer obtenerSiguienteId()
	{
		Integer idActual = null;
		ResultSet rsVentas = null;
		StringBuffer sql = new StringBuffer();
		
		try
		{
			idActual = 0;
			
			sql.append(" SELECT max(idCodigo) vta_id ");
			sql.append(" from crm_ventas ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			while(rsVentas.next())
			{
				idActual=rsVentas.getInt("vta_id");
			}
				
			idActual = idActual + 1;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return idActual;
	}
	
	private Integer obtenerSiguienteOrdenId(Integer r_id)
	{
		Integer idOrden = null;
		ResultSet rsVentas = null;
		StringBuffer sql = new StringBuffer();
		
		try
		{
			idOrden = 0;
			
			sql.append(" SELECT max(orden) vta_ord ");
			sql.append(" from crm_ventas ");
			sql.append(" where crm_ventas.idCodigo = " + r_id);
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVentas = cs.executeQuery(sql.toString());
			while(rsVentas.next())
			{
				idOrden =rsVentas.getInt("vta_ord");
			}
				
			idOrden = idOrden + 1;
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return idOrden;
	}
	
	public String imrpimirPdf(Integer r_id)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(r_id);
    	libImpresion.setArchivoDefinitivo("/estadisticaVentas" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("ventasCRM.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle("stockVinosDetalle.jrxml");
//    	libImpresion.setArchivoPlantillaDetalleCompilado("stockVinosDetalle.jasper");
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado(null);
    	libImpresion.setCarpeta("crm");
    	libImpresion.setBackGround("/fondoA4LogoBlancoH.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	return resultadoGeneracion;
    	
	}
}
