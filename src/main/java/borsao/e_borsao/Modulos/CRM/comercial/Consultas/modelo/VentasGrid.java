package borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo;

import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.treegrid.TreeGrid;

import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.HtmlRenderer;

import borsao.e_borsao.ClasesPropias.EuroConverter;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class VentasGrid extends TreeGrid {
	
  	 public HashMap<String , String> opcionesEscogidas = null;

    public VentasGrid(HashMap<String , String> r_opcionesEscogidas, boolean r_hayGrid) {
        setSizeFull();
        setSelectionMode(SelectionMode.SINGLE);

		/*
		 * codigo para cargar el jtree-grid
		 */
        
		setCaption("<h2><b>Consulta Ventas</b></h2>");
		setCaptionAsHtml(true);

		this.generarGrid(r_opcionesEscogidas,r_hayGrid);

        setEditorEnabled(false);
        setFrozenColumnCount(1);
        getColumn(consultaVentasContainer.getColumnaPrincipal()).setEditable(false);
        getColumn(consultaVentasContainer.getColumnaPrincipal()).setHidden(false);        
        setHierarchyColumn(consultaVentasContainer.getColumnaPrincipal());
        setColumnOrder(consultaVentasContainer.getColumnaPrincipal());
        setColumnResizeMode(ColumnResizeMode.ANIMATED);
        setColumnReorderingAllowed(false);
		setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	
                if ("Descripcion".equals(cellReference.getPropertyId())) {
                    // when the current cell is number such as age, align text to right
                    return "leftAligned";
                } else {
                    // otherwise, align text to left
                	return "rightAligned";
                }
            }
        });

    }

    public void generarGrid(HashMap<String , String> r_opcionesEscogidas, boolean r_hayGrid )
    {


    	consultaVentasContainer container = new consultaVentasContainer(r_opcionesEscogidas,r_hayGrid);
        
        setContainerDataSource(container);
                
        Iterator<Column> it = getColumns().iterator();
        
        while (it.hasNext()) 
        {
        	Column col = (Column)it.next();
        	
			if (!(col.getPropertyId().equals(container.getColumnaPrincipal())) && !col.getPropertyId().toString().contains("Diff"))
        	{
        		col.setConverter(new EuroConverter());        		
        	}
			else if (col.getPropertyId().toString().contains("Diff"))
			{
				col.setRenderer(new HtmlRenderer());
			}
        	col.setWidthUndefined();
        }

    }
}


