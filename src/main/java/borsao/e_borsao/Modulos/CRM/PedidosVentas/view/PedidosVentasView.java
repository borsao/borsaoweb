package borsao.e_borsao.Modulos.CRM.PedidosVentas.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.modelo.MapeoEtiquetasPrepedidos;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPedidosVentas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PedidosVentasView extends GridViewRefresh {

	public static final String VIEW_NAME = "Pedidos Ventas";
	public boolean conTotales = true;
	public boolean soyAlmacen = false;
	
	private ComboBox cmbPrinter = null;	
	private final String titulo = "Pedidos de Ventas";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private CheckBox pendientes = null;
	private Button sincronizarSGA = null;
	private Button imprimirSGA = null;
	protected ComboBox cmbEjercicios;

//	private MapeoPedidosVentas mapeoPedidosVentas =null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoPedidosVentas> r_vector=null;
//    	hashToMapeo hm = new hashToMapeo();
    	
//    	MapeoPedidosVentas=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaPedidosVentasServer cus = new consultaPedidosVentasServer(CurrentUser.get());
    	r_vector=cus.datosOpcionesGlobal(null,this.pendientes.getValue(), this.cmbEjercicios.getValue().toString());
    
    	grid = new OpcionesGrid(this, r_vector);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PedidosVentasView() 
    {
    	setConFormulario(this.conFormulario);
    	
    	if (eBorsao.get().accessControl.getDepartamento().toUpperCase().contentEquals("ALMACEN") || eBorsao.get().accessControl.getNombre().contains("Claver"))
    	{
    		soyAlmacen=true;
    	}
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.sincronizarSGA = new Button("-> SGA");
    	this.sincronizarSGA.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.sincronizarSGA.addStyleName(ValoTheme.BUTTON_TINY);
    	this.sincronizarSGA.setVisible(this.soyAlmacen);
    	
    	this.imprimirSGA = new Button("-> Imprimir SGA");
    	this.imprimirSGA.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.imprimirSGA.addStyleName(ValoTheme.BUTTON_TINY);
    	this.imprimirSGA.setVisible(this.soyAlmacen);
    	
		this.cmbPrinter=new ComboBox("Impresora");
		this.cmbPrinter.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbPrinter.setInvalidAllowed(false);
		this.cmbPrinter.setNewItemsAllowed(false);
		this.cmbPrinter.setNullSelectionAllowed(false);
		this.cmbPrinter.setRequired(true);				
		this.cmbPrinter.setWidth("200px");
		this.cmbPrinter.setVisible(this.soyAlmacen);
		this.cargarCombo();

    	this.pendientes=new CheckBox("Ver sólo pendientes de cumplimentar.");
		this.pendientes.setValue(true);
		
		this.cmbEjercicios=new ComboBox("Ejercicio");
		this.cmbEjercicios.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cargarComboEjercicio();
		this.cmbEjercicios.setInvalidAllowed(false);
		this.cmbEjercicios.setNewItemsAllowed(false);
		this.cmbEjercicios.setNullSelectionAllowed(false);
		this.cmbEjercicios.setWidth("200px");

		this.cabLayout.addComponent(this.cmbEjercicios);
		this.cabLayout.setComponentAlignment(this.cmbEjercicios, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.pendientes);
		this.cabLayout.setComponentAlignment(pendientes, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.sincronizarSGA);
		this.cabLayout.setComponentAlignment(sincronizarSGA, Alignment.BOTTOM_CENTER);
		this.cabLayout.addComponent(this.cmbPrinter);
		this.cabLayout.setComponentAlignment(this.cmbPrinter, Alignment.BOTTOM_CENTER);
		this.cabLayout.addComponent(this.imprimirSGA);
		this.cabLayout.setComponentAlignment(this.imprimirSGA, Alignment.BOTTOM_CENTER);

		this.cargarListeners();
		
    	this.generarGrid(null);
//        this.form = new OpcionesForm(this);
//        this.verForm(true);
        
//        addComponent(this.form);
    }

    private void cargarListeners()
    {
    	this.pendientes.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);

					generarGrid(null);
					
				}
				
			}
		});

    	this.cmbEjercicios.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);

					generarGrid(null);
					
				}
				
			}
		});

    	this.sincronizarSGA.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
				mostrarFilas(grid.getSelectedRows());
			}
		});

    	this.imprimirSGA.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			
    			imprimirFilas(grid.getSelectedRows());
    		}
    	});
    }
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) 
    {
    	
    	
		Iterator it = null;
		String situacionPedido = "Ok";
		
		if (!tieneDatos())
		{
			consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
			
			it = r_filas.iterator();
			
			while (it.hasNext())
			{			
				MapeoPedidosVentas mapeo = (MapeoPedidosVentas) it.next();
				
				if (cps.comprobarClienteSGA(mapeo.getEjercicio().toString(),mapeo.getDocumento().toString(), mapeo.getIdCodigoVenta().toString()))
				{
					if (cps.comprobarArticuloSGA(mapeo.getEjercicio().toString(),mapeo.getDocumento().toString(), mapeo.getIdCodigoVenta().toString()))
					{
						cps.actualizarPedido(mapeo, situacionPedido);
					}
					else
					{
						Notificaciones.getInstance().mensajeError("Articulo del pedido " + mapeo.getIdCodigoVenta().toString() + " no está en el SGA.");
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("El cliente" + mapeo.getCliente().toString() + " del pedido " + mapeo.getIdCodigoVenta().toString() + " no está en el SGA.");
				}
			}
			Notificaciones.getInstance().mensajeInformativo("Proceso sincronizacion terminado");
		}
		else
		{
			Notificaciones.getInstance().mensajeInformativo("La tabla tiene datos. Espera que se vacien. (5 minutos)");
		}
		
    }

    public void imprimirFilas(Collection<Object> r_filas) 
    {

		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres imprimir los pedidos?", "Imprimir", "Cancelar", "imprimir", "cancelarImpresion");
		getUI().addWindow(vt);
    }

	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicios.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicios.setValue(vector.get(0).toString());
	}

    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) 
	{
		switch (r_accion.toUpperCase()) 
		{
			case "IMPRIMIR":
			{
				procesarImpresion();
				grid.deselectAll();
				break;
			}
			default:
				break;
		}		
	}

	@Override
	public void cancelarProceso(String r_accion) 
	{
		switch (r_accion.toUpperCase()) 
		{
			case "CANCELARIMPRESION":
			{
				grid.deselectAll();
				break;
			}
			default:
				break;
		}
		
	}
	
	private void procesarImpresion()
	{
		Iterator it = null;
		
		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		it = grid.getSelectedRows().iterator();
		
		while (it.hasNext())
		{			
			MapeoPedidosVentas mapeo = (MapeoPedidosVentas) it.next();
			boolean rdo = cps.traerPedidoSGAMysql(mapeo);
			if (rdo)
				rdo = cps.traerPedidoGreensysMysql(mapeo);
			
			if (rdo)
			{
				String pdfGenerado = cps.generarInformeSGA(new Integer(mapeo.getIdCodigoVenta()), new Integer(mapeo.getIdCodigoVenta()));
				
				/*
				 * agregar combo impresoras para usuario almacen
				 */
				if(pdfGenerado.length()>0)
				{
					if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
					{
						RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
					}
					else
					{
						String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
						RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
					    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
					}
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("Problemas al capturar los datos del pedido");
			}
		}
		
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private boolean tieneDatos()
	{
		servidorSincronizadorBBDD ss = null;
		ss =new servidorSincronizadorBBDD();
		try
		{
			return ss.comprobarDestinoConDatos("Objective", "CABECERA_PEDIDOS_VTA");
		}
		catch (Exception ex)
		{
			return false;
		}
		finally
		{
			ss=null;
		}
	}
}
