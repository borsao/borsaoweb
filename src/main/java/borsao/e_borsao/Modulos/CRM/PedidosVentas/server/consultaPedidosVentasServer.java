package borsao.e_borsao.Modulos.CRM.PedidosVentas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosSGA;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPedidosSGA;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPedidosVentas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPrimerPlazo;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorSGA;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaPedidosVentasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPedidosVentasServer instance;
	private ArrayList<MapeoSituacionEmbotellado> vectorReservado = null;	
//	private String tablaCabecera = null;
//	private String tablaLineas = null;
//	private String tablaPies = null;
	
	public consultaPedidosVentasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPedidosVentasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPedidosVentasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoPedidosVentas> datosOpcionesGlobal(MapeoPedidosVentas r_mapeo, boolean r_ptes, String r_ejercicio)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoPedidosVentas> vector = null;
		MapeoPedidosVentas mapeo = null;
		RutinasEjercicios rej = new RutinasEjercicios();
		
		Integer digito = rej.cargarDigitoEjercicio(new Integer(r_ejercicio));
		String tabla = "a_vp_c_" + digito;
		String tablaPie = "a_vp_p_" + digito;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.clave_tabla vp_cl, ");
        cadenaSQL.append(" a.documento vp_dc, ");
        cadenaSQL.append(" a.serie vp_sr, ");
        cadenaSQL.append(" a.codigo vp_cd, ");	    
        cadenaSQL.append(" a.fecha_documento vp_fec, ");
        cadenaSQL.append(" a.cumplimentacion vp_cum, ");
        cadenaSQL.append(" a.ejercicio vp_ej, ");
        cadenaSQL.append(" a.cliente vp_cli, ");
        cadenaSQL.append(" a.nombre_comercial vp_nc, ");
        cadenaSQL.append(" a.ref_cliente vp_rf, ");
        cadenaSQL.append(" a.finalidad vp_es, ");        
        cadenaSQL.append(" a.fecha_list_ruta vp_fc, ");
        cadenaSQL.append(" a.plazo_entrega vp_pe, ");
        cadenaSQL.append(" a.almacen vp_al, ");
        cadenaSQL.append(" a.moneda vp_mon, ");
        cadenaSQL.append(" c.nombre vp_pa, ");
        cadenaSQL.append(" b.descripcion vp_fp, ");
        cadenaSQL.append(" p.importe vp_imp ");
        
        cadenaSQL.append(" FROM " + tabla + " a, e_frmpag b, e_paises c, " + tablaPie + " p ");
        cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla");
        cadenaSQL.append(" and a.documento = p.documento");
        cadenaSQL.append(" and a.serie = p.serie ");
        cadenaSQL.append(" and a.codigo = p.codigo ");
        cadenaSQL.append(" and p.porimp = 'T' and p.orden_porimp ='4' ");
     	cadenaSQL.append(" and c.pais = a.pais ");
     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
     	
     	if (r_mapeo!=null && r_mapeo.getCliente()!=null)
     	{
     		cadenaSQL.append(" and a.cliente_factura = '" + r_mapeo.getCliente().toString() + "'");
     	}
     	else
     	{
     		if (eBorsao.get().accessControl.isAccesoCliente()) cadenaSQL.append(" and a.cliente = '" + eBorsao.get().accessControl.getCodigoGestion().toString() + "'");
     	}
     	if (eBorsao.get().accessControl.getNombre().toUpperCase().contains("PALOMAR"))
     		cadenaSQL.append(" and a.pais in ('001','002') ");

     	if (r_ptes) cadenaSQL.append(" AND a.cumplimentacion Is Null ");
     	
     	cadenaSQL.append(" order by 12 desc,5,4");
     	
		try
		{
//			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestionInd();			
				this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoPedidosVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoPedidosVentas();
				
				mapeo.setIdCodigoVenta(rsOpcion.getInt("vp_cd"));
				mapeo.setClave_tabla(rsOpcion.getString("vp_cl"));
				mapeo.setDocumento(rsOpcion.getString("vp_dc"));
				mapeo.setSerie(rsOpcion.getString("vp_sr"));
				mapeo.setEjercicio(rsOpcion.getInt("vp_ej"));
				mapeo.setNombre(RutinasCadenas.conversion(rsOpcion.getString("vp_nc")));
				mapeo.setMoneda(rsOpcion.getString("vp_mon"));
				mapeo.setAlmacen(rsOpcion.getInt("vp_al"));
				mapeo.setCliente(rsOpcion.getInt("vp_cli"));
				mapeo.setPais(RutinasCadenas.conversion(rsOpcion.getString("vp_pa")));
				mapeo.setReferencia(rsOpcion.getString("vp_rf"));
				mapeo.setImporte(rsOpcion.getInt("vp_imp"));
				mapeo.setFecha(rsOpcion.getDate("vp_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getString("vp_pe"));
				mapeo.setFechaCliente(rsOpcion.getDate("vp_fc"));
				
				if (r_ptes)
					mapeo.setEstado(rsOpcion.getString("vp_es"));
				else
					if (rsOpcion.getString("vp_cum")!=null && rsOpcion.getString("vp_cum").length()>0)
						mapeo.setEstado("SRV");
					else
						mapeo.setEstado(rsOpcion.getString("vp_es"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}
	
	public ArrayList<MapeoLineasPedidosVentas> datosOpcionesGlobal(MapeoLineasPedidosVentas r_mapeo, Integer r_stock, String r_cumplimentados)
	{
		
		ResultSet rsOpcion = null;		
		Integer stockCalculado = null;
		ArrayList<MapeoLineasPedidosVentas> vector = null;
		MapeoLineasPedidosVentas mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		stockCalculado=r_stock;
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");
        cadenaSQL.append(" p.almacen det_alm, ");
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" a.ref_cliente det_ref, ");        
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.movimiento det_mov, ");
        cadenaSQL.append(" p.num_linea det_lin, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" a.finalidad vp_es, ");
        if (r_stock != null)
        {
        	cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
        	cadenaSQL.append(" b.descrip_articulo det_pal ");
        }
        else
        {
        	cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte ");
        }
        
     	cadenaSQL.append(" FROM a_vp_l_0 p, e_client c, a_vp_c_0 a ");
     	
     	if (r_stock != null) cadenaSQL.append(" , outer a_vp_l_0 b ");
     	
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");

     	if (r_stock != null)
     	{
	     	cadenaSQL.append(" AND b.clave_tabla = p.clave_tabla ");
	     	cadenaSQL.append(" AND b.documento = p.documento ");
	     	cadenaSQL.append(" AND b.serie = p.serie ");
	     	cadenaSQL.append(" AND b.codigo = p.codigo ");
	     	cadenaSQL.append(" AND b.articulo matches '01193*' ");
     	}
     	
     	if (r_cumplimentados.equals("S"))
     	{
     		cadenaSQL.append(" AND p.cumplimentacion Is not Null ");
     	}
     	else if (r_cumplimentados.equals("N"))
     	{
     		cadenaSQL.append(" AND p.cumplimentacion Is Null ");
     	}     	
     	
     	if (r_mapeo.getArticulo()!=null)
     	{
     		cadenaSQL.append(" AND p.articulo matches '*" + r_mapeo.getArticulo().trim() + "*' ");
     	}
     	if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen().length()>0)
     	{
     		cadenaSQL.append(" AND a.almacen= '" + r_mapeo.getAlmacen() + "' ");
     	}
     	if (r_mapeo.getPais()!=null)
     	{
     		cadenaSQL.append(" AND a.pais = '" + r_mapeo.getPais() + "' ");
     	}
     	if (r_mapeo.getCliente()!=null)
     	{
     		cadenaSQL.append(" and a.cliente_factura = '" + r_mapeo.getCliente().toString() + "'");
     	}
     	else
     	{
     		if (eBorsao.get().accessControl.isAccesoCliente()) cadenaSQL.append(" and a.cliente = '" + eBorsao.get().accessControl.getCodigoGestion().toString() + "'");
     	}

     	if (r_mapeo.getIdCodigoVenta()!=null)
     	{
     		cadenaSQL.append(" AND a.codigo = " + r_mapeo.getIdCodigoVenta() );
     	}
     	
     	cadenaSQL.append(" order by 3,2,9 ");
     	
		try
		{
			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestion();			
				this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosVentas>();
			
	        String docOld = null;
	        Integer codOld = null;
	        String artOld = null;
	        Integer linOld = null;
	        String doc = null;
	        Integer cod = null;
	        String art = null;
	        Integer lin = null;
	        String mov = null;

			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosVentas();
				doc = rsOpcion.getString("det_doc");
				cod = rsOpcion.getInt("det_cod");
				if (rsOpcion.getString("det_art")!=null) art = rsOpcion.getString("det_art"); else art="";
				lin = rsOpcion.getInt("det_lin");
				
				if (!doc.equals(docOld) || !cod.equals(codOld) || !art.equals(artOld) || !lin.equals(linOld))
				{
					mov = rsOpcion.getString("det_mov");
					
					mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
					mapeo.setAlmacen(rsOpcion.getString("det_alm"));
					mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
					mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
					
					if (mov!=null && mov.equals("RE"))
					{
						mapeo.setCliente(0);
						mapeo.setNombreCliente(rsOpcion.getString("det_des"));
					}
					else
					{
						mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
						mapeo.setCliente(rsOpcion.getInt("det_cli"));
					}	
					mapeo.setDocumento(rsOpcion.getString("det_doc"));
					mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
					mapeo.setArticulo(rsOpcion.getString("det_art"));
					mapeo.setDescripcion(rsOpcion.getString("det_des"));
					mapeo.setUnidades(rsOpcion.getInt("det_uds"));
					mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
					mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
					mapeo.setReferencia(rsOpcion.getString("det_ref"));
					mapeo.setFinalidad(rsOpcion.getString("vp_es"));
					
					
					if (r_stock!=null && mov != null && !mov.equals("RE"))
					{
						mapeo.setStock_queda(stockCalculado - rsOpcion.getInt("det_pte"));
						stockCalculado = stockCalculado - rsOpcion.getInt("det_pte"); 
					}
					mapeo.setStock_queda(stockCalculado);
				}					
				if (r_stock != null) mapeo.setDescripcionPalet(rsOpcion.getString("det_pal")); else mapeo.setDescripcionPalet("");
				vector.add(mapeo);
				docOld = rsOpcion.getString("det_doc");
				codOld = rsOpcion.getInt("det_cod");
				artOld = rsOpcion.getString("det_art");
				linOld = rsOpcion.getInt("det_lin");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}
	
	public ArrayList<MapeoLineasPedidosVentas> datosOpcionesGlobal(String r_articulo, String r_cumplimentados)
	{
		
		ResultSet rsOpcion = null;		
		Integer stockCalculado = null;
		ArrayList<MapeoLineasPedidosVentas> vector = null;
		MapeoLineasPedidosVentas mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.plazo_entrega det_plz, ");
		cadenaSQL.append(" p.almacen det_alm, ");
		cadenaSQL.append(" c.cliente det_cli, ");
		cadenaSQL.append(" c.nombre_comercial det_nom, ");
		cadenaSQL.append(" a.pais det_pai, ");
		cadenaSQL.append(" a.ref_cliente det_ref, ");        
		cadenaSQL.append(" p.documento det_doc, ");
		cadenaSQL.append(" p.codigo det_cod, ");
		cadenaSQL.append(" p.movimiento det_mov, ");
		cadenaSQL.append(" p.num_linea det_lin, ");
		cadenaSQL.append(" p.articulo det_art, ");
		cadenaSQL.append(" p.descrip_articulo det_des, ");
		cadenaSQL.append(" p.unidades det_uds, ");
		cadenaSQL.append(" p.unidades_serv det_ser, ");
		cadenaSQL.append(" a.finalidad vp_es, ");
		cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte ");
		cadenaSQL.append(" FROM a_vp_l_0 p, e_client c, a_vp_c_0 a ");
		cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
		cadenaSQL.append(" AND a.documento = p.documento ");
		cadenaSQL.append(" AND a.serie = p.serie ");
		cadenaSQL.append(" AND a.codigo = p.codigo ");
		cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
		
		if (r_cumplimentados.equals("S"))
		{
			cadenaSQL.append(" AND p.cumplimentacion Is not Null ");
		}
		else if (r_cumplimentados.equals("N"))
		{
			cadenaSQL.append(" AND p.cumplimentacion Is Null ");
		}     	
		
		if (r_articulo!=null)
		{
			cadenaSQL.append(" and p.articulo in (select padre from e__lconj where hijo = '" + r_articulo + "') ");
		}
		if (eBorsao.get().accessControl.isAccesoCliente()) cadenaSQL.append(" and a.cliente = '" + eBorsao.get().accessControl.getCodigoGestion().toString() + "'");
		cadenaSQL.append(" order by 3,2,9 ");
		
		try
		{
			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestion();			
				this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosVentas>();
			
			String docOld = null;
			Integer codOld = null;
			String artOld = null;
			Integer linOld = null;
			String doc = null;
			Integer cod = null;
			String art = null;
			Integer lin = null;
			String mov = null;
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosVentas();
				doc = rsOpcion.getString("det_doc");
				cod = rsOpcion.getInt("det_cod");
				if (rsOpcion.getString("det_art")!=null) art = rsOpcion.getString("det_art"); else art="";
				lin = rsOpcion.getInt("det_lin");
				
				if (!doc.equals(docOld) || !cod.equals(codOld) || !art.equals(artOld) || !lin.equals(linOld))
				{
					mov = rsOpcion.getString("det_mov");
					
					mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
					mapeo.setAlmacen(rsOpcion.getString("det_alm"));
					mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
					mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
					
					if (mov!=null && mov.equals("RE"))
					{
						mapeo.setCliente(0);
						mapeo.setNombreCliente(rsOpcion.getString("det_des"));
					}
					else
					{
						mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
						mapeo.setCliente(rsOpcion.getInt("det_cli"));
					}	
					mapeo.setDocumento(rsOpcion.getString("det_doc"));
					mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
					mapeo.setArticulo(rsOpcion.getString("det_art"));
					mapeo.setDescripcion(rsOpcion.getString("det_des"));
					mapeo.setUnidades(rsOpcion.getInt("det_uds"));
					mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
					mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
					mapeo.setReferencia(rsOpcion.getString("det_ref"));
					mapeo.setFinalidad(rsOpcion.getString("vp_es"));
				}					
				vector.add(mapeo);
				docOld = rsOpcion.getString("det_doc");
				codOld = rsOpcion.getInt("det_cod");
				artOld = rsOpcion.getString("det_art");
				linOld = rsOpcion.getInt("det_lin");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}
	
	public ArrayList<MapeoLineasPedidosVentas> pedidosSinPallet()
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasPedidosVentas> vector = null;
		MapeoLineasPedidosVentas mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.ejercicio det_eje, ");
		cadenaSQL.append(" a.codigo det_cod, ");
		cadenaSQL.append(" a.fecha_documento det_fec, ");
        cadenaSQL.append(" a.plazo_entrega det_plz, ");
        cadenaSQL.append(" a.finalidad det_es, ");
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom ");
     	cadenaSQL.append(" FROM a_vp_c_0 a, e_client c ");
     	cadenaSQL.append(" WHERE a.cliente = c.cliente ");
     	cadenaSQL.append(" AND a.codigo not in (select codigo from a_vp_l_0 where articulo matches '01193*') ");
     	cadenaSQL.append(" AND a.cumplimentacion is null ");
     	cadenaSQL.append(" AND a.plazo_entrega < '31/12/2999' ");
     	
     	cadenaSQL.append(" order by 2 ");
     	
		try
		{
			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestion();			
				this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
				mapeo.setCliente(rsOpcion.getInt("det_cli"));
				mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
				mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
				mapeo.setFinalidad(rsOpcion.getString("det_es"));

				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}
	
	public MapeoPrimerPlazo traerPrimerPlazoEntrega(Connection r_con, String r_articulo, Integer r_stock_actual, String r_almacen)
	{
		
		ResultSet rsOpcion = null;		
		Date plazoCalculado = null;
		String nombreCliente = null;
		String fechaObtenida = null;
		Integer diasCarga = null;
		MapeoPrimerPlazo mapeoPlazo = null;
		
		Integer stockCalculado = r_stock_actual;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT p.plazo_entrega det_plz, unidades-unidades_serv det_can, c.nombre_comercial det_nom ");
     	cadenaSQL.append(" FROM a_vp_l_0 p, a_vp_c_0 c ");
     	cadenaSQL.append(" WHERE c.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND c.documento = p.documento ");
     	cadenaSQL.append(" AND c.serie = p.serie ");
     	cadenaSQL.append(" AND c.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null ");
     	if (r_almacen.equals("1")) cadenaSQL.append(" and (c.almacen = '1') ");
     	if (r_almacen.equals("4")) cadenaSQL.append(" and (c.almacen = '4') ");
     	if (r_almacen.equals("")) cadenaSQL.append(" and (c.almacen in  ('1','4')) ");

     	cadenaSQL.append(" AND p.articulo = '" + r_articulo + "' ");
     	
     	cadenaSQL.append(" order by 1 "); 
     	
		try
		{
			this.cs = r_con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (stockCalculado - rsOpcion.getInt("det_can") < 0 )
				{
					mapeoPlazo = new MapeoPrimerPlazo();
					
					nombreCliente = rsOpcion.getString("det_nom");
					plazoCalculado = RutinasFechas.conversionDeString(RutinasFechas.convertirDateToString(rsOpcion.getDate("det_plz")));
					
//					if (nombreCliente.toUpperCase().contains("SABECO") || nombreCliente.toUpperCase().contains("DAGESA") )
//					{
//						diasCarga = 0;					
//					}
//					else if (nombreCliente.toUpperCase().contains("MERCADONA ANTEQUERA") || nombreCliente.toUpperCase().contains("MERCADONA HUEVAR") )
//					{
//						diasCarga = 2;					
//					}
//					else
//					{
//						diasCarga = 1;
//					}
//					
//					fechaObtenida = RutinasFechas.restarDiasFecha(RutinasFechas.convertirDateToString(rsOpcion.getDate("det_plz")), diasCarga);
//					plazoCalculado = RutinasFechas.conversionDeString(fechaObtenida);
//					if (RutinasFechas.obtenerDiaDeLaSemana(plazoCalculado)==1)
//					{
//						diasCarga = 2;
//					}
//					else if (RutinasFechas.obtenerDiaDeLaSemana(plazoCalculado)==7)
//					{
//						diasCarga=1;
//					}
//					else
//					{
//						diasCarga=0;
//					}
//					
//					fechaObtenida = RutinasFechas.restarDiasFecha(RutinasFechas.convertirDateToString(plazoCalculado), diasCarga);
//					plazoCalculado = RutinasFechas.conversionDeString(fechaObtenida);
					
					
					mapeoPlazo.setPrimerPlazo(plazoCalculado);
					mapeoPlazo.setNombreCliente(nombreCliente);
					
					return mapeoPlazo;
				}
				else
				{
					stockCalculado = stockCalculado - rsOpcion.getInt("det_can");
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return null;
	}
	
	public Integer traerCantidadReservado(Connection r_con, String r_articulo)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer reservado =null;
		
		cadenaSQL.append(" SELECT sum(unidades-unidades_serv) det_res ");
     	cadenaSQL.append(" FROM a_vp_l_0 p, a_vp_c_0 c ");
     	cadenaSQL.append(" WHERE c.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND c.documento = p.documento ");
     	cadenaSQL.append(" AND c.serie = p.serie ");
     	cadenaSQL.append(" AND c.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null "); 
     	cadenaSQL.append(" AND p.articulo = '" + r_articulo + "' ");
     	cadenaSQL.append(" AND p.plazo_entrega = '31/12/2999'" );
     	
     	cadenaSQL.append(" order by 1 "); 
     	
		try
		{
			
			this.cs = r_con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				reservado = rsOpcion.getInt("det_res");
				break;
			}				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return reservado;
	}

	public boolean actualizarCabeceraGreensys(MapeoPedidosVentas r_mapeo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		boolean resultado = false;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_mapeo.getEjercicio());
		}
		
		if (r_mapeo.getPlazoEntrega()==null || r_mapeo.getPlazoEntrega().length()==0)
			return false;
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_c_" + ejer + " set ");
    		cadenaSQL.append(" finalidad = ?, ");
    		cadenaSQL.append(" plazo_entrega = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getEstado());
		    preparedStatement.setString(2, r_mapeo.getPlazoEntrega());
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    preparedStatement.setInt(6, r_mapeo.getIdCodigoVenta());

	        preparedStatement.executeUpdate();
	        
	        resultado = actualizarLineasGreensys(r_mapeo);
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	resultado = false;
	    }
		return resultado;
	}
	
	public boolean actualizarCabeceraGreensys(MapeoPedidosVentas r_mapeo, boolean r_preparado)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		boolean resultado = false;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_mapeo.getEjercicio());
		}
		
		if (r_mapeo.getPlazoEntrega()==null || r_mapeo.getPlazoEntrega().length()==0)
			return false;
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_c_" + ejer + " set ");
    		cadenaSQL.append(" finalidad = ?, ");
    		if (r_preparado) cadenaSQL.append(" tty[7] = 'P', ");
    		cadenaSQL.append(" plazo_entrega = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getEstado());
		    preparedStatement.setString(2, r_mapeo.getPlazoEntrega());
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    preparedStatement.setInt(6, r_mapeo.getIdCodigoVenta());

	        preparedStatement.executeUpdate();
	        resultado=true;
//	        resultado = actualizarLineasGreensys(r_mapeo);
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	resultado = false;
	    }
		return resultado;
	}
	
	public boolean actualizarPedidoProgramado(Integer r_idProg, String r_sql)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		boolean resultado = false;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_c_" + ejer + " set ");
    		cadenaSQL.append(" orden_fabric = " + r_idProg);
	        cadenaSQL.append(" where documento = 'P1' ");
	        cadenaSQL.append(" and codigo in ("+ r_sql + ") ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	resultado = false;
	    }
		return resultado;
	}

	public boolean liberarPedidoProgramado(Integer r_idProg)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		boolean resultado = false;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_c_" + ejer + " set ");
    		cadenaSQL.append(" orden_fabric = null ");
	        cadenaSQL.append(" where documento = 'P1' ");
	        cadenaSQL.append(" and orden_fabric = " + r_idProg );
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	resultado = false;
	    }
		return resultado;
	}

	private boolean actualizarLineasGreensys(MapeoPedidosVentas r_mapeo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_l_0 set ");
    		cadenaSQL.append(" plazo_entrega = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getPlazoEntrega());
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigoVenta());

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		return true;
	}
	
	public String actualizarLineaPedidoExterno(int r_digito, Integer r_codigo, Integer r_linea)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_vp_l_" + r_digito + " set ");
    		cadenaSQL.append(" observacion = 'OK' ");
	        cadenaSQL.append(" where clave_tabla = 'P' ");
	        cadenaSQL.append(" and documento = 'P1' ");
	        cadenaSQL.append(" and serie = 'P' ");
	        cadenaSQL.append(" and codigo = ? ");
	        cadenaSQL.append(" and num_linea = ? ");
		    
		    con= this.conManager.establecerConexionGestion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_codigo);
		    preparedStatement.setInt(2, r_linea);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}
	
	@Override
	public String semaforos() 
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsVentas = null;
		
		StringBuffer sql = new StringBuffer();
		
		try
		{
			sql.append(" SELECT codigo ped_cod");
			sql.append(" from a_vp_c_0 ");
			sql.append(" where codigo not in (select codigo from a_vp_l_0 where articulo matches '01193*') ");
			
			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsVentas.TYPE_FORWARD_ONLY,rsVentas.CONCUR_READ_ONLY);

			rsVentas = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			while(rsVentas.next())
			{
				return "2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return "0";
	}
	
	public ArrayList<String> recogerRetrabajos()
	{
		ResultSet rsRetrabajos = null;
		ArrayList<String> vectorRetrabajo = null;
		/*
		 * saco informacion prepedidos
		 */
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(a.unidades-unidades_serv) se_res, ");
		cadenaSQL.append(" a.articulo[2,8] se_art ");
     	cadenaSQL.append(" FROM a_vp_l_0 a ");
     	cadenaSQL.append(" where a.cumplimentacion is null ");
     	cadenaSQL.append(" and a.movimiento = 'RE' ");
     	cadenaSQL.append(" group by 2 ");
     	

     	try
		{
     		con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsRetrabajos.TYPE_FORWARD_ONLY,rsRetrabajos.CONCUR_READ_ONLY);
			rsRetrabajos = this.cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vectorRetrabajo = new ArrayList<String>();
			
			while(rsRetrabajos.next())
			{
				vectorRetrabajo.add(rsRetrabajos.getString("se_art").trim());
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsRetrabajos!=null)
     		{
     			rsRetrabajos=null;
     		}
     	}
     	return vectorRetrabajo;
	}
	public ArrayList<String> recuperarPedidosPendientesCumplimentarSGA(String r_ejercicio, String r_documento)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsExpedicion = null;
		Connection con = null;
		Statement cs = null;

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());

		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct a_vp_c_0.codigo ");
			cadenaSql.append(" from a_vp_c_0 ");
			cadenaSql.append(" where a_vp_c_0.ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append(" and a_vp_c_0.documento = '" + r_documento+ "' ");
			cadenaSql.append(" and a_vp_c_0.cumplimentacion is null ");
			cadenaSql.append(" and finalidad in ('FAB','Ok', 'Ppd')");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsExpedicion= cs.executeQuery(cadenaSql.toString());
			while(rsExpedicion.next())
			{
				strDocumentos.add(rsExpedicion.getString("codigo"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsExpedicion!=null)
				{
					rsExpedicion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}

	public ArrayList<String> recuperarArticulosPedido(Integer r_codigo)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsExpedicion = null;
		Connection con = null;
		Statement cs = null;

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());

		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct a_vp_l_0.articulo ");
			cadenaSql.append(" from a_vp_l_0 ");
			cadenaSql.append(" where a_vp_l_0.codigo = " + r_codigo );
			cadenaSql.append(" and a_vp_l_0.cumplimentacion is null ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsExpedicion= cs.executeQuery(cadenaSql.toString());
			while(rsExpedicion.next())
			{
				strDocumentos.add(rsExpedicion.getString("articulo"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsExpedicion!=null)
				{
					rsExpedicion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}

	public boolean comprobarPedidoPendiente(String r_codigo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL =null;
		Connection con = null;	
		Statement cs = null;
		
		String ejer = "0";
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" select codigo from a_vp_c_" + ejer );
			cadenaSQL.append(" where codigo = " + new Integer(r_codigo));
			cadenaSQL.append(" and cumplimentacion is null" );
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return true;
			}			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return false;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
	public boolean traerPedidoGreensysMysql(MapeoPedidosVentas r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT clave_tabla vp_clt, ");
        cadenaSQL.append(" documento vp_doc, ");
        cadenaSQL.append(" serie vp_ser, ");
        cadenaSQL.append(" codigo vp_cod, ");	    
        cadenaSQL.append(" ejercicio vp_eje ");
     	cadenaSQL.append(" FROM a_vp_c_0");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie= '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoVenta()!=null && r_mapeo.getIdCodigoVenta().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoVenta() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, clave_tabla, documento, serie, codigo desc");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			String cadenaSQLBorrado = null;
			Statement cd = null;
			
			while(rsOpcion.next())
			{
				r_mapeo.setIdCodigo(rsOpcion.getInt("vp_cod"));
				conDelete = this.conManager.establecerConexionInd();
				
				cd = conDelete.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				/*
				 * 1.- lo borro
				 */
				cadenaSQLBorrado = " delete from a_vp_l_0 where codigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());

				cadenaSQLBorrado = " delete from a_vp_c_0 where codigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				conDelete=null;
				cd=null;
				
			}
			/*
			 * inserto de nuevo el pedido
			 */
			insertado = this.insertarCabecera(r_mapeo);
			/*
			 * devuelvo verdadero
			 */
			return insertado;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
//	private void establecerTablas(String r_ejercicio)
//	{
//		RutinasEjercicios rej = new RutinasEjercicios();
//				
//		Integer digito = rej.cargarDigitoEjercicio(new Integer(r_ejercicio));
//		this.tablaCabecera = "a_vp_c_" + digito;
//		this.tablaLineas = "a_vp_l_" + digito;
//		this.tablaPies = "a_vp_p_" + digito;
//	}

	public String generarInforme(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(codigoInterno);
    	libImpresion.setArchivoDefinitivo("/pedido - " + numeroDocumento.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
    	libImpresion.setArchivoPlantilla("impresoPedido.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle(null);
//    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaDetalle("impresoPedido_subreport0.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("impresoPedido_subreport0.jasper");
    	libImpresion.setArchivoPlantillaNotas("impresoPedido_notas.jrxml");
    	libImpresion.setArchivoPlantillaNotasCompilado("impresoPedido_notas.jasper");
//    	libImpresion.setArchivoPlantillaNotas(null);
//    	libImpresion.setArchivoPlantillaNotasCompilado(null);
    	libImpresion.setCarpeta("pedidosVenta");
    	libImpresion.setBackGround("/fondoA4PedidoCompra.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo(); 
    	
    	return resultadoGeneracion;
	}

	public String generarInformeSGA(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion=new LibreriaImpresion();
		
		libImpresion.setCodigo(codigoInterno);
		libImpresion.setArchivoDefinitivo("/pedido - " + numeroDocumento.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("impresoPedidoSGA.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle(null);
//    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaDetalle("impresoPedido_subreportGSYS.jrxml");
		libImpresion.setArchivoPlantillaDetalleCompilado("impresoPedido_subreportGSYS.jasper");
		libImpresion.setArchivoPlantillaNotas("impresoPedido_subreportSGA.jrxml");
		libImpresion.setArchivoPlantillaNotasCompilado("impresoPedido_subreportSGA.jasper");
//    	libImpresion.setArchivoPlantillaNotas(null);
//    	libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("pedidosVenta");
		libImpresion.setBackGround("/fondoA4LogoBlanco.jpg");
		
		resultadoGeneracion=libImpresion.generacionFinal();
		
		
		if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo(); 
		
		return resultadoGeneracion;
	}
	

	public String actualizarPedido(MapeoPedidosVentas r_mapeo,String r_situacion)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
//		String tabla = "e__vp_c";
		if (r_situacion.toUpperCase().contentEquals("PPD")) return "";
		
		
		if (r_mapeo.getEstado().toUpperCase().contentEquals("PPD") && r_situacion.toUpperCase().contentEquals("OK"))
		{
			String ubic = traerUbicacionLPNSGA(r_mapeo);
			if (ubic==null || (ubic!=null && !ubic.trim().toUpperCase().contentEquals("1") && !ubic.trim().toUpperCase().contentEquals("2") && !ubic.trim().toUpperCase().contentEquals("3") && !ubic.trim().toUpperCase().contentEquals("4")))
			{
				Notificaciones.getInstance().mensajeError("El LPN del prepedido no está en PS 1,2,3 o 4");
				return "";
			}
		}
		try
		{
			
			cadenaSQL.append(" UPDATE a_vp_c_0 set ");
			cadenaSQL.append(" a_vp_c_0.finalidad = ?, ");
			cadenaSQL.append(" a_vp_c_0.fpaso_fabric = '' ");
			cadenaSQL.append(" where a_vp_c_0.documento = ? ");
			cadenaSQL.append(" and a_vp_c_0.codigo = ? ");
			
			con= this.conManager.establecerConexionGestionInd();	 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			
			preparedStatement.setString(1, r_situacion);
			preparedStatement.setString(2, r_mapeo.getDocumento());
			preparedStatement.setInt(3, r_mapeo.getIdCodigoVenta());
			
			preparedStatement.executeUpdate();
			
			rdo=null;
			servidorSincronizadorSGA ssSGA  = null;
			
			if (r_situacion.contentEquals("Ok"))
			{
				ssSGA = servidorSincronizadorSGA.getInstance(CurrentUser.get());
				ssSGA.sincronizarSGA("PEDIDOS VENTA",r_mapeo.getIdCodigoVenta().toString(),null, null ,false,true,false);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	    	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		return rdo;
	}

	public boolean traerPedidoSGAMysql(MapeoPedidosVentas r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		Boolean borrado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT ORDLIST.NAME AS pickList, ");
		cadenaSQL.append(" CAB.ID as idPedido , ");
		cadenaSQL.append(" CAB.DTSVALIDFROM as fecha, ");
        cadenaSQL.append(" ART.NAME AS articulo, ");
        cadenaSQL.append(" ART.DESCRIPTION AS descripcion, ");
        cadenaSQL.append(" ART.EANCODE AS gtin, ");
        cadenaSQL.append(" PALET.NAME AS lpn, ");        
        cadenaSQL.append(" LIST.QTY qty, ");        
        cadenaSQL.append(" UBIC.FULLNAME AS ubic ");
        
        cadenaSQL.append(" from OBJT_PICKORDER AS PICK ");
        cadenaSQL.append(" LEFT JOIN OBJT_TASK AS LIST ON PICK.OID=LIST.ORDEROID ");
        cadenaSQL.append(" LEFT JOIN OBJT_TASKORDER AS ORDLIST ON LIST.TASKORDEROID = ORDLIST.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_ITEM AS ART ON LIST.ITEMOID = ART.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_ORDERLINK AS ORD ON PICK.OID = ORD.CHILDOID ");
        cadenaSQL.append(" LEFT JOIN OBJT_OUTBOUNDORDER AS CAB ON ORD.PARENTOID=CAB.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS UBIC ON LIST.SOURCELOCATIONOID = UBIC.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_CONTAINERSTORAGEITEMQTY AS PALET ON LIST.CONTAINERSTORAGEITEMQTYOID = PALET.OID ");
        
        
/*
-- O0000000065  abortado (pedido cancelado)
-- 2401728		cerrado (pedido servido/cerrado)
-- 2401527		para cargar (tiene orden y lista de pick)
-- 2401599		pendiente de servir (tiene orden de pick pero no lista de pick)
-- 2401528		pendiente de procesar (no tiene nada generado)
*/
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigoVenta()!=null && r_mapeo.getIdCodigoVenta().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" CAB.id like '%" + r_mapeo.getIdCodigoVenta() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" and ");
			}
			else
			{
				cadenaSQL.append(" where ");
			}
			cadenaSQL.append(" LIST.STATUS = '2' ");
			cadenaSQL.append(" and CAB.ID is not null ");
			
			/*
			 * conectamos con SQL real
			 * Buscamos el pedido y nos lo traemos para insertar los datos a imprimir en el mysql
			 * 
			 * tablaNueva a crear: pedidosSGA
			 * tablaNueva a crear: lineasPedidosSGA
			 * 
			 * posteriormente generaremos el informe de pedido de venta partiendo de estos datos y esta nueva tabla
			 * 
			 */
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			String cadenaSQLBorrado = null;
			Statement cd = null;
			
			
			while(rsOpcion.next())
			{
				/*
				 * Borramos los datos de este pedido en las tablas del mysql
				 */
				if (!borrado)
				{
					r_mapeo.setIdCodigo(r_mapeo.getIdCodigoVenta());
					conDelete = this.conManager.establecerConexionInd();
				
					cd = conDelete.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				
					cadenaSQLBorrado = " delete from sga_sales_orders_lin where idPedido like '%" + r_mapeo.getIdCodigo() + "'";				
					cd.execute(cadenaSQLBorrado.toString());
				
					cadenaSQLBorrado = " delete from sga_sales_orders where idPedido like '%" + r_mapeo.getIdCodigo()+"'";
					cd.execute(cadenaSQLBorrado.toString());

					conDelete=null;
					cd=null;
					borrado=true;
					
					/*
					 * Recojo los datos de la cabecera y los mando insertar
					 */
					MapeoPedidosSGA mapeoPedidosSGA = new MapeoPedidosSGA();
					mapeoPedidosSGA.setIdCodigoVenta(rsOpcion.getString("idPedido"));
					mapeoPedidosSGA.setPickList(rsOpcion.getString("pickList"));
					mapeoPedidosSGA.setFecha(rsOpcion.getDate("fecha"));
					mapeoPedidosSGA.setIdPed(r_mapeo.getIdCodigoVenta());
					
					insertado = this.insertarCabeceraSGA(mapeoPedidosSGA);
				}
				/*
				 * recojo los datos de las lineas y las mando insertar.
				 */
				if (insertado)
				{
					MapeoLineasPedidosSGA mapeoLineas = new MapeoLineasPedidosSGA();
					mapeoLineas.setArticulo(rsOpcion.getString("articulo"));
					mapeoLineas.setDescripcion(rsOpcion.getString("descripcion"));
					mapeoLineas.setGtin(rsOpcion.getString("gtin"));
					mapeoLineas.setLpn(rsOpcion.getString("lpn"));
					mapeoLineas.setUnidades(rsOpcion.getDouble("qty"));
					mapeoLineas.setUbic(rsOpcion.getString("ubic"));
					mapeoLineas.setIdCodigoVenta(rsOpcion.getString("idPedido"));
					mapeoLineas.setPick(rsOpcion.getString("pickList"));
					mapeoLineas.setIdPed(r_mapeo.getIdCodigoVenta());
					
					insertado = this.insertarLineasSGA(mapeoLineas);
				}
				else
				{
					break;
				}

			}
			/*
			 * inserto de nuevo el pedido
			 */
			/*
			 * devuelvo verdadero
			 */
			return insertado;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}

	public String traerUbicacionLPNSGA(MapeoPedidosVentas r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		Boolean borrado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lpn.OID, ");
		cadenaSQL.append(" lpn.ITEMOID, ");
		cadenaSQL.append(" lpn.NAME, ");
		cadenaSQL.append(" lpn.VALUE, ");
		cadenaSQL.append(" lpn.WAREHOUSELOCATIONOID, ");
		cadenaSQL.append(" ubic.name ubicadoEn ");
		
		cadenaSQL.append(" from OBJT_CONTAINERSTORAGEITEMQTY AS lpn ");
		cadenaSQL.append(" left join OBJT_WAREHOUSELOCATION ubic on ubic.OID = lpn.WAREHOUSELOCATIONOID ");
		
		/*
-- O0000000065  abortado (pedido cancelado)
-- 2401728		cerrado (pedido servido/cerrado)
-- 2401527		para cargar (tiene orden y lista de pick)
-- 2401599		pendiente de servir (tiene orden de pick pero no lista de pick)
-- 2401528		pendiente de procesar (no tiene nada generado)
		 */
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigoVenta()!=null && r_mapeo.getIdCodigoVenta().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lpn.USRTXT1 = '" + r_mapeo.getIdCodigoVenta() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());

			while(rsOpcion.next())
			{
				/*
				 * Borramos los datos de este pedido en las tablas del mysql
				 */
					return rsOpcion.getString("ubicadoEn");
			}
			/*
			 * inserto de nuevo el pedido
			 */
			/*
			 * devuelvo verdadero
			 */
			return null;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public boolean insertarCabeceraSGA(MapeoPedidosSGA r_mapeo)
	{
		/*
		 * Codigo del proveedor
		 * Nombre comercial del proveedor
		 * Direccion del proveedor 
		 * Codigo postal del proveedor
		 * Poblacion del proveedor 
		 * Provincia del proveedor
		 * Num. Fax del proveedor
		 * mail1
		 * mail2
		 * Fecha de pedido
		 * Numero de pedido
		 * Descripcion de forma de pago
		 * Plazo de entrega del pedido
		 * 
		 */
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		boolean insertado = true;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" INSERT INTO sga_sales_orders ( ");
			cadenaSQL.append(" idPedido, ");
	        cadenaSQL.append(" estado, ");
	        cadenaSQL.append(" fecha, ");
	        cadenaSQL.append(" pickList, ");
	        cadenaSQL.append(" codigo) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getIdCodigoVenta()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getIdCodigoVenta());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(2, "");
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getPickList() !=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getPickList());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getIdPed() !=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getIdPed());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }

	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	
	
	public boolean insertarLineasSGA(MapeoLineasPedidosSGA r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		Boolean insertado = true;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" INSERT INTO sga_sales_orders_lin ( ");
			cadenaSQL.append(" articulo, ");
			cadenaSQL.append(" descripcion, ");
			cadenaSQL.append(" gtin, ");
			cadenaSQL.append(" lpn, ");
			cadenaSQL.append(" qty, ");
			cadenaSQL.append(" qty_srv, ");
			cadenaSQL.append(" status, ");
			cadenaSQL.append(" ubic, ");
			cadenaSQL.append(" idPedido, ");
			cadenaSQL.append(" pick, ");
			cadenaSQL.append(" codigo ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");
			
			conInsert = this.conManager.establecerConexionInd();
			preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getGtin()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getGtin());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getLpn()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getLpn());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			preparedStatement.setDouble(5, r_mapeo.getUnidades());
			preparedStatement.setInt(6,0);
			if (r_mapeo.getEstado()!=null)
			{
				preparedStatement.setString(7, "");
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getUbic()!=null)
			{
				preparedStatement.setString(8, r_mapeo.getUbic());
			}
			else
			{
				preparedStatement.setString(8, null);
			}
			preparedStatement.setString(9, r_mapeo.getIdCodigoVenta());
			preparedStatement.setString(10, r_mapeo.getPick());
			preparedStatement.setInt(11, r_mapeo.getIdPed());
			
			preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	
	public boolean insertarCabecera(MapeoPedidosVentas r_mapeo)
	{
		/*
		 * Codigo del proveedor
		 * Nombre comercial del proveedor
		 * Direccion del proveedor 
		 * Codigo postal del proveedor
		 * Poblacion del proveedor 
		 * Provincia del proveedor
		 * Num. Fax del proveedor
		 * mail1
		 * mail2
		 * Fecha de pedido
		 * Numero de pedido
		 * Descripcion de forma de pago
		 * Plazo de entrega del pedido
		 * 
		 * 
		 * MapeoPedidosVentas extends MapeoGlobal
			{
			    private Integer idCodigoVenta;
			    private Integer ejercicio;
			    private Integer almacen;
			    private Integer cliente;
			    private Integer importe;
			
			    private String clave_tabla;
				private String documento;
				private String serie;
				private String nombre;
				private String moneda;
				private String pais;
				private String estado;
				private String plazoEntrega;
				private String referencia;
				private String ordenFabric;
				private String lineas = " ";
				private String pdf = " ";
				
				private Date fecha;
				private Date fechaCliente;
		 */
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		boolean insertado = true;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" INSERT INTO a_vp_c_0 ( ");
    		cadenaSQL.append(" clave_tabla, ");
	        cadenaSQL.append(" documento, ");
	        cadenaSQL.append(" serie, ");
	        cadenaSQL.append(" codigo, ");
	        cadenaSQL.append(" fecha_documento, ");
	        cadenaSQL.append(" ejercicio, ");
	        cadenaSQL.append(" cliente, ");
	        cadenaSQL.append(" nombre_comercial, ");
	        cadenaSQL.append(" plazo_entrega) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getClave_tabla()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdCodigoVenta());
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    preparedStatement.setInt(6, r_mapeo.getEjercicio());
		    preparedStatement.setInt(7, r_mapeo.getCliente());
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(8, RutinasCadenas.conversion(r_mapeo.getNombre()));
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getPlazoEntrega()!=null)
		    {
		    	preparedStatement.setString(9, RutinasFechas.convertirAFechaMysql(r_mapeo.getPlazoEntrega()));
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }

	        preparedStatement.executeUpdate();
	        
			
	        /*
	         * insertamos las lineas tras el exito de la cabecera
	         */
	        insertado=this.insertarLineas(r_mapeo);
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	public boolean insertarLineas(MapeoPedidosVentas r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaWhere =null;
		Boolean insertado = true;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT clave_tabla cl_clt, ");
        cadenaSQL.append(" documento cl_doc, ");
        cadenaSQL.append(" serie cl_ser, ");
        cadenaSQL.append(" codigo cl_cod, ");	    
        cadenaSQL.append(" ejercicio cl_eje, ");
        cadenaSQL.append(" articulo cl_ar, ");
        cadenaSQL.append(" posicion cl_pos, ");
        cadenaSQL.append(" num_linea cl_lin, ");
        cadenaSQL.append(" descrip_articulo cl_des, ");
        cadenaSQL.append(" cantidad cl_can, ");
        cadenaSQL.append(" unidades cl_ud, ");
        cadenaSQL.append(" unidades_serv cl_uds ");
        
     	cadenaSQL.append(" FROM a_vp_l_0 ");
     	
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie= '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoVenta()!=null && r_mapeo.getIdCodigoVenta().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoVenta() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by clave_tabla, documento, serie, codigo ,posicion");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			conInsert= this.conManager.establecerConexionInd();	
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				MapeoLineasPedidosVentas mapeoLineas = new MapeoLineasPedidosVentas();
				mapeoLineas.setDocumento(r_mapeo.getDocumento());
				mapeoLineas.setIdCodigoVenta(r_mapeo.getIdCodigoVenta());
				mapeoLineas.setEjercicio(r_mapeo.getEjercicio());
				mapeoLineas.setArticulo(rsOpcion.getString("cl_ar"));
				mapeoLineas.setDescripcion(rsOpcion.getString("cl_des"));
				mapeoLineas.setUnidades(rsOpcion.getInt("cl_ud"));
				mapeoLineas.setUnidades_serv(rsOpcion.getInt("cl_uds"));
				
				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_vp_l_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");
		        cadenaSQL.append(" ejercicio, ");
		        
		        cadenaSQL.append(" articulo, ");
		        cadenaSQL.append(" descrip_articulo, ");
		        cadenaSQL.append(" unidades, ");
		        cadenaSQL.append(" unidades_serv) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			    
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoVenta());
			    preparedStatement.setInt(5, r_mapeo.getEjercicio());
			    if (mapeoLineas.getArticulo()!=null)
			    {
			    	preparedStatement.setString(6, mapeoLineas.getArticulo());
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    if (mapeoLineas.getDescripcion()!=null)
			    {
			    	preparedStatement.setString(7, RutinasCadenas.conversion(mapeoLineas.getDescripcion()));
			    }
			    else
			    {
			    	preparedStatement.setString(7, null);
			    }
			    preparedStatement.setDouble(8, mapeoLineas.getUnidades());
			    preparedStatement.setDouble(9, mapeoLineas.getUnidades_serv());

		        preparedStatement.executeUpdate();
		        preparedStatement.close();
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}

	public boolean comprobarArticuloSGA(String r_ejercicio, String r_documento, String r_codigo)
	{
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		String articulo = null;
		StringBuffer cadenaSql = new StringBuffer();
		
		try
		{
			cadenaSql.append("select a_vp_l_0.articulo ");
			cadenaSql.append("from a_vp_l_0 ");
			cadenaSql.append("where a_vp_l_0.ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append("and a_vp_l_0.documento = '" + r_documento + "' ");
			cadenaSql.append("and a_vp_l_0.codigo = '" + r_codigo + "' ");
			cadenaSql.append("and a_vp_l_0.cumplimentacion is null ");
			cadenaSql.append("and (a_vp_l_0.articulo matches '0102*' or a_vp_l_0.articulo matches '0103*' or a_vp_l_0.articulo matches '0111*' or a_vp_l_0.articulo matches '0106*' ");
			cadenaSql.append("     or a_vp_l_0.articulo matches '0107*' or a_vp_l_0.articulo matches '0109*') ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				articulo = rsPrepedidos.getString("articulo");
//				articulo="123";
				cadenaSql = new StringBuffer();
				cadenaSql.append("select OBJT_ITEM.CLASSOID ");
				cadenaSql.append("from OBJT_ITEM ");
				cadenaSql.append("where OBJT_ITEM.name = '" + articulo + "' ");
				
				con=null;
				cs=null;
				rsPrepedidos.close();
				rsPrepedidos = null;
				con= this.conManager.establecerConexionSGAInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsPrepedidos= cs.executeQuery(cadenaSql.toString());
				
				while(rsPrepedidos.next())
				{
					return true;
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
	public boolean comprobarClienteSGA(String r_ejercicio, String r_documento, String r_codigo)
	{
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		Integer cliente = null;
		StringBuffer cadenaSql = new StringBuffer();
		
		try
		{
			cadenaSql.append("select a_vp_c_0.cliente ");
			cadenaSql.append("from a_vp_c_0 ");
			cadenaSql.append("where a_vp_c_0.ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append("and a_vp_c_0.documento = '" + r_documento + "' ");
			cadenaSql.append("and a_vp_c_0.codigo = '" + r_codigo + "' ");
			cadenaSql.append("and a_vp_c_0.cumplimentacion is null");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				cliente = rsPrepedidos.getInt("cliente");
//				cliente=123;
				cadenaSql = new StringBuffer();
				cadenaSql.append("select OBJT_CUSTOMER.id ");
				cadenaSql.append("from OBJT_CUSTOMER ");
				cadenaSql.append("where OBJT_CUSTOMER.ID = " + cliente);
				
				con=null;
				cs=null;
				rsPrepedidos.close();
				rsPrepedidos = null;
				con= this.conManager.establecerConexionSGAInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsPrepedidos= cs.executeQuery(cadenaSql.toString());
				
				while(rsPrepedidos.next())
				{
					return true;
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
}