package borsao.e_borsao.Modulos.CRM.PedidosVentas.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.CRM.consultaStockAlmacenesExternos.server.consultaStockExternosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionAsignacionProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasPedidosVentas extends Window
{
	
	private Button btnBotonCVentana = null;
	private Button btnVerPedidos= null;
	private Button btnRegistroCorrecto = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private boolean verStock=true;
	private boolean verArticulo=false;
	private boolean externos=false;
	public boolean multiSelect = false;
	private Window pantallaPadre=null;
	
	private VerticalLayout principal=null;
	private HorizontalLayout botonera=null;
	
	
	private void crearBotones()
	{
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		btnVerPedidos = new Button("Ver Pedido");
		btnVerPedidos.addStyleName(ValoTheme.BUTTON_FRIENDLY); 
		btnVerPedidos.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnVerPedidos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verPedidos();
			}
		});
		
		botonera.addComponent(btnVerPedidos);	
		botonera.addComponent(btnBotonCVentana);		

		botonera.setComponentAlignment(btnVerPedidos, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);

	}
	
	private void estiloPantalla(String r_titulo)
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
	}
	public pantallaLineasPedidosVentas(String r_articulo, Integer r_stock, String r_titulo)
	{
		consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
		MapeoLineasPedidosVentas mapeo = new MapeoLineasPedidosVentas();
		mapeo.setArticulo(r_articulo);
		ArrayList<MapeoLineasPedidosVentas> vector = cse.datosOpcionesGlobal(mapeo, r_stock,"N");

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.asignarEstiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasPedidosVentas(String r_articulo, String r_titulo,boolean r_verArticulo)
	{
		this.verArticulo=r_verArticulo;
		consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
		ArrayList<MapeoLineasPedidosVentas> vector = cse.datosOpcionesGlobal(r_articulo, "N");

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.asignarEstiloGrid();
		
		principal.addComponent(this.gridDatos);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	
	public pantallaLineasPedidosVentas(Window r_app,String r_articulo, Integer r_stock, String r_titulo, boolean r_multi)
	{
		this.pantallaPadre=r_app;
		this.multiSelect=r_multi;

		consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
		MapeoLineasPedidosVentas mapeo = new MapeoLineasPedidosVentas();
		mapeo.setArticulo(r_articulo);
		ArrayList<MapeoLineasPedidosVentas> vector = cse.datosOpcionesGlobal(mapeo, r_stock,"N");

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.asignarEstiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasPedidosVentas(String r_almacen, String r_articulo, Integer r_stock, String r_titulo)
	{
		consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
		MapeoLineasPedidosVentas mapeo = new MapeoLineasPedidosVentas();
		mapeo.setArticulo(r_articulo);		
		mapeo.setAlmacen(r_almacen);
		ArrayList<MapeoLineasPedidosVentas> vector = cse.datosOpcionesGlobal(mapeo, r_stock,"N");

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.asignarEstiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasPedidosVentas(String r_titulo)
	{
		ArrayList<MapeoLineasPedidosVentas> vector = null;
		
		this.estiloPantalla(r_titulo);
		this.crearBotones();

		switch(r_titulo.trim())
		{
			case "Pedidos sin PALLET":
			{
				consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
				vector = cse.pedidosSinPallet();
				this.llenarRegistrosSinPalet(vector);
				break;
			}
			case "Pedidos Externos Erroneos":
			{
				consultaStockExternosServer csex = new consultaStockExternosServer(CurrentUser.get());
				vector = csex.PedidosMalCumplimentados();
				this.llenarRegistrosExternos(vector);
				externos=true;
				break;
			}
		}

		this.asignarEstiloGrid();
		
		if (r_titulo.trim().equals("Pedidos Externos Erroneos"))
		{
			btnRegistroCorrecto = new Button("Dar por bueno");
			btnRegistroCorrecto.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnRegistroCorrecto.addStyleName(ValoTheme.BUTTON_TINY);
			
			btnRegistroCorrecto.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					confirmarRegistro();
				}
			});
		}
		
		principal.addComponent(this.gridDatos);
		if (r_titulo.trim().equals("Pedidos Externos Erroneos"))
		{
			botonera.addComponent(btnRegistroCorrecto);
		}		
		botonera.addComponent(btnBotonCVentana);
		if (r_titulo.trim().equals("Pedidos Externos Erroneos"))
		{
			botonera.setComponentAlignment(btnRegistroCorrecto, Alignment.BOTTOM_CENTER);
		}		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	private void confirmarRegistro()
	{
		String rdo = null;
		Item file = null;
		
		if (gridDatos.getSelectedRow()==null)
			Notificaciones.getInstance().mensajeAdvertencia("Debes seleccionar el resgistro a confirmar");
		else
		{
			/*
			 * actualizaremos lal inea del pedido poniendo en el campo obs un valor ok que no tendrá en cuenta el chequeo
			 */
			file = gridDatos.getContainerDataSource().getItem(gridDatos.getSelectedRow());
			
			consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
			
			int digito = new Integer (RutinasFechas.añoActualYYYY()) - new Integer(file.getItemProperty("Ejercicio").getValue().toString().trim());
			
			rdo = cps.actualizarLineaPedidoExterno(digito, new Integer(file.getItemProperty("Codigo").getValue().toString().trim()), new Integer(file.getItemProperty("Linea").getValue().toString().trim()));
			
			if (rdo!=null)
				Notificaciones.getInstance().mensajeAdvertencia(rdo);
			else
				close();
		}
	}
	
	public pantallaLineasPedidosVentas(Integer r_codigo, String r_titulo)
	{
		this.verStock=false;
		this.verArticulo=true;
		
		consultaPedidosVentasServer cse = new consultaPedidosVentasServer(CurrentUser.get());
		MapeoLineasPedidosVentas mapeo = new MapeoLineasPedidosVentas();
		mapeo.setIdCodigoVenta(r_codigo);
		ArrayList<MapeoLineasPedidosVentas> vector = cse.datosOpcionesGlobal(mapeo, null,"T");

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.asignarEstiloGrid();
		
		botonera.removeComponent(this.btnVerPedidos);
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasPedidosVentas(ArrayList<MapeoLineasPedidosVentas> r_vector, String r_titulo)
	{
		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(r_vector);
		this.asignarEstiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoLineasPedidosVentas> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosVentas mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("estado", String.class, null);
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Almacen", String.class, null);
//        container.addContainerProperty("Documento", String.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Plazo Entrega", String.class, null);
		container.addContainerProperty("Cliente", String.class, null);
		container.addContainerProperty("Nombre", String.class, null);
		if (verArticulo) container.addContainerProperty("Articulo", String.class, null);
		if (verArticulo) container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Unidades", Integer.class, null);
		container.addContainerProperty("Servidas", Integer.class, null);
		container.addContainerProperty("Pendientes", Integer.class, null);
		if (verStock) container.addContainerProperty("Stock", Integer.class, null);
        container.addContainerProperty("Palet", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosVentas) iterator.next();
			
    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreCliente()));
//    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
    		
    		if (mapeoAyuda.getFechaDocumento()!=null)
    		{
    			file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    			file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
    			file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoVenta().toString());
    			file.getItemProperty("Cliente").setValue(mapeoAyuda.getCliente().toString());
    		}
    		else
    		{
    			file.getItemProperty("Fecha Documento").setValue(null);
    			file.getItemProperty("Plazo Entrega").setValue(null);
    			file.getItemProperty("Codigo").setValue(null);
    			file.getItemProperty("Cliente").setValue(null);
    		}
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen());
			if (verArticulo) file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			if (verArticulo) file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
    		file.getItemProperty("Servidas").setValue(mapeoAyuda.getUnidades_serv());
    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getUnidades_pdte());
    		if (verStock) file.getItemProperty("Stock").setValue(mapeoAyuda.getStock_queda());    		
    		file.getItemProperty("Palet").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcionPalet()));
    		file.getItemProperty("estado").setValue(RutinasCadenas.conversion(mapeoAyuda.getFinalidad()));
		}

		this.gridDatos= new Grid(container);
		if (this.multiSelect) this.gridDatos.setSelectionMode(SelectionMode.MULTI); else this.gridDatos.setSelectionMode(SelectionMode.NONE);
//		this.gridDatos.getColumn("estado").setHidden(true);
		this.asignarEstilos();
	}
	
	private void llenarRegistrosSinPalet(ArrayList<MapeoLineasPedidosVentas> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosVentas mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("estado", String.class, null);
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Plazo Entrega", String.class, null);
		container.addContainerProperty("Cliente", String.class, null);
		container.addContainerProperty("Nombre", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosVentas) iterator.next();
			file.getItemProperty("estado").setValue(RutinasCadenas.conversion(mapeoAyuda.getFinalidad()));
    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreCliente()));
    		
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoVenta().toString());
    		file.getItemProperty("Cliente").setValue(mapeoAyuda.getCliente().toString());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);

		this.asignarEstilos();
	}
	
	private void llenarRegistrosExternos(ArrayList<MapeoLineasPedidosVentas> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosVentas mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Linea", Integer.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripción", String.class, null);
		container.addContainerProperty("Unidades", Integer.class, null);
		container.addContainerProperty("Pendientes", Integer.class, null);

        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosVentas) iterator.next();
    		
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
//    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoVenta().toString());
    		file.getItemProperty("Linea").setValue(mapeoAyuda.getNum_linea());
    		file.getItemProperty("Articulo").setValue(mapeoAyuda.getArticulo());
    		file.getItemProperty("Descripción").setValue(mapeoAyuda.getDescripcion());
    		
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getUnidades_pdte());
    		
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);

		this.asignarEstilos();
	}
	private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
		this.gridDatos.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
	private String getCellDescription(CellReference cell) 
	{
		
		String strEstado = "";
		String descriptionText = "";
			                
		Item file = cell.getItem();
				
				
		if ("Plazo Entrega".equals(cell.getPropertyId()))
        {
			if (file.getItemProperty("estado")!=null && file.getItemProperty("estado").getValue()!=null)
			{
				strEstado = file.getItemProperty("estado").getValue().toString();
			}
			
        	switch (strEstado.trim())
        	{
        		case "Pdt":
        		{
        			descriptionText = "Pendiente Confirmar";
        			break;
        		}
        		case "Pre":
        		{
        			descriptionText = "Preparado";
        			break;
        		}
        		case "Ok":
        		{
        			descriptionText = "Carga Confirmada";
        			break;
        		}
        		case "FAB":
        		{
        			descriptionText = "Planificar Fabricacion";
        			break;
        		}
        		case "Fin":
        		{
        			descriptionText = "Retenido Contabilidad";
        			break;
        		}
        		case "Ret":
        		{
        			descriptionText = "Pendiente Retrabajo";
        			break;
        		}
        		case "Mat":
        		{
        			descriptionText = "Falta Materiales";
        			break;
        		}
        		case "Tpt":
        		{
        			descriptionText = "Avisado Transporte";
        			break;
        		}
        		case "Tpc":
        		{
        			descriptionText = "Avisado Cliente";
        			break;
        		}
        		case "TyC":
        		{
        			descriptionText = "Avisado Transporte y Cliente";
        			break;
        		}
        		default:
        		{
        			descriptionText = "Pendiente Confirmar";
        			break;
        		}
        	}
        }
		return descriptionText;
	}

	public void asignarEstilos()
    {
		this.asignarTooltips();
		
	
		
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            String strRetrabajo = "";
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()==null)
            		{
            			strEstado = "Pdt";
            		}
            		else
            		{
            			strEstado=cellReference.getValue().toString();
            		}
            	}
            	if ("Plazo Entrega".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "PRE":
	        			{
	        				return "Rcell-darkgreen";
	        			}
	        			case "PDT":
	        			{
	        				return "Rcell-error";
	        			}
	        			case "OK":
	        			{
	        				return "Rcell-green";
	        			}
	        			case "FAB":
	        			{
	        				return "Rcell-warning";
	        			}
	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	else if ("Cliente".equals(cellReference.getPropertyId())) 
            	{
            		strRetrabajo="N";
            		if (cellReference.getValue() !=null && (new Integer(cellReference.getValue().toString()) == 0))
        			{
            			strRetrabajo="S";
        			}
            		return "Rcell-normal";
            	}
            	else if ("Almacen".equals(cellReference.getPropertyId()) || "Unidades".equals(cellReference.getPropertyId()) || "Servidas".equals(cellReference.getPropertyId()) || "Pendientes".equals(cellReference.getPropertyId()) || "Stock".equals(cellReference.getPropertyId())
            			|| "Ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		if ("Stock".equals(cellReference.getPropertyId()) && cellReference.getValue() !=null && new Integer(cellReference.getValue().toString()) < 0)
            		{
            			return "Rcell-error";
            		}        			
        			else 
        			{
        				return "Rcell-normal";
        			}
            	}
            	else if ("Nombre".equals(cellReference.getPropertyId())) 
            	{
            		if (strRetrabajo.equals("S")) return "Rcell-green"; else return "Rcell-normal";  
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void cerrar()
	{
		String ids = "";
		Iterator it = null;

		if (this.multiSelect)
		{
			Collection<Object> r_filas = this.gridDatos.getSelectedRows();
			it = r_filas.iterator();
	
	
	
			while (it.hasNext())
			{			
				Integer itemSeleccionado = (Integer) it.next();
				Item file = (Item) this.gridDatos.getContainerDataSource().getItem(itemSeleccionado);
				
				if (file.getItemProperty("Codigo").getValue()!= null && file.getItemProperty("Codigo").getValue().toString().length()>0)
				{
					if (ids.length()==0)
						ids=file.getItemProperty("Codigo").getValue().toString();
					else
						ids = ids + "," + file.getItemProperty("Codigo").getValue().toString();
				}
			}
			
			if (this.pantallaPadre instanceof PeticionAsignacionProgramacion && ids.length()>0)
			{
				((PeticionAsignacionProgramacion) this.pantallaPadre).recogerIds(ids);
			}
		}	
		close();
	}
	
	private void verPedidos()
	{
		if (this.gridDatos.getSelectedRow()!=null)
		{
			Item file = this.gridDatos.getContainerDataSource().getItem(this.gridDatos.getSelectedRow());
			
			pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(new Integer(file.getItemProperty("Codigo").getValue().toString()),"Pedido: " + file.getItemProperty("Codigo").getValue().toString());
			this.getUI().addWindow(vt);
		}
		else
			Notificaciones.getInstance().mensajeAdvertencia("Debes seleccionar un albarán para consultar sus pedidos" );

	}
	
	private void asignarEstiloGrid()
	{
		if (this.gridDatos!=null)
		{
			this.gridDatos.setSizeFull();
			this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
			this.gridDatos.setWidth("100%");
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setHeight("650");
			this.gridDatos.setFrozenColumnCount(3);
		}

	}
}