package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view.pantallaConsultaInventarioSGA;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaEstadoPedidosServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.PedidosVentasView;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	public boolean actualizar = false;	
	private boolean editable = true;
	private boolean conFiltro = true;
	private boolean enviadoMail=false;
	private boolean conTotales = false;
	private PedidosVentasView app = null;
	private MapeoPedidosVentas mapeo = null;
	private MapeoPedidosVentas mapeoOrig = null;
	
    public OpcionesGrid(PedidosVentasView r_app,  ArrayList<MapeoPedidosVentas> r_vector) 
    {
        
        this.vector=r_vector;
        this.app = r_app;
        
		this.asignarTitulo("Pedidos de Venta");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("lineas");
    	this.camposNoFiltrar.add("pdf");
    	this.camposNoFiltrar.add("exis");
    	this.camposNoFiltrar.add("importe");
    }
    
    private void generarGrid()
    {
		this.actualizar = false;
		this.crearGrid(MapeoPedidosVentas.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setConTotales(true);
		if (app.soyAlmacen)
		{
			this.setSeleccion(SelectionMode.MULTI);
		}
		else
		{
			this.setSeleccion(SelectionMode.SINGLE);
		}
		this.setFrozenColumnCount(4);
		if (this.app.conTotales) this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder( "exis", "lineas", "pdf" ,"estado", "plazoEntrega", "almacen", "ejercicio","clave_tabla","documento","idCodigoVenta","referencia", "fecha", "fechaCliente", "cliente", "nombre", "importe", "moneda", "pais");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "75");
    	this.widthFiltros.put("almacen", "75");
    	this.widthFiltros.put("estado", "75");
    	this.widthFiltros.put("clave_tabla", "75");
    	this.widthFiltros.put("documento", "75");
    	this.widthFiltros.put("idCodigoVenta", "100");
    	this.widthFiltros.put("fecha", "100");
    	this.widthFiltros.put("fechaCliente", "100");
    	this.widthFiltros.put("plazoEntrega", "100");
    	this.widthFiltros.put("cliente", "100");
    	this.widthFiltros.put("nombre", "200");
    	this.widthFiltros.put("referencia", "100");
    	this.widthFiltros.put("moneda", "100");
    	this.widthFiltros.put("pais", "100");
    	
    	this.getColumn("lineas").setHeaderCaption("");
    	this.getColumn("lineas").setSortable(false);
    	this.getColumn("lineas").setWidth(new Double(50));
    	this.getColumn("lineas").setEditable(false);
    	this.getColumn("exis").setHeaderCaption("");
    	this.getColumn("exis").setSortable(false);
    	this.getColumn("exis").setWidth(new Double(50));
    	this.getColumn("exis").setEditable(false);
    	this.getColumn("pdf").setHeaderCaption("");
    	this.getColumn("pdf").setSortable(false);
    	this.getColumn("pdf").setWidth(new Double(50));
    	this.getColumn("pdf").setEditable(false);
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(100));
    	this.getColumn("ejercicio").setEditable(false);
    	this.getColumn("clave_tabla").setHeaderCaption("Clave Tabla");
    	this.getColumn("clave_tabla").setWidth(new Double(100));
    	this.getColumn("clave_tabla").setHidden(true);
    	this.getColumn("clave_tabla").setEditable(false);
    	this.getColumn("documento").setHeaderCaption("Documento");
    	this.getColumn("documento").setWidth(new Double(100));
    	this.getColumn("documento").setEditable(false);
    	this.getColumn("referencia").setHeaderCaption("Referencia");
    	this.getColumn("referencia").setWidth(new Double(150));
    	this.getColumn("referencia").setEditable(false);
    	
    	this.getColumn("cliente").setHeaderCaption("Cliente");
    	this.getColumn("cliente").setWidth(new Double(125));
    	this.getColumn("cliente").setEditable(false);
    	this.getColumn("idCodigoVenta").setHeaderCaption("Codigo");
    	this.getColumn("idCodigoVenta").setWidth(new Double(125));
    	this.getColumn("idCodigoVenta").setEditable(false);
    	this.getColumn("idCodigoVenta").setRenderer((Renderer) new NumberRenderer("%d"));
    	
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(125));
    	this.getColumn("fecha").setEditable(false);
    	this.getColumn("fechaCliente").setHeaderCaption("Fecha Cliente");
    	this.getColumn("fechaCliente").setWidth(new Double(125));
    	this.getColumn("fechaCliente").setEditable(false);
    	this.getColumn("nombre").setHeaderCaption("Nombre Cliente");
    	this.getColumn("nombre").setEditable(false);
    	this.getColumn("almacen").setHeaderCaption("almacen");
    	this.getColumn("almacen").setEditable(false);
    	this.getColumn("moneda").setHeaderCaption("Divisa");
    	this.getColumn("moneda").setEditable(false);
    	this.getColumn("pais").setHeaderCaption("Pais");
    	this.getColumn("pais").setEditable(false);
    	
    	this.getColumn("estado").setEditorField(getComboBox("El campo es obligatorio!"));
    	this.getColumn("estado").setEditable(true);
    	this.getColumn("plazoEntrega").setHeaderCaption("Plazo Entrega");
    	this.getColumn("plazoEntrega").setWidth(new Double(125));
    	this.getColumn("plazoEntrega").setEditorField(getFecha());
//    	this.getColumn("plazoEntrega").setEditable(true);
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("exis").setHidden(!app.soyAlmacen);
    	this.getColumn("pdf").setHidden(!app.soyAlmacen);
    	this.getColumn("documento").setHidden(true);
    	this.getColumn("ordenFabric").setHidden(true);
	}
	
    public void cargarListeners()
    {
    	
    	this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
        		mapeo = (MapeoPedidosVentas) getEditedItemId();
        		
        		java.util.Date fecha = RutinasFechas.conversionDeString(mapeo.getPlazoEntrega());
        		
        		
//        		indice = getContainer().indexOfId(mapeo);
		        
//	        	consultaProgramacionServer cs = new consultaProgramacionServer(CurrentUser.get());
//	        	Integer status = cs.obtenerStatus(mapeo.getIdProgramacion());
	        	if (fecha != null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeAdvertencia("Registro modificado por otro usuario. Actualiza los cambios antes de modificar los datos.");
	        		doCancelEditor();
	        	}
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		boolean rdo = false;

	        		mapeo = (MapeoPedidosVentas) getEditedItemId();	        		
	        		java.util.Date fecha = RutinasFechas.conversionDeString(mapeo.getPlazoEntrega());
	        		
	        		if (fecha != null) rdo = actualizarPedidoEnviado(mapeo);
	        		
	        		if (!rdo)
	        		{
	        			Notificaciones.getInstance().mensajeError("Fecha Incorrecta");
	        			refreshAllRows();
	        		}
	        		else
	        		{
		        		((ArrayList<MapeoPedidosVentas>) vector).remove(mapeoOrig);
		        		((ArrayList<MapeoPedidosVentas>) vector).add(mapeo);
	
		        		refreshAllRows();
		        		scrollTo(mapeo);
		        		select(mapeo);
	        		}
	        	}
	        }
		});
    	
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("lineas"))
            	{
            		MapeoPedidosVentas mapeo = (MapeoPedidosVentas) event.getItemId();
            		pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(mapeo.getIdCodigoVenta(),"Lineas Pedido " + mapeo.getIdCodigoVenta());
        			getUI().addWindow(vt);
            	}
            	else if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("exis"))
            	{
            		MapeoPedidosVentas mapeo = (MapeoPedidosVentas) event.getItemId();
            		pantallaConsultaInventarioSGA vt = new pantallaConsultaInventarioSGA("Stock SGA Lineas Pedido " + mapeo.getIdCodigoVenta(), mapeo.getIdCodigoVenta());
            		getUI().addWindow(vt);
            	}
            	else if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("pdf"))
            	{
            		MapeoPedidosVentas mapeo = (MapeoPedidosVentas) event.getItemId();
            		
	    			boolean generado = traerPedidoSeleccionado(mapeo);
	    			
	    			if (generado)
	    			{
	    				generacionPdf(mapeo,true);
	    			}
					else
					{
						Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoVenta().toString() + " e insertarlo en la plataforma.");
					}
            	}
            }
        });
    }
    private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoPedidosVentas) {
                MapeoPedidosVentas progRow = (MapeoPedidosVentas)bean;
                // The actual description text is depending on the application
                if ("exis".equals(cell.getPropertyId()))
                {
                	descriptionText = "Comprobacion Stock SGA";
                }
                else if ("lineas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Lineas Pedido";
                }
                else if ("pdf".equals(cell.getPropertyId()))
                {
                	descriptionText = "Impresion pedido + expedicion";
                }
                else if ("idCodigoVenta".equals(cell.getPropertyId()))
                {
                	if (progRow.getEstado()!=null)
                	{
	                	switch (progRow.getEstado().trim())
	                	{
	                		case "Pdt":
	                		{
	                			descriptionText = "Pendiente Confirmar";
	                			break;
	                		}
	                		case "Pre":
	                		{
	                			descriptionText = "Preparado";
	                			break;
	                		}
	                		case "Ok":
	                		{
	                			descriptionText = "Carga Confirmada";
	                			break;
	                		}
	                		case "Fin":
	                		{
	                			descriptionText = "Retenido Contabilidad";
	                			break;
	                		}
	                		case "Ret":
	                		{
	                			descriptionText = "Pendiente Retrabajo";
	                			break;
	                		}
	                		case "Mat":
	                		{
	                			descriptionText = "Falta Materiales";
	                			break;
	                		}
	                		case "Tpt":
	                		{
	                			descriptionText = "Avisado Transporte";
	                			break;
	                		}
	                		case "Tpc":
	                		{
	                			descriptionText = "Avisado Cliente";
	                			break;
	                		}
	                		case "TyC":
	                		{
	                			descriptionText = "Avisado Transporte y Cliente";
	                			break;
	                		}
	                		case "SRV":
	                		{
	                			descriptionText = "Pedido Servido";
	                			break;
	                		}
	                		default:
	                		{
	                			descriptionText = "Pendiente Confirmar";
	                			break;
	                		}
	                	}
                	}
                	else
                	{
                		descriptionText = "Pendiente Confirmar";
                	}
                }
            }
        }
        return descriptionText;
    }
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()==null)
            		{
            			strEstado = "Pdt";
            		}
            		else
            		{
            			strEstado=cellReference.getValue().toString();
            		}
            	}
            	
            	if ("idCodigoVenta".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "PRE":
	        			{
	        				return "Rcell-darkgreen";
	        			}
	        			case "PDT":
	        			{
	        				return "Rcell-error";
	        			}
	        			case "OK":
	        			{
	        				return "Rcell-green";
	        			}
	        			case "RET":
	        			{
	        				return "Rcell-warning";
	        			}
	        			case "SRV":
	        			{
	        				return "Rcell-orange";
	        			}
	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	else if ("ejercicio".equals(cellReference.getPropertyId()) ||"importe".equals(cellReference.getPropertyId()) || "cliente".equals(cellReference.getPropertyId()))
            	{
            		if (strEstado.toUpperCase().trim().contentEquals("SRV"))
            			return "Rcell-orange";
            		else            			
            			return "Rcell-normal";
            	}
            	else if ("lineas".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonLineas";
            	}
            	else if ("exis".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else if ( "pdf".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonPdf";
            	}
            	else
            	{
            		if (strEstado.toUpperCase().trim().contentEquals("SRV"))
            			return "cell-orange";
            		else            			
            			return "cell-normal";

            	}
            }
        });
    }
    
    private boolean traerPedidoSeleccionado(MapeoPedidosVentas r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	boolean traido=false;
    	consultaPedidosVentasServer cps = new consultaPedidosVentasServer(CurrentUser.get());
    	
		traido = cps.traerPedidoSGAMysql(r_mapeo);
//		if (traido)
		traido = cps.traerPedidoGreensysMysql(r_mapeo);

		
    	return traido;
    }
    
    private boolean actualizarPedidoEnviado(MapeoPedidosVentas r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaPedidosVentasServer cps = new consultaPedidosVentasServer(CurrentUser.get());
    	boolean traido = cps.actualizarCabeceraGreensys(r_mapeo);
    	return traido;
    }

    private Field<?> getComboBox(String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();
		ArrayList<String> valores = new ArrayList<String>();
		
		consultaEstadoPedidosServer cep = new consultaEstadoPedidosServer(CurrentUser.get());
		valores = cep.datosOpcionesGlobal();
		
		IndexedContainer container = new IndexedContainer(valores);
		comboBox.setContainerDataSource(container);
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
//		comboBox.setStyleName("blanco");
		return comboBox;
	}
    
	private Field<?> getFecha() {
		TextField fecha = new TextField();		
		fecha.setStyleName("areablanca");
		fecha.addValidator(new Validator() {
			
			@Override
			public void validate(Object value) throws InvalidValueException {
				java.util.Date v;
				try{
					v = RutinasFechas.conversionDeString((String)value);
				}catch (Exception e){
					throw new InvalidValueException("Debes introducir una fecha");
				}
			}
		});
		return fecha;
	}
    
    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	

    	super.doEditItem();
    }
  
    private boolean generacionPdf(MapeoPedidosVentas r_mapeo, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaPedidosVentasServer cps = new consultaPedidosVentasServer(CurrentUser.get());
    	
    	pdfGenerado = cps.generarInformeSGA(r_mapeo.getIdCodigoVenta(),r_mapeo.getIdCodigoVenta());
    	
    	if (pdfGenerado!=null)
    	{
    		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
    	}
		
		pdfGenerado = null;
    	return true;
    }

	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };
    public void calcularTotal() {
    	Long totalC = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Integer) item.getItemProperty("importe").getValue();
        	totalC += valor.longValue();
        }
        
        footer.getCell("nombre").setText("Totales ");
		footer.getCell("importe").setText(RutinasNumericas.formatearDouble(totalC.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("importe").setStyleName("Rcell-pie");
		

	}
}
