package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;

public class MapeoLineasPedidosSGA
{
	private String articulo;
	private String descripcion;
	private String gtin;
	private String lpn;
	private String estado;
	private String ubic;
	private String pick;

	private String idCodigoVenta;
	private Integer idPed;
	private Integer unidades_srv;
	private Double unidades;

	public MapeoLineasPedidosSGA()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getGtin() {
		return gtin;
	}

	public String getLpn() {
		return lpn;
	}

	public String getEstado() {
		return estado;
	}

	public String getUbic() {
		return ubic;
	}

	public String getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public Integer getUnidades_srv() {
		return unidades_srv;
	}

	public Double getUnidades() {
		return unidades;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public void setLpn(String lpn) {
		this.lpn = lpn;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public void setUbic(String ubic) {
		this.ubic = ubic;
	}

	public void setIdCodigoVenta(String idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}

	public void setUnidades_srv(Integer unidades_srv) {
		this.unidades_srv = unidades_srv;
	}

	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}

	public Integer getIdPed() {
		return idPed;
	}

	public void setIdPed(Integer idPed) {
		this.idPed = idPed;
	}

	public String getPick() {
		return pick;
	}

	public void setPick(String pick) {
		this.pick = pick;
	}

}