package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoPedidosVentas extends MapeoGlobal
{
    private Integer idCodigoVenta;
    private Integer ejercicio;
    private Integer almacen;
    private Integer cliente;
    private Integer importe;

    private String clave_tabla;
	private String documento;
	private String serie;
	private String nombre;
	private String moneda;
	private String pais;
	private String estado;
	private String plazoEntrega;
	private String referencia;
	private String ordenFabric;
	private String exis = " ";
	private String lineas = " ";
	private String pdf = " ";
	
	private Date fecha;
	private Date fechaCliente;
	

	public MapeoPedidosVentas()
	{
	}

	public Integer getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public void setIdCodigoVenta(Integer idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}

	public String getClave_tabla() {
		return clave_tabla;
	}

	public void setClave_tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public Integer getCliente() {
		return cliente;
	}

	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return RutinasFechas.convertirDateToString(fecha);
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
		
	}
	
	public String getPlazoEntrega() {
//		return plazoEntrega;
//		return RutinasFechas.convertirDateToString(plazoEntrega);
		try {
			return RutinasFechas.convertirDeFecha(plazoEntrega);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setPlazoEntrega(String plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}

	public String getFechaCliente() {
		if (fechaCliente==null)
		{
			return "";
		}
		else
		{
			return RutinasFechas.convertirDateToString(fechaCliente);
		}
	}

	public void setFechaCliente(Date fechaCliente) {
		this.fechaCliente = fechaCliente;
	}

	public String getLineas() {
		return lineas;
	}

	public void setLineas(String lineas) {
		this.lineas = lineas;
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Integer getImporte() {
		return importe;
	}

	public void setImporte(Integer importe) {
		this.importe = importe;
	}

	public String getOrdenFabric() {
		return ordenFabric;
	}

	public void setOrdenFabric(String ordenFabric) {
		this.ordenFabric = ordenFabric;
	}

	public String getExis() {
		return exis;
	}

	public void setExis(String exis) {
		this.exis = exis;
	}


}