package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;

import java.util.Date;

public class MapeoLineasPedidosVentas
{
	
    private Integer ejercicio;
    private Integer cliente;
    private Integer idCodigoVenta;
    private Integer unidades;
    private Integer unidades_serv;
    private Integer unidades_pdte;
    private Integer num_linea;
    private Integer stock_queda;
    private String descripcionPalet;
    private String referencia;
    private String almacen;
    private String finalidad;
	private String nombreCliente;
	private String pais;
	private String documento;
	private String articulo;
	private String descripcion;
	private Date plazoEntrega;
	private Date fechaDocumento;
	
	

	public MapeoLineasPedidosVentas()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public Date getPlazoEntrega() {
		return plazoEntrega;
	}

	public void setPlazoEntrega(Date plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}

	public Integer getCliente() {
		return cliente;
	}

	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public void setIdCodigoVenta(Integer idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public Integer getUnidades_serv() {
		return unidades_serv;
	}

	public void setUnidades_serv(Integer unidades_serv) {
		this.unidades_serv = unidades_serv;
	}

	public Integer getUnidades_pdte() {
		return unidades_pdte;
	}

	public void setUnidades_pdte(Integer unidades_pdte) {
		this.unidades_pdte = unidades_pdte;
	}

	public Integer getStock_queda() {
		return stock_queda;
	}

	public void setStock_queda(Integer stock_queda) {
		this.stock_queda = stock_queda;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	public String getDescripcionPalet() {
		return descripcionPalet;
	}

	public void setDescripcionPalet(String descripcionPalet) {
		this.descripcionPalet = descripcionPalet;
	}

	public String getFinalidad() {
		return finalidad;
	}

	public void setFinalidad(String finalidad) {
		this.finalidad = finalidad;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public Integer getNum_linea() {
		return num_linea;
	}

	public void setNum_linea(Integer num_linea) {
		this.num_linea = num_linea;
	}
}