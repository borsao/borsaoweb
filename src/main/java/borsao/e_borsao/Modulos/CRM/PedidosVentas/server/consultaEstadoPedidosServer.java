package borsao.e_borsao.Modulos.CRM.PedidosVentas.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;

public class consultaEstadoPedidosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEstadoPedidosServer instance;
	
	
	public consultaEstadoPedidosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEstadoPedidosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEstadoPedidosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<String> datosOpcionesGlobal()
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<String> vector = null;
//		MapeoPedidosVentas mapeo = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.finalidad fin_fin ");
        
        cadenaSQL.append(" FROM e_finali a");
     	
     	
		try
		{
			if (this.con==null)
			{
				this.con= this.conManager.establecerConexionGestion();			
				this.cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			}
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<String>();
			
			while(rsOpcion.next())
			{
				vector.add(rsOpcion.getString("fin_fin"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		
		return vector;
	}

	@Override
	public String semaforos() 
	{
		return null;
	}
	
}