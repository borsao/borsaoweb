package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoPedidosSGA extends MapeoGlobal
{
    private String idCodigoVenta;
    private Integer idPed;
    private String pickList;
    private String estado;
	private Date fecha;
	

	public MapeoPedidosSGA()
	{
	}

	public String getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public void setIdCodigoVenta(String idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}
	
	public void setPickList(String idPickList) {
		this.pickList = idPickList;
	}

	public String getPickList() {
		return pickList;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return RutinasFechas.convertirDateToString(fecha);
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
		
	}

	public Integer getIdPed() {
		return idPed;
	}

	public void setIdPed(Integer idPed) {
		this.idPed = idPed;
	}
}