package borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoPrimerPlazo extends MapeoGlobal
{
	private Date primerPlazo = null;
	private String nombreCliente = null;

	public MapeoPrimerPlazo()
	{
	}

	public Date getPrimerPlazo() {
		return primerPlazo;
	}

	public void setPrimerPlazo(Date primerPlazo) {
		this.primerPlazo = primerPlazo;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

}