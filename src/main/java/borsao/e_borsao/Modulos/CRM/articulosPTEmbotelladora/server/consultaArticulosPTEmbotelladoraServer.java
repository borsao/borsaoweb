package borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.modelo.MapeoArticulosEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosPTEmbotelladoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosPTEmbotelladoraServer instance;
	
	public consultaArticulosPTEmbotelladoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosPTEmbotelladoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosPTEmbotelladoraServer(r_usuario);			
		}
		return instance;
	}
	
	public HashMap<String, String> datosOpcionesGlobal(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		HashMap<String, String> hasCols = null;
		
		cadenaSQL.append(" SELECT art.articulo pte_art, ");
		cadenaSQL.append(" art.nombreMostrar pte_ali, ");
		cadenaSQL.append(" con.articulo con_art, ");
		cadenaSQL.append(" art.area pte_are ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" LEFT OUTER JOIN prd_ptemb con on con.calculaCon = art.articulo and art.area = con.area ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			cadenaSQL.append(" order by art.orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hasCols = new HashMap<String, String>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				if (rsOpcion.getString("con_art")!=null && rsOpcion.getString("con_art").length()>0)
				{
					hasCols.put(rsOpcion.getString("con_art"), rsOpcion.getString("pte_ali"));
				}
				else
				{
					hasCols.put(rsOpcion.getString("pte_art"), rsOpcion.getString("pte_ali"));
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hasCols;
	}
	
	public HashMap<String, String> datosColumnasContenido(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		HashMap<String, String> hasCols = null;
		
		cadenaSQL.append(" SELECT art.articulo pte_art, ");
		cadenaSQL.append(" art.nombreMostrar pte_ali, ");
		cadenaSQL.append(" con.articulo con_art, ");
		cadenaSQL.append(" art.area pte_are ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" LEFT OUTER JOIN prd_ptemb con on con.calculaCon = art.articulo and art.area = con.area ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			cadenaSQL.append(" order by art.orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hasCols = new HashMap<String, String>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				if (rsOpcion.getString("con_art")!=null && rsOpcion.getString("con_art").length()>0)
				{
					hasCols.put("c-" + rsOpcion.getString("pte_ali"),rsOpcion.getString("con_art"));
				}
				else
				{
					hasCols.put(rsOpcion.getString("pte_ali"), rsOpcion.getString("pte_art"));
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hasCols;
	}

	public HashMap<String, String> datosColumnasAgrupacion(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		HashMap<String, String> hasCols = null;
		
		cadenaSQL.append(" SELECT art.nombreMostrar pte_ali, ");
		cadenaSQL.append(" art.gama pte_gam ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			cadenaSQL.append(" order by art.gama asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hasCols = new HashMap<String, String>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				hasCols.put(rsOpcion.getString("pte_ali"), rsOpcion.getString("pte_gam"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hasCols;
	}

	public Integer datosAgrupacion(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(distinct art.gama) pte_gam ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				return rsOpcion.getInt("pte_gam");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public ArrayList<String> datosAliasGlobal(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		ArrayList<String> listCols = null;
		
		cadenaSQL.append(" SELECT distinct art.nombreMostrar pte_ali ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			cadenaSQL.append(" order by art.orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			listCols = new ArrayList<String>();
				
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				listCols.add(rsOpcion.getString("pte_ali"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return listCols;
	}

	public ArrayList<String> datosAliasHeraderGlobal(String r_area)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		ArrayList<String> listCols = null;
		
		cadenaSQL.append(" SELECT distinct art.nombreMostrar pte_ali ");
     	cadenaSQL.append(" FROM prd_ptemb art ");
     	cadenaSQL.append(" where art.area ='" + r_area + "' ");
		try
		{
			cadenaSQL.append(" order by art.orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			listCols = new ArrayList<String>();
				
			while(rsOpcion.next())
			{
				/*
				 * recojo titulos y articulos
				 */
				listCols.add(rsOpcion.getString("pte_ali"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return listCols;
	}

	public ArrayList<MapeoArticulosEmbotelladora> datosOpcionesGlobal(MapeoArticulosEmbotelladora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulosEmbotelladora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_ptemb.articulo pte_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo pte_des, ");
		cadenaSQL.append(" prd_ptemb.nombreMostrar pte_ali, ");
		cadenaSQL.append(" prd_ptemb.calculaCon pte_con, ");
		cadenaSQL.append(" prd_ptemb.area pte_are, ");
		cadenaSQL.append(" prd_ptemb.orden pte_ord, ");
		cadenaSQL.append(" prd_ptemb.gama pte_gam ");
     	cadenaSQL.append(" FROM prd_ptemb ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_ptemb.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptemb.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptemb.area = '" + r_mapeo.getArea() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptemb.orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosEmbotelladora>();
			
			while(rsOpcion.next())
			{
				MapeoArticulosEmbotelladora mapeoArticulosEmbotelladora = new MapeoArticulosEmbotelladora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosEmbotelladora.setArticulo(rsOpcion.getString("pte_art"));
				mapeoArticulosEmbotelladora.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pte_des")));
				mapeoArticulosEmbotelladora.setAlias(rsOpcion.getString("pte_ali"));
				mapeoArticulosEmbotelladora.setCalculaCon(rsOpcion.getString("pte_con"));
				mapeoArticulosEmbotelladora.setArea(rsOpcion.getString("pte_are"));
				mapeoArticulosEmbotelladora.setOrden(rsOpcion.getInt("pte_ord"));
				mapeoArticulosEmbotelladora.setGama(rsOpcion.getInt("pte_gam"));
				vector.add(mapeoArticulosEmbotelladora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoArticulosEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_ptemb ( ");
			cadenaSQL.append(" prd_ptemb.area, ");
			cadenaSQL.append(" prd_ptemb.articulo, ");
			cadenaSQL.append(" prd_ptemb.nombreMostrar, ");
			cadenaSQL.append(" prd_ptemb.calculaCon, ");
			cadenaSQL.append(" prd_ptemb.orden, ");
			cadenaSQL.append(" prd_ptemb.gama ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getAlias()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getAlias());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCalculaCon()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCalculaCon());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getGama()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getGama());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoArticulosEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_ptemb  SET ");
			cadenaSQL.append(" prd_ptemb.area =?, ");
			cadenaSQL.append(" prd_ptemb.nombreMostrar=?, ");
			cadenaSQL.append(" prd_ptemb.calculaCon=?, ");
			cadenaSQL.append(" prd_ptemb.orden=?, ");
			cadenaSQL.append(" prd_ptemb.gama=? ");
    		cadenaSQL.append(" WHERE prd_ptemb.articulo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getAlias()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getAlias());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCalculaCon()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCalculaCon());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getGama()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getGama());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoArticulosEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_ptemb ");            
			cadenaSQL.append(" WHERE prd_ptemb.articulo = ?"); 
			cadenaSQL.append(" and prd_ptemb.area = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.setString(2, r_mapeo.getArea());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
}