package borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoArticulosEmbotelladora extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String alias;
	private String area;
	private String calculaCon;
	private Integer orden;
	private Integer gama;
	
	public MapeoArticulosEmbotelladora()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setAlias("");
		this.setArea("");
		this.setCalculaCon("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCalculaCon() {
		return calculaCon;
	}

	public void setCalculaCon(String calculaCon) {
		this.calculaCon = calculaCon;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Integer getGama() {
		return gama;
	}

	public void setGama(Integer gama) {
		this.gama = gama;
	}
}