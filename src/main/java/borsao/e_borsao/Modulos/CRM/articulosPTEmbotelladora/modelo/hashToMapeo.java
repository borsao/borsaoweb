package borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulosEmbotelladora mapeo = null;
	 
	 public MapeoArticulosEmbotelladora convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEmbotelladora();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setAlias(r_hash.get("alias"));
		 this.mapeo.setCalculaCon(r_hash.get("calculaCon"));
		 this.mapeo.setArea(r_hash.get("area"));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(r_hash.get("orden")));
		 if (r_hash.get("gama")!=null && r_hash.get("gama").length()>0) this.mapeo.setGama(new Integer(r_hash.get("gama")));
		 return mapeo;		 
	 }
	 
	 public MapeoArticulosEmbotelladora convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEmbotelladora();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 this.mapeo.setAlias(r_hash.get("alias"));
		 this.mapeo.setCalculaCon(r_hash.get("calculaCon"));
		 
		 this.mapeo.setArea(r_hash.get("area"));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(r_hash.get("orden")));
		 if (r_hash.get("gama")!=null && r_hash.get("gama").length()>0) this.mapeo.setGama(new Integer(r_hash.get("gama")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulosEmbotelladora r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("alias", this.mapeo.getAlias());
		 this.hash.put("calculaCon", this.mapeo.getCalculaCon());
		 this.hash.put("area", this.mapeo.getArea());
		 if (this.mapeo.getOrden()!=null) this.hash.put("orden", String.valueOf(this.mapeo.getOrden().intValue()));
		 if (this.mapeo.getGama()!=null) this.hash.put("gama", String.valueOf(this.mapeo.getGama().intValue()));
		 return hash;		 
	 }
}