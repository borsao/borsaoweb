package borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionMail;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.view.FacturasVentasNacionalView;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoFacturasVentas;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.server.consultaFacturasVentasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean enviadoMail=false;
	private FacturasVentasNacionalView view = null;
    public OpcionesGrid(ArrayList<MapeoFacturasVentas> r_vector, FacturasVentasNacionalView r_view) 
    {
        
        this.vector=r_vector;
        this.view = r_view;
        
		this.asignarTitulo("Facturas de Venta " + this.view.cmbArea.getValue().toString());
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("enviar");
    	this.camposNoFiltrar.add("enviado");
    	this.camposNoFiltrar.add("boton");
    	this.camposNoFiltrar.add("pdf");
    	this.camposNoFiltrar.add("marcar");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoFacturasVentas.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(5);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("enviar", "enviado", "tipoFactura", "boton", "pdf" , "marcar", "ejercicio","clave_tabla","documento","idCodigoFactura","fecha", "cliente", "nombre", "nif", "fax", "mail","mail2","mail3");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "75");
    	this.widthFiltros.put("clave_tabla", "75");
    	this.widthFiltros.put("documento", "75");
    	this.widthFiltros.put("idCodigoFactura", "100");
    	this.widthFiltros.put("fecha", "100");
    	this.widthFiltros.put("fax", "125");
    	this.widthFiltros.put("nif", "125");
    	this.widthFiltros.put("cliente", "100");
    	this.widthFiltros.put("nombre", "200");
    	this.widthFiltros.put("mail", "200");
    	this.widthFiltros.put("mail2", "200");
    	this.widthFiltros.put("mail3", "200");
    	
    	this.getColumn("boton").setHeaderCaption("");
    	this.getColumn("boton").setSortable(false);
    	this.getColumn("boton").setWidth(new Double(50));
    	this.getColumn("pdf").setHeaderCaption("");
    	this.getColumn("pdf").setSortable(false);
    	this.getColumn("pdf").setWidth(new Double(50));
    	this.getColumn("marcar").setHeaderCaption("");
    	this.getColumn("marcar").setSortable(false);
    	this.getColumn("marcar").setWidth(new Double(50));
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(100));
    	this.getColumn("clave_tabla").setHeaderCaption("Clave Tabla");
    	this.getColumn("clave_tabla").setWidth(new Double(100));
    	this.getColumn("clave_tabla").setHidden(true);
    	this.getColumn("documento").setHeaderCaption("Documento");
    	this.getColumn("documento").setWidth(new Double(100));
    	this.getColumn("cliente").setHeaderCaption("Cliente");
    	this.getColumn("cliente").setWidth(new Double(125));
    	this.getColumn("idCodigoFactura").setHeaderCaption("Codigo");
    	this.getColumn("idCodigoFactura").setWidth(new Double(125));
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(125));
    	this.getColumn("nombre").setHeaderCaption("Nombre Cliente");
    	this.getColumn("mail").setHeaderCaption("PARA");
    	this.getColumn("mail2").setHeaderCaption("CC");
    	this.getColumn("mail3").setHeaderCaption("CC");
    	this.getColumn("nif").setHeaderCaption("CIF");
    	this.getColumn("nif").setWidth(new Double(150));

    	this.getColumn("fax").setHeaderCaption("FAX");
    	this.getColumn("fax").setWidth(new Double(150));
    	
    	this.getColumn("direccion").setHidden(true);
    	this.getColumn("cp").setHidden(true);
    	this.getColumn("poblacion").setHidden(true);
    	this.getColumn("provincia").setHidden(true);
//    	this.getColumn("plazoEntrega").setHidden(true);
    	this.getColumn("formaPago").setHidden(true);
    	this.getColumn("pais").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("moneda").setHidden(true);
    	this.getColumn("enviar").setHidden(true);
    	this.getColumn("enviado").setHidden(true);
    	this.getColumn("tipoFactura").setHidden(true);
    	this.getColumn("nombreBanco").setHidden(true);
    	this.getColumn("iban").setHidden(true);
    	this.getColumn("swift").setHidden(true);
    	this.getColumn("vencimiento").setHidden(true);
    	
    	this.getColumn("diasVencimiento").setHidden(true);
    	this.getColumn("diasPago1").setHidden(true);
    	this.getColumn("diasPago2").setHidden(true);
    	this.getColumn("diasPago3").setHidden(true);
	}
	
    
	public void envioFactura(MapeoFacturasVentas r_mapeo) 
    {
		boolean generado = traerFacturaSeleccionada(r_mapeo);
		if (r_mapeo.getMail()!=null && r_mapeo.getMail().length()>0)
		{
			if (generado)
			{
				enviadoMail=generacionPdf(r_mapeo, true, true);
				if (enviadoMail)
				{
					r_mapeo.setEnviado("S");
					boolean send = actualizarFacturaEnviada(r_mapeo);
					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar la factura como enviada en GREENsys.");
					refresh(r_mapeo, r_mapeo);
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("Error al recoger los datos de la factura de GREENsys : " + r_mapeo.getIdCodigoFactura().toString() + " e insertarlo en la plataforma.");
			}
    	}
    }

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	
            	if (event.getPropertyId().toString().equals("boton"))
            	{
            		MapeoFacturasVentas mapeo = (MapeoFacturasVentas) event.getItemId();
            		if ((mapeo.getCliente()==6662 && mapeo.getTipoFactura().equals("M")))
            		{
            			/*
            			 * Ventana solicitando mail y almacenarlo en la factura que guardamos en el mysql
            			 */
            			
        	    		PeticionMail vm = null;
        	    		vm= new PeticionMail((OpcionesGrid) event.getSource(), mapeo, "Peticion Mail Envío Factura");
        	    		getUI().addWindow(vm);
 
            		}
            		else
            		{
		    			boolean generado = traerFacturaSeleccionada(mapeo);
		    			if ((mapeo.getTipoFactura().equals("M")) && ((mapeo.getMail()!=null && mapeo.getMail().length()>0) || (mapeo.getMail2()!=null && mapeo.getMail2().length()>0) || (mapeo.getMail3()!=null && mapeo.getMail3().length()>0)))
		    			{
	    	    			if (generado)
	    	    			{
	    	    				enviadoMail=generacionPdf(mapeo, true, true);
	    	    				if (enviadoMail)
	    	    				{
	    	    					mapeo.setEnviado("S");
	    	    					boolean send = actualizarFacturaEnviada(mapeo);
	    	    					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar la factura como enviada en GREENsys.");
	    	    					refresh(mapeo, mapeo);
	    	    				}
	    	    			}
							else
							{
								Notificaciones.getInstance().mensajeError("Error al recoger los datos de la factura de GREENsys : " + mapeo.getIdCodigoFactura().toString() + " e insertarlo en la plataforma.");
							}
	        	    	}
            		}
            	}
            	else if (event.getPropertyId().toString().equals("pdf"))
            	{
            		MapeoFacturasVentas mapeo = (MapeoFacturasVentas) event.getItemId();        		
        			boolean generado = traerFacturaSeleccionada(mapeo);
	    			if (generado)
	    			{
	    				generacionPdf(mapeo,false,true);
	    				if (mapeo.getTipoFactura().equals("O")) 
    					{
	    					mapeo.setEnviado("S");
	    					boolean send = actualizarFacturaEnviada(mapeo);
	    					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar la factura como enviada en GREENsys.");
	    					refresh(mapeo, mapeo);
    					}
	    			}
					else
					{
						Notificaciones.getInstance().mensajeError("Error al recoger los datos de la factura de GREENsys : " + mapeo.getIdCodigoFactura().toString() + " e insertarlo en la plataforma.");
					}
            	}
            	else if (event.getPropertyId().toString().equals("marcar"))
            	{
            		MapeoFacturasVentas mapeo = (MapeoFacturasVentas) event.getItemId();
            		mapeo.setEnviado("S");
					boolean send = actualizarFacturaEnviada(mapeo);

    				if (!send) Notificaciones.getInstance().mensajeError("Error al marcar la factura como enviada en GREENsys.");
    				refresh(mapeo, mapeo);
            	}
            }
        });
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoFacturasVentas) {
                MapeoFacturasVentas progRow = (MapeoFacturasVentas)bean;
                // The actual description text is depending on the application
                if ("pdf".equals(cell.getPropertyId()))
                {
                	descriptionText = "Ver factura en pdf";
                }
                else if ("marcar".equals(cell.getPropertyId()))
                {
                	descriptionText = "Marcar factura como enviada";
                }
                else if ("boton".equals(cell.getPropertyId()))
                {
                	descriptionText = "Envio Factura Mail";
                }
            }
        }
        return descriptionText;
    }
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            boolean enviar = true;
            String enviado = "";
            String tf = "";
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("enviar".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue().toString().equals("Enviar"))
	            	{
	            		enviar=true;
	            	}
	            	else
	            	{
	            		enviar=false;
	            	}
            	}

            	if ("enviado".equals(cellReference.getPropertyId()))
            	{
            		enviado=cellReference.getValue().toString();
            	}
            	
            	if ("tipoFactura".equals(cellReference.getPropertyId()))
            	{
            		tf=cellReference.getValue().toString();
            	}
            	
            	if ("idCodigoFactura".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()) || "cliente".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ("boton".equals(cellReference.getPropertyId()))
            	{
            		if (enviar)
            		{
            			if (enviado.equals("S"))
            			{
            				return "nativebuttonGreen";
            			}
            			else if (enviado==null || enviado.equals("") || enviado.length()==0 || enviado.equals("N"))
            			{
            				return "nativebutton";
            			}
            			else
            			{
            				return "nativebuttonNo";
            			}
            		}
            		else
            		{
            			return "nativebuttonOrd";
            		}
            	}
            	else if ( "pdf".equals(cellReference.getPropertyId()))
            	{
            		if (tf.equals("O"))
            		{
	            		if (enviado.equals("S"))
	        			{
	            			return "nativebuttonPdfOk";
	        			}
	            		else if (enviado==null || enviado.equals("") || enviado.length()==0 || enviado.equals("N"))
	        			{
	            			return "nativebuttonPdfPdte";
	        			}
	            		else
	            		{
	            			return "nativebuttonNo";
	            		}
            		}
            		else
        			{
        				return "nativebuttonPdf";
        			}
            	}
            	else if ( "marcar".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonCheck";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    private boolean traerFacturaSeleccionada(MapeoFacturasVentas r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaFacturasVentasServer cps = new consultaFacturasVentasServer(CurrentUser.get());
    	boolean traido = cps.traerFacturaGreensysMysql(r_mapeo);
    	return traido;
    }
    
    private boolean actualizarFacturaEnviada(MapeoFacturasVentas r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaFacturasVentasServer cps = new consultaFacturasVentasServer(CurrentUser.get());
    	boolean traido = cps.actualizarCabeceraGreensys(r_mapeo);
    	return traido;
    }
    
    private boolean generacionPdf(MapeoFacturasVentas r_mapeo, boolean r_EnviarMail, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	String cuentaDestino = null;
    	String cuentaCopia = null;
    	String cuentaCopia2 = null;
    	String asunto = null;
    	String cuerpo = null;
    	HashMap adjuntos = null;
    	boolean envio = false;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaFacturasVentasServer cps = new consultaFacturasVentasServer(CurrentUser.get());
    	pdfGenerado = cps.generarInforme(r_mapeo.getIdCodigo(),r_mapeo.getIdCodigoFactura(),r_mapeo.getDocumento(),r_mapeo.getCliente());
    	
		if (r_EnviarMail)
		{    			
			cuentaDestino = r_mapeo.getMail();
			cuentaCopia = r_mapeo.getMail2();
			cuentaCopia2 = r_mapeo.getMail3();
			
			asunto = "Envío Factura Venta " + r_mapeo.getIdCodigoFactura().toString();
			cuerpo = "Adjunto enviamos la Factura: " + r_mapeo.getIdCodigoFactura().toString();
			
			adjuntos = new HashMap();
			
			adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
			adjuntos.put("nombre", "factura-" + r_mapeo.getIdCodigoFactura().toString() + ".pdf");
			
    		envio=ClienteMail.getInstance().envioMail(cuentaDestino, cuentaCopia, cuentaCopia2, asunto , cuerpo, adjuntos, r_eliminar);
    		return envio;
		}
		else
		{
			String impresora = this.view.cmbPrinter.getValue().toString();
			
			if (impresora.toUpperCase().equals("PANTALLA"))
			{
				RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);			
			}
			else
			{
				String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;			
				RutinasFicheros.imprimir(archivo, impresora);
			    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
			}
		}
		
		pdfGenerado = null;
    	cuentaDestino = null;
    	cuentaCopia = null;
    	asunto = null;
    	cuerpo = null;
    	adjuntos = null;
    	
    	return envio;
    }

	@Override
	public void calcularTotal() {
		
	}
}

