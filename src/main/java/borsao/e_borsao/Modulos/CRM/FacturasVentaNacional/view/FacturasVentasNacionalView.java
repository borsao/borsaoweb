package borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.CRM.FacturasVentaNacional.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.modelo.MapeoFacturasVentas;
import borsao.e_borsao.Modulos.CRM.FacturasVentas.server.consultaFacturasVentasServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class FacturasVentasNacionalView extends GridViewRefresh {

	public static final String VIEW_NAME = "Facturas Venta";
	public ComboBox cmbPrinter = null;
	
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private CheckBox pendientes = null;
	protected ComboBox cmbEjercicios;
	public ComboBox cmbArea;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public FacturasVentasNacionalView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
    	
		this.pendientes=new CheckBox("Ver sólo pendientes de enviar.");
		this.pendientes.setValue(true);
		
		this.cmbArea=new ComboBox("Area");
		this.cmbArea.addItem("Nacional");
		this.cmbArea.addItem("Tienda");
		this.cmbArea.addItem("Exportacion");
		this.cmbArea.setValue("Nacional");
		
		this.cmbArea.setInvalidAllowed(false);
		this.cmbArea.setNewItemsAllowed(false);
		this.cmbArea.setNullSelectionAllowed(false);
		this.cmbArea.setWidth("200px");
		this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cmbPrinter=new ComboBox("Impresora");
		this.cargarCombo();
		this.cmbPrinter.setInvalidAllowed(false);
		this.cmbPrinter.setNewItemsAllowed(false);
		this.cmbPrinter.setNullSelectionAllowed(false);
		this.cmbPrinter.setWidth("200px");
		this.cmbPrinter.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cmbEjercicios=new ComboBox("Ejercicio");
		this.cargarComboEjercicio();
		this.cmbEjercicios.setInvalidAllowed(false);
		this.cmbEjercicios.setNewItemsAllowed(false);
		this.cmbEjercicios.setNullSelectionAllowed(false);
		this.cmbEjercicios.setWidth("200px");
		this.cmbEjercicios.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cabLayout.addComponent(this.cmbArea);
		this.cabLayout.addComponent(this.cmbEjercicios);
		this.cabLayout.addComponent(this.cmbPrinter);
		this.cabLayout.addComponent(this.pendientes);
		this.cabLayout.setComponentAlignment(pendientes, Alignment.BOTTOM_LEFT);
		
		this.cargarListeners();

    	this.generarGrid(null);
    }

    private void cargarListeners()
    {
    	this.pendientes.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);

					generarGrid(null);
					
				}
				
			}
		});

    	this.cmbEjercicios.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);

					generarGrid(null);
					
				}
				
			}
		});

    	this.cmbArea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);

					generarGrid(null);
					
				}
				
			}
		});
    }
    
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	String documento = null;
    	String cliente = null;
    	ArrayList<MapeoFacturasVentas> r_vector=null;
    	
    	consultaFacturasVentasServer cus = new consultaFacturasVentasServer(CurrentUser.get());
    	
    	if (this.cmbArea.getValue().equals("Nacional")) documento = "N";
    	if (this.cmbArea.getValue().equals("Exportacion")) documento = "E";
    	if (this.cmbArea.getValue().equals("Tienda"))
    	{
    		documento = "W";
    	}
    	
    	r_vector=cus.datosOpcionesGlobal(documento, cliente, this.pendientes.getValue(), this.cmbEjercicios.getValue().toString());
    	
    	grid = new OpcionesGrid(r_vector, this);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }
    
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicios.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicios.setValue(vector.get(0).toString());
	}

    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
