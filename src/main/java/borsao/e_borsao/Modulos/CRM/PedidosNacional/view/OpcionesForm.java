package borsao.e_borsao.Modulos.CRM.PedidosNacional.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.GridDinamico;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo.MapeoPedidosNacional;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.server.consultaPedidosNacionalServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoPedidosNacional mapeoPedidosNacional = null;
	 
	 private PreparacionPedidosView uw= null;
	 public MapeoPedidosNacional mapeoModificado = null;
	 private BeanFieldGroup<MapeoPedidosNacional> fieldGroup;
	 
	 public OpcionesForm(PreparacionPedidosView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoPedidosNacional>(MapeoPedidosNacional.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeoPedidosNacional = (MapeoPedidosNacional) uw.maq.grid.getSelectedRow();
                eliminar(mapeoPedidosNacional);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.maq.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.maq.grid);
		 				uw.maq.grid.setHayGrid(false);
		 				uw.maq.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeoPedidosNacional = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crear(mapeoPedidosNacional);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 				
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
//		 				MapeoPedidosNacional mapeoPedidos_orig = (MapeoPedidosNacional) uw.maq.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeoPedidosNacional= hm.convertirHashAMapeo(opcionesEscogidas);
		 				modificar(mapeoPedidosNacional,null);
		 				hm=null;
		 				
			           	removeStyleName("visible");
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 				if (((GridDinamico) uw.maq.grid)!=null)
	 				{
	 					uw.maq.grid.setHayGrid(true);
	 				}
	 			}
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		
		this.shipment.focus();
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editar(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		
		if (this.codigoPedido.getValue()!=null && this.codigoPedido.getValue().length()>0) 
		{
			opcionesEscogidas.put("codigo", RutinasCadenas.quitarPuntoMiles(this.codigoPedido.getValue().toString()));
		}
		else
		{
			opcionesEscogidas.put("codigo", "");
		}
		if (this.shipment.getValue()!=null && this.shipment.getValue().length()>0) 
		{
			opcionesEscogidas.put("shipment", this.shipment.getValue());
		}
		else
		{
			opcionesEscogidas.put("shipment", "");
		}
		if (this.observaciones1.getValue()!=null && this.observaciones1.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones1.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		
		if (this.codigoPedido.getValue()!=null && this.codigoPedido.getValue().length()>0) 
		{
			opcionesEscogidas.put("codigo", RutinasCadenas.quitarPuntoMiles(this.codigoPedido.getValue().toString()));
		}
		else
		{
			opcionesEscogidas.put("codigo", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editar(null);
		}
	}
	
	public void editar(MapeoPedidosNacional r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoPedidosNacional();
		}
		if (!this.uw.modificando) creacion=true; 
		if (this.uw.modificando) this.btnEliminar.setEnabled(true);
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoPedidosNacional>(r_mapeo));
		
		if (r_mapeo!=null)
		{
//			this.codigoPedido.setValue(r_mapeo.getCodigoPedido().toString());
//			this.shipment.setValue(r_mapeo.getShipment());
//			this.observaciones.setValue(r_mapeo.getObservaciones1());
		}
		
		this.uw.modificando=false;
	}
	
	public void eliminar(MapeoPedidosNacional r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		consultaPedidosNacionalServer cus = new consultaPedidosNacionalServer(CurrentUser.get());
		cus.eliminar(r_mapeo);
		
		if (uw.maq.grid!=null && uw.maq.grid.isHayGrid())
		{
			uw.maq.grid.removeAllColumns();			
			uw.barAndGridLayout.removeComponent(uw.maq.grid);
			uw.maq.grid.setHayGrid(false);
			uw.maq.grid=null;
			
			uw.generarGrid(null);

		}
	}
	
	public void modificar(MapeoPedidosNacional r_mapeo, MapeoPedidosNacional r_mapeo_orig)
	{
		consultaPedidosNacionalServer cus  = new consultaPedidosNacionalServer(CurrentUser.get());
		cus.guardarCambiosDatosPedidos(r_mapeo);
		
		if (uw.maq.grid!=null && uw.maq.grid.isHayGrid())
		{
			uw.maq.grid.removeAllColumns();			
			uw.barAndGridLayout.removeComponent(uw.maq.grid);
			uw.maq.grid.setHayGrid(false);
			uw.maq.grid=null;
			
			uw.generarGrid(null);

		}

		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crear(MapeoPedidosNacional r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		consultaPedidosNacionalServer cus  = new consultaPedidosNacionalServer(CurrentUser.get());
		String rdo = cus.guardaraNuevoDatosPedidos(r_mapeo);
		
		
		if (rdo== null)
		{
			if (uw.maq.grid!=null && uw.maq.grid.isHayGrid())
			{
				uw.maq.grid.removeAllColumns();			
				uw.barAndGridLayout.removeComponent(uw.maq.grid);
				uw.maq.grid.setHayGrid(false);
				uw.maq.grid=null;
				
				uw.generarGrid(null);

			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoPedidosNacional> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoPedidosNacional= item.getBean();
            if (this.mapeoPedidosNacional.getCodigoPedido()==null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    
	private boolean comprobarCamposObligatorios()
	{
		if (this.codigoPedido.getValue()==null || this.codigoPedido.getValue().toString().length()==0) return false;
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();        
	}
}
