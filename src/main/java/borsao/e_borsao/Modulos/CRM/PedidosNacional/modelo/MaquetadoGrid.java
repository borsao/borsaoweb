package borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridDinamico;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.MapeoEstructuraGridDinamico;
import borsao.e_borsao.ClasesPropias.MapeoGridDinamico;
import borsao.e_borsao.ClasesPropias.MaquetadoDinamico;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.server.consultaPedidosNacionalServer;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.view.PreparacionPedidosView;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.server.consultaArticulosPTEmbotelladoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class MaquetadoGrid extends MaquetadoDinamico 
{
	public GridDinamico grid = null;
	public IndexedContainer container =null;
	public GridViewRefresh app = null;
	private ArrayList<String> vectorCeldasPreparadas=null;
	/*
	 * este vector contendra los datos a presentar en el grid
	 */
	public ArrayList<MapeoGridDinamico> vectorDatos = null;

	/*
	 * este vector contendra toda la estructura y sus caracteristicas del grid
	 */
	public MapeoEstructuraGridDinamico mapeoEstructura = null;
	public consultaPedidosNacionalServer cis = null; 
	private String areaConsulta = null;
	private boolean estoyEnCelda = false;
	/**
	 * 			CONSTRUCTOR DE LA CLASE
	 * 
	 * 
	 * @param r_app						GridViewRefresh para poder actualizar la parte grafica 
	 * @param opcionesEscogidas			Hash que recoge opciones seleccionadas en pantalla para query de bbdd	 * 
	 * @param r_area					String	recoger el valor del area a consultar
	 * 											pretendemos que la misma parte servidora sirva para 
	 * 												- pedidos nacional
	 * 												- pedidos exportacion
	 * 												- pedidos usa
	 * @param r_subtotalesColumnas		boolean	sirve a para presentar o no subtotales por columna de agrupacion
	 */
	
	public MaquetadoGrid(GridViewRefresh r_app, HashMap<String, String> opcionesEscogidas, String r_area, boolean r_subtotalesColumnas)
	{
		this.app = r_app;
		this.areaConsulta = r_area;
    	cis = new consultaPedidosNacionalServer(CurrentUser.get());
    	consultaArticulosPTEmbotelladoraServer cpts = new consultaArticulosPTEmbotelladoraServer(CurrentUser.get());

    	/*
    	 * Si hemos escogido el area de consulta, sacamos los datos a presentar
    	 */
    	if (r_area!=null && r_area.length()>0) 
    	{
    		vectorDatos = cis.recuperarMovimientosPedidos(r_area);
    	}
    	
    	/*
    	 * Si hay datos a presentar...
    	 */
    	if (vectorDatos!=null && vectorDatos.size()>0)
    	{
    		mapeoEstructura = new MapeoEstructuraGridDinamico();
    		mapeoEstructura.setVectorTitulos(cpts.datosAliasGlobal(r_area));
    		mapeoEstructura.setVectorTitulosHeader(cpts.datosAliasHeraderGlobal(r_area));
    		mapeoEstructura.setVectorColumnas(cpts.datosColumnasContenido(r_area));
    		mapeoEstructura.setHashGamas(cpts.datosColumnasAgrupacion(r_area));
    		mapeoEstructura.setTotalAgrupaciones(cpts.datosAgrupacion(r_area));
    		this.cargarNombreAgrupaciones();
    		this.cargarEstructuraColumnasFijas();
    		
    		this.crearGrid(r_subtotalesColumnas);
    	}
    	else
    	{
    		this.grid = new GridDinamico();
    		this.grid.setHayGrid(true);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
	}

	public void crearGrid(boolean r_subtotalesColumnas)
	{
		
		this.grid = new GridDinamico((MaquetadoDinamico) this, mapeoEstructura, this.vectorDatos, r_subtotalesColumnas, false, true);
		
		this.asignarEstilos();
		
		this.grid.setConFiltro(true);
		this.grid.getColumn("Preparado").setHidden(true);
		this.grid.getColumn("Pedido").setRenderer((Renderer<?>) new NumberRenderer("%d"));
		this.grid.setFrozenColumnCount(3);		
		this.generarTotalesGrid();
		this.cargarListenersGrid();

		this.grid.setHayGrid(true);

	}
	
	private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
		this.grid.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
	private String getCellDescription(CellReference cell) 
	{
		
		String descriptionText = "";
			                
		Item file = cell.getItem();
				
		if (file.getItemProperty("Situacion").getValue().toString().contentEquals("Ppd")) descriptionText="Pre-Pedido Hecho";
		if (file.getItemProperty("Situacion").getValue().toString().contentEquals("Pre")) descriptionText="Pedido Preparado";
		if (file.getItemProperty("Situacion").getValue().toString().contentEquals("BLQ")) descriptionText="Bloqueado Servir";
				
		return descriptionText;
	}

	public void asignarEstilos()
    {
    	this.grid.widthFiltros.put("Situacion", "40");
    	this.grid.widthFiltros.put("Almacen", "40");
    	this.grid.widthFiltros.put("Cliente", "220");
    	this.grid.widthFiltros.put("Pedido", "80");
    	this.grid.widthFiltros.put("Referencia", "80");
    	this.grid.widthFiltros.put("Shipment", "80");

    	this.grid.camposFiltrar.add("Situacion");
    	this.grid.camposFiltrar.add("Almacen");
    	this.grid.camposFiltrar.add("Cliente");
    	this.grid.camposFiltrar.add("Pedido");
    	this.grid.camposFiltrar.add("Referencia");
    	this.grid.camposFiltrar.add("Shipment");
    	
		this.asignarTooltips();
		
		this.grid.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
			boolean prepedido = false;
			boolean preparado = false;
			boolean bloquear = false;
			
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Situacion".equals(cellReference.getPropertyId())) 
            	{            		
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("Ppd"))
            		{
            			prepedido=true;
            		}
            		else
            		{
            			prepedido=false;
            		}
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("Pre"))
            		{
            			preparado=true;
            		}
            		else
            		{
            			preparado=false;
            		}
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("BLQ"))
            		{
            			bloquear=true;
            		}
            		else 
            		{
            			bloquear=false;
            		}
            		
            	}
            	if ("Preparado".equals(cellReference.getPropertyId())) 
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("P")) preparado=true;
            	}
            	if ("Almacen".equals(cellReference.getPropertyId()) && bloquear)
            	{
            		return "cell-error";
            	}
            	if ("Cliente".equals(cellReference.getPropertyId()) && bloquear)
            	{
            		return "cell-error";
            	}
            	else if ("Cliente".equals(cellReference.getPropertyId())||"Referencia".equals(cellReference.getPropertyId())||"Fecha".equals(cellReference.getPropertyId()))
            	{
            		if (prepedido)
            		{
            			return "cell-green";
            		}
            		else
            		{
            			if (preparado)
            			{
            				return "cell-dgreen";
            			}
            			else
            			{
            				return "cell-normal";
            			}
            		}
            	}
            	else if ("Pedido".equals(cellReference.getPropertyId()))
            	{
            		obtenerCeldasPreparadas(new Integer(cellReference.getValue().toString()), areaConsulta);
            		
            		if (prepedido)
            		{
            			return "Rcell-greenp";
            		}
            		else
            		{
            			if (preparado)
            			{
            				return "Rcell-dgreenp";
            			}
            			else
            			{
            				return "Rcell-pointer";
            			}

            		}
            	}
            	else if (!mapeoEstructura.getHashColumnasNombresFijas().containsValue(cellReference.getPropertyId().toString()))
            	{
            		if (prepedido)
            		{
            			return "Rcell-greenp";
            		}
            		else
            		{
            			if (preparado)
            			{
            				return "Rcell-dgreenp";
            			}
            			else
            			{
            				/*
            				 * si celda preparada
            				 */
            				if (vectorCeldasPreparadas!=null && !vectorCeldasPreparadas.isEmpty() && vectorCeldasPreparadas.contains(cellReference.getPropertyId().toString()))
                			{
                				return "Rcell-dgreenp";
                			}
                			else
                			{
                				return "Rcell-pointer";
                			}
            			}

            		}
            	}

            	else
            	{
            		if (prepedido)
            		{
            			return "Rcell-green";
            		}
            		else
            		{
            			if (preparado)
            			{
            				return "Rcell-dgreen";
            			}
            			else
            			{
            				return "Rcell-normal";
            			}

            		}
            	}	
            }
        });
    }

	private void cargarListenersGrid()
	{
		
		this.grid.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	estoyEnCelda=false;
            	Item file = grid.getContainerDataSource().getItem(event.getItemId());
            	if (event.getPropertyId()!=null)
            	{
            		if (event.getPropertyId().toString().contentEquals("Pedido"))
            		{
//            			activadaVentanaPeticion=true;
//            			ordenando = false;
            			estoyEnCelda=true;
            			String valor = file.getItemProperty("Pedido").getValue().toString();
            			
            			pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(new Integer(valor), "Lineas Pedido: " + valor);
    					app.getUI().addWindow(vt);
            		}
            		else if (!mapeoEstructura.getHashColumnasNombresFijas().containsValue(event.getPropertyId().toString()))
            		{
            			estoyEnCelda=true;
            			Integer pedido = new Integer(file.getItemProperty("Pedido").getValue().toString());
            			String valor = event.getPropertyId().toString();
            			
            			boolean existe = cis.comprobarCeldasPedidos(areaConsulta, pedido, valor);
            			if (existe)
            			{
            				cis.eliminarCeldasPedidos(areaConsulta, pedido, valor);
            			}
            			else
            			{
            				cis.guardarNuevoCeldasPedidos(areaConsulta, pedido, valor);
            			}
//            			Notificaciones.getInstance().mensajeAdvertencia("completar" + event.getPropertyId().toString() + " " +  valor + " Del Pedido " + pedido + " de " + areaConsulta);
            			/*
            			 * mandaremos estos datos a guardar, si existen los borramos y des-preparamos, si no existen guardamos y marcamos como preparada
            			 */
            			grid.refreshAllRows();
            			generarTotalesGrid();
            		}
            	}
            }
    	});
		
		this.grid.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
					
				if (grid.getSelectionModel() instanceof SingleSelectionModel && !estoyEnCelda)
					app.filaSeleccionada(grid.getContainerDataSource().getItem(grid.getSelectedRow()));
				else
					estoyEnCelda = false;
			}
		});
	}

	private void cargarNombreAgrupaciones()
	{
		HashMap<Integer, String> nombreAgrupaciones = null;
		HashMap<Integer, String> nombreTotalesAgrupaciones = null;
		
		if (app instanceof PreparacionPedidosView)
		{
			if (((PreparacionPedidosView)app).areaEscogida.contentEquals("Pedidos Nacional") )
			{
				nombreAgrupaciones = new HashMap<Integer, String>();
				nombreAgrupaciones.put(1,"Gama SELECCION");
				nombreAgrupaciones.put(2,"Gama CLASICA");
				nombreAgrupaciones.put(3,"VINO MESA");
				nombreAgrupaciones.put(4,"VARIOS");
		
				nombreTotalesAgrupaciones = new HashMap<Integer, String>();
				nombreTotalesAgrupaciones.put(1,"G.S.");
				nombreTotalesAgrupaciones.put(2,"G.C.");
				nombreTotalesAgrupaciones.put(3,"V.M.");
				nombreTotalesAgrupaciones.put(4,"V.");
			}
			else if (((PreparacionPedidosView)app).areaEscogida.contentEquals("Pedidos USA") )
			{
				nombreAgrupaciones = new HashMap<Integer, String>();
				nombreAgrupaciones.put(1,"Botella");
				nombreAgrupaciones.put(2,"BIB");
		
				nombreTotalesAgrupaciones = new HashMap<Integer, String>();
				nombreTotalesAgrupaciones.put(1,"Botella");
				nombreTotalesAgrupaciones.put(2,"BIB");
			}
		}
		mapeoEstructura.setNombreAgrupaciones(nombreAgrupaciones);
		mapeoEstructura.setNombreTotalesAgrupaciones(nombreTotalesAgrupaciones);
		
	}
	
	private void cargarEstructuraColumnasFijas()
	{
		HashMap<String, String> hashColumnasTiposFijas = new HashMap<String, String>();
		HashMap<String, String> hashColumnasNombresFijas = new HashMap<String, String>();

			hashColumnasTiposFijas.put("0", "String");
			hashColumnasTiposFijas.put("1", "String");
			hashColumnasTiposFijas.put("2", "String");
			hashColumnasTiposFijas.put("3", "String");
			hashColumnasTiposFijas.put("4", "String");
			hashColumnasTiposFijas.put("5", "Integer");
			hashColumnasTiposFijas.put("6", "Integer");
			hashColumnasTiposFijas.put("7", "String");
			hashColumnasTiposFijas.put("8", "String");
			
			hashColumnasNombresFijas.put("0","Situacion");
			hashColumnasNombresFijas.put("1","Preparado");
			hashColumnasNombresFijas.put("2","Cliente");
			hashColumnasNombresFijas.put("3","Referencia");
			hashColumnasNombresFijas.put("4","Shipment");
			hashColumnasNombresFijas.put("5","Pedido");
			hashColumnasNombresFijas.put("6","Almacen");
			hashColumnasNombresFijas.put("7","Fecha");
			hashColumnasNombresFijas.put("8","Entrega");
		
		mapeoEstructura.setHashColumnasNombresFijas(hashColumnasNombresFijas);
		mapeoEstructura.setHashColumnasTiposFijas(hashColumnasTiposFijas);
	}
	

	public void calcularTotal() 
	{
//		footer.setStyleName("smallgrid");
	}

	@Override
	public void generarTotalesGrid() 
	{
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double totalCuadre = new Double(0) ;
		Double totalGeneral = new Double(0) ;
		Double totalTotalCuadre= new Double(0) ;
		Double totalTotalGeneral= new Double(0) ;
		ArrayList<Integer> vectorPreparadas=null;
		
    	if (this.grid.getFooterRowCount()==0)
    	{
    		this.grid.appendFooterRow();
    		this.grid.appendFooterRow();
    	}
    	
    	Indexed indexed = this.grid.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        
        this.grid.getFooterRow(0).getCell("Cliente").setText("Pendiente Preparar");
        this.grid.getFooterRow(1).getCell("Cliente").setText("Pendiente Servir");
        
		for (int i=0;i<this.mapeoEstructura.getVectorTitulos().size();i++)
		{
			totalCuadre = new Double(0) ;
			totalGeneral = new Double(0) ;
			vectorPreparadas = obtenerPreparadas(this.mapeoEstructura.getVectorTitulos().get(i).toString().trim(), areaConsulta);
			
			for(Object itemId : list)
	        {
	        	Item item = indexed.getItem(itemId);
	        	Integer q1Value = (Integer) item.getItemProperty(this.mapeoEstructura.getVectorTitulos().get(i)).getValue();
	        	if (item!=null && !item.getItemProperty("Preparado").getValue().toString().contentEquals("P") && !item.getItemProperty("Situacion").getValue().toString().contentEquals("Pre") &&  !item.getItemProperty("Situacion").getValue().toString().contentEquals("Ppd") && !vectorPreparadas.contains(new Integer (item.getItemProperty("Pedido").getValue().toString())))
	        	{
		        	if (q1Value!=null )
		        	{
		        		totalCuadre += q1Value;
		        		if (!this.mapeoEstructura.getVectorTitulos().get(i).contains("Total")) totalTotalCuadre += q1Value;
		        	}
	        	}
	        	if (q1Value!=null )
	        	{
	        		totalGeneral += q1Value;
	        		if (!this.mapeoEstructura.getVectorTitulos().get(i).contains("Total")) totalTotalGeneral += q1Value;
	        	}
	        }
			int gama = grid.obtenerAgrupacion(this.mapeoEstructura.getVectorTitulos().get(i));
			this.grid.getFooterRow(0).getCell(this.mapeoEstructura.getVectorTitulos().get(i)).setText(RutinasNumericas.formatearDouble(totalCuadre).toString());
			this.grid.getFooterRow(0).getCell(this.mapeoEstructura.getVectorTitulos().get(i)).setStyleName("R" + grid.recogerEstiloAgrupacion(gama));
			this.grid.getFooterRow(1).getCell(this.mapeoEstructura.getVectorTitulos().get(i)).setText(RutinasNumericas.formatearDouble(totalGeneral).toString());
			this.grid.getFooterRow(1).getCell(this.mapeoEstructura.getVectorTitulos().get(i)).setStyleName("R" + grid.recogerEstiloAgrupacion(gama));
		}

		this.grid.getFooterRow(0).getCell("Total").setText(RutinasNumericas.formatearDouble(totalTotalCuadre).toString());
		this.grid.getFooterRow(0).getCell("Total").setStyleName("R" + grid.recogerEstiloAgrupacion(0));
		this.grid.getFooterRow(1).getCell("Total").setText(RutinasNumericas.formatearDouble(totalTotalGeneral).toString());
		this.grid.getFooterRow(1).getCell("Total").setStyleName("R" + grid.recogerEstiloAgrupacion(0));
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.grid.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");

		
	}

	private void obtenerCeldasPreparadas(Integer r_pedido, String r_area)
	{
		/*
		 * con el numero de pedido obtener las celdas pintadas
		 */
		
		vectorCeldasPreparadas = null;		
		
		/*
		 * recorro el hash de pedidos celdas y relleno el array con las celdas de mi numero de pedido
		 * 
		 */
		vectorCeldasPreparadas = cis.comprobarCeldasPreparadas(r_area, r_pedido);
		
	}
	
	private ArrayList<Integer> obtenerPreparadas(String r_celda, String r_area)
	{
		/*
		 * con el numero de pedido obtener las celdas pintadas
		 */
		ArrayList<Integer> vectorPreparadas=new ArrayList<Integer>();
		
		/*
		 * recorro el hash de pedidos celdas y relleno el array con las celdas de mi numero de pedido
		 * 
		 */
		vectorPreparadas = cis.comprobarCeldasPedidoPreparadas(r_area, r_celda);
		return vectorPreparadas;
	}
}