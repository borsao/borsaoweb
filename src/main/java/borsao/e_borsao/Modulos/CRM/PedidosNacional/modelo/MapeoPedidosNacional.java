package borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo;

import java.sql.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoPedidosNacional extends MapeoGlobal
{
	private Integer codigoPedido;
	private Date fechaDocumento;
	private Date fechaCarga;
	private String referencia;
	private String shipment;
	private String cumplimentado;
	private String situacion;	
	private Integer almacen;
	private String cliente;
	private HashMap<String, Integer> hashValores = null;
	
	private String observaciones1 = null;
	private String observaciones2 = null;
	private String observaciones3 = null;
	
	public MapeoPedidosNacional()
	{
		
	}

	public Integer getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(Integer codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getCumplimentado() {
		return cumplimentado;
	}

	public void setCumplimentado(String cumplimentado) {
		this.cumplimentado = cumplimentado;
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public HashMap<String, Integer> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Integer> hashValores) {
		this.hashValores = hashValores;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getObservaciones1() {
		return observaciones1;
	}

	public void setObservaciones1(String observaciones1) {
		this.observaciones1 = observaciones1;
	}

	public String getObservaciones2() {
		return observaciones2;
	}

	public void setObservaciones2(String observaciones2) {
		this.observaciones2 = observaciones2;
	}

	public String getObservaciones3() {
		return observaciones3;
	}

	public void setObservaciones3(String observaciones3) {
		this.observaciones3 = observaciones3;
	}

	public String getShipment() {
		return shipment;
	}

	public void setShipment(String shipment) {
		this.shipment = shipment;
	}

	
}