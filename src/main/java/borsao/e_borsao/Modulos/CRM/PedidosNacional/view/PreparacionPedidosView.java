package borsao.e_borsao.Modulos.CRM.PedidosNacional.view;

import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo.MapeoPedidosNacional;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo.MaquetadoGrid;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPedidosVentas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PreparacionPedidosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Preparacion Pedidos";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean conTotalesColumnas = false;
	public boolean modificando = false;
	
	public MaquetadoGrid maq = null;
	public OpcionesForm form = null;
	private Combo cmbArea= null;
	private Button opcCompletar = null;
	public String areaEscogida = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */

    public PreparacionPedidosView() 
    {
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(soloConsulta);
    	setConFormulario(conFormulario);
    	setConTotalesColumnas(conTotalesColumnas);
    	
    	if (eBorsao.get().accessControl.getDepartamento().contains("lmacen"))
    	{
    		this.areaEscogida="Pedidos Nacional";
    	}

    	this.cabLayout.setSpacing(true);
    	
    	this.opcBuscar.setVisible(false);
    	
    	this.cmbArea = new Combo("Area");
    	this.cmbArea.setNewItemsAllowed(false);
    	this.cmbArea.setNullSelectionAllowed(true);
    	this.cmbArea.addItem("Pedidos Nacional");
//    	this.cmbArea.addItem("Pedidos Exportación");
    	this.cmbArea.addItem("Pedidos USA");
    	this.cmbArea.setValue(this.areaEscogida);
    	this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);
    	
		this.opcCompletar= new Button("Dar por preparado");    	
		this.opcCompletar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcCompletar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCompletar.setEnabled(eBorsao.get().accessControl.getDepartamento().equals("Almacen")||eBorsao.get().accessControl.getDepartamento().equals("informatica"));
		this.cabLayout.addComponent(this.cmbArea);
		this.cabLayout.addComponent(this.opcCompletar);
		this.cabLayout.setComponentAlignment(this.opcCompletar, Alignment.BOTTOM_LEFT);

		this.cargarListeners();
		this.generarGrid(null);
//	
    }

    private void cargarListeners()
    {
    	this.cmbArea.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				areaEscogida=cmbArea.getValue().toString();
				if (areaEscogida!=null)
				{
					if (maq!=null && maq.grid!=null && maq.grid.isHayGrid())
					{			
						maq.grid.removeAllColumns();			
						barAndGridLayout.removeComponent(maq.grid);
						maq.grid.setHayGrid(false);
						maq.grid=null;
					}
					generarGrid(opcionesEscogidas);
				}
			}
		});
    	this.opcCompletar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				boolean rdo = false;
				areaEscogida=cmbArea.getValue().toString();
				Item file = maq.grid.getContainerDataSource().getItem(maq.grid.getSelectedRow());
				
				if (file!=null)
				{
					if (file.getItemProperty("Situacion")!=null)
					{
						MapeoPedidosVentas mapeoPedido = new MapeoPedidosVentas();
						
						mapeoPedido.setClave_tabla("P");
						mapeoPedido.setDocumento("P1");
						mapeoPedido.setSerie("P");
						mapeoPedido.setIdCodigoVenta((Integer) file.getItemProperty("Pedido").getValue());
						
						if (file.getItemProperty("Situacion").getValue().toString().contentEquals("OK") || file.getItemProperty("Situacion").getValue().toString().contentEquals("FAB") )
						{
							mapeoPedido.setEstado("Pre");
						}
						else 
						{
							mapeoPedido.setEstado(file.getItemProperty("Situacion").getValue().toString());
						}
						mapeoPedido.setPlazoEntrega(file.getItemProperty("Fecha").getValue().toString());
						
						consultaPedidosVentasServer cpvs = new consultaPedidosVentasServer(CurrentUser.get());
						rdo = cpvs.actualizarCabeceraGreensys(mapeoPedido, true);
						
						if (rdo)
						{
							if (maq.grid.isHayGrid())
							{			
								maq.grid.removeAllColumns();			
								barAndGridLayout.removeComponent(maq.grid);
								maq.grid.setHayGrid(false);
								maq.grid=null;
							}
							generarGrid(null);
						}
						else
						{
							Notificaciones.getInstance().mensajeError("Error al actualizar el pedido");
						}
					}
					else
					{
						Notificaciones.getInstance().mensajeError("Error al actualizar el pedido. Problemas con el estado del pedido.");
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se puede procesar el registro.");
				}
			}
		});
    }
    
    public void generarGrid(HashMap<String, String> opcionesEscogidas) 
	{
    	if (maq!=null && maq.grid!=null && maq.grid.isHayGrid())
		{			
			maq.grid.removeAllColumns();			
			barAndGridLayout.removeComponent(maq.grid);
			maq.grid.setHayGrid(false);
			maq.grid=null;
			maq=null;
		}
    	maq = new MaquetadoGrid(this, opcionesEscogidas, areaEscogida, isConTotalesColumnas());
		
		this.barAndGridLayout.addComponent(this.lblSeparador);
		this.barAndGridLayout.addComponent(this.maq.grid);
		this.barAndGridLayout.setExpandRatio(this.maq.grid, 1);
		this.barAndGridLayout.setMargin(true);
    }
    
    public void newForm()
    {

    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	
    	Item file = maq.grid.getContainerDataSource().getItem(maq.grid.getSelectedRow());
		
		if (file!=null)
		{
			MapeoPedidosNacional mapeoPedido = new MapeoPedidosNacional();
			this.modificando=true;
		
			mapeoPedido.setCodigoPedido((Integer) file.getItemProperty("Pedido").getValue());
			
			if ((file.getItemProperty("Shipment").getValue()==null || file.getItemProperty("Shipment").getValue().toString().length()==0) && (file.getItemProperty("Observaciones").getValue()==null || file.getItemProperty("Observaciones").getValue().toString().length()==0))
			{
				this.modificando=false;
			}
			if (file.getItemProperty("Shipment").getValue()!=null) mapeoPedido.setShipment((String) file.getItemProperty("Shipment").getValue()); else mapeoPedido.setShipment("");
			if (file.getItemProperty("Observaciones").getValue()!=null) mapeoPedido.setObservaciones1((String) file.getItemProperty("Observaciones").getValue()); else mapeoPedido.setObservaciones1("");
		
			this.verForm(false);
			this.form.editar(mapeoPedido);
		}

    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	@Override
	public void eliminarRegistro() {
		
	}
	
	@Override
	public void aceptarProceso(String r_accion) {
		
	}
	
	@Override
	public void cancelarProceso(String r_accion) {
		
	}

    private boolean isConTotalesColumnas() {
		return conTotalesColumnas;
	}

	private void setConTotalesColumnas(boolean conTotalesColumnas) {
		this.conTotalesColumnas = conTotalesColumnas;
	}

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(PreparacionPedidosView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(PreparacionPedidosView.VIEW_NAME, this.getClass());
	}
    
}
