package borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoPedidosNacional mapeo = null;
	 
	 public MapeoPedidosNacional convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPedidosNacional();
		 
		 this.mapeo.setCodigoPedido(new Integer(r_hash.get("codigo")));		 
		 
		 return mapeo;		 
	 }
	 
	 public MapeoPedidosNacional convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPedidosNacional();
		 
		 this.mapeo.setCodigoPedido(new Integer(r_hash.get("codigo")));		 
		 this.mapeo.setShipment(r_hash.get("shipment"));
		 this.mapeo.setObservaciones1(r_hash.get("observaciones"));		 
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoPedidosNacional r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.mapeo = new MapeoPedidosNacional();
		 
		 this.hash.put("codigo", RutinasCadenas.quitarPuntoMiles(r_mapeo.getCodigoPedido().toString()));		 
		 this.hash.put("shipment", r_mapeo.getShipment());
		 this.hash.put("observaciones", r_mapeo.getObservaciones1());
		 return hash;		 
	 }
}