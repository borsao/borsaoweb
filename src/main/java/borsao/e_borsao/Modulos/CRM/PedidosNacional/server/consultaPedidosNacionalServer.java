package borsao.e_borsao.Modulos.CRM.PedidosNacional.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGridDinamico;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CRM.PedidosNacional.modelo.MapeoPedidosNacional;
import borsao.e_borsao.Modulos.CRM.articulosPTEmbotelladora.server.consultaArticulosPTEmbotelladoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaPedidosNacionalServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaPedidosNacionalServer instance;
	
	private ArrayList<MapeoGridDinamico> vectorDatos = null;
	
	public consultaPedidosNacionalServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPedidosNacionalServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPedidosNacionalServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoGridDinamico> recuperarMovimientosPedidos(String r_area)
	{
		Integer pedido_old = null;
		ResultSet rsMovimientos = null;
//		ArrayList<MapeoPedidosNacional> vector = null;
//		MapeoPedidosNacional mapeo =null;
		
		
		MapeoGridDinamico mapeoFijas =null;
		HashMap<String, String> hashColumnasValoresFijas = new HashMap<String, String>();
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		HashMap<String, Integer> hashValores = null;
		HashMap<String, String> hashColumnas = null;
		String col = null;
		Double valor = new Double(0);
		Double valorActual= new Double(0);
		try
		{
			
			pedido_old=new Integer(0);
			String digito = "0";
			hashValores = new HashMap<String, Integer>();
			
			/*
			 * Relleno el hash de pesos por articulo
			 */
			consultaArticulosPTEmbotelladoraServer cptes = consultaArticulosPTEmbotelladoraServer.getInstance(CurrentUser.get());
			hashColumnas = cptes.datosOpcionesGlobal(r_area); 
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(a.plazo_entrega) ped_eje, ");
			sql.append(" b.ref_cliente ped_ref, ");
			sql.append(" b.finalidad ped_sit, ");
			sql.append(" a.plazo_entrega ped_fec, ");
			sql.append(" b.fecha_list_ruta ped_car, ");
			sql.append(" a.articulo ped_art, " );
			sql.append(" a.codigo ped_cod, ");
			sql.append(" b.nombre_comercial ped_nom, ");
			sql.append(" b.cumplimentacion ped_cum, ");
			sql.append(" a.almacen ped_alm, ");
			sql.append(" b.tty[7] ped_pre, ");
			sql.append(" sum(a.unidades-a.unidades_serv)  ped_uds ");
			sql.append(" FROM a_vp_l_0 a, a_vp_c_0 b ");
			sql.append(" where a.clave_tabla = b.clave_tabla ");
			sql.append(" and a.documento = b.documento ");
			sql.append(" and a.serie = b.serie ");
			sql.append(" and a.codigo = b.codigo ");
			sql.append(" and a.clave_movimiento='T'");
			if (r_area.contains("Pedidos Nacional")) sql.append(" and (a.cumplimentacion is null or a.cumplimentacion >= '" + RutinasFechas.fechaHoy() + "') ");
			if (r_area.contains("Pedidos USA"))
			{
				sql.append(" and a.cumplimentacion is null ");
				sql.append(" and b.ref_cliente not in (select apartado from a_tr_c_0) ");
			}
			sql.append(" and (a.articulo matches '0102*' or a.articulo matches '0103*' or a.articulo matches '0111*') ");
			
			if (r_area.contains("Pedidos Nacional"))
			{
				sql.append(" and b.pais = '000' ");
				sql.append(" and b.cif not in ('A46103834','A50008150') ");
				sql.append(" and b.cliente <> 6662 ");
				sql.append(" and (a.almacen in (1,4)) ");
//				sql.append(" or (a.almacen = 30 and b.ref_cliente not in (select apartado from a_tr_c_0)) )");
			}			
			else if (r_area.contains("Pedidos USA"))
			{
				sql.append(" and b.pais = '001' ");
				sql.append(" and (a.almacen in (1,4,10,20)) ");
//				sql.append(" or (a.almacen = 30 and b.ref_cliente not in (select apartado from a_tr_c_0)) )");
			}	
			sql.append(" GROUP BY 1,2,3,4,5,6,7,8,9,10,11 ");
			sql.append(" order by 8,1,4,7 ");
			
			con= this.conManager.establecerConexionGestion();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vectorDatos = new ArrayList<MapeoGridDinamico>();
			mapeoFijas = new MapeoGridDinamico();
			hashColumnasValoresFijas = new HashMap<String, String>();
			boolean primeraVez=true;
			
			while(rsMovimientos.next())
			{
				
				if (pedido_old.intValue()!=0 && pedido_old.intValue()!= rsMovimientos.getInt("ped_cod"))
				{
					
					mapeoFijas.setHashValores(hashValores);
					vectorDatos.add(mapeoFijas);
					
					hashValores = new HashMap<String, Integer>();
					hashColumnasValoresFijas = new HashMap<String, String>();
					mapeoFijas = new MapeoGridDinamico();
					
					primeraVez=true;
				}
				
				pedido_old=rsMovimientos.getInt("ped_cod");
				
				if (primeraVez)
				{
					primeraVez=false;
					
					hashColumnasValoresFijas.put("0", rsMovimientos.getString("ped_sit"));
					hashColumnasValoresFijas.put("1", rsMovimientos.getString("ped_pre"));
					hashColumnasValoresFijas.put("2", RutinasCadenas.conversion(rsMovimientos.getString("ped_nom")));
					hashColumnasValoresFijas.put("3", rsMovimientos.getString("ped_ref"));
					hashColumnasValoresFijas.put("4", this.datosPedido(rsMovimientos.getInt("ped_cod"),"ship"));
					hashColumnasValoresFijas.put("5", rsMovimientos.getString("ped_cod"));
					hashColumnasValoresFijas.put("6", rsMovimientos.getString("ped_alm"));
					hashColumnasValoresFijas.put("7",rsMovimientos.getString("ped_fec"));
					hashColumnasValoresFijas.put("8", rsMovimientos.getString("ped_car"));
					hashColumnasValoresFijas.put("observaciones", this.datosPedido(rsMovimientos.getInt("ped_cod"),"obs"));
					mapeoFijas.setHashColumnasFijasValores(hashColumnasValoresFijas);
				}				

				valor = rsMovimientos.getDouble("ped_uds");
				
				valorActual = new Double(0);
				
				if (hashColumnas.containsKey(rsMovimientos.getString("ped_art").trim()))
				{

					col = hashColumnas.get(rsMovimientos.getString("ped_art").trim());
				}
				else
				{
					col = "RESTO";
				}						
				
				if (hashValores== null || hashValores.isEmpty())
				{
					hashValores.put(col , valor.intValue());	
				}
				else
				{
					if (hashValores.containsKey(col))
					{
						valorActual = new Double(hashValores.get(col));
						valorActual = valorActual + valor;
						hashValores.put(col , valorActual.intValue());
					}
					else
					{
						hashValores.put(col , valor.intValue());
					}
				}
			}
			mapeoFijas.setHashValores(hashValores);
			vectorDatos.add(mapeoFijas);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vectorDatos;
	}
	
	public HashMap<String,	Double> recuperarMovimientosPedidosEjercicio(Integer r_ejercicio, String r_articulos)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		HashMap<String, Integer> hashPalets = null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			consultaArticulosPTEnvasadoraServer cptes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
			hashPalets = cptes.datosPaletizacionesGlobal(null);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio >= ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(a.plazo_entrega) as ejercicio, ");
			for (int i = 1;i<=52;i++)
			{
				sql.append(" SUM( CASE week(a.plazo_entrega,3) WHEN '" + i + "' THEN a.unidades ELSE 0 END ) AS '" + i + "', ");
			}
			sql.append(" SUM( CASE week(a.plazo_entrega,3) WHEN '53' THEN a.unidades ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_vp_l_" + digito + " a, a_vp_c_" + digito + " b ");
			sql.append(" where a.clave_tabla = b.clave_tabla ");
			sql.append(" and a.documento = b.documento ");
			sql.append(" and a.serie = b.serie ");
			sql.append(" and a.codigo = b.codigo ");
			sql.append(" and b.pais = '000' ");
			sql.append(" and a.almacen = 1 ");
			sql.append(" and a.clave_movimiento='T'");
			sql.append(" and a.articulo in " + r_articulos + " " );
			
			sql.append(" GROUP BY ejercicio ");

//			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestion();
//			else 
				con= this.conManager.establecerConexion();
				
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			Double valor = new Double(0);
			Integer paletizacion = hashPalets.get(r_articulos.substring(2, 9));
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				
				if (r_ejercicio.intValue()==rsMovimientos.getInt("ejercicio"))
				{
					for (int i = 1; i<=53;i++)
					{
						valor = rsMovimientos.getDouble(String.valueOf(i));
						valor = valor / paletizacion;
	
						hash.put(String.valueOf(i), valor.doubleValue());
					}
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}

	public String guardarNuevoCeldasPedidos(String r_area, Integer r_pedido, String r_celda)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
//		CREATE TABLE `crm_celdas_pedidos` (
//				  `area` varchar(40) DEFAULT NULL,
//				  `pedido` int(11) DEFAULT NULL,
//				  `celda` varchar(500) DEFAULT NULL
//				) ENGINE=MyISAM DEFAULT CHARSET=latin1;

		try
		{
			cadenaSQL.append(" INSERT INTO crm_celdas_pedidos( ");
			cadenaSQL.append(" crm_celdas_pedidos.area, ");
			cadenaSQL.append(" crm_celdas_pedidos.pedido, ");
			cadenaSQL.append(" crm_celdas_pedidos.celda) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_area!=null)
		    {
		    	preparedStatement.setString(1, r_area);
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_pedido!=null)
		    {
		    	preparedStatement.setInt(2, r_pedido);
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_celda!=null)
		    {
		    	preparedStatement.setString(3, r_celda);
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return rdo;
	}
	
	public String eliminarCeldasPedidos(String r_area, Integer r_pedido, String r_celda)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		try
		{
			cadenaSQL.append(" DELETE FROM crm_celdas_pedidos ");
			cadenaSQL.append(" where crm_celdas_pedidos.area = ? ");
			cadenaSQL.append(" and crm_celdas_pedidos.pedido=?  ");
			cadenaSQL.append(" and crm_celdas_pedidos.celda =? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_area!=null)
		    {
		    	preparedStatement.setString(1, r_area);
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_pedido!=null)
		    {
		    	preparedStatement.setInt(2, r_pedido);
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_celda!=null)
		    {
		    	preparedStatement.setString(3, r_celda);
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return rdo;
	}
	
	public boolean comprobarCeldasPedidos(String r_area, Integer r_pedido, String r_celda)
	{
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		ResultSet rsOpcion = null;		
		Statement cs = null;

		try
		{
			cadenaSQL.append(" select pedido FROM crm_celdas_pedidos ");
			cadenaSQL.append(" where crm_celdas_pedidos.area = '" + r_area + "' ");
			cadenaSQL.append(" and crm_celdas_pedidos.pedido= " + r_pedido );
			cadenaSQL.append(" and crm_celdas_pedidos.celda = '" + r_celda + "' ");
		    
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}

	public ArrayList<String> comprobarCeldasPreparadas(String r_area, Integer r_pedido)
	{
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		ResultSet rsOpcion = null;		
		Statement cs = null;
		ArrayList<String> vectorCeldasPreparadas = null;
		try
		{
			cadenaSQL.append(" select celda FROM crm_celdas_pedidos ");
			cadenaSQL.append(" where crm_celdas_pedidos.area = '" + r_area + "' ");
			cadenaSQL.append(" and crm_celdas_pedidos.pedido= " + r_pedido );
		    
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vectorCeldasPreparadas = new ArrayList<String>();
			
			while(rsOpcion.next())
			{
				vectorCeldasPreparadas.add(rsOpcion.getString("celda"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vectorCeldasPreparadas;
	}

	public ArrayList<Integer> comprobarCeldasPedidoPreparadas(String r_area, String r_celda)
	{
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		ResultSet rsOpcion = null;		
		Statement cs = null;
		ArrayList<Integer> vectorCeldasPreparadas = null;
		try
		{
			cadenaSQL.append(" select pedido FROM crm_celdas_pedidos ");
			cadenaSQL.append(" where crm_celdas_pedidos.area = '" + r_area + "' ");
			cadenaSQL.append(" and crm_celdas_pedidos.celda= '" + r_celda + "' ");
		    
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vectorCeldasPreparadas = new ArrayList<Integer>();
			
			while(rsOpcion.next())
			{
				vectorCeldasPreparadas.add(rsOpcion.getInt("pedido"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vectorCeldasPreparadas;
	}

	public String guardaraNuevoDatosPedidos(MapeoPedidosNacional r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		try
		{
			cadenaSQL.append(" INSERT INTO crm_datos_pedido ( ");
			cadenaSQL.append(" crm_datos_pedido.codigo, ");
			cadenaSQL.append(" crm_datos_pedido.shipment, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones1, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones2, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones3 ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getCodigoPedido()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getCodigoPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getShipment()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getShipment());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getObservaciones1()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones1());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getObservaciones2()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getObservaciones2());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getObservaciones3()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getObservaciones3());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	

	public String guardarCambiosDatosPedidos(MapeoPedidosNacional r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		try
		{
			cadenaSQL.append(" update crm_datos_pedido SET ");
			cadenaSQL.append(" crm_datos_pedido.shipment =?, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones1=?, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones2=?, ");
			cadenaSQL.append(" crm_datos_pedido.observaciones3=? ");
    		cadenaSQL.append(" WHERE crm_datos_pedido.codigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getShipment()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getShipment());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getObservaciones1()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getObservaciones1());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getObservaciones2()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones2());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getObservaciones3()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getObservaciones3());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCodigoPedido()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCodigoPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoPedidosNacional r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;
		
		try
		{
			cadenaSQL.append(" DELETE FROM crm_datos_pedido ");            
			cadenaSQL.append(" WHERE crm_datos_pedido.codigo = ?"); 

			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getCodigoPedido());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public String datosPedido(Integer r_codigo, String r_valor)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Statement cs = null;
		Connection con = null;
		
		cadenaSQL.append(" SELECT ped.codigo ped_cod, ");
		cadenaSQL.append(" ped.shipment ped_shi, ");
		cadenaSQL.append(" ped.observaciones1 ped_ob1 ");
     	cadenaSQL.append(" FROM crm_datos_pedido ped ");
     	cadenaSQL.append(" where ped.codigo =" + r_codigo  );
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (r_valor.contentEquals("ship")) return rsOpcion.getString("ped_shi");
				if (r_valor.contentEquals("obs")) return rsOpcion.getString("ped_ob1");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return "";
	}

}
