package borsao.e_borsao.Modulos.CRM.albaranesVenta.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaVentas extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private Button btnBotonAVentana = null;
	private Button btnBotonMVentana = null;
	private VerticalLayout principal = null;
	
	public pantallaVentas(Integer r_ejercicio, String r_articulos)
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Ventas");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesVentasServer cse = new consultaAlbaranesVentasServer(CurrentUser.get());
		ArrayList<MapeoLineasAlbaranesVentas> vector = cse.datosOpcionesVentasAgrupadas(r_ejercicio, r_articulos);

		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		this.calcularTotal();
		this.activarFiltro();
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	private void llenarRegistros(ArrayList<MapeoLineasAlbaranesVentas> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasAlbaranesVentas mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("ejercicio", Integer.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
		container.addContainerProperty("unidades", Integer.class, null);
		container.addContainerProperty("añada", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasAlbaranesVentas) iterator.next();
			
			file.getItemProperty("ejercicio").setValue(mapeoAyuda.getEjercicio());
			file.getItemProperty("articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
    		file.getItemProperty("descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
    		file.getItemProperty("unidades").setValue(mapeoAyuda.getUnidades());
    		file.getItemProperty("añada").setValue(mapeoAyuda.getLote());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);

		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("unidades".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()) || "añada".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	String añada = null;
    	String añada_old = "";
    	Integer valor = null;
    	
    	if (this.gridDatos.getFooterRowCount()==0) this.gridDatos.appendFooterRow();
    	
    	Indexed indexed = this.gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Integer) item.getItemProperty("unidades").getValue();
        	totalU += valor.longValue();
				
        }
        
        this.gridDatos.getFooterRow(0).getCell("articulo").setText("Totales ");
        this.gridDatos.getFooterRow(0).getCell("unidades").setText(RutinasNumericas.formatearDouble(totalU.toString()));
//		this.principal.setWidth("100%");
//		this.principal.setHeight((this.principal.getHeight()-this.principal.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		this.gridDatos.getFooterRow(0).getCell("unidades").setStyleName("Rcell-pie");
		

	}

	private void activarFiltro()
	{
		HeaderRow filterRow = this.gridDatos.appendHeaderRow();
		
		for (Object pid: this.gridDatos.getContainerDataSource().getContainerPropertyIds()) 
		{
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");				
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(new TextChangeListener() {
				
				@Override
				public void textChange(TextChangeEvent event) {
					// Can't modify filters so need to replace
					container.removeContainerFilters(pid);
					// (Re)create the filter if necessary
					if (!event.getText().isEmpty()) container.addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
					calcularTotal();
			}});
			
			cell.setComponent(filterField);
		}
		
	}

}