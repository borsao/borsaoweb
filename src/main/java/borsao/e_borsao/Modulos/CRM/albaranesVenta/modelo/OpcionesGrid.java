package borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.consultaVentasLoteView;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;
	private consultaVentasLoteView padre =null;
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean conTotales = false;
	
    public OpcionesGrid(consultaVentasLoteView app, ArrayList<MapeoLineasAlbaranesVentas> r_vector) 
    {
        this.padre=app;
        this.vector=r_vector;
        
		this.asignarTitulo("Ventas Articulo Lote");
		
    	if (this.vector==null)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	
    	this.camposNoFiltrar.add("unidades");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
    	this.setConTotales(this.conTotales);
		this.crearGrid(MapeoLineasAlbaranesVentas.class);
		this.setRecords(this.vector);
//		this.setStyleName("programacion");
		
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		
		if (this.padre.conTotales) this.calcularTotal();
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "fechaDocumento", "documento", "idCodigoVenta", "pais", "cliente", "nombreCliente", "telefono",  "articulo","descripcion","unidades", "lote","referencia");
    }
    
    public void establecerTitulosColumnas()
    {

    	this.widthFiltros.put("ejercicio", "70");
    	this.widthFiltros.put("fechaDocumento", "80");
    	this.widthFiltros.put("articulo", "100");
    	this.widthFiltros.put("descripcion", "225");
    	this.widthFiltros.put("idCodigoVenta", "90");
    	this.widthFiltros.put("pais", "70");
    	this.widthFiltros.put("referencia", "100");
    	this.widthFiltros.put("almacen", "70");
    	this.widthFiltros.put("documento", "70");
    	this.widthFiltros.put("cliente", "100");
    	this.widthFiltros.put("lote", "100");
    	this.widthFiltros.put("nombreCliente", "225");
    	this.widthFiltros.put("telefono", "150");

    	this.getColumn("nombreCliente").setHidable(true);
    	this.getColumn("fechaDocumento").setHidable(true);
    	this.getColumn("idCodigoVenta").setHidable(true);
    	this.getColumn("unidades").setHidable(true);
    	this.getColumn("lote").setHidable(true);
    	this.getColumn("referencia").setHidable(true);
    	
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(100));

    	this.getColumn("fechaDocumento").setHeaderCaption("Fecha ");
    	this.getColumn("fechaDocumento").setWidth(new Double(120));

    	this.getColumn("documento").setHeaderCaption("Doc.");
    	this.getColumn("documento").setWidth(new Double(100));

    	this.getColumn("idCodigoVenta").setHeaderCaption("Albaran");
    	this.getColumn("idCodigoVenta").setWidth(new Double(120));
    	
    	this.getColumn("pais").setHeaderCaption("Pais");
    	this.getColumn("pais").setWidth(new Double(100));

    	this.getColumn("cliente").setHeaderCaption("Cliente");
    	this.getColumn("cliente").setWidth(new Double(140));

    	this.getColumn("nombreCliente").setHeaderCaption("Nombre");
    	this.getColumn("telefono").setHeaderCaption("Telefono");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(140));

    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
//    	this.getColumn("descripcion").setWidth(new Double(400));
    	
    	this.getColumn("unidades").setHeaderCaption("Unidades");
    	this.getColumn("unidades").setWidth(new Double(125));
    	
    	this.getColumn("lote").setHeaderCaption("Lote");
    	this.getColumn("lote").setWidth(new Double(125));

    	this.getColumn("claveTabla").setHidden(true);
    	this.getColumn("codigoPostal").setHidden(true);
    	this.getColumn("direccion").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("nif").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	
    	this.ocultarColumnas();
	}
	
    public void cargarListeners()
    {
    	this.addColumnVisibilityChangeListener(new ColumnVisibilityChangeListener() {
			
			@Override
			public void columnVisibilityChanged(ColumnVisibilityChangeEvent event) {
				//javi
			}
		});
//    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
//    	{
//            public void itemClick(ItemClickEvent event) 
//            {
//            	if (event.getPropertyId()!=null)
//            	{
//            		MapeoStockExternos mapeo = (MapeoStockExternos) event.getItemId();
//            		
//	            	if (event.getPropertyId().toString().equals("albaranes"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		pantallaConsultaLineasVentas vt = null;
//						vt = new pantallaConsultaLineasVentas("Salidas del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getArticulo());
//	            		getUI().addWindow(vt);
//	            	}
//	            	else if (event.getPropertyId().toString().equals("traslados"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		pantallaLineasAlbaranesTraslados vt = null;
//	            		vt = new pantallaLineasAlbaranesTraslados("Envíos del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getAlmacen(), mapeo.getArticulo(),mapeo.getPedidoCliente());
//	            		getUI().addWindow(vt);	            			
//	            	}
//	            	else
//	            	{
//	            		activadaVentanaPeticion=false;
//	            	}
//	            }
//    		}
//        });
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoProgramacion) {
                MapeoLineasAlbaranesVentas progRow = (MapeoLineasAlbaranesVentas)bean;
                // The actual description text is depending on the application
//                if ("traslados".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Lineas Envio";
//                }
//                else if ("albaranes".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Lineas facturadas";
//                }
            }
        }
        return descriptionText;
    }
    
    public void asignarEstilos()
    {
    	asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	String estilo = "cell-normal";
            	
            	if ( "unidades".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()) || "cliente".equals(cellReference.getPropertyId()) || "idCodigoVenta".equals(cellReference.getPropertyId()))
            	{
            			estilo = "R" + estilo;
            	}
            	return estilo;	
            }
        });
    }
    
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
//    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
//    	consultaStockExternosServer ccs = consultaStockExternosServer.getInstance(CurrentUser.get());
//    	pdfGenerado = ccs.generarInforme((ArrayList<MapeoStockExternos>) this.vector, regenerar);
//    	
//    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//		
//		pdfGenerado = null;

    }

	@Override
	public void calcularTotal() {
    	Long total = new Long(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
			Integer q1Value = (Integer) item
					.getItemProperty("unidades").getValue();
			total += q1Value.longValue();

        }
        footer.getCell("articulo").setText("Total Unidades");
		footer.getCell("unidades").setText(RutinasNumericas.formatearDouble(total.toString()));
		this.padre.barAndGridLayout.setWidth("100%");
		this.padre.barAndGridLayout.setHeight((this.padre.getHeight()-this.padre.cabLayout.getHeight()-this.padre.lblSeparador.getHeight()-this.padre.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("unidades").setStyleName("Rcell-pie");
		

	}
	
	private void ocultarColumnas()
	{
		if (this.padre.cols!=null)
		{
			for (int j=0; j<this.getColumns().size();j++)
			{
				String columna = this.getColumns().get(j).getPropertyId().toString();
				if (!this.padre.cols.contains(columna)) this.getColumn(columna).setHidden(true);
			}
		}
	}
}

