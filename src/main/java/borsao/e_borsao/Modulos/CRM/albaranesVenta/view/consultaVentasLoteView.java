package borsao.e_borsao.Modulos.CRM.albaranesVenta.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.vaadin.haijian.ExcelExporter;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaVentasLoteView extends GridViewRefresh {

	public static final String VIEW_NAME = "Ventas Lote";
	private final String titulo = "Salidas Articulo Lote";
	private final int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean generado = false;
	private Table tabla = null;
	private consultaAlbaranesVentasServer cas  = null;

	public boolean conTotales = true;
	public TextFieldAyuda txtArticulo = null;
	public TextFieldAyuda txtCliente = null;
	public TextField txtLote = null;
	public TextField txtLotePS = null;
	public Button btnBuscar = null;
	public HashMap<String , String> opcionesEscogidas;
	public ExcelExporter excelExporter=null;
	public ArrayList<String> cols = null;
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaVentasLoteView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	opcionesEscogidas = new HashMap<String , String>();
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.txtArticulo= new TextFieldAyuda("ARTICULO VENTAS", "");
		this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtCliente= new TextFieldAyuda("CLIENTE", "");
		this.txtCliente.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtLote= new TextField("Lote");
		this.txtLote.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtLotePS= new TextField("Lote Semiterminado");
		this.txtLotePS.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.btnBuscar= new Button("Buscar");
		this.btnBuscar.addStyleName(ValoTheme.BUTTON_TINY);
		
		this.cabLayout.addComponent(this.txtCliente);
		this.cabLayout.addComponent(this.txtArticulo);
		this.cabLayout.addComponent(this.txtLote);
		this.cabLayout.addComponent(this.txtLotePS);
		this.cabLayout.addComponent(this.btnBuscar);
		this.cabLayout.setComponentAlignment(this.btnBuscar, Alignment.BOTTOM_LEFT);
		
		excelExporter = new ExcelExporter();
	  	
		excelExporter.setDateFormat("yyyy-MM-dd");
		excelExporter.setStyleName(ValoTheme.BUTTON_PRIMARY);
		excelExporter.setStyleName(ValoTheme.BUTTON_TINY);
		excelExporter.setIcon(FontAwesome.DOWNLOAD);
		excelExporter.setCaption("Export to Excel");
		excelExporter.setDownloadFileName( "Ventas Articulo Lote");

		topLayoutR.addComponent(excelExporter);
		topLayoutR.setComponentAlignment(excelExporter, Alignment.MIDDLE_RIGHT);
		

		this.cargarListeners();
		
		this.presentarGrid(new ArrayList<MapeoLineasAlbaranesVentas>());
		
//    	this.barAndGridLayout.removeComponent(this.cabLayout);
//    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    
    private void presentarGrid(ArrayList<MapeoLineasAlbaranesVentas> r_vector)
    {
    	
    	grid = new OpcionesGrid(this,r_vector);
    	if (((OpcionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else if (((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    public void generarGrid(HashMap<String , String> opcionesEscogidas, ArrayList<String> r_columnas)
    {
    	
    	StringBuilder lotesEncontrados = new StringBuilder();
    	ArrayList<MapeoLineasAlbaranesVentas> r_vector=null;
    	
    	consultaProduccionDiariaServer cps = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
    	
    	
    	if (opcionesEscogidas.get("lotePS")!=null &&  opcionesEscogidas.get("lotePS").length()>0)
     	{
     		/*
     		 * obtener lotes PT buscando el lote PS que me llega
     		 * montar un string con todos los posibles
     		 */
     		lotesEncontrados = cps.obtenerLotesPT(opcionesEscogidas.get("lotePS"));
     	}
    	
    	this.cas = new consultaAlbaranesVentasServer(CurrentUser.get());
    	r_vector=this.cas.datosVentasLote(opcionesEscogidas.get("articulo"), opcionesEscogidas.get("lote"), lotesEncontrados ,opcionesEscogidas.get("cliente"), r_columnas);
    	
    	this.presentarGrid(r_vector);
    	
    }
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }

    private void cargarListeners()
    {
    	
		this.excelExporter.addClickListener(new ClickListener() {
	        @Override
	        public void buttonClick(ClickEvent event) {
	        	tabla = generarTablaExcel();
	    		excelExporter.setTableToBeExported(tabla);
	        }
		});

    	this.btnBuscar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (comprobarValores())
				{
					opcionesEscogidas.put("cliente", txtCliente.txtTexto.getValue().toString());
					opcionesEscogidas.put("articulo", txtArticulo.txtTexto.getValue().toString());
					opcionesEscogidas.put("lote", txtLote.getValue().toString());
					opcionesEscogidas.put("lotePS", txtLotePS.getValue().toString());
					ArrayList<String> columnas = obtenerColumnas();
					
					if (isHayGrid())
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;	
					}
					generarGrid(opcionesEscogidas,columnas);
				}
			}
		});
//		this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;			
//					opcionesEscogidas=null;
//					
//					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().length()>0) 
//					{
//						opcionesEscogidas = new HashMap<String , String>();
//						opcionesEscogidas.put("almacen", cmbAlmacen.getValue().toString());
//					}
//					
//					generarGrid(opcionesEscogidas);
//				}
//			}
//		}); 
    }
    

    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(false);
    }
    public void print() {
    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;

    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}
	
	private Table generarTablaExcel()
	{
		Table table = new Table();
		ArrayList<Object> cols = new ArrayList<Object>();

		for (int i=0;i<this.grid.getColumns().size();i++)
		{
			Column c = this.grid.getColumns().get(i);
			
			if (!c.isHidden())
			{
				cols.add(c.getPropertyId());
			}
		}
		
		table.setContainerDataSource(this.grid.getContainerDataSource());
		//new Object[]{"ejercicio", "fechaDocumento", "documento", "idCodigoVenta", "pais", "cliente", "nombreCliente", "articulo","descripcion","unidades", "lote"}
		table.setVisibleColumns(cols.toArray());
		return table;
	}
	
	private ArrayList<String> obtenerColumnas()
	{
		this.cols = new ArrayList<String>();

		for (int i=0;i<this.grid.getColumns().size();i++)
		{
			Column c = this.grid.getColumns().get(i);
			
			if (c.isHidden() && c.getPropertyId().toString().equals("unidades")) conTotales=false;
			if (!c.isHidden())
			{
				this.cols.add(c.getPropertyId().toString());
			}
		}
		return this.cols;
	}

	private boolean comprobarValores()
	{
		if (this.txtArticulo.txtTexto.getValue().length()==0 && this.txtCliente.txtTexto.getValue().length()==0 && this.txtLote.getValue().length()==0 && this.txtLotePS.getValue().length()==0) 
		{
			Notificaciones.getInstance().mensajeAdvertencia("Debes introducir el cliente o el articulo o el/los lote/s a buscar");
			return false;
		}
//		if (this.txtLote.getValue().length()==0) 
//		{
//			Notificaciones.getInstance().mensajeAdvertencia("Debes introducir el Lote a buscar");
//			return false;
//		}
		return true;
	}

	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		// Auto-generated method stub
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

