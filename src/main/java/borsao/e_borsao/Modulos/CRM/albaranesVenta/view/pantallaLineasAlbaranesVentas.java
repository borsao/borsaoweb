package borsao.e_borsao.Modulos.CRM.albaranesVenta.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.SeguimientoEMCSView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasAlbaranesVentas extends Window
{
	private boolean mp = false;
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private Button btnBotonAVentana = null;
	private Button btnBotonMVentana = null;
	private GridViewRefresh app = null;
	public ArrayList<String> camposNoFiltrar = new ArrayList<String>();
	public HashMap<String,String> widthFiltros = new HashMap<String, String>();
	
	public pantallaLineasAlbaranesVentas(GridViewRefresh r_app, String r_titulo, Integer r_ejercicio, String r_claveTabla, String r_documento, String r_serie, Integer r_albaran)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		this.app=r_app;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesVentasServer cse = new consultaAlbaranesVentasServer(CurrentUser.get());
		ArrayList<MapeoLineasAlbaranesVentas> vector = cse.datosOpcionesGlobal(r_ejercicio, r_claveTabla, r_documento, r_serie, r_albaran);

		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonAVentana = new Button("Quitar Albaran");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				liberar();
			}
		});

		btnBotonMVentana = new Button("NO DECLARAR");
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonMVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				marcar();
			}
		});

		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);
		if ((!(this.app instanceof AsignacionSalidasEMCSView)) && (!(this.app instanceof AsignacionEntradasEMCSView)))
		{
			botonera.addComponent(btnBotonAVentana);	
			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		}
		if (((this.app instanceof AsignacionSalidasEMCSView)) || ((this.app instanceof AsignacionEntradasEMCSView)))
		{
			botonera.addComponent(btnBotonMVentana);	
			botonera.setComponentAlignment(btnBotonMVentana, Alignment.BOTTOM_LEFT);
		}
		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasAlbaranesVentas(Integer r_ejercicio, String r_articulo, boolean r_padre)
	{
		ArrayList<MapeoLineasAlbaranesVentas> vector =null;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		this.app=null;
		this.mp=!r_padre;
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Ventas");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesVentasServer cse = new consultaAlbaranesVentasServer(CurrentUser.get());
		
		if (r_padre)
		{
			MapeoLineasAlbaranesVentas mapeoAlb = new MapeoLineasAlbaranesVentas();
			mapeoAlb.setEjercicio(r_ejercicio);
			mapeoAlb.setArticulo(r_articulo);
			vector = cse.datosOpcionesGlobal(mapeoAlb);
		}
		else
		{
			vector = cse.datosOpcionesVentasAgrupadas(r_ejercicio,r_articulo);
		}
		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		this.calcularTotal();
		this.activarFiltro();
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasAlbaranesVentas(Integer r_ejercicio, String r_articulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		this.app=null;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Ventas");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesVentasServer cse = new consultaAlbaranesVentasServer(CurrentUser.get());
		MapeoLineasAlbaranesVentas mapeoAlb = new MapeoLineasAlbaranesVentas();
		mapeoAlb.setEjercicio(r_ejercicio);
		mapeoAlb.setArticulo(r_articulo);
		ArrayList<MapeoLineasAlbaranesVentas> vector = cse.datosOpcionesGlobal(mapeoAlb);

		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		this.calcularTotal();
		this.activarFiltro();
		
		
		
		btnBotonAVentana = new Button("Quitar Albaran");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				liberar();
			}
		});

		btnBotonMVentana = new Button("NO DECLARAR");
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonMVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				marcar();
			}
		});

		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);
		if (this.app!=null)
		{
			if ((!(this.app instanceof AsignacionSalidasEMCSView)) && (!(this.app instanceof AsignacionEntradasEMCSView)))
			{
				botonera.addComponent(btnBotonAVentana);	
				botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
			}
			if (((this.app instanceof AsignacionSalidasEMCSView)) || ((this.app instanceof AsignacionEntradasEMCSView)))
			{
				botonera.addComponent(btnBotonMVentana);	
				botonera.setComponentAlignment(btnBotonMVentana, Alignment.BOTTOM_LEFT);
			}
		}
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	private void llenarRegistros(ArrayList<MapeoLineasAlbaranesVentas> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasAlbaranesVentas mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("ejercicio", Integer.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
		container.addContainerProperty("lote", String.class, null);
		container.addContainerProperty("ubicacion", String.class, null);
		container.addContainerProperty("unidades", Integer.class, null);
		if (!this.mp)
		{
			container.addContainerProperty("fechaDocumento", String.class, null);
			container.addContainerProperty("documento", String.class, null);
			container.addContainerProperty("codigo", String.class, null);
			container.addContainerProperty("referencia", String.class, null);
			container.addContainerProperty("cliente", Integer.class, null);
			container.addContainerProperty("nombreCliente", String.class, null);
		}
		
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasAlbaranesVentas) iterator.next();
			
			file.getItemProperty("lote").setValue(mapeoAyuda.getLote());
			file.getItemProperty("ubicacion").setValue(mapeoAyuda.getUbicacion());
			file.getItemProperty("articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
    		file.getItemProperty("descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
    		file.getItemProperty("ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("unidades").setValue(mapeoAyuda.getUnidades());

    		if (!this.mp)
    		{
	    		file.getItemProperty("cliente").setValue(mapeoAyuda.getCliente());
	    		file.getItemProperty("nombreCliente").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreCliente()));
	    		file.getItemProperty("referencia").setValue(RutinasCadenas.conversion(mapeoAyuda.getReferencia()));
	    		
	    		if (mapeoAyuda.getFechaDocumento()!=null)
	    		{
	    			file.getItemProperty("fechaDocumento").setValue(mapeoAyuda.getFechaDocumento());
	    			file.getItemProperty("documento").setValue(mapeoAyuda.getDocumento());
	    			file.getItemProperty("codigo").setValue(mapeoAyuda.getIdCodigoVenta().toString());
	    		}
	    		else
	    		{
	    			file.getItemProperty("fechaDocumento").setValue(null);
	    			file.getItemProperty("documento").setValue(null);
	    			file.getItemProperty("codigo").setValue(null);
	    		}
	    		this.camposNoFiltrar.add("fechaDocumento");
	        	this.camposNoFiltrar.add("documento");
	        	this.camposNoFiltrar.add("referencia");
	        	this.camposNoFiltrar.add("cliente");
//	        	this.camposNoFiltrar.add("nombreCliente");
	        	this.camposNoFiltrar.add("codigo");
    		}
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);

    	this.widthFiltros.put("lote", "150");
    	this.widthFiltros.put("ubicacion", "80");
    	if (!this.mp)
    		this.widthFiltros.put("nombreCliente", "200");
    		
    	this.gridDatos.getColumn("ejercicio").setWidth(140);
    	this.gridDatos.getColumn("articulo").setWidth(120);
    	this.gridDatos.getColumn("descripcion").setWidth(340);
    	this.gridDatos.getColumn("lote").setWidth(190);
    	this.gridDatos.getColumn("ubicacion").setWidth(120);
    	
    	this.camposNoFiltrar.add("unidades");
    	this.camposNoFiltrar.add("ejercicio");
    	this.camposNoFiltrar.add("articulo");
    	this.camposNoFiltrar.add("descripcion");
    	

		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("unidades".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()) || "codigo".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void liberar()
	{
		if (this.app instanceof SeguimientoEMCSView)
			((SeguimientoEMCSView) this.app).cerrarVentanaBusqueda(null, null, null, null,null,null);
		else
			((AsignacionSalidasEMCSView) this.app).cerrarVentanaBusqueda(null, null, null, null,null,null);
		
		close();
	}
	
	private void marcar()
	{
		((AsignacionSalidasEMCSView) this.app).cerrarVentanaMarcar();
		close();
	}
	
	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	String añada = null;
    	String añada_old = "";
    	Integer valor = null;
    	
    	if (this.gridDatos.getFooterRowCount()==0) this.gridDatos.appendFooterRow();
    	
    	Indexed indexed = this.gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Integer) item.getItemProperty("unidades").getValue();
        	totalU += valor.longValue();
				
        }
        
        this.gridDatos.getFooterRow(0).getCell("articulo").setText("Totales ");
        this.gridDatos.getFooterRow(0).getCell("unidades").setText(RutinasNumericas.formatearDouble(totalU.toString()));
//		this.principal.setWidth("100%");
//		this.principal.setHeight((this.principal.getHeight()-this.principal.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		this.gridDatos.getFooterRow(0).getCell("unidades").setStyleName("Rcell-pie");
		

	}

	
	private void activarFiltro()
	{
		HeaderRow filterRow = this.gridDatos.appendHeaderRow();
		
		for (Object pid: this.gridDatos.getContainerDataSource().getContainerPropertyIds()) 
		{
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");				
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(new TextChangeListener() {
				
				@Override
				public void textChange(TextChangeEvent event) {
					// Can't modify filters so need to replace
					container.removeContainerFilters(pid);
					// (Re)create the filter if necessary
					if (!event.getText().isEmpty()) container.addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
					calcularTotal();
			}});
			
			if (isFiltrable(pid.toString())) 
			{
				filterField.setWidth(obtenerAncho(pid.toString()));
				cell.setComponent(filterField);
			}

		}
		
	}
	
	private boolean isFiltrable(String r_columna)
	{
		if (camposNoFiltrar==null) return true;
		
		for (int i=0;i<camposNoFiltrar.size();i++)
		{
			if (camposNoFiltrar.get(i)!=null && camposNoFiltrar.get(i).equals(r_columna)) return false;
		}
		return true;
	}

	private String obtenerAncho(String r_columna)
	{
		if (!widthFiltros.isEmpty())
		{
			String rdo = widthFiltros.get(r_columna);
			if (rdo!=null && rdo.length()>0) return rdo;
		}
		return "0";
	}

}