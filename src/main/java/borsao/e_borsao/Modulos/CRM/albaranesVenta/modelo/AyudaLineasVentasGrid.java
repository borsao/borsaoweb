package borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaLineasVentasGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public AyudaLineasVentasGrid(ArrayList<MapeoLineasAlbaranesVentas> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoLineasAlbaranesVentas.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo", "ejercicio", "documento", "idCodigoVenta", "fechaDocumento", "referencia" ,"unidades", "cliente", "nombreCliente", "nif");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("documento", "35");
    	this.widthFiltros.put("idCodigoVenta", "85");
    	this.widthFiltros.put("referencia", "85");
    	this.widthFiltros.put("cliente", "70");
    	this.widthFiltros.put("nombreCliente", "410");

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setSortable(false);
    	this.getColumn("ejercicio").setWidth(new Double(100));

    	this.getColumn("documento").setHeaderCaption("Doc");
    	this.getColumn("documento").setSortable(false);
    	this.getColumn("documento").setWidth(new Double(75));
    	
    	this.getColumn("idCodigoVenta").setHeaderCaption("Numero");
    	this.getColumn("idCodigoVenta").setSortable(false);
    	this.getColumn("idCodigoVenta").setWidth(new Double(125));
    	
    	this.getColumn("fechaDocumento").setHeaderCaption("Fecha");
    	this.getColumn("fechaDocumento").setSortable(false);
    	this.getColumn("fechaDocumento").setWidth(new Double(125));
    	
    	this.getColumn("referencia").setHeaderCaption("Referencia");
    	this.getColumn("referencia").setSortable(false);
    	this.getColumn("referencia").setWidth(new Double(125));
    	
    	this.getColumn("cliente").setHeaderCaption("Cliente");
    	this.getColumn("cliente").setSortable(false);
    	this.getColumn("cliente").setWidth(new Double(110));
    	
    	this.getColumn("nombreCliente").setHeaderCaption("Nombre");
    	this.getColumn("nombreCliente").setSortable(false);
    	this.getColumn("nombreCliente").setWidth(450);
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(110));
    	
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(450);

    	this.getColumn("unidades").setHeaderCaption("Unidades");
    	this.getColumn("unidades").setSortable(false);
    	this.getColumn("unidades").setWidth(new Double(110));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("pais").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("claveTabla").setHidden(true);
    	this.getColumn("direccion").setHidden(true);
    	this.getColumn("codigoPostal").setHidden(true);
    	this.getColumn("descripcion").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("lote").setHidden(true);
    	this.getColumn("ubicacion").setHidden(true);
    	this.getColumn("articulo").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("idCodigoVenta".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) ) 
            	{
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fechaDocumento");
		this.camposNoFiltrar.add("unidades");
		this.camposNoFiltrar.add("nif");
		
//		this.camposNoFiltrar.add("almacen");
//		this.camposNoFiltrar.add("nombreAlmacen");
//		this.camposNoFiltrar.add("cae");
//		this.camposNoFiltrar.add("codigoPostal");
//		this.camposNoFiltrar.add("direccion");
//		this.camposNoFiltrar.add("descripcion");		
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}


