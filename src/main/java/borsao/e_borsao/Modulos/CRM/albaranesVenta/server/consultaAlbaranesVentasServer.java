package borsao.e_borsao.Modulos.CRM.albaranesVenta.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;

public class consultaAlbaranesVentasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAlbaranesVentasServer instance;
//	private static int litrosMinimos=consultaMovimientosSilicieServer.litrosMinimos;	
	
	public consultaAlbaranesVentasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlbaranesVentasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlbaranesVentasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosOpcionesGlobal(MapeoLineasAlbaranesVentas r_mapeo, Integer r_almacen)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		Connection con = null;
		Statement cs = null;
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_mapeo.getEjercicio()!=null)
		{
			if (r_mapeo.getEjercicio().intValue() == ejercicioActual.intValue()) digito="0";
			if (r_mapeo.getEjercicio().intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" a.ref_cliente det_ref, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, e_client c, a_va_c_" + digito + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
     	cadenaSQL.append(" AND p.almacen = " + r_almacen);
     	
     	if (r_mapeo.getArticulo()!=null)
     	{
     		cadenaSQL.append(" AND (p.articulo = '" + r_mapeo.getArticulo() + "'))");
     	}
     	if (r_mapeo.getPais()!=null)
     	{
     		cadenaSQL.append(" AND (a.pais = '" + r_mapeo.getPais() + "'))");
     	}
     	if (r_mapeo.getCliente()!=null)
     	{
     		cadenaSQL.append(" AND (c.cliente = '" + r_mapeo.getCliente() + "'))");
     	}
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setCliente(rsOpcion.getInt("det_cli"));
				mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public String traerUltimoLoteSalida(String r_articulo)
	{
		
		ResultSet rsOpcion = null;		
		String numeroLote = null;
		Statement cs = null;
		Connection con = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(numero_lote) det_lot ");
		cadenaSQL.append(" FROM a_lin_0 ");
		cadenaSQL.append(" WHERE articulo = '" + r_articulo + "' ");
		cadenaSQL.append(" AND movimiento in ('51','53') ");
		cadenaSQL.append(" AND clave_tabla in ('A','F') ");
		cadenaSQL.append(" AND almacen in (1,4) ");
		cadenaSQL.append(" AND receptor_emisor in (select cliente from e_client ");
		cadenaSQL.append(" where estadistico_a1='2') ");
		
		try
		{
			con=this.conManager.establecerConexionInd();
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				numeroLote = rsOpcion.getString("det_lot");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return numeroLote;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosOpcionesVentasAgrupadas(Integer r_ejercicio, String r_articulosPadre)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		Connection con = null;
		Statement cs = null;
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio!=null)
		{
			if (r_ejercicio.intValue() == ejercicioActual.intValue()) digito="0";
			if (r_ejercicio.intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_ejercicio);
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" a.descrip_articulo det_des, ");        
        cadenaSQL.append(" p.ubicacion det_ubi, ");
        cadenaSQL.append(" sum(p.unidades) det_uds ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, e_articu a ");
     	cadenaSQL.append(" WHERE p.articulo = a.articulo  ");
     	cadenaSQL.append(" AND p.movimiento in ('51','53')  ");
     	if (r_articulosPadre.startsWith("01")) cadenaSQL.append(" and p.articulo in (select padre from e__lconj where hijo = '"+r_articulosPadre+"' and padre matches '0102*') ");
     	else  cadenaSQL.append(" and p.articulo in (select padre from e__lconj where descripcion matches '*"+r_articulosPadre+"*' and padre matches '0102*') ");
//     	cadenaSQL.append(" and p.almacen in (1,4) ");
     	cadenaSQL.append(" and p.ubicacion <> '' and p.ubicacion is not null" );
     	cadenaSQL.append(" group by 1,2,3,4 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				if (rsOpcion.getString("det_ubi")!=null && rsOpcion.getString("det_ubi").length()>0) mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public HashMap<String, Double> datosOpcionesVentasPadres(Integer r_ejercicio, String r_articulosPadre)
	{
		
		ResultSet rsOpcion = null;		
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio!=null)
		{
			if (r_ejercicio.intValue() == ejercicioActual.intValue()) digito="0";
			if (r_ejercicio.intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_ejercicio);
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" sum(p.unidades) det_uds ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p ");
     	cadenaSQL.append(" WHERE p.almacen in (1,4) ");
     	if (r_articulosPadre.startsWith("01")) cadenaSQL.append(" and p.articulo in (select padre from e__lconj where hijo = '"+r_articulosPadre+"' and padre matches '0102*') ");
     	else  cadenaSQL.append(" and p.articulo in (select padre from e__lconj where descripcion matches '*"+r_articulosPadre+"*' and padre matches '0102*') ");
     	cadenaSQL.append(" and p.movimiento in ('51','53') ");
     	cadenaSQL.append(" group by 1,2 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("det_art").trim(), rsOpcion.getDouble("det_uds"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosOpcionesGlobal(MapeoLineasAlbaranesVentas r_mapeo)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		Connection con = null;
		Statement cs = null;

		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_mapeo.getEjercicio()!=null)
		{
			if (r_mapeo.getEjercicio().intValue() == ejercicioActual.intValue()) digito="0";
			if (r_mapeo.getEjercicio().intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" a.ref_cliente det_ref, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi, ");        
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, e_client c, a_va_c_" + digito + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
     	cadenaSQL.append(" AND a.clave_tabla in ('A','F') ");
     	cadenaSQL.append(" AND p.movimiento in ('51','53') ");
     	
     	if (r_mapeo.getArticulo()!=null)
     	{
     		cadenaSQL.append(" AND (p.articulo = '" + r_mapeo.getArticulo() + "') ");
     	}
     	if (r_mapeo.getPais()!=null)
     	{
     		cadenaSQL.append(" AND (a.pais = '" + r_mapeo.getPais() + "') ");
     	}
     	if (r_mapeo.getCliente()!=null)
     	{
     		cadenaSQL.append(" AND (c.cliente = '" + r_mapeo.getCliente() + "') ");
     	}
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setCliente(rsOpcion.getInt("det_cli"));
				mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				mapeo.setLote(rsOpcion.getBigDecimal("det_lot"));
				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosOpcionesGlobal(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran )
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		Connection con = null;
		Statement cs = null;

		
		StringBuffer cadenaSQL = new StringBuffer();
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" c.cliente det_cli, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" a.ref_cliente det_ref, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_lin_" + digito +" p, e_client c, a_va_c_" + digito + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
     	
     	if (r_clave_tabla!=null)
     	{
     		cadenaSQL.append(" AND p.clave_tabla = '" + r_clave_tabla + "' ");
     	}
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" AND p.documento = '" + r_documento + "' ");
     	}
     	if (r_serie!=null)
     	{
     		cadenaSQL.append(" AND p.serie = '" + r_serie + "' ");
     	}
     	if (r_albaran!=null)
     	{
     		cadenaSQL.append(" AND p.codigo = " + r_albaran);
     	}
     	
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
				mapeo.setCliente(rsOpcion.getInt("det_cli"));
				mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosOpcionesGlobalSinEMCS(String r_area, Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Date r_fecha, Integer r_almacen )
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		try
		{
			cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
			cadenaSQL.append(" p.fecha_documento det_fec, ");
	        cadenaSQL.append(" a.cliente det_cli, ");
	        cadenaSQL.append(" a.nombre_comercial det_nom, ");
//	        cadenaSQL.append(" a.direccion det_dir, ");
//	        cadenaSQL.append(" a.cod_postal det_cp, ");
	        cadenaSQL.append(" a.cif det_cif, ");
	        cadenaSQL.append(" a.serie det_ser, ");
	        cadenaSQL.append(" a.pais det_pai, ");
	        cadenaSQL.append(" p.documento det_doc, ");
	        cadenaSQL.append(" p.codigo det_cod, ");
	        cadenaSQL.append(" p.clave_tabla det_ct, ");
	//        cadenaSQL.append(" p.articulo det_art, ");
	//        cadenaSQL.append(" p.descrip_articulo det_des, ");
	        cadenaSQL.append(" sum(p.unidades * e.marca) det_uds ");
	//        cadenaSQL.append(" p.almacen det_alm ");
	     	cadenaSQL.append(" FROM a_lin_" + digito +" p, a_va_c_" + digito + " a, e_articu e, e_client t ");
	     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
	     	cadenaSQL.append(" AND a.documento = p.documento ");
	     	cadenaSQL.append(" AND a.serie = p.serie ");
	     	cadenaSQL.append(" AND a.codigo = p.codigo ");
	     	if (r_clave_tabla!=null)
	     	{
	     		cadenaSQL.append(" AND p.clave_tabla = '" + r_clave_tabla + "' ");
	     	}
	     	if (r_documento!=null && r_documento.length()>0)
	     	{
	     		cadenaSQL.append(" AND p.documento = '" + r_documento + "' ");
	     	}
	     	else
	     	{
	     		cadenaSQL.append(" AND p.documento not in ('e','a','A','E') ");
	     	}
	     	if (r_serie!=null)
	     	{
	     		cadenaSQL.append(" AND p.serie = '" + r_serie + "' ");
	     	}
	     	if (r_albaran!=null)
	     	{
	     		cadenaSQL.append(" AND p.codigo = " + r_albaran);
	     	}
	     	if (r_almacen!=null)
	     	{
	     		cadenaSQL.append(" AND p.almacen = " + r_almacen);
	     	}
	     	if (r_fecha!=null)
	     	{
	     		cadenaSQL.append(" AND p.fecha_documento <= '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
	     	}
	     	cadenaSQL.append(" AND e.articulo = p.articulo ");
	     	cadenaSQL.append(" AND p.receptor_emisor = t.cliente ");
	     	cadenaSQL.append(" AND a.calidad = 'N' ");
	     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0103*' or p.articulo matches '0111*' or p.articulo matches '0101*') ");  
	     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
	//     	cadenaSQL.append(" AND (a.documento = 'e' or (a.documento = 'n' and a.cliente = 431264) or (a.documento='n' and a.provincia in ('07','35','38')) or (a.documento='n' and t.nom_abreviado <> '') ) ");
	     	cadenaSQL.append(" AND t.estadistico_b3 in ('" + this.obtenerTipo(r_area) + "') "); 
	     	
	     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10 ");
//	     	cadenaSQL.append(" having (sum(p.unidades*e.marca)>= " + litrosMinimos + " or sum(p.unidades)=0 ) " );
	     	
	     	cadenaSQL.append(" UNION ALL ");
	     	
			cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
			cadenaSQL.append(" p.fecha_documento det_fec, ");
	        cadenaSQL.append(" a.cliente det_cli, ");
	        cadenaSQL.append(" a.nombre_comercial det_nom, ");
//	        cadenaSQL.append(" a.direccion det_dir, ");
//	        cadenaSQL.append(" a.cod_postal det_cp, ");
	        cadenaSQL.append(" a.cif det_cif, ");
	        cadenaSQL.append(" a.serie det_ser, ");
	        cadenaSQL.append(" a.pais det_pai, ");
	        cadenaSQL.append(" p.documento det_doc, ");
	        cadenaSQL.append(" p.codigo det_cod, ");
	        cadenaSQL.append(" p.clave_tabla det_ct, ");
	//        cadenaSQL.append(" p.articulo det_art, ");
	//        cadenaSQL.append(" p.descrip_articulo det_des, ");
	        cadenaSQL.append(" sum(p.unidades * e.marca) det_uds ");
	//        cadenaSQL.append(" p.almacen det_alm ");
	        cadenaSQL.append(" FROM a_lin_" + digito +" p, a_va_c_" + digito + " a, e_articu e, e_client t ");
	     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
	     	cadenaSQL.append(" AND a.documento = p.documento ");
	     	cadenaSQL.append(" AND a.serie = p.serie ");
	     	cadenaSQL.append(" AND a.codigo = p.codigo ");
	     	if (r_clave_tabla!=null)
	     	{
	     		cadenaSQL.append(" AND p.clave_tabla = '" + r_clave_tabla + "' ");
	     	}
	     	if (r_documento!=null && r_documento.length()>0)
	     	{
	     		cadenaSQL.append(" AND p.documento = '" + r_documento + "' ");
	     	}
	     	else
	     	{
	     		cadenaSQL.append(" AND p.documento in ('e','E') ");
	     	}
	     	if (r_serie!=null)
	     	{
	     		cadenaSQL.append(" AND p.serie = '" + r_serie + "' ");
	     	}
	     	if (r_albaran!=null)
	     	{
	     		cadenaSQL.append(" AND p.codigo = " + r_albaran);
	     	}
	     	if (r_almacen!=null)
	     	{
	     		cadenaSQL.append(" AND p.almacen = " + r_almacen);
	     	}
	     	if (r_fecha!=null)
	     	{
	     		cadenaSQL.append(" AND p.fecha_documento <= '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
	     	}

	     	cadenaSQL.append(" AND e.articulo = p.articulo ");
	     	cadenaSQL.append(" AND p.receptor_emisor = t.cliente ");
	     	cadenaSQL.append(" AND a.calidad = 'N' ");
	     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0103*' or p.articulo matches '0111*' or p.articulo matches '0101*') ");  
	     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
	     	cadenaSQL.append(" AND t.estadistico_b3 in ('" + this.obtenerTipo(r_area) + "') "); 
	     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10 ");
	     	
			
	     	con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesVentas();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				
				mapeo.setClaveTabla(rsOpcion.getString("det_ct"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setSerie(rsOpcion.getString("det_ser"));				
				mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
				
				mapeo.setCliente(rsOpcion.getInt("det_cli"));
				mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
//				mapeo.setDireccion(rsOpcion.getString("det_dir"));
//				mapeo.setCodigoPostal(rsOpcion.getString("det_cp"));
				mapeo.setNif(rsOpcion.getString("det_cif"));
				
				mapeo.setArticulo("PT");
//				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
//				mapeo.setReferencia(rsOpcion.getString("det_ref"));
//				mapeo.setAlmacen(rsOpcion.getInt("det_alm"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesVentas> datosVentasLote(String r_articulo, String r_lote, StringBuilder r_lotePS, String r_cliente, ArrayList<String> r_columnas)
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		int cuantos = 0;
		boolean cc = false;
		boolean fd = false;
		boolean id = false;
		boolean ud = false;
		boolean lt = false;
		boolean rf = false;
		
		
		/*
		 *     	this.getColumn("nombreCliente").setHidable(true);
    	 *		this.getColumn("fechaDocumento").setHidable(true);
    	 *		this.getColumn("idCodigoVenta").setHidable(true);
    	 *		this.getColumn("unidades").setHidable(true);
    	 *		this.getColumn("lote").setHidable(true);
		 */
		for (int i=0; i< r_columnas.size();i++)
		{
			String col = r_columnas.get(i).toString();
			
			switch (col)
			{
				case "cliente":
				{
					cc = true;
					cuantos = cuantos + 3;
					break;
				}
				case "fechaDocumento":
				{
					fd = true;
					cuantos = cuantos + 1;
					break;
				}
				case "idCodigoVenta":
				{
					id = true;
					cuantos = cuantos + 1;
					break;
				}
				case "unidades":
				{
					ud = true;
					break;
				}
				case "lote":
				{
					lt = true;
					cuantos = cuantos + 1;
					break;
				}
				case "referencia":
				{
					rf = true;
					cuantos = cuantos + 1;
					break;
				}

			}
		}
		
		
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			vector = new ArrayList<MapeoLineasAlbaranesVentas>();
			
			for (int ej=5;ej>=0;ej--)
			{
				StringBuffer cadenaSQL = new StringBuffer();
	
							cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
							cadenaSQL.append(" a.pais det_pai, ");
							cadenaSQL.append(" p.documento det_doc, ");
							cadenaSQL.append(" p.articulo det_art, ");
							cadenaSQL.append(" p.descrip_articulo det_des, ");
				if (fd) 	cadenaSQL.append(" p.fecha_documento det_fec, ");
		        if (cc) 	cadenaSQL.append(" c.cliente det_cli, ");
		        if (cc) 	cadenaSQL.append(" c.nombre_comercial det_nom, ");
		        if (cc) 	cadenaSQL.append(" c.telefono1 det_tel, ");
		        if (rf) 	cadenaSQL.append(" a.ref_cliente det_ref, ");
		        if (id) 	cadenaSQL.append(" p.codigo det_cod, ");
		        if (lt) 	cadenaSQL.append(" p.numero_lote det_lot, ");
		        
		        if (ud)
		        {
		        			cadenaSQL.append(" p.almacen det_alm, ");
		        			cadenaSQL.append(" sum(p.unidades) det_uds ");
		        }
		        else
		        {
		        			cadenaSQL.append(" p.almacen det_alm ");	
		        }
		     				cadenaSQL.append(" FROM a_lin_" + String.valueOf(ej) + " p, e_client c, a_va_c_" + String.valueOf(ej) + " a ");
		 					cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
		 					cadenaSQL.append(" AND a.documento = p.documento ");
		 					cadenaSQL.append(" AND a.serie = p.serie ");
		 					cadenaSQL.append(" AND a.codigo = p.codigo ");
		 					cadenaSQL.append(" AND p.receptor_emisor = c.cliente ");
		 					cadenaSQL.append(" AND a.clave_tabla in ('A','F') ");
		     	
		     	if ((r_articulo==null || r_articulo.length()==0) && (r_lote==null || r_lote.length()==0) && (r_lotePS==null || r_lotePS.length()==0) && (r_cliente==null || r_cliente.length()==0)) return null;
		     	
		     	if (r_articulo!=null &&  r_articulo.length()>0)
		     	{
		     		cadenaSQL.append(" AND (p.articulo = '" + r_articulo.trim() + "') ");
		     	}
		     	if (r_cliente!=null &&  r_cliente.length()>0)
		     	{
		     		cadenaSQL.append(" AND (c.cliente = " + r_cliente.trim() + ") ");
		     	}
		     	if (r_lote!=null &&  r_lote.length()>0)
		     	{
		     		cadenaSQL.append(" AND (p.numero_lote between " + RutinasCadenas.formatCerosDerecha(r_lote.trim(),10) + " and " + RutinasCadenas.formatNuevesDerecha(r_lote.trim(),10) + ") ");
		     	}
		     	
		     	if (r_lotePS!=null &&  r_lotePS.length()>0)
		     	{
		     		cadenaSQL.append(" AND (p.numero_lote in (" +  r_lotePS.toString() + ")) ");
		     	}
		
		     	if (cuantos!=0)
		     	{
		     		cadenaSQL.append(" group by 1,2,3,4,5,6" );
		     	
		     		for (int j = 0 ; j < cuantos; j++)
		     		{
		     			cadenaSQL.append(" ," + (j+7) );
		     		}
		     	}
		     	else
		     	{
		     		cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10,11,12,13" );
		     	}
	     	
		
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoLineasAlbaranesVentas();
					mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
					if (fd) mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
					if (cc) mapeo.setCliente(rsOpcion.getInt("det_cli"));
					if (cc) mapeo.setNombreCliente(rsOpcion.getString("det_nom"));
					if (cc) mapeo.setTelefono(rsOpcion.getString("det_tel"));
					mapeo.setDocumento(rsOpcion.getString("det_doc"));
					if (id) mapeo.setIdCodigoVenta(rsOpcion.getInt("det_cod"));
					mapeo.setArticulo(rsOpcion.getString("det_art"));
					mapeo.setDescripcion(rsOpcion.getString("det_des"));
					mapeo.setPais(rsOpcion.getString("det_pai"));
					mapeo.setAlmacen(rsOpcion.getInt("det_alm"));
					if (ud) mapeo.setUnidades(rsOpcion.getInt("det_uds"));
					if (lt) mapeo.setLote(rsOpcion.getBigDecimal("det_lot"));
					if (rf) mapeo.setReferencia(rsOpcion.getString("det_ref"));
					
					vector.add(mapeo);
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public boolean marcarCabeceraAlbaranEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		Connection con = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		try
		{
	        
			cadenaSQL.append(" UPDATE a_va_c_" + ejer + " set ");
    		cadenaSQL.append(" calidad = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "S" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return true;
	}

	public boolean marcarAlbaranSilicie(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		Connection con = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
    		cadenaSQL.append(" observacion[1] = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return true;
	}

	public boolean liberarCabeceraAlbaranEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		Connection con = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_va_c_" + ejer + " set ");
    		cadenaSQL.append(" calidad = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}

	private String obtenerTipo(String r_area)
	{

		switch (r_area)
		{
			case "EMCS Interno":
			{
				return "1','3";
			}
			case "EMCS Europa":
			{
				return "2','3";
			}
		}
		return null;
	}
	
	public boolean chequeoSalidasSinEMCS(Integer r_ejercicio, String r_cae_titular, Integer r_mes) 
	{

		ResultSet rsOpcion = null;
		String rdo = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT distinct a.codigo alb, ");
		cadenaSQL.append(" a.documento doc ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_va_c_" + digito + " a, e_articu e, e_almac w, e_client t ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.cliente = t.cliente ");
     	cadenaSQL.append(" AND w.almacen = p.almacen and w.encargado = '" + r_cae_titular + "' ");     
     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
     	cadenaSQL.append(" AND (p.articulo matches '0101*' or p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*') ");
     	cadenaSQL.append(" AND a.calidad='N' ");     	
     	cadenaSQL.append(" AND (t.estadistico_b3 in ('1','2') or t.estadistico_b3 is null or t.estadistico_b3 = '') " );
		try
		{
			cadenaSQL.append(" AND month(a.fecha_documento) = " + r_mes);
//			cadenaSQL.append(" AND ( (a.documento NOT IN ('e','a','E','A') and (p.unidades*e.marca)>= " + litrosMinimos + " ) OR a.documento IN ('e','E') ) " );
	     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rdo== null) rdo="";
				rdo = rdo + " " + rsOpcion.getString("doc") + "-" + rsOpcion.getInt("alb");
				Notificaciones.getInstance().mensajeSeguimiento("Albaranes Venta Erroneos " + rsOpcion.getString("doc") + "-" + rsOpcion.getInt("alb"));
			}
			if (rdo!= null) Notificaciones.getInstance().mensajeError("Albaranes Venta Erroneos " + rdo);
			if (rdo!= null) return false;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return true;
	}
	
	@Override
	public String semaforos() 
	{

		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT distinct a.codigo alb, ");
		cadenaSQL.append(" a.documento doc ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_va_c_0 a, e_articu e, e_almac w, e_client t ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.cliente = t.cliente ");
     	cadenaSQL.append(" AND a.documento NOT IN ('e','a') " );
     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*') ");
     	cadenaSQL.append(" AND a.calidad='N' ");     	
     	cadenaSQL.append(" AND (t.estadistico_b3 in ('1','2') or t.estadistico_b3 is null or t.estadistico_b3 = '') " );
		try
		{
			cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 1) + "' ");
//			cadenaSQL.append(" and (p.unidades*e.marca)>= " + litrosMinimos);

			cadenaSQL.append(" UNION ALL ");
				
			cadenaSQL.append(" SELECT distinct a.codigo alb, ");
			cadenaSQL.append(" a.documento doc ");
	     	cadenaSQL.append(" FROM a_lin_0 p, a_va_c_0 a, e_articu e, e_almac w, e_client t ");
	     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
	     	cadenaSQL.append(" AND a.documento = p.documento ");
	     	cadenaSQL.append(" AND a.serie = p.serie ");
	     	cadenaSQL.append(" AND a.codigo = p.codigo ");
	     	cadenaSQL.append(" AND e.articulo = p.articulo ");
	     	cadenaSQL.append(" AND a.cliente = t.cliente ");
	     	cadenaSQL.append(" AND a.documento IN ('e') " );
	     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
	     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*') ");
	     	cadenaSQL.append(" AND a.calidad='N' ");     	
	     	cadenaSQL.append(" AND (t.estadistico_b3 in ('1','2') or t.estadistico_b3 is null or t.estadistico_b3 = '') " );

	     	cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 1) + "' ");
			
	     	con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return "2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "1";
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return "0";
	}
}
