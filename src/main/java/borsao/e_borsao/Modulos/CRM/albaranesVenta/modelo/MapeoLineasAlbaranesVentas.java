package borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo;

import java.math.BigDecimal;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoLineasAlbaranesVentas extends MapeoGlobal
{
	
    private Integer ejercicio;
    private Date fechaDocumento;
    private String referencia;
	private Integer cliente;
	private String nombreCliente;
	private String telefono;
	private String direccion;
	private String codigoPostal;
	private String nif;
	private String pais;
	private String documento;
	private String claveTabla;
	private Integer idCodigoVenta;
	private String articulo;
	private String serie;
	private String descripcion;
	private String ubicacion;
	private BigDecimal lote;
	private Integer unidades;
	private Integer almacen;
	
	public MapeoLineasAlbaranesVentas()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getFechaDocumento() {
		if (fechaDocumento!=null)
		{
			return RutinasFechas.convertirDateToString(fechaDocumento);
		}
		return null;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public Integer getCliente() {
		return cliente;
	}

	public void setCliente(Integer cliente) {
		this.cliente = cliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getIdCodigoVenta() {
		return idCodigoVenta;
	}

	public void setIdCodigoVenta(Integer idCodigoVenta) {
		this.idCodigoVenta = idCodigoVenta;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getLote() {
		if (lote!=null)	return lote.toString();
		return null;
	}

	public void setLote(BigDecimal lote) {
		this.lote = lote;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getClaveTabla() {
		return claveTabla;
	}

	public void setClaveTabla(String claveTabla) {
		this.claveTabla = claveTabla;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

}