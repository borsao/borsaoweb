package borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo.MapeoVariedadesDga;

public class consultaVariedadesDgaServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaVariedadesDgaServer instance;
	
	public consultaVariedadesDgaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaVariedadesDgaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaVariedadesDgaServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoVariedadesDga> datosVariedadesDgaGlobal(MapeoVariedadesDga r_mapeo)
	{
		ResultSet rsVariedadesDga = null;		
		ArrayList<MapeoVariedadesDga> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT dga_variedades_uva.id var_id, ");
		cadenaSQL.append(" dga_variedades_uva.codigo var_cod, ");
		cadenaSQL.append(" dga_variedades_uva.descripcion var_des ");
     	cadenaSQL.append(" FROM dga_variedades_uva ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsVariedadesDga= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoVariedadesDga>();
			
			while(rsVariedadesDga.next())
			{
				MapeoVariedadesDga mapeoVariedadesDga = new MapeoVariedadesDga();
				/*
				 * recojo mapeo operarios
				 */
				mapeoVariedadesDga.setIdCodigo(rsVariedadesDga.getInt("var_id"));
				mapeoVariedadesDga.setCodigo(rsVariedadesDga.getString("var_cod"));
				mapeoVariedadesDga.setDescripcion(rsVariedadesDga.getString("var_des"));
				vector.add(mapeoVariedadesDga);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerIdVariedad(String r_nombre)
	{
		ResultSet rsVariedadesDga = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT dga_variedades_uva.id var_id ");
     	cadenaSQL.append(" FROM dga_variedades_uva ");
     	cadenaSQL.append(" where descripcion = '" + r_nombre + "' ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsVariedadesDga= cs.executeQuery(cadenaSQL.toString());
			
			while(rsVariedadesDga.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				return rsVariedadesDga.getInt("var_id");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsVariedadesDga = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(dga_variedades_uva.id) var_sig ");
     	cadenaSQL.append(" FROM dga_variedades_uva ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsVariedadesDga= cs.executeQuery(cadenaSQL.toString());
			
			while(rsVariedadesDga.next())
			{
				return rsVariedadesDga.getInt("var_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoVariedadesDga r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO dga_variedades_uva ( ");
    		cadenaSQL.append(" dga_variedades_uva.id, ");
    		cadenaSQL.append(" dga_variedades_uva.codigo, ");
    		cadenaSQL.append(" dga_variedades_uva.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoVariedadesDga r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE dga_variedades_uva set ");
			cadenaSQL.append(" dga_variedades_uva.codigo=?, ");
		    cadenaSQL.append(" dga_variedades_uva.descripcion=? ");
		    cadenaSQL.append(" WHERE dga_variedades_uva.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoVariedadesDga r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM dga_variedades_uva ");            
			cadenaSQL.append(" WHERE dga_variedades_uva.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}