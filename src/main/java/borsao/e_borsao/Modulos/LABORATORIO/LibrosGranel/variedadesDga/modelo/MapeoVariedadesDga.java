package borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoVariedadesDga extends MapeoGlobal
{
	private String codigo;
	private String descripcion;

	public MapeoVariedadesDga()
	{
		this.setCodigo("");
		this.setDescripcion("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}