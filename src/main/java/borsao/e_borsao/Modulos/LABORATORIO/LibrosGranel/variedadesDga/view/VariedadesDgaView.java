package borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo.MapeoVariedadesDga;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo.VariedadesDgaGrid;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.server.consultaVariedadesDgaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class VariedadesDgaView extends GridViewRefresh {

	public static final String VIEW_NAME = "Variedades DGA";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoVariedadesDga> r_vector=null;
    	MapeoVariedadesDga MapeoVariedadesDga =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	MapeoVariedadesDga=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaVariedadesDgaServer cus = new consultaVariedadesDgaServer(CurrentUser.get());
    	r_vector=cus.datosVariedadesDgaGlobal(MapeoVariedadesDga);
    	
    	if (r_vector!=null && r_vector.size()>0)
    	{
    		grid = new VariedadesDgaGrid(r_vector);
			setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public VariedadesDgaView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
    }

    public void newForm()
    {    	
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarVariedadesDga((MapeoVariedadesDga) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }

    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
	}

	@Override
	public void cancelarProceso(String r_accion) {
	}
}
