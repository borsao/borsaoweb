package borsao.e_borsao.Modulos.LABORATORIO.Analiticas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo.MapeoAnaliticas;
import borsao.e_borsao.Modulos.LABORATORIO.Localizaciones.server.consultaLocalizacionesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaAnaliticasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaAnaliticasServer instance;
	
	public consultaAnaliticasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAnaliticasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAnaliticasServer(r_usuario);			
		}
		return instance;
	}
	

	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsDepositos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select codigo, tipo from localizaciones order by codigo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsDepositos= cs.executeQuery(sql);
			while(rsDepositos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsDepositos.getString("codigo"));
				mA.setDescripcion(rsDepositos.getString("tipo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsDepositos!=null)
				{
					rsDepositos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public ArrayList<String> cargarEjercicios()
	{
		ResultSet rsUser = null;	
		ArrayList<String> ejer = null;
		String sql = null;
		try
		{
			ejer = new ArrayList<String>();
			sql="select distinct ejercicio as eje from vi_ejercicios order by ejercicio desc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsUser = cs.executeQuery(sql);
			while(rsUser.next())
			{
				ejer.add(rsUser.getString("eje"));				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsUser!=null)
				{
					rsUser.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return ejer;
	}
	
	
	public ArrayList<MapeoAnaliticas> datosAnaliticasGlobal(MapeoAnaliticas r_mapeoAnaliticas)
	{
		ResultSet rsAnaliticas = null;		
		ArrayList<MapeoAnaliticas> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT analiticas.codigo anal_id, ");
        cadenaSQL.append(" analiticas.fecha anal_fec, ");
	    cadenaSQL.append(" analiticas.vino anal_vin, ");
	    cadenaSQL.append(" analiticas.sulfuroso anal_sul, ");
	    cadenaSQL.append(" analiticas.tecSulfuroso anal_tec_sul, ");
	    cadenaSQL.append(" analiticas.sulfurosoT anal_sult, ");
	    cadenaSQL.append(" analiticas.tecSulfurosoT anal_tec_sult, ");
	    cadenaSQL.append(" analiticas.azucar anal_azu, ");
	    cadenaSQL.append(" analiticas.tecAzucar anal_tec_azu, ");
	    cadenaSQL.append(" analiticas.grado anal_gra, ");
	    cadenaSQL.append(" analiticas.tecGrado anal_tec_gra, ");
	    cadenaSQL.append(" analiticas.glufru anal_glu, ");
	    cadenaSQL.append(" analiticas.tecGlufru anal_tec_glu, ");
	    cadenaSQL.append(" analiticas.acidezT anal_aci, ");
	    cadenaSQL.append(" analiticas.tecAcidezT anal_tec_aci, ");
	    cadenaSQL.append(" analiticas.ph anal_ph, ");
	    cadenaSQL.append(" analiticas.tecPh anal_tec_ph, ");
	    cadenaSQL.append(" analiticas.malico anal_mal, ");
	    cadenaSQL.append(" analiticas.tecMalico anal_tec_mal, ");
	    cadenaSQL.append(" analiticas.color anal_col, ");
	    cadenaSQL.append(" analiticas.tecColor anal_tec_col, ");
	    cadenaSQL.append(" analiticas.polif anal_pol, ");
	    cadenaSQL.append(" analiticas.tecPolif anal_tec_pol, ");
	    cadenaSQL.append(" analiticas.responsable anal_res, ");
	    cadenaSQL.append(" analiticas.observaciones anal_obs, ");
	    cadenaSQL.append(" analiticas.litros anal_lit, ");
	    cadenaSQL.append(" analiticas.lnkDeposito anal_dep,");
	    cadenaSQL.append(" analiticas.tipoAnalitica anal_tp ");
     	cadenaSQL.append(" FROM analiticas ");

		try
		{
			if (r_mapeoAnaliticas!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeoAnaliticas.getTipoAnalitica()!=null && r_mapeoAnaliticas.getTipoAnalitica().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoAnalitica = '" + r_mapeoAnaliticas.getTipoAnalitica() + "' ");
				}
				if (r_mapeoAnaliticas.getFecha()!=null && r_mapeoAnaliticas.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeoAnaliticas.getFecha())) + "' ");
				}
				if (r_mapeoAnaliticas.getLnkDeposito()!=null && r_mapeoAnaliticas.getLnkDeposito().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lnkDeposito = '" + r_mapeoAnaliticas.getLnkDeposito() + "' ");
				}
				if (r_mapeoAnaliticas.getVino()!=null && r_mapeoAnaliticas.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino like '%" + r_mapeoAnaliticas.getVino() + "%' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by tipoAnalitica desc, fecha desc, lnkDeposito asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsAnaliticas.TYPE_SCROLL_SENSITIVE,rsAnaliticas.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsAnaliticas = cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoAnaliticas>();
			
			while(rsAnaliticas.next())
			{
				MapeoAnaliticas mapeoAnaliticas = new MapeoAnaliticas();
				/*
				 * recojo mapeo analiticas
				 */
				mapeoAnaliticas.setCodigo(new Integer(rsAnaliticas.getInt("anal_id")));
                mapeoAnaliticas.setFecha(RutinasFechas.convertirADate(rsAnaliticas.getString("anal_fec")));
                mapeoAnaliticas.setVino(rsAnaliticas.getString("anal_vin"));
                if (rsAnaliticas.getString("anal_sul")!=null)
                {
                	mapeoAnaliticas.setSulfuroso(new Double(rsAnaliticas.getDouble("anal_sul")));
                }
                else
                {
                	mapeoAnaliticas.setSulfuroso(null);
                }
                mapeoAnaliticas.setTecSulfuroso(rsAnaliticas.getString("anal_tec_sul"));
                if (rsAnaliticas.getString("anal_sult")!=null)
                {
                	mapeoAnaliticas.setSulfurosoT(new Double(rsAnaliticas.getDouble("anal_sult")));
                }
                else
                {
                	mapeoAnaliticas.setSulfurosoT(null);
                }
                mapeoAnaliticas.setTecSulfurosoT(rsAnaliticas.getString("anal_tec_sult"));
                if (rsAnaliticas.getString("anal_azu")!=null)
                {
                	mapeoAnaliticas.setAzucar(new Double(rsAnaliticas.getDouble("anal_azu")));
                }
                else
                {
                	mapeoAnaliticas.setAzucar(null);
                }
                mapeoAnaliticas.setTecAzucar(rsAnaliticas.getString("anal_tec_azu"));
                if (rsAnaliticas.getString("anal_gra")!=null)
                {
                	mapeoAnaliticas.setGrado(new Double(rsAnaliticas.getDouble("anal_gra")));
                }
                else
                {
                	mapeoAnaliticas.setGrado(null);
                }
                mapeoAnaliticas.setTecGrado(rsAnaliticas.getString("anal_tec_gra"));
                if (rsAnaliticas.getString("anal_glu")!=null)
                {
                	mapeoAnaliticas.setGlufru(new Double(rsAnaliticas.getDouble("anal_glu")));
                }
                else
                {
                	mapeoAnaliticas.setGlufru(null);
                }
                mapeoAnaliticas.setTecGlufru(rsAnaliticas.getString("anal_tec_glu"));
                if (rsAnaliticas.getString("anal_aci")!=null)
                {
                	mapeoAnaliticas.setAcidezt(new Double(rsAnaliticas.getDouble("anal_aci")));
                }
                else
                {
                	mapeoAnaliticas.setAcidezt(null);
                }
                mapeoAnaliticas.setTecAcidezt(rsAnaliticas.getString("anal_tec_aci"));
                if (rsAnaliticas.getString("anal_ph")!=null)
                {
                	mapeoAnaliticas.setPh(new Double(rsAnaliticas.getDouble("anal_ph")));
                }
                else
                {
                	mapeoAnaliticas.setPh(null);
                }
                mapeoAnaliticas.setTecPh(rsAnaliticas.getString("anal_tec_ph"));
                if (rsAnaliticas.getString("anal_mal")!=null)
                {
                	mapeoAnaliticas.setMalico(new Double(rsAnaliticas.getDouble("anal_mal")));
                }
                else
                {
                	mapeoAnaliticas.setMalico(null);
                }
                mapeoAnaliticas.setTecMalico(rsAnaliticas.getString("anal_tec_mal"));
                if (rsAnaliticas.getString("anal_col")!=null)
                {
                	mapeoAnaliticas.setColor(new Double(rsAnaliticas.getDouble("anal_col")));
                }
                else
                {
                	mapeoAnaliticas.setColor(null);
                }
                mapeoAnaliticas.setTecColor(rsAnaliticas.getString("anal_tec_col"));
                
                if (rsAnaliticas.getString("anal_pol")!=null)
                {
                	mapeoAnaliticas.setPolif(new Double(rsAnaliticas.getDouble("anal_pol")));
                }
                else
                {
                	mapeoAnaliticas.setPolif(null);
                }
                mapeoAnaliticas.setTecPolif(rsAnaliticas.getString("anal_tec_pol"));
                mapeoAnaliticas.setResponsable(rsAnaliticas.getString("anal_res"));
                mapeoAnaliticas.setObservaciones(rsAnaliticas.getString("anal_obs"));    
                if (rsAnaliticas.getString("anal_lit")!=null)
                {
                	mapeoAnaliticas.setLitros(new Double(rsAnaliticas.getInt("anal_lit")));
                }
                else
                {
                	mapeoAnaliticas.setLitros(null);
                }               
                mapeoAnaliticas.setLnkDeposito(rsAnaliticas.getString("anal_dep"));    
                mapeoAnaliticas.setTipoAnalitica(rsAnaliticas.getString("anal_tp"));
				vector.add(mapeoAnaliticas);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoAnaliticas parmMapeoAnaliticas)
	{
		PreparedStatement preparedStatement = null;
		serverBasico sv = null;
		Integer codigoInterno=null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
//		sv=new serverBasico();
		
		try
		{
			cadenaSQL.append(" INSERT INTO analiticas ( ");
    		cadenaSQL.append(" analiticas.codigo, ");
	        cadenaSQL.append(" analiticas.fecha, ");
		    cadenaSQL.append(" analiticas.vino, ");
		    cadenaSQL.append(" analiticas.sulfuroso, ");
		    cadenaSQL.append(" analiticas.tecSulfuroso, ");
		    cadenaSQL.append(" analiticas.sulfurosoT, ");
		    cadenaSQL.append(" analiticas.tecSulfurosoT, ");
		    cadenaSQL.append(" analiticas.azucar, ");
		    cadenaSQL.append(" analiticas.tecAzucar, ");
		    cadenaSQL.append(" analiticas.grado, ");
		    cadenaSQL.append(" analiticas.tecGrado, ");
		    cadenaSQL.append(" analiticas.glufru, ");
		    cadenaSQL.append(" analiticas.tecGlufru, ");
		    cadenaSQL.append(" analiticas.acidezT, ");
		    cadenaSQL.append(" analiticas.tecAcidezT, ");
		    cadenaSQL.append(" analiticas.ph, ");
		    cadenaSQL.append(" analiticas.tecPh, ");
		    cadenaSQL.append(" analiticas.malico, ");
		    cadenaSQL.append(" analiticas.tecMalico, ");
		    cadenaSQL.append(" analiticas.color, ");
		    cadenaSQL.append(" analiticas.tecColor, ");
		    cadenaSQL.append(" analiticas.polif, ");
		    cadenaSQL.append(" analiticas.tecPolif, ");
		    cadenaSQL.append(" analiticas.responsable, ");
		    cadenaSQL.append(" analiticas.observaciones, ");
		    cadenaSQL.append(" analiticas.litros, ");
		    cadenaSQL.append(" analiticas.lnkDeposito, ");
		    cadenaSQL.append(" analiticas.tipoAnalitica ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    codigoInterno = sv.obtenerSiguienteCodigoInterno(con, "analiticas", "codigo");
		    preparedStatement.setInt(1, codigoInterno.intValue());			    
		    preparedStatement.setDate(2, new java.sql.Date(parmMapeoAnaliticas.getFecha().getTime()));
		    preparedStatement.setString(3, parmMapeoAnaliticas.getVino());			    
		    if (parmMapeoAnaliticas.getSulfuroso()!=null)
		    {
		    	preparedStatement.setDouble(4, new Double(parmMapeoAnaliticas.getSulfuroso()));
		    }
		    else
		    {
		    	preparedStatement.setNull(4, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(5, parmMapeoAnaliticas.getTecSulfuroso());
		    if (parmMapeoAnaliticas.getSulfurosoT()!=null)
		    {
		    	preparedStatement.setDouble(6, new Double(parmMapeoAnaliticas.getSulfurosoT()));
		    }
		    else
		    {
		    	preparedStatement.setNull(6, java.sql.Types.NULL);
		    }
		    
		    preparedStatement.setString(7, parmMapeoAnaliticas.getTecSulfurosoT());
		    if (parmMapeoAnaliticas.getAzucar()!=null)
		    {
		    	preparedStatement.setDouble(8, new Double(parmMapeoAnaliticas.getAzucar()));
		    }
		    else
		    {
		    	preparedStatement.setNull(8, java.sql.Types.NULL);
		    }
		    
		    preparedStatement.setString(9, parmMapeoAnaliticas.getTecAzucar());
		    if (parmMapeoAnaliticas.getGrado()!=null)
		    {
		    	preparedStatement.setDouble(10, new Double(parmMapeoAnaliticas.getGrado()));
		    }
		    else
		    {
		    	preparedStatement.setNull(10, java.sql.Types.NULL);
		    }
		    
		    preparedStatement.setString(11, parmMapeoAnaliticas.getTecGrado());
		    if (parmMapeoAnaliticas.getGlufru()!=null)
		    {
			    preparedStatement.setDouble(12, new Double(parmMapeoAnaliticas.getGlufru()));
		    }
		    else
		    {
		    	preparedStatement.setNull(12, java.sql.Types.NULL);
		    }

		    preparedStatement.setString(13, parmMapeoAnaliticas.getTecGlufru());
		    if (parmMapeoAnaliticas.getAcidezt()!=null)
		    {
			    preparedStatement.setDouble(14, new Double(parmMapeoAnaliticas.getAcidezt()));
		    }
		    else
		    {
		    	preparedStatement.setNull(14, java.sql.Types.NULL);
		    }

		    preparedStatement.setString(15, parmMapeoAnaliticas.getTecAcidezt());			    			    
		    if (parmMapeoAnaliticas.getPh()!=null)
		    {
			    preparedStatement.setDouble(16, new Double(parmMapeoAnaliticas.getPh()));
		    }
		    else
		    {
		    	preparedStatement.setNull(16, java.sql.Types.NULL);
		    }

		    preparedStatement.setString(17, parmMapeoAnaliticas.getTecPh());
		    if (parmMapeoAnaliticas.getMalico()!=null)
		    {
		    	preparedStatement.setDouble(18, new Double(parmMapeoAnaliticas.getMalico()));
		    }
		    else
		    {
		    	preparedStatement.setNull(18, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(19, parmMapeoAnaliticas.getTecMalico());
		    if (parmMapeoAnaliticas.getColor()!=null)
		    {
		    	preparedStatement.setDouble(20, new Double(parmMapeoAnaliticas.getColor()));
		    }
		    else
		    {
		    	preparedStatement.setNull(20, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(21, parmMapeoAnaliticas.getTecColor());
		    if (parmMapeoAnaliticas.getPolif()!=null)
		    {
		    	preparedStatement.setDouble(22, new Double(parmMapeoAnaliticas.getPolif()));
		    }
		    else
		    {
		    	preparedStatement.setNull(22, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(23, parmMapeoAnaliticas.getTecPolif());
		    if (parmMapeoAnaliticas.getResponsable()!=null)
		    {
		    	preparedStatement.setString(24, parmMapeoAnaliticas.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(24, "M CH");
		    }
		    preparedStatement.setString(25, parmMapeoAnaliticas.getObservaciones());
		    if (parmMapeoAnaliticas.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(26, parmMapeoAnaliticas.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setNull(26, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(27, parmMapeoAnaliticas.getLnkDeposito());
		    preparedStatement.setString(28, parmMapeoAnaliticas.getTipoAnalitica());
	        
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoAnaliticas parmMapeoAnaliticas)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE analiticas set ");
	        cadenaSQL.append(" analiticas.fecha =?, ");
		    cadenaSQL.append(" analiticas.vino =?, ");
		    cadenaSQL.append(" analiticas.sulfuroso=?, ");
		    cadenaSQL.append(" analiticas.tecSulfuroso =?, ");
		    cadenaSQL.append(" analiticas.sulfurosoT =?, ");
		    cadenaSQL.append(" analiticas.tecSulfurosoT =?, ");
		    cadenaSQL.append(" analiticas.azucar =?, ");
		    cadenaSQL.append(" analiticas.tecAzucar =?, ");
		    cadenaSQL.append(" analiticas.grado =?, ");
		    cadenaSQL.append(" analiticas.tecGrado =?, ");
		    cadenaSQL.append(" analiticas.glufru =?, ");
		    cadenaSQL.append(" analiticas.tecGlufru =?, ");
		    cadenaSQL.append(" analiticas.acidezT =?, ");
		    cadenaSQL.append(" analiticas.tecAcidezT =?, ");
		    cadenaSQL.append(" analiticas.ph =?, ");
		    cadenaSQL.append(" analiticas.tecPh =?, ");
		    cadenaSQL.append(" analiticas.malico =?, ");
		    cadenaSQL.append(" analiticas.tecMalico =?, ");
		    cadenaSQL.append(" analiticas.color =?, ");
		    cadenaSQL.append(" analiticas.tecColor =?, ");
		    cadenaSQL.append(" analiticas.polif =?, ");
		    cadenaSQL.append(" analiticas.tecPolif =?, ");
		    cadenaSQL.append(" analiticas.responsable =?, ");
		    cadenaSQL.append(" analiticas.observaciones =?, ");
		    cadenaSQL.append(" analiticas.litros =?, ");
		    cadenaSQL.append(" analiticas.lnkDeposito =?, ");
		    cadenaSQL.append(" analiticas.tipoAnalitica =? ");
		    cadenaSQL.append(" WHERE analiticas.codigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setDate(1, new java.sql.Date(parmMapeoAnaliticas.getFecha().getTime()));
		    preparedStatement.setString(2, parmMapeoAnaliticas.getVino());
		    if (parmMapeoAnaliticas.getSulfuroso()!=null)
		    {
			    preparedStatement.setDouble(3, new Double(parmMapeoAnaliticas.getSulfuroso()));
		    }
		    else
		    {
		    	preparedStatement.setNull(3, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(4, parmMapeoAnaliticas.getTecSulfuroso());
		    if (parmMapeoAnaliticas.getSulfurosoT()!=null)
		    {
			    preparedStatement.setDouble(5, new Double(parmMapeoAnaliticas.getSulfurosoT()));
		    }
		    else
		    {
		    	preparedStatement.setNull(5, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(6, parmMapeoAnaliticas.getTecSulfurosoT());
		    if (parmMapeoAnaliticas.getAzucar()!=null)
		    {
			    preparedStatement.setDouble(7, new Double(parmMapeoAnaliticas.getAzucar()));
		    }
		    else
		    {
		    	preparedStatement.setNull(7, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(8, parmMapeoAnaliticas.getTecAzucar());
		    if (parmMapeoAnaliticas.getGrado()!=null)
		    {
			    preparedStatement.setDouble(9, new Double(parmMapeoAnaliticas.getGrado()));
		    }
		    else
		    {
		    	preparedStatement.setNull(9, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(10, parmMapeoAnaliticas.getTecGrado());
		    if (parmMapeoAnaliticas.getGlufru()!=null)
		    {
			    preparedStatement.setDouble(11, new Double(parmMapeoAnaliticas.getGlufru()));
		    }
		    else
		    {
		    	preparedStatement.setNull(11, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(12, parmMapeoAnaliticas.getTecGlufru());
		    if (parmMapeoAnaliticas.getAcidezt()!=null)
		    {
			    preparedStatement.setDouble(13, new Double(parmMapeoAnaliticas.getAcidezt()));
		    }
		    else
		    {
		    	preparedStatement.setNull(13, java.sql.Types.NULL);
		    }
	
		    preparedStatement.setString(14, parmMapeoAnaliticas.getTecAcidezt());
		    if (parmMapeoAnaliticas.getPh()!=null)
		    {
		    	preparedStatement.setDouble(15, new Double(parmMapeoAnaliticas.getPh()));
		    }
		    else
		    {
		    	preparedStatement.setNull(15, java.sql.Types.NULL);
		    }			    			    
		    preparedStatement.setString(16, parmMapeoAnaliticas.getTecPh());
		    if (parmMapeoAnaliticas.getMalico()!=null)
		    {
			    preparedStatement.setDouble(17, new Double(parmMapeoAnaliticas.getMalico()));
		    }
		    else
		    {
		    	preparedStatement.setNull(17, java.sql.Types.NULL);
		    }			    			    
		    preparedStatement.setString(18, parmMapeoAnaliticas.getTecMalico());
		    if (parmMapeoAnaliticas.getColor()!=null)
		    {
			    preparedStatement.setDouble(19, new Double(parmMapeoAnaliticas.getColor()));
		    }
		    else
		    {
		    	preparedStatement.setNull(19, java.sql.Types.NULL);
		    }			    			    
	
		    preparedStatement.setString(20, parmMapeoAnaliticas.getTecColor());
		    if (parmMapeoAnaliticas.getPolif()!=null)
		    {
			    preparedStatement.setDouble(21, new Double(parmMapeoAnaliticas.getPolif()));
		    }
		    else
		    {
		    	preparedStatement.setNull(21, java.sql.Types.NULL);
		    }			    			    
		    preparedStatement.setString(22, parmMapeoAnaliticas.getTecPolif());
		    if (parmMapeoAnaliticas.getResponsable()!=null)
		    {
		    	preparedStatement.setString(23, parmMapeoAnaliticas.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(23, "Marta");
		    }			    			    
		    
		    preparedStatement.setString(24, parmMapeoAnaliticas.getObservaciones());
		    if (parmMapeoAnaliticas.getLitros()!=null)
		    {
			    preparedStatement.setDouble(25, parmMapeoAnaliticas.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setNull(25, java.sql.Types.NULL);
		    }
		    preparedStatement.setString(26, parmMapeoAnaliticas.getLnkDeposito());
		    preparedStatement.setString(27, parmMapeoAnaliticas.getTipoAnalitica());
		    preparedStatement.setInt(28, parmMapeoAnaliticas.getCodigo().intValue());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoAnaliticas r_mapeoAnaliticas)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM analiticas ");            
			cadenaSQL.append(" WHERE analiticas.codigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeoAnaliticas.getCodigo().intValue());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public boolean comprobarDeposito(String r_deposito)
	{
		consultaLocalizacionesServer cls = null;
		boolean encontrado=false;
		
		cls=consultaLocalizacionesServer.getInstance(CurrentUser.get());
		encontrado = cls.comprobarDeposito(r_deposito);
		
		return encontrado;
	}
}