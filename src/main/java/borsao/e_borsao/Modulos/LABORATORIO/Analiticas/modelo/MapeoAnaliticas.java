package borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo;

import java.util.Date;

import javax.validation.constraints.Size;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoAnaliticas extends MapeoGlobal
{
//    @NotNull
//    private int id = -1;
//    @NotNull
    @Size(min = 2, message = "Product name must have at least two characters")
    private String productName = "";
//    @Min(0)
//    @NotNull
    
	private Integer codigo;
	
	private Date fecha;
		
	private String lnkDeposito;
	private String vino;
	private String tecSulfuroso;
	private String tecSulfurosoT;
	private String tecAzucar;
	private String tecGrado;
	private String tecGlufru;
	private String tecAcidezt;
	private String tecPh;
	private String tecMalico;
	private String tecColor;
	private String tecPolif;
	private String responsable;
	private String observaciones;
	private String tipoAnalitica;
	
	private Double litros;
	private Double sulfuroso;
	private Double sulfurosoT;
	private Double azucar;
	private Double grado;
	private Double glufru;
	private Double acidezt;
	private Double ph;
	private Double malico;
	private Double color;
	private Double polif;	
	

//	private MapeoAyudas mapeoLocalizaciones;
	
	public MapeoAnaliticas()
	{		
		
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getLnkDeposito() {
		return RutinasCadenas.conversion(lnkDeposito);
	}

	public void setLnkDeposito(String lnkDeposito) {
		this.lnkDeposito = lnkDeposito;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getVino() {
		return RutinasCadenas.conversion(vino);
	}

	public void setVino(String vino) {
		this.vino = vino;
	}

	public String getTecSulfuroso() {
		return RutinasCadenas.conversion(tecSulfuroso);
	}

	public void setTecSulfuroso(String tecSulfuroso) {
		this.tecSulfuroso = tecSulfuroso;
	}

	public String getTecSulfurosoT() {
		return RutinasCadenas.conversion(tecSulfurosoT);
	}

	public void setTecSulfurosoT(String tecSulfurosoT) {
		this.tecSulfurosoT = tecSulfurosoT;
	}

	public String getTecAzucar() {
		return RutinasCadenas.conversion(tecAzucar);
	}

	public void setTecAzucar(String tecAzucar) {
		this.tecAzucar = tecAzucar;
	}

	public String getTecGrado() {
		return RutinasCadenas.conversion(tecGrado);
	}

	public void setTecGrado(String tecGrado) {
		this.tecGrado = tecGrado;
	}

	public String getTecGlufru() {
		return RutinasCadenas.conversion(tecGlufru);
	}

	public void setTecGlufru(String tecGlufru) {
		this.tecGlufru = tecGlufru;
	}

	public String getTecAcidezt() {
		return RutinasCadenas.conversion(tecAcidezt);
	}

	public void setTecAcidezt(String tecAcidezt) {
		this.tecAcidezt = tecAcidezt;
	}

	public String getTecPh() {
		return RutinasCadenas.conversion(tecPh);
	}

	public void setTecPh(String tecPh) {
		this.tecPh = tecPh;
	}

	public String getTecMalico() {
		return RutinasCadenas.conversion(tecMalico);
	}

	public void setTecMalico(String tecMalico) {
		this.tecMalico = tecMalico;
	}

	public String getTecColor() {
		return RutinasCadenas.conversion(tecColor);
	}

	public void setTecColor(String tecColor) {
		this.tecColor = tecColor;
	}

	public String getTecPolif() {
		return RutinasCadenas.conversion(tecPolif);
	}

	public void setTecPolif(String tecPolif) {
		this.tecPolif = tecPolif;
	}

	public String getResponsable() {
		return RutinasCadenas.conversion(responsable);
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getObservaciones() {
		return RutinasCadenas.conversion(observaciones);
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Double getSulfuroso() {
		return this.sulfuroso;
//		return RutinasNumericas.formatearDouble(sulfuroso);
	}

	public void setSulfuroso(Double sulfuroso) {
		this.sulfuroso = sulfuroso;
	}

	public Double getSulfurosoT() {
		return this.sulfurosoT;
//		return RutinasNumericas.formatearDouble(sulfurosoT);	
	}

	public void setSulfurosoT(Double sulfurosoT) {
		this.sulfurosoT = sulfurosoT;
	}

	public Double getAzucar() {
		return this.azucar;
//		return RutinasNumericas.formatearDouble(azucar);
	}

	public void setAzucar(Double azucar) {
		this.azucar = azucar;
	}

	public Double getGrado() {
		return this.grado;
//		return RutinasNumericas.formatearDouble(grado);
	}

	public void setGrado(Double grado) {
		this.grado = grado;
	}

	public Double getGlufru() {
		return this.glufru;
//		return RutinasNumericas.formatearDouble(glufru);
	}

	public void setGlufru(Double glufru) {
		this.glufru = glufru;
	}

	public Double getAcidezt() {
		return this.acidezt;
//		return RutinasNumericas.formatearDouble(acidezt);
	}

	public void setAcidezt(Double acidezt) {
		this.acidezt = acidezt;
	}

	public Double getPh() {
		return this.ph;
//		return RutinasNumericas.formatearDouble(ph);
	}

	public void setPh(Double ph) {
		this.ph = ph;
	}

	public Double getMalico() {
		return this.malico;
//		return RutinasNumericas.formatearDouble(malico);
	}

	public void setMalico(Double malico) {
		this.malico = malico;
	}

	public Double getColor() {
		return this.color;
//		return RutinasNumericas.formatearDouble(color);
	}

	public void setColor(Double color) {
		this.color = color;
	}

	public Double getPolif() {
		return this.polif;
//		return RutinasNumericas.formatearDouble(polif);
	}

	public void setPolif(Double polif) {
		this.polif = polif;
	}

	public Double getLitros() {
		return litros;
	}

	public void setLitros(Double litros) {
		this.litros = litros;
	}

//	public MapeoAyudas getMapeoLocalizaciones() {
//		return mapeoLocalizaciones;
//	}
//
//	public void setMapeoLocalizaciones(MapeoAyudas mapeoLocalizaciones) {
//		this.mapeoLocalizaciones = mapeoLocalizaciones;
//	}
//
	public String getTipoAnalitica() {
		return RutinasCadenas.conversion(tipoAnalitica);
	}

	public void setTipoAnalitica(String tipoAnalitica) {
		this.tipoAnalitica = tipoAnalitica;
	}
	
}