package borsao.e_borsao.Modulos.LABORATORIO.Analiticas.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Window;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo.AnaliticasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo.MapeoAnaliticas;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.server.consultaAnaliticasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoAnaliticas mapeoAnaliticas = null;
	 
	 private consultaAnaliticasServer cas = null;	 
	 private ventanaAyuda vHelp  =null;	 
	 private AnaliticasView aw = null;
	 private BeanFieldGroup<MapeoAnaliticas> fieldGroup;
	 
	 public OpcionesForm(AnaliticasView r_aw) {
        super();
        aw=r_aw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.tipoAnalitica);
                	
        this.fecha.addStyleName("v-datefield-textfield");
        this.fecha.setDateFormat("dd/MM/yyyy");
        this.fecha.setShowISOWeekNumbers(true);
        this.fecha.setResolution(Resolution.DAY);
        
        
        fieldGroup = new BeanFieldGroup<MapeoAnaliticas>(MapeoAnaliticas.class);
        fieldGroup.bindMemberFields(this);

//        this.pruebas();
        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{

		lnkDeposito.setValidationVisible(false);
		class MyValidator implements Validator {
            private static final long serialVersionUID = -8281962473854901819L;

            @Override
            public void validate(Object value)
                    throws InvalidValueException {
            	cas  = new consultaAnaliticasServer(CurrentUser.get());
            	if (lnkDeposito.getValue()!=null && lnkDeposito.getValue().length()>0 && isBusqueda())
            	{
	            	if (!cas.comprobarDeposito(lnkDeposito.getValue()))
					{
	            		lnkDeposito.focus();	
	                    throw new InvalidValueException("Valor incorrecto");
					}
            	}
            }
        }
		lnkDeposito.addValidator(new MyValidator());
		
		lnkDeposito.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				lnkDeposito.setValidationVisible(true);
			}
		});
	}

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
        	if (f.getCaption()!=null && f.getCaption().equals("Fecha"))
        	{
        		f.addValueChangeListener(new ValueChangeListener() 
            	{
        			@Override
					public void valueChange(ValueChangeEvent event) {
        				f.setValue(RutinasFechas.conversion((Date)f.getValue()));
        			}
        		});
        	}
    		else if (f.getCaption()!=null && f.getStyleName().equals("right") && f.getCaption()!="Litros")
        	{    			
        		f.addValueChangeListener(new ValueChangeListener() 
            	{
        			@Override
					public void valueChange(ValueChangeEvent event) {
        					try {        						
        						f.setValue(RutinasNumericas.formatearDouble(f.getValue().toString()));
        					}
        					catch (Exception pex)
        					{}
        			}
        		});
        	}
        	else
        	{
        		f.addValueChangeListener(valueListener);
        	}
        	
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoAnaliticas = (MapeoAnaliticas) aw.grid.getSelectedRow();
                eliminarAnalitica(mapeoAnaliticas);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				aw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (aw.isHayGrid())
		 			{			
		 				aw.grid.removeAllColumns();			
		 				aw.barAndGridLayout.removeComponent(aw.grid);
		 				aw.grid=null;			
		 			}
		 			aw.generarGrid(aw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			aw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				aw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoAnaliticas = hm.convertirHashAMapeo(aw.opcionesEscogidas);
	                crearAnalitica(mapeoAnaliticas);
	 			}
	 			else
	 			{
	 				MapeoAnaliticas mapeoAnaliticas_orig = (MapeoAnaliticas) aw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				aw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoAnaliticas = hm.convertirHashAMapeo(aw.opcionesEscogidas);
	 				mapeoAnaliticas.setCodigo(mapeoAnaliticas_orig.getCodigo());
	 				modificarAnalitica(mapeoAnaliticas,mapeoAnaliticas_orig);	 				
	 				mapeoAnaliticas_orig=null;
	 				hm=null;
	 			}
	 			aw.setHayGrid(true);
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnAyuda.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
		
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             aw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo)
	{
		r_combo.addItem("SE");
		r_combo.addItem("PE");
		r_combo.addItem("CC");
		r_combo.addItem("C");
		r_combo.addItem("DC");
		r_combo.addItem("DA");
	}
	
	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorDepositos = null;
		cas  = new consultaAnaliticasServer(CurrentUser.get());
		vectorDepositos=cas.vector();
		this.vHelp = new ventanaAyuda(this.lnkDeposito, vectorDepositos, "Depositos");
		getUI().addWindow(this.vHelp);	
	}
	
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnAyuda.setEnabled(r_activoono);
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
		tipoAnalitica.setEnabled(r_activoono);		
		fecha.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarAnalitica(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.tipoAnalitica.getValue()!=null)
		{
			opcionesEscogidas.put("tipo", this.tipoAnalitica.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipo", null);
		}
		
		if (this.fecha.getValue()!=null)
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", null);
		}
		
		if (this.lnkDeposito.getValue()!=null) 
		{
			opcionesEscogidas.put("deposito", this.lnkDeposito.getValue());
		}
		else
		{
			opcionesEscogidas.put("deposito", null);
		}
		
		if (this.vino.getValue()!=null) 
		{
			opcionesEscogidas.put("vino", this.vino.getValue());
		}
		else
		{
			opcionesEscogidas.put("vino", null);
		}
		if (this.litros.getValue()!=null) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue());
		}
		else
		{
			opcionesEscogidas.put("litros", null);
		}
		if (this.observaciones.getValue()!=null) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue());
		}
		else
		{
			opcionesEscogidas.put("observaciones", null);
		}
		if (this.responsable.getValue()!=null) 
		{
			opcionesEscogidas.put("responsable", this.responsable.getValue());
		}
		else
		{
			opcionesEscogidas.put("responsable", null);
		}
		if (this.sulfuroso.getValue()!=null) 
		{
			opcionesEscogidas.put("so", this.sulfuroso.getValue());
		}
		else
		{
			opcionesEscogidas.put("so", null);
		}
		if (this.tecSulfuroso.getValue()!=null) 
		{
			opcionesEscogidas.put("sot", this.tecSulfuroso.getValue());
		}
		else
		{
			opcionesEscogidas.put("sot", null);
		}
		if (this.sulfurosoT.getValue()!=null) 
		{
			opcionesEscogidas.put("sott", this.sulfurosoT.getValue());
		}
		else
		{
			opcionesEscogidas.put("sott", null);
		}
		if (this.tecSulfurosoT.getValue()!=null) 
		{
			opcionesEscogidas.put("sottt", this.tecSulfurosoT.getValue());
		}
		else
		{
			opcionesEscogidas.put("sottt", null);
		}
		if (this.azucar.getValue()!=null) 
		{
			opcionesEscogidas.put("ac", this.azucar.getValue());
		}
		else
		{
			opcionesEscogidas.put("ac", null);
		}
		if (this.tecAzucar.getValue()!=null) 
		{
			opcionesEscogidas.put("act", this.tecAzucar.getValue());
		}
		else
		{
			opcionesEscogidas.put("act", null);
		}
		if (this.grado.getValue()!=null) 
		{
			opcionesEscogidas.put("gr", this.grado.getValue());
		}
		else
		{
			opcionesEscogidas.put("gr", null);
		}
		if (this.tecGrado.getValue()!=null) 
		{
			opcionesEscogidas.put("grt", this.tecGrado.getValue());
		}
		else
		{
			opcionesEscogidas.put("grt", null);
		}
		if (this.gluFru.getValue()!=null) 
		{
			opcionesEscogidas.put("glu", this.gluFru.getValue());
		}
		else
		{
			opcionesEscogidas.put("glu", null);
		}
		if (this.tecGluFru.getValue()!=null) 
		{
			opcionesEscogidas.put("glut", this.tecGluFru.getValue());
		}
		else
		{
			opcionesEscogidas.put("glut", null);
		}
		if (this.acidezT.getValue()!=null) 
		{
			opcionesEscogidas.put("acdt", this.acidezT.getValue());
		}
		else
		{
			opcionesEscogidas.put("acdt", null);
		}
		if (this.tecAcidezT.getValue()!=null) 
		{
			opcionesEscogidas.put("acdtt", this.tecAcidezT.getValue());
		}
		else
		{
			opcionesEscogidas.put("acdtt", null);
		}
		if (this.malico.getValue()!=null) 
		{
			opcionesEscogidas.put("mal", this.malico.getValue());
		}
		else
		{
			opcionesEscogidas.put("mal", null);
		}
		if (this.tecMalico.getValue()!=null) 
		{
			opcionesEscogidas.put("malt", this.tecMalico.getValue());
		}
		else
		{
			opcionesEscogidas.put("malt", null);
		}
		if (this.color.getValue()!=null) 
		{
			opcionesEscogidas.put("col", this.color.getValue());
		}
		else
		{
			opcionesEscogidas.put("col", null);
		}
		if (this.tecColor.getValue()!=null) 
		{
			opcionesEscogidas.put("colt", this.tecColor.getValue());
		}
		else
		{
			opcionesEscogidas.put("coltt", null);
		}
		if (this.polif.getValue()!=null) 
		{
			opcionesEscogidas.put("pol", this.polif.getValue());
		}
		else
		{
			opcionesEscogidas.put("pol", null);
		}
		if (this.tecPolif.getValue()!=null) 
		{
			opcionesEscogidas.put("polt", this.tecPolif.getValue());
		}
		else
		{
			opcionesEscogidas.put("polt", null);
		}
		if (this.ph.getValue()!=null) 
		{
			opcionesEscogidas.put("ph", this.ph.getValue());
		}
		else
		{
			opcionesEscogidas.put("ph", null);
		}
		if (this.tecPh.getValue()!=null) 
		{
			opcionesEscogidas.put("pht", this.tecPh.getValue());
		}
		else
		{
			opcionesEscogidas.put("pht", null);
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarAnalitica(null);
		this.sulfuroso.setEnabled(!r_buscar);
		this.sulfurosoT.setEnabled(!r_buscar);
		this.observaciones.setEnabled(!r_buscar);
		this.responsable.setEnabled(!r_buscar);
		this.litros.setEnabled(!r_buscar);
		this.acidezT.setEnabled(!r_buscar);
		this.azucar.setEnabled(!r_buscar);
		this.color.setEnabled(!r_buscar);
		this.gluFru.setEnabled(!r_buscar);
		this.grado.setEnabled(!r_buscar);
		this.malico.setEnabled(!r_buscar);
		this.color.setEnabled(!r_buscar);
		this.polif.setEnabled(!r_buscar);
		this.ph.setEnabled(!r_buscar);
		this.tecAcidezT.setEnabled(!r_buscar);
		this.tecAzucar.setEnabled(!r_buscar);
		this.tecColor.setEnabled(!r_buscar);
		this.tecGluFru.setEnabled(!r_buscar);
		this.tecGrado.setEnabled(!r_buscar);
		this.tecMalico.setEnabled(!r_buscar);
		this.tecPolif.setEnabled(!r_buscar);
		this.tecSulfuroso.setEnabled(!r_buscar);
		this.tecSulfurosoT.setEnabled(!r_buscar);
		this.tecPh.setEnabled(!r_buscar);
	}
	
	public void editarAnalitica(MapeoAnaliticas r_mapeoAnaliticas) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeoAnaliticas==null) r_mapeoAnaliticas=new MapeoAnaliticas();
		fieldGroup.setItemDataSource(new BeanItem<MapeoAnaliticas>(r_mapeoAnaliticas));
		tipoAnalitica.setValue(r_mapeoAnaliticas.getTipoAnalitica());
	}
	public void eliminarAnalitica(MapeoAnaliticas r_mapeoAnaliticas) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cas  = new consultaAnaliticasServer(CurrentUser.get());
		((AnaliticasGrid) aw.grid).remove(r_mapeoAnaliticas);
		cas.eliminar(r_mapeoAnaliticas);
		
	}
	
	public void modificarAnalitica(MapeoAnaliticas analitica_new, MapeoAnaliticas analitica_orig)
	{
		cas  = new consultaAnaliticasServer(CurrentUser.get());
		cas.guardarCambios(analitica_new);		
		((AnaliticasGrid) aw.grid).refresh(analitica_new,analitica_orig);
	}
	public void crearAnalitica(MapeoAnaliticas r_mapeoAnaliticas) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cas  = new consultaAnaliticasServer(CurrentUser.get());
		String rdo = cas.guardarNuevo(r_mapeoAnaliticas);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoAnaliticas>(r_mapeoAnaliticas));
			((AnaliticasGrid) aw.grid).refresh(r_mapeoAnaliticas,null);
			this.setCreacion(false);
			aw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoAnaliticas> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoAnaliticas = item.getBean();
            if (this.mapeoAnaliticas.getCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private void pruebas()
    {
    	
//    	
//    	sulfuroso.addShortcutListener(
//                //pass any KeyCode.A, KeyCode.B, KeyCode.C etc..
//        new ShortcutListener("", 110, null) {
//            @Override
//            public void handleAction(Object sender, Object target) {
//                Notificaciones.getInstance().mensajeSeguimiento("sender " + sender + " target " + target);
//                Notificaciones.getInstance().mensajeSeguimiento("getKeyCode() " + getKeyCode());
//                if (getModifiers().length > 0) {
//                    Notificaciones.getInstance().mensajeSeguimiento("modifier " + getModifiers()[0]);
//                }
//                if (getKeyCode() == KeyCode.TAB) {
//
//                }
//                
//                ((TextField) target).setValue(((TextField) target).getValue().toString()+",");
//            }
//        }
//
//        );
//    	
    	
		
    	/*
    	 * prueba carga validacion numerica
    	 */
//		sulfuroso.setValidationVisible(false);
//		
//		class validarNumero implements Validator {
//            private static final long serialVersionUID = -8281962473854901819L;
//
//            @Override
//            public void validate(Object value)
//                    throws InvalidValueException {
//            	if (!(value instanceof Number))
//				{            		
//                    throw new InvalidValueException("Debes introducir un valor numérico");
//				}
//            }
//        }
//		sulfuroso.addValidator(new validarNumero());
//		
//		sulfuroso.addValueChangeListener(new ValueChangeListener() {
//			@Override
//			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
//				sulfuroso.setValidationVisible(true);
//			}
//		});		
//		
//		
//		
//		

    	/*
    	 * Impresion PDF generado
    	 */
	    class pdfBarras {
	    	 
	    	
	    	 
	    	    public void createPdf(String dest) throws IOException, DocumentException {
	    	        Document document = new Document();
	    	        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
	    	        document.open();
	    	        
	    	        
	    	        /*
	    	         * 1- Logo y titulo 
	    	         * 2- Descripcion articulo (grande y centrada
	    	         * 3- codigo ean del articulo
	    	         * 4- cajas y cantidad de botellas
	    	         * 5- lote, añada y caducidad
	    	         * 6- codigo sscc
	    	         * 7- codigo barras representando articuki, lote, cantidad
	    	         * 8- codigo barras representando codigo barras mas secuencial y la caducidad
	    	         * 
	    	         */
	    	        
	    	        String code = "(01)18412423999999(10)9999";
	    	 
	    	        PdfContentByte cb = writer.getDirectContent();
	    	 
	    	        PdfPTable table = new PdfPTable(2);
	    	 
	    	        table.addCell("Change baseline:");
	    	        Barcode128 code128 = new Barcode128();
	    	        code128.setBaseline(-1);
	    	        code128.setSize(12);
	    	        code128.setCode(code);
//	    	        code128.setFont(bf);
	    	        code128.setCodeType(Barcode128.CODE128);
	    	        Image code128Image = code128.createImageWithBarcode(cb, null, null);
	    	        PdfPCell cell = new PdfPCell(code128Image);
	    	        table.addCell(cell);
	    	 
	    	        table.addCell("Add text and bar code separately:");
	    	        code128.setFont(null);
	    	        code128.setCode(code);
//	    	        code128.setFont(bf);
	    	        code128.setCodeType(Barcode128.CODE128);
	    	        code128Image = code128.createImageWithBarcode(cb, null, null);
	    	        cell = new PdfPCell();
	    	        cell.addElement(new Phrase("PO #: " + code));
	    	        cell.addElement(code128Image);
	    	        table.addCell(cell);
	    	 
	    	        document.add(table);
	    	 
	    	        document.close();
	    	    }
	    	 
	    }
		
        File file = new File("c:/barcode_in_table.pdf");
        file.getParentFile().mkdirs();
        try {
			new pdfBarras().createPdf("c:/barcode_in_table.pdf");
			
		    Window window = new Window();
//		    ((VerticalLayout) window.getContent()).setSizeFull();
		    window.setResizable(true);
		    window.setCaption("Exemplo PDF");
		    window.setWidth("800");
		    window.setHeight("600");
		    window.center();
			
			   StreamSource s = new StreamResource.StreamSource() {

			   @Override
			   public InputStream getStream() {
			   try {
			   FileInputStream fis = new FileInputStream(file);
			   return fis;
			   } catch (Exception e) {
			   e.printStackTrace();
			   return null;
			   }
			   }
			   };

			   StreamResource r = new StreamResource(s, "c:/barcode_in_table.pdf");
			   r.setMIMEType("application/pdf");

			   BrowserFrame e = new BrowserFrame();
//			   Embedded e = new Embedded();
			   e.setSizeFull();
//			   e.setType(Embedded.TYPE_BROWSER);
			   e.setSource(r);
			   
			   window.setContent(e);
			   window.setSizeFull();
			   eBorsao.getCurrent().addWindow(window);

			   BrowserWindowOpener opener =
		                new BrowserWindowOpener(r);			
			   opener.setResource(r);
			   
//			   JavaScript.getCurrent().execute(
//			            "setTimeout(function() {" +
//			            "  print(); self.close();}, 0);");
//			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		
		/*
		 * pruebas para quitar poner el punto decimal como en españa
		 */
//    	sulfuroso.setNullRepresentation("");
//    	 
//    	//Add validation hints in UI??
//    	sulfuroso.setValidationVisible(true);
//    	 
//    	sulfuroso.setTextChangeEventMode(TextChangeEventMode.LAZY);
//    	 
//    	sulfuroso.addTextChangeListener(new FieldEvents.TextChangeListener() {
//    	    private static final long serialVersionUID = 1L;
//    	 
//    	    @Override
//    	    public void textChange(FieldEvents.TextChangeEvent event) {
//    	        try {
//    	            sulfuroso.setCursorPosition(event.getCursorPosition());
//    	            if (sulfuroso.getValue().charAt(event.getCursorPosition())=='.') sulfuroso.setValue(sulfuroso.getValue().substring(0, sulfuroso.getValue().length()-1) + ",");
//    	        } catch (InvalidValueException e) {
//    	            //Log error
//    	        }
//    	    }
//    	});
    
    
    }
    
    
}
