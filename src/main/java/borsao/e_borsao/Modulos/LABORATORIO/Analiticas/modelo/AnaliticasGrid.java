package borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.util.converter.StringToDateConverter;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.DoubleConverter;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AnaliticasGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;

    public AnaliticasGrid(ArrayList<MapeoAnaliticas> r_vector) 
    {
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
        
		this.asignarTitulo("Analíticas</b></h2>");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoAnaliticas.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
		this.removeColumn("codigo");
		this.setFrozenColumnCount(3);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("tipoAnalitica", "fecha", "lnkDeposito","vino","sulfuroso","tecSulfuroso","sulfurosoT","tecSulfurosoT",
    					"azucar","tecAzucar","grado","tecGrado","glufru","tecGlufru","acidezt","tecAcidezt","ph","tecPh",
    					"malico","tecMalico","color","tecColor","polif","tecPolif","litros","observaciones","responsable");

    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("tipoAnalitica", "75");
    	this.widthFiltros.put("fecha", "150");
    	this.widthFiltros.put("lnkDeposito", "150");
    	this.widthFiltros.put("vino", "150");
    	
    	this.getColumn("tipoAnalitica").setHeaderCaption("Tipo");
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("lnkDeposito").setHeaderCaption("Deposito");
    	this.getColumn("vino").setHeaderCaption("Vino");    	
    	this.getColumn("sulfuroso").setHeaderCaption("SO2 Libre");
    	this.getColumn("tecSulfuroso").setHeaderCaption("T. SO2 Libre");
    	this.getColumn("sulfurosoT").setHeaderCaption("SO2 Total");
    	this.getColumn("tecSulfurosoT").setHeaderCaption("T. SO2 Total");
    	this.getColumn("azucar").setHeaderCaption("Ac Vol");
    	this.getColumn("tecAzucar").setHeaderCaption("T. Ac. Vol.");
    	this.getColumn("grado").setHeaderCaption("Grado Al.");
    	this.getColumn("tecGrado").setHeaderCaption("T. Grado Al.");
    	this.getColumn("glufru").setHeaderCaption("Glu - Fru");
    	this.getColumn("tecGlufru").setHeaderCaption("T. Glu - Fru.");
    	this.getColumn("acidezt").setHeaderCaption("Ac. Total");
    	this.getColumn("tecAcidezt").setHeaderCaption("T. Ac. Total.");
    	this.getColumn("ph").setHeaderCaption("PH");
    	this.getColumn("tecPh").setHeaderCaption("T. PH");
    	this.getColumn("malico").setHeaderCaption("Mal.");
    	this.getColumn("tecMalico").setHeaderCaption("T. Mal.");
    	this.getColumn("color").setHeaderCaption("Ind. Color");
    	this.getColumn("tecColor").setHeaderCaption("T. Ind. Col.");
    	this.getColumn("polif").setHeaderCaption("Polif. Total");
    	this.getColumn("tecPolif").setHeaderCaption("T. Polif. Total");
    	this.getColumn("litros").setHeaderCaption("Litros");
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("responsable").setHeaderCaption("Responsable");
    	
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
                if ("fecha".equals(cellReference.getPropertyId()) || "lnkDeposito".equals(cellReference.getPropertyId())
                		|| "tipoAnalitica".equals(cellReference.getPropertyId()) || "vino".equals(cellReference.getPropertyId()) 
                		|| "observaciones".equals(cellReference.getPropertyId()) || "responsable".equals(cellReference.getPropertyId())
                		|| cellReference.getPropertyId().toString().startsWith("tec") || "idCodigo".equals(cellReference.getPropertyId()) ) {
                    // when the current cell is number such as age, align text to right                	
                    return "leftAligned";
                } else {
                    // otherwise, align text to left
                	getColumn(cellReference.getPropertyId()).setConverter(new DoubleConverter());
                	return "rightAligned";
                }
            }
        });
    	
    	this.getColumn("fecha").setConverter(new StringToDateConverter(){
			@Override
			public DateFormat getFormat(Locale locale){
				return new SimpleDateFormat("dd/MM/yyyy");
			}
		});
    	
    	this.sort("fecha",SortDirection.DESCENDING);
    }

	@Override
	public void establecerColumnasNoFiltro() {
		this.camposNoFiltrar.add("");
		
		this.camposNoFiltrar.add("sulfuroso");
		this.camposNoFiltrar.add("tecSulfuroso");
		this.camposNoFiltrar.add("sulfurosoT");
		this.camposNoFiltrar.add("tecSulfurosoT");
		this.camposNoFiltrar.add("azucar");
		this.camposNoFiltrar.add("tecAzucar");
		this.camposNoFiltrar.add("grado");
		this.camposNoFiltrar.add("tecGrado");
		this.camposNoFiltrar.add("glufru");
		this.camposNoFiltrar.add("tecGlufru");
		this.camposNoFiltrar.add("acidezt");
		this.camposNoFiltrar.add("tecAcidezt");
		this.camposNoFiltrar.add("ph");
		this.camposNoFiltrar.add("tecPh");
		this.camposNoFiltrar.add("malico");
		this.camposNoFiltrar.add("tecMalico");
		this.camposNoFiltrar.add("color");
		this.camposNoFiltrar.add("tecColor");
		this.camposNoFiltrar.add("polif");
		this.camposNoFiltrar.add("tecPolif");
		this.camposNoFiltrar.add("litros");
		this.camposNoFiltrar.add("observaciones");
		this.camposNoFiltrar.add("responsable");
	}

	@Override
	public void cargarListeners() {
		
	}

	@Override
	public void calcularTotal() {
		
	}
    
}
