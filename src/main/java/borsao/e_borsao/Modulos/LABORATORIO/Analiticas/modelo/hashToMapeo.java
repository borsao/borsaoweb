package borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoAnaliticas mapeo = null;
	 
	 public MapeoAnaliticas convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoAnaliticas();
		 this.mapeo.setTipoAnalitica(r_hash.get("tipo"));
		 this.mapeo.setFecha(rtFecha.conversionDeString(r_hash.get("fecha")));
		 this.mapeo.setLnkDeposito(r_hash.get("deposito"));		 
		 this.mapeo.setVino(r_hash.get("vino"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoAnaliticas convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoAnaliticas();
		 this.mapeo.setTipoAnalitica(r_hash.get("tipo"));
		 this.mapeo.setFecha(rtFecha.conversionDeString(r_hash.get("fecha")));
		 this.mapeo.setLnkDeposito(r_hash.get("deposito"));		 
		 this.mapeo.setVino(r_hash.get("vino"));
		 if (r_hash.get("litros")!=null) this.mapeo.setLitros(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("litros"))));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setResponsable(r_hash.get("responsable"));
		 
		 this.mapeo.setTecSulfuroso(r_hash.get("sot"));
		 this.mapeo.setTecSulfurosoT(r_hash.get("sottt"));
		 this.mapeo.setTecAzucar(r_hash.get("act"));
		 this.mapeo.setTecGrado(r_hash.get("grt"));
		 this.mapeo.setTecGlufru(r_hash.get("glut"));
		 this.mapeo.setTecAcidezt(r_hash.get("acdtt"));
		 this.mapeo.setTecMalico(r_hash.get("malt"));
		 this.mapeo.setTecColor(r_hash.get("colt"));
		 this.mapeo.setTecPolif(r_hash.get("polt"));
		 this.mapeo.setTecPh(r_hash.get("pht"));

		 if (r_hash.get("so")!=null) this.mapeo.setSulfuroso(RutinasNumericas.formatearDoubleDeESP(r_hash.get("so")));
		 if (r_hash.get("sott")!=null) this.mapeo.setSulfurosoT(RutinasNumericas.formatearDoubleDeESP(r_hash.get("sott")));
		 if (r_hash.get("ac")!=null) this.mapeo.setAzucar(RutinasNumericas.formatearDoubleDeESP(r_hash.get("ac")));
		 if (r_hash.get("gr")!=null) this.mapeo.setGrado(RutinasNumericas.formatearDoubleDeESP(r_hash.get("gr")));
		 if (r_hash.get("glu")!=null) this.mapeo.setGlufru(RutinasNumericas.formatearDoubleDeESP(r_hash.get("glu")));
		 if (r_hash.get("acdt")!=null) this.mapeo.setAcidezt(RutinasNumericas.formatearDoubleDeESP(r_hash.get("acdt")));
		 if (r_hash.get("mal")!=null) this.mapeo.setMalico(RutinasNumericas.formatearDoubleDeESP(r_hash.get("mal")));
		 if (r_hash.get("col")!=null) this.mapeo.setColor(RutinasNumericas.formatearDoubleDeESP(r_hash.get("col")));
		 if (r_hash.get("pol")!=null) this.mapeo.setPolif(RutinasNumericas.formatearDoubleDeESP(r_hash.get("pol")));
		 if (r_hash.get("ph")!=null) this.mapeo.setPh(RutinasNumericas.formatearDoubleDeESP(r_hash.get("ph")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoAnaliticas r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("tipo", r_mapeo.getTipoAnalitica());
		 if (r_mapeo.getFecha()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFecha().toString());
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }
		 this.hash.put("deposito", r_mapeo.getLnkDeposito());
		 this.hash.put("vino", this.mapeo.getVino());
		 this.hash.put("litros", this.mapeo.getLitros().toString());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 this.hash.put("responsable", this.mapeo.getResponsable());
		 this.hash.put("so", String.valueOf(this.mapeo.getSulfuroso()));this.hash.put("sot", this.mapeo.getTecSulfuroso());
		 this.hash.put("sott", String.valueOf(this.mapeo.getSulfurosoT()));this.hash.put("sottt", this.mapeo.getTecSulfurosoT());
		 this.hash.put("ac", String.valueOf(this.mapeo.getAzucar()));this.hash.put("act", this.mapeo.getTecAzucar());
		 this.hash.put("gr", String.valueOf(this.mapeo.getGrado()));this.hash.put("grt", this.mapeo.getTecGrado());
		 this.hash.put("glu", String.valueOf(this.mapeo.getGlufru()));this.hash.put("glut", this.mapeo.getTecGlufru());
		 this.hash.put("acdt", String.valueOf(this.mapeo.getAcidezt()));this.hash.put("acdtt", this.mapeo.getTecAcidezt());
		 this.hash.put("mal", String.valueOf(this.mapeo.getMalico()));this.hash.put("malt", this.mapeo.getTecMalico());
		 this.hash.put("col", String.valueOf(this.mapeo.getColor()));this.hash.put("coltt", this.mapeo.getTecColor());
		 this.hash.put("pol", String.valueOf(this.mapeo.getPolif()));this.hash.put("polt", this.mapeo.getTecPolif());
		 this.hash.put("ph", String.valueOf(this.mapeo.getPh()));this.hash.put("pht", this.mapeo.getTecPh());
		 
		 return hash;		 
	 }
}