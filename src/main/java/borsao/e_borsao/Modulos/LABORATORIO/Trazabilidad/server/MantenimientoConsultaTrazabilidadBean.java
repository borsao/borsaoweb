package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.Movimientos.server.MantenimientoMovimientosBean;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;


public class MantenimientoConsultaTrazabilidadBean 
{
 
    /************************METODOS DE CONSULTA DE REGISTROS*********************/
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	

	MantenimientoMovimientosBean mantenimientoMovimientosBean = null;
	MantenimientoTrazabilidadBean mantenimientoTrazabilidadBean = null;

	
    public MantenimientoConsultaTrazabilidadBean()
    {
    	this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
    	this.mantenimientoMovimientosBean = new MantenimientoMovimientosBean();
    	this.mantenimientoTrazabilidadBean = new MantenimientoTrazabilidadBean();    	
    }
    
	/*
	 * La idea es recibir el numero de lote del que vamos a generar la trazabilidad
	 * 
	 * Vamos a intentar que el proceso lleve una barra de progreso que indique que estamos trabajando
	 * 
	 * 
	 * - Ir� llamadno al mantenimiento de movimientos con la clausula where que corresponda
	 * - Ir� insertando en la tabla temporal los datos que damos como correctos 
	 * 
	 * 
	 */
    
    public String generar(MapeoTrazabilidad r_mapeo, boolean r_eliminarDatos)
    {
    	String resultadoAccionGenerar = null;    	

    	if (r_eliminarDatos) resultadoAccionGenerar= this.eliminar();
    	
    	if (resultadoAccionGenerar==null)
    	{
    		this.iterarPorPartida(r_mapeo,r_mapeo.getLitros());    	
    		resultadoAccionGenerar="ProcesoTerminado";
    		return resultadoAccionGenerar;
    	}
    	else
    	{
    		return resultadoAccionGenerar;
    	}
    }
    
    private void iterarPorPartida(MapeoTrazabilidad r_mapeo, Double r_litros)
    {
    	ResultSet rsMovPartida = null;
    	MapeoTrazabilidad mapeoTrazabilidad = null;
    	String rdo=null;
    	Integer nivel=1;
    	rsMovPartida = mantenimientoMovimientosBean.recuperarRegistrosResultSetPartida(r_mapeo);
    	
    	try 
    	{
	    	if (rsMovPartida!=null)
	    	{	    		
				while (rsMovPartida.next())
				{					
					
				    mapeoTrazabilidad = new MapeoTrazabilidad();
				    mapeoTrazabilidad.setParte(rsMovPartida.getString("trz_par"));
				    mapeoTrazabilidad.setLinea(rsMovPartida.getString("trz_lin"));
				    mapeoTrazabilidad.setFecha(rsMovPartida.getTimestamp("trz_fec"));
				    mapeoTrazabilidad.setSeccion(rsMovPartida.getString("trz_sec"));
				    mapeoTrazabilidad.setTipo(rsMovPartida.getString("trz_tip"));
				    mapeoTrazabilidad.setLocalizacion(rsMovPartida.getString("trz_loc"));
				    mapeoTrazabilidad.setVino(rsMovPartida.getString("trz_vin"));
				    mapeoTrazabilidad.setLitros(new Double(rsMovPartida.getDouble("trz_lit")));
				    mapeoTrazabilidad.setDiasSal(new Integer(rsMovPartida.getInt("trz_ds")));
				    mapeoTrazabilidad.setCantidad(new Double(rsMovPartida.getDouble("trz_can")));
				    mapeoTrazabilidad.setPartida(rsMovPartida.getString("trz_prt"));
				    mapeoTrazabilidad.setLoteSalida(rsMovPartida.getString("trz_ls"));
				    mapeoTrazabilidad.setReferencia(rsMovPartida.getString("trz_ref"));
				    if (rsMovPartida.getString("con_des")!=null)
				    {
				    	mapeoTrazabilidad.setAlmacen(rsMovPartida.getString("con_des"));
				    } 
				    else
				    {
				    	mapeoTrazabilidad.setAlmacen(rsMovPartida.getString("trz_alm"));
				    }
				    mapeoTrazabilidad.setImpreso("N");
					if (r_mapeo==null || r_mapeo.getNivel()==null)
					{
						mapeoTrazabilidad.setNivel("1." + nivel.toString());
					}
					
				    if ( mapeoTrazabilidad.getTipo().equals("T"))
				    {
				    	if (r_mapeo.getNivel()!=null)
						{				    	
				    		if (nivel <=1) nivel=nivel+1;
				    		mapeoTrazabilidad.setNivel(r_mapeo.getNivel() + "." + nivel.toString());
						}
				    	mapeoTrazabilidad.setPadre(r_mapeo.getNivel());
					    mapeoTrazabilidad.setDeposito(r_mapeo.getLocalizacion());
						mapeoTrazabilidad.setLitros(r_mapeo.getLitros());
						
					    if (mapeoTrazabilidad.getTipo().equals("SE"))
					    {
					    	mapeoTrazabilidad.setPorcentaje(new Double(100));
						    mapeoTrazabilidad.setLitros(r_litros);
					    }
					    
						LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
					    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
					    mapeoTrazabilidad.setNivel(miNivel);

			    		rdo= mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidad);
						if (rdo==null)
						{
							if (r_mapeo==null || r_mapeo.getNivel()==null)
							{
								nivel=nivel+1;
							}
						}
				    }
				    else if (mapeoTrazabilidad.getTipo().equals("SE"))
				    {
				    	if (mapeoTrazabilidad.getReferencia().equals(r_mapeo.getReferencia()))
				    	{
					    	if (r_mapeo.getNivel()!=null)
							{				    	
					    		if (nivel <=1) nivel=nivel+1;
					    		mapeoTrazabilidad.setNivel(r_mapeo.getNivel() + "." + nivel.toString());
							}
					    	mapeoTrazabilidad.setPadre(r_mapeo.getNivel());
						    mapeoTrazabilidad.setDeposito(r_mapeo.getLocalizacion());
							mapeoTrazabilidad.setLitros(r_mapeo.getLitros());
							
						    if (mapeoTrazabilidad.getTipo().equals("SE"))
						    {
						    	mapeoTrazabilidad.setPorcentaje(new Double(100));
							    mapeoTrazabilidad.setLitros(r_litros);
						    }
						    
							LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
						    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
						    mapeoTrazabilidad.setNivel(miNivel);
	
				    		rdo= mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidad);
							if (rdo==null)
							{
								if (r_mapeo==null || r_mapeo.getNivel()==null)
								{
									nivel=nivel+1;
								}
							}
				    	}
				    }
				    else if ((mapeoTrazabilidad.getTipo().equals("ET") || mapeoTrazabilidad.getTipo().equals("E") || mapeoTrazabilidad.getTipo().equals("EC")) )
				    {
				    	if (r_mapeo.getNivel()!=null)
						{				    	
				    		mapeoTrazabilidad.setNivel(r_mapeo.getNivel());
				    		mapeoTrazabilidad.setFecha(r_mapeo.getFecha());
				    		mapeoTrazabilidad.setDiasSal(r_mapeo.getDiasSal());
						}
				    	else
				    	{
				    		mapeoTrazabilidad.setFecha(r_mapeo.getFecha());
				    		mapeoTrazabilidad.setDiasSal(r_mapeo.getDiasSal());
				    	}
				    	
					    /*
					     * JAVI
					     */
				    	this.iterarPorParte(mapeoTrazabilidad,r_mapeo.getLitros(),nivel);
				    	nivel=nivel+1;
				    }
				    else if (mapeoTrazabilidad.getTipo().equals("EV") || mapeoTrazabilidad.getTipo().equals("EL") || mapeoTrazabilidad.getTipo().equals("EM"))
				    {				    	
				    	if (r_mapeo.getNivel()!=null)
						{				    	
				    		mapeoTrazabilidad.setNivel(r_mapeo.getNivel() + "." + nivel.toString());
						}
					    mapeoTrazabilidad.setPorcentaje(new Double(100));
					    mapeoTrazabilidad.setLitros(r_mapeo.getLitros());
					    mapeoTrazabilidad.setPadre(r_mapeo.getNivel());
					    mapeoTrazabilidad.setDeposito(r_mapeo.getLocalizacion());

						LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
					    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
					    mapeoTrazabilidad.setNivel(miNivel);
					    
				    	rdo=mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidad);
						if (rdo==null)
						{
							nivel=nivel+1;
						}
				    }
				      		
				}
	    	}
		}
    	catch (Exception e) 
    	{
    		e.printStackTrace();
		}
    	
    }
    
    private void iterarPorParte(MapeoTrazabilidad r_mapeo, Double r_litros, int r_nivel)
    {
    	
		String resultadoAccionGuardarNuevo = null;
    	ResultSet rsMovParte = null;
    	MapeoTrazabilidad mapeoTrazabilidadParte = null;
    	Integer nivel=1;
    	
    	try 
    	{
	    	rsMovParte = mantenimientoMovimientosBean.recuperarRegistrosResultSetParte(r_mapeo);
	    	if (rsMovParte!=null)
	    	{	    		
				while (rsMovParte.next())
				{					
					mapeoTrazabilidadParte = new MapeoTrazabilidad();
					mapeoTrazabilidadParte.setParte(rsMovParte.getString("trz_par"));
					mapeoTrazabilidadParte.setLinea(rsMovParte.getString("trz_lin"));
					mapeoTrazabilidadParte.setFecha(rsMovParte.getTimestamp("trz_fec"));
					mapeoTrazabilidadParte.setSeccion(rsMovParte.getString("trz_sec"));
					mapeoTrazabilidadParte.setTipo(rsMovParte.getString("trz_tip"));
					mapeoTrazabilidadParte.setLocalizacion(rsMovParte.getString("trz_loc"));
					mapeoTrazabilidadParte.setVino(rsMovParte.getString("trz_vin"));
					mapeoTrazabilidadParte.setLitros(new Double(rsMovParte.getDouble("trz_lit")));
					mapeoTrazabilidadParte.setDiasSal(new Integer(rsMovParte.getInt("trz_ds")));
					mapeoTrazabilidadParte.setCantidad(new Double(rsMovParte.getDouble("trz_can")));
					mapeoTrazabilidadParte.setPartida(rsMovParte.getString("trz_prt"));
					mapeoTrazabilidadParte.setLoteSalida(rsMovParte.getString("trz_ls"));
					mapeoTrazabilidadParte.setReferencia(rsMovParte.getString("trz_ref"));
					if (rsMovParte.getString("con_des")!=null )
				    {
						mapeoTrazabilidadParte.setAlmacen(rsMovParte.getString("con_des"));
				    } 
				    else
				    {
				    	mapeoTrazabilidadParte.setAlmacen(rsMovParte.getString("trz_alm"));
				    }
					
					mapeoTrazabilidadParte.setImpreso("N");
					    			
					if (mapeoTrazabilidadParte.getTipo().equals("ST") || mapeoTrazabilidadParte.getTipo().equals("SC") )
					{
						if (r_mapeo.getTipo().equals("T") && nivel==1) nivel=r_nivel+1;
						 	
						mapeoTrazabilidadParte.setNivel(r_mapeo.getNivel() + "." + nivel.toString());						
					    mapeoTrazabilidadParte.setPadre(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setDeposito(r_mapeo.getLocalizacion());
					    
					    if (mapeoTrazabilidadParte.getLitros()>r_mapeo.getLitros())
					    {
					    	mapeoTrazabilidadParte.setPorcentaje(new Double(100));			
					    	mapeoTrazabilidadParte.setLitros(r_litros);
					    }
					    else
					    {
					    	mapeoTrazabilidadParte.setPorcentaje(new Double(Math.round((mapeoTrazabilidadParte.getLitros()/r_mapeo.getLitros())*100)));
					    	mapeoTrazabilidadParte.setLitros(new Double(Math.round(r_litros*(mapeoTrazabilidadParte.getLitros()/r_mapeo.getLitros()))));
					    }					    

						LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
					    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setNivel(miNivel);
					    
					    
						resultadoAccionGuardarNuevo= mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidadParte);
						if (resultadoAccionGuardarNuevo==null)
						{
							nivel=nivel+1;
						}
					}
					else if (mapeoTrazabilidadParte.getTipo().equals("S"))
					{
						mapeoTrazabilidadParte.setNivel(r_mapeo.getNivel() + "." + nivel.toString());						
					    mapeoTrazabilidadParte.setPadre(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setDeposito(r_mapeo.getLocalizacion());
					    
					    if (mapeoTrazabilidadParte.getLitros()>r_mapeo.getLitros())
					    {
					    	mapeoTrazabilidadParte.setPorcentaje(new Double(100));			
					    	mapeoTrazabilidadParte.setLitros(r_litros);
					    }
					    else
					    {
					    	mapeoTrazabilidadParte.setPorcentaje(new Double(Math.round((mapeoTrazabilidadParte.getLitros()/r_mapeo.getLitros())*100)));
					    	mapeoTrazabilidadParte.setLitros(new Double(Math.round(r_litros*(mapeoTrazabilidadParte.getLitros()/r_mapeo.getLitros()))));
					    }

						LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
					    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setNivel(miNivel);

						resultadoAccionGuardarNuevo= mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidadParte);
						if (resultadoAccionGuardarNuevo==null)
						{
							nivel=nivel+1;
						}
						
					}
					else if (mapeoTrazabilidadParte.getTipo().equals("EV")  || mapeoTrazabilidadParte.getTipo().equals("EL") || mapeoTrazabilidadParte.getTipo().equals("SM"))
					{
						mapeoTrazabilidadParte.setNivel(r_mapeo.getNivel() + "." + nivel.toString());					    
					    mapeoTrazabilidadParte.setPadre(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setDeposito(r_mapeo.getLocalizacion());
					    mapeoTrazabilidadParte.setPorcentaje(new Double(100));
						mapeoTrazabilidadParte.setLitros(r_litros);

						LibTrazabilidadBean libTrazabilidadBean = new LibTrazabilidadBean();
					    String miNivel=libTrazabilidadBean.recuperaNivel(r_mapeo.getNivel());
					    mapeoTrazabilidadParte.setNivel(miNivel);

						resultadoAccionGuardarNuevo= mantenimientoTrazabilidadBean.guardarNuevo(mapeoTrazabilidadParte);
						if (resultadoAccionGuardarNuevo==null)
						{
							nivel=nivel+1;
						}
					}
					
				}
			} 
		}
    	catch (Exception e) 
    	{
    		e.printStackTrace();
    	}

    }
    

	/****************************METODOS DE ALTERACION DATOS***********************/
	public void generarTraza(ArrayList<HashMap<String, String>> r_nodos)
	{
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
	      trazabilidadBean = new TrazabilidadBean();
	      trazabilidadBean.guardarTraza(r_nodos);	 
        }	    
	    catch(Exception exception)
	    {
	        exception.printStackTrace();
	    }
	}
	
	public String guardar(MapeoTrazabilidad parmMapeoTrazabilidad) throws Exception
	{
	    /*
	     * Metodo que se llama desde la parte cliente cuando estamos modificando datos de un
	     * registro ya existente.
	     * Recibe el mapeo completo incluyendo los cambios de la parte cliente.
	     * Hace un referencia al entity y con el findby lo crea y le pasamos los cambios en los datos
	     */
		String resultadoAccionGuardar = null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
	      trazabilidadBean = new TrazabilidadBean(parmMapeoTrazabilidad);
    	  
    	  if (trazabilidadBean.getCodigoInterno() != null)
	      {
            	resultadoAccionGuardar=trazabilidadBean.guardarCambios(parmMapeoTrazabilidad, false);	 
	      }        
	      else
	      {
	    	  resultadoAccionGuardar="Alguien a borrado el registro de la base de datos";
	      }
        }	    
	    catch(Exception exception)
	    {
	        exception.printStackTrace();
	        resultadoAccionGuardar=exception.getMessage();
	    }
	    return resultadoAccionGuardar;
	}
	
	public String guardarNuevo(MapeoTrazabilidad parmMapeoTrazabilidad) throws Exception
	{
		String resultadoAccionGuardarNuevo = null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
		      trazabilidadBean = new TrazabilidadBean(parmMapeoTrazabilidad);
		      parmMapeoTrazabilidad.setCodigo(this.obtenerSiguienteValorCodigo());		      
		      
	          if (trazabilidadBean.getCodigoInterno()!=null)
	          {
	        	  resultadoAccionGuardarNuevo="El registro ya existe en la base de datos";            
	          }
	          else
	          {
          			resultadoAccionGuardarNuevo=trazabilidadBean.guardarCambios(parmMapeoTrazabilidad,true);
//JAVI          			this. resultadoAccionGuardarNuevo.setCodigoInterno(parmMapeoTrazabilidad.getCodigo());
	          }
        }
	    catch(Exception exception)
	    {
	    	this.serNotif.mensajeError("Mantenimiento Trazabilidad Bean - guardarNuevo"+exception.getMessage());
	    }	    
	    return resultadoAccionGuardarNuevo;
	}	
	
	private Integer obtenerSiguienteValorCodigo() throws Exception
	{
		serverBasico sv=null;
		Integer codigoInterno = null;
		
//		sv=new serverBasico();
		con=this.conManager.establecerConexion();
		codigoInterno = sv.obtenerSiguienteCodigoInterno(con, "Trazabilidad", "codigo");
		return codigoInterno;
	}
	
	public String eliminar() 
	{
		String resultadoAccionEliminar = null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
	    	trazabilidadBean = new TrazabilidadBean();
	    	resultadoAccionEliminar=trazabilidadBean.inicializar();
	    }
	    catch(Exception ex)	    
	    {
			this.serNotif.mensajeError("Mantenimiento Trazabilidad Bean - eliminar"+ex.getMessage());
		}
	    return resultadoAccionEliminar;
	}
}