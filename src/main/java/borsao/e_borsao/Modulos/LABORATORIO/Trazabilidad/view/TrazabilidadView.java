package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.view;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.modelo.consultaVentasContainer;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.TrazabilidadGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.consultaTrazabilidadContainer;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class TrazabilidadView extends GridViewRefresh {

	public static final String VIEW_NAME="Trazabilidad";	
	private final String titulo = "TRAZABILIDAD LOTES";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private Button btnExpandir = null;
	
	private OpcionesForm form;
	

    public TrazabilidadView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
    	this.btnExpandir= new Button("Expandir");
    	this.btnExpandir.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.btnExpandir.addStyleName(ValoTheme.BUTTON_TINY);
    	this.btnExpandir.setVisible(true);
    	this.btnExpandir.setIcon(FontAwesome.MINUS_CIRCLE);
    	this.btnExpandir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			expandirNodos();
    		}
    	});

        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
        
        this.topLayoutL.addComponent(this.btnExpandir);
    }
    

    public void verForm(boolean r_busqueda)
    {
    	this.form.addStyleName("visible");
    	this.form.setEnabled(r_busqueda);    	
    }
    
    public void newForm()
    {
    	
    }
    
    private void expandirNodos()
    {
    	consultaTrazabilidadContainer container = expandirNodosC(((TrazabilidadGrid) this.grid).container, true);
        this.grid.setContainerDataSource(container);
	}
	
	private consultaTrazabilidadContainer expandirNodosC(consultaTrazabilidadContainer container, boolean r_expandir)
	{
		for (Object item: container.getItemIds().toArray()) 
		{
			//abre todos
			System.out.println("arranco " + new Date().toString());
			if (container.isCollapsed(item) && container.hasChildren(item)) container.setCollapsed(item, !r_expandir);
			if (container.getChildren(item)!=null)
			{
				this.expandirNodosR(container, item, r_expandir);
			}			
		}
		System.out.println("termino " + new Date().toString());
		return container;
	}    	
    
	private void expandirNodosR(consultaTrazabilidadContainer r_container, Object r_item, boolean r_expandir)
	{
		
		if (r_container.isCollapsed(r_item) && r_container.hasChildren(r_item))
		{
			r_container.setCollapsed(r_item, !r_expandir);
		}
		
		if (r_container.getChildren(r_item)!=null && r_container.hasChildren(r_item))
		{
			for (Object item2: r_container.getChildren(r_item).toArray())
			{
				this.expandirNodosR(r_container, item2, r_expandir);
			}
		}
	}
	
    public void generarGrid(HashMap<String , String> r_opcionesEscogidas)
    {
    	grid = new TrazabilidadGrid(r_opcionesEscogidas,isHayGrid());
    	if (grid==null || grid.getColumn(consultaTrazabilidadContainer.getColumnaPrincipal())==null)
    	{
    		Notificaciones.getInstance().mensajeError("Error al generar los datos. Revisa el Lote");
    		this.setHayGrid(false);
    		grid.removeAllColumns();			
    		barAndGridLayout.removeComponent(grid);
			grid=null;	
    	}
    	else
    	{
    		this.setHayGrid(true);
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);
    	}
      
    }

	@Override
	public void filaSeleccionada(Object r_fila) {
		
	}

    @Override
    public void reestablecerPantalla() {
    	
    }
    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
