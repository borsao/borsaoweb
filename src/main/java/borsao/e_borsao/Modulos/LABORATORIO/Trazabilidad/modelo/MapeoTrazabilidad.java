package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo;

import java.sql.Timestamp;

public class MapeoTrazabilidad implements java.io.Serializable
{

	private Integer codigo;
	private Integer diasSal;
	private Integer diasSal_tra;
	
	private Timestamp fecha;
	private Timestamp fecha_tra;
		
	private String parte;
	private String linea;
	private String seccion;
	private String tipo;
	private String localizacion;
	private String vino;
	private String partida;
	private String loteSalida;
	private String referencia;
	private String almacen;
	private String nivel;
	private String padre;
	private String deposito;
	private String impreso;
	
	private Double litros;
	private Double porcentaje;	
	private Double cantidad;

	private String parte_tra;
	private String linea_tra;
	private String seccion_tra;
	private String tipo_tra;
	private String localizacion_tra;
	private String vino_tra;
	private String partida_tra;
	private String loteSalida_tra;
	private String referencia_tra;
	private String almacen_tra;
	private String nivel_tra;
	private String padre_tra;
	private String deposito_tra;
	
	
	private Double litros_tra;
	private Double cantidad_tra;
	private Double porcentaje_tra;

	
	public MapeoTrazabilidad()
	{		
	
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public Integer getDiasSal() {
		return diasSal;
	}
	
	public void setDiasSal(Integer diasSal) {
		this.diasSal = diasSal;
	}
	
	public Timestamp getFecha() {
		return fecha;
	}
	
	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}
	
	public String getParte() {
		return parte;
	}
	
	public void setParte(String parte) {
		this.parte = parte;
	}
	
	public String getLinea() {
		return linea;
	}
	
	public void setLinea(String linea) {
		this.linea = linea;
	}
	
	public String getSeccion() {
		return seccion;
	}
	
	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getLocalizacion() {
		return localizacion;
	}
	
	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}
	
	public String getVino() {
		return vino;
	}
	
	public void setVino(String vino) {
		this.vino = vino;
	}
	
	public String getPartida() {
		return partida;
	}
	
	public void setPartida(String partida) {
		this.partida = partida;
	}
	
	public String getLoteSalida() {
		return loteSalida;
	}
	
	public void setLoteSalida(String loteSalida) {
		this.loteSalida = loteSalida;
	}
	
	public String getReferencia() {
		return referencia;
	}
	
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	
	public String getAlmacen() {
		return almacen;
	}
	
	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}
	
	public Double getLitros() {
		return litros;
	}
	
	public void setLitros(Double litros) {
		this.litros = litros;
	}
	
	public Double getCantidad() {
		return cantidad;
	}
	
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getParte_tra() {
		return parte_tra;
	}

	public void setParte_tra(String parte_tra) {
		this.parte_tra = parte_tra;
	}

	public String getLinea_tra() {
		return linea_tra;
	}

	public void setLinea_tra(String linea_tra) {
		this.linea_tra = linea_tra;
	}

	public String getSeccion_tra() {
		return seccion_tra;
	}

	public void setSeccion_tra(String seccion_tra) {
		this.seccion_tra = seccion_tra;
	}

	public String getTipo_tra() {
		return tipo_tra;
	}

	public void setTipo_tra(String tipo_tra) {
		this.tipo_tra = tipo_tra;
	}

	public String getLocalizacion_tra() {
		return localizacion_tra;
	}

	public void setLocalizacion_tra(String localizacion_tra) {
		this.localizacion_tra = localizacion_tra;
	}

	public String getVino_tra() {
		return vino_tra;
	}

	public void setVino_tra(String vino_tra) {
		this.vino_tra = vino_tra;
	}

	public String getPartida_tra() {
		return partida_tra;
	}

	public void setPartida_tra(String partida_tra) {
		this.partida_tra = partida_tra;
	}

	public String getLoteSalida_tra() {
		return loteSalida_tra;
	}

	public void setLoteSalida_tra(String loteSalida_tra) {
		this.loteSalida_tra = loteSalida_tra;
	}

	public String getReferencia_tra() {
		return referencia_tra;
	}

	public void setReferencia_tra(String referencia_tra) {
		this.referencia_tra = referencia_tra;
	}

	public String getAlmacen_tra() {
		return almacen_tra;
	}

	public void setAlmacen_tra(String almacen_tra) {
		this.almacen_tra = almacen_tra;
	}

	public String getNivel_tra() {
		return nivel_tra;
	}

	public void setNivel_tra(String nivel_tra) {
		this.nivel_tra = nivel_tra;
	}

	public Double getLitros_tra() {
		return litros_tra;
	}

	public void setLitros_tra(Double litros_tra) {
		this.litros_tra = litros_tra;
	}

	public Double getCantidad_tra() {
		return cantidad_tra;
	}

	public void setCantidad_tra(Double cantidad_tra) {
		this.cantidad_tra = cantidad_tra;
	}

	public Timestamp getFecha_tra() {
		return fecha_tra;
	}

	public void setFecha_tra(Timestamp fecha_tra) {
		this.fecha_tra = fecha_tra;
	}

	public String getPadre() {
		return padre;
	}

	public void setPadre(String padre) {
		this.padre = padre;
	}

	public Double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public Double getPorcentaje_tra() {
		return porcentaje_tra;
	}

	public void setPorcentaje_tra(Double porcentaje_tra) {
		this.porcentaje_tra = porcentaje_tra;
	}

	public String getDeposito() {
		return deposito;
	}

	public void setDeposito(String deposito) {
		this.deposito = deposito;
	}

	public String getDeposito_tra() {
		return deposito_tra;
	}

	public void setDeposito_tra(String deposito_tra) {
		this.deposito_tra = deposito_tra;
	}

	public String getPadre_tra() {
		return padre_tra;
	}

	public void setPadre_tra(String padre_tra) {
		this.padre_tra = padre_tra;
	}

	public Integer getDiasSal_tra() {
		return diasSal_tra;
	}

	public void setDiasSal_tra(Integer diasSal_tra) {
		this.diasSal_tra = diasSal_tra;
	}

	public String getImpreso() {
		return impreso;
	}

	public void setImpreso(String impreso) {
		this.impreso = impreso;
	}
}