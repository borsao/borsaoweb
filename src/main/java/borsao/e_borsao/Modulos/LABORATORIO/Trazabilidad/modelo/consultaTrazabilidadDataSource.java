package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vaadin.server.FontAwesome;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.LABORATORIO.Analiticas.modelo.MapeoAnaliticas;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server.consultaTrazabilidadServer;

public class consultaTrazabilidadDataSource {

	static HashMap<String, String> opcionesUsuario = null;
	private static consultaTrazabilidadServer cts = null;
    private static List<Object[]> rootNodes = null;
    private static Map<Object, List<Object[]>> children = new HashMap<>();
    private static Set<Object[]> leaves = new HashSet<>();
    public ResultSet rs = null;

    public consultaTrazabilidadDataSource(HashMap<String, String> r_hashOpciones, boolean r_eliminar) {
    	
    	if (r_eliminar)
    	{
    		this.rootNodes=null;
    	}
    	this.rootNodes = new ArrayList<>();
    	this.setOpciones(r_hashOpciones);
        rs = populateWithRandomHierarchicalData();
        
    }

    static void setOpciones(HashMap<String, String> r_hashOpciones)
    {
    	opcionesUsuario=r_hashOpciones;
    }
    
    static List<Object[]> getRoot() {
        return rootNodes;
    }

    static List<Object[]> getChildren(Object parent) {
        return children.get(parent);
    }

    static boolean isLeaf(Object itemId) {
        return leaves.contains(itemId);
    }

    private static ResultSet populateWithRandomHierarchicalData() {
        ResultSet datosLote = null;
        Double litros = null;
        
        Object[] allProjects = new Object[]{"",""};        
        
        Notificaciones.getInstance().mensajeSeguimiento("Empezamos" + new Date());
        cts=consultaTrazabilidadServer.getInstance();

        datosLote = cts.datosLote(opcionesUsuario.get("Lote"));

        Notificaciones.getInstance().mensajeSeguimiento("Global " + new Date());
        
        if (datosLote!=null)
        {
        	litros = new Double(0);
    		try
    		{
    			datosLote.last();
    			int total = datosLote.getRow();
    			datosLote.beforeFirst();
    			
				while(datosLote.next())
				{
					litros=litros + datosLote.getDouble("trz_lit");
					String nuevoNivel = "Lote " + datosLote.getString("trz_ls") + " - Producto " + datosLote.getString("trz_vin") + " - " + litros.toString()+ " L - " + RutinasFechas.restarDiasFecha(datosLote.getString("trz_fec"),new Integer(datosLote.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLote.getString("trz_fec")) + " parte " + datosLote.getString("trz_par") + " linea " + datosLote.getString("trz_lin");
					Notificaciones.getInstance().mensajeSeguimiento(nuevoNivel);
					
					if (total==datosLote.getRow())
					{
						int columnas=0;
						nuevoNivel = "Lote " + datosLote.getString("trz_ls") + " - Producto " + datosLote.getString("trz_vin") + " - " + litros.toString()+ " L - " + RutinasFechas.restarDiasFecha(datosLote.getString("trz_fec"),new Integer(datosLote.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLote.getString("trz_fec"));
						allProjects[columnas]=establecerIcono("inicio") + " "  + nuevoNivel;
						columnas=columnas +1;
						allProjects[columnas]="";
					}
				}
				
				
				rootNodes.add(allProjects);
				if (datosLote.getRow()>=0)
				{
					datosLote.beforeFirst();
					while(datosLote.next())
					{
						Object[] hijos = new Object[]{"",""};      
						int columnas=0;
	
						MapeoTrazabilidad mapeoTrazabilidad = rellenarMapeo(datosLote);
						mapeoTrazabilidad.setPorcentaje(new Double(Math.round((mapeoTrazabilidad.getLitros()/litros)*100)));
						if (mapeoTrazabilidad.getTipo().equals("SE"))
						{
							String nuevoNivel = mapeoTrazabilidad.getPorcentaje().toString() + "% Dep " + mapeoTrazabilidad.getLocalizacion() + "- Mov. " + mapeoTrazabilidad.getTipo() + " - " + mapeoTrazabilidad.getAlmacen() + " - Producto " + mapeoTrazabilidad.getVino() + " - " + mapeoTrazabilidad.getLitros().toString() + " L - " +  RutinasFechas.restarDiasFecha(datosLote.getString("trz_fec"),new Integer(datosLote.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLote.getString("trz_fec")) + " (" + mapeoTrazabilidad.getPartida() + ")"; 
							hijos[columnas]=establecerIcono(mapeoTrazabilidad.getTipo()) + " "  + nuevoNivel;
							columnas=columnas +1;
							hijos[columnas]=mapeoTrazabilidad;
							addChild(allProjects, hijos);
							
							buscarAnaliticas(mapeoTrazabilidad, hijos);
						}	
					}
				}
    		}
    		catch (Exception ex)
    		{
    			ex.printStackTrace();
    		}
    		        
        }
        
        Notificaciones.getInstance().mensajeSeguimiento("Terminamos" + new Date());
        return datosLote;
    }

    public static void recuperarHijos(Object parent, MapeoTrazabilidad r_mapeo, boolean r_parte) {
        ResultSet datosLote = null;
        ResultSet datosLoteParte = null;
        Object[] hijos = null;
        Object[] hijos2 = null;
        String nuevoNivel=null;
        MapeoTrazabilidad mapeo =null;
        
        cts=consultaTrazabilidadServer.getInstance();

        if (parent!=null && ((Object[]) parent)[1]!=null && ((Object[]) parent)[1]!="")
        {
        	if (r_parte)
        	{
        		datosLote = cts.datosLoteParte(r_mapeo);
        		mapeo=r_mapeo;
        	}
        	else
        	{
        		mapeo = (MapeoTrazabilidad) ((Object[]) parent)[1];
        		datosLote = cts.datosLotePartida(mapeo);
        	}
        }
        
        if (datosLote!=null)
        {
    		try
    		{
				while(datosLote.next())
				{

					MapeoTrazabilidad mapeoTrazabilidad = new MapeoTrazabilidad();
					mapeoTrazabilidad = rellenarMapeo(datosLote);
					
					if (mapeoTrazabilidad.getTipo().equals("T") || mapeoTrazabilidad.getTipo().equals("EV") || mapeoTrazabilidad.getTipo().equals("EL") || mapeoTrazabilidad.getTipo().equals("EM"))
					{
					 	if (!mapeoTrazabilidad.getTipo().equals("T")) mapeoTrazabilidad.setPorcentaje(new Double(100));
					    mapeoTrazabilidad.setLitros(mapeo.getLitros());
					    mapeoTrazabilidad.setPadre(mapeo.getNivel());
					    mapeoTrazabilidad.setDeposito(mapeo.getLocalizacion());
					}
					
					if (mapeoTrazabilidad.getPorcentaje()!=null && mapeoTrazabilidad.getPorcentaje()!=0)
					{
						nuevoNivel = mapeoTrazabilidad.getPorcentaje().toString() + "% Dep " + mapeoTrazabilidad.getLocalizacion() + "- Mov. " + mapeoTrazabilidad.getTipo() + " - " + mapeoTrazabilidad.getAlmacen() + " - Producto " + mapeoTrazabilidad.getVino() + " - " + mapeoTrazabilidad.getLitros().toString() + " L - " +  RutinasFechas.restarDiasFecha(datosLote.getString("trz_fec"),new Integer(datosLote.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLote.getString("trz_fec")) + " (" + mapeoTrazabilidad.getPartida() + ")";
					}
					else
					{
						nuevoNivel = "Dep " + mapeoTrazabilidad.getLocalizacion() + "- Mov. " + mapeoTrazabilidad.getTipo() + " - " + mapeoTrazabilidad.getAlmacen() + " - Producto " + mapeoTrazabilidad.getVino() + " - " + mapeoTrazabilidad.getLitros().toString() + " L - " +  RutinasFechas.restarDiasFecha(datosLote.getString("trz_fec"),new Integer(datosLote.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLote.getString("trz_fec")) + " (" + mapeoTrazabilidad.getPartida() + ")";
					}
					
					if ((mapeoTrazabilidad.getTipo().equals("ET") || mapeoTrazabilidad.getTipo().equals("E") || mapeoTrazabilidad.getTipo().equals("EC")) )
					{
						
						datosLoteParte = cts.datosLoteParte(mapeoTrazabilidad);
						Double sumaLitros = cts.sumaLitrosLoteParte(mapeoTrazabilidad.getParte());
						while(datosLoteParte.next())
						{
							
							MapeoTrazabilidad mapeoTrazabilidadParte = new MapeoTrazabilidad();
							mapeoTrazabilidadParte = rellenarMapeo(datosLoteParte);
							mapeoTrazabilidad.setFecha(mapeo.getFecha());
				    		mapeoTrazabilidad.setDiasSal(mapeo.getDiasSal());
				    		
				    		mapeoTrazabilidadParte.setDeposito(mapeoTrazabilidad.getLocalizacion());
				    		
				    		if (mapeoTrazabilidadParte.getLitros()>sumaLitros)
				    		{
				    			mapeoTrazabilidadParte.setPorcentaje(new Double(100));			
//				    			mapeoTrazabilidadParte.setLitros(mapeo.getLitros());
				    		}
				    		else
				    		{
				    			mapeoTrazabilidadParte.setPorcentaje(new Double(Math.round((mapeoTrazabilidadParte.getLitros()/sumaLitros)*100)));
				    			mapeoTrazabilidadParte.setLitros(new Double(Math.round(mapeo.getLitros()*(mapeoTrazabilidadParte.getLitros()/sumaLitros))));
				    		}		
							
							if (mapeoTrazabilidadParte.getPorcentaje()!=null && mapeoTrazabilidadParte.getPorcentaje()!=0)
							{
								nuevoNivel = mapeoTrazabilidadParte.getPorcentaje().toString() + "% Dep " + mapeoTrazabilidadParte.getLocalizacion() + "- Mov. " + mapeoTrazabilidadParte.getTipo() + " - " + mapeoTrazabilidadParte.getAlmacen() + " - Producto " + mapeoTrazabilidadParte.getVino() + " - " + mapeoTrazabilidadParte.getLitros().toString() + " L - " +  RutinasFechas.restarDiasFecha(datosLoteParte.getString("trz_fec"),new Integer(datosLoteParte.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLoteParte.getString("trz_fec")) + " (" + mapeoTrazabilidadParte.getPartida() + ")";
							}
							else
							{
								nuevoNivel = "Dep " + mapeoTrazabilidadParte.getLocalizacion() + "- Mov. " + mapeoTrazabilidadParte.getTipo() + " - " + mapeoTrazabilidadParte.getAlmacen() + " - Producto " + mapeoTrazabilidadParte.getVino() + " - " + mapeoTrazabilidadParte.getLitros().toString() + " L - " +  RutinasFechas.restarDiasFecha(datosLoteParte.getString("trz_fec"),new Integer(datosLoteParte.getInt("trz_ds"))) + " a " + RutinasFechas.convertirDeFecha(datosLoteParte.getString("trz_fec")) + " (" + mapeoTrazabilidadParte.getPartida() + ")";
							}
							if (!mapeoTrazabilidadParte.getTipo().equals("ET") && !mapeoTrazabilidadParte.getTipo().equals("E") && !mapeoTrazabilidadParte.getTipo().equals("EC") && !mapeoTrazabilidadParte.getTipo().equals("SE")) 
							{
								hijos2 = new Object[]{"",""};
								int columnasHijos2 = 0;
								
								hijos2[columnasHijos2]=establecerIcono(mapeoTrazabilidadParte.getTipo()) + " "  + nuevoNivel;
								columnasHijos2=columnasHijos2 +1;
								hijos2[columnasHijos2]=mapeoTrazabilidadParte;
								if (parent!=null)
								{
									addChild(parent,hijos2);

									Object padre = new Object();
									padre=parent;
									buscarAnaliticas(mapeoTrazabilidadParte, padre);
								}
								else
								{
									addChild(hijos,hijos2);

									Object padre = new Object();
									padre=hijos;
									buscarAnaliticas(mapeoTrazabilidadParte, padre);
								}
							}
						}
					}
					else
					{
						if (!mapeoTrazabilidad.getTipo().equals("SC") && !mapeoTrazabilidad.getTipo().equals("ST") && !mapeoTrazabilidad.getTipo().equals("SE"))
						{
							hijos = new Object[]{"",""};      
							int columnas=0;
							
							if (mapeoTrazabilidad.getTipo().equals("T") || mapeoTrazabilidad.getTipo().equals("EV") || mapeoTrazabilidad.getTipo().equals("EL") || mapeoTrazabilidad.getTipo().equals("EM"))
							{
								leaves.add(hijos);
							}
							hijos[columnas]=establecerIcono(mapeoTrazabilidad.getTipo()) + " "  + nuevoNivel;
							columnas=columnas +1;
							hijos[columnas]=mapeoTrazabilidad;
							addChild(parent, hijos);
							
							Object padre = new Object();
							padre=parent;
							if (!mapeoTrazabilidad.getTipo().equals("T") && !mapeoTrazabilidad.getTipo().equals("EV") && !mapeoTrazabilidad.getTipo().equals("EL") && !mapeoTrazabilidad.getTipo().equals("EM"))
							{
								buscarAnaliticas(mapeoTrazabilidad, padre);
							}
						}	
					}
				}				

    		}
    		catch (Exception ex)
    		{
    			ex.printStackTrace();
    		}
    		        
        }
    }
    
    private static void addChild(Object parent, Object[] child) {
        if (!children.containsKey(parent)) {
            children.put(parent, new ArrayList<Object[]>());
        }
        children.get(parent).add(child);        
    }
    
    
    private static MapeoTrazabilidad rellenarMapeo(ResultSet rsMovPartida) 
    {
    	MapeoTrazabilidad mapeoTrazabilidad = new MapeoTrazabilidad();
	    try {
	    	mapeoTrazabilidad.setParte(rsMovPartida.getString("trz_par"));
			mapeoTrazabilidad.setLinea(rsMovPartida.getString("trz_lin"));
			mapeoTrazabilidad.setFecha(rsMovPartida.getTimestamp("trz_fec"));
			mapeoTrazabilidad.setSeccion(rsMovPartida.getString("trz_sec"));
			mapeoTrazabilidad.setTipo(rsMovPartida.getString("trz_tip"));
			mapeoTrazabilidad.setLocalizacion(rsMovPartida.getString("trz_loc"));
			mapeoTrazabilidad.setVino(rsMovPartida.getString("trz_vin"));
			mapeoTrazabilidad.setLitros(new Double(rsMovPartida.getDouble("trz_lit")));
			mapeoTrazabilidad.setDiasSal(new Integer(rsMovPartida.getInt("trz_ds")));
			mapeoTrazabilidad.setCantidad(new Double(rsMovPartida.getDouble("trz_can")));
			mapeoTrazabilidad.setPartida(rsMovPartida.getString("trz_prt"));
			mapeoTrazabilidad.setLoteSalida(rsMovPartida.getString("trz_ls"));
			mapeoTrazabilidad.setReferencia(rsMovPartida.getString("trz_ref"));
			if (rsMovPartida.getString("con_des")!=null)
			{
				mapeoTrazabilidad.setAlmacen(rsMovPartida.getString("con_des"));
			} 
			else
			{
				mapeoTrazabilidad.setAlmacen(rsMovPartida.getString("trz_alm"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	    return mapeoTrazabilidad;
    }
    
    private static String establecerIcono(String r_tipo)
    {
    	String iconCode=null;
    	String color = "lightgrey";
    	
    	iconCode = "<span class=\"v-icon\" style=\"font-family: "
                + FontAwesome.DOT_CIRCLE_O.getFontFamily() + ";color:" + color
                + "\">&#x"
                + Integer.toHexString(FontAwesome.DOT_CIRCLE_O.getCodepoint())
                + ";</span>"; 
//    	
		if (r_tipo.equals("SE") )
		{
	    	iconCode = "<span class=\"v-icon\" style=\"font-family: "
	                + FontAwesome.BUILDING.getFontFamily() + ";color:" + color
	                + "\">&#x"
	                + Integer.toHexString(FontAwesome.BUILDING.getCodepoint())
	                + ";</span>";
		}
		else if (r_tipo.equals("SC") || r_tipo.equals("ST"))
		{
	    	iconCode = "<span class=\"v-icon\" style=\"font-family: "
	                + FontAwesome.DATABASE.getFontFamily() + ";color:" + color
	                + "\">&#x"
	                + Integer.toHexString(FontAwesome.DATABASE.getCodepoint())
	                + ";</span>";
			
		}
		else if (r_tipo.equals("T"))
		{
		iconCode = "<span class=\"v-icon\" style=\"font-family: "
                + FontAwesome.EYEDROPPER.getFontFamily() + ";color:" + color
                + "\">&#x"
                + Integer.toHexString(FontAwesome.EYEDROPPER.getCodepoint())
                + ";</span>";
		}
		else if (r_tipo.equals("A"))
		{
			iconCode = "<span class=\"v-icon\" style=\"font-family: "
	                + FontAwesome.FLASK.getFontFamily() + ";color:" + color
	                + "\">&#x"
	                + Integer.toHexString(FontAwesome.FLASK.getCodepoint())
	                + ";</span>";
			
		}
		else if (r_tipo.equals("I"))		
		{
			iconCode = "<span class=\"v-icon\" style=\"font-family: "
	                + FontAwesome.FLASK.getFontFamily() + ";color:" + color
	                + "\">&#x"
	                + Integer.toHexString(FontAwesome.FLASK.getCodepoint())
	                + ";</span>";
			
		}
		else if (r_tipo.equals("inicio"))
		{
	    	iconCode = "<span class=\"v-icon\" style=\"font-family: "
	                + FontAwesome.SITEMAP.getFontFamily() + ";color:" + color
	                + "\">&#x"
	                + Integer.toHexString(FontAwesome.SITEMAP.getCodepoint())
	                + ";</span>";
		}
    	return iconCode ;
    }
    
    private static void buscarAnaliticas(MapeoTrazabilidad r_mapeo, Object parent)
	{
		RutinasFechas rutFecha = null;
		ResultSet rstAnaliticas = null;
		
		rutFecha = new RutinasFechas();
		try
		{
			if (opcionesUsuario.get("verAnalitica").equals("Si"))
			{
				rstAnaliticas = cts.recuperaAnaliticas(r_mapeo.getLocalizacion(), rutFecha.restarDiasFecha(r_mapeo.getFecha().toString(),r_mapeo.getDiasSal()) , rutFecha.convertirDeFecha(r_mapeo.getFecha().toString()));
				while (rstAnaliticas.next())
				{
					MapeoAnaliticas mapeoAnaliticas = new MapeoAnaliticas();
		            mapeoAnaliticas.setCodigo(new Integer(rstAnaliticas.getInt("anal_id")));
		            mapeoAnaliticas.setFecha(rstAnaliticas.getTimestamp("anal_fec"));
		            mapeoAnaliticas.setVino(rstAnaliticas.getString("anal_vin"));
		            mapeoAnaliticas.setSulfuroso(new Double(rstAnaliticas.getDouble("anal_sul")));
		            if (rstAnaliticas.getString("anal_sul")==null)
		            {
		            	mapeoAnaliticas.setSulfuroso(null);
		            }
		            mapeoAnaliticas.setTecSulfuroso(rstAnaliticas.getString("anal_tec_sul"));
		            mapeoAnaliticas.setSulfurosoT(new Double(rstAnaliticas.getDouble("anal_sult")));
		            if (rstAnaliticas.getString("anal_sult")==null)
		            {
		            	mapeoAnaliticas.setSulfurosoT(null);
		            }
		            mapeoAnaliticas.setTecSulfurosoT(rstAnaliticas.getString("anal_tec_sult"));
		            mapeoAnaliticas.setAzucar(new Double(rstAnaliticas.getDouble("anal_azu")));
		            if (rstAnaliticas.getString("anal_azu")==null)
		            {
		            	mapeoAnaliticas.setAzucar(null);
		            }
		            mapeoAnaliticas.setTecAzucar(rstAnaliticas.getString("anal_tec_azu"));
		            mapeoAnaliticas.setGrado(new Double(rstAnaliticas.getDouble("anal_gra")));
		            if (rstAnaliticas.getString("anal_gra")==null)
		            {
		            	mapeoAnaliticas.setGrado(null);
		            }
		            mapeoAnaliticas.setTecGrado(rstAnaliticas.getString("anal_tec_gra"));
		            mapeoAnaliticas.setGlufru(new Double(rstAnaliticas.getDouble("anal_glu")));
		            if (rstAnaliticas.getString("anal_glu")==null)
		            {
		            	mapeoAnaliticas.setGlufru(null);
		            }
		            mapeoAnaliticas.setTecGlufru(rstAnaliticas.getString("anal_tec_glu"));
		            mapeoAnaliticas.setAcidezt(new Double(rstAnaliticas.getDouble("anal_aci")));
		            if (rstAnaliticas.getString("anal_aci")==null)
		            {
		            	mapeoAnaliticas.setAcidezt(null);
		            }
		            mapeoAnaliticas.setTecAcidezt(rstAnaliticas.getString("anal_tec_aci"));
		            mapeoAnaliticas.setPh(new Double(rstAnaliticas.getDouble("anal_ph")));
		            if (rstAnaliticas.getString("anal_ph")==null)
		            {
		            	mapeoAnaliticas.setPh(null);
		            }
		            mapeoAnaliticas.setTecPh(rstAnaliticas.getString("anal_tec_ph"));
		            mapeoAnaliticas.setMalico(new Double(rstAnaliticas.getDouble("anal_mal")));
		            if (rstAnaliticas.getString("anal_mal")==null)
		            {
		            	mapeoAnaliticas.setMalico(null);
		            }
		            mapeoAnaliticas.setTecMalico(rstAnaliticas.getString("anal_tec_mal"));
		            mapeoAnaliticas.setColor(new Double(rstAnaliticas.getDouble("anal_col")));
		            if (rstAnaliticas.getString("anal_col")==null)
		            {
		            	mapeoAnaliticas.setColor(null);
		            }
		            mapeoAnaliticas.setTecColor(rstAnaliticas.getString("anal_tec_col"));
		            mapeoAnaliticas.setPolif(new Double(rstAnaliticas.getDouble("anal_pol")));
		            if (rstAnaliticas.getString("anal_pol")==null)
		            {
		            	mapeoAnaliticas.setPolif(null);
		            }
		            mapeoAnaliticas.setTecPolif(rstAnaliticas.getString("anal_tec_pol"));
		            mapeoAnaliticas.setResponsable(rstAnaliticas.getString("anal_res"));
		            mapeoAnaliticas.setObservaciones(rstAnaliticas.getString("anal_obs"));    
		            mapeoAnaliticas.setLitros(new Double(rstAnaliticas.getInt("anal_lit")));
		            mapeoAnaliticas.setLnkDeposito(rstAnaliticas.getString("anal_dep"));    
		
		            String sol = null;
		            String sot = null;
		            String av = null;
		            String gra = null;
		            String glu = null;
		            String at = null;
		            String ph = null;
		            String mal = null;
		            String ic = null;
		            String pt = null;
		            String ltr = null;
		            
		            if (mapeoAnaliticas.getSulfuroso()!=null)
		            {
		            	sol=mapeoAnaliticas.getSulfuroso().toString()+mapeoAnaliticas.getTecSulfuroso();
		            }
		            if (mapeoAnaliticas.getSulfurosoT()!=null)
		            {
		            	sot=mapeoAnaliticas.getSulfurosoT().toString()+mapeoAnaliticas.getTecSulfurosoT();
		            }
		            if (mapeoAnaliticas.getAzucar()!=null)
		            {
		            	av=mapeoAnaliticas.getAzucar().toString()+mapeoAnaliticas.getTecAzucar();
		            }
		            if (mapeoAnaliticas.getGrado()!=null)
		            {
		            	gra=mapeoAnaliticas.getGrado().toString()+mapeoAnaliticas.getTecGrado();
		            }
		            if (mapeoAnaliticas.getGlufru()!=null)
		            {
		            	glu=mapeoAnaliticas.getGlufru().toString()+mapeoAnaliticas.getTecGlufru();
		            }
		            if (mapeoAnaliticas.getAcidezt()!=null)
		            {
		            	at=mapeoAnaliticas.getAcidezt().toString()+mapeoAnaliticas.getTecAcidezt();
		            }
		            if (mapeoAnaliticas.getPh()!=null)
		            {
		            	ph=mapeoAnaliticas.getPh().toString()+mapeoAnaliticas.getTecPh();
		            }
		            if (mapeoAnaliticas.getMalico()!=null)
		            {
		            	mal=mapeoAnaliticas.getMalico().toString()+mapeoAnaliticas.getTecMalico();
		            }
		            if (mapeoAnaliticas.getColor()!=null)
		            {
		            	ic=mapeoAnaliticas.getColor().toString()+mapeoAnaliticas.getTecColor();
		            }
		            if (mapeoAnaliticas.getPolif()!=null)
		            {
		            	pt=mapeoAnaliticas.getPolif().toString()+mapeoAnaliticas.getTecPolif();
		            }
		            if (mapeoAnaliticas.getLitros()!=null)
		            {
		            	ltr=mapeoAnaliticas.getLitros().toString();
		            }
					String descr = "Dep " + r_mapeo.getLocalizacion() + " - Producto " + mapeoAnaliticas.getVino() + " - " + ltr +  "L. ANALITICA: ";
					if (sol!=null)
					{
						descr=descr + " - SO2 " + sol ;
					}
					if (sot!=null)
					{
						descr=descr + " - SO2T " + sot;
					}
					if (av!=null)
					{
						descr=descr + " - Ac V " + av;
					}
					if (gra!=null)
					{
						descr=descr + " - Gr.Al " + gra;
					}
					if (glu!=null)
					{
						descr = descr + " - Glu/Fru " + glu ;
					}
					if (at!=null)
					{
						descr = descr + " - Ac. T " + at ;
					}
					if (ph!=null)
					{
						descr = descr + " - PH " + ph ;
					}
					if (mal!=null)
					{
						descr=descr + " - Mal " + mal ;					
					}
					if (ic!=null)
					{
						descr = descr + " - IC " + ic ;
					}
					if (pt != null)
					{
						descr = descr + " - Pol.T " + pt;
					}
					descr = descr + " - " + rutFecha.convertirDeFecha(mapeoAnaliticas.getFecha().toString());
					r_mapeo.setTipo("A");
					

					Object[] analiticas = new Object[]{"",""};      
					int columnas=0;

					leaves.add(analiticas);
					analiticas[columnas]=establecerIcono(r_mapeo.getTipo()) + " "  + descr;
					columnas=columnas +1;
					analiticas[columnas]=r_mapeo;
					addChild(parent, analiticas);
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}