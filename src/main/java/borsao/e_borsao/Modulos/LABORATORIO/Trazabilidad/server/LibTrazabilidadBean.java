package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;

public class LibTrazabilidadBean 
{
    MapeoTrazabilidad mapeoTrazabilidad = null;
	private connectionManager conManager = null;
	private Connection con = null;	
	private static LibTrazabilidadBean instance;

    
    public LibTrazabilidadBean()
    {
		this.conManager = connectionManager.getInstance();
    }
    
    public static LibTrazabilidadBean getInstance() 
	{
		if (instance == null) 
		{
			instance = new LibTrazabilidadBean();			
		}
		return instance;
	}
    
    public String recuperaNivel(String r_nivel) throws Exception
	{
	   /**
	    * Metodo que me devuelve todas las areas que estan utilizadas por
	    * la definicion de los programas.
	    */
	   
	   PreparedStatement preparedStatement = null;
	   ResultSet resultSet = null;
	   StringBuffer cadenaSQL = new StringBuffer();
	   MapeoTrazabilidad mapeoTrazabilidad = null;
	   int niveles = 0;
	   
	   if (r_nivel==null) r_nivel="%";
	   
	   try
	   {		   
		   	cadenaSQL.append(" SELECT trazabilidad.parte trz_par, ");
		    cadenaSQL.append(" trazabilidad.linea trz_lin, ");
		    cadenaSQL.append(" trazabilidad.fecha trz_fec , ");
		    cadenaSQL.append(" trazabilidad.seccion trz_sec, ");
		    cadenaSQL.append(" trazabilidad.tipo trz_tip, ");
		    cadenaSQL.append(" trazabilidad.localizacion trz_loc, ");
		    cadenaSQL.append(" trazabilidad.vino trz_vin, ");
		    cadenaSQL.append(" trazabilidad.litros trz_lit, ");
		    cadenaSQL.append(" trazabilidad.diasSal trz_ds, ");
		    cadenaSQL.append(" trazabilidad.cantidad trz_can, ");
		    cadenaSQL.append(" trazabilidad.partida trz_prt, ");
		    cadenaSQL.append(" trazabilidad.loteSalida trz_ls, ");
		    cadenaSQL.append(" trazabilidad.referencia trz_ref, ");
		    cadenaSQL.append(" trazabilidad.nivel trz_niv, ");
		    cadenaSQL.append(" trazabilidad.padre trz_pad, ");
		    cadenaSQL.append(" trazabilidad.almacen trz_alm");		    
		    cadenaSQL.append(" FROM trazabilidad ");
		    if (!r_nivel.contentEquals("%"))
		    {
		    	cadenaSQL.append(" WHERE padre = '" + r_nivel + "'");
		    }

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());      
		    resultSet = preparedStatement.executeQuery();
	    
		    while (resultSet.next())
		    {        
		    	 mapeoTrazabilidad = new MapeoTrazabilidad();
	             mapeoTrazabilidad.setParte(resultSet.getString("trz_par"));
	             mapeoTrazabilidad.setLinea(resultSet.getString("trz_lin"));
	             mapeoTrazabilidad.setFecha(resultSet.getTimestamp("trz_fec"));
	             mapeoTrazabilidad.setSeccion(resultSet.getString("trz_sec"));
	             mapeoTrazabilidad.setTipo(resultSet.getString("trz_tip"));
	             mapeoTrazabilidad.setLocalizacion(resultSet.getString("trz_loc"));
	             mapeoTrazabilidad.setVino(resultSet.getString("trz_vin"));
	             mapeoTrazabilidad.setLitros(new Double(resultSet.getDouble("trz_lit")));
	             mapeoTrazabilidad.setDiasSal(new Integer(resultSet.getInt("trz_ds")));
	             mapeoTrazabilidad.setCantidad(new Double(resultSet.getDouble("trz_can")));
	             mapeoTrazabilidad.setPartida(resultSet.getString("trz_prt"));
	             mapeoTrazabilidad.setLoteSalida(resultSet.getString("trz_ls"));
	             mapeoTrazabilidad.setReferencia(resultSet.getString("trz_ref"));
	             mapeoTrazabilidad.setNivel(resultSet.getString("trz_niv"));
	             mapeoTrazabilidad.setPadre(resultSet.getString("trz_pad"));
	             mapeoTrazabilidad.setAlmacen(resultSet.getString("trz_alm"));             
	             niveles=niveles + 1;
		    }
	     
		    if (niveles!=0)
		    {
		    	niveles=niveles+1;
		    	if (r_nivel.equals("%"))	
		    	{
		    		return "1." + String.valueOf(niveles) ; 
		    	}	
		    	else
		    	{
		    		return r_nivel + "." + String.valueOf(niveles) ;
		    	}
		    }
		    else
		    {
		    	if (r_nivel.equals("%"))
		    	{
		    		return "1.1";
		    	}
		    	else
		    	{
		    		return r_nivel + ".1"  ;
		    	}
		    }
	   }
	   catch (Exception exception)
	   {
		   exception.printStackTrace();
	   }
	   finally
	   {
		   try
		   {
			   if (resultSet != null)
		       {
		    	   resultSet.close();
		       }
		       if (preparedStatement != null)
		       {
		    	   preparedStatement.close();
		       }
		   }
		   catch (SQLException sqlException)
		   {
			   sqlException.printStackTrace();
		   }
	   }
	   return "";
	}
}