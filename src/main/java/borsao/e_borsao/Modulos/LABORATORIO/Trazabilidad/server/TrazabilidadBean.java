package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;

/** 
 * Clase encargada de almacenar y eliminar registros en la BD.
 */
public class TrazabilidadBean
{
	private Integer codigoInterno = null;
	private connectionManager conManager = null;
	private Connection con = null;	

	
	public TrazabilidadBean()
	{
    	this.conManager = connectionManager.getInstance();
	}
	public TrazabilidadBean(MapeoTrazabilidad mapeoTrazabilidadRecibo)
	{
    	this.conManager = connectionManager.getInstance();
		this.buscoCodigoInterno(mapeoTrazabilidadRecibo.getParte(), mapeoTrazabilidadRecibo.getLinea());	
	}

	private void buscoCodigoInterno(String parmParte, String parmLinea)
	{
	   PreparedStatement preparedStatement = null;
	   ResultSet resultSet = null;
	   StringBuffer cadenaSQL = new StringBuffer();
	   try
	   {
         cadenaSQL.append(" SELECT trazabilidad.parte trz_par ");
	     cadenaSQL.append(" FROM trazabilidad ");
	     cadenaSQL.append(" WHERE trazabilidad.parte = ? ");
	     cadenaSQL.append(" AND trazabilidad.linea = ? ");
	     con=this.conManager.establecerConexion();
	     preparedStatement = con.prepareStatement(cadenaSQL.toString());
	     preparedStatement.setString(1,parmParte);
	     preparedStatement.setString(2,parmLinea);
	     resultSet = preparedStatement.executeQuery();
	     while (resultSet.next())
	     {        
             this.setCodigoInterno(new Integer(resultSet.getString("trz_par")));
	     }
	   }
	   catch (Exception exception)
	   {
	   		exception.printStackTrace();
	   }
	   finally
	   {
	     try
	     {
	       if (resultSet != null)
	       {
	         resultSet.close();
	       }
	       if (preparedStatement != null)
	       {
	         preparedStatement.close();
	       }
	     }
	     catch (SQLException sqlException)
	     {
	       sqlException.printStackTrace();
	     }
	   }
	}
	
	
	public String guardarCambios(MapeoTrazabilidad parmMapeoTrazabilidad, boolean parmGuardarNuevo)
    {
	  	PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
	    try
	    {
	    	if (!parmGuardarNuevo)
	    	{
	    		cadenaSQL.append(" UPDATE trazabilidad set ");
		        cadenaSQL.append(" trazabilidad.parte =?, ");
			    cadenaSQL.append(" trazabilidad.linea =?, ");
			    cadenaSQL.append(" trazabilidad.fecha =?, ");
			    cadenaSQL.append(" trazabilidad.seccion =?, ");
			    cadenaSQL.append(" trazabilidad.tipo =?, ");
			    cadenaSQL.append(" trazabilidad.localizacion =?, ");
			    cadenaSQL.append(" trazabilidad.vino =?, ");
			    cadenaSQL.append(" trazabilidad.litros =?, ");
			    cadenaSQL.append(" trazabilidad.diasSal =?, ");
			    cadenaSQL.append(" trazabilidad.cantidad =?, ");
			    cadenaSQL.append(" trazabilidad.partida =?, ");
			    cadenaSQL.append(" trazabilidad.loteSalida =?, ");
			    cadenaSQL.append(" trazabilidad.referencia =?, ");
			    cadenaSQL.append(" trazabilidad.almacen =?, ");
			    cadenaSQL.append(" trazabilidad.nivel =?, ");
			    cadenaSQL.append(" trazabilidad.padre =?, ");
			    cadenaSQL.append(" trazabilidad.porcentaje =?, ");
			    cadenaSQL.append(" trazabilidad.deposito =?, ");
			    cadenaSQL.append(" trazabilidad.impreso =? ");
			    cadenaSQL.append(" WHERE Trazabilidad.codigo = ? ");
			    con=this.conManager.establecerConexion();
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    preparedStatement.setString(1, parmMapeoTrazabilidad.getParte());
			    preparedStatement.setString(2, parmMapeoTrazabilidad.getLinea());
			    preparedStatement.setTimestamp(3, parmMapeoTrazabilidad.getFecha());
			    preparedStatement.setString(4, parmMapeoTrazabilidad.getSeccion());
			    preparedStatement.setString(5, parmMapeoTrazabilidad.getTipo());
			    preparedStatement.setString(6, parmMapeoTrazabilidad.getLocalizacion());
			    preparedStatement.setString(7, parmMapeoTrazabilidad.getVino());
			    
			    if (parmMapeoTrazabilidad.getLitros()!=null)
			    {
				    preparedStatement.setDouble(8, parmMapeoTrazabilidad.getLitros());
			    }
			    else
			    {
			    	preparedStatement.setNull(8, java.sql.Types.NULL);
			    }			    
			    if (parmMapeoTrazabilidad.getDiasSal()!=null)
			    {
				    preparedStatement.setInt(9, parmMapeoTrazabilidad.getDiasSal());
			    }
			    else
			    {
			    	preparedStatement.setNull(9, java.sql.Types.NULL);
			    }			    
			    if (parmMapeoTrazabilidad.getCantidad()!=null)
			    {
				    preparedStatement.setDouble(10, parmMapeoTrazabilidad.getCantidad());
			    }
			    else
			    {
			    	preparedStatement.setNull(10, java.sql.Types.NULL);
			    }
			    preparedStatement.setString(11, parmMapeoTrazabilidad.getPartida());
			    preparedStatement.setString(12, parmMapeoTrazabilidad.getLoteSalida());
			    preparedStatement.setString(13, parmMapeoTrazabilidad.getReferencia());
			    preparedStatement.setString(14, parmMapeoTrazabilidad.getAlmacen());
			    preparedStatement.setString(15, parmMapeoTrazabilidad.getNivel());
			    preparedStatement.setString(16, parmMapeoTrazabilidad.getPadre());
			    if (parmMapeoTrazabilidad.getPorcentaje()!=null)
			    {
				    preparedStatement.setDouble(17, parmMapeoTrazabilidad.getPorcentaje());
			    }
			    else
			    {
			    	preparedStatement.setNull(17, java.sql.Types.NULL);
			    }
			    preparedStatement.setString(18, parmMapeoTrazabilidad.getDeposito());
			    preparedStatement.setString(19, parmMapeoTrazabilidad.getImpreso());
			    preparedStatement.setInt(20, this.getCodigoInterno().intValue());
	    	}
	    	else
	    	{
	    		cadenaSQL.append(" INSERT INTO trazabilidad ( ");
	    		cadenaSQL.append(" trazabilidad.parte, ");
			    cadenaSQL.append(" trazabilidad.linea, ");
			    cadenaSQL.append(" trazabilidad.fecha, ");
			    cadenaSQL.append(" trazabilidad.seccion, ");
			    cadenaSQL.append(" trazabilidad.tipo, ");
			    cadenaSQL.append(" trazabilidad.localizacion, ");
			    cadenaSQL.append(" trazabilidad.vino, ");
			    cadenaSQL.append(" trazabilidad.litros, ");
			    cadenaSQL.append(" trazabilidad.diasSal, ");
			    cadenaSQL.append(" trazabilidad.cantidad, ");
			    cadenaSQL.append(" trazabilidad.partida, ");
			    cadenaSQL.append(" trazabilidad.loteSalida, ");
			    cadenaSQL.append(" trazabilidad.referencia, ");
			    cadenaSQL.append(" trazabilidad.almacen, ");
			    cadenaSQL.append(" trazabilidad.nivel,");
	    		cadenaSQL.append(" trazabilidad.padre,");
	    		cadenaSQL.append(" trazabilidad.porcentaje,");
	    		cadenaSQL.append(" trazabilidad.deposito,");
	    		cadenaSQL.append(" trazabilidad.impreso) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			    con=this.conManager.establecerConexion();
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    
			    preparedStatement.setString(1, parmMapeoTrazabilidad.getParte());
			    preparedStatement.setString(2, parmMapeoTrazabilidad.getLinea());
			    preparedStatement.setTimestamp(3, parmMapeoTrazabilidad.getFecha());
			    preparedStatement.setString(4, parmMapeoTrazabilidad.getSeccion());
			    preparedStatement.setString(5, parmMapeoTrazabilidad.getTipo());
			    preparedStatement.setString(6, parmMapeoTrazabilidad.getLocalizacion());
			    preparedStatement.setString(7, parmMapeoTrazabilidad.getVino());
			    
			    if (parmMapeoTrazabilidad.getLitros()!=null)
			    {
				    preparedStatement.setDouble(8, parmMapeoTrazabilidad.getLitros());
			    }
			    else
			    {
			    	preparedStatement.setNull(8, java.sql.Types.NULL);
			    }			    
			    if (parmMapeoTrazabilidad.getDiasSal()!=null)
			    {
				    preparedStatement.setInt(9, parmMapeoTrazabilidad.getDiasSal());
			    }
			    else
			    {
			    	preparedStatement.setNull(9, java.sql.Types.NULL);
			    }			    
			    if (parmMapeoTrazabilidad.getCantidad()!=null)
			    {
				    preparedStatement.setDouble(10, parmMapeoTrazabilidad.getCantidad());
			    }
			    else
			    {
			    	preparedStatement.setNull(10, java.sql.Types.NULL);
			    }
			    preparedStatement.setString(11, parmMapeoTrazabilidad.getPartida());
			    preparedStatement.setString(12, parmMapeoTrazabilidad.getLoteSalida());
			    preparedStatement.setString(13, parmMapeoTrazabilidad.getReferencia());
			    preparedStatement.setString(14, parmMapeoTrazabilidad.getAlmacen());
			    preparedStatement.setString(15, parmMapeoTrazabilidad.getNivel());
			    preparedStatement.setString(16, parmMapeoTrazabilidad.getPadre());
			    if (parmMapeoTrazabilidad.getPorcentaje()!=null)
			    {
				    preparedStatement.setDouble(17, parmMapeoTrazabilidad.getPorcentaje());
			    }
			    else
			    {
			    	preparedStatement.setNull(17, java.sql.Types.NULL);
			    }
			    preparedStatement.setString(18, parmMapeoTrazabilidad.getDeposito());
			    preparedStatement.setString(19, parmMapeoTrazabilidad.getImpreso());
	    	}
	        preparedStatement.executeUpdate();
	      
	    }
	    catch (Exception e)
	    {
	    	return (e.getMessage());	    	
	    }
	    return null;
	}
	
	public void guardarTraza(ArrayList<HashMap<String, String>> r_nodos)
    {
	  	PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;
		HashMap<String, String> nodosNiveles = null;
		
	    try
	    {
	    	
	    	for (int fila=0;fila < r_nodos.size();fila++)
	    	{
	    		/*
	    		 * grabo los datos en la tabla nueva
	    		 * desde donde se imprime traza + analitica
	    		 */
	    		nodosNiveles = r_nodos.get(fila);
	    		
	    		cadenaSQL = new StringBuffer();	    	
	    		cadenaSQL.append(" INSERT INTO lstTrazabilidad ( ");
	    		cadenaSQL.append(" lstTrazabilidad.nivel, ");
	    		cadenaSQL.append(" lstTrazabilidad.descripcion) VALUES (");
	    		cadenaSQL.append(" ?,?) ");
	    		con=this.conManager.establecerConexion();
	    		preparedStatement = con.prepareStatement(cadenaSQL.toString());
	    		
	    		preparedStatement.setString(1, nodosNiveles.get("id").toString());
	    		preparedStatement.setString(2, nodosNiveles.get("texto").toString());
	    		    		
	    		preparedStatement.executeUpdate();
	    	}
	    	
	    }
	    catch (Exception e)
	    {
	    	e.getMessage();	    	
	    }
	}

	public String inicializar()
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = null;        
		
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM trazabilidad ");
			con=this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();

			if (preparedStatement != null)
			{
				preparedStatement.close();
			}     
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM lstTrazabilidad ");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();

		}
		catch (Exception exception)
		{
			return ("Se produjo un error al Eliminar los registros. ");
		}
		return null; 
	}
	
	public Integer getCodigoInterno() 
	{
		return codigoInterno;
	}
	public void setCodigoInterno(Integer codigoInterno) 
	{
		this.codigoInterno = codigoInterno;
	}
	
}

