package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;


public class MantenimientoTrazabilidadBean 
{

	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	

	public MapeoTrazabilidad recuperarRegistros(String r_nivel) 
    {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        StringBuffer cadenaSQL = new StringBuffer();
        MapeoTrazabilidad mapeoTrazabilidad = null;
        try
        {
        	cadenaSQL.append(" SELECT trazabilidad.parte trz_par, ");
		    cadenaSQL.append(" trazabilidad.linea trz_lin, ");
		    cadenaSQL.append(" trazabilidad.fecha trz_fec , ");
		    cadenaSQL.append(" trazabilidad.seccion trz_sec, ");
		    cadenaSQL.append(" trazabilidad.tipo trz_tip, ");
		    cadenaSQL.append(" trazabilidad.localizacion trz_loc, ");
		    cadenaSQL.append(" trazabilidad.vino trz_vin, ");
		    cadenaSQL.append(" trazabilidad.litros trz_lit, ");
		    cadenaSQL.append(" trazabilidad.diasSal trz_ds, ");
		    cadenaSQL.append(" trazabilidad.cantidad trz_can, ");
		    cadenaSQL.append(" trazabilidad.partida trz_prt, ");
		    cadenaSQL.append(" trazabilidad.loteSalida trz_ls, ");
		    cadenaSQL.append(" trazabilidad.referencia trz_ref, ");
		    cadenaSQL.append(" trazabilidad.nivel trz_niv, ");
		    cadenaSQL.append(" trazabilidad.almacen trz_alm,");
		    cadenaSQL.append(" trazabilidad.padre trz_pad,");
		    cadenaSQL.append(" trazabilidad.porcentaje trz_por,");
		    cadenaSQL.append(" trazabilidad.deposito trz_dep ,");
		    cadenaSQL.append(" trazabilidad.impreso trz_imp ,");
		    cadenaSQL.append(" tra.parte tra_par, ");		    
		    cadenaSQL.append(" tra.linea tra_lin, ");
		    cadenaSQL.append(" tra.fecha tra_fec , ");
		    cadenaSQL.append(" tra.seccion tra_sec, ");
		    cadenaSQL.append(" tra.tipo tra_tip, ");
		    cadenaSQL.append(" tra.localizacion tra_loc, ");
		    cadenaSQL.append(" tra.vino tra_vin, ");
		    cadenaSQL.append(" tra.litros tra_lit, ");
		    cadenaSQL.append(" tra.diasSal tra_ds, ");
		    cadenaSQL.append(" tra.cantidad tra_can, ");
		    cadenaSQL.append(" tra.partida tra_prt, ");
		    cadenaSQL.append(" tra.loteSalida tra_ls, ");
		    cadenaSQL.append(" tra.referencia tra_ref, ");
		    cadenaSQL.append(" tra.nivel tra_niv, ");
		    cadenaSQL.append(" tra.padre tra_pad, ");
		    cadenaSQL.append(" tra.almacen tra_alm,");
		    cadenaSQL.append(" tra.porcentaje tra_por, ");
		    cadenaSQL.append(" tra.deposito tra_dep ");	    
		    cadenaSQL.append(" FROM trazabilidad ");
		    cadenaSQL.append(" left outer join trazabilidad tra on tra.nivel=trazabilidad.padre and tra.tipo not in ('T') ");
		    cadenaSQL.append(" WHERE trazabilidad.nivel = '" + r_nivel + "' ");
		    
		    con=this.conManager.establecerConexion();
            preparedStatement = con.prepareStatement(cadenaSQL.toString());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                mapeoTrazabilidad = new MapeoTrazabilidad();
                mapeoTrazabilidad.setParte(resultSet.getString("trz_par"));
                mapeoTrazabilidad.setLinea(resultSet.getString("trz_lin"));
                mapeoTrazabilidad.setFecha(resultSet.getTimestamp("trz_fec"));
                mapeoTrazabilidad.setSeccion(resultSet.getString("trz_sec"));
                mapeoTrazabilidad.setTipo(resultSet.getString("trz_tip"));
                mapeoTrazabilidad.setLocalizacion(resultSet.getString("trz_loc"));
                mapeoTrazabilidad.setVino(resultSet.getString("trz_vin"));
                mapeoTrazabilidad.setLitros(new Double(resultSet.getDouble("trz_lit")));
                mapeoTrazabilidad.setDiasSal(new Integer(resultSet.getInt("trz_ds")));
                mapeoTrazabilidad.setCantidad(new Double(resultSet.getDouble("trz_can")));
                mapeoTrazabilidad.setPartida(resultSet.getString("trz_prt"));
                mapeoTrazabilidad.setLoteSalida(resultSet.getString("trz_ls"));
                mapeoTrazabilidad.setReferencia(resultSet.getString("trz_ref"));
                mapeoTrazabilidad.setNivel(resultSet.getString("trz_niv"));
                mapeoTrazabilidad.setAlmacen(resultSet.getString("trz_alm"));
                mapeoTrazabilidad.setPadre(resultSet.getString("trz_pad"));
                mapeoTrazabilidad.setPorcentaje(new Double(resultSet.getDouble("trz_por")));
                mapeoTrazabilidad.setDeposito(resultSet.getString("trz_dep"));
                mapeoTrazabilidad.setImpreso(resultSet.getString("trz_imp"));

                mapeoTrazabilidad.setLoteSalida_tra(resultSet.getString("tra_ls"));
				mapeoTrazabilidad.setVino_tra(resultSet.getString("tra_vin"));
				mapeoTrazabilidad.setReferencia_tra(resultSet.getString("tra_ref"));
				mapeoTrazabilidad.setLocalizacion_tra(resultSet.getString("tra_loc"));
				mapeoTrazabilidad.setLitros_tra(new Double(resultSet.getDouble("tra_lit")));
				mapeoTrazabilidad.setDiasSal_tra(new Integer(resultSet.getInt("tra_ds")));
				mapeoTrazabilidad.setFecha_tra(resultSet.getTimestamp("tra_fec"));
				mapeoTrazabilidad.setParte_tra(resultSet.getString("tra_par"));
				mapeoTrazabilidad.setLinea_tra(resultSet.getString("tra_lin"));
				mapeoTrazabilidad.setPartida_tra(resultSet.getString("tra_prt"));
				mapeoTrazabilidad.setTipo_tra(resultSet.getString("tra_tip"));
				mapeoTrazabilidad.setNivel_tra(resultSet.getString("tra_niv"));
				mapeoTrazabilidad.setPadre_tra(resultSet.getString("tra_pad"));
				mapeoTrazabilidad.setAlmacen_tra(resultSet.getString("tra_alm"));
				mapeoTrazabilidad.setPorcentaje_tra(new Double(resultSet.getDouble("tra_por")));
				
                return mapeoTrazabilidad;
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        finally
        {
            try
            {
                if (resultSet != null)
                {
                    resultSet.close();
                }
                if (preparedStatement != null)
                {
                    preparedStatement.close();
                }
            }
            catch (SQLException sqlException)
            {
                sqlException.printStackTrace();
            }
        }
        return null;
    }	  
	
	public ResultSet recuperarRegistrosResultSet(String r_padre)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		try
		{
            cadenaSQL.append(" SELECT trazabilidad.parte trz_par, ");
		    cadenaSQL.append(" trazabilidad.linea trz_lin, ");
		    cadenaSQL.append(" trazabilidad.fecha trz_fec , ");
		    cadenaSQL.append(" trazabilidad.seccion trz_sec, ");
		    cadenaSQL.append(" trazabilidad.tipo trz_tip, ");
		    cadenaSQL.append(" trazabilidad.localizacion trz_loc, ");
		    cadenaSQL.append(" trazabilidad.vino trz_vin, ");
		    cadenaSQL.append(" trazabilidad.litros trz_lit, ");
		    cadenaSQL.append(" trazabilidad.diasSal trz_ds, ");
		    cadenaSQL.append(" trazabilidad.cantidad trz_can, ");
		    cadenaSQL.append(" trazabilidad.partida trz_prt, ");
		    cadenaSQL.append(" trazabilidad.loteSalida trz_ls, ");
		    cadenaSQL.append(" trazabilidad.referencia trz_ref, ");
		    cadenaSQL.append(" trazabilidad.nivel trz_niv, ");
		    cadenaSQL.append(" trazabilidad.almacen trz_alm,");
		    cadenaSQL.append(" trazabilidad.padre trz_pad,");
		    cadenaSQL.append(" trazabilidad.porcentaje trz_por,");
		    cadenaSQL.append(" trazabilidad.deposito trz_dep,");
		    cadenaSQL.append(" trazabilidad.impreso trz_imp,");
		    cadenaSQL.append(" tra.parte tra_par, ");		    
		    cadenaSQL.append(" tra.linea tra_lin, ");
		    cadenaSQL.append(" tra.fecha tra_fec , ");
		    cadenaSQL.append(" tra.seccion tra_sec, ");
		    cadenaSQL.append(" tra.tipo tra_tip, ");
		    cadenaSQL.append(" tra.localizacion tra_loc, ");
		    cadenaSQL.append(" tra.vino tra_vin, ");
		    cadenaSQL.append(" tra.litros tra_lit, ");
		    cadenaSQL.append(" tra.diasSal tra_ds, ");
		    cadenaSQL.append(" tra.cantidad tra_can, ");
		    cadenaSQL.append(" tra.partida tra_prt, ");
		    cadenaSQL.append(" tra.loteSalida tra_ls, ");
		    cadenaSQL.append(" tra.referencia tra_ref, ");
		    cadenaSQL.append(" tra.nivel tra_niv, ");
		    cadenaSQL.append(" tra.padre tra_pad, ");
		    cadenaSQL.append(" tra.almacen tra_alm,");
		    cadenaSQL.append(" tra.porcentaje tra_por, ");
		    cadenaSQL.append(" tra.deposito tra_dep ");
		    cadenaSQL.append(" FROM trazabilidad ");
		    cadenaSQL.append(" left outer join trazabilidad tra on tra.nivel=trazabilidad.padre and tra.tipo not in ('T') ");
		    
		    if (r_padre!=null)
		    {
		    	if (r_padre=="")
		    	{
		    		cadenaSQL.append(" WHERE (trazabilidad.padre is null or length(trazabilidad.padre)=3)");
		    	}
		    	else
		    	{
		    		cadenaSQL.append(" WHERE trazabilidad.padre = '" + r_padre + "' ");
		    	}
		    }
		    
            cadenaSQL.append(" ORDER BY trazabilidad.nivel asc" );

            con=this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			ResultSet rstRegistrosBuscados = preparedStatement.executeQuery(cadenaSQL.toString());
			return rstRegistrosBuscados;
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Trazabilidad Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		return null;
	}

	/****************************METODOS DE ALTERACION DATOS***********************/
	public Integer obtenerSiguienteNumeroAnalitica() throws Exception
	{
		serverBasico sv = null;
		Integer codigoTrazabilidad = null;
		
		con=this.conManager.establecerConexion();
		codigoTrazabilidad = sv.obtenerSiguienteCodigoInterno(con, "Trazabilidad", "codigo");
		return codigoTrazabilidad;
	}

	public String guardar(MapeoTrazabilidad parmMapeoTrazabilidad) throws Exception
	{
	    /*
	     * Metodo que se llama desde la parte cliente cuando estamos modificando datos de un
	     * registro ya existente.
	     * Recibe el mapeo completo incluyendo los cambios de la parte cliente.
	     * Hace un referencia al entity y con el findby lo crea y le pasamos los cambios en los datos
	     */
		String rdo=null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
	      trazabilidadBean = new TrazabilidadBean(parmMapeoTrazabilidad);
    	  
    	  if (trazabilidadBean.getCodigoInterno() != null)
	      {
            	rdo=trazabilidadBean.guardarCambios(parmMapeoTrazabilidad, false);	 
	      }        
	      else
	      {
	    	  rdo="Alguien a borrado el registro de la base de datos";
	      }
        }	    
	    catch(Exception exception)
	    {
	        exception.printStackTrace();
	        rdo=exception.getMessage();
	    }
	    return rdo;
	}
	
	public String guardarNuevo(MapeoTrazabilidad parmMapeoTrazabilidad) throws Exception
	{
		String resultadoAccionGuardarNuevo = null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
		      trazabilidadBean = new TrazabilidadBean(parmMapeoTrazabilidad);
		      
	          if (trazabilidadBean.getCodigoInterno()!=null)
	          {
	        	  resultadoAccionGuardarNuevo="El registro ya existe en la base de datos";            
	          }
	          else
	          {
          			resultadoAccionGuardarNuevo=trazabilidadBean.guardarCambios(parmMapeoTrazabilidad,true);
	          }
        }
	    catch(Exception exception)
	    {
	    	this.serNotif.mensajeError("Mantenimiento Trazabilidad Bean - guardarNuevo"+exception.getMessage());
	    }	    
	    return resultadoAccionGuardarNuevo;
	}	
	
//	private Integer obtenerSiguienteValorCodigo() throws Exception
//	{
//		Integer codigoInterno = null;
//		codigoInterno = this.obtenerSiguienteCodigoInterno("Mantenimiento Trazabilidad Bean", this.conection, "Trazabilidad", "codigo");
//		return codigoInterno;
//	}
	
	public String eliminar() 
	{
		String resultadoAccionEliminar = null;
	    TrazabilidadBean trazabilidadBean = null;
	    try
	    {
	    	trazabilidadBean = new TrazabilidadBean();
    		resultadoAccionEliminar=trazabilidadBean.inicializar();
	    }
	    catch(Exception ex)	    
	    {
			this.serNotif.mensajeError("Mantenimiento Trazabilidad Bean - eliminar"+ex.getMessage());
		}
	    return resultadoAccionEliminar;
	}
}