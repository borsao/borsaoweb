package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.vaadin.treegrid.container.Measurable;

import com.vaadin.data.Collapsible;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;


public class consultaTrazabilidadContainer extends HierarchicalContainer implements Collapsible, Measurable 
{
	
    static HashMap<String, String> opcionesSeleccionadas = null;    
    consultaTrazabilidadDataSource ctds = null;
    
    public consultaTrazabilidadContainer(HashMap<String, String> r_hashOpciones, boolean r_eliminar) 
    {
    	
    	if (r_eliminar)
    	{
    		removeChildrenRecursively(ctds.getRoot());
    		ctds=null;
    	}
    	
    	this.opcionesSeleccionadas= r_hashOpciones;    	
    	ctds = new consultaTrazabilidadDataSource(this.opcionesSeleccionadas, r_eliminar);
    	
//    	try 
//    	{
//			if (ctds.rs.last() && ctds.rs.getRow()==1)
			{
				addContainerProperty("Descripcion",String.class,"");
				
				for (Object[] r : ctds.getRoot()) {
					addItem(r);
				}
			}
//		} 
//    	catch (SQLException e) 
//    	{
//			e.printStackTrace();
//		}
    	/*
    	 * Cargo titulos columnas GRID
    	 */
    }

    private Object addItem(Object[] values) {
        Item item = addItem((Object) values);
        setProperties(item, values);
        return values;
    }

    private Object addChild(Object[] values, Object parentId) {
        Item item = addItemAfter(parentId, values);
        setProperties(item, values);
        setParent(values, parentId);
        return values;
    }

    private void setProperties(Item item, Object[] values) {
    	
    	int columnas = 0;
    	item.getItemProperty("Descripcion").setValue(values[columnas]);
    }

    @Override
    public void setCollapsed(Object itemId, boolean collapsed) {
        expandedNodes.put(itemId, !collapsed);

        if (collapsed) {
            // remove children
            removeChildrenRecursively(itemId);
        } else {
            // lazy load children
            addChildren(itemId);
        }
    }

    private void addChildren(Object itemId) {
    	boolean soloAnaliticas=true;
    	List<Object[]> hijos = ctds.getChildren(itemId);
    	if (hijos!=null)
    	{
    		for (int i=0;i<hijos.size();i++)
    		{
    			if (!((MapeoTrazabilidad)((Object[]) ((List<Object[]>) hijos).get(i))[1]).getTipo().equals("A")) soloAnaliticas=false;
    		}
    		
    		if (soloAnaliticas)
    		{
    			this.ctds.recuperarHijos(itemId, null, false);
    		}
	        for (Object[] child : ctds.getChildren(itemId)) {
	            Object childId = addChild(child, itemId);
	            if (Boolean.TRUE.equals(expandedNodes.get(childId))) {
	                addChildren(childId);
	            }
	        }
    	}
    	else
    	{
    		/*
    		 *	crear hijos y luego expandirlos 
    		 */
    		this.ctds.recuperarHijos(itemId, null, false);
    		/*
    		 * expando el arbol calculado
    		 */
    		hijos = ctds.getChildren(itemId);
    		if (hijos!=null)
    		{
	    		for (Object[] child : ctds.getChildren(itemId)) 
	    		{
	    			if (child!=null && itemId!=null)
	    			{
			            Object childId = addChild(child, itemId);
			            if (Boolean.TRUE.equals(expandedNodes.get(childId))) 
			            {
			                addChildren(childId);
			            }
	    			}
	    		}
    		}
    		
    	}
    		
    }

    private boolean removeChildrenRecursively(Object itemId) {
        boolean success = true;
        Collection<?> children2 = getChildren(itemId);
        if (children2 != null) {
            Object[] array = children2.toArray();
            for (int i = 0; i < array.length; i++) {
                boolean removeItemRecursively = removeItemRecursively(
                        this, array[i]);
                if (!removeItemRecursively) {
                    success = false;
                }
            }
        }
        return success;
    }

    @Override
    public boolean hasChildren(Object itemId) {
        return !ctds.isLeaf(itemId);
    }

    private Map<Object, Boolean> expandedNodes = new HashMap<>();

    @Override
    public boolean isCollapsed(Object itemId) {
        return !Boolean.TRUE.equals(expandedNodes.get(itemId));
    }

    @Override
    public int getDepth(Object itemId) {
        int depth = 0;
        while (!isRoot(itemId)) {
            depth ++;
            itemId = getParent(itemId);
        }
        return depth;
    }
    
    public static String getColumnaPrincipal()
    {
    	return "Descripcion";   	
    }
}