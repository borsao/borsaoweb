package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo;

import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.treegrid.TreeGrid;

import com.vaadin.shared.ui.grid.ColumnResizeMode;
import com.vaadin.ui.renderers.HtmlRenderer;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class TrazabilidadGrid extends TreeGrid {
	
  	public HashMap<String , String> opcionesEscogidas = null;
  	public consultaTrazabilidadContainer container = null;
  	
    public TrazabilidadGrid(HashMap<String , String> r_opcionesEscogidas, boolean r_hayGrid) {
        setSizeFull();
        setSelectionMode(SelectionMode.SINGLE);

		/*
		 * codigo para cargar el jtree-grid
		 */
        
		setCaption("<h2><b>Trazabilidad Descendente del Lote de Salida " + r_opcionesEscogidas.get("Lote") + "</b></h2>");
		setCaptionAsHtml(true);
		addStyleName("smallgrid");
		this.generarGrid(r_opcionesEscogidas,r_hayGrid);
        setEditorEnabled(false);

        if (getColumn(consultaTrazabilidadContainer.getColumnaPrincipal())!=null)
        {
        	getColumn(consultaTrazabilidadContainer.getColumnaPrincipal()).setWidthUndefined();
		    getColumn(consultaTrazabilidadContainer.getColumnaPrincipal()).setEditable(false);
		    getColumn(consultaTrazabilidadContainer.getColumnaPrincipal()).setHidden(false);        
		    setHierarchyColumn(consultaTrazabilidadContainer.getColumnaPrincipal());
		    setColumnOrder(consultaTrazabilidadContainer.getColumnaPrincipal());
		    setColumnResizeMode(ColumnResizeMode.ANIMATED);
		    setColumnReorderingAllowed(false);
        }
    }

    public void generarGrid(HashMap<String , String> r_opcionesEscogidas, boolean r_hayGrid )
    {

    	container = new consultaTrazabilidadContainer(r_opcionesEscogidas,r_hayGrid);
        setContainerDataSource(container);
        Iterator<Column> it = getColumns().iterator();

        while (it.hasNext()) 
        {
        	Column col = (Column)it.next();
        	if (col.getPropertyId().toString().contains("Descripcion"))
			{
				col.setRenderer(new HtmlRenderer());
			}
        }

    }
}
