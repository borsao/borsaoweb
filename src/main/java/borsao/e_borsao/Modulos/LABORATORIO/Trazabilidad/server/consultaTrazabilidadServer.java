package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.Movimientos.server.MantenimientoMovimientosBean;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;

public class consultaTrazabilidadServer
{
	private connectionManager conManager = null;
	private Connection con = null;	
	private static consultaTrazabilidadServer instance;
	private MantenimientoMovimientosBean movimientos = null;
	
	public consultaTrazabilidadServer()
	{
		this.conManager = connectionManager.getInstance();
	}
	
	public static consultaTrazabilidadServer getInstance() 
	{
		if (instance == null) 
		{
			instance = new consultaTrazabilidadServer();			
		}
		return instance;
	}
	

	public ResultSet datosLote(String r_lote)
	{
		ResultSet rs = null;
		this.movimientos=new MantenimientoMovimientosBean();
		rs=this.movimientos.recuperarRegistrosResultSet(r_lote);
		return rs;
	}

	public ResultSet datosLotePartida(MapeoTrazabilidad r_mapeo)
	{
		ResultSet rs = null;
		this.movimientos=new MantenimientoMovimientosBean();
		rs=this.movimientos.recuperarRegistrosResultSetPartida(r_mapeo);
		return rs;
	}
	
	public ResultSet datosLoteParte(MapeoTrazabilidad r_mapeo)
	{
		ResultSet rs = null;
		this.movimientos=new MantenimientoMovimientosBean();
		rs=this.movimientos.recuperarRegistrosResultSetParte(r_mapeo);
		return rs;
	}
	
	public Double sumaLitrosLoteParte(String r_parte)
	{
		Double rs = null;
		this.movimientos=new MantenimientoMovimientosBean();
		rs=this.movimientos.recuperarSumaLitrosParte(r_parte);
		return rs;
	}
	
    public ResultSet recuperaAnaliticas(String r_deposito, String r_desdeFecha, String r_hastaFecha) throws Exception
	{
	   /**
	    * Metodo que me devuelve todas las areas que estan utilizadas por
	    * la definicion de los programas.
	    */
	   
	   PreparedStatement preparedStatement = null;
	   ResultSet rstRegistrosBuscados = null;
	   StringBuffer cadenaSQL = new StringBuffer();
	   
	   try
	   {		   
		   cadenaSQL.append(" SELECT analiticas.codigo anal_id, ");
		   cadenaSQL.append(" analiticas.fecha anal_fec, ");
		   cadenaSQL.append(" analiticas.vino anal_vin, ");
		   cadenaSQL.append(" analiticas.sulfuroso anal_sul, ");
		   cadenaSQL.append(" analiticas.tecSulfuroso anal_tec_sul, ");
		   cadenaSQL.append(" analiticas.sulfurosoT anal_sult, ");
		   cadenaSQL.append(" analiticas.tecSulfurosoT anal_tec_sult, ");
		   cadenaSQL.append(" analiticas.azucar anal_azu, ");
		   cadenaSQL.append(" analiticas.tecAzucar anal_tec_azu, ");
		   cadenaSQL.append(" analiticas.grado anal_gra, ");
		   cadenaSQL.append(" analiticas.tecGrado anal_tec_gra, ");
		   cadenaSQL.append(" analiticas.glufru anal_glu, ");
		   cadenaSQL.append(" analiticas.tecGlufru anal_tec_glu, ");
		   cadenaSQL.append(" analiticas.acidezT anal_aci, ");
		   cadenaSQL.append(" analiticas.tecAcidezT anal_tec_aci, ");
		   cadenaSQL.append(" analiticas.ph anal_ph, ");
		   cadenaSQL.append(" analiticas.tecPh anal_tec_ph, ");
		   cadenaSQL.append(" analiticas.malico anal_mal, ");
		   cadenaSQL.append(" analiticas.tecMalico anal_tec_mal, ");
		   cadenaSQL.append(" analiticas.color anal_col, ");
		   cadenaSQL.append(" analiticas.tecColor anal_tec_col, ");
		   cadenaSQL.append(" analiticas.polif anal_pol, ");
		   cadenaSQL.append(" analiticas.tecPolif anal_tec_pol, ");
		   cadenaSQL.append(" analiticas.responsable anal_res, ");
		   cadenaSQL.append(" analiticas.observaciones anal_obs, ");
		   cadenaSQL.append(" analiticas.litros anal_lit, ");
		   cadenaSQL.append(" analiticas.lnkDeposito anal_dep,");
		   cadenaSQL.append(" analiticas.tipoAnalitica anal_tp ");
		   cadenaSQL.append(" FROM analiticas ");
		   
		   RutinasFechas rutFecha = new RutinasFechas();

		   String desdeFecha = rutFecha.convertirAFechaMysql(r_desdeFecha);
		   String hastaFecha = rutFecha.convertirAFechaMysql(r_hastaFecha);
		   
		   cadenaSQL.append(" WHERE fecha between '" + desdeFecha + "' and '" + hastaFecha + "'");
		   cadenaSQL.append(" AND lnkDeposito = '" + r_deposito + "' ");
		    
		   con=this.conManager.establecerConexion();
		   preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
		   rstRegistrosBuscados = preparedStatement.executeQuery();
		   return rstRegistrosBuscados;
	   }	
	   catch (Exception exception)
	   {
		   exception.printStackTrace();
	   }
			
	  
	   return null;
	}
	
	public ArrayList<MapeoAyudas> buscarLotes(String r_lote)
	{
		ArrayList<MapeoAyudas> mapeoAyudas = null;
		MantenimientoMovimientosBean mantenimientoMovimientos = MantenimientoMovimientosBean.getInstance();
		mapeoAyudas = mantenimientoMovimientos.recuperarLotes(r_lote);
		return mapeoAyudas;
	}
	
	public boolean comprobarLote(String r_lote)
	{
		MapeoAyudas mapeoAyudas = null;
		boolean encontrado = false;
		MantenimientoMovimientosBean mantenimientoMovimientos = MantenimientoMovimientosBean.getInstance();
		mapeoAyudas = mantenimientoMovimientos.comprobarLote(r_lote);
		if (mapeoAyudas!=null) encontrado=true;
		return encontrado;
	}
}