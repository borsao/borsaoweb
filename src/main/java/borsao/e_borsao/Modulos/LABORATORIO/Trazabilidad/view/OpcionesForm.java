package borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.server.consultaTrazabilidadServer;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign  {
	
	 
	 private TrazabilidadView vw = null;
	 private consultaTrazabilidadServer cts  = null;
	 private ventanaAyuda vHelp  =null;
	 
	 public OpcionesForm(TrazabilidadView r_vw) {
        super();
        vw=r_vw;
        addStyleName("mytheme product-form");
        this.cargarCombos(this.verAnalitica);
        this.verAnalitica.setValue("No");
        this.cargarCombos(this.verCompras);
        this.verCompras.setValue("No");
        this.cargarValidators();
		this.cargarListeners();
		this.activarDesactivarControles(false);
    }   

	private void cargarValidators()
	{

		lotes.setValidationVisible(false);
		class MyValidator implements Validator {
            private static final long serialVersionUID = -8281962473854901819L;

            @Override
            public void validate(Object value)
                    throws InvalidValueException {
            	cts  = consultaTrazabilidadServer.getInstance();
            	if (lotes.getValue()!=null && lotes.getValue().length()>0)
            	{
	            	if (!cts.comprobarLote(lotes.getValue()))
					{
	            		lotes.focus();
	            		ventanaMargenes();
	            		btnBuscar.setEnabled(false);
	                    throw new InvalidValueException("Valor incorrecto");
					}
	            	else
	            	{
	            		btnBuscar.setEnabled(true);
	            	}
            	}
            }
        }
		lotes.addValidator(new MyValidator());
		
		lotes.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				lotes.setValidationVisible(true);
			}
		});
	}
    private void cargarListeners()
	{
        btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
             setEnabled(false);
            }
        });

        btnBuscar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	if (vw.isHayGrid())
	 			{			
	 				vw.grid.removeAllColumns();			
	 				vw.barAndGridLayout.removeComponent(vw.grid);
	 				vw.grid=null;			
	 			}
            	removeStyleName("visible");
            	setEnabled(false);
            	generarHashOpciones();
            	vw.generarGrid(vw.opcionesEscogidas);
            }
        });
        
		btnAyuda.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
	}
    
    private void generarHashOpciones()
	{
		vw.opcionesEscogidas = new HashMap<String, String>();
		/*
		 * Guardo las opciones escogidas por el usuario 
		 * 
		 */
		vw.opcionesEscogidas.put("Usuario", eBorsao.get().accessControl.getNombre());
		vw.opcionesEscogidas.put("Lote", this.lotes.getValue().toString());
		vw.opcionesEscogidas.put("verAnalitica", this.verAnalitica.getValue().toString());
		vw.opcionesEscogidas.put("verCompras", this.verCompras.getValue().toString());
		
	}
	
	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorLotes = null;
		cts  = consultaTrazabilidadServer.getInstance();
		vectorLotes =cts.buscarLotes(this.lotes.getValue());
		this.vHelp = new ventanaAyuda(this.lotes, vectorLotes, "Lotes");
		getUI().addWindow(this.vHelp);	
	}

	private void cargarCombos(ComboBox r_combo)
	{
		r_combo.addItem("Si");
		r_combo.addItem("No");
	}
	private void activarDesactivarControles(boolean r_activoono)
	{
//		btnCancel.setEnabled(r_activoono);
		btnBuscar.setEnabled(r_activoono);
	}
}
