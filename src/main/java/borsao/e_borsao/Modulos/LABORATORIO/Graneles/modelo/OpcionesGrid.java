package borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMCompras;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.view.pantallaMezclas;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.view.granelesView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private GridViewRefresh app = null;
	
    public OpcionesGrid(granelesView r_app, ArrayList<MapeoGraneles> r_vector) 
    {
    	this.app = r_app;
        
        this.vector=r_vector;
        this.setSizeFull();
        
		this.asignarTitulo("Datos Graneles");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoGraneles.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("btnMezclas", "btnFichaTecnica", "btnBotellas", "btnEtiqueta", "articulo","descripcion", "anada", "cantidad", "resto","calcular_sn","calcula");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "500");
    	this.widthFiltros.put("anada", "60");
    	this.widthFiltros.put("calcula", "60");
    
    	this.getColumn("btnMezclas").setHeaderCaption("");
    	this.getColumn("btnMezclas").setSortable(false);
    	this.getColumn("btnMezclas").setWidth(new Double(50));

    	this.getColumn("btnBotellas").setHeaderCaption("");
    	this.getColumn("btnBotellas").setSortable(false);
    	this.getColumn("btnBotellas").setWidth(new Double(50));
    	
    	this.getColumn("btnEtiqueta").setHeaderCaption("");
    	this.getColumn("btnEtiqueta").setSortable(false);
    	this.getColumn("btnEtiqueta").setWidth(new Double(50));
    	
    	this.getColumn("btnFichaTecnica").setHeaderCaption("");
    	this.getColumn("btnFichaTecnica").setSortable(false);
    	this.getColumn("btnFichaTecnica").setWidth(new Double(50));

    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("anada").setHeaderCaption("Añada");
    	this.getColumn("anada").setWidth(new Double(90));
    	this.getColumn("calcula").setHeaderCaption("Calcular?");
    	this.getColumn("calcula").setWidth(new Double(90));
    	this.getColumn("cantidad").setHeaderCaption("Litros/Botellas");
    	this.getColumn("cantidad").setWidth(new Double(150));
    	this.getColumn("resto").setHeaderCaption("Resto Litros/Botellas");
    	this.getColumn("resto").setWidth(new Double(150));
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("calcular_sn").setHidden(true);
    	this.getColumn("btnBotellas").setHidden(true);
    	this.getColumn("btnEtiqueta").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoGraneles) {
                MapeoGraneles progRow = (MapeoGraneles)bean;
                // The actual description text is depending on the application
                if ("btnBotellas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Prevision Embotellados";
                }
                else if ("btnEtiqueta".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Datos en Etiqueta";
                }
                else if ("btnFichaTecnica".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ficha Técnica Vino";
                }
                else if ("btnMezclas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso Mezclas Granel";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ( "btnMezclas".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "btnFichaTecnica".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "btnBotellas".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "btnEtiqueta".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "cantidad".equals(cellReference.getPropertyId()) || "resto".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	return "cell-normal";
            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
		this.camposNoFiltrar.add("btnMezclas");
		this.camposNoFiltrar.add("btnBotellas");
		this.camposNoFiltrar.add("btnEtiqueta");
		this.camposNoFiltrar.add("btnFichaTecnica");
		this.camposNoFiltrar.add("cantidad");
		this.camposNoFiltrar.add("resto");
		this.camposNoFiltrar.add("calcular_sn");
	}

	@Override
	public void cargarListeners() 
	{		
		this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoGraneles mapeo = (MapeoGraneles) event.getItemId();
            		String articulo = null;
            		articulo = mapeo.getArticulo().trim();
            		
            		if (event.getPropertyId().toString().equals("btnFichaTecnica"))
            		{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	            			Notificaciones.getInstance().mensajeInformativo("Pulsado acceso Ficha Tecnica");
//	            			pantallaLineasProduccion vt = new pantallaLineasProduccion(null,"Embotelladora", null, articulo, true, mapeo.getAnada());
//	            			getUI().addWindow(vt);	            			
	            		}            			
            		}
            		else if (event.getPropertyId().toString().equals("btnBotellas"))
            		{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	            			Notificaciones.getInstance().mensajeInformativo("Pulsado acceso Botellas");
	            				
//	            			pantallaLineasProduccion vt = new pantallaLineasProduccion(null,"Embotelladora", null, articulo, true, mapeo.getAnada());
//	            			getUI().addWindow(vt);	            			
	            		}
            		}
            		else if (event.getPropertyId().toString().equals("btnEtiqueta"))
            		{
            			activadaVentanaPeticion=true;
            			ordenando = false;
            			if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
            			{
            				Notificaciones.getInstance().mensajeInformativo("Pulsado acceso Datos Etiqueta");
            				
//	            			pantallaLineasProduccion vt = new pantallaLineasProduccion(null,"Embotelladora", null, articulo, true, mapeo.getAnada());
//	            			getUI().addWindow(vt);	            			
            			}
            		}
            		else if (event.getPropertyId().toString().equals("btnMezclas"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	            			pantallaMezclas vt = new pantallaMezclas((granelesView) app,"Datos Mezclas", mapeo);
	            			getUI().addWindow(vt);	            			
	            		}	            		
	            	}
	            	else if (event.getPropertyId().toString().equals("articulo"))
	            	{
	            		/*
	            		 * acceso a pendientes de servir
	            		 */
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo!=null && mapeo.getArticulo()!=null && (mapeo.getArticulo().trim().startsWith("0102") || mapeo.getArticulo().trim().startsWith("0103") || mapeo.getArticulo().trim().startsWith("0111")))
	            		{
	            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo", mapeo.getArticulo());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            		else
	            		{
	            			pantallaCRMCompras vt = new pantallaCRMCompras("CRM Compras", mapeo.getArticulo());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            	}
	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
	            }
    		}
        });
	}

	@Override
	public void calcularTotal() 
	{
		if (this.footer==null) this.footer=this.appendFooterRow();
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setWidth("100%");
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		}

	}

}
