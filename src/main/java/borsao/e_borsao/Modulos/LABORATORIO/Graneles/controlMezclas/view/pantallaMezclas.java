package borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.view;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.modelo.MapeoControlMezclas;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.server.consultaMezclasServer;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo.MapeoGraneles;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.view.granelesView;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaMezclas extends Ventana
{
	private MapeoGraneles mapeoGraneles = null;
	private MapeoControlMezclas mapeoControlMezclas = null;
	private consultaMezclasServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;
	
	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private Label lblGranel = null;
	private Label lblNombreGranel = null;
	private Label lblAñada = null;
	private TextField txtMezclas= null;
	private TextField txtCantidad = null;
	private EntradaDatosFecha txtFecha = null;
	private TextArea txtObservaciones= null;
	
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	
	private granelesView app=null;
	private Ventana appPantalla=null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaMezclas(granelesView r_app, String r_titulo, MapeoGraneles r_mapeo)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.appPantalla=this;
		this.mapeoGraneles = r_mapeo;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaMezclasServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.lblGranel=new Label("Granel");
        		this.lblGranel.setValue(this.mapeoGraneles.getArticulo());
//        		this.lblGranel.addStyleName("lblTitulo");

        		this.lblNombreGranel=new Label("Nombre");
        		this.lblNombreGranel.setValue(this.mapeoGraneles.getDescripcion());
        		this.lblNombreGranel.addStyleName("lblTitulo");
        		
        		this.lblAñada=new Label("Añada");
        		this.lblAñada.setValue(this.mapeoGraneles.getAnada());
        		this.lblAñada.addStyleName("lblTituloCalidad");

				linea1.addComponent(this.lblGranel);
				linea1.addComponent(this.lblNombreGranel);
				linea1.addComponent(this.lblAñada);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtFecha = new EntradaDatosFecha();
    			this.txtFecha.setCaption("Fecha");
    			this.txtFecha.setEnabled(true);
    			this.txtFecha.setValue(RutinasFechas.fechaActual());
    			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.txtFecha.setWidth("150px");
    			
    			this.txtMezclas = new TextField();
    			this.txtMezclas.setCaption("Identificador Mezcla");
    			this.txtMezclas.setWidth("320px");
				this.txtMezclas.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.txtCantidad = new TextField();
				this.txtCantidad.setCaption("Litros");
				this.txtCantidad.setWidth("120px");
				this.txtCantidad.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtCantidad.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
    			
				linea2.addComponent(this.txtFecha);
    			linea2.addComponent(this.txtMezclas);
    			linea2.addComponent(this.txtCantidad);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

    			
    			linea3.addComponent(this.txtObservaciones);
    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
//		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
//		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(appPantalla, "Quieres eliminar la mezcla?", "Si", "No", "Salir", null);
				getUI().addWindow(vt);
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		MapeoControlMezclas mapeo = new MapeoControlMezclas();
		mapeo.setIdCodigo(this.mapeoControlMezclas.getIdCodigo());
		
		cncs.eliminar(mapeo);
		
		if (gridDatos!=null)
		{
			gridDatos.removeAllColumns();
			gridDatos=null;
			frameGrid.removeComponent(panelGrid);
			panelGrid=null;
		}
		llenarRegistros();
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlMezclas mapeo = new MapeoControlMezclas();
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setCantidad(new Integer(this.txtCantidad.getValue()));
			mapeo.setIdentificadorMezcla(this.txtMezclas.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdGranel(this.mapeoGraneles.getIdCodigo());

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoMezclas(mapeo);
				if (rdo==null)
				{
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlMezclas.getIdCodigo());
				rdo = cncs.guardarCambiosMezclas(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlMezclas> vector=null;

    	MapeoControlMezclas mapeoControlMezclas = new MapeoControlMezclas();
    	mapeoControlMezclas.setIdGranel(this.mapeoGraneles.getIdCodigo());
    	vector = this.cncs.datosOpcionesGlobal(this.mapeoGraneles.getIdCodigo());

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlMezclas r_mapeo)
	{
		this.mapeoControlMezclas = new MapeoControlMezclas();
		this.mapeoControlMezclas.setIdCodigo(r_mapeo.getIdCodigo());
		setCreacion(false);
		
		this.txtMezclas.setValue(r_mapeo.getIdentificadorMezcla());
		this.txtCantidad.setValue(r_mapeo.getCantidad().toString());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		this.txtFecha.setValue(r_mapeo.getFecha());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtCantidad.setEnabled(r_activar);
    	txtMezclas.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtCantidad.setValue("");
		this.txtMezclas.setValue("");
		this.txtObservaciones.setValue("");
	}
	
    private boolean todoEnOrden()
    {
    	if ((this.txtMezclas.getValue()==null || this.txtMezclas.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el grifo revisado.");
    		return false;
    	}
    	if ((this.txtCantidad.getValue()==null || this.txtCantidad.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la cantidad de litros.");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		eliminar();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlMezclas) r_fila)!=null)
    	{
    		MapeoControlMezclas mapeoMezclas = (MapeoControlMezclas) r_fila;
    		if (mapeoMezclas!=null) this.llenarEntradasDatos(mapeoMezclas);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlMezclas> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Desglose Mezclas ");
//    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("70%");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlMezclas> container = new BeanItemContainer<MapeoControlMezclas>(MapeoControlMezclas.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("identificadorMezcla").setHeaderCaption("Mezcla");
			    	getColumn("identificadorMezcla").setWidth(new Double(250));
			    	getColumn("cantidad").setHeaderCaption("Litros");
			    	getColumn("cantidad").setWidth(new Double(100));

			    	this.getColumn("btnEmbotellados").setHeaderCaption("");
			    	this.getColumn("btnEmbotellados").setSortable(false);
			    	this.getColumn("btnEmbotellados").setWidth(new Double(50));

					getColumn("idCodigo").setHidden(true);
//					getColumn("fecha").setHidden(true);
					getColumn("orden").setHidden(true);
					getColumn("idGranel").setHidden(true);
//					getColumn("btnEmbotellados").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					setColumnOrder("btnEmbotellados", "fecha", "identificadorMezcla", "cantidad", "observaciones");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
			    	if (this.footer==null) this.footer=this.appendFooterRow();
			    	
				}
				
			    private void asignarTooltips()
			    {
			    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
			    }
			    
			    private String getCellDescription(CellReference cell) {
			        String descriptionText = null;
			        Item item = cell.getItem();
			        if (item instanceof BeanItem<?>) {
			            Object bean = ((BeanItem<?>)item).getBean();
			            if (bean instanceof MapeoControlMezclas) {
			                MapeoControlMezclas progRow = (MapeoControlMezclas)bean;
			                // The actual description text is depending on the application
			                if ("btnEmbotellados".equals(cell.getPropertyId()))
			                {
			                	descriptionText = "Acceso a embotellados mezcla";
			                }
			            }
			        }
			        return descriptionText;
			    }				
				@Override
				public void asignarEstilos() {
			    	asignarTooltips();
			    	
			    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
			    	{
			            public String getStyle(Grid.CellReference cellReference) 
			            {
			            	if ( "btnEmbotellados".equals(cellReference.getPropertyId()))
			            	{
			            		return "cell-nativebuttonProgramacion";
			            	}
			            	else if ( "cantidad".equals(cellReference.getPropertyId()))
			            	{
			            		return "Rcell-normal";
			            	}
			            	else
			        		{
			            		return "cell-normal";
			            	}
			            }
			        });
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
//			this.gridDatos.setWidth("100%");
//			this.gridDatos.setHeight("85%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.asignarEstilos();
			this.gridDatos.calcularTotal();
			this.gridDatos.cargarListeners();

	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}