package borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGraneles extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String anada;
	private Integer cantidad;
	private Integer resto;
	private boolean calcular_sn;
	private String calcula;
	private String btnFichaTecnica = "";
	private String btnMezclas = "";
	private String btnBotellas = "";
	private String btnEtiqueta = "";

	public MapeoGraneles()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setAnada("");
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getAnada() {
		return anada;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public Integer getResto() {
		return resto;
	}

	public boolean isCalcular_sn() {
		return calcular_sn;
	}

	public String getBtnFichaTecnica() {
		return btnFichaTecnica;
	}

	public String getBtnMezclas() {
		return btnMezclas;
	}

	public String getBtnBotellas() {
		return btnBotellas;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public void setResto(Integer resto) {
		this.resto = resto;
	}

	public void setCalcular_sn(boolean calcular_sn) {
		this.calcular_sn = calcular_sn;
	}

	public void setBtnFichaTecnica(String btnFichaTecnica) {
		this.btnFichaTecnica = btnFichaTecnica;
	}

	public void setBtnMezclas(String btnMezclas) {
		this.btnMezclas = btnMezclas;
	}

	public void setBtnBotellas(String btnBotellas) {
		this.btnBotellas = btnBotellas;
	}

	public String getCalcula() {
		if (isCalcular_sn()) return "S"; else return "N";
	}

	public String getBtnEtiqueta() {
		return btnEtiqueta;
	}

	public void setBtnEtiqueta(String btnEtiqueta) {
		this.btnEtiqueta = btnEtiqueta;
	}

}