package borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlMezclas extends MapeoGlobal
{
	private Integer idGranel;
	private String identificadorMezcla;
	private Integer cantidad;
	private Date fecha;
	private String observaciones;
	private Integer orden;
	private String btnEmbotellados="";
	
	public MapeoControlMezclas()
	{
	}	

	public Integer getIdGranel() {
		return idGranel;
	}


	public String getIdentificadorMezcla() {
		return identificadorMezcla;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public void setIdGranel(Integer idGranel) {
		this.idGranel = idGranel;
	}


	public void setIdentificadorMezcla(String identificadorMezcla) {
		this.identificadorMezcla = identificadorMezcla;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getBtnEmbotellados() {
		return btnEmbotellados;
	}

	public void setBtnEmbotellados(String btnEmbotellados) {
		this.btnEmbotellados = btnEmbotellados;
	}

}