package borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.controlMezclas.modelo.MapeoControlMezclas;

public class consultaMezclasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaMezclasServer  instance;
	
	public consultaMezclasServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaMezclasServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaMezclasServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlMezclas datosOpcionesGlobal(MapeoControlMezclas r_mapeo)
	{
		ResultSet rsOpcion = null;		
		MapeoControlMezclas mapeo = null;
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		String tabla = "lab_mezclas_granel";
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT lab_mezclas_granel.idCodigo mez_id, ");
			cadenaSQL.append(" lab_mezclas_granel.idGranel mez_gra, ");
			cadenaSQL.append(" lab_mezclas_granel.identificadorMezcla mez_nom, ");
			cadenaSQL.append(" lab_mezclas_granel.cantidad mez_can, ");
			cadenaSQL.append(" lab_mezclas_granel.orden mez_ord, ");
			cadenaSQL.append(" lab_mezclas_granel.fecha mez_fec, ");
			cadenaSQL.append(" lab_mezclas_granel.observaciones mez_obs ");
			cadenaSQL.append(" FROM " + tabla + " lab_mezclas_granel ");
			
			try
			{
				if (r_mapeo!=null)
				{
					if (r_mapeo.getIdGranel()!=null && r_mapeo.getIdGranel()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" lab_mezclas_granel.idGranel = " + r_mapeo.getIdGranel());
					}
					if (r_mapeo.getFecha()!=null)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" lab_mezclas_granel.fecha = " + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0)
				{
					cadenaSQL.append(" where ");
					cadenaSQL.append(cadenaWhere.toString() );
				}
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlMezclas();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("mez_id"));
					mapeo.setIdGranel(rsOpcion.getInt("mez_gra"));
					mapeo.setIdentificadorMezcla(rsOpcion.getString("mez_nom"));
					mapeo.setCantidad(rsOpcion.getInt("mez_can"));
					mapeo.setObservaciones(rsOpcion.getString("mez_obs"));
					mapeo.setFecha(rsOpcion.getDate("mez_fec"));
					mapeo.setOrden(rsOpcion.getInt("mez_ord"));
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return mapeo;
	}
	
	public ArrayList<MapeoControlMezclas> datosOpcionesGlobal(Integer r_idGranel)
	{
		ArrayList<MapeoControlMezclas> vector = null;
		ResultSet rsOpcion = null;		
		MapeoControlMezclas mapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
			tabla = "lab_mezclas_granel";
			campo= "idCodigo";
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT lab_mezclas_granel.idCodigo mez_id, ");
			cadenaSQL.append(" lab_mezclas_granel.idGranel mez_gra, ");
			cadenaSQL.append(" lab_mezclas_granel.identificadorMezcla mez_nom, ");
			cadenaSQL.append(" lab_mezclas_granel.cantidad mez_can, ");
			cadenaSQL.append(" lab_mezclas_granel.orden mez_ord, ");
			cadenaSQL.append(" lab_mezclas_granel.fecha mez_fec, ");
			cadenaSQL.append(" lab_mezclas_granel.observaciones mez_obs ");
			cadenaSQL.append(" FROM " + tabla + " lab_mezclas_granel ");
			
			try
			{
				if (r_idGranel!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_mezclas_granel.idGranel = " + r_idGranel);
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0)
				{
					cadenaSQL.append(" where ");
					cadenaSQL.append(cadenaWhere.toString() );
				}
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				vector = new ArrayList<MapeoControlMezclas>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlMezclas();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("mez_id"));
					mapeo.setIdGranel(rsOpcion.getInt("mez_gra"));
					mapeo.setIdentificadorMezcla(rsOpcion.getString("mez_nom"));
					mapeo.setCantidad(rsOpcion.getInt("mez_can"));
					mapeo.setObservaciones(rsOpcion.getString("mez_obs"));
					mapeo.setFecha(rsOpcion.getDate("mez_fec"));
					mapeo.setOrden(rsOpcion.getInt("mez_ord"));
					
					vector.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
			
		
		return vector;
	}
	
	
	public String guardarNuevoMezclas(MapeoControlMezclas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		tabla = "lab_mezclas_granel";

		try
		{
			
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".idGranel, ");
			cadenaSQL.append(tabla + ".identificadorMezcla , ");
			cadenaSQL.append(tabla + ".cantidad , ");
			cadenaSQL.append(tabla + ".orden , ");
			cadenaSQL.append(tabla + ".fecha, ");
			cadenaSQL.append(tabla + ".observaciones ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    r_mapeo.setOrden(obtenerSiguienteCodigoInterno(con, tabla , "orden"));
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getIdGranel()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdGranel());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getIdentificadorMezcla()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getIdentificadorMezcla());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    preparedStatement.setInt(5, r_mapeo.getOrden());

		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(6, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	
	public String guardarCambiosMezclas(MapeoControlMezclas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		
		tabla = "lab_mezclas_granel";

		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".identificadorMezcla =?, ");
			cadenaSQL.append(tabla + ".cantidad=?, ");
			cadenaSQL.append(tabla + ".observaciones =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getIdentificadorMezcla()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getIdentificadorMezcla());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlMezclas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		
		tabla = "lab_mezclas_granel";
		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}