package borsao.e_borsao.Modulos.LABORATORIO.Graneles.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo.MapeoGraneles;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.server.consultaGranelesServer;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoGraneles mapeoGraneles = null;
	 private ventanaAyuda vHelp  =null;
	 
	 private consultaGranelesServer ser = null;	 
	 private granelesView uw = null;
	 private BeanFieldGroup<MapeoGraneles> fieldGroup;
	 
	 public OpcionesForm(granelesView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoGraneles>(MapeoGraneles.class);
        fieldGroup.bindMemberFields(this);

        this.descripcion.setCaptionAsHtml(true);
        this.anada.setCaptionAsHtml(true);
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
        
        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoGraneles = (MapeoGraneles) uw.grid.getSelectedRow();
                eliminarOpcion(mapeoGraneles);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoGraneles = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOpcion(mapeoGraneles);
	 			}
	 			else
	 			{
	 				MapeoGraneles mapeo_orig= (MapeoGraneles) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo();
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoGraneles = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarGraneles(mapeoGraneles,mapeo_orig);
	 				hm=null;
	 			}
	 			
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0 )
				{
					ser = new consultaGranelesServer(CurrentUser.get());
					String desc = ser.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
				else
					descripcion.setValue(null);
			}
		});
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if ((this.articulo.getValue()!=null && this.articulo.getValue().length()>0)  || (this.anada.getValue()!=null && this.anada.getValue().length()>0))
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue());
			opcionesEscogidas.put("descripcion", this.descripcion.getValue());
			opcionesEscogidas.put("anada", this.anada.getValue());
			opcionesEscogidas.put("cantidad", this.cantidad.getValue());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
			opcionesEscogidas.put("descripcion", "");
			opcionesEscogidas.put("anada", "");
			opcionesEscogidas.put("cantidad", "");
		}
		
		if (this.calcular_sn.getValue()==true)
			opcionesEscogidas.put("calcular_sn", "S");
		else
			opcionesEscogidas.put("calcular_sn", "N");

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoGraneles r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoGraneles();
		fieldGroup.setItemDataSource(new BeanItem<MapeoGraneles>(r_mapeo));
		articulo.focus();
	}
	
	public void eliminarOpcion(MapeoGraneles r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaGranelesServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		ser.eliminar(r_mapeo);
		((ArrayList<MapeoGraneles>) ((OpcionesGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((OpcionesGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
				
			ArrayList<MapeoGraneles> r_vector = (ArrayList<MapeoGraneles>) ((OpcionesGrid) uw.grid).vector;
			
			Indexed indexed = ((OpcionesGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((OpcionesGrid) uw.grid).sort("articulo");
		}

	}
	
	public void crearOpcion(MapeoGraneles r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaGranelesServer(CurrentUser.get());
		String rdo = ser.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoGraneles>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null )//&& )
			{
				if (!((OpcionesGrid) uw.grid).vector.isEmpty())
				{
					Indexed indexed = ((OpcionesGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
					((ArrayList<MapeoGraneles>) ((OpcionesGrid) uw.grid).vector).add(r_mapeo);
					((OpcionesGrid) uw.grid).setRecords((ArrayList<MapeoGraneles>) ((OpcionesGrid) uw.grid).vector);
	//				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
					((OpcionesGrid) uw.grid).sort("articulo");
					((OpcionesGrid) uw.grid).scrollTo(r_mapeo);
					((OpcionesGrid) uw.grid).select(r_mapeo);
	
					((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.actualizarGrid();
				}
			}			
			
			this.setCreacion(false);
			uw.regresarDesdeForm();	
		}
	}
	
	public void modificarGraneles(MapeoGraneles r_mapeo, MapeoGraneles r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			consultaGranelesServer cus  = new consultaGranelesServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((OpcionesGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}

    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoGraneles> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoGraneles= item.getBean();
            if (this.mapeoGraneles.getArticulo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
    
    private boolean todoEnOrden()
    {
    	if (this.articulo.getValue()==null || this.articulo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el articulo");
    		return false;
    	}
    	if (this.anada.getValue()==null || this.anada.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la añada");
    		return false;
    	}
    	if (this.cantidad.getValue()==null || this.cantidad.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la cantidad");
    		return false;
    	}
    	return true;
    }

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		consultaArticulosSMPTServer cps  = new consultaArticulosSMPTServer(CurrentUser.get());
		vectorArticulos=cps.vectorGRSMPT();
		this.vHelp = new ventanaAyuda(this.articulo, this.descripcion, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

}
