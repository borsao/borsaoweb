package borsao.e_borsao.Modulos.LABORATORIO.Graneles.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.LABORATORIO.Graneles.modelo.MapeoGraneles;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaGranelesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaGranelesServer instance;
	
	public consultaGranelesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaGranelesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaGranelesServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoGraneles> datosOpcionesGlobal(MapeoGraneles r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGraneles> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_graneles.articulo pte_art, ");
		cadenaSQL.append(" prd_graneles.idCodigo pte_id, ");
		cadenaSQL.append(" e_articu.descrip_articulo pte_des, ");
		cadenaSQL.append(" prd_graneles.cantidad pte_can, ");
		cadenaSQL.append(" prd_graneles.calcular_sn pte_cal, ");
		cadenaSQL.append(" prd_graneles.añada pte_aña ");
     	cadenaSQL.append(" FROM prd_graneles ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_graneles.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getAnada()!=null && r_mapeo.getAnada().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.añada = '" + r_mapeo.getAnada() + "' ");
				}
				if (r_mapeo.isCalcular_sn())
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.calcular_sn = 'S' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_graneles.añada , prd_graneles.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoGraneles>();
			
			while(rsOpcion.next())
			{
				MapeoGraneles mapeoGraneles = new MapeoGraneles();
				/*
				 * recojo mapeo operarios
				 */
				mapeoGraneles.setArticulo(rsOpcion.getString("pte_art"));
				mapeoGraneles.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pte_des")));
				mapeoGraneles.setAnada(rsOpcion.getString("pte_aña"));
				mapeoGraneles.setCantidad(rsOpcion.getInt("pte_can"));
				mapeoGraneles.setIdCodigo(rsOpcion.getInt("pte_id"));
				mapeoGraneles.setCalcular_sn(rsOpcion.getString("pte_cal").contentEquals("S"));
				
				if (mapeoGraneles.isCalcular_sn())
				{
					Integer traerResto = 0;
	//				if (!mapeoGraneles.getArticulo().startsWith("0101") || 
	//						(mapeoGraneles.getArticulo().startsWith("0101") && mapeoGraneles.getArticuloDestino()!=null && mapeoGraneles.getArticuloDestino().length()>0))
						
						traerResto = devolverEmbotellados(mapeoGraneles.getArticulo(), mapeoGraneles.getAnada());
					
					if (traerResto == null || traerResto == 0) mapeoGraneles.setResto(rsOpcion.getInt("pte_can"));
					else mapeoGraneles.setResto(rsOpcion.getInt("pte_can")-traerResto);
				}
				else
					mapeoGraneles.setResto(0);
				
				vector.add(mapeoGraneles);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public Integer devolverRestoPrevisionArticulo(String r_articulo, String r_anada)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_graneles.cantidad pte_can, ");
		cadenaSQL.append(" prd_graneles.añada pte_aña ");
		cadenaSQL.append(" FROM prd_graneles ");
		cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_graneles.articulo ");
		
		try
		{
			if (r_articulo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_articulo!=null && r_articulo.trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.articulo = '" + r_articulo + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				Integer traerResto = 0;
				traerResto = devolverEmbotellados(r_articulo,rsOpcion.getString("pte_aña"));
								
				if (traerResto == null || traerResto == 0) 
					return rsOpcion.getInt("pte_can");
				else 
					return (rsOpcion.getInt("pte_can")-traerResto);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 0;
	}

	public Integer devolverPrevisionArticulo(String r_articulo, String r_anada)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_graneles.cantidad pte_can ");
		cadenaSQL.append(" FROM prd_graneles ");
		cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_graneles.articulo ");
		
		try
		{
			if (r_articulo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_articulo!=null && r_articulo.trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.articulo = '" + r_articulo + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pte_can");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 0;
	}
	
	public ArrayList<MapeoGraneles> datosOpcionesGlobalCalculando(MapeoGraneles r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGraneles> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_graneles.articulo pte_art, ");
		cadenaSQL.append(" prd_graneles.idCodigo pte_id, ");
		cadenaSQL.append(" e_articu.descrip_articulo pte_des, ");
		cadenaSQL.append(" prd_graneles.cantidad pte_can, ");
		cadenaSQL.append(" prd_graneles.calcular_sn pte_cal, ");
		cadenaSQL.append(" prd_graneles.añada pte_aña ");
		cadenaSQL.append(" FROM prd_graneles ");
		cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_graneles.articulo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getAnada()!=null && r_mapeo.getAnada().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_graneles.añada = '" + r_mapeo.getAnada() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_graneles.añada , prd_graneles.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoGraneles>();
			
			while(rsOpcion.next())
			{
				MapeoGraneles mapeoGraneles = new MapeoGraneles();
				/*
				 * recojo mapeo operarios
				 */
				mapeoGraneles.setArticulo(rsOpcion.getString("pte_art"));
				mapeoGraneles.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pte_des")));
				mapeoGraneles.setAnada(rsOpcion.getString("pte_aña"));
				mapeoGraneles.setCantidad(rsOpcion.getInt("pte_can"));
				mapeoGraneles.setIdCodigo(rsOpcion.getInt("pte_id"));
				mapeoGraneles.setCalcular_sn(rsOpcion.getString("pte_cal").contentEquals("S"));
				
				if (r_mapeo.isCalcular_sn())
				{
					Integer traerResto = 0;
					//				if (!mapeoGraneles.getArticulo().startsWith("0101") || 
					//						(mapeoGraneles.getArticulo().startsWith("0101") && mapeoGraneles.getArticuloDestino()!=null && mapeoGraneles.getArticuloDestino().length()>0))
					
					traerResto = devolverEmbotellados(mapeoGraneles.getArticulo(), mapeoGraneles.getAnada());
					
					if (traerResto == null || traerResto == 0) mapeoGraneles.setResto(rsOpcion.getInt("pte_can"));
					else mapeoGraneles.setResto(rsOpcion.getInt("pte_can")-traerResto);
				}
				else
					mapeoGraneles.setResto(0);
				
				vector.add(mapeoGraneles);				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	private Integer devolverEmbotellados (String r_articulo, String r_anada)
	{
		Integer produccionObtenida = 0;
		consultaProduccionDiariaServer cpds = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
		produccionObtenida = cpds.obtenerProduccionArticuloAgrupada(r_articulo,r_anada,null);
		return produccionObtenida;
	}

	public String guardarNuevo(MapeoGraneles r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		try
		{
			cadenaSQL.append(" INSERT INTO prd_graneles ( ");
			cadenaSQL.append(" prd_graneles.idCodigo, ");
			cadenaSQL.append(" prd_graneles.articulo, ");
			cadenaSQL.append(" prd_graneles.descripcion, ");
			cadenaSQL.append(" prd_graneles.añada, ");
			cadenaSQL.append(" prd_graneles.cantidad, ");
    		cadenaSQL.append(" prd_graneles.calcular_sn) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    Integer id = this.obtenerSiguienteCodigoInterno(con, "prd_graneles", "idCodigo");
		    r_mapeo.setIdCodigo(id);
		    
		    preparedStatement.setInt(1, id);
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.isCalcular_sn())
		    {
		    	preparedStatement.setString(6, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(6, "N");
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoGraneles r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		try
		{
			cadenaSQL.append(" update prd_graneles  SET ");
			cadenaSQL.append(" prd_graneles.añada=?, ");
			cadenaSQL.append(" prd_graneles.cantidad=?, ");
    		cadenaSQL.append(" prd_graneles.calcular_sn=? ");
    		cadenaSQL.append(" WHERE prd_graneles.idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.isCalcular_sn())
		    {
		    	preparedStatement.setString(3, "S");
		    }
		    else
		    {
		    	preparedStatement.setString(3, "N");
		    }
		    if (r_mapeo.getIdCodigo()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getIdCodigo());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoGraneles r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;

		try
		{
			cadenaSQL.append(" DELETE FROM prd_graneles ");            
			cadenaSQL.append(" WHERE prd_graneles.idCodigo= ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}

	@Override
	public String semaforos() {
		return null;
	}
}