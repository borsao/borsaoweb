package borsao.e_borsao.Modulos.LABORATORIO.Conceptos.modelo;


public class MapeoConceptos implements java.io.Serializable
{

	private String codigo;
	private String descripcion;
	
	public MapeoConceptos()
	{		
	
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String r_codigo) {
		this.codigo = r_codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String r_descripcion) {
		this.descripcion= r_descripcion;
	}
	

}