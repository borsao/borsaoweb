package borsao.e_borsao.Modulos.LABORATORIO.Conceptos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.Conceptos.modelo.MapeoConceptos;


public class MantenimientoConceptosBean 
{
 
    /************************METODOS DE CONSULTA DE REGISTROS*********************/
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private static MantenimientoConceptosBean instance;
	
    
	public MantenimientoConceptosBean()
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static MantenimientoConceptosBean getInstance() 
	{
		if (instance == null) 
		{
			instance = new MantenimientoConceptosBean();			
		}
		return instance;
	}
	
	public ResultSet recuperarRegistrosResultSetPartida( MapeoConceptos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" SELECT conceptos.codigo con_cod, ");
		    cadenaSQL.append(" conceptos.descripcion con_des");		    
		    cadenaSQL.append(" FROM conceptos ");
		    
		    cadenaSQL.append(" where codigo = '" + r_mapeo.getCodigo() + "' ");
		    
		    cadenaSQL.append("ORDER BY conceptos.codigo");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
			ResultSet rstRegistrosBuscados = preparedStatement.executeQuery(cadenaSQL.toString());
			return rstRegistrosBuscados;
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError(exception.getMessage());
		}
		return null;
	}
	
	public ArrayList<MapeoConceptos> recuperarRegistros( String r_codigo)
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		MapeoConceptos mapeoConceptos = null;
		ArrayList<MapeoConceptos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" SELECT conceptos.codigo con_cod, ");
		    cadenaSQL.append(" conceptos.descripcion con_des");		    
		    cadenaSQL.append(" FROM conceptos ");
		    
		    cadenaSQL.append(" where codigo = '" + r_codigo + "' ");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);			
			resultSet = preparedStatement.executeQuery();
			
			vector = new ArrayList<MapeoConceptos>();
			
            while (resultSet.next())
            {
                mapeoConceptos = new MapeoConceptos();
                mapeoConceptos.setCodigo(resultSet.getString("con_cod"));
                mapeoConceptos.setDescripcion (resultSet.getString("con_des"));
                vector.add(mapeoConceptos);
            }
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError(exception.getMessage());
		}
		
		return vector;
	}
}