package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo.MapeoLineasMezclasVinos;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo.MapeoSituacionMezclasVinos;

public class consultaSituacionMezclaVinosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaSituacionMezclaVinosServer instance;
	
	
	public consultaSituacionMezclaVinosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSituacionMezclaVinosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSituacionMezclaVinosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoSituacionMezclasVinos> datosOpcionesGlobal()
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoSituacionMezclasVinos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" SELECT idCodigo se_id, " );
			cadenaSQL.append(" articulo se_art, ");
	        cadenaSQL.append(" descripcion se_des, ");
	        cadenaSQL.append(" ejercicio se_eje, ");
	        cadenaSQL.append(" inicial se_exa, ");
	        cadenaSQL.append(" final se_fin, ");
	        cadenaSQL.append(" status se_sta ");	    
	        
	     	cadenaSQL.append(" FROM lab_mezclas_vinos ");
	     	cadenaSQL.append(" order by 1,2 ");

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoSituacionMezclasVinos>();
			
			while(rsOpcion.next())
			{
				MapeoSituacionMezclasVinos mapeoSituacionMezclasVinos = new MapeoSituacionMezclasVinos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSituacionMezclasVinos.setIdCodigo(rsOpcion.getInt("se_id"));
				mapeoSituacionMezclasVinos.setArticulo(rsOpcion.getString("se_art"));
				mapeoSituacionMezclasVinos.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoSituacionMezclasVinos.setEjercicio(rsOpcion.getInt("se_eje"));
				mapeoSituacionMezclasVinos.setStock_inicial(rsOpcion.getInt("se_exa"));
				
				Integer stockFinal = obtenerMovimientosMezcla(mapeoSituacionMezclasVinos.getIdCodigo());
				mapeoSituacionMezclasVinos.setStock_real(mapeoSituacionMezclasVinos.getStock_inicial()-stockFinal);
				
				mapeoSituacionMezclasVinos.setStatus(rsOpcion.getInt("se_sta"));
				vector.add(mapeoSituacionMezclasVinos);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	

	public ArrayList<MapeoLineasMezclasVinos> datosOpcionesLineasGlobal(Integer r_idmezcla, Integer r_stock_inicial)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasMezclasVinos> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Integer litrosQuedan =r_stock_inicial;
		
		try
		{
			cadenaSQL.append(" SELECT lab_lineas_mezclas_vinos.idCodigo det_id, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.orden det_ord, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.tipoOperacion det_tip, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.descripcion det_des, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.status det_sta, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.cantidad det_can, ");
			cadenaSQL.append(" prd_programacion.ejercicio det_eje, ");
			cadenaSQL.append(" prd_programacion.semana det_sem, ");
	        cadenaSQL.append(" prd_programacion.dia det_dia, ");
	        cadenaSQL.append(" prd_programacion.vino det_vin, ");	    
	        cadenaSQL.append(" lab_mezclas_vinos.idCodigo det_mez, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.idprd_programacion det_prg ");
	        
	     	cadenaSQL.append(" FROM lab_lineas_mezclas_vinos ");
	     	cadenaSQL.append(" inner join lab_mezclas_vinos on lab_lineas_mezclas_vinos.idMezcla = lab_mezclas_vinos.idCodigo ");
	     	cadenaSQL.append(" left outer join prd_programacion on lab_lineas_mezclas_vinos.idprd_programacion = prd_programacion.idPrd_programacion");
	     	
	     	if (r_idmezcla!=null)
	     	{
	     		cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idMezcla = " + r_idmezcla);
	     	}
	     	cadenaSQL.append(" order by 2,1 ");

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoLineasMezclasVinos>();
			
			while(rsOpcion.next())
			{
				MapeoLineasMezclasVinos mapeoLineasMezclasVinos = new MapeoLineasMezclasVinos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoLineasMezclasVinos.setIdCodigo(rsOpcion.getInt("det_id"));
				mapeoLineasMezclasVinos.setIdMezcla(rsOpcion.getInt("det_mez"));
				mapeoLineasMezclasVinos.setOrden(rsOpcion.getInt("det_ord"));
				mapeoLineasMezclasVinos.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeoLineasMezclasVinos.setIdPrdProgramacion(rsOpcion.getInt("det_prg"));
				mapeoLineasMezclasVinos.setLitros(rsOpcion.getInt("det_can"));
				mapeoLineasMezclasVinos.setSemana(rsOpcion.getString("det_sem"));
				mapeoLineasMezclasVinos.setDia(rsOpcion.getString("det_dia"));
				mapeoLineasMezclasVinos.setTipo(rsOpcion.getString("det_tip"));
				mapeoLineasMezclasVinos.setDescripcion(rsOpcion.getString("det_des"));
				mapeoLineasMezclasVinos.setStatus(rsOpcion.getInt("det_sta"));
				if (!mapeoLineasMezclasVinos.getTipo().equals("VINO") && !mapeoLineasMezclasVinos.getTipo().equals("COMPRAS"))
				{
					litrosQuedan = litrosQuedan - mapeoLineasMezclasVinos.getLitros();
				}
				mapeoLineasMezclasVinos.setLitrosQuedan(litrosQuedan);
				vector.add(mapeoLineasMezclasVinos);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	private Integer obtenerMovimientosMezcla(Integer r_idMezcla)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(lab_lineas_mezclas_vinos.cantidad) vin_mov ");
     	cadenaSQL.append(" FROM lab_lineas_mezclas_vinos ");
     	cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idMezcla = " + r_idMezcla);
     	cadenaSQL.append(" AND (lab_lineas_mezclas_vinos.tipoOperacion <> 'VINO' AND lab_lineas_mezclas_vinos.tipoOperacion <> 'COMPRAS') ");
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("vin_mov");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_mezclas_vinos.idCodigo) vin_sta ");
     	cadenaSQL.append(" FROM lab_mezclas_vinos ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("vin_sta")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	private Integer obtenerSiguienteDetalle()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_lineas_mezclas_vinos.idCodigo) vin_sta ");
     	cadenaSQL.append(" FROM lab_lineas_mezclas_vinos ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("vin_sta")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	private Integer obtenerSiguienteOrden(Integer r_mezcla)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_lineas_mezclas_vinos.orden) mez_ord ");
     	cadenaSQL.append(" FROM lab_lineas_mezclas_vinos ");
     	cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idMezcla = " + r_mezcla);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("mez_ord")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public Integer obtenerStatus(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_mezclas_vinos.status vin_sta ");
     	cadenaSQL.append(" FROM lab_mezclas_vinos ");
     	cadenaSQL.append(" WHERE lab_mezclas_vinos.idCodigo = " + r_id);
     	
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("vin_sta");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public Integer obtenerStatusDetalle(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_lineas_mezclas_vinos.status vin_sta ");
     	cadenaSQL.append(" FROM lab_lineas_mezclas_vinos ");
     	cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idCodigo = " + r_id);
     	
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("vin_sta");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	
	public String generarInforme(ArrayList<MapeoSituacionMezclasVinos> r_vector)
	{
		
		String resultadoGeneracion = null;
		Integer codigo = null;
		
		
		LibreriaImpresion libImpresion = null;

    	libImpresion=new LibreriaImpresion();
    	
    	for (int i = 0; i< r_vector.size(); i++)
    	{
    		codigo = ((MapeoSituacionMezclasVinos) r_vector.get(i)).getIdCodigo();
    		this.marcarImpresion(1,codigo);
    	}
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/stockVinos" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("stockVinos.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle("stockVinosDetalle.jrxml");
//    	libImpresion.setArchivoPlantillaDetalleCompilado("stockVinosDetalle.jasper");
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado(null);
    	libImpresion.setCarpeta("laboratorio");
    	libImpresion.setBackGround("/fondoA4LogoBlanco.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	for (int i = 0; i< r_vector.size(); i++)
    	{
    		codigo = ((MapeoSituacionMezclasVinos) r_vector.get(i)).getIdCodigo();
    		this.marcarImpresion(0,codigo);
    	}

    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		return null;
	}

	public void eliminar(MapeoSituacionMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_mezclas_vinos ");            
			cadenaSQL.append(" WHERE lab_mezclas_vinos.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			
			eliminarDetalleMezcla(r_mapeo.getIdCodigo());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public String guardarNuevo(MapeoSituacionMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO lab_mezclas_vinos ( ");
    		cadenaSQL.append(" lab_mezclas_vinos.idCodigo, ");
    		cadenaSQL.append(" lab_mezclas_vinos.articulo, ");
    		cadenaSQL.append(" lab_mezclas_vinos.descripcion, ");
	        cadenaSQL.append(" lab_mezclas_vinos.inicial, ");
	        cadenaSQL.append(" lab_mezclas_vinos.final, ");
	        cadenaSQL.append(" lab_mezclas_vinos.ejercicio, ");
	        cadenaSQL.append(" lab_mezclas_vinos.status, ");
		    cadenaSQL.append(" lab_mezclas_vinos.imprimir ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getStock_inicial()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getStock_inicial());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getStock_real()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getStock_real());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    preparedStatement.setInt(7, 0);
		    preparedStatement.setInt(8, 0);

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	public void eliminarDetalle(MapeoLineasMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_lineas_mezclas_vinos ");            
			cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public void eliminarDetalleMezcla(Integer r_idMezcla)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_lineas_mezclas_vinos ");            
			cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idMezcla = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idMezcla);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}

	}

	public void guardarCambios(MapeoSituacionMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE lab_mezclas_vinos set ");
	        cadenaSQL.append(" lab_mezclas_vinos.articulo=?, ");
	        cadenaSQL.append(" lab_mezclas_vinos.descripcion =?, ");
		    cadenaSQL.append(" lab_mezclas_vinos.inicial=?, ");
		    cadenaSQL.append(" lab_mezclas_vinos.final=?, ");
		    cadenaSQL.append(" lab_mezclas_vinos.ejercicio=?, ");
		    cadenaSQL.append(" lab_mezclas_vinos.imprimir=?, ");
		    cadenaSQL.append(" lab_mezclas_vinos.status=? ");
		    cadenaSQL.append(" WHERE lab_mezclas_vinos.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getArticulo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getStock_inicial());
		    preparedStatement.setInt(4, r_mapeo.getStock_real());
		    preparedStatement.setInt(5, r_mapeo.getEjercicio());
		    preparedStatement.setInt(6, 0);
		    preparedStatement.setInt(7, r_mapeo.getStatus()+1);
		    preparedStatement.setInt(8, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	

	public void marcarImpresion(Integer r_imprimir, Integer r_codigo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE lab_mezclas_vinos set ");
		    cadenaSQL.append(" lab_mezclas_vinos.imprimir=? ");
		    cadenaSQL.append(" WHERE lab_mezclas_vinos.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setInt(1, r_imprimir);
		    preparedStatement.setInt(2, r_codigo);
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void guardarCambiosDetalle(MapeoLineasMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE lab_lineas_mezclas_vinos set ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.orden=?, ");
			cadenaSQL.append(" lab_lineas_mezclas_vinos.tipoOperacion=?, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.descripcion=?, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.cantidad =?, ");
		    cadenaSQL.append(" lab_lineas_mezclas_vinos.idprd_programacion=?, ");
		    cadenaSQL.append(" lab_lineas_mezclas_vinos.idMezcla=?, ");
		    cadenaSQL.append(" lab_lineas_mezclas_vinos.ejercicio=?, ");
		    cadenaSQL.append(" lab_lineas_mezclas_vinos.status=? ");
		    cadenaSQL.append(" WHERE lab_lineas_mezclas_vinos.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setInt(1, r_mapeo.getOrden());
		    preparedStatement.setString(2, r_mapeo.getTipo());
		    preparedStatement.setString(3, r_mapeo.getDescripcion());
		    preparedStatement.setInt(4, r_mapeo.getLitros());
		    preparedStatement.setInt(5, r_mapeo.getIdPrdProgramacion());
		    preparedStatement.setInt(6, r_mapeo.getIdMezcla());
		    preparedStatement.setInt(7, r_mapeo.getEjercicio());
		    preparedStatement.setInt(8, r_mapeo.getStatus()+1);
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }

	}
	
	public String guardarNuevoDetalle(MapeoLineasMezclasVinos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO lab_lineas_mezclas_vinos ( ");
    		cadenaSQL.append(" lab_lineas_mezclas_vinos.idCodigo, ");
    		cadenaSQL.append(" lab_lineas_mezclas_vinos.orden, ");
    		cadenaSQL.append(" lab_lineas_mezclas_vinos.tipoOperacion, ");
    		cadenaSQL.append(" lab_lineas_mezclas_vinos.descripcion, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.cantidad, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.idprd_programacion, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.idMezcla, ");
	        cadenaSQL.append(" lab_lineas_mezclas_vinos.ejercicio, ");
		    cadenaSQL.append(" lab_lineas_mezclas_vinos.status ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguienteDetalle());
		    preparedStatement.setInt(2, this.obtenerSiguienteOrden(r_mapeo.getIdMezcla()));
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getIdPrdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdPrdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getIdMezcla()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdMezcla());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    preparedStatement.setInt(9, 0);

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;

	}

	
}

