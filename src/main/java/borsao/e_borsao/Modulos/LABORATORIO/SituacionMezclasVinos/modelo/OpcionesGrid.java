package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.server.consultaSituacionMezclaVinosServer;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view.consultaSituacionMezclasVinosView;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view.pantallaLineasDetalleMezclaVinos;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = true;
	private boolean conFiltro = true;
	private boolean actualizar = false;
	private consultaSituacionMezclasVinosView app = null;
	
    public OpcionesGrid(consultaSituacionMezclasVinosView r_app, ArrayList<MapeoSituacionMezclasVinos> r_vector) 
    {
        this.app=r_app;
        this.vector=r_vector;
        this.actualizar = false;
        
		this.asignarTitulo("Situación Stocks Vinos");
		
		this.generarGrid();    		
    	
    }
    
    public void doEditItem() {
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());    	
    	super.doEditItem();
    }
    
    @Override
    public void doCancelEditor()
    {
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	    super.doCancelEditor();
	  }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        	
	        	MapeoSituacionMezclasVinos mapeo = (MapeoSituacionMezclasVinos) getEditedItemId();
		        
	        	consultaSituacionMezclaVinosServer cs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
	        	Integer status = cs.obtenerStatus(mapeo.getIdCodigo());
	        	if (mapeo.getStatus()==status || status == null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeAdvertencia("Registro modificado por otro usuario. Actualiza los cambios antes de modificar los datos.");
	        		doCancelEditor();
	        	}
	        }
	
	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	if (actualizar)
	        	{
		        	MapeoSituacionMezclasVinos mapeo = (MapeoSituacionMezclasVinos) getEditedItemId();
		        	MapeoSituacionMezclasVinos mapeoOrig=(MapeoSituacionMezclasVinos) getEditedItemId();

		        	consultaSituacionMezclaVinosServer cs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
		        
		        	if (mapeo.isEliminar())
		        	{
		        		cs.eliminar(mapeo);
		        		((ArrayList<MapeoSituacionMezclasVinos>) vector).remove(mapeo);
		        		remove(mapeo);
		        	}
		        	else
		        	{
		        		if (mapeo.getIdCodigo()!=null)
			        	{
			        		cs.guardarCambios(mapeo);
			        		mapeo.setStatus(mapeo.getStatus()+1);
			        		((ArrayList<MapeoSituacionMezclasVinos>) vector).remove(mapeoOrig);
			    			((ArrayList<MapeoSituacionMezclasVinos>) vector).add(mapeo);
			        	}
			        	else
			        	{
			        		cs.guardarNuevo(mapeo);
			        		mapeo.setStatus(0);
			        		((ArrayList<MapeoSituacionMezclasVinos>) vector).add(mapeo);
			        	}
		    			refreshAllRows();
		    			scrollTo(mapeo);
		    			select(mapeo);
		        	}
	        	}
	        	app.reestablecerPantalla();
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("ver"))
            	{
            		MapeoSituacionMezclasVinos mapeo = (MapeoSituacionMezclasVinos) event.getItemId();
            		pantallaLineasDetalleMezclaVinos vt = new pantallaLineasDetalleMezclaVinos(app, mapeo.getIdCodigo(), mapeo.getStock_inicial(), "Detalle de Movimientos " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
            		getUI().addWindow(vt);
            	}
            }
        });

    }
    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("ver");
    	this.camposNoFiltrar.add("stock_inicial");
    	this.camposNoFiltrar.add("stock_real");
    	this.camposNoFiltrar.add("eliminar");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoSituacionMezclasVinos.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.setFrozenColumnCount(3);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ver", "ejercicio", "articulo","descripcion","stock_inicial","stock_real");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.widthFiltros.put("ejercicio", "100");
    	this.widthFiltros.put("articulo", "200");
    	this.widthFiltros.put("descripcion", "400");
    	
    	this.getColumn("ver").setHeaderCaption("");
    	this.getColumn("ver").setSortable(false);
    	this.getColumn("ver").setWidth(new Double(50));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("status").setHidden(true);
    	this.getColumn("stock_real").setEditable(false);

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(150));

    	this.getColumn("articulo").setHeaderCaption("Vino");
    	this.getColumn("articulo").setWidth(new Double(250));
    	this.getColumn("descripcion").setHeaderCaption("Nomenclatura");
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("stock_inicial").setHeaderCaption("Iniciales");
    	this.getColumn("stock_inicial").setWidth(new Double(125));
    	this.getColumn("stock_real").setHeaderCaption("Stock Real");
    	this.getColumn("stock_real").setWidth(new Double(125));
    	
    	this.getColumn("eliminar").setHeaderCaption(" ");
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    	this.getColumn("ejercicio").setEditorField(
    			getComboBox("El ejercicio es obligatorio!"));
	}
	
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
 		   @Override
 		   protected String getTrueString() {
 		      return "";
 		   }
 		   @Override
 		   protected String getFalseString() {
 		      return "";
 		   }
 		});

    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("stock_inicial".equals(cellReference.getPropertyId()) || "stock_real".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ( "ver".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonBars";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaSituacionMezclaVinosServer cmes = consultaSituacionMezclaVinosServer.getInstance(CurrentUser.get());
    	pdfGenerado = cmes.generarInforme((ArrayList<MapeoSituacionMezclasVinos>) this.vector);
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		
		pdfGenerado = null;

    }
    
    private Field<?> getComboBox(String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();
		comboBox.setNullSelectionAllowed(true);
		
		
		consultaVentasServer cv=null;
		ArrayList<String> ej =null;
		
		if (cv==null)
		{
			cv = new consultaVentasServer(eBorsao.get().accessControl.getNombre());
		}
		
		ej = cv.cargarEjercicios();
		
		if (ej!=null)
		{
			IndexedContainer container = new IndexedContainer(ej);
			comboBox.setContainerDataSource(container);
		}
		comboBox.setRequired(true);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
		
		return comboBox;
	}

    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoSituacionMezclasVinos) {
//                MapeoSituacionMezclasVinos progRow = (MapeoSituacionMezclasVinos)bean;
                // The actual description text is depending on the application
                if ("ver".equals(cell.getPropertyId()))
                {
                	descriptionText = "Desglose de Movimientos";
                }
            }
        }
        return descriptionText;
    }
    
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
}
