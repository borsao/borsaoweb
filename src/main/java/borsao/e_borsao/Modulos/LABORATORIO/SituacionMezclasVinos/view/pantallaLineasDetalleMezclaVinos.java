package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CRM.comercial.Consultas.server.consultaVentasServer;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo.MapeoLineasMezclasVinos;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.server.consultaSituacionMezclaVinosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasDetalleMezclaVinos extends Window
{
	private VerticalLayout principal=null;
	private Button btnBotonCVentana = null;
	private Button btnAgregar = null;
	private Button btnSeleccionar = null;
	private Button btnSubir = null;
	private Button btnBajar= null;
	
	private Combo cmbEjercicio = null;
	private Combo cmbTipos = null;
	private TextField txtSemana = null;
	private TextField txtDescripcion = null;
	private TextField txtLitros = null;
	
	private Grid gridDatos = null;
//	private IndexedContainer container =null;
	private Integer stock_incicial = null;
	private Integer stock_real = null;
	private ArrayList<MapeoLineasMezclasVinos> vector=null;
	private MapeoLineasMezclasVinos mapeo = null;

	private boolean actualizar = false;
	private Integer idMezcla = null;
	private consultaSituacionMezclasVinosView csv = null;
	
	private Integer ejercicio =null;
	private Integer semana = null;

	
	public pantallaLineasDetalleMezclaVinos(consultaSituacionMezclasVinosView r_csv, Integer r_idMezcla, Integer r_stock, String r_titulo)
	{
		this.actualizar = false;
		this.idMezcla=r_idMezcla;
		this.stock_incicial=r_stock;
		this.csv = r_csv;
		/*
		 * Establezco los paneles que contendran los controles graficos
		 */
		principal = new VerticalLayout();
		principal.setSizeFull();
		this.stock_real=r_stock;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidth("95%");
//		botonera.setHeight("25px");
		
		HorizontalLayout entradaDatos = new HorizontalLayout();
		entradaDatos.setWidth("95%");
//		entradaDatos.setHeight("20px");
//		entradaDatos.setSpacing(true);

		this.cmbEjercicio=new Combo("Ejercicio");
		this.cmbEjercicio.setWidth("200px");
		this.cmbEjercicio.setRequired(true);
		this.cmbEjercicio.setInvalidAllowed(false);
		this.cmbEjercicio.setNullSelectionAllowed(false);
		this.cargarComboEjercicio();
		
		this.txtSemana = new TextField("Semana");
		this.txtSemana.setWidth("100px");
		this.txtDescripcion= new TextField("Descripcion");
		this.txtDescripcion.setWidth("275px");
		
		this.cmbTipos=new Combo("Tipo");
		this.cmbTipos.setWidth("175px");
		this.cargarComboTipos();

		this.txtLitros= new TextField("Litros");
		this.txtLitros.setWidth("100px");
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);

		
		btnAgregar = new Button("Agregar");
		btnAgregar.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		this.btnSubir= new Button();    	
		this.btnSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.btnSubir.setIcon(FontAwesome.ARROW_CIRCLE_UP);

		this.btnBajar= new Button();    	
		this.btnBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.btnBajar.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

		btnSeleccionar = new Button("Seleccionar");
		btnSeleccionar.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		/*
		 * cargo el grid de datos
		 */
		consultaSituacionMezclaVinosServer cse = new consultaSituacionMezclaVinosServer(CurrentUser.get());
		this.vector = cse.datosOpcionesLineasGlobal(r_idMezcla, r_stock);
		this.llenarRegistros(this.vector);
		
		/*
		 * cargo controles panel superior
		 */
//		entradaDatos.addComponent(this.cmbTipos);
//		entradaDatos.addComponent(this.txtDescripcion);
//		entradaDatos.addComponent(this.txtLitros);
//		entradaDatos.setComponentAlignment(txtLitros, Alignment.TOP_RIGHT);
		
		entradaDatos.addComponent(this.btnAgregar);
		entradaDatos.setComponentAlignment(btnAgregar, Alignment.BOTTOM_LEFT);
		entradaDatos.addComponent(this.btnSubir);
		entradaDatos.setComponentAlignment(btnSubir, Alignment.BOTTOM_LEFT);
		entradaDatos.addComponent(this.btnBajar);
		entradaDatos.setComponentAlignment(btnBajar, Alignment.BOTTOM_LEFT);

		
		entradaDatos.addComponent(this.cmbEjercicio);
		entradaDatos.setComponentAlignment(this.cmbEjercicio, Alignment.BOTTOM_RIGHT);
		entradaDatos.addComponent(this.txtSemana);
		entradaDatos.setComponentAlignment(this.txtSemana, Alignment.BOTTOM_RIGHT);
		entradaDatos.addComponent(this.btnSeleccionar);
		entradaDatos.setComponentAlignment(btnSeleccionar, Alignment.BOTTOM_RIGHT);
		
		/*
		 * cargo controles panel inferior
		 */
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.MIDDLE_CENTER);
		botonera.setSpacing(true);
		/*
		 * agrego paneles al contendor principal
		 */
		principal.addComponent(entradaDatos);
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);		
		principal.setComponentAlignment(entradaDatos, Alignment.TOP_CENTER);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_CENTER);
		principal.setComponentAlignment(this.gridDatos, Alignment.MIDDLE_CENTER);
		principal.setExpandRatio(this.gridDatos, 6);
		principal.setExpandRatio(entradaDatos, 1);
		principal.setExpandRatio(botonera, 1);
		principal.setSpacing(true);
		/*
		 * agrego el contenedor principal a la ventana
		 */
		this.setContent(principal);
		
		/*
		 * Establezco caracteristicas de la ventana
		 */
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("950px");
		
		/*
		 * cargo eventos de los controles
		 */
		this.cargarListeners();
	}
	
	private void cargarListeners()
	{
    	this.btnSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("subir");
			}
		});

    	this.btnBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("bajar");
			}
		});

		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				cerrar();
				
			}
		});

		btnAgregar.addClickListener(new ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				grabarDatos();
			}
		});

		btnSeleccionar.addClickListener(new ClickListener() 
		{
			public void buttonClick(ClickEvent event) 
			{
				llamadaProgramacion(false);
			}
		});

		this.gridDatos.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        	
	        	MapeoLineasMezclasVinos mapeo = (MapeoLineasMezclasVinos) gridDatos.getEditedItemId();
		        
	        	consultaSituacionMezclaVinosServer cs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
	        	Integer status = cs.obtenerStatusDetalle(mapeo.getIdCodigo());
	        	if (mapeo.getStatus()==status || status == null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeAdvertencia("Registro modificado por otro usuario. Actualiza los cambios antes de modificar los datos.");
	        		gridDatos.cancelEditor();
	        	}
	        }
	
	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	if (actualizar)
	        	{
		        	MapeoLineasMezclasVinos mapeo = (MapeoLineasMezclasVinos) gridDatos.getEditedItemId();
		        	MapeoLineasMezclasVinos mapeoOrig=(MapeoLineasMezclasVinos) gridDatos.getEditedItemId();

		        	consultaSituacionMezclaVinosServer cs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
		        
		        	if (mapeo.isEliminar())
		        	{
		        		cs.eliminarDetalle(mapeo);
		        		vector.remove(mapeo);
		        		gridDatos.getContainerDataSource().removeItem(mapeo);
		        	}
		        	else
		        	{
		        		if (mapeo.getIdCodigo()!=null)
			        	{
			        		cs.guardarCambiosDetalle(mapeo);
			        		mapeo.setStatus(mapeo.getStatus()+1);
			        		if (!mapeo.getTipo().equals("VINO") && !mapeo.getTipo().equals("COMPRAS"))
			        		{
			        			stock_real=stock_real+mapeoOrig.getLitros()-mapeo.getLitros();
			        		}
			        		mapeo.setLitrosQuedan(stock_real);
			        		vector.remove(mapeoOrig);
			    			vector.add(mapeo);
			        	}
			        	else
			        	{
			        		cs.guardarNuevoDetalle(mapeo);
			        		mapeo.setStatus(0);			        		
			        		if (!mapeo.getTipo().equals("VINO") && !mapeo.getTipo().equals("COMPRAS"))
			        		{
			        			stock_real=stock_real-mapeo.getLitros();
			        		}
			        		mapeo.setLitrosQuedan(stock_real);
			        		vector.add(mapeo);
			        	}
		        		gridDatos.refreshAllRows();
		        		gridDatos.scrollTo(mapeo);
		        		gridDatos.select(mapeo);
		        	}
	        	}
	        }
		});
		
    	this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoLineasMezclasVinos mapeo = (MapeoLineasMezclasVinos) event.getItemId();
            		
	            	if (event.getPropertyId().toString().equals("semana"))
	            	{
	            		semana = new Integer(mapeo.getSemana());
	            		ejercicio = mapeo.getEjercicio();
	            		llamadaProgramacion(true);
	            	}
	            }
    		}
        });
	}
	
	private void llenarRegistros(ArrayList<MapeoLineasMezclasVinos> r_vector)
	{

		Iterator iterator = null;
		MapeoLineasMezclasVinos mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		BeanItemContainer<MapeoLineasMezclasVinos> container = new BeanItemContainer<MapeoLineasMezclasVinos>(MapeoLineasMezclasVinos.class);		
		this.gridDatos= new Grid(container);
		
		while (iterator.hasNext())
		{
			
			mapeoAyuda = (MapeoLineasMezclasVinos) iterator.next();
//    		this.stock_real = ;
    		this.gridDatos.getContainerDataSource().addItem(mapeoAyuda);
		}
		if (mapeoAyuda!=null)
		{
			this.stock_real = mapeoAyuda.getLitrosQuedan();
		}
		/*
		 * creo el grid con el contenedor calculado
		 */
		

		/*
		 * establezco las caracteristicas del grid
		 */
		this.gridDatos.setEditorEnabled(true);
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("90%");
		this.gridDatos.setHeight("100%");

		/*
		 * caracteristicas edicion celdas
		 */
		this.gridDatos.getColumn("tipo").setEditorField(
				getComboBox("Tipo", "El tipo operacion es obligatorio!"));
		this.gridDatos.getColumn("semana").setEditable(false);
		this.gridDatos.getColumn("dia").setEditable(false);
		this.gridDatos.getColumn("litrosQuedan").setEditable(false);
		
		/*
		 * asignamos los estlos de las columnas
		 */
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setColumnOrder("tipo","ejercicio", "semana","dia","descripcion","litros","litrosQuedan","eliminar","idPrdProgramacion");
		
		this.gridDatos.getColumn("ejercicio").setHeaderCaption("Ejercicio");
		this.gridDatos.getColumn("semana").setHeaderCaption("Semana");
		this.gridDatos.getColumn("dia").setHeaderCaption("Dia");
		this.gridDatos.getColumn("tipo").setHeaderCaption("Tipo");
		this.gridDatos.getColumn("litros").setHeaderCaption("Litros");
		this.gridDatos.getColumn("litrosQuedan").setHeaderCaption("Resto");
		this.gridDatos.getColumn("descripcion").setHeaderCaption("Descripcion");
		this.gridDatos.getColumn("eliminar").setHeaderCaption(" ");
		
		this.gridDatos.getColumn("idCodigo").setHidden(true);
		this.gridDatos.getColumn("idPrdProgramacion").setHidden(true);
		this.gridDatos.getColumn("idMezcla").setHidden(true);
		this.gridDatos.getColumn("status").setHidden(true);
		this.gridDatos.getColumn("orden").setHidden(true);
		
		this.gridDatos.getColumn("ejercicio").setWidth(new Double(100));
		this.gridDatos.getColumn("semana").setWidth(new Double(100));
		this.gridDatos.getColumn("dia").setWidth(new Double(100));
		this.gridDatos.getColumn("tipo").setWidth(new Double(200));
		this.gridDatos.getColumn("descripcion").setWidth(new Double(400));
		this.gridDatos.getColumn("litros").setWidth(new Double(100));
		this.gridDatos.getColumn("litrosQuedan").setWidth(new Double(100));

    	this.gridDatos.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
  		   @Override
  		   protected String getTrueString() {
  		      return "";
  		   }
  		   @Override
  		   protected String getFalseString() {
  		      return "";
  		   }
  		});


		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
			boolean lineaResaltada = false;
            public String getStyle(Grid.CellReference cellReference) 
            {            	
            	if ("tipo".equals(cellReference.getPropertyId()) && cellReference.getValue().toString().equals("VINO"))
            	{
            		lineaResaltada=true;
            	}
            	else if ("tipo".equals(cellReference.getPropertyId()) && !cellReference.getValue().toString().equals("VINO"))
            	{
            		lineaResaltada=false;
            	}
            	else if (("semana".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()))  && (cellReference.getValue()!=null && cellReference.getValue().toString().length()>0))
            	{
            		if (lineaResaltada)
            		{
            			return "Rcell-07fff8P";
            		}
            		else
            		{
            			return "cell-07fff8P";
            		}
			
            	}
            	if ("litros".equals(cellReference.getPropertyId()) || "litrosQuedan".equals(cellReference.getPropertyId())) 
            	{
            		if (lineaResaltada)
            		{
            			return "Rcell-07fff8";
            		}            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		if (lineaResaltada)
            		{
            			return "cell-07fff8";
            		}
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void grabarDatos()
	{
		/*
		 * recoge los datos de las entradas de datos y las manda a la parte serivodora para guardarlos
		 * actualiza el grid con los datos guardados
		 */
		this.gridDatos.getColumn("semana").setEditable(false);
		
    	this.mapeo=new MapeoLineasMezclasVinos();
    	this.mapeo.setIdMezcla(this.idMezcla);
    	this.mapeo.setEliminar(false);
    	this.mapeo.setDia(" ");
    	this.mapeo.setStatus(0);
    	this.mapeo.setEjercicio(0);
    	
    	this.mapeo.setSemana(" ");
    	this.mapeo.setDescripcion(" ");
    	this.mapeo.setTipo(" ");
    	this.mapeo.setLitros(0);
    	this.mapeo.setLitrosQuedan(0);
    	
    	gridDatos.getContainerDataSource().addItem(this.mapeo);    	
    	gridDatos.editItem(this.mapeo);

	}
	private Field<?> getComboBox(String r_area, String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();

		switch (r_area)
		{
			case "Ejercicio":
			{
				consultaVentasServer cv=null;
				ArrayList<String> ej =null;
				
				if (cv==null)
				{
					cv = new consultaVentasServer(eBorsao.get().accessControl.getNombre());
				}
				
				ej = cv.cargarEjercicios();
				
				if (ej!=null)
				{
					IndexedContainer container = new IndexedContainer(ej);
					comboBox.setContainerDataSource(container);
				}
				break;
			}
			case "Tipo":
			{
				comboBox.addItem("EMBOTELLADO");
				comboBox.addItem("VINO");
				comboBox.addItem("MERMAS");
				comboBox.addItem("HISTORICO");
				comboBox.addItem("OTROS");
				comboBox.addItem("DESVIO");
				comboBox.addItem("COMPRAS");
				break;
			}
			
		}
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
		return comboBox;
	}
	
	private void cargarComboEjercicio()
	{
		consultaVentasServer cv=null;
		ArrayList<String> ej =null;
		
		if (cv==null)
		{
			cv = new consultaVentasServer(eBorsao.get().accessControl.getNombre());
		}
		
		ej = cv.cargarEjercicios();
		
		if (ej!=null)
		{
			IndexedContainer container = new IndexedContainer(ej);
			cmbEjercicio.setContainerDataSource(container);
		}
	}
	private void cargarComboTipos()
	{
//		consultaTiposMovimientosVinosServer ctv=null;
//		ArrayList<String> tp =null;
//		
//		if (ctv==null)
//		{
//			ctv = new consultaTiposMovimientosVinosServer(eBorsao.get().accessControl.getNombre());
//		}
//		
//		tp = ctv.cargarTiposMovimientos();
//		
//		if (tp!=null)
//		{
//			IndexedContainer container = new IndexedContainer(tp);
//			cmbEjercicio.setContainerDataSource(container);
//		}
		cmbTipos.setRequired(true);
		cmbTipos.setInvalidAllowed(false);
		cmbTipos.setNullSelectionAllowed(false);
	}

	private void cerrar()
	{
		this.csv.reestablecerPantalla();
		close();
	}
	
	public void insertarDatos(MapeoProgramacion r_mapeo)
	{
		this.gridDatos.getColumn("semana").setEditable(true);
		
		this.mapeo=new MapeoLineasMezclasVinos();
		this.mapeo.setIdPrdProgramacion(r_mapeo.getIdProgramacion());		
    	this.mapeo.setIdMezcla(this.idMezcla);
    	this.mapeo.setEliminar(false);
    	this.mapeo.setDia(r_mapeo.getDia());
    	this.mapeo.setStatus(0);

    	this.mapeo.setSemana(r_mapeo.getSemana());
    	this.mapeo.setDescripcion(r_mapeo.getVino());
    	this.mapeo.setTipo(r_mapeo.getTipo());
    	this.mapeo.setLitros(r_mapeo.getCantidad());
    	this.mapeo.setLitrosQuedan(0);
    	
    	gridDatos.getContainerDataSource().addItem(this.mapeo);    	
    	gridDatos.editItem(this.mapeo);
	}
	
	private void llamadaProgramacion(boolean r_consulta)
	{		
		if (!r_consulta)
		{
			if (this.txtSemana.getValue()!=null && this.txtSemana.getValue().length()>0)
			{
				semana =new Integer(this.txtSemana.getValue());
			}
			else
			{
				semana=null;
			}
			if (this.cmbEjercicio.getValue()!=null)
			{
				ejercicio= new Integer(this.cmbEjercicio.getValue().toString());
			}
			else
			{
				ejercicio=null;
			}
		}
		pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(this, SelectionMode.SINGLE, ejercicio, semana, r_consulta);
		getUI().addWindow(vt);

	}
	
	private void ordenarFilas(String r_direccion)
	{
		int filaOrigen = -1;
		int filaDestino = -1;
		
		
		if (gridDatos.getSelectedRow()!=null)
		{
			MapeoLineasMezclasVinos mapeoOrigen=(MapeoLineasMezclasVinos) gridDatos.getSelectedRow();
			
			filaOrigen = obtenerIndice(mapeoOrigen);
			
			if (r_direccion.equals("subir")) 
			{
				filaDestino = filaOrigen - 1;
			}
			else
			{
				filaDestino = filaOrigen + 1;
			}
			
    		try
    		{
    			
        		MapeoLineasMezclasVinos mapeoDestino = (MapeoLineasMezclasVinos)  gridDatos.getContainerDataSource().getIdByIndex(filaDestino);
        		
        		vector.remove(mapeoOrigen);
    			vector.remove(mapeoDestino);
    			
        		Integer origen = mapeoOrigen.getOrden();
        		Integer destino = mapeoDestino.getOrden();
        		
        		mapeoOrigen.setOrden(destino);
        		mapeoDestino.setOrden(origen);
        		
        		guardarCambios(mapeoOrigen, mapeoDestino);
        		gridDatos.sort("orden");
//        		gridDatos.select(mapeoOrigen);
//        		gridDatos.scrollTo(mapeoOrigen);
//        		this.principal.addComponent(this.gridDatos);
        		
    		}
			catch (IndexOutOfBoundsException ex)
			{
				
			}
		}
		
	}
	
	private int obtenerIndice(MapeoLineasMezclasVinos r_mapeo)
	{
		
		for (int i = 0; i<gridDatos.getContainerDataSource().getItemIds().size();i++)
		{
			MapeoLineasMezclasVinos mapeoLeido = (MapeoLineasMezclasVinos) gridDatos.getContainerDataSource().getIdByIndex(i);
			if (mapeoLeido.equals(r_mapeo))
			{
				return i;
			}
		}
		return -1;
	}

	private void guardarCambios(MapeoLineasMezclasVinos r_mapeo, MapeoLineasMezclasVinos  r_mapeoPrevio)
	{
		consultaSituacionMezclaVinosServer cmvs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
		
		if (r_mapeo!=null)
		{
			cmvs.guardarCambiosDetalle(r_mapeo);
			vector.add(r_mapeo);
		}				
		if (r_mapeoPrevio!=null)
		{
			cmvs.guardarCambiosDetalle(r_mapeoPrevio);
			vector.add(r_mapeoPrevio);
		}
			
		if (!this.vector.isEmpty())
		{
			
//			this.gridDatos.removeAllColumns();
//			this.principal.removeComponent(this.gridDatos);
//			this.gridDatos=null;
//			this.gridDatos.refreshAllRows();
//			consultaSituacionMezclaVinosServer cse = new consultaSituacionMezclaVinosServer(CurrentUser.get());
//			this.vector = cse.datosOpcionesLineasGlobal(this.idMezcla, this.stock_incicial);
//			
//			this.llenarRegistros(this.vector);
		}
	}
}