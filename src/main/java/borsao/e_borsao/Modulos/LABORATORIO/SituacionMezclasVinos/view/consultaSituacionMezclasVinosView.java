package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo.MapeoSituacionMezclasVinos;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.server.consultaSituacionMezclaVinosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaSituacionMezclasVinosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Situacion Mezclas Vinos";
	private final String titulo = "Situacion Stocks Mezclas Vinos";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private boolean generado = false;
	private MapeoSituacionMezclasVinos mapeo = null;
	private Button opcAceptar = null;
	private Button opcSeleccionar = null;
		
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSituacionMezclasVinos> r_vector=null;
    	consultaSituacionMezclaVinosServer cmvs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
    	r_vector=cmvs.datosOpcionesGlobal();
    	
    	
    	grid = new OpcionesGrid(this, r_vector);
//    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
//    	{
//    		setHayGrid(false);
//    	}
//    	else
//    	{
    		generado=true;
    		setHayGrid(true);
//    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaSituacionMezclasVinosView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	this.generarGrid(null);
    	this.barAndGridLayout.removeComponent(this.cabLayout);
    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    	
    	this.opcSeleccionar= new Button();    	
    	this.opcSeleccionar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.opcSeleccionar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcSeleccionar.setIcon(FontAwesome.PLUS_SQUARE);

    	this.opcAceptar= new Button();    	
    	this.opcAceptar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.opcAceptar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.opcAceptar.setIcon(FontAwesome.CALCULATOR);
    	this.opcAceptar.setVisible(false);

		this.cabLayout.addComponent(this.opcSeleccionar);
    	this.cabLayout.setComponentAlignment(this.opcSeleccionar,Alignment.BOTTOM_LEFT);

    	this.cabLayout.addComponent(this.opcAceptar);
    	this.cabLayout.setComponentAlignment(this.opcAceptar,Alignment.BOTTOM_LEFT);
    	
    	this.cargarListeners();
    }

    private void cargarListeners()
    {
    	this.opcAceptar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				mostrarFilas(grid.getSelectedRows());
			}
		});
    	this.opcAceptar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				mostrarFilas(grid.getSelectedRows());
			}
		});
    }
    
    public void newForm()
    {
    	this.mapeo=new MapeoSituacionMezclasVinos();
    	this.mapeo.setArticulo("");
    	this.mapeo.setDescripcion("");
    	this.mapeo.setEliminar(false);
    	this.mapeo.setStock_inicial(0);
    	this.mapeo.setStock_real(0);
    	this.mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
    	
    	if (grid==null)
    	{
    		grid = new OpcionesGrid(this,null);
    	}
//    	else
//    	{
//    		if (!grid.isEditorActive()) grid.ad;
//    	}
    	grid.getContainerDataSource().addItem(mapeo);    	
    	grid.editItem(this.mapeo);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	this.mapeo = (MapeoSituacionMezclasVinos) r_fila;
    }
    
    @Override
    public void reestablecerPantalla() {
    	this.botonesGenerales(true);
    	this.opcImprimir.setEnabled(true);
    	
//		grid.refreshAllRows();
		
    	ArrayList<MapeoSituacionMezclasVinos> r_vector=null;
    	consultaSituacionMezclaVinosServer cmvs = new consultaSituacionMezclaVinosServer(CurrentUser.get());
    	r_vector=cmvs.datosOpcionesGlobal();

		
		Indexed indexed = ((OpcionesGrid) this.grid).getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
            indexed.removeItem(itemId);
        }
//        indexed.addItemAt(list.size()+1, r_mapeo);
        
		((OpcionesGrid) this.grid).setRecords(r_vector);
//		((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);


//    	this.generarGrid(null);
    }
    
    public void print() 
    {
    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;

    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) 
	{
		Iterator it = null;
		ArrayList<MapeoSituacionMezclasVinos> vector=null;
		
		it = r_filas.iterator();
		vector = new ArrayList<MapeoSituacionMezclasVinos>();
		
		while (it.hasNext())
		{			
			vector.add((MapeoSituacionMezclasVinos) it.next());
		}

		
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
