package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoSituacionMezclasVinos extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer stock_inicial;
	private Integer ejercicio;
	private Integer stock_real;
	private Integer status;
	private String ver = " ";
	private boolean eliminar=false;

	public MapeoSituacionMezclasVinos()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getStock_inicial() {
		return stock_inicial;
	}

	public void setStock_inicial(Integer stock_inicial) {
		this.stock_inicial = stock_inicial;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getStock_real() {
		return stock_real;
	}

	public void setStock_real(Integer stock_real) {
		this.stock_real = stock_real;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}