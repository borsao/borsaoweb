package borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoLineasMezclasVinos extends MapeoGlobal
{
	private Integer idPrdProgramacion;
	private Integer idMezcla;
	private Integer orden;
	private Integer litros;
	private Integer litrosQuedan;
	private Integer ejercicio;
	private String semana;
	private String dia;
	private String tipo;
	private String descripcion;
	private boolean eliminar;
	private Integer status ;
	
	public MapeoLineasMezclasVinos()
	{
	}

	public Integer getIdPrdProgramacion() {
		return idPrdProgramacion;
	}

	public void setIdPrdProgramacion(Integer idPrdProgramacion) {
		this.idPrdProgramacion = idPrdProgramacion;
	}

	public Integer getIdMezcla() {
		return idMezcla;
	}

	public void setIdMezcla(Integer idMezcla) {
		this.idMezcla = idMezcla;
	}

	public Integer getLitros() {
		return litros;
	}

	public void setLitros(Integer litros) {
		this.litros = litros;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getLitrosQuedan() {
		return litrosQuedan;
	}

	public void setLitrosQuedan(Integer litrosQuedan) {
		this.litrosQuedan = litrosQuedan;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	
}