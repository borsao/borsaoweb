package borsao.e_borsao.Modulos.LABORATORIO.Movimientos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.Trazabilidad.modelo.MapeoTrazabilidad;


public class MantenimientoMovimientosBean 
{
 
    /************************METODOS DE CONSULTA DE REGISTROS*********************/
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private static MantenimientoMovimientosBean instance;
	
    
	public MantenimientoMovimientosBean()
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static MantenimientoMovimientosBean getInstance() 
	{
		if (instance == null) 
		{
			instance = new MantenimientoMovimientosBean();			
		}
		return instance;
	}
	    
	public ResultSet recuperarRegistrosResultSet(String r_lote)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		ResultSet reg = null;
		try
		{
            cadenaSQL.append(" SELECT movimientos.parte trz_par, ");
		    cadenaSQL.append(" movimientos.linea trz_lin, ");
		    cadenaSQL.append(" movimientos.fecha trz_fec , ");
		    cadenaSQL.append(" movimientos.seccion trz_sec, ");
		    cadenaSQL.append(" movimientos.tipo trz_tip, ");
		    cadenaSQL.append(" movimientos.localizacion trz_loc, ");
		    cadenaSQL.append(" movimientos.vino trz_vin, ");
		    cadenaSQL.append(" movimientos.litros trz_lit, ");
		    cadenaSQL.append(" movimientos.diasSal trz_ds, ");
		    cadenaSQL.append(" movimientos.cantidad trz_can, ");
		    cadenaSQL.append(" movimientos.partida trz_prt, ");
		    cadenaSQL.append(" movimientos.loteSalida trz_ls, ");
		    cadenaSQL.append(" movimientos.referencia trz_ref, ");
		    cadenaSQL.append(" movimientos.almacen trz_alm,");		    
		    cadenaSQL.append(" conceptos.descripcion con_des");
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" LEFT OUTER JOIN conceptos on conceptos.codigo = movimientos.concepto and conceptos.codigo='FIM'");		    
		    cadenaSQL.append(" where loteSalida = '" + r_lote + "'");
		    
		    con=this.conManager.establecerConexion();            
			preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			reg = preparedStatement.executeQuery();
			return reg;
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		return null;
	}
	
	public Integer recuperarRegistrosLike(String parmClausulaWhere) throws Exception
	{
		 PreparedStatement preparedStatement = null;
	        ResultSet resultSet = null;
	        StringBuffer cadenaSQL = new StringBuffer();
	        int totalRegistros =0;
	        
	        try
	        {
	            cadenaSQL.append(" SELECT * " );		    
			    cadenaSQL.append(" FROM movimientos ");
			    
   	     		cadenaSQL.append(parmClausulaWhere);
   	     		
   	     		con=this.conManager.establecerConexion();
	            preparedStatement = con.prepareStatement(cadenaSQL.toString());
	            resultSet = preparedStatement.executeQuery();
	            resultSet.last();
				totalRegistros = resultSet.getRow();
	        }
	        catch (Exception exception)
	        {
	            exception.printStackTrace();
	        }
	        finally
	        {
	            try
	            {
	                if (resultSet != null)
	                {
	                    resultSet.close();
	                }
	                if (preparedStatement != null)
	                {
	                    preparedStatement.close();
	                }
	            }
	            catch (SQLException sqlException)
	            {
	                sqlException.printStackTrace();
	            }
	        }	        
	        return (new Integer(totalRegistros));		
	}
	
	public ResultSet recuperarRegistrosResultSetPartida( MapeoTrazabilidad r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		RutinasFechas rutFecha=null;
		String desdeFecha=null;
		String hastaFecha=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		rutFecha = new RutinasFechas();
		
		try
		{
			desdeFecha = rutFecha.convertirAFechaMysql(rutFecha.restarDiasFecha(r_mapeo.getFecha().toString(),r_mapeo.getDiasSal()));
			hastaFecha = rutFecha.convertirAFechaMysql(r_mapeo.getFecha().toString());
			
            cadenaSQL.append(" SELECT movimientos.parte trz_par, ");
		    cadenaSQL.append(" movimientos.linea trz_lin, ");
		    cadenaSQL.append(" movimientos.fecha trz_fec , ");
		    cadenaSQL.append(" movimientos.seccion trz_sec, ");
		    cadenaSQL.append(" movimientos.tipo trz_tip, ");
		    cadenaSQL.append(" movimientos.localizacion trz_loc, ");
		    cadenaSQL.append(" movimientos.vino trz_vin, ");
		    cadenaSQL.append(" movimientos.litros trz_lit, ");
		    cadenaSQL.append(" movimientos.diasSal trz_ds, ");
		    cadenaSQL.append(" movimientos.cantidad trz_can, ");
		    cadenaSQL.append(" movimientos.partida trz_prt, ");
		    cadenaSQL.append(" movimientos.loteSalida trz_ls, ");
		    cadenaSQL.append(" movimientos.referencia trz_ref, ");
		    cadenaSQL.append(" movimientos.almacen trz_alm,");		    
		    cadenaSQL.append(" conceptos.descripcion con_des");
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" LEFT OUTER JOIN conceptos on conceptos.codigo = movimientos.concepto and conceptos.codigo='FIM'");		    
		    cadenaSQL.append(" where partida = '" + r_mapeo.getPartida() + "' ");
		    cadenaSQL.append(" and fecha >= ' " + desdeFecha + "' ");
		    cadenaSQL.append(" and fecha <= ' " + hastaFecha + "' ");
		    
		    cadenaSQL.append("ORDER BY movimientos.parte desc, movimientos.linea desc");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ResultSet rstRegistrosBuscados = preparedStatement.executeQuery(cadenaSQL.toString());
			return rstRegistrosBuscados;
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		return null;
	}
	
	public ArrayList<MapeoAyudas> recuperarLotes( String r_lote)
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		MapeoAyudas mapeoMovimientos = null;
		ArrayList<MapeoAyudas> vector =null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
            cadenaSQL.append(" SELECT distinct movimientos.loteSalida codigo, ");
            cadenaSQL.append(" movimientos.referencia descripcion ");
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" where loteSalida is not null ");
		    cadenaSQL.append(" and loteSalida like '%" + r_lote + "%' ");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);			
			resultSet = preparedStatement.executeQuery();
			
			vector = new ArrayList<MapeoAyudas>();
			
            while (resultSet.next())
            {
            	if (resultSet.getString("codigo").trim().length()>0)
            	{
	                mapeoMovimientos = new MapeoAyudas();
	                mapeoMovimientos.setCodigo(resultSet.getString("codigo"));
	                mapeoMovimientos.setDescripcion(resultSet.getString("descripcion"));
	                vector.add(mapeoMovimientos);
            	}
            }
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		
		return vector;
	}

	public MapeoAyudas comprobarLote(String r_lote)
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		MapeoAyudas mapeoMovimientos = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
            cadenaSQL.append(" SELECT distinct movimientos.loteSalida codigo, ");
            cadenaSQL.append(" movimientos.referencia descripcion ");
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" where loteSalida is not null ");
		    cadenaSQL.append(" and loteSalida ='" + r_lote + "' ");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);			
			resultSet = preparedStatement.executeQuery();
			
			
            while (resultSet.next())
            {
            	if (resultSet.getString("codigo").trim().length()>0)
            	{
	                mapeoMovimientos = new MapeoAyudas();
	                mapeoMovimientos.setCodigo(resultSet.getString("codigo"));
	                mapeoMovimientos.setDescripcion(resultSet.getString("descripcion"));
            	}
            }
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		
		return mapeoMovimientos;
	}

	public ResultSet recuperarRegistrosResultSetParte( MapeoTrazabilidad r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		RutinasFechas rutFecha=null;
		String desdeFecha=null;
		String hastaFecha=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		rutFecha = new RutinasFechas();

		try
		{
			desdeFecha = rutFecha.convertirAFechaMysql(rutFecha.restarDiasFecha(r_mapeo.getFecha().toString(),r_mapeo.getDiasSal()));
			hastaFecha = rutFecha.convertirAFechaMysql(r_mapeo.getFecha().toString());

			cadenaSQL.append(" SELECT movimientos.parte trz_par, ");
		    cadenaSQL.append(" movimientos.linea trz_lin, ");
		    cadenaSQL.append(" movimientos.fecha trz_fec , ");
		    cadenaSQL.append(" movimientos.seccion trz_sec, ");
		    cadenaSQL.append(" movimientos.tipo trz_tip, ");
		    cadenaSQL.append(" movimientos.localizacion trz_loc, ");
		    cadenaSQL.append(" movimientos.vino trz_vin, ");
		    cadenaSQL.append(" movimientos.litros trz_lit, ");
		    cadenaSQL.append(" movimientos.diasSal trz_ds, ");
		    cadenaSQL.append(" movimientos.cantidad trz_can, ");
		    cadenaSQL.append(" movimientos.partida trz_prt, ");
		    cadenaSQL.append(" movimientos.loteSalida trz_ls, ");
		    cadenaSQL.append(" movimientos.referencia trz_ref, ");
		    cadenaSQL.append(" movimientos.almacen trz_alm,");		    
		    cadenaSQL.append(" conceptos.descripcion con_des");
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" LEFT OUTER JOIN conceptos on conceptos.codigo = movimientos.concepto and conceptos.codigo='FIM'");
		    cadenaSQL.append(" where parte = '" + r_mapeo.getParte() + "' ");
		    cadenaSQL.append(" and fecha >= ' " + desdeFecha + "' ");
		    cadenaSQL.append(" and fecha <= ' " + hastaFecha + "' ");
		    
		    cadenaSQL.append("ORDER BY movimientos.parte desc, movimientos.linea desc");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ResultSet rstRegistrosBuscados = preparedStatement.executeQuery(cadenaSQL.toString());
			return rstRegistrosBuscados;
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarRegistrosResultSet" + exception.getMessage());
		}
		return null;
	}
	
	public Double recuperarSumaLitrosParte( String r_parte)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Double suma = null;
		
		try
		{
			suma = new Double(0);
			
			cadenaSQL.append(" SELECT sum(movimientos.litros) trz_lit ");		    
		    cadenaSQL.append(" FROM movimientos ");
		    cadenaSQL.append(" where parte = '" + r_parte + "' ");
		    cadenaSQL.append(" and tipo like 'S%'");

		    con=this.conManager.establecerConexion();
		    preparedStatement = con.prepareStatement(cadenaSQL.toString(), ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			ResultSet rstRegistrosBuscados = preparedStatement.executeQuery(cadenaSQL.toString());
			while (rstRegistrosBuscados.next())
			{
				suma = rstRegistrosBuscados.getDouble("trz_lit");
			}
		}
		catch (Exception exception)
		{
			this.serNotif.mensajeError("Mantenimiento Movimientos Bean - recuperarSumaLitrosParte" + exception.getMessage());
		}
		return suma;
	}
}