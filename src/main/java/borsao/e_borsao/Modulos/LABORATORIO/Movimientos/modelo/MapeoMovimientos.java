package borsao.e_borsao.Modulos.LABORATORIO.Movimientos.modelo;


public class MapeoMovimientos implements java.io.Serializable
{

	private String loteSalida;
	private String referencia;
	
	public MapeoMovimientos()
	{		
	
	}
	
	public String getLoteSalida() {
		return loteSalida;
	}
	
	public void setLoteSalida(String loteSalida) {
		this.loteSalida = loteSalida;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	

}