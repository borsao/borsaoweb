package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajoBarricas;

public class consultaOrdenesTrabajoBarricasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaOrdenesTrabajoBarricasServer instance;
	
	public consultaOrdenesTrabajoBarricasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaOrdenesTrabajoBarricasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaOrdenesTrabajoBarricasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoOrdenesTrabajoBarricas> datosOrdenesTrabajoBarricasGlobal(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoOrdenesTrabajoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_ordenes_bar.idCodigo ord_bar_id,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica ord_bar_idBar,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden ord_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap, ");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje ");
		
		cadenaSQL.append(" from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes_bar.idOrden =  lab_barricas_ordenes.idCodigo ");
		cadenaSQL.append(" inner join lab_barricas on lab_barricas_ordenes_bar.idBarrica =  lab_barricas.idCodigo ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getMapeoBarricas()!=null && r_mapeo.getMapeoBarricas().getNumero()!=null && r_mapeo.getMapeoBarricas().getNumero().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas.numero = " + r_mapeo.getMapeoBarricas().getNumero());
				}
				if (r_mapeo.getMapeoOrdenes()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes_bar.idOrden = " + r_mapeo.getMapeoOrdenes().getIdCodigo());
				}
				if (r_mapeo.getMapeoOrdenes().getEjercicio()!=null && r_mapeo.getMapeoOrdenes().getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.ejercicio = " + r_mapeo.getMapeoOrdenes().getEjercicio());
				}
				if (r_mapeo.getMapeoOrdenes().getReferencia()!=null && r_mapeo.getMapeoOrdenes().getReferencia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.referencia = '" + r_mapeo.getMapeoOrdenes().getReferencia() + "' ");
				}

			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by lab_barricas.numero ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOrdenesTrabajoBarricas.setIdCodigo(rsOpcion.getInt("ord_bar_id"));
				mapeoOrdenesTrabajoBarricas.setIdBarrica(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.setIdOrden(rsOpcion.getInt("ord_id"));
				
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setIdCodigo(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setNumero(rsOpcion.getInt("bar_num"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setDescripcion(rsOpcion.getString("bar_des"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setCapacidad(rsOpcion.getInt("bar_cap"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setEjercicio(rsOpcion.getInt("bar_eje"));
				
				vector.add(mapeoOrdenesTrabajoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoOrdenesTrabajoBarricas> datosOrdenesTrabajoBarricasGlobalPendientes(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoOrdenesTrabajoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_ordenes_bar.idCodigo ord_bar_id,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica ord_bar_idBar,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden ord_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap, ");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje ");
		
		cadenaSQL.append(" from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes_bar.idOrden =  lab_barricas_ordenes.idCodigo ");
		cadenaSQL.append(" inner join lab_barricas on lab_barricas_ordenes_bar.idBarrica =  lab_barricas.idCodigo ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getMapeoBarricas()!=null && r_mapeo.getMapeoBarricas().getNumero()!=null && r_mapeo.getMapeoBarricas().getNumero().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas.numero = " + r_mapeo.getMapeoBarricas().getNumero());
				}
				if (r_mapeo.getMapeoOrdenes()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes_bar.idOrden = " + r_mapeo.getMapeoOrdenes().getIdCodigo());
				}
				if (r_mapeo.getMapeoOrdenes().getEjercicio()!=null && r_mapeo.getMapeoOrdenes().getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.ejercicio = " + r_mapeo.getMapeoOrdenes().getEjercicio());
				}
				if (r_mapeo.getMapeoOrdenes().getReferencia()!=null && r_mapeo.getMapeoOrdenes().getReferencia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.referencia = '" + r_mapeo.getMapeoOrdenes().getReferencia() + "' ");
				}
			}
			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" lab_barricas_ordenes_bar.idBarrica not in (select idBarrica from lab_barricas_mov ");
			cadenaWhere.append(" where lab_barricas_mov.idOrden = lab_barricas_ordenes_bar.idOrden) ");

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by lab_barricas.numero ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOrdenesTrabajoBarricas.setIdCodigo(rsOpcion.getInt("ord_bar_id"));
				mapeoOrdenesTrabajoBarricas.setIdBarrica(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.setIdOrden(rsOpcion.getInt("ord_id"));
				
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setIdCodigo(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setNumero(rsOpcion.getInt("bar_num"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setDescripcion(rsOpcion.getString("bar_des"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setCapacidad(rsOpcion.getInt("bar_cap"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setEjercicio(rsOpcion.getInt("bar_eje"));
				
				vector.add(mapeoOrdenesTrabajoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoOrdenesTrabajoBarricas> datosOrdenesTrabajoBarricasGlobalLlenas(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoOrdenesTrabajoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_ordenes_bar.idCodigo ord_bar_id,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica ord_bar_idBar,");
		cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden ord_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap, ");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje ");
		
		cadenaSQL.append(" from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes_bar.idOrden =  lab_barricas_ordenes.idCodigo ");
		cadenaSQL.append(" inner join lab_barricas on lab_barricas_ordenes_bar.idBarrica =  lab_barricas.idCodigo ");
		cadenaSQL.append(" inner join lab_barricas_existencias on lab_barricas.idCodigo =  lab_barricas_existencias.idBarrica ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getMapeoBarricas()!=null && r_mapeo.getMapeoBarricas().getNumero()!=null && r_mapeo.getMapeoBarricas().getNumero().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas.numero = " + r_mapeo.getMapeoBarricas().getNumero());
				}
				if (r_mapeo.getMapeoOrdenes()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo()!=null && r_mapeo.getMapeoOrdenes().getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes_bar.idOrden = " + r_mapeo.getMapeoOrdenes().getIdCodigo());
				}
				if (r_mapeo.getMapeoOrdenes().getEjercicio()!=null && r_mapeo.getMapeoOrdenes().getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.ejercicio = " + r_mapeo.getMapeoOrdenes().getEjercicio());
				}
				if (r_mapeo.getMapeoOrdenes().getReferencia()!=null && r_mapeo.getMapeoOrdenes().getReferencia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_ordenes.referencia = '" + r_mapeo.getMapeoOrdenes().getReferencia() + "' ");
				}
			}
			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" lab_barricas_ordenes_bar.idBarrica in (select idBarrica from lab_barricas_existencias ) ");

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by lab_barricas.numero ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOrdenesTrabajoBarricas.setIdCodigo(rsOpcion.getInt("ord_bar_id"));
				mapeoOrdenesTrabajoBarricas.setIdBarrica(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.setIdOrden(rsOpcion.getInt("ord_id"));
				
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setIdCodigo(rsOpcion.getInt("ord_bar_idBar"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setNumero(rsOpcion.getInt("bar_num"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setDescripcion(rsOpcion.getString("bar_des"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setCapacidad(rsOpcion.getInt("bar_cap"));
				mapeoOrdenesTrabajoBarricas.getMapeoBarricas().setEjercicio(rsOpcion.getInt("bar_eje"));
				
				vector.add(mapeoOrdenesTrabajoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String obtenerEstadoOrden(Integer r_idOrden)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(lab_barricas_ordenes_bar.idCodigo) ord_bar_id ");
		cadenaSQL.append(" from lab_barricas_ordenes_bar ");
		try
		{
			if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" lab_barricas_ordenes_bar.idOrden = " + r_idOrden);
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("ord_bar_id")!=0) return "Completo";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Vacio";
		}
		return "Vacio";
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_ordenes_bar.idCodigo) ord_bar_sig ");
     	cadenaSQL.append(" FROM lab_barricas_ordenes_bar ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("ord_bar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public String guardarNuevo(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_ordenes_bar( ");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idCodigo,");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica,");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden ) VALUES ( ");
			cadenaSQL.append(" ?,?,?) ");

		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getIdOrden()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getIdOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarNuevoTransaccional(Connection r_con, MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_ordenes_bar( ");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idCodigo,");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica,");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden ) VALUES ( ");
			cadenaSQL.append(" ?,?,?) ");

		    preparedStatement = r_con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getIdOrden()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getIdOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarCambios(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE lab_barricas_ordenes_bar SET ");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idBarrica =?,");
			cadenaSQL.append(" lab_barricas_ordenes_bar.idOrden =? ");
			cadenaSQL.append(" where lab_barricas_ordenes_bar.idCodigo = ? ");
			
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getIdOrden()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoOrdenesTrabajoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_barricas_ordenes_bar ");            
			cadenaSQL.append(" WHERE lab_barricas_ordenes_bar.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoBarricas r_mapeo)
	{
		return null;
	}
}