package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoExistenciasBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.server.consultaExistenciasBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.modelo.MapeoMovimientoBarricas;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaMovimientoBarricasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaMovimientoBarricasServer instance;
	
	public consultaMovimientoBarricasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaMovimientoBarricasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaMovimientoBarricasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoMovimientoBarricas> datosMovimientoBarricasGlobal(MapeoMovimientoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoMovimientoBarricas mapeoMovimientoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_mov.idCodigo mov_id,");
		cadenaSQL.append(" lab_barricas_mov.tipoMovimiento mov_tip,");
		cadenaSQL.append(" lab_barricas_mov.fechaMovimiento mov_fec,");
		cadenaSQL.append(" lab_barricas_mov.depositoVaciado mov_dva,");
		cadenaSQL.append(" lab_barricas_mov.depositoLlenado mov_dll,");
		cadenaSQL.append(" lab_barricas_mov.destinoFinal mov_fin,");
		cadenaSQL.append(" lab_barricas_mov.variedad mov_var,");
		cadenaSQL.append(" lab_barricas_mov.referencia mov_ref,");
		cadenaSQL.append(" lab_barricas_mov.observaciones mov_obs,");
		cadenaSQL.append(" lab_barricas_mov.idDesgloseNave mov_nav,");
		cadenaSQL.append(" lab_barricas_mov.idBarrica mov_bar, ");
		cadenaSQL.append(" lab_barricas_mov.idOrden mov_ord, ");
		
		cadenaSQL.append(" lab_barricas.numero bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap, ");

		cadenaSQL.append(" lab_barricas_nav.nombre nav_nom, ");
		
		cadenaSQL.append(" lab_barricas_nav_des.fila des_fil, ");
		cadenaSQL.append(" lab_barricas_nav_des.calle des_cal ");
		
		cadenaSQL.append(" from lab_barricas_mov ");
		cadenaSQL.append(" INNER JOIN lab_barricas_nav_des ON lab_barricas_nav_des.idCodigo = lab_barricas_mov.idDesgloseNave ");
		cadenaSQL.append(" INNER JOIN lab_barricas_nav ON lab_barricas_nav.idCodigo = lab_barricas_nav_des.idNave ");
		cadenaSQL.append(" INNER JOIN lab_barricas ON lab_barricas.idCodigo = lab_barricas_nav_mov.idBarrica ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoMovimiento = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getDepositoVaciado()!=null && r_mapeo.getDepositoVaciado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoVaciado = " + r_mapeo.getDepositoVaciado() + " ");
				}
				if (r_mapeo.getDepositoLlenado()!=null && r_mapeo.getDepositoLlenado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoLlenado = " + r_mapeo.getDepositoLlenado() + " ");
				}
				if (r_mapeo.getDestinoFinal()!=null && r_mapeo.getDestinoFinal().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destinoFinal = " + r_mapeo.getDestinoFinal() + " ");
				}
				if (r_mapeo.getMapeoBarricas()!=null && r_mapeo.getMapeoBarricas().getNumero()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas.numero = " + r_mapeo.getMapeoBarricas().getNumero());
				}
				if (r_mapeo.getMapeoDesgloseNaves()!=null && r_mapeo.getMapeoDesgloseNaves().getFilas()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav_des.fila = " + r_mapeo.getMapeoDesgloseNaves().getFilas());
				}
				if (r_mapeo.getMapeoDesgloseNaves()!=null && r_mapeo.getMapeoDesgloseNaves().getCalle()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav_des.calle = " + r_mapeo.getMapeoDesgloseNaves().getCalle());
				}
				if (r_mapeo.getMapeoNaves()!=null && r_mapeo.getMapeoNaves().getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav.nombre = '" + r_mapeo.getMapeoNaves().getNombre() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fechaMovimiento asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoMovimientoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientoBarricas = new MapeoMovimientoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMovimientoBarricas.setIdCodigo(rsOpcion.getInt("mov_id"));
				mapeoMovimientoBarricas.setTipo(rsOpcion.getString("mov_tip"));
				mapeoMovimientoBarricas.setFechaMovimiento(rsOpcion.getDate("mov_fec"));
				mapeoMovimientoBarricas.setDepositoVaciado(rsOpcion.getString("mov_dva"));
				mapeoMovimientoBarricas.setDepositoLlenado(rsOpcion.getString("mov_dll"));
				mapeoMovimientoBarricas.setDestinoFinal(rsOpcion.getString("mov_fin"));
				mapeoMovimientoBarricas.setVariedad(rsOpcion.getString("mov_var"));
				mapeoMovimientoBarricas.setReferencia(rsOpcion.getString("mov_ref"));
				mapeoMovimientoBarricas.setObservaciones(rsOpcion.getString("mov_obs"));
				mapeoMovimientoBarricas.setIdDesgloseNave(rsOpcion.getInt("mov_nav"));
				mapeoMovimientoBarricas.setIdBarrica(rsOpcion.getInt("mov_bar"));
				mapeoMovimientoBarricas.setIdOrden(rsOpcion.getInt("mov_ord"));
				
				mapeoMovimientoBarricas.getMapeoBarricas().setNumero(rsOpcion.getInt("bar_num"));
				mapeoMovimientoBarricas.getMapeoBarricas().setDescripcion(rsOpcion.getString("bar_des"));
				mapeoMovimientoBarricas.getMapeoBarricas().setCapacidad(rsOpcion.getInt("bar_cap"));

				mapeoMovimientoBarricas.getMapeoNaves().setNombre(rsOpcion.getString("nav_nom"));

				mapeoMovimientoBarricas.getMapeoDesgloseNaves().setFilas(rsOpcion.getInt("des_fil"));
				mapeoMovimientoBarricas.getMapeoDesgloseNaves().setCalle(rsOpcion.getInt("des_cal"));
				
				vector.add(mapeoMovimientoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoMovimientoBarricas> datosMovimientoBarricasGlobalTransaccional(Connection r_con, MapeoMovimientoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoMovimientoBarricas mapeoMovimientoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_mov.idCodigo mov_id,");
		cadenaSQL.append(" lab_barricas_mov.tipoMovimiento mov_tip,");
		cadenaSQL.append(" lab_barricas_mov.fechaMovimiento mov_fec,");
		cadenaSQL.append(" lab_barricas_mov.depositoVaciado mov_dva,");
		cadenaSQL.append(" lab_barricas_mov.depositoLlenado mov_dll,");
		cadenaSQL.append(" lab_barricas_mov.destinoFinal mov_fin,");
		cadenaSQL.append(" lab_barricas_mov.variedad mov_var,");
		cadenaSQL.append(" lab_barricas_mov.referencia mov_ref,");
		cadenaSQL.append(" lab_barricas_mov.observaciones mov_obs,");
		cadenaSQL.append(" lab_barricas_mov.idDesgloseNave mov_nav,");
		cadenaSQL.append(" lab_barricas_mov.idBarrica mov_bar, ");
		cadenaSQL.append(" lab_barricas_mov.idOrden mov_ord, ");
		
		cadenaSQL.append(" lab_barricas.numero bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap, ");

		cadenaSQL.append(" lab_barricas_nav.nombre nav_nom, ");
		
		cadenaSQL.append(" lab_barricas_nav_des.fila des_fil, ");
		cadenaSQL.append(" lab_barricas_nav_des.calle des_cal ");
		
		cadenaSQL.append(" from lab_barricas_mov ");
		cadenaSQL.append(" INNER JOIN lab_barricas_nav_des ON lab_barricas_nav_des.idCodigo = lab_barricas_mov.idDesgloseNave ");
		cadenaSQL.append(" INNER JOIN lab_barricas_nav ON lab_barricas_nav.idCodigo = lab_barricas_nav_des.idNave ");
		cadenaSQL.append(" INNER JOIN lab_barricas ON lab_barricas.idCodigo = lab_barricas_mov.idBarrica ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoMovimiento = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getDepositoVaciado()!=null && r_mapeo.getDepositoVaciado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoVaciado = " + r_mapeo.getDepositoVaciado() + " ");
				}
				if (r_mapeo.getDepositoLlenado()!=null && r_mapeo.getDepositoLlenado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoLlenado = " + r_mapeo.getDepositoLlenado() + " ");
				}
				if (r_mapeo.getDestinoFinal()!=null && r_mapeo.getDestinoFinal().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destinoFinal = " + r_mapeo.getDestinoFinal() + " ");
				}
				if (r_mapeo.getMapeoBarricas()!=null && r_mapeo.getMapeoBarricas().getNumero()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas.numero = " + r_mapeo.getMapeoBarricas().getNumero());
				}
				if (r_mapeo.getMapeoDesgloseNaves()!=null && r_mapeo.getMapeoDesgloseNaves().getFilas()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav_des.fila = " + r_mapeo.getMapeoDesgloseNaves().getFilas());
				}
				if (r_mapeo.getMapeoDesgloseNaves()!=null && r_mapeo.getMapeoDesgloseNaves().getCalle()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav_des.calle = " + r_mapeo.getMapeoDesgloseNaves().getCalle());
				}
				if (r_mapeo.getMapeoNaves()!=null && r_mapeo.getMapeoNaves().getNombre()!=null && r_mapeo.getMapeoNaves().getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_nav.nombre = '" + r_mapeo.getMapeoNaves().getNombre() + "' ");
				}
				if (r_mapeo.getIdOrden()!=null && r_mapeo.getIdOrden()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lab_barricas_mov.idOrden = " + r_mapeo.getIdOrden());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fechaMovimiento asc");
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoMovimientoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientoBarricas = new MapeoMovimientoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMovimientoBarricas.setIdCodigo(rsOpcion.getInt("mov_id"));
				mapeoMovimientoBarricas.setTipo(rsOpcion.getString("mov_tip"));
				mapeoMovimientoBarricas.setFechaMovimiento(RutinasFechas.convertirADate(rsOpcion.getString("mov_fec")));
				mapeoMovimientoBarricas.setDepositoVaciado(rsOpcion.getString("mov_dva"));
				mapeoMovimientoBarricas.setDepositoLlenado(rsOpcion.getString("mov_dll"));
				mapeoMovimientoBarricas.setDestinoFinal(rsOpcion.getString("mov_fin"));
				mapeoMovimientoBarricas.setVariedad(rsOpcion.getString("mov_var"));
				mapeoMovimientoBarricas.setReferencia(rsOpcion.getString("mov_ref"));
				mapeoMovimientoBarricas.setObservaciones(rsOpcion.getString("mov_obs"));
				mapeoMovimientoBarricas.setIdDesgloseNave(rsOpcion.getInt("mov_nav"));
				mapeoMovimientoBarricas.setIdBarrica(rsOpcion.getInt("mov_bar"));
				mapeoMovimientoBarricas.setIdOrden(rsOpcion.getInt("mov_ord"));
				
				mapeoMovimientoBarricas.getMapeoBarricas().setNumero(rsOpcion.getInt("bar_num"));
				mapeoMovimientoBarricas.getMapeoBarricas().setDescripcion(rsOpcion.getString("bar_des"));
				mapeoMovimientoBarricas.getMapeoBarricas().setCapacidad(rsOpcion.getInt("bar_cap"));

				mapeoMovimientoBarricas.getMapeoNaves().setNombre(rsOpcion.getString("nav_nom"));

				mapeoMovimientoBarricas.getMapeoDesgloseNaves().setFilas(rsOpcion.getInt("des_fil"));
				mapeoMovimientoBarricas.getMapeoDesgloseNaves().setCalle(rsOpcion.getInt("des_cal"));
				
				vector.add(mapeoMovimientoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_mov.idCodigo) mov_sig ");
     	cadenaSQL.append(" FROM lab_barricas_mov ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("mov_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public Integer obtenerSiguienteTransaccional(Connection r_con)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_mov.idCodigo) mov_sig ");
     	cadenaSQL.append(" FROM lab_barricas_mov ");
		try
		{
     	
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("mov_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}
	
	private MapeoExistenciasBarricas rellenarMapeoExistencias(MapeoMovimientoBarricas r_mapeo)
	{
		MapeoExistenciasBarricas mapeoExistencias = new MapeoExistenciasBarricas();
		mapeoExistencias.setVariedad(r_mapeo.getVariedad());
		mapeoExistencias.setReferencia(r_mapeo.getReferencia());
		mapeoExistencias.setAnada(r_mapeo.getAnada());
		mapeoExistencias.setIdBarrica(r_mapeo.getIdBarrica());
		mapeoExistencias.setIdDesgloseNave(r_mapeo.getIdDesgloseNave());
		mapeoExistencias.setExistencias(r_mapeo.getMapeoBarricas().getCapacidad());
		mapeoExistencias.setIdMovimiento(r_mapeo.getIdCodigo());
		return mapeoExistencias;
	}
	
	public String guardarNuevo(MapeoMovimientoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		MapeoExistenciasBarricas mapeoExistencias = new MapeoExistenciasBarricas();
		mapeoExistencias=this.rellenarMapeoExistencias(r_mapeo);
		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_mov( ");
			cadenaSQL.append(" lab_barricas_mov.idCodigo,");
			cadenaSQL.append(" lab_barricas_mov.tipoMovimiento,");
			cadenaSQL.append(" lab_barricas_mov.fechaMovimiento,");
			cadenaSQL.append(" lab_barricas_mov.depositoVaciado,");
			cadenaSQL.append(" lab_barricas_mov.depositoLlenado,");
			cadenaSQL.append(" lab_barricas_mov.destinoFinal,");
			cadenaSQL.append(" lab_barricas_mov.variedad,");
			cadenaSQL.append(" lab_barricas_mov.referencia,");
			cadenaSQL.append(" lab_barricas_mov.observaciones,");
			cadenaSQL.append(" lab_barricas_mov.idDesgloseNave,");
			cadenaSQL.append(" lab_barricas_mov.idBarrica,");
			cadenaSQL.append(" lab_barricas_mov.idOrden) VALUES ( ");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");

		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getFechaMovimiento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getFechaMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDepositoVaciado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDepositoVaciado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDepositoLlenado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDepositoLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDestinoFinal()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDestinoFinal());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdDesgloseNave()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdDesgloseNave());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getIdOrden()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getIdOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
	        preparedStatement.executeUpdate();
	        
	        this.insertarExistencias(con, mapeoExistencias);
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarNuevoTransaccional(Connection r_con, MapeoMovimientoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String rdo = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		MapeoExistenciasBarricas mapeoExistencias = new MapeoExistenciasBarricas();
		mapeoExistencias=this.rellenarMapeoExistencias(r_mapeo);

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_mov( ");
			cadenaSQL.append(" lab_barricas_mov.idCodigo,");
			cadenaSQL.append(" lab_barricas_mov.tipoMovimiento,");
			cadenaSQL.append(" lab_barricas_mov.fechaMovimiento,");
			cadenaSQL.append(" lab_barricas_mov.depositoVaciado,");
			cadenaSQL.append(" lab_barricas_mov.depositoLlenado,");
			cadenaSQL.append(" lab_barricas_mov.destinoFinal,");
			cadenaSQL.append(" lab_barricas_mov.variedad,");
			cadenaSQL.append(" lab_barricas_mov.referencia,");
			cadenaSQL.append(" lab_barricas_mov.observaciones,");
			cadenaSQL.append(" lab_barricas_mov.idDesgloseNave,");
			cadenaSQL.append(" lab_barricas_mov.idBarrica,");
			cadenaSQL.append(" lab_barricas_mov.idOrden) VALUES ( ");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");

		    preparedStatement = r_con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getFechaMovimiento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getFechaMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDepositoVaciado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDepositoVaciado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDepositoLlenado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDepositoLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDestinoFinal()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDestinoFinal());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdDesgloseNave()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdDesgloseNave());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getIdOrden()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getIdOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }

	        preparedStatement.executeUpdate();
	        
	        if (r_mapeo.getTipo().contentEquals("Llenado")) rdo = this.insertarExistencias(r_con, mapeoExistencias);
	        else rdo = this.eliminarExistencias(r_con, r_mapeo);
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError("error guardar movimiento " + ex.getMessage());
	    	rdo = "error guardar movimiento " + ex.getMessage();
	    }
		return rdo;
	}
	
	public String guardarCambios(MapeoMovimientoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		MapeoExistenciasBarricas mapeoExistencias = new MapeoExistenciasBarricas();
		mapeoExistencias=this.rellenarMapeoExistencias(r_mapeo);
		try
		{
			
			cadenaSQL.append(" UPDATE lab_barricas_mov SET ");
			cadenaSQL.append(" lab_barricas_mov.tipoMovimiento =?,");
			cadenaSQL.append(" lab_barricas_mov.fechaMovimiento =?,");
			cadenaSQL.append(" lab_barricas_mov.depositoVaciado =?,");
			cadenaSQL.append(" lab_barricas_mov.depositoLlenado =?,");
			cadenaSQL.append(" lab_barricas_mov.destinoFinal =?,");
			cadenaSQL.append(" lab_barricas_mov.variedad =?,");
			cadenaSQL.append(" lab_barricas_mov.referencia =?,");
			cadenaSQL.append(" lab_barricas_mov.observaciones =?,");
			cadenaSQL.append(" lab_barricas_mov.idDesgloseNave =?,");
			cadenaSQL.append(" lab_barricas_mov.idBarrica =? ");
			cadenaSQL.append(" where lab_barricas_mov.idCodigo = ? ");
			
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getFechaMovimiento()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getFechaMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDepositoVaciado()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDepositoVaciado());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDepositoLlenado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDepositoLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDestinoFinal()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDestinoFinal());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getIdDesgloseNave()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getIdDesgloseNave());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    
		    preparedStatement.setInt(11, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		    this.eliminarExistencias(con, r_mapeo);
		    this.insertarExistencias(con, mapeoExistencias);
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoMovimientoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_barricas_mov ");            
			cadenaSQL.append(" WHERE lab_barricas_mov.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			
			this.eliminarExistencias(con, r_mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoBarricas r_mapeo)
	{
		return null;
	}

	private String insertarExistencias(Connection r_con, MapeoExistenciasBarricas r_mapeo)
	{
		
		String rdo = null;
		consultaExistenciasBarricasServer cexb = consultaExistenciasBarricasServer.getInstance(CurrentUser.get());
		rdo = cexb.guardarNuevoTransaccional(r_con, r_mapeo);
		return rdo;
	}

	private String eliminarExistencias(Connection r_con, MapeoMovimientoBarricas r_mapeo)
	{
		
		String rdo = null;
		consultaExistenciasBarricasServer cexb = consultaExistenciasBarricasServer.getInstance(CurrentUser.get());
		rdo = cexb.eliminarTransaccional(r_con, r_mapeo);
		return rdo;
	}

}