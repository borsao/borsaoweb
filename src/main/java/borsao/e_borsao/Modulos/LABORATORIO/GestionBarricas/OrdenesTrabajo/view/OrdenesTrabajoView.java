package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view;
 
 import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.OrdenesTrabajoGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server.consultaOrdenesTrabajoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class OrdenesTrabajoView extends GridViewRefresh{

	public static final String VIEW_NAME = "Ordenes Trabajo Barricas";
	public consultaOrdenesTrabajoServer cus =null;
	public boolean modificando = false;
	
	public final String titulo = "Ordenes Trabajo Barricas";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private OpcionesForm form = null;
	public boolean conTotales = true;
	private Button opcSumar = null;
	public OrdenesTrabajoView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private ExcelExporter opcExcel = null;
	public TextField txtEjercicio= null;
	public ComboBox cmbEstados = null;	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public OrdenesTrabajoView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaOrdenesTrabajoServer.getInstance(CurrentUser.get());
		this.intervaloRefresco=0;
		this.setSoloConsulta(false);
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);

		this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setWidth("100px");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		
		this.cmbEstados= new ComboBox("Estado");    	
		this.cmbEstados.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbEstados.setNewItemsAllowed(false);
		this.cmbEstados.setWidth("100px");
		this.cmbEstados.setNullSelectionAllowed(false);

//		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.opcSumar= new Button();    	
		this.opcSumar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcSumar.setIcon(FontAwesome.PLUS_SQUARE);
		this.opcSumar.setDescription("Seleccion Lineas");

		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.cmbEstados);

//		this.cabLayout.addComponent(this.opcSumar);
//		this.cabLayout.setComponentAlignment(this.opcSumar,Alignment.BOTTOM_LEFT);

		this.cargarCombo();
		this.cargarListeners();
//		this.establecerModo();
		
		this.generarGrid(opcionesEscogidas);
		if (isHayGrid())
		{
			this.opcImprimir.setVisible(true);
			this.opcSumar.setVisible(true);
		}
		this.opcImprimir.setEnabled(false);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoOrdenesTrabajo> r_vector=null;
    	MapeoOrdenesTrabajo mapeoOrdenesTrabajo =null;
    	hashToMapeo hm = new hashToMapeo();
    	mapeoOrdenesTrabajo=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	mapeoOrdenesTrabajo.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    	mapeoOrdenesTrabajo.setEstado(this.cmbEstados.getValue().toString());
    	
    	r_vector=this.cus.datosOrdenesTrabajoGlobal(mapeoOrdenesTrabajo);
    	
    	this.presentarGrid(r_vector);
    	((OrdenesTrabajoGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((OrdenesTrabajoGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((OrdenesTrabajoGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
//    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((OrdenesTrabajoGrid) this.grid).activadaVentanaPeticion && !((OrdenesTrabajoGrid) this.grid).ordenando)
    	{
    		if (((MapeoOrdenesTrabajo) r_fila)!=null)
    		{
    			this.opcImprimir.setEnabled(true);
    			if (((MapeoOrdenesTrabajo) r_fila).getEstado().contentEquals("A"))
    			{
    				this.modificando=true;
    				this.verForm(false);
    				this.form.editarOrdenTrabajo((MapeoOrdenesTrabajo) r_fila);
    			}
    		}
    		else
    		{
    			this.opcImprimir.setEnabled(false);
    		}
    	}    		
    }
    
//    public void generarGrid(MapeoOrdenesTrabajo r_mapeo)
//    {
//    	ArrayList<MapeoOrdenesTrabajo> r_vector=null;
//    	
//    	r_vector=this.cus.datosOrdenesTrabajoGlobal(r_mapeo);
//    	this.presentarGrid(r_vector);
//    	((OrdenesTrabajoGrid) grid).establecerFiltros(filtrosRec);
//    }
    
    public void presentarGrid(ArrayList<MapeoOrdenesTrabajo> r_vector)
    {
    	grid = new OrdenesTrabajoGrid(this,r_vector);
    	if (((OrdenesTrabajoGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.opcImprimir.setEnabled(false);
    		this.opcSumar.setEnabled(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((OrdenesTrabajoGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.opcImprimir.setEnabled(false);
    		this.opcSumar.setEnabled(false);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.opcImprimir.setEnabled(true);
    		this.opcSumar.setEnabled(true);
    		this.establecerTableExportable();
//    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
    	this.txtEjercicio.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
	 			if (isHayGrid())
	 			{			
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;	
	 			}
	 			opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
	 			opcionesEscogidas.put("estado", cmbEstados.getValue().toString());
	 			generarGrid(opcionesEscogidas);

			}
		});
    	
    	this.cmbEstados.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
	 			if (isHayGrid())
	 			{			
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;	
	 			}
	 			opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
	 			opcionesEscogidas.put("estado", cmbEstados.getValue().toString());
	 			generarGrid(opcionesEscogidas);

			}
		});
    	
    	this.opcSumar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (grid.getSelectionModel() instanceof SingleSelectionModel)
				{
					opcSumar.addStyleName(ValoTheme.BUTTON_DANGER);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(false);
					botonesGenerales(false);
					activarBotones(false);
					opcSumar.setEnabled(true);
					opcImprimir.setEnabled(true);
					grid.setSelectionMode(SelectionMode.MULTI);
				}
				else
				{					
					opcSumar.setStyleName(ValoTheme.BUTTON_FRIENDLY);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(true);
					botonesGenerales(true);
					activarBotones(true);
					opcSumar.setEnabled(true);
					grid.deselectAll();
					grid.setSelectionMode(SelectionMode.SINGLE);
				}
			}
		});

//    	this.opcImprimir.addClickListener(new ClickListener() {
//			@Override
//			public void buttonClick(ClickEvent event) {
////				imprimirRetrabajo();
//			}
//		});
    }
    
	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
    public void print() 
    {
    	String pdfGenerado=null;
    	consultaOrdenesTrabajoServer cos = consultaOrdenesTrabajoServer.getInstance(CurrentUser.get());
    	String listaLineasSeleccionados = "";
    	String orden = null;
    	
    	if (((OrdenesTrabajoGrid) grid).getSelectedRows()!=null && ((OrdenesTrabajoGrid) grid).getSelectedRows().size()!=0)
		{
			Iterator it = null;
			
			it = ((OrdenesTrabajoGrid) grid).getSelectedRows().iterator();
			
			while (it.hasNext())
			{			
				MapeoOrdenesTrabajo mapeo = (MapeoOrdenesTrabajo) it.next();
				orden = mapeo.getTipo();
				if (listaLineasSeleccionados.length()==0)
				{
					listaLineasSeleccionados = mapeo.getNumero().toString();
				}
				else
				{
					listaLineasSeleccionados = listaLineasSeleccionados + "," + mapeo.getNumero().toString();
				}
			}
			if (orden.contentEquals("Llenado"))
				pdfGenerado = cos.generarInforme(listaLineasSeleccionados);
			else
				pdfGenerado = cos.generarInformeVaciado(listaLineasSeleccionados);
		}
		else
		{
			Object r_fila = this.grid.getSelectedRow();
			if (((MapeoOrdenesTrabajo) r_fila)!=null)
			{
				if (((MapeoOrdenesTrabajo) r_fila).getTipo().contentEquals("Llenado"))
					pdfGenerado = cos.generarInforme(((MapeoOrdenesTrabajo) r_fila).getNumero().toString());
				else
					pdfGenerado = cos.generarInformeVaciado(((MapeoOrdenesTrabajo) r_fila).getNumero().toString());
			}
		}
    	if(pdfGenerado!=null && pdfGenerado.length()>0)
    	{
    		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
    	}	    	
    	botonesGenerales(true);
    }
	
	private void establecerTableExportable()
	{
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(OrdenesTrabajoView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(OrdenesTrabajoView.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((OrdenesTrabajoGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstados.addItem("Abiertas");
		this.cmbEstados.addItem("Cerradas");
		this.cmbEstados.addItem("Todas");
		
		this.cmbEstados.setValue("Abiertas");
	}
	
}

