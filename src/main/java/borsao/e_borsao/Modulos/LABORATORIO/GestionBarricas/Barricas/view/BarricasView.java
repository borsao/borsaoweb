package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.BarricasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class BarricasView extends GridViewRefresh {

	public static final String VIEW_NAME = "control BARRICAS";
	public consultaBarricasServer cus =null;
	public boolean modificando = false;
	public ComboBox cmbEstado = null;
	
	private final String titulo = "control BARRICAS";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
//	private final boolean soloConsulta = false;
	private OpcionesForm form = null;
	public boolean conTotales = true;
	public BarricasView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public BarricasView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaBarricasServer.getInstance(CurrentUser.get());
		this.intervaloRefresco=0;
		this.setSoloConsulta(false);
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.cmbEstado= new ComboBox("Estado Barricas");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cabLayout.addComponent(this.cmbEstado);
		
		this.cargarCombo();
		this.cargarListeners();
//		this.establecerModo();
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoBarricas> r_vector=null;
    	MapeoBarricas mapeoBarricas =null;
    	hashToMapeo hm = new hashToMapeo();
    	mapeoBarricas=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);

    	r_vector=this.cus.datosBarricasGlobal(mapeoBarricas);
    	this.presentarGrid(r_vector);
    	((BarricasGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((BarricasGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((BarricasGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
//    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((BarricasGrid) this.grid).activadaVentanaPeticion && !((BarricasGrid) this.grid).ordenando)
    	{
    		if (((MapeoBarricas) r_fila)!=null)
    		{
	    		this.modificando=true;
	    		this.verForm(false);
		    	this.form.editarBarrica((MapeoBarricas) r_fila);
    		}	    	
    	}    		
    }
    
    public void generarGrid(MapeoBarricas r_mapeo)
    {
    	ArrayList<MapeoBarricas> r_vector=null;
    	r_vector=this.cus.datosBarricasGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    	((BarricasGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoBarricas> r_vector)
    {
    	grid = new BarricasGrid(this,r_vector);
    	if (((BarricasGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((BarricasGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
//    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((BarricasGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoBarricas mapeo = new MapeoBarricas();
					
					if (cmbEstado.getValue()!=null) 
					{
						if (cmbEstado.getValue().toString().equals("Alta"))
						{
							mapeo.setEstado("A");
						}
						else if (cmbEstado.getValue().toString().equals("Todas"))
						{
							mapeo.setEstado("T");
						}
						else if (cmbEstado.getValue().toString().equals("Baja"))
						{
							mapeo.setEstado("B");
						}
					}
					else
					{
						mapeo.setEstado("A");
					}
					generarGrid(mapeo);

				}
			}
		}); 
    	
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
//	private void establecerModo()
//	{
//		/*
//		 * Establece:  0 - Sin permisos
//		 * 			  10 - Laboratorio
//		 * 			  30 - Solo Consulta
//		 * 			  40 - Completar
//		 * 			  50 - Cambio Observaciones
//		 * 			  70 - Cambio observaciones y materia seca
//		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
//		 *  			 
//		 * 		      99 - Acceso total
//		 */
//		this.opcNuevo.setVisible(false);
//		this.opcNuevo.setEnabled(false);
////		this.opcImprimir.setVisible(true);
////		this.opcImprimir.setEnabled(true);
//
//		this.verBotones(true);
//		this.activarBotones(true);
//		this.navegacion(true);
//				
//		this.opcNuevo.setVisible(true);
//		this.opcNuevo.setEnabled(true);
//		setSoloConsulta(this.soloConsulta);
//	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	public void imprimirBarricas()
	{
//		((BarricasGrid) grid).activadaVentanaPeticion=false;
//		((BarricasGrid) grid).ordenando = false;
//		
//		
//		MapeoBarricas mapeo=(MapeoBarricas) grid.getSelectedRow();
//		
//		if (form!=null) form.cerrar();
//		
//		if (mapeo!=null && mapeo.getIdRetrabajo()!=null)
//		{
//			PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion(mapeo.getIdqc_incidencias());
//			getUI().addWindow(vtPeticion);
//		}
//		else
//		{
//			Notificaciones.getInstance().mensajeInformativo("Debes seleccionar el registro a imprimir");
//			this.opcImprimir.setEnabled(true);
//		}
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		this.cmbEstado.addItem("Alta");
		this.cmbEstado.addItem("Baja");
		this.cmbEstado.addItem("Todas");
		this.cmbEstado.setValue("Alta");
		this.opcionesEscogidas.put("estado", "A");
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(BarricasView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(BarricasView.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((BarricasGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

