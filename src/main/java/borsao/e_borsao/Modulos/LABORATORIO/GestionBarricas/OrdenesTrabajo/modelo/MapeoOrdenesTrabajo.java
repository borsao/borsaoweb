package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoOrdenesTrabajo extends MapeoGlobal
{
	private Integer numero;
	private Integer ejercicio;
	private Integer litros;
	private Date fechaOrden;	
	private String tipo;
	private String anadaLlenado;
	private String depositoOrigen;
	private String depositoVaciado;
	private String depositoLlenado;
	private String destinoFinal;
	private String variedad;
	private String referencia;
	private String bars;
	private String estado;
	private String est;
	private String pro;
	
	public MapeoOrdenesTrabajo()
	{ 
	}

	public Date getFechaOrden() {
		//ejemplo cogido de articulos nuevos
		return fechaOrden;
	}

	public void setFechaOrden(Date fechaOrden) {
		this.fechaOrden = fechaOrden;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getLitros() {
		return litros;
	}

	public void setLitros(Integer litros) {
		this.litros = litros;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDepositoVaciado() {
		return depositoVaciado;
	}

	public void setDepositoVaciado(String depositoVaciado) {
		this.depositoVaciado = depositoVaciado;
	}

	public String getDepositoLlenado() {
		return depositoLlenado;
	}

	public void setDepositoLlenado(String depositoLlenado) {
		this.depositoLlenado = depositoLlenado;
	}

	public String getDestinoFinal() {
		return destinoFinal;
	}

	public void setDestinoFinal(String destinoFinal) {
		this.destinoFinal = destinoFinal;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getBars() {
		return bars;
	}

	public void setBars(String bars) {
		this.bars = bars;
	}

	public String getEst() {
		return est;
	}

	public void setEst(String est) {
		this.est = est;
	}

	public String getAnadaLlenado() {
		return anadaLlenado;
	}

	public void setAnadaLlenado(String anadaLlenado) {
		this.anadaLlenado = anadaLlenado;
	}

	public String getDepositoOrigen() {
		return depositoOrigen;
	}

	public void setDepositoOrigen(String depositoOrigen) {
		this.depositoOrigen = depositoOrigen;
	}

	public String getPro() {
		return pro;
	}

	public void setPro(String pro) {
		this.pro = pro;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}