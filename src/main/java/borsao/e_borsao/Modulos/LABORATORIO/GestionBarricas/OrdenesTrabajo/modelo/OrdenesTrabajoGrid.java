package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.view.ProcesarMovimientosBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view.OrdenesTrabajoView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view.PeticionBarricasOrdenTrabajo;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OrdenesTrabajoGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	public OrdenesTrabajoView app = null;
	
    public OrdenesTrabajoGrid(OrdenesTrabajoView r_app, ArrayList<MapeoOrdenesTrabajo> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setWidth("100%");
        this.setHeight("95%");
        this.asignarTitulo(this.app.titulo);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoOrdenesTrabajo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
		this.setConTotales(this.conTotales);
		this.getColumn("numero").setRenderer((Renderer<?>) new NumberRenderer("%d"));
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.SINGLE);
		
		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("est", "estado", "pro", "bars",  "ejercicio", "numero", "fechaOrden", "tipo", "variedad", "referencia", "depositoOrigen", "depositoVaciado", "destinoFinal", "depositoLlenado","anadaLlenado",  "litros");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "50");
    	this.widthFiltros.put("numero", "80");
    	this.widthFiltros.put("tipo", "60");
    	this.widthFiltros.put("referencia", "140");
    	this.widthFiltros.put("variedad", "120");
    	this.widthFiltros.put("anadaLlenado", "50");
    	this.widthFiltros.put("depositoOrigen", "60");
    	this.widthFiltros.put("depositoLlenado", "160");
    	this.widthFiltros.put("depositoVaciado", "160");
    	this.widthFiltros.put("destinoFinal", "160");

    	this.getColumn("bars").setHeaderCaption("");
    	this.getColumn("bars").setSortable(false);
    	this.getColumn("bars").setWidth(new Double(50));
    	this.getColumn("pro").setHeaderCaption("");
    	this.getColumn("pro").setSortable(false);
    	this.getColumn("pro").setWidth(new Double(50));

    	this.getColumn("numero").setHeaderCaption("Numero");
    	this.getColumn("numero").setWidth(120);
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(90);
    	this.getColumn("fechaOrden").setHeaderCaption("Fecha");
    	this.getColumn("fechaOrden").setWidth(120);
    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setWidth(100);
    	this.getColumn("variedad").setHeaderCaption("Variedad");
    	this.getColumn("variedad").setWidth(160);
    	this.getColumn("referencia").setHeaderCaption("Referencia");
    	this.getColumn("referencia").setWidth(180);
    	this.getColumn("depositoOrigen").setHeaderCaption("Dep. Origen");
    	this.getColumn("depositoOrigen").setWidth(100);
    	this.getColumn("depositoVaciado").setHeaderCaption("Dep. Vaciado");
    	this.getColumn("depositoVaciado").setWidth(200);
    	this.getColumn("depositoLlenado").setHeaderCaption("Dep. Llenado");
    	this.getColumn("depositoLlenado").setWidth(200);
    	this.getColumn("anadaLlenado").setHeaderCaption("Añada");
    	this.getColumn("anadaLlenado").setWidth(90);
    	this.getColumn("destinoFinal").setHeaderCaption("Destino Final");
    	this.getColumn("destinoFinal").setWidth(200);
    	this.getColumn("litros").setHeaderCaption("Litros");
    	this.getColumn("litros").setWidth(120);
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("est").setHidden(true);
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("litros").setRenderer((Renderer) new NumberRenderer(new DecimalFormat("#,##0.00")));    	
    	this.getColumn("fechaOrden").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));

    	
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoOrdenesTrabajo) {
                MapeoOrdenesTrabajo progRow = (MapeoOrdenesTrabajo)bean;
                // The actual description text is depending on the application
                if ("bars".equals(cell.getPropertyId()))
                {
                	if (progRow.getEst()==null)
                	{
                		descriptionText = "Acceso a Barricas. Tienes que rellenarlas.";
                	}
                	else if (progRow.getEst().contentEquals("Completo"))
            		{
                		descriptionText = "Acceso a Barricas. ";
            		}
                	else if (progRow.getEst().contentEquals("Parcial"))
            		{
            			descriptionText = "Acceso a Barricas. No cuadran, faltan de introducir.";
            		}
                	else
            		{
            			descriptionText = "Acceso a Barricas. Tienes que rellenarlas.";
            		}
                }
                if ("pro".equals(cell.getPropertyId()))
                {
                	descriptionText = "Procesar la orden de trabajo";
                }
                if ("depositoVaciado".equals(cell.getPropertyId()))
                {
                	if (progRow!=null && progRow.getDepositoVaciado()!=null) descriptionText = progRow.getDepositoVaciado();
            	}
            	else if ("depositoLlenado".equals(cell.getPropertyId()))
        		{
            		if (progRow!=null && progRow.getDepositoLlenado()!=null) descriptionText = progRow.getDepositoLlenado();
        		}
            	else if ("destinoFinal".equals(cell.getPropertyId()))
        		{
            		if (progRow!=null && progRow.getDestinoFinal()!=null) descriptionText = progRow.getDestinoFinal();
        		}
            	else if ("referencia".equals(cell.getPropertyId()))
        		{
            		if (progRow!=null && progRow.getReferencia()!=null) descriptionText = progRow.getReferencia();
        		}
            	else if ("depositoOrigen".equals(cell.getPropertyId()))
        		{
            		if (progRow!=null && progRow.getDepositoOrigen()!=null) descriptionText = progRow.getDepositoOrigen();
        		}
            	else if ("variedad".equals(cell.getPropertyId()))
        		{
            		if (progRow!=null && progRow.getVariedad()!=null) descriptionText = progRow.getVariedad();
        		}
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String hayBarricas = "";
    		String estadoOrden = "";
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("est".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null)
            		{
            			hayBarricas = cellReference.getValue().toString();
            		}
            	}
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null)
            		{
            			estadoOrden = cellReference.getValue().toString();
            		}
            	}
            	if ("numero".equals(cellReference.getPropertyId())||"ejercicio".equals(cellReference.getPropertyId())||"litros".equals(cellReference.getPropertyId()))
            	{
            		if (estadoOrden.contentEquals("C")) return "Rcell-linfin";
            		else return "Rcell-normal";
    			}
            	else if ( "pro".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "bars".equals(cellReference.getPropertyId()))
            	{
            		if (hayBarricas.contentEquals("Completo"))
            		{
            			return "cell-nativebuttonRackGreen";
            		}
            		if (hayBarricas.contentEquals("Parcial"))
            		{
            			return "cell-nativebuttonRackYellow";
            		}
            		else
            		{
            			return "cell-nativebuttonRackRed";
            		}
            	}
            	else
        		{
            		if (estadoOrden.contentEquals("C")) return "cell-linfin";
            		else return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoOrdenesTrabajo mapeo = (MapeoOrdenesTrabajo) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("bars"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getIdCodigo()!=null && mapeo.getIdCodigo()!=0)
            		{
            			PeticionBarricasOrdenTrabajo vtPeticion = new PeticionBarricasOrdenTrabajo((OrdenesTrabajoGrid) event.getSource(), mapeo, "Asociación Barricas Orden Trabajo");
        	    		getUI().addWindow(vtPeticion);            		
            		}
            	}
            	else if (event.getPropertyId().toString().equals("pro"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;

        			ProcesarMovimientosBarricas vtPeticion = new ProcesarMovimientosBarricas((OrdenesTrabajoGrid) event.getSource(), mapeo, "Procesar Orden Trabajo");
    	    		getUI().addWindow(vtPeticion);            		
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("bars");
		this.camposNoFiltrar.add("pro");
		this.camposNoFiltrar.add("fechaOrden");
		this.camposNoFiltrar.add("litros");
	}

	public void generacionPdf(MapeoOrdenesTrabajo r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	public void calcularTotal() 
	{
		Integer totalLitrosVacios = new Integer(0) ;
		Integer totalLitrosLlenos= new Integer(0) ;
		
		Integer q1Value = new Integer(0);
		Integer q2Value = new Integer(0);
		
    	if (this.getFooterRowCount()==0)
    	{
    		this.appendFooterRow();
    		this.appendFooterRow();
    	}

    	this.getFooterRow(0).getCell("referencia").setText("Total Vaciado");
        this.getFooterRow(1).getCell("referencia").setText("Total Llenado");
        
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null && item.getItemProperty("tipo").getValue().toString().contentEquals("Vaciado"))
        	{
        		q1Value = (Integer) item.getItemProperty("litros").getValue();
        		if (q1Value!=null) totalLitrosVacios += q1Value;
        	}
        	else if (item!=null && item.getItemProperty("tipo").getValue().toString().contentEquals("Llenado"))
        	{
        		q2Value = (Integer) item.getItemProperty("litros").getValue();
        		if (q2Value!=null) totalLitrosLlenos += q2Value;
        	}
        }
        
        this.getFooterRow(0).getCell("litros").setText(RutinasNumericas.formatearIntegerDigitos(totalLitrosVacios,0).toString());
        this.getFooterRow(0).getCell("litros").setStyleName("Rcell-pie");
        this.getFooterRow(1).getCell("litros").setText(RutinasNumericas.formatearIntegerDigitos(totalLitrosLlenos,0).toString());
        this.getFooterRow(1).getCell("litros").setStyleName("Rcell-pie");
        
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight("100%");
//		footer.setStyleName("smallgrid");

	}
}


