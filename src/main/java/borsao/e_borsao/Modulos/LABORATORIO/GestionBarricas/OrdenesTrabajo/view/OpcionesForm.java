package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.OrdenesTrabajoGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.hashToMapeo;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoOrdenesTrabajo mapeo = null;
	 
	 private OrdenesTrabajoView uw = null;
	 public MapeoOrdenesTrabajo mapeoModificado = null;
	 private BeanFieldGroup<MapeoOrdenesTrabajo> fieldGroup;
	 public String litrosNuevo = null;
	 
	 public OpcionesForm(OrdenesTrabajoView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoOrdenesTrabajo>(MapeoOrdenesTrabajo.class);
        fieldGroup.bindMemberFields(this);
        
        this.cargarCombo();
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fechaOrden.addStyleName("v-datefield-textfield");
        this.fechaOrden.setDateFormat("dd/MM/yyyy");
        this.fechaOrden.setShowISOWeekNumbers(true);
        this.fechaOrden.setResolution(Resolution.DAY);
        this.fechaOrden.setEnabled(true);
        this.litros.setLocale(new Locale("es_ES"));
        
        
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
		this.litros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (litros.getValue()!=null)
				{
					litrosNuevo = litros.getValue().toString().replace(".", ",");	
				}
			}
		});

        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoOrdenesTrabajo) uw.grid.getSelectedRow();
        		eliminarOrdenTrabajo(mapeo);
        		removeStyleName("visible");
        		btnGuardar.setCaption("Guardar");
        		btnEliminar.setEnabled(true);
        		setEnabled(false);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;	
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearOrdenTrabajo(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoOrdenesTrabajo mapeo_orig= (MapeoOrdenesTrabajo) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				
		 				modificarOrdenTrabajo(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((OrdenesTrabajoGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		this.ejercicio.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (ejercicio.getValue()!=null && ejercicio.getValue().toString().length()>0)
				{
					consultaContadoresEjercicioServer cces=consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
					Long orden = cces.recuperarContador(new Integer(RutinasCadenas.quitarPuntoMiles(ejercicio.getValue().toString())), "ORDEN", "BARRICAS");
					numero.setValue(orden.toString());
				}
				else
				{
					if (!isBusqueda())
					{
						Notificaciones.getInstance().mensajeError("No puedes dejar vacío el ejercicio");
						ejercicio.focus();
					}
				}
			}
		});
		this.tipo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (tipo.getValue()!=null && tipo.getValue().toString().contentEquals("Vaciado"))
				{
					depositoLlenado.setRequired(false);
					anadaLlenado.setRequired(false);
					destinoFinal.setRequired(true);
					depositoVaciado.setRequired(true);
					depositoOrigen.setRequired(true);
				}
				else if (tipo.getValue()!=null && tipo.getValue().toString().contentEquals("Llenado"))
				{
					depositoLlenado.setRequired(true);
					anadaLlenado.setRequired(true);
					destinoFinal.setRequired(false);
					depositoVaciado.setRequired(false);
					depositoOrigen.setRequired(false);
				}
			}
		});
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarOrdenTrabajo(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", RutinasCadenas.reemplazarComaMiles(this.litros.getValue().toString()));
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.numero.getValue()!=null && this.numero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numero", this.numero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numero", "");
		}
		if (this.tipo.getValue()!=null && this.tipo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tipo", this.tipo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipo", "");
		}
		if (this.fechaOrden.getValue()!=null && this.fechaOrden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaOrden", RutinasFechas.convertirDateToString(this.fechaOrden.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaOrden", "");
		}
		if (this.variedad.getValue()!=null && this.variedad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("variedad", this.variedad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("variedad", "");
		}
		if (this.referencia.getValue()!=null && this.referencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("referencia", this.referencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("referencia", "");
		}
		if (this.anadaLlenado.getValue()!=null && this.anadaLlenado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("anadaLlenado", this.anadaLlenado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("anadaLlenado", "");
		}
		if (this.depositoOrigen.getValue()!=null && this.depositoOrigen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoOrigen", this.depositoOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoOrigen", "");
		}
		if (this.depositoVaciado.getValue()!=null && this.depositoVaciado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoVaciado", this.depositoVaciado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoVaciado", "");
		}
		if (this.depositoLlenado.getValue()!=null && this.depositoLlenado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoLlenado", this.depositoLlenado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoLlenado", "");
		}
		if (this.destinoFinal.getValue()!=null && this.destinoFinal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinoFinal", this.destinoFinal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinoFinal", "");
		}
		opcionesEscogidas.put("estado", "A");

		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.numero.getValue()!=null && this.numero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numero", this.numero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numero", "");
		}
		if (this.tipo.getValue()!=null && this.tipo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tipo", this.tipo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipo", "");
		}
		if (this.fechaOrden.getValue()!=null && this.fechaOrden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaOrden", RutinasFechas.convertirDateToString(this.fechaOrden.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaOrden", "");
		}
		if (this.variedad.getValue()!=null && this.variedad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("variedad", this.variedad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("variedad", "");
		}
		if (this.referencia.getValue()!=null && this.referencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("referencia", this.referencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("referencia", "");
		}
		if (this.anadaLlenado.getValue()!=null && this.anadaLlenado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("anadaLlenado", this.anadaLlenado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("anadaLlenado", "");
		}
		if (this.depositoOrigen.getValue()!=null && this.depositoOrigen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoOrigen", this.depositoOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoOrigen", "");
		}
		if (this.depositoVaciado.getValue()!=null && this.depositoVaciado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoVaciado", this.depositoVaciado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoVaciado", "");
		}
		if (this.depositoLlenado.getValue()!=null && this.depositoLlenado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("depositoLlenado", this.depositoLlenado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("depositoLlenado", "");
		}
		if (this.destinoFinal.getValue()!=null && this.destinoFinal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinoFinal", this.destinoFinal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinoFinal", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarOrdenTrabajo(null);
		}
	}
	
	public void editarOrdenTrabajo(MapeoOrdenesTrabajo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoOrdenesTrabajo();
			consultaContadoresEjercicioServer cces=consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
			Long orden = cces.recuperarContador(new Integer(RutinasFechas.añoActualYYYY()), "ORDEN", "BARRICAS");
			r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
			r_mapeo.setNumero(orden.intValue());
			
			if (!isBusqueda())
			{
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoOrdenesTrabajo>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarOrdenTrabajo(MapeoOrdenesTrabajo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		String rdo = uw.cus.eliminar(r_mapeo);
		
		if (rdo==null)
		{
			((OrdenesTrabajoGrid) uw.grid).remove(r_mapeo);
			((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector).remove(r_mapeo);
		
			if (((OrdenesTrabajoGrid) uw.grid).vector.isEmpty())
			{
				uw.reestablecerPantalla();
			}
			else
			{
				HashMap<String,String> filtrosRec = ((OrdenesTrabajoGrid) uw.grid).recogerFiltros();
				ArrayList<MapeoOrdenesTrabajo> r_vector = (ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector;
				
				Indexed indexed = ((OrdenesTrabajoGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	
	            uw.presentarGrid(r_vector);
	
				((OrdenesTrabajoGrid) uw.grid).sort("numero");
				((OrdenesTrabajoGrid) uw.grid).establecerFiltros(filtrosRec);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	public void modificarOrdenTrabajo(MapeoOrdenesTrabajo r_mapeo, MapeoOrdenesTrabajo r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo==null)
		{
			if (!((OrdenesTrabajoGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoOrdenesTrabajo> r_vector = (ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((OrdenesTrabajoGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((OrdenesTrabajoGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	            uw.presentarGrid(r_vector);
	            
				((OrdenesTrabajoGrid) uw.grid).setRecords((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector);
				((OrdenesTrabajoGrid) uw.grid).sort("numero");
				((OrdenesTrabajoGrid) uw.grid).scrollTo(r_mapeo);
//				((OrdenesTrabajoGrid) uw.grid).select(r_mapeo);
				
				((OrdenesTrabajoGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearOrdenTrabajo(MapeoOrdenesTrabajo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdCodigo(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoOrdenesTrabajo>(r_mapeo));
			if (((OrdenesTrabajoGrid) uw.grid)!=null )//&& )
			{
				if (!((OrdenesTrabajoGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((OrdenesTrabajoGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((OrdenesTrabajoGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector).add(r_mapeo);
					((OrdenesTrabajoGrid) uw.grid).setRecords((ArrayList<MapeoOrdenesTrabajo>) ((OrdenesTrabajoGrid) uw.grid).vector);
					((OrdenesTrabajoGrid) uw.grid).sort("numero");
					((OrdenesTrabajoGrid) uw.grid).scrollTo(r_mapeo);
//					((OrdenesTrabajoGrid) uw.grid).select(r_mapeo);
					
					((OrdenesTrabajoGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoOrdenesTrabajo> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
		this.ejercicio.setRequired(r_obliga);
		this.numero.setRequired(r_obliga);
		this.tipo.setRequired(r_obliga);
		this.fechaOrden.setRequired(r_obliga);
		this.litros.setRequired(r_obliga);
		this.referencia.setRequired(r_obliga);
		this.variedad.setRequired(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.numero.getValue()==null || this.numero.getValue().toString().length()==0) return false;
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0) return false;
			if (this.fechaOrden.getValue()==null || this.fechaOrden.getValue().toString().length()==0) return false;
			if (this.tipo.getValue()==null || this.tipo.getValue().toString().length()==0) return false;
			if (this.referencia.getValue()==null || this.referencia.getValue().toString().length()==0) return false;
			if (this.variedad.getValue()==null || this.variedad.getValue().toString().length()==0) return false;
			if (this.litros.getValue()==null || this.litros.getValue().toString().length()==0) return false;
			if (this.tipo.getValue()!=null && this.tipo.getValue().toString().contentEquals("Llenado"))
			{
				if (this.depositoLlenado.getValue()==null || this.depositoLlenado.getValue().toString().length()==0) return false;
				if (this.anadaLlenado.getValue()==null || this.anadaLlenado.getValue().toString().length()==0) return false;
			}
			else if (this.tipo.getValue()!=null && this.tipo.getValue().toString().contentEquals("Vaciado"))
			{
				if (this.depositoVaciado.getValue()==null || this.depositoVaciado.getValue().toString().length()==0) return false;
				if (this.destinoFinal.getValue()==null || this.destinoFinal.getValue().toString().length()==0) return false;
				if (this.depositoOrigen.getValue()==null || this.depositoOrigen.getValue().toString().length()==0) return false;
			}
			else
			{
				return false;
			}
				
		}
		return true;
	}

	private void cargarCombo()
	{
		this.tipo.addItem("Llenado");
		this.tipo.addItem("Vaciado");
	}
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
}
