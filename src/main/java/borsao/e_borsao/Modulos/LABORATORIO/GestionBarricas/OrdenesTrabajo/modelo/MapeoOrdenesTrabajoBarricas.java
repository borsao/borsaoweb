package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;

public class MapeoOrdenesTrabajoBarricas extends MapeoGlobal
{
	private Integer idBarrica;
	private Integer idOrden;
	
	private MapeoOrdenesTrabajo  mapeoOrdenes=null;
	private MapeoBarricas  mapeoBarricas=null;
	
	public MapeoOrdenesTrabajoBarricas()
	{ 
		mapeoOrdenes = new MapeoOrdenesTrabajo();
		mapeoBarricas = new MapeoBarricas();
	}

	public Integer getIdBarrica() {
		return idBarrica;
	}

	public void setIdBarrica(Integer idBarrica) {
		this.idBarrica = idBarrica;
	}

	public Integer getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Integer idOrden) {
		this.idOrden = idOrden;
	}

	public MapeoOrdenesTrabajo getMapeoOrdenes() {
		return mapeoOrdenes;
	}

	public void setMapeoOrdenes(MapeoOrdenesTrabajo mapeoOrdenes) {
		this.mapeoOrdenes = mapeoOrdenes;
	}

	public MapeoBarricas getMapeoBarricas() {
		return mapeoBarricas;
	}

	public void setMapeoBarricas(MapeoBarricas mapeoBarricas) {
		this.mapeoBarricas = mapeoBarricas;
	}


}