package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.view;
 
 import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.DesgloseNavesGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server.consultaDesgloseNavesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DesgloseNavesView extends GridViewRefresh {

	public static final String VIEW_NAME = "EStructura Naves Barricas";
	public consultaDesgloseNavesServer cus =null;
	public boolean modificando = false;
	
	private final String titulo = "Estructura Naves Barricas";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private OpcionesForm form = null;
	public boolean conTotales = true;
	public DesgloseNavesView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public DesgloseNavesView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaDesgloseNavesServer.getInstance(CurrentUser.get());
		this.intervaloRefresco=0;
		this.setSoloConsulta(false);
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.cargarListeners();
//		this.establecerModo();
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoDesgloseNaves> r_vector=null;
    	MapeoDesgloseNaves mapeoDesgloseNaves =null;
    	hashToMapeo hm = new hashToMapeo();
    	mapeoDesgloseNaves=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);

    	r_vector=this.cus.datosDesgloseNavesGlobal(mapeoDesgloseNaves);
    	this.presentarGrid(r_vector);
    	((DesgloseNavesGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((DesgloseNavesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((DesgloseNavesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
//    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((DesgloseNavesGrid) this.grid).activadaVentanaPeticion && !((DesgloseNavesGrid) this.grid).ordenando)
    	{
    		if (((MapeoDesgloseNaves) r_fila)!=null)
    		{
	    		this.modificando=true;
	    		this.verForm(false);
		    	this.form.editarDesgloseNave((MapeoDesgloseNaves) r_fila);
    		}	    	
    	}    		
    }
    
    public void generarGrid(MapeoDesgloseNaves r_mapeo)
    {
    	ArrayList<MapeoDesgloseNaves> r_vector=null;
    	r_vector=this.cus.datosDesgloseNavesGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    	((DesgloseNavesGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoDesgloseNaves> r_vector)
    {
    	grid = new DesgloseNavesGrid(this,r_vector);
    	if (((DesgloseNavesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((DesgloseNavesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
//    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	public void imprimirNaves()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(DesgloseNavesView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(DesgloseNavesView.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((DesgloseNavesGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

