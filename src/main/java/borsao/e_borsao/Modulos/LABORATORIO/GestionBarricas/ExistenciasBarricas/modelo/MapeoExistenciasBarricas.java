package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoExistenciasBarricas extends MapeoGlobal
{
	private Integer idBarrica;
	private Integer idDesgloseNave;
	private Integer idMovimiento;
	private Integer existencias;
	private String anada;
	private String variedad;
	private String referencia;
	
	public Integer getIdBarrica() {
		return idBarrica;
	}

	public void setIdBarrica(Integer idBarrica) {
		this.idBarrica = idBarrica;
	}

	public Integer getIdDesgloseNave() {
		return idDesgloseNave;
	}

	public void setIdDesgloseNave(Integer idDesgloseNave) {
		this.idDesgloseNave = idDesgloseNave;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public MapeoExistenciasBarricas()
	{ 
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	public String getAnada() {
		return anada;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public Integer getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Integer idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

}