package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoDesgloseNaves mapeo = null;
	 
	 public MapeoDesgloseNaves convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoDesgloseNaves();

		 this.mapeo.setNombre(r_hash.get("nombre"));
		 
		 if (r_hash.get("fila")!=null && r_hash.get("fila").length()>0) this.mapeo.setFilas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("fila")));
		 if (r_hash.get("calle")!=null && r_hash.get("calle").length()>0) this.mapeo.setCalle(RutinasNumericas.formatearIntegerDeESP(r_hash.get("calle")));
		 if (r_hash.get("idNave")!=null && r_hash.get("idNave").length()>0) this.mapeo.setIdNave(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idNave")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoDesgloseNaves convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoDesgloseNaves();

		 this.mapeo.setNombre(r_hash.get("nombre"));
		 
		 if (r_hash.get("fila")!=null && r_hash.get("fila").length()>0) this.mapeo.setFilas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("fila")));
		 if (r_hash.get("calle")!=null && r_hash.get("calle").length()>0) this.mapeo.setCalle(RutinasNumericas.formatearIntegerDeESP(r_hash.get("calle")));
		 if (r_hash.get("idNave")!=null && r_hash.get("idNave").length()>0) this.mapeo.setIdNave(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idNave")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoDesgloseNaves r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("nombre", this.mapeo.getNombre());
		 
		 this.hash.put("fila", String.valueOf(this.mapeo.getFilas()));
		 this.hash.put("calle", String.valueOf(this.mapeo.getCalle()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 if (this.mapeo.getIdNave()!=null) this.hash.put("idNave", String.valueOf(this.mapeo.getIdNave()));
		 
		 return hash;		 
	 }
}