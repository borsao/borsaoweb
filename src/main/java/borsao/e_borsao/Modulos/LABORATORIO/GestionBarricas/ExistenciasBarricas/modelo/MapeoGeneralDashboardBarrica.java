package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGeneralDashboardBarrica extends MapeoGlobal
{
	private HashMap<String, Double> hashValores = null;
	private ArrayList<MapeoDashboardBarrica> vectorProduccion = null;
	private ArrayList<MapeoDashboardBarrica> vectorVentas= null;
	private ArrayList<MapeoDashboardBarrica> vectorVentasAnterior= null;
	
	public MapeoGeneralDashboardBarrica()
	{
	}
 
	public HashMap<String, Double> getHashValores() {
		return hashValores;
	}

	public void setHashValores(HashMap<String, Double> hashValores) {
		this.hashValores = hashValores;
	}

	public ArrayList<MapeoDashboardBarrica> getVectorProduccion() {
		return vectorProduccion;
	}

	public void setVectorProduccion(ArrayList<MapeoDashboardBarrica> vectorProduccion) {
		this.vectorProduccion = vectorProduccion;
	}

	public ArrayList<MapeoDashboardBarrica> getVectorVentas() {
		return vectorVentas;
	}

	public void setVectorVentas(ArrayList<MapeoDashboardBarrica> vectorVentas) {
		this.vectorVentas = vectorVentas;
	}

	public ArrayList<MapeoDashboardBarrica> getVectorVentasAnterior() {
		return vectorVentasAnterior;
	}

	public void setVectorVentasAnterior(ArrayList<MapeoDashboardBarrica> vectorVentasAnterior) {
		this.vectorVentasAnterior = vectorVentasAnterior;
	}

	
}