package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDesgloseNaves extends MapeoGlobal
{
	private Integer filas;
	private Integer calle;
	private Integer idNave;
	private String nombre;
	private String desg;
	
	public MapeoDesgloseNaves()
	{ 
	}

	public Integer getFilas() {
		return filas;
	}

	public void setFilas(Integer filas) {
		this.filas = filas;
	}

	public Integer getCalle() {
		return calle;
	}

	public void setCalle(Integer calle) {
		this.calle = calle;
	}

	public Integer getIdNave() {
		return idNave;
	}

	public void setIdNave(Integer idNave) {
		this.idNave = idNave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDesg() {
		return desg;
	}

	public void setDesg(String desg) {
		this.desg = desg;
	}

}