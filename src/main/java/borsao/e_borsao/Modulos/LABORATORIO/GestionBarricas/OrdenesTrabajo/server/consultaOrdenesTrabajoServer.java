package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajoBarricas;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaOrdenesTrabajoServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaOrdenesTrabajoServer instance;
	
	public consultaOrdenesTrabajoServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaOrdenesTrabajoServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaOrdenesTrabajoServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoOrdenesTrabajo> datosOrdenesTrabajoGlobal(MapeoOrdenesTrabajo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoOrdenesTrabajo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoOrdenesTrabajo mapeoOrdenesTrabajo=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_ordenes.idCodigo ord_id,");
		cadenaSQL.append(" lab_barricas_ordenes.numero ord_num,");
		cadenaSQL.append(" lab_barricas_ordenes.ejercicio ord_eje,");
		cadenaSQL.append(" lab_barricas_ordenes.fechaOrden ord_fec,");
		cadenaSQL.append(" lab_barricas_ordenes.tipo ord_tip,");
		cadenaSQL.append(" lab_barricas_ordenes.depositoOrigen ord_dor,");
		cadenaSQL.append(" lab_barricas_ordenes.depositoVaciado ord_dva,");
		cadenaSQL.append(" lab_barricas_ordenes.depositoLlenado ord_dll,");
		cadenaSQL.append(" lab_barricas_ordenes.anadaLlenado ord_ana,");
		cadenaSQL.append(" lab_barricas_ordenes.destinoFinal ord_fin,");
		cadenaSQL.append(" lab_barricas_ordenes.variedad ord_var,");
		cadenaSQL.append(" lab_barricas_ordenes.litros ord_lit,");
		cadenaSQL.append(" lab_barricas_ordenes.referencia ord_ref, ");
		cadenaSQL.append(" lab_barricas_ordenes.estado ord_est");
		cadenaSQL.append(" from lab_barricas_ordenes ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNumero()!=null && r_mapeo.getNumero().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numero = " + r_mapeo.getNumero());
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" Ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipo = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getFechaOrden()!=null && RutinasFechas.convertirDateToString(r_mapeo.getFechaOrden()).length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fechaOrden = '" + r_mapeo.getFechaOrden() + "' ");
				}
				if (r_mapeo.getDepositoOrigen()!=null && r_mapeo.getDepositoOrigen().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoOrigen = " + r_mapeo.getDepositoOrigen() + " ");
				}
				if (r_mapeo.getDepositoVaciado()!=null && r_mapeo.getDepositoVaciado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoVaciado = " + r_mapeo.getDepositoVaciado() + " ");
				}
				if (r_mapeo.getDepositoLlenado()!=null && r_mapeo.getDepositoLlenado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" depositoLlenado = " + r_mapeo.getDepositoLlenado() + " ");
				}
				if (r_mapeo.getDepositoLlenado()!=null && r_mapeo.getDepositoLlenado().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" anadaLlenado = " + r_mapeo.getAnadaLlenado() + " ");
				}
				if (r_mapeo.getDestinoFinal()!=null && r_mapeo.getDestinoFinal().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destinoFinal = " + r_mapeo.getDestinoFinal() + " ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().toString().length()>0)
				{
					if (!r_mapeo.getEstado().contentEquals("Todas"))
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						if (r_mapeo.getEstado().contentEquals("Abiertas")) cadenaWhere.append(" estado = 'A'");
						else if (r_mapeo.getEstado().contentEquals("Cerradas")) cadenaWhere.append(" estado = 'C'");
						else cadenaWhere.append(" estado = 'A'");
					}
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fechaOrden, numero ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOrdenesTrabajo>();
			
			while(rsOpcion.next())
			{
				mapeoOrdenesTrabajo = new MapeoOrdenesTrabajo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOrdenesTrabajo.setIdCodigo(rsOpcion.getInt("ord_id"));
				mapeoOrdenesTrabajo.setNumero(rsOpcion.getInt("ord_num"));
				mapeoOrdenesTrabajo.setEjercicio(rsOpcion.getInt("ord_eje"));
				mapeoOrdenesTrabajo.setTipo(rsOpcion.getString("ord_tip"));
				mapeoOrdenesTrabajo.setFechaOrden(rsOpcion.getDate("ord_fec"));
				mapeoOrdenesTrabajo.setDepositoOrigen(rsOpcion.getString("ord_dor"));
				mapeoOrdenesTrabajo.setDepositoVaciado(rsOpcion.getString("ord_dva"));
				mapeoOrdenesTrabajo.setDepositoLlenado(rsOpcion.getString("ord_dll"));
				mapeoOrdenesTrabajo.setAnadaLlenado(rsOpcion.getString("ord_ana"));
				mapeoOrdenesTrabajo.setDestinoFinal(rsOpcion.getString("ord_fin"));
				mapeoOrdenesTrabajo.setVariedad(rsOpcion.getString("ord_var"));
				mapeoOrdenesTrabajo.setReferencia(rsOpcion.getString("ord_ref"));
				mapeoOrdenesTrabajo.setLitros(rsOpcion.getInt("ord_lit"));
				mapeoOrdenesTrabajo.setEstado(rsOpcion.getString("ord_est"));
				mapeoOrdenesTrabajo.setEst(this.obtenerEstado(rsOpcion.getInt("ord_id")));
				vector.add(mapeoOrdenesTrabajo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String obtenerEstado ( Integer r_idOrden)
	{
		consultaOrdenesTrabajoBarricasServer cob = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
		MapeoOrdenesTrabajoBarricas map = new MapeoOrdenesTrabajoBarricas();
		String rdo = cob.obtenerEstadoOrden(r_idOrden);
		return rdo;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_ordenes.idCodigo) ord_sig ");
     	cadenaSQL.append(" FROM lab_barricas_ordenes ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("ord_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public String guardarNuevo(MapeoOrdenesTrabajo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		String rdo = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_ordenes( ");
			cadenaSQL.append(" lab_barricas_ordenes.idCodigo,");
			cadenaSQL.append(" lab_barricas_ordenes.numero ,");
			cadenaSQL.append(" lab_barricas_ordenes.ejercicio ,");
			cadenaSQL.append(" lab_barricas_ordenes.tipo ,");
			cadenaSQL.append(" lab_barricas_ordenes.fechaOrden ,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoOrigen ,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoVaciado ,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoLlenado ,");
			cadenaSQL.append(" lab_barricas_ordenes.AnadaLlenado ,");
			cadenaSQL.append(" lab_barricas_ordenes.destinoFinal ,");
			cadenaSQL.append(" lab_barricas_ordenes.variedad ,");
			cadenaSQL.append(" lab_barricas_ordenes.referencia , ");
			cadenaSQL.append(" lab_barricas_ordenes.litros , ");
			cadenaSQL.append(" lab_barricas_ordenes.estado ) VALUES ( ");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		    con= this.conManager.establecerConexion();
		    con.setAutoCommit(false);
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    Integer idOrden = r_mapeo.getIdCodigo();
		    if (r_mapeo.getNumero()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getNumero());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getFechaOrden()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaOrden())));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDepositoOrigen()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDepositoOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getDepositoVaciado()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getDepositoVaciado());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getDepositoLlenado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getDepositoLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getAnadaLlenado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getAnadaLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getDestinoFinal()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getDestinoFinal());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(14, "A");
		    }
	        preparedStatement.executeUpdate();
	        consultaContadoresEjercicioServer cces = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
	        cces.actualizarContadorTransaccional(con, r_mapeo.getEjercicio(), r_mapeo.getNumero().doubleValue(), "ORDEN", "BARRICAS");
	        
	        if(r_mapeo.getTipo().contentEquals("Vaciado"))
	        {
	        	rdo = this.incorporarBarricasVaciado(con, r_mapeo);
	        	if (rdo!=null)
	        		con.rollback(); 
	        	else 
	        	{
	        		con.commit();
	        		con.setAutoCommit(true);
	        	}
	        }
        	else 
        	{
        		con.commit();
        		con.setAutoCommit(true);
        	}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    	try
	    	{
	    		con.rollback();
	    	}
	    	catch(SQLException sqlEx)
	    	{
	    		rdo = sqlEx.getMessage();
	    	}
	    }
		return rdo;
	}
	
	public String guardarCambios(MapeoOrdenesTrabajo r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE lab_barricas_ordenes SET ");
			cadenaSQL.append(" lab_barricas_ordenes.tipo =?,");
			cadenaSQL.append(" lab_barricas_ordenes.fechaOrden =?,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoOrigen =?,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoVaciado =?,");
			cadenaSQL.append(" lab_barricas_ordenes.depositoLlenado =?,");
			cadenaSQL.append(" lab_barricas_ordenes.anadaLlenado =?,");
			cadenaSQL.append(" lab_barricas_ordenes.destinoFinal =?,");
			cadenaSQL.append(" lab_barricas_ordenes.variedad =?,");
			cadenaSQL.append(" lab_barricas_ordenes.referencia =?,");
			cadenaSQL.append(" lab_barricas_ordenes.litros =?,");
			cadenaSQL.append(" lab_barricas_ordenes.estado =?");
			cadenaSQL.append(" where lab_barricas_ordenes.idCodigo = ? ");
			
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getFechaOrden()!=null)
		    {
		    	preparedStatement.setString(2, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaOrden())));
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDepositoOrigen()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDepositoOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDepositoVaciado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDepositoVaciado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDepositoLlenado()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDepositoLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getAnadaLlenado()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getAnadaLlenado());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getDestinoFinal()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getDestinoFinal());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(11, "A");
		    }
		    preparedStatement.setInt(12, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	
	public String cerrarOrdenTransaccional(Connection r_con, Integer r_idOrden)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE lab_barricas_ordenes SET ");
			cadenaSQL.append(" lab_barricas_ordenes.estado ='C' ");
			cadenaSQL.append(" where lab_barricas_ordenes.idCodigo = ? ");
			
			
		    preparedStatement = r_con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setInt(1, r_idOrden);
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public String eliminar(MapeoOrdenesTrabajo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		try
		{
			con= this.conManager.establecerConexion();
			/*
			 * Abro transaccionalidad por si hay algun error revertir los cambios"
			 */
			con.setAutoCommit(false);
			
			rdo = this.eliminarEnalceOrdenBarricas(con, r_mapeo);
			
			if (rdo==null)
			{
				cadenaSQL.append(" DELETE FROM lab_barricas_ordenes ");            
				cadenaSQL.append(" where lab_barricas_ordenes.idCodigo = ?");
				cadenaSQL.append(" and lab_barricas_ordenes.estado = 'A'");
				preparedStatement = con.prepareStatement(cadenaSQL.toString());
				preparedStatement.setInt(1, r_mapeo.getIdCodigo());
				preparedStatement.executeUpdate();
				con.commit();
				con.setAutoCommit(true);
			}
			else
			{
				con.rollback();
			}
		}
		catch (Exception ex)
		{
			try
			{
				con.rollback();
				rdo = ex.getMessage();
			}
			catch (SQLException sqlEx)
			{
				rdo = sqlEx.getMessage();
			}
		}
		return rdo;
	}
	
	
	public String generarInforme(String r_valores)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_valores);
		libImpresion.setArchivoDefinitivo("/ordenesTrabajoBarrica" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("ListadoOrdenesBarricas.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
    	libImpresion.setArchivoPlantillaDetalle("OrdenesTrabajoBarricasDetalle.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("OrdenesTrabajoBarricasDetalle.jasper");
		
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("laboratorio");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}
	
	public String generarInformeVaciado(String r_valores)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_valores);
		libImpresion.setArchivoDefinitivo("/ordenesTrabajoBarrica" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("ListadoOrdenesBarricasVaciado.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
    	libImpresion.setArchivoPlantillaDetalle("OrdenesTrabajoBarricasDetalleVaciado.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("OrdenesTrabajoBarricasDetalleVaciado.jasper");
		
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("laboratorio");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	private String eliminarEnalceOrdenBarricas(Connection r_con, MapeoOrdenesTrabajo r_mapeo)
	{
		String rdo = null;
		/*
		 * eliminar enlace orden-barrica
		 */
		
		if (r_mapeo!=null)
		{
			/*
			 * eliminar enlace orden-barrica
			 */
			PreparedStatement preparedStatement = null;
			StringBuffer cadenaSQL = new StringBuffer();        
			
			try
			{
				cadenaSQL.append(" DELETE FROM lab_barricas_ordenes_bar ");            
				cadenaSQL.append(" WHERE lab_barricas_ordenes_bar.idOrden = ?"); 
				preparedStatement = r_con.prepareStatement(cadenaSQL.toString());
				preparedStatement.setInt(1, r_mapeo.getIdCodigo());
				preparedStatement.executeUpdate();
				
			}
			catch (Exception ex)
			{
				rdo = ex.getMessage().toString();
			}

		}
		return rdo;
	}
	
	public String incorporarBarricasVaciado(Connection r_con, MapeoOrdenesTrabajo r_mapeo)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoOrdenesTrabajoBarricas> vector = null;
		MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas=null;
		
		consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_existencias.idBarrica ex_bar");
		cadenaSQL.append(" from lab_barricas_existencias ");
		cadenaSQL.append(" where lab_barricas_existencias.variedad = '" + r_mapeo.getVariedad() + "' ");
		cadenaSQL.append(" and lab_barricas_existencias.referencia = '" + r_mapeo.getReferencia() + "' ");
		try
		{
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				
				mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
				mapeoOrdenesTrabajoBarricas.setIdCodigo(cotbs.obtenerSiguiente());
				mapeoOrdenesTrabajoBarricas.setIdBarrica(rsOpcion.getInt("ex_bar"));
				mapeoOrdenesTrabajoBarricas.setIdOrden(r_mapeo.getIdCodigo());
				
				rdo = cotbs.guardarNuevoTransaccional(r_con, mapeoOrdenesTrabajoBarricas);
				
				if (rdo!=null)
				{
					r_con.rollback();
					break;
				}
			}
		}
		catch (Exception ex)
		{
			rdo = "ERROR " + ex.getMessage();
		}
		return rdo;
	}


	@Override
	public String semaforos() 
	{
		return "0";
	}

}