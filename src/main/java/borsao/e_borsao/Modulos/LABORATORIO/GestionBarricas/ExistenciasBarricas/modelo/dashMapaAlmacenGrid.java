package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.ImageRenderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class dashMapaAlmacenGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public dashMapaAlmacenGrid(ArrayList<MapeoEstructuraAlmacen> r_vector) 
    {
        this.vector=r_vector;
        this.setSizeFull();
        this.setSeleccion(SelectionMode.SINGLE);
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoEstructuraAlmacen.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
		this.setConTotales(false);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		if (this.isConTotales()) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("nombreNave", "fila","calle", "columna12", "columna11", "columna10", "columna9", "columna8", "columna7", "columna6", "columna5", "columna4", "columna3", "columna2", "columna1");
    }
    
    public void establecerTitulosColumnas()
    {
    	RendererClickListener lis = new RendererClickListener() {

			@Override
			public void click(RendererClickEvent event) {
				if (event.getPropertyId()!=null)
            	{
					MapeoEstructuraAlmacen mapeo = (MapeoEstructuraAlmacen) event.getItemId();
					Notificaciones.getInstance().mensajeInformativo(mapeo.getVariedadColumna1().split("-")[1]);
//            		app.rellenarEntradasDatos(mapeo.getIdCodigo(), mapeo.getIdentificacion(), event.getPropertyId().toString());
            	}
			}
		};

		this.getColumn("nombreNave").setSortable(false);
		this.getColumn("nombreNave").setWidth(new Double(125));    	
    	this.getColumn("fila").setSortable(false);
    	this.getColumn("fila").setWidth(new Double(85));    	
    	this.getColumn("calle").setSortable(false);
    	this.getColumn("calle").setWidth(new Double(85));    	

    	this.getColumn("columna1").setSortable(false);
    	this.getColumn("columna1").setHeaderCaption(null);
    	this.getColumn("columna1").setWidth(new Double(85));    	
    	this.getColumn("columna1").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna2").setSortable(false);
    	this.getColumn("columna2").setHeaderCaption(null);
    	this.getColumn("columna2").setWidth(new Double(85));    	
    	this.getColumn("columna2").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna3").setSortable(false);
    	this.getColumn("columna3").setHeaderCaption(null);
    	this.getColumn("columna3").setWidth(new Double(85));    	
    	this.getColumn("columna3").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna4").setSortable(false);
    	this.getColumn("columna4").setHeaderCaption(null);
    	this.getColumn("columna4").setWidth(new Double(85));    	
    	this.getColumn("columna4").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna5").setSortable(false);
    	this.getColumn("columna5").setHeaderCaption(null);
    	this.getColumn("columna5").setWidth(new Double(85));    	
    	this.getColumn("columna5").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna6").setSortable(false);
    	this.getColumn("columna6").setHeaderCaption(null);
    	this.getColumn("columna6").setWidth(new Double(85));    	
    	this.getColumn("columna6").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna7").setSortable(false);
    	this.getColumn("columna7").setHeaderCaption(null);
    	this.getColumn("columna7").setWidth(new Double(85));    	
    	this.getColumn("columna7").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna8").setSortable(false);
    	this.getColumn("columna8").setHeaderCaption(null);
    	this.getColumn("columna8").setWidth(new Double(85));    	
    	this.getColumn("columna8").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna9").setSortable(false);
    	this.getColumn("columna9").setHeaderCaption(null);
    	this.getColumn("columna9").setWidth(new Double(85));    	
    	this.getColumn("columna9").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna10").setSortable(false);
    	this.getColumn("columna10").setHeaderCaption(null);
    	this.getColumn("columna10").setWidth(new Double(85));    	
    	this.getColumn("columna10").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna11").setSortable(false);
    	this.getColumn("columna11").setHeaderCaption(null);
    	this.getColumn("columna11").setWidth(new Double(85));    	
    	this.getColumn("columna11").setRenderer(new ImageRenderer(lis));
    	this.getColumn("columna12").setSortable(false);
    	this.getColumn("columna12").setWidth(new Double(85));    	
    	this.getColumn("columna12").setHeaderCaption(null);
    	this.getColumn("columna12").setRenderer(new ImageRenderer(lis));

    	this.getColumn("idNave").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idDesgloseNave").setHidden(true);
    	this.getColumn("nombreNave").setHidden(false);
    	this.getColumn("variedadColumna1").setHidden(true);
    	this.getColumn("variedadColumna2").setHidden(true);
    	this.getColumn("variedadColumna3").setHidden(true);
    	this.getColumn("variedadColumna4").setHidden(true);
    	this.getColumn("variedadColumna5").setHidden(true);
    	this.getColumn("variedadColumna6").setHidden(true);
    	this.getColumn("variedadColumna7").setHidden(true);
    	this.getColumn("variedadColumna8").setHidden(true);
    	this.getColumn("variedadColumna9").setHidden(true);
    	this.getColumn("variedadColumna10").setHidden(true);
    	this.getColumn("variedadColumna11").setHidden(true);
    	this.getColumn("variedadColumna12").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoEstructuraAlmacen) {
                MapeoEstructuraAlmacen progRow = (MapeoEstructuraAlmacen)bean;
                // The actual description text is depending on the application
                if ("columna1".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna1();
                }
                else if ("columna2".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna2();
                }
                else if ("columna3".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna3();
                }
                else if ("columna4".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna4();
                }
                else if ("columna5".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna5();
                }
                else if ("columna6".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna6();
                }
                else if ("columna7".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna7();
                }
                else if ("columna8".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna8();
                }
                else if ("columna9".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna9();
                }
                else if ("columna10".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna10();
                }
                else if ("columna11".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna11();
                }
                else if ("columna12".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getVariedadColumna12();
                }
            }
        }
        return descriptionText;
    }    
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
    			if (cellReference.getPropertyId().toString().contains("fila")) 
    			{
    				return "Rcell-normal";
    			}
            	else
        		{
            		return "cell-nativebuttonBocas" ;
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoEstructuraAlmacen mapeo = (MapeoEstructuraAlmacen) event.getItemId();
            	
            	if (event.getPropertyId().toString().startsWith("columna"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		Notificaciones.getInstance().mensajeInformativo(mapeo.getVariedadColumna1().split("-")[1]);
//            		if (mapeo.getNumero()!=null && mapeo.getNumero()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
//            		}
            	}
            	else
            	{
            		Notificaciones.getInstance().mensajeInformativo("else");
            		activadaVentanaPeticion=false;
            		ordenando = false;
//            		app.verForm(false);
//            		MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
            		
//	            	if (event.getPropertyId().toString().equals("papel"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		ordenando = false;
//	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
//	            		{
//		        	    	if (permisos>=30)
//		        	    	{
//		        	    		
//		        	    		/*
//		        	    		 * Abriremos ventana peticion parametros
//		        	    		 * rellenaremos la variables publicas
//		        	    		 * 		cajas y anada
//		        	    		 * 		llamaremos al metodo generacionPdf
//		        	    		 */
//		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
//		        	    		getUI().addWindow(vtPeticion);
//		        	    	}	    			
//							else
//							{
//								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
//							}
//	            		}
//	            	}
//	            	}            	
//	            	else
//	            	{
//	            		activadaVentanaPeticion=false;
//	            		ordenando = false;
//	            	}
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("fila");
//		this.camposNoFiltrar.add("calle");
	}

	public void generacionPdf(MapeoBarricas r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
//		Integer totalCapacidad = new Integer(0);
//		Integer q1Value = new Integer(0);
//		
//    	if (this.getFooterRowCount()==0)
//    	{
//    		this.appendFooterRow();
//    	}
//
//    	this.getFooterRow(0).getCell("descripcion").setText("Total:");
//        
//    	Indexed indexed = this.getContainerDataSource();
//        List<?> list = new ArrayList<Object>(indexed.getItemIds());
//        for(Object itemId : list)
//        {
//        	Item item = indexed.getItem(itemId);
//        	q1Value = (Integer) item.getItemProperty("capacidad").getValue();
//    		if (q1Value!=null) totalCapacidad+= q1Value;
//        }
//        
//        this.getFooterRow(0).getCell("capacidad").setText(RutinasNumericas.formatearIntegerDigitos(totalCapacidad,0).toString());
//        this.getFooterRow(0).getCell("capacidad").setStyleName("Rcell-pie");

	}
}


