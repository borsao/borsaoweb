package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoNaves extends MapeoGlobal
{
	private Integer totalFilas;
	private Integer callesFila;
	
	private String nombre;
	private String desg;
	
	public MapeoNaves()
	{ 
	}

	public Integer getTotalFilas() {
		return totalFilas;
	}

	public void setTotalFilas(Integer totalFilas) {
		this.totalFilas = totalFilas;
	}

	public Integer getCallesFila() {
		return callesFila;
	}

	public void setCallesFila(Integer callesFila) {
		this.callesFila = callesFila;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDesg() {
		return desg;
	}

	public void setDesg(String desg) {
		this.desg = desg;
	}


}