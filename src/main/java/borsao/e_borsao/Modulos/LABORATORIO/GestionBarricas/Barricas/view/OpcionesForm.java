package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.BarricasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoBarricas mapeo = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaBarricasServer cps = null;
	 private BarricasView uw = null;
	 public MapeoBarricas mapeoModificado = null;
	 private BeanFieldGroup<MapeoBarricas> fieldGroup;
	 
	 public OpcionesForm(BarricasView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoBarricas>(MapeoBarricas.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoBarricas) uw.grid.getSelectedRow();
                eliminarBarrica(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
        this.numero.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (numero.getValue()!=null && numero.getValue().length()>0 && comprobarBarrica() && isCreacion())
				{
					Notificaciones.getInstance().mensajeError("Número de Barrica existente.");
					btnGuardar.setEnabled(false);
					numero.focus();
				}
				else if (!isCreacion()&& !isBusqueda())
				{
					MapeoBarricas mapeo_orig= (MapeoBarricas) uw.grid.getSelectedRow();
					if (!mapeo_orig.getNumero().toString().contentEquals(RutinasCadenas.quitarPuntoMiles(numero.getValue())))
					{
						Notificaciones.getInstance().mensajeError("No puedes modificar el número de barrica. Lo reestablezco.");
						numero.setValue(mapeo_orig.getNumero().toString());
						numero.focus();
					}
				}
				else
				{
					btnGuardar.setEnabled(true);
				}
			}
		});
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;	
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearBarrica(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoBarricas mapeo_orig= (MapeoBarricas) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				
		 				modificarBarrica(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((BarricasGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarBarrica(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.numero.getValue()!=null && this.numero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numero", this.numero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numero", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.capacidad.getValue()!=null && this.capacidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("capacidad", this.capacidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("capacidad", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.estado.getValue()!=null && this.estado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("estado", this.estado.getValue().toString().substring(0, 1));
		}
		else
		{
			opcionesEscogidas.put("estado", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.numero.getValue()!=null && this.numero.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numero", this.numero.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numero", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.capacidad.getValue()!=null && this.capacidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("capacidad", this.capacidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("capacidad", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.estado.getValue()!=null && this.estado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("estado", this.estado.getValue().toString().substring(0, 1));
		}
		else
		{
			opcionesEscogidas.put("estado", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarBarrica(null);
		}
	}
	
	public void editarBarrica(MapeoBarricas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoBarricas();
			if (!isBusqueda())
			{
				cps = consultaBarricasServer.getInstance(CurrentUser.get());
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoBarricas>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarBarrica(MapeoBarricas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((BarricasGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((BarricasGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
			HashMap<String,String> filtrosRec = ((BarricasGrid) uw.grid).recogerFiltros();
			ArrayList<MapeoBarricas> r_vector = (ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector;
			
			Indexed indexed = ((BarricasGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((BarricasGrid) uw.grid).sort("numero");
			((BarricasGrid) uw.grid).establecerFiltros(filtrosRec);
		}
	}
	
	public void modificarBarrica(MapeoBarricas r_mapeo, MapeoBarricas r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		r_mapeo.setEstado(r_mapeo_orig.getEstado().substring(0, 1));
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo.length()==1)
		{
			r_mapeo.setEstado(rdo);
		
			if (!((BarricasGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoBarricas> r_vector = (ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((BarricasGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((BarricasGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	//            indexed.addItemAt(list.size()+1, r_mapeo);
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	//            uw.generarGrid(uw.opcionesEscogidas);
	            uw.presentarGrid(r_vector);
	            
				((BarricasGrid) uw.grid).setRecords((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector);
	////			((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);
				((BarricasGrid) uw.grid).sort("numero");
				((BarricasGrid) uw.grid).scrollTo(r_mapeo);
				((BarricasGrid) uw.grid).select(r_mapeo);
				
				((BarricasGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearBarrica(MapeoBarricas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdCodigo(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoBarricas>(r_mapeo));
			if (((BarricasGrid) uw.grid)!=null )//&& )
			{
				if (!((BarricasGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((BarricasGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((BarricasGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector).add(r_mapeo);
					((BarricasGrid) uw.grid).setRecords((ArrayList<MapeoBarricas>) ((BarricasGrid) uw.grid).vector);
					((BarricasGrid) uw.grid).sort("numero");
					((BarricasGrid) uw.grid).scrollTo(r_mapeo);
					((BarricasGrid) uw.grid).select(r_mapeo);
					
					((BarricasGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoBarricas> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
//		this.articulo.setRequired(r_obliga);
//		this.numerador.setRequired(r_obliga);
//		this.origen.setRequired(r_obliga);
//		this.notificador.setRequired(r_obliga);
//		this.cantidad.setRequired(r_obliga);
//		this.fechaNotificacion.setRequired(r_obliga);
//		this.fechaNotificacion.setEnabled(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.numero.getValue()==null || this.numero.getValue().toString().length()==0) return false;
			if (this.estado.getValue()==null || this.estado.getValue().toString().length()==0) return false;
			if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0) return false;
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0) return false;
			if (this.capacidad.getValue()==null || this.capacidad.getValue().toString().length()==0) return false;
		}
		
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}

	private void cargarCombo()
	{
		this.estado.addItem("Alta");
		this.estado.addItem("Baja");
	}
	
	private boolean comprobarBarrica()
	{
		boolean existe = false;
		consultaBarricasServer cbs = consultaBarricasServer.getInstance(CurrentUser.get());
		existe = cbs.comprobarBarricas(new Integer(RutinasCadenas.quitarPuntoMiles(numero.getValue())));
		return existe;
	}
}
