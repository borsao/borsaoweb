package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server.consultaDesgloseNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo.MapeoNaves;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaNavesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaNavesServer instance;
	
	public consultaNavesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaNavesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaNavesServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoNaves> datosNavesGlobal(MapeoNaves r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoNaves> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoNaves mapeoNaves=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_nav.idCodigo nav_id,");
		cadenaSQL.append(" lab_barricas_nav.nombre nav_nom,");
		cadenaSQL.append(" lab_barricas_nav.totalFilas nav_fil,");
		cadenaSQL.append(" lab_barricas_nav.callesFila nav_col ");
		cadenaSQL.append(" from lab_barricas_nav ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getTotalFilas()!=null && r_mapeo.getTotalFilas()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" totalFilas = " + r_mapeo.getTotalFilas() + " ");
				}
				if (r_mapeo.getCallesFila()!=null && r_mapeo.getCallesFila()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" callesFila = " + r_mapeo.getCallesFila());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by nombre asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoNaves>();
			
			while(rsOpcion.next())
			{
				mapeoNaves = new MapeoNaves();
				/*
				 * recojo mapeo operarios
				 */
				mapeoNaves.setIdCodigo(rsOpcion.getInt("nav_id"));
				mapeoNaves.setNombre(rsOpcion.getString("nav_nom"));
				mapeoNaves.setTotalFilas(rsOpcion.getInt("nav_fil"));
				mapeoNaves.setCallesFila(rsOpcion.getInt("nav_col"));

				vector.add(mapeoNaves);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_nav.idCodigo) nav_sig ");
     	cadenaSQL.append(" FROM lab_barricas_nav ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("nav_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	
	public Integer obtenerId(String r_nombre)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_nav.idCodigo nav_id ");
     	cadenaSQL.append(" FROM lab_barricas_nav ");
     	cadenaSQL.append(" where lab_barricas_nav.nombre = '" + r_nombre  + "' ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("nav_id");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}


	public String guardarNuevo(MapeoNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_nav( ");
			cadenaSQL.append(" lab_barricas_nav.idCodigo,");			
			cadenaSQL.append(" lab_barricas_nav.nombre, ");			
			cadenaSQL.append(" lab_barricas_nav.totalFilas, ");
			cadenaSQL.append(" lab_barricas_nav.callesFila ) VALUES (");
			cadenaSQL.append(" ?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getNombre());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getTotalFilas()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getTotalFilas());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getCallesFila()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCallesFila());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE lab_barricas_nav set ");
		    cadenaSQL.append(" lab_barricas_nav.nombre =?,");
		    cadenaSQL.append(" lab_barricas_nav.totalFilas =?,");
		    cadenaSQL.append(" lab_barricas_nav.callesFila =? ");
			cadenaSQL.append(" where lab_barricas_nav.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getNombre());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getTotalFilas()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getTotalFilas());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getCallesFila()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getCallesFila());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_barricas_nav ");            
			cadenaSQL.append(" WHERE lab_barricas_nav.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoBarricas r_mapeo)
	{
		return null;
	}
	
	public String generarDesglose(MapeoNaves r_mapeo)
	{
		String rdo = null;
		
		MapeoDesgloseNaves mapeo = null;
		consultaDesgloseNavesServer cdns = consultaDesgloseNavesServer.getInstance(CurrentUser.get());
		/*
		 * guardaremos todas las combinaciones posibles entre filas y columnas para la nave seleccionada.
		 */
		
		if (!cdns.hayDatos(r_mapeo.getIdCodigo()))
		{
			for (int i = 1; i<= r_mapeo.getTotalFilas();i++)
			{
				mapeo = new MapeoDesgloseNaves();
				mapeo.setFilas(i);
				mapeo.setIdNave(r_mapeo.getIdCodigo());
				/*
				 * rellenamos datos fijos por fila
				 */
				for (int j=1;j<=r_mapeo.getCallesFila();j++)
				{
					/*
					 * rellenamos datos columnas
					 */
					mapeo.setCalle(j);
					rdo = cdns.guardarNuevo(mapeo);
					if (rdo!=null) break;
				}
			}
		}
		else
		{
			rdo = "Nave ya generada. Debes modificar su contenido manualmente";
		}
		return rdo;
	}
}