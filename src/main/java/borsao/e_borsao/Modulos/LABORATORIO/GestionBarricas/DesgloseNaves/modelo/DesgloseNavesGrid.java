package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.view.DesgloseNavesView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.server.consultaNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.view.NavesView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class DesgloseNavesGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	private DesgloseNavesView app = null;
	
    public DesgloseNavesGrid(DesgloseNavesView r_app, ArrayList<MapeoDesgloseNaves> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoDesgloseNaves.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setEditorEnabled(false);
		this.setSeleccion(SelectionMode.SINGLE);
		
		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("desg", "nombre", "filas", "calle");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("nombre", "235");

    	this.getColumn("nombre").setHeaderCaption("NOMBRE");
    	this.getColumn("nombre").setWidth(300);
    	this.getColumn("filas").setHeaderCaption("FILA");
    	this.getColumn("filas").setWidth(300);
    	this.getColumn("calle").setHeaderCaption("CALLE");
    	this.getColumn("calle").setWidth(300);

    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idNave").setHidden(true);
    	this.getColumn("desg").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoDesgloseNaves) {
                MapeoDesgloseNaves progRow = (MapeoDesgloseNaves)bean;
                // The actual description text is depending on the application
//                if ("desg".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Desglose Nave";
//                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("nombre".equals(cellReference.getPropertyId())) 
            	{
            		return "cell-normal";
            	}
            	else if ("filas".equals(cellReference.getPropertyId())||"calle".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
    			}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoDesgloseNaves mapeo = (MapeoDesgloseNaves) event.getItemId();
            	
//            	if (event.getPropertyId().toString().equals("desg"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		if (mapeo.getNombre()!=null && mapeo.getNombre().length()!=0)
//            		{
//        	    		consultaNavesServer cns = consultaNavesServer.getInstance(CurrentUser.get());
//        	    		String rdo = cns.generarDesglose(mapeo);
//        	    		if (rdo!=null)
//        	    		{
//        	    			Notificaciones.getInstance().mensajeError(rdo);
//        	    		}
//            		}
//            	}
//            	else
//            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
//	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("filas");
		this.camposNoFiltrar.add("calle");
	}

	public void generacionPdf(MapeoDesgloseNaves r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	public void calcularTotal() {

	}
}


