package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view.pantallaAyudaBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.OrdenesTrabajoGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server.consultaOrdenesTrabajoBarricasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionBarricasOrdenTrabajo extends Window
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	
	private TextField txtTipo = null;
	private TextField txtNumeroDesde = null;
	private TextField txtNumeroHasta = null;
	private Button btnInsertar = null;
	private Button btnAyudaBar = null;
	private Button btnSeleccionar = null;
	private Button btnEliminar = null;
	private Panel panelGrid = null;
	private Grid gridDatos = null;
	private HorizontalLayout fila4 =null;
	private VerticalLayout botonesGrid = null; 
	/*
	 * Valores
	 */
	public MapeoOrdenesTrabajo mapeoOrdenesTrabajo = null;
	private OrdenesTrabajoGrid App = null;
	
	public PeticionBarricasOrdenTrabajo(GridPropio r_App, MapeoOrdenesTrabajo r_mapeo, String r_titulo)
	{
		this.mapeoOrdenesTrabajo = r_mapeo;
		this.App = (OrdenesTrabajoGrid) r_App;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
			
				this.btnAyudaBar= new Button("Buscar Barricas");
				this.btnAyudaBar.addStyleName(ValoTheme.BUTTON_PRIMARY);
				this.btnAyudaBar.addStyleName(ValoTheme.BUTTON_TINY);
				this.btnAyudaBar.setIcon(FontAwesome.PLUS_CIRCLE);
				this.btnAyudaBar.setEnabled(this.mapeoOrdenesTrabajo.getEstado().contentEquals("A"));
				
				fila1.addComponent(btnAyudaBar);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtTipo=new TextField("Tipo Barricas");
				this.txtTipo.setWidth("200px");
				
				this.txtNumeroDesde=new TextField("Desde Barrica");				
				this.txtNumeroDesde.setWidth("150px");
				
				this.txtNumeroHasta=new TextField("Hasta Barrica");				
				this.txtNumeroHasta.setWidth("150px");
				
				fila3.addComponent(txtTipo);
				fila3.addComponent(txtNumeroDesde);
				fila3.addComponent(txtNumeroHasta);

			fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
			
				/*
				 * aqui metemos el grid de las seleccionadas
				 */
				botonesGrid = new VerticalLayout();
				botonesGrid.setSpacing(true);
					this.btnInsertar = new Button("Insertar");
					this.btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
					this.btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);
					this.btnInsertar.setIcon(FontAwesome.PLUS_CIRCLE);
					this.btnInsertar.setEnabled(this.mapeoOrdenesTrabajo.getEstado().contentEquals("A"));
				
					this.btnSeleccionar= new Button("Seleccionar");
		    		this.btnSeleccionar.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    		this.btnSeleccionar.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.btnSeleccionar.setIcon(FontAwesome.PLUS_CIRCLE);
		    		this.btnSeleccionar.setEnabled(this.mapeoOrdenesTrabajo.getEstado().contentEquals("A"));
		    		
					this.btnEliminar= new Button("Eliminar");
		    		this.btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
		    		this.btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.btnEliminar.setIcon(FontAwesome.MINUS_CIRCLE);
		    		this.btnEliminar.setEnabled(this.mapeoOrdenesTrabajo.getEstado().contentEquals("A"));
		    		
		    		botonesGrid.addComponent(this.btnInsertar);
		    		botonesGrid.addComponent(this.btnSeleccionar);
		    		botonesGrid.addComponent(this.btnEliminar);
				
		    		this.generarGrid();
		    						
				
		controles.addComponent(fila1);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("750px");
		
		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
		this.cargarListeners();
	}
	
	private void cargarListeners()
	{

		txtNumeroDesde.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (txtNumeroHasta.getValue()!=null && txtNumeroHasta.getValue().length()==0)
				{
					txtNumeroHasta.setValue(txtNumeroDesde.getValue().toString());
					txtNumeroHasta.focus();
				}
			}
		});
		
		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (gridDatos.getSelectedRows()!=null && gridDatos.getSelectedRows().size()!=0)
				{
					Iterator it = null;
					Item file  =null;
					Integer valor = null;
					consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
					MapeoOrdenesTrabajoBarricas map = new MapeoOrdenesTrabajoBarricas();
					
					it = gridDatos.getSelectedRows().iterator();
					
					while (it.hasNext())
					{			
						valor = (Integer) it.next();
						file = gridDatos.getContainerDataSource().getItem(valor);
						map.setIdCodigo(new Integer(file.getItemProperty("idCodigo").getValue().toString()));
						cotbs.eliminar(map);
					}
					
					fila4.removeComponent(panelGrid);
					fila4.removeComponent(botonesGrid);
					gridDatos = null;
					panelGrid = null;
					generarGrid();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Debes seleccionar algún registro para borrar");
				}
			}
		});

		btnAyudaBar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				seleccionar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				boolean correcto = true;
				String rdo =null;
				MapeoBarricas map = null;
				MapeoOrdenesTrabajoBarricas mapOrdBar = null;
				Iterator<MapeoBarricas> iterBarricas = null;
				
				consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
				consultaBarricasServer cbs = consultaBarricasServer.getInstance(CurrentUser.get());
				ArrayList<MapeoBarricas> vectorBarricasSeleccionadas = new ArrayList<MapeoBarricas>();
				MapeoBarricas mapeoBarricas = new MapeoBarricas();
				/*
				 * Guardar los datos y rellenar el grid
				 */
				if (txtTipo.getValue()!=null && txtTipo.getValue().length()>0 && (txtNumeroDesde.getValue()==null || txtNumeroDesde.getValue().length()==0|| txtNumeroHasta.getValue()==null || txtNumeroHasta.getValue().length()==0))
				{
					mapeoBarricas.setDescripcion(txtTipo.getValue());
					if (mapeoOrdenesTrabajo.getTipo().toUpperCase().contains("LLENADO")) vectorBarricasSeleccionadas = cbs.datosBarricasGlobalVaciasSinUsar(mapeoBarricas, mapeoOrdenesTrabajo.getNumero());
					else if (mapeoOrdenesTrabajo.getTipo().toUpperCase().contains("VACIADO")) vectorBarricasSeleccionadas = cbs.datosBarricasGlobalLlenas(mapeoBarricas, mapeoOrdenesTrabajo.getVariedad(), mapeoOrdenesTrabajo.getReferencia());
					else correcto = false;
					
					if (vectorBarricasSeleccionadas==null || vectorBarricasSeleccionadas.isEmpty()) correcto = false;
					
				}
				else if (txtNumeroDesde.getValue()!=null && txtNumeroDesde.getValue().length()>0 && txtNumeroHasta.getValue()!=null && txtNumeroHasta.getValue().length()>0)
				{
					Integer dsd = new Integer(txtNumeroDesde.getValue());
					Integer hst = new Integer(txtNumeroHasta.getValue());
					String tipBar = null;
					if (txtTipo.getValue()!=null && txtTipo.getValue().length()>0) 
					{
						tipBar=txtTipo.getValue();
					}
					if (dsd.compareTo(hst)<=0)
					{
						if (mapeoOrdenesTrabajo.getTipo().toUpperCase().contains("LLENADO")) vectorBarricasSeleccionadas = cbs.datosBarricasEntreNumerosVacias(dsd, hst, tipBar, mapeoOrdenesTrabajo.getNumero());
						else if (mapeoOrdenesTrabajo.getTipo().toUpperCase().contains("VACIADO")) vectorBarricasSeleccionadas = cbs.datosBarricasEntreNumeros(dsd, hst, tipBar);
						else correcto = false;
						
						
						if (vectorBarricasSeleccionadas==null || vectorBarricasSeleccionadas.isEmpty()) correcto = false;
					}
					else
					{
						Notificaciones.getInstance().mensajeError("Debes rellenar bien los datos de búsqueda");
						correcto = false;
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Debes rellenar bien los datos de búsqueda");
					correcto = false;
				}
				/*
				 * Dibujo el grid agregando los datos seleccionados
				 */
				if (correcto)
				{
					iterBarricas = vectorBarricasSeleccionadas.iterator();
					if (iterBarricas.hasNext())
					{
						while (iterBarricas.hasNext())
						{
							map = (MapeoBarricas) iterBarricas.next();
							mapOrdBar = new MapeoOrdenesTrabajoBarricas();
							mapOrdBar.setIdBarrica(map.getIdCodigo());
							mapOrdBar.setIdOrden(mapeoOrdenesTrabajo.getIdCodigo());
							mapOrdBar.setIdCodigo(cotbs.obtenerSiguiente());
							
							rdo = cotbs.guardarNuevo(mapOrdBar);
							if (rdo!=null)
							{
								Notificaciones.getInstance().mensajeError("Debes Error al guardar las barricas seleccionadas. Error: " + rdo);
								correcto=false;
								break;
							}
						}
						map=null;
						mapOrdBar=null;
						mapeoBarricas=null;
						iterBarricas=null;
						vectorBarricasSeleccionadas=null;
						cotbs=null;
						cbs=null;
	
						if (panelGrid!=null && gridDatos!=null)
						{
							fila4.removeComponent(panelGrid);
							fila4.removeComponent(botonesGrid);
							gridDatos = null;
							panelGrid = null;
						}
						generarGrid();
					}
					else
					{
						Notificaciones.getInstance().mensajeError("No se han encontrado barricas a insertar. Revisa los valores de búsqueda");
					}
				}
			}
		});
		
		btnSeleccionar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				if (gridDatos!=null)
				{
					if (!(gridDatos.getSelectionModel() instanceof MultiSelectionModel))
					{
						gridDatos.setSelectionMode(SelectionMode.MULTI);
						btnSeleccionar.setCaption("Cancelar Seleccion");
					}
					else
					{
						gridDatos.setSelectionMode(SelectionMode.NONE);
						btnSeleccionar.setCaption("Seleccionar");
					}
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				salir();
			}
		});	
	}
	
	private void salir()
	{
		((OrdenesTrabajoGrid) this.App).app.actualizarDatos();
		close();
	}
	
	private void generarGrid()
	{
    	ArrayList<MapeoOrdenesTrabajoBarricas> vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
    	consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
    	MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
 
    	mapeoOrdenesTrabajoBarricas.getMapeoOrdenes().setIdCodigo(this.mapeoOrdenesTrabajo.getIdCodigo());
    	vector = cotbs.datosOrdenesTrabajoBarricasGlobal(mapeoOrdenesTrabajoBarricas);
    	
    	IndexedContainer container =null;
    	Iterator<MapeoOrdenesTrabajoBarricas> iterator = null;
    	MapeoOrdenesTrabajoBarricas mapeo = null;
    	Integer total = new Integer(0);
    	
    	container = new IndexedContainer();
    	container.addContainerProperty("idCodigo", Integer.class, null);
    	container.addContainerProperty("Numero", Integer.class, null);
    	container.addContainerProperty("Descripcion", String.class, null);
    	container.addContainerProperty("Año", Integer.class, null);
    	container.addContainerProperty("Capacidad", Integer.class, null);
    	
    	iterator = vector.iterator();
    	
    	while (iterator.hasNext())
    	{
    		Object newItemId = container.addItem();
    		Item file = container.getItem(newItemId);
    		
    		mapeo= (MapeoOrdenesTrabajoBarricas) iterator.next();
    		total = total + mapeo.getMapeoBarricas().getCapacidad();
    		
    		file.getItemProperty("idCodigo").setValue(mapeo.getIdCodigo());
    		file.getItemProperty("Numero").setValue(mapeo.getMapeoBarricas().getNumero());
    		file.getItemProperty("Descripcion").setValue(mapeo.getMapeoBarricas().getDescripcion());
    		file.getItemProperty("Año").setValue(mapeo.getMapeoBarricas().getEjercicio());
    		file.getItemProperty("Capacidad").setValue(mapeo.getMapeoBarricas().getCapacidad());
    	}
    	
    	gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.NONE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	
    	gridDatos.getColumn("idCodigo").setHidden(true);
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
    				return "Rcell-normal";
    			}
    			return null;
    		}
    	});
    	
		Integer totalCapacidad = new Integer(0);
		Integer q1Value = new Integer(0);
		
    	if (gridDatos.getFooterRowCount()==0)
    	{
    		gridDatos.appendFooterRow();
    	}

    	gridDatos.getFooterRow(0).getCell("Descripcion").setText("Total:");
        
    	Indexed indexed = gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	q1Value = (Integer) item.getItemProperty("Capacidad").getValue();
    		if (q1Value!=null) totalCapacidad+= q1Value;
        }
        
        gridDatos.getFooterRow(0).getCell("Capacidad").setText(RutinasNumericas.formatearIntegerDigitos(totalCapacidad,0).toString());
        gridDatos.getFooterRow(0).getCell("Capacidad").setStyleName("Rcell-pie");
        

    	
    	panelGrid = new Panel("Barricas Asignadas");
    	panelGrid.setSizeUndefined(); // Shrink to fit content
    	panelGrid.setHeight("450px");
    	panelGrid.setWidth("100%");
    	panelGrid.setContent(gridDatos);
    	fila4.addComponent(panelGrid);
    	fila4.addComponent(botonesGrid);
	}
	
	private void seleccionar()
	{
		
		pantallaAyudaBarricas vt = new pantallaAyudaBarricas(this, "Ayuda Barricas");
		getUI().addWindow(vt);
	}
	
	public void insertar(ArrayList<MapeoBarricas> r_vector)
	{
		MapeoOrdenesTrabajoBarricas mapOrdBar = null;
		MapeoBarricas map = null;
		Iterator<MapeoBarricas> iterBarricas = null;
		consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
		consultaBarricasServer cbs = consultaBarricasServer.getInstance(CurrentUser.get());

		iterBarricas = r_vector.iterator();
		if (iterBarricas.hasNext())
		{
			while (iterBarricas.hasNext())
			{
				map = (MapeoBarricas) iterBarricas.next();
				mapOrdBar = new MapeoOrdenesTrabajoBarricas();
				mapOrdBar.setIdBarrica(map.getIdCodigo());
				mapOrdBar.setIdOrden(mapeoOrdenesTrabajo.getIdCodigo());
				mapOrdBar.setIdCodigo(cotbs.obtenerSiguiente());
				
				String rdo = cotbs.guardarNuevo(mapOrdBar);
				if (rdo!=null)
				{
					Notificaciones.getInstance().mensajeError("Error al guardar las barricas seleccionadas. Error: " + rdo);
					break;
				}
			}
			map=null;
			mapOrdBar=null;
			iterBarricas=null;
			cotbs=null;
			cbs=null;

			if (panelGrid!=null && gridDatos!=null)
			{
				fila4.removeComponent(panelGrid);
				fila4.removeComponent(botonesGrid);
				gridDatos = null;
				panelGrid = null;
			}
			generarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No se han encontrado barricas a insertar.");
		}
	}
}