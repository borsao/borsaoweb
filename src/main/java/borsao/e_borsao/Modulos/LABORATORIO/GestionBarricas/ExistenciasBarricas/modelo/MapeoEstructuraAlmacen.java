package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import com.vaadin.server.ThemeResource;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEstructuraAlmacen extends MapeoGlobal
{
	private Integer idDesgloseNave;
	private Integer idNave;
	
	private Integer fila;
	private String nombreNave;
	private String calle;	
	private ThemeResource columna1;
	private ThemeResource columna2;
	private ThemeResource columna3;
	private ThemeResource columna4;
	private ThemeResource columna5;
	private ThemeResource columna6;
	private ThemeResource columna7;
	private ThemeResource columna8;
	private ThemeResource columna9;
	private ThemeResource columna10;
	private ThemeResource columna11;
	private ThemeResource columna12;
	private String variedadColumna1;
	private String variedadColumna2;
	private String variedadColumna3;
	private String variedadColumna4;
	private String variedadColumna5;
	private String variedadColumna6;
	private String variedadColumna7;
	private String variedadColumna8;
	private String variedadColumna9;
	private String variedadColumna10;
	private String variedadColumna11;
	private String variedadColumna12;

	public MapeoEstructuraAlmacen()
	{ 
	}

	public Integer getIdDesgloseNave() {
		return idDesgloseNave;
	}

	public void setIdDesgloseNave(Integer idDesgloseNave) {
		this.idDesgloseNave = idDesgloseNave;
	}

	public Integer getIdNave() {
		return idNave;
	}

	public void setIdNave(Integer idNave) {
		this.idNave = idNave;
	}

	public Integer getFila() {
		return fila;
	}

	public void setFila(Integer fila) {
		this.fila = fila;
	}

	public String getNombreNave() {
		return nombreNave;
	}

	public void setNombreNave(String nombreNave) {
		this.nombreNave = nombreNave;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public ThemeResource getColumna1() {
		return columna1;
	}

	public void setColumna1(ThemeResource columna1) {
		this.columna1 = columna1;
	}

	public ThemeResource getColumna2() {
		return columna2;
	}

	public void setColumna2(ThemeResource columna2) {
		this.columna2 = columna2;
	}

	public ThemeResource getColumna3() {
		return columna3;
	}

	public void setColumna3(ThemeResource columna3) {
		this.columna3 = columna3;
	}

	public ThemeResource getColumna4() {
		return columna4;
	}

	public void setColumna4(ThemeResource columna4) {
		this.columna4 = columna4;
	}

	public ThemeResource getColumna5() {
		return columna5;
	}

	public void setColumna5(ThemeResource columna5) {
		this.columna5 = columna5;
	}

	public ThemeResource getColumna6() {
		return columna6;
	}

	public void setColumna6(ThemeResource columna6) {
		this.columna6 = columna6;
	}

	public ThemeResource getColumna7() {
		return columna7;
	}

	public void setColumna7(ThemeResource columna7) {
		this.columna7 = columna7;
	}

	public ThemeResource getColumna8() {
		return columna8;
	}

	public void setColumna8(ThemeResource columna8) {
		this.columna8 = columna8;
	}

	public ThemeResource getColumna9() {
		return columna9;
	}

	public void setColumna9(ThemeResource columna9) {
		this.columna9 = columna9;
	}

	public ThemeResource getColumna10() {
		return columna10;
	}

	public void setColumna10(ThemeResource columna10) {
		this.columna10 = columna10;
	}

	public ThemeResource getColumna11() {
		return columna11;
	}

	public void setColumna11(ThemeResource columna11) {
		this.columna11 = columna11;
	}

	public ThemeResource getColumna12() {
		return columna12;
	}

	public void setColumna12(ThemeResource columna12) {
		this.columna12 = columna12;
	}

	public String getVariedadColumna1() {
		return variedadColumna1;
	}

	public void setVariedadColumna1(String variedadColumna1) {
		this.variedadColumna1 = variedadColumna1;
	}

	public String getVariedadColumna2() {
		return variedadColumna2;
	}

	public void setVariedadColumna2(String variedadColumna2) {
		this.variedadColumna2 = variedadColumna2;
	}

	public String getVariedadColumna3() {
		return variedadColumna3;
	}

	public void setVariedadColumna3(String variedadColumna3) {
		this.variedadColumna3 = variedadColumna3;
	}

	public String getVariedadColumna4() {
		return variedadColumna4;
	}

	public void setVariedadColumna4(String variedadColumna4) {
		this.variedadColumna4 = variedadColumna4;
	}

	public String getVariedadColumna5() {
		return variedadColumna5;
	}

	public void setVariedadColumna5(String variedadColumna5) {
		this.variedadColumna5 = variedadColumna5;
	}

	public String getVariedadColumna6() {
		return variedadColumna6;
	}

	public void setVariedadColumna6(String variedadColumna6) {
		this.variedadColumna6 = variedadColumna6;
	}

	public String getVariedadColumna7() {
		return variedadColumna7;
	}

	public void setVariedadColumna7(String variedadColumna7) {
		this.variedadColumna7 = variedadColumna7;
	}

	public String getVariedadColumna8() {
		return variedadColumna8;
	}

	public void setVariedadColumna8(String variedadColumna8) {
		this.variedadColumna8 = variedadColumna8;
	}

	public String getVariedadColumna9() {
		return variedadColumna9;
	}

	public void setVariedadColumna9(String variedadColumna9) {
		this.variedadColumna9 = variedadColumna9;
	}

	public String getVariedadColumna10() {
		return variedadColumna10;
	}

	public void setVariedadColumna10(String variedadColumna10) {
		this.variedadColumna10 = variedadColumna10;
	}

	public String getVariedadColumna11() {
		return variedadColumna11;
	}

	public void setVariedadColumna11(String variedadColumna11) {
		this.variedadColumna11 = variedadColumna11;
	}

	public String getVariedadColumna12() {
		return variedadColumna12;
	}

	public void setVariedadColumna12(String variedadColumna12) {
		this.variedadColumna12 = variedadColumna12;
	}
}