package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.HtmlRenderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view.BarricasView;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view.pantallaAyudaBarricas;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class BarricasGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	public Integer permisos = null;
	private BarricasView app = null;
	private Window vt = null;
	
    public BarricasGrid(BarricasView r_app, ArrayList<MapeoBarricas> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
        this.setSeleccion(SelectionMode.SINGLE);
		this.asignarTitulo("CONTROL BARRICAS");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    public BarricasGrid(Window r_app, ArrayList<MapeoBarricas> r_vector) 
    {
        this.vector=r_vector;
        this.vt=r_app;
        this.setSizeFull();
        this.setSeleccion(SelectionMode.MULTI);
		this.asignarTitulo("CONTROL BARRICAS");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoBarricas.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		
		if (this.app instanceof BarricasView)
		{
			this.setConTotales(this.app.conTotales);
		}
		else
			this.setConTotales(true);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		if (this.isConTotales()) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("desg",  "numero", "descripcion", "ejercicio", "capacidad", "estado", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("numero", "75");
    	this.widthFiltros.put("descripcion", "225");
    	this.widthFiltros.put("observaciones", "400");
    	this.widthFiltros.put("estado", "85");
    	this.widthFiltros.put("ejercicio", "85");

    	this.getColumn("desg").setHeaderCaption("");
    	this.getColumn("desg").setSortable(false);
    	this.getColumn("desg").setWidth(new Double(40));

    	this.getColumn("capacidad").setHeaderCaption("Capacidad");
    	this.getColumn("capacidad").setWidthUndefined();
    	this.getColumn("capacidad").setWidth(120);
    	this.getColumn("ejercicio").setHeaderCaption("EJERCICIO");
    	this.getColumn("ejercicio").setWidthUndefined();
    	this.getColumn("ejercicio").setWidth(120);
    	this.getColumn("numero").setHeaderCaption("Numero");
    	this.getColumn("numero").setWidth(100);

    	this.getColumn("descripcion").setHeaderCaption("DESCRIPCION.");
    	this.getColumn("descripcion").setWidth(300);
    	this.getColumn("estado").setHeaderCaption("ESTADO");
    	this.getColumn("estado").setWidth(120);
    	this.getColumn("observaciones").setHeaderCaption("OBSERVACIONES");
    	this.getColumn("observaciones").setWidth(500);
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoBarricas) {
                MapeoBarricas progRow = (MapeoBarricas)bean;
                // The actual description text is depending on the application
                if ("numero".equals(cell.getPropertyId()))
                {
                	switch (progRow.getEstado())
                	{
                		case "B":
                		{
                			descriptionText = "Baja";
                			break;
                		}
                		case "A":
                		{
                			descriptionText = "Alta";
                			break;
                		}
                	}
                	
                }
                else if ("desg".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Movimientos de la Barrica";
                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strEstado=cellReference.getValue().toString();
            	}

            	if ("numero".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "A":
	        			{
	        				return "Rcell-normal";
	        			}
	        			case "B":
	        			{
	        				return "Rcell-error";
	        			}
	        			default:
	        			{
	        				return "Rcell-normal";
	        			}
            		}
            	}
            	else if ( "desg".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonDesg";
            	}
            	else if ( "capacidad".equals(cellReference.getPropertyId())||"ejercicio".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("desg"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getNumero()!=null && mapeo.getNumero()!=0)
            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
            		}
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
//            		app.verForm(false);
//            		MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
            		
//	            	if (event.getPropertyId().toString().equals("papel"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		ordenando = false;
//	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
//	            		{
//		        	    	if (permisos>=30)
//		        	    	{
//		        	    		
//		        	    		/*
//		        	    		 * Abriremos ventana peticion parametros
//		        	    		 * rellenaremos la variables publicas
//		        	    		 * 		cajas y anada
//		        	    		 * 		llamaremos al metodo generacionPdf
//		        	    		 */
//		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
//		        	    		getUI().addWindow(vtPeticion);
//		        	    	}	    			
//							else
//							{
//								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
//							}
//	            		}
//	            	}
//	            	}            	
//	            	else
//	            	{
//	            		activadaVentanaPeticion=false;
//	            		ordenando = false;
//	            	}
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("desg");
		this.camposNoFiltrar.add("capacidad");
	}

	public void generacionPdf(MapeoBarricas r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
		Integer totalLitrosVacios = new Integer(0) ;
		
		Integer q1Value = new Integer(0);
		
    	if (this.getFooterRowCount()==0)
    	{
    		this.appendFooterRow();
    	}

    	this.getFooterRow(0).getCell("descripcion").setText("Total Litros: ");
        
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	q1Value = (Integer) item.getItemProperty("capacidad").getValue();
        	if (q1Value!=null) totalLitrosVacios += q1Value;
        }
        
        this.getFooterRow(0).getCell("capacidad").setText(RutinasNumericas.formatearIntegerDigitos(totalLitrosVacios,0).toString());
        this.getFooterRow(0).getCell("capacidad").setStyleName("Rcell-pie");
        
		if (this.app instanceof BarricasView)
		{

			this.app.barAndGridLayout.setWidth("100%");
			this.app.barAndGridLayout.setHeight("100%");
		}
		else
		{
			((pantallaAyudaBarricas) this.vt).frameCentral.setWidth("100%");
			((pantallaAyudaBarricas) this.vt).frameCentral.setHeight("100%");
		}

	}
}


