package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server;

import java.sql.Connection; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;

public class consultaBarricasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaBarricasServer instance;
	
	public consultaBarricasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaBarricasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaBarricasServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoBarricas> datosBarricasGlobal(MapeoBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.idCodigo bar_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num,");
		cadenaSQL.append(" lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.estado bar_est,");
		cadenaSQL.append(" lab_barricas.observaciones bar_obs,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNumero()!=null && r_mapeo.getNumero()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numero = " + r_mapeo.getNumero() + " ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0 && !r_mapeo.getEstado().contains("T"))
				{
					if (cadenaWhere.length()>0)  cadenaWhere.append(" and ");
					cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getCapacidad()!=null && r_mapeo.getCapacidad()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capacidad = " + r_mapeo.getCapacidad());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by numero asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setIdCodigo(rsOpcion.getInt("bar_id"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				if (rsOpcion.getString("bar_est").contentEquals("A"))
				{
					mapeoBarricas.setEstado("Alta");
				}
				if (rsOpcion.getString("bar_est").contentEquals("B"))
				{
					mapeoBarricas.setEstado("Baja");
				}
				mapeoBarricas.setObservaciones(rsOpcion.getString("bar_obs"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public ArrayList<MapeoBarricas> datosBarricasGlobalVaciasSinUsar(MapeoBarricas r_mapeo, Integer r_numeroOrden)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.idCodigo bar_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num,");
		cadenaSQL.append(" lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.estado bar_est,");
		cadenaSQL.append(" lab_barricas.observaciones bar_obs,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.idCodigo not in (select idBarrica from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes.idCodigo = lab_barricas_ordenes_bar.idOrden ");
		
		if (r_numeroOrden!=null) 
			cadenaSQL.append(" and lab_barricas_ordenes.estado = 'A' and lab_barricas_ordenes.numero !=" + r_numeroOrden +  ") ");
		else
			cadenaSQL.append(" and lab_barricas_ordenes.estado = 'A' ) ");
		
		cadenaSQL.append(" and lab_barricas.idCodigo not in (select idBarrica from lab_barricas_existencias)  ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNumero()!=null && r_mapeo.getNumero()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numero = " + r_mapeo.getNumero() + " ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0 && !r_mapeo.getEstado().contains("T"))
				{
					if (cadenaWhere.length()>0)  cadenaWhere.append(" and ");
					cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getCapacidad()!=null && r_mapeo.getCapacidad()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capacidad = " + r_mapeo.getCapacidad());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" and ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by numero asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setIdCodigo(rsOpcion.getInt("bar_id"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				if (rsOpcion.getString("bar_est").contentEquals("A"))
				{
					mapeoBarricas.setEstado("Alta");
				}
				if (rsOpcion.getString("bar_est").contentEquals("B"))
				{
					mapeoBarricas.setEstado("Baja");
				}
				mapeoBarricas.setObservaciones(rsOpcion.getString("bar_obs"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoBarricas> datosBarricasVaciasAgrupadas()
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" count(lab_barricas.numero) bar_num, ");
		cadenaSQL.append(" sum(lab_barricas.capacidad) bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.estado = 'A' ");
		cadenaSQL.append(" and lab_barricas.idCodigo not in (select idBarrica from lab_barricas_existencias)  ");
		cadenaSQL.append(" group by lab_barricas.descripcion, lab_barricas.ejercicio");
		cadenaSQL.append(" order by lab_barricas.ejercicio, lab_barricas.descripcion ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	public ArrayList<MapeoBarricas> datosBarricasGlobalLlenas(MapeoBarricas r_mapeo, String r_variedad, String r_referencia)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.idCodigo bar_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num,");
		cadenaSQL.append(" lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.estado bar_est,");
		cadenaSQL.append(" lab_barricas.observaciones bar_obs,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.idCodigo not in (select idBarrica from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" 										inner join lab_barricas_ordenes on lab_barricas_ordenes.idCodigo = lab_barricas_ordenes_bar.idOrden ");
		cadenaSQL.append(" 										and lab_barricas_ordenes.estado = 'A') ");
		cadenaSQL.append(" and lab_barricas.idCodigo in (select idBarrica from lab_barricas_existencias  ");
		cadenaSQL.append(" 								where lab_barricas_existencias.variedad = '" + r_variedad + "' ");
		cadenaSQL.append(" 								and lab_barricas_existencias.referencia = '" + r_referencia + "') ");
		
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNumero()!=null && r_mapeo.getNumero()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numero = " + r_mapeo.getNumero() + " ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0 && !r_mapeo.getEstado().contains("T"))
				{
					if (cadenaWhere.length()>0)  cadenaWhere.append(" and ");
					cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getCapacidad()!=null && r_mapeo.getCapacidad()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capacidad = " + r_mapeo.getCapacidad());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" and ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by numero asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setIdCodigo(rsOpcion.getInt("bar_id"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				if (rsOpcion.getString("bar_est").contentEquals("A"))
				{
					mapeoBarricas.setEstado("Alta");
				}
				if (rsOpcion.getString("bar_est").contentEquals("B"))
				{
					mapeoBarricas.setEstado("Baja");
				}
				mapeoBarricas.setObservaciones(rsOpcion.getString("bar_obs"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoBarricas> datosBarricasEntreNumeros(Integer r_desde, Integer r_hasta, String r_tipo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.idCodigo bar_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num,");
		cadenaSQL.append(" lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.estado bar_est,");
		cadenaSQL.append(" lab_barricas.observaciones bar_obs,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.idCodigo not in (select idBarrica from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes.idCodigo = lab_barricas_ordenes_bar.idOrden ");
		cadenaSQL.append(" and lab_barricas_ordenes.estado = 'A') ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			cadenaWhere.append(" and numero between " + r_desde + " and " + r_hasta);
			
			if (r_tipo!=null && r_tipo.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" descripcion = '" + r_tipo + "' " );
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by numero asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setIdCodigo(rsOpcion.getInt("bar_id"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				if (rsOpcion.getString("bar_est").contentEquals("A"))
				{
					mapeoBarricas.setEstado("Alta");
				}
				if (rsOpcion.getString("bar_est").contentEquals("B"))
				{
					mapeoBarricas.setEstado("Baja");
				}
				mapeoBarricas.setObservaciones(rsOpcion.getString("bar_obs"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoBarricas> datosBarricasEntreNumerosVacias(Integer r_desde, Integer r_hasta, String r_tipo, Integer r_numeroOrden)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoBarricas mapeoBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas.idCodigo bar_id,");
		cadenaSQL.append(" lab_barricas.numero bar_num,");
		cadenaSQL.append(" lab_barricas.descripcion bar_des,");
		cadenaSQL.append(" lab_barricas.estado bar_est,");
		cadenaSQL.append(" lab_barricas.observaciones bar_obs,");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje, ");
		cadenaSQL.append(" lab_barricas.capacidad bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.idCodigo not in (select idBarrica from lab_barricas_ordenes_bar ");
		cadenaSQL.append(" inner join lab_barricas_ordenes on lab_barricas_ordenes.idCodigo = lab_barricas_ordenes_bar.idOrden ");
		cadenaSQL.append(" and lab_barricas_ordenes.estado = 'A') ");
		cadenaSQL.append(" and lab_barricas.idCodigo not in (select idBarrica from lab_barricas_existencias)  ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			cadenaWhere.append(" and lab_barricas.numero between " + r_desde + " and " + r_hasta);
			
			if (r_tipo!=null && r_tipo.length()>0)
			{
				cadenaWhere.append(" and lab_barricas.descripcion = '" + r_tipo + "' " );
			}
			
			cadenaSQL.append(cadenaWhere.toString() );
			
			cadenaSQL.append(" order by lab_barricas.numero asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoBarricas = new MapeoBarricas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoBarricas.setIdCodigo(rsOpcion.getInt("bar_id"));
				mapeoBarricas.setNumero(rsOpcion.getInt("bar_num"));
				mapeoBarricas.setDescripcion(rsOpcion.getString("bar_des"));
				if (rsOpcion.getString("bar_est").contentEquals("A"))
				{
					mapeoBarricas.setEstado("Alta");
				}
				if (rsOpcion.getString("bar_est").contentEquals("B"))
				{
					mapeoBarricas.setEstado("Baja");
				}
				mapeoBarricas.setObservaciones(rsOpcion.getString("bar_obs"));
				mapeoBarricas.setEjercicio(rsOpcion.getInt("bar_eje"));
				mapeoBarricas.setCapacidad(rsOpcion.getInt("bar_cap"));

				vector.add(mapeoBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public Double obtenerCapacidadTotalBarricas()
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT sum(capacidad) bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where lab_barricas.estado = 'A' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getDouble("bar_cap");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return new Double(0);
	}

	public Double obtenerTotalBarricas()
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT count(idCodigo) bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getDouble("bar_cap");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return new Double(0);
	}

	public boolean comprobarBarricas(Integer r_numero)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT lab_barricas.numero bar_num ");
		cadenaSQL.append(" from lab_barricas ");
		cadenaSQL.append(" where numero = " + r_numero );
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String imprimirBarrica(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas.idCodigo) bar_sig ");
     	cadenaSQL.append(" FROM lab_barricas ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("bar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}


	public String guardarNuevo(MapeoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas( ");
			cadenaSQL.append(" lab_barricas.idCodigo,");			
			cadenaSQL.append(" lab_barricas.numero, ");			
			cadenaSQL.append(" lab_barricas.descripcion, ");
			cadenaSQL.append(" lab_barricas.estado, ");
			cadenaSQL.append(" lab_barricas.observaciones, ");
			cadenaSQL.append(" lab_barricas.ejercicio, ");
			cadenaSQL.append(" lab_barricas.capacidad ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getNumero()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getNumero());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getCapacidad()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getCapacidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE lab_barricas set ");
		    cadenaSQL.append(" lab_barricas.descripcion =?,");
		    cadenaSQL.append(" lab_barricas.estado =?,");
		    cadenaSQL.append(" lab_barricas.observaciones =?, ");
		    cadenaSQL.append(" lab_barricas.capacidad =? ");
			cadenaSQL.append(" where lab_barricas.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCapacidad()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCapacidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return r_mapeo.getEstado();
	}
	

	public void eliminar(MapeoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			/*
			 * TODO Borrar todos los movimientos asociados a la barrica en el resto de tablas
			 */
			cadenaSQL.append(" DELETE FROM lab_barricas ");            
			cadenaSQL.append(" WHERE lab_barricas.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoBarricas r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/barricas" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("barricas.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("barricas");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}