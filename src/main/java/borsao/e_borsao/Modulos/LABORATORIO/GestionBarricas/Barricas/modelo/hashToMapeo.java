package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo;

import java.util.HashMap; 
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoBarricas mapeo = null;
	 
	 public MapeoBarricas convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoBarricas();

		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 if (r_hash.get("numero")!=null && r_hash.get("numero").length()>0) this.mapeo.setNumero(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numero"))));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio")));
		 if (r_hash.get("capacidad")!=null && r_hash.get("capacidad").length()>0) this.mapeo.setCapacidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("capacidad")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoBarricas convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoBarricas();

		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 if (r_hash.get("numero")!=null && r_hash.get("numero").length()>0) this.mapeo.setNumero(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numero"))));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio")));
		 if (r_hash.get("capacidad")!=null && r_hash.get("capacidad").length()>0) this.mapeo.setCapacidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("capacidad")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoBarricas r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 this.hash.put("estado", this.mapeo.getEstado());
		 
		 this.hash.put("capacidad", String.valueOf(this.mapeo.getCapacidad()));
		 this.hash.put("ejercicio", String.valueOf(this.mapeo.getEjercicio()));
		 this.hash.put("numero", String.valueOf(this.mapeo.getNumero()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}