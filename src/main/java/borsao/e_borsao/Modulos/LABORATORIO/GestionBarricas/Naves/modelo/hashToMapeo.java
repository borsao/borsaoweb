package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoNaves mapeo = null;
	 
	 public MapeoNaves convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoNaves();

		 this.mapeo.setNombre(r_hash.get("nombre"));
		 
		 if (r_hash.get("totalFilas")!=null && r_hash.get("totalFilas").length()>0) this.mapeo.setTotalFilas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("totalFilas")));
		 if (r_hash.get("callesFila")!=null && r_hash.get("callesFila").length()>0) this.mapeo.setCallesFila(RutinasNumericas.formatearIntegerDeESP(r_hash.get("callesFila")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoNaves convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoNaves();

		 this.mapeo.setNombre(r_hash.get("nombre"));
		 
		 if (r_hash.get("totalFilas")!=null && r_hash.get("totalFilas").length()>0) this.mapeo.setTotalFilas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("totalFilas")));
		 if (r_hash.get("callesFila")!=null && r_hash.get("callesFila").length()>0) this.mapeo.setCallesFila(RutinasNumericas.formatearIntegerDeESP(r_hash.get("callesFila")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoNaves r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("nombre", this.mapeo.getNombre());
		 
		 this.hash.put("totalFilas", String.valueOf(this.mapeo.getTotalFilas()));
		 this.hash.put("callesFila", String.valueOf(this.mapeo.getCallesFila()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}