package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.dashBarricasGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PantallaConsultaBarricas extends Window
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	
	private Panel panelGrid = null;
	private dashBarricasGrid gridDatos = null;
	private HorizontalLayout fila4 =null;
	/*
	 * Valores
	 */
	public PantallaConsultaBarricas(String r_titulo)
	{
		this.setCaption(r_titulo);		
		this.cargarPantalla();
		this.generarGrid();
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			this.fila4 = new HorizontalLayout();
			this.fila4.setSizeFull();
			this.fila4.setSpacing(true);
			
				/*
				 * aqui metemos el grid de las seleccionadas
				 */
				controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");

		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("750px");
		
		this.cargarListeners();

	}
	
	private void cargarListeners()
	{

		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				salir();
			}
		});	
	}
	
	private void generarGrid()
	{
    	ArrayList<MapeoBarricas> vector = new ArrayList<MapeoBarricas>();
    	consultaBarricasServer cotbs = consultaBarricasServer.getInstance(CurrentUser.get());
    	vector = cotbs.datosBarricasVaciasAgrupadas();

    	gridDatos = new dashBarricasGrid(vector);
    	
    	gridDatos.getColumn("descripcion").setWidth(250);
    	gridDatos.getColumn("ejercicio").setWidth(250);
    	gridDatos.getColumn("numero").setWidth(150);
    	gridDatos.getColumn("capacidad").setWidth(250);
    	
    	panelGrid = new Panel("Barricas Disponibles");
    	panelGrid.setSizeFull(); // Shrink to fit content
    	panelGrid.setHeight("600px");
    	panelGrid.setContent(gridDatos);
    	fila4.addComponent(panelGrid);
	}

	private void salir()
	{
		close();
	}
}