package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo.MapeoNaves;

public class MapeoMovimientoBarricas extends MapeoGlobal
{
	private Integer idBarrica;
	private Integer idOrden;
	private Integer idDesgloseNave;
	
	private String tipo;
	private String depositoVaciado;
	private String depositoLlenado;
	private String destinoFinal;
	private String anada;
	private String variedad;
	private String referencia;
	private String observaciones;
	private Date fechaMovimiento;
	private MapeoNaves mapeoNaves=null;
	private MapeoDesgloseNaves mapeoDesgloseNaves=null;
	private MapeoBarricas mapeoBarricas=null;
	
	public MapeoMovimientoBarricas()
	{ 
		mapeoBarricas = new MapeoBarricas();
		mapeoNaves = new MapeoNaves();
		mapeoDesgloseNaves = new MapeoDesgloseNaves();
	}

	public String getFechaMovimiento() {
		//ejemplo cogido de articulos nuevos
		if (fechaMovimiento!= null)  return RutinasFechas.convertirDateToString(fechaMovimiento); else return null;
	}

	public void setFechaMovimiento(Date fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	public Integer getIdBarrica() {
		return idBarrica;
	}

	public void setIdBarrica(Integer idBarrica) {
		this.idBarrica = idBarrica;
	}

	public Integer getIdDesgloseNave() {
		return idDesgloseNave;
	}

	public void setIdDesgloseNave(Integer idDesgloseNave) {
		this.idDesgloseNave = idDesgloseNave;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDepositoVaciado() {
		return depositoVaciado;
	}

	public void setDepositoVaciado(String depositoVaciado) {
		this.depositoVaciado = depositoVaciado;
	}

	public String getDepositoLlenado() {
		return depositoLlenado;
	}

	public void setDepositoLlenado(String depositoLlenado) {
		this.depositoLlenado = depositoLlenado;
	}

	public String getDestinoFinal() {
		return destinoFinal;
	}

	public void setDestinoFinal(String destinoFinal) {
		this.destinoFinal = destinoFinal;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public MapeoNaves getMapeoNaves() {
		return mapeoNaves;
	}

	public void setMapeoNaves(MapeoNaves mapeoNaves) {
		this.mapeoNaves = mapeoNaves;
	}

	public MapeoDesgloseNaves getMapeoDesgloseNaves() {
		return mapeoDesgloseNaves;
	}

	public void setMapeoDesgloseNaves(MapeoDesgloseNaves mapeoDesgloseNaves) {
		this.mapeoDesgloseNaves = mapeoDesgloseNaves;
	}

	public MapeoBarricas getMapeoBarricas() {
		return mapeoBarricas;
	}

	public void setMapeoBarricas(MapeoBarricas mapeoBarricas) {
		this.mapeoBarricas = mapeoBarricas;
	}

	public Integer getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Integer idOrden) {
		this.idOrden = idOrden;
	}

	public String getAnada() {
		return anada;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

}