package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoOrdenesTrabajo mapeo = null;
	 
	 public MapeoOrdenesTrabajo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoOrdenesTrabajo();

		 this.mapeo.setTipo(r_hash.get("tipo"));
		 this.mapeo.setDepositoOrigen(r_hash.get("depositoOrigen"));
		 this.mapeo.setDepositoVaciado(r_hash.get("depositoVaciado"));
		 this.mapeo.setDepositoLlenado(r_hash.get("depositoLlenado"));
		 this.mapeo.setAnadaLlenado(r_hash.get("anadaLlenado"));
		 this.mapeo.setDestinoFinal(r_hash.get("destinoFinal"));
		 this.mapeo.setVariedad(r_hash.get("variedad"));
		 this.mapeo.setReferencia(r_hash.get("referencia"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 this.mapeo.setFechaOrden(RutinasFechas.conversionDeString(r_hash.get("fechaOrden")));
		 
		 if (r_hash.get("numero")!=null && r_hash.get("numero").length()>0) this.mapeo.setNumero(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numero")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio")));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoOrdenesTrabajo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoOrdenesTrabajo();

		 this.mapeo.setTipo(r_hash.get("tipo"));
		 this.mapeo.setDepositoOrigen(r_hash.get("depositoOrigen"));
		 this.mapeo.setDepositoVaciado(r_hash.get("depositoVaciado"));
		 this.mapeo.setDepositoLlenado(r_hash.get("depositoLlenado"));
		 this.mapeo.setAnadaLlenado(r_hash.get("anadaLlenado"));
		 this.mapeo.setDestinoFinal(r_hash.get("destinoFinal"));
		 this.mapeo.setVariedad(r_hash.get("variedad"));
		 this.mapeo.setReferencia(r_hash.get("referencia"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 this.mapeo.setFechaOrden(RutinasFechas.conversionDeString(r_hash.get("fechaOrden")));
		 
		 if (r_hash.get("numero")!=null && r_hash.get("numero").length()>0) this.mapeo.setNumero(RutinasNumericas.formatearIntegerDeESP(r_hash.get("numero")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio")));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros")));
		 
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoOrdenesTrabajo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("tipo", this.mapeo.getTipo());
		 this.hash.put("estado", this.mapeo.getEstado());
		 this.hash.put("variedad", this.mapeo.getVariedad());
		 this.hash.put("referencia", this.mapeo.getReferencia());
		 this.hash.put("depositoOrigen", this.mapeo.getDepositoOrigen());
		 this.hash.put("depositoVaciado", this.mapeo.getDepositoVaciado());
		 this.hash.put("depositoLlenado", this.mapeo.getDepositoLlenado());
		 this.hash.put("anadaLlenado", this.mapeo.getAnadaLlenado());
		 this.hash.put("destinoFinal", this.mapeo.getDestinoFinal());
		 this.hash.put("fechaOrden", RutinasFechas.convertirDateToString(this.mapeo.getFechaOrden()));
		 
		 this.hash.put("numero", String.valueOf(this.mapeo.getNumero()));
		 this.hash.put("ejercicio", String.valueOf(this.mapeo.getEjercicio()));
		 this.hash.put("litros", String.valueOf(this.mapeo.getLitros()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}