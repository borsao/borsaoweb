package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.BarricasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.view.PeticionBarricasOrdenTrabajo;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAyudaBarricas extends Window
{
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	private boolean hayGridComponentes= false;
	public VerticalLayout frameCentral = null;	
	public PeticionBarricasOrdenTrabajo app = null;
	
	public pantallaAyudaBarricas(PeticionBarricasOrdenTrabajo r_app, String r_titulo)
	{
		this.app = r_app;
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);

	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

				HorizontalLayout frameCabecera = new HorizontalLayout();
				frameCabecera.setHeight("1%");
				
				this.frameCentral = new VerticalLayout();
		
				HorizontalLayout framePie = new HorizontalLayout();

					btnBotonAVentana = new Button("Aceptar");
					btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);

					btnBotonCVentana = new Button("Cerrar");
					btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
					framePie.addComponent(btnBotonAVentana);		
					framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
					framePie.addComponent(btnBotonCVentana);		
					framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ArrayList<MapeoBarricas> vectorBarricasSeleccionadas = null;
				if (gridComponentes.getSelectedRows()!=null && gridComponentes.getSelectedRows().size()!=0)
				{
					Iterator it = null;
					Item file  =null;
					Integer valor = null;
					consultaBarricasServer cbs = consultaBarricasServer.getInstance(CurrentUser.get());
					MapeoBarricas map = new MapeoBarricas();
					
					vectorBarricasSeleccionadas = new ArrayList<MapeoBarricas>();
					
					it = gridComponentes.getSelectedRows().iterator();
					
					while (it.hasNext())
					{			
						map = (MapeoBarricas) it.next();
//						file = gridComponentes.getContainerDataSource().getItem(valor);
//						map.setIdCodigo(new Integer(file.getItemProperty("idCodigo").getValue().toString()));
//						map.setNumero(new Integer(file.getItemProperty("numero").getValue().toString()));
//						map.setDescripcion(file.getItemProperty("descsripcion").getValue().toString());
//						map.setCapacidad(new Integer(file.getItemProperty("capacidad").getValue().toString()));
						vectorBarricasSeleccionadas.add(map);
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Debes seleccionar algún registro para borrar");
				}

				app.insertar(vectorBarricasSeleccionadas);
				close();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	ArrayList<MapeoBarricas> r_vector=null;
    	
    	consultaBarricasServer ces = new consultaBarricasServer(CurrentUser.get());
    	
    	if (this.app.mapeoOrdenesTrabajo!=null && this.app.mapeoOrdenesTrabajo.getTipo().contentEquals("Llenado"))
    	{
    		r_vector=ces.datosBarricasGlobalVaciasSinUsar(null,null);
    	}
    	else
    	{
    		r_vector=ces.datosBarricasGlobalLlenas(null, this.app.mapeoOrdenesTrabajo.getVariedad(), this.app.mapeoOrdenesTrabajo.getReferencia());
    	}
    	
    	this.presentarGrid(r_vector);

	}

    private void presentarGrid(ArrayList<MapeoBarricas> r_vector)
    {
    	
    	gridComponentes = new BarricasGrid(this, r_vector);
    	
    	if (((BarricasGrid) gridComponentes).vector==null)
    	{
    		setHayGridComponentes(false);
    	}
    	else 
    	{
    		setHayGridComponentes(true);

    			this.gridComponentes.setHeight("550px");
    			this.gridComponentes.setWidth("100%");
    			this.frameCentral.addComponent(gridComponentes);
    			
    			this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
    	}
    }

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
}	