package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.view;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.CellStyleGenerator;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server.consultaDesgloseNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.modelo.MapeoMovimientoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.server.consultaMovimientoBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo.MapeoNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.server.consultaNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.OrdenesTrabajoGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server.consultaOrdenesTrabajoBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.server.consultaOrdenesTrabajoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class ProcesarMovimientosBarricas extends Window
{
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	
	private CheckBox chkTraslado = null;
	private TextField txtNumeroOrden = null;
	private TextArea txtObservaciones=null;
	private Combo cmbNave = null;
	private Combo cmbFila = null;
	private Combo cmbColumna = null;
	private Combo cmbNaveDestino = null;
	private Combo cmbFilaDestino = null;
	private Combo cmbColumnaDestino = null;

	private Label lblTipo = null;
	private Label lblReferencia = null;
	private Label lblLitros = null;
	
	private Button btnUbicar = null;
	private Button btnSeleccionar = null;
	
	private Panel panelGrid = null;
	private Grid gridDatos = null;
	private HorizontalLayout fila4 =null;
	private VerticalLayout botonesGrid = null; 
	/*
	 * Valores
	 */
	private MapeoOrdenesTrabajo mapeoOrdenesTrabajo = null;
	private OrdenesTrabajoGrid App = null;
	private Integer ubic = 0;
	
	public ProcesarMovimientosBarricas(GridPropio r_app, MapeoOrdenesTrabajo r_mapeo, String r_titulo)
	{
		this.mapeoOrdenesTrabajo = r_mapeo;
		this.App = (OrdenesTrabajoGrid) r_app;
		this.setCaption(r_titulo);
		
		this.cargarPantalla();

		if (this.mapeoOrdenesTrabajo.getTipo().contentEquals("Llenado"))
		{
			this.chkTraslado.setEnabled(false);
			this.chkTraslado.setValue(false);
			this.activarControles(false);
			this.txtNumeroOrden.setValue(r_mapeo.getNumero().toString());
		}
		else
		{
			this.chkTraslado.setEnabled(false);
			this.chkTraslado.setValue(false);
			this.activarControles(false);
			
			this.cmbFilaDestino.setEnabled(false);
			this.cmbColumnaDestino.setEnabled(false);
			this.cmbNaveDestino.setEnabled(false);
			this.txtNumeroOrden.setValue(r_mapeo.getNumero().toString());
			this.btnUbicar.setCaption("Vaciar");
		}
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
			
				this.chkTraslado= new CheckBox("Cambiar Ubicacion");
				this.chkTraslado.addStyleName(ValoTheme.CHECKBOX_SMALL);
				
				this.txtNumeroOrden=new TextField("Orden Trabajo");
				this.txtNumeroOrden.setWidth("120px");
				this.txtNumeroOrden.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.lblTipo = new Label(this.mapeoOrdenesTrabajo.getTipo());
				this.lblTipo.setCaption("Orden de ");
				this.lblReferencia= new Label(this.mapeoOrdenesTrabajo.getReferencia());
				this.lblReferencia.setCaption("Referencia ");
				this.lblLitros = new Label(this.mapeoOrdenesTrabajo.getLitros().toString());
				this.lblLitros.setCaption("Litros ");
				
				
				fila1.addComponent(this.chkTraslado);
				fila1.addComponent(this.txtNumeroOrden);
				fila1.addComponent(this.lblTipo);
				fila1.addComponent(this.lblReferencia);
				fila1.addComponent(this.lblLitros);
				fila1.setComponentAlignment(this.chkTraslado, Alignment.MIDDLE_LEFT);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
			
				this.cmbNave = new Combo("Nave");
				this.cmbNave.setNullSelectionAllowed(false);
				this.cmbNave.setNewItemsAllowed(false);
				this.cmbNave.addStyleName(ValoTheme.COMBOBOX_TINY);
				
				this.cmbFila=new Combo("Fila");				
				this.cmbFila.setWidth("120px");
				this.cmbFila.setNullSelectionAllowed(false);
				this.cmbFila.setNewItemsAllowed(false);
				this.cmbFila.addStyleName(ValoTheme.COMBOBOX_TINY);
				
				this.cmbColumna=new Combo("Columna");				
				this.cmbColumna.setWidth("120px");
				this.cmbColumna.setNullSelectionAllowed(false);
				this.cmbColumna.setNewItemsAllowed(false);
				this.cmbColumna.addStyleName(ValoTheme.COMBOBOX_TINY);
				
				fila2.addComponent(cmbNave);
				fila2.addComponent(cmbFila);
				fila2.addComponent(cmbColumna);

			fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
			
				/*
				 * aqui metemos el grid de las seleccionadas
				 */
				botonesGrid = new VerticalLayout();
				botonesGrid.setSpacing(true);
					this.btnUbicar = new Button("Ubicar");
					this.btnUbicar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
					this.btnUbicar.addStyleName(ValoTheme.BUTTON_TINY);
					this.btnUbicar.setIcon(FontAwesome.PLUS_CIRCLE);
					this.btnUbicar.setEnabled(true);
				
					this.btnSeleccionar= new Button("Seleccionar");
		    		this.btnSeleccionar.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    		this.btnSeleccionar.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.btnSeleccionar.setIcon(FontAwesome.PLUS_CIRCLE);
		    		this.btnSeleccionar.setEnabled(true);

					this.cmbNaveDestino = new Combo("Nave Destino");
					this.cmbNaveDestino.setNullSelectionAllowed(false);
					this.cmbNaveDestino.setNewItemsAllowed(false);
					this.cmbNaveDestino.addStyleName(ValoTheme.COMBOBOX_TINY);

					this.cmbFilaDestino=new Combo("Fila Destino");				
					this.cmbFilaDestino.setWidth("120px");
					this.cmbFilaDestino.setNullSelectionAllowed(false);
					this.cmbFilaDestino.setNewItemsAllowed(false);
					this.cmbFilaDestino.addStyleName(ValoTheme.COMBOBOX_TINY);
					
					this.cmbColumnaDestino=new Combo("Columna Destino");				
					this.cmbColumnaDestino.setWidth("120px");
					this.cmbColumnaDestino.setNullSelectionAllowed(false);
					this.cmbColumnaDestino.setNewItemsAllowed(false);
					this.cmbColumnaDestino.addStyleName(ValoTheme.COMBOBOX_TINY);
					
					this.txtObservaciones = new TextArea("Observaciones");
					this.txtObservaciones.setWidth("300px");
					
		    		botonesGrid.addComponent(this.btnSeleccionar);
		    		botonesGrid.addComponent(this.cmbNaveDestino);
		    		botonesGrid.addComponent(this.cmbFilaDestino);
		    		botonesGrid.addComponent(this.cmbColumnaDestino);
		    		botonesGrid.addComponent(this.txtObservaciones);
		    		botonesGrid.addComponent(this.btnUbicar);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");

		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("750px");
		
		this.cargarCombo();
		this.cargarListeners();

	}
	
	private void cargarListeners()
	{

		chkTraslado.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				/*
				 * llamaremos a generar el grid
				 */
				activarControles(chkTraslado.getValue());
			}
		});
		
		txtNumeroOrden.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				/*
				 * llamaremos a generar el grid
				 * con las barricas incluidas en la orden de trabajo
				 */
				generarGrid();
			}
		});

		cmbNave.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				cargarComboFilas();
			}
		});
		
		cmbNaveDestino.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				cargarComboFilasDestino();
			}
		});
		
		cmbColumna.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				/*
				 * En caso de traslado 
				 * llamaremos a generar el grid
				 * sabiendo fila y columna de origen 
				 * reecogemos las barricas ubicadas en existencias en dicha fila-columna
				 */
				if (chkTraslado.getValue())
				{
					generarGrid();
				}
			}
		});
		
		btnUbicar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * llamaremos a generar el movimiento
				 */
				
				if (chkTraslado.getValue())
				{
					cambiarUbicacion();
				}
				else if (mapeoOrdenesTrabajo.getTipo().contentEquals("Vaciado"))
				{
					desUbicarOrden();
				}
				else
				{
					btnSeleccionar.setCaption("Seleccionar");
					ubicarOrden();
				}
			}
		});
		
		btnSeleccionar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				if (gridDatos!=null)
				{
					if (!(gridDatos.getSelectionModel() instanceof MultiSelectionModel))
					{
						gridDatos.setSelectionMode(SelectionMode.MULTI);
						btnSeleccionar.setCaption("Cancelar Seleccion");
					}
					else
					{
						gridDatos.setSelectionMode(SelectionMode.NONE);
						btnSeleccionar.setCaption("Seleccionar");
					}
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				salir();
			}
		});	
	}
	
	private void generarGrid()
	{
		IndexedContainer container =null;
		Iterator<MapeoOrdenesTrabajoBarricas> iterator = null;
		MapeoOrdenesTrabajoBarricas mapeo = null;
		Integer total = new Integer(0);

		container = new IndexedContainer();
		container.addContainerProperty("idCodigo", Integer.class, null);
		container.addContainerProperty("Numero", Integer.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Año", Integer.class, null);
		container.addContainerProperty("Capacidad", Integer.class, null);

		if (!chkTraslado.getValue())
		{
	    	ArrayList<MapeoOrdenesTrabajoBarricas> vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
	    	consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
	    	MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
	 
	    	mapeoOrdenesTrabajoBarricas.getMapeoOrdenes().setIdCodigo(this.mapeoOrdenesTrabajo.getIdCodigo());
	    	
	    	if (this.mapeoOrdenesTrabajo.getTipo().contentEquals("Llenado")) vector = cotbs.datosOrdenesTrabajoBarricasGlobalPendientes(mapeoOrdenesTrabajoBarricas);
	    	else vector = cotbs.datosOrdenesTrabajoBarricasGlobalLlenas(mapeoOrdenesTrabajoBarricas);

	    	iterator = vector.iterator();
	    	
	    	while (iterator.hasNext())
	    	{
	    		Object newItemId = container.addItem();
	    		Item file = container.getItem(newItemId);
	    		
	    		mapeo= (MapeoOrdenesTrabajoBarricas) iterator.next();
	    		total = total + mapeo.getMapeoBarricas().getCapacidad();
	    		
	    		file.getItemProperty("idCodigo").setValue(mapeo.getMapeoBarricas().getIdCodigo());
	    		file.getItemProperty("Numero").setValue(mapeo.getMapeoBarricas().getNumero());
	    		file.getItemProperty("Descripcion").setValue(mapeo.getMapeoBarricas().getDescripcion());
	    		file.getItemProperty("Año").setValue(mapeo.getMapeoBarricas().getEjercicio());
	    		file.getItemProperty("Capacidad").setValue(mapeo.getMapeoBarricas().getCapacidad());
	    	}
		}
		else
		{
			
		}
		
    	gridDatos= new Grid(container);
    	gridDatos.setSelectionMode(SelectionMode.NONE);
    	gridDatos.setSizeFull();
    	gridDatos.setWidth("100%");
    	gridDatos.addStyleName("smallgrid");
    	
    	gridDatos.getColumn("idCodigo").setHidden(true);
    	
    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
    		@Override
    		public String getStyle(CellReference cellReference) {
    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
    				return "Rcell-normal";
    			}
    			return null;
    		}
    	});
    	
    	this.calcularTotalGrid();
    	
    	panelGrid = new Panel("Barricas Asignadas");
    	panelGrid.setSizeUndefined(); // Shrink to fit content
    	panelGrid.setHeight("450px");
    	panelGrid.setWidth("100%");
    	panelGrid.setContent(gridDatos);
    	fila4.addComponent(panelGrid);
    	fila4.addComponent(botonesGrid);
	}

	private void cambiarUbicacion()
	{
		
	}
	
	private void ubicarOrden()
	{
		if (todoEnOrden())
		{
			/*
			 * Proceso de generacion movimientos y aactualizacion de existencias 
			 */
			Iterator it = null;
			Item file  =null;
			Integer valor = null;
			Connection con = null;
			String rdo = null;
			
			try 
			{
				
				con = connectionManager.getInstance().establecerConexion();
				con.setAutoCommit(false);

				consultaMovimientoBarricasServer cmbs = consultaMovimientoBarricasServer.getInstance(CurrentUser.get());
				MapeoMovimientoBarricas mapeoMovimientos = new MapeoMovimientoBarricas();
				
				mapeoMovimientos.setAnada(this.mapeoOrdenesTrabajo.getAnadaLlenado());
				mapeoMovimientos.setVariedad(this.mapeoOrdenesTrabajo.getVariedad());
				mapeoMovimientos.setDepositoLlenado(this.mapeoOrdenesTrabajo.getDepositoLlenado());
				mapeoMovimientos.setDepositoVaciado(this.mapeoOrdenesTrabajo.getDepositoVaciado());
				mapeoMovimientos.setDestinoFinal(this.mapeoOrdenesTrabajo.getDestinoFinal());
				mapeoMovimientos.setFechaMovimiento(RutinasFechas.conversionDeString(RutinasFechas.fechaActual()));
				mapeoMovimientos.setObservaciones(this.txtObservaciones.getValue());
				mapeoMovimientos.setReferencia(this.mapeoOrdenesTrabajo.getReferencia());
				mapeoMovimientos.setTipo(this.mapeoOrdenesTrabajo.getTipo());
				mapeoMovimientos.setIdOrden(this.mapeoOrdenesTrabajo.getIdCodigo());
				
				mapeoMovimientos.setIdDesgloseNave(this.ubic);
				
				it = gridDatos.getSelectedRows().iterator();
				
				while (it.hasNext())
				{			
					valor = (Integer) it.next();
					file = gridDatos.getContainerDataSource().getItem(valor);
					
					mapeoMovimientos.setIdCodigo(cmbs.obtenerSiguienteTransaccional(con));
					mapeoMovimientos.setIdBarrica(new Integer(file.getItemProperty("idCodigo").getValue().toString()));
					
					rdo = cmbs.guardarNuevoTransaccional(con, mapeoMovimientos);
					if (rdo!=null)
					{
						Notificaciones.getInstance().mensajeError(rdo);
						con.rollback();
						break;
					}
				}
				
				if (rdo==null)
				{
					if (comprobarMovimientos())
					{
						/*
						 * confirmo los cambios porque ha ido todo bien
						 */
						con.commit();
						/*
						 * regenero el grid para ver solo las pendientes de asignar
						 */
						if (panelGrid!=null && gridDatos!=null)
						{
							fila4.removeComponent(panelGrid);
							fila4.removeComponent(botonesGrid);
							gridDatos = null;
							panelGrid = null;
						}
						generarGrid();
					}
					else
					{
						/*
						 * cierro la orden porque no quedan mas y me salgo
						 */
						consultaOrdenesTrabajoServer cots = consultaOrdenesTrabajoServer.getInstance(CurrentUser.get());
						rdo = cots.cerrarOrdenTransaccional(con, mapeoMovimientos.getIdOrden());
						if (rdo!=null)
						{
							/*
							 * algo ha ido mal, asi que anulo los ultimos cambios
							 */
							con.rollback();
							Notificaciones.getInstance().mensajeError("Problemas al cerrar la orden");
						}
						else
						{
							/*
							 * ha ido todo bien con lo que confirmo los cambios
							 */
							con.commit();
							Notificaciones.getInstance().mensajeError("Orden terminada.");
						}
						
						salir();
					}
				}

			} 
			catch (SQLException e) 
			{
				try 
				{
					con.rollback();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}
	}
	
	private void desUbicarOrden()
	{
		/*
		 * Proceso de generacion movimientos y aactualizacion de existencias 
		 */
		Iterator it = null;
		Item file  =null;
		Integer valor = null;
		Connection con = null;
		String rdo = null;
		
		try 
		{
			
			con = connectionManager.getInstance().establecerConexion();
			con.setAutoCommit(false);

			consultaMovimientoBarricasServer cmbs = consultaMovimientoBarricasServer.getInstance(CurrentUser.get());
			MapeoMovimientoBarricas mapeoMovimientos = new MapeoMovimientoBarricas();
			
			mapeoMovimientos.setAnada(this.mapeoOrdenesTrabajo.getAnadaLlenado());
			mapeoMovimientos.setVariedad(this.mapeoOrdenesTrabajo.getVariedad());
			mapeoMovimientos.setDepositoLlenado(this.mapeoOrdenesTrabajo.getDepositoLlenado());
			mapeoMovimientos.setDepositoVaciado(this.mapeoOrdenesTrabajo.getDepositoVaciado());
			mapeoMovimientos.setDestinoFinal(this.mapeoOrdenesTrabajo.getDestinoFinal());
			mapeoMovimientos.setFechaMovimiento(RutinasFechas.conversionDeString(RutinasFechas.fechaActual()));
			mapeoMovimientos.setObservaciones(this.txtObservaciones.getValue());
			mapeoMovimientos.setReferencia(this.mapeoOrdenesTrabajo.getReferencia());
			mapeoMovimientos.setTipo(this.mapeoOrdenesTrabajo.getTipo());
			mapeoMovimientos.setIdOrden(this.mapeoOrdenesTrabajo.getIdCodigo());
			mapeoMovimientos.setIdDesgloseNave(this.ubic);
			
			it = gridDatos.getSelectedRows().iterator();
			
			while (it.hasNext())
			{			
				valor = (Integer) it.next();
				file = gridDatos.getContainerDataSource().getItem(valor);
				
				mapeoMovimientos.setIdCodigo(cmbs.obtenerSiguienteTransaccional(con));
				mapeoMovimientos.setIdBarrica(new Integer(file.getItemProperty("idCodigo").getValue().toString()));
				
				rdo = cmbs.guardarNuevoTransaccional(con, mapeoMovimientos);
				if (rdo!=null)
				{
					Notificaciones.getInstance().mensajeError(rdo);
					con.rollback();
					break;
				}
			}
			
			if (rdo==null)
			{
				if (comprobarMovimientos())
				{
					/*
					 * confirmo los cambios porque ha ido todo bien
					 */
					con.commit();
					/*
					 * regenero el grid para ver solo las pendientes de asignar
					 */
					if (panelGrid!=null && gridDatos!=null)
					{
						fila4.removeComponent(panelGrid);
						fila4.removeComponent(botonesGrid);
						gridDatos = null;
						panelGrid = null;
					}
					generarGrid();
				}
				else
				{
					/*
					 * cierro la orden porque no quedan mas y me salgo
					 */
					consultaOrdenesTrabajoServer cots = consultaOrdenesTrabajoServer.getInstance(CurrentUser.get());
					rdo = cots.cerrarOrdenTransaccional(con, mapeoMovimientos.getIdOrden());
					if (rdo!=null)
					{
						/*
						 * algo ha ido mal, asi que anulo los ultimos cambios
						 */
						con.rollback();
						Notificaciones.getInstance().mensajeError("Problemas al cerrar la orden");
					}
					else
					{
						/*
						 * ha ido todo bien con lo que confirmo los cambios
						 */
						con.commit();
						Notificaciones.getInstance().mensajeError("Orden terminada.");
					}
					
					salir();
				}
			}

		} 
		catch (SQLException e) 
		{
			try 
			{
				con.rollback();
			} 
			catch (SQLException e1) 
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	private void activarControles(boolean r_activar)
	{
		this.txtNumeroOrden.setEnabled(!r_activar);
		this.cmbFila.setEnabled(r_activar);
		this.cmbColumna.setEnabled(r_activar);
		this.cmbNave.setEnabled(r_activar);
	}
	
	private void cargarCombo()
	{
		ArrayList<MapeoNaves> vector = null;
		Iterator<MapeoNaves> iter = null;
		MapeoNaves mapeo = null;
		consultaNavesServer cns = consultaNavesServer.getInstance(CurrentUser.get());
		
		cmbNave.removeAllItems();
		cmbNaveDestino.removeAllItems();
		
		vector = cns.datosNavesGlobal(null);
		iter =vector.iterator();
		
		while (iter.hasNext())
		{
			mapeo = iter.next();
			cmbNave.addItem(mapeo.getNombre());
			cmbNaveDestino.addItem(mapeo.getNombre());
		}
	}

	private void cargarComboFilasDestino()
	{
		ArrayList<MapeoNaves> vector = null;
		Iterator<MapeoNaves> iter = null;
		MapeoNaves mapeo = null;
		MapeoNaves mapeoNaves = null;
		
		consultaNavesServer cns = consultaNavesServer.getInstance(CurrentUser.get());
		mapeoNaves=new MapeoNaves();
		mapeoNaves.setNombre(cmbNaveDestino.getValue().toString());
		
		cmbFilaDestino.removeAllItems();
		cmbColumnaDestino.removeAllItems();
		
		vector = cns.datosNavesGlobal(mapeoNaves);
		iter =vector.iterator();
		
		while (iter.hasNext())
		{
			mapeo = iter.next();
			for (int i = 0;i<mapeo.getTotalFilas(); i++)
			{
				cmbFilaDestino.addItem(i+1);	
			}
		}
		cmbColumnaDestino.addItem("A");
		cmbColumnaDestino.addItem("B");
	}
	
	
	private void cargarComboFilas()
	{
		ArrayList<MapeoNaves> vector = null;
		Iterator<MapeoNaves> iter = null;
		MapeoNaves mapeo = null;
		MapeoNaves mapeoNaves = null;
		
		consultaNavesServer cns = consultaNavesServer.getInstance(CurrentUser.get());
		mapeoNaves=new MapeoNaves();
		mapeoNaves.setNombre(cmbNave.getValue().toString());
		
		cmbFila.removeAllItems();
		cmbColumna.removeAllItems();
		
		vector = cns.datosNavesGlobal(mapeoNaves);
		iter =vector.iterator();
		
		while (iter.hasNext())
		{
			mapeo = iter.next();
			for (int i = 0;i<mapeo.getTotalFilas(); i++)
			{
				cmbFila.addItem(i+1);	
			}
		}
		
		cmbColumna.addItem("A");
		cmbColumna.addItem("B");
	}
	
	private void calcularTotalGrid()
	{
		Integer totalCapacidad = new Integer(0);
		Integer q1Value = new Integer(0);
		
    	if (gridDatos.getFooterRowCount()==0)
    	{
    		gridDatos.appendFooterRow();
    	}

    	gridDatos.getFooterRow(0).getCell("Descripcion").setText("Total:");
        
    	Indexed indexed = gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	q1Value = (Integer) item.getItemProperty("Capacidad").getValue();
    		if (q1Value!=null) totalCapacidad+= q1Value;
        }
        
        gridDatos.getFooterRow(0).getCell("Capacidad").setText(RutinasNumericas.formatearIntegerDigitos(totalCapacidad,0).toString());
        gridDatos.getFooterRow(0).getCell("Capacidad").setStyleName("Rcell-pie");
	}
	private void salir()
	{
		if (this.App!=null) ((OrdenesTrabajoGrid) this.App).app.actualizarDatos();
		close();
	}
	
	private boolean todoEnOrden()
	{
		boolean resultado = true;
		String mensaje = null;
		
		if (gridDatos.getSelectedRows()==null || gridDatos.getSelectedRows().size()==0)
		{
			mensaje = "Debes seleccionar barricas a ubicar.";
			resultado = false;
		}
		else if (cmbNaveDestino.getValue()==null)
		{
			mensaje = "Debes rellenar la nave de destino";
			this.cmbNaveDestino.focus();
			resultado = false;
		}
		else if (cmbFilaDestino.getValue()==null)
		{
			mensaje = "Debes rellenar la fila de destino";
			this.cmbFilaDestino.focus();
			resultado = false;
		}
		else if (cmbColumnaDestino.getValue()==null)
		{
			mensaje = "Debes rellenar la columna de destino";
			this.cmbColumnaDestino.focus();
			resultado = false;
		}
		else 
		{
			this.ubic = this.obtenerUbicacionAlmacen();
			
			if (this.ubic==0)
			{
				mensaje = "Problemas con la ubicacion seleccionada.";
				resultado = false;
			}
		}
		
		if (mensaje!=null) Notificaciones.getInstance().mensajeError(mensaje);
		
		return resultado;		
	}
	
	private boolean comprobarMovimientos()
	{
		boolean hayMas = true;
		
		ArrayList<MapeoOrdenesTrabajoBarricas> vector = new ArrayList<MapeoOrdenesTrabajoBarricas>();
    	consultaOrdenesTrabajoBarricasServer cotbs = consultaOrdenesTrabajoBarricasServer.getInstance(CurrentUser.get());
    	MapeoOrdenesTrabajoBarricas mapeoOrdenesTrabajoBarricas = new MapeoOrdenesTrabajoBarricas();
 
    	mapeoOrdenesTrabajoBarricas.getMapeoOrdenes().setIdCodigo(this.mapeoOrdenesTrabajo.getIdCodigo());
    	vector = cotbs.datosOrdenesTrabajoBarricasGlobalPendientes(mapeoOrdenesTrabajoBarricas);
    	
    	if (vector==null || vector.isEmpty()) hayMas=false;
    	
    	return hayMas;
	}
	
	private Integer obtenerUbicacionAlmacen()
	{
		
		ArrayList<MapeoDesgloseNaves> vector = new ArrayList<MapeoDesgloseNaves>();
		MapeoDesgloseNaves mapeoDesgloseNaves = new MapeoDesgloseNaves();
    	
		consultaDesgloseNavesServer cdns = consultaDesgloseNavesServer.getInstance(CurrentUser.get());
 
    	mapeoDesgloseNaves.setNombre(this.cmbNaveDestino.getValue().toString());
    	mapeoDesgloseNaves.setFilas(new Integer(this.cmbFilaDestino.getValue().toString()));
    	if (this.cmbColumnaDestino.getValue().toString().contentEquals("A"))
    		mapeoDesgloseNaves.setCalle(1);
    	else if (this.cmbColumnaDestino.getValue().toString().contentEquals("B"))
    		mapeoDesgloseNaves.setCalle(2);
    	
    	vector = cdns.datosDesgloseNavesGlobal(mapeoDesgloseNaves);
    	
    	if (vector==null || vector.isEmpty()) return 0;
    	else 
    	{
    		MapeoDesgloseNaves map = (MapeoDesgloseNaves) vector.get(0);
    		return map.getIdCodigo();
    	}
	}
}