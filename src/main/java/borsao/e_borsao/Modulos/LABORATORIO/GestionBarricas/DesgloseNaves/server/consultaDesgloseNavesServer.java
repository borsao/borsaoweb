package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.server.consultaNavesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDesgloseNavesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDesgloseNavesServer instance;
	
	public consultaDesgloseNavesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDesgloseNavesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDesgloseNavesServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoDesgloseNaves> datosDesgloseNavesGlobal(MapeoDesgloseNaves r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoDesgloseNaves> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoDesgloseNaves mapeoDesgloseNaves=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_nav_des.idCodigo nav_id,");
		cadenaSQL.append(" lab_barricas_nav_des.fila nav_fil,");
		cadenaSQL.append(" lab_barricas_nav_des.calle nav_col, ");
		cadenaSQL.append(" lab_barricas_nav.idCodigo nav_nav,");		
		cadenaSQL.append(" lab_barricas_nav.nombre nav_nom");
		cadenaSQL.append(" from lab_barricas_nav_des ");
		cadenaSQL.append(" inner join lab_barricas_nav on lab_barricas_nav_des.idNave = lab_barricas_nav.idCodigo");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getFilas()!=null && r_mapeo.getFilas()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fila = " + r_mapeo.getFilas() + " ");
				}
				if (r_mapeo.getCalle()!=null && r_mapeo.getCalle()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" calle = " + r_mapeo.getCalle());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idNave,fila, calle asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoDesgloseNaves>();
			
			while(rsOpcion.next())
			{
				mapeoDesgloseNaves = new MapeoDesgloseNaves();
				/*
				 * recojo mapeo operarios
				 */
				mapeoDesgloseNaves.setIdCodigo(rsOpcion.getInt("nav_id"));
				mapeoDesgloseNaves.setIdNave(rsOpcion.getInt("nav_nav"));
				mapeoDesgloseNaves.setNombre(rsOpcion.getString("nav_nom"));
				mapeoDesgloseNaves.setFilas(rsOpcion.getInt("nav_fil"));
				mapeoDesgloseNaves.setCalle(rsOpcion.getInt("nav_col"));

				vector.add(mapeoDesgloseNaves);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_nav_des.idCodigo) nav_sig ");
     	cadenaSQL.append(" FROM lab_barricas_nav_des ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("nav_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public boolean hayDatos(Integer r_idNave)
	{
		ResultSet rsOpcion = null;		
		boolean hayDatos = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(lab_barricas_nav_des.idCodigo) nav_tot ");
     	cadenaSQL.append(" FROM lab_barricas_nav_des ");
     	cadenaSQL.append(" WHERE lab_barricas_nav_des.idNave =  " + r_idNave);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("nav_tot")>0 )
				{
					hayDatos = true;
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			hayDatos=true;
		}
		return hayDatos;
	}


	public String guardarNuevo(MapeoDesgloseNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO lab_barricas_nav_des( ");
			cadenaSQL.append(" lab_barricas_nav_des.idCodigo,");			
			cadenaSQL.append(" lab_barricas_nav_des.fila, ");
			cadenaSQL.append(" lab_barricas_nav_des.calle, ");			
			cadenaSQL.append(" lab_barricas_nav_des.idNave) VALUES (");
			cadenaSQL.append(" ?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    Integer id = this.obtenerSiguiente();
		    preparedStatement.setInt(1, id);
		    
		    if (r_mapeo.getFilas()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getFilas());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getCalle()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getCalle());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    
		    if (r_mapeo.getIdNave()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getIdNave());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public Integer obtenerIdNavePorNombre(String r_nombre)
	{
		consultaNavesServer cns = new consultaNavesServer(CurrentUser.get());
		Integer id = cns.obtenerId(r_nombre);
		return id;
	}
	public Integer obtenerIdNavePorDesglose(Integer r_desglose)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_nav_des.idNave nav_nav ");
     	cadenaSQL.append(" FROM lab_barricas_nav_des ");
     	cadenaSQL.append(" where lab_barricas_nav_des.idCodigo = " + r_desglose);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("nav_nav");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}

		return null;
	}

	public String guardarCambios(MapeoDesgloseNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE lab_barricas_nav_des set ");
		    cadenaSQL.append(" lab_barricas_nav_des.fila =?,");
		    cadenaSQL.append(" lab_barricas_nav_des.calle =?, ");
		    cadenaSQL.append(" lab_barricas_nav_des.idNave =? ");
			cadenaSQL.append(" where lab_barricas_nav_des.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getFilas()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getFilas());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getCalle()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCalle());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    preparedStatement.setInt(3, r_mapeo.getIdNave());
		    preparedStatement.setInt(4, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoDesgloseNaves r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM lab_barricas_nav_des ");            
			cadenaSQL.append(" WHERE lab_barricas_nav_des.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoBarricas r_mapeo)
	{
		return null;
	}
	
	
	
}