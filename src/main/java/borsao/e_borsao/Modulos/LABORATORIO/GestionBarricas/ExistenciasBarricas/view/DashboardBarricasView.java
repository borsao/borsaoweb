package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.view;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.PanelCaptionBarToggler;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.MapeoValores;
import borsao.e_borsao.ClasesPropias.graficas.queso;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoEstructuraAlmacen;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoExistenciasBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoGeneralDashboardBarrica;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.dashBarricasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.dashExistenciasGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.dashMapaAlmacenGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.server.consultaExistenciasBarricasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class DashboardBarricasView extends CssLayout implements View {

	public static final String VIEW_NAME = "DashBoard Barricas";
	private final String titulo = "CUADRO MANDO BARRICAS";
	
	private CssLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcRefresh = null;
    private Button opcSalir = null;
    
    private Panel panelResumen =null;
    private Panel panelVentas =null;
    private VerticalLayout panelVentas0 =null;
    private Panel panelVentas1 =null;
    private Panel panelVentas2 =null;
    private Panel panelProduccion =null;
    private Panel panelPlanificacion =null;
    private Panel panelArticulos =null;

    private Panel panelGridBarricas = null;
    private Panel panelGridEstructura = null;
    private Panel panelGridReferencias = null;
    private Panel panelGridAñadas = null;
    
    private GridLayout centralTop = null;
    private GridLayout centralTop2 = null;
    private GridLayout centralMiddle = null;
    private GridLayout centralMiddle1 = null;
    private GridLayout centralMiddle2 = null;
    private GridLayout centralBottom2 = null;
    private HorizontalLayout centralBottom = null;
    
	private MapeoGeneralDashboardBarrica mapeo=null;

	private boolean prevision = false;
	
	private dashExistenciasGrid gridDatosAñada=null;
	private Grid gridDatosReferencia=null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */


    public DashboardBarricasView() 
    {
    }

    public void enter(ViewChangeEvent event) 
	{
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		
		this.cargarListeners();
//		this.ejecutarTimer();
		this.procesar();
	}

    private void cargarPantalla() 
    {
    	
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new CssLayout();
        this.barAndGridLayout.setSizeUndefined();
        this.barAndGridLayout.setStyleName("crud-main-layout");


		    	this.topLayoutL = new HorizontalLayout();
		    	this.topLayoutL.setSpacing(true);
		    	this.topLayoutL.setSizeUndefined();
		    	this.topLayoutL.setMargin(true);
		    	
			    	this.opcSalir= new Button("Salir");
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
			    	
			    	this.opcRefresh= new Button("Refresh");
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_PRIMARY);
			    	this.opcRefresh.addStyleName(ValoTheme.BUTTON_TINY);
			    	this.opcRefresh.setIcon(FontAwesome.REFRESH);

			    	this.lblTitulo= new Label();
			    	this.lblTitulo.setValue(titulo);
			    	this.lblTitulo.setStyleName("labelTituloVentas");
			    	
		    	if (LecturaProperties.semaforos)
		    	{
		    		this.topLayoutL.addComponent(this.opcRefresh);
		    		this.topLayoutL.setComponentAlignment(this.opcRefresh,  Alignment.BOTTOM_LEFT);
		    	}
		    	
		    	this.topLayoutL.addComponent(this.opcSalir);
		    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
		    	this.topLayoutL.addComponent(this.lblTitulo);
		    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	panelArticulos = new Panel("Padres del seleccionado");
		    panelArticulos.setSizeUndefined();
		    panelArticulos.setVisible(false);
		    panelArticulos.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelArticulos );

		    	this.centralTop = new GridLayout(4,1);
		    	this.centralTop.setSpacing(true);
		    	this.centralTop.setWidth("100%");
		    	this.centralTop.setMargin(true);

		    	panelArticulos.setContent(centralTop);
		    	
	    	panelResumen = new Panel("Resumen ");
		    panelResumen.setSizeUndefined();
		    panelResumen.addStyleName("showpointer");

			    this.centralTop2 = new GridLayout(3,1);
			    this.centralTop2.setWidth("100%");
			    this.centralTop2.setMargin(true);
			    this.centralTop2.setSpacing(true);
		    	panelResumen.setContent(centralTop2);

/*
 * Deberá cargar tantos paneles como naves 
 * ya veremos como		    	
 */
	    	panelVentas = new Panel("Resumen Añadas");
		    panelVentas.setWidth("100%");

		    	panelVentas0 = new VerticalLayout();
			    panelVentas0.setWidth("100%");

			    	panelVentas1 = new Panel("");
				    panelVentas1.setWidth("100%");
		
				    	this.centralMiddle = new GridLayout(2,1);
				    	this.centralMiddle.setWidth("100%");
				    	this.centralMiddle.setMargin(true);
				    	this.centralMiddle.setSpacing(true);
				    	
			    	panelVentas1.setContent(centralMiddle);
			    	
			    	panelVentas2 = new Panel("");
				    panelVentas2.setWidth("100%");
		
				    	this.centralMiddle1 = new GridLayout(1,1);
				    	this.centralMiddle1.setWidth("100%");
				    	this.centralMiddle1.setMargin(true);
				    	this.centralMiddle1.setSpacing(true);
				    	
			    	panelVentas2.setContent(centralMiddle1);
			    	
			    	panelVentas0.addComponent(panelVentas1);
//			    	panelVentas0.addComponent(panelVentas2);
			    	
	    	panelVentas.setContent(panelVentas0);
	    	panelVentas.getContent().setVisible(true);

	    	this.centralMiddle2 = new GridLayout(2,1);
	    	this.centralMiddle2.setSizeUndefined();
	    	this.centralMiddle2.setMargin(true);


	    	panelProduccion = new Panel("Nave Vieja");
		    panelProduccion.setWidth("100%");
	    	
		    	this.centralBottom = new HorizontalLayout();
		    	this.centralBottom.setWidth("100%");
		    	this.centralBottom.setMargin(true);

		    	panelProduccion.setContent(centralBottom);

	    	panelPlanificacion = new Panel("Datos Planificación");
		    panelPlanificacion.setWidth("100%");
		    panelProduccion.addStyleName("showpointer");
		    new PanelCaptionBarToggler<Panel>( panelPlanificacion );
		    
	    	
		    	this.centralBottom2 = new GridLayout(4,1);
		    	this.centralBottom2.setWidth("100%");
		    	this.centralBottom2.setMargin(true);

		    	panelPlanificacion.setContent(centralBottom2);
		    	panelPlanificacion.getContent().setVisible(false);
		    	panelPlanificacion.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);
		    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.centralTop);
    	this.barAndGridLayout.addComponent(this.panelResumen);
//    	this.barAndGridLayout.addComponent(this.centralMiddle2);
    	this.barAndGridLayout.addComponent(this.panelVentas);
    	this.barAndGridLayout.addComponent(this.panelProduccion);
    	
    	this.addComponent(this.barAndGridLayout);
    	this.setResponsive(true);
        
    }

    private void cargarListeners() 
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eBorsao.get().mainScreen.menu.verMenu(true);
    			destructor();
    		}
    	});

    	this.opcRefresh.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			refrescar();
    		}
    	});
    }
    
    private void procesar()
    {
    	eliminarDashBoards(true);
    	cargarDashboards();
    }
    
    public void eliminarDashBoards(boolean r_todos)
    {
    	centralTop2.removeAllComponents();
    	centralMiddle.removeAllComponents();
    	centralMiddle1.removeAllComponents();
    	centralMiddle2.removeAllComponents();
    	centralBottom.removeAllComponents();
    	panelResumen.setCaption("Resumen Año");
    	panelVentas.setCaption("Detalle Añadas");;
    	panelProduccion.setCaption("Ubicación Barricas");
    	    	
    }
    
    public void cargarDashboards() 
    {
    	ejecutarPadres();
    }

    private void ejecutarPadres()
    {
    	this.mapeo = consultaExistenciasBarricasServer.getInstance(CurrentUser.get()).recuperarValoresEstadisticos();
//		cargarAcciones();
		cargarResumenes();
//		cargarVentas();
//		cargarProduccion();
    }
    
    private void cargarResumenes()
    {
		this.cargarResumen();
		this.pieTotales();
		this.resumenStockAñada();
//		this.resumenStockReferencia(null);
//		this.cargarResumenStock();
//		this.cargarDatosPlanificacion();
		
		panelResumen.setCaption("Resumen Capacidad ");
    }
    
//    private void cargarVentas()
//    {
//		this.cargarResumenVentas();
//		this.cargarResumenVentasAnterior();
//		this.cargarResumenVentasAnterior2();
//		this.cargarResumenVentasAnterior3();
//
//    	this.evolucionVentas();
//    	this.evolucionVentasAcumuladas();
//    	this.resumenVentaPaises();
//    	panelVentas.setCaption("Datos Ventas del " + articuloPadre + " " + descripcionArticuloPadre);
//    }
//
//    private void cargarProduccion()
//    {
//		this.cargarResumenProduccion();
//		this.cargarResumenProduccionAnterior();
//		this.cargarResumenProduccionAnterior2();
//		this.cargarResumenProduccionAnterior3();
//		
//    	panelProduccion.setCaption("Datos Produccion del " + articuloPadre + " " + descripcionArticuloPadre);
//    }
    private void cargarResumen()
    {
    	String anchoLabelTitulo = "140px";
    	String anchoLabelDatos = "120px";
    	
    	Label lblCuantas= null;
    	Label lblProduccion= null;
    	Label lblPlanificacion= null;
    	Label lblVenta= null;

    	Button lblValorProduccion= null;
    	Button lblValorPlanificacion= null;
    	Button lblValorVenta= null;
    	Button lblValorCuantas= null;
    	    	
    	Panel panel = new Panel("RESUMEN GLOBAL ");
    	panel.setSizeUndefined(); // Shrink to fit content

    	// Create the content
    	VerticalLayout content = new VerticalLayout();

    	HorizontalLayout lin = new HorizontalLayout();
    	lin.setSpacing(true);
    	HorizontalLayout lin2 = new HorizontalLayout();
    	lin2.setSpacing(true);
    	HorizontalLayout lin3 = new HorizontalLayout();
    	lin3.setSpacing(true);
    	HorizontalLayout lin4 = new HorizontalLayout();
    	lin4.setSpacing(true);
    	
    	lblProduccion= new Label();
    	lblProduccion.setValue("Capacidad (ltrs.): ");
    	lblProduccion.setWidth(anchoLabelTitulo);    	
    	lblProduccion.addStyleName("lblTitulo");

    	lblValorProduccion= new Button();
    	lblValorProduccion.setWidth(anchoLabelDatos);
    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("capacidadTotal")));
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorProduccion.addStyleName("lblDatosBlanco");
    	
//    	lblValorProduccion.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),false);
//				
//			}
//		});

    	lin.addComponent(lblProduccion);
    	lin.addComponent(lblValorProduccion);
    	lin.setComponentAlignment(lblProduccion,Alignment.MIDDLE_LEFT);
    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
    	
    	lblPlanificacion= new Label();
    	lblPlanificacion.setValue("Disponible (ltrs.): ");
    	lblPlanificacion.setWidth(anchoLabelTitulo);    	
    	lblPlanificacion.addStyleName("lblTitulo");

    	lblValorPlanificacion= new Button();
    	lblValorPlanificacion.setWidth(anchoLabelDatos);
    	lblValorPlanificacion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("capacidadDisponible")));
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorPlanificacion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorPlanificacion.addStyleName("lblDatos");
		lblValorPlanificacion.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					consultaBarricas();					
				}
			});
    	lin2.addComponent(lblPlanificacion);
    	lin2.addComponent(lblValorPlanificacion);
    	lin2.setComponentAlignment(lblPlanificacion,Alignment.MIDDLE_LEFT);
    	lin2.setComponentAlignment(lblValorPlanificacion, Alignment.MIDDLE_RIGHT);
    	
    	lblVenta= new Label();
    	lblVenta.setValue("Existencias (ltrs.): ");
    	lblVenta.setWidth(anchoLabelTitulo);    	
    	lblVenta.addStyleName("lblTitulo");

    	lblValorVenta= new Button();
    	lblValorVenta.setWidth(anchoLabelDatos);
    	lblValorVenta.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("capacidadOcupada")));
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_TINY);
    	lblValorVenta.addStyleName(ValoTheme.BUTTON_BORDERLESS);
    	lblValorVenta.addStyleName("lblDatosBlanco");
//		lblValorVenta.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				pantallaVentas();
//				
//			}
//		});
    	
    	lin3.addComponent(lblVenta);
    	lin3.addComponent(lblValorVenta);
    	lin3.setComponentAlignment(lblVenta,Alignment.MIDDLE_LEFT);
    	lin3.setComponentAlignment(lblValorVenta, Alignment.MIDDLE_RIGHT);
    	
	    	lblCuantas= new Label();
	    	lblCuantas.setValue("N. Barricas (uds.): ");
	    	lblCuantas.setWidth(anchoLabelTitulo);    	
	    	lblCuantas.addStyleName("lblTitulo");
	
	    	lblValorCuantas= new Button();
	    	lblValorCuantas.setWidth(anchoLabelDatos);
	    	lblValorCuantas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("barricasTotales")));
	    	lblValorCuantas.addStyleName(ValoTheme.BUTTON_TINY);
	    	lblValorCuantas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
	    	lblValorCuantas.addStyleName("lblDatosBlanco");

    	lin4.addComponent(lblCuantas);
    	lin4.addComponent(lblValorCuantas);
    	lin4.setComponentAlignment(lblCuantas,Alignment.MIDDLE_LEFT);
    	lin4.setComponentAlignment(lblValorCuantas, Alignment.MIDDLE_RIGHT);

    	content.addComponent(lin);
    	content.addComponent(lin2);
    	content.addComponent(lin3);
    	content.addComponent(lin4);
    	
    	content.setSizeUndefined(); // Shrink to fit
    	content.setSpacing(false);
    	content.setMargin(true);
    	panel.setContent(content);
    	
    	centralTop2.addComponent(panel,0,0);
    }
    
    
//    private void cargarResumenProduccion()
//    {
//    	String anchoLabelTitulo = "140px";
//    	String anchoLabelDatos = "140px";
//    	
//    	Label lblProduccion= null;
//    	Label lblMediaMes= null;
//    	Label lblVecesPlanificadas= null;
//    	Label lblEmbotelladas= null;
//    	Label lblEtiquetadas= null;
//    	Label lblRetrabajadas= null;
//    	
//    	Button lblValorProduccion= null;
//    	Button lblValorMediaMes= null;
//    	Button lblValorVecesPlanificadas= null;
//    	Button lblValorEmbotelladas= null;
//    	Button lblValorEtiquetadas= null;
//    	Button lblValorRetrabajadas= null;
//    	
//    	Panel panel = new Panel("Resumen Produccion / Planificacion " + String.valueOf(new Integer(txtEjercicio.getValue())));
//    	panel.setSizeUndefined(); // Shrink to fit content
//
//    	// Create the content
//    	VerticalLayout content = new VerticalLayout();
//
//    	HorizontalLayout lin = new HorizontalLayout();
//    	lin.setSpacing(true);
//    	HorizontalLayout lin2 = new HorizontalLayout();
//    	lin2.setSpacing(true);
//    	HorizontalLayout lin3 = new HorizontalLayout();
//    	lin3.setSpacing(true);
//    	HorizontalLayout lin4 = new HorizontalLayout();
//    	lin4.setSpacing(true);
//    	HorizontalLayout lin5 = new HorizontalLayout();
//    	lin5.setSpacing(true);
//    	HorizontalLayout lin6 = new HorizontalLayout();
//    	lin6.setSpacing(true);
//    	
//    	lblProduccion= new Label();
//    	lblProduccion.setValue("Produccion: ");
//    	lblProduccion.setWidth(anchoLabelTitulo);    	
//    	lblProduccion.addStyleName("lblTitulo");
//
//    	lblValorProduccion= new Button();
//    	lblValorProduccion.setWidth(anchoLabelDatos);
//    	lblValorProduccion.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")));
//    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorProduccion.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorProduccion.addStyleName("lblDatos");
//    	lblValorProduccion.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),false);
//				
//			}
//		});
//    	lin.addComponent(lblProduccion);
//    	lin.addComponent(lblValorProduccion);
//    	lin.setComponentAlignment(lblProduccion, Alignment.MIDDLE_LEFT);
//    	lin.setComponentAlignment(lblValorProduccion, Alignment.MIDDLE_RIGHT);
//    	
//    	lblMediaMes= new Label();
//    	lblMediaMes.setValue("Media Mes: ");
//    	lblMediaMes.setWidth(anchoLabelTitulo);    	
//    	lblMediaMes.addStyleName("lblTitulo");
//
//    	lblValorMediaMes= new Button();
//    	lblValorMediaMes.setWidth(anchoLabelDatos);
//    	lblValorMediaMes.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.prod")/new Integer(RutinasFechas.mesActualMM())));
//    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorMediaMes.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorMediaMes.addStyleName("lblDatos");
//    	lblValorMediaMes.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				pantallaProduccion(new Integer(txtEjercicio.getValue().toString()),true);
//				
//			}
//		});
//
//    	lin2.addComponent(lblMediaMes);
//    	lin2.addComponent(lblValorMediaMes);
//    	lin2.setComponentAlignment(lblMediaMes, Alignment.MIDDLE_LEFT);
//    	lin2.setComponentAlignment(lblValorMediaMes, Alignment.MIDDLE_RIGHT);
//	    	
//    	lblVecesPlanificadas= new Label();
//    	lblVecesPlanificadas.setValue("Veces Planificadas: ");
//    	lblVecesPlanificadas.setWidth(anchoLabelTitulo);    	
//    	lblVecesPlanificadas.addStyleName("lblTitulo");
//
//    	lblValorVecesPlanificadas= new Button();
//    	lblValorVecesPlanificadas.setWidth(anchoLabelDatos);
//    	lblValorVecesPlanificadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.vecesPlan")));
//    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorVecesPlanificadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorVecesPlanificadas.addStyleName("lblDatos");
//    	
//    	lblValorVecesPlanificadas.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				pantallaProgramacion(true,new Integer(txtEjercicio.getValue().toString()));
//				
//			}
//		});
//    	
//    	lin3.addComponent(lblVecesPlanificadas);
//    	lin3.addComponent(lblValorVecesPlanificadas);
//    	lin3.setComponentAlignment(lblVecesPlanificadas, Alignment.MIDDLE_LEFT);
//    	lin3.setComponentAlignment(lblValorVecesPlanificadas, Alignment.MIDDLE_RIGHT);
//	    	
//    	lblEmbotelladas= new Label();
//    	lblEmbotelladas.setValue("Embotelladas: ");
//    	lblEmbotelladas.setWidth(anchoLabelTitulo);    	
//    	lblEmbotelladas.addStyleName("lblTitulo");
//
//    	lblValorEmbotelladas= new Button();
//    	lblValorEmbotelladas.setWidth(anchoLabelDatos);
//    	lblValorEmbotelladas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.embPlan")));
//    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorEmbotelladas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorEmbotelladas.addStyleName("lblDatosBlanco");
//    	
//    	lin4.addComponent(lblEmbotelladas);
//    	lin4.addComponent(lblValorEmbotelladas);
//    	lin4.setComponentAlignment(lblEmbotelladas, Alignment.MIDDLE_LEFT);
//    	lin4.setComponentAlignment(lblValorEmbotelladas, Alignment.MIDDLE_RIGHT);
//
//    	lblEtiquetadas= new Label();
//    	lblEtiquetadas.setValue("Etiquetadas: ");
//    	lblEtiquetadas.setWidth(anchoLabelTitulo);    	
//    	lblEtiquetadas.addStyleName("lblTitulo");
//
//    	lblValorEtiquetadas= new Button();
//    	lblValorEtiquetadas.setWidth(anchoLabelDatos);
//    	lblValorEtiquetadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.etiPlan")));
//    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorEtiquetadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorEtiquetadas.addStyleName("lblDatosBlanco");
//    	
//    	lin5.addComponent(lblEtiquetadas);
//    	lin5.addComponent(lblValorEtiquetadas);
//    	lin5.setComponentAlignment(lblEtiquetadas, Alignment.MIDDLE_LEFT);
//    	lin5.setComponentAlignment(lblValorEtiquetadas, Alignment.MIDDLE_RIGHT);
//    	
//    	lblRetrabajadas= new Label();
//    	lblRetrabajadas.setValue("Retrabajadas: ");
//    	lblRetrabajadas.setWidth(anchoLabelTitulo);    	
//    	lblRetrabajadas.addStyleName("lblTitulo");
//
//    	lblValorRetrabajadas= new Button();
//    	lblValorRetrabajadas.setWidth(anchoLabelDatos);
//    	lblValorRetrabajadas.setCaption(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.retPlan")));
//    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_TINY);
//    	lblValorRetrabajadas.addStyleName(ValoTheme.BUTTON_BORDERLESS);
//    	lblValorRetrabajadas.addStyleName("lblDatosBlanco");
//    	
//    	lin6.addComponent(lblRetrabajadas);
//    	lin6.addComponent(lblValorRetrabajadas);
//    	lin6.setComponentAlignment(lblRetrabajadas, Alignment.MIDDLE_LEFT);
//    	lin6.setComponentAlignment(lblValorRetrabajadas, Alignment.MIDDLE_RIGHT);
//		    	
//    	content.addComponent(lin);
//    	content.addComponent(lin2);
//    	content.addComponent(lin3);
//    	content.addComponent(lin4);
//    	content.addComponent(lin5);
//    	content.addComponent(lin6);
//    	
//    	content.setSizeUndefined(); // Shrink to fit
//    	content.setSpacing(false);
//    	content.setMargin(true);
//    	panel.setContent(content);
//    	
//		centralBottom.addComponent(panel,0,0);
//    }
//    
//    private void cargarResumenVentas()
//    {
//    	String anchoLabelTitulo = "120px";
//    	String anchoLabelDatos = "120px";
//    	
//    	Label lblVentasImporte = null;
//    	Label lblValorVentasImporte= null;
//    	Label lblVentasUnidades = null;
//    	Label lblVentasClientes = null;
//    	Label lblValorVentasClientes = null;
//    	Label lblValorVentasUnidades = null;
//    	Label lblVentasPVM = null;
//    	Label lblValorVentasPVM = null;
//    	Label lblVentasMedia = null;
//    	Label lblValorVentasMedia = null;
//    	
//    	
//    	Panel panel = new Panel("RESUMEN VENTAS " + txtEjercicio.getValue());
//
//    	panel.setSizeUndefined(); // Shrink to fit content
//
//    	// Create the content
//    	VerticalLayout content = new VerticalLayout();
//
//    	HorizontalLayout lin = new HorizontalLayout();
//    	lin.setSpacing(true);
//    	HorizontalLayout lin2 = new HorizontalLayout();
//    	lin2.setSpacing(true);
//    	HorizontalLayout lin3 = new HorizontalLayout();
//    	lin3.setSpacing(true);
//    	HorizontalLayout lin4 = new HorizontalLayout();
//    	lin4.setSpacing(true);
//    	HorizontalLayout lin5 = new HorizontalLayout();
//    	lin5.setSpacing(true);
//    	
//    	lblVentasImporte= new Label();
//    	lblVentasImporte.setValue("Importe: ");
//    	lblVentasImporte.setWidth(anchoLabelTitulo);    	
//    	lblVentasImporte.addStyleName("lblTitulo");
//
//    	lblValorVentasImporte = new Label();
//    	lblValorVentasImporte.setWidth(anchoLabelDatos);
//    	lblValorVentasImporte.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasImp")));
//    	lblValorVentasImporte.addStyleName("lblDatosBlanco");
//    	
//    	lin.addComponent(lblVentasImporte);
//    	lin.addComponent(lblValorVentasImporte);
//    	
//    	lblVentasUnidades= new Label();
//    	lblVentasUnidades.setValue("Unidades: ");
//    	lblVentasUnidades.setWidth(anchoLabelTitulo);    	
//    	lblVentasUnidades.addStyleName("lblTitulo");
//
//    	lblValorVentasUnidades = new Label();
//    	lblValorVentasUnidades.setWidth(anchoLabelDatos);
//    	lblValorVentasUnidades.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventas")));
//    	lblValorVentasUnidades.addStyleName("lblDatos");
//    	
//    	lin2.addComponent(lblVentasUnidades);
//    	lin2.addComponent(lblValorVentasUnidades);
//    	
//    	lblVentasPVM= new Label();
//    	lblVentasPVM.setValue("PVM: ");
//    	lblVentasPVM.setWidth(anchoLabelTitulo);    	
//    	lblVentasPVM.addStyleName("lblTitulo");
//
//    	lblValorVentasPVM = new Label();
//    	lblValorVentasPVM.setWidth(anchoLabelDatos);
//    	lblValorVentasPVM.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.pvpVentas")));
//    	lblValorVentasPVM.addStyleName("lblDatosBlanco");
//    	
//    	lin3.addComponent(lblVentasPVM);
//    	lin3.addComponent(lblValorVentasPVM);
//    	
//    	lblVentasMedia= new Label();
//    	lblVentasMedia.setValue("Media Mes: ");
//    	lblVentasMedia.setWidth(anchoLabelTitulo);    	
//    	lblVentasMedia.addStyleName("lblTitulo");
//
//    	lblValorVentasMedia = new Label();
//    	lblValorVentasMedia.setWidth(anchoLabelDatos);
//    	lblValorVentasMedia.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.ventasAvg")));
//    	lblValorVentasMedia.addStyleName("lblDatosBlanco");
//    	
//    	lin4.addComponent(lblVentasMedia);
//    	lin4.addComponent(lblValorVentasMedia);
//    	
////    	lblVentasClientes= new Label();
////    	lblVentasClientes.setValue("Clientes: ");
////    	lblVentasClientes.setWidth(anchoLabelTitulo);    	
////    	lblVentasClientes.addStyleName("lblTitulo");
////
////    	lblValorVentasClientes = new Label();
////    	lblValorVentasClientes.setWidth(anchoLabelDatos);
////    	lblValorVentasClientes.setValue(RutinasNumericas.formatearDouble(this.mapeo.getHashValores().get("0.clientes")));
////    	lblValorVentasClientes.addStyleName("lblDatosBlanco");
////
////    	lin5.addComponent(lblVentasClientes);
////    	lin5.addComponent(lblValorVentasClientes);
//
//    	content.addComponent(lin);
//    	content.addComponent(lin2);
//    	content.addComponent(lin3);
//    	content.addComponent(lin4);
////    	content.addComponent(lin5);
//    	
//    	content.setSizeUndefined(); // Shrink to fit
//    	content.setSpacing(false);
//    	content.setMargin(true);
//    	panel.setContent(content);
//    	
//    	centralMiddle.addComponent(panel,0,0);
//    }
//    
    
    private void vaciarPantalla()
    {
        centralMiddle=null;
        centralMiddle1=null;
        centralBottom=null;
        topLayoutL=null;
    	panelGridAñadas = null;
    	panelGridBarricas = null;
    	panelGridEstructura = null;
    	panelGridReferencias = null;
    	
        barAndGridLayout=null;
        this.removeAllComponents();
    }
    
    public boolean isPrevision() {
    	return prevision;
    }
    
    public void setPrevision(boolean prevision) {
    	this.prevision = prevision;
    }
    
	public void destructor()
	{
		this.vaciarPantalla();
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}


    private void refrescar()
    {
    	this.vaciarPantalla();
    	this.cargarPantalla();
    	this.cargarListeners();
    	procesar();
    }


    private void pieTotales()
    {
    	String destacar = null;
    	MapeoValores map = null;
    	int numero = 0;
    	
    	HashMap<Integer, Color> hash = new HashMap<Integer, Color>();
    	ArrayList<MapeoValores> vector = new ArrayList<MapeoValores>();
    	
    	if (this.mapeo.getHashValores().get("capacidadOcupada")!=null && this.mapeo.getHashValores().get("capacidadOcupada").doubleValue()!=0)
    	{
    		map = new MapeoValores();
	    	map.setClave("Lleno");
	    	map.setValorDouble(this.mapeo.getHashValores().get("capacidadOcupada"));
	    	vector.add(map);
	    	destacar = map.getClave();
	    	hash.put(numero, new Color(130,0,19));
	    	numero+=1;
    	}
    	
    	if (this.mapeo.getHashValores().get("capacidadDisponible")!=null && this.mapeo.getHashValores().get("capacidadDisponible").doubleValue()!=0)
    	{
	    	map = new MapeoValores();
	    	map.setClave("Vacio");
	    	map.setValorDouble(this.mapeo.getHashValores().get("capacidadDisponible"));
	    	vector.add(map);
			hash.put(numero, new Color(255,176,188));
    	}
    	
    	if(!vector.isEmpty())
    	{
    		Panel grafico = queso.generarGrafico("CAPACIDAD", "200px","222px", vector, destacar, hash);
    		centralTop2.addComponent(grafico,1,0);
    	}

    }

    private void resumenStockAñada()
    {
    	consultaExistenciasBarricasServer ces = new consultaExistenciasBarricasServer(CurrentUser.get());
    	ArrayList<MapeoExistenciasBarricas> vector = new ArrayList<MapeoExistenciasBarricas>();
    	vector = ces.recuperarStockAñada();
    	
        gridDatosAñada= new dashExistenciasGrid(vector);
        gridDatosAñada.getColumn("variedad").setHidden(true);
        gridDatosAñada.getColumn("referencia").setHidden(true);
        
    	this.cargarListenersGridAñada();
		
    	panelGridAñadas = new Panel();
    	panelGridAñadas.setSizeUndefined(); // Shrink to fit content
    	panelGridAñadas.setHeight("250px");
    	panelGridAñadas.setWidth("305px");
    	panelGridAñadas.setContent(gridDatosAñada);
    	centralTop2.addComponent(panelGridAñadas,2,0);
    }
    
    private void resumenStockReferencia(String r_añada)
    {
    	ArrayList<MapeoExistenciasBarricas> vector = new ArrayList<MapeoExistenciasBarricas>();
    	consultaExistenciasBarricasServer cotbs = consultaExistenciasBarricasServer.getInstance(CurrentUser.get());
    	vector = cotbs.recuperarStockReferencia(r_añada);

    	gridDatosReferencia= new dashExistenciasGrid(vector);
    	
    	this.cargarListenersGridReferencia();
    	
    	panelGridReferencias = new Panel();
    	panelGridReferencias.setSizeUndefined(); // Shrink to fit content
    	panelGridReferencias.setHeight("250px");
    	panelGridReferencias.setWidth("815px");
    	panelGridReferencias.setContent(gridDatosReferencia);
    	centralMiddle.addComponent(panelGridReferencias,0,0);
    }

    private void resumenReferenciaVariedadBarricas(String r_añada, String r_referencia, String r_variedad)
    {
    	ArrayList<MapeoBarricas> vector = new ArrayList<MapeoBarricas>();
    	consultaExistenciasBarricasServer cotbs = consultaExistenciasBarricasServer.getInstance(CurrentUser.get());
    	vector = cotbs.recuperarReferenciaVariedadBarricas(r_añada, r_referencia, r_variedad);

    	dashBarricasGrid grid = new dashBarricasGrid(vector);
    	
    	panelGridBarricas = new Panel();
    	panelGridBarricas.setSizeUndefined(); // Shrink to fit content
    	panelGridBarricas.setHeight("250px");
    	panelGridBarricas.setWidth("655px");
    	panelGridBarricas.setContent(grid);
    	centralMiddle.addComponent(panelGridBarricas,1,0);
    }

    private void generarMapaEstructuraAlmacen(String r_añada, String r_referencia, String r_variedad)
    {
    	ArrayList<MapeoEstructuraAlmacen> vector = new ArrayList<MapeoEstructuraAlmacen>();
    	consultaExistenciasBarricasServer cotbs = consultaExistenciasBarricasServer.getInstance(CurrentUser.get());
    	vector = cotbs.recuperarEstructuraAlmacen(r_añada, r_referencia, r_variedad);

    	dashMapaAlmacenGrid grid = new dashMapaAlmacenGrid(vector);
    	
    	panelGridEstructura = new Panel("");
    	panelGridEstructura.setSizeFull(); // Shrink to fit content
    	panelGridEstructura.setHeight("250px");
//    	panelGridEstructura.setWidth("850px");
    	panelGridEstructura.setContent(grid);
    	centralBottom.addComponent(panelGridEstructura);
    }

    private void cargarListenersGridAñada()
    {
		gridDatosAñada.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
					
				if (gridDatosAñada.getSelectionModel() instanceof SingleSelectionModel)
				{
					MapeoExistenciasBarricas mapeo = (MapeoExistenciasBarricas) gridDatosAñada.getSelectedRow();
					if (mapeo!=null)
					{
						centralMiddle.removeAllComponents();
						resumenStockReferencia(mapeo.getAnada());
					}
				}
			}
		});

    }
    
    private void cargarListenersGridReferencia()
    {
		gridDatosReferencia.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
					
				if (gridDatosReferencia.getSelectionModel() instanceof SingleSelectionModel)
				{
					MapeoExistenciasBarricas mapeo = (MapeoExistenciasBarricas) gridDatosReferencia.getSelectedRow();
					if (mapeo!=null)
					{
						centralMiddle.removeComponent(panelGridBarricas);
						resumenReferenciaVariedadBarricas(mapeo.getAnada(),mapeo.getReferencia(),mapeo.getVariedad());
						if (panelGridEstructura!=null && centralBottom!=null) centralBottom.removeComponent(panelGridEstructura);
						generarMapaEstructuraAlmacen(mapeo.getAnada(),mapeo.getReferencia(),mapeo.getVariedad());
					}
				}
			}
		});

    }

    private void consultaBarricas()
    {
    	PantallaConsultaBarricas vt = new PantallaConsultaBarricas("Varricas Vacias");
    	getUI().addWindow(vt);
    }
}
