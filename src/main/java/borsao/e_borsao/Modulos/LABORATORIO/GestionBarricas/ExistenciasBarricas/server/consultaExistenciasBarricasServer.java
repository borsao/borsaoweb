package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.server.ThemeResource;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.server.consultaBarricasServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server.consultaDesgloseNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoEstructuraAlmacen;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoExistenciasBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo.MapeoGeneralDashboardBarrica;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.MovimientosBarricas.modelo.MapeoMovimientoBarricas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.OrdenesTrabajo.modelo.MapeoOrdenesTrabajo;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaExistenciasBarricasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaExistenciasBarricasServer instance;
	
	public consultaExistenciasBarricasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaExistenciasBarricasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaExistenciasBarricasServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoGeneralDashboardBarrica recuperarValoresEstadisticos()
	{
		/*
		 * Capacidad Total
		 * Capacidad Vacia
		 * Capacidad LLena
		 */
		String clave = null;
		Double valorClave = new Double(0);
		consultaBarricasServer cbs = consultaBarricasServer.getInstance(CurrentUser.get());
		MapeoGeneralDashboardBarrica mapeos = new MapeoGeneralDashboardBarrica();
		HashMap<String, Double> hashValores = new HashMap<String, Double>();
		
		clave = "capacidadTotal";
		valorClave = cbs.obtenerCapacidadTotalBarricas();
		hashValores.put(clave, valorClave);
		
		clave = "barricasTotales";
		valorClave = cbs.obtenerTotalBarricas();
		hashValores.put(clave, valorClave);
		
		clave = "capacidadDisponible";
		valorClave = this.obtenerCapacidadDisponibleBarricas("vacias");
		hashValores.put(clave, valorClave);

		clave = "capacidadOcupada";
		valorClave = this.obtenerCapacidadDisponibleBarricas("llenas");
		hashValores.put(clave, valorClave);


		
		mapeos.setHashValores(hashValores);
		return mapeos;
	}
	
	public Double obtenerCapacidadDisponibleBarricas(String r_clave)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT sum(capacidad) bar_cap ");
		cadenaSQL.append(" from lab_barricas ");
		if (r_clave.contentEquals("vacias")) cadenaSQL.append(" where idCodigo not in (select idBarrica from lab_barricas_existencias )");
		else if (r_clave.contentEquals("llenas")) cadenaSQL.append(" where idCodigo in (select idBarrica from lab_barricas_existencias )");
		
		cadenaSQL.append(" and lab_barricas.estado='A' ");
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getDouble("bar_cap");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return new Double(0);
	}

	
	public ArrayList<MapeoExistenciasBarricas> recuperarStockReferencia(String r_añada)
	{
		ArrayList<MapeoExistenciasBarricas> vector = null;
		ResultSet rsOpcion = null;
		MapeoExistenciasBarricas  mapeoValores=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(lab_barricas.capacidad) bar_tot,");
		cadenaSQL.append(" lab_barricas_existencias.añada ex_na, ");
		cadenaSQL.append(" lab_barricas_existencias.referencia ex_ref, ");
		cadenaSQL.append(" lab_barricas_existencias.variedad ex_var ");
		cadenaSQL.append(" from lab_barricas_existencias");
		cadenaSQL.append(" inner join lab_barricas on lab_barricas.idCodigo = lab_barricas_existencias.idBarrica ");
		cadenaSQL.append(" where lab_barricas_existencias.añada = '" + r_añada + "' ");
		cadenaSQL.append(" group by lab_barricas_existencias.añada, lab_barricas_existencias.variedad, lab_barricas_existencias.referencia ");
		cadenaSQL.append(" order by lab_barricas_existencias.añada, lab_barricas_existencias.variedad, lab_barricas_existencias.referencia ");
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoExistenciasBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoValores = new MapeoExistenciasBarricas();
				mapeoValores.setAnada(rsOpcion.getString("ex_na"));
				mapeoValores.setReferencia(rsOpcion.getString("ex_ref"));
				mapeoValores.setVariedad(rsOpcion.getString("ex_var"));
				mapeoValores.setExistencias(rsOpcion.getInt("bar_tot"));
				vector.add(mapeoValores);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}			
		return vector;
	}
	
	public ArrayList<MapeoBarricas> recuperarReferenciaVariedadBarricas(String r_añada, String r_referencia, String r_variedad)
	{
		ArrayList<MapeoBarricas> vector = null;
		ResultSet rsOpcion = null;
		MapeoBarricas  mapeoValores=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(lab_barricas.capacidad) bar_tot,");
		cadenaSQL.append(" count(lab_barricas.numero) bar_num, ");
		cadenaSQL.append(" lab_barricas.descripcion bar_des, ");
		cadenaSQL.append(" lab_barricas.ejercicio bar_eje ");
		cadenaSQL.append(" from lab_barricas_existencias " );
		cadenaSQL.append(" inner join lab_barricas on lab_barricas.idCodigo = lab_barricas_existencias.idBarrica ");
		cadenaSQL.append(" where lab_barricas_existencias.añada = '" + r_añada + "' ");
		cadenaSQL.append(" and lab_barricas_existencias.variedad = '" + r_variedad + "' ");
		cadenaSQL.append(" and lab_barricas_existencias.referencia = '" + r_referencia + "' ");
		cadenaSQL.append(" group by lab_barricas.descripcion, lab_barricas.ejercicio ");
		cadenaSQL.append(" order by lab_barricas.ejercicio, lab_barricas.descripcion ");
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoValores = new MapeoBarricas();
				mapeoValores.setEjercicio(new Integer(rsOpcion.getString("bar_eje")));
				mapeoValores.setDescripcion(rsOpcion.getString("bar_des"));
				mapeoValores.setNumero(new Integer(rsOpcion.getString("bar_num")));
				mapeoValores.setCapacidad(rsOpcion.getInt("bar_tot"));
				vector.add(mapeoValores);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}			
		return vector;
	}

	public ArrayList<MapeoExistenciasBarricas> recuperarStockAñada()
	{
		ArrayList<MapeoExistenciasBarricas> vector = null;
		ResultSet rsOpcion = null;
		MapeoExistenciasBarricas  mapeoValores=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(lab_barricas.capacidad) bar_tot,");
		cadenaSQL.append(" lab_barricas_existencias.añada ex_ana ");
		cadenaSQL.append(" from lab_barricas_existencias");
		cadenaSQL.append(" inner join lab_barricas on lab_barricas.idCodigo = lab_barricas_existencias.idBarrica "); 
		cadenaSQL.append(" group by lab_barricas_existencias.añada ");
		cadenaSQL.append(" order by lab_barricas_existencias.añada ");
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoExistenciasBarricas>();
			
			while(rsOpcion.next())
			{
				mapeoValores = new MapeoExistenciasBarricas();
				mapeoValores.setAnada(rsOpcion.getString("ex_ana"));
				mapeoValores.setExistencias(rsOpcion.getInt("bar_tot"));
				vector.add(mapeoValores);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}			
		return vector;
	}
	public ArrayList<MapeoExistenciasBarricas> datosExistenciasGlobal(MapeoExistenciasBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoExistenciasBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoExistenciasBarricas mapeoExistenciasBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_existencias.id ex_id,");
		cadenaSQL.append(" lab_barricas_existencias.añada ex_ana,");
		cadenaSQL.append(" lab_barricas_existencias.variedad ex_var,");
		cadenaSQL.append(" lab_barricas_existencias.referencia ex_ref,");
		cadenaSQL.append(" lab_barricas_existencias.idDesgloseNave ex_nav,");
		cadenaSQL.append(" lab_barricas_existencias.idBarrica ex_bar,");
		cadenaSQL.append(" lab_barricas_existencias.idMovimiento ex_mov ");
		cadenaSQL.append(" from lab_barricas_existencias");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getVariedad()!=null && r_mapeo.getVariedad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" variedad = '" + r_mapeo.getVariedad() + "' ");
				}
				if (r_mapeo.getReferencia()!=null && r_mapeo.getReferencia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" referencia= '" + r_mapeo.getReferencia() + "' ");
				}
				if (r_mapeo.getIdBarrica()!=null && r_mapeo.getIdBarrica().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idBarrica = '" + r_mapeo.getIdBarrica() + "' ");
				}
				if (r_mapeo.getIdMovimiento()!=null && r_mapeo.getIdMovimiento().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idMovimiento = '" + r_mapeo.getIdMovimiento() + "' ");
				}
				if (r_mapeo.getIdDesgloseNave()!=null && r_mapeo.getIdDesgloseNave().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idDesgloseNave = '" + r_mapeo.getIdDesgloseNave() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoExistenciasBarricas>();
			
			while(rsOpcion.next())
			{
				MapeoExistenciasBarricas mapeo = new MapeoExistenciasBarricas();
				mapeo.setIdCodigo(rsOpcion.getInt("ex_id"));
				mapeo.setAnada(rsOpcion.getString("ex_ana"));
				mapeo.setVariedad(rsOpcion.getString("ex_var"));
				mapeo.setReferencia(rsOpcion.getString("ex_ref"));
				mapeo.setIdDesgloseNave(rsOpcion.getInt("ex_nav"));
				mapeo.setIdBarrica(rsOpcion.getInt("ex_bar"));
				mapeo.setIdMovimiento(rsOpcion.getInt("ex_mov"));
				vector.add(mapeoExistenciasBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoExistenciasBarricas> datosExistenciasGlobalTransaccional(Connection r_con, MapeoExistenciasBarricas r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoExistenciasBarricas> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoExistenciasBarricas mapeoExistenciasBarricas=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT lab_barricas_existencias.id ex_id,");
		cadenaSQL.append(" lab_barricas_existencias.añada ex_ana,");
		cadenaSQL.append(" lab_barricas_existencias.variedad ex_var,");
		cadenaSQL.append(" lab_barricas_existencias.referencia ex_ref,");
		cadenaSQL.append(" lab_barricas_existencias.idDesgloseNave ex_nav,");
		cadenaSQL.append(" lab_barricas_existencias.idBarrica ex_bar,");
		cadenaSQL.append(" lab_barricas_existencias.idMovimiento ex_mov ");
		cadenaSQL.append(" from lab_barricas_existencias");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getVariedad()!=null && r_mapeo.getVariedad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" variedad = '" + r_mapeo.getVariedad() + "' ");
				}
				if (r_mapeo.getReferencia()!=null && r_mapeo.getReferencia().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" referencia= '" + r_mapeo.getReferencia() + "' ");
				}
				if (r_mapeo.getIdBarrica()!=null && r_mapeo.getIdBarrica().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idBarrica = '" + r_mapeo.getIdBarrica() + "' ");
				}
				if (r_mapeo.getIdMovimiento()!=null && r_mapeo.getIdMovimiento().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idMovimiento = '" + r_mapeo.getIdMovimiento() + "' ");
				}
				if (r_mapeo.getIdDesgloseNave()!=null && r_mapeo.getIdDesgloseNave().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idDesgloseNave = '" + r_mapeo.getIdDesgloseNave() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id");
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoExistenciasBarricas>();
			
			while(rsOpcion.next())
			{
				MapeoExistenciasBarricas mapeo = new MapeoExistenciasBarricas();
				mapeo.setIdCodigo(rsOpcion.getInt("ex_id"));
				mapeo.setAnada(rsOpcion.getString("ex_ana"));
				mapeo.setVariedad(rsOpcion.getString("ex_var"));
				mapeo.setReferencia(rsOpcion.getString("ex_ref"));
				mapeo.setIdDesgloseNave(rsOpcion.getInt("ex_nav"));
				mapeo.setIdBarrica(rsOpcion.getInt("ex_bar"));
				mapeo.setIdMovimiento(rsOpcion.getInt("ex_mov"));
				vector.add(mapeoExistenciasBarricas);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_existencias.id) ord_sig ");
     	cadenaSQL.append(" FROM lab_barricas_existencias ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("ord_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}
	public Integer obtenerSiguienteTransaccional(Connection r_con)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(lab_barricas_existencias.id) ord_sig ");
     	cadenaSQL.append(" FROM lab_barricas_existencias ");
		try
		{
     	
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("ord_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public String guardarNuevo(MapeoExistenciasBarricas r_mapeo)
	{
//		PreparedStatement preparedStatement = null;		
//		StringBuffer cadenaSQL = null;
		String rdo = null;
		return rdo;
	}
	
	public String guardarNuevoTransaccional(Connection r_con, MapeoExistenciasBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;
		String rdo = null;

		cadenaSQL = new StringBuffer();
		cadenaSQL.append(" INSERT INTO lab_barricas_existencias( ");
		cadenaSQL.append(" lab_barricas_existencias.id,");
		cadenaSQL.append(" lab_barricas_existencias.añada ,");
		cadenaSQL.append(" lab_barricas_existencias.variedad ,");
		cadenaSQL.append(" lab_barricas_existencias.referencia , ");
		cadenaSQL.append(" lab_barricas_existencias.observaciones , ");
		cadenaSQL.append(" lab_barricas_existencias.idDesgloseNave , ");
		cadenaSQL.append(" lab_barricas_existencias.idBarrica , ");
		cadenaSQL.append(" lab_barricas_existencias.idMovimiento ) VALUES ( ");
		cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		
		try
		{
			preparedStatement = r_con.prepareStatement(cadenaSQL.toString()); 
				
	    	preparedStatement.setInt(1, this.obtenerSiguienteTransaccional(r_con));
			
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getVariedad()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getVariedad());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getReferencia()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getReferencia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setString(5, null);
		    
		    if (r_mapeo.getIdDesgloseNave()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdDesgloseNave());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getIdBarrica()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdBarrica());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getIdMovimiento()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getIdMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
	
	        preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError("error exis " + ex.getMessage());	    
			rdo = "error exis " + ex.getMessage();
		}
		return rdo;
	}
	
	public String guardarCambios(MapeoOrdenesTrabajo r_mapeo)
	{
		
		return null;
	}
	
	public String eliminarTransaccional(Connection r_con, MapeoMovimientoBarricas r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;
		String rdo = null;

		cadenaSQL = new StringBuffer();
		cadenaSQL.append(" delete from lab_barricas_existencias  ");
		cadenaSQL.append(" where lab_barricas_existencias.idBarrica in (select idBarrica from lab_barricas_ordenes_bar where lab_barricas_ordenes_bar.idOrden= " + r_mapeo.getIdCodigo() +" ) ");
		cadenaSQL.append(" and variedad = '" + r_mapeo.getVariedad() +"' ");
		cadenaSQL.append(" and referencia = '" + r_mapeo.getReferencia() +"' ");
		cadenaSQL.append(" and idBarrica = " + r_mapeo.getIdBarrica());

		try
		{
			preparedStatement = r_con.prepareStatement(cadenaSQL.toString());	
	        preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	    
			rdo = "Error eliminar existencias " + ex.getMessage();
		}
		return rdo;
	}

	public String eliminar(MapeoOrdenesTrabajo r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;
		String rdo = null;

		cadenaSQL = new StringBuffer();
		cadenaSQL.append(" delete from lab_barricas_existencias  ");
		cadenaSQL.append(" where lab_barricas_existencias.idBarrica in (select idBarrica from lab_barricas_ordenes_bar where lab_barricas_ordenes_bar.idOrden= " + r_mapeo.getIdCodigo() +" ) ");
		cadenaSQL.append(" and variedad = '" + r_mapeo.getVariedad() +"' ");
		cadenaSQL.append(" and referencia = '" + r_mapeo.getReferencia() +"' ");

		try
		{
			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());	
	        preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	    
			rdo = ex.getMessage();
		}
		return rdo;
	}
	
	

	public String generarInforme(String r_valores)
	{
		String resultadoGeneracion = null;
//		LibreriaImpresion libImpresion = null;
//		libImpresion = new LibreriaImpresion();
//
//		libImpresion.setCodigoTxt(r_valores);
//		libImpresion.setArchivoDefinitivo("/ordenesTrabajoBarrica" + RutinasFechas.horaActualSinSeparador() + ".pdf");
//		
//		libImpresion.setArchivoPlantilla("ListadoOrdenesBarricas.jrxml");
//		libImpresion.setBackGroundEtiqueta("/fondoA4LogoNaranjaH.jpg");
//    	libImpresion.setArchivoPlantillaDetalle("OrdenesTrabajoBarricasDetalle.jrxml");
//    	libImpresion.setArchivoPlantillaDetalleCompilado("OrdenesTrabajoBarricasDetalle.jasper");
//		
//		libImpresion.setArchivoPlantillaNotas(null);
//		libImpresion.setArchivoPlantillaNotasCompilado(null);
//		libImpresion.setCarpeta("laboratorio");
//
//		resultadoGeneracion = libImpresion.generacionInformeTxtXML();
//
//		if (resultadoGeneracion == null)
//			resultadoGeneracion = libImpresion.getArchivoDefinitivo();
//
		return resultadoGeneracion;		
		

	}

	private Integer recuperarBarricasExistenciasAlmacen(Integer r_idDesglose, String r_añada, String r_referencia, String r_variedad)
	{
		ResultSet rsOpcion = null;
		Integer cuantas = 0;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" SELECT count(lab_barricas_existencias.idBarrica) ex_tot");
			cadenaSQL.append(" from lab_barricas_existencias");
			cadenaSQL.append(" where lab_barricas_existencias.idDesgloseNave = " + r_idDesglose);
			cadenaSQL.append(" and lab_barricas_existencias.añada = '" + r_añada + "' ");
			cadenaSQL.append(" and lab_barricas_existencias.referencia= '" + r_referencia + "' ");
			cadenaSQL.append(" and lab_barricas_existencias.variedad = '" + r_variedad + "' ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				cuantas = rsOpcion.getInt("ex_tot");				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cuantas;
	}
	
	private Integer recuperarDesglose(String r_añada, String r_referencia, String r_variedad)
	{
		ResultSet rsOpcion = null;
		Integer cuantas = 0;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" SELECT min(lab_barricas_existencias.idDesgloseNave) ex_des");
			cadenaSQL.append(" from lab_barricas_existencias");
			cadenaSQL.append(" where lab_barricas_existencias.añada = '" + r_añada + "' ");
			cadenaSQL.append(" and lab_barricas_existencias.referencia= '" + r_referencia + "' ");
			cadenaSQL.append(" and lab_barricas_existencias.variedad = '" + r_variedad + "' ");

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				cuantas = rsOpcion.getInt("ex_des");				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cuantas;
	}
	public ArrayList<MapeoEstructuraAlmacen> recuperarEstructuraAlmacen(String r_añada, String r_referencia, String r_variedad)
	{
		ArrayList<MapeoEstructuraAlmacen> vector = null;
		ArrayList<MapeoDesgloseNaves> vectorDesglose = null;
		Iterator<MapeoDesgloseNaves> iterDesglose = null;
		MapeoEstructuraAlmacen mapeoEstructuraAlmacen = null;
		
		
		consultaDesgloseNavesServer cdns = consultaDesgloseNavesServer.getInstance(CurrentUser.get());
		MapeoDesgloseNaves mapeoDesglose = new MapeoDesgloseNaves();
		
		
		Integer idDesglose = this.recuperarDesglose(r_añada, r_referencia, r_variedad);
		Integer idNave = cdns.obtenerIdNavePorDesglose(idDesglose);
		
		if (idNave!=null)
		{
			mapeoDesglose.setIdNave(idNave);
			vectorDesglose = cdns.datosDesgloseNavesGlobal(mapeoDesglose);
			iterDesglose = vectorDesglose.iterator();
			vector = new ArrayList<MapeoEstructuraAlmacen>();
			
			while (iterDesglose.hasNext())
			{
				mapeoEstructuraAlmacen = new MapeoEstructuraAlmacen();
				MapeoDesgloseNaves mapeoDesgloseNave = iterDesglose.next();
	
				/*
				 * consulto existencias por idNave, fila, calle  (idDesglose)
				 * 
				 * recojo el resultado y en multiplos de 50 los voy asignando en el mapeo Estructura
				 */
				Integer cuantas = this.recuperarBarricasExistenciasAlmacen(mapeoDesgloseNave.getIdCodigo(),r_añada, r_referencia, r_variedad);
				Double veces = Math.ceil(cuantas.doubleValue()/50);
				Integer quedan = cuantas;
				for (int j=1;j<=veces.intValue();j=j+1)
				{
					int factor=50;
					String valor = "50";
					int i = factor*j;
					
					if (quedan.compareTo(50)<=0) valor = quedan.toString();					
					if (i>550 && quedan.compareTo(cuantas)>0) valor = quedan.toString();
					/*
					 * En fucnion del valor de la variedad cargaremos una u otra barrica (color)
					 * para diferenciar de forma grafica el stock de cada tipo
					 */
					String dib ="img/barricaLlena.png";
					ThemeResource barrica = new ThemeResource(dib);
					
					if(i==50)
					{
						mapeoEstructuraAlmacen.setColumna1(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna1(r_variedad + " - " + r_referencia);
					}
					if(i==100)
					{
						mapeoEstructuraAlmacen.setColumna2(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna2(r_variedad + " - " + r_referencia);
					}
					if(i==150) 
					{
						mapeoEstructuraAlmacen.setColumna3(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna3(r_variedad + " - " + r_referencia);
					}
					if(i==200) 
					{
						mapeoEstructuraAlmacen.setColumna4(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna4(r_variedad + " - " + r_referencia);
					}
					if(i==250) 
					{
						mapeoEstructuraAlmacen.setColumna5(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna5(r_variedad + " - " + r_referencia);
					}
					if(i==300) 
					{
						mapeoEstructuraAlmacen.setColumna6(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna6(r_variedad + " - " + r_referencia);
					}
					if(i==350) 
					{
						mapeoEstructuraAlmacen.setColumna7(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna7(r_variedad + " - " + r_referencia);
					}
					if(i==400) 
					{
						mapeoEstructuraAlmacen.setColumna8(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna8(r_variedad + " - " + r_referencia);
					}
					if(i==450) 
					{
						mapeoEstructuraAlmacen.setColumna9(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna9(r_variedad + " - " + r_referencia);
					}
					if(i==500) 
					{
						mapeoEstructuraAlmacen.setColumna10(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna10(r_variedad + " - " + r_referencia);
					}
					if(i==550) 
					{
						mapeoEstructuraAlmacen.setColumna11(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna11(r_variedad + " - " + r_referencia);
					}
					if(i==600) 
					{
						mapeoEstructuraAlmacen.setColumna12(barrica);
						mapeoEstructuraAlmacen.setVariedadColumna12(r_variedad + " - " + r_referencia);
					}
					
					quedan = quedan - 50;
				}
				
				mapeoEstructuraAlmacen.setIdNave(idNave);
				mapeoEstructuraAlmacen.setNombreNave(mapeoDesgloseNave.getNombre());
				mapeoEstructuraAlmacen.setFila(mapeoDesgloseNave.getFilas());
				if (mapeoDesgloseNave.getCalle()==1)
					mapeoEstructuraAlmacen.setCalle("A");
				else
					mapeoEstructuraAlmacen.setCalle("B");
				vector.add(mapeoEstructuraAlmacen);
			}
		}
		

		return vector;
	}
	
	@Override
	public String semaforos() 
	{
		return "0";
	}

}