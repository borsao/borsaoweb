package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class dashExistenciasGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public dashExistenciasGrid(ArrayList<MapeoExistenciasBarricas> r_vector) 
    {
        this.vector=r_vector;
        this.setSizeFull();
        this.setSeleccion(SelectionMode.SINGLE);
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoExistenciasBarricas.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		this.setConTotales(true);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		if (this.isConTotales()) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("anada","variedad", "referencia","existencias");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("anada", "85");
    	this.widthFiltros.put("variedad", "210");
    	this.widthFiltros.put("referencia", "210");

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idDesgloseNave").setHidden(true);
    	this.getColumn("idBarrica").setHidden(true);
    	this.getColumn("idMovimiento").setHidden(true);
            	
    	this.getColumn("anada").setHeaderCaption("Añada");
    	this.getColumn("anada").setWidth(120);
    	this.getColumn("variedad").setHeaderCaption("Variedad");
    	this.getColumn("variedad").setWidth(250);
    	this.getColumn("referencia").setHeaderCaption("Referencia");
    	this.getColumn("referencia").setWidth(250);
    	this.getColumn("existencias").setHeaderCaption("Litros");
    	this.getColumn("existencias").setWidth(150);

    }
    
    private void asignarTooltips()
    {
    }
    
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{

            public String getStyle(Grid.CellReference cellReference) 
            {
            	if (cellReference.getPropertyId().toString().contains("existencias") || cellReference.getPropertyId().toString().contains("añada")) 
    			{
    				return "Rcell-normal";
    			}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
//    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
//    	{
//            public void itemClick(ItemClickEvent event) 
//            {
//            	MapeoExistenciasBarricas mapeo = (MapeoExistenciasBarricas) event.getItemId();
//            	
//            	if (event.getPropertyId().toString().equals("desg"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		{
////            			pantallaLineasProgramaciones vt = null;
////        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
////        	    		getUI().addWindow(vt);
//            		}
//            	}
//            	else
//            	{
//            		activadaVentanaPeticion=false;
//            		ordenando = false;
////            		app.verForm(false);
////            		MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
//            		
////	            	if (event.getPropertyId().toString().equals("papel"))
////	            	{
////	            		activadaVentanaPeticion=true;
////	            		ordenando = false;
////	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
////	            		{
////		        	    	if (permisos>=30)
////		        	    	{
////		        	    		
////		        	    		/*
////		        	    		 * Abriremos ventana peticion parametros
////		        	    		 * rellenaremos la variables publicas
////		        	    		 * 		cajas y anada
////		        	    		 * 		llamaremos al metodo generacionPdf
////		        	    		 */
////		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
////		        	    		getUI().addWindow(vtPeticion);
////		        	    	}	    			
////							else
////							{
////								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
////							}
////	            		}
////	            	}
////	            	}            	
////	            	else
////	            	{
////	            		activadaVentanaPeticion=false;
////	            		ordenando = false;
////	            	}
//	            }
//    		}
//        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("existencias");
	}

	public void generacionPdf(MapeoBarricas r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
		Integer totalCapacidad = new Integer(0);
		Integer q1Value = new Integer(0);
		
    	if (this.getFooterRowCount()==0)
    	{
    		this.appendFooterRow();
    	}

    	this.getFooterRow(0).getCell("anada").setText("Total:");
        
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	q1Value = (Integer) item.getItemProperty("existencias").getValue();
    		if (q1Value!=null) totalCapacidad+= q1Value;
        }
        
        this.getFooterRow(0).getCell("existencias").setText(RutinasNumericas.formatearIntegerDigitos(totalCapacidad,0).toString());
        this.getFooterRow(0).getCell("existencias").setStyleName("Rcell-pie");

	}
}


