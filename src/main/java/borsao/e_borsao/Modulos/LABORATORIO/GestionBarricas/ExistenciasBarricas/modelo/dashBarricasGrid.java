package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.ExistenciasBarricas.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo.MapeoBarricas;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class dashBarricasGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public dashBarricasGrid(ArrayList<MapeoBarricas> r_vector) 
    {
        this.vector=r_vector;
        this.setSizeFull();
        this.setSeleccion(SelectionMode.SINGLE);
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoBarricas.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		this.setConTotales(true);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		if (this.isConTotales()) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("descripcion","ejercicio", "numero","capacidad");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("descripcion", "225");
    	this.widthFiltros.put("ejercicio", "85");

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("observaciones").setHidden(true);
    	this.getColumn("desg").setHidden(true);
    	
    	this.getColumn("descripcion").setHeaderCaption("Tonelero");
    	this.getColumn("descripcion").setWidth(250);
    	this.getColumn("ejercicio").setHeaderCaption("Año");
    	this.getColumn("ejercicio").setWidth(120);
    	this.getColumn("numero").setHeaderCaption("Total");
    	this.getColumn("numero").setWidth(120);
    	this.getColumn("capacidad").setHeaderCaption("Capacidad");
    	this.getColumn("capacidad").setWidth(120);

    }
    
    private void asignarTooltips()
    {
    }
    
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{

            public String getStyle(Grid.CellReference cellReference) 
            {
    			if (cellReference.getPropertyId().toString().contains("capacidad") || cellReference.getPropertyId().toString().contains("ejercicio") || cellReference.getPropertyId().toString().contains("numero")) 
    			{
    				return "Rcell-normal";
    			}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("desg"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getNumero()!=null && mapeo.getNumero()!=0)
            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
            		}
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
//            		app.verForm(false);
//            		MapeoBarricas mapeo = (MapeoBarricas) event.getItemId();
            		
//	            	if (event.getPropertyId().toString().equals("papel"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		ordenando = false;
//	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
//	            		{
//		        	    	if (permisos>=30)
//		        	    	{
//		        	    		
//		        	    		/*
//		        	    		 * Abriremos ventana peticion parametros
//		        	    		 * rellenaremos la variables publicas
//		        	    		 * 		cajas y anada
//		        	    		 * 		llamaremos al metodo generacionPdf
//		        	    		 */
//		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
//		        	    		getUI().addWindow(vtPeticion);
//		        	    	}	    			
//							else
//							{
//								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
//							}
//	            		}
//	            	}
//	            	}            	
//	            	else
//	            	{
//	            		activadaVentanaPeticion=false;
//	            		ordenando = false;
//	            	}
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("desg");
		this.camposNoFiltrar.add("capacidad");
		this.camposNoFiltrar.add("numero");
	}

	public void generacionPdf(MapeoBarricas r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
		Integer totalCapacidad = new Integer(0);
		Integer q1Value = new Integer(0);
		
    	if (this.getFooterRowCount()==0)
    	{
    		this.appendFooterRow();
    	}

    	this.getFooterRow(0).getCell("descripcion").setText("Total:");
        
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	q1Value = (Integer) item.getItemProperty("capacidad").getValue();
    		if (q1Value!=null) totalCapacidad+= q1Value;
        }
        
        this.getFooterRow(0).getCell("capacidad").setText(RutinasNumericas.formatearIntegerDigitos(totalCapacidad,0).toString());
        this.getFooterRow(0).getCell("capacidad").setStyleName("Rcell-pie");

	}
}


