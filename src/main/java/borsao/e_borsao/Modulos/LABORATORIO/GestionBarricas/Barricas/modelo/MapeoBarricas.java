package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Barricas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoBarricas extends MapeoGlobal
{
	private Integer numero;
	private Integer ejercicio;
	private Integer capacidad;
	
	private String descripcion;
	private String observaciones;
	private String estado;
	
	private String desg;
	
	
	public MapeoBarricas()
	{ 
	}


	public Integer getNumero() {
		return numero;
	}


	public void setNumero(Integer numero) {
		this.numero = numero;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getDesg() {
		return desg;
	}


	public void setDesg(String desg) {
		this.desg = desg;
	}


	public Integer getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

}