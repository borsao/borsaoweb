package borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.sort.Sort;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.DesgloseNavesGrid;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.MapeoDesgloseNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.DesgloseNaves.server.consultaDesgloseNavesServer;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.modelo.MapeoNaves;
import borsao.e_borsao.Modulos.LABORATORIO.GestionBarricas.Naves.server.consultaNavesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoDesgloseNaves mapeo = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaDesgloseNavesServer cps = null;
	 private DesgloseNavesView uw = null;
	 public MapeoDesgloseNaves mapeoModificado = null;
	 private BeanFieldGroup<MapeoDesgloseNaves> fieldGroup;
	 
	 public OpcionesForm(DesgloseNavesView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoDesgloseNaves>(MapeoDesgloseNaves.class);
        fieldGroup.bindMemberFields(this);
        this.establecerCamposObligatorios();
        this.cargarCombo();
        this.cargarValidators();
        this.cargarListeners();
        
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoDesgloseNaves) uw.grid.getSelectedRow();
                eliminarDesgloseNave(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;	
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearDesgloseNave(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoDesgloseNaves mapeo_orig= (MapeoDesgloseNaves) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				mapeo.setIdNave(mapeo_orig.getIdNave());
		 				
		 				modificarDesgloseNave(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((DesgloseNavesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarDesgloseNave(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.filas.getValue()!=null && this.filas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fila", this.filas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fila", "");
		}
		if (this.calle.getValue()!=null && this.calle.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("calle", this.calle.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("calle", "");
		}
		if (this.nombre.getValue()!=null && this.nombre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.filas.getValue()!=null && this.filas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fila", this.filas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fila", "");
		}
		if (this.calle.getValue()!=null && this.calle.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("calle", this.calle.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("calle", "");
		}
		if (this.nombre.getValue()!=null && this.nombre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}

		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarDesgloseNave(null);
		}
	}
	
	public void editarDesgloseNave(MapeoDesgloseNaves r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoDesgloseNaves();
			
			if (!isBusqueda())
			{
				cps = consultaDesgloseNavesServer.getInstance(CurrentUser.get());
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoDesgloseNaves>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarDesgloseNave(MapeoDesgloseNaves r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((DesgloseNavesGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((DesgloseNavesGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
			HashMap<String,String> filtrosRec = ((DesgloseNavesGrid) uw.grid).recogerFiltros();
			ArrayList<MapeoDesgloseNaves> r_vector = (ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector;
			
			Indexed indexed = ((DesgloseNavesGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

            ((DesgloseNavesGrid) uw.grid).sort(Sort.by("nombre").then("filas"));
			((DesgloseNavesGrid) uw.grid).establecerFiltros(filtrosRec);
		}
	}
	
	public void modificarDesgloseNave(MapeoDesgloseNaves r_mapeo, MapeoDesgloseNaves r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
//		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo==null)
		{
			if (!((DesgloseNavesGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoDesgloseNaves> r_vector = (ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((DesgloseNavesGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((DesgloseNavesGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	            uw.presentarGrid(r_vector);
	            
				((DesgloseNavesGrid) uw.grid).setRecords((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector);
				((DesgloseNavesGrid) uw.grid).sort(Sort.by("nombre").then("filas"));
				((DesgloseNavesGrid) uw.grid).scrollTo(r_mapeo);
				((DesgloseNavesGrid) uw.grid).select(r_mapeo);
				
				((DesgloseNavesGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearDesgloseNave(MapeoDesgloseNaves r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdCodigo(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoDesgloseNaves>(r_mapeo));
			if (((DesgloseNavesGrid) uw.grid)!=null )//&& )
			{
				if (!((DesgloseNavesGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((DesgloseNavesGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((DesgloseNavesGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector).add(r_mapeo);
					((DesgloseNavesGrid) uw.grid).setRecords((ArrayList<MapeoDesgloseNaves>) ((DesgloseNavesGrid) uw.grid).vector);
					((DesgloseNavesGrid) uw.grid).sort(Sort.by("nombre").then("filas"));
					((DesgloseNavesGrid) uw.grid).scrollTo(r_mapeo);
					((DesgloseNavesGrid) uw.grid).select(r_mapeo);
					
					((DesgloseNavesGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoDesgloseNaves> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
		this.nombre.setRequired(r_obliga);
		this.filas.setRequired(r_obliga);
		this.calle.setRequired(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.calle.getValue()==null || this.calle.getValue().toString().length()==0) return false;
			if (this.filas.getValue()==null || this.filas.getValue().toString().length()==0) return false;
			if (this.nombre.getValue()==null || this.nombre.getValue().toString().length()==0) return false;
		}
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
	
	public void cargarCombo()
	{
		ArrayList<MapeoNaves> vector = null;
		consultaNavesServer cns = consultaNavesServer.getInstance(CurrentUser.get());
		
		vector = cns.datosNavesGlobal(null);
		Iterator<MapeoNaves> iter = vector.iterator();
		
		while (iter.hasNext())
		{
			MapeoNaves mapeo = (MapeoNaves) iter.next();
			this.nombre.addItem(mapeo.getNombre());
		}
	}
}
