package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view.FincasVendimiaView;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view.pantallaLineasEntradaCosecha;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class FincasVendimiaGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	
//	private PeticionEtiquetasEmbotellado vtPeticion = null;
	private boolean editable = false;
	private boolean conFiltro = true;
	
//	public Integer permisos = null;
//	private Integer indice = null;	
	private MapeoFincasVendimia mapeo=null;
	private MapeoFincasVendimia mapeoOrig=null;
	private FincasVendimiaView app = null;
	
    public FincasVendimiaGrid(FincasVendimiaView r_app , ArrayList<MapeoFincasVendimia> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("CONTROL FINCAS ENTREGADAS");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoFincasVendimia.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.NONE);
		
		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("estadoCT", "par", "parcelas", "entregas","finca", "socio","nombre" , "municipio", "superficie",  "rendimientoTeorico", "rendimiento", "rendimientoDisponible", "variedad", "secre", "alias", "nombreParaje");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("estadoCT", "85");
    	this.widthFiltros.put("finca", "85");
    	this.widthFiltros.put("socio", "85");
    	this.widthFiltros.put("nombre", "225");
    	this.widthFiltros.put("municipio", "85");
    	this.widthFiltros.put("variedad", "85");
    	this.widthFiltros.put("agregado", "85");
    	this.widthFiltros.put("nombreParaje", "225");
    	this.widthFiltros.put("alias", "225");

    	this.getColumn("par").setHeaderCaption("");
    	this.getColumn("par").setSortable(false);
    	this.getColumn("par").setWidth(new Double(40));
    	this.getColumn("parcelas").setHeaderCaption("");
    	this.getColumn("parcelas").setSortable(false);
    	this.getColumn("parcelas").setWidth(new Double(40));
    	this.getColumn("entregas").setHeaderCaption("");
    	this.getColumn("entregas").setSortable(false);
    	this.getColumn("entregas").setWidth(new Double(40));

    	this.getColumn("socio").setHeaderCaption("Socio");
    	this.getColumn("socio").setWidthUndefined();
    	this.getColumn("socio").setWidth(120);
    	this.getColumn("nombre").setHeaderCaption("Nombre");
    	this.getColumn("nombre").setWidthUndefined();
    	this.getColumn("nombre").setWidth(245);
    	this.getColumn("municipio").setHeaderCaption("Municipio");
    	this.getColumn("municipio").setWidth(120);
    	this.getColumn("superficie").setHeaderCaption("SUP");
    	this.getColumn("superficie").setWidth(85);
    	this.getColumn("rendimientoTeorico").setHeaderCaption("Rdto. Teorico");
    	this.getColumn("rendimientoTeorico").setWidth(125);
    	this.getColumn("rendimiento").setHeaderCaption("Rdto.");
    	this.getColumn("rendimiento").setWidth(125);
    	this.getColumn("rendimientoDisponible").setHeaderCaption("Disponible");
    	this.getColumn("rendimientoDisponible").setWidth(125);
    	this.getColumn("alias").setHeaderCaption("Alias");
    	this.getColumn("alias").setWidth(225);
    	this.getColumn("nombreParaje").setHeaderCaption("Paraje");
    	this.getColumn("nombreParaje").setWidth(500);
    	this.getColumn("secre").setHeaderCaption("Secre");
    	this.getColumn("secre").setWidth(120);
    	this.getColumn("variedad").setHeaderCaption("Variedad");
    	this.getColumn("variedad").setWidth(125);

    	this.getColumn("agregado").setHeaderCaption("");
    	this.getColumn("agregado").setWidth(125);

    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("par").setHidden(true);
    	this.getColumn("parcelas").setHidden(true);
    	this.getColumn("entregas").setHidden(true);
    	this.getColumn("plantacion").setHidden(true);
    	this.getColumn("paraje").setHidden(true);
    	this.getColumn("idFincaVendimia").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	
//    	this.getColumn("agregado").setHidden(true);
    	this.getColumn("cif").setHidden(true);
    	this.getColumn("cosechero").setHidden(true);
    	this.getColumn("parcela").setHidden(true);
    	this.getColumn("poligono").setHidden(true);
    	this.getColumn("provincia").setHidden(true);
    	this.getColumn("recinto").setHidden(true);
    	this.getColumn("subParcela").setHidden(true);
    	this.getColumn("superficieParcela").setHidden(true);    	
    	this.getColumn("zona").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoFincasVendimia) {
                MapeoFincasVendimia row = (MapeoFincasVendimia)bean;
                // The actual description text is depending on the application
                if ("estado".equals(cell.getPropertyId()))
                {
                	switch (row.getEstadoCT())
                	{
                		case "CT":
                		{
                			descriptionText = "Cerrada";
                			break;
                		}
                		default: 
                		{
                			descriptionText = "Abierta";
                			break;
                		}
                	}
                	
                }
                else if ("par".equals(cell.getPropertyId()))
                {
                	descriptionText = "Parcelas Asociadas";
                }
                else if ("entregas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Entregas Realizadas";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            Double dblCarga = new Double(0);
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strEstado=cellReference.getValue().toString();
            	}
            	if ("rendimientoTeorico".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) dblCarga=new Double(cellReference.getValue().toString());
            	}
            	if ("rendimiento".equals(cellReference.getPropertyId()) && cellReference.getValue()!=null) 
            	{
            		boolean comparacion = dblCarga.compareTo(new Double(cellReference.getValue().toString()))<0;
            		
            		if (comparacion)
            		{
            			return "Rcell-errorP";
        			}
            		else
            		{
            			return "Rcell-pointer";
            		}
            	}
            	else if ("rendimientoDisponible".equals(cellReference.getPropertyId()) && cellReference.getValue()!=null) 
            	{
            		boolean comparacion = dblCarga.compareTo(new Double(0))<0;
            		
            		if (comparacion)
            		{
            			return "Rcell-errorP";
        			}
            		else
            		{
            			return "Rcell-pointer";
            		}
            	}

            	else if ( "entregas".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "par".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else if ( "finca".equals(cellReference.getPropertyId()) || "socio".equals(cellReference.getPropertyId()) || "municipio".equals(cellReference.getPropertyId()) 
            			|| "superficie".equals(cellReference.getPropertyId()) || "rendimientoTeorico".equals(cellReference.getPropertyId()) 
            			|| "rendimiento".equals(cellReference.getPropertyId()) || "variedad".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("rendimiento"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoFincasVendimia mapeo = (MapeoFincasVendimia) event.getItemId();
            		
            		pantallaLineasEntradaCosecha vt = new pantallaLineasEntradaCosecha(app, mapeo.getEjercicio(), mapeo.getFinca(), "Lineas Entrega Cosecha Finca " + mapeo.getFinca() + " Kilos disponibles: " + mapeo.getRendimientoDisponible(),app.cmbCooperativa.getValue().toString());
        			getUI().addWindow(vt);
            		
            	}
            }
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("superficie");
		this.camposNoFiltrar.add("rendimientoTeorico");
		this.camposNoFiltrar.add("rendimientoDisponible");
		this.camposNoFiltrar.add("rendimiento");
		this.camposNoFiltrar.add("parcelas");
		this.camposNoFiltrar.add("entregas");
		this.camposNoFiltrar.add("par");
	}

	public void generacionPdf(MapeoFincasVendimia r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    	/*
    	 * 
    	 */
//    	String defecto=null;
//		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
//		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
//		
//		for (int i=0;i<vector.size();i++)
//		{
//			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
//		}
//		
//		if (defecto.length()==0) defecto="PANTALLA";
//		
//		
//    	consultaRetrabajosServer cps = new consultaRetrabajosServer(CurrentUser.get());
//    	pdfGenerado = cps.generarInforme(r_mapeo);
//    	if(pdfGenerado.length()>0)
//    	{
//			try
//            { 
//				if (r_impresora.toUpperCase().equals("PANTALLA"))
//				{
//					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//				}
//				else
//				{
//					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
//					
//					RutinasFicheros.imprimir(archivo, r_impresora);
//				    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
//				}
//				
//            }
//			catch(Exception e)
//			{ 
//                 Notificaciones.getInstance().mensajeSeguimiento(e.getMessage()); 
//			}
//    	}
//    	else
//    		Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Double valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Double) item.getItemProperty("rendimiento").getValue();
        	totalU += valor.longValue();
				
        }
        
        footer.getCell("nombre").setText("Totales ");
		footer.getCell("rendimiento").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("rendimiento").setStyleName("Rcell-pie");
		

	}
}


