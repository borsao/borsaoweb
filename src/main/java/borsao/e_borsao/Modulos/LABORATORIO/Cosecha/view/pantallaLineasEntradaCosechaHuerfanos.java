package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoEntradasCosecha;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoFincasVendimia;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.entradaCosechaGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server.consultaFincasVendimiaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasEntradaCosechaHuerfanos extends Window
{
	public FooterRow footer = null;	
	private Button btnBotonCVentana = null;
	private entradaCosechaGrid gridDatos = null;
	private VerticalLayout principal = null;
	private pantallaLineasEntradaCosecha app = null;
	public String coop = null;
	
	public pantallaLineasEntradaCosechaHuerfanos(pantallaLineasEntradaCosecha r_app, Integer r_ejercicio, String r_titulo, String r_conexion)
	{
		this.app = r_app;
		this.coop=r_conexion;
		
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		
		MapeoFincasVendimia mapeo = new MapeoFincasVendimia();
		mapeo.setEjercicio(r_ejercicio);

		this.generarGrid(mapeo);

		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		principal.setExpandRatio(this.gridDatos, 1);
		
		
		this.setContent(principal);
		this.cargarListeners();


	}

		
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
	}
	
    public void generarGrid(MapeoFincasVendimia r_mapeo)
    {
    	ArrayList<MapeoEntradasCosecha> vector = null;
    	consultaFincasVendimiaServer cfvs = new consultaFincasVendimiaServer(CurrentUser.get());
    	
    	if (this.coop!=null && this.coop.length()>0)
    	{
    		vector = cfvs.obtenerEntradasCosechaHuerfanos(r_mapeo, this.coop);
    	}
    	
    	gridDatos = new entradaCosechaGrid(this, vector);
    	
    }
    
	
	private void cerrar()
	{
		this.app.refrescarDatos();
		close();
	}
}