package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server.consultaFincasVendimiaServer;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view.FincasVendimiaView;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view.pantallaLineasEntradaCosechaHuerfanos;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class entradaCosechaGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = true;
	private String fincaIntroducida =null;
	
//	private PeticionEtiquetasEmbotellado vtPeticion = null;
	private boolean editable = true;
	private boolean conFiltro = true;
	
//	public Integer permisos = null;
//	private Integer indice = null;	
	private MapeoEntradasCosecha mapeo=null;
	private MapeoEntradasCosecha mapeoOrig=null;
	private FincasVendimiaView app = null;
	private pantallaLineasEntradaCosechaHuerfanos win = null;
	
    public entradaCosechaGrid(FincasVendimiaView r_app, ArrayList<MapeoEntradasCosecha> r_vector) 
    {
        this.vector=r_vector;
        
        this.app=r_app;
        this.win=null;
        this.setSizeFull();
        
		this.asignarTitulo("ENTREGA COSECHA");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public entradaCosechaGrid(pantallaLineasEntradaCosechaHuerfanos r_app, ArrayList<MapeoEntradasCosecha> r_vector) 
    {
    	this.vector=r_vector;
    	
    	this.app=null;
        this.win=r_app;
        
    	this.setSizeFull();
    	
    	this.asignarTitulo("ENTREGA COSECHA HUERFANOS");
    	
    	if (this.vector==null || this.vector.size()==0)
    	{
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
    	}
    	else
    	{
    		this.generarGrid();
    	}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoEntradasCosecha.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.SINGLE);
		
		if (this.win!=null)
			this.calcularTotal();
		else if (this.app!=null && this.app.conTotales) this.calcularTotal();
		
    }
    
    

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	
    	this.mapeoOrig = new MapeoEntradasCosecha();
    	this.mapeo=((MapeoEntradasCosecha) getEditedItemId());
    	
    	this.mapeoOrig.setFinca(this.mapeo.getFinca());
    		
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  super.doCancelEditor();
  	}
	

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "finca", "codigo", "socio", "fecha","articulo", "nombreArticulo","recogida" , "kgs_bruto", "tara",  "kgs_neto", "porcentaje", "kgs_neto_liq", "pueblo", "grado", "estadoSanitario", "calidad", "almacen", "nombreSocio", "kgs_neto_liq_grado");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("finca", "85");
    	this.widthFiltros.put("albaran", "85");
    	this.widthFiltros.put("codigo", "85");
    	this.widthFiltros.put("socio", "85");
    	this.widthFiltros.put("pueblo", "85");
    	this.widthFiltros.put("articulo", "85");
    	this.widthFiltros.put("nombreArticulo", "200");
    	this.widthFiltros.put("nombreSocio", "200");
    	this.widthFiltros.put("recogida", "85");
    	this.widthFiltros.put("estadoSanitario", "85");
    	this.widthFiltros.put("calidad", "85");
    	this.widthFiltros.put("almacen", "85");

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidthUndefined();
    	this.getColumn("ejercicio").setWidth(120);
    	this.getColumn("ejercicio").setEditable(false);
    	
    	this.getColumn("finca").setHeaderCaption("Finca");
    	this.getColumn("finca").setWidthUndefined();
    	this.getColumn("finca").setWidth(120);
    	this.getColumn("finca").setEditorField(
				getTextField("La finca es obligatoria", "finca"));
    	
    	this.getColumn("codigo").setHeaderCaption("Albaran");
    	this.getColumn("codigo").setWidthUndefined();
    	this.getColumn("codigo").setWidth(120);
    	this.getColumn("codigo").setEditable(false);

    	this.getColumn("socio").setHeaderCaption("Socio");
    	this.getColumn("socio").setWidthUndefined();
    	this.getColumn("socio").setWidth(120);
    	this.getColumn("socio").setEditable(false);
    	this.getColumn("nombreSocio").setHeaderCaption("Nombre");
    	this.getColumn("nombreSocio").setWidthUndefined();
    	this.getColumn("nombreSocio").setWidth(245);
    	this.getColumn("nombreSocio").setEditable(false);
    	
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(120);
    	this.getColumn("fecha").setEditable(false);
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(125);
    	this.getColumn("articulo").setEditable(false);
    	this.getColumn("nombreArticulo").setHeaderCaption("Descripcion");
    	this.getColumn("nombreArticulo").setWidth(245);
    	this.getColumn("nombreArticulo").setEditable(false);
    	
    	this.getColumn("kgs_bruto").setHeaderCaption("Bruto");
    	this.getColumn("kgs_bruto").setWidth(125);
    	this.getColumn("kgs_bruto").setEditable(false);
    	
    	this.getColumn("tara").setHeaderCaption("Tara");
    	this.getColumn("tara").setWidth(125);
    	this.getColumn("tara").setEditable(false);
    	this.getColumn("kgs_neto").setHeaderCaption("Neto");
    	this.getColumn("kgs_neto").setWidth(125);
    	this.getColumn("kgs_neto").setEditable(false);
    	this.getColumn("porcentaje").setHeaderCaption("Porc.");
    	this.getColumn("porcentaje").setWidth(125);
    	this.getColumn("porcentaje").setEditable(false);
    	this.getColumn("kgs_neto_liq").setHeaderCaption("Liquidos");
    	this.getColumn("kgs_neto_liq").setWidth(225);
    	this.getColumn("kgs_neto_liq").setEditable(false);
    	this.getColumn("pueblo").setHeaderCaption("Pueblo");
    	this.getColumn("pueblo").setWidth(150);
    	this.getColumn("pueblo").setEditable(false);
    	this.getColumn("grado").setHeaderCaption("Grado");
    	this.getColumn("grado").setWidth(120);
    	this.getColumn("grado").setEditable(false);
    	this.getColumn("estadoSanitario").setHeaderCaption("E.S.");
    	this.getColumn("estadoSanitario").setWidth(125);
    	this.getColumn("estadoSanitario").setEditable(false);
    	this.getColumn("calidad").setHeaderCaption("Calidad");
    	this.getColumn("calidad").setWidth(120);
    	this.getColumn("calidad").setEditable(false);
    	this.getColumn("almacen").setHeaderCaption("Almacen");
    	this.getColumn("almacen").setWidth(120);
    	this.getColumn("almacen").setEditable(false);
    	this.getColumn("kgs_neto_liq_grado").setHeaderCaption("Liq-Grado");
    	this.getColumn("kgs_neto_liq_grado").setWidth(120);
    	this.getColumn("kgs_neto_liq_grado").setEditable(false);
    	
    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    	this.getColumn("idFincaVendimia").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoFincasVendimia) {
                MapeoEntradasCosecha row = (MapeoEntradasCosecha)bean;
                // The actual description text is depending on the application
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
		this.asignarTooltips();
		
		this.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "finca".equals(cellReference.getPropertyId()) || "socio".equals(cellReference.getPropertyId()) || "codigo".equals(cellReference.getPropertyId()) || "kgs_bruto".equals(cellReference.getPropertyId())
            			|| "tara".equals(cellReference.getPropertyId()) || "kgs_neto".equals(cellReference.getPropertyId()) || "grado".equals(cellReference.getPropertyId()) || "porcentaje".equals(cellReference.getPropertyId())
            			|| "kgs_neto_liq".equals(cellReference.getPropertyId()) || "kgs_neto_liq_grado".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	return "cell-normal";
            }
        });

    }
    
    public void cargarListeners()
	{
    	
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
//        		mapeo = (MapeoEntradasCosecha) getEditedItemId();
//        		indice = getContainer().indexOfId(mapeo);
		        if(mapeo.getFinca().compareTo(mapeoOrig.getFinca())!=0)
		        	actualizar=true;
		        else doCancelEditor();
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		mapeo = (MapeoEntradasCosecha) getEditedItemId();
	        		
	        		consultaFincasVendimiaServer cs = new consultaFincasVendimiaServer(CurrentUser.get());
		        
	        		if (app!=null)
	        			cs.guardarCambiosEntradaCosecha(mapeo, app.cmbCooperativa.getValue().toString());
	        		else
	        			cs.guardarCambiosEntradaCosecha(mapeo, win.coop);
		        		
	        		((ArrayList<MapeoEntradasCosecha>) vector).remove(mapeoOrig);
	    			((ArrayList<MapeoEntradasCosecha>) vector).add(mapeo);

	    			refreshAllRows();
	    			scrollTo(mapeo);
	    			select(mapeo);
	        	}
	        }
		});
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
//            	if (event.getPropertyId().toString().equals("rendimiento"))
//            	{
//            		/*
//            		 * acceso a pendientes de servir
//            		 */
//            		
//            		MapeoFincasVendimia mapeo = (MapeoFincasVendimia) event.getItemId();
//            		
//            		pantallaLineasEntradaCosecha vt = new pantallaLineasEntradaCosecha(app, mapeo.getEjercicio(), mapeo.getFinca(), "Lineas Entrega Cosecha Finca " + mapeo.getFinca(),app.cmbCooperativa.getValue().toString());
//        			getUI().addWindow(vt);
//            		
//            	}
            }
        });
	}

    
	
	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("kgs_neto");
		this.camposNoFiltrar.add("tara");
		this.camposNoFiltrar.add("kgs_bruto");
		this.camposNoFiltrar.add("fecha");
		this.camposNoFiltrar.add("porcentaje");
		this.camposNoFiltrar.add("kgs_neto_liq");
		this.camposNoFiltrar.add("kgs_neto_liq_grado");
		this.camposNoFiltrar.add("grado");
	}

	public void generacionPdf(MapeoFincasVendimia r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    	/*
    	 * 
    	 */
//    	String defecto=null;
//		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
//		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
//		
//		for (int i=0;i<vector.size();i++)
//		{
//			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
//		}
//		
//		if (defecto.length()==0) defecto="PANTALLA";
//		
//		
//    	consultaRetrabajosServer cps = new consultaRetrabajosServer(CurrentUser.get());
//    	pdfGenerado = cps.generarInforme(r_mapeo);
//    	if(pdfGenerado.length()>0)
//    	{
//			try
//            { 
//				if (r_impresora.toUpperCase().equals("PANTALLA"))
//				{
//					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//				}
//				else
//				{
//					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
//					
//					RutinasFicheros.imprimir(archivo, r_impresora);
//				    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
//				}
//				
//            }
//			catch(Exception e)
//			{ 
//                 Notificaciones.getInstance().mensajeSeguimiento(e.getMessage()); 
//			}
//    	}
//    	else
//    		Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Double valor = null;
    	
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
    	
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Double) item.getItemProperty("kgs_neto_liq").getValue();
        	totalU += valor.longValue();
				
        }
        
        footer.getCell("nombreArticulo").setText("Totales ");
		footer.getCell("kgs_neto_liq").setText(RutinasNumericas.formatearDouble(totalU.toString()));
//		this.setHeight((this.app.getHeight()-10)+"%");
		footer.getCell("kgs_neto_liq").setStyleName("Rcell-pie");

	}

	private Field<?> getTextField(String requiredErrorMsg, String r_nombre) {
		TextField texto = new TextField();
		
		switch (r_nombre)
		{
			case "finca":
			{
				texto.setRequired(true);
				texto.addValueChangeListener(new ValueChangeListener() {
					
					@Override
					public void valueChange(ValueChangeEvent event) {
						if (texto.getValue()!=null)
						{
							consultaFincasVendimiaServer cfvs = new consultaFincasVendimiaServer(CurrentUser.get());
							
							if (app!=null)
								fincaIntroducida=cfvs.obtenerFinca(new Integer(texto.getValue().toString().replace(".", "")),app.cmbCooperativa.getValue().toString(), mapeo.getEjercicio(), mapeo.getSocio());
							else
								fincaIntroducida=cfvs.obtenerFinca(new Integer(texto.getValue().toString().replace(".", "")),win.coop, mapeo.getEjercicio(), mapeo.getSocio());
							
							if (fincaIntroducida==null) 
							{
								Notificaciones.getInstance().mensajeError("Finca no correspondiente al socio");
							}
//							else mapeo.setFinca(new Integer(fincaIntroducida));
							mapeo.setFinca(new Integer(texto.getValue().toString().replace(".", "")));
						}
					}
				});
				
				break;
			}
		}
		return texto;
	}

}

/*
 * 	otra opciones es convertir el campo a combo y no dejar meter mas que las que carguemos. las fincas del socio 
 */
// private Field<?> getComboBox(String requiredErrorMsg) {
//		ComboBox comboBox = new ComboBox();
//		ArrayList<String> valores = new ArrayList<String>();
//		
//		consultaOpcionesMateriaSecaServer cms = new consultaOpcionesMateriaSecaServer(CurrentUser.get());
//		valores = cms.datosOpcionesCombo();
//		
//		IndexedContainer container = new IndexedContainer(valores);
//		comboBox.setContainerDataSource(container);
//		comboBox.setRequired(true);
//		comboBox.setRequiredError(requiredErrorMsg);
//		comboBox.setInvalidAllowed(false);
//		comboBox.setNullSelectionAllowed(false);
//		comboBox.setStyleName("blanco");
//		return comboBox;
//	}

