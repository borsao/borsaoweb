package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.FincasVendimiaGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoFincasVendimia;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server.consultaFincasVendimiaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class FincasVendimiaView extends GridViewRefresh {

	public static final String VIEW_NAME = "Control FINCAS ENTREGADAS";
	public consultaFincasVendimiaServer cfvs =null;
	public boolean modificando = false;
	public TextField txtEjercicio = null;
	public ComboBox cmbCooperativa = null;
	
	private final String titulo = "control FINCAS ENTREGADAS";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	public boolean conTotales = true;
	public FincasVendimiaView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private Button opcImprimir = null;
	private Button opcExportar = null;
	private Button opcExportarAlb = null;
	private Button opcExportarEUD = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public FincasVendimiaView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cfvs= consultaFincasVendimiaServer.getInstance(CurrentUser.get());
		
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.txtEjercicio= new TextField("Ejercicio");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		
		this.cmbCooperativa= new ComboBox("Cooperativa");    		
		this.cmbCooperativa.setNewItemsAllowed(false);
		this.cmbCooperativa.setNullSelectionAllowed(false);
		this.cmbCooperativa.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbCooperativa.addItem("Borja");
		this.cmbCooperativa.addItem("Pozuelo");
		this.cmbCooperativa.addItem("Tabuenca");

		this.opcExportar= new Button("Exportar");    	
		this.opcExportar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcExportar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcExportar.setIcon(FontAwesome.FILE_EXCEL_O);

		this.opcExportarAlb= new Button("Albaranes");    	
		this.opcExportarAlb.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcExportarAlb.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcExportarAlb.setIcon(FontAwesome.FILE_EXCEL_O);

		this.opcExportarEUD= new Button("Entrada UVA");    	
		this.opcExportarEUD.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcExportarEUD.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcExportarEUD.setIcon(FontAwesome.FILE_EXCEL_O);
		
		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.cmbCooperativa);
		this.cabLayout.addComponent(this.opcExportar);
		this.cabLayout.setComponentAlignment(this.opcExportar, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.opcExportarEUD);
		this.cabLayout.setComponentAlignment(this.opcExportarEUD, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.opcExportarAlb);
		this.cabLayout.setComponentAlignment(this.opcExportarAlb, Alignment.BOTTOM_LEFT);
		
		this.cargarListeners();
		this.establecerModo();
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoFincasVendimia> r_vector=new ArrayList<MapeoFincasVendimia>();
    	
    	MapeoFincasVendimia mapeoFincasVendimia =new MapeoFincasVendimia();
    	
    	if (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()>0);
    	{
    		mapeoFincasVendimia.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    	}

    	if (this.cmbCooperativa.getValue()!=null && this.cmbCooperativa.getValue().toString().length()>0)
    	{
    		r_vector=this.cfvs.datosFincasVendimiaGlobal(mapeoFincasVendimia, this.cmbCooperativa.getValue().toString());
    	}
    	
    	this.presentarGrid(r_vector);
    	((FincasVendimiaGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((FincasVendimiaGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((FincasVendimiaGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    public void generarGrid(MapeoFincasVendimia r_mapeo)
    {
    	ArrayList<MapeoFincasVendimia> r_vector=null;
    	r_vector=this.cfvs.datosFincasVendimiaGlobal(r_mapeo, this.cmbCooperativa.getValue().toString());
    	this.presentarGrid(r_vector);
    	((FincasVendimiaGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoFincasVendimia > r_vector)
    {
    	grid = new FincasVendimiaGrid(this, r_vector);
    	if (((FincasVendimiaGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((FincasVendimiaGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
		this.cmbCooperativa.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((FincasVendimiaGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}	
				MapeoFincasVendimia mapeo = new MapeoFincasVendimia();
				
				if (txtEjercicio.getValue()!=null) 
				{
					mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
				}					
				generarGrid(mapeo);
			}
		}); 
    	
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    	
    	this.opcExportar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				exportarFincasDGA(txtEjercicio.getValue().toString(), cmbCooperativa.getValue().toString());
			}
		});

    	this.opcExportarAlb.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				exportarFincasAlbaranes(txtEjercicio.getValue().toString(), cmbCooperativa.getValue().toString());
			}
		});

    	this.opcExportarEUD.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			exportarEntradaUva(txtEjercicio.getValue().toString(), cmbCooperativa.getValue().toString());
    		}
    	});

    }
    private void exportarEntradaUva(String r_ejercicio, String r_cooperativa)
    {
    	/*
    	 * Proceso exportacion csv para generar el fichero de la dga
    	 * 
    	 *  En realidad sacamos la consulta de las fincas agregaremos los datos del sigpac y lo
    	 *  pasaremos todo a csv
    	 */
    	String ruta = null;
    	
    	ruta = RutinasFicheros.generarCsv("EntradaUvaDeposito", new Integer(r_ejercicio), null, null, cmbCooperativa.getValue().toString());
    	if (ruta!=null)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Exportación generada. " + ruta);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Problemas en la generacion del fichero.");
		}
    } 

    private void exportarFincasAlbaranes(String r_ejercicio, String r_cooperativa)
    {
    	/*
    	 * Proceso exportacion csv para generar el fichero de la dga
    	 * 
    	 *  En realidad sacamos la consulta de las fincas agregaremos los datos del sigpac y lo
    	 *  pasaremos todo a csv
    	 */
    	String ruta = null;
    	
    	ruta = RutinasFicheros.generarCsv("FincasVendimiaAlbaranes", new Integer(r_ejercicio), null, null, cmbCooperativa.getValue().toString());
    	if (ruta!=null)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Exportación generada. " + ruta);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No se ha generado fichero.");
		}
    } 
    
    private void exportarFincasDGA(String r_ejercicio, String r_cooperativa)
    {
    	/*
    	 * Proceso exportacion csv para generar el fichero de la dga
    	 * 
    	 *  En realidad sacamos la consulta de las fincas agregaremos los datos del sigpac y lo
    	 *  pasaremos todo a csv
    	 */
    	String ruta = null;
    	
    	ruta = RutinasFicheros.generarCsv("DGAFincasVendimia", new Integer(r_ejercicio), null, null, cmbCooperativa.getValue().toString());
    	if (ruta!=null)
		{
			Notificaciones.getInstance().mensajeAdvertencia("Declaración generada. " + ruta);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Problemas en la generacion del fichero.");
		}
    }
    
	private void establecerModo()
	{
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((FincasVendimiaGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			setHayGrid(false);
		}
		generarGrid(opcionesEscogidas);

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

