package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoFincasVendimia extends MapeoGlobal
{
    private Integer idFincaVendimia;
	private Double superficie;
	private Double rendimientoTeorico;
	private Double rendimiento;
	private Double rendimientoDisponible;
	
	private Double superficieParcela;
	
	private Integer ejercicio;
	private Integer socio;
	private Integer finca;
	private Integer paraje;
	
	private Integer municipio;
	
	private String cif;
	private String cosechero;
	private String provincia;
	private String poligono;
	private String parcela;
	private String agregado;
	private String zona;
	private String subParcela;
	private String recinto;
	
	private String variedad;
	
	private String nombre;
	private String secre;
	private String alias;
	private String nombreParaje;
	private String plantacion;
	private String estadoCT;
	
	private String par;
	private String parcelas;
	private String entregas;

	public MapeoFincasVendimia()
	{
	}

	public Integer getIdFincaVendimia() {
		return idFincaVendimia;
	}

	public void setIdFincaVendimia(Integer idFincaVendimia) {
		this.idFincaVendimia = idFincaVendimia;
	}

	public Double getSuperficie() {
		return superficie;
	}

	public void setSuperficie(Double superficie) {
		this.superficie = superficie;
	}

	public Double getRendimiento() {
		return rendimiento;
	}

	public void setRendimiento(Double rendimiento) {
		this.rendimiento = rendimiento;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSocio() {
		return socio;
	}

	public void setSocio(Integer socio) {
		this.socio = socio;
	}

	public Integer getFinca() {
		return finca;
	}

	public void setFinca(Integer finca) {
		this.finca = finca;
	}

	public Integer getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Integer municipio) {
		this.municipio = municipio;
	}

	public Integer getParaje() {
		return paraje;
	}

	public void setParaje(Integer paraje) {
		this.paraje = paraje;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public String getSecre() {
		return secre;
	}

	public void setSecre(String secre) {
		this.secre = secre;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getNombreParaje() {
		return nombreParaje;
	}

	public void setNombreParaje(String nombreParaje) {
		this.nombreParaje = nombreParaje;
	}

	public String getPlantacion() {
		return plantacion;
	}

	public void setPlantacion(String plantacion) {
		this.plantacion = plantacion;
	}

	public String getEstadoCT() {
		return estadoCT;
	}

	public void setEstadoCT(String estadoCT) {
		this.estadoCT = estadoCT;
	}

	public String getPar() {
		return par;
	}

	public void setPar(String par) {
		this.par = par;
	}

	public String getParcelas() {
		return parcelas;
	}

	public void setParcelas(String parcelas) {
		this.parcelas = parcelas;
	}

	public String getEntregas() {
		return entregas;
	}

	public void setEntregas(String entregas) {
		this.entregas = entregas;
	}

	public Double getRendimientoTeorico() {
		return rendimientoTeorico;
	}

	public void setRendimientoTeorico(Double rendimientoTeorico) {
		this.rendimientoTeorico = rendimientoTeorico;
	}

	public Double getRendimientoDisponible() {
		return rendimientoDisponible;
	}

	public void setRendimientoDisponible(Double rendimientoDisponible) {
		this.rendimientoDisponible = rendimientoDisponible;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public Double getSuperficieParcela() {
		return superficieParcela;
	}

	public void setSuperficieParcela(Double superficieParcela) {
		this.superficieParcela = superficieParcela;
	}

	public String getPoligono() {
		return poligono;
	}

	public void setPoligono(String poligono) {
		this.poligono = poligono;
	}

	public String getParcela() {
		return parcela;
	}

	public void setParcela(String parcela) {
		this.parcela = parcela;
	}

	public String getAgregado() {
		return agregado;
	}

	public void setAgregado(String agregado) {
		this.agregado = agregado;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getSubParcela() {
		return subParcela;
	}

	public void setSubParcela(String subParcela) {
		this.subParcela = subParcela;
	}

	public String getRecinto() {
		return recinto;
	}

	public void setRecinto(String recinto) {
		this.recinto = recinto;
	}

	public String getCosechero() {
		return cosechero;
	}

	public void setCosechero(String cosechero) {
		this.cosechero = cosechero;
	}

}