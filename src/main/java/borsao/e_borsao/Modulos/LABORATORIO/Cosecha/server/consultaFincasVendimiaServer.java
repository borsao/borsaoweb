package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.modelo.MapeoContadoresEjercicio;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoEntradasCosecha;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoFincasVendimia;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaFincasVendimiaServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaFincasVendimiaServer instance;
	private Integer topeHas = 0;
	private HashMap<Integer , String> socios = null;
	
	public consultaFincasVendimiaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaFincasVendimiaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaFincasVendimiaServer(r_usuario);			
		}
		return instance;
	}
	
	private void obtenerTopeHa(Integer r_ejercicio)
	{
		ArrayList<MapeoContadoresEjercicio> vec = null;
		MapeoContadoresEjercicio mapeoContador = null;
		Iterator<MapeoContadoresEjercicio> iter = null;
		consultaContadoresEjercicioServer cces = new consultaContadoresEjercicioServer(CurrentUser.get());
		mapeoContador = new MapeoContadoresEjercicio();
		mapeoContador.setEjercicio(r_ejercicio);
		mapeoContador.setOrigen("FINCAS");
		mapeoContador.setArea("RENDIMIENTO");
		
		vec = cces.datosOpcionesGlobal(mapeoContador);
		
		iter = vec.iterator();
		
		while (iter.hasNext())
		{
			MapeoContadoresEjercicio mapeo = new MapeoContadoresEjercicio();
			mapeo = iter.next();
			topeHas = mapeo.getContador().intValue();
		}
		
	}
	public ArrayList<MapeoFincasVendimia> datosFincasVendimiaGlobal(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		HashMap<Integer, Double> hashFincasKilos=null;
		HashMap<String , Integer > hashVariedades=null;
		ArrayList<MapeoFincasVendimia> vector = null;
		StringBuffer cadenaSQL = null;
		MapeoFincasVendimia mapeoFincasVendimia=null;
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
				rdo = this.aplicarCambios(r_conexion,r_mapeo.getEjercicio());
				
				if (rdo==null)
				{
					obtenerTopeHa(r_mapeo.getEjercicio());
					hashFincasKilos = this.obtenerKilosEntregadosPorFincas(r_mapeo.getEjercicio(),r_conexion);
					hashVariedades = this.comprobarVariedades(r_conexion, r_mapeo.getEjercicio());
					
					cadenaSQL = new StringBuffer();
					
					cadenaSQL.append(" SELECT a.ejercicio fin_eje, ");
					cadenaSQL.append(" a.socio fin_soc, ");
					cadenaSQL.append(" a.finca fin_fin, ");
					cadenaSQL.append(" b.nombre soc_nom, ");
					cadenaSQL.append(" a.municipio fin_mun, ");
					cadenaSQL.append(" a.superficie fin_sup, ");
					cadenaSQL.append(" a.superficie * " + topeHas + " fin_rdt, ");
					cadenaSQL.append(" a.variedad fin_var, ");
					cadenaSQL.append(" a.secre fin_sr, ");
					cadenaSQL.append(" a.alias fin_ali, ");
					cadenaSQL.append(" a.estado fin_est, ");
					cadenaSQL.append(" c.nombre par_nom ");
					cadenaSQL.append(" from e__fincv a, e__socio b, e__paraj c ");
					cadenaSQL.append(" where a.socio=b.socio ");
					cadenaSQL.append(" and a.paraje=c.paraje ");
					cadenaSQL.append(" and a.ejercicio = '" + r_mapeo.getEjercicio() + "' ");
					cadenaSQL.append(" order by 1,2,3");
					
					if (r_conexion.equals("Borja"))
					{
						con= this.conManager.establecerConexionGestionBorja();			
					}
					else if (r_conexion.equals("Tabuenca"))
					{
						con= this.conManager.establecerConexionGestionTabuenca();			
					}
					else if (r_conexion.equals("Pozuelo"))
					{
						con= this.conManager.establecerConexionGestionPozuelo();			
					}
					
					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					
					vector = new ArrayList<MapeoFincasVendimia>();
					
					while(rsOpcion.next())
					{
						mapeoFincasVendimia = new MapeoFincasVendimia();
						/*
						 * recojo mapeo operarios
						 */
						mapeoFincasVendimia.setEjercicio(rsOpcion.getInt("fin_eje"));
						mapeoFincasVendimia.setSocio(rsOpcion.getInt("fin_soc"));
						mapeoFincasVendimia.setFinca(rsOpcion.getInt("fin_fin"));
						mapeoFincasVendimia.setNombre(RutinasCadenas.conversion(rsOpcion.getString("soc_nom")));
						mapeoFincasVendimia.setNombreParaje(RutinasCadenas.conversion(rsOpcion.getString("par_nom")));
						mapeoFincasVendimia.setMunicipio(rsOpcion.getInt("fin_mun"));
						mapeoFincasVendimia.setSuperficie(rsOpcion.getDouble("fin_sup"));
						mapeoFincasVendimia.setRendimientoTeorico(rsOpcion.getDouble("fin_rdt"));
						
						mapeoFincasVendimia.setVariedad(rsOpcion.getString("fin_var"));
						mapeoFincasVendimia.setSecre(rsOpcion.getString("fin_sr"));
						mapeoFincasVendimia.setAlias(RutinasCadenas.conversion(rsOpcion.getString("fin_ali")));
						mapeoFincasVendimia.setEstadoCT(rsOpcion.getString("fin_est"));
						mapeoFincasVendimia.setRendimiento(new Double(0));
						
						if (hashVariedades.get(rsOpcion.getString("fin_fin"))!=null) mapeoFincasVendimia.setAgregado(hashVariedades.get(rsOpcion.getString("fin_fin")).toString()); else mapeoFincasVendimia.setAgregado("0");
						
						if (hashFincasKilos.get(mapeoFincasVendimia.getFinca())!=null) mapeoFincasVendimia.setRendimiento(hashFincasKilos.get(mapeoFincasVendimia.getFinca())); else mapeoFincasVendimia.setRendimiento(new Double(0));
						
						mapeoFincasVendimia.setRendimientoDisponible(mapeoFincasVendimia.getRendimientoTeorico()-mapeoFincasVendimia.getRendimiento());
						vector.add(mapeoFincasVendimia);				
					}
						
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se han podido aplicar los cambios en la tabla de " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}
	
	
	private HashMap<Integer, Double> obtenerKilosEntregadosPorFincas(Integer r_ejercicio, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		HashMap<Integer, Double> hashFincasKilos=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(kgs_neto_liq) ent_kgs,");
		cadenaSQL.append(" finca ent_fin ");
		
		if (r_conexion.equals("Borja"))
		{
			cadenaSQL.append(" from e__ccs_bor");			
		}
		else if (r_conexion.equals("Tabuenca"))
		{
			cadenaSQL.append(" from e__ccs_tab");			
		}
		else if (r_conexion.equals("Pozuelo"))
		{
			cadenaSQL.append(" from e__ccs_poz");			
		}

		cadenaSQL.append(" where cultivo = 1 ");
		cadenaSQL.append(" and ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" group by 2 ");
		try
		{

			con= this.conManager.establecerConexion();			
			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hashFincasKilos=new HashMap<Integer, Double>();
				
				while(rsOpcion.next())
				{
					hashFincasKilos.put(rsOpcion.getInt("ent_fin"), rsOpcion.getDouble("ent_kgs"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}

		return hashFincasKilos;
			
	}
	
	private HashMap<Integer, Double> obtenerKilosDisponiblesPorFincas(Integer r_ejercicio, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		HashMap<Integer, Double> hashFincasKilos=null;
		HashMap<Integer, Double> hashFincasSuperficies=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(kgs_neto_liq) ent_kgs,");
		cadenaSQL.append(" finca ent_fin ");
		
		
		if (r_conexion.equals("Borja"))
		{
			cadenaSQL.append(" from e__ccs_bor");
		}
		else if (r_conexion.equals("Tabuenca"))
		{
			cadenaSQL.append(" from e__ccs_tab");			
		}
		else if (r_conexion.equals("Pozuelo"))
		{
			cadenaSQL.append(" from e__ccs_poz");			
		}

		cadenaSQL.append(" where cultivo = 1 ");
//		cadenaSQL.append(" and finca = 4670 ");
		cadenaSQL.append(" and ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" group by 2 ");
		try
		{

			con= this.conManager.establecerConexion();			
			if (con!=null)
			{
				hashFincasSuperficies = obtenerSuperficiesPorFincas(r_ejercicio,r_conexion);
				
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hashFincasKilos=new HashMap<Integer, Double>();
				
				while(rsOpcion.next())
				{
					BigDecimal valor = new BigDecimal(0);
					if (hashFincasSuperficies.get(rsOpcion.getInt("ent_fin"))!=null)
					{
						
						valor=new BigDecimal(hashFincasSuperficies.get(rsOpcion.getInt("ent_fin")).doubleValue() - rsOpcion.getDouble("ent_kgs")).setScale(2, RoundingMode.UP);
						
//						valor = new Double(RutinasNumericas.formatearDouble(valor).replace(",", "."));
						
						hashFincasKilos.put(rsOpcion.getInt("ent_fin"), valor.doubleValue());
					}
					else 
						hashFincasKilos.put(rsOpcion.getInt("ent_fin"), valor.doubleValue());
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}

		return hashFincasKilos;
			
	}

	private HashMap<Integer, Double> obtenerSuperficiesPorFincas(Integer r_ejercicio, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		HashMap<Integer, Double> hashFincasKilos=null;
		Connection conex = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT superficie * " + topeHas.intValue() + " fin_Sup,");
		cadenaSQL.append(" finca fin_fin ");
		cadenaSQL.append(" from e__fincv ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
//		cadenaSQL.append(" and finca = 4670  ");

		try
		{

			if (r_conexion.equals("Borja"))
			{
				conex= this.conManager.establecerConexionGestionBorja();	
			}
			else if (r_conexion.equals("Tabuenca"))
			{
				conex= this.conManager.establecerConexionGestionTabuenca();
			}
			else if (r_conexion.equals("Pozuelo"))
			{
				conex= this.conManager.establecerConexionGestionPozuelo();
			}
			if (con!=null)
			{
				cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hashFincasKilos=new HashMap<Integer, Double>();
				
				while(rsOpcion.next())
				{
					hashFincasKilos.put(rsOpcion.getInt("fin_fin"), rsOpcion.getDouble("fin_sup"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}

		return hashFincasKilos;
			
	}

	private HashMap<String, Integer> comprobarVariedades(String r_conexion, Integer r_ejercicio)
	{
		String rdo = null;
		HashMap<String, Integer> hashRdo=null;
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(distinct substring(articulo,1,4)) ent_cuan, ");
		cadenaSQL.append(" finca ent_fin ");
		
		if (r_conexion.equals("Borja"))
		{
			cadenaSQL.append(" from e__ccs_bor");			
		}
		else if (r_conexion.equals("Tabuenca"))
		{
			cadenaSQL.append(" from e__ccs_tab");			
		}
		else if (r_conexion.equals("Pozuelo"))
		{
			cadenaSQL.append(" from e__ccs_poz");			
		}

		cadenaSQL.append(" where cultivo = 1 ");
		cadenaSQL.append(" and ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" group by 2 ");
//		cadenaSQL.append(" and finca = '" + r_finca+ "' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				hashRdo = new HashMap<String, Integer>();
				
				while(rsOpcion.next())
				{
					hashRdo.put(rsOpcion.getString("ent_fin"),rsOpcion.getInt("ent_cuan"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashRdo;
	}
	//
	public ArrayList<MapeoEntradasCosecha> datosFincasVendimiaAlbGlobal(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		String rdo = null;
		Connection conectMysql = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoEntradasCosecha> vector = null;
		MapeoEntradasCosecha mapeoEntradasCosecha=null;
		HashMap<String, String> hash_fincasVariedad = null;
		HashMap<String, String> hash_descripciones = null;
		StringBuffer cadenaSQL = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
			
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" SELECT e__fincv.finca fin_fin,");
				cadenaSQL.append(" e__fincv.variedad fin_var ");
				cadenaSQL.append(" from e__fincv");
				cadenaSQL.append(" where e__fincv.ejercicio= '" + r_mapeo.getEjercicio() + "' ");

				if (r_conexion.equals("Borja"))
				{
					con= this.conManager.establecerConexionGestionBorja();			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					con= this.conManager.establecerConexionGestionTabuenca();			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					con= this.conManager.establecerConexionGestionPozuelo();			
				}

				if (con!=null)
				{
					hash_descripciones = cas.obtenerDescripcionesArticulos(con);
					
					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					hash_fincasVariedad = new HashMap<String, String>();
					
					while(rsOpcion.next())
					{
						hash_fincasVariedad.put(rsOpcion.getString("fin_fin"), rsOpcion.getString("fin_Var"));
					}
				}
				String bodega="";
				if (r_conexion.equals("Borja"))
				{
					bodega="bor";			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					bodega="tab";			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					bodega="poz";						
				}
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" SELECT e__ccs_"+ bodega + ".codigo ent_cod,");
				cadenaSQL.append(" e__ccs_"+ bodega + ".finca ent_fin, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".articulo ent_art, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_neto_liq ent_kg, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".socio ent_soc, ");				
				cadenaSQL.append(" e__socio_"+ bodega + ".nombre soc_nom, ");
				cadenaSQL.append(" substring(e__ccs_"+ bodega + ".articulo,3,2) ent_var ");
				cadenaSQL.append(" from e__ccs_"+ bodega + ",e__socio_"+ bodega );
				cadenaSQL.append(" where e__ccs_"+ bodega + ".socio = e__socio_"+ bodega + ".socio ");
				cadenaSQL.append(" and e__ccs_"+ bodega + ".cultivo = 1 ");
				cadenaSQL.append(" and e__ccs_"+ bodega + ".ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				cadenaSQL.append(" order by 3,1 ");

				conectMysql= this.conManager.establecerConexion();			

				if (conectMysql!=null)
				{
					cs = conectMysql.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					
					vector = new ArrayList<MapeoEntradasCosecha>();
					
					while(rsOpcion.next())
					{
						if (hash_fincasVariedad.get(rsOpcion.getString("ent_fin"))!=null && !rsOpcion.getString("ent_var").trim().contentEquals(hash_fincasVariedad.get(rsOpcion.getString("ent_fin")).trim()))
						{
							mapeoEntradasCosecha = new MapeoEntradasCosecha();
							/*
							 * recojo mapeo operarios
							 */
							mapeoEntradasCosecha.setSocio(rsOpcion.getInt("ent_soc"));
							mapeoEntradasCosecha.setNombreSocio(rsOpcion.getString("soc_nom"));
							mapeoEntradasCosecha.setFinca(rsOpcion.getInt("ent_fin"));
							mapeoEntradasCosecha.setCodigo(rsOpcion.getInt("ent_cod"));
							mapeoEntradasCosecha.setAlmacen(rsOpcion.getString("ent_var"));  //variedad albaran
							mapeoEntradasCosecha.setCalidad(hash_fincasVariedad.get(rsOpcion.getString("ent_fin")).trim()); //variedadFinca
							mapeoEntradasCosecha.setArticulo(rsOpcion.getString("ent_art")); //articulo albaran
							mapeoEntradasCosecha.setNombreArticulo(hash_descripciones.get(rsOpcion.getString("ent_art")));
							mapeoEntradasCosecha.setKgs_neto_liq(rsOpcion.getDouble("ent_kg"));
							vector.add(mapeoEntradasCosecha);
						}
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());		
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoEntradasCosecha> datosEntradasUvaDeposito(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		String rdo = null;
		Connection conectMysql = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoEntradasCosecha> vector = null;
		MapeoEntradasCosecha mapeoEntradasCosecha=null;
		HashMap<String, String> hash_fincasVariedad = null;
		HashMap<String, String> hash_descripciones = null;
		HashMap<Integer, Double> hashFincasKilos=null;
		StringBuffer cadenaSQL = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
				obtenerTopeHa(r_mapeo.getEjercicio());
				hashFincasKilos = this.obtenerKilosDisponiblesPorFincas(r_mapeo.getEjercicio(), r_conexion);
				
				if (r_conexion.equals("Borja"))
				{
					con= this.conManager.establecerConexionGestionBorja();			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					con= this.conManager.establecerConexionGestionTabuenca();			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					con= this.conManager.establecerConexionGestionPozuelo();			
				}

				if (con!=null)
				{
					hash_descripciones = cas.obtenerDescripcionesArticulos(con);
				}

				String bodega="";
				if (r_conexion.equals("Borja"))
				{
					bodega="bor";			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					bodega="tab";			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					bodega="poz";						
				}
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" SELECT e__ccs_"+ bodega + ".codigo ent_cod,");
				cadenaSQL.append(" e__ccs_"+ bodega + ".fecha ent_fec, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".socio ent_soc, ");				
				cadenaSQL.append(" e__socio_"+ bodega + ".nombre soc_nom, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".articulo ent_art, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".tipo_recogida ent_rec, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_bruto ent_kgb, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_tara ent_kgt, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_neto ent_kgn, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".porcentaje ent_por, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_neto_liq ent_kg, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".finca ent_fin, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".cod_pueblo ent_pue, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".grado ent_gr, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".estado_sanit ent_san, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".calidad ent_cal, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".almacen ent_al, ");
				cadenaSQL.append(" e__ccs_"+ bodega + ".kgs_neto_liq * e__ccs_"+ bodega + ".grado ent_exp ");
				cadenaSQL.append(" from e__ccs_"+ bodega + ",e__socio_"+ bodega );
				cadenaSQL.append(" where e__ccs_"+ bodega + ".socio = e__socio_"+ bodega + ".socio ");
				cadenaSQL.append(" and e__ccs_"+ bodega + ".cultivo = 1 ");
				cadenaSQL.append(" and e__ccs_"+ bodega + ".ejercicio = '" + r_mapeo.getEjercicio() + "' ");
//				cadenaSQL.append(" and e__ccs_"+ bodega + ".finca = 4670 ");
				cadenaSQL.append(" order by 1 ");

				conectMysql= this.conManager.establecerConexion();			

				if (conectMysql!=null)
				{
					cs = conectMysql.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					
					vector = new ArrayList<MapeoEntradasCosecha>();
					
					while(rsOpcion.next())
					{
						mapeoEntradasCosecha = new MapeoEntradasCosecha();
						/*
						 * recojo mapeo operarios
						 */
						mapeoEntradasCosecha.setCodigo(rsOpcion.getInt("ent_cod"));
						mapeoEntradasCosecha.setFecha(rsOpcion.getDate("ent_Fec"));
						mapeoEntradasCosecha.setSocio(rsOpcion.getInt("ent_soc"));
						mapeoEntradasCosecha.setNombreSocio(rsOpcion.getString("soc_nom"));
						mapeoEntradasCosecha.setArticulo(rsOpcion.getString("ent_art")); //articulo albaran
						mapeoEntradasCosecha.setNombreArticulo(hash_descripciones.get(rsOpcion.getString("ent_art")));
						mapeoEntradasCosecha.setRecogida(rsOpcion.getString("ent_rec"));
						mapeoEntradasCosecha.setKgs_bruto(rsOpcion.getDouble("ent_kgb"));
						mapeoEntradasCosecha.setTara(rsOpcion.getDouble("ent_kgt"));
						mapeoEntradasCosecha.setKgs_neto(rsOpcion.getDouble("ent_kgn"));
						mapeoEntradasCosecha.setPorcentaje(rsOpcion.getDouble("ent_por"));
						mapeoEntradasCosecha.setKgs_neto_liq(rsOpcion.getDouble("ent_kg"));
						mapeoEntradasCosecha.setFinca(rsOpcion.getInt("ent_fin"));
						mapeoEntradasCosecha.setPueblo(rsOpcion.getString("ent_pue"));
						mapeoEntradasCosecha.setGrado(rsOpcion.getDouble("ent_gr"));
						mapeoEntradasCosecha.setEstadoSanitario(rsOpcion.getString("ent_san"));
						mapeoEntradasCosecha.setCalidad(rsOpcion.getString("ent_cal"));
						mapeoEntradasCosecha.setAlmacen(rsOpcion.getString("ent_al"));  
						mapeoEntradasCosecha.setKgs_neto_liq_grado(rsOpcion.getDouble("ent_exp"));
						if (hashFincasKilos.get(rsOpcion.getInt("ent_fin"))!=null) mapeoEntradasCosecha.setDisponible(hashFincasKilos.get(rsOpcion.getInt("ent_fin"))); else mapeoEntradasCosecha.setDisponible(new Double(0));
						vector.add(mapeoEntradasCosecha);
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());		
				ex.printStackTrace();
			}
		}
		return vector;
	}
	public ArrayList<MapeoFincasVendimia> datosFincasVendimiaDGAGlobal(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoFincasVendimia> vector = null;
		ArrayList<MapeoFincasVendimia> vector2 = null;
		MapeoFincasVendimia mapeoFincasVendimia=null;
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
				rdo = this.aplicarCambios(r_conexion,r_mapeo.getEjercicio());
				obtenerTopeHa(r_mapeo.getEjercicio()); 
				
				if (rdo==null)
				{
				
					StringBuffer cadenaSQL = new StringBuffer();
					cadenaSQL.append(" SELECT sum(kgs_neto_liq) ent_kgs,");
					cadenaSQL.append(" finca ent_fin, ");
					cadenaSQL.append(" socio ent_soc ");
					
					if (r_conexion.equals("Borja"))
					{
						cadenaSQL.append(" from e__ccs_bor");			
					}
					else if (r_conexion.equals("Tabuenca"))
					{
						cadenaSQL.append(" from e__ccs_tab");			
					}
					else if (r_conexion.equals("Pozuelo"))
					{
						cadenaSQL.append(" from e__ccs_poz");			
					}
	
					cadenaSQL.append(" where cultivo = 1 ");
					cadenaSQL.append(" and ejercicio = '" + r_mapeo.getEjercicio() + "' ");
					cadenaSQL.append(" group by 2,3 ");
					
					con= this.conManager.establecerConexion();			
					if (con!=null)
					{
						cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
						rsOpcion= cs.executeQuery(cadenaSQL.toString());
						Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
						
						vector2 = new ArrayList<MapeoFincasVendimia>();
						
						while(rsOpcion.next())
						{
							mapeoFincasVendimia = new MapeoFincasVendimia();
							/*
							 * recojo mapeo operarios
							 */
							mapeoFincasVendimia.setFinca(rsOpcion.getInt("ent_fin"));
							mapeoFincasVendimia.setRendimiento(rsOpcion.getDouble("ent_kgs"));
							mapeoFincasVendimia.setSocio(rsOpcion.getInt("ent_soc"));
							
							vector2.add(mapeoFincasVendimia);				
						}
						
						
						cadenaSQL = new StringBuffer();
						
						cadenaSQL.append(" SELECT a.ejercicio fin_eje, ");
						cadenaSQL.append(" a.socio fin_soc, ");
						cadenaSQL.append(" a.finca fin_fin, ");
						cadenaSQL.append(" a.provincia fin_pro, ");
						cadenaSQL.append(" b.nombre soc_nom, ");
						cadenaSQL.append(" b.cif soc_cif, ");
						cadenaSQL.append(" a.municipio fin_mun, ");
						cadenaSQL.append(" a.superficie fin_sup, ");
						cadenaSQL.append(" a.superficie * " + topeHas + " fin_rdt, ");
						cadenaSQL.append(" a.variedad fin_var, ");
						cadenaSQL.append(" a.secre fin_sr, ");
						cadenaSQL.append(" a.alias fin_ali, ");
						cadenaSQL.append(" a.estado fin_est, ");
						cadenaSQL.append(" c.nombre par_nom ");
						cadenaSQL.append(" from e__fincv a, e__socio b, e__paraj c ");
						cadenaSQL.append(" where a.socio=b.socio ");
						cadenaSQL.append(" and a.paraje=c.paraje ");
						cadenaSQL.append(" and a.ejercicio = '" + r_mapeo.getEjercicio() + "' ");
						cadenaSQL.append(" order by 1,2,3");
						
						if (r_conexion.equals("Borja"))
						{
							con= this.conManager.establecerConexionGestionBorja();			
						}
						else if (r_conexion.equals("Tabuenca"))
						{
							con= this.conManager.establecerConexionGestionTabuenca();			
						}
						else if (r_conexion.equals("Pozuelo"))
						{
							con= this.conManager.establecerConexionGestionPozuelo();			
						}
						
						cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
						rsOpcion= cs.executeQuery(cadenaSQL.toString());
						Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
						
						vector = new ArrayList<MapeoFincasVendimia>();
						
						while(rsOpcion.next())
						{
							/*
							 * recojo mapeo operarios
							 */
							ArrayList<MapeoFincasVendimia> vectorRegistro = this.recogerDatosRegistroFinca(rsOpcion.getInt("fin_eje"), rsOpcion.getInt("fin_fin"), r_conexion);
							
							for (int j=0;j<vectorRegistro.size();j++)
							{
							
								mapeoFincasVendimia = new MapeoFincasVendimia();
								
								mapeoFincasVendimia.setEjercicio(rsOpcion.getInt("fin_eje"));
								mapeoFincasVendimia.setSocio(rsOpcion.getInt("fin_soc"));
								mapeoFincasVendimia.setCif(rsOpcion.getString("soc_cif"));
								
								mapeoFincasVendimia.setFinca(rsOpcion.getInt("fin_fin"));
								mapeoFincasVendimia.setNombre(RutinasCadenas.conversion(rsOpcion.getString("soc_nom")));
								mapeoFincasVendimia.setNombreParaje(RutinasCadenas.conversion(rsOpcion.getString("par_nom")));
								mapeoFincasVendimia.setMunicipio(rsOpcion.getInt("fin_mun"));
								mapeoFincasVendimia.setSuperficie(rsOpcion.getDouble("fin_sup"));
								mapeoFincasVendimia.setRendimientoTeorico(rsOpcion.getDouble("fin_rdt"));
								mapeoFincasVendimia.setProvincia(rsOpcion.getString("fin_pro"));
								mapeoFincasVendimia.setSecre(rsOpcion.getString("fin_sr"));
								mapeoFincasVendimia.setAlias(RutinasCadenas.conversion(rsOpcion.getString("fin_ali")));
								mapeoFincasVendimia.setEstadoCT(rsOpcion.getString("fin_est"));
								mapeoFincasVendimia.setRendimiento(new Double(0));
								
								double kgs_entregados = 0;
								int socio_old = 0;
								int cuantos = 0;
								
								for (int i = 0; i<vector2.size();i++)
								{
									if (mapeoFincasVendimia.getFinca().equals(((MapeoFincasVendimia) vector2.get(i)).getFinca()))
									{
										if (socio_old!=((MapeoFincasVendimia) vector2.get(i)).getSocio())
										{
											cuantos += 1;
											if (!((MapeoFincasVendimia) vector2.get(i)).getSocio().toString().contentEquals(rsOpcion.getString("fin_Soc"))) socio_old=((MapeoFincasVendimia) vector2.get(i)).getSocio();
										}
										
										kgs_entregados = kgs_entregados + ((MapeoFincasVendimia) vector2.get(i)).getRendimiento();
									}
								}
								
								if (cuantos ==1 && socio_old == 0) 
								{
									mapeoFincasVendimia.setCosechero(rsOpcion.getString("soc_cif"));
								}
								else if (cuantos ==1 && socio_old != 0) 
								{
									mapeoFincasVendimia.setCosechero(this.cifSocio(socio_old, r_conexion));
								}
								else if (cuantos == 2) // entrega parcial por porcentaje
								{
									mapeoFincasVendimia.setCosechero(" ");
								}
								else if (cuantos > 2) // más de un cosechero
								{									
									mapeoFincasVendimia.setCosechero(" ");
								}
								else
								{
									mapeoFincasVendimia.setCosechero(rsOpcion.getString("soc_cif"));
								}
									
								mapeoFincasVendimia.setRendimiento(kgs_entregados);
								
								mapeoFincasVendimia.setRendimientoDisponible(mapeoFincasVendimia.getRendimientoTeorico()-mapeoFincasVendimia.getRendimiento());
							
							
								MapeoFincasVendimia mapeo = vectorRegistro.get(j);
								
								mapeoFincasVendimia.setPoligono(mapeo.getPoligono());
								mapeoFincasVendimia.setParcela(mapeo.getParcela());
								mapeoFincasVendimia.setSubParcela(mapeo.getSubParcela());
								mapeoFincasVendimia.setRecinto(mapeo.getRecinto());
								mapeoFincasVendimia.setSuperficieParcela(mapeo.getSuperficieParcela());
								mapeoFincasVendimia.setVariedad(mapeo.getVariedad());

								
								vector.add(mapeoFincasVendimia);				
							}
						}
						
					}
					else
					{
						Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
						return null;
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se han podido aplicar los cambios en la tabla de " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoFincasVendimia> recogerDatosRegistroFinca(Integer r_ejercicio, Integer r_finca, String r_conexion)
	{
		String rdo = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoFincasVendimia> vector = null;
		
		try
		{
			StringBuffer cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT a.poligono reg_pol,");
			cadenaSQL.append(" a.parcela reg_par, ");
			cadenaSQL.append(" a.subparcela reg_sbp, ");
			cadenaSQL.append(" a.recinto reg_rec, ");
			cadenaSQL.append(" a.superficie reg_sup, ");
			cadenaSQL.append(" a.variedad_dga reg_vd " );
			
			if (r_conexion.equals("Borja"))
			{
				cadenaSQL.append(" from e__fireg_bor as a ");
				cadenaSQL.append(" inner join e__sigpa_bor on e__sigpa_bor.sigpac=a.sigpac and e__sigpa_bor.ejercicio=a.ejercicio and a.ejercicio = " + r_ejercicio);
				cadenaSQL.append(" and e__sigpa_bor.finca = " + r_finca );
			}
			else if (r_conexion.equals("Tabuenca"))
			{
				cadenaSQL.append(" from e__fireg_tab as a ");
				cadenaSQL.append(" inner join e__sigpa_tab on e__sigpa_tab.sigpac=a.sigpac and e__sigpa_tab.ejercicio=a.ejercicio and a.ejercicio = " + r_ejercicio);
				cadenaSQL.append(" and e__sigpa_tab.finca = " + r_finca );
			}
			else if (r_conexion.equals("Pozuelo"))
			{
				cadenaSQL.append(" from e__fireg_poz as a ");
				cadenaSQL.append(" inner join e__sigpa_poz on e__sigpa_poz.sigpac=a.sigpac and e__sigpa_poz.ejercicio=a.ejercicio and a.ejercicio = " + r_ejercicio);
				cadenaSQL.append(" and e__sigpa_poz.finca = " + r_finca );
			}

			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
			vector = new ArrayList<MapeoFincasVendimia>();
				
			while(rsOpcion.next())
			{
				MapeoFincasVendimia mapeoFincasVendimia = new MapeoFincasVendimia();
				/*
				 * recojo mapeo operarios
				 */
				mapeoFincasVendimia.setPoligono(rsOpcion.getString("reg_pol"));
				mapeoFincasVendimia.setParcela(rsOpcion.getString("reg_par"));
				mapeoFincasVendimia.setSubParcela(rsOpcion.getString("reg_sbp"));
				mapeoFincasVendimia.setRecinto(rsOpcion.getString("reg_rec"));
				mapeoFincasVendimia.setSuperficieParcela(rsOpcion.getDouble("reg_sup"));
				mapeoFincasVendimia.setVariedad(rsOpcion.getString("reg_vd"));
				
				vector.add(mapeoFincasVendimia);				
			}
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		return vector;			
						
	}
	
	
	public ArrayList<MapeoEntradasCosecha> obtenerEntradasCosecha(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoEntradasCosecha> vector = null;
		MapeoEntradasCosecha mapeoEntradasCosecha=null;
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
				StringBuffer cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" SELECT d.finca ent_fin, ");
				cadenaSQL.append(" d.ejercicio ent_eje, ");
				cadenaSQL.append(" d.fecha ent_fec, ");
				cadenaSQL.append(" d.codigo ent_alb, ");
				cadenaSQL.append(" d.socio ent_soc, ");
				cadenaSQL.append(" d.articulo ent_art, ");
				cadenaSQL.append(" d.tipo_recogida ent_rec, ");
				cadenaSQL.append(" d.kgs_bruto ent_kbr, ");
				cadenaSQL.append(" d.kgs_tara ent_tar,");
				cadenaSQL.append(" d.kgs_neto ent_knt, ");
				cadenaSQL.append(" d.porcentaje ent_por, ");
				cadenaSQL.append(" d.kgs_neto_liq ent_klq, ");
				cadenaSQL.append(" d.cod_pueblo ent_pue, ");
				cadenaSQL.append(" d.grado ent_gr, ");
				cadenaSQL.append(" d.calidad ent_cal, ");
				cadenaSQL.append(" d.almacen ent_alm, ");
				cadenaSQL.append(" d.estado_sanit ent_es, ");
				cadenaSQL.append(" d.kgs_neto_liq*d.grado ent_kg ");
				
				if (r_conexion.equals("Borja"))
				{
					cadenaSQL.append(" from e__ccs_bor d ");			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					cadenaSQL.append(" from e__ccs_tab d ");			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					cadenaSQL.append(" from e__ccs_poz d ");
				}
				cadenaSQL.append(" where d.ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				cadenaSQL.append(" and d.finca = " + r_mapeo.getFinca());
				cadenaSQL.append(" and d.cultivo = 1 ");
				cadenaSQL.append(" order by 1,2,3");
				
				con= this.conManager.establecerConexion();			

				if (con!=null)
				{
					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					
					vector = new ArrayList<MapeoEntradasCosecha>();
					
					while(rsOpcion.next())
					{
						mapeoEntradasCosecha = new MapeoEntradasCosecha();
						/*
						 * recojo mapeo operarios
						 */
						mapeoEntradasCosecha.setEjercicio(rsOpcion.getInt("ent_eje"));
						mapeoEntradasCosecha.setFinca(rsOpcion.getInt("ent_fin"));
						mapeoEntradasCosecha.setFecha(rsOpcion.getDate("ent_fec"));
						mapeoEntradasCosecha.setCodigo(rsOpcion.getInt("ent_alb"));
						mapeoEntradasCosecha.setSocio(rsOpcion.getInt("ent_soc"));
						mapeoEntradasCosecha.setArticulo(rsOpcion.getString("ent_art"));
						mapeoEntradasCosecha.setNombreArticulo(this.nombreArticulo(rsOpcion.getString("ent_art"),r_conexion));
						mapeoEntradasCosecha.setRecogida(rsOpcion.getString("ent_rec"));
						mapeoEntradasCosecha.setKgs_bruto(rsOpcion.getDouble("ent_kbr"));
						mapeoEntradasCosecha.setTara(rsOpcion.getDouble("ent_tar"));
						mapeoEntradasCosecha.setKgs_neto(rsOpcion.getDouble("ent_knt"));
						mapeoEntradasCosecha.setPorcentaje(rsOpcion.getDouble("ent_por"));
						mapeoEntradasCosecha.setKgs_neto_liq(rsOpcion.getDouble("ent_klq"));
						mapeoEntradasCosecha.setPueblo(rsOpcion.getString("ent_pue"));
						mapeoEntradasCosecha.setGrado(rsOpcion.getDouble("ent_gr"));
						mapeoEntradasCosecha.setEstadoSanitario(rsOpcion.getString("ent_es"));
						if (vector.size()==0) mapeoEntradasCosecha.setNombreSocio(this.nombreSocio(rsOpcion.getInt("ent_soc"),r_conexion));
						mapeoEntradasCosecha.setKgs_neto_liq_grado(rsOpcion.getDouble("ent_kg"));
						vector.add(mapeoEntradasCosecha);
					}
					
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}

	public ArrayList<MapeoEntradasCosecha> obtenerEntradasCosechaHuerfanos(MapeoFincasVendimia r_mapeo, String r_conexion)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoEntradasCosecha> vector = null;
		MapeoEntradasCosecha mapeoEntradasCosecha=null;
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			try
			{
				StringBuffer cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" SELECT d.finca ent_fin, ");
				cadenaSQL.append(" d.ejercicio ent_eje, ");
				cadenaSQL.append(" d.fecha ent_fec, ");
				cadenaSQL.append(" d.codigo ent_alb, ");
				cadenaSQL.append(" d.socio ent_soc, ");
				cadenaSQL.append(" d.articulo ent_art, ");
				cadenaSQL.append(" d.tipo_recogida ent_rec, ");
				cadenaSQL.append(" d.kgs_bruto ent_kbr, ");
				cadenaSQL.append(" d.kgs_tara ent_tar,");
				cadenaSQL.append(" d.kgs_neto ent_knt, ");
				cadenaSQL.append(" d.porcentaje ent_por, ");
				cadenaSQL.append(" d.kgs_neto_liq ent_klq, ");
				cadenaSQL.append(" d.cod_pueblo ent_pue, ");
				cadenaSQL.append(" d.grado ent_gr, ");
				cadenaSQL.append(" d.calidad ent_cal, ");
				cadenaSQL.append(" d.almacen ent_alm, ");
				cadenaSQL.append(" d.estado_sanit ent_es, ");
				cadenaSQL.append(" d.kgs_neto_liq*d.grado ent_kg ");
				cadenaSQL.append(" from e__ccs d ");			
				cadenaSQL.append(" where d.ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				cadenaSQL.append(" and d.finca not in (select finca from e__fincv where ejercicio = '" + r_mapeo.getEjercicio() + "') ");
				cadenaSQL.append(" and d.cultivo = 1 ");
				cadenaSQL.append(" order by 1,2,3");
				
				if (r_conexion.equals("Borja"))
				{
					con= this.conManager.establecerConexionGestionBorja();			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					con= this.conManager.establecerConexionGestionTabuenca();			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					con= this.conManager.establecerConexionGestionPozuelo();			
				}			
				
				if (con!=null)
				{
					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					
					vector = new ArrayList<MapeoEntradasCosecha>();
					
					while(rsOpcion.next())
					{
						mapeoEntradasCosecha = new MapeoEntradasCosecha();
						/*
						 * recojo mapeo operarios
						 */
						mapeoEntradasCosecha.setEjercicio(rsOpcion.getInt("ent_eje"));
						mapeoEntradasCosecha.setFinca(rsOpcion.getInt("ent_fin"));
						mapeoEntradasCosecha.setFecha(rsOpcion.getDate("ent_fec"));
						mapeoEntradasCosecha.setCodigo(rsOpcion.getInt("ent_alb"));
						mapeoEntradasCosecha.setSocio(rsOpcion.getInt("ent_soc"));
						mapeoEntradasCosecha.setArticulo(rsOpcion.getString("ent_art"));
						mapeoEntradasCosecha.setNombreArticulo(this.nombreArticulo(rsOpcion.getString("ent_art"),r_conexion));
						mapeoEntradasCosecha.setRecogida(rsOpcion.getString("ent_rec"));
						mapeoEntradasCosecha.setKgs_bruto(rsOpcion.getDouble("ent_kbr"));
						mapeoEntradasCosecha.setTara(rsOpcion.getDouble("ent_tar"));
						mapeoEntradasCosecha.setKgs_neto(rsOpcion.getDouble("ent_knt"));
						mapeoEntradasCosecha.setPorcentaje(rsOpcion.getDouble("ent_por"));
						mapeoEntradasCosecha.setKgs_neto_liq(rsOpcion.getDouble("ent_klq"));
						mapeoEntradasCosecha.setPueblo(rsOpcion.getString("ent_pue"));
						mapeoEntradasCosecha.setGrado(rsOpcion.getDouble("ent_gr"));
						mapeoEntradasCosecha.setEstadoSanitario(rsOpcion.getString("ent_es"));
						if (vector.size()==0) mapeoEntradasCosecha.setNombreSocio(this.nombreSocio(rsOpcion.getInt("ent_soc"),r_conexion));
						mapeoEntradasCosecha.setKgs_neto_liq_grado(rsOpcion.getDouble("ent_kg"));
						
						boolean yaCambiado=this.comprobarCambios(r_conexion, r_mapeo.getEjercicio(), rsOpcion.getInt("ent_alb"));
						
						if (!yaCambiado) vector.add(mapeoEntradasCosecha);
					}
					
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		return vector;
	}
	
	
	public String nombreSocio(Integer r_socio, String r_conexion)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.nombre fin_nom ");
		cadenaSQL.append(" from e__socio a ");
		cadenaSQL.append(" where a.socio= " + r_socio);

		try
		{
			if (r_conexion.equals("Borja"))
			{
				con= this.conManager.establecerConexionGestionBorja();			
			}
			else if (r_conexion.equals("Tabuenca"))
			{
				con= this.conManager.establecerConexionGestionTabuenca();			
			}
			else if (r_conexion.equals("Pozuelo"))
			{
				con= this.conManager.establecerConexionGestionPozuelo();			
			}

			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					return RutinasCadenas.conversion(rsOpcion.getString("fin_nom"));
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String cifSocio(Integer r_socio, String r_conexion)
	{
		ResultSet rsOpcion = null;
		
		if (this.socios==null || this.socios.isEmpty() )
		{
			StringBuffer cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.socio soc_soc, ");
			cadenaSQL.append(" a.cif soc_cif ");
			cadenaSQL.append(" from e__socio a ");
	
			try
			{
				if (r_conexion.equals("Borja"))
				{
					con= this.conManager.establecerConexionGestionBorja();			
				}
				else if (r_conexion.equals("Tabuenca"))
				{
					con= this.conManager.establecerConexionGestionTabuenca();			
				}
				else if (r_conexion.equals("Pozuelo"))
				{
					con= this.conManager.establecerConexionGestionPozuelo();			
				}
	
				if (con!=null)
				{
					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
					this.socios = new HashMap<Integer, String>();
					
					while(rsOpcion.next())
					{
						this.socios.put(rsOpcion.getInt("soc_soc"), rsOpcion.getString("soc_cif"));
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
					return null;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
		}
		
		return socios.get(r_socio);
	}

	public String nombreArticulo(String r_articulo, String r_conexion)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.descrip_articulo art_des ");
		cadenaSQL.append(" from e_articu a ");
		cadenaSQL.append(" where a.articulo = '" + r_articulo + "' ");

		try
		{
			if (r_conexion.equals("Borja"))
			{
				con= this.conManager.establecerConexionGestionBorja();			
			}
			else if (r_conexion.equals("Tabuenca"))
			{
				con= this.conManager.establecerConexionGestionTabuenca();			
			}
			else if (r_conexion.equals("Pozuelo"))
			{
				con= this.conManager.establecerConexionGestionPozuelo();			
			}

			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					return RutinasCadenas.conversion(rsOpcion.getString("art_des"));
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String obtenerFinca(Integer r_finca, String r_conexion, Integer r_ejercicio, Integer r_socio)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT finca fin_fin ");
		cadenaSQL.append(" from e__fincv  ");
		cadenaSQL.append(" where finca = " + r_finca);
		cadenaSQL.append(" and ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and socio = " + r_socio);

		try
		{
			if (r_conexion.equals("Borja"))
			{
				con= this.conManager.establecerConexionGestionBorja();			
			}
			else if (r_conexion.equals("Tabuenca"))
			{
				con= this.conManager.establecerConexionGestionTabuenca();			
			}
			else if (r_conexion.equals("Pozuelo"))
			{
				con= this.conManager.establecerConexionGestionPozuelo();			
			}

			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					return RutinasCadenas.conversion(rsOpcion.getString("fin_fin"));
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}


	public String guardarNuevo(MapeoFincasVendimia r_mapeo)
	{
//		PreparedStatement preparedStatement = null;
//		
//		StringBuffer cadenaSQL = new StringBuffer();  
//
//		try
//		{
//			
//			cadenaSQL.append(" INSERT INTO prd_retrabajos( ");
//			cadenaSQL.append(" prd_retrabajos.idRetrabajo,");			
//			cadenaSQL.append(" prd_retrabajos.origen, ");			
//			cadenaSQL.append(" prd_retrabajos.articulo, ");
//			cadenaSQL.append(" prd_retrabajos.descripcion, ");
//			cadenaSQL.append(" prd_retrabajos.cantidad, ");
//			cadenaSQL.append(" prd_retrabajos.factor, ");
//			cadenaSQL.append(" prd_retrabajos.unidades, ");
//			cadenaSQL.append(" prd_retrabajos.fecha, ");
//			cadenaSQL.append(" prd_retrabajos.cumplimentacion, ");
//			cadenaSQL.append(" prd_retrabajos.observaciones, ");
//			cadenaSQL.append(" prd_retrabajos.cliente,");
//			cadenaSQL.append(" prd_retrabajos.vencimiento ) VALUES (");
//			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");
//			
//		    con= this.conManager.establecerConexion();	
//		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
//
//		    preparedStatement.setInt(1, r_mapeo.getIdRetrabajo());
//		    String fecha = "";
//		    
//		    if (r_mapeo.getOrigen()!=null)
//		    {
//		    	preparedStatement.setString(2, r_mapeo.getOrigen());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(2, null);
//		    }
//		    if (r_mapeo.getArticulo()!=null)
//		    {
//		    	preparedStatement.setString(3, r_mapeo.getArticulo());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(3, null);
//		    }
//		    if (r_mapeo.getDescripcion()!=null)
//		    {
//		    	preparedStatement.setString(4, r_mapeo.getDescripcion());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(4, null);
//		    }
//		    if (r_mapeo.getCantidad()!=null)
//		    {
//		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(5, 0);
//		    }
//		    if (r_mapeo.getFactor()!=null)
//		    {
//		    	preparedStatement.setInt(6, r_mapeo.getFactor());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(6, 0);
//		    }
//		    if (r_mapeo.getUnidades()!=null)
//		    {
//		    	preparedStatement.setInt(7, r_mapeo.getUnidades());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(7, 0);
//		    }
//		    
//		    if (r_mapeo.getFecha()!=null)
//		    {
//		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
//		    }
//		    else
//		    {
//		    	fecha = RutinasFechas.convertirDateToString(new Date());
//		    }
//	    	preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(fecha));
//	    	
//		    if (r_mapeo.getCumplimentacion()!=null)
//		    {
//		    	preparedStatement.setString(9, r_mapeo.getCumplimentacion());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(9, null);
//		    }
//		    if (r_mapeo.getObservaciones()!=null)
//		    {
//		    	preparedStatement.setString(10, r_mapeo.getObservaciones());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(10, null);
//		    }
//		    
//		    if (r_mapeo.getCliente()!=null)
//		    {
//		    	preparedStatement.setString(11, r_mapeo.getCliente());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(11, 0);
//		    }
//		    if (r_mapeo.getFechaCarga()!=null)
//		    {
//		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaCarga());
//		    }
//		    else
//		    {
//		    	fecha = RutinasFechas.convertirDateToString(new Date());
//		    }
//		    preparedStatement.setString(12, RutinasFechas.convertirAFechaMysql(fecha));
//	        preparedStatement.executeUpdate();
//	        
//		}
//	    catch (Exception ex)
//	    {
//	    	serNotif.mensajeError(ex.getMessage());
//	    	return ex.getMessage();
//	    }
//		return "A";
		return null;
	}
	
	
	public String aplicarCambios(String r_conexion, Integer r_ejercicio)
	{
		
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String rdo = null;

		cadenaSQL.append(" SELECT a.codigo ts_cod, ");
		cadenaSQL.append(" a.finca ts_fin ");
		cadenaSQL.append(" from tec_ccs a");
		cadenaSQL.append(" where a.coop= '" + r_conexion + "' ");
		cadenaSQL.append(" and a.ejercicio = " + r_ejercicio);

		try
		{
			con= this.conManager.establecerConexion();			
			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					MapeoEntradasCosecha map = new MapeoEntradasCosecha();
					
					map.setFinca(rsOpcion.getInt("ts_fin"));
					map.setCodigo(rsOpcion.getInt("ts_cod"));
					map.setEjercicio(r_ejercicio);
					
					rdo = this.guardarAplcacionCambios(map, r_conexion);
					if (rdo!=null) return rdo;
					
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public boolean comprobarCambios(String r_conexion, Integer r_ejercicio, Integer r_codigo)
	{
		
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String rdo = null;
		
		cadenaSQL.append(" SELECT a.codigo ts_cod, ");
		cadenaSQL.append(" a.finca ts_fin ");
		cadenaSQL.append(" from tec_ccs a");
		cadenaSQL.append(" where a.coop= '" + r_conexion + "' ");
		cadenaSQL.append(" and a.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and a.codigo = " + r_codigo);
		
		try
		{
			con= this.conManager.establecerConexion();			
			if (con!=null)
			{
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					return true;
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return false;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String guardarAplcacionCambios(MapeoEntradasCosecha r_mapeo, String r_conexion)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			if (r_conexion.toUpperCase().equals("BORJA")) cadenaSQL.append(" UPDATE e__ccs_bor set ");
			else if (r_conexion.toUpperCase().equals("POZUELO")) cadenaSQL.append(" UPDATE e__ccs_poz set ");
			else cadenaSQL.append(" UPDATE e__ccs_tab set ");
			
		    cadenaSQL.append(" finca =? ");
		    cadenaSQL.append(" where codigo = ? ");
		    cadenaSQL.append(" and ejercicio = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getFinca()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getFinca());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambiosEntradaCosecha(MapeoEntradasCosecha r_mapeo, String r_conexion)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			if (r_conexion.toUpperCase().equals("BORJA")) cadenaSQL.append(" UPDATE e__ccs_bor set ");
			else if (r_conexion.toUpperCase().equals("POZUELO")) cadenaSQL.append(" UPDATE e__ccs_poz set ");
			else cadenaSQL.append(" UPDATE e__ccs_tab set ");
			
		    cadenaSQL.append(" finca =? ");
		    cadenaSQL.append(" where codigo = ? ");
		    cadenaSQL.append(" and ejercicio = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getFinca()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getFinca());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    
		    preparedStatement.executeUpdate();


		    /*
		     * Borro cualquier modificacion anterior sobre ese albaran
		     */
		    cadenaSQL = new StringBuffer();
		    
		    cadenaSQL.append(" delete from  tec_ccs ");
		    cadenaSQL.append(" where codigo = " + r_mapeo.getCodigo());
		    cadenaSQL.append(" and ejercicio = " + r_mapeo.getEjercicio());
		    cadenaSQL.append(" and coop = '" + r_conexion + "' ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.executeUpdate();

		    /*
		     * Inserto la nueva modificacion del albaran
		     */
		    cadenaSQL = new StringBuffer();
		    
		    cadenaSQL.append(" insert into tec_ccs( ");
		    cadenaSQL.append(" coop, ");
		    cadenaSQL.append(" ejercicio, ");
		    cadenaSQL.append(" codigo, ");
		    cadenaSQL.append(" finca) VALUES( ");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
			preparedStatement.setString(1, r_conexion);
			if (r_mapeo.getEjercicio()!=null)
			{
				preparedStatement.setInt(2, r_mapeo.getEjercicio());
			}
			else
			{
				preparedStatement.setInt(2, 0);
			}
			if (r_mapeo.getCodigo()!=null)
			{
				preparedStatement.setInt(3, r_mapeo.getCodigo());
			}
			else
			{
				preparedStatement.setInt(3, 0);
			}
		    if (r_mapeo.getFinca()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getFinca());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }

		    preparedStatement.executeUpdate();


		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoFincasVendimia r_mapeo)
	{
//		PreparedStatement preparedStatement = null;		
//		StringBuffer cadenaSQL = new StringBuffer();  
//		if (r_mapeo.getOrigen().equals("PEDIDOS")) return "Registro no modificable. Modifica en GREENsys";
//		try
//		{
//			
//		    cadenaSQL.append(" UPDATE prd_retrabajos set ");
//		    cadenaSQL.append(" prd_retrabajos.origen =?,");
//		    cadenaSQL.append(" prd_retrabajos.articulo =?,");
//		    cadenaSQL.append(" prd_retrabajos.descripcion =?,");
//		    cadenaSQL.append(" prd_retrabajos.cantidad =?,");
//		    cadenaSQL.append(" prd_retrabajos.factor =?,");
//			cadenaSQL.append(" prd_retrabajos.unidades =?,");
//			cadenaSQL.append(" prd_retrabajos.fecha =?,");
//			cadenaSQL.append(" prd_retrabajos.cumplimentacion =?,");
//			cadenaSQL.append(" prd_retrabajos.observaciones = ?, ");
//			cadenaSQL.append(" prd_retrabajos.cliente = ?, ");
//			cadenaSQL.append(" prd_retrabajos.vencimiento = ? ");
//			cadenaSQL.append(" where prd_retrabajos.idRetrabajo = ? ");
//			
//		    con= this.conManager.establecerConexion();	 
//		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
//		    
//		    String fecha ="";
//
//		    if (r_mapeo.getOrigen()!=null)
//		    {
//		    	preparedStatement.setString(1, r_mapeo.getOrigen());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(1, null);
//		    }
//		    if (r_mapeo.getArticulo()!=null)
//		    {
//		    	preparedStatement.setString(2, r_mapeo.getArticulo());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(2, null);
//		    }
//		    if (r_mapeo.getDescripcion()!=null)
//		    {
//		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(3, null);
//		    }
//		    if (r_mapeo.getCantidad()!=null)
//		    {
//		    	preparedStatement.setInt(4, r_mapeo.getCantidad());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(4, 0);
//		    }
//		    if (r_mapeo.getFactor()!=null)
//		    {
//		    	preparedStatement.setInt(5, r_mapeo.getFactor());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(5, 0);
//		    }
//			   
//		    if (r_mapeo.getUnidades()!=null)
//		    {
//		    	preparedStatement.setInt(6, r_mapeo.getUnidades());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(6, 0);
//		    }
//		    if (r_mapeo.getFecha()!=null)
//		    {
//		    	fecha=RutinasFechas.convertirDateToString(r_mapeo.getFecha());
//		    }
//		    else
//		    {
//		    	fecha = RutinasFechas.convertirDateToString(new Date());
//		    }
//		    
//		    preparedStatement.setString(7, RutinasFechas.convertirAFechaMysql(fecha));
//		    if (r_mapeo.getCumplimentacion()!=null)
//		    {
//		    	preparedStatement.setString(8, r_mapeo.getCumplimentacion());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(8, null);
//		    }
//		    if (r_mapeo.getObservaciones()!=null)
//		    {
//		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
//		    }
//		    else
//		    {
//		    	preparedStatement.setString(9, null);
//		    }
//		    if (r_mapeo.getCliente()!=null)
//		    {
//		    	preparedStatement.setString(10, r_mapeo.getCliente());
//		    }
//		    else
//		    {
//		    	preparedStatement.setInt(10, 0);
//		    }
//		    if (r_mapeo.getFechaCarga()!=null)
//		    {
//		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaCarga());
//		    }
//		    else
//		    {
//		    	fecha = RutinasFechas.convertirDateToString(new Date());
//		    }
//		    preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(fecha));
//		    preparedStatement.setInt(12, r_mapeo.getIdRetrabajo());
//		    
//		    preparedStatement.executeUpdate();
//		    
//		}
//		catch (Exception ex)
//	    {
//			serNotif.mensajeError(ex.getMessage());	    
//			return ex.getMessage();
//	    }
//		return r_mapeo.getEstado();
		return null;
	}
	
	public void eliminar(MapeoFincasVendimia r_mapeo)
	{
//		PreparedStatement preparedStatement = null;
//		StringBuffer cadenaSQL = new StringBuffer();        
//		
//		try
//		{
//			cadenaSQL.append(" DELETE FROM prd_retrabajos ");            
//			cadenaSQL.append(" WHERE prd_retrabajos.idRetrabajo = ?"); 
//			con= this.conManager.establecerConexion();	          
//			preparedStatement = con.prepareStatement(cadenaSQL.toString());
//			preparedStatement.setInt(1, r_mapeo.getIdRetrabajo());
//			preparedStatement.executeUpdate();
//		}
//		catch (Exception ex)
//		{
//			serNotif.mensajeError(ex.getMessage());	
//		}
	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
//		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
//		permisos = cos.obtenerPermisos("Embotelladora");
//		
//		if (permisos!=null)
//		{
//			if (permisos.length()==0) permisos="0";				
//		}
//		else
//		{
//			permisos = cos.obtenerPermisos("bib");
//			
//			if (permisos!=null)
//			{
//				if (permisos.length()==0) permisos="0";				
//			}
//			else
//			{
//				permisos = cos.obtenerPermisos(null);
//				if (permisos!=null)
//				{
//					if (permisos.length()==0) permisos="0";
//				}
//				else
//				{
//					permisos="99";
//				}
//			}
//		}
		return permisos;
	}

	@Override
	public String semaforos() 
	{
//		ResultSet rsOpcion = null;
//		
//		StringBuffer cadenaSQL = new StringBuffer();
//		
//		cadenaSQL.append(" SELECT prd_retrabajos.idRetrabajo rt_id,");
//		cadenaSQL.append(" prd_retrabajos.origen rt_ori,");
//		cadenaSQL.append(" prd_retrabajos.articulo rt_art,");
//		cadenaSQL.append(" prd_retrabajos.descripcion rt_des,");
//		cadenaSQL.append(" prd_retrabajos.cantidad rt_can,");
//		cadenaSQL.append(" prd_retrabajos.factor rt_fac,");
//		cadenaSQL.append(" prd_retrabajos.unidades rt_uni,");
//		cadenaSQL.append(" prd_retrabajos.fecha rt_fec,");
//		cadenaSQL.append(" prd_retrabajos.cumplimentacion rt_cum,");
//		cadenaSQL.append(" prd_retrabajos.observaciones rt_obs, ");
//		cadenaSQL.append(" prd_retrabajos.idProgramacion rt_id_pro ");
//		cadenaSQL.append(" from prd_retrabajos ");
//		
//		try
//		{
//			
//			con= this.conManager.establecerConexion();			
//			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
//			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
//			
//			while(rsOpcion.next())
//			{
//				return "2";
//			}
//				
//		}
//		catch (Exception ex)
//		{
//			serNotif.mensajeError(ex.getMessage());			
//		}
		
		return "0";
	}

	public String generarInforme(MapeoFincasVendimia r_mapeo)
	{
		String informe = "";
		
//		String resultadoGeneracion = null;
//		LibreriaImpresion libImpresion = null;
//		libImpresion = new LibreriaImpresion();
//
////		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
//		libImpresion.setCodigo(1);
//		libImpresion.setArchivoDefinitivo("/retrabajo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
//		libImpresion.setArchivoPlantilla("retrabajos.jasper");
//		libImpresion.setArchivoPlantillaDetalle(null);
//		libImpresion.setArchivoPlantillaDetalleCompilado(null);
//		libImpresion.setArchivoPlantillaNotas(null);
//		libImpresion.setArchivoPlantillaNotasCompilado(null);
//		libImpresion.setCarpeta("retrabajos");
//		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");
//
//		resultadoGeneracion = libImpresion.generacionInformeInteger();
//
//		if (resultadoGeneracion == null)
//			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}