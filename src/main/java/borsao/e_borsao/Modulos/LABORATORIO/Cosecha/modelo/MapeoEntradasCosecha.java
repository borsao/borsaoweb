package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo;

import java.sql.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEntradasCosecha extends MapeoGlobal
{
    private Integer idFincaVendimia;
	private Double kgs_bruto;
	private Double kgs_neto;
	private Double kgs_neto_liq;
	private Double grado;
	private Double tara;
	private Double kgs_neto_liq_grado;
	private Double porcentaje;
	private Double disponible;
	
	private Integer ejercicio;
	private Integer socio;
	private Integer finca;
	private Integer codigo;
	
	private Date fecha;
	
	private String nombreSocio;
	private String articulo;
	private String nombreArticulo;
	private String recogida;
	private String pueblo;
	private String almacen;
	private String estadoSanitario;
	private String calidad;
	

	public MapeoEntradasCosecha()
	{
	}


	public Integer getIdFincaVendimia() {
		return idFincaVendimia;
	}


	public void setIdFincaVendimia(Integer idFincaVendimia) {
		this.idFincaVendimia = idFincaVendimia;
	}


	public Double getKgs_bruto() {
		return kgs_bruto;
	}


	public void setKgs_bruto(Double kgs_bruto) {
		this.kgs_bruto = kgs_bruto;
	}


	public Double getKgs_neto() {
		return kgs_neto;
	}


	public void setKgs_neto(Double kgs_neto) {
		this.kgs_neto = kgs_neto;
	}


	public Double getKgs_neto_liq() {
		return kgs_neto_liq;
	}


	public void setKgs_neto_liq(Double kgs_neto_liq) {
		this.kgs_neto_liq = kgs_neto_liq;
	}


	public Double getGrado() {
		return grado;
	}


	public void setGrado(Double grado) {
		this.grado = grado;
	}


	public Double getTara() {
		return tara;
	}


	public void setTara(Double tara) {
		this.tara = tara;
	}


	public Double getKgs_neto_liq_grado() {
		return kgs_neto_liq_grado;
	}


	public void setKgs_neto_liq_grado(Double kgs_neto_liq_grado) {
		this.kgs_neto_liq_grado = kgs_neto_liq_grado;
	}


	public Double getPorcentaje() {
		return porcentaje;
	}


	public Double getDisponible() {
		return disponible;
	}


	public void setDisponible(Double disponible) {
		this.disponible = disponible;
	}


	public void setPorcentaje(Double porcentaje) {
		this.porcentaje = porcentaje;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public Integer getSocio() {
		return socio;
	}


	public void setSocio(Integer socio) {
		this.socio = socio;
	}


	public Integer getFinca() {
		return finca;
	}


	public void setFinca(Integer finca) {
		this.finca = finca;
	}


	public Integer getCodigo() {
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public String getNombreSocio() {
		return nombreSocio;
	}


	public void setNombreSocio(String nombreSocio) {
		this.nombreSocio = nombreSocio;
	}


	public String getNombreArticulo() {
		return nombreArticulo;
	}


	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}


	public String getRecogida() {
		return recogida;
	}


	public void setRecogida(String recogida) {
		this.recogida = recogida;
	}


	public String getPueblo() {
		return pueblo;
	}


	public void setPueblo(String pueblo) {
		this.pueblo = pueblo;
	}


	public String getAlmacen() {
		return almacen;
	}


	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}


	public String getEstadoSanitario() {
		return estadoSanitario;
	}


	public void setEstadoSanitario(String estadoSanitario) {
		this.estadoSanitario = estadoSanitario;
	}


	public String getCalidad() {
		return calidad;
	}


	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


}