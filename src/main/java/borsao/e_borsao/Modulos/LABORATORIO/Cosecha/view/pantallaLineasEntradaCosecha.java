package borsao.e_borsao.Modulos.LABORATORIO.Cosecha.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoEntradasCosecha;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoFincasVendimia;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.entradaCosechaGrid;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.server.consultaFincasVendimiaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasEntradaCosecha extends Window
{
	public FooterRow footer = null;	
	private Button btnBotonCVentana = null;
	private Button btnAlbaranesHuerfanos = null;
	private entradaCosechaGrid gridDatos = null;
	private VerticalLayout principal = null;
	private FincasVendimiaView app = null;
	private MapeoFincasVendimia mapeo=null;
	private HorizontalLayout botonera =null;
	
	public pantallaLineasEntradaCosecha(FincasVendimiaView r_app, Integer r_ejercicio, Integer r_finca, String r_titulo, String r_conexion)
	{
		this.app = r_app;
		
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		
		mapeo = new MapeoFincasVendimia();
		mapeo.setEjercicio(r_ejercicio);
		mapeo.setFinca(r_finca);

		this.generarGrid(mapeo);

		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnAlbaranesHuerfanos = new Button("Albaranes sin Finca");
		btnAlbaranesHuerfanos.addStyleName(ValoTheme.BUTTON_TINY);
		btnAlbaranesHuerfanos.addStyleName(ValoTheme.BUTTON_DANGER);

		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		botonera.addComponent(btnAlbaranesHuerfanos);		
		botonera.setComponentAlignment(btnAlbaranesHuerfanos, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		principal.setExpandRatio(this.gridDatos, 1);

		this.setContent(principal);
		this.cargarListeners();


	}

		
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		btnAlbaranesHuerfanos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				huerfanos();
			}
		});
		
	}
	
    public void generarGrid(MapeoFincasVendimia r_mapeo)
    {
    	ArrayList<MapeoEntradasCosecha> vector = null;
    	consultaFincasVendimiaServer cfvs = new consultaFincasVendimiaServer(CurrentUser.get());
    	
    	if (this.app.cmbCooperativa.getValue()!=null && this.app.cmbCooperativa.getValue().toString().length()>0)
    	{
    		vector = cfvs.obtenerEntradasCosecha(r_mapeo, this.app.cmbCooperativa.getValue().toString());
    	}
    	
    	if (this.gridDatos!=null)
    	{
    		this.principal.removeComponent(this.gridDatos);
    		this.gridDatos.removeAllColumns();
    		this.gridDatos=null;
    	}
    	gridDatos = new entradaCosechaGrid(this.app, vector);
    	
    }
    

    private void huerfanos()
    {
		pantallaLineasEntradaCosechaHuerfanos vt = new pantallaLineasEntradaCosechaHuerfanos(this, mapeo.getEjercicio(), "Lineas Entrega Cosecha sin Finca ", app.cmbCooperativa.getValue().toString());
		getUI().addWindow(vt);
    }
    
	private void cerrar()
	{
		this.app.actualizarDatos();
		close();
	}
	
	public void refrescarDatos()
	{
		this.generarGrid(mapeo);
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		principal.setExpandRatio(this.gridDatos, 1);

	}
}