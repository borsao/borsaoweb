package borsao.e_borsao.Modulos.LABORATORIO.Localizaciones.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;

public class consultaLocalizacionesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaLocalizacionesServer instance;
	
	public consultaLocalizacionesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaLocalizacionesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaLocalizacionesServer(r_usuario);			
		}
		return instance;
	}
	
	public boolean comprobarDeposito(String r_deposito)
	{
		boolean encontrado = false;
		
		ResultSet rsDepositos = null;
		
		String sql = null;
		try
		{
			sql="select codigo from localizaciones where codigo = '" + r_deposito + "'" ;
			
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsDepositos= cs.executeQuery(sql);
			while(rsDepositos.next())
			{
				encontrado=true;				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsDepositos!=null)
				{
					rsDepositos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return encontrado;
	}
	
		
}