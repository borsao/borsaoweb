package borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.view.consultaInventarioSGAGsysView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean conTotales = true;
	private consultaInventarioSGAGsysView app = null;
	
    public OpcionesGrid(consultaInventarioSGAGsysView r_app,  ArrayList<MapeoInventarioSGAGsys> r_vector) 
    {
        this.app = r_app;
        this.vector=r_vector;
        
		this.asignarTitulo("Inventario SGA vs. Gsys");
		
    	if (this.vector==null)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("alerta");
    	this.camposNoFiltrar.add("stock_actual_sga");
    	this.camposNoFiltrar.add("stock_actual_gsys");
    	this.camposNoFiltrar.add("valor_diferencia");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoInventarioSGAGsys.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		this.calcularTotal();
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("articulo","descripcion","stock_actual_sga", "stock_actual_gsys", "valor_diferencia", "alerta");
    }
    
    public void establecerTitulosColumnas()
    {

    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "180");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("alerta").setHeaderCaption("Alerta!");
    	this.getColumn("alerta").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_actual_sga").setHeaderCaption("Existencias SGA");
    	this.getColumn("stock_actual_sga").setWidth(new Double(145));
    	this.getColumn("stock_actual_gsys").setHeaderCaption("Existencias Gsys");
    	this.getColumn("stock_actual_gsys").setWidth(new Double(145));
    	this.getColumn("valor_diferencia").setHeaderCaption("Descuadre");
    	this.getColumn("valor_diferencia").setWidth(new Double(145));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("lote").setHidden(true);
    	this.getColumn("lpn").setHidden(true);
    	this.getColumn("ubicacion").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("tipoArticulo").setHidden(true);
    	this.getColumn("alerta").setHidden(true);
	}
	
    public void cargarListeners()
    {
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ("stock_actual_sga".equals(cellReference.getPropertyId()) || "stock_actual_gsys".equals(cellReference.getPropertyId()) || "valor_diferencia".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    public void generacionPdf(String r_impresora, boolean regenerar, boolean r_eliminar) 
    {
    }

	@Override
	public void calcularTotal() 
	{
    	Long totalUS = new Long(0) ;
    	Integer valorS = null;
    	Long totalUG = new Long(0) ;
    	Integer valorG = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valorS = (Integer) item.getItemProperty("stock_actual_sga").getValue();
        	valorG = (Integer) item.getItemProperty("stock_actual_gsys").getValue();
        	totalUS += valorS.longValue();
        	totalUG += valorG.longValue();
        }
        
        footer.getCell("articulo").setText("Totales ");
		footer.getCell("stock_actual_sga").setText(RutinasNumericas.formatearDouble(totalUS.toString()));
		footer.getCell("stock_actual_gsys").setText(RutinasNumericas.formatearDouble(totalUG.toString()));
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		footer.getCell("stock_actual_sga").setStyleName("Rcell-pie");
		footer.getCell("stock_actual_gsys").setStyleName("Rcell-pie");

	}
}
