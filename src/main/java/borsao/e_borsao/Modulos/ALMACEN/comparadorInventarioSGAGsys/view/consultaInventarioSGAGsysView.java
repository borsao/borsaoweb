package borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo.MapeoInventarioSGAGsys;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.server.consultaInventarioSGAGsysServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaInventarioSGAGsysView extends GridViewRefresh {

	public static final String VIEW_NAME = "Comparador Inventario";
	private final String titulo = "Comparador Inventario SGA-Gsys";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private ArrayList<MapeoInventarioSGAGsys> vector=null;
	private VentanaAceptarCancelar vt = null;
	private boolean generado = false;
	public TextField articulo=null;
	public Combo cmbTipoArticulo = null;
	public Button generarRotativo = null;
	
//	public TextField lote=null;
//	public TextField ubicacion=null;
//	public Combo cmbPrinter = null;
	
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaInventarioSGAGsysView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
//    	this.opcImprimir.setVisible(true);
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.articulo = new TextField("Articulo");
		this.articulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.cmbTipoArticulo = new Combo();
		this.cmbTipoArticulo.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbTipoArticulo.setNewItemsAllowed(false);
		this.cmbTipoArticulo.setNullSelectionAllowed(false);
		this.cmbTipoArticulo.addItem("PT");
		this.cmbTipoArticulo.addItem("PS");
		this.cmbTipoArticulo.addItem("MP");
		this.cmbTipoArticulo.addItem("Todos");
		
		this.generarRotativo = new Button("Generar Rotativo");
		this.generarRotativo.addStyleName(ValoTheme.BUTTON_TINY);
		this.generarRotativo.addStyleName(ValoTheme.BUTTON_FRIENDLY);

//		this.lpn= new TextField("Lpn");
//		this.lpn.addStyleName(ValoTheme.TEXTFIELD_TINY);
//
//		this.lote= new TextField("Lote");
//		this.lote.addStyleName(ValoTheme.TEXTFIELD_TINY);
//
//		this.ubicacion= new TextField("Ubicacion");
//		this.ubicacion.addStyleName(ValoTheme.TEXTFIELD_TINY);
//
//		this.cmbPrinter = new Combo();
//		this.cmbPrinter.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cabLayout.addComponent(this.articulo);
//		this.cabLayout.addComponent(this.lote);
//		this.cabLayout.addComponent(this.lpn);
//		this.cabLayout.addComponent(this.ubicacion);
//		this.cabLayout.addComponent(this.cmbPrinter);
//		this.cabLayout.setComponentAlignment(this.cmbPrinter, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.cmbTipoArticulo);
		this.cabLayout.setComponentAlignment(this.cmbTipoArticulo, Alignment.BOTTOM_LEFT);
		this.cabLayout.addComponent(this.generarRotativo);
		this.cabLayout.setComponentAlignment(this.generarRotativo, Alignment.BOTTOM_LEFT);
		
//		this.cargarCombo();
		this.cargarListeners();
		
//		opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
//		opcionesEscogidas.put("lote", this.lote.getValue().toString());
//		opcionesEscogidas.put("lpn", this.lpn.getValue().toString());
//		opcionesEscogidas.put("ubicacion", this.ubicacion.getValue().toString());
//		
		this.generarGrid(new MapeoInventarioSGAGsys());

//    	this.barAndGridLayout.removeComponent(this.cabLayout);
//    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    public void generarGrid(MapeoInventarioSGAGsys r_mapeo)
    {
    	
    	consultaInventarioSGAGsysServer cis = new consultaInventarioSGAGsysServer(CurrentUser.get());
    	
    	if (r_mapeo.getArticulo()!=null || r_mapeo.getTipoArticulo()!=null)
    		vector=cis.datosComparacionInventario(r_mapeo);
    	else
    		vector = new ArrayList<MapeoInventarioSGAGsys>();
    	
    	grid = new OpcionesGrid(this, vector);
    	
    	((OpcionesGrid) grid).asignarTitulo("Inventario SGA vs. Gsys");
    	if (((OpcionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else if (((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		generado=false;
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {
    	this.generarRotativo.addClickListener(new ClickListener() 
    	{
			
			@Override
			public void buttonClick(ClickEvent event) {
				generarFichero();
			}
		});
		this.articulo.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
				}
				
				MapeoInventarioSGAGsys mapeo = new MapeoInventarioSGAGsys();
				if (cmbTipoArticulo.getValue()!=null)
				{
					mapeo.setTipoArticulo(cmbTipoArticulo.getValue().toString());
	
					if (articulo.getValue()!=null && articulo.getValue().toString().length()>0) 
					{
						mapeo.setArticulo(articulo.getValue());
					}
					generarGrid(mapeo);
				}
				else
				{
					generarGrid(new MapeoInventarioSGAGsys());
				}
			}
		});
		
		this.cmbTipoArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
				}
				
				MapeoInventarioSGAGsys mapeo = new MapeoInventarioSGAGsys();
				mapeo.setTipoArticulo(cmbTipoArticulo.getValue().toString());

				if (articulo.getValue()!=null && articulo.getValue().toString().length()>0) 
				{
					mapeo.setArticulo(articulo.getValue());
				}
				generarGrid(mapeo);	
			}
		});
//		this.lote.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;			
//					if (lote.getValue()!=null && lote.getValue().toString().length()>0) 
//					{
//						ubicacion.setEnabled(false);
//						opcionesEscogidas = new HashMap<String , String>();
//						opcionesEscogidas.put("articulo", articulo.getValue());
//						opcionesEscogidas.put("lote", lote.getValue());
//						opcionesEscogidas.put("lpn", lpn.getValue());
//						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
//					}
//					else
//					{
//						ubicacion.setEnabled(true);
//					}
//					generarGrid(opcionesEscogidas);
//				}
//			}
//		});
//
//		this.lpn.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;			
//					if (lpn.getValue()!=null && lpn.getValue().toString().length()>0) 
//					{
//						ubicacion.setEnabled(false);
//						opcionesEscogidas = new HashMap<String , String>();
//						opcionesEscogidas.put("articulo", articulo.getValue());
//						opcionesEscogidas.put("lote", lote.getValue());
//						opcionesEscogidas.put("lpn", lpn.getValue());
//						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
//					}
//					else
//					{
//						ubicacion.setEnabled(true);
//					}
//					generarGrid(opcionesEscogidas);
//				}
//			}
//		});
//
//		this.ubicacion.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;			
//					if (ubicacion.getValue()!=null && ubicacion.getValue().toString().length()>0) 
//					{
//						lote.setEnabled(false);
//						lpn.setEnabled(false);
//						articulo.setEnabled(false);
//						
//						opcionesEscogidas = new HashMap<String , String>();
//						opcionesEscogidas.put("articulo", articulo.getValue());
//						opcionesEscogidas.put("lote", lote.getValue());
//						opcionesEscogidas.put("lpn", lpn.getValue());
//						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
//					}
//					else
//					{
//						lote.setEnabled(true);
//						lpn.setEnabled(true);
//						articulo.setEnabled(true);
//					}
//					generarGrid(opcionesEscogidas);
//				}
//			}
//		});
    }
    
//	private void cargarCombo()
//	{
//		String defecto=null;
//		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
//		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
//		
//		for (int i=0;i<vector.size();i++)
//		{
//			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
//			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
//		}
//		
//		this.cmbPrinter.setValue(defecto);
//	}

    
    @Override
    public void reestablecerPantalla() 
    {
    	this.opcImprimir.setEnabled(false);
    }
    
    public void print() 
    {
//    	((OpcionesGrid) this.grid).generacionPdf(this.cmbPrinter.getValue().toString(), generado,true);
//    	this.regresarDesdeForm();
//    	generado=false;
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		switch (r_accion)
		{
			case "generarFichero":
			{
				String ruta = RutinasFicheros.generarCsv(vector);
				if (ruta!=null)
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rotativo Generado: " + ruta);
				}
				ruta=null;
				if (this.vt.isVisible()) this.vt.close();
				break;
			}
		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}
	
	private void generarFichero()
	{
		vt = new VentanaAceptarCancelar(this, "GenerarFichero rotativo=", "SI", "NO", "generarFichero", null);
		getUI().addWindow(vt);
	}
}
