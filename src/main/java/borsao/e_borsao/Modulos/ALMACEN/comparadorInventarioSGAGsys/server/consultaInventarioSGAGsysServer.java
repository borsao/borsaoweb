package borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo.MapeoInventarioSGAGsys;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaInventarioSGAGsysServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	Connection con = null;	
	Statement cs = null;
	private static consultaInventarioSGAGsysServer instance;
	
	
	public consultaInventarioSGAGsysServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaInventarioSGAGsysServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaInventarioSGAGsysServer(r_usuario);			
		}
		return instance;
	}

	public ArrayList<MapeoInventarioSGAGsys> datosComparacionInventario(MapeoInventarioSGAGsys r_mapeo)
	{
		ArrayList<MapeoInventarioSGAGsys> vectorResultado = null;
		MapeoInventarioSGAGsys mapeo = new MapeoInventarioSGAGsys();
		HashMap<String , Integer> hashSGA = null;
		HashMap<String , Integer> hashGsys = null;
		HashMap<Integer , String> hashArticulos = null;
		
		hashArticulos= datosOpcionesArticulos(r_mapeo);
		hashSGA = datosOpcionesInventarioSGAGlobal(r_mapeo);
		hashGsys = datosOpcionesInventarioGsysGlobal(r_mapeo);
		vectorResultado = new ArrayList<MapeoInventarioSGAGsys>();
		
		for (int i = 0; i<hashArticulos.size();i++)
		{
			mapeo = new MapeoInventarioSGAGsys();
			
			mapeo.setArticulo(hashArticulos.get(i));
			mapeo.setDescripcion(obtenerDescripcion(hashArticulos.get(i)));
			
			if (hashSGA.containsKey(hashArticulos.get(i)))
			{
				mapeo.setStock_actual_sga(hashSGA.get(hashArticulos.get(i)));
			}
			else
			{
				mapeo.setStock_actual_sga(new Integer(0));
//				mapeo.setAlerta("No SGA");
			}
			if (hashGsys.containsKey(hashArticulos.get(i)))
			{
				mapeo.setStock_actual_gsys(hashGsys.get(hashArticulos.get(i)));
			}
			else
			{
				mapeo.setStock_actual_gsys(new Integer(0));
			}
			mapeo.setValor_diferencia(mapeo.getStock_actual_sga()-mapeo.getStock_actual_gsys());
			
			vectorResultado.add(mapeo);
		}
		
		return vectorResultado;
	}
	
	
	private HashMap<Integer, String> datosOpcionesArticulos(MapeoInventarioSGAGsys r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<Integer, String> hash = null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		Integer contador = 0;
		
		try
		{
	     	if (r_mapeo!=null)
	     	{
	     		cadenaSQL.append(" SELECT a.articulo ");
	     		cadenaSQL.append(" FROM e_articu a ");
	     		
	     		cadenaWhere = new StringBuffer();
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					cadenaWhere.append(" where a.articulo matches '" + r_mapeo.getArticulo().trim() + "*' ");
				}
				else if (r_mapeo.getTipoArticulo()!=null && r_mapeo.getTipoArticulo().length()>0)
				{
					if (r_mapeo.getTipoArticulo().contentEquals("PT"))
					{
						cadenaWhere.append(" where (a.articulo matches '0102*' or a.articulo matches '0111*' or a.articulo matches '0103*') and tipo_articulo = 'PT'");
					}
					else if (r_mapeo.getTipoArticulo().contentEquals("PS"))
					{
						cadenaWhere.append(" where (a.articulo matches '0102*') and tipo_articulo = 'PS'");
					}
					else if (r_mapeo.getTipoArticulo().contentEquals("MP"))
					{
						cadenaWhere.append(" where (a.articulo matches '0104*' or a.articulo matches '0105*' or a.articulo matches '0106*' or a.articulo matches '0107*' or a.articulo matches '0108*' or a.articulo matches '0109*') ");
					}
				}
		     		
		     	cadenaSQL.append(cadenaWhere.toString() );
		     	
		     	cadenaSQL.append(" and a.cod_desarrollo='N' ");
		     	cadenaSQL.append(" ORDER BY a.articulo ASC ");
	     		
			    con= this.conManager.establecerConexionGestionInd();
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hash= new HashMap<Integer, String>();
				
				while(rsOpcion.next())
				{
					/*
					 * recojo datos inventario sga
					 */
					hash.put(contador, rsOpcion.getString("articulo").trim());
					contador++;
				}
	     	}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return hash;
	}
	
	private HashMap<String, Integer> datosOpcionesInventarioSGAGlobal(MapeoInventarioSGAGsys r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<String, Integer> hashSga = null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
	     	if (r_mapeo!=null)
	     	{
	     		cadenaSQL.append(" SELECT DISTINCT T1.FULLNAME AS se_ubi, ");
	     		cadenaSQL.append(" t3.ITEM_NAME as se_art, ");
	     		cadenaSQL.append(" T3.ITEM_DESCRIPTION AS se_des, ");
	     		cadenaSQL.append(" T3.LOT AS se_lot, ");
	     		cadenaSQL.append(" T3.LPN AS se_LPN, ");
	     		cadenaSQL.append(" T3.VALUE AS se_ex ");	    
	     		cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
	     		cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID ");
	     		cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION and t1.fullname not in ('PS.NO CONFORME','PT.S.99','PT.S.95') ");
				 
	     		cadenaWhere = new StringBuffer();
	     		
	     		if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
	     		{
//	     			cadenaWhere.append("WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
	     			cadenaWhere.append("WHERE PARENTCLASSNAME in ('objt.wms.bo.inventorymgt.Zone','objt.common.inventorymgt.bo.Warehouse','objt.common.inventorymgt.bo.WarehouseLocation') ");
	     			cadenaWhere.append(" and t3.ITEM_NAME like '" + r_mapeo.getArticulo().trim() + "%' ");
	     		}
	     		else if (r_mapeo.getTipoArticulo()!=null && r_mapeo.getTipoArticulo().length()>0)
	     		{
	     			if (r_mapeo.getTipoArticulo().contentEquals("PT"))
	     			{
//	     				cadenaWhere.append("WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
	     				cadenaWhere.append("WHERE PARENTCLASSNAME in ('objt.wms.bo.inventorymgt.Zone','objt.common.inventorymgt.bo.Warehouse','objt.common.inventorymgt.bo.WarehouseLocation') ");
	     				cadenaWhere.append(" and (t3.ITEM_NAME like '0102%' or t3.ITEM_NAME like '0111%' or t3.ITEM_NAME like '0103%') ");
	     			}
	     			else if (r_mapeo.getTipoArticulo().contentEquals("PS"))
	     			{
	     				cadenaWhere.append(" where (t3.ITEM_NAME like '0102%') ");
	     			}
	     			else if (r_mapeo.getTipoArticulo().contentEquals("MP"))
	     			{
//	     				cadenaWhere.append("WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
	     				cadenaWhere.append("WHERE PARENTCLASSNAME in ('objt.wms.bo.inventorymgt.Zone','objt.common.inventorymgt.bo.Warehouse','objt.common.inventorymgt.bo.WarehouseLocation') ");
	     				cadenaWhere.append(" and (t3.ITEM_NAME like '0104%' or t3.ITEM_NAME like '0105%' or t3.ITEM_NAME like '0106%' or t3.ITEM_NAME like '0107%' or t3.ITEM_NAME like '0108%' or t3.ITEM_NAME like '0109%') ");
	     			}
	     		}
		     		
		     	cadenaSQL.append(cadenaWhere.toString() );
		     	
		     	cadenaSQL.append(" ORDER BY ITEM_NAME ASC ");
	     		
			    con= this.conManager.establecerConexionSGAInd();
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hashSga = new HashMap<String, Integer>();
				String articuloOld="";
				String articulo="";
				Integer valor = 0;
				while(rsOpcion.next())
				{
					/*
					 * recojo datos inventario sga
					 */
					if (articuloOld.length()==0)
					{
						articuloOld = rsOpcion.getString("se_art").trim();
					}
					
					if (articuloOld.contentEquals(rsOpcion.getString("se_art").trim()))
					{
						valor = valor + rsOpcion.getInt("se_ex");
					}
					else
					{
						hashSga.put(articuloOld,valor);
						valor=rsOpcion.getInt("se_ex");
						articuloOld = rsOpcion.getString("se_art").trim();
					}
				}
				hashSga.put(articuloOld,valor);
	     	}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return hashSga;
	}

	private HashMap<String, Integer> datosOpcionesInventarioGsysGlobal(MapeoInventarioSGAGsys r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<String, Integer> hashInv = null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaSQL.append(" SELECT DISTINCT a.articulo as se_art, ");
				cadenaSQL.append(" b.descrip_articulo as se_des, ");
				cadenaSQL.append(" SUM(a.existencias) as se_ex ");	    
				cadenaSQL.append(" FROM a_exis_0 a");
				cadenaSQL.append(" , e_articu b ");
				cadenaSQL.append(" where a.articulo=b.articulo ");
				
				cadenaWhere = new StringBuffer();
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					cadenaWhere.append(" and a.articulo matches '" + r_mapeo.getArticulo().trim() + "*' ");
				}
				else if (r_mapeo.getTipoArticulo()!=null && r_mapeo.getTipoArticulo().length()>0)
				{
					if (r_mapeo.getTipoArticulo().contentEquals("PT"))
					{
						cadenaWhere.append(" and (a.articulo matches '0102*' or a.articulo matches '0111*' or a.articulo matches '0103*') and tipo_articulo = 'PT' ");
					}
					else if (r_mapeo.getTipoArticulo().contentEquals("PS"))
					{
						cadenaWhere.append(" and (a.articulo matches '0102*') and tipo_articulo = 'PS' ");
					}
					else if (r_mapeo.getTipoArticulo().contentEquals("MP"))
					{
						cadenaWhere.append(" and (a.articulo matches '0104*' or a.articulo matches '0105*' or a.articulo matches '0106*' or a.articulo matches '0107*' or a.articulo matches '0108*' or a.articulo matches '0109*') ' ");
					}
				}
				
				cadenaSQL.append(cadenaWhere.toString() );
				
				cadenaSQL.append(" and a.almacen in (3,4) ");
				cadenaSQL.append(" GROUP BY 1, 2 ");
				cadenaSQL.append(" ORDER BY se_art ASC ");
				
				con= this.conManager.establecerConexionGestionInd();
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				hashInv = new HashMap<String, Integer>();
				
				while(rsOpcion.next())
				{
					/*
					 * recojo datos inventario sga
					 */
					hashInv.put(rsOpcion.getString("se_art").trim(),rsOpcion.getInt("se_ex"));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return hashInv;
	}

	public String generarInforme(ArrayList<MapeoInventarioSGAGsys> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		return "0";
	}

	public void eliminar()
	{
	}

	public String guardarNuevo(MapeoInventarioSGAGsys r_mapeo)
	{

		return null;
	}
	
	private String obtenerDescripcion(String r_articulo)
	{
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		String desc = cas.obtenerDescripcionArticulo(r_articulo);
		return desc;
	}
}