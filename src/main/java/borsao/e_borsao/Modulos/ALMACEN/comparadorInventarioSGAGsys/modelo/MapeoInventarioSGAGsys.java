package borsao.e_borsao.Modulos.ALMACEN.comparadorInventarioSGAGsys.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioSGAGsys extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	
	private String ubicacion;
	private String lote;
	private String lpn;
	private String almacen;
	private String tipoArticulo;
	private String alerta;
	
	private Integer stock_actual_sga;
	private Integer stock_actual_gsys;
	private Integer valor_diferencia;

	public MapeoInventarioSGAGsys()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public String getLote() {
		return lote;
	}

	public String getLpn() {
		return lpn;
	}

	public String getAlmacen() {
		return almacen;
	}

	public Integer getStock_actual_sga() {
		return stock_actual_sga;
	}

	public Integer getStock_actual_gsys() {
		return stock_actual_gsys;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setLpn(String lpn) {
		this.lpn = lpn;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public void setStock_actual_sga(Integer stock_actual_sga) {
		this.stock_actual_sga = stock_actual_sga;
	}

	public void setStock_actual_gsys(Integer stock_actual_gsys) {
		this.stock_actual_gsys = stock_actual_gsys;
	}

	public String getTipoArticulo() {
		return tipoArticulo;
	}

	public void setTipoArticulo(String tipoArticulo) {
		this.tipoArticulo = tipoArticulo;
	}

	public Integer getValor_diferencia() {
		return valor_diferencia;
	}

	public void setValor_diferencia(Integer valor_diferencia) {
		this.valor_diferencia = valor_diferencia;
	}

	public String getAlerta() {
		return alerta;
	}

	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}

}