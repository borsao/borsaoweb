package borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCargaInventarioSGA extends MapeoGlobal
{

	private String codigoRec; 
	private String ordenFabr; 
	private String sscc;
	private String ubicacion;
	private String articulo;
	private Integer cantidad;
	private String tipo;
	private String lote;
	private String fechaProd; 
	private String fechaCad; 
	private String proveedor; 
	
	
	public MapeoCargaInventarioSGA()
	{
		this.setCodigoRec("");
		this.setOrdenFabr("");
		this.setProveedor("");
		this.setFechaCad("");
		this.setFechaProd("");
		this.setTipo("PT");
	}


	public String getCodigoRec() {
		return codigoRec;
	}


	public String getOrdenFabr() {
		return ordenFabr;
	}


	public String getSscc() {
		return sscc;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public String getTipo() {
		return tipo;
	}


	public String getLote() {
		return lote;
	}


	public String getFechaProd() {
		return fechaProd;
	}


	public String getFechaCad() {
		return fechaCad;
	}


	public String getProveedor() {
		return proveedor;
	}


	public void setCodigoRec(String codigoRec) {
		this.codigoRec = codigoRec;
	}


	public void setOrdenFabr(String ordenFabr) {
		this.ordenFabr = ordenFabr;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public void setLote(String lote) {
		this.lote = lote;
	}


	public void setFechaProd(String fechaProd) {
		this.fechaProd = fechaProd;
	}


	public void setFechaCad(String fechaCad) {
		this.fechaCad = fechaCad;
	}


	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

}