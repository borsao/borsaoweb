package borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo.MapeoCargaInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.server.consultaCargaInventarioSGAServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionCargaInventarioSGA extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtUbicacion = null;
	private Combo cmbFicheros = null;
	private Button btnCargar = null;
	private HorizontalLayout fila2  = null;
	private CargaInventarioSGAView origen = null;
	private Grid gridDatos = null;
	private Panel panelGrid = null;
	
	public PeticionCargaInventarioSGA(CargaInventarioSGAView r_view)
	{
		this.origen=r_view;

		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				
				this.cmbFicheros=new Combo("Ficheros");
				this.cmbFicheros.setStyleName(ValoTheme.TEXTFIELD_TINY);
				this.cmbFicheros.setNullSelectionAllowed(false);
				this.cmbFicheros.setNewItemsAllowed(false);
				this.cmbFicheros.setInvalidAllowed(false);
				
				this.txtUbicacion=new TextField("Ubicacion");
				this.txtUbicacion.setStyleName(ValoTheme.TEXTFIELD_TINY);
				
				this.btnCargar = new Button("Buscar Fichero");
				this.btnCargar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.btnCargar.addStyleName(ValoTheme.BUTTON_TINY);

				fila1.addComponent(cmbFicheros);
				fila1.addComponent(txtUbicacion);
				fila1.addComponent(btnCargar);
				fila1.setComponentAlignment(this.cmbFicheros, Alignment.MIDDLE_LEFT);
				fila1.setComponentAlignment(this.txtUbicacion, Alignment.MIDDLE_LEFT);
				fila1.setComponentAlignment(this.btnCargar, Alignment.MIDDLE_LEFT);

			fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

			

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Cargar");
		btnBotonAVentana.setEnabled(false);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.btnCargar.focus();
		cargarFicheros();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Procesamos el fichero seleccionado en el combo y lo cargamos en la tabla
				 */
				if (cmbFicheros.getValue()!=null)
				{
					procesarFichero(cmbFicheros.getValue().toString());
					cargarDatosUbicacion();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Tienes que seleccionar un Fichero.");
				}
			}
		});
		btnCargar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				/*
				 * Proceso que leera la ruta de archivos y cargara los encontrados en el combo
				 */
				cargarFicheros();
			}
		});	
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
		cmbFicheros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (cmbFicheros.getValue()!=null && cmbFicheros.getValue().toString().length()>0 && txtUbicacion.getValue()!=null && txtUbicacion.getValue().toString().length()>0)
				{
					btnBotonAVentana.setEnabled(true);
				}
				else
					btnBotonAVentana.setEnabled(false);
			}
		});
		
		txtUbicacion.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				cargarDatosUbicacion();
				if (cmbFicheros.getValue()!=null && cmbFicheros.getValue().toString().length()>0 && txtUbicacion.getValue()!=null && txtUbicacion.getValue().toString().length()>0)
				{
					btnBotonAVentana.setEnabled(true);
				}
				else
					btnBotonAVentana.setEnabled(false);
			}
		});
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private void procesarFichero(String r_nombreArchivo)
	{
		consultaCargaInventarioSGAServer ci= null;
		String guardarDatos = null;
		boolean correcto = true;
		ArrayList<MapeoInventarioSGA> vector = RutinasFicheros.leerCsvInventarioSGA(",", LecturaProperties.rutaInventario + "/" + this.cmbFicheros.getValue().toString(), "N");
		Iterator iterator = vector.iterator();
		ci = consultaCargaInventarioSGAServer.getInstance(CurrentUser.get());
		
		while (iterator.hasNext())
		{
			MapeoInventarioSGA mapeo = (MapeoInventarioSGA) iterator.next();
		 	MapeoCargaInventarioSGA mapeoCarga = new MapeoCargaInventarioSGA();
			/*
			 * ahora el mapeo lo readapto y llamare a guardar
			 */
			mapeoCarga = procesarMapeo(mapeo);
			guardarDatos = ci.guardarNuevo(mapeoCarga);
			if (guardarDatos!=null)
			{
				correcto=false;
			}
		}
		if (correcto) 
			Notificaciones.getInstance().mensajeInformativo("Datos cargados correctamente.");
		else
			Notificaciones.getInstance().mensajeError("Errores cargando datos. Revisad ubicacion.");
	}

	private MapeoCargaInventarioSGA procesarMapeo(MapeoInventarioSGA r_mapeo)
	{
		MapeoCargaInventarioSGA mapeoCarga = null;
		consultaArticulosServer cas = null;
		
		mapeoCarga = new MapeoCargaInventarioSGA();
		cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		mapeoCarga.setCodigoRec("");
		mapeoCarga.setOrdenFabr("");
		mapeoCarga.setProveedor("");
		
		mapeoCarga.setSscc(r_mapeo.getLpn().substring(2));
		mapeoCarga.setLote(r_mapeo.getLote().substring(2));
		mapeoCarga.setArticulo(cas.obtenerArticuloEan(r_mapeo.getSscc().substring(2)));
		
		if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().length()>0) 
			mapeoCarga.setCantidad(new Integer(r_mapeo.getCantidad().substring(2)));
		else
			mapeoCarga.setCantidad(new Integer(cas.obtenerCantidadEan(mapeoCarga.getArticulo(), r_mapeo.getSscc().substring(2, 5))));
		
		mapeoCarga.setTipo("PT");
		mapeoCarga.setUbicacion(this.txtUbicacion.getValue().trim());
		
		return mapeoCarga;
	}
	
	private void cargarFicheros()
	{
		
		/*
		 * Proceso que lee la ruta de archivos y los carga en la aplicacion 
		 */
		
		String [] r_valores = RutinasFicheros.recuperarNombresArchivosCarpeta(LecturaProperties.rutaInventario, null, null);
		this.cmbFicheros.removeAllItems();
		
		if (r_valores!=null && r_valores.length>0)
		{
			for (int i=0; i<r_valores.length; i++)
			{
				this.cmbFicheros.addItem(r_valores[i]);
			}
		}		

	}
	
    private void cargarDatosUbicacion()
    {
    	ArrayList<MapeoInventarioSGA> vectorUbicacion = null;
    	consultaCargaInventarioSGAServer cas = new consultaCargaInventarioSGAServer(CurrentUser.get());
    	vectorUbicacion = cas.datosOpcionesGlobal(this.txtUbicacion.getValue().trim());
    	IndexedContainer container =null;
    	Iterator<MapeoInventarioSGA> iterator = null;
    	MapeoInventarioSGA mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("sscc", String.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("cantidad", String.class, null);
		container.addContainerProperty("lote", String.class, null);
			
		iterator = vectorUbicacion.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoInventarioSGA) iterator.next();
			
			file.getItemProperty("sscc").setValue(mapeo.getSscc());
			file.getItemProperty("articulo").setValue(mapeo.getArticulo());
			file.getItemProperty("cantidad").setValue(mapeo.getCantidad());
			file.getItemProperty("lote").setValue(mapeo.getLote());
		}

		if (gridDatos!=null)
		{
			gridDatos.removeAllColumns();
			gridDatos=null;
			fila2.removeAllComponents();
		}
		
		gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
		gridDatos.setWidth("100%");
		gridDatos.addStyleName("minigrid");
		
		if (panelGrid!=null)
		{
			panelGrid=null;
		}
		
		panelGrid = new Panel("Ubicacion: " + this.txtUbicacion.getValue());
    	panelGrid.setSizeUndefined(); // Shrink to fit content
    	panelGrid.setHeight("250px");
    	panelGrid.setContent(gridDatos);
		fila2.addComponent(panelGrid);
    }
}