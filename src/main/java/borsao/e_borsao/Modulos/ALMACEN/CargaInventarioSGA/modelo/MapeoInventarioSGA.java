package borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioSGA extends MapeoGlobal
{
	
	private String articulo;
	private String sscc;
	private String lote;
	private String fechaCad; 
	private String lpn; 
	private String cantidad; 
	
	
	public MapeoInventarioSGA()
	{
	}


	public String getSscc() {
		return sscc;
	}


	public String getLote() {
		return lote;
	}


	public String getFechaCad() {
		return fechaCad;
	}


	public String getLpn() {
		return lpn;
	}


	public String getCantidad() {
		return cantidad;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public void setLote(String lote) {
		this.lote = lote;
	}


	public void setFechaCad(String fechaCad) {
		this.fechaCad = fechaCad;
	}


	public void setLpn(String lpn) {
		this.lpn = lpn;
	}


	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}



}