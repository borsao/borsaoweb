package borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo.MapeoCargaInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.CargaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;

public class consultaCargaInventarioSGAServer {
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private static consultaCargaInventarioSGAServer instance;
	public String tabla = "inventarioPT";
	
	
	public consultaCargaInventarioSGAServer(String r_usuario) {
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaCargaInventarioSGAServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaCargaInventarioSGAServer(r_usuario);
		}
		return instance;
	}

	public ArrayList<MapeoInventarioSGA> datosOpcionesGlobal(String r_ubicacion)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventarioSGA> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT inventarioPT.sscc sga_sscc,");
		cadenaSQL.append(" inventarioPT.ubicacion sga_ubi,");
		cadenaSQL.append(" inventarioPT.articulo sga_art,");
		cadenaSQL.append(" inventarioPT.cantidad sga_can,");
		cadenaSQL.append(" inventarioPT.lote sga_lot ");
		cadenaSQL.append(" from inventarioPT ");

		try
		{
			if (r_ubicacion!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_ubicacion!=null && r_ubicacion.toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ubicacion = '" + r_ubicacion + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by lote asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventarioSGA>();
			
			while(rsOpcion.next())
			{
				MapeoInventarioSGA mapeoInventarioSGA = new MapeoInventarioSGA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventarioSGA.setSscc(rsOpcion.getString("sga_sscc"));
				mapeoInventarioSGA.setArticulo(rsOpcion.getString("sga_art"));
				mapeoInventarioSGA.setCantidad(rsOpcion.getString("sga_can"));
				mapeoInventarioSGA.setLote(rsOpcion.getString("sga_lot"));
				
				vector.add(mapeoInventarioSGA);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;	
	}
	
	public ArrayList<String> recuperarFicheros()
	{
		ArrayList<String> strFicheros = null;

		try
		{
			strFicheros= new ArrayList<String>();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return strFicheros;
	}

	public String guardarNuevo(MapeoCargaInventarioSGA r_mapeo) 
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO " + tabla + "( ");
			cadenaSQL.append(tabla + ".codigoRec,");
			cadenaSQL.append(tabla + ".ordenFabr,");
			cadenaSQL.append(tabla + ".sscc,");
			cadenaSQL.append(tabla + ".ubicacion,");
			cadenaSQL.append(tabla + ".articulo,");
			cadenaSQL.append(tabla + ".cantidad,");
			cadenaSQL.append(tabla + ".tipo,");
			cadenaSQL.append(tabla + ".lote,");
			cadenaSQL.append(tabla + ".fechaProd,");
			cadenaSQL.append(tabla + ".fechaCad,");
			cadenaSQL.append(tabla + ".proveedor) VALUES (");
			
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");

			con = this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());


			if (r_mapeo.getCodigoRec() != null) {
				preparedStatement.setString(1, r_mapeo.getCodigoRec());
			} else {
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getOrdenFabr() != null) {
				preparedStatement.setString(2, r_mapeo.getOrdenFabr());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(3, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getUbicacion() != null) {
				preparedStatement.setString(4, r_mapeo.getUbicacion());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getArticulo() != null) {
				preparedStatement.setString(5, r_mapeo.getArticulo());
			} else {
				preparedStatement.setString(5, null);
			}
			preparedStatement.setInt(6, r_mapeo.getCantidad());
			
			if (r_mapeo.getTipo() != null) {
				preparedStatement.setString(7, r_mapeo.getTipo());
			} else {
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getLote() != null) {
				preparedStatement.setString(8, r_mapeo.getLote());
			} else {
				preparedStatement.setString(8, null);
			}
			if (r_mapeo.getFechaProd() != null) {
				preparedStatement.setString(9, r_mapeo.getFechaProd());
			} else {
				preparedStatement.setString(9, null);
			}
			if (r_mapeo.getFechaCad() != null) {
				preparedStatement.setString(10, r_mapeo.getFechaCad());
			} else {
				preparedStatement.setString(10, null);
			}
			if (r_mapeo.getProveedor() != null) {
				preparedStatement.setString(11, r_mapeo.getProveedor());
			} else {
				preparedStatement.setString(11, null);
			}

			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
}