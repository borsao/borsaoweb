package borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;

public class consultaAlbaranesTrasladoServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAlbaranesTrasladoServer instance;
//	private static int litrosMinimos=consultaMovimientosSilicieServer.litrosMinimos;
	
	public consultaAlbaranesTrasladoServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlbaranesTrasladoServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlbaranesTrasladoServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> datosOpcionesGlobal(MapeoLineasAlbaranesTraslado r_mapeo, Integer r_almacen, String r_ref, boolean r_verObs)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		MapeoLineasAlbaranesTraslado mapeo = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.nombre det_nom, ");
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.num_linea det_lin, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi, ");
        cadenaSQL.append(" p.almacen det_alo ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_tr_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	if (r_ref!=null) cadenaSQL.append(" AND a.apartado = '" + r_ref + "' ");
     	cadenaSQL.append(" AND a.almacen_destino = " + r_almacen);
     	
     	if (r_mapeo.getArticulo()!=null)
     	{
     		cadenaSQL.append(" AND (p.articulo = '" + r_mapeo.getArticulo() + "') ");
     	}
     	if (r_mapeo.getLote()!=null)
     	{
     		cadenaSQL.append(" AND (p.numero_lote = '" + r_mapeo.getLote() + "') ");
     	}
     	
     	cadenaSQL.append(" UNION  ");
     	
     	cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.nombre det_nom, ");
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.num_linea det_lin, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi, ");
        cadenaSQL.append(" p.almacen det_alo ");
     	cadenaSQL.append(" FROM a_lin_1 p, a_tr_c_1 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	if (r_ref!=null)  cadenaSQL.append(" AND a.apartado = '" + r_ref + "' ");
     	cadenaSQL.append(" AND a.almacen_destino = " + r_almacen);
     	
     	if (r_mapeo.getArticulo()!=null)
     	{
     		cadenaSQL.append(" AND (p.articulo = '" + r_mapeo.getArticulo() + "') ");
     	}
     	if (r_mapeo.getLote()!=null)
     	{
     		cadenaSQL.append(" AND (p.numero_lote = '" + r_mapeo.getLote() + "') ");
     	}
     	cadenaSQL.append(" order by 7,2 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesTraslado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setAlmacen(rsOpcion.getInt("det_ald"));
				mapeo.setNombreAlmacen(rsOpcion.getString("det_nom"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				mapeo.setLote(rsOpcion.getString("det_lot"));
				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
				rellenarObs(vector,mapeo, rsOpcion.getInt("det_lin"));
				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	private void rellenarObs(ArrayList<MapeoLineasAlbaranesTraslado> r_vector, MapeoLineasAlbaranesTraslado r_mapeo, Integer r_numLinea )
	{
		MapeoLineasAlbaranesTraslado mapeo = null;
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		String ejer = "0";
		if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_mapeo.getEjercicio());

		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.nombre det_nom, ");
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.num_linea det_lin, ");
        cadenaSQL.append(" p.movimiento det_mov, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi, ");
        cadenaSQL.append(" p.almacen det_alo ");
     	cadenaSQL.append(" FROM a_lin_"+ ejer + " p, a_tr_c_" + ejer + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.documento = '" + r_mapeo.getDocumento() + "' ");
     	cadenaSQL.append(" AND p.codigo = " + r_mapeo.getIdCodigoTraslado());
     	cadenaSQL.append(" AND a.almacen_destino = " + r_mapeo.getAlmacen());
     	cadenaSQL.append(" order by p.documento,p.codigo,p.num_linea ");

     	/*
		 * saco las lineas siguientes a la mia y paro al encontrar otro articulo
		 */
     	
     	try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			boolean estamos = false;
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("det_lot")!=null && rsOpcion.getString("det_art")!=null && r_mapeo.getArticulo().contentEquals(rsOpcion.getString("det_art")) && r_numLinea.equals(rsOpcion.getInt("det_lin")) && r_mapeo.getLote().contentEquals(rsOpcion.getString("det_lot")))
				{
					estamos = true;
				} 
				else if (estamos && rsOpcion.getString("det_mov").contentEquals("OA"))
				{
					mapeo = new MapeoLineasAlbaranesTraslado();
					mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
					mapeo.setDocumento(rsOpcion.getString("det_doc"));
					mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
					mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
					mapeo.setAlmacen(rsOpcion.getInt("det_ald"));
					mapeo.setNombreAlmacen(rsOpcion.getString("det_nom"));
					mapeo.setArticulo(rsOpcion.getString("det_art"));
					mapeo.setDescripcion(rsOpcion.getString("det_des"));
					mapeo.setUnidades(rsOpcion.getInt("det_uds"));
					mapeo.setReferencia(rsOpcion.getString("det_ref"));
					mapeo.setLote(rsOpcion.getString("det_lot"));
					mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
					r_vector.add(mapeo);
				}
				else if (rsOpcion.getString("det_lot")!=null && rsOpcion.getString("det_art")!=null && (!r_mapeo.getArticulo().contentEquals(rsOpcion.getString("det_art")) || r_mapeo.getLote().contentEquals(rsOpcion.getString("det_lot"))))
				{
					estamos = false;
				}					

			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> datosOpcionesGlobal(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		MapeoLineasAlbaranesTraslado mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
		cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.almacen_origen det_alo, ");
        
        cadenaSQL.append(" d.nombre det_and, ");        
        cadenaSQL.append(" d.direccion det_dird, ");
        cadenaSQL.append(" d.cod_postal det_cpd, ");
        cadenaSQL.append(" d.encargado det_caed, ");

        cadenaSQL.append(" o.nombre det_ano, ");
        cadenaSQL.append(" o.direccion det_diro, ");
        cadenaSQL.append(" o.cod_postal det_cpo, ");
        cadenaSQL.append(" o.encargado det_caeo, ");
        
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi, ");
        cadenaSQL.append(" p.almacen det_alo ");
        cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_almac d, e_almac o, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.almacen_destino = d.almacen and d.encargado <>'' ");
     	cadenaSQL.append(" AND a.almacen_origen = o.almacen and o.encargado <>'' ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	
     	if (r_clave_tabla!=null)
     	{
     		cadenaSQL.append(" AND a.clave_tabla = '" + r_clave_tabla + "' ");
     	}
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" AND a.documento = '" + r_documento + "' ");
     	}
     	if (r_serie!=null)
     	{
     		cadenaSQL.append(" AND a.serie = '" + r_serie + "' ");
     	}
     	if (r_albaran!=null)
     	{
     		cadenaSQL.append(" AND a.codigo = " + r_albaran);
     	}
     	
     	cadenaSQL.append(" order by 7,2 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesTraslado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				if (r_clave_tabla!=null && r_clave_tabla.equals("t"))
				{
					mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
					mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
					mapeo.setCae(rsOpcion.getString("det_caed"));
					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
				}
				else
				{
					mapeo.setAlmacen(rsOpcion.getInt("det_alo"));
					mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
					mapeo.setCae(rsOpcion.getString("det_caeo"));
					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_diro")));
					mapeo.setCodigoPostal(rsOpcion.getString("det_cpo"));
				}
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				mapeo.setLote(rsOpcion.getString("det_lot"));
				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}		
		return vector;
	}
	
	
	public ArrayList<MapeoLineasAlbaranesTraslado> datosOpcionesGlobalSinEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Date r_fecha, String r_almacen, String r_almacenDestino)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		MapeoLineasAlbaranesTraslado mapeo = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.almacen_origen det_alo, ");
        
        cadenaSQL.append(" d.nombre det_and, ");        
        cadenaSQL.append(" d.direccion det_dird, ");
        cadenaSQL.append(" d.cod_postal det_cpd, ");
        cadenaSQL.append(" d.encargado det_caed, ");

        cadenaSQL.append(" o.nombre det_ano, ");
        cadenaSQL.append(" o.direccion det_diro, ");
        cadenaSQL.append(" o.cod_postal det_cpo, ");
        cadenaSQL.append(" o.encargado det_caeo, ");
        
        cadenaSQL.append(" a.serie det_ser, ");
        cadenaSQL.append(" a.apartado det_ref, ");	
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades * e.marca det_uds, ");
        cadenaSQL.append(" p.numero_lote det_lot, ");
        cadenaSQL.append(" p.ubicacion det_ubi ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_almac d, e_almac o, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.almacen_destino = d.almacen and d.encargado <>'' ");
     	cadenaSQL.append(" AND a.almacen_origen = o.almacen and o.encargado <>'' ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*') ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	
     	
     	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacen!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacen + "' ");
     	}
     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacen!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacen + "' ");
     	}
     	
     	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacenDestino!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacenDestino + "' ");
     	}
     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacenDestino!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacenDestino + "' ");
     	}
     	if (r_clave_tabla!=null)
     	{
     		cadenaSQL.append(" AND a.clave_tabla = '" + r_clave_tabla + "' ");
     	}
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" AND a.documento = '" + r_documento + "' ");
     	}
     	if (r_serie!=null)
     	{
     		cadenaSQL.append(" AND a.serie = '" + r_serie + "' ");
     	}
     	if (r_albaran!=null)
     	{
     		cadenaSQL.append(" AND a.codigo = " + r_albaran);
     	}
     	if (r_fecha!=null)
     	{
     		cadenaSQL.append(" AND a.fecha_documento = '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
     	}
     	
     	cadenaSQL.append(" order by 7,2 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesTraslado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				
				if (r_clave_tabla!=null && r_clave_tabla.equals("t"))
				{
					mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
					mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
					mapeo.setCae(rsOpcion.getString("det_caed"));
					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
				}
				else
				{
					mapeo.setAlmacen(rsOpcion.getInt("det_alo"));
					mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
					mapeo.setCae(rsOpcion.getString("det_caeo"));
					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_diro")));
					mapeo.setCodigoPostal(rsOpcion.getString("det_cpo"));
				}
				
				mapeo.setSerie(rsOpcion.getString("det_ser"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("det_des")));
				mapeo.setNif("A50889955");
				mapeo.setUnidades(Long.valueOf(Math.round(rsOpcion.getDouble("det_uds"))).intValue());				
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				mapeo.setLote(rsOpcion.getString("det_lot"));
				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> datosOpcionesGlobalSumaSinEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Date r_fecha, String r_almacen, String r_almacenDestino)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		MapeoLineasAlbaranesTraslado mapeo = null;
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.almacen_origen det_alo, ");
        cadenaSQL.append(" a.codigo det_cod, ");
        
        cadenaSQL.append(" d.nombre det_and, ");        
//        cadenaSQL.append(" d.direccion det_dird, ");
//        cadenaSQL.append(" d.cod_postal det_cpd, ");
        cadenaSQL.append(" d.encargado det_caed, ");

        cadenaSQL.append(" o.nombre det_ano, ");        
//        cadenaSQL.append(" o.direccion det_diro, ");
//        cadenaSQL.append(" o.cod_postal det_cpo, ");
        cadenaSQL.append(" o.encargado det_caeo, ");

        cadenaSQL.append(" a.serie det_ser, ");
        
        cadenaSQL.append(" sum(p.unidades * e.marca) det_uds ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_almac d, e_almac o, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.almacen_destino = d.almacen and d.encargado <>'' ");
     	cadenaSQL.append(" AND a.almacen_origen = o.almacen and o.encargado <>'' ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.articulo matches '0101*'  ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	
     	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacen!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacen + "' ");
     	}
     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacen!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacen + "' ");
     	}
     	
     	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacenDestino!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacenDestino + "' ");
     	}
     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacenDestino!=null)
     	{
     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacenDestino + "' ");
     	}
     	if (r_clave_tabla!=null)
     	{
     		cadenaSQL.append(" AND a.clave_tabla = '" + r_clave_tabla + "' ");
     	}
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" AND a.documento = '" + r_documento + "' ");
     	}
     	if (r_serie!=null)
     	{
     		cadenaSQL.append(" AND a.serie = '" + r_serie + "' ");
     	}
     	if (r_albaran!=null)
     	{
     		cadenaSQL.append(" AND a.codigo = " + r_albaran);
     	}
     	if (r_fecha!=null)
     	{
     		cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
     	}
     	
     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10,11 ");
//     	cadenaSQL.append(" having sum(p.unidades * e.marca) >= "+ litrosMinimos);
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesTraslado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				
				
				if (r_clave_tabla!=null && r_clave_tabla.equals("T" ))
				{
					if (r_almacen!=null)
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
						mapeo.setCae(rsOpcion.getString("det_caed"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
					else
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_alo")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
						mapeo.setCae(rsOpcion.getString("det_caeo"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
				}
				else
				{
					if (r_almacenDestino!=null)
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
						mapeo.setCae(rsOpcion.getString("det_caed"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_diro")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpo"));
					}
					else
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_alo")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
						mapeo.setCae(rsOpcion.getString("det_caeo"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
				}

				mapeo.setSerie(rsOpcion.getString("det_ser"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				
				mapeo.setArticulo("Granel");
				
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
//				mapeo.setArticulo(rsOpcion.getString("det_art"));
//				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("det_des")));
				mapeo.setNif("A50889955");
				mapeo.setUnidades(Long.valueOf(Math.round(rsOpcion.getDouble("det_uds"))).intValue());				
//				mapeo.setReferencia(rsOpcion.getString("det_ref"));
//				mapeo.setLote(rsOpcion.getString("det_lot"));
//				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
			}
				

			rsOpcion.close();
			rsOpcion=null;
			cs.close();
			cs=null;
			con.close();
			con=null;
			
			cadenaSQL = new StringBuffer();
			digito = "0";
			ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio == ejercicioActual-1) digito="1";
			
			cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
			cadenaSQL.append(" p.fecha_documento det_fec, ");
			cadenaSQL.append(" p.documento det_doc, ");
	        cadenaSQL.append(" a.almacen_destino det_ald, ");
	        cadenaSQL.append(" a.almacen_origen det_alo, ");
	        cadenaSQL.append(" a.codigo det_cod, ");
	        
	        cadenaSQL.append(" d.nombre det_and, ");        
//	        cadenaSQL.append(" d.direccion det_dird, ");
//	        cadenaSQL.append(" d.cod_postal det_cpd, ");
	        cadenaSQL.append(" d.encargado det_caed, ");


	        cadenaSQL.append(" o.nombre det_ano, ");        
//	        cadenaSQL.append(" o.direccion det_diro, ");
//	        cadenaSQL.append(" o.cod_postal det_cpo, ");
	        cadenaSQL.append(" o.encargado det_caeo, ");
	        
	        cadenaSQL.append(" a.serie det_ser, ");
	        
	        cadenaSQL.append(" sum(p.unidades * e.marca) det_uds ");
	     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_almac d, e_almac o, e_articu e ");
	     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
	     	cadenaSQL.append(" AND a.documento = p.documento ");
	     	cadenaSQL.append(" AND a.serie = p.serie ");
	     	cadenaSQL.append(" AND a.codigo = p.codigo ");
	     	cadenaSQL.append(" AND a.cl_listada_ruta='N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
	     	cadenaSQL.append(" AND a.almacen_destino = d.almacen and d.encargado <>'' ");
	     	cadenaSQL.append(" AND a.almacen_origen = o.almacen and o.encargado <>'' ");
	     	cadenaSQL.append(" AND e.articulo = p.articulo ");
	     	cadenaSQL.append(" AND (p.articulo matches '0111*' or p.articulo matches '0103*' or p.articulo matches '0102*') ");
	     	
	    	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacen!=null)
	     	{
	     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacen + "' ");
	     	}
	     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacen!=null)
	     	{
	     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacen + "' ");
	     	}
	     	
	     	if (r_clave_tabla!=null && r_clave_tabla.equals("T" ) && r_almacenDestino!=null)
	     	{
	     		cadenaSQL.append(" AND a.almacen_destino = '" + r_almacenDestino + "' ");
	     	}
	     	if (r_clave_tabla!=null && r_clave_tabla.equals("t" ) && r_almacenDestino!=null)
	     	{
	     		cadenaSQL.append(" AND a.almacen_origen = '" + r_almacenDestino + "' ");
	     	}
	     	if (r_clave_tabla!=null)
	     	{
	     		cadenaSQL.append(" AND a.clave_tabla = '" + r_clave_tabla + "' ");
	     	}
	     	if (r_documento!=null)
	     	{
	     		cadenaSQL.append(" AND a.documento = '" + r_documento + "' ");
	     	}
	     	if (r_serie!=null)
	     	{
	     		cadenaSQL.append(" AND a.serie = '" + r_serie + "' ");
	     	}
	     	if (r_albaran!=null)
	     	{
	     		cadenaSQL.append(" AND a.codigo = " + r_albaran);
	     	}
	     	if (r_fecha!=null)
	     	{
	     		cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
	     	}
	     	
	     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10,11 ");
//	     	cadenaSQL.append(" having sum(p.unidades * e.marca) >= " + litrosMinimos);
	     	con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				
				if (r_clave_tabla!=null && r_clave_tabla.equals("T" ))
				{
					if (r_almacen!=null)
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
						mapeo.setCae(rsOpcion.getString("det_caed"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
					else
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_alo")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
						mapeo.setCae(rsOpcion.getString("det_caeo"));
	//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
	//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
				}
				else
				{
					if (r_almacenDestino!=null)
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_ald")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_and")));
						mapeo.setCae(rsOpcion.getString("det_caed"));
						//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dird")));
						//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpd"));
					}
					else
					{
						mapeo.setAlmacen(rsOpcion.getInt("det_alo")); 
						mapeo.setNombreAlmacen(RutinasCadenas.conversion(rsOpcion.getString("det_ano")));
						mapeo.setCae(rsOpcion.getString("det_caeo"));
						//					mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_diro")));
						//					mapeo.setCodigoPostal(rsOpcion.getString("det_cpo"));
					}
				}
				
				mapeo.setSerie(rsOpcion.getString("det_ser"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				
				mapeo.setArticulo("PT");
				
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
//				mapeo.setArticulo(rsOpcion.getString("det_art"));
//				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("det_des")));
				mapeo.setNif("A50889955");
				mapeo.setUnidades(Long.valueOf(Math.round(rsOpcion.getDouble("det_uds"))).intValue());				
//				mapeo.setReferencia(rsOpcion.getString("det_ref"));
//				mapeo.setLote(rsOpcion.getString("det_lot"));
//				mapeo.setUbicacion(rsOpcion.getString("det_ubi"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> datosOpcionesEnviosTotales(Integer r_almacen)
	{
		Connection con =null;
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		MapeoLineasAlbaranesTraslado mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.nombre det_nom, ");
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.almacen det_alo, ");
        cadenaSQL.append(" sum(p.unidades) det_uds ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_tr_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND a.apartado in (select ref_cliente from a_vp_c_0 where cumplimentacion is null)" );
     	cadenaSQL.append(" AND a.almacen_destino = " + r_almacen);
     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10 ");
     	
     	cadenaSQL.append(" UNION ");
     	
     	cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" a.almacen_destino det_ald, ");
        cadenaSQL.append(" a.nombre det_nom, ");
        cadenaSQL.append(" a.apartado det_ref, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.almacen det_alo, ");
        cadenaSQL.append(" sum(p.unidades) det_uds ");
     	cadenaSQL.append(" FROM a_lin_1 p, a_tr_c_1 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND a.apartado in (select ref_cliente from a_vp_c_1 where cumplimentacion is null)" );
     	cadenaSQL.append(" AND a.almacen_destino = " + r_almacen);
     	
     	cadenaSQL.append(" group by 1,2,3,4,5,6,7,8,9,10 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesTraslado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesTraslado();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setAlmacen(rsOpcion.getInt("det_ald"));
				mapeo.setNombreAlmacen(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoTraslado(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getInt("det_uds"));
				mapeo.setReferencia(rsOpcion.getString("det_ref"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public String estadoCabeceraAlbaranEMCS(Integer r_ejercicio, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		Statement cs = null;
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" select cl_listada_ruta tr_ru from a_tr_c_" + ejer );    		
	        cadenaSQL.append(" where clave_tabla = 't' ");
	        cadenaSQL.append(" and documento = '" + r_documento + "' ");
	        cadenaSQL.append(" and serie = '"  + r_serie + "' " );
	        cadenaSQL.append(" and codigo = " + r_albaran);
		    
	        con= this.conManager.establecerConexionGestionInd();			
	        cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());

			while(rsOpcion.next())
			{
				return rsOpcion.getString("tr_ru");
			}			
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return null;
	    }
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "N";
	}

	public boolean marcarCabeceraAlbaranEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		try
		{
	        
			cadenaSQL.append(" UPDATE a_tr_c_" + ejer + " set ");
    		cadenaSQL.append(" cl_listada_ruta = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "S" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	
	public boolean marcarAlbaranSilicie(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
    		cadenaSQL.append(" observacion[1] = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	
	public boolean liberarCabeceraAlbaranEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con  = null;
		
		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_tr_c_" + ejer + " set ");
    		cadenaSQL.append(" cl_listada_ruta = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N" );
		    preparedStatement.setString(2, r_clave_tabla );
		    preparedStatement.setString(3, r_documento );
		    preparedStatement.setString(4, r_serie );
		    preparedStatement.setInt(5, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeSeguimiento("Metodo liberar cabecera albaran traslado ");
	    	serNotif.mensajeSeguimiento("Ejercicio " + String.valueOf(r_ejercicio) );
	    	serNotif.mensajeSeguimiento("Clave tabla " + r_clave_tabla );
	    	serNotif.mensajeSeguimiento("Documento " + r_documento );
	    	serNotif.mensajeSeguimiento("Serie " + r_serie);
	    	serNotif.mensajeSeguimiento("Codigo " + String.valueOf(r_albaran) );
	    	
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;

	}

	public boolean chequeoEntradaSinEmcs(Integer r_ejercicio, String r_cae_titular, Integer r_mes) 
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsOpcion = null;		
		boolean error = false;
		String rdo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";

		cadenaSQL.append(" SELECT p.codigo, sum(p.unidades*e.marca) cuantos ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_articu e, e_almac w, e_almac d");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");     	
     	cadenaSQL.append(" AND w.almacen = a.almacen_destino and w.encargado = '" + r_cae_titular + "' ");
     	cadenaSQL.append(" AND d.almacen = a.almacen_origen and d.encargado <> '' and d.pais='000'");
     	cadenaSQL.append(" AND a.clave_tabla = 't' ");
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*' or p.articulo matches '0101*') ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' ");
     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
     	
     	
     	
		try
		{
			cadenaSQL.append(" AND month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" GROUP BY 1 ");
//			cadenaSQL.append(" having sum(p.unidades*e.marca)>= " + litrosMinimos);
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rdo==null) rdo= "";
				rdo = rdo + " " + rsOpcion.getString("codigo") + " cuantos: " + rsOpcion.getString("cuantos");
				Notificaciones.getInstance().mensajeSeguimiento("codigo: " + rsOpcion.getString("codigo") + " cuantos: " + rsOpcion.getString("cuantos"));
				error = true;
			}
			if (error) Notificaciones.getInstance().mensajeError(rdo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return !error;
	}

	public boolean chequeoSalidaSinEmcs(Integer r_ejercicio, String r_cae_titular, Integer r_mes) 
	{
		Connection con = null;
		Statement cs = null;

		ResultSet rsOpcion = null;		
		boolean error = false;
		String rdo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";

		cadenaSQL.append(" SELECT p.codigo, sum(p.unidades*e.marca) cuantos ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_tr_c_" + digito + " a, e_articu e, e_almac w, e_almac d ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND w.almacen = a.almacen_origen and w.encargado = '" + r_cae_titular + "' ");
     	cadenaSQL.append(" AND d.almacen = a.almacen_destino and d.encargado <> ''");
     	cadenaSQL.append(" AND a.clave_tabla = 'T' ");
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*' or p.articulo matches '0101*') ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' ");
     	cadenaSQL.append(" and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) " );
     	
		try
		{
			cadenaSQL.append(" AND month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" GROUP BY 1 ");
//			cadenaSQL.append(" having sum(p.unidades*e.marca)>= " + litrosMinimos);
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rdo==null) rdo= "";
				rdo = rdo + " " + rsOpcion.getString("codigo") + " litros: " + rsOpcion.getString("cuantos");
				Notificaciones.getInstance().mensajeSeguimiento("codigo: " + rsOpcion.getString("codigo") + " litros: " + rsOpcion.getString("cuantos"));
				error = true;
			}
			if (error) Notificaciones.getInstance().mensajeError(rdo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return !error;
	}

	@Override
	public String semaforos() {

		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.codigo, sum(p.unidades*e.marca) cuantos ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_tr_c_0 a, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND a.clave_tabla = 't' ");
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*' or p.articulo matches '0101*') ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' and p.observacion[1] <> 'N' ");
     	     	
		try
		{
			cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 1) + "' ");
			cadenaSQL.append(" GROUP BY 1 ");
//			cadenaSQL.append(" having sum(p.unidades*e.marca)>= "+ litrosMinimos);
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return "2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "1";
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}
	
	
	public String semaforosSalida() {

		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.codigo, sum(p.unidades*e.marca) cuantos ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_tr_c_0 a, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND a.clave_tabla = 'T' ");
     	cadenaSQL.append(" AND (p.articulo matches '0102*' or p.articulo matches '0111*' or p.articulo matches '0103*' or p.articulo matches '0101*') ");
     	cadenaSQL.append(" AND a.cl_listada_ruta='N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	
		try
		{
			cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 1) + "' ");
			cadenaSQL.append(" GROUP BY 1 ");
//			cadenaSQL.append(" having sum(p.unidades*e.marca)>= " + litrosMinimos);
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return "2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "1";
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}
}


