package borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo;

import java.util.ArrayList;

import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.pantallaAsignacionDatosRecepcionEMCSAlbaran;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaLineasTrasladosGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = true;
	private pantallaAsignacionDatosRecepcionEMCSAlbaran appRec = null;
	
    public AyudaLineasTrasladosGrid(ArrayList<MapeoLineasAlbaranesTraslado> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    public AyudaLineasTrasladosGrid(pantallaAsignacionDatosRecepcionEMCSAlbaran r_appRec, ArrayList<MapeoLineasAlbaranesTraslado> r_vector) 
    {
         	this.vector=r_vector;
    	this.asignarTitulo("");
    	this.appRec = r_appRec;
    	
    	if (this.vector==null || this.vector.size()==0)
    	{
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
    	}
    	else
    	{
    		this.generarGrid();
    	}
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoLineasAlbaranesTraslado.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo", "ejercicio", "documento", "idCodigoTraslado", "fechaDocumento", "referencia" ,"almacen", "nombreAlmacen", "unidades", "nif", "cae");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("documento", "35");
    	this.widthFiltros.put("idCodigoTraslado", "85");
//    	this.widthFiltros.put("nombreAlmacen", "410");
    	this.widthFiltros.put("almacen", "70");
    	this.widthFiltros.put("articulo", "70");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(110));

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setSortable(false);
    	this.getColumn("ejercicio").setWidth(new Double(95));

    	this.getColumn("documento").setHeaderCaption("Doc");
    	this.getColumn("documento").setSortable(false);
    	this.getColumn("documento").setWidth(new Double(75));
    	
    	this.getColumn("idCodigoTraslado").setHeaderCaption("Numero");
    	this.getColumn("idCodigoTraslado").setSortable(true);
    	this.getColumn("idCodigoTraslado").setWidth(new Double(125));
    	
    	this.getColumn("fechaDocumento").setHeaderCaption("Fecha");
    	this.getColumn("fechaDocumento").setSortable(true);
    	this.getColumn("fechaDocumento").setWidth(new Double(125));
    	this.getColumn("fechaDocumento").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));

    	
    	this.getColumn("almacen").setHeaderCaption("Origen");
    	this.getColumn("almacen").setSortable(false);
    	this.getColumn("almacen").setWidth(new Double(110));
    	
    	this.getColumn("nombreAlmacen").setHeaderCaption("Almacen Origen");
    	this.getColumn("nombreAlmacen").setSortable(false);
    	this.getColumn("nombreAlmacen").setWidth(450);

    	this.getColumn("unidades").setHeaderCaption("Litros");
    	this.getColumn("unidades").setSortable(false);
    	this.getColumn("unidades").setWidth(new Double(110));
    	
    	this.getColumn("cae").setHeaderCaption("CAE");
    	this.getColumn("cae").setSortable(false);
    	this.getColumn("cae").setWidth(150);

    	this.getColumn("nif").setHeaderCaption("NIF");
    	this.getColumn("nif").setSortable(false);
    	this.getColumn("nif").setWidth(125);
    	
//    	this.getColumn("articulo").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("referencia").setHidden(true);
    	
    	this.getColumn("serie").setHidden(true);    	
    	this.getColumn("lote").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ubicacion").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("idControlEmbotellado".equals(cellReference.getPropertyId()) || "cantidad".equals(cellReference.getPropertyId()) ) 
            	{
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fechaDocumento");
		this.camposNoFiltrar.add("almacen");
		this.camposNoFiltrar.add("nombreAlmacen");
		this.camposNoFiltrar.add("unidades");
		this.camposNoFiltrar.add("nif");
		this.camposNoFiltrar.add("cae");
		this.camposNoFiltrar.add("codigoPostal");
		this.camposNoFiltrar.add("direccion");
		this.camposNoFiltrar.add("descripcion");
	}

	@Override
	public void cargarListeners() 
	{
		this.addSelectionListener(new SelectionListener() {
			
			@Override
			public void select(SelectionEvent event) {
					
				if (getSelectionModel() instanceof SingleSelectionModel)
					if (getSelectedRow()!=null && appRec!=null) appRec.filaSeleccionada(getSelectedRow());
			}
		});
	}

	@Override
	public void calcularTotal() {
		
	}
}


