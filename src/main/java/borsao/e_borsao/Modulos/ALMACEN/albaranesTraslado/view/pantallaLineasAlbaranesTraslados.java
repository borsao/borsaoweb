package borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoLineasPedidosVentas;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.RecepcionEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.SeguimientoEMCSView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasAlbaranesTraslados extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private Button btnBotonAVentana = null;
	private GridViewRefresh app = null;

	public pantallaLineasAlbaranesTraslados(String r_titulo, Integer r_almacen, String r_articulo, String r_ref)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesTrasladoServer cse = new consultaAlbaranesTrasladoServer(CurrentUser.get());
		MapeoLineasAlbaranesTraslado mapeo = new MapeoLineasAlbaranesTraslado();
		mapeo.setArticulo(r_articulo);
		mapeo.setAlmacen(r_almacen);
		ArrayList<MapeoLineasAlbaranesTraslado> vector = cse.datosOpcionesGlobal(mapeo, r_almacen, r_ref,false);

		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);

		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasAlbaranesTraslados(String r_titulo, Integer r_almacen, String r_articulo, String r_lote, String r_ref, boolean r_verObs)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesTrasladoServer cse = new consultaAlbaranesTrasladoServer(CurrentUser.get());
		MapeoLineasAlbaranesTraslado mapeo = new MapeoLineasAlbaranesTraslado();
		mapeo.setArticulo(r_articulo.trim());
		mapeo.setAlmacen(r_almacen);
		mapeo.setLote(r_lote.trim());
		ArrayList<MapeoLineasAlbaranesTraslado> vector = cse.datosOpcionesGlobal(mapeo, r_almacen, r_ref, r_verObs);
		
		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		
		principal.addComponent(this.gridDatos);
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	
	public pantallaLineasAlbaranesTraslados(GridViewRefresh r_app, String r_titulo, Integer r_ejercicio, String r_claveTabla, String r_documento, String r_serie, Integer r_albaran)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		this.app=r_app;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaAlbaranesTrasladoServer cse = new consultaAlbaranesTrasladoServer(CurrentUser.get());
		ArrayList<MapeoLineasAlbaranesTraslado> vector = cse.datosOpcionesGlobal(r_ejercicio, r_claveTabla, r_documento, r_serie, r_albaran);

		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonAVentana = new Button("Quitar Albaran");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY); 

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				liberar();
			}
		});
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);
		
		if ((!(this.app instanceof AsignacionSalidasEMCSView)) && (!(this.app instanceof AsignacionEntradasEMCSView)))
		{
			botonera.addComponent(btnBotonAVentana);	
			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		}
		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	private void verLineasAlbaran()
	{
		
	}
	
	private void llenarRegistros(ArrayList<MapeoLineasAlbaranesTraslado> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasAlbaranesTraslado mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Fecha Documento", Date.class, null);
		container.addContainerProperty("Documento", String.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("NombreAlmacen", String.class, null);
		container.addContainerProperty("Unidades", Integer.class, null);
		container.addContainerProperty("Lote", String.class, null);
		container.addContainerProperty("Añada", String.class, null);
		container.addContainerProperty("Almacen", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasAlbaranesTraslado) iterator.next();
			
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
    		file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
    		file.getItemProperty("NombreAlmacen").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreAlmacen()));
    		file.getItemProperty("Lote").setValue(RutinasCadenas.conversion(mapeoAyuda.getLote()));
    		file.getItemProperty("Añada").setValue(RutinasCadenas.conversion(mapeoAyuda.getUbicacion()));
    		
    		if (mapeoAyuda.getFechaDocumento()!=null)
    		{
    			file.getItemProperty("Fecha Documento").setValue(mapeoAyuda.getFechaDocumento());
    			file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoTraslado().toString());
    			file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen().toString());
    		}
    		else
    		{
    			file.getItemProperty("Fecha Documento").setValue(null);
    			file.getItemProperty("Codigo").setValue(null);
    			file.getItemProperty("Almacen").setValue(null);
    		}
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.gridDatos.getColumn("Fecha Documento").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
		
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Unidades".equals(cellReference.getPropertyId()) || "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId()) || "Almacen".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void liberar()
	{
		if (this.app instanceof SeguimientoEMCSView) 
			((SeguimientoEMCSView) this.app).cerrarVentanaBusqueda(null, null, null, null,null,null);
		if (this.app instanceof RecepcionEMCSView) 
			((RecepcionEMCSView) this.app).cerrarVentanaBusqueda(null, null, null, null, null,null,null,null,null);
		
		close();
	}
}