package borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasCorteIngles extends MapeoGlobal
{
	private String sscc;
	private String code;
	private String ean;
	private String articulo= null;
	private String descripcion = null;
	private Integer bultos = null;
	private Integer bultosTotal = null;
	
	public MapeoEtiquetasCorteIngles()
	{
		
	}


	public String getSscc() {
		return sscc;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getEan() {
		return ean;
	}


	public void setEan(String ean) {
		this.ean = ean;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getBultos() {
		return bultos;
	}


	public void setBultos(Integer bultos) {
		this.bultos = bultos;
	}


	public Integer getBultosTotal() {
		return bultosTotal;
	}


	public void setBultosTotal(Integer bultosTotal) {
		this.bultosTotal = bultosTotal;
	}


}