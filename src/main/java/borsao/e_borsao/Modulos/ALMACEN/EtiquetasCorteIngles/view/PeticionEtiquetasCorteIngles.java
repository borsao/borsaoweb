package borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.view;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.modelo.MapeoEtiquetasCorteIngles;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.server.consultaEtiquetasCorteInglesServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasCorteIngles extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbPrinter = null;
	private TextField lblDescripcion = null;
	private TextFieldAyuda txtArticulo = null;
	private TextField txtBulto = null;
	private TextField txtBultos = null;
	private TextField txtSscc= null;
	
	
	private consultaEtiquetasCorteInglesServer cecis = null;
	private EtiquetasCorteInglesView origen = null;
	
	public PeticionEtiquetasCorteIngles(EtiquetasCorteInglesView r_view)
	{
		cecis = new consultaEtiquetasCorteInglesServer(CurrentUser.get());
		this.origen=r_view;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtArticulo=new TextFieldAyuda("Articulo","");
				this.lblDescripcion = new TextField();
				this.lblDescripcion.setWidth("400px");
				this.lblDescripcion.setEnabled(false);
				this.lblDescripcion.addStyleName("planos");
				this.lblDescripcion.setStyleName("planos");
				
				fila1.addComponent(this.txtArticulo);
				fila1.addComponent(this.lblDescripcion);
				fila1.setComponentAlignment(this.lblDescripcion, Alignment.BOTTOM_CENTER);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtBulto = new TextField("BULTO");
				this.txtBulto.setWidth("100px");
				this.txtBulto.setRequired(true);
				this.txtBultos = new TextField("TOTAL BULTOS");
				this.txtBultos.setWidth("100px");
				this.txtBultos.setRequired(true);

				fila2.addComponent(this.txtBulto);
				fila2.addComponent(this.txtBultos);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtSscc= new TextField("SSCC: ");
				this.txtSscc.setWidth("200px");
				this.txtSscc.setRequired(true);
				
				fila3.addComponent(this.txtSscc);
			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);
				this.cmbPrinter.setWidth("200px");
				fila4.addComponent(cmbPrinter);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}

	private void cargarListeners()
	{

		ValueChangeListener art =  new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				String strDesc = null;
				if (txtArticulo.txtTexto.getValue()!=null)
				{
					strDesc= cecis.obtenerDescripcionArticulo(txtArticulo.txtTexto.getValue().trim());
					lblDescripcion.setValue(strDesc);
				}
				else
				{
					lblDescripcion.setValue("");
				}
			}
		};
		
		this.txtArticulo.txtTexto.addValueChangeListener(art);
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				MapeoEtiquetasCorteIngles mapeo = null;
				if (todoEnOrden())
				{
					mapeo = new MapeoEtiquetasCorteIngles();
					mapeo.setArticulo(txtArticulo.txtTexto.getValue());
					mapeo.setDescripcion(lblDescripcion.getValue());
					mapeo.setBultos(new Integer(txtBulto.getValue()));
					mapeo.setBultosTotal(new Integer(txtBultos.getValue()));
					mapeo.setSscc(txtSscc.getValue());
					
					boolean generado = cecis.generarDatosEtiquetas(mapeo);
					if (generado)
					{
						String pdfGenerado = cecis.generarInforme(mapeo.getArticulo());
						
						if(pdfGenerado.length()>0) 
						{
							if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
							{
								RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
							}
							else
							{
								String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
								RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
							    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
							}
						}
				    	else
				    		Notificaciones.getInstance().mensajeError("Error en la generacion");
					}
					
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.txtArticulo.txtTexto.getValue()==null || this.txtArticulo.txtTexto.getValue().toString().length()==0)
		{
			devolver = false;
			this.txtArticulo.txtTexto.focus();
		}
		else if (this.txtBulto.getValue()==null || this.txtBulto.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtBulto.focus();
		}
		else if (this.txtBultos.getValue()==null || this.txtBultos.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtBultos.focus();
		}
		else if (this.txtSscc.getValue()==null || this.txtSscc.getValue().toString().length()==0) 
		{
			devolver =false;
			txtSscc.focus();
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		this.cmbPrinter.setValue(defecto);
	}

}