package borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.server;

import java.sql.Connection;
import java.sql.PreparedStatement;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCodigoBarras;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasCorteIngles.modelo.MapeoEtiquetasCorteIngles;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEtiquetasCorteInglesServer {
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private Connection con = null;
//	private Statement cs = null;
	private static consultaEtiquetasCorteInglesServer instance;

	public consultaEtiquetasCorteInglesServer(String r_usuario) 
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasCorteInglesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEtiquetasCorteInglesServer(r_usuario);
		}
		return instance;
	}

	public String generarInforme(String r_art) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_art);
		libImpresion.setArchivoDefinitivo("/etiqueta" + r_art +  RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("etiquetasCorteIngles.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasCorteIngles");
		libImpresion.setBackGround("");

		resultadoGeneracion = libImpresion.generacionInformeTxt();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}

	public boolean generarDatosEtiquetas(MapeoEtiquetasCorteIngles r_mapeo) {
		String datosGenerados = null;

		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		MapeoEtiquetasCorteIngles mapeoEtiquetas = new MapeoEtiquetasCorteIngles();

		String codigo = this.recuperarEan(r_mapeo.getArticulo(), "84");
		
		if (codigo==null) 
		{
			codigo = this.recuperarEan(r_mapeo.getArticulo(), "89");
		}
		
		if (codigo!=null)
		{
			codigo = "1" + codigo.substring(0, 12);
	//		Integer contador = this.recuperarContador();
			
	//		emp_ean = "08412423120" + RutinasNumericas.formatearIntegerDigitos(contador,6);
	//		emp_ean = RutinasCodigoBarras.calcula_ean(emp_ean);
			
	
			mapeoEtiquetas.setArticulo(r_mapeo.getArticulo());
			mapeoEtiquetas.setDescripcion(r_mapeo.getDescripcion());
			mapeoEtiquetas.setBultos(r_mapeo.getBultos());
			mapeoEtiquetas.setBultosTotal(r_mapeo.getBultosTotal());
			
			mapeoEtiquetas.setCode(RutinasCodigoBarras.calcula_codigo(codigo));
			mapeoEtiquetas.setSscc(r_mapeo.getSscc());
	
			try {
				/*
				 * Borro datos anteriores si los hubiera
				 */
				this.eliminar(r_mapeo.getArticulo());
				/*
				 * Genero los datos para la impresion
				 */
				datosGenerados = this.guardarNuevo(mapeoEtiquetas);
	
				if (datosGenerados == null)
				{
//					this.actualizarContador(contador);
					return true;
				}
				else
					return false;
			} catch (Exception ex) {
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}

	public String guardarNuevo(MapeoEtiquetasCorteIngles r_mapeo) {
		PreparedStatement preparedStatement = null;

		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO prd_etiquetas_corteingles ( ");
			cadenaSQL.append(" prd_etiquetas_corteingles.articulo ,");
			cadenaSQL.append(" prd_etiquetas_corteingles.descripcion ,");
			cadenaSQL.append(" prd_etiquetas_corteingles.sscc ,");
			cadenaSQL.append(" prd_etiquetas_corteingles.code ,");
			cadenaSQL.append(" prd_etiquetas_corteingles.bulto ,");
			cadenaSQL.append(" prd_etiquetas_corteingles.bultosTotal) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?) ");

			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());

			if (r_mapeo.getArticulo() != null) {
				preparedStatement.setString(1, r_mapeo.getArticulo());
			} else {
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getDescripcion() != null) {
				preparedStatement.setString(2, r_mapeo.getDescripcion());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(3, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getCode() != null) {
				preparedStatement.setString(4, r_mapeo.getCode());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getBultos() != null) {
				preparedStatement.setInt(5, r_mapeo.getBultos());
			} else {
				preparedStatement.setInt(5, 0);
			}
			if (r_mapeo.getBultosTotal() != null) {
				preparedStatement.setInt(6, r_mapeo.getBultosTotal());
			} else {
				preparedStatement.setInt(6, 0);
			}

			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		return null;
	}

	public void eliminar(String r_art) {
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {
			cadenaSQL.append(" DELETE FROM prd_etiquetas_corteingles ");
			cadenaSQL.append(" WHERE prd_etiquetas_corteingles.articulo = ?");
			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_art);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}
	}

	
	public String obtenerDescripcionArticulo(String r_articulo) 
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		
		return rdo;
	}

	private String recuperarEan(String r_articulo, String r_prefijo)
	{
		String ean_obtenido = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		ean_obtenido = cas.obtenerEan(r_articulo, r_prefijo);
		return ean_obtenido;
	}
	
	private Integer recuperarContador()
	{
		Integer contador_obtenido = null;
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		contador_obtenido = ccs.recuperarContador("CorteIngles");
		
		return contador_obtenido + 1 ;
	}
	
	private void actualizarContador(Integer r_contador)
	{
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		ccs.actualizarContador(r_contador, "CorteIngles");
	}
}