package borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.server.consultaInventarioSGAServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaInventarioSGAView extends GridViewRefresh {

	public static final String VIEW_NAME = "Inventario Actual SGA";
	private final String titulo = "Inventario Actual SGA";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean generado = false;
	public TextField articulo=null;
	public TextField lpn=null;
	public TextField lote=null;
	public TextField ubicacion=null;
	public Combo cmbPrinter = null;
	public HashMap<String , String> opcionesEscogidas;
	
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaInventarioSGAView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	opcionesEscogidas = new HashMap<String , String>();
    	
    	this.opcImprimir.setVisible(true);
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.articulo = new TextField("Articulo");
		this.articulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.lpn= new TextField("Lpn");
		this.lpn.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.lote= new TextField("Lote");
		this.lote.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.ubicacion= new TextField("Ubicacion");
		this.ubicacion.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.cmbPrinter = new Combo();
		this.cmbPrinter.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cabLayout.addComponent(this.articulo);
		this.cabLayout.addComponent(this.lote);
		this.cabLayout.addComponent(this.lpn);
		this.cabLayout.addComponent(this.ubicacion);
		this.cabLayout.addComponent(this.cmbPrinter);
		this.cabLayout.setComponentAlignment(this.cmbPrinter, Alignment.BOTTOM_LEFT);
		
		this.cargarCombo();
		this.cargarListeners();
		
//		opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
//		opcionesEscogidas.put("lote", this.lote.getValue().toString());
//		opcionesEscogidas.put("lpn", this.lpn.getValue().toString());
//		opcionesEscogidas.put("ubicacion", this.ubicacion.getValue().toString());
//		
		this.generarGrid(null);

//    	this.barAndGridLayout.removeComponent(this.cabLayout);
//    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoInventarioSGA> r_vector=null;
    	consultaInventarioSGAServer cis = new consultaInventarioSGAServer(CurrentUser.get());
    	
    	if (opcionesEscogidas!=null)
    	{
	    	MapeoInventarioSGA mapeo = new MapeoInventarioSGA();
	    	
	    	mapeo.setArticulo(opcionesEscogidas.get("articulo"));
	    	mapeo.setLote(opcionesEscogidas.get("lote"));
	    	mapeo.setLpn(opcionesEscogidas.get("lpn"));
	    	mapeo.setUbicacion(opcionesEscogidas.get("ubicacion"));
	    	
	    	r_vector=cis.datosOpcionesArticuloGlobal(mapeo);
	    	
    	}
    	else
    	{
    		r_vector = new ArrayList<MapeoInventarioSGA>();
    	}
    	grid = new OpcionesGrid(this, r_vector);
    	
    	((OpcionesGrid) grid).asignarTitulo("Inventario SGA");
    	if (((OpcionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else if (((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		generado=false;
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {
		this.articulo.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (articulo.getValue()!=null && articulo.getValue().toString().length()>0) 
					{
						ubicacion.setEnabled(false);
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("articulo", articulo.getValue());
						opcionesEscogidas.put("lote", lote.getValue());
						opcionesEscogidas.put("lpn", lpn.getValue());
						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
					}
					else
					{
						ubicacion.setEnabled(true);
					}
					
					generarGrid(opcionesEscogidas);
				}
			}
		});
		this.lote.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (lote.getValue()!=null && lote.getValue().toString().length()>0) 
					{
						ubicacion.setEnabled(false);
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("articulo", articulo.getValue());
						opcionesEscogidas.put("lote", lote.getValue());
						opcionesEscogidas.put("lpn", lpn.getValue());
						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
					}
					else
					{
						ubicacion.setEnabled(true);
					}
					generarGrid(opcionesEscogidas);
				}
			}
		});

		this.lpn.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (lpn.getValue()!=null && lpn.getValue().toString().length()>0) 
					{
						ubicacion.setEnabled(false);
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("articulo", articulo.getValue());
						opcionesEscogidas.put("lote", lote.getValue());
						opcionesEscogidas.put("lpn", lpn.getValue());
						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
					}
					else
					{
						ubicacion.setEnabled(true);
					}
					generarGrid(opcionesEscogidas);
				}
			}
		});

		this.ubicacion.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (ubicacion.getValue()!=null && ubicacion.getValue().toString().length()>0) 
					{
						lote.setEnabled(false);
						lpn.setEnabled(false);
						articulo.setEnabled(false);
						
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("articulo", articulo.getValue());
						opcionesEscogidas.put("lote", lote.getValue());
						opcionesEscogidas.put("lpn", lpn.getValue());
						opcionesEscogidas.put("ubicacion", ubicacion.getValue());
					}
					else
					{
						lote.setEnabled(true);
						lpn.setEnabled(true);
						articulo.setEnabled(true);
					}
					generarGrid(opcionesEscogidas);
				}
			}
		});
    }
    
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

    
    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(true);
    }
    public void print() {
    	((OpcionesGrid) this.grid).generacionPdf(this.cmbPrinter.getValue().toString(), generado,true);
    	this.regresarDesdeForm();
    	generado=false;
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
