package borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.MapeoInventarioSGA;

public class consultaInventarioSGAServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	Connection con = null;	
	Statement cs = null;
	private static consultaInventarioSGAServer instance;
	
	
	public consultaInventarioSGAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaInventarioSGAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaInventarioSGAServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoInventarioSGA> datosOpcionesArticuloGlobal(MapeoInventarioSGA r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventarioSGA> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
	     	if (r_mapeo!=null)
	     	{
	     		if (r_mapeo.getUbicacion()==null || r_mapeo.getUbicacion().length()==0)
	     		{
		     		cadenaSQL.append(" SELECT DISTINCT T1.FULLNAME AS se_ubi, ");
		     		cadenaSQL.append(" t3.ITEM_NAME as se_art, ");
		     		cadenaSQL.append(" T3.ITEM_DESCRIPTION AS se_des, ");
		     		cadenaSQL.append(" T3.LOT AS se_lot, ");
		     		cadenaSQL.append(" T3.LPN AS se_lpn, ");
		     		cadenaSQL.append(" T3.VALUE AS se_ex ");	    
		     		
		     		cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
		     		cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID");
		     		cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION");
		     		cadenaSQL.append(" WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
	
		     		cadenaWhere = new StringBuffer();
		     		if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
		     		{
						cadenaWhere.append(" and t3.ITEM_NAME = '" + r_mapeo.getArticulo().trim() + "' ");
		     		}
		     		if (r_mapeo.getLpn()!=null && r_mapeo.getLpn().length()>0)
		     		{
						cadenaWhere.append(" and T3.LPN like '%" + r_mapeo.getLpn().trim() + "%' ");
		     		}
		     		if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
		     		{
						cadenaWhere.append(" and T3.LOT like '%" + r_mapeo.getLote().trim() + "%' ");
			     	}
		     		
		     		cadenaSQL.append(cadenaWhere.toString() );
					cadenaSQL.append(" ORDER BY T3.LPN ASC ");
		
	     		}
		     	else
		     	{
		     	     
		     		cadenaSQL.append(" SELECT ITEM_NAME as se_art, ");
		     		cadenaSQL.append(" ITEM_DESCRIPTION AS se_des, ");
		     		cadenaSQL.append(" LOT AS se_lot, ");
		     		cadenaSQL.append(" LOCATION AS se_ubi, ");
		     		cadenaSQL.append(" LPN AS se_lpn, ");
		     		cadenaSQL.append(" VALUE AS se_ex ");	    
		     		
		     		cadenaSQL.append(" FROM DCEREPORT_INVENTORY  ");
	
		     		cadenaWhere = new StringBuffer();
		     		if (r_mapeo.getUbicacion()!=null && r_mapeo.getUbicacion().length()>0)
		     		{
						cadenaWhere.append(" WHERE LOCATION = '" + r_mapeo.getUbicacion().trim() + "' ");
		     		}
		     		cadenaSQL.append(cadenaWhere.toString() );
					cadenaSQL.append(" ORDER BY lpn ASC ");
		     	}
	     		
			    con= this.conManager.establecerConexionSGAInd();
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				vector = new ArrayList<MapeoInventarioSGA>();
				
				while(rsOpcion.next())
				{
					MapeoInventarioSGA mapeoInventario = new MapeoInventarioSGA();
					/*
					 * recojo mapeo operarios
					 */
					mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
					mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
					mapeoInventario.setLote(rsOpcion.getString("se_lot"));
					mapeoInventario.setLpn(rsOpcion.getString("se_lpn"));
					mapeoInventario.setUbicacion(rsOpcion.getString("se_ubi"));
					mapeoInventario.setStock_actual(rsOpcion.getInt("se_ex"));
//					if (rsOpcion.getString("se_alm")!=null)
//						mapeoInventario.setAlmacen(rsOpcion.getString("se_alm"));
//					else
						mapeoInventario.setAlmacen(null);
					
					vector.add(mapeoInventario);				
				}
	     	}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}

	public ArrayList<MapeoInventarioSGA> consultaUbicacionesArticulosPedidos(ArrayList<String> r_articulos)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventarioSGA> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
			if (r_articulos!=null && r_articulos.size()>=0)
			{
				cadenaSQL.append(" SELECT DISTINCT T1.FULLNAME AS se_ubi, ");
				cadenaSQL.append(" t3.ITEM_NAME as se_art, ");
				cadenaSQL.append(" T3.ITEM_DESCRIPTION AS se_des, ");
				cadenaSQL.append(" T3.LOT AS se_lot, ");
				cadenaSQL.append(" T3.LPN AS se_LPN, ");
				cadenaSQL.append(" T3.VALUE AS se_ex ");	    
				
				cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
				cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID");
				cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION");
				cadenaSQL.append(" WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
				
				cadenaWhere = new StringBuffer();
				cadenaWhere.append(" and t3.ITEM_NAME in (");
				for (int i=0; i< r_articulos.size()-1;i++)
				{
					cadenaWhere.append(" '" + r_articulos.get(i) + "', ");
				}
				cadenaWhere.append(" '" + r_articulos.get(r_articulos.size()-1) + "') ");
				
				cadenaSQL.append(cadenaWhere.toString() );
//				cadenaSQL.append(" GROUP BY T1.FULLNAME, T3.ITEM_NAME, T3.ITEM_DESCRIPTION, T3.LOT ");
				cadenaSQL.append(" ORDER BY T3.ITEM_NAME ASC ,T1.FULLNAME ASC, T3.LOT ASC ");
				
			}
				
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventarioSGA>();
			
			while(rsOpcion.next())
			{
				MapeoInventarioSGA mapeoInventario = new MapeoInventarioSGA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setLote(rsOpcion.getString("se_lot"));
				mapeoInventario.setUbicacion(rsOpcion.getString("se_ubi"));
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_ex"));
				mapeoInventario.setAlmacen(null);
				
				vector.add(mapeoInventario);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return vector;
	}
	

	public MapeoInventarioSGA consultaUbicacionesArticulos(String r_articulo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
			cadenaSQL.append(" SELECT DISTINCT T1.FULLNAME AS se_ubi, ");
			cadenaSQL.append(" t3.ITEM_NAME as se_art, ");
			cadenaSQL.append(" T3.ITEM_DESCRIPTION AS se_des, ");
			cadenaSQL.append(" T3.LOT AS se_lot, ");
			cadenaSQL.append(" T3.LPN AS se_LPN, ");
			cadenaSQL.append(" T3.VALUE AS se_ex ");	    
			
			cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
			cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID");
			cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION");
			cadenaSQL.append(" WHERE t3.ITEM_NAME = '" + r_articulo + "' ");
			
			cadenaSQL.append(" and T1.FULLNAME not like 'PT.S.99%' ");
		    cadenaSQL.append(" and T1.FULLNAME not like 'PS.OUT%' ");
		    cadenaSQL.append(" and T1.FULLNAME not like 'PS.NO C%' ");
		    cadenaSQL.append(" and T1.FULLNAME not like 'Entradas.%' ");
			cadenaSQL.append(" ORDER BY T3.ITEM_NAME ASC ,T3.LPN ASC ");
			
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoInventarioSGA mapeoInventario = new MapeoInventarioSGA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setLote(rsOpcion.getString("se_lot"));
				mapeoInventario.setUbicacion(rsOpcion.getString("se_ubi"));
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_ex"));
				mapeoInventario.setLpn(rsOpcion.getString("se_LPN"));
				mapeoInventario.setAlmacen(null);

				return mapeoInventario;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return null;
	}

	public MapeoInventarioSGA consultaWipInArticulos(String r_articulo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
			cadenaSQL.append(" SELECT sum(T3.VALUE) AS se_ex ");	    
			
			cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
			cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID");
			cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION");
			cadenaSQL.append(" WHERE t3.ITEM_NAME = '" + r_articulo + "' ");
			
			cadenaSQL.append(" and T1.FULLNAME like 'Entradas.%' ");
			
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoInventarioSGA mapeoInventario = new MapeoInventarioSGA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_ex"));
				
				return mapeoInventario;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return null;
	}

	public MapeoInventarioSGA consultaProduccionOrden(String r_orden)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;
		
		try
		{
			cadenaSQL.append(" SELECT QTYYIELD AS ord_pro ");	    
			
			cadenaSQL.append(" FROM DCEREPORT_PRODUCTIONORDERS  ");
			cadenaSQL.append(" WHERE NAME = '" + r_orden + "' ");
			
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoInventarioSGA mapeoInventario = new MapeoInventarioSGA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setStock_actual(rsOpcion.getInt("ord_pro"));
				
				return mapeoInventario;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return null;
	}
	

	public String generarInforme(ArrayList<MapeoInventarioSGA> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

		if (regenerar)
		{
			this.eliminar();
			
			for (int i=0; i< r_vector.size();i++)
			{
				MapeoInventarioSGA mapeo = (MapeoInventarioSGA) r_vector.get(i);
				this.guardarNuevo(mapeo);	
			}
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/calculoInventarioSGA" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("inventarioSGA.jrxml");
    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado(null);
    	libImpresion.setCarpeta("existencias");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
//		Connection con = null;	
//		Statement cs = null;
//		ResultSet rsOpcion = null;		
//		ArrayList<MapeoInventario> vector = null;
//		StringBuffer cadenaSQL = new StringBuffer();
//		
//		try
//		{
//		
//			cadenaSQL.append(" SELECT a.articulo se_art, ");
//	        cadenaSQL.append(" b.descrip_articulo se_des, ");
//	        cadenaSQL.append(" a.almacen se_alm, ");
//	        cadenaSQL.append(" a.existencias se_exa ");	    
//	        
//	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
//	     	cadenaSQL.append(" where a.articulo = b.articulo ");
//	     	cadenaSQL.append(" and a.existencias < 0 " );
//	     	cadenaSQL.append(" order by 3,1 ");
//
//			con= this.conManager.establecerConexionGestionInd();			
//			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
//			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
//			
//			vector = new ArrayList<MapeoInventario>();
//			
//			while(rsOpcion.next())
//			{
//				return "2";				
//			}
//				
//		}
//		catch (Exception ex)
//		{
//			serNotif.mensajeError(ex.getMessage());			
//			ex.printStackTrace();
//		}
//		finally
//		{
//			try
//			{
//				if (rsOpcion!=null)
//				{
//					rsOpcion.close();
//				}
//				if (cs!=null)
//				{
//					cs.close();
//				}
//				if (con!=null)
//				{
//					con.close();
//				}
//			}
//			catch (Exception ex)
//			{
//				serNotif.mensajeError(ex.getMessage());
//			}
//		}
		return "0";
	}

	public void eliminar()
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;	
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_inventario_sga ");
			con = this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarNuevo(MapeoInventarioSGA r_mapeo)
	{

		PreparedStatement preparedStatement = null;
		Integer codigoInterno = 1;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_inventario_sga( ");
			cadenaSQL.append(" prd_inventario_sga.idprd_inventario_sga,");
			cadenaSQL.append(" prd_inventario_sga.articulo ,");
			cadenaSQL.append(" prd_inventario_sga.descripcion ,");
			cadenaSQL.append(" prd_inventario_sga.stockActual ,");
			cadenaSQL.append(" prd_inventario_sga.lote ,");
			cadenaSQL.append(" prd_inventario_sga.ubicacion ,");
			cadenaSQL.append(" prd_inventario_sga.lpn ,");
			cadenaSQL.append(" prd_inventario_sga.almacen) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, codigoInterno);
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getStock_actual()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getStock_actual());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getUbicacion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getUbicacion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLpn()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getLpn());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getAlmacen()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getAlmacen());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
}