package borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioSGA extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String ubicacion;
	private String lote;
	private String lpn;
	private Integer stock_actual;
	private String almacen;
//	private Double pmc;
//	private Double importe;

	public MapeoInventarioSGA()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public String getLote() {
		return lote;
	}

	public String getLpn() {
		return lpn;
	}

	public Integer getStock_actual() {
		return stock_actual;
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setLpn(String lpn) {
		this.lpn = lpn;
	}

	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

}