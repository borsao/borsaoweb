package borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.server.consultaInventarioSGAServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaConsultaInventarioSGA extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private boolean verArticulo=false;
	private String area = null;
	
	public pantallaConsultaInventarioSGA(String r_titulo, Integer r_pedido)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaInventarioSGAServer cse = new consultaInventarioSGAServer(CurrentUser.get());
		consultaPedidosVentasServer cps = new consultaPedidosVentasServer(CurrentUser.get());
		
		ArrayList<String> arrayArticulos = cps.recuperarArticulosPedido(r_pedido);
		ArrayList<MapeoInventarioSGA> vector = cse.consultaUbicacionesArticulosPedidos(arrayArticulos);
		
		this.llenarRegistros(vector);
		this.cargarListeners();
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoInventarioSGA> r_vector)
	{
		Iterator iterator = null;
		MapeoInventarioSGA mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Lote", String.class, null);
		container.addContainerProperty("Ubicacion", String.class, null);
		container.addContainerProperty("Existencias", Integer.class, null);
		
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoInventarioSGA) iterator.next();
			
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			file.getItemProperty("Lote").setValue(RutinasCadenas.conversion(mapeoAyuda.getLote()));
			file.getItemProperty("Ubicacion").setValue(RutinasCadenas.conversion(mapeoAyuda.getUbicacion()));
			file.getItemProperty("Existencias").setValue(mapeoAyuda.getStock_actual());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.asignarEstilos();
	}
	
    public void cargarListeners()
    {    	
    }

	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Existencias".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-pointer";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}