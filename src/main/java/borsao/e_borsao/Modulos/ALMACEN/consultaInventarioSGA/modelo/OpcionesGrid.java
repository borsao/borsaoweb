package borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.server.VaadinService;
import com.vaadin.data.Item;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.server.consultaInventarioSGAServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view.consultaInventarioSGAView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean conTotales = true;
	private consultaInventarioSGAView app = null;
	
    public OpcionesGrid(consultaInventarioSGAView r_app,  ArrayList<MapeoInventarioSGA> r_vector) 
    {
        this.app = r_app;
        this.vector=r_vector;
        
		this.asignarTitulo("Inventario SGA");
		
    	if (this.vector==null)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("stock_actual");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoInventarioSGA.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		this.calcularTotal();
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("articulo","descripcion","lote", "lpn", "ubicacion", "stock_actual", "almacen");
    }
    
    public void establecerTitulosColumnas()
    {

    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "180");
    	this.widthFiltros.put("lpn", "180");
    	this.widthFiltros.put("lote", "80");
    	this.widthFiltros.put("ubicacion", "80");
    	this.widthFiltros.put("almacen", "180");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_actual").setHeaderCaption("Existencias");
    	this.getColumn("stock_actual").setWidth(new Double(125));
    	this.getColumn("lote").setHeaderCaption("Lote");
    	this.getColumn("lote").setWidth(new Double(120));
    	this.getColumn("lpn").setHeaderCaption("Lpn");
    	this.getColumn("lpn").setWidth(new Double(220));
    	this.getColumn("ubicacion").setHeaderCaption("Añada / Ubic");
    	this.getColumn("ubicacion").setWidth(new Double(120));
    	this.getColumn("almacen").setHeaderCaption("Zona Almacen");
    	this.getColumn("almacen").setWidth(new Double(220));

    	this.getColumn("idCodigo").setHidden(true);
	}
	
    public void cargarListeners()
    {
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ("stock_actual".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    public void generacionPdf(String r_impresora, boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaInventarioSGAServer cisgas = consultaInventarioSGAServer.getInstance(CurrentUser.get());
    	pdfGenerado = cisgas.generarInforme((ArrayList<MapeoInventarioSGA>) this.vector, regenerar);
//    	
    	if (r_impresora.contentEquals("Pantalla"))
    	{
    		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
    	}
    	else
    	{
			String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
			RutinasFicheros.imprimir(archivo, r_impresora);
		    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
		}
		pdfGenerado = null;
    }

	@Override
	public void calcularTotal() 
	{
    	Long totalU = new Long(0) ;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Integer) item.getItemProperty("stock_actual").getValue();
        	totalU += valor.longValue();
        }
        
        footer.getCell("articulo").setText("Totales ");
		footer.getCell("stock_actual").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("stock_actual").setStyleName("Rcell-pie");

	}
}
