package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventarioLote;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo.MapeoArticulosEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaStocksLotes extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private String articuloSeleccionado = null;
	private boolean conLotes = true;
	private boolean soyPalet = false;
	private String fechaConsulta=null;
	
	public pantallaStocksLotes(String r_titulo, String r_articulo, String r_fecha)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		this.articuloSeleccionado=r_articulo;
		this.fechaConsulta = r_fecha;
		
		if (r_articulo.startsWith("01193")) soyPalet=true; else soyPalet=false;
		if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103"))  conLotes=true; else conLotes=false;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaInventarioServer cse = new consultaInventarioServer(CurrentUser.get());
		ArrayList<MapeoInventarioLote> vector = cse.recuperarExistenciasLotes(r_articulo);
		
		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoInventarioLote> r_vector)
	{
		Iterator iterator = null;
		Integer totalExistencias = null;
		Integer totalPalets = null;
		Object newItemId = null;
		Item file =null;
		MapeoInventarioLote mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("btn", String.class, null);
		container.addContainerProperty("Almacen", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		if (conLotes)
		{
			container.addContainerProperty("Lote", String.class, null);
			container.addContainerProperty("Añada", String.class, null);
		}
		container.addContainerProperty("Existencias", Integer.class, null);
		if (conLotes)
		{
			container.addContainerProperty("Palets", Integer.class, null);
		}
		if(soyPalet)
		{
			container.addContainerProperty("Vacios", Integer.class, null);
			container.addContainerProperty("Llenos", Integer.class, null);
		}
		
		totalExistencias=0;
		totalPalets=0;
		
		while (iterator.hasNext())
		{
			newItemId = container.addItem();
			file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoInventarioLote) iterator.next();
			
			file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen().toString());
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			file.getItemProperty("Existencias").setValue(mapeoAyuda.getStock_actual());
			if (conLotes)
			{
				file.getItemProperty("Lote").setValue(mapeoAyuda.getLote().toString());
				file.getItemProperty("Añada").setValue(mapeoAyuda.getUbicacion().toString());
				file.getItemProperty("Palets").setValue(mapeoAyuda.getPalets());
				totalPalets= totalPalets+ mapeoAyuda.getPalets();
			}
			if (soyPalet)
			{
				file.getItemProperty("Vacios").setValue(mapeoAyuda.getStock_actual()-mapeoAyuda.getStock_real());
				file.getItemProperty("Llenos").setValue(mapeoAyuda.getStock_real());
			}
			totalExistencias = totalExistencias + mapeoAyuda.getStock_actual();
		}

		newItemId = container.addItem();
		file = container.getItem(newItemId);
		
		file.getItemProperty("Almacen").setValue("TOTAL");
		file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
		file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
		file.getItemProperty("Existencias").setValue(totalExistencias);
		if (conLotes)
		{
			file.getItemProperty("Lote").setValue("");
			file.getItemProperty("Añada").setValue("");
			file.getItemProperty("Palets").setValue(totalPalets);
		}
		
		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.setStyleName("smallgrid");
		this.gridDatos.getColumn("btn").setHeaderCaption(" ");
		this.gridDatos.getColumn("btn").setWidth(new Double(50));
		this.gridDatos.getColumn("Almacen").setHeaderCaption("Almacen");
		this.gridDatos.getColumn("Almacen").setWidth(new Double(120));
		this.gridDatos.getColumn("Articulo").setHeaderCaption("Articulo");
		this.gridDatos.getColumn("Articulo").setWidth(new Double(140));
		this.gridDatos.getColumn("Descripcion").setHeaderCaption("Descripcion");
		this.gridDatos.getColumn("Descripcion").setWidth(new Double(450));
		this.gridDatos.getColumn("Existencias").setHeaderCaption("Existencias");
		this.gridDatos.getColumn("Existencias").setWidth(new Double(120));
		
//		if (this.articuloSeleccionado.startsWith("0102") || this.articuloSeleccionado.startsWith("0111") || this.articuloSeleccionado.startsWith("0103")) this.gridDatos.getColumn("btn").setHidden(true);
		this.asignarEstilos();
		this.cargarListenersGrid();
	}
	
	private void cargarListenersGrid()
	{
		
		gridDatos.addSelectionListener(new SelectionListener() {
    		
    		@Override
    		public void select(SelectionEvent event) {
    			
    		}
    	});
		
    	this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		String almacen = null;
            		
	            	if (event.getPropertyId().toString().equals("btn"))
	            	{
	            		String ext = null;
	            		String art = null;
	            		Item file = gridDatos.getContainerDataSource().getItem(event.getItemId());
	            		almacen= file.getItemProperty("Almacen").getValue().toString();
		    			
	    				if (articuloSeleccionado.trim().startsWith("0104") || articuloSeleccionado.trim().startsWith("0109") || articuloSeleccionado.trim().startsWith("0108")) ext="pdf";
	        			else ext = "jpg";
	    				
	    				if (!almacen.trim().contentEquals("1") &&  !almacen.trim().contentEquals("4")) art = articuloSeleccionado.trim() + "-" + almacen.trim(); else art = articuloSeleccionado;  
	    				if (articuloSeleccionado.trim().startsWith("0102") || articuloSeleccionado.trim().startsWith("0103") || articuloSeleccionado.trim().startsWith("0111")) art = articuloSeleccionado;
	    					
	    				if (ext.contentEquals("pdf"))
	    				{
	    					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.rutaImagenesIntranet, articuloSeleccionado +"."+ext, false);
	    				}
	    				else
	    				{
	    					RutinasFicheros.abrirImagenGenerado(getUI(), LecturaProperties.rutaImagenesIntranet, art +"."+ext);
	    				}
	    			}
	            		
	            }
    		}
        });
	}
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
			String almacen = null;
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ("Almacen".equals(cellReference.getPropertyId()) ) 
            	{
            		almacen = cellReference.getValue().toString();
            		
            		return "Rcell-normal";
            	}
            	else if ("Existencias".equals(cellReference.getPropertyId()) || "Añada".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ("Lote".equals(cellReference.getPropertyId()))
    			{
            		if (cellReference.getValue().toString().length()>7 && (almacen.equals("1")||almacen.equals("4")))
            		{
	            		String fechaLote = cellReference.getValue().toString().substring(4, 6) + "/" + cellReference.getValue().toString().substring(2,4) + "/" + cellReference.getValue().toString().substring(0,2);
						
	            		if(fechaConsulta!=null)
	            		{
	            			MapeoArticulosEnvasadora mapeoArtEnv = new MapeoArticulosEnvasadora();
	            			mapeoArtEnv.setArticulo(articuloSeleccionado);
	            			
	            			consultaArticulosPTEnvasadoraServer cptes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
	            			HashMap< String, Integer> hashDias = cptes.datosDiasGlobal(mapeoArtEnv);

	            			Integer diasBodega=0;
	            			int dias = RutinasFechas.restarFechas(RutinasFechas.conversionDeString(fechaLote),RutinasFechas.conversionDeString(fechaConsulta));
	            			
	    					if (hashDias!=null && hashDias.get(articuloSeleccionado)!=null)
	    						diasBodega = hashDias.get(articuloSeleccionado );
	    					else
	    						diasBodega = 0;
	    						
	    					if (diasBodega==0 )
	    						return "Rcell-normal";
	    					else
	    						if (dias>=diasBodega) 
	    							return "Rcell-pointer-green";					
	    						else 
	    							return "Rcell-normal";
	            		}
	            		else return "Rcell-normal";
            		}
            		else return "Rcell-normal";
					
    			}
            	else if ("btn".equals(cellReference.getPropertyId())) 
    			{
            		return "cell-nativebuttonImagenVerde";
    			}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}