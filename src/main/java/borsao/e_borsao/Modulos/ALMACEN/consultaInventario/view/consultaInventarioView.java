package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaInventarioView extends GridViewRefresh {

	public static final String VIEW_NAME = "Inventario Actual ";
	private final String titulo = "Inventario Actual ";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean generado = false;
	public ComboBox cmbAlmacen = null;
	public ComboBox cmbTipoProducto = null;
	public TextField mascara=null;
	public HashMap<String , String> opcionesEscogidas;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoInventario> r_vector=null;
    	consultaInventarioServer cis = new consultaInventarioServer(CurrentUser.get());
    	r_vector=cis.datosOpcionesGlobal(opcionesEscogidas.get("almacen"),opcionesEscogidas.get("mascara"),opcionesEscogidas.get("tipo"));
    	
    	grid = new OpcionesGrid(r_vector);
    	
    	((OpcionesGrid) grid).asignarTitulo("Inventario " + cis.obtenerDescripcionAlmacen(opcionesEscogidas.get("almacen")));
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaInventarioView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	opcionesEscogidas = new HashMap<String , String>();
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.cmbAlmacen= new ComboBox("Almacen");    		
		this.cmbAlmacen.setNewItemsAllowed(false);
		this.cmbAlmacen.setNullSelectionAllowed(false);
		this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.mascara = new TextField("Mascara Articulos");
		this.mascara.addStyleName(ValoTheme.TEXTFIELD_TINY);

		this.cmbTipoProducto= new ComboBox("Tipo Producto");    		
		this.cmbTipoProducto.setNewItemsAllowed(false);
		this.cmbTipoProducto.setNullSelectionAllowed(false);
		this.cmbTipoProducto.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cabLayout.addComponent(this.cmbAlmacen);
		this.cabLayout.addComponent(this.mascara);
		this.cabLayout.addComponent(this.cmbTipoProducto);
		
		this.cargarCombo();
		this.cargarListeners();
		this.cmbAlmacen.setValue("1");
		this.cmbTipoProducto.setValue("Todos");
		
		opcionesEscogidas.put("almacen", this.cmbAlmacen.getValue().toString());
		opcionesEscogidas.put("mascara", this.mascara.getValue().toString());
		opcionesEscogidas.put("tipo", this.cmbTipoProducto.getValue().toString());
		this.generarGrid(opcionesEscogidas);

//    	this.barAndGridLayout.removeComponent(this.cabLayout);
    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }

    private void cargarListeners()
    {
		this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().length()>0) 
					{
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("almacen", cmbAlmacen.getValue().toString());
						opcionesEscogidas.put("mascara", mascara.getValue());
						opcionesEscogidas.put("tipo", cmbTipoProducto.getValue().toString());
					}
					
					generarGrid(opcionesEscogidas);
				}
			}
		});
		
		this.cmbTipoProducto.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbTipoProducto.getValue()!=null && cmbTipoProducto.getValue().toString().length()>0) 
					{
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("almacen", cmbAlmacen.getValue().toString());
						opcionesEscogidas.put("mascara", mascara.getValue());
						opcionesEscogidas.put("tipo", cmbTipoProducto.getValue().toString());
					}
					
					generarGrid(opcionesEscogidas);
				}
			}
		});
		
		this.mascara.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
//					if (mascara.getValue()!=null && mascara.getValue().length()>0) 
					{
						opcionesEscogidas = new HashMap<String , String>();
						opcionesEscogidas.put("almacen", cmbAlmacen.getValue().toString());
						opcionesEscogidas.put("mascara", mascara.getValue());
						opcionesEscogidas.put("tipo", cmbTipoProducto.getValue().toString());
					}
					
					generarGrid(opcionesEscogidas);
				}
			}
		}); 

    }
    

    private void cargarCombo()
    {
    	consultaInventarioServer cis = new consultaInventarioServer(CurrentUser.get());
    	ArrayList<String> almacenes = cis.obtenerAlmacenes();
    	
    	for (int i = 0 ; i < almacenes.size(); i++)
    	{
    		this.cmbAlmacen.addItem(almacenes.get(i));
    	}
    	
    	this.cmbTipoProducto.addItem("PT");
    	this.cmbTipoProducto.addItem("PS");
    	this.cmbTipoProducto.addItem("MP");
    	this.cmbTipoProducto.addItem("OA");
    	this.cmbTipoProducto.addItem("SP");
    	this.cmbTipoProducto.addItem("MD");
    	this.cmbTipoProducto.addItem("Todos");
    	
    }
    
    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(true);
    }
    public void print() {
    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;

    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
