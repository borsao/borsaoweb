package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasStocksErroneos extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private boolean verArticulo=false;
	private String area = null;
	
	public pantallaLineasStocksErroneos(String r_titulo, String r_area)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		this.area=r_area;
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaInventarioServer cse = new consultaInventarioServer(CurrentUser.get());
		ArrayList<MapeoInventario> vector = cse.recuperarExistencias(this.area);
		
		this.llenarRegistros(vector);
		this.cargarListeners();
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoInventario> r_vector)
	{
		Iterator iterator = null;
		MapeoInventario mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Almacen", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Existencias", Integer.class, null);
		if (this.area=="LotesNeg")
		{
			container.addContainerProperty("Añada", String.class, null);
			container.addContainerProperty("Lote", String.class, null);			
		}
		
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoInventario) iterator.next();
			
			file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen().toString());
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			file.getItemProperty("Existencias").setValue(mapeoAyuda.getStock_actual());
			if (this.area=="LotesNeg")
			{
				file.getItemProperty("Añada").setValue(RutinasCadenas.conversion(mapeoAyuda.getUbicacion()));
				file.getItemProperty("Lote").setValue(mapeoAyuda.getLote());				
			}
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.asignarEstilos();
	}
	
    public void cargarListeners()
    {    	
    	this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {

				if (event.getPropertyId().toString().equals("Existencias") && !area.equals("LotesNeg"))
		    	{
		    		
					Item file = event.getItem();
		    		String articulo = file.getItemProperty("Articulo").getValue().toString().trim();
		    		String descripcion = file.getItemProperty("Descripcion").getValue().toString().trim();
		    		
		    		pantallaStocksLotes vt = new pantallaStocksLotes("Desglose lotes por Articulo " + articulo + " " + descripcion ,articulo,null);
					getUI().addWindow(vt);
		    		
		    	}
            }
        });
    }

	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Existencias".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-pointer";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}