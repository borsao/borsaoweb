package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioLote extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String ubicacion;
	private String lote;
	private Integer stock_actual;
	private Integer stock_real;
	private Integer almacen;
	private Integer palets;
//	private Double pmc;
//	private Double importe;

	public MapeoInventarioLote()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getStock_actual() {
		return stock_actual;
	}

	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}

	public Integer getStock_real() {
		return stock_real;
	}

	public void setStock_real(Integer stock_real) {
		this.stock_real = stock_real;
	}

	public Integer getAlmacen() {
		return almacen;
	}

	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Integer getPalets() {
		return palets;
	}

	public void setPalets(Integer palets) {
		this.palets = palets;
	}
}