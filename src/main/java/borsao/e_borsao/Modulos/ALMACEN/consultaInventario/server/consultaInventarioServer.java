package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoAñadas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventarioLote;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo.MapeoArticulosEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaInventarioServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	Connection con = null;	
	Statement cs = null;
	private static consultaInventarioServer instance;
	
	
	public consultaInventarioServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaInventarioServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaInventarioServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionAlmacen(String r_almacen)
	{
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		return cas.obtenerDescripcionAlmacen(r_almacen);
	}
	
	
	public ArrayList<MapeoInventario> datosOpcionesGlobal(String r_almacen, String r_mascara, String r_tipo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;
		
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        cadenaSQL.append(" c.numero_lote se_lot, ");
	        cadenaSQL.append(" c.ubicacion se_ubi, ");
	        cadenaSQL.append(" a.existencias se_exa, ");
	        cadenaSQL.append(" a.ubicacion ex_ubi, ");
	        cadenaSQL.append(" c.existencias se_exb ");	    
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b, outer a_ubic_0 c ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.articulo = c.articulo ");
	     	cadenaSQL.append(" and a.almacen = c.almacen ");
	     	cadenaSQL.append(" and a.almacen = " + r_almacen );
	     	cadenaSQL.append(" and a.existencias <> 0 " );
	     	cadenaSQL.append(" and c.existencias <> 0 " );
	     	if (r_mascara!=null && r_mascara.length()>0) cadenaSQL.append(" and a.articulo matches '" + r_mascara + "*' "  );
	     	if (r_tipo!=null && r_tipo.length()>0 && !r_tipo.contentEquals("Todos")) cadenaSQL.append(" and b.tipo_articulo = '" + r_tipo + "' "  );
	     	cadenaSQL.append(" order by 1,5,4 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				MapeoInventario mapeoInventario = new MapeoInventario();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setLote(rsOpcion.getString("se_lot"));
				
				if ((rsOpcion.getString("se_art").startsWith("0106") || rsOpcion.getString("se_art").startsWith("0107")) )
				{
					mapeoInventario.setUbicacion(this.obtenerUbicacionBiblia(rsOpcion.getString("se_art")));
				}
				else if (rsOpcion.getString("se_art").startsWith("0109") || rsOpcion.getString("se_art").startsWith("0108") || rsOpcion.getString("se_art").startsWith("0105"))
				{
					mapeoInventario.setUbicacion(rsOpcion.getString("ex_ubi"));
				}
				else
				{
					mapeoInventario.setUbicacion(rsOpcion.getString("se_ubi"));
				}
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoInventario.setStock_ubi(rsOpcion.getInt("se_exb"));
				mapeoInventario.setAlmacen(rsOpcion.getInt("se_alm"));
				mapeoInventario.setStock_real(0);
				
				vector.add(mapeoInventario);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}
	

	public ArrayList<MapeoInventario> recuperarExistencias(String r_area)
	{
		ArrayList<MapeoInventario> vector = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		String tabla = "";
		Connection con = null;	
		Statement cs = null;
		
		try
		{
		
			if (r_area == "LotesNeg") tabla = "a_ubic_0"; else tabla ="a_exis_0";
			
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        
	        
	        if (tabla=="a_ubic_0")
	        {
	        	cadenaSQL.append(" a.ubicacion se_ubi, ");
	        	cadenaSQL.append(" a.numero_lote se_lot, ");
	        }
	        
	        
	        cadenaSQL.append(" a.existencias se_exa ");
	     	cadenaSQL.append(" FROM " + tabla + " a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	
	     	if (r_area.equals("Negativos") || r_area.equals("LotesNeg"))
	     	{
	     		cadenaSQL.append(" and a.existencias < 0 " );
	     	}
	     	else
	     	{
	     		cadenaSQL.append(" and a.existencias <>  (select sum(existencias) from a_ubic_0 " );
	     		cadenaSQL.append(" where a_ubic_0.almacen=a.almacen and a_ubic_0.articulo = a.articulo) " );
	     	}
	     	
	     	cadenaSQL.append(" order by 3,1 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				MapeoInventario mapeoInventario = new MapeoInventario();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoInventario.setAlmacen(rsOpcion.getInt("se_alm"));
				mapeoInventario.setStock_real(0);
				
				if (tabla=="a_ubic_0")
				{
					mapeoInventario.setUbicacion(rsOpcion.getString("se_ubi"));
					mapeoInventario.setLote(rsOpcion.getString("se_lot"));
				}
				
				vector.add(mapeoInventario);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}

		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;				
	}
	
	public ArrayList<MapeoAñadas> recuperarAñadas()
	{
		ArrayList<MapeoAñadas> vector = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.ubicacion se_ubi, ");
	        cadenaSQL.append(" a.fecha_documento se_fec, ");
	        cadenaSQL.append(" sum(a.unidades) se_uds ");	    
	        
	     	cadenaSQL.append(" FROM a_lin_0 a, e_articu b, a_ubic_0 c ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.articulo = c.articulo ");
	     	cadenaSQL.append(" and a.numero_lote = c.numero_lote ");
	     	cadenaSQL.append(" and a.ubicacion = c.ubicacion ");
	     	cadenaSQL.append(" and a.numero_lote = c.numero_lote ");
	     	cadenaSQL.append(" and a.almacen = c.almacen ");
	     	cadenaSQL.append(" and a.clave_tabla = 'B' ");
	     	cadenaSQL.append(" and (a.ubicacion < '2010' or a.ubicacion[1] <> '2') ");
	     	cadenaSQL.append(" and c.existencias <> 0 ");
	     	cadenaSQL.append(" group by 1,2,3,4 ");	     	
	     	cadenaSQL.append(" order by 1 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoAñadas>();
			
			while(rsOpcion.next())
			{
				MapeoAñadas mapeo= new MapeoAñadas();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setArticulo(rsOpcion.getString("se_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeo.setUnidades(rsOpcion.getInt("se_uds"));
				mapeo.setFecha(rsOpcion.getDate("se_fec"));
				mapeo.setUbicacion(rsOpcion.getString("se_ubi"));
				
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return vector;				
	}
	
	public ArrayList<MapeoInventarioLote> recuperarExistenciasLotes(String r_articulo)
	{
		ArrayList<MapeoInventarioLote> vector = null;
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
		
			if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103"))  
			{
				cadenaSQL.append(" SELECT a.articulo se_art, ");
		        cadenaSQL.append(" b.descrip_articulo se_des, ");
		        cadenaSQL.append(" a.almacen se_alm, ");
		        cadenaSQL.append(" a.existencias se_exa, ");	    
		        cadenaSQL.append(" a.ubicacion se_ana, ");
		        cadenaSQL.append(" a.numero_lote se_lot ");
		        
		     	cadenaSQL.append(" FROM a_ubic_0 a, e_articu b ");
		     	cadenaSQL.append(" where a.articulo = b.articulo ");	     	
		     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "'" );
		     	cadenaSQL.append(" and a.existencias <> 0 " );
		     	
		     	cadenaSQL.append(" order by 3,1,6 ");
			}
			else
			{
				cadenaSQL.append(" SELECT a.articulo se_art, ");
		        cadenaSQL.append(" b.descrip_articulo se_des, ");
		        cadenaSQL.append(" a.almacen se_alm, ");
		        cadenaSQL.append(" a.existencias se_exa ");	    
		        
		     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
		     	cadenaSQL.append(" where a.articulo = b.articulo ");	     	
		     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "'" );
		     	cadenaSQL.append(" and a.existencias <> 0 " );
		     	
		     	cadenaSQL.append(" order by 3,1 ");
			}
			
//			if (r_articulo.startsWith("01193"))
//				con= this.conManager.establecerConexionInd();
//			else
				con= this.conManager.establecerConexionGestionInd();
				
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventarioLote>();
			
			while(rsOpcion.next())
			{
				MapeoInventarioLote mapeoInventario = new MapeoInventarioLote();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoInventario.setPalets(rsOpcion.getInt("se_exa")/consultaEscandalloServer.getInstance(CurrentUser.get()).obtenerCantidadPaletEnvasadora(rsOpcion.getString("se_art")).intValue());
				
				if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103")) 
				{
					mapeoInventario.setUbicacion(rsOpcion.getString("se_ana"));
					mapeoInventario.setLote(rsOpcion.getString("se_lot"));
				}
				mapeoInventario.setStock_real(0);
				if (r_articulo.startsWith("01193"))
				{
					/*
					 * Obtengo los padres y llamo a calcular stock pallets llenos
					 */
					ArrayList<String> padres = consultaEscandalloServer.getInstance(CurrentUser.get()).recuperarPadres(r_articulo);
					if (rsOpcion.getInt("se_alm")==1)
						mapeoInventario.setStock_real(this.obtenerSumaExistenciasUbicacion(con, padres, "1,8"));
					else
						mapeoInventario.setStock_real(this.obtenerSumaExistenciasUbicacion(con, padres, "4,6,7"));
				}
				mapeoInventario.setAlmacen(rsOpcion.getInt("se_alm"));
				
				vector.add(mapeoInventario);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}

		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;				
	}
	
	public Double recuperarExistenciasLotesServibles(String r_articulo,String r_almacen, String r_fecha)
	{
		Double existenciasServibles = new Double(0);
		HashMap<String, Integer> hashDias = null;
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Integer diasBodega = 0;
		try
		{
		
			MapeoArticulosEnvasadora mapeoArtEnv = new MapeoArticulosEnvasadora();
			mapeoArtEnv.setArticulo(r_articulo.trim());
			
			consultaArticulosPTEnvasadoraServer cptes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
			hashDias = cptes.datosDiasGlobal(mapeoArtEnv);
			
			if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103"))  
			{
				cadenaSQL.append(" SELECT a.articulo se_art, ");
		        cadenaSQL.append(" b.descrip_articulo se_des, ");
		        cadenaSQL.append(" a.almacen se_alm, ");
		        cadenaSQL.append(" a.existencias se_exa, ");	    
		        cadenaSQL.append(" a.ubicacion se_ana, ");
		        cadenaSQL.append(" a.numero_lote se_lot ");
		        
		     	cadenaSQL.append(" FROM a_ubic_0 a, e_articu b ");
		     	cadenaSQL.append(" where a.articulo = b.articulo ");	     	
		     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "'" );
		     	cadenaSQL.append(" and a.existencias <> 0 " );
		     	if (r_almacen.equals("1")) cadenaSQL.append(" and (a.almacen = '1') ");
		     	if (r_almacen.equals("4")) cadenaSQL.append(" and (a.almacen = '4') ");
		     	if (r_almacen.equals("")) cadenaSQL.append(" and (a.almacen in  ('1','4')) ");
		     	
		     	cadenaSQL.append(" order by 3,1 ");
			}
			else
			{
				cadenaSQL.append(" SELECT a.articulo se_art, ");
		        cadenaSQL.append(" b.descrip_articulo se_des, ");
		        cadenaSQL.append(" a.almacen se_alm, ");
		        cadenaSQL.append(" a.existencias se_exa ");	    
		        
		     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
		     	cadenaSQL.append(" where a.articulo = b.articulo ");	     	
		     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "'" );
		     	cadenaSQL.append(" and a.existencias <> 0 " );
		     	if (r_almacen.equals("1")) cadenaSQL.append(" and (a.almacen = '1') ");
		     	if (r_almacen.equals("4")) cadenaSQL.append(" and (a.almacen = '4') ");
		     	if (r_almacen.equals("")) cadenaSQL.append(" and (a.almacen in  ('1','4')) ");		     	
		     	cadenaSQL.append(" order by 3,1 ");
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				diasBodega=0;
				
				if (r_articulo.startsWith("0102") || r_articulo.startsWith("0111") || r_articulo.startsWith("0103")) 
				{
					String fechaLote = rsOpcion.getString("se_lot").substring(4, 6) + "/" + rsOpcion.getString("se_lot").substring(2,4) + "/" + rsOpcion.getString("se_lot").substring(0,2);
					
					int dias = RutinasFechas.restarFechas(RutinasFechas.conversionDeString(fechaLote),RutinasFechas.conversionDeString(r_fecha));
					if (hashDias!=null && hashDias.get(r_articulo)!=null)
						diasBodega = hashDias.get(r_articulo);
					else
						diasBodega = 0;
						
					if (diasBodega==0 )
						existenciasServibles = existenciasServibles +  rsOpcion.getInt("se_exa");
					else
						if (dias>=diasBodega) 
							existenciasServibles = existenciasServibles +  rsOpcion.getInt("se_exa");					
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}

		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return existenciasServibles;				
	}

	
	public String generarInforme(ArrayList<MapeoInventario> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

		if (regenerar)
		{
			this.eliminar();
			
			for (int i=0; i< r_vector.size();i++)
			{
				MapeoInventario mapeo = (MapeoInventario) r_vector.get(i);
				this.guardarNuevo(mapeo);	
			}
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/calculoInventario" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("inventario.jrxml");
    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado("calculoInventario.jasper");
    	libImpresion.setCarpeta("existencias");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        cadenaSQL.append(" a.existencias se_exa ");	    
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.existencias < 0 " );
	     	cadenaSQL.append(" order by 3,1 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				return "2";				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}

	public String semaforosLotesNegativos()
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        cadenaSQL.append(" a.existencias se_exa ");	    
	        
	     	cadenaSQL.append(" FROM a_ubic_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.existencias < 0 " );
	     	cadenaSQL.append(" order by 3,1 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				return "2";				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}

	public String semaforosLotes()
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String error ="0";
		
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        cadenaSQL.append(" a.existencias se_exa ");	    
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and ((a.articulo between '0102000' and '0103999') " );
	     	cadenaSQL.append(" or (a.articulo between '0111000' and '0111999')) " );
	     	cadenaSQL.append(" and a.existencias <> (select sum(existencias) from a_ubic_0 " );
	     	cadenaSQL.append(" where a_ubic_0.almacen=a.almacen and a_ubic_0.articulo = a.articulo) " );
	     	cadenaSQL.append(" order by 3,1 ");

	     	if (this.con==null) con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				if (!rsOpcion.getString("se_alm").equals("9") && !rsOpcion.getString("se_alm").equals("10"))
				{
					return "2";				
				}
				else
				{
					error="1";							
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return error;
	}

	public String semaforosAñada()
	{
		Connection con = null;	
		Statement cs = null;	
		ResultSet rsOpcion = null;		
		ArrayList<MapeoInventario> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String error ="0";
		
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.ubicacion se_alm, ");
	        cadenaSQL.append(" sum(a.unidades) se_uds ");	    
	        
	     	cadenaSQL.append(" FROM a_lin_0 a, e_articu b, a_ubic_0 c ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.articulo = c.articulo ");
	     	cadenaSQL.append(" and a.numero_lote = c.numero_lote ");
	     	cadenaSQL.append(" and a.ubicacion = c.ubicacion ");
	     	cadenaSQL.append(" and a.numero_lote = c.numero_lote ");
	     	cadenaSQL.append(" and a.almacen = c.almacen ");
	     	cadenaSQL.append(" and a.clave_tabla = 'B' ");
	     	cadenaSQL.append(" and (a.ubicacion < '2010' or a.ubicacion[1] <> '2') ");
	     	cadenaSQL.append(" and c.existencias <> 0 ");
	     	cadenaSQL.append(" group by 1,2,3 ");
	     	cadenaSQL.append(" order by 1 ");

	     	if (this.con==null) con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				return "2";				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return error;
	}

	public void eliminar()
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;	
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_inventario_env ");
			con = this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarNuevo(MapeoInventario r_mapeo)
	{
		
		PreparedStatement preparedStatement = null;
		Integer codigoInterno = 1;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_inventario_env( ");
			cadenaSQL.append(" prd_inventario_env.idprd_inventario_env,");
			cadenaSQL.append(" prd_inventario_env.articulo ,");
			cadenaSQL.append(" prd_inventario_env.descripcion ,");
			cadenaSQL.append(" prd_inventario_env.stockActual ,");
			cadenaSQL.append(" prd_inventario_env.lote ,");
			cadenaSQL.append(" prd_inventario_env.ubicacion ,");
			cadenaSQL.append(" prd_inventario_env.stockLote ,");
			cadenaSQL.append(" prd_inventario_env.almacen) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, codigoInterno);
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getStock_actual()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getStock_actual());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getUbicacion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getUbicacion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getStock_ubi()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getStock_ubi());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getAlmacen()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getAlmacen());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public ArrayList<String> obtenerAlmacenes()
	{
		ArrayList<String> almacenes = null;
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		almacenes = cas.obtenerAlmacenes();
		
		return almacenes;
	}
	
	private String obtenerUbicacionBiblia(String r_articulo)
	{
		ResultSet rsOpcion = null;
		String ubicacion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;
		try
		{
		
			cadenaSQL.append(" SELECT distinct articulo bib_art, ");
	        cadenaSQL.append(" descripcion bib_des, ");
	        cadenaSQL.append(" ubicacion bib_ubi ");
	        
	     	cadenaSQL.append(" FROM bor_etiquetas ");
	     	cadenaSQL.append(" where articulo = '" + r_articulo + "' " );

			con= this.conManager.establecerConexionBiblia();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				ubicacion = rsOpcion.getString("bib_ubi");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return ubicacion;
	}
	
	public ArrayList<MapeoInventario> comprobarStockErroneo(String r_area)
	{
		ArrayList<MapeoInventario> vector = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" a.almacen se_alm, ");
	        cadenaSQL.append(" a.existencias se_exa ");	    
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	
	     	if (r_area.equals("Negativos"))
	     	{
	     		cadenaSQL.append(" and a.existencias < 0 " );
	     	}
	     	else
	     	{
	     		cadenaSQL.append(" and a.existencias <>  (select sum(existencias) from a_ubic_0 " );
	     		cadenaSQL.append(" where a_ubic_0.almacen=a.almacen and a_ubic_0.articulo = a.articulo) " );
	     	}
	     	
	     	cadenaSQL.append(" order by 3,1 ");

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoInventario>();
			
			while(rsOpcion.next())
			{
				MapeoInventario mapeoInventario = new MapeoInventario();
				/*
				 * recojo mapeo operarios
				 */
				mapeoInventario.setArticulo(rsOpcion.getString("se_art"));
				mapeoInventario.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeoInventario.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoInventario.setAlmacen(rsOpcion.getInt("se_alm"));
				mapeoInventario.setStock_real(0);
				
				vector.add(mapeoInventario);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}

		return vector;				
	}
	
	public HashMap<String, Double>  recuperarDatosResumenStock(Integer r_ejercicio,String r_articulo, String r_almacenes)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		Double actual = null;
		Double pendiente_s = null;
		Double pendiente_r = null;
		Double preparado = null;
		
		try
		{
			
			actual = new Double(0);
			pendiente_s = new Double(0);
			pendiente_r = new Double(0);
			preparado = new Double(0);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" SUM( existencias)  actual, ");
			sql.append(" SUM( pdte_servir)  pdte_s, ");
			sql.append(" SUM( pdte_recibir)  pdte_r ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			sql.append(" and a_exis_" + digito + ".almacen in (" + r_almacenes + ") ");
				
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd(); else con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			
			while(rsMovimientos.next())
			{
				actual=rsMovimientos.getDouble("actual");
				pendiente_s=rsMovimientos.getDouble("pdte_s");
				pendiente_r=rsMovimientos.getDouble("pdte_r");
			}


			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" SUM( existencias)  actual ");
			sql.append(" FROM a_exis_" + digito + ", e_articu ");
			sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo = '" + r_articulo + "' ");
			sql.append(" and a_exis_" + digito + ".almacen in (6) ");
				
			rsMovimientos = cs.executeQuery(sql.toString());

			while(rsMovimientos.next())
			{
				preparado=rsMovimientos.getDouble("actual");
			}

			
			hash.put(digito+".stock", actual);
			hash.put(digito+".pdteServir", pendiente_s);
			hash.put(digito+".pdteRecibir", pendiente_r);
			hash.put(digito+".prep", preparado);
			
			consultaArticulosSMPTServer csmpt = new consultaArticulosSMPTServer(CurrentUser.get());
			hash.put("0.minStock", csmpt.mesesMinStock(r_articulo));
			
			consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
			String codJaulon = ces.recuperarComponente(r_articulo+"-1", "0102");

			if (codJaulon!=null && codJaulon.length()>0)
			{
				sql = new StringBuffer();
				
				sql.append(" SELECT  ");
				sql.append(" SUM( existencias) actual ");
				sql.append(" FROM a_exis_" + digito + ", e_articu ");
				sql.append(" where a_exis_" + digito + ".articulo = e_articu.articulo ");
				sql.append(" and e_articu.articulo = '" + codJaulon + "' ");
				sql.append(" and a_exis_" + digito + ".almacen in (4) ");
					
				rsMovimientos = cs.executeQuery(sql.toString());
		
				while(rsMovimientos.next())
				{
					hash.put(digito+".stockJaulon", rsMovimientos.getDouble("actual"));
					break;
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}

	public HashMap<String, Double>  recuperarExistenciasMes(Integer r_ejercicio,String r_tipo, String r_almacenes)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(ejercicioActual-r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT  ");
			sql.append(" SUM( stock_1 ) '1',");
			sql.append(" SUM( stock_2 ) '2',");
			sql.append(" SUM( stock_3 ) '3',");
			sql.append(" SUM( stock_4 ) '4',");
			sql.append(" SUM( stock_5 ) '5',");
			sql.append(" SUM( stock_6 ) '6',");
			sql.append(" SUM( stock_7 ) '7',");
			sql.append(" SUM( stock_8 ) '8',");
			sql.append(" SUM( stock_9 ) '9',");
			sql.append(" SUM( stock_10) '10',");
			sql.append(" SUM( stock_11) '11',");
			sql.append(" SUM( stock_12) '12' ");
			sql.append(" FROM a_exis_" + digito);
			sql.append(" inner join e_articu on a_exis_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" where e_articu.tipo_articulo = '" + r_tipo + "' ");
			sql.append(" and a_exis_" + digito + ".almacen in (" + r_almacenes + ") ");
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			hash = new HashMap<String, Double>();
			int contador = 0;
			while(rsMovimientos.next())
			{
				for (int i=1;i<=12;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(i));
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hash;
	}
	
	public HashMap<String,	Double> recuperarStockArticulo(String r_mascaraArticulo, String r_formato, String r_almacenes)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Double> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			if (r_formato.contentEquals("Litros")) r_formato = "replace(e_articu.marca, ',','.')"; else r_formato="1";	
			sql = new StringBuffer();
			
			sql.append(" SELECT a_exis_0.articulo articulo, ");
			sql.append(" SUM( existencias*" + r_formato + " ) total ");
			  
			sql.append(" FROM a_exis_0, e_articu ");
			sql.append(" where a_exis_0.articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '" + r_mascaraArticulo + "%' ");
			sql.append(" and a_exis_0.almacen in (" + r_almacenes + ") ");
			
			sql.append(" GROUP BY a_exis_0.articulo");

				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				hash.put(rsMovimientos.getString("articulo").trim(), rsMovimientos.getDouble("total"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hash;
	}
	
	public Integer obtenerSumaExistenciasUbicacion(Connection r_con, ArrayList<String> r_vector_padres, String r_almacenes)
	{
		Integer cantidad = null;
		Double calculo = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		String sufijo=null;
		StringBuffer cadenaSQL =null; 
		
		HashMap<String, BigInteger> lotesLogi = new HashMap<String, BigInteger>();
		lotesLogi.put("0102003",new BigInteger("2310181003"));
		lotesLogi.put("0102006",new BigInteger("2310181006"));
		lotesLogi.put("0102454",new BigInteger("2310052454"));
		lotesLogi.put("0102091",new BigInteger("2309122091"));
		lotesLogi.put("0102297",new BigInteger("2304042297"));
		lotesLogi.put("0102340",new BigInteger("2102182340"));
		lotesLogi.put("0102515",null);
		
		lotesLogi.put("0103001",new BigInteger("2310103001"));
		lotesLogi.put("0103004",new BigInteger("2310103004"));
		lotesLogi.put("0103010",new BigInteger("2309303010"));
		
		lotesLogi.put("0103100",new BigInteger("2310113100"));
		lotesLogi.put("0103104",new BigInteger("2310113104"));
		lotesLogi.put("0103110",new BigInteger("2309304110"));
				
		
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		cantidad = 0;
		try
		{
			cs = r_con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);

			for (int i = 0;i<r_vector_padres.size();i++)
			{
				cadenaSQL = new StringBuffer();
				calculo = new Double(0);
			
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				
				cadenaSQL.append(" SELECT sum(existencias) ubi_ex ");		
				cadenaSQL.append(" from a_ubic_0 ");
				cadenaSQL.append(" where almacen in (" + r_almacenes + ") ");
				cadenaSQL.append(" and articulo = '" + r_vector_padres.get(i) + "'");
				
				if (lotesLogi.get(r_vector_padres.get(i))!= null)
				{			
					cadenaSQL.append(" and numero_lote > " + lotesLogi.get(r_vector_padres.get(i)));
				}
				
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
	//			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	
				while(rsOpcion.next())
				{
					if (r_vector_padres.get(i).contentEquals("0103004")|| r_vector_padres.get(i).contentEquals("0103104"))
						sufijo = "28";
					else if (r_vector_padres.get(i).contentEquals("0102006") || r_vector_padres.get(i).contentEquals("0102091") 
							|| r_vector_padres.get(i).contentEquals("0103005") || r_vector_padres.get(i).contentEquals("0103012")
							|| r_vector_padres.get(i).contentEquals("0103105") || r_vector_padres.get(i).contentEquals("0103112"))
						sufijo="38";
					else
						sufijo ="18";
					
						
					String paletizacionArticulo=cas.obtenerCantidadEan(r_con, r_vector_padres.get(i), sufijo);
					
					if (paletizacionArticulo!=null)
					{
						Double paletizacion = new Double(paletizacionArticulo);				
						calculo = calculo + Math.ceil(rsOpcion.getInt("ubi_ex")/paletizacion);
						cantidad = cantidad + calculo.intValue();
					}
					else
						calculo= new Double(0);
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		return cantidad;		
	}
}