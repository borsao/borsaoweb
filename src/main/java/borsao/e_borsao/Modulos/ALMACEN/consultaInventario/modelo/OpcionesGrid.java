package borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public OpcionesGrid(ArrayList<MapeoInventario> r_vector) 
    {
        
        this.vector=r_vector;
        
		this.asignarTitulo("Inventario ");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("stock_actual");
    	this.camposNoFiltrar.add("stock_real");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoInventario.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("articulo","descripcion","stock_actual","lote", "ubicacion", "stock_ubi", "stock_real");
    }
    
    public void establecerTitulosColumnas()
    {

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_actual").setHeaderCaption("Existencias");
    	this.getColumn("stock_actual").setWidth(new Double(125));
    	this.getColumn("stock_ubi").setHeaderCaption("Ex. Lote");
    	this.getColumn("stock_ubi").setWidth(new Double(125));
    	this.getColumn("lote").setHeaderCaption("Lote");
    	this.getColumn("lote").setWidth(new Double(120));
    	this.getColumn("ubicacion").setHeaderCaption("Añada / Ubic");
    	this.getColumn("ubicacion").setWidth(new Double(120));

    	this.getColumn("stock_real").setHeaderCaption("Stock Real");
    	this.getColumn("stock_real").setWidth(new Double(125));
	}
	
    public void cargarListeners()
    {
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ("stock_actual".equals(cellReference.getPropertyId()) || "stock_real".equals(cellReference.getPropertyId()) || "stock_ubi".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
    	pdfGenerado = cis.generarInforme((ArrayList<MapeoInventario>) this.vector, regenerar);
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		
		pdfGenerado = null;

    }

	@Override
	public void calcularTotal() {
		
	}
}
