package borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.modelo.MapeoEtiquetasUbicacion;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.server.consultaEtiquetasUbicacionServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class ImpresionEtiquetasUbicacion extends Window
{
	private EtiquetasUbicacionView origen = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbPrinter = null;
	private ComboBox cmbAlmacen = null;
	private ComboBox cmbFila = null;
	private TextField dsdColumna = null;
	private TextField hstColumna = null;
	private TextField dsdAltura = null;
	private TextField hstAltura = null;
	private TextField dsdHueco= null;
	private TextField hstHueco = null;
	private TextField ubicManual = null;
	
	public ImpresionEtiquetasUbicacion(EtiquetasUbicacionView r_origen)
	{
		this.origen= (EtiquetasUbicacionView) r_origen;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
		HorizontalLayout fila1 = new HorizontalLayout();
		fila1.setWidthUndefined();
		fila1.setSpacing(true);
			this.cmbAlmacen=new ComboBox("Almacen");
			this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);
			this.cmbAlmacen.setInvalidAllowed(false);
			this.cmbAlmacen.setNewItemsAllowed(false);
			this.cmbAlmacen.setNullSelectionAllowed(false);
			this.cmbAlmacen.setRequired(true);
			this.cmbAlmacen.setWidth("120px");

			this.cmbFila=new ComboBox("Fila");
			this.cmbFila.addStyleName(ValoTheme.COMBOBOX_TINY);
			this.cmbFila.setInvalidAllowed(false);
			this.cmbFila.setNewItemsAllowed(false);
			this.cmbFila.setNullSelectionAllowed(false);
			this.cmbFila.setRequired(true);
			this.cmbFila.setWidth("120px");

			fila1.addComponent(cmbAlmacen);
			fila1.addComponent(cmbFila);

		HorizontalLayout fila2 = new HorizontalLayout();
		fila2.setWidthUndefined();
		fila2.setSpacing(true);
		
			this.dsdColumna= new TextField("Desde Columna");
			this.dsdColumna.setWidth("75px");
			this.dsdColumna.addStyleName(ValoTheme.TEXTFIELD_TINY);

			this.hstColumna= new TextField("Hasta Columna");
			this.hstColumna.setWidth("75px");
			this.hstColumna.addStyleName(ValoTheme.TEXTFIELD_TINY);

			fila2.addComponent(dsdColumna);
			fila2.addComponent(hstColumna);

		HorizontalLayout fila3 = new HorizontalLayout();
		fila3.setWidthUndefined();
		fila3.setSpacing(true);
			
			this.dsdAltura= new TextField("Desde Altura");
			this.dsdAltura.setWidth("75px");
			this.dsdAltura.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			this.hstAltura= new TextField("Hasta Altura");
			this.hstAltura.setWidth("75px");
			this.hstAltura.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			fila3.addComponent(dsdAltura);
			fila3.addComponent(hstAltura);

		HorizontalLayout fila4 = new HorizontalLayout();
		fila4.setWidthUndefined();
		fila4.setSpacing(true);
			
			this.dsdHueco= new TextField("Desde Hueco");
			this.dsdHueco.setWidth("75px");
			this.dsdHueco.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			this.hstHueco= new TextField("Hasta Hueco");
			this.hstHueco.setWidth("75px");
			this.hstHueco.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
			fila4.addComponent(dsdHueco);
			fila4.addComponent(hstHueco);

			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
			
				this.ubicManual= new TextField("Ubicacion:");
				this.ubicManual.setWidth("250px");
				this.ubicManual.addStyleName(ValoTheme.TEXTFIELD_TINY);

				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				
				fila5.addComponent(ubicManual);
				fila5.addComponent(cmbPrinter);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Impresion Etiquetas Ubicacion");
		this.center();
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("950px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarComboAlmacen();
		this.cargarComboFila();
		this.habilitarCampos();
		this.cargarListeners();
	}

	private void habilitarCampos()
	{
		this.cmbAlmacen.setEnabled(true);
		this.cmbPrinter.setEnabled(true);
		this.cmbFila.setEnabled(true);
		this.dsdColumna.setEnabled(true);
		this.hstColumna.setEnabled(true);
		this.dsdAltura.setEnabled(true);
		this.hstAltura.setEnabled(true);
		this.dsdHueco.setEnabled(true);
		this.hstHueco.setEnabled(true);
		
		this.cmbFila.setVisible(true);
		this.dsdColumna.setVisible(true);

		this.hstColumna.setVisible(false);
		this.dsdAltura.setVisible(false);
		this.hstAltura.setVisible(false);
		this.dsdHueco.setVisible(false);
		this.hstHueco.setVisible(false);
		this.ubicManual.setVisible(false);

		switch (cmbAlmacen.getValue().toString().toUpperCase())
		{
			case "MP":
			{
				this.hstColumna.setVisible(true);
				this.dsdAltura.setVisible(true);
				this.hstAltura.setVisible(true);
				this.dsdHueco.setVisible(true);
				this.hstHueco.setVisible(true);
				break;						
			}
			case "PS":
			{
				this.hstColumna.setVisible(true);
				break;
			}
			case "PT":
			{
				this.dsdAltura.setVisible(true);
				this.hstAltura.setVisible(true);
				this.hstColumna.setVisible(true);

				break;
			}
			case "":
			{
				dsdColumna.setVisible(false);
				break;
			}
			case "ET":
			{
				dsdColumna.setVisible(true);
				hstColumna.setVisible(true);
				dsdAltura.setVisible(true);
				hstAltura.setVisible(true);
				dsdHueco.setVisible(true);
				hstHueco.setVisible(true);
				this.cmbFila.setEnabled(false);
				break;
			}
			default:
			{
			}
		}
	}
	
	private void cargarListeners()
	{

		cmbFila.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				switch (cmbAlmacen.getValue().toString().toUpperCase())
				{
					case "MP":
					{
						if (cmbFila.getValue()!=null && cmbFila.getValue().toString().contentEquals("Manual"))
						{
							ubicManual.setVisible(true);
							break;
						}
						else
						{
							ubicManual.setVisible(false);
							break;
						}
					}
					case "":
					{
						if (cmbFila.getValue()!=null && cmbFila.getValue().toString().contentEquals("Manual"))
						{
							ubicManual.setVisible(true);
							break;
						}
						else
						{
							ubicManual.setVisible(false);
							break;
						}
					}
					case "PS":
					{
						if (cmbFila.getValue()!=null && !cmbFila.getValue().toString().contentEquals("Manual"))
						{
							hstColumna.setCaption("Hasta Fila");
							dsdAltura.setVisible(false);
							hstAltura.setVisible(false);
							ubicManual.setVisible(false);
							break;
						}
						else
						{
							ubicManual.setVisible(true);
						}
					}
					case "PT":
					{
						if (cmbFila.getValue()!=null && cmbFila.getValue().toString().contentEquals("S"))
						{
							ubicManual.setVisible(false);
							hstColumna.setCaption("Hasta Fila");
							hstColumna.setVisible(true);
							dsdAltura.setVisible(false);
							hstAltura.setVisible(false);
							dsdHueco.setVisible(false);
							hstHueco.setVisible(false);
						}
						else if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("A") || cmbFila.getValue().toString().contentEquals("B") || cmbFila.getValue().toString().contentEquals("D")))
						{
							ubicManual.setVisible(false);
							hstColumna.setCaption("Hasta Fila");
							hstColumna.setVisible(true);
							dsdAltura.setCaption("Desde Altura");
							dsdAltura.setVisible(true);
							hstAltura.setCaption("Hasta Altura");
							hstAltura.setVisible(true);
							dsdHueco.setCaption("Desde Hueco");
							dsdHueco.setVisible(true);
							hstHueco.setCaption("Hasta Hueco");
							hstHueco.setVisible(true);
						}
						else if (cmbFila.getValue()!=null && !cmbFila.getValue().toString().contentEquals("Manual"))
						{
							ubicManual.setVisible(true);
						}
						else
						{
							ubicManual.setVisible(false);
							dsdColumna.setCaption("Desde Fila");
							hstColumna.setCaption("Hasta Fila");
							hstColumna.setVisible(true);
							dsdAltura.setCaption("Desde Altura");
							dsdAltura.setVisible(true);
							hstAltura.setCaption("Hasta Altura");
							hstAltura.setVisible(true);
							dsdHueco.setVisible(false);
							hstHueco.setVisible(false);
						}
						break;
					}
				}
			}
		});
		
		cmbAlmacen.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				cmbFila.setEnabled(true);
				
				switch (cmbAlmacen.getValue().toString().toUpperCase())
				{
					case "MP":
					{
						dsdAltura.setCaption("Desde Altura");
						dsdColumna.setCaption("Desde Columna");
						hstColumna.setCaption("Hasta Columna");
						cmbFila.setCaption("Fila");
						break;						
					}
					case "PS":
					{
						cmbFila.setCaption("Zona");
						dsdColumna.setCaption("Desde Fila");
						hstColumna.setCaption("Hasta Fila");
						break;
					}
					case "PT":
					{
						dsdColumna.setCaption("Desde Fila");
						hstColumna.setCaption("Hasta Fila");
						cmbFila.setCaption("Zona");
						break;
					}
					case "ET":
					{
						cmbFila.setEnabled(false);
						break;
					}
				}
				habilitarCampos();
				cargarComboFila();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				
				switch (cmbAlmacen.getValue().toString().toUpperCase())
				{
					case "MP":
					{
						procesarUbicacionesMateriaPrima();
						break;						
					}
					case "ET":
					{
						procesarUbicacionesMateriaPrima();
						break;						
					}
					case "PS":
					{
						procesarUbicacionesPS();
						break;
					}
					case "PT":
					{
						procesarUbicacionesPT();
						break;
					}
					case "":
					{
						procesarUbicacionesManuales();
						break;
					}
				}
			}
		});

		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				eBorsao.get().mainScreen.menu.verMenu(true);
				origen.destructor();
				close();
			}
		});	
	}
	

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		
		if (this.cmbPrinter.getValue()==null || this.cmbPrinter.getValue().toString().length()==0)
		{
			devolver = false;
			cmbPrinter.focus();
		}
		if (this.cmbFila.getValue()!=null && this.cmbFila.getValue().toString().contentEquals("Manual"))
		{
			if (this.ubicManual.getValue()==null || this.ubicManual.getValue().length()==0)
			{
				devolver = false;
				cmbAlmacen.focus();
			}
		}
		if ((this.cmbFila.getValue()==null || this.cmbFila.getValue().toString().length()==0) && !cmbAlmacen.getValue().toString().toUpperCase().contentEquals("ET"))
		{
			devolver = false;
			cmbFila.focus();
		}
		if (this.dsdColumna.getValue()==null || this.dsdColumna.getValue().toString().length()==0)
		{
			if (this.cmbFila.getValue()!=null && !this.cmbFila.getValue().toString().contentEquals("Manual"))
			{
				devolver = false;
				dsdColumna.focus();
			}
		}

		if (cmbAlmacen.getValue().toString().contentEquals("MP"))
		{
			if (this.hstColumna.getValue()==null || this.hstColumna.getValue().toString().length()==0)
			{
				devolver = false;
				ubicManual.focus();
			}
		}
		else
		{
			/*
			 * Comprueba los campso obligatorios en pantalla 
			 */
			if (this.cmbAlmacen.getValue()==null || this.cmbAlmacen.getValue().toString().length()==0)
			{
				if (this.cmbFila.getValue()!=null && !this.cmbFila.getValue().toString().contentEquals("Manual"))
				{
					devolver = false;
					cmbAlmacen.focus();
				}
			}
			if ((this.cmbFila.getValue()==null || this.cmbFila.getValue().toString().length()==0) && !cmbAlmacen.getValue().toString().toUpperCase().contentEquals("ET"))
			{
				devolver = false;
				cmbFila.focus();
			}
			if (this.dsdColumna.getValue()==null || this.dsdColumna.getValue().toString().length()==0)
			{
				if (this.cmbFila.getValue()!=null && !this.cmbFila.getValue().toString().contentEquals("Manual"))
				{
					devolver = false;
					dsdColumna.focus();
				}
			}
	
			if (cmbAlmacen.getValue().toString().contentEquals("MP"))
			{
				if (this.hstColumna.getValue()==null || this.hstColumna.getValue().toString().length()==0)
				{
					devolver = false;
					hstColumna.focus();
				}
				if (this.dsdAltura.getValue()==null || this.dsdAltura.getValue().toString().length()==0)
				{
					devolver = false;
					dsdAltura.focus();
				}
				if (this.hstAltura.getValue()==null || this.hstAltura.getValue().toString().length()==0)
				{
					devolver = false;
					hstAltura.focus();
				}
				if (this.dsdHueco.getValue()==null || this.dsdHueco.getValue().toString().length()==0)
				{
					devolver = false;
					dsdHueco.focus();
				}
				if (this.hstHueco.getValue()==null || this.hstHueco.getValue().toString().length()==0)
				{
					devolver = false;
					hstHueco.focus();
				}
			}
			else if (cmbAlmacen.getValue().toString().contentEquals("PT") && !cmbFila.getValue().toString().contentEquals("S"))
			{
				if (this.dsdAltura.getValue()==null || this.dsdAltura.getValue().toString().length()==0)
				{
					devolver = false;
					dsdAltura.focus();
				}			
			}
		}
		return devolver;
	}

	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private void cargarComboAlmacen()
	{
		this.cmbAlmacen.removeAllItems();
		this.cmbAlmacen.addItem("MP");
		this.cmbAlmacen.addItem("PS");
		this.cmbAlmacen.addItem("PT");
		this.cmbAlmacen.addItem("ET");
		this.cmbAlmacen.addItem("");
		this.cmbAlmacen.setValue("MP");
	}

	private void cargarComboFila()
	{
		this.cmbFila.removeAllItems();

		if (cmbAlmacen.getValue().toString().contentEquals(""))
		{
			this.cmbFila.addItem("Manual");
		}
		if (cmbAlmacen.getValue().toString().contentEquals("MP"))
		{
			this.cmbFila.addItem("A");
			this.cmbFila.addItem("B");
			this.cmbFila.addItem("C");
			this.cmbFila.addItem("D");
			this.cmbFila.addItem("E");
			this.cmbFila.addItem("F");
			this.cmbFila.addItem("G");
			this.cmbFila.addItem("H");
			this.cmbFila.addItem("Manual");
			this.cmbFila.setValue("A");
		}
		else if (cmbAlmacen.getValue().toString().contentEquals("PS"))
		{
			this.cmbFila.addItem("I");
			this.cmbFila.addItem("N");
			this.cmbFila.addItem("W");
			this.cmbFila.addItem("Z");
			this.cmbFila.addItem("JAULONES");
			this.cmbFila.addItem("Manual");
			this.cmbFila.setValue("Z");
		}
		else if (cmbAlmacen.getValue().toString().contentEquals("PT"))
		{
			this.cmbFila.addItem("A");
			this.cmbFila.addItem("B");
			this.cmbFila.addItem("D");
			this.cmbFila.addItem("C");
			this.cmbFila.addItem("R");
			this.cmbFila.addItem("S");
			this.cmbFila.addItem("Manual");
			this.cmbFila.setValue("R");
		}
	}
	
	private void procesarUbicacionesMateriaPrima()
	{
		HashMap<Integer, MapeoGlobal> hash = null;
		MapeoEtiquetasUbicacion mapeo=null;
		int contador = 0;
		
		if (todoEnOrden())
		{
			
			if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("Manual")))
			{
				procesarUbicacionesManuales();
			}
			else
			{
				hash = new HashMap<Integer, MapeoGlobal>();				
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				Integer dsdHco = new Integer(dsdHueco.getValue().toString().trim())-1;
				Integer hstHco = new Integer(hstHueco.getValue().toString().trim())-1;
				Integer dsdAlt = 0;
				Integer hstAlt = 0;
				
				if (dsdAltura.getValue().contains("A")) dsdAlt = 1; 
				else if (dsdAltura.getValue().contains("B")) dsdAlt = 2; 
				else if (dsdAltura.getValue().contains("C")) dsdAlt = 3;
				else if (dsdAltura.getValue().contains("D")) dsdAlt = 4;
				else dsdAlt=1;
				
				if (hstAltura.getValue().contains("A")) hstAlt = 1; 
				else if (hstAltura.getValue().contains("B")) hstAlt = 2; 
				else if (hstAltura.getValue().contains("C")) hstAlt = 3;
				else if (hstAltura.getValue().contains("D")) hstAlt = 4;
				else hstAlt=1;
				
				if (hstAlt.compareTo(dsdAlt)<0)
				{
					dsdAlt=1;
					if (this.cmbAlmacen.getValue().toString().contentEquals("ET"))
					{
						hstAlt=4;
					}
					else
					{
						hstAlt=3;
					}
				}
				
				mapeo = new MapeoEtiquetasUbicacion();
	
				for (int i=dsdCol;i<=hstCol;i++)
				{
					
					for (int h=dsdAlt;h<=hstAlt;h++)
					{
						for (int j=dsdHco;j<=hstHco;j++)
						{
							mapeo.setAlm(cmbAlmacen.getValue().toString().trim());
							if (cmbFila.getValue()==null ) 
								mapeo.setFil("");
							else
								mapeo.setFil(cmbFila.getValue().toString());
							
							mapeo.setCol(String.valueOf(i+1));
							if ((h)==1)
								mapeo.setAlt("A");
							else if ((h)==2)
								mapeo.setAlt("B");
							else if ((h)==3)
								mapeo.setAlt("C");
							else 
								mapeo.setAlt("A");
							
							mapeo.setHue(String.valueOf(j+1));
							hash.put(contador, mapeo);
							mapeo = new MapeoEtiquetasUbicacion();
							contador++;
						}
					}
				}					
				if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
				{
					String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/ubicacion.prn";
					RutinasFicheros.imprimirCuantasUbicacion("ubicacion", r_archivo, "etiquetas", new Integer(1), hash);
				}
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
		}
	}
	
	private void procesarUbicacionesPS()
	{
		MapeoEtiquetasUbicacion mapeo=null;
		HashMap<Integer, MapeoGlobal> hash = null;
		int contador = 0;

		if (todoEnOrden())
		{
			if (cmbFila.getValue()!=null && cmbFila.getValue().toString().contentEquals("JAULONES"))
			{
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				
				mapeo = new MapeoEtiquetasUbicacion();
				hash = new HashMap<Integer, MapeoGlobal>();
				
				for (int i=dsdCol;i<=hstCol;i++)
				{
					mapeo.setCol(String.valueOf(i+1));
					
					hash.put(contador, mapeo);
					
					mapeo = new MapeoEtiquetasUbicacion();
					contador++;
				}
				if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
				{
					String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/jaulones.prn";
					RutinasFicheros.imprimirCuantasUbicacion("jaulones", r_archivo, "etiquetas", new Integer(2), hash);
				}
			}
			else if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("Manual")))
			{
				procesarUbicacionesManuales();
			}
			else
			{
				
				consultaEtiquetasUbicacionServer ceus = consultaEtiquetasUbicacionServer.getInstance(CurrentUser.get());
				
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				
				mapeo = new MapeoEtiquetasUbicacion();
	
				for (int i=dsdCol;i<=hstCol;i++)
				{
					mapeo.setAlm(cmbAlmacen.getValue().toString().trim());
					mapeo.setFil(cmbFila.getValue().toString());
					mapeo.setCol(String.valueOf(i+1));
				
					String ean = ceus.generarDatosEtiquetas(mapeo);
					String pdfGenerado = ceus.generarInforme(ean);
					
					if(pdfGenerado.length()>0)
					{
						if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA") || cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
						{
							RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
						}
						else
						{
							String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
							RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
						    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
						}
					}
			    	else
			    		Notificaciones.getInstance().mensajeError("Error en la generacion");
					
					mapeo = new MapeoEtiquetasUbicacion();
				}
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
		}
	}
	private void procesarUbicacionesPT()
	{
		MapeoEtiquetasUbicacion mapeo=null;
		HashMap<Integer, MapeoGlobal> hash = null;
		int contador = 0;
		Integer dsdAlt = 0;
		Integer hstAlt = 0;
		
		if (todoEnOrden())
		{
			if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("C") || cmbFila.getValue().toString().contentEquals("R")))
			{
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				
				if (dsdAltura.getValue().contains("A")) dsdAlt = 1; 
				else if (dsdAltura.getValue().contains("B")) dsdAlt = 2; 
				else if (dsdAltura.getValue().contains("C")) dsdAlt = 3;
				else dsdAlt=1;
				
				if (hstAltura.getValue().contains("A")) hstAlt = 1; 
				else if (hstAltura.getValue().contains("B")) hstAlt = 2; 
				else if (hstAltura.getValue().contains("C")) hstAlt = 3;
				else hstAlt=1;

				hash = new HashMap<Integer, MapeoGlobal>();
				
				for (int i=dsdCol;i<=hstCol;i++)
				{
					for (int h=dsdAlt;h<=hstAlt;h++)
					{
						mapeo = new MapeoEtiquetasUbicacion();
						mapeo.setAlm(cmbAlmacen.getValue().toString());
						mapeo.setFil(cmbFila.getValue().toString());
						mapeo.setCol(String.valueOf(i+1));
						if ((h)==1)
							mapeo.setAlt("A");
						else if ((h)==2)
							mapeo.setAlt("B");
						else if ((h)==3)
							mapeo.setAlt("C");
						else 
							mapeo.setAlt("A");
						
						hash.put(contador, mapeo);
						
//						mapeo = new MapeoEtiquetasUbicacion();
						contador++;
					}
				}
				if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
				{
					String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/ubicacion.prn";
					int cuantas = 1; 
					if (cmbFila.getValue()!=null && cmbFila.getValue().toString().contentEquals("R")) cuantas=2;					
					RutinasFicheros.imprimirCuantasUbicacion("ubicacion", r_archivo, "etiquetas", cuantas, hash);
				}
			}
			else if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("A") || cmbFila.getValue().toString().contentEquals("B") || cmbFila.getValue().toString().contentEquals("D")))
			{
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				Integer dsdHco = new Integer(dsdHueco.getValue().toString().trim())-1;
				Integer hstHco = new Integer(hstHueco.getValue().toString().trim())-1;

				if (dsdAltura.getValue().contains("A")) dsdAlt = 1; 
				else if (dsdAltura.getValue().contains("B")) dsdAlt = 2; 
				else if (dsdAltura.getValue().contains("C")) dsdAlt = 3;
				else dsdAlt=1;
				
				if (hstAltura.getValue().contains("A")) hstAlt = 1; 
				else if (hstAltura.getValue().contains("B")) hstAlt = 2; 
				else if (hstAltura.getValue().contains("C")) hstAlt = 3;
				else hstAlt=1;

				hash = new HashMap<Integer, MapeoGlobal>();
				mapeo = new MapeoEtiquetasUbicacion();
				
				for (int i=dsdCol;i<=hstCol;i++)
				{
					for (int h=dsdAlt;h<=hstAlt;h++)
					{
						for (int j=dsdHco;j<=hstHco;j++)
						{
							mapeo.setAlm(cmbAlmacen.getValue().toString());
							mapeo.setFil(cmbFila.getValue().toString());
							mapeo.setCol(String.valueOf(i+1));
							if ((h)==1)
								mapeo.setAlt("A");
							else if ((h)==2)
								mapeo.setAlt("B");
							else if ((h)==3)
								mapeo.setAlt("C");
							else 
								mapeo.setAlt("A");
							
							mapeo.setHue(String.valueOf(j+1));	
							hash.put(contador, mapeo);
							
							mapeo = new MapeoEtiquetasUbicacion();
							contador++;
						}
					}
				}
				if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
				{
					String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/ubicacion.prn";
					RutinasFicheros.imprimirCuantasUbicacion("ubicacion", r_archivo, "etiquetas", new Integer(1), hash);
				}
			}
			else if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("S")))
			{
				
				consultaEtiquetasUbicacionServer ceus = consultaEtiquetasUbicacionServer.getInstance(CurrentUser.get());
				
				Integer dsdCol = new Integer(dsdColumna.getValue().toString().trim())-1;
				Integer hstCol = new Integer(hstColumna.getValue().toString().trim())-1;
				
				mapeo = new MapeoEtiquetasUbicacion();
				
				for (int i=dsdCol;i<=hstCol;i++)
				{
					mapeo.setAlm(cmbAlmacen.getValue().toString().trim());
					mapeo.setFil(cmbFila.getValue().toString());
					mapeo.setCol(String.valueOf(i+1));
					
					String ean = ceus.generarDatosEtiquetas(mapeo);
					String pdfGenerado = ceus.generarInforme(ean);
					
					if(pdfGenerado.length()>0)
					{
						if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA") || cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
						{
							RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
						}
						else
						{
							String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
							RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
							RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
						}
					}
					else
						Notificaciones.getInstance().mensajeError("Error en la generacion");
					
					mapeo = new MapeoEtiquetasUbicacion();
				}
			}
			else if (cmbFila.getValue()!=null && (cmbFila.getValue().toString().contentEquals("Manual")))
			{
				procesarUbicacionesManuales();
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
		}
	}
	private void procesarUbicacionesManuales()
	{
		MapeoEtiquetasUbicacion mapeo=null;
		HashMap<Integer, MapeoGlobal> hash = null;
		
		if (todoEnOrden())
		{
			consultaEtiquetasUbicacionServer ceus = consultaEtiquetasUbicacionServer.getInstance(CurrentUser.get());
			
			mapeo = new MapeoEtiquetasUbicacion();
			mapeo.setAlm(cmbAlmacen.getValue().toString().trim());
			mapeo.setFil(ubicManual.getValue());
			mapeo.setCol("");
			
			String ean = ceus.generarDatosEtiquetas(mapeo);
		
			hash = new HashMap<Integer, MapeoGlobal>();
			hash.put(0,mapeo);
			
			if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA") )
			{
				String pdfGenerado = ceus.generarInforme(ean);
				RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
			}
			else if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
			{
				String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/manual.prn";
				RutinasFicheros.imprimirCuantasUbicacion("manuales", r_archivo, "etiquetas", new Integer(1), hash);
			}
			else
			{
				String pdfGenerado = ceus.generarInforme(ean);
				String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
				RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
				RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
		}
	}
}