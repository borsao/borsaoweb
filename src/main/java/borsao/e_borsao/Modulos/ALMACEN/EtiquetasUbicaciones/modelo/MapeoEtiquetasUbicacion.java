package borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasUbicacion extends MapeoGlobal
{
	String alm = null;
	String fil = null;
	String col = null;
	String alt = null;
	String hue = null;
	
	public MapeoEtiquetasUbicacion()
	{
		
	}

	public String getAlm() {
		return alm;
	}

	public String getFil() {
		return fil;
	}

	public String getCol() {
		return col;
	}

	public String getAlt() {
		return alt;
	}

	public String getHue() {
		return hue;
	}

	public void setAlm(String alm) {
		this.alm = alm;
	}

	public void setFil(String fil) {
		this.fil = fil;
	}

	public void setCol(String col) {
		this.col = col;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public void setHue(String hue) {
		this.hue = hue;
	}

}