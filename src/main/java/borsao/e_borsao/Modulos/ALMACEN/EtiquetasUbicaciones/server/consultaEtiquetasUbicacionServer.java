package borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.server;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.modelo.MapeoEtiquetasUbicacion;

public class consultaEtiquetasUbicacionServer 
{
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private static consultaEtiquetasUbicacionServer instance;

	public consultaEtiquetasUbicacionServer(String r_usuario) {
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasUbicacionServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaEtiquetasUbicacionServer(r_usuario);
		}
		return instance;
	}

	public String generarDatosEtiquetas(MapeoEtiquetasUbicacion r_mapeo) 
	{
		String emp_ean = null;
		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		if (r_mapeo.getAlm()==null || r_mapeo.getAlm().length()==0)
		{
			emp_ean = "";
		}
		else
		{
			emp_ean = r_mapeo.getAlm().trim() + ".";
		}
		if (r_mapeo.getCol()==null || r_mapeo.getCol().length()==0)
		{
			emp_ean = emp_ean + r_mapeo.getFil();
		}
		else
		{
			emp_ean = emp_ean + r_mapeo.getFil() + "." + r_mapeo.getCol();
		}
		
		return emp_ean;
	}

	public String generarInforme(String r_id) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_id);
		libImpresion.setArchivoDefinitivo("/etiquetaubicacion" + r_id.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("etiquetasUbicacion.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasUbicacion");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeTxt();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}
}