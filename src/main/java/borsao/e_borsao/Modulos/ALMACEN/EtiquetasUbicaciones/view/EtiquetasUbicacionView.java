package borsao.e_borsao.Modulos.ALMACEN.EtiquetasUbicaciones.view;

import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class EtiquetasUbicacionView extends GridViewRefresh {

	public static final String VIEW_NAME = "Etiquetas Ubicaciones";
	private final String titulo = "Etiquetas UBICACIONES";

	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;

	private ImpresionEtiquetasUbicacion vtPeticion = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    }
    
    public void generarGrid()
    {
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public EtiquetasUbicacionView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	setPantallaPeticion(true);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
    	
    	
		vtPeticion = new ImpresionEtiquetasUbicacion(this);
		getUI().addWindow(vtPeticion);

    		
    }

    public void newForm()
    {    	
    }

    public void print()
    {    	
    }

    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    @Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}
    
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
