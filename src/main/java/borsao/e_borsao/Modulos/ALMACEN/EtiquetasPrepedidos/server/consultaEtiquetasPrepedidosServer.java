package borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCodigoBarras;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.modelo.MapeoEtiquetasPrepedidos;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorSGA;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEtiquetasPrepedidosServer {
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private static consultaEtiquetasPrepedidosServer instance;
	public String tabla = "e__vp_c";
	
	
	public consultaEtiquetasPrepedidosServer(String r_usuario) {
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasPrepedidosServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaEtiquetasPrepedidosServer(r_usuario);
		}
		return instance;
	}

	
	public ArrayList<String> recuperarEjerciciosPrepedidos()
	{
		ArrayList<String> strEjercicios = null;
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strEjercicios = new ArrayList<String>();
			
			cadenaSql.append(" select distinct e__vp_c.ejercicio ");
			cadenaSql.append(" from e__vp_c");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				strEjercicios.add(rsPrepedidos.getString("ejercicio"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strEjercicios;
	}

	public ArrayList<String> recuperarDocumentosPrepedidos(String r_ejercicio)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		String campo = null;
		boolean prepedido = true;
		
		StringBuffer cadenaSql = new StringBuffer();
		
		prepedido = tabla.contentEquals("e__vp_c");
		
		if(prepedido)
		{
			campo = "doc_origen";
		}
		else
		{
			campo = "documento";
		}
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct " + tabla + "." + campo + " doc ");
			cadenaSql.append(" from " + tabla);
			cadenaSql.append(" where " + tabla + ".ejercicio = '" + r_ejercicio + "' ");
			if (!prepedido) 
				cadenaSql.append(" and cumplimentacion is null and almacen in (2,4,11) ");
			
			con= this.conManager.establecerConexionGestionInd();
			System.out.println(cadenaSql.toString());
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				strDocumentos.add(rsPrepedidos.getString("doc"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}

	public ArrayList<String> recuperarPedidos(String r_ejercicio, String r_documento)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		String campoDocumento = null;
		String campo = null;
		boolean prepedido = true;
		
		StringBuffer cadenaSql = new StringBuffer();
		
		prepedido = tabla.contentEquals("e__vp_c");
		
		if(prepedido)
		{
			campo = "num_origen";
			campoDocumento = "doc_origen";
		}
		else
		{
			campo = "codigo";
			campoDocumento = "documento";
		}
		
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct " + tabla + "." + campo + " num ");
			cadenaSql.append(" from " + tabla );
			cadenaSql.append(" where " + tabla + ".ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append(" and " + tabla + "." + campoDocumento + " = '" + r_documento+ "' ");
			
			if (prepedido) 
				cadenaSql.append(" and " + tabla + ".estado = 2 ");
			else 
				cadenaSql.append(" and cumplimentacion is null and almacen in ('4','2','11') ");
			
			con= this.conManager.establecerConexionGestionInd();	
			System.out.println(cadenaSql.toString());
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				strDocumentos.add(rsPrepedidos.getString("num"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}

	public boolean comprobarClienteSGA(String r_ejercicio, String r_documento, String r_codigo)
	{
		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		boolean encontrado = false;
		
		encontrado = cps.comprobarClienteSGA(r_ejercicio, r_documento, r_codigo);
		
		return encontrado;
	}
	
	public boolean comprobarArticuloSGA(String r_ejercicio, String r_documento, String r_codigo)
	{
		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		boolean encontrado = false;
		
		encontrado = cps.comprobarArticuloSGA(r_ejercicio, r_documento, r_codigo);
		
		return encontrado;
	}
	public String recuperarNombreCliente(String r_ejercicio, String r_documento, String r_codigo)
	{
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;

		boolean prepedido = true;
		
		StringBuffer cadenaSql = new StringBuffer();
		
		prepedido = tabla.contentEquals("e__vp_c");
		
		try
		{
			cadenaSql.append("select a_vp_c_0.nombre_comercial ");
			if (prepedido)
			{
				cadenaSql.append("from e__vp_c, a_vp_c_0  ");
				cadenaSql.append("where e__vp_c.doc_origen = a_vp_c_0.documento ");
				cadenaSql.append("and e__vp_c.num_origen = a_vp_c_0.codigo " );
				cadenaSql.append("and e__vp_c.ejercicio = '" + r_ejercicio + "' ");
				cadenaSql.append("and e__vp_c.doc_origen = '" + r_documento + "' ");
				cadenaSql.append("and e__vp_c.num_origen = '" + r_codigo + "' ");
				cadenaSql.append("and e__vp_c.estado = 2");
			}
			else
			{
				cadenaSql.append("from a_vp_c_0 ");
				cadenaSql.append("where a_vp_c_0.ejercicio = '" + r_ejercicio + "' ");
				cadenaSql.append("and a_vp_c_0.documento = '" + r_documento + "' ");
				cadenaSql.append("and a_vp_c_0.codigo = '" + r_codigo + "' ");
				cadenaSql.append("and a_vp_c_0.cumplimentacion is null and almacen = 4 ");
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				return rsPrepedidos.getString("nombre_comercial");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public MapeoEtiquetasPrepedidos recuperarPrepedido(Integer r_ejercicio, String r_documento, String r_codigo)
	{
		MapeoEtiquetasPrepedidos mapeo = null;
		ResultSet rsPrepedidos = null;
		Connection con = null;
		Statement cs = null;
		boolean prepedido = true;
		
		
		prepedido = tabla.contentEquals("e__vp_c");
		
		StringBuffer cadenaSql = new StringBuffer();
		
		try
		{
			if (prepedido)
			{
				cadenaSql.append("select e__vp_c.ejercicio, ");
				cadenaSql.append("e__vp_c.doc_origen doc, ");
				cadenaSql.append("e__vp_c.num_origen num, ");
				cadenaSql.append("e__vp_c.sscc sscc, ");
				cadenaSql.append("e__vp_c.tty tty, ");
				cadenaSql.append("a_vp_c_0.nombre_comercial, ");
				cadenaSql.append("a_vp_c_0.ref_cliente ");
				cadenaSql.append("from e__vp_c, a_vp_c_0  ");
				cadenaSql.append("where e__vp_c.doc_origen = a_vp_c_0.documento ");
				cadenaSql.append("and e__vp_c.num_origen = a_vp_c_0.codigo " );
				cadenaSql.append("and e__vp_c.ejercicio = " + r_ejercicio );
				cadenaSql.append("and e__vp_c.doc_origen = '" + r_documento + "' ");
				cadenaSql.append("and e__vp_c.num_origen = '" + r_codigo + "' ");
				cadenaSql.append("and e__vp_c.estado = 2");
			}
			else
			{
				cadenaSql.append("select a_vp_c_0.ejercicio, ");
				cadenaSql.append("a_vp_c_0.documento doc, ");
				cadenaSql.append("a_vp_c_0.codigo num, ");
				cadenaSql.append("0 sscc, ");
				cadenaSql.append("0 tty, ");
				cadenaSql.append("a_vp_c_0.nombre_comercial, ");
				cadenaSql.append("a_vp_c_0.ref_cliente ");
				cadenaSql.append("from a_vp_c_0  ");
				cadenaSql.append("where a_vp_c_0.ejercicio = " + r_ejercicio );
				cadenaSql.append("and a_vp_c_0.documento = '" + r_documento + "' ");
				cadenaSql.append("and a_vp_c_0.codigo = '" + r_codigo + "' ");
				cadenaSql.append("and a_vp_c_0.cumplimentacion is null and almacen in (2,4,11) ");	
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos= cs.executeQuery(cadenaSql.toString());
			while(rsPrepedidos.next())
			{
				mapeo = new MapeoEtiquetasPrepedidos();
				
				mapeo.setEjercicio(new Integer(rsPrepedidos.getInt("ejercicio")));
				mapeo.setDocumento(rsPrepedidos.getString("doc"));
				mapeo.setNumero(rsPrepedidos.getString("num"));
				mapeo.setSscc(rsPrepedidos.getString("sscc"));
				mapeo.setCode(rsPrepedidos.getString("tty"));
				mapeo.setNombre(rsPrepedidos.getString("nombre_comercial"));
				mapeo.setReferencia(rsPrepedidos.getString("ref_cliente"));
				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPrepedidos!=null)
				{
					rsPrepedidos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeo;
	}
	
	public boolean generarDatosEtiquetas(MapeoEtiquetasPrepedidos r_mapeo, boolean r_sga) {
		String datosGenerados = null;
		boolean tocoContador=false;
		Integer contador=null;

		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		MapeoEtiquetasPrepedidos mapeoEtiquetas = new MapeoEtiquetasPrepedidos();

		mapeoEtiquetas.setEjercicio(r_mapeo.getEjercicio());
		mapeoEtiquetas.setDocumento(r_mapeo.getDocumento());
		mapeoEtiquetas.setNumero(r_mapeo.getNumero());
		mapeoEtiquetas.setNombre(r_mapeo.getNombre());
		mapeoEtiquetas.setReferencia(r_mapeo.getReferencia());
		
		if (r_mapeo.getSscc().trim().contentEquals("0"))
		{
			contador = this.recuperarContador();
			tocoContador=true;
			String emp_ean = "08412423120" + RutinasNumericas.formatearIntegerDigitos(contador,6);
			emp_ean = RutinasCodigoBarras.calcula_ean(emp_ean);
			mapeoEtiquetas.setSscc1(emp_ean.trim());
			r_mapeo.setSscc(emp_ean.trim());
		}
		else
		{
			tocoContador=false;
			mapeoEtiquetas.setSscc1(r_mapeo.getSscc().trim());  
		}
		if (r_sga)
		{
			mapeoEtiquetas.setSscc("(00)" + r_mapeo.getSscc().trim() + "(10)999999");  
			mapeoEtiquetas.setCode("00" + r_mapeo.getSscc().trim() + "10999999");
		}
		else
		{
			mapeoEtiquetas.setSscc("(00)" + r_mapeo.getSscc().trim() + "(17)010101");  
			mapeoEtiquetas.setCode("00" + r_mapeo.getSscc().trim() + "17010101");
		}
			
		
		try {
			/*
			 * Borro datos anteriores si los hubiera
			 */
			this.eliminar(r_mapeo.getDocumento(),r_mapeo.getNumero());
			/*
			 * Genero los datos para la impresion
			 */
			datosGenerados = this.guardarNuevo(mapeoEtiquetas);

			if (datosGenerados == null)
			{
				if (tocoContador) this.actualizarContador(contador);
				return true;
			}
			else
				return false;
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}

		return false;
	}

	private void actualizarContador(Integer r_contador)
	{
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		ccs.actualizarContador(r_contador, "Programacion");
	}

	public void eliminar(String r_doc, String r_cod) {
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;

		
		try {
			cadenaSQL.append(" DELETE FROM prd_etiquetas_prepedidos ");
			cadenaSQL.append(" WHERE prd_etiquetas_prepedidos.documento = ?");
			cadenaSQL.append(" and prd_etiquetas_prepedidos.codigo = ?");
			con = this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_doc);
			preparedStatement.setString(2, r_cod);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarNuevo(MapeoEtiquetasPrepedidos r_mapeo) {
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO prd_etiquetas_prepedidos ( ");
			cadenaSQL.append(" prd_etiquetas_prepedidos.ejercicio,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.documento ,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.codigo,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.sscc ,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.code ,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.nombre ,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.referencia ,");
			cadenaSQL.append(" prd_etiquetas_prepedidos.sscc1) VALUES (");
			
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");

			con = this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());

			preparedStatement.setInt(1, r_mapeo.getEjercicio());

			if (r_mapeo.getDocumento() != null) {
				preparedStatement.setString(2, r_mapeo.getDocumento());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getNumero() != null) {
				preparedStatement.setString(3, r_mapeo.getNumero());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(4, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getCode() != null) {
				preparedStatement.setString(5, r_mapeo.getCode());
			} else {
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getNombre() != null) {
				preparedStatement.setString(6, r_mapeo.getNombre());
			} else {
				preparedStatement.setString(6, null);
			}
			if (r_mapeo.getReferencia() != null) {
				preparedStatement.setString(7, r_mapeo.getReferencia());
			} else {
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getSscc1() != null) {
				preparedStatement.setString(8, r_mapeo.getSscc1());
			} else {
				preparedStatement.setString(8, null);
			}

			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String generarInforme(Integer r_id, boolean r_sga) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigo(r_id);
		libImpresion.setArchivoDefinitivo("/etiqueta" + r_id.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		if (r_sga)
			libImpresion.setArchivoPlantilla("etiquetasPrepedidosSGA.jasper");
		else
			libImpresion.setArchivoPlantilla("etiquetasPrepedidos.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasPrepedidos");
		libImpresion.setBackGround(null);

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}
	
	private Integer recuperarContador()
	{
		Integer contador_obtenido = null;
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		contador_obtenido = ccs.recuperarContador("Programacion");
		
		return contador_obtenido + 1 ;
	}	

	public String actualizarPedido(MapeoEtiquetasPrepedidos r_mapeo,String r_situacion)
	{
		String rdo = null;
		String fechaPrepedido = null;
		PreparedStatement preparedStatement = null;		
		Connection con = null;	
		boolean prepedido = true;
		
		
		StringBuffer cadenaSQL = new StringBuffer();  
//		String tabla = "e__vp_c";
		
		prepedido = tabla.contentEquals("e__vp_c");

		if (prepedido) return rdo;
		else
		{
			if (r_situacion.contentEquals("Ppd")) fechaPrepedido = RutinasFechas.fechaActual(); else fechaPrepedido=""; 
			try
			{
				
				cadenaSQL.append(" UPDATE a_vp_c_0 set ");
				cadenaSQL.append(" a_vp_c_0.finalidad = ?, ");
				cadenaSQL.append(" a_vp_c_0.fpaso_fabric = ? ");
				cadenaSQL.append(" where a_vp_c_0.documento = ? ");
				cadenaSQL.append(" and a_vp_c_0.codigo = ? ");
				
				con= this.conManager.establecerConexionGestionInd();	 
				preparedStatement = con.prepareStatement(cadenaSQL.toString());
				
				
				preparedStatement.setString(1, r_situacion);
				preparedStatement.setString(2, fechaPrepedido);
				preparedStatement.setString(3, r_mapeo.getDocumento());
				preparedStatement.setString(4, r_mapeo.getNumero());
				
				preparedStatement.executeUpdate();
				
				rdo=null;
				servidorSincronizadorSGA ssSGA  = null;
				
				if (r_situacion.contentEquals("Ppd"))
				{
					ssSGA = servidorSincronizadorSGA.getInstance(CurrentUser.get());
					ssSGA.sincronizarSGA("PEDIDOS VENTA",r_mapeo.getNumero(),null, null ,false,true,false);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());	    	
			}
			finally
			{
				try
				{
					if (preparedStatement!=null)
					{
						preparedStatement.close();
						preparedStatement=null;
					}
					if (con!=null)
					{
						con.close();
						con=null;
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
		}
		
		
		return rdo;
	}

}