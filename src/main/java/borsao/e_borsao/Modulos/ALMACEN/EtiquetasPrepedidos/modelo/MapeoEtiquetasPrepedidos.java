package borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasPrepedidos extends MapeoGlobal
{
	private Integer ejercicio;
	private String documento; 
	private String numero;
	private String sscc;
	private String sscc1;
	private String code;
	private String nombre; 
	private String referencia; 
	
	
	public MapeoEtiquetasPrepedidos()
	{
		
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public String getSscc() {
		return sscc;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getReferencia() {
		return referencia;
	}


	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	public String getSscc1() {
		return sscc1;
	}


	public void setSscc1(String sscc1) {
		this.sscc1 = sscc1;
	}


}