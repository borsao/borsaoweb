package borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCodigoBarras;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.modelo.MapeoEtiquetasPrepedidos;
import borsao.e_borsao.Modulos.ALMACEN.EtiquetasPrepedidos.server.consultaEtiquetasPrepedidosServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.view.pantallaConsultaInventarioSGA;
import borsao.e_borsao.Modulos.GENERALES.Ejercicios.server.consultaEjerciciosServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasPrepedidos extends Window
{
	
	public CheckBox chkSGA= null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbEjercicio = null;
	private ComboBox cmbDocumento = null;
	private ComboBox cmbPrinter = null;
	private TextField txtCopias= null;
	private TextField txtLPN= null;
	private CheckBox chkCerrar= null;
	private ComboBox cmbPedido = null;
	private Label nombreCliente = null;
	private Label tituloNombreCliente = null;
	private ArrayList<String> datos = null;
	private Button verStock = null;
	
	private consultaEtiquetasPrepedidosServer ceps = null;
	private EtiquetasPrepedidosView origen = null;
	
	public PeticionEtiquetasPrepedidos(EtiquetasPrepedidosView r_view)
	{
		ArrayList<String> datos = new ArrayList<String>();
		this.origen=r_view;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		ceps = new consultaEtiquetasPrepedidosServer(CurrentUser.get());
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.chkSGA= new CheckBox("Trabajar con SGA");
				this.chkSGA.setValue(true);
				
				if (chkSGA.getValue()) ceps.tabla="a_vp_c_0"; else ceps.tabla="e__vp_c";
				
				this.cmbEjercicio=new ComboBox("Ejercicio");
				
				if (!chkSGA.getValue()) datos = this.ceps.recuperarEjerciciosPrepedidos(); 
				else
				{
					consultaEjerciciosServer ces = consultaEjerciciosServer.getInstance(CurrentUser.get());
					datos = ces.obtenerEjercicios();
				}
				
				Iterator<String> iter = datos.iterator();
				
				while (iter.hasNext())
				{
					String valor = iter.next();
					this.cmbEjercicio.addItem(valor);
				}
				
				this.cmbEjercicio.setInvalidAllowed(false);
				this.cmbEjercicio.setNewItemsAllowed(false);
				this.cmbEjercicio.setNullSelectionAllowed(false);
				this.cmbEjercicio.setRequired(true);
				
				this.cmbEjercicio.setWidth("200px");
				
				this.verStock = new Button("Ver stock SGA");
				this.verStock.addStyleName(ValoTheme.BUTTON_TINY);
				this.verStock.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				this.verStock.setVisible(false);

				fila1.addComponent(cmbEjercicio);
				fila1.addComponent(chkSGA);
				fila1.setComponentAlignment(chkSGA, Alignment.MIDDLE_LEFT);
				fila1.addComponent(verStock);
				fila1.setComponentAlignment(verStock, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbDocumento = new ComboBox("Documentos");
				this.cmbDocumento.setWidth("200px");
				this.cmbDocumento.setRequired(true);
				this.cmbDocumento.setNullSelectionAllowed(false);
				this.cmbDocumento.setInvalidAllowed(false);
				this.cmbDocumento.setNewItemsAllowed(false);

				this.cmbPedido = new ComboBox("Pedidos");
				this.cmbPedido.setWidth("200px");
				this.cmbPedido.setRequired(true);
				this.cmbPedido.setNullSelectionAllowed(false);
				this.cmbPedido.setInvalidAllowed(false);
				this.cmbPedido.setNewItemsAllowed(false);
				
				this.txtLPN = new TextField("Forzar LPN '00000000....'");
				this.txtLPN.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);
				
				
				this.chkCerrar = new CheckBox("Cerrar Prepedido");
				this.chkCerrar.setValue(false);
				
				fila2.addComponent(cmbDocumento);
				fila2.addComponent(cmbPedido);
				fila2.addComponent(txtLPN);
				fila2.setComponentAlignment(txtLPN, Alignment.MIDDLE_LEFT);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.tituloNombreCliente = new Label("CLIENTE: ");
				this.tituloNombreCliente.setWidth("75px");

				this.nombreCliente = new Label();
				this.nombreCliente.setWidth("500px");

				fila3.addComponent(tituloNombreCliente);
				fila3.addComponent(nombreCliente);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");

				this.txtCopias=new TextField("Copias");
				this.txtCopias.setValue("1");

				fila4.addComponent(cmbPrinter);
				fila4.addComponent(txtCopias);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cmbEjercicio.setValue(RutinasFechas.añoActualYYYY());
		this.cmbEjercicio.focus();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				MapeoEtiquetasPrepedidos mapeo = null;
				String situacionPrepedido=null;
				boolean impresionLPNSGA=false;
				int copias = new Integer(txtCopias.getValue().toString()).intValue();
				if (todoEnOrden())
				{
					mapeo=ceps.recuperarPrepedido(new Integer(cmbEjercicio.getValue().toString()), cmbDocumento.getValue().toString(), cmbPedido.getValue().toString());
					
					if (chkCerrar.getValue())
					{
						situacionPrepedido="Ok";
						if (chkSGA.getValue())  actualizarDatosPedido(mapeo, situacionPrepedido);
					}
					else
					{
						situacionPrepedido="Ppd";
						if (txtLPN.getValue()!=null && txtLPN.getValue().length()>0)
						{
							mapeo.setSscc(RutinasCodigoBarras.calcula_ean("00000000000" + RutinasNumericas.formatearIntegerDigitos(new Integer(txtLPN.getValue()),6)));
							impresionLPNSGA=true;
						}
						
						for (int i=0;i<copias;i++)
						{
							boolean generado = ceps.generarDatosEtiquetas(mapeo,chkSGA.getValue());
							if (generado)
							{
								String pdfGenerado = null;
								if (chkSGA.getValue() && !impresionLPNSGA) actualizarDatosPedido(mapeo, situacionPrepedido);
								
								pdfGenerado = ceps.generarInforme(new Integer(mapeo.getNumero()), chkSGA.getValue());
								if(pdfGenerado.length()>0)
								{
									if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
									{
										RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
									}
									else
									{
										String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
										RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
									    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
									}
								}
						    	else
						    		Notificaciones.getInstance().mensajeError("Error en la generacion");
							}
						}
					}					
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
		ValueChangeListener lisEj = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbEjercicio.getValue()!=null && cmbEjercicio.getValue()!="")
				{
					
					cmbDocumento.removeAllItems();
					datos = ceps.recuperarDocumentosPrepedidos(cmbEjercicio.getValue().toString());
					
					Iterator<String> iter = datos.iterator();
					
					while (iter.hasNext())
					{
						String valor = iter.next();
						cmbDocumento.addItem(valor);
					}
					cmbDocumento.setValue("P1");
				}
			}
		};
		
		cmbEjercicio.addValueChangeListener(lisEj);
	
		ValueChangeListener lisDoc = new ValueChangeListener() {
		
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbDocumento.getValue()!=null && cmbDocumento.getValue()!="")
				{
					cmbPedido.removeAllItems();
					
					datos = ceps.recuperarPedidos(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString());
					
					Iterator<String> iter = datos.iterator();
					
					while (iter.hasNext())
					{
						String valor = iter.next();
						cmbPedido.addItem(valor);
					}
				}
			}
		};
	
		cmbDocumento.addValueChangeListener(lisDoc);
		
		ValueChangeListener lisPed = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbPedido.getValue()!=null && cmbPedido.getValue()!="")
				{
					
					String nombre = ceps.recuperarNombreCliente(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString(), cmbPedido.getValue().toString());
					nombreCliente.setCaption(nombre);
					verStock.setVisible(true);
				}
				else
				{
					verStock.setVisible(false);
				}
			}
		};
	
		cmbPedido.addValueChangeListener(lisPed);
		
		
		chkSGA.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				chkCerrar.setVisible(chkSGA.getValue());
				if (chkSGA.getValue())
				{
					ceps.tabla="a_vp_c_0";
				}
				else
				{
					ceps.tabla="e__vp_c";
				}
				
				if (cmbDocumento.getValue()!=null && cmbDocumento.getValue()!="")
				{
					cmbPedido.removeAllItems();
					
					datos = ceps.recuperarPedidos(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString());
					
					Iterator<String> iter = datos.iterator();
					
					while (iter.hasNext())
					{
						String valor = iter.next();
						cmbPedido.addItem(valor);
					}
				}
			}
		});
		
		verStock.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verStock();
			}
		});
	}
	
	private void verStock()
	{
		if (cmbPedido.getValue()!=null)
		{
			pantallaConsultaInventarioSGA vt = new pantallaConsultaInventarioSGA("Stock SGA del Pedido: " + cmbPedido.getValue().toString(), new Integer(cmbPedido.getValue().toString()));
			getUI().addWindow(vt);
		}
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.cmbEjercicio.getValue()==null || this.cmbEjercicio.getValue().toString().length()==0)
		{
			devolver = false;
			cmbEjercicio.focus();
		}
		else if (this.cmbDocumento.getValue()==null || this.cmbDocumento.getValue().toString().length()==0) 
		{
			devolver =false;			
			cmbDocumento.focus();
		}
		else if (this.cmbPedido.getValue()==null || this.cmbPedido.getValue().toString().length()==0) 
		{
			devolver =false;
			cmbPedido.focus();
		}
		else if (!ceps.comprobarClienteSGA(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString(), cmbPedido.getValue().toString()))
		{
			devolver = false;
			Notificaciones.getInstance().mensajeError("No existe el cliente del pedido en el SGA");
		}
		else if (!ceps.comprobarArticuloSGA(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString(), cmbPedido.getValue().toString()))
		{
			devolver = false;
			Notificaciones.getInstance().mensajeError("No existe articulo del pedido en el SGA");
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private void actualizarDatosPedido(MapeoEtiquetasPrepedidos r_mapeo, String r_situacion)
	{
		if (!tieneDatos())
		{
			ceps.actualizarPedido(r_mapeo, r_situacion);
		}
		else
		{
			Notificaciones.getInstance().mensajeInformativo("La tabla tiene datos. Espera que se vacien. (5 minutos)");
		}
	}
	private boolean tieneDatos()
	{
		servidorSincronizadorBBDD ss = null;
		ss =new servidorSincronizadorBBDD();
		try
		{
			return ss.comprobarDestinoConDatos("Objective", "CABECERA_PEDIDOS_VTA");
		}
		catch (Exception ex)
		{
			return false;
		}
		finally
		{
			ss=null;
		}
	}
}