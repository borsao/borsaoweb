package borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaLineasComprasGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public AyudaLineasComprasGrid(ArrayList<MapeoLineasAlbaranesCompras> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoLineasAlbaranesCompras.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "documento", "idCodigoCompra", "fechaDocumento",  "articulo", "descripcion", "unidades", "proveedor", "nombreProveedor", "nif", "alias" , "serie");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("documento", "35");
    	this.widthFiltros.put("idCodigoCompra", "85");
//    	this.widthFiltros.put("nombreAlmacen", "410");
    	this.widthFiltros.put("articulo", "70");
    	this.widthFiltros.put("descripcion", "410");
    	this.widthFiltros.put("almacen", "70");
    	this.widthFiltros.put("proveedor", "70");
    	this.widthFiltros.put("nombreProveedor", "410");
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setSortable(false);
    	this.getColumn("ejercicio").setWidth(new Double(100));

    	this.getColumn("documento").setHeaderCaption("Doc");
    	this.getColumn("documento").setSortable(false);
    	this.getColumn("documento").setWidth(new Double(75));
    	
    	this.getColumn("idCodigoCompra").setHeaderCaption("Numero");
    	this.getColumn("idCodigoCompra").setSortable(true);
    	this.getColumn("idCodigoCompra").setWidth(new Double(125));
    	
    	this.getColumn("fechaDocumento").setHeaderCaption("Fecha");
    	this.getColumn("fechaDocumento").setSortable(true);
    	this.getColumn("fechaDocumento").setWidth(new Double(125));
    	this.getColumn("fechaDocumento").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
//    	this.getColumn("referencia").setHeaderCaption("Referencia");
//    	this.getColumn("referencia").setSortable(false);
//    	this.getColumn("referencia").setWidth(new Double(100));
    	
    	this.getColumn("proveedor").setHeaderCaption("Proveedor");
    	this.getColumn("proveedor").setSortable(false);
    	this.getColumn("proveedor").setWidth(new Double(110));
    	
    	this.getColumn("nombreProveedor").setHeaderCaption("Nombre");
    	this.getColumn("nombreProveedor").setSortable(false);
    	this.getColumn("nombreProveedor").setWidth(450);
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(110));

    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(450);

    	this.getColumn("alias").setHeaderCaption("CAE");
    	this.getColumn("alias").setSortable(false);
    	this.getColumn("alias").setWidth(200);

    	this.getColumn("nif").setHeaderCaption("NIF");
    	this.getColumn("nif").setSortable(false);
    	this.getColumn("nif").setWidth(125);

    	this.getColumn("unidades").setHeaderCaption("Litros");
    	this.getColumn("unidades").setSortable(false);
    	this.getColumn("unidades").setWidth(new Double(110));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("cantidad").setHidden(true);
    	this.getColumn("clave_tabla").setHidden(true);
    	this.getColumn("descuento").setHidden(true);
    	this.getColumn("importe").setHidden(true);
    	this.getColumn("importeLocal").setHidden(true);
    	this.getColumn("linea").setHidden(true);
    	this.getColumn("posicion").setHidden(true);
    	this.getColumn("precio").setHidden(true);
    	this.getColumn("moneda").setHidden(true);
    	this.getColumn("plazoEntrega").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	
//    	this.getColumn("articulo").setHidden(true);
//    	this.getColumn("descripcion").setHidden(true);
//    	this.getColumn("lote").setHidden(true);    	
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("idCodigoCompra".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) ) 
            	{
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fechaDocumento");
		this.camposNoFiltrar.add("unidades");
		this.camposNoFiltrar.add("nif");
		this.camposNoFiltrar.add("alias");
		this.camposNoFiltrar.add("serie");
		this.camposNoFiltrar.add("direccion");
		this.camposNoFiltrar.add("codigoPostal");

//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}


