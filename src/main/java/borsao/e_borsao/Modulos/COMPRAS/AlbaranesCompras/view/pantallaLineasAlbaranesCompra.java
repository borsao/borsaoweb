package borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo.MapeoLineasAlbaranesCompras;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.RecepcionEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasAlbaranesCompra extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private Button btnBotonAVentana = null;
	private Button btnLineasPedidos = null;
	private GridViewRefresh app = null;
	private Button btnBotonMVentana = null;
	
	
	public pantallaLineasAlbaranesCompra(GridViewRefresh r_app, Integer r_ejercicio, String r_titulo, String r_documento, Integer r_albaran, String r_almacen)
	{
		this.app = r_app;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaAlbaranesComprasServer cacs = new consultaAlbaranesComprasServer(CurrentUser.get());
		MapeoLineasAlbaranesCompras mapeo = new MapeoLineasAlbaranesCompras();
		
		mapeo.setDocumento(r_documento);
		mapeo.setIdCodigoCompra(r_albaran);
		mapeo.setAlmacen(r_almacen);
		mapeo.setEjercicio(r_ejercicio);
		
		ArrayList<MapeoLineasAlbaranesCompras> vector = cacs.datosLineasAlbaranesGlobal(mapeo);
		
		this.llenarRegistros(vector,SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonAVentana = new Button("Quitar Albaran");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				liberar();
			}
		});
	
		btnBotonMVentana = new Button(" NO DECLARAR ");
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnBotonMVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				marcar();
			}
		});

		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		
		if ((!(this.app instanceof AsignacionSalidasEMCSView)) && (!(this.app instanceof AsignacionEntradasEMCSView)))
		{
			botonera.addComponent(btnBotonAVentana);	
			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		}
		if (((this.app instanceof AsignacionSalidasEMCSView)) || ((this.app instanceof AsignacionEntradasEMCSView)))
		{
			botonera.addComponent(btnBotonMVentana);	
			botonera.setComponentAlignment(btnBotonMVentana, Alignment.BOTTOM_LEFT);
		}
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);

		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public pantallaLineasAlbaranesCompra(Integer r_ejercicio, String r_articulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Pantalla Lineas Albaran Compras del: " + r_articulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaAlbaranesComprasServer cacs = new consultaAlbaranesComprasServer(CurrentUser.get());
		MapeoLineasAlbaranesCompras mapeo = new MapeoLineasAlbaranesCompras();
		
		mapeo.setArticulo(r_articulo);
		mapeo.setEjercicio(r_ejercicio);
		
		ArrayList<MapeoLineasAlbaranesCompras> vector = cacs.datosLineasAlbaranesGlobal(mapeo);
		
		this.llenarRegistros(vector, SelectionMode.SINGLE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("550");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnLineasPedidos = new Button("Lineas Pedidos");
		btnLineasPedidos.addStyleName(ValoTheme.BUTTON_FRIENDLY); 
		btnLineasPedidos.addStyleName(ValoTheme.BUTTON_TINY); 
		
		btnLineasPedidos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				lineasPedidos();
			}
		});
		
//		btnBotonMVentana = new Button(" NO DECLARAR ");
//		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_DANGER); 
//		btnBotonMVentana.addStyleName(ValoTheme.BUTTON_TINY); 
//		
//		btnBotonMVentana.addClickListener(new ClickListener() {
//			
//			@Override
//			public void buttonClick(ClickEvent event) {
//				marcar();
//			}
//		});
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnLineasPedidos);		
		botonera.addComponent(btnBotonCVentana);		
		
//		if ((!(this.app instanceof AsignacionSalidasEMCSView)) && (!(this.app instanceof AsignacionEntradasEMCSView)))
//		{
//			botonera.addComponent(btnBotonAVentana);	
//			botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
//		}
//		if (((this.app instanceof AsignacionSalidasEMCSView)) || ((this.app instanceof AsignacionEntradasEMCSView)))
//		{
//			botonera.addComponent(btnBotonMVentana);	
//			botonera.setComponentAlignment(btnBotonMVentana, Alignment.BOTTOM_LEFT);
//		}
		botonera.setComponentAlignment(btnLineasPedidos, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_LEFT);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}

	private void llenarRegistros(ArrayList<MapeoLineasAlbaranesCompras> r_vector, SelectionMode r_selection)
	{
		Iterator iterator = null;
		MapeoLineasAlbaranesCompras mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Documento", String.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Proveedor", String.class, null);
		container.addContainerProperty("Nombre", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Unidades", Double.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasAlbaranesCompras) iterator.next();
			
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			
    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreProveedor()));
    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
    		
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoCompra().toString());
    		file.getItemProperty("Proveedor").setValue(mapeoAyuda.getProveedor().toString());
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(r_selection);
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Unidades".equals(cellReference.getPropertyId()) || "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId()) || "Proveedor".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void liberar()
	{
		if (this.app instanceof RecepcionEMCSView)
			((RecepcionEMCSView) this.app).cerrarVentanaBusqueda(null, null, null, null,null,null);
		
		close();
	}
	private void lineasPedidos()
	{
		
		if (this.gridDatos.getSelectedRow()!=null)
		{
			Item file = this.gridDatos.getContainerDataSource().getItem(this.gridDatos.getSelectedRow());
			
			pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(new Integer(file.getItemProperty("Codigo").getValue().toString()),new Integer(file.getItemProperty("Ejercicio").getValue().toString()));
			this.getUI().addWindow(vt);
		}
		else
			Notificaciones.getInstance().mensajeAdvertencia("Debes seleccionar un albarán para consultar sus pedidos" );
	}
	
	private void marcar()
	{
		if (this.app instanceof AsignacionEntradasEMCSView)
			((AsignacionEntradasEMCSView) this.app).cerrarVentanaMarcar();
//		else if (this.app instanceof AsignacionSalidasEMCSView)				
//			((AsignacionSalidasEMCSView) this.app).cerrarVentanaMarcar();
		
		close();
	}
}