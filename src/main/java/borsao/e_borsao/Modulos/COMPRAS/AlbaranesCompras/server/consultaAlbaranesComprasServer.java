package borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo.MapeoLineasAlbaranesCompras;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;

public class consultaAlbaranesComprasServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAlbaranesComprasServer instance;
//	private static int litrosMinimos=consultaMovimientosSilicieServer.litrosMinimos;
	
	
	public consultaAlbaranesComprasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlbaranesComprasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlbaranesComprasServer(r_usuario);			
		}
		return instance;
	}
	
	public String generarInforme(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	

	public boolean actualizarLineasAlbaran(Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		String ejer = "0";
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
    		cadenaSQL.append(" observacion = ? ");
	        cadenaSQL.append(" where clave_tabla = 'E' ");
	        cadenaSQL.append(" and documento = 'E1' ");
	        cadenaSQL.append(" and serie = 'E' ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "IE" );
		    preparedStatement.setInt(2, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	
	public boolean marcarCabeceraAlbaranEMCS(Integer r_ejercicio, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_ce_c_" + ejer + " set ");
    		cadenaSQL.append(" control_entradas = ? ");
	        cadenaSQL.append(" where clave_tabla = 'E' ");
	        cadenaSQL.append(" and documento = 'E1' ");
	        cadenaSQL.append(" and serie = 'E' ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "S" );
		    preparedStatement.setInt(2, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		try
		{
			if (preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if (con!=null)
			{
				con.close();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}

		return true;
	}
	
	public boolean marcarAlbaranSilicie(Integer r_ejercicio, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
    		cadenaSQL.append(" observacion = ? ");
	        cadenaSQL.append(" where clave_tabla = 'E' ");
	        cadenaSQL.append(" and documento = 'E1' ");
	        cadenaSQL.append(" and serie = 'E' ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "ND" );
		    preparedStatement.setInt(2, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		try
		{
			if (preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if (con!=null)
			{
				con.close();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}

		return true;
	}
	
	public boolean liberarCabeceraAlbaranEMCS(Integer r_ejercicio, Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		String ejer = "0";
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_ejercicio);
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_ce_c_" + ejer + " set ");
    		cadenaSQL.append(" control_entradas = ? ");
	        cadenaSQL.append(" where clave_tabla = 'E' ");
	        cadenaSQL.append(" and documento = 'E1' ");
	        cadenaSQL.append(" and serie = 'E' ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "N" );
		    preparedStatement.setInt(2, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		try
		{
			if (preparedStatement!=null)
			{
				preparedStatement.close();
			}
			if (con!=null)
			{
				con.close();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}

		return true;
	}
//	
	public String semaforo()
	{
		String errores = "0";
		return errores;
	}
	
	public String semaforoPlazos()
	{
		String errores = "0";
		return errores;
	}
	
	public ArrayList<MapeoLineasAlbaranesCompras> datosOpcionesGlobalSinEMCS(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Date r_fecha, String r_almacen )
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesCompras> vector = null;
		MapeoLineasAlbaranesCompras mapeo = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio == ejercicioActual) digito="0";
		if (r_ejercicio == ejercicioActual-1) digito="1";
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" a.proveedor det_pro, ");
        cadenaSQL.append(" c.nom_abreviado det_cae, ");
        cadenaSQL.append(" a.nombre_comercial det_nom, ");
        cadenaSQL.append(" a.direccion det_dir, ");
        cadenaSQL.append(" a.cod_postal det_cp, ");
        cadenaSQL.append(" a.cif det_cif, ");
        cadenaSQL.append(" a.clave_tabla det_cl, ");
        cadenaSQL.append(" a.serie det_ser, ");
        cadenaSQL.append(" a.pais det_pai, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_lin_" + digito +" p, e_provee c, a_ce_c_" + digito + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
     	cadenaSQL.append(" AND a.control_entradas = 'N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	cadenaSQL.append(" AND p.articulo matches '0101*' ");
//     	cadenaSQL.append(" and p.unidades >= " + litrosMinimos );
     	
     	if (r_clave_tabla!=null)
     	{
     		cadenaSQL.append(" AND a.clave_tabla = '" + r_clave_tabla + "' ");
     	}
     	if (r_documento!=null)
     	{
     		cadenaSQL.append(" AND a.documento = '" + r_documento + "' ");
     	}
     	if (r_serie!=null)
     	{
     		cadenaSQL.append(" AND a.serie = '" + r_serie + "' ");
     	}
     	if (r_albaran!=null)
     	{
     		cadenaSQL.append(" AND a.codigo = " + r_albaran);
     	}
     	if (r_almacen!=null)
     	{
     		cadenaSQL.append(" AND p.almacen = " + r_almacen);
     	}
     	if (r_fecha!=null)
     	{
     		cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
     	}
     	
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesCompras();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("det_dir")));
				mapeo.setCodigoPostal(rsOpcion.getString("det_cp"));
				mapeo.setClave_tabla(rsOpcion.getString("det_cl"));
				mapeo.setNif(rsOpcion.getString("det_cif"));
				mapeo.setSerie(rsOpcion.getString("det_ser"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setAlias(rsOpcion.getString("det_cae"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNombreProveedor(RutinasCadenas.conversion(rsOpcion.getString("det_nom")));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("det_des")));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	public ArrayList<MapeoLineasAlbaranesCompras> datosLineasAlbaranesGlobal(MapeoLineasAlbaranesCompras r_mapeo)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesCompras> vector = null;
		MapeoLineasAlbaranesCompras mapeo = null;
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		Connection con = null;
		Statement cs = null;

		if (r_mapeo.getEjercicio()!=null)
		{
			if (r_mapeo.getEjercicio().intValue() == ejercicioActual.intValue()) digito="0";
			if (r_mapeo.getEjercicio().intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" c.proveedor det_pro, ");
        cadenaSQL.append(" c.cif det_nif, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.almacen det_alm, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.num_linea det_lin ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, e_provee c, a_ce_c_" + digito + " a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
//     	cadenaSQL.append(" AND a.control_entradas = 'N' ");
     	
		try
		{
			if (r_mapeo!=null)
			{
					
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().length()>0)
				{
					cadenaSQL.append(" and c.cif = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					cadenaSQL.append(" and p.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					cadenaSQL.append(" and p.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra()>0)
				{
					cadenaSQL.append(" and p.codigo = " + r_mapeo.getIdCodigoCompra());
				}
				if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen().length()>0)
				{
					cadenaSQL.append(" and p.almacen = '" + r_mapeo.getAlmacen() + "' ");
				}
				if (r_mapeo.getFechaDocumento()!=null && (RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento())).length()>0)
				{
					cadenaSQL.append(" and p.fecha_documento = '" + RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento()) + "' ");
				}
				
			}
			cadenaSQL.append(" order by 8,2 ");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesCompras();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNif(rsOpcion.getString("det_nif"));
				mapeo.setNombreProveedor(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public boolean chequeoSinEmcs(Integer r_ejercicio, String r_cae_titular, Integer r_mes) 
	{

		ResultSet rsOpcion = null;		
		String rdo = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio >= ejercicioActual) digito="0";
		if (r_ejercicio < ejercicioActual)  digito="1";
		
		cadenaSQL.append(" SELECT distinct a.codigo alb, ");
		cadenaSQL.append(" a.documento doc ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p, a_ce_c_" + digito + " a, e_articu e, e_almac w ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.control_entradas = 'N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	cadenaSQL.append(" AND w.almacen = p.almacen and w.encargado = '" + r_cae_titular + "' ");
     	cadenaSQL.append(" AND p.articulo matches '0101*' ");
//     	cadenaSQL.append(" and p.unidades>= " + litrosMinimos);
     	
		try
		{
			cadenaSQL.append(" AND month(a.fecha_documento) = " + r_mes );
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rdo== null) rdo="";
				rdo = rdo + " " + rsOpcion.getString("doc") + "-" + rsOpcion.getInt("alb");
				Notificaciones.getInstance().mensajeSeguimiento("Albaranes Erroneos " + rsOpcion.getString("doc") + "-" + rsOpcion.getInt("alb"));
			}
			if (rdo!= null) Notificaciones.getInstance().mensajeError("Albaranes Erroneos " + rdo);
			if (rdo!= null) return false;

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}

	public String semaforos() {

		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;

		cadenaSQL.append(" SELECT count(*) cuantos ");
     	cadenaSQL.append(" FROM a_lin_0 p, a_ce_c_0 a, e_articu e ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND e.articulo = p.articulo ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND a.control_entradas = 'N' and (p.observacion is null or ( p.observacion[1] <> 'N' and p.observacion[2] <> 'D')) ");
     	cadenaSQL.append(" AND p.articulo matches '0101*' ");
//     	cadenaSQL.append(" and (p.unidades)>= " + litrosMinimos);
     	
		try
		{
			cadenaSQL.append(" AND a.fecha_documento <= '" + RutinasFechas.restarDiasFecha(RutinasFechas.fechaActual(), 1) + "' ");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return "2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "1";
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return "0";
	}
}
