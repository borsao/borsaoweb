package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoPedidosCompras extends MapeoGlobal
{
    private Integer idCodigoCompra;
	private String clave_tabla;
	private String documento;
	private String serie;
	private String mail;
	private String mail2;
	private String mail3;
	private String nombre;
	private Integer ejercicio;
	private String enviar;
	private Date fecha;
	private String boton = " ";
	private String pdf = " ";
	private String moneda;
	private String enviado = null;
	
	private Integer proveedor;
	private String direccion;
	private String cp;
	private String poblacion;
	private String provincia;
	private String pais;
	private String fax;
	private Date plazoEntrega;
	private String formaPago;

	public MapeoPedidosCompras()
	{
		this.setEnviar("Enviar");
	}


	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}


	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}


	public String getClave_Tabla() {
		return clave_tabla;
	}


	public void setClave_Tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getEnviar() {
		return enviar;
	}


	public void setEnviar(String enviar) {
		this.enviar = enviar;
	}


	public String getFecha() {
		return RutinasFechas.convertirDateToString(fecha);
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
		
	}



	public String getMail2() {
		return mail2;
	}


	public void setMail2(String mail) {
		this.mail2 = mail;
	}


	public String getPdf() {
		return pdf;
	}


	public void setPdf(String pdf) {
		this.pdf = pdf;
	}

	public String getBoton() {
		return boton;
	}


	public void setBoton(String boton) {
		this.boton = boton;
	}


	public Integer getProveedor() {
		return proveedor;
	}


	public void setProveedor(Integer proveedor) {
		this.proveedor = proveedor;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCp() {
		return cp;
	}


	public void setCp(String cp) {
		this.cp = cp;
	}


	public String getPoblacion() {
		return poblacion;
	}


	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}


	public String getProvincia() {
		return provincia;
	}


	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getPlazoEntrega() {
		return RutinasFechas.convertirDateToString(plazoEntrega);
	}


	public void setPlazoEntrega(Date plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}


	public String getFormaPago() {
		return formaPago;
	}


	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getEnviado() {
		return enviado;
	}


	public void setEnviado(String enviado) {
		this.enviado = enviado;
	}


	public String getMail3() {
		return mail3;
	}


	public void setMail3(String mail3) {
		this.mail3 = mail3;
	}



}