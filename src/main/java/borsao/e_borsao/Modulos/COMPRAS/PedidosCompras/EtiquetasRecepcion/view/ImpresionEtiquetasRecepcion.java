package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.modelo.MapeoEtiquetasRecepcion;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.server.consultaEtiquetasRecepcionServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class ImpresionEtiquetasRecepcion extends Window
{
	private PeticionEtiquetasRecepcion App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbPrinter = null;
	private TextField txtArticulo = null;
	private TextField txtDescripcion= null;
	private TextField txtLote= null;
	private TextField txtAñada= null;
	private TextField txtCajas = null;
	private TextField txtCopias = null;
	private TextField txtPalets = null;
	private TextField txtNumPalet = null;
	
	private String idCodigo = null;
	private boolean isEmbotellado = false;
	/*
	 * Valores devueltos
	 */
	private Integer cajas = null;
	
	public ImpresionEtiquetasRecepcion(PeticionEtiquetasRecepcion r_app, String r_articulo, String r_codigo, String r_titulo)
	{
		this.App = (PeticionEtiquetasRecepcion) r_app;
		this.idCodigo = r_codigo;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setValue(r_articulo);
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setRequired(true);
				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setValue(recogerArticulo(r_articulo));
				this.txtDescripcion.setWidth("400px");
				this.txtDescripcion.setRequired(true);
				fila1.addComponent(txtArticulo);
				fila1.addComponent(txtDescripcion);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.txtPalets=new TextField("Numero de palets: ");				
				this.txtPalets.setWidth("150px");
				this.txtPalets.setRequired(true);
				
				this.txtCajas=new TextField("Cantidad por palet ");				
				this.txtCajas.setWidth("150px");
				this.txtCajas.setRequired(true);

				this.txtLote=new TextField("Lote");
				this.txtLote.setWidth("200px");
				this.txtLote.setRequired(true);

				this.txtAñada=new TextField("Añada");
				this.txtAñada.setWidth("200px");
				this.txtAñada.setVisible(r_articulo.startsWith("0102") || r_articulo.startsWith("0103") || r_articulo.startsWith("0111"));
				this.txtAñada.setRequired(r_articulo.startsWith("0102")  || r_articulo.startsWith("0103") || r_articulo.startsWith("0111"));

				this.txtNumPalet=new TextField("Numerador palets");
				this.txtNumPalet.setWidth("200px");
				this.txtNumPalet.setRequired(true);
				
				fila2.addComponent(txtLote);
				fila2.addComponent(txtAñada);
				fila2.addComponent(txtPalets);
				fila2.addComponent(txtCajas);
				fila2.addComponent(txtNumPalet);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				
				this.txtCopias=new TextField("Copias ");				
				this.txtCopias.setWidth("70px");
				this.txtCopias.setRequired(true);
				this.txtCopias.setValue("1");

				fila4.addComponent(cmbPrinter);
				fila4.addComponent(txtCopias);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1050px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
		this.txtLote.focus();
	}

	public ImpresionEtiquetasRecepcion(PeticionEtiquetasRecepcion r_app, String r_articulo, String r_codigo, String r_titulo, boolean r_embotellado)
	{
		this.App = (PeticionEtiquetasRecepcion) r_app;
		this.idCodigo = r_codigo;
		this.isEmbotellado=r_embotellado;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
		HorizontalLayout fila1 = new HorizontalLayout();
		fila1.setWidthUndefined();
		fila1.setSpacing(true);
		this.txtArticulo=new TextField("Articulo");
		this.txtArticulo.setValue(r_articulo);
		this.txtArticulo.setWidth("200px");
		this.txtArticulo.setRequired(true);
		this.txtDescripcion=new TextField("Descripcion");
		this.txtDescripcion.setValue(recogerArticulo(r_articulo));
		this.txtDescripcion.setWidth("400px");
		this.txtDescripcion.setRequired(true);
		fila1.addComponent(txtArticulo);
		fila1.addComponent(txtDescripcion);
		
		HorizontalLayout fila2 = new HorizontalLayout();
		fila2.setWidthUndefined();
		fila2.setSpacing(true);
		
		this.txtPalets=new TextField("Numero de palets: ");				
		this.txtPalets.setWidth("150px");
		this.txtPalets.setRequired(true);
		
		this.txtCajas=new TextField("Cantidad por palet ");				
		this.txtCajas.setWidth("150px");
		this.txtCajas.setRequired(true);
		
		this.txtLote=new TextField("Lote");
		this.txtLote.setWidth("200px");
		this.txtLote.setRequired(true);
		
		this.txtAñada=new TextField("Añada");
		this.txtAñada.setWidth("200px");
		this.txtAñada.setVisible(r_articulo.startsWith("0102") || r_articulo.startsWith("0103") || r_articulo.startsWith("0111"));
		this.txtAñada.setRequired(r_articulo.startsWith("0102") || r_articulo.startsWith("0103") || r_articulo.startsWith("0111"));
		
		this.txtNumPalet=new TextField("Numerador palets");
		this.txtNumPalet.setWidth("200px");
		this.txtNumPalet.setRequired(true);
		
		fila2.addComponent(txtLote);
		fila2.addComponent(txtAñada);
		fila2.addComponent(txtPalets);
		fila2.addComponent(txtCajas);
		fila2.addComponent(txtNumPalet);
		
		HorizontalLayout fila4 = new HorizontalLayout();
		fila4.setWidthUndefined();
		fila4.setSpacing(true);
		this.cmbPrinter=new ComboBox("Impresora");
		this.cargarCombo();
		this.cmbPrinter.setInvalidAllowed(false);
		this.cmbPrinter.setNewItemsAllowed(false);
		this.cmbPrinter.setNullSelectionAllowed(false);
		this.cmbPrinter.setRequired(true);				
		this.cmbPrinter.setWidth("200px");
		
		this.txtCopias=new TextField("Copias ");				
		this.txtCopias.setWidth("70px");
		this.txtCopias.setRequired(true);
		this.txtCopias.setValue("1");
		
		fila4.addComponent(cmbPrinter);
		fila4.addComponent(txtCopias);
		
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1050px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
		this.habilitarCampos();
		this.cargarListeners();
		this.txtLote.focus();
	}

	private String recogerArticulo(String r_articulo)
	{
		String descripcion = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		descripcion=cas.obtenerDescripcionArticulo(r_articulo);
		return descripcion;
	}
	
	private void habilitarCampos()
	{
		this.txtArticulo.setEnabled(true);
		this.txtDescripcion.setEnabled(true);
		this.cmbPrinter.setEnabled(true);
		this.txtCajas.setEnabled(true);
		this.txtLote.setEnabled(true);			
		this.txtAñada.setEnabled(true);			
		this.txtPalets.setEnabled(true);			
		this.txtNumPalet.setEnabled(true);			
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					HashMap<Integer, MapeoGlobal> hash = null;
					hash = new HashMap<Integer, MapeoGlobal>();
					cajas=new Integer(txtCajas.getValue());

					Integer copias = new Integer(txtCopias.getValue().toString());
					Integer palets = new Integer(txtPalets.getValue().toString());
					
					for (int i=0;i<palets;i++)
					{
						MapeoEtiquetasRecepcion mapeoEtiquetas = new MapeoEtiquetasRecepcion();
						
						consultaEtiquetasRecepcionServer cers = consultaEtiquetasRecepcionServer.getInstance(CurrentUser.get());
						mapeoEtiquetas=cers.recuperarRecepcion(txtArticulo.getValue(), txtDescripcion.getValue(), txtLote.getValue(),txtAñada.getValue(), txtCajas.getValue(), idCodigo, txtNumPalet.getValue());
						if (!isEmbotellado && (txtArticulo.getValue().startsWith("0102") || txtArticulo.getValue().startsWith("0111") || txtArticulo.getValue().startsWith("0103")))
							mapeoEtiquetas.setArticulo(mapeoEtiquetas.getArticulo().trim()+"-1");
						
						mapeoEtiquetas = cers.generarDatosEtiquetas(mapeoEtiquetas);
						
						if (mapeoEtiquetas!=null)
						{
							if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
							{
								hash.put(i,mapeoEtiquetas);
							}
							else if (i==0 && cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
							{
								String pdfGenerado = cers.generarInforme(new Integer(mapeoEtiquetas.getIdPedido()));
								
								if(pdfGenerado.length()>0)
								{
									if (i==0 && cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
									{
										RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
									}
									else
									{
										String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
										RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
									    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
									}
								}
						    	else
						    		Notificaciones.getInstance().mensajeError("Error en la generacion");
							}
						}
					}
					if (cmbPrinter.getValue().toString().toUpperCase().equals("ETIQUETAS"))
					{
						String r_archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()+"/germark/recepcion.prn";
						RutinasFicheros.imprimirCuantasUbicacion("recepcion", r_archivo, "etiquetas", new Integer(txtCopias.getValue()), hash);
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});

		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		txtNumPalet.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {

				if (txtNumPalet.getValue()!=null && txtNumPalet.getValue().length()!=0 && RutinasNumericas.esNumero(txtNumPalet.getValue(), "Integer"))
				{
					txtPalets.setValue("1");
					txtPalets.setEnabled(false);
				}
				else
				{
					txtPalets.setEnabled(true);
				}
			}
		});
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.cmbPrinter.getValue()==null || this.cmbPrinter.getValue().toString().length()==0)
		{
			devolver = false;
			cmbPrinter.focus();
		}
		else if (this.txtCajas.getValue()==null || this.txtCajas.getValue().length()==0) 
		{
			devolver =false;			
			txtCajas.focus();
		}
		else if (this.txtCajas.getValue()!=null && this.txtCajas.getValue().length()>0) 
		{
			if (!RutinasNumericas.esNumero(txtCajas.getValue(), "Integer"))
			{
				devolver =false;
				txtCajas.focus();
			}
		}
		else if (this.txtLote.getValue()==null || this.txtLote.getValue().length()==0) 
		{
			devolver =false;
			txtLote.focus();
		}
		else if (this.txtCopias.getValue()==null || this.txtCopias.getValue().length()==0) 
		{
			devolver =false;
			txtCopias.focus();
		}
		else if (this.txtCopias.getValue()!=null && this.txtCopias.getValue().length()>0) 
		{
			if (!RutinasNumericas.esNumero(txtCopias.getValue(), "Integer"))
			{
				devolver =false;
				txtCopias.focus();
			}
		}
		else if (this.txtPalets.getValue()!=null && this.txtPalets.getValue().length()>0) 
		{
			if (!RutinasNumericas.esNumero(txtPalets.getValue(), "Integer") || txtPalets.getValue().contentEquals("0"))
			{
				devolver =false;
				txtPalets.focus();
			}
		}
		return devolver;
	}

	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}
}