package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasCodigoBarras;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventarioLote;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.modelo.MapeoEtiquetasRecepcion;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoLineasPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEtiquetasRecepcionServer {
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private static consultaEtiquetasRecepcionServer instance;
	private static int añosCaducidad = 4;
	
	public consultaEtiquetasRecepcionServer(String r_usuario) {
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasRecepcionServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaEtiquetasRecepcionServer(r_usuario);
		}
		return instance;
	}

	
	public ArrayList<String> recuperarEjerciciosRecepcion()
	{
		ArrayList<String> strEjercicios = null;
		ResultSet rsRecepcion = null;
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strEjercicios = new ArrayList<String>();
			
			cadenaSql.append(" select distinct a_cc_c_0.ejercicio ");
			cadenaSql.append(" from a_cc_c_0");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsRecepcion= cs.executeQuery(cadenaSql.toString());
			while(rsRecepcion.next())
			{
				strEjercicios.add(rsRecepcion.getString("ejercicio"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsRecepcion!=null)
				{
					rsRecepcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strEjercicios;
	}

	public ArrayList<String> recuperarDocumentosRecepcion(String r_ejercicio)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsRecepcion = null;
		Connection con = null;
		Statement cs = null;

		
		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct a_cc_c_0.documento ");
			cadenaSql.append(" from a_cc_c_0 ");
			cadenaSql.append(" where a_cc_c_0.ejercicio = '" + r_ejercicio + "' ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsRecepcion= cs.executeQuery(cadenaSql.toString());
			while(rsRecepcion.next())
			{
				strDocumentos.add(rsRecepcion.getString("documento"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsRecepcion!=null)
				{
					rsRecepcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}

	public ArrayList<String> recuperarPedidos(String r_ejercicio, String r_documento)
	{
		ArrayList<String> strDocumentos= null;
		
		consultaPedidosComprasServer cps = null;
		
		cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
		strDocumentos = cps.recuperarPedidosPendientesCumplimentar(r_ejercicio, r_documento);
		
		return strDocumentos;
	}

	public String recuperarNombreProveedor(String r_ejercicio, String r_documento, String r_codigo)
	{
		ResultSet rsRecepcion = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			cadenaSql.append("select a_cc_c_0.nombre_comercial ");
			cadenaSql.append("from  a_cc_c_0  ");
			cadenaSql.append("where a_cc_c_0.ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append("and a_cc_c_0.documento = '" + r_documento + "' ");
			cadenaSql.append("and a_cc_c_0.codigo = '" + r_codigo + "' ");
			cadenaSql.append("and a_cc_c_0.cumplimentacion is null ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsRecepcion= cs.executeQuery(cadenaSql.toString());
			while(rsRecepcion.next())
			{
				return rsRecepcion.getString("nombre_comercial");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsRecepcion!=null)
				{
					rsRecepcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String recuperarNombreArticulo(String r_articulo)
	{
		String nombre=null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		nombre = cas.obtenerDescripcionArticulo(r_articulo);
		return nombre;
	}
	
	
	public ArrayList<MapeoInventarioLote> recuperarInventario(String r_articulo)
	{
		ArrayList<MapeoInventarioLote> vectorInventario = null;
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		vectorInventario = cis.recuperarExistenciasLotes(r_articulo);
		return vectorInventario;
	}
	
	
	public ArrayList<MapeoLineasPedidosCompras>  recuperarLineasPedidos(Integer r_ejercicio, String r_documento, String r_codigo)
	{
		ArrayList<MapeoLineasPedidosCompras> vectorLineas = null;
		MapeoLineasPedidosCompras mapeo = null;
		
		mapeo=new MapeoLineasPedidosCompras();
		mapeo.setEjercicio(r_ejercicio);
		mapeo.setDocumento(r_documento);
		mapeo.setIdCodigoCompra(new Integer(r_codigo));
		
		consultaPedidosComprasServer cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
		
		try
		{
			vectorLineas=cps.recuperarLineasPedidoGreensys(mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vectorLineas;
	}
	
	public MapeoEtiquetasRecepcion recuperarRecepcion(String r_articulo, String r_descripcion, String r_lote, String r_añada, String r_cantidad, String r_idCodigo,String r_numerador)
	{
		MapeoEtiquetasRecepcion mapeo = null;

				mapeo = new MapeoEtiquetasRecepcion();
				
				mapeo.setArticulo(r_articulo);
				mapeo.setDescripcion(r_descripcion);
				mapeo.setLote(r_lote);
				mapeo.setAñada(r_añada);
				if (r_numerador!=null && RutinasNumericas.esNumero(r_numerador, "Integer") && (new Integer(r_numerador)) > 0)
				{
					mapeo.setNumerador(new Integer(r_numerador));
				}
				else
				{
					mapeo.setNumerador(null);
				}
				
				mapeo.setCajas(r_cantidad);
				mapeo.setIdPedido(r_idCodigo);
				
		return mapeo;
	}
	
	public MapeoEtiquetasRecepcion generarDatosEtiquetas(MapeoEtiquetasRecepcion r_mapeo) 
	{
		String datosGenerados = null;
		String emp_ean = null;
		String prefijo=null;
		String fechaCaducidad=null;
		Integer contador  = null;
		boolean tocoContador = false;
		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		MapeoEtiquetasRecepcion mapeoEtiquetas = new MapeoEtiquetasRecepcion();

		if (r_mapeo.getNumerador()!=null && r_mapeo.getNumerador()>0)
		{
			tocoContador=false;
			contador = r_mapeo.getNumerador();
		}
		else
		{
			tocoContador=true;
			contador = this.recuperarContador();
		}
		
		if (r_mapeo.getArticulo().endsWith("-1"))
			mapeoEtiquetas.setArticulo(r_mapeo.getArticulo().substring(0,7));
		else
			mapeoEtiquetas.setArticulo(r_mapeo.getArticulo().trim());
		mapeoEtiquetas.setIdPedido(r_mapeo.getIdPedido());
		mapeoEtiquetas.setDescripcion(r_mapeo.getDescripcion());
		mapeoEtiquetas.setCajas(r_mapeo.getCajas());
		mapeoEtiquetas.setLote(r_mapeo.getLote());
		mapeoEtiquetas.setAñada(r_mapeo.getAñada());
		
		if (r_mapeo.getArticulo().startsWith("0102") || r_mapeo.getArticulo().startsWith("0111") || r_mapeo.getArticulo().startsWith("0103"))
		{
			emp_ean = "08412423120" + RutinasNumericas.formatearIntegerDigitos(contador,6);
			if (r_mapeo.getArticulo().endsWith("-1"))
				mapeoEtiquetas.setEan(consultaArticulosServer.getInstance(CurrentUser.get()).obtenerEan(mapeoEtiquetas.getArticulo(), "28"));
			else
			{
				mapeoEtiquetas.setEan(consultaArticulosServer.getInstance(CurrentUser.get()).obtenerEan(r_mapeo.getArticulo(), "18"));
				if (mapeoEtiquetas.getEan()==null)
				{
					mapeoEtiquetas.setEan(consultaArticulosServer.getInstance(CurrentUser.get()).obtenerEan(r_mapeo.getArticulo(), "38"));
				}
			}
			
			Date fecha = new Date();
			
			Integer años = this.recuperarCaducidad(r_mapeo.getArticulo());

			fecha = RutinasFechas.conversionDeString(mapeoEtiquetas.getLote().substring(4, 6) + "/" +  mapeoEtiquetas.getLote().substring(2, 4) + "/" + mapeoEtiquetas.getLote().substring(0, 2));
			try
			{
				fechaCaducidad= RutinasFechas.fechaSumandoAños(fecha, años);
				if (fechaCaducidad==null || fechaCaducidad.length()==0 )
				{
					fechaCaducidad= RutinasFechas.fechaActualSumandoAños(años);
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Revisar fecha Caducidad");
				return null;
			}

		}
		else
		{
			emp_ean = "00000000000" + RutinasNumericas.formatearIntegerDigitos(contador,6);
			mapeoEtiquetas.setEan(consultaArticulosServer.getInstance(CurrentUser.get()).obtenerEan(r_mapeo.getArticulo(), "187"));
			fechaCaducidad= RutinasFechas.fechaSumandoAños(RutinasFechas.conversionDeString(RutinasFechas.fechaActual()), 4);
		}

		emp_ean = RutinasCodigoBarras.calcula_ean(emp_ean);
		

		mapeoEtiquetas.setSscc(emp_ean);
		mapeoEtiquetas.setSscc1("00" + emp_ean.trim() + "17" + RutinasFechas.convertirDateToString(RutinasFechas.conversionDeString(fechaCaducidad), "yyMMdd"));
		mapeoEtiquetas.setSscc1t("(00)" + emp_ean.trim() + "(17)" + RutinasFechas.convertirDateToString(RutinasFechas.conversionDeString(fechaCaducidad), "yyMMdd"));
		mapeoEtiquetas.setPresscc1t("00");
		
		mapeoEtiquetas.setCodigo("02" + mapeoEtiquetas.getEan().trim()+ "10" + mapeoEtiquetas.getLote() + "37" + RutinasCadenas.formatCerosIzquierda(r_mapeo.getCajas(), 6));
		mapeoEtiquetas.setCodigot("(02)" +  mapeoEtiquetas.getEan().trim()+ "(10)" + mapeoEtiquetas.getLote() + "(37)" + RutinasCadenas.formatCerosIzquierda(r_mapeo.getCajas(), 6));
		mapeoEtiquetas.setPrecodigot("02");

		try {
			/*
			 * Borro datos anteriores si los hubiera
			 */
			this.eliminar(r_mapeo.getIdPedido());
			/*
			 * Genero los datos para la impresion
			 */
			datosGenerados = this.guardarNuevo(mapeoEtiquetas);

			if (datosGenerados == null )
			{
				if (tocoContador) this.actualizarContador(contador);
			}
			else
				return null;
		} 
		catch (Exception ex) 
		{
			serNotif.mensajeError(ex.getMessage());
			return null;
		}

		return mapeoEtiquetas;
	}

	public void eliminar(String r_cod) {
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;

		
		try {
			cadenaSQL.append(" DELETE FROM prd_etiquetas_recepcion ");
			cadenaSQL.append(" WHERE prd_etiquetas_recepcion.idPedido = ?");
			con = this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_cod);
			preparedStatement.executeUpdate();
		} 
		catch (Exception ex) 
		{
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String guardarNuevo(MapeoEtiquetasRecepcion r_mapeo) {
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO prd_etiquetas_recepcion ( ");
			cadenaSQL.append(" prd_etiquetas_recepcion.idPedido,");
			cadenaSQL.append(" prd_etiquetas_recepcion.articulo ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.descripcion ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.cajas ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.lote ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.sscc ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.sscc1 ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.ean ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.codigo ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.codigot ,");
			cadenaSQL.append(" prd_etiquetas_recepcion.sscc1t, ");
			cadenaSQL.append(" prd_etiquetas_recepcion.presscc1t, ");
			cadenaSQL.append(" prd_etiquetas_recepcion.precodigot, ");
			cadenaSQL.append(" prd_etiquetas_recepcion.añada) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con = this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());

			if (r_mapeo.getIdPedido() != null) {
				preparedStatement.setString(1, r_mapeo.getIdPedido());
			} else {
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getArticulo() != null) {
				preparedStatement.setString(2, r_mapeo.getArticulo());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getDescripcion() != null) {
				preparedStatement.setString(3, r_mapeo.getDescripcion());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getCajas() != null) {
				preparedStatement.setString(4, r_mapeo.getCajas());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getLote() != null) {
				preparedStatement.setString(5, r_mapeo.getLote());
			} else {
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(6, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(6, null);
			}
			if (r_mapeo.getSscc1() != null) {
				preparedStatement.setString(7, r_mapeo.getSscc1());
			} else {
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getEan() != null) {
				preparedStatement.setString(8, r_mapeo.getEan());
			} else {
				preparedStatement.setString(8, null);
			}
			if (r_mapeo.getCodigo() != null) {
				preparedStatement.setString(9, r_mapeo.getCodigo());
			} else {
				preparedStatement.setString(9, null);
			}
			if (r_mapeo.getCodigot() != null) {
				preparedStatement.setString(10, r_mapeo.getCodigot());
			} else {
				preparedStatement.setString(10, null);
			}
			if (r_mapeo.getSscc1t() != null) {
				preparedStatement.setString(11, r_mapeo.getSscc1t());
			} else {
				preparedStatement.setString(11, null);
			}
			if (r_mapeo.getPresscc1t() != null) {
				preparedStatement.setString(12, r_mapeo.getPresscc1t());
			} else {
				preparedStatement.setString(12, null);
			}
			if (r_mapeo.getPrecodigot() != null) {
				preparedStatement.setString(13, r_mapeo.getPrecodigot());
			} else {
				preparedStatement.setString(13, null);
			}
			if (r_mapeo.getAñada() != null) {
				preparedStatement.setString(14, r_mapeo.getAñada());
			} else {
				preparedStatement.setString(14, null);
			}
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String generarInforme(Integer r_id) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigo(r_id);
		libImpresion.setArchivoDefinitivo("/etiquetaRecepcion" + r_id.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("etiquetasRecepcion.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasRecepcion");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}
	

	private Integer recuperarContador()
	{
		Integer contador_obtenido = null;
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		contador_obtenido = ccs.recuperarContador("Recepciones");
		
		return contador_obtenido;
	}
	
	private void actualizarContador(Integer r_contador)
	{
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
		ccs.actualizarContador(r_contador, "Recepciones");
	}

	private Integer recuperarCaducidad(String r_articulo)
	{
		Integer años = null;
		
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());

		años = cas.obtenerCaducidadArticulo(r_articulo);
		if (años == null || años==0) años = añosCaducidad;
		return añosCaducidad;
	}
	
	
}