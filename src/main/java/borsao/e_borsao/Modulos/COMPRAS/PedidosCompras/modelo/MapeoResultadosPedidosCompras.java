package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

public class MapeoResultadosPedidosCompras
{
	private Integer idCodigo;
	private String clave_tabla;
	private String documento;
	private String serie;
	private Integer idCodigoCompra;
	private Double importe;
	private String moneda;
	private Double importeLocal;
	
	public MapeoResultadosPedidosCompras()
	{
	}


	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}


	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}


	public String getClave_Tabla() {
		return clave_tabla;
	}


	public void setClave_Tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Double getImporte() {
		return importe;
	}


	public void setImporte(Double importe) {
		this.importe = importe;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public Double getImporteLocal() {
		return importeLocal;
	}


	public void setImporteLocal(Double importeLocal) {
		this.importeLocal = importeLocal;
	}


	public Integer getIdCodigo() {
		return idCodigo;
	}


	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}

}