package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean enviadoMail=false;
	
    public OpcionesGrid(ArrayList<MapeoPedidosCompras> r_vector) 
    {
        
        this.vector=r_vector;
        
		this.asignarTitulo("Pedidos de Compra");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("enviar");
    	this.camposNoFiltrar.add("enviado");
    	this.camposNoFiltrar.add("boton");
    	this.camposNoFiltrar.add("pdf");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoPedidosCompras.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(4);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("enviar", "enviado", "boton", "pdf" , "ejercicio","clave_Tabla","documento","idCodigoCompra","fecha", "proveedor", "nombre", "fax", "mail","mail2","mail3");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "75");
    	this.widthFiltros.put("clave_Tabla", "75");
    	this.widthFiltros.put("documento", "75");
    	this.widthFiltros.put("idCodigoCompra", "100");
    	this.widthFiltros.put("fecha", "100");
    	this.widthFiltros.put("fax", "125");
    	this.widthFiltros.put("proveedor", "100");
    	this.widthFiltros.put("nombre", "200");
    	this.widthFiltros.put("mail", "200");
    	this.widthFiltros.put("mail2", "200");
    	this.widthFiltros.put("mail3", "200");
    	
    	this.getColumn("boton").setHeaderCaption("");
    	this.getColumn("boton").setSortable(false);
    	this.getColumn("boton").setWidth(new Double(50));
    	this.getColumn("pdf").setHeaderCaption("");
    	this.getColumn("pdf").setSortable(false);
    	this.getColumn("pdf").setWidth(new Double(50));
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(100));
    	this.getColumn("clave_Tabla").setHeaderCaption("Clave Tabla");
    	this.getColumn("clave_Tabla").setWidth(new Double(100));
    	this.getColumn("clave_Tabla").setHidden(true);
    	this.getColumn("documento").setHeaderCaption("Documento");
    	this.getColumn("documento").setWidth(new Double(100));
    	this.getColumn("proveedor").setHeaderCaption("Proveedor");
    	this.getColumn("proveedor").setWidth(new Double(125));
    	this.getColumn("idCodigoCompra").setHeaderCaption("Codigo");
    	this.getColumn("idCodigoCompra").setWidth(new Double(125));
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(125));
    	this.getColumn("nombre").setHeaderCaption("Nombre Proveedor");
    	this.getColumn("mail").setHeaderCaption("PARA");
    	this.getColumn("mail2").setHeaderCaption("CC");
    	this.getColumn("mail3").setHeaderCaption("CC");
    	this.getColumn("fax").setHeaderCaption("FAX");
    	this.getColumn("fax").setWidth(new Double(150));
    	
    	this.getColumn("direccion").setHidden(true);
    	this.getColumn("cp").setHidden(true);
    	this.getColumn("poblacion").setHidden(true);
    	this.getColumn("provincia").setHidden(true);
    	this.getColumn("plazoEntrega").setHidden(true);
    	this.getColumn("formaPago").setHidden(true);
    	this.getColumn("pais").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("moneda").setHidden(true);
    	this.getColumn("enviar").setHidden(true);
    	this.getColumn("enviado").setHidden(true);
	}
	
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("boton"))
            	{
            		MapeoPedidosCompras mapeo = (MapeoPedidosCompras) event.getItemId();
        	    	if (!mapeo.getEnviado().equals("N") && ((mapeo.getMail()!=null && mapeo.getMail().length()>0) || (mapeo.getMail2()!=null && mapeo.getMail2().length()>0) || (mapeo.getMail3()!=null && mapeo.getMail3().length()>0)))
        	    	{
    	    			boolean generado = traerPedidoSeleccionado(mapeo);
    	    			
    	    			if (generado)
    	    			{
    	    				enviadoMail=generacionPdf(mapeo, true, true);
//    	    				enviadoMail=true;
    	    				if (enviadoMail)
    	    				{
    	    					mapeo.setEnviado("S");
    	    					boolean send = actualizarPedidoEnviado(mapeo);
    	    					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar el pedido como enviado en GREENsys.");
    	    					refresh(mapeo, mapeo);
    	    				}
    	    			}
						else
						{
							Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoCompra().toString() + " e insertarlo en la plataforma.");
						}
        	    	}	
            	}
            	else if (event.getPropertyId().toString().equals("pdf"))
            	{
            		MapeoPedidosCompras mapeo = (MapeoPedidosCompras) event.getItemId();
            		
	    			boolean generado = traerPedidoSeleccionado(mapeo);
	    			
	    			if (generado)
	    			{
	    				generacionPdf(mapeo,false,true);
	    			}
					else
					{
						Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoCompra().toString() + " e insertarlo en la plataforma.");
					}
            	}
            }
        });
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            boolean enviar = true;
            String enviado = "";
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("enviar".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue().toString().equals("Enviar"))
	            	{
	            		enviar=true;
	            	}
	            	else
	            	{
	            		enviar=false;
	            	}
            	}
            	if ("enviado".equals(cellReference.getPropertyId()))
            	{
            		enviado=cellReference.getValue().toString();
            	}
            	if ("idCodigoCompra".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()) || "proveedor".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ("boton".equals(cellReference.getPropertyId()))
            	{
            		if (enviar)
            		{
            			if (enviado.equals("S"))
            			{
            				return "nativebuttonGreen";
            			}
            			else if (enviado==null || enviado.equals("") || enviado.length()==0)
            			{
            				return "nativebutton";
            			}
            			else
            			{
            				return "nativebuttonNo";
            			}
            		}
            		else
            		{
            			return "nativebuttonRed";
            		}
            	}
            	else if ( "pdf".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonPdf";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
    private boolean traerPedidoSeleccionado(MapeoPedidosCompras r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
    	boolean traido = cps.traerPedidoGreensysMysql(r_mapeo);
    	return traido;
    }
    
    private boolean actualizarPedidoEnviado(MapeoPedidosCompras r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
    	boolean traido = cps.actualizarCabeceraGreensys(r_mapeo);
    	return traido;
    }
    
    private boolean generacionPdf(MapeoPedidosCompras r_mapeo, boolean r_EnviarMail, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	String cuentaDestino = null;
    	String cuentaCopia = null;
    	String cuentaCopia2 = null;
    	String asunto = null;
    	String cuerpo = null;
    	HashMap adjuntos = null;
    	boolean envio = false;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
    	pdfGenerado = cps.generarInforme(r_mapeo.getIdCodigo(),r_mapeo.getIdCodigoCompra());
    	
		if (r_EnviarMail)
		{    			
			cuentaDestino = r_mapeo.getMail();
			cuentaCopia = r_mapeo.getMail2();
			cuentaCopia2 = r_mapeo.getMail3();
			
			asunto = "Envío Pedido Compra " + r_mapeo.getIdCodigoCompra().toString();
			cuerpo = "Adjunto enviamos el pedido: " + r_mapeo.getIdCodigoCompra().toString();
			
			adjuntos = new HashMap();
			
			adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
			adjuntos.put("nombre", "pedido - " + r_mapeo.getIdCodigoCompra().toString() + ".pdf");
			
    		envio=ClienteMail.getInstance().envioMail(cuentaDestino, cuentaCopia, cuentaCopia2, asunto , cuerpo, adjuntos, r_eliminar);
    		return envio;
		}
		else
		{
			RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		}
		
		pdfGenerado = null;
    	cuentaDestino = null;
    	cuentaCopia = null;
    	asunto = null;
    	cuerpo = null;
    	adjuntos = null;
    	
    	return envio;
    }

	@Override
	public void calcularTotal() {
		
	}
}
