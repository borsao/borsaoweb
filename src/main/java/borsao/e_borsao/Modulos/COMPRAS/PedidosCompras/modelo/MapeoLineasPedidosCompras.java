package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

import java.util.Date;

public class MapeoLineasPedidosCompras
{
	private Integer idCodigo;
	private String clave_tabla;
	private String documento;
	private String serie;
	private String articulo;
	private String descripcion;
	private String moneda;
	private String nombreProveedor;

	private Integer idCodigoCompra;
	private Integer ejercicio;
	private Integer posicion;
	private Integer linea;
	private Integer unidades_serv;
	private Integer unidades_pdte;
	private Integer proveedor;
	private Integer stock_queda;
	private Integer almacen;

	private Double unidades;
	private Double cantidad;
	private Double precio;
	private Double descuento;
	private Double importe;
	private Double importeLocal;
    
	private Date fechaDocumento;
    private Date plazoEntrega;

	public MapeoLineasPedidosCompras()
	{
	}


	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}


	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}


	public String getClave_Tabla() {
		return clave_tabla;
	}


	public void setClave_Tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}



	public Integer getPosicion() {
		return posicion;
	}


	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}


	public Integer getLinea() {
		return linea;
	}


	public void setLinea(Integer linea) {
		this.linea = linea;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Double getCantidad() {
		return cantidad;
	}


	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}


	public Double getUnidades() {
		return unidades;
	}


	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public Double getDescuento() {
		return descuento;
	}


	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}


	public Double getImporte() {
		return importe;
	}


	public void setImporte(Double importe) {
		this.importe = importe;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public Double getImporteLocal() {
		return importeLocal;
	}


	public void setImporteLocal(Double importeLocal) {
		this.importeLocal = importeLocal;
	}


	public Integer getIdCodigo() {
		return idCodigo;
	}


	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}


	
	public Date getFechaDocumento() {
		return fechaDocumento;
	}


	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}


	public Date getPlazoEntrega() {
		return plazoEntrega;
	}


	public void setPlazoEntrega(Date plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}


	public Integer getProveedor() {
		return proveedor;
	}


	public void setProveedor(Integer proveedor) {
		this.proveedor = proveedor;
	}


	public String getNombreProveedor() {
		return nombreProveedor;
	}


	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}


	public Integer getUnidades_serv() {
		return unidades_serv;
	}


	public void setUnidades_serv(Integer unidades_serv) {
		this.unidades_serv = unidades_serv;
	}


	public Integer getUnidades_pdte() {
		return unidades_pdte;
	}


	public void setUnidades_pdte(Integer unidades_pdte) {
		this.unidades_pdte = unidades_pdte;
	}


	public Integer getStock_queda() {
		return stock_queda;
	}


	public void setStock_queda(Integer stock_queda) {
		this.stock_queda = stock_queda;
	}


	public Integer getAlmacen() {
		return almacen;
	}


	public void setAlmacen(Integer almacen) {
		this.almacen = almacen;
	}


}