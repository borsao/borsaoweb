package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoPedidosComprasSGA extends MapeoGlobal
{
	private Integer idCodigoCompra;
    private String receiptList;
    private String estado;
	private Date fecha;
	

	public MapeoPedidosComprasSGA()
	{
	}

	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}

	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecha() {
		return RutinasFechas.convertirDateToString(fecha);
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
		
	}

	public String getReceiptList() {
		return receiptList;
	}

	public void setReceiptList(String receiptList) {
		this.receiptList = receiptList;
	}
}