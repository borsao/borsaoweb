package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoLineasPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasPedidosCompra extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private boolean verArticulo=false;
	private VerticalLayout  principal=null;
	private HorizontalLayout  botonera=null;
	
	private void estiloPantalla(String r_titulo)
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");

		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
	}
	
	private void crearBotones()
	{
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
	}

	private void estiloGrid()
	{
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
	}
	
	public pantallaLineasPedidosCompra(String r_articulo, Integer r_stock, String r_titulo)
	{
		
		consultaPedidosComprasServer cse = new consultaPedidosComprasServer(CurrentUser.get());
		ArrayList<MapeoLineasPedidosCompras> vector = cse.recuperarPedidoGreensys(r_articulo, r_stock);
		this.verArticulo=false;
		
		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.estiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasPedidosCompra(Integer r_codigo, String r_titulo)
	{
		consultaPedidosComprasServer cse = new consultaPedidosComprasServer(CurrentUser.get());
		MapeoLineasPedidosCompras mapeo = new MapeoLineasPedidosCompras();
		mapeo.setIdCodigoCompra(r_codigo);
		
		ArrayList<MapeoLineasPedidosCompras> vector = cse.recuperarLineasPedidoGreensys(mapeo);
		this.verArticulo=true;

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.estiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasPedidosCompra(Integer r_codigo, Integer r_ejercicio)
	{
		consultaPedidosComprasServer cse = new consultaPedidosComprasServer(CurrentUser.get());
		MapeoLineasPedidosCompras mapeo = new MapeoLineasPedidosCompras();
		mapeo.setIdCodigoCompra(r_codigo);
		mapeo.setEjercicio(r_ejercicio);
		ArrayList<MapeoLineasPedidosCompras> vector = cse.recuperarLineasPedidoAlbaranGreensys(mapeo);
		this.verArticulo=true;

		this.estiloPantalla("Pedido" + r_codigo.toString());
		this.crearBotones();
		this.llenarRegistros(vector);
		this.estiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}

	public pantallaLineasPedidosCompra(String r_titulo)
	{
		consultaPedidosComprasServer cse = new consultaPedidosComprasServer(CurrentUser.get());
		ArrayList<MapeoLineasPedidosCompras> vector = cse.recuperarPedidoVencidos();
		this.verArticulo=true;

		this.estiloPantalla(r_titulo);
		this.crearBotones();
		this.llenarRegistros(vector);
		this.estiloGrid();
		
		principal.addComponent(this.gridDatos);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoLineasPedidosCompras> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosCompras mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Almacen", Integer.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Plazo Entrega", String.class, null);
		container.addContainerProperty("Proveedor", String.class, null);
		container.addContainerProperty("Nombre", String.class, null);
		if (verArticulo)
		{
			container.addContainerProperty("Articulo", String.class, null);
			container.addContainerProperty("Descripcion", String.class, null);
		}
		container.addContainerProperty("Unidades", Double.class, null);
		container.addContainerProperty("Servidas", Integer.class, null);
		container.addContainerProperty("Pendientes", Integer.class, null);
		container.addContainerProperty("Stock", Integer.class, null);
        container.addContainerProperty("Ejercicio", Integer.class, null);
        container.addContainerProperty("Documento", String.class, null);
        container.addContainerProperty("Codigo", String.class, null);
        
        boolean verStock = true;
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosCompras) iterator.next();
			
			if (verArticulo)
			{
				file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
				file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			}
			
    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreProveedor()));
    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
    		
    		file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen());
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoCompra().toString());
    		file.getItemProperty("Proveedor").setValue(mapeoAyuda.getProveedor().toString());
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
    		file.getItemProperty("Servidas").setValue(mapeoAyuda.getUnidades_serv());
    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getUnidades_pdte());
    		if (mapeoAyuda.getStock_queda()!=null) file.getItemProperty("Stock").setValue(mapeoAyuda.getStock_queda());else verStock=false;
    		
		}

		if (!verStock) container.removeContainerProperty("Stock");
		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Unidades".equals(cellReference.getPropertyId()) || "Servidas".equals(cellReference.getPropertyId()) || "Pendientes".equals(cellReference.getPropertyId()) || "Stock".equals(cellReference.getPropertyId())
            			|| "Almacen".equals(cellReference.getPropertyId()) || "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId()) || "Proveedor".equals(cellReference.getPropertyId())) 
            	{
            		if ("Stock".equals(cellReference.getPropertyId()) && new Integer(cellReference.getValue().toString()) < 0)
            		{
            			return "Rcell-error";
            		}        			
        			else 
        			{
        				return "Rcell-normal";
        			}
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}