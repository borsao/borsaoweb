package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoLineasPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoLineasPedidosComprasSGA;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoNotasPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoPedidosComprasSGA;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoResultadosPedidosCompras;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo.MapeoMPPrimerPlazo;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaPedidosComprasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaPedidosComprasServer instance;
	
	
	public consultaPedidosComprasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPedidosComprasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPedidosComprasServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoPedidosCompras> datosOpcionesGlobal(MapeoPedidosCompras r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoPedidosCompras> vector = null;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
        cadenaSQL.append(" a.documento cc_dc, ");
        cadenaSQL.append(" a.serie cc_sr, ");
        cadenaSQL.append(" a.codigo cc_cd, ");	    
        cadenaSQL.append(" a.fecha_documento cc_fec, ");
        cadenaSQL.append(" a.ejercicio cc_ej, ");
        cadenaSQL.append(" a.proveedor cc_pr, ");
        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
        cadenaSQL.append(" a.obs_envio2 cc_se, ");        
        cadenaSQL.append(" f.texto pr_mai, ");
        cadenaSQL.append(" g.texto pr_mai2, ");        
        cadenaSQL.append(" h.texto pr_mai3, ");
        cadenaSQL.append(" a.direccion cc_dir, ");
        cadenaSQL.append(" a.poblacion cc_pob, ");
        cadenaSQL.append(" a.cod_postal cc_cp, ");
        cadenaSQL.append(" a.plazo_entrega cc_pe, ");
        cadenaSQL.append(" e.fax cc_fax, ");
        cadenaSQL.append(" c.nombre cc_pa, ");
        cadenaSQL.append(" d.nombre cc_pv, ");
        cadenaSQL.append(" b.descripcion cc_fp ");
        
     	cadenaSQL.append(" FROM a_cc_c_0 a, e_frmpag b, e_paises c, e_provin d, outer e_provee e, outer e_pro_nt f, outer e_pro_nt g, outer e_pro_nt h ");
     	cadenaSQL.append(" where e.proveedor = a.proveedor ");
     	cadenaSQL.append(" and c.pais = a.pais ");
     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
     	cadenaSQL.append(" and f.proveedor = a.proveedor and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
     	cadenaSQL.append(" and g.proveedor = a.proveedor and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
     	cadenaSQL.append(" and h.proveedor = a.proveedor and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
     	
//     	cadenaSQL.append(" union ");
//     	
//     	cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
//        cadenaSQL.append(" a.documento cc_dc, ");
//        cadenaSQL.append(" a.serie cc_sr, ");
//        cadenaSQL.append(" a.codigo cc_cd, ");	    
//        cadenaSQL.append(" a.fecha_documento cc_fec, ");
//        cadenaSQL.append(" a.ejercicio cc_ej, ");
//        cadenaSQL.append(" a.proveedor cc_pr, ");
//        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
//        cadenaSQL.append(" a.obs_envio2 cc_se, ");        
//        cadenaSQL.append(" f.texto pr_mai, ");
//        cadenaSQL.append(" g.texto pr_mai2, ");        
//        cadenaSQL.append(" h.texto pr_mai3, ");
//        cadenaSQL.append(" a.direccion cc_dir, ");
//        cadenaSQL.append(" a.poblacion cc_pob, ");
//        cadenaSQL.append(" a.cod_postal cc_cp, ");
//        cadenaSQL.append(" a.plazo_entrega cc_pe, ");
//        cadenaSQL.append(" e.fax cc_fax, ");
//        cadenaSQL.append(" c.nombre cc_pa, ");
//        cadenaSQL.append(" d.nombre cc_pv, ");
//        cadenaSQL.append(" b.descripcion cc_fp ");
//        
//     	cadenaSQL.append(" FROM a_cc_c_1 a, e_frmpag b, e_paises c, e_provin d, outer e_provee e, outer e_pro_nt f, outer e_pro_nt g, outer e_pro_nt h ");
//     	cadenaSQL.append(" where e.proveedor = a.proveedor ");
//     	cadenaSQL.append(" and c.pais = a.pais ");
//     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
//     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
//     	cadenaSQL.append(" and f.proveedor = a.proveedor and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
//     	cadenaSQL.append(" and g.proveedor = a.proveedor and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
//     	cadenaSQL.append(" and h.proveedor = a.proveedor and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
		try
		{
			cadenaSQL.append(" order by 4 desc ");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoPedidosCompras>();
			
			while(rsOpcion.next())
			{
				MapeoPedidosCompras mapeoPedidosCompras = new MapeoPedidosCompras();
				/*
				 * recojo mapeo operarios
				 */
				mapeoPedidosCompras.setEjercicio(rsOpcion.getInt("cc_ej"));
				mapeoPedidosCompras.setClave_Tabla(rsOpcion.getString("cc_cl"));
				mapeoPedidosCompras.setDocumento(rsOpcion.getString("cc_dc"));
				mapeoPedidosCompras.setSerie(rsOpcion.getString("cc_sr"));
				mapeoPedidosCompras.setIdCodigoCompra(rsOpcion.getInt("cc_cd"));
				mapeoPedidosCompras.setNombre(RutinasCadenas.conversion(rsOpcion.getString("cc_nc")));
				mapeoPedidosCompras.setMail(RutinasCadenas.conversion(rsOpcion.getString("pr_mai")));
				mapeoPedidosCompras.setFecha(rsOpcion.getDate("cc_fec"));
				mapeoPedidosCompras.setMail2(RutinasCadenas.conversion(rsOpcion.getString("pr_mai2")));
				mapeoPedidosCompras.setMail3(RutinasCadenas.conversion(rsOpcion.getString("pr_mai3")));
				if ((rsOpcion.getString("pr_mai") == null || rsOpcion.getString("pr_mai").length()==0) && (rsOpcion.getString("pr_mai2")==null || rsOpcion.getString("pr_mai2").length()==0) && (rsOpcion.getString("pr_mai3")==null || rsOpcion.getString("pr_mai3").length()==0)) 
				{
					mapeoPedidosCompras.setEnviar("--------");
				}
				if (rsOpcion.getString("cc_se") == null || rsOpcion.getString("cc_se").length()==0)
				{
					mapeoPedidosCompras.setEnviado("");
				}
				else
				{
					mapeoPedidosCompras.setEnviado(rsOpcion.getString("cc_se").trim().toUpperCase());
				}
				mapeoPedidosCompras.setProveedor(rsOpcion.getInt("cc_pr"));
				mapeoPedidosCompras.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("cc_dir")));
				mapeoPedidosCompras.setPoblacion(RutinasCadenas.conversion(rsOpcion.getString("cc_pob")));
				mapeoPedidosCompras.setCp(rsOpcion.getString("cc_cp"));
				mapeoPedidosCompras.setPlazoEntrega(rsOpcion.getDate("cc_pe"));
				mapeoPedidosCompras.setFax(rsOpcion.getString("cc_fax"));
				mapeoPedidosCompras.setPais(RutinasCadenas.conversion(rsOpcion.getString("cc_pa")));
				mapeoPedidosCompras.setProvincia(RutinasCadenas.conversion(rsOpcion.getString("cc_pv")));
				mapeoPedidosCompras.setFormaPago(RutinasCadenas.conversion(rsOpcion.getString("cc_fp")));
		        
				vector.add(mapeoPedidosCompras);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public MapeoPedidosCompras datosOpcionesPedido(MapeoPedidosCompras r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
		cadenaSQL.append(" a.documento cc_dc, ");
		cadenaSQL.append(" a.serie cc_sr, ");
		cadenaSQL.append(" a.codigo cc_cd, ");	    
		cadenaSQL.append(" a.fecha_documento cc_fec, ");
		cadenaSQL.append(" a.ejercicio cc_ej, ");
		cadenaSQL.append(" a.proveedor cc_pr, ");
		cadenaSQL.append(" a.nombre_comercial cc_nc, ");
		cadenaSQL.append(" a.obs_envio2 cc_se, ");        
		cadenaSQL.append(" a.direccion cc_dir, ");
		cadenaSQL.append(" a.poblacion cc_pob, ");
		cadenaSQL.append(" a.cod_postal cc_cp, ");
		cadenaSQL.append(" a.plazo_entrega cc_pe, ");
		cadenaSQL.append(" e.fax cc_fax, ");
		cadenaSQL.append(" c.nombre cc_pa, ");
		cadenaSQL.append(" d.nombre cc_pv ");
		
		cadenaSQL.append(" FROM a_cc_c_0 a, e_paises c, e_provin d, outer e_provee e  ");
		cadenaSQL.append(" where e.proveedor = a.proveedor ");
		cadenaSQL.append(" and c.pais = a.pais ");
		cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
		cadenaSQL.append(" and a.codigo =  " + r_mapeo.getIdCodigoCompra());
		
		try
		{
			cadenaSQL.append(" order by 4 desc ");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoPedidosCompras mapeoPedidosCompras = new MapeoPedidosCompras();
				/*
				 * recojo mapeo operarios
				 */
				mapeoPedidosCompras.setEjercicio(rsOpcion.getInt("cc_ej"));
				mapeoPedidosCompras.setClave_Tabla(rsOpcion.getString("cc_cl"));
				mapeoPedidosCompras.setDocumento(rsOpcion.getString("cc_dc"));
				mapeoPedidosCompras.setSerie(rsOpcion.getString("cc_sr"));
				mapeoPedidosCompras.setIdCodigoCompra(rsOpcion.getInt("cc_cd"));
				mapeoPedidosCompras.setNombre(RutinasCadenas.conversion(rsOpcion.getString("cc_nc")));
				mapeoPedidosCompras.setFecha(rsOpcion.getDate("cc_fec"));
				mapeoPedidosCompras.setProveedor(rsOpcion.getInt("cc_pr"));
				mapeoPedidosCompras.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("cc_dir")));
				mapeoPedidosCompras.setPoblacion(RutinasCadenas.conversion(rsOpcion.getString("cc_pob")));
				mapeoPedidosCompras.setCp(rsOpcion.getString("cc_cp"));
				mapeoPedidosCompras.setPlazoEntrega(rsOpcion.getDate("cc_pe"));
				mapeoPedidosCompras.setFax(rsOpcion.getString("cc_fax"));
				mapeoPedidosCompras.setPais(RutinasCadenas.conversion(rsOpcion.getString("cc_pa")));
				mapeoPedidosCompras.setProvincia(RutinasCadenas.conversion(rsOpcion.getString("cc_pv")));
				
				return mapeoPedidosCompras;				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public boolean traerPedidoGreensysMysql(MapeoPedidosCompras r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT idCodigo cc_id, ");
		cadenaSQL.append(" clave_tabla cc_clt, ");
        cadenaSQL.append(" documento cc_doc, ");
        cadenaSQL.append(" serie cc_ser, ");
        cadenaSQL.append(" codigo cc_cod, ");	    
        cadenaSQL.append(" ejercicio cc_eje ");
     	cadenaSQL.append(" FROM a_cc_c_0");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getClave_Tabla()!=null && r_mapeo.getClave_Tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_Tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie= '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoCompra() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, clave_tabla, documento, serie, codigo desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			String cadenaSQLBorrado = null;
			Statement cd = null;
			
			while(rsOpcion.next())
			{
				r_mapeo.setIdCodigo(rsOpcion.getInt("cc_id"));
				conDelete = this.conManager.establecerConexionInd();
				
				cd = conDelete.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				/*
				 * 1.- lo borro
				 */
				cadenaSQLBorrado = " delete from a_cc_p_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				cadenaSQLBorrado = " delete from a_cc_l_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());

				cadenaSQLBorrado = " delete from a_cc_c_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				cadenaSQLBorrado = " delete from a_cc_n_0 where idCodigo = " + r_mapeo.getIdCodigo();
				cd.execute(cadenaSQLBorrado.toString());
				
				conDelete=null;
				cd=null;
				
			}
			/*
			 * inserto de nuevo el pedido
			 */
			insertado = this.insertarCabecera(r_mapeo);
			/*
			 * devuelvo verdadero
			 */
			return insertado;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
	public boolean insertarCabecera(MapeoPedidosCompras r_mapeo)
	{
		/*
		 * Codigo del proveedor
		 * Nombre comercial del proveedor
		 * Direccion del proveedor 
		 * Codigo postal del proveedor
		 * Poblacion del proveedor 
		 * Provincia del proveedor
		 * Num. Fax del proveedor
		 * mail1
		 * mail2
		 * Fecha de pedido
		 * Numero de pedido
		 * Descripcion de forma de pago
		 * Plazo de entrega del pedido
		 * 
		 */
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		boolean insertado = true;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
	        
			cadenaSQL.append(" INSERT INTO a_cc_c_0 ( ");
    		cadenaSQL.append(" clave_tabla, ");
	        cadenaSQL.append(" documento, ");
	        cadenaSQL.append(" serie, ");
	        cadenaSQL.append(" codigo, ");
	        cadenaSQL.append(" fecha_documento, ");
	        cadenaSQL.append(" ejercicio, ");
	        cadenaSQL.append(" proveedor, ");
	        cadenaSQL.append(" nombre_comercial, ");
	        cadenaSQL.append(" mail1, ");
	        cadenaSQL.append(" mail2, ");
	        cadenaSQL.append(" mail3, ");
	        cadenaSQL.append(" direccion, ");
	        cadenaSQL.append(" poblacion, ");
	        cadenaSQL.append(" cod_postal, ");
	        cadenaSQL.append(" pais, ");
	        cadenaSQL.append(" provincia, ");
	        cadenaSQL.append(" plazo_entrega, ");
	        cadenaSQL.append(" fpago_aplazado, ");
	        cadenaSQL.append(" fax, ");
	        cadenaSQL.append(" idCodigo ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguiente());
		    
		    
		    if (r_mapeo.getClave_Tabla()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getClave_Tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    preparedStatement.setInt(4, r_mapeo.getIdCodigoCompra());
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    preparedStatement.setInt(6, r_mapeo.getEjercicio());
		    preparedStatement.setInt(7, r_mapeo.getProveedor());
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(8, RutinasCadenas.conversion(r_mapeo.getNombre()));
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getMail()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getMail());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getMail2()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getMail2());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getMail3()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getMail3());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }		    
		    if (r_mapeo.getDireccion()!=null)
		    {
		    	preparedStatement.setString(12, RutinasCadenas.conversion(r_mapeo.getDireccion()));
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getPoblacion()!=null)
		    {
		    	preparedStatement.setString(13, RutinasCadenas.conversion(r_mapeo.getPoblacion()));
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getCp()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getCp());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getPais()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getPais());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getProvincia()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getProvincia());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getPlazoEntrega()!=null)
		    {
		    	preparedStatement.setString(17, RutinasFechas.convertirAFechaMysql(r_mapeo.getPlazoEntrega()));
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getFormaPago()!=null)
		    {
		    	preparedStatement.setString(18, RutinasCadenas.conversion(r_mapeo.getFormaPago()));
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getFax()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getFax());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    preparedStatement.setInt(20, r_mapeo.getIdCodigo());

	        preparedStatement.executeUpdate();
	        
			
	        /*
	         * insertamos las lineas tras el exito de la cabecera
	         */
	        insertado=this.insertarLineas(r_mapeo);
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	
	public boolean insertarCabeceraSGA(MapeoPedidosComprasSGA r_mapeo)
	{
		/*
		 * Codigo del proveedor
		 * Nombre comercial del proveedor
		 * Direccion del proveedor 
		 * Codigo postal del proveedor
		 * Poblacion del proveedor 
		 * Provincia del proveedor
		 * Num. Fax del proveedor
		 * mail1
		 * mail2
		 * Fecha de pedido
		 * Numero de pedido
		 * Descripcion de forma de pago
		 * Plazo de entrega del pedido
		 * 
		 */
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		boolean insertado = true;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO sga_receipts ( ");
			cadenaSQL.append(" idPedido, ");
			cadenaSQL.append(" fecha, ");
			cadenaSQL.append(" estado, ");
			cadenaSQL.append(" receiptList ) VALUES (");
			cadenaSQL.append(" ?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getIdCodigoCompra()!=null)
			{
				preparedStatement.setInt(1, r_mapeo.getIdCodigoCompra());
			}
			else
			{
				preparedStatement.setInt(1, 0);
			}
			if (r_mapeo.getFecha()!=null)
			{
				preparedStatement.setString(2, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getEstado()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getEstado());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getReceiptList()!=null)
			{
				preparedStatement.setString(4, r_mapeo.getReceiptList());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			
			preparedStatement.executeUpdate();
			
			
			/*
			 * insertamos las lineas tras el exito de la cabecera
			 */
//			insertado=this.insertarLineas(r_mapeo);
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return false;
		}        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	
	
	public boolean insertarLineas(MapeoPedidosCompras r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaWhere =null;
		Boolean insertado = true;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT clave_tabla cl_clt, ");
        cadenaSQL.append(" documento cl_doc, ");
        cadenaSQL.append(" serie cl_ser, ");
        cadenaSQL.append(" codigo cl_cod, ");	    
        cadenaSQL.append(" ejercicio cl_eje, ");
        cadenaSQL.append(" a_cc_l_0.articulo cl_ar, ");
        cadenaSQL.append(" e_pro_ar.articulo_par cl_arp, ");
        cadenaSQL.append(" posicion cl_pos, ");
        cadenaSQL.append(" num_linea cl_lin, ");
        cadenaSQL.append(" a_cc_l_0.descrip_articulo cl_des, ");
        cadenaSQL.append(" e_pro_ar.descrip_articulo cl_desp, ");
        cadenaSQL.append(" cantidad cl_can, ");
        cadenaSQL.append(" unidades cl_ud, ");
        cadenaSQL.append(" precio cl_pre, ");
        cadenaSQL.append(" dto_unico cl_dto, ");
        cadenaSQL.append(" importe cl_imd, ");
        cadenaSQL.append(" importe_local cl_iml, ");
        cadenaSQL.append(" e_divisa.nemotecnico cl_div ");
        
     	cadenaSQL.append(" FROM a_cc_l_0, e_divisa, outer e_pro_ar ");
     	cadenaSQL.append(" where a_cc_l_0.receptor_emisor=e_pro_ar.proveedor ");
     	cadenaSQL.append(" and a_cc_l_0.articulo =e_pro_ar.articulo ");
     	cadenaSQL.append(" and a_cc_l_0.moneda =e_divisa.numero ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getClave_Tabla()!=null && r_mapeo.getClave_Tabla().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_Tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" serie= '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra().toString().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoCompra() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by clave_tabla, documento, serie, codigo ,posicion");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			conInsert= this.conManager.establecerConexionInd();	
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				MapeoLineasPedidosCompras mapeoLineas = new MapeoLineasPedidosCompras();
				mapeoLineas.setIdCodigo(r_mapeo.getIdCodigo());
				mapeoLineas.setClave_Tabla(r_mapeo.getClave_Tabla());
				mapeoLineas.setDocumento(r_mapeo.getDocumento());
				mapeoLineas.setSerie(r_mapeo.getSerie());
				mapeoLineas.setIdCodigoCompra(r_mapeo.getIdCodigoCompra());
				mapeoLineas.setEjercicio(r_mapeo.getEjercicio());
								
				if (rsOpcion.getString("cl_arp")!=null && rsOpcion.getString("cl_arp").trim().length()>0)
				{
					mapeoLineas.setArticulo(rsOpcion.getString("cl_arp"));
				}
				else
				{
					mapeoLineas.setArticulo(rsOpcion.getString("cl_ar"));
				}
				if (rsOpcion.getString("cl_desp")!=null && rsOpcion.getString("cl_desp").trim().length()>0)
				{
					mapeoLineas.setDescripcion(rsOpcion.getString("cl_desp"));
				}
				else
				{
					mapeoLineas.setDescripcion(rsOpcion.getString("cl_des"));
				}
				mapeoLineas.setCantidad(rsOpcion.getDouble("cl_can"));
				mapeoLineas.setUnidades(rsOpcion.getDouble("cl_ud"));
				mapeoLineas.setPrecio(rsOpcion.getDouble("cl_pre"));
				if (rsOpcion.getString("cl_dto")!=null && rsOpcion.getString("cl_dto").length()>0)
				{
					mapeoLineas.setDescuento(rsOpcion.getDouble("cl_dto"));
				}
				else
				{
					mapeoLineas.setDescuento(new Double(0));
				}
				mapeoLineas.setImporte(rsOpcion.getDouble("cl_imd"));
				mapeoLineas.setImporteLocal(rsOpcion.getDouble("cl_iml"));
				
				mapeoLineas.setMoneda(rsOpcion.getString("cl_div").substring(0, 3));
				r_mapeo.setMoneda(rsOpcion.getString("cl_div").substring(0, 3));
				
				mapeoLineas.setPosicion(rsOpcion.getInt("cl_pos"));
				mapeoLineas.setLinea(rsOpcion.getInt("cl_lin"));
				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_cc_l_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");
		        cadenaSQL.append(" ejercicio, ");
		        
		        cadenaSQL.append(" articulo, ");
		        cadenaSQL.append(" posicion, ");
		        cadenaSQL.append(" num_linea, ");
		        cadenaSQL.append(" descrip_articulo, ");
		        cadenaSQL.append(" cantidad, ");
		        cadenaSQL.append(" unidades, ");
		        cadenaSQL.append(" precio, ");
		        cadenaSQL.append(" dto_unico, ");
		        cadenaSQL.append(" importe, ");
		        cadenaSQL.append(" importe_local, ");
		        cadenaSQL.append(" moneda, ");
		        cadenaSQL.append(" idCodigo ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			    
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_Tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_Tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoCompra());
			    preparedStatement.setInt(5, r_mapeo.getEjercicio());
			    if (mapeoLineas.getArticulo()!=null)
			    {
			    	preparedStatement.setString(6, mapeoLineas.getArticulo());
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    preparedStatement.setInt(7, mapeoLineas.getPosicion());
			    preparedStatement.setInt(8, mapeoLineas.getLinea());

			    if (mapeoLineas.getDescripcion()!=null)
			    {
			    	preparedStatement.setString(9, RutinasCadenas.conversion(mapeoLineas.getDescripcion()));
			    }
			    else
			    {
			    	preparedStatement.setString(9, null);
			    }
			    preparedStatement.setDouble(10, mapeoLineas.getCantidad());
			    preparedStatement.setDouble(11, mapeoLineas.getUnidades());
			    preparedStatement.setDouble(12, mapeoLineas.getPrecio());
			    preparedStatement.setDouble(13, mapeoLineas.getDescuento());
			    preparedStatement.setDouble(14, mapeoLineas.getImporte());
			    preparedStatement.setDouble(15, mapeoLineas.getImporteLocal());
			    preparedStatement.setString(16, mapeoLineas.getMoneda());
			    preparedStatement.setInt(17, r_mapeo.getIdCodigo());

		        preparedStatement.executeUpdate();
		        preparedStatement.close();
			}
			
			insertado= this.insertarNotas(r_mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}

	public boolean insertarLineasSGA(MapeoLineasPedidosComprasSGA r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		Boolean insertado = true;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			cadenaSQL.append(" INSERT INTO sga_receipts_lin ( ");
			cadenaSQL.append(" articulo, ");
			cadenaSQL.append(" descripcion, ");
			cadenaSQL.append(" gtin, ");
			cadenaSQL.append(" qty, ");
			cadenaSQL.append(" qty_rec, ");
			cadenaSQL.append(" idPedido ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?) ");
			
			conInsert = this.conManager.establecerConexionInd();
			preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getGtin()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getGtin());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			preparedStatement.setDouble(4, r_mapeo.getUnidades());
			preparedStatement.setInt(5, r_mapeo.getUnidades_rec());
			preparedStatement.setInt(6, r_mapeo.getIdCodigoCompra());
			
			preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	
	public boolean insertarNotas(MapeoPedidosCompras r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		Boolean insertado = true;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT num_linea cn_lin, ");
		cadenaSQL.append(" tipo_dato cn_td, ");
        cadenaSQL.append(" texto cn_obs ");
        
     	cadenaSQL.append(" FROM e_pro_nt ");
     	cadenaSQL.append(" where e_pro_nt.proveedor = " + r_mapeo.getProveedor());
     	cadenaSQL.append(" and e_pro_nt.tipo_dato IN ('IE')" );
     	cadenaSQL.append(" order by tipo_dato, num_linea ");
     	
		try
		{

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			conInsert= this.conManager.establecerConexionInd();	
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				MapeoNotasPedidosCompras mapeoNotas = new MapeoNotasPedidosCompras();
				mapeoNotas.setIdCodigo(r_mapeo.getIdCodigo());
				mapeoNotas.setClave_Tabla(r_mapeo.getClave_Tabla());
				mapeoNotas.setDocumento(r_mapeo.getDocumento());
				mapeoNotas.setSerie(r_mapeo.getSerie());
				mapeoNotas.setIdCodigoCompra(r_mapeo.getIdCodigoCompra());
								
				mapeoNotas.setNumeroLinea(rsOpcion.getInt("cn_lin"));
				
				if (rsOpcion.getString("cn_obs")!=null && rsOpcion.getString("cn_obs").length()>0)
				{
					mapeoNotas.setObservaciones(rsOpcion.getString("cn_obs"));
				}
				else
				{
					mapeoNotas.setObservaciones(null);
				}

				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_cc_n_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");
		        
		        cadenaSQL.append(" num_linea, ");
		        cadenaSQL.append(" observaciones, ");
		        cadenaSQL.append(" idCodigo ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?) ");
			    
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_Tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_Tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoCompra());
			    preparedStatement.setInt(5, mapeoNotas.getNumeroLinea());

			    if (mapeoNotas.getObservaciones()!=null)
			    {
			    	preparedStatement.setString(6, RutinasCadenas.conversion(mapeoNotas.getObservaciones()));
			    }
			    else
			    {
			    	preparedStatement.setString(6, null);
			    }
			    preparedStatement.setInt(7, r_mapeo.getIdCodigo());

		        preparedStatement.executeUpdate();
		        preparedStatement.close();
			}
			
			insertado= this.insertarPies(r_mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			insertado= false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return insertado;
	}
	public boolean insertarPies(MapeoPedidosCompras r_mapeo)
	{
		/*
		 * Acumulado de importes de detalle en divisa (bruto)
		 * Nemotecnico de la moneda
		 * Notas de proveedor: solo pedidos
		 * 
		 */
		ResultSet rsOpcion = null;
		Connection conInsert = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT clave_tabla cp_clt, ");
        cadenaSQL.append(" documento cp_doc, ");
        cadenaSQL.append(" serie cp_ser, ");
        cadenaSQL.append(" codigo cp_cod, ");	    
        cadenaSQL.append(" importe cp_imd, ");
        cadenaSQL.append(" importe_local cp_iml ");
        
     	cadenaSQL.append(" FROM a_cc_p_0 ");
     	cadenaSQL.append(" where porimp='T' and orden_porimp=4 and orden = 1 ");
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getClave_Tabla()!=null && r_mapeo.getClave_Tabla().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClave_Tabla() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" serie= '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra().toString().length()>0)
				{
					cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = " + r_mapeo.getIdCodigoCompra() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			conInsert= this.conManager.establecerConexionInd();	
			
			while(rsOpcion.next())
			{
				/*
				 * relleno el mapeo de la linea procesada para insertarla en mi tabla
				 */
				MapeoResultadosPedidosCompras mapeoResultados = new MapeoResultadosPedidosCompras();
				mapeoResultados.setIdCodigo(r_mapeo.getIdCodigo());
				mapeoResultados.setClave_Tabla(r_mapeo.getClave_Tabla());
				mapeoResultados.setDocumento(r_mapeo.getDocumento());
				mapeoResultados.setSerie(r_mapeo.getSerie());
				mapeoResultados.setIdCodigoCompra(r_mapeo.getIdCodigoCompra());
				mapeoResultados.setImporte(rsOpcion.getDouble("cp_imd"));
				mapeoResultados.setImporteLocal(rsOpcion.getDouble("cp_iml"));
				
				mapeoResultados.setMoneda(r_mapeo.getMoneda());
				
				/*
				 * Inserto los datos en el mysql
				 */
				cadenaSQL= new StringBuffer();
				cadenaSQL.append(" INSERT INTO a_cc_p_0 ( ");
	    		cadenaSQL.append(" clave_tabla, ");
		        cadenaSQL.append(" documento, ");
		        cadenaSQL.append(" serie, ");
		        cadenaSQL.append(" codigo, ");
		        cadenaSQL.append(" moneda, ");
		        cadenaSQL.append(" importe, ");
		        cadenaSQL.append(" importe_local, ");
		        cadenaSQL.append(" idCodigo ) VALUES (");
			    cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
			    
			    preparedStatement = conInsert.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getClave_Tabla()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getClave_Tabla());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    if (r_mapeo.getDocumento()!=null)
			    {
			    	preparedStatement.setString(2, r_mapeo.getDocumento());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    if (r_mapeo.getSerie()!=null)
			    {
			    	preparedStatement.setString(3, r_mapeo.getSerie());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    preparedStatement.setInt(4, r_mapeo.getIdCodigoCompra());
			    preparedStatement.setString(5, mapeoResultados.getMoneda());
			    preparedStatement.setDouble(6, mapeoResultados.getImporte());
			    preparedStatement.setDouble(7, mapeoResultados.getImporteLocal());
			    preparedStatement.setInt(8, r_mapeo.getIdCodigo());

		        preparedStatement.executeUpdate();
		        preparedStatement.close();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return false;
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conInsert!=null)
				{
					conInsert.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	public boolean traerPedidoSGAMysql(MapeoPedidosCompras r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		Boolean borrado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();

		cadenaSQL.append(" SELECT CAB.ID as idPedido, ");
		cadenaSQL.append(" CAB.DTSVALIDFROM as fecha, ");
		cadenaSQL.append(" CAB.INBOUNDSTATUS as estado, ");
		cadenaSQL.append(" CAB.NAME as numeroPedido, ");
		
        cadenaSQL.append(" ART.NAME AS articulo, ");
        cadenaSQL.append(" ART.DESCRIPTION AS descripcion, ");
        cadenaSQL.append(" ART.EANCODE AS gtin, ");
        
        cadenaSQL.append(" PED.INPUTQTYTARGET as cantidad,");
        cadenaSQL.append(" PED.VALUE as RECIBIDO ");
        
        cadenaSQL.append(" FROM OBJT_INBOUNDORDER as CAB ");
        cadenaSQL.append(" left join OBJT_INBOUNDOPERATION as OPE ON OPE.INBOUNDORDEROID=CAB.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_INBOUNDITEMQTY AS PED ON PED.INBOUNDOPERATIONOID=OPE.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_ITEM as ART ON ART.OID = PED.ITEMOID ");
        
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" CAB.id = '" + r_mapeo.getIdCodigoCompra() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			
			/*
			 * conectamos con SQL real
			 * Buscamos el pedido y nos lo traemos para insertar los datos a imprimir en el mysql
			 * 
			 * tablaNueva a crear: pedidosSGA
			 * tablaNueva a crear: lineasPedidosSGA
			 * 
			 * posteriormente generaremos el informe de pedido de venta partiendo de estos datos y esta nueva tabla
			 * 
			 */
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			String cadenaSQLBorrado = null;
			Statement cd = null;
			
			
			while(rsOpcion.next())
			{
				/*
				 * Borramos los datos de este pedido en las tablas del mysql
				 */
				if (!borrado)
				{
					r_mapeo.setIdCodigo(r_mapeo.getIdCodigoCompra());
					conDelete = this.conManager.establecerConexionInd();
				
					cd = conDelete.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				
					cadenaSQLBorrado = " delete from sga_receipts_lin where idPedido = " + r_mapeo.getIdCodigoCompra();				
					cd.execute(cadenaSQLBorrado.toString());
				
					cadenaSQLBorrado = " delete from sga_receipts where idPedido = " + r_mapeo.getIdCodigoCompra();
					cd.execute(cadenaSQLBorrado.toString());

					conDelete=null;
					cd=null;
					borrado=true;
					
					/*
					 * Recojo los datos de la cabecera y los mando insertar
					 */
					MapeoPedidosComprasSGA mapeoPedidosComprasSGA = new MapeoPedidosComprasSGA();
					mapeoPedidosComprasSGA.setEstado(rsOpcion.getString("estado"));
					mapeoPedidosComprasSGA.setIdCodigoCompra(rsOpcion.getInt("idPedido"));
					mapeoPedidosComprasSGA.setReceiptList(rsOpcion.getString("numeroPedido"));
					mapeoPedidosComprasSGA.setFecha(rsOpcion.getDate("fecha"));
					
					insertado = this.insertarCabeceraSGA(mapeoPedidosComprasSGA);
				}
				/*
				 * recojo los datos de las lineas y las mando insertar.
				 */
				if (insertado)
				{
					MapeoLineasPedidosComprasSGA mapeoLineas = new MapeoLineasPedidosComprasSGA();
					mapeoLineas.setArticulo(rsOpcion.getString("articulo"));
					mapeoLineas.setDescripcion(rsOpcion.getString("descripcion"));
					if (rsOpcion.getString("gtin")==null || rsOpcion.getString("gtin").length()==0)
					{
						insertado = false;
						break;
					}
					mapeoLineas.setGtin(rsOpcion.getString("gtin"));
					mapeoLineas.setUnidades(rsOpcion.getDouble("cantidad"));
					mapeoLineas.setUnidades_rec(rsOpcion.getInt("recibido"));
					mapeoLineas.setIdCodigoCompra(rsOpcion.getInt("idPedido"));
					
					insertado = this.insertarLineasSGA(mapeoLineas);
				}
				else
				{
					break;
				}
			}
			/*
			 * inserto de nuevo el pedido
			 */
			/*
			 * devuelvo verdadero
			 */
			return insertado;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	public String generarInforme(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(codigoInterno);
    	libImpresion.setArchivoDefinitivo("/pedido - " + numeroDocumento.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
    	libImpresion.setArchivoPlantilla("impresoPedido.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle(null);
//    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaDetalle("impresoPedido_subreport0.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("impresoPedido_subreport0.jasper");
    	libImpresion.setArchivoPlantillaNotas("impresoPedido_notas.jrxml");
    	libImpresion.setArchivoPlantillaNotasCompilado("impresoPedido_notas.jasper");
//    	libImpresion.setArchivoPlantillaNotas(null);
//    	libImpresion.setArchivoPlantillaNotasCompilado(null);
    	libImpresion.setCarpeta("pedidosCompra");
    	libImpresion.setBackGround("/fondoA4PedidoCompra.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo(); 
    	
    	return resultadoGeneracion;
	}

	public String generarInformeSGA(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion=new LibreriaImpresion();
		
		libImpresion.setCodigo(codigoInterno);
		libImpresion.setArchivoDefinitivo("/pedido - " + numeroDocumento.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("impresoPedidoSGA.jrxml");
//    	libImpresion.setArchivoPlantillaDetalle(null);
//    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaDetalle("impresoPedido_subreportGSYS.jrxml");
		libImpresion.setArchivoPlantillaDetalleCompilado("impresoPedido_subreportGSYS.jasper");
		libImpresion.setArchivoPlantillaNotas("impresoPedido_subreportSGA.jrxml");
		libImpresion.setArchivoPlantillaNotasCompilado("impresoPedido_subreportSGA.jasper");
//    	libImpresion.setArchivoPlantillaNotas(null);
//    	libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("pedidosCompra");
		libImpresion.setBackGround("/fondoA4LogoBlanco.jpg");
		
		resultadoGeneracion=libImpresion.generacionFinal();
		
		
		if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo(); 
		
		return resultadoGeneracion;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(a_cc_c_0.idCodigo) opc_sig ");
     	cadenaSQL.append(" FROM a_cc_c_0 ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("opc_sig")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 0;
	}

	public boolean actualizarCabeceraGreensys(MapeoPedidosCompras r_mapeo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		Connection con = null;	

		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		String ejer = "0";
		
		if (r_mapeo!=null && r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
		{
			ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - r_mapeo.getEjercicio());
		}

		try
		{
	        
			cadenaSQL.append(" UPDATE a_cc_c_" + ejer + " set ");
    		cadenaSQL.append(" obs_envio2 = ? ");
	        cadenaSQL.append(" where clave_tabla = ? ");
	        cadenaSQL.append(" and documento = ? ");
	        cadenaSQL.append(" and serie = ? ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getEnviado());
		    
		    if (r_mapeo.getClave_Tabla()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClave_Tabla());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigoCompra());

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	public boolean comprobarPedidoPendiente(String r_codigo)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL =null;
		Connection con = null;	
		Statement cs = null;
		
		String ejer = "0";
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" select codigo from a_cc_c_" + ejer );
			cadenaSQL.append(" where codigo = " + new Integer(r_codigo));
			cadenaSQL.append(" and cumplimentacion is null" );
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return true;
			}			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return false;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}
	
	public String semaforo()
	{
		ResultSet rsOpcion = null;
		String errores = "0";
		StringBuffer cadenaSQL =null;
		Connection con = null;	
		Statement cs = null;

		
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento cc_dc, ");
	        cadenaSQL.append(" a.serie cc_sr, ");
	        cadenaSQL.append(" a.codigo cc_cd, ");	    
	        cadenaSQL.append(" a.fecha_documento cc_fec, ");
	        cadenaSQL.append(" a.ejercicio cc_ej, ");
	        cadenaSQL.append(" a.proveedor cc_pr, ");
	        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" a.obs_envio2 cc_se, ");        
	        cadenaSQL.append(" f.texto pr_mai, ");
	        cadenaSQL.append(" g.texto pr_mai2, ");        
	        cadenaSQL.append(" h.texto pr_mai3, ");
	        cadenaSQL.append(" a.direccion cc_dir, ");
	        cadenaSQL.append(" a.poblacion cc_pob, ");
	        cadenaSQL.append(" a.cod_postal cc_cp, ");
	        cadenaSQL.append(" a.plazo_entrega cc_pe, ");
	        cadenaSQL.append(" e.fax cc_fax, ");
	        cadenaSQL.append(" c.nombre cc_pa, ");
	        cadenaSQL.append(" d.nombre cc_pv, ");
	        cadenaSQL.append(" b.descripcion cc_fp ");
	        
	     	cadenaSQL.append(" FROM a_cc_c_0 a, e_frmpag b, e_paises c, e_provin d, outer e_provee e, outer e_pro_nt f, outer e_pro_nt g, outer e_pro_nt h ");
	     	cadenaSQL.append(" where e.proveedor = a.proveedor ");
	     	cadenaSQL.append(" and c.pais = a.pais ");
	     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
	     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
	     	cadenaSQL.append(" and f.proveedor = a.proveedor and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
	     	cadenaSQL.append(" and g.proveedor = a.proveedor and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
	     	cadenaSQL.append(" and h.proveedor = a.proveedor and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
	     	
	     	
	     	cadenaSQL.append(" union ");
	     	
	    	cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento cc_dc, ");
	        cadenaSQL.append(" a.serie cc_sr, ");
	        cadenaSQL.append(" a.codigo cc_cd, ");	    
	        cadenaSQL.append(" a.fecha_documento cc_fec, ");
	        cadenaSQL.append(" a.ejercicio cc_ej, ");
	        cadenaSQL.append(" a.proveedor cc_pr, ");
	        cadenaSQL.append(" a.nombre_comercial cc_nc, ");
	        cadenaSQL.append(" a.obs_envio2 cc_se, ");        
	        cadenaSQL.append(" f.texto pr_mai, ");
	        cadenaSQL.append(" g.texto pr_mai2, ");        
	        cadenaSQL.append(" h.texto pr_mai3, ");
	        cadenaSQL.append(" a.direccion cc_dir, ");
	        cadenaSQL.append(" a.poblacion cc_pob, ");
	        cadenaSQL.append(" a.cod_postal cc_cp, ");
	        cadenaSQL.append(" a.plazo_entrega cc_pe, ");
	        cadenaSQL.append(" e.fax cc_fax, ");
	        cadenaSQL.append(" c.nombre cc_pa, ");
	        cadenaSQL.append(" d.nombre cc_pv, ");
	        cadenaSQL.append(" b.descripcion cc_fp ");
	        
	     	cadenaSQL.append(" FROM a_cc_c_1 a, e_frmpag b, e_paises c, e_provin d, outer e_provee e, outer e_pro_nt f, outer e_pro_nt g, outer e_pro_nt h ");
	     	cadenaSQL.append(" where e.proveedor = a.proveedor ");
	     	cadenaSQL.append(" and c.pais = a.pais ");
	     	cadenaSQL.append(" and d.pais = a.pais and d.provincia = a.provincia ");
	     	cadenaSQL.append(" and b.codigo = a.fpago_aplazado ");     	
	     	cadenaSQL.append(" and f.proveedor = a.proveedor and f.tipo_dato = 'EM' and f.subtipo_dato = '9O' and f.num_linea = 1 ");
	     	cadenaSQL.append(" and g.proveedor = a.proveedor and g.tipo_dato = 'EM' and g.subtipo_dato = '9O' and g.num_linea = 2 ");
	     	cadenaSQL.append(" and h.proveedor = a.proveedor and h.tipo_dato = 'EM' and h.subtipo_dato = '9O' and h.num_linea = 3 ");
	     	
	     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				
				MapeoPedidosCompras mapeoPedidosCompras = new MapeoPedidosCompras();
				/*
				 * recojo mapeo operarios
				 */
				mapeoPedidosCompras.setEjercicio(rsOpcion.getInt("cc_ej"));
				mapeoPedidosCompras.setClave_Tabla(rsOpcion.getString("cc_cl"));
				mapeoPedidosCompras.setDocumento(rsOpcion.getString("cc_dc"));
				mapeoPedidosCompras.setSerie(rsOpcion.getString("cc_sr"));
				mapeoPedidosCompras.setIdCodigoCompra(rsOpcion.getInt("cc_cd"));
				mapeoPedidosCompras.setNombre(RutinasCadenas.conversion(rsOpcion.getString("cc_nc")));
				mapeoPedidosCompras.setMail(RutinasCadenas.conversion(rsOpcion.getString("pr_mai")));
				mapeoPedidosCompras.setFecha(rsOpcion.getDate("cc_fec"));
				mapeoPedidosCompras.setMail2(RutinasCadenas.conversion(rsOpcion.getString("pr_mai2")));
				mapeoPedidosCompras.setMail3(RutinasCadenas.conversion(rsOpcion.getString("pr_mai3")));
				if ((rsOpcion.getString("pr_mai") == null || rsOpcion.getString("pr_mai").length()==0) && (rsOpcion.getString("pr_mai2")==null || rsOpcion.getString("pr_mai2").length()==0) && (rsOpcion.getString("pr_mai3")==null || rsOpcion.getString("pr_mai3").length()==0)) 
				{
					mapeoPedidosCompras.setEnviar("--------");
				}
				if (rsOpcion.getString("cc_se") == null || rsOpcion.getString("cc_se").length()==0)
				{
					mapeoPedidosCompras.setEnviado("");
				}
				else
				{
					mapeoPedidosCompras.setEnviado(rsOpcion.getString("cc_se").trim().toUpperCase());
				}
				mapeoPedidosCompras.setProveedor(rsOpcion.getInt("cc_pr"));
				mapeoPedidosCompras.setDireccion(RutinasCadenas.conversion(rsOpcion.getString("cc_dir")));
				mapeoPedidosCompras.setPoblacion(RutinasCadenas.conversion(rsOpcion.getString("cc_pob")));
				mapeoPedidosCompras.setCp(rsOpcion.getString("cc_cp"));
				mapeoPedidosCompras.setPlazoEntrega(rsOpcion.getDate("cc_pe"));
				mapeoPedidosCompras.setFax(rsOpcion.getString("cc_fax"));
				mapeoPedidosCompras.setPais(RutinasCadenas.conversion(rsOpcion.getString("cc_pa")));
				mapeoPedidosCompras.setProvincia(RutinasCadenas.conversion(rsOpcion.getString("cc_pv")));
				mapeoPedidosCompras.setFormaPago(RutinasCadenas.conversion(rsOpcion.getString("cc_fp")));
				
				if (mapeoPedidosCompras.getEnviado().equals("") && !mapeoPedidosCompras.getEnviar().equals("--------"))
				{
					errores="2";
					break;
				}
				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return errores;
	}
	
	public String semaforoPlazos()
	{
		ResultSet rsOpcion = null;
		String errores = "0";
		StringBuffer cadenaSQL =null;
		Connection con = null;	
		Statement cs = null;

		
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento cc_dc, ");
	        cadenaSQL.append(" a.serie cc_sr, ");
	        cadenaSQL.append(" a.codigo cc_cd ");
	        
	     	cadenaSQL.append(" FROM a_cc_l_0 a ");
	     	cadenaSQL.append(" where a.plazo_entrega < today ");
	     	cadenaSQL.append(" and a.cumplimentacion is null ");
	     	cadenaSQL.append(" AND a.clave_movimiento <> 'O' ");
	     	
	     	cadenaSQL.append(" union ");
	     	
	     	cadenaSQL.append(" SELECT a.clave_tabla cc_cl, ");
	        cadenaSQL.append(" a.documento cc_dc, ");
	        cadenaSQL.append(" a.serie cc_sr, ");
	        cadenaSQL.append(" a.codigo cc_cd ");
	        
	     	cadenaSQL.append(" FROM a_cc_l_1 a ");
	     	cadenaSQL.append(" where a.plazo_entrega < today ");
	     	cadenaSQL.append(" and a.cumplimentacion is null ");
	     	cadenaSQL.append(" AND a.clave_movimiento <> 'O' ");
	     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				errores="2";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return errores;
	}
	
	public MapeoMPPrimerPlazo traerPrimerPlazo(Connection r_con, String r_articulo)
	{
		Statement cs = null;
	
		ResultSet rsOpcion = null;		
		MapeoMPPrimerPlazo mapeoPlazo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT min(p.plazo_entrega) det_plz, c.nombre_comercial det_nom ");
     	cadenaSQL.append(" FROM a_cc_l_0 p, a_cc_c_0 c ");
     	cadenaSQL.append(" WHERE c.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND c.documento = p.documento ");
     	cadenaSQL.append(" AND c.serie = p.serie ");
     	cadenaSQL.append(" AND c.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null "); 
     	cadenaSQL.append(" AND p.articulo = '" + r_articulo.trim() + "' ");
     	cadenaSQL.append(" group by c.nombre_comercial ");
     	
     	cadenaSQL.append(" union ");
     	
     	cadenaSQL.append(" SELECT min(p.plazo_entrega) det_plz, c.nombre_comercial det_nom ");
     	cadenaSQL.append(" FROM a_cc_l_1 p, a_cc_c_1 c ");
     	cadenaSQL.append(" WHERE c.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND c.documento = p.documento ");
     	cadenaSQL.append(" AND c.serie = p.serie ");
     	cadenaSQL.append(" AND c.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null "); 
     	cadenaSQL.append(" AND p.articulo = '" + r_articulo.trim() + "' ");
     	cadenaSQL.append(" group by c.nombre_comercial ");
     	
     	cadenaSQL.append(" order by 1 "); 
     	
		try
		{
			cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoPlazo = new MapeoMPPrimerPlazo();
				
				mapeoPlazo.setPrimerPlazo(rsOpcion.getDate("det_plz"));
				mapeoPlazo.setNombre(rsOpcion.getString("det_nom"));
				
				return mapeoPlazo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public ArrayList<MapeoLineasPedidosCompras> recuperarPedidoGreensys(String r_articulo, Integer r_stock)
	{
		Connection con = null;	
		Statement cs = null;
		
		ResultSet rsOpcion = null;		
		Integer stockCalculado = null;
		ArrayList<MapeoLineasPedidosCompras> vector = null;
		MapeoLineasPedidosCompras mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		stockCalculado=r_stock;
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");
        cadenaSQL.append(" c.proveedor det_pro, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_cc_l_0 p, e_provee c, a_cc_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
     	cadenaSQL.append(" AND ((p.cumplimentacion Is Null) "); 
     	cadenaSQL.append(" AND (p.articulo = '" + r_articulo + "'))");
     	
//     	cadenaSQL.append(" union ");
//     	
//    	cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
//		cadenaSQL.append(" p.fecha_documento det_fec, ");
//        cadenaSQL.append(" p.plazo_entrega det_plz, ");
//        cadenaSQL.append(" c.proveedor det_pro, ");
//        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
//        cadenaSQL.append(" p.documento det_doc, ");
//        cadenaSQL.append(" p.codigo det_cod, ");
//        cadenaSQL.append(" p.articulo det_art, ");
//        cadenaSQL.append(" p.descrip_articulo det_des, ");
//        cadenaSQL.append(" p.unidades det_uds, ");
//        cadenaSQL.append(" p.unidades_serv det_ser, ");
//        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
//        cadenaSQL.append(" p.almacen det_alm ");
//     	cadenaSQL.append(" FROM a_cc_l_1 p, e_provee c, a_cc_c_1 a ");
//     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
//     	cadenaSQL.append(" AND a.documento = p.documento ");
//     	cadenaSQL.append(" AND a.serie = p.serie ");
//     	cadenaSQL.append(" AND a.codigo = p.codigo ");
//     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
//     	cadenaSQL.append(" AND ((p.cumplimentacion Is Null) "); 
//     	cadenaSQL.append(" AND (p.articulo = '" + r_articulo + "'))");
     	
     	
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosCompras();
				mapeo.setAlmacen(rsOpcion.getInt("det_alm"));
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNombreProveedor(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
				mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
				mapeo.setStock_queda(stockCalculado + rsOpcion.getInt("det_pte"));
				
				stockCalculado = stockCalculado + rsOpcion.getInt("det_pte"); 
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasPedidosCompras> recuperarLineasPedidoGreensys(MapeoLineasPedidosCompras r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasPedidosCompras> vector = null;
		MapeoLineasPedidosCompras mapeo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");
        cadenaSQL.append(" c.proveedor det_pro, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
        cadenaSQL.append(" p.num_linea det_lin, ");
        cadenaSQL.append(" p.almacen det_alm ");
     	cadenaSQL.append(" FROM a_cc_l_0 p, e_provee c, a_cc_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
     	cadenaSQL.append(" AND p.codigo = " + r_mapeo.getIdCodigoCompra());
     	
//     	cadenaSQL.append(" union ");
//     	
//    	cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
//		cadenaSQL.append(" p.fecha_documento det_fec, ");
//        cadenaSQL.append(" p.plazo_entrega det_plz, ");
//        cadenaSQL.append(" c.proveedor det_pro, ");
//        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
//        cadenaSQL.append(" p.documento det_doc, ");
//        cadenaSQL.append(" p.codigo det_cod, ");
//        cadenaSQL.append(" p.articulo det_art, ");
//        cadenaSQL.append(" p.descrip_articulo det_des, ");
//        cadenaSQL.append(" p.unidades det_uds, ");
//        cadenaSQL.append(" p.unidades_serv det_ser, ");
//        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
//        cadenaSQL.append(" p.num_linea det_lin, ");
//        cadenaSQL.append(" p.almacen det_alm ");
//     	cadenaSQL.append(" FROM a_cc_l_1 p, e_provee c, a_cc_c_1 a ");
//     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
//     	cadenaSQL.append(" AND a.documento = p.documento ");
//     	cadenaSQL.append(" AND a.serie = p.serie ");
//     	cadenaSQL.append(" AND a.codigo = p.codigo ");
//     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
//     	cadenaSQL.append(" AND p.codigo = " + r_mapeo.getIdCodigoCompra());
     	
     	
     	cadenaSQL.append(" order by 13 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosCompras();
				mapeo.setAlmacen(rsOpcion.getInt("det_alm"));
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNombreProveedor(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod")); 
				mapeo.setLinea(rsOpcion.getInt("det_lin")); 
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
				mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	public ArrayList<MapeoLineasPedidosCompras> recuperarLineasPedidoAlbaranGreensys(MapeoLineasPedidosCompras r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasPedidosCompras> vector = null;
		MapeoLineasPedidosCompras mapeo = null;
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_mapeo.getEjercicio()== ejercicioActual) digito="0";
		if (r_mapeo.getEjercicio() < ejercicioActual) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
		cadenaSQL.append(" p.plazo_entrega det_plz, ");
		cadenaSQL.append(" c.proveedor det_pro, ");
		cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
		cadenaSQL.append(" p.documento det_doc, ");
		cadenaSQL.append(" p.codigo det_cod, ");
		cadenaSQL.append(" p.articulo det_art, ");
		cadenaSQL.append(" p.descrip_articulo det_des, ");
		cadenaSQL.append(" p.unidades det_uds, ");
		cadenaSQL.append(" p.unidades_serv det_ser, ");
		cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte, ");
		cadenaSQL.append(" p.num_linea det_lin ");
		cadenaSQL.append(" FROM a_cc_l_" + digito + " p, e_provee c, a_cc_c_" + digito + " a ");
		cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
		cadenaSQL.append(" AND a.documento = p.documento ");
		cadenaSQL.append(" AND a.serie = p.serie ");
		cadenaSQL.append(" AND a.codigo = p.codigo ");
		cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
		cadenaSQL.append(" AND p.codigo in (select num_origen from a_lin_"+ digito);
		cadenaSQL.append(" where codigo = " + r_mapeo.getIdCodigoCompra());
		cadenaSQL.append(" and clave_Tabla in ('E','M'))");
		
		cadenaSQL.append(" order by 13 ");
		
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosCompras();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNombreProveedor(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
				mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public ArrayList<MapeoLineasPedidosCompras> recuperarPedidoVencidos()
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasPedidosCompras> vector = null;
		MapeoLineasPedidosCompras mapeo = null;
		Connection con = null;	
		Statement cs = null;

		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");
        cadenaSQL.append(" c.proveedor det_pro, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte ");
     	cadenaSQL.append(" FROM a_cc_l_0 p, e_provee c, a_cc_c_0 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null "); 
     	cadenaSQL.append(" AND p.clave_movimiento <> 'O' ");
     	cadenaSQL.append(" and p.plazo_entrega < today ");

     	cadenaSQL.append(" union ");
     	
     	cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.plazo_entrega det_plz, ");
        cadenaSQL.append(" c.proveedor det_pro, ");
        cadenaSQL.append(" c.nombre_comercial det_nom, ");	    
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.unidades_serv det_ser, ");
        cadenaSQL.append(" (p.unidades - p.unidades_serv) det_pte ");
     	cadenaSQL.append(" FROM a_cc_l_1 p, e_provee c, a_cc_c_1 a ");
     	cadenaSQL.append(" WHERE a.clave_tabla = p.clave_tabla ");
     	cadenaSQL.append(" AND a.documento = p.documento ");
     	cadenaSQL.append(" AND a.serie = p.serie ");
     	cadenaSQL.append(" AND a.codigo = p.codigo ");
     	cadenaSQL.append(" AND p.receptor_emisor = c.proveedor ");
     	cadenaSQL.append(" AND p.cumplimentacion Is Null "); 
     	cadenaSQL.append(" AND p.clave_movimiento <> 'O' ");
     	cadenaSQL.append(" and p.plazo_entrega < today ");
     	
     	cadenaSQL.append(" order by 8,3 ");
     	
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasPedidosCompras>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasPedidosCompras();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setPlazoEntrega(rsOpcion.getDate("det_plz"));
				mapeo.setProveedor(rsOpcion.getInt("det_pro"));
				mapeo.setNombreProveedor(rsOpcion.getString("det_nom"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				mapeo.setUnidades_serv(rsOpcion.getInt("det_ser"));
				mapeo.setUnidades_pdte(rsOpcion.getInt("det_pte"));
				mapeo.setStock_queda(new Integer(0));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public HashMap<String,String> obtenerPlazosEntrega()
	{
		StringBuffer cadenaSQL2 = new StringBuffer();
		ResultSet rsPlazos =null;
		HashMap<String,String> plazosObtenidos = null;
		Connection con = null;
		Statement cs = null;		

		try
		{
			cadenaSQL2.append(" SELECT min(a_cc_l_0.plazo_entrega) ped_fec, ");	    
			cadenaSQL2.append(" a_cc_l_0.articulo ped_art ");
			cadenaSQL2.append(" FROM a_cc_l_0 ");
	     	cadenaSQL2.append(" where a_cc_l_0.cumplimentacion is null ");
	     	cadenaSQL2.append(" group by 2 ");
	
	     	con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	     	
	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL2.toString() + " : " + new Date());
			rsPlazos = cs.executeQuery(cadenaSQL2.toString());
	
			plazosObtenidos = new HashMap<String, String>();
			
			while(rsPlazos.next())
			{
				plazosObtenidos.put(rsPlazos.getString("ped_art"), rsPlazos.getString("ped_fec"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsPlazos!=null)
				{
					rsPlazos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return plazosObtenidos;
	}
	
	public HashMap<String,String> obtenerConfirmacionPlazos()
	{
		StringBuffer cadenaSQL2 = new StringBuffer();
		ResultSet rsPlazos =null;
		HashMap<String,String> plazosObtenidos = null;
		Connection con = null;
		Statement cs = null;		

		try
		{
			cadenaSQL2.append(" SELECT min(a_cc_l_0.plazo_entrega) ped_fec, ");
			cadenaSQL2.append(" a_cc_l_0.articulo ped_art, ");
			cadenaSQL2.append(" a_cc_c_0.ref_proveedor ped_conf ");
//	     	cadenaSQL2.append(" FROM a_cc_l_0 , a_cc_c_0, green.e__lconj b");
	     	cadenaSQL2.append(" FROM a_cc_l_0 , a_cc_c_0 ");
	     	cadenaSQL2.append(" where a_cc_l_0.clave_tabla = a_cc_c_0.clave_tabla ");
	     	cadenaSQL2.append(" and a_cc_l_0.documento = a_cc_c_0.documento ");
	     	cadenaSQL2.append(" and a_cc_l_0.serie = a_cc_c_0.serie ");
	     	cadenaSQL2.append(" and a_cc_l_0.codigo = a_cc_c_0.codigo ");
//	     	cadenaSQL2.append(" and a_cc_l_0.articulo = b.hijo " );
	     	cadenaSQL2.append(" and a_cc_l_0.cumplimentacion is null ");
	     	cadenaSQL2.append(" group by 2,3 ");
	
	     	con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	     	
	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL2.toString() + " : " + new Date());
			rsPlazos = cs.executeQuery(cadenaSQL2.toString());
	
			plazosObtenidos = new HashMap<String, String>();
			
			while(rsPlazos.next())
			{
				plazosObtenidos.put(rsPlazos.getString("ped_art"), rsPlazos.getString("ped_conf"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsPlazos!=null)
				{
					rsPlazos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return plazosObtenidos;
	}
	
	public ArrayList<String> recuperarPedidosPendientesCumplimentar(String r_ejercicio, String r_documento)
	{
		ArrayList<String> strDocumentos= null;
		ResultSet rsRecepcion = null;
		Connection con = null;
		Statement cs = null;

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());

		StringBuffer cadenaSql = new StringBuffer();
		try
		{
			strDocumentos = new ArrayList<String>();
			
			cadenaSql.append(" select distinct a_cc_c_0.codigo ");
			cadenaSql.append(" from a_cc_c_0 ");
			cadenaSql.append(" where a_cc_c_0.ejercicio = '" + r_ejercicio + "' ");
			cadenaSql.append(" and a_cc_c_0.documento = '" + r_documento+ "' ");
			cadenaSql.append(" and a_cc_c_0.cumplimentacion is null ");
		
			if (eBorsao.get().accessControl.getNombre().toUpperCase().contains("CAPU"))
			{
				cadenaSql.append(" and a_cc_c_0.almacen in (" + cas.capuchinos + ") ");
			}
			else
			{
				cadenaSql.append(" and a_cc_c_0.almacen not in (" + cas.restoAlmacenes + ") ");
			}
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsRecepcion= cs.executeQuery(cadenaSql.toString());
			while(rsRecepcion.next())
			{
				strDocumentos.add(rsRecepcion.getString("codigo"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsRecepcion!=null)
				{
					rsRecepcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return strDocumentos;
	}
}