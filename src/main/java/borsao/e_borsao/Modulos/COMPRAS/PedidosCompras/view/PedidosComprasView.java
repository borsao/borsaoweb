package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PedidosComprasView extends GridViewRefresh {

	public static final String VIEW_NAME = "Pedidos Compras";
	private final String titulo = "Pedidos de Compras";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
//	private MapeoPedidosCompras mapeoPedidosCompras =null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoPedidosCompras> r_vector=null;
//    	hashToMapeo hm = new hashToMapeo();
    	
//    	MapeoPedidosCompras=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaPedidosComprasServer cus = new consultaPedidosComprasServer(CurrentUser.get());
    	r_vector=cus.datosOpcionesGlobal(null);
    	
    	grid = new OpcionesGrid(r_vector);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PedidosComprasView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
//        this.form = new OpcionesForm(this);
//        this.verForm(true);
        
//        addComponent(this.form);
    }

    public void newForm()
    {
//    	if (grid.getEditedItemId()==null)
//    	{
//			this.mapeoPedidosCompras=new MapeoPedidosCompras();
//			mapeoPedidosCompras.setTurno("");
//			mapeoPedidosCompras.setDescripcion("");
//			mapeoPedidosCompras.setEliminar(false);
//			mapeoPedidosCompras.setHoras(0);
//			grid.getContainerDataSource().addItem(mapeoPedidosCompras);    	
//			grid.editItem(mapeoPedidosCompras);
//    	}
    	
//    	this.form.setCreacion(true);
//    	this.form.setBusqueda(false);
//    	this.form.btnGuardar.setCaption("Guardar");
//    	this.form.btnEliminar.setEnabled(false);    	
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
//    	this.form.setBusqueda(r_busqueda);
//    	this.form.setCreacion(false);
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
//    	this.form.btnGuardar.setCaption("Buscar");
//    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
//    	this.mapeoPedidosCompras = (MapeoPedidosCompras) r_fila;
//    	this.form.editarOpcion((MapeoPedidosCompras) r_fila);
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
//    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }
    
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
