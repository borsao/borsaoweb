package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventarioLote;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.server.consultaEtiquetasRecepcionServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoLineasPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo.MapeoPedidosCompras;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasRecepcion extends Ventana
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbEjercicio = null;
	private ComboBox cmbDocumento = null;
	private Label nombreProveedor = null;
	private Label tituloNombreProveedor= null;
	private ComboBox cmbArea = null;
	private ComboBox cmbPedido = null;
	private TextField txtArticulo = null;
	private Label nombreReferencia = null;
	private Label tituloNombreReferencia= null;
	private ArrayList<String> datos = null; 
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private VerticalLayout  principal=null;
	private HorizontalLayout  botonera=null;
	private CheckBox embotelladoEtiquetado=null;
		
	private consultaEtiquetasRecepcionServer ceps = null;
	private EtiquetasRecepcionView origen = null;
	
	public PeticionEtiquetasRecepcion(EtiquetasRecepcionView r_view)
	{
		ArrayList<String> datos = new ArrayList<String>();
		ceps = new consultaEtiquetasRecepcionServer(CurrentUser.get());
		this.origen=r_view;
		
//		setWindowMode(WindowMode.MAXIMIZED);
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbEjercicio=new ComboBox("Ejercicio");
				this.cmbEjercicio.addStyleName(ValoTheme.COMBOBOX_TINY);

					datos = this.ceps.recuperarEjerciciosRecepcion();
					Iterator<String> iter = datos.iterator();
					while (iter.hasNext())
					{
						String valor = iter.next();
						this.cmbEjercicio.addItem(valor);
					}
				
				this.cmbEjercicio.setInvalidAllowed(false);
				this.cmbEjercicio.setNewItemsAllowed(false);
				this.cmbEjercicio.setNullSelectionAllowed(false);
				this.cmbEjercicio.setRequired(true);
				
				this.cmbEjercicio.setWidth("120px");

				this.cmbArea = new ComboBox("Area");
				this.cmbArea.setWidth("100px");
				this.cmbArea.setRequired(true);
				this.cmbArea.setNullSelectionAllowed(false);
				this.cmbArea.setInvalidAllowed(false);
				this.cmbArea.setNewItemsAllowed(false);
				this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);
				this.cmbArea.addItem("Inventario");
				this.cmbArea.addItem("Recepciones");

				this.cmbDocumento = new ComboBox("Documentos");
				this.cmbDocumento.setWidth("100px");
				this.cmbDocumento.setRequired(true);
				this.cmbDocumento.setNullSelectionAllowed(false);
				this.cmbDocumento.setInvalidAllowed(false);
				this.cmbDocumento.setNewItemsAllowed(false);
				this.cmbDocumento.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.cmbPedido = new ComboBox("Pedidos");
				this.cmbPedido.setWidth("120px");
				this.cmbPedido.setRequired(true);
				this.cmbPedido.setNullSelectionAllowed(false);
				this.cmbPedido.setInvalidAllowed(false);
				this.cmbPedido.setNewItemsAllowed(false);
				this.cmbPedido.addStyleName(ValoTheme.COMBOBOX_TINY);
				
				this.txtArticulo= new TextField("Articulo");
				this.txtArticulo.setWidth("150px");
				this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtArticulo.setVisible(false);

				embotelladoEtiquetado = new CheckBox("Es Embotellado?");
				this.embotelladoEtiquetado.addStyleName(ValoTheme.CHECKBOX_SMALL);
				this.embotelladoEtiquetado.setVisible(false);
				this.embotelladoEtiquetado.setValue(false);
				
				fila1.addComponent(cmbEjercicio);
				fila1.addComponent(cmbArea);
				fila1.addComponent(cmbDocumento);
				fila1.addComponent(cmbPedido);
				fila1.addComponent(txtArticulo);
				fila1.addComponent(embotelladoEtiquetado);
				fila1.setComponentAlignment(embotelladoEtiquetado, Alignment.BOTTOM_LEFT);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.tituloNombreProveedor= new Label("PROVEEDOR: ");
				this.tituloNombreProveedor.setWidth("75px");
				this.tituloNombreProveedor.addStyleName(ValoTheme.LABEL_TINY);

				this.nombreProveedor= new Label();
				this.nombreProveedor.setWidth("500px");

				fila3.addComponent(tituloNombreProveedor);
				fila3.addComponent(nombreProveedor);
				
				this.tituloNombreReferencia= new Label("");
				this.tituloNombreReferencia.setWidth("75px");
				this.tituloNombreReferencia.addStyleName(ValoTheme.LABEL_TINY);

				this.nombreReferencia= new Label();
				this.nombreReferencia.setWidth("500px");
				

				fila3.addComponent(tituloNombreReferencia);
				fila3.addComponent(nombreReferencia);

		controles.addComponent(fila1);
		controles.addComponent(fila3);
		
		botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("1250px");
		this.setHeight("750px");
		this.center();
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cmbEjercicio.focus();
		this.cargarValoresPorDefecto();
	}

	private void cargarValoresPorDefecto()
	{
		this.cmbEjercicio.setValue(RutinasFechas.añoActualYYYY().trim());
		this.cmbDocumento.setValue("C1");
		this.cmbArea.setValue("Recepciones");
	}
	
	private void cargarListeners()
	{

		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
		ValueChangeListener lisEj = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbEjercicio.getValue()!=null && cmbEjercicio.getValue()!="")
				{
					
					cmbDocumento.removeAllItems();
					
					datos = ceps.recuperarDocumentosRecepcion(cmbEjercicio.getValue().toString());
					
					Iterator<String> iter = datos.iterator();
					
					while (iter.hasNext())
					{
						String valor = iter.next();
						cmbDocumento.addItem(valor);
					}
					cmbDocumento.setValue("C1");
				}
			}
		};
		
		cmbEjercicio.addValueChangeListener(lisEj);
	
		ValueChangeListener lisDoc = new ValueChangeListener() {
		
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbDocumento.getValue()!=null && cmbDocumento.getValue()!="")
				{
					cmbPedido.removeAllItems();
					
					datos = ceps.recuperarPedidos(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString());
					
					Iterator<String> iter = datos.iterator();
					
					while (iter.hasNext())
					{
						String valor = iter.next();
						cmbPedido.addItem(valor);
					}
				}
			}
		};
	
		cmbDocumento.addValueChangeListener(lisDoc);
		
		ValueChangeListener lisPed = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbPedido.getValue()!=null && cmbPedido.getValue()!="")
				{
					if (todoEnOrden())
					{
						String nombre = ceps.recuperarNombreProveedor(cmbEjercicio.getValue().toString(),cmbDocumento.getValue().toString(), cmbPedido.getValue().toString());
						nombreProveedor.setCaption(nombre);
						traerLineasPedido();
					}
					else
						Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos");
				}
			}
		};
	
		cmbPedido.addValueChangeListener(lisPed);
		
		ValueChangeListener lisArea = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbArea.getValue()!=null && cmbArea.getValue()!="")
				{
					if (cmbArea.getValue().toString().toUpperCase().contentEquals("RECEPCIONES"))
					{
						cmbDocumento.setVisible(true);
						cmbPedido.setVisible(true);
						txtArticulo.setVisible(false);
						tituloNombreReferencia.setCaption("NOMBRE PROVEEDOR");
						
					}
					else
					{
						cmbDocumento.setVisible(false);
						cmbPedido.setVisible(false);
						txtArticulo.setVisible(true);
						tituloNombreReferencia.setCaption("NOMBRE ARTICULO");
					}
				}
			}
		};
	
		cmbArea.addValueChangeListener(lisArea);
		
		ValueChangeListener lisArt = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtArticulo.getValue()!=null && txtArticulo.getValue()!="")
				{
					String nombre = ceps.recuperarNombreArticulo(txtArticulo.getValue());
					nombreReferencia.setCaption(nombre);
					embotelladoEtiquetado.setVisible(txtArticulo.getValue().startsWith("0102") || txtArticulo.getValue().startsWith("0111") || txtArticulo.getValue().startsWith("0103"));
					traerLineasStock();
				}
			}
		};
		
		txtArticulo.addValueChangeListener(lisArt);
	}
	
	private void traerLineasPedido()
	{
		consultaEtiquetasRecepcionServer cers = consultaEtiquetasRecepcionServer.getInstance(CurrentUser.get());
		ArrayList<MapeoLineasPedidosCompras> vectorLineas = cers.recuperarLineasPedidos(new Integer(cmbEjercicio.getValue().toString()), cmbDocumento.getValue().toString(), cmbPedido.getValue().toString());
		
		if (this.gridDatos!=null)
		{
			this.gridDatos.removeAllColumns();
			this.principal.removeComponent(this.gridDatos);
			this.gridDatos=null;
			principal.removeComponent(botonera);
		}
		
		if (!vectorLineas.isEmpty() && vectorLineas.size()!=0)
		{
			this.llenarRegistros(vectorLineas);
			/*
			 * cargar grid lineas
			 */
			
			this.gridDatos.setSizeFull();
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setWidth("100%");
			this.gridDatos.setHeight("100%");
			this.gridDatos.setFrozenColumnCount(3);
			
			principal.addComponent(this.gridDatos);
			principal.addComponent(botonera);
			principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
			principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No hay datos para mostrar");
		}
	}

	private void traerLineasStock()
	{
		consultaEtiquetasRecepcionServer cers = consultaEtiquetasRecepcionServer.getInstance(CurrentUser.get());
		ArrayList<MapeoInventarioLote> vectorInventario = cers.recuperarInventario(txtArticulo.getValue());
		
		if (this.gridDatos!=null)
		{
			this.gridDatos.removeAllColumns();
			this.principal.removeComponent(this.gridDatos);
			this.gridDatos=null;
			principal.removeComponent(botonera);
		}
		
		if (!vectorInventario.isEmpty() && vectorInventario.size()!=0)
		{
			this.llenarRegistrosInventario(vectorInventario);
			/*
			 * cargar grid lineas
			 */
			
			this.gridDatos.setSizeFull();
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setWidth("100%");
			this.gridDatos.setHeight("100%");
			this.gridDatos.setFrozenColumnCount(3);
			
			principal.addComponent(this.gridDatos);
			principal.addComponent(botonera);
			principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
			principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No hay datos para mostrar");
		}
	}
	
	private void llenarRegistros(ArrayList<MapeoLineasPedidosCompras> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasPedidosCompras mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("eti", String.class, null);
		container.addContainerProperty("pdf", String.class, null);
		container.addContainerProperty("Almacen", Integer.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Plazo Entrega", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Unidades", Double.class, null);
		container.addContainerProperty("Servidas", Integer.class, null);
		container.addContainerProperty("Pendientes", Integer.class, null);
		container.addContainerProperty("idPedido", Integer.class, null);
		container.addContainerProperty("linea", Integer.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasPedidosCompras) iterator.next();
			
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			
    		file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen());
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
    		
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
    		file.getItemProperty("Servidas").setValue(mapeoAyuda.getUnidades_serv());
    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getUnidades_pdte());
    		file.getItemProperty("eti").setValue("");
    		file.getItemProperty("pdf").setValue("");
    		file.getItemProperty("idPedido").setValue(mapeoAyuda.getIdCodigoCompra());
    		file.getItemProperty("linea").setValue(mapeoAyuda.getLinea());
    		
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.getColumn("eti").setHeaderCaption("");
		this.gridDatos.getColumn("eti").setWidth(new Double(40));
		this.gridDatos.getColumn("eti").setSortable(false);

		this.gridDatos.getColumn("pdf").setHeaderCaption("");
		this.gridDatos.getColumn("pdf").setWidth(new Double(40));
		this.gridDatos.getColumn("pdf").setSortable(false);
		
		this.gridDatos.getColumn("idPedido").setHidden(true);
		this.gridDatos.getColumn("linea").setHidden(true);
		
		this.asignarEstilos();
		this.cargarListenersGrid();
	}

	private void llenarRegistrosInventario(ArrayList<MapeoInventarioLote> r_vector)
	{
		Iterator iterator = null;
		MapeoInventarioLote mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("eti", String.class, null);
		container.addContainerProperty("Almacen", Integer.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Lote", String.class, null);
		container.addContainerProperty("Añada", String.class, null);
		container.addContainerProperty("stock_actual", Integer.class, null);
		
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoInventarioLote) iterator.next();
			
			file.getItemProperty("eti").setValue("");
			file.getItemProperty("Almacen").setValue(mapeoAyuda.getAlmacen());
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			
			file.getItemProperty("Lote").setValue(mapeoAyuda.getLote());
			file.getItemProperty("Añada").setValue(mapeoAyuda.getUbicacion());
			file.getItemProperty("stock_actual").setValue(mapeoAyuda.getStock_actual());
		}
		
		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.SINGLE);
		this.gridDatos.getColumn("eti").setHeaderCaption("");
		this.gridDatos.getColumn("eti").setWidth(new Double(40));
		this.gridDatos.getColumn("eti").setSortable(false);
		
		this.asignarEstilosInventario();
		this.cargarListenersGridInventario();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "eti".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEan";
            	}
            	else if ( "pdf".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonPdf";
            	}
            	else if ("Unidades".equals(cellReference.getPropertyId()) || "Servidas".equals(cellReference.getPropertyId()) || "Pendientes".equals(cellReference.getPropertyId()) || "Stock".equals(cellReference.getPropertyId())
            			|| "Almacen".equals(cellReference.getPropertyId()) || "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId()) || "Proveedor".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
	}

	public void asignarEstilosInventario()
	{
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
		{
			public String getStyle(Grid.CellReference cellReference) 
			{
				if ( "eti".equals(cellReference.getPropertyId()))
				{
					return "cell-nativebuttonEan";
				}
				else if ("stock_actual".equals(cellReference.getPropertyId()) ) 
				{
					return "Rcell-normal";
				}
				else
				{
					return "cell-normal";
				}
			}
		});
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.cmbEjercicio.getValue()==null || this.cmbEjercicio.getValue().toString().length()==0)
		{
			devolver = false;
			cmbEjercicio.focus();
		}
		else if (this.cmbDocumento.getValue()==null || this.cmbDocumento.getValue().toString().length()==0) 
		{
			devolver =false;			
			cmbDocumento.focus();
		}
		else if (this.cmbPedido.getValue()==null || this.cmbPedido.getValue().toString().length()==0) 
		{
			devolver =false;
			cmbPedido.focus();
		}
		return devolver;
	}
	
	@Override
	public void aceptarProceso(String r_accion) 
	{
		
	}

	@Override
	public void cancelarProceso(String r_accion) 
	{
		
	}

	public void cargarListenersGrid() 
	{		
		this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		
            		Item file = gridDatos.getContainerDataSource().getItem(event.getItemId());
            		
	            	if (event.getPropertyId().toString().equals("eti"))
	            	{
            			String articulo = null;
            			String pedido = null;
            			String codigo = null;
            			articulo = file.getItemProperty("Articulo").getValue().toString();
            			codigo = file.getItemProperty("idPedido").getValue().toString().trim()+file.getItemProperty("linea").getValue().toString().trim();
            			pedido = cmbPedido.getValue().toString();
            			ImpresionEtiquetasRecepcion vt = new ImpresionEtiquetasRecepcion(null,articulo,codigo,"Impresion Etiquetas Recepcion del pedido " + pedido);
            			getUI().addWindow(vt);	           			
	            	}
	            	else if (event.getPropertyId().toString().equals("pdf"))
	            	{
	            		String pedido = null;
	            		pedido = cmbPedido.getValue().toString();
	            		
//	            		String articulo = null;
//	            		String codigo = null;
//	            		articulo = file.getItemProperty("Articulo").getValue().toString();
//	            		codigo = file.getItemProperty("idPedido").getValue().toString().trim()+file.getItemProperty("linea").getValue().toString().trim();

	            		MapeoPedidosCompras mapeo = new MapeoPedidosCompras();
	            		mapeo.setIdCodigoCompra(new Integer(pedido));
	            		mapeo.setIdCodigo(new Integer(pedido));
	            		consultaPedidosComprasServer cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
	            		mapeo = cps.datosOpcionesPedido(mapeo);
		    			boolean generado = traerPedidoSeleccionado(mapeo);
		    			
		    			if (generado)
		    			{
		    				generacionPdf(mapeo,true);
		    			}
						else
						{
							Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoCompra().toString() + " e insertarlo en la plataforma.");
						}
	            	}
	            }
    		}
        });
	}

	public void cargarListenersGridInventario() 
	{		
		this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
		{
			public void itemClick(ItemClickEvent event) 
			{
				if (event.getPropertyId()!=null)
				{
					
					Item file = gridDatos.getContainerDataSource().getItem(event.getItemId());
					
					if (event.getPropertyId().toString().equals("eti"))
					{
						String articulo = null;
						String almacen = null;
						String codigo = null;
						boolean embotellado = embotelladoEtiquetado.getValue();
						articulo = file.getItemProperty("Articulo").getValue().toString();
						codigo = file.getItemProperty("Articulo").getValue().toString().trim()+file.getItemProperty("Almacen").getValue().toString().trim();
						almacen = file.getItemProperty("Almacen").getValue().toString().trim();
						ImpresionEtiquetasRecepcion vt = new ImpresionEtiquetasRecepcion(null,articulo,codigo,"Impresion Etiquetas Inventario del articulo " + articulo, embotellado);
						getUI().addWindow(vt);	           			
					}
				}
			}
		});
	}
	
    private boolean traerPedidoSeleccionado(MapeoPedidosCompras r_mapeo)
    {
    
    	/*
    	 * 1.- comprueba si el pedido esta en la BBDD de mysql
    	 * 		en caso afirmativo lo borra
    	 * 2.- inserta el pedido con los datos de greensys en el mysql
    	 */
    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
    	boolean traido = cps.traerPedidoSGAMysql(r_mapeo);
    	if (traido)
    		traido = cps.traerPedidoGreensysMysql(r_mapeo);
    	return traido;
    }

    private boolean generacionPdf(MapeoPedidosCompras r_mapeo, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
    	pdfGenerado = cps.generarInformeSGA(r_mapeo.getIdCodigoCompra(),r_mapeo.getIdCodigoCompra());
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		
		pdfGenerado = null;
    	return true;
    }
}