package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.modelo;

import java.util.Date;

public class MapeoLineasPedidosComprasSGA
{
	private String articulo;
	private String descripcion;
	private String gtin;

	private Integer idCodigoCompra;
	private Integer unidades_rec;

	private Double unidades;

	public MapeoLineasPedidosComprasSGA()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getGtin() {
		return gtin;
	}

	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}

	public Integer getUnidades_rec() {
		return unidades_rec;
	}

	public Double getUnidades() {
		return unidades;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}

	public void setUnidades_rec(Integer unidades_rec) {
		this.unidades_rec = unidades_rec;
	}

	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}

}