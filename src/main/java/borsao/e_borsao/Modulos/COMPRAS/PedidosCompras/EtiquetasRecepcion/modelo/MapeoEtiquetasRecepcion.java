package borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.EtiquetasRecepcion.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasRecepcion extends MapeoGlobal
{
	private String idPedido; 
	private String articulo; 
	private String descripcion; 
	private String cajas;
	private String lote; 
	private String añada; 
	private Integer numerador; 
	private String sscc;
	private String sscc1;
	private String ean;
	
	private String codigo;
	private String codigot;
	private String precodigot;
	private String sscc1t;
	private String presscc1t;
	
	public MapeoEtiquetasRecepcion()
	{
		
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getCajas() {
		return cajas;
	}


	public void setCajas(String cajas) {
		this.cajas = cajas;
	}


	public String getLote() {
		return lote;
	}


	public void setLote(String lote) {
		this.lote = lote;
	}


	public String getSscc() {
		return sscc;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public String getSscc1() {
		return sscc1;
	}


	public void setSscc1(String sscc1) {
		this.sscc1 = sscc1;
	}


	public String getEan() {
		return ean;
	}


	public void setEan(String ean) {
		this.ean = ean;
	}


	public String getCodigot() {
		return codigot;
	}


	public String getPrecodigot() {
		return precodigot;
	}


	public String getSscc1t() {
		return sscc1t;
	}


	public String getPresscc1t() {
		return presscc1t;
	}


	public void setCodigot(String codigot) {
		this.codigot = codigot;
	}


	public void setPrecodigot(String precodigot) {
		this.precodigot = precodigot;
	}


	public void setSscc1t(String sscc1t) {
		this.sscc1t = sscc1t;
	}


	public void setPresscc1t(String presscc1t) {
		this.presscc1t = presscc1t;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getIdPedido() {
		return idPedido;
	}


	public void setIdPedido(String idPedido) {
		this.idPedido = idPedido;
	}

	public Integer getNumerador() {
		return numerador;
	}


	public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}


	public String getAñada() {
		return añada;
	}


	public void setAñada(String añada) {
		this.añada = añada;
	}

}