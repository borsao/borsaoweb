package borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo.MapeoArticulosIntranet;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.server.consultaArticulosIntranetServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoArticulosIntranet mapeoArticulosIntranet = null;
	 
	 private consultaArticulosIntranetServer cais = null;	 
	 private ArticulosIntranetView uw = null;
	 private BeanFieldGroup<MapeoArticulosIntranet> fieldGroup;
	 
	 public OpcionesForm(ArticulosIntranetView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoArticulosIntranet>(MapeoArticulosIntranet.class);
        fieldGroup.bindMemberFields(this);

        this.aviso.setCaptionAsHtml(true);
        this.aviso.setCaption("<p> AVISO </p>");
        this.aviso.setStyleName("bordeado");
        this.avisoTexto.setCaptionAsHtml(true);
        this.avisoTexto.setStyleName("bordeado");
        this.avisoTexto.setCaption("<p>Al guardar los cambios se actualizará el dato introducido</p>" +
                                   "<p>en el campo valor, en todos los escandallos del articulo </p>" +
                                   "<p>indicado. </p>");
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
	 			}
	 			else
	 			{
	 				MapeoArticulosIntranet mapeoArticulosIntranet_orig = (MapeoArticulosIntranet) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoArticulosIntranet= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarRegistro(mapeoArticulosIntranet,mapeoArticulosIntranet_orig);
	 				hm=null;
	 			}
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
             setEnabled(false);
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public void setCreacion(boolean creacion) {
		editarArticulo(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.articulo.getValue()!=null && this.articulo.getValue().length()>0)
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		
		if (this.area.getValue()!=null && this.area.getValue().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.valor.getValue()!=null && this.valor.getValue().length()>0) 
		{
			opcionesEscogidas.put("valor", this.valor.getValue());
		}
		else
		{
			opcionesEscogidas.put("valor", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarArticulo(null);
	}
	
	public void editarArticulo(MapeoArticulosIntranet r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoArticulosIntranet();
		fieldGroup.setItemDataSource(new BeanItem<MapeoArticulosIntranet>(r_mapeo));
		valor.focus();
	}
	
	public void modificarRegistro(MapeoArticulosIntranet r_mapeo, MapeoArticulosIntranet r_mapeo_orig)
	{
		cais = new consultaArticulosIntranetServer(CurrentUser.get());
		cais.actualizarArticulosIntranet(r_mapeo);

		uw.grid.removeAllColumns();			
		uw.barAndGridLayout.removeComponent(uw.grid);
		uw.grid=null;			

		uw.generarGrid(uw.opcionesEscogidas);
//		((OpcionesGrid) uw.grid).refreshAllRows();	
	}
	
    private void formHasChanged() {

        BeanItem<MapeoArticulosIntranet> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoArticulosIntranet= item.getBean();
        }
    }

}
