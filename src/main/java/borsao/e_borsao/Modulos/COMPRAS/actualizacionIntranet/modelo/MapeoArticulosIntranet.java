package borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoArticulosIntranet extends MapeoGlobal
{
	public String articulo;
	public String descripcion;
	public String articuloPadre;
	public String descripcionPadre;
	public String area;
	public String valor;
	
	public MapeoArticulosIntranet()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getArticuloPadre() {
		return articuloPadre;
	}

	public void setArticuloPadre(String articuloPadre) {
		this.articuloPadre = articuloPadre;
	}

	public String getDescripcionPadre() {
		return descripcionPadre;
	}

	public void setDescripcionPadre(String descripcionPadre) {
		this.descripcionPadre = descripcionPadre;
	}
}