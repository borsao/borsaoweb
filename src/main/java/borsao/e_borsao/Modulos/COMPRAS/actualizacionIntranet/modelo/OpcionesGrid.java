package borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public OpcionesGrid(ArrayList<MapeoArticulosIntranet> r_vector) 
    {
        
        this.vector=r_vector;
        
		this.asignarTitulo("Articulos Erroneos Intranet");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoArticulosIntranet.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("area", "articulo", "descripcion", "valor", "articuloPadre", "descripcionPadre");
    }
    
    public void establecerTitulosColumnas()
    {
   	
    	this.getColumn("area").setHeaderCaption("Area");
    	this.getColumn("area").setWidth(new Double(150));
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(150));
    	this.getColumn("articuloPadre").setHeaderCaption("Padre");
    	this.getColumn("articuloPadre").setWidth(new Double(150));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(500));
    	this.getColumn("descripcionPadre").setHeaderCaption("Descripcion Padre");
    	this.getColumn("descripcionPadre").setWidth(new Double(500));

    	this.getColumn("valor").setHeaderCaption("Valor");
    	this.getColumn("valor").setWidth(new Double(200));
    	this.getColumn("idCodigo").setHidden(true);
	}
	
    public void cargarListeners()
    {
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	return "cell-normal";
            }
        });
    }

	@Override
	public void calcularTotal() {
		
	}
}
