package borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo.MapeoArticulosIntranet;

public class consultaArticulosIntranetServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosIntranetServer instance;
	
	
	public consultaArticulosIntranetServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosIntranetServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosIntranetServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerUbicacion(String r_articulo)
	{
		Connection con = null;
		Statement cs = null;		

		ResultSet rsOpcion = null;		
		String ubicacionObtenida = null;
		String tabla = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		if (r_articulo.startsWith("0106") || r_articulo.startsWith("0107"))
		{
			tabla = "bor_etiquetas";
			cadenaSQL.append(" SELECT ubicacion as valor ");
			cadenaSQL.append(" FROM " + tabla );
			cadenaSQL.append(" where articulo = '" + r_articulo + "' " );
			
			try
			{
				con= this.conManager.establecerConexionBiblia();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					ubicacionObtenida=rsOpcion.getString("valor");
				}
				if (ubicacionObtenida==null)
				{
					rsOpcion.close();
					tabla = "bor_medallas";
					cadenaSQL = new StringBuffer();
					cadenaSQL.append(" SELECT ubicacion as valor ");
					cadenaSQL.append(" FROM " + tabla );
					cadenaSQL.append(" where articulo = '" + r_articulo + "' " );
					rsOpcion= cs.executeQuery(cadenaSQL.toString());
					while(rsOpcion.next())
					{
						ubicacionObtenida=rsOpcion.getString("valor");
					}
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				Notificaciones.getInstance().mensajeError(e.getMessage());
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}

		}
		else
		{
			/*
			 * Si llegamos a meter ubicaciones en materias primas de greensys al meter compras la buscaremos aqui
			 * en caso contrario todo lo que no sean etiquetas quedarán si ubicacion
			 */
			
		}
		
		return ubicacionObtenida;
	}
	
	public ArrayList<MapeoArticulosIntranet> datosOpcionesGlobal(MapeoArticulosIntranet r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulosIntranet> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		/*
		 * Etiquetas
		 */
		cadenaSQL.append(" SELECT 'Etiquetas' as area, ");
		cadenaSQL.append(" a.articulo as articulo, ");
		cadenaSQL.append(" a.descripcion as descripcion, ");
		cadenaSQL.append(" c.articulo as articuloPadre, ");
		cadenaSQL.append(" c.descripcion as descripcionPadre, ");
		cadenaSQL.append(" a.ubicacion as valor ");
		cadenaSQL.append(" FROM bor_etiquetas as a ");
		cadenaSQL.append(" inner join bor_articulos as c on c.codigo = a.fk_articulo");
		cadenaSQL.append(" where a.articulo in (select articulo from bor_etiquetas as b where a.articulo=b.articulo and (a.ubicacion<>b.ubicacion or b.ubicacion is null)) ");
		cadenaSQL.append(" union all ");
		cadenaSQL.append(" SELECT 'Medallas' as area, ");
		cadenaSQL.append(" a.articulo as articulo, ");
		cadenaSQL.append(" a.descripcion as descripcion, ");
		cadenaSQL.append(" c.articulo as articuloPadre, ");
		cadenaSQL.append(" c.descripcion as descripcionPadre, ");
		cadenaSQL.append(" a.ubicacion as valor ");
		cadenaSQL.append(" FROM bor_medallas as a ");
		cadenaSQL.append(" inner join bor_articulos as c on c.codigo = a.fk_articulo");
		cadenaSQL.append(" where a.articulo in (select articulo from bor_medallas as b where a.articulo=b.articulo and (a.ubicacion<>b.ubicacion or b.ubicacion is null)) ");
		cadenaSQL.append(" union all ");
		cadenaSQL.append(" SELECT 'Cajas' as area, ");
		cadenaSQL.append(" a.articulo as articulo, ");
		cadenaSQL.append(" a.descripcion as descripcion, ");
		cadenaSQL.append(" c.articulo as articuloPadre, ");
		cadenaSQL.append(" c.descripcion as descripcionPadre, ");
		cadenaSQL.append(" a.peso as valor ");
		cadenaSQL.append(" FROM bor_cajas as a ");
		cadenaSQL.append(" inner join bor_articulos as c on c.codigo = a.fk_articulo");
		cadenaSQL.append(" where a.articulo in (select articulo from bor_cajas as b where a.articulo=b.articulo and (a.peso<>b.peso or b.peso is null)) ");
		cadenaSQL.append(" order by articulo ");
		
		try
		{
			con= this.conManager.establecerConexionBiblia();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosIntranet>();
			
			while(rsOpcion.next())
			{
				MapeoArticulosIntranet mapeoArticulosIntranet = new MapeoArticulosIntranet();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosIntranet.setArea(rsOpcion.getString("area"));
				mapeoArticulosIntranet.setArticulo(rsOpcion.getString("articulo"));
				mapeoArticulosIntranet.setValor(rsOpcion.getString("valor"));
				mapeoArticulosIntranet.setArticuloPadre(rsOpcion.getString("articuloPadre"));
				mapeoArticulosIntranet.setDescripcion(rsOpcion.getString("descripcion"));
				mapeoArticulosIntranet.setDescripcionPadre(rsOpcion.getString("descripcionPadre"));
				
				vector.add(mapeoArticulosIntranet);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	public String semaforos()
	{
		ResultSet rsOpcion = null;
		String errores = "0";
		StringBuffer cadenaSQL =null;
		
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT count(articulo) as errores ");
			cadenaSQL.append(" FROM bor_etiquetas as a ");
			cadenaSQL.append(" where articulo in (select articulo from bor_etiquetas as b where a.articulo=b.articulo and (a.ubicacion<>b.ubicacion or b.ubicacion is null)) ");
			
			con= this.conManager.establecerConexionBiblia();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("errores")>0) errores="2";
			}

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT count(articulo) as errores ");
			cadenaSQL.append(" FROM bor_medallas as a ");
			cadenaSQL.append(" where articulo in (select articulo from bor_medallas as b where a.articulo=b.articulo and ( a.ubicacion<>b.ubicacion or b.ubicacion is null)) ");
			
			con= this.conManager.establecerConexionBiblia();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("errores")>0) errores="2";
			}

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT count(articulo) as errores ");
			cadenaSQL.append(" FROM bor_cajas as a ");
			cadenaSQL.append(" where articulo in (select articulo from bor_cajas as b where a.articulo=b.articulo and (a.peso<>b.peso or b.peso is null)) ");
			cadenaSQL.append(" order by articulo ");
			
			con= this.conManager.establecerConexionBiblia();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("errores")>0) errores="2";
				
			}


		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return errores;
	}
	

	public boolean actualizarArticulosIntranet(MapeoArticulosIntranet r_mapeo)
	{
		/*
		 * actualizamos la ubicacion indicada por el usuario en la pantalla
		 */
		String tablaActualizar = null;
		String campoActualizar = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			if (r_mapeo.getArea().equals("Etiquetas"))
			{
				tablaActualizar="bor_etiquetas";
				campoActualizar = "ubicacion";
			}
			if (r_mapeo.getArea().equals("Medallas"))
			{
				tablaActualizar="bor_medallas";
				campoActualizar = "ubicacion";
			}
			if (r_mapeo.getArea().equals("Cajas"))
			{
				tablaActualizar="bor_cajas";
				campoActualizar = "peso";
			}
	        
			cadenaSQL.append(" UPDATE " +  tablaActualizar + " set ");
    		cadenaSQL.append(campoActualizar + " = ? ");
	        cadenaSQL.append(" where articulo = ? ");
		    
		    con= this.conManager.establecerConexionBiblia();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getValor());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    	
		    	preparedStatement.executeUpdate();
		    }
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		return true;
	}
}