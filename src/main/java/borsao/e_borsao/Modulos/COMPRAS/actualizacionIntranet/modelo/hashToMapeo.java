package borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulosIntranet mapeo = null;
	 
	 public MapeoArticulosIntranet convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosIntranet();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));		 
		 this.mapeo.setValor(r_hash.get("valor"));
		 this.mapeo.setArticuloPadre(r_hash.get("articuloPadre"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setDescripcionPadre(r_hash.get("descripcionPadre"));		 

		 return mapeo;		 
	 }
	 
	 public MapeoArticulosIntranet convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosIntranet();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));		 
		 this.mapeo.setValor(r_hash.get("valor"));
		 this.mapeo.setArticuloPadre(r_hash.get("articuloPadre"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setDescripcionPadre(r_hash.get("descripcionPadre"));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulosIntranet r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("valor", r_mapeo.getValor());
		 this.hash.put("articuloPadre", r_mapeo.getArticuloPadre());
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 this.hash.put("descripcionPadre", r_mapeo.getDescripcionPadre());
		 return hash;		 
	 }
}