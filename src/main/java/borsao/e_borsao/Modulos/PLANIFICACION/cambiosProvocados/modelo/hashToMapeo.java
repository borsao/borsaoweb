package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoCambiosProvocados mapeo = null;
	 
	 public MapeoCambiosProvocados convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoCambiosProvocados();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(r_hash.get("semana")));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(r_hash.get("orden")));
		 
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 this.mapeo.setCausa(r_hash.get("causa"));		 
		 this.mapeo.setModificacion(r_hash.get("modificacion"));
		 return mapeo;		 
	 }
	 
	 public MapeoCambiosProvocados convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoCambiosProvocados();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(r_hash.get("semana")));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(r_hash.get("orden")));
		 
		 this.mapeo.setDepartamento(r_hash.get("departamento"));
		 this.mapeo.setCausa(r_hash.get("causa"));		 
		 this.mapeo.setModificacion(r_hash.get("modificacion"));

		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoCambiosProvocados r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());		 
		 if (r_mapeo.getEjercicio()!=null) this.hash.put("ejercicio", r_mapeo.getEjercicio().toString());
		 if (r_mapeo.getSemana()!=null) this.hash.put("semana", r_mapeo.getSemana().toString());
		 if (r_mapeo.getOrden()!=null) this.hash.put("orden", r_mapeo.getOrden().toString());

		 
		 this.hash.put("departamento", r_mapeo.getDepartamento());		 
		 this.hash.put("causa", r_mapeo.getCausa());
		 this.hash.put("modificacion", r_mapeo.getModificacion());
		 return hash;		 
	 }
}