package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.CambiosProvocadosGrid;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.MapeoCambiosProvocados;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.server.consultaCambiosProvocadosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	
{	
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoCambiosProvocados mapeoCambiosProvocados = null;
	 
	 private consultaCambiosProvocadosServer cus = null;	 
	 private cambiosProvocadosView uw = null;
	 private BeanFieldGroup<MapeoCambiosProvocados> fieldGroup;
	 
	 public OpcionesForm(cambiosProvocadosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoCambiosProvocados>(MapeoCambiosProvocados.class);
        fieldGroup.bindMemberFields(this);

        this.cargarCombo(null);
        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoCambiosProvocados = (MapeoCambiosProvocados) uw.grid.getSelectedRow();
                eliminarRegistro(mapeoCambiosProvocados);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			
		 			
		 			uw.ejercicio=new Integer(RutinasCadenas.quitarPuntoMiles(ejercicio.getValue()));
		 			if (semana.getValue()!=null) uw.semana =new Integer(semana.getValue().toString()); else uw.semana=0;
		 			
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoCambiosProvocados = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearRegistro(mapeoCambiosProvocados);
	 			}
	 			else
	 			{
	 				MapeoCambiosProvocados mapeoEquipos_orig = (MapeoCambiosProvocados) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoCambiosProvocados = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarRegistro(mapeoCambiosProvocados,mapeoEquipos_orig);
	 				hm=null;
	 			}
	 			if (((CambiosProvocadosGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 				uw.grid.setEnabled(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	
        		btnGuardar.setCaption("Guardar");
        		btnEliminar.setEnabled(true);
                setEnabled(false);
                if (((CambiosProvocadosGrid) uw.grid)!=null)
	 			{			
    				((cambiosProvocadosView) uw).grid.removeAllColumns();			
    				((cambiosProvocadosView) uw).barAndGridLayout.removeComponent(uw.grid);
    				((cambiosProvocadosView) uw).grid=null;
    				((cambiosProvocadosView) uw).setHayGrid(false);
     			}
                if (((cambiosProvocadosView) uw).opcionesEscogidas!=null) ((cambiosProvocadosView) uw).generarGrid(((cambiosProvocadosView) uw).opcionesEscogidas);
                
                uw.regresarDesdeForm();
                removeStyleName("visible");
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarRegistro(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.orden.getValue()!=null && this.orden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("orden", this.orden.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("orden", "");
		}
		
		if (this.departamento.getValue()!=null && this.departamento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("departamento", this.departamento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("departamento", "");
		}

		if (this.causa.getValue()!=null && this.causa.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("causa", this.causa.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("causa", "");
		}
		
		if (this.modificacion.getValue()!=null && this.modificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("modificacion", this.modificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("modificacion", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarRegistro(null);
	}
	
	public void editarRegistro(MapeoCambiosProvocados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoCambiosProvocados(); 
			r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
			r_mapeo.setSemana(RutinasFechas.semanaActual(r_mapeo.getEjercicio().toString()));
		}
		else
		{
			uw.grid.setEnabled(false);
		}
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoCambiosProvocados>(r_mapeo));
		causa.focus();
	}
	
	public void eliminarRegistro(MapeoCambiosProvocados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = consultaCambiosProvocadosServer.getInstance(CurrentUser.get());
		((CambiosProvocadosGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoCambiosProvocados>) ((CambiosProvocadosGrid) uw.grid).vector).remove(r_mapeo);
		
		uw.regresarDesdeForm();
        removeStyleName("visible");
       	activarDesactivarControles(true);

	}
	
	public void modificarRegistro(MapeoCambiosProvocados r_mapeo, MapeoCambiosProvocados r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaCambiosProvocadosServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((CambiosProvocadosGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
			
			uw.regresarDesdeForm();
	        removeStyleName("visible");
	       	activarDesactivarControles(true);
		}
	}
	public void crearRegistro(MapeoCambiosProvocados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaCambiosProvocadosServer(CurrentUser.get());
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente(r_mapeo.getEjercicio(), r_mapeo.getSemana()));
			
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoCambiosProvocados>(r_mapeo));
				if (((CambiosProvocadosGrid) uw.grid)!=null)
				{
					((CambiosProvocadosGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoCambiosProvocados> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoCambiosProvocados= item.getBean();
            if (this.mapeoCambiosProvocados.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el Ejercicio");
    		return false;
    	}
    	if (this.semana.getValue()==null || this.semana.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la semana");
    		return false;
    	}
    	if ((this.departamento.getValue()==null || this.departamento.getValue().toString().length()==0)) 
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el departamento ");
    		return false;
    	}
    	if ((this.causa.getValue()==null || this.causa.getValue().toString().length()==0)) 
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la causa ");
    		return false;
    	}
    	if ((this.modificacion.getValue()==null || this.modificacion.getValue().toString().length()==0)) 
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la modificacion hecha");
    		return false;
    	}
    	return true;
    }

	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.ejercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.semana.removeAllItems();
		}
		
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.semana.addItem(String.valueOf(i));
		}
	}
}
