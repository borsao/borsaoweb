package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.view.cambiosProvocadosView;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class CambiosProvocadosGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private cambiosProvocadosView app = null;
	
    public CambiosProvocadosGrid(cambiosProvocadosView r_app, ArrayList<MapeoCambiosProvocados> r_vector) {
        
        this.vector=r_vector;
        this.app = r_app;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Cambios Realizados Planificación");		
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoCambiosProvocados.class);        
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("programacion","ejercicio", "semana", "orden", "departamento", "causa", "modificacion");;
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "80");
    	this.widthFiltros.put("semana", "80");
    	this.widthFiltros.put("orden", "80");
    	this.widthFiltros.put("departamento", "110");
    	this.widthFiltros.put("causa", "210");
    	this.widthFiltros.put("modificacion", "210");
    	
    	this.getColumn("programacion").setHeaderCaption("");
    	this.getColumn("programacion").setSortable(false);
    	this.getColumn("programacion").setWidth(new Double(50));

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setSortable(false);
    	this.getColumn("ejercicio").setWidth(new Double(120));

    	this.getColumn("semana").setHeaderCaption("Semana");
    	this.getColumn("semana").setSortable(false);
    	this.getColumn("semana").setWidth(new Double(120));
    	
    	this.getColumn("orden").setHeaderCaption("Orden");
    	this.getColumn("orden").setSortable(false);
    	this.getColumn("orden").setWidth(new Double(120));
    	
    	
    	this.getColumn("departamento").setHeaderCaption("Departamento");
    	this.getColumn("departamento").setSortable(true);
    	this.getColumn("departamento").setWidth(new Double(150));
    	this.getColumn("causa").setHeaderCaption("Causa");
    	this.getColumn("causa").setSortable(true);
    	this.getColumn("causa").setWidth(new Double(250));
    	this.getColumn("modificacion").setHeaderCaption("Modificacion");
    	this.getColumn("modificacion").setSortable(true);
    	this.getColumn("modificacion").setWidthUndefined();
    	
    	this.getColumn("idCodigo").setHidden(true);

    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("programacion");

	}

    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoCambiosProvocados) {
                // The actual description text is depending on the application
                if ("programacion".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Programacion de la Semana";
                }
            }
        }
        return descriptionText;
    }
	public void asignarEstilos() 
	{		
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	String estilo = null;
            	if ( "ejercicio".equals(cellReference.getPropertyId()) || "semana".equals(cellReference.getPropertyId()) || "orden".equals(cellReference.getPropertyId()))
            	{
            		estilo = "Rcell-normal";
            	}
            	else if ( "programacion".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonProgramacion";
            	}
            	else 
            	{
            		estilo = "cell-normal";
            	}

            	return estilo;
            }
        });
    	
    	
	}

	public void cargarListeners() 
	{		
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoCambiosProvocados mapeo = (MapeoCambiosProvocados) event.getItemId();
            		
            		if (event.getPropertyId().toString().equals("programacion"))
	            	{
            			activadaVentanaPeticion=true;
	            		ordenando = false;
	            		pantallaLineasProgramaciones vtPeticion = new pantallaLineasProgramaciones(mapeo.getEjercicio(), mapeo.getSemana().toString(), "Programacion " + mapeo.getEjercicio().toString() + " - " + mapeo.getSemana().toString());
    	    			getUI().addWindow(vtPeticion);
            		}
	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
            	}
            }
    	});
    	
	}

	@Override
	public void calcularTotal() {
		
	}
}
