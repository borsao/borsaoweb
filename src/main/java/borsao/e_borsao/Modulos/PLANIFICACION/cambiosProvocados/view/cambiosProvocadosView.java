//https://vaadin.com/forum/thread/2864064/how-to-achieve-click-button-to-download-file-in-vaadin-7

package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionFormatoImpresion;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.CambiosProvocadosGrid;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.MapeoCambiosProvocados;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.server.consultaCambiosProvocadosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class cambiosProvocadosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Cambios Planificación";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	
	private OpcionesForm form = null;
	public Integer ejercicio =null;
	public Integer semana =null;
	
	
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoCambiosProvocados> r_vector=null;
    	MapeoCambiosProvocados mapeoCambiosProvocados = null;
    	hashToMapeo  hm = new hashToMapeo();
    	
    	mapeoCambiosProvocados = hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaCambiosProvocadosServer cus = new consultaCambiosProvocadosServer(CurrentUser.get());
    	r_vector=cus.datosCambiosProvocadosGlobal(mapeoCambiosProvocados);
    	
    	if (r_vector!=null && r_vector.size()>=0)
    	{
    		grid = new CambiosProvocadosGrid(this, r_vector);
			setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public cambiosProvocadosView() 
    {
    }

    public void cargarPantalla() 
    {
    	    
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(cambiosProvocadosView.VIEW_NAME);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        this.opcImprimir.setVisible(true);
        addComponent(this.form);
    }

    public void newForm()
    {    	
    	this.botonesGenerales(false);
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {   
    	this.botonesGenerales(!r_busqueda);
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	if (r_busqueda) this.form.addStyleName("visible"); else this.form.removeStyleName("visible");
    	this.form.setEnabled(r_busqueda);
    	if (r_busqueda) this.form.btnGuardar.setCaption("Buscar"); else this.form.btnGuardar.setCaption("Guardar");
    	if (r_busqueda) this.form.btnEliminar.setEnabled(false); else this.form.btnEliminar.setEnabled(true);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((CambiosProvocadosGrid) this.grid).activadaVentanaPeticion && !((CambiosProvocadosGrid) this.grid).ordenando)
    	{
    		this.botonesGenerales(false);
    		this.form.setBusqueda(false);
    		this.form.editarRegistro((MapeoCambiosProvocados) r_fila);
    		this.form.addStyleName("visible");
    		this.form.setEnabled(true);
    	}
    }
    
    @Override
    public void reestablecerPantalla() {
    	this.botonesGenerales(true);
    }

    public void print() {
    	this.botonesGenerales(true);
    	if (this.ejercicio!=null && this.semana!=null)
    	{
    		PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion("CambiosPlanificacion", this.ejercicio.toString(), this.semana.toString());
    		getUI().addWindow(vtPeticion);
    	}
    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }

    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(cambiosProvocadosView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(cambiosProvocadosView.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	
}
