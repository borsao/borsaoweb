package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo.MapeoCambiosProvocados;

public class consultaCambiosProvocadosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCambiosProvocadosServer instance;
	
	public consultaCambiosProvocadosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCambiosProvocadosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCambiosProvocadosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoCambiosProvocados> datosCambiosProvocadosGlobal(MapeoCambiosProvocados r_mapeo)
	{
		ResultSet rsCambios = null;		
		ArrayList<MapeoCambiosProvocados> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_cambios.ejercicio cmb_eje, ");
		cadenaSQL.append(" prd_programacion_cambios.semana cmb_sem, ");
		cadenaSQL.append(" prd_programacion_cambios.orden cmb_ord, ");
		cadenaSQL.append(" prd_programacion_cambios.departamento cmb_dep, ");
		cadenaSQL.append(" prd_programacion_cambios.causa cmb_cau, ");
		cadenaSQL.append(" prd_programacion_cambios.modificacion cmb_mod ");
     	cadenaSQL.append(" FROM prd_programacion_cambios ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().toString().length()>0 && r_mapeo.getSemana()!=0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.semana = " + r_mapeo.getSemana());
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.orden = " + r_mapeo.getOrden());
				}
				if (r_mapeo.getDepartamento()!=null && r_mapeo.getDepartamento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.departamento like '%" + r_mapeo.getDepartamento() + "%' ");
				}
				if (r_mapeo.getCausa()!=null && r_mapeo.getCausa().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" causa = '" + r_mapeo.getCausa() + "' ");
				}
				if (r_mapeo.getModificacion()!=null && r_mapeo.getModificacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" modificacion = '" + r_mapeo.getModificacion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana, orden");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsCambios= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoCambiosProvocados>();
			
			while(rsCambios.next())
			{
				MapeoCambiosProvocados mapeocambiosProvocados = new MapeoCambiosProvocados();
				/*
				 * recojo mapeo operarios
				 */
				mapeocambiosProvocados.setEjercicio(rsCambios.getInt("cmb_eje"));
				mapeocambiosProvocados.setSemana(rsCambios.getInt("cmb_sem"));
				mapeocambiosProvocados.setOrden(rsCambios.getInt("cmb_ord"));

				mapeocambiosProvocados.setDepartamento(rsCambios.getString("cmb_dep"));
				mapeocambiosProvocados.setCausa(rsCambios.getString("cmb_cau"));
				mapeocambiosProvocados.setModificacion(rsCambios.getString("cmb_mod"));
				vector.add(mapeocambiosProvocados);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public MapeoCambiosProvocados datoscambiosProvocados(MapeoCambiosProvocados r_mapeo)
	{
		ResultSet rsCambios = null;		
		StringBuffer cadenaWhere =null;
		MapeoCambiosProvocados mapeocambiosProvocados =null; 
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_cambios.ejercicio cmb_eje, ");
		cadenaSQL.append(" prd_programacion_cambios.semana cmb_sem, ");
		cadenaSQL.append(" prd_programacion_cambios.orden cmb_ord, ");
		cadenaSQL.append(" prd_programacion_cambios.departamento cmb_dep, ");
		cadenaSQL.append(" prd_programacion_cambios.causa cmb_cau, ");
		cadenaSQL.append(" prd_programacion_cambios.modificacion cmb_mod ");
     	cadenaSQL.append(" FROM prd_programacion_cambios ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.semana = " + r_mapeo.getSemana());
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.orden = " + r_mapeo.getOrden());
				}
				if (r_mapeo.getDepartamento()!=null && r_mapeo.getDepartamento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_programacion_cambios.departamento like '%" + r_mapeo.getDepartamento() + "%' ");
				}
				if (r_mapeo.getCausa()!=null && r_mapeo.getCausa().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" causa = '" + r_mapeo.getCausa() + "' ");
				}
				if (r_mapeo.getModificacion()!=null && r_mapeo.getModificacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" modificacion = '" + r_mapeo.getModificacion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana, orden");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsCambios= cs.executeQuery(cadenaSQL.toString());
			
			while(rsCambios.next())
			{
				mapeocambiosProvocados = new MapeoCambiosProvocados();
				/*
				 * recojo mapeo operarios
				 */
				mapeocambiosProvocados.setEjercicio(rsCambios.getInt("cmb_eje"));
				mapeocambiosProvocados.setSemana(rsCambios.getInt("cmb_sem"));
				mapeocambiosProvocados.setOrden(rsCambios.getInt("cmb_ord"));

				mapeocambiosProvocados.setDepartamento(rsCambios.getString("cmb_dep"));
				mapeocambiosProvocados.setCausa(rsCambios.getString("cmb_cau"));
				mapeocambiosProvocados.setModificacion(rsCambios.getString("cmb_mod"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeocambiosProvocados;
	}
	
	public Integer obtenerSiguiente(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsCambios = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion_cambios.orden) cmb_ord ");
     	cadenaSQL.append(" FROM prd_programacion_cambios ");
     	cadenaSQL.append(" WHERE prd_programacion_cambios.ejercicio = " + r_ejercicio);
     	cadenaSQL.append(" and prd_programacion_cambios.semana = " + r_semana);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsCambios= cs.executeQuery(cadenaSQL.toString());
			
			while(rsCambios.next())
			{
				return rsCambios.getInt("cmb_ord") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoCambiosProvocados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion_cambios ( ");
    		cadenaSQL.append(" prd_programacion_cambios.ejercicio, ");
    		cadenaSQL.append(" prd_programacion_cambios.semana, ");
    		cadenaSQL.append(" prd_programacion_cambios.orden, ");
    		cadenaSQL.append(" prd_programacion_cambios.departamento, ");
    		cadenaSQL.append(" prd_programacion_cambios.causa, ");
    		cadenaSQL.append(" prd_programacion_cambios.modificacion) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }

		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    preparedStatement.setInt(3, this.obtenerSiguiente(r_mapeo.getEjercicio(), r_mapeo.getSemana()));
		    if (r_mapeo.getDepartamento()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDepartamento());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCausa()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getCausa());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getModificacion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getModificacion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoCambiosProvocados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_programacion_cambios set ");
			cadenaSQL.append(" prd_programacion_cambios.departamento=?, ");
			cadenaSQL.append(" prd_programacion_cambios.causa=?, ");
			cadenaSQL.append(" prd_programacion_cambios.modificacion=? ");
		    cadenaSQL.append(" WHERE prd_programacion_cambios.ejercicio = ? ");
		    cadenaSQL.append(" AND prd_programacion_cambios.semana = ? ");
		    cadenaSQL.append(" AND prd_programacion_cambios.orden = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setString(1, r_mapeo.getDepartamento());
		    preparedStatement.setString(2, r_mapeo.getCausa());
		    preparedStatement.setString(3, r_mapeo.getModificacion());
		    preparedStatement.setInt(4, r_mapeo.getEjercicio());
		    preparedStatement.setInt(5, r_mapeo.getSemana());
		    preparedStatement.setInt(6, r_mapeo.getOrden());
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoCambiosProvocados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_programacion_cambios ");            
			cadenaSQL.append(" WHERE prd_programacion_cambios.ejercicio = ?");
			cadenaSQL.append(" and prd_programacion_cambios.semana = ?");
			cadenaSQL.append(" and prd_programacion_cambios.orden = ?");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getEjercicio());
			preparedStatement.setInt(2, r_mapeo.getSemana());
			preparedStatement.setInt(3, r_mapeo.getOrden());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex) 
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String imprimirCambiosProgramacion(Integer r_ejercicio, Integer r_semana, String r_formato)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio.toString());
		libImpresion.setArchivoDefinitivo("/cambiosPlanificacion" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		if (r_formato.equals("Relacion"))
		{
			if (r_semana == 0)
			{
				libImpresion.setArchivoPlantilla("relacionCambiosPlanificacion.jrxml");
			}
			else
			{
				libImpresion.setCodigo2Txt(r_semana.toString());
				libImpresion.setArchivoPlantilla("relacionCambiosPlanificacionSemanal.jrxml");
			}
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlancoH.jpg");
		}
		else
		{
			libImpresion.setArchivoPlantilla("IndicadorCambiosPlanificacion.jrxml");
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlancoH.jpg");
		}
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("planificacion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

}