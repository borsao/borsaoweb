package borsao.e_borsao.Modulos.PLANIFICACION.cambiosProvocados.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCambiosProvocados extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	private Integer orden;
	
	private String departamento;
	private String causa;
	private String modificacion;
	
	private String programacion="";
	
	public MapeoCambiosProvocados()
	{
		this.setDepartamento("");
		this.setCausa("");
		this.setModificacion("");
		this.setProgramacion("");
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public String getModificacion() {
		return modificacion;
	}

	public void setModificacion(String modificacion) {
		this.modificacion = modificacion;
	}

	public String getProgramacion() {
		return programacion;
	}

	public void setProgramacion(String programacion) {
		this.programacion = programacion;
	}

}