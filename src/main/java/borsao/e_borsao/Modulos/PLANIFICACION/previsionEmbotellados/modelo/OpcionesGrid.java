package borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.view.pantallaObservacionesProgramacion;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMCompras;
import borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.view.previsionEmbotelladosView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo.MapeoArticulosEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.ProgramacionGrid;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionEtiquetasBotella;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionEtiquetasEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionStock;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private GridViewRefresh app = null;
	
    public OpcionesGrid(previsionEmbotelladosView r_app, ArrayList<MapeoPrevisionEmbotellados> r_vector) 
    {
    	this.app = r_app;
        
        this.vector=r_vector;
        this.setSizeFull();
        
		this.asignarTitulo("Prevision Embotellados");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoPrevisionEmbotellados.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("btn", "articulo","descripcion", "anada", "cantidad", "articuloDestino","descripcionDestino", "resto","calcular_sn","calcula","tipo");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "300");
    	this.widthFiltros.put("articuloDestino", "80");
    	this.widthFiltros.put("descripcionDestino", "300");
    	this.widthFiltros.put("anada", "60");
    	this.widthFiltros.put("tipo", "80");
    	this.widthFiltros.put("calcula", "60");
    
    	this.getColumn("btn").setHeaderCaption("");
    	this.getColumn("btn").setSortable(false);
    	this.getColumn("btn").setWidth(new Double(50));

    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("articuloDestino").setHeaderCaption("ArticuloDestino");
    	this.getColumn("articuloDestino").setWidth(new Double(120));
    	this.getColumn("descripcionDestino").setHeaderCaption("DescripcionDestino");
    	this.getColumn("anada").setHeaderCaption("Añada");
    	this.getColumn("anada").setWidth(new Double(90));
    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setWidth(new Double(90));
    	this.getColumn("calcula").setHeaderCaption("Calcular?");
    	this.getColumn("calcula").setWidth(new Double(90));
    	this.getColumn("cantidad").setHeaderCaption("Litros/Botellas");
    	this.getColumn("cantidad").setWidth(new Double(150));
    	this.getColumn("resto").setHeaderCaption("Resto Litros/Botellas");
    	this.getColumn("resto").setWidth(new Double(150));
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("calcular_sn").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ( "btn".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "cantidad".equals(cellReference.getPropertyId()) || "resto".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	return "cell-normal";
            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
		this.camposNoFiltrar.add("btn");
		this.camposNoFiltrar.add("cantidad");
		this.camposNoFiltrar.add("resto");
		this.camposNoFiltrar.add("calcular_sn");
	}

	@Override
	public void cargarListeners() 
	{		
		this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoPrevisionEmbotellados mapeo = (MapeoPrevisionEmbotellados) event.getItemId();
            		
	            	if (event.getPropertyId().toString().equals("btn"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
//	            			Notificaciones.getInstance().mensajeInformativo("Pulsado acceso embotellados");
	            			String articulo = null;
	            			if (mapeo.getArticuloDestino()==null || mapeo.getArticuloDestino().length()==0)
	            				articulo = mapeo.getArticulo().trim();
	            			else
	            				articulo = mapeo.getArticuloDestino().trim(); 
	            				
	            			pantallaLineasProduccion vt = new pantallaLineasProduccion(null,"Embotelladora", null, articulo, true, mapeo.getAnada());
	            			
//	            			pantallaAyudaProduccion vt = null;
//	            			if (mapeo.getTipo().toUpperCase().equals("EMBOTELLADO"))
//	            			{
//	            				vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo());
//	            			}
//	            			else
//	            			{	            				
//	            				vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo()+"-1");
//	            			}
	            			getUI().addWindow(vt);	            			
	            		}	            		
	            	}
	            	else if (event.getPropertyId().toString().equals("articulo"))
	            	{
	            		/*
	            		 * acceso a pendientes de servir
	            		 */
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo!=null && mapeo.getArticulo()!=null && (mapeo.getArticulo().trim().startsWith("0102") || mapeo.getArticulo().trim().startsWith("0103") || mapeo.getArticulo().trim().startsWith("0111")))
	            		{
	            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo", mapeo.getArticulo());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            		else
	            		{
	            			pantallaCRMCompras vt = new pantallaCRMCompras("CRM Compras", mapeo.getArticulo());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            	}
	            	else if (event.getPropertyId().toString().equals("articuloDestino"))
	            	{
	            		/*
	            		 * acceso a pendientes de servir
	            		 */
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo!=null && mapeo.getArticuloDestino()!=null && mapeo.getArticuloDestino().length()>0 && (mapeo.getArticuloDestino().trim().startsWith("0102") || mapeo.getArticuloDestino().trim().startsWith("0103") || mapeo.getArticuloDestino().trim().startsWith("0111")))
	            		{
	            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo", mapeo.getArticuloDestino());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            		else if (mapeo!=null && mapeo.getArticuloDestino()!=null && mapeo.getArticuloDestino().length()>0)
	            		{
	            			pantallaCRMCompras vt = new pantallaCRMCompras("CRM Compras", mapeo.getArticuloDestino());
	            			if (vt.acceso)
	            				getUI().addWindow(vt);
	            			else 
	            				vt=null;
	            		}
	            	}

	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
	            }
    		}
        });
	}

	@Override
	public void calcularTotal() 
	{
		if (this.footer==null) this.footer=this.appendFooterRow();
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setWidth("100%");
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		}

	}

}
