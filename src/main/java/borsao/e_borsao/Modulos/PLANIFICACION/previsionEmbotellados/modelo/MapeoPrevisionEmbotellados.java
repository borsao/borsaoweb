package borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoPrevisionEmbotellados extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String articuloDestino;
	private String descripcionDestino;
	private String anada;
	private String tipo;
	private Integer cantidad;
	private Integer resto;
	private boolean calcular_sn;
	private String calcula;
	private String btn = "";

	public MapeoPrevisionEmbotellados()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setArticuloDestino("");
		this.setDescripcionDestino("");
		this.setAnada("");
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getArticuloDestino() {
		return articuloDestino;
	}

	public String getDescripcionDestino() {
		return descripcionDestino;
	}

	public String getAnada() {
		return anada;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setArticuloDestino(String articuloDestino) {
		this.articuloDestino = articuloDestino;
	}

	public void setDescripcionDestino(String descripcionDestino) {
		this.descripcionDestino = descripcionDestino;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

	public Integer getResto() {
		return resto;
	}

	public void setResto(Integer resto) {
		this.resto = resto;
	}

	public boolean isCalcular_sn() {
		return calcular_sn;
	}

	public void setCalcular_sn(boolean calcular_sn) {
		this.calcular_sn = calcular_sn;
	}

	public String getCalcula() {
		if (isCalcular_sn()) return "S"; else return "N";
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}