package borsao.e_borsao.Modulos.PLANIFICACION.previsionEmbotellados.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoPrevisionEmbotellados mapeo = null;
	 
	 public MapeoPrevisionEmbotellados convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPrevisionEmbotellados();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setArticuloDestino(r_hash.get("articuloDestino"));
		 this.mapeo.setDescripcionDestino(r_hash.get("descripcionDestino"));
		 this.mapeo.setCalcular_sn(r_hash.get("calcular_sn").contentEquals("S"));
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setTipo(r_hash.get("tipo"));
		 
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("cantidad"))));
		 return mapeo;		 
	 }
	 
	 public MapeoPrevisionEmbotellados convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPrevisionEmbotellados();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setArticuloDestino(r_hash.get("articuloDestino"));
		 this.mapeo.setDescripcionDestino(r_hash.get("descripcionDestino"));
		 this.mapeo.setCalcular_sn(r_hash.get("calcular_sn").contentEquals("S"));
		 
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setTipo(r_hash.get("tipo"));
		 
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("cantidad"))));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoPrevisionEmbotellados r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("articuloDestino", r_mapeo.getArticuloDestino());
		 this.hash.put("descripcionDestino", this.mapeo.getDescripcionDestino());
		 this.hash.put("anada", this.mapeo.getAnada());
		 this.hash.put("tipo", this.mapeo.getTipo());
		 if (this.mapeo.isCalcular_sn()) this.hash.put("calcular_sn", "S"); else this.hash.put("calcular_sn", "N");
		 if (this.mapeo.getCantidad()!= null) this.hash.put("cantidad", this.mapeo.getCantidad().toString());
		 return hash;		 
	 }
}