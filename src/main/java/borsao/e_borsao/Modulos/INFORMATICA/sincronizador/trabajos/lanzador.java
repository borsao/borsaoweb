package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.trabajos;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;

import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
public class lanzador 
{
	
	public void arrancarProcesosCron()
    {
		/*
		 * Se trata de ir poniendo distintos bloques para programar distintas operaciones
		 */
    	
    	/*
    	 * 	1-	Seconds
		 *	2-	Minutes
		 *	3-	Hours
		 *	4-	Day-of-Month
		 *	5-	Month
		 *	6-	Day-of-Week
		 *	7-	Year (optional field)
    	 */
		
		/*
		 * SINCRONIZACION TABLAS BBDD
		 * 
		 * 
		 */

		    	JobDetail script1 = null;
		    	Trigger disparador1 = null;
		    	Scheduler programacion1 = null;
		    	
		    	try 
		    	{
		    		programacion1 = new StdSchedulerFactory().getScheduler();
		    		if (programacion1.getJobGroupNames().size()==0)    	
		    		{	
						script1 = JobBuilder.newJob(trabajos.class).withIdentity("script1", "noche").build();
						disparador1 = TriggerBuilder.newTrigger()
						        .withIdentity("cronDiario", "noche")
						        .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(01, 00))
						        .build();
			
						//		.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
				        	        
						programacion1.scheduleJob(script1, disparador1);
						programacion1.start();
						
			//            Thread.sleep(5000);
			//            programacion1.shutdown();
		    		}
				} 
		    	catch (Exception e) 
		    	{
		    		Notificaciones.getInstance().mensajeSeguimiento("Sincronizacion Tablas: " + e.getMessage());
					e.printStackTrace();
				}
         

    }
	
}