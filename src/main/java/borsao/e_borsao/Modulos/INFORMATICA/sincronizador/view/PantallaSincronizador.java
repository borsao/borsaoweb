package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.trabajos.trabajos;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.server.consultaCamaraVisionArtificialServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PantallaSincronizador extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbPeticion = null;
	
	private ComboBox cmbOrigen = null;
	private ComboBox cmbDestino = null;
	private TextField txtTablaOrigen= null;
	private TextField txtTablaDestino= null;
	private TextField txtCamposOrigen= null;
	private TextField txtGroupOrigen= null;
	private TextField txtSQL= null;
	private TextField txtFecha= null;
	private Label lblLastFecha= null;
	private CheckBox chkTodos= null;

	private ComboBox cmbEjecucion = null;
	private TextField txtHora= null;
	private HorizontalLayout fila5 = null;
	private HorizontalLayout fila6 = null;
	
	private SincronizadorView origen = null;
	private HorizontalLayout fila2 = null;
	private HorizontalLayout fila3 = null;
	private HorizontalLayout fila4 = null;
	
	private VerticalLayout controles = null;
	
	public PantallaSincronizador(SincronizadorView r_view)
	{
		this.origen=r_view;

		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbPeticion =new ComboBox("Petición");
				this.cmbPeticion.setWidth("250px");
				this.cmbPeticion.setNullSelectionAllowed(false);
				fila1.addComponent(this.cmbPeticion);
				
			fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbOrigen = new ComboBox("BBDD Origen");

				this.txtTablaOrigen= new TextField("Tabla");
				this.txtTablaOrigen.setWidth("300px");

				fila2.addComponent(this.cmbOrigen);
				fila2.addComponent(this.txtTablaOrigen);

			fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtCamposOrigen= new TextField("Campos tabla Origen");
				this.txtCamposOrigen.setWidth("300px");
				
				this.txtGroupOrigen= new TextField("Condicion Group by Origen");
				this.txtGroupOrigen.setWidth("300px");

				this.txtSQL = new TextField("Condicion Where Origen");
				this.txtSQL.setWidth("300px");
				
				fila3.addComponent(this.txtCamposOrigen);
				fila3.addComponent(this.txtSQL);
				fila3.addComponent(this.txtGroupOrigen);
				
			fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbDestino = new ComboBox("BBDD Destino");
				
				this.txtTablaDestino= new TextField("Tabla");
				this.txtTablaDestino.setWidth("300px");
				
				fila4.addComponent(this.cmbDestino);
				fila4.addComponent(this.txtTablaDestino);
				
			fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
			

				this.cmbEjecucion =new ComboBox("Accion");
				this.cmbEjecucion.setWidth("250px");
				this.cmbEjecucion.setNullSelectionAllowed(false);
				this.cmbEjecucion.addItem("Ejecutar ahora");
				this.cmbEjecucion.addItem("Programar");
				this.cmbEjecucion.addItem("Consultar");

				this.txtHora = new TextField("Hora para programacion");
				this.txtHora.setWidth("120px");
				this.txtHora.setVisible(false);
				
				fila5.addComponent(this.cmbEjecucion);
				fila5.addComponent(this.txtHora);

			fila6 = new HorizontalLayout();
			fila6.setWidthUndefined();
			fila6.setSpacing(true);
			
				
				this.txtFecha =new TextField();
				this.txtFecha.setCaption("Fecha produccion como yyyyMMdd");
				this.txtFecha.setValue(RutinasFechas.convertirDateToStringYYYMMDD(RutinasFechas.conversionDeString(RutinasFechas.fechaActual())));
				this.txtFecha.setWidth("140px");
				this.txtFecha.setVisible(true);
				
				this.lblLastFecha = new Label();
				this.lblLastFecha.setCaption("Última Sincronizacion");
				
				this.chkTodos = new CheckBox("Procesar Todas");
				this.chkTodos.setValue(false);
				
				
				fila6.addComponent(this.txtFecha);
				fila6.addComponent(this.lblLastFecha);
				fila6.addComponent(this.chkTodos);
				fila6.setComponentAlignment(this.chkTodos, Alignment.BOTTOM_LEFT);
				
		controles.addComponent(fila1);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición Sincronización");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("1250px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cargarCombo();
	}

	private void cargarListeners()
	{

		this.chkTodos.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				txtFecha.setEnabled(!chkTodos.getValue());
			}
		});
		this.cmbPeticion.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbPeticion.getValue().toString().contains("anual"))
				{
					controles.removeComponent(fila6);
					controles.addComponent(fila2);
					controles.addComponent(fila3);
					controles.addComponent(fila4);
				}
				else if (cmbPeticion.getValue().toString().contains("gramados"))
				{
					controles.removeComponent(fila2);
					controles.removeComponent(fila3);
					controles.removeComponent(fila4);
					controles.removeComponent(fila6);
					controles.addComponent(fila5);
				}
				else if (cmbPeticion.getValue().toString().contains("ision"))
				{
					controles.removeComponent(fila2);
					controles.removeComponent(fila3);
					controles.removeComponent(fila4);
					controles.removeComponent(fila5);
					comprobarUltimaSincro();
					controles.addComponent(fila6);
				}
				else
				{
					controles.removeComponent(fila2);
					controles.removeComponent(fila3);
					controles.removeComponent(fila4);
					controles.removeComponent(fila5);
					controles.removeComponent(fila6);
				}
			}
		});
		
		this.cmbEjecucion.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbPeticion.getValue().toString().contains("ahora") || cmbEjecucion.getValue().toString().contains("Consultar"))
				{
					txtHora.setVisible(false);
				}
				else txtHora.setVisible(true);
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() 
		{
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				servidorSincronizadorBBDD ss = null;
				ArrayList<MapeoSincronizarTablas> arr = null;
				MapeoSincronizarTablas mapeo = null;

				if (cmbPeticion.getValue().toString().contains("ision"))
				{
					String todasSN="N";
					if (chkTodos.getValue()) todasSN="S";
					ss = new servidorSincronizadorBBDD(txtFecha.getValue(),todasSN);	
				}
				else if ((!cmbPeticion.getValue().toString().contains("Programados")) && todoEnOrden())
				{
					if (cmbPeticion.getValue().toString().contains("predeter"))
					{
						ss = new servidorSincronizadorBBDD(null,true);
					}
					else if (cmbPeticion.getValue().toString().contains("Manual"))
					{
						mapeo = new MapeoSincronizarTablas();
					
						mapeo.setBbddOrigen(cmbOrigen.getValue().toString().trim());
						mapeo.setBbddDestino(cmbDestino.getValue().toString().trim());
						mapeo.setTablaOrigen(txtTablaOrigen.getValue().trim());
						mapeo.setTablaDestino(txtTablaDestino.getValue().trim());
						mapeo.setSql(txtSQL.getValue().trim());
						mapeo.setCamposOrigen(txtCamposOrigen.getValue().trim());
						mapeo.setCamposGroupOrigen(txtGroupOrigen.getValue().trim());
				
						if (!cmbDestino.getValue().toString().trim().contentEquals("UNL GSYS"))
						{
							arr = new ArrayList<MapeoSincronizarTablas>();
							arr.add(mapeo);
						
							ss = new servidorSincronizadorBBDD(arr,true);
						}
						else
						{
							RutinasFicheros.generarUNLGsys(cmbOrigen.getValue().toString().trim(), txtTablaOrigen.getValue().trim(), txtSQL.getValue().trim());
						}
					}
				}
				else if (cmbPeticion.getValue().toString().contains("Programados"))
				{
					Scheduler scheduler;
			    	JobDetail script1 = null;
			    	Trigger disparador1 = null;
			    	Scheduler programacion1 = null;
			    	List<Trigger> triggers = null;
					try 
					{
						scheduler = new StdSchedulerFactory().getScheduler();
						for (String groupName : scheduler.getJobGroupNames()) 
						{
							 for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) 
							 {
								  String jobName = jobKey.getName();
								  String jobGroup = jobKey.getGroup();

								  if (jobGroup=="noche")
								  {

										  /*
										   * borro el job que hay programado y genero uno nuevo con los datos de la pantalla
										   * 
										   */
									  if (cmbEjecucion.getValue().toString().contains("ahora"))
									  {
										  /*
										   * para ejecutar ahora
										   */
										  triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
										  triggers.get(0).getTriggerBuilder().startNow();
									  }
									  
									  else if (cmbEjecucion.getValue().toString().contains("Programar") && txtHora.getValue()!=null && txtHora.getValue().length()>0)
									  {
										  scheduler.deleteJob(jobKey);
										  script1 = JobBuilder.newJob(trabajos.class).withIdentity("script1", "noche").build();
										  disparador1 = TriggerBuilder.newTrigger()
												  .withIdentity("cronDiario", "noche")
												  .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(new Integer(txtHora.getValue()), 00))
												  .build();
										  
										  //		.withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?"))
										  
										  scheduler.scheduleJob(script1, disparador1);
										  
									  }		
									  else
									  {
										  triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
										  Date nextFireTime = triggers.get(0).getNextFireTime();
										  	Notificaciones.getInstance().mensajeAdvertencia("[jobName] : " + jobName + " [groupName] : " + jobGroup + " - " + nextFireTime);										  
									  }
								  }
								  else
								  {
									  //get job's trigger
									  triggers = (List<Trigger>) scheduler.getTriggersOfJob(jobKey);
									  Date nextFireTime = triggers.get(0).getNextFireTime();
									  	Notificaciones.getInstance().mensajeAdvertencia("[jobName] : " + jobName + " [groupName] : " + jobGroup + " - " + nextFireTime);
								  }
							 }
						}
					} 
					catch (SchedulerException e) 
					{
						e.printStackTrace();
					}
						
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		}
		);
		
		
		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			@Override
			public void buttonClick(ClickEvent event) 
			{
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	
	private void comprobarUltimaSincro()
	{
		consultaCamaraVisionArtificialServer ccvas = consultaCamaraVisionArtificialServer.getInstance(CurrentUser.get());
		this.lblLastFecha.setValue(ccvas.comprobarUltimaSincronizacionCV());
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		
		if (cmbPeticion.getValue().toString().contains("Tablas")) return devolver;
		if (cmbPeticion.getValue().toString().contains("ision") && txtFecha.getValue()==null)
		{
			devolver = false;
			this.txtFecha.focus();
		}
		if (this.cmbOrigen.getValue()==null || this.cmbOrigen.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbOrigen.focus();
		}
		else if (this.cmbDestino.getValue()==null || this.cmbDestino.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbDestino.focus();
		}
		else if (this.txtTablaOrigen.getValue()==null || this.txtTablaOrigen.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtTablaOrigen.focus();
		}
		else if (this.txtTablaDestino.getValue()==null || this.txtTablaDestino.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtTablaDestino.focus();
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		this.cmbPeticion.addItem("Selección Manual");
		this.cmbPeticion.addItem("Tablas predeterminadas");
		this.cmbPeticion.addItem("Trabajos Programados");
		this.cmbPeticion.addItem("Camara Vision");
		
		this.cmbOrigen.addItem("GREENsys");
		this.cmbOrigen.addItem("Objective");
		this.cmbOrigen.addItem("Tabuenca");
		this.cmbOrigen.addItem("Pozuelo");
		this.cmbOrigen.addItem("Borja");
		this.cmbOrigen.addItem("MySQL");
		this.cmbOrigen.addItem("Tienda");
		this.cmbOrigen.addItem("Access");
		this.cmbDestino.addItem("GREENsys");
		this.cmbDestino.addItem("MySQL");
		this.cmbDestino.addItem("Objective");
		this.cmbDestino.addItem("Tienda");
		this.cmbDestino.addItem("Access");
		this.cmbDestino.addItem("UNL GSYS");
	}

}