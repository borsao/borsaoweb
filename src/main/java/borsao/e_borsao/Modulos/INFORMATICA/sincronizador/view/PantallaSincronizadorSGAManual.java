package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.Modulos.GENERALES.proveedores.server.consultaProveedoresServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorSGA;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PantallaSincronizadorSGAManual extends Ventana
{
	private PantallaSincronizadorSGAManual app = null;
	private SincronizadorSGAManualView origen = null;
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbTabla = null;
	private TextField txtValor= null;
	private TextField txtValor2= null;
	private TextField txtValor3= null;
	private CheckBox chkTodos= null;
	private CheckBox chkBorrado= null;
	private ComboBox cmbValor=null;

	private HorizontalLayout fila2 = null;
	private HorizontalLayout fila3 = null;
	private HorizontalLayout fila4 = null;
	
	private VerticalLayout controles = null;
	boolean cabeceraLineas=false;
	
	public PantallaSincronizadorSGAManual(SincronizadorSGAManualView r_view)
	{
		this.origen=r_view;
		this.app=this;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbTabla = new ComboBox("Tabla para traspasar");

				this.txtValor= new TextField("Valor a traspasar");
				this.txtValor.setDescription("Multiples valores separados por comas y entrecomillados(si procede)");
				this.txtValor.setWidth("300px");

				this.txtValor2= new TextField("Valor a traspasar");
				this.txtValor2.setDescription("Multiples valores separados por comas y entrecomillados(si procede)");
				this.txtValor2.setWidth("300px");
				this.txtValor2.setVisible(false);

				this.txtValor3= new TextField("Valor a traspasar");
				this.txtValor3.setDescription("Multiples valores separados por comas y entrecomillados(si procede)");
				this.txtValor3.setWidth("300px");
				this.txtValor3.setVisible(false);

				this.cmbValor= new ComboBox("Pedidos");
				this.cmbValor.setWidth("120px");
				this.cmbValor.setRequired(true);
				this.cmbValor.setNullSelectionAllowed(false);
				this.cmbValor.setInvalidAllowed(false);
				this.cmbValor.setNewItemsAllowed(false);
				this.cmbValor.setVisible(false);
	
				fila2.addComponent(this.cmbTabla);
				fila2.addComponent(this.txtValor);
				fila2.addComponent(this.txtValor2);
				fila2.addComponent(this.txtValor3);
				fila2.addComponent(this.cmbValor);

			fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.chkTodos = new CheckBox("Procesar Tabla Completa");
				this.chkTodos.setValue(false);
				
				fila3.addComponent(this.chkTodos);
				fila3.setComponentAlignment(this.chkTodos, Alignment.BOTTOM_CENTER);

			fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				
				this.chkBorrado= new CheckBox("Borrar datos tabla destino?");
				this.chkBorrado.setValue(false);
				fila4.addComponent(this.chkBorrado);
				fila4.setComponentAlignment(this.chkBorrado, Alignment.BOTTOM_CENTER);
				
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición Sincronización Manual SGA");
		this.setModal(true);
		this.setClosable(false);
		this.setResizable(true);
		this.setWidth("1250px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cargarCombo();
	}

	private void cargarListeners()
	{
		btnBotonAVentana.addClickListener(new ClickListener() 
		{
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(app, "Sincronizamos la tabla escogida?", "Si", "No", "sincronizar", "cancelar");
				getUI().addWindow(vt);
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			@Override
			public void buttonClick(ClickEvent event) 
			{
				origen.opcSalir.click();
				close();
			}
		});	
		
		cmbTabla.addValueChangeListener(new ValueChangeListener() 
		{			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbTabla.getValue()!=null && cmbTabla.getValue().toString().length()>0)
				{
					chkTodos.setEnabled(true);
					chkBorrado.setEnabled(true);
					txtValor2.setVisible(false);
					txtValor3.setVisible(false);
					cmbValor.setVisible(false);
					
					if (tieneDatos()) Notificaciones.getInstance().mensajeAdvertencia("La tabla destino tiene datos.");
					switch (cmbTabla.getValue().toString())
					{
						case "PROGRAMACION":
						{
							txtValor.setCaption("EJERCICIO = ...");
							txtValor2.setCaption("SEMANA = ...");
							txtValor3.setCaption("Linea = ...");
							txtValor2.setVisible(true);
							txtValor3.setVisible(true);
							txtValor2.setValue("");
							txtValor3.setValue("");
							cabeceraLineas=false;
							break;
						}
						case "ARTICULOS":
						{
							txtValor.setCaption("ARTICULO = ...");
							cabeceraLineas=false;
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							break;
						}
						case "CLIENTES":
						{
							txtValor.setCaption("CLIENTE = ...");
							cabeceraLineas=false;
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							break;
						}
						case "PROVEEDORES":
						{
							txtValor.setCaption("PROVEEDOR = ...");
							cabeceraLineas=false;
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							break;
						}
						case "ESCANDALLO":
						{
							txtValor.setCaption("PADRE = ...");
							chkTodos.setValue(false);
							chkTodos.setEnabled(false);
							chkBorrado.setValue(false);
							chkBorrado.setEnabled(false);
							cabeceraLineas=false;
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							break;
						}
						case "PEDIDOS COMPRA":
						{
							
							txtValor.setCaption("EJERCICIO = ...");
							txtValor.setValue(RutinasFechas.añoActualYYYY());
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							cmbValor.setVisible(true);
							cargarPedidos();
							cabeceraLineas=true;
							break;
						}
						case "PEDIDOS VENTA":
						{
							txtValor.setCaption("EJERCICIO = ...");
							txtValor.setValue(RutinasFechas.añoActualYYYY());
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							cmbValor.setVisible(true);
							cargarPedidos();

							cabeceraLineas=true;
							break;
						}
						default:
						{
							cabeceraLineas=false;
							txtValor2.setVisible(false);
							txtValor3.setVisible(false);
							txtValor2.setValue("");
							txtValor3.setValue("");
							txtValor.setCaption("Valor a traspasar");
						}
					}
				}
			}
		});
		
		txtValor.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (!comprobarExisteValor(txtValor.getValue()))
				{
					Notificaciones.getInstance().mensajeError("Valor incorrecto.");
					txtValor.focus();
				}
			}
		});
	}
	
	private boolean tieneDatos()
	{
		servidorSincronizadorBBDD ss = null;
		ss =new servidorSincronizadorBBDD();
		String tablaComprobacion = null;
		try
		{
			
			switch (cmbTabla.getValue().toString())
			{
				case "PROGRAMACION":
				{
					tablaComprobacion = "INTRANET";
					break;
				}
				case "ARTICULOS":
				{
					tablaComprobacion = "ARTICULOS";
					break;
				}
				case "CLIENTES":
				{
					tablaComprobacion = "CLIENTES";
					break;
				}
				case "PROVEEDORES":
				{
					tablaComprobacion = "PROVEEDORES";
					break;
				}
				case "ESCANDALLO":
				{
					tablaComprobacion = "RECETAS";
					break;
				}
				case "PEDIDOS COMPRA":
				{
					tablaComprobacion = "CABECERA_PEDIDOS_COMPRA";
					break;
				}
				case "PEDIDOS VENTA":
				{
					tablaComprobacion = "CABECERA_PEDIDOS_VTA";
					break;
				}
			}
			return ss.comprobarDestinoConDatos("Objective", tablaComprobacion);
		}
		catch (Exception ex)
		{
			return false;
		}
		finally
		{
			ss=null;
		}
	}
	
	private void cargarCombo()
	{
		this.cmbTabla.addItem("ARTICULOS");
		this.cmbTabla.addItem("CLIENTES");
		this.cmbTabla.addItem("PROVEEDORES");
		this.cmbTabla.addItem("ESCANDALLO");
		this.cmbTabla.addItem("PEDIDOS COMPRA");
		this.cmbTabla.addItem("PEDIDOS VENTA");
		this.cmbTabla.addItem("PROGRAMACION");
	}

	private void cargarPedidos()
	{
		consultaPedidosComprasServer cpcs = null;
		consultaPedidosVentasServer cpvs = null;
		ArrayList<String> strDocumentos= null;
		String documento = "";
		String ejercicio = "";
		
		if (cmbTabla.getValue()!=null && cmbTabla.getValue()=="PEDIDOS COMPRA")
		{
			documento = "C1";
			ejercicio = txtValor.getValue();
			
			if (txtValor.getValue()==null || txtValor.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeError("Debes rellenar correctamente los datos");
			}
			else
			{
				cmbValor.removeAllItems();
				
				cpcs = consultaPedidosComprasServer.getInstance(CurrentUser.get());
				strDocumentos = cpcs.recuperarPedidosPendientesCumplimentar(ejercicio, documento);
			
				Iterator<String> iter = strDocumentos.iterator();
			
				while (iter.hasNext())
				{
					String valor = iter.next();
					cmbValor.addItem(valor);
				}
			}
		}
		else if (cmbTabla.getValue()!=null && cmbTabla.getValue()=="PEDIDOS VENTA")
		{
			documento = "P1";
			ejercicio = txtValor.getValue();
			
			if (txtValor.getValue()==null || txtValor.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeError("Debes rellenar correctamente los datos");
			}
			else
			{
				cmbValor.removeAllItems();
				cpvs = consultaPedidosVentasServer.getInstance(CurrentUser.get());
				strDocumentos = cpvs.recuperarPedidosPendientesCumplimentarSGA(ejercicio, documento);
			
				Iterator<String> iter = strDocumentos.iterator();
			
				while (iter.hasNext())
				{
					String valor = iter.next();
					cmbValor.addItem(valor);
				}
			}
		}
	}

	public void aceptarProceso(String r_accion)
	{
		switch (r_accion.toUpperCase())
		{
			case "SINCRONIZAR":
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				servidorSincronizadorSGA ssSGA  = null;
				
				if ((cmbTabla.getValue()!=null && txtValor.getValue()!=null && txtValor.getValue().length()>0) || chkTodos.getValue())
				{
					ssSGA = servidorSincronizadorSGA.getInstance(CurrentUser.get());
					if (txtValor3.getValue()!=null && txtValor3.getValue().length()>0)
					{
						ssSGA.sincronizarSGA(cmbTabla.getValue().toString().trim(),txtValor.getValue().toString().trim(), txtValor2.getValue().toString().trim(), txtValor3.getValue().toString().trim(), chkTodos.getValue(),cabeceraLineas,chkBorrado.getValue());
					}
					else if (txtValor2.getValue()!=null && txtValor2.getValue().length()>0)
					{
						ssSGA.sincronizarSGA(cmbTabla.getValue().toString().trim(),txtValor.getValue().toString().trim(), txtValor2.getValue().toString().trim(), null, chkTodos.getValue(),cabeceraLineas,chkBorrado.getValue());
					}
					else
					{
						if (cmbValor.getValue()!=null && cmbValor.getValue().toString().length()>0)
						{
							ssSGA.sincronizarSGA(cmbTabla.getValue().toString().trim(),cmbValor.getValue().toString().trim(), null, null, chkTodos.getValue(),cabeceraLineas,chkBorrado.getValue());
						}
						else
						{
							ssSGA.sincronizarSGA(cmbTabla.getValue().toString().trim(),txtValor.getValue().toString().trim(), null, null, chkTodos.getValue(),cabeceraLineas,chkBorrado.getValue());
						}
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}

			}
		}
	}
	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private boolean comprobarExisteValor(String r_valor)
	{
		boolean existe = true;
		
		switch (cmbTabla.getValue().toString())
		{
			case "ARTICULOS":
			{
				consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
				existe = cas.comprobarArticulo(r_valor);
				cas=null;
				break;
			}
			case "CLIENTES":
			{
				consultaClientesServer cis = consultaClientesServer.getInstance(CurrentUser.get());
				existe = cis.comprobarCliente(r_valor);
				cis=null;
				break;
			}
			case "PROVEEDORES":
			{
				consultaProveedoresServer cps = consultaProveedoresServer.getInstance(CurrentUser.get());
				existe=cps.comprobarProveedor(r_valor);
				cps=null;
				break;
			}
			case "ESCANDALLO":
			{
				consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
				existe = ces.comrprobarEscandallo(r_valor);
				ces=null;
				break;
			}
//			case "PEDIDOS COMPRA":
//			{
//				consultaPedidosComprasServer cps = consultaPedidosComprasServer.getInstance(CurrentUser.get());
//				existe = cps.comprobarPedidoPendiente(r_valor);
//				cps=null;
//				break;
//			}
//			case "PEDIDOS VENTA":
//			{
//				consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
//				existe = cps.comprobarPedidoPendiente(r_valor);
//				cps=null;
//				break;
//			}
		}
		return existe;
	}
}