package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.view;

import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SincronizadorSGAManualView extends GridViewRefresh {

	public static final String VIEW_NAME = "SincronizadorSGA";
	private final String titulo = "Sincronizador SGA";

	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;

	private PantallaSincronizadorSGAManual vtPeticion = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    }
    
    public void generarGrid()
    {
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public SincronizadorSGAManualView() 
    {
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	setPantallaPeticion(true);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		vtPeticion = new PantallaSincronizadorSGAManual(this);
		getUI().addWindow(vtPeticion);
    }

    public void newForm()
    {    	
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }

    public void print()
    {    	
    }

    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    
    @Override
    public void reestablecerPantalla() {
    	
    }
    
    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    	
    }
    
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().navigateTo("");
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
