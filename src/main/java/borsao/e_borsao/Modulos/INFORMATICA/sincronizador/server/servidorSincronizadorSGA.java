package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class servidorSincronizadorSGA extends serverBasico
{
	private String campoId = null;
	private String campoId2 = null;
	private String campoId3 = null;
	private static servidorSincronizadorSGA instance;
	private ArrayList<MapeoSincronizarTablas> arr =null;
	
	public servidorSincronizadorSGA()
	{
	}

	public servidorSincronizadorSGA(String r_usuario)
	{
		
	}
	
	public static servidorSincronizadorSGA getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new servidorSincronizadorSGA(r_usuario);			
		}
		return instance;
	}
	
	public void sincronizarSGA(String r_tabla, String r_valor, String r_valor2, String r_valor3, boolean r_volcarTodo, boolean r_cabeceraLineas, boolean r_borrado)
	{
		String mensaje ="";
		boolean correcto = true;
		this.arr = new ArrayList<MapeoSincronizarTablas>();
		MapeoSincronizarTablas mapeo = new MapeoSincronizarTablas();
		
		switch (r_tabla)
		{
			case "PROGRAMACION":
			{
				correcto = this.comprobarOrdenProduccion(r_valor, r_valor2,r_valor3);
				if (!correcto) 
					mensaje = "No sincronizo. Comprueba los lotes de fabricación";				
				else
					mapeo = this.rellenarMapeoProgramacion(r_valor, r_valor2,r_valor3, r_volcarTodo);
				break;
			}
			case "ARTICULOS":
			{
				mapeo = this.rellenarMapeoArticulos(r_valor,r_volcarTodo);
				break;
			}
			case "CLIENTES":
			{
				mapeo = this.rellenarMapeoClientes(r_valor,r_volcarTodo);
				break;
			}
			case "PROVEEDORES":
			{
				mapeo = this.rellenarMapeoProveedores(r_valor,r_volcarTodo);
				break;
			}
			case "ESCANDALLO":
			{
				mapeo = this.rellenarMapeoEscandallo(r_valor,r_volcarTodo);
				break;
			}
			case "PEDIDOS COMPRA":
			{
				mapeo = this.rellenarMapeoPedidosCompra(r_valor,r_volcarTodo);
				break;
			}
			case "PEDIDOS VENTA":
			{
				mapeo = this.rellenarMapeoPedidosVenta(r_valor,r_volcarTodo);
				break;
			}
			case "INTRANET":
			{
				mapeo = this.rellenarMapeoProgramacion(r_valor,r_valor2,r_valor3,r_volcarTodo);
				break;
			}
		}
		mapeo.setBbddDestino("Objective");
		
		if (correcto)
		{
			if (r_volcarTodo)
				mapeo.setManual(false);
			else
				mapeo.setManual(true);
			
			this.arr.add(mapeo);
			
			if (r_cabeceraLineas)
				this.rellenarMapeoSincronizacionLineas(r_tabla, r_valor, r_volcarTodo);
		
			this.sincronizar(r_borrado,!r_volcarTodo);
		}
		else
			Notificaciones.getInstance().mensajeError(mensaje);
		
		this.arr = null;
	}
	private void rellenarMapeoSincronizacionLineas(String r_tabla, String r_valor, boolean r_volcarTodo)
	{
		
		MapeoSincronizarTablas mapeo = new MapeoSincronizarTablas();
		
		switch (r_tabla)
		{
			case "PEDIDOS COMPRA":
			{
				mapeo = this.rellenarMapeoLineasPedidosCompra(r_valor,r_volcarTodo);
				break;
			}
			case "PEDIDOS VENTA":
			{
				mapeo = this.rellenarMapeoLineasPedidosVenta(r_valor,r_volcarTodo);
				break;
			}
		}
		
		if (r_volcarTodo)
			mapeo.setManual(false);
		else
			mapeo.setManual(true);

		mapeo.setBbddDestino("Objective");
		this.arr.add(mapeo);
	}
	
	private void sincronizar(boolean r_borrado, boolean r_manual)
	{
		servidorSincronizadorBBDD ss = null;
		ss = new servidorSincronizadorBBDD(arr,false,r_borrado);
		
		if (ss.exito && r_manual)
		{
			this.tocarSemaforo();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Error en la sincronización. Probablemente orden terminada.");
		}
		
	}
	
	private void tocarSemaforo()
	{
		servidorSincronizadorBBDD ss = null;
		ss = new servidorSincronizadorBBDD();
		
		for (int i = 0; i < this.arr.size(); i++)
		{
			MapeoSincronizarTablas mapeo = (MapeoSincronizarTablas) this.arr.get(i);
			ss.tocarSemaforoSGA(null,mapeo.getTablaDestino(), 1);
		}
	}

	private MapeoSincronizarTablas rellenarMapeoArticulos(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="articulo";
		mapeo.setTablaOrigen("e_articu");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("ARTICULOS");
		mapeo.setCamposOrigen("articulo, descrip_articulo, tipo_articulo, e_famili.descripcion descripcion, e_subfam.descripcion descripcion2, caract_4, vl_caract_4, caract_3, vl_caract_3, caract_5, vl_caract_5, caract_6, vl_caract_6, caract_7, vl_caract_7 ");
		if (!r_volcarTodo)
			mapeo.setSql(", e_famili, e_subfam where e_famili.familia = e_articu.familia and e_subfam.familia = e_articu.familia and e_subfam.sub_familia = e_articu.sub_familia and " + campoId + " in ('" + r_valor.trim() + "')");
		else
			mapeo.setSql(", e_famili, e_subfam where e_famili.familia = e_articu.familia and e_subfam.familia = e_articu.familia and e_subfam.sub_familia = e_articu.sub_familia and articulo between '0101001' and '0111999' and articulo not matches '0110*' ");
		return mapeo;
	}

	private MapeoSincronizarTablas rellenarMapeoClientes(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="cliente";
		mapeo.setTablaOrigen("e_client");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("CLIENTES");
		mapeo.setCamposOrigen("cliente, nombre_comercial");
		if (!r_volcarTodo)
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ")");
		return mapeo;
	}

	private MapeoSincronizarTablas rellenarMapeoProveedores(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="Proveedor";
		mapeo.setTablaOrigen("e_provee");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("PROVEEDORES");
		mapeo.setCamposOrigen("proveedor, nombre_comercial");
		if (!r_volcarTodo)
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ")");

		return mapeo;
	}
	
	private MapeoSincronizarTablas rellenarMapeoEscandallo(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="padre";
		mapeo.setTablaOrigen("e__lconj");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("RECETAS");
		mapeo.setCamposOrigen("a.padre,b.descrip_articulo descripcion,a.orden,a.hijo, a.descripcion descripcion2, a.cantidad");
		mapeo.setSql(" a, e_articu b, e__cconj c ");
		if (!r_volcarTodo)
			mapeo.setSql(" a, e_articu b, e__cconj c where a.padre=c.padre and a.padre[1,7]=b.articulo and a." + campoId + " in ('" + r_valor.trim() + "') order by a.orden");
		
		return mapeo;
	}
	
	private MapeoSincronizarTablas rellenarMapeoLineasPedidosCompra(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="codigo";
		mapeo.setTablaOrigen("a_cc_l_0");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("LINEAS_PEDIDOS_COMPRA");
		mapeo.setCamposOrigen("documento,codigo, fecha_documento,articulo, descrip_articulo, unidades, unidades_serv, almacen, cumplimentacion, plazo_entrega");
		mapeo.setSql("");
		if (!r_volcarTodo)
		{
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ") and movimiento ='01' and cumplimentacion is null and articulo between '0104000' and '0109999'" );
		}
		else
		{
			mapeo.setSql(" where movimiento ='01' and cumplimentacion is null and articulo between '0104000' and '0109999' " );
		}
		
		return mapeo;
	}

	private MapeoSincronizarTablas rellenarMapeoLineasPedidosVenta(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="codigo";
		mapeo.setTablaOrigen("a_vp_l_0");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("LINEAS_PEDIDOS_VTA");
		mapeo.setCamposOrigen("documento,codigo, fecha_documento,articulo, descrip_articulo, unidades, unidades_serv, cumplimentacion, plazo_entrega, almacen");
		mapeo.setSql("");
		if (!r_volcarTodo)
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ") and movimiento  in ('51','53') and articulo between '0102000' and '0111999' and cumplimentacion is null ");
		else
			mapeo.setSql(" where movimiento  in ('51','53') and articulo between '0102000' and '0111999' and cumplimentacion is null ");
		return mapeo;
	}
	
	private MapeoSincronizarTablas rellenarMapeoPedidosVenta(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="codigo";
		mapeo.setTablaOrigen("a_vp_c_0");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("CABECERA_PEDIDOS_VTA");
		mapeo.setCamposOrigen("documento,codigo, fecha_documento, cliente, nombre_comercial, plazo_entrega, cumplimentacion, finalidad");
		mapeo.setSql("");
		if (!r_volcarTodo)
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ") and cumplimentacion is null and finalidad in ('FAB','Ok','Ppd') ");
		else
			mapeo.setSql(" where cumplimentacion is null and finalidad in ('FAB','Ok','Ppd') ");
		
		return mapeo;
	}
	
	private MapeoSincronizarTablas rellenarMapeoPedidosCompra(String r_valor, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="codigo";
		mapeo.setTablaOrigen("a_cc_c_0");
		mapeo.setBbddOrigen("GREENsys");
		mapeo.setTablaDestino("CABECERA_PEDIDOS_COMPRA");
		mapeo.setCamposOrigen("documento,codigo, fecha_documento,proveedor, nombre_comercial, plazo_entrega, cumplimentacion, almacen");
		mapeo.setSql("");
		if (!r_volcarTodo)
			mapeo.setSql(" where " + campoId + " in (" + r_valor.trim() + ") and cumplimentacion is null ");
		else
			mapeo.setSql(" where cumplimentacion is null ");
		
		return mapeo;
	}
	
	private MapeoSincronizarTablas rellenarMapeoProgramacion(String r_valor, String r_valor2, String r_valor3, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		this.campoId="ejercicio";
		this.campoId2="semana";
		this.campoId3="orden";
		mapeo.setTablaOrigen("prd_programacion");
		mapeo.setBbddOrigen("MySQL");
		mapeo.setTablaDestino("INTRANET");

		mapeo.setCamposOrigen("concat(ejercicio, lpad(semana,2,'0'),lpad(orden,3,'0')) as idprd_programacion,semana, fecha, tipoOperacion, lote_pt, articulo, unidades, velocidad, lote_jaulon_1 as lote_jaulon, anada, (case when articulo in ('0102091','0102006') then '1.5' else '1' end) as maquina");
		mapeo.setSql("");
		if (!r_volcarTodo)
			if (r_valor3!=null && r_valor3.length()>0)
				mapeo.setSql(" where " + campoId + " = (" + r_valor.trim() + ")  and " + campoId2 + " = (" + r_valor2 + ") and " + campoId3 + " = (" + r_valor3 + ") and estado in ('A','N') and length(articulo)>0 ");
			else
				mapeo.setSql(" where " + campoId + " = (" + r_valor.trim() + ")  and " + campoId2 + " = (" + r_valor2 + ") and estado in ('A','N') and length(articulo)>0 ");
		else
			mapeo.setSql(" where estado in ('A','N') and length(articulo)>0");

		return mapeo;
	}
	
	@Override
	public String semaforos() {
		return null;
	}
	
	private boolean comprobarOrdenProduccion(String r_ejercicio, String r_semana, String r_orden)
	{
		boolean correcto = false;
		consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
		correcto = cps.comprobarProgramadoSGA(r_ejercicio, r_semana, r_orden);
		return correcto;
		
	}
}