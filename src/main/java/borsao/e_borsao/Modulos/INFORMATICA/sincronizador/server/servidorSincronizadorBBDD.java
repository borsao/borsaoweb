package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.server.consultaSincronizaTablasServer;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.server.consultaCamaraVisionArtificialServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class servidorSincronizadorBBDD extends serverBasico
{
	public Double poolsize = new Double(25000);
	public boolean objective = false;
	public boolean exito=false;
	private String orig = null;
	private String dest = null;
	private MapeoSincronizarTablas datoSincro = null;	
	boolean actualizarSemaforo = true;	
	String carpeta = null;
	String fecha = null;
	String articulo = null;
	String hora = null;
	String bbdd = null;
	String rutaLectura = null;
	String[] nombresFicheros = null;
	String[] nombresCarpetas = null;

	public servidorSincronizadorBBDD()
	{
	}
	
	public servidorSincronizadorBBDD(ArrayList<MapeoSincronizarTablas> r_sincroRecibo, boolean r_mail)
	{
		/*
		 * Este es el que usa el CRON
		 */
		ArrayList<MapeoSincronizarTablas> sincro = null;
		String tablasProcesar=null;
		if (r_mail) ClienteMail.getInstance().envioMailSincro("j.claveria@bodegasborsao.com", null, "Sincronizador " + eBorsao.strEmpresaAlias, "Inicio Sincronizacion: " + new java.util.Date().toString());
		Notificaciones.getInstance().mensajeSeguimiento("Inicio sincro" + new java.util.Date());
		if (r_sincroRecibo==null)
		{
			consultaSincronizaTablasServer cst = new consultaSincronizaTablasServer(null);
			MapeoSincronizarTablas mapeo = new MapeoSincronizarTablas();
			mapeo.setBbddDestino("");
			tablasProcesar="S";

//			mapeo.setBbddDestino("Objective");
//			mapeo.setTablaDestino("RECETAS");
//			tablasProcesar="S";
			
			sincro = cst.datosSincronizarTablasGlobal(mapeo,tablasProcesar);
		}
		else
		{
			sincro=r_sincroRecibo;
		}
		
		this.sincronizar(sincro,true);
		Notificaciones.getInstance().mensajeSeguimiento("Fin sincro" + new java.util.Date());
		if (r_mail) ClienteMail.getInstance().envioMailSincro("j.claveria@bodegasborsao.com", null, "Sincronizador " + eBorsao.strEmpresaAlias, "Fin Sincronizacion: " + new java.util.Date().toString());
	}

	public servidorSincronizadorBBDD(ArrayList<MapeoSincronizarTablas> r_sincroRecibo, boolean r_mail, boolean r_borradoDestino)
	{
		/*
		 * Este se llama desde el sincronizados manual del SGA
		 */
		ArrayList<MapeoSincronizarTablas> sincro = null;
		
		if (r_mail) ClienteMail.getInstance().envioMailSincro("j.claveria@bodegasborsao.com", null, "Sincronizador " + eBorsao.strEmpresaAlias, "Inicio Sincronizacion: " + new java.util.Date().toString());
		Notificaciones.getInstance().mensajeSeguimiento("Inicio sincro" + new java.util.Date());
		if (r_sincroRecibo==null)
		{
			Notificaciones.getInstance().mensajeInformativo("No hay datos para sincronizar");
			exito=false;
		}
		else
		{
			sincro=r_sincroRecibo;
		}
		
		this.sincronizar(sincro, r_borradoDestino);
		Notificaciones.getInstance().mensajeSeguimiento("Fin sincro" + new java.util.Date());
		if (r_mail) ClienteMail.getInstance().envioMailSincro("j.claveria@bodegasborsao.com", null, "Sincronizador " + eBorsao.strEmpresaAlias, "Fin Sincronizacion: " + new java.util.Date().toString());
	}

	public servidorSincronizadorBBDD(String r_fecha, String r_falso)
	{
		/*
		 * Este se llama para la soncronizacion de la camara de vision artificial
		 */
		boolean ok=true;
		consultaCamaraVisionArtificialServer ccvas = consultaCamaraVisionArtificialServer.getInstance(CurrentUser.get());
		Notificaciones.getInstance().mensajeSeguimiento("Inicio sincro CV" + new java.util.Date());

		rutaLectura=LecturaProperties.rutaLecturaCV;
		
		nombresCarpetas = RutinasFicheros.recuperarNombresCarpeta(rutaLectura);
		
		for (int i=0; i< nombresCarpetas.length; i++)
		{
			carpeta = nombresCarpetas[i];
			if (carpeta.startsWith(r_fecha)|| r_falso.contentEquals("S"))
			{
				fecha=carpeta;
				boolean correcto = ccvas.comprobarFecha(fecha);
				if (correcto)
				{
					nombresFicheros = RutinasFicheros.recuperarNombresArchivosCarpeta(rutaLectura+"/"+carpeta, fecha, "mdb");
					
					for (int j=0; j< nombresFicheros.length; j++)
					{
						bbdd = carpeta + "/" + nombresFicheros[j];
						hora = nombresFicheros[j].substring(16, 22);
						articulo = nombresFicheros[j].substring(23, 30);
						
						ok = this.sincronizar(bbdd, fecha, hora, articulo);
						if (!ok)
						{
							break;
						}
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Problemas al procesar este dia.");
				}
			}
		}
		if (ok)
			Notificaciones.getInstance().mensajeAdvertencia("Proceso terminado");
		else
			Notificaciones.getInstance().mensajeError("Proceso terminado con errores");
			
		
		Notificaciones.getInstance().mensajeSeguimiento("Fin sincro CV" + new java.util.Date());
//		ClienteMail.getInstance().envioMailSincro("j.claveria@bodegasborsao.com", null, "Sincronizador CV" + eBorsao.strEmpresaAlias, "Fin Sincronizacion CV: " + new java.util.Date().toString());
	}

	public HashMap<Integer, HashMap<Integer, String>> generarEstructuraTabla(String r_bbdd, String r_tabla, String r_sql)
	{
		Connection conOrigen = null;
		PreparedStatement stOrigen = null;
		ResultSet rsOrigen = null;		
		ResultSetMetaData rsMetaDataOrigen = null;
		StringBuffer sql_origen = null;
		
		HashMap<Integer, HashMap<Integer, String>> sql_destino = null;		
		HashMap<Integer, String> valores = null;		
		Double contador = null;
		dest="GREENsys";
		try
		{
			conOrigen=this.establecerConexion(r_bbdd);
			
			sql_origen = new StringBuffer();
			sql_origen.append("select * from " + r_tabla);
			
			if (r_sql!=null && r_sql.length()>0) sql_origen.append(" " + r_sql.trim());
			
			stOrigen = conOrigen.prepareStatement(sql_origen.toString());
			rsOrigen = stOrigen.executeQuery();
			rsMetaDataOrigen = rsOrigen.getMetaData();

			contador = new Double(0);
			
			sql_destino = new HashMap<Integer, HashMap<Integer,String>>();
			while(rsOrigen.next())
			{
				
				valores = new HashMap<Integer,String>();
				
				for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
				{
					Object rdo = this.obtenerValorGsys(rsMetaDataOrigen, rsOrigen , j+1);
					if (rdo==null ||  rdo.equals(null) || rdo.equals("NULL") || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
					{
						rdo="";
					}
					valores.put(j,rdo.toString().trim());
					
				}
				Object rdo=this.obtenerValorGsys(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
				if (rdo==null ||  rdo.equals(null) || rdo.equals("NULL") || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
				{
					rdo="";
				}
				valores.put(rsMetaDataOrigen.getColumnCount(),rdo.toString().trim());
				
				sql_destino.put(contador.intValue(),valores);
				contador++;
			}				
		}
		catch (Exception ex)
		{
			
		}
		return sql_destino;
	}
	
	private void sincronizar (ArrayList<MapeoSincronizarTablas> sincroRecibo, boolean r_borradoDestino)
	{
		Connection conOrigen = null;
		Connection conDestino = null;
		PreparedStatement stDestino = null;
		PreparedStatement stOrigen = null;
		ResultSet rsOrigen = null;		
		ResultSet rsDestino	= null;		
		ResultSetMetaData rsMetaDataOrigen = null;
		StringBuffer sql_origen = null;
		StringBuffer sql_destino = null;		
		Iterator<MapeoSincronizarTablas> sincronizaciones = null;
		Boolean destinoCorrecto;
		Boolean conErrores;
		Double contador = null;
		
		/*
		 * recorremos el array y vamos extrayendo los hashmaps
		 */
		contador=new Double(0);
		conErrores=false;
		destinoCorrecto=true;
		sincronizaciones = sincroRecibo.iterator();
		
		while (sincronizaciones.hasNext())
		{
			datoSincro = (MapeoSincronizarTablas) sincronizaciones.next();

//			if (datoSincro.getBbddDestino().toString().trim().toUpperCase().contentEquals("OBJECTIVE"))
			{
				actualizarSemaforo=!datoSincro.isManual();
				
				try
				{
					 /*
					  * Establecemos la conexion con el origen
					  */
					conOrigen=this.establecerConexion(datoSincro.getBbddOrigen().toString().trim());
					orig = datoSincro.getBbddOrigen();
					if (conOrigen!=null)
					{
						sql_origen = new StringBuffer();
						
						if (datoSincro.getCamposOrigen()!=null && datoSincro.getCamposOrigen().length()>0)
						{
							sql_origen.append("select "  + datoSincro.getCamposOrigen() + " from " + datoSincro.getTablaOrigen().trim());
						}
						else
						{
							sql_origen.append("select * from " + datoSincro.getTablaOrigen().trim());
						}	
						
						if (datoSincro.getSql()!=null && datoSincro.getSql().length()>0) sql_origen.append(" " + datoSincro.getSql().trim());
	
						
						if (datoSincro.getBbddDestino().toString().trim().toUpperCase().contentEquals("OBJECTIVE") && !datoSincro.isManual())
						{
							/*
							 * recupero la fecha de ultima actualizacion
							 * agrego el where / and comparando contra la fecha obtenida
							 * 
							 * hay que tener en cuenta que la fecha obtenida está en MySQL y 
							 * no en la BBDD origen que me viene.
							 */
							String campoFechaTabla = campoFechaModificacion(datoSincro.getTablaOrigen().trim());
							String fechaEncontrada = valorSemaforo(datoSincro.getTablaOrigen().trim());
							
							if (fechaEncontrada!=null)
							{
								if (datoSincro.getSql()!=null && datoSincro.getSql().length()>0) 
									sql_origen.append(" AND " );
								else
									sql_origen.append(" where " );
								
								sql_origen.append(campoFechaTabla + " >= '" + fechaEncontrada + "' ");
							}
						}
						
						if (datoSincro.getCamposGroupOrigen()!=null && datoSincro.getCamposGroupOrigen().length()>0) sql_origen.append(" " + datoSincro.getCamposGroupOrigen().trim());
						
						stOrigen = conOrigen.prepareStatement(sql_origen.toString());
						rsOrigen = stOrigen.executeQuery();
						rsMetaDataOrigen = rsOrigen.getMetaData();
						
						/*
						 * Comprobamos la tabla destino
						 */
						destinoCorrecto = this.comprobarDestino(datoSincro.getBbddDestino().trim(), datoSincro.getTablaDestino().trim());
						if (!destinoCorrecto)
						{	
							this.crearTablaDestino(rsMetaDataOrigen, datoSincro.getBbddDestino().toString(), datoSincro.getTablaDestino());
						}
						/*
						 * Al ser copia total borro primero los datos de la tabla de destino
						 */
						conDestino=this.establecerConexion(datoSincro.getBbddDestino());
						dest = datoSincro.getBbddDestino();
						sql_destino = new StringBuffer();
						if(r_borradoDestino)
						{
							sql_destino.append("delete from " + datoSincro.getTablaDestino());
							
							if (!datoSincro.getBbddDestino().toString().trim().toUpperCase().contentEquals("OBJECTIVE") && datoSincro.getSql()!=null && datoSincro.getSql().length()>0) sql_destino.append(" " + datoSincro.getSql().trim());
							stDestino = conDestino.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
						}					
						/*
						 * Me recoge del origen los datos sean totales o incrementales
						 */
						stOrigen.close();
						rsOrigen.close();
						stOrigen = conOrigen.prepareStatement(sql_origen.toString());
						rsOrigen = stOrigen.executeQuery();
						rsMetaDataOrigen = rsOrigen.getMetaData();
						/*
						 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
						 * por otro los registros obtenidos
						 * 
						 * con todo esto montamos el SQL del insert en destino
						 */
						//	rsOrigen.last();
						conErrores=false;
	//					conDestino=this.establcerConexion(datoSincro.getBbddDestino());
						
						sql_destino = new StringBuffer();
						sql_destino.append("insert into " + datoSincro.getTablaDestino() + "(");
						
						// bucle para obtener los campos										
						for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
						{	
							sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
						}
						sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
						sql_destino.append(") values (");
						
						Boolean primero = true;
						Boolean hayDatos = false;
						while(rsOrigen.next())
						{
							hayDatos=true;
							if (contador.doubleValue()>= this.poolsize || sql_destino.toString().length()>300000)
							{
	//							conDestino=this.establcerConexion(datoSincro.getBbddDestino());
								sql_destino = new StringBuffer();
								sql_destino.append("insert into " + datoSincro.getTablaDestino() + "(");
								
								// bucle para obtener los campos										
								for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
								{	
									sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
								}
								sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
								sql_destino.append(") values (");
								
								primero = true;
								contador=new Double(0);
								
							}
							if (!primero) 
							{
								sql_destino.append(",(");
							}
							else
							{
								primero=false;
							}
							
							contador++;
							//bucle para obtener los datos
							
							for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
							{
								Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , j+1);
								if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
								{
									rdo=null;
									conErrores=false;
								}						
								sql_destino.append(rdo + ",");
							}
							//Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
							Object rdo=this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
							if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
							{
								rdo=null;
							}
							sql_destino.append(rdo + ")");
							
							if (contador.doubleValue()>= this.poolsize || sql_destino.toString().length()>300000)
							{
								stDestino = conDestino.prepareStatement(sql_destino.toString());
								stDestino.executeUpdate();
								exito=true;
							}
						}				
						
						if (!conErrores && hayDatos)
						{
							stDestino = conDestino.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
							exito=true;
						}
						if (objective)
						{
							String tabla = null;
							if (datoSincro.getTablaOrigen().trim().toUpperCase().contentEquals("E__LCONJ"))
								tabla = "e__cconj";
							else
								tabla = datoSincro.getTablaOrigen().trim();
								
							actualizarSemaforo(conOrigen, tabla);
							actualizarSemaforoSGA(conDestino, datoSincro.getTablaDestino().trim());
						}
					}
					else
					{
	//					Notificaciones.getInstance().mensajeError("Sincronizador: " + "No pudo establecerse la conexion");
						Notificaciones.getInstance().mensajeSeguimiento("Sincronizador: " + datoSincro.getBbddOrigen().toString().trim() + " No pudo establecerse la conexion");
						exito=false;
					}
				}
				catch (Exception sqlEx)
				{
	//				Notificaciones.getInstance().mensajeError(sqlEx.getMessage());
					exito=false;
					Notificaciones.getInstance().mensajeError("Sincronizador - Tabla Error : " + datoSincro.getTablaDestino());
					sqlEx.printStackTrace();	
				}
				finally
				{
					// al terminar las inserciones cierro las conexiones y los resultsets, statements, etc.				
					try
					{
						if (rsOrigen!=null)
						{
							rsOrigen.close();
						}
						if (rsDestino!=null)
						{
							rsDestino.close();
						}
						if (stOrigen!=null)
						{
							stOrigen.close();
						}
						if (stDestino!=null)
						{
							stDestino.close();
						}
						if (conOrigen!=null)
						{
							conOrigen.close();
						}
						if (conDestino!=null)
						{
							conDestino.close();
						}
					}
					catch (SQLException sqlEx)
					{
						exito=false;
	//					Notificaciones.getInstance().mensajeError("Sincronizador: " + sqlEx.getMessage());
						Notificaciones.getInstance().mensajeSeguimiento("Sincronizador: " + sqlEx.getMessage());
					}
				}
			}
		}
		/*
		 * - Preparamos el SQL de actualizacion
		 * - Ejecutamos el SQL
		 */
	}
	
	/**
	 * Método que se encarga de actualizar la tabla semáforo de la BBDD MySQL
	 * De este modo tendremos claro qué es lo último que se ha actualizado y podremos 
	 * volcar los datos creados/modificados a partir de la fecha indicada.
	 * 
	 * @param r_tabla
	 * 
	 * 			este parametro recoge el valor de la tabla origen de greensys
	 * 	
	 * Con este dato recogemos el valor máximo de la fecha ultima noodificacion y
	 * lo almacena en la tabla semaforo del mysql para la entrada coincidente con 
	 * el nombre de la tabla origen
	 * 
	 */
	
	private String campoFechaModificacion(String r_tablaOrigen)
	{
		String campo = null;
		
		switch (r_tablaOrigen.toUpperCase())
		{
			case "E_CLIENT":
			case "E_PROVEE":
			{
				campo = "fecha_ult_mod";
				break;
			}
			case "E_ARTICU":
			case "A_VP_L_0":
			case "A_CC_L_0":
			{
				campo = "fecha_ult_modif";
				break;
			}
			case "A_VP_C_0":
			case "A_CC_C_0":
			{
				campo = "fecha";
				break;
			}
			case "E__CCONJ":
			case "E__LCONJ":
			{
				campo = "fecha";
				break;
			}
		}
		
		return campo;
	}
	
	private String valorSemaforo(String r_tablaOrigen)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;
		PreparedStatement preparedStatement = null;
		
		String campo = null;
		String fecha = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL = new StringBuffer();
		
		campo = campoFechaModificacion(r_tablaOrigen);
		
		try
		{
			cadenaSQL.append(" select fecha from semaforo where entrada = '" + r_tablaOrigen + "' ");
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + RutinasFechas.fechaActual());
			con = this.establecerConexion("Mysql");
			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			while(rsOpcion.next())
			{
				fecha = rsOpcion.getString("fecha");
			}
			return fecha;
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeSeguimiento(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeSeguimiento(ex.getMessage());
			}
		}
		return null;
	}
	private void actualizarSemaforo(Connection r_conOrigen, String r_tablaOrigen)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;
		PreparedStatement preparedStatement = null;
		
		String campo = null;
		String fecha = null;
		
		StringBuffer cadenaSQL = new StringBuffer();

		if (actualizarSemaforo)
		{
			cadenaSQL = new StringBuffer();
			
			campo = campoFechaModificacion(r_tablaOrigen);
			
			try
			{
				cadenaSQL.append(" select max(" + campo + ") fecha from " + r_tablaOrigen);
	
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + RutinasFechas.fechaActual());
				
				cs = r_conOrigen.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				while(rsOpcion.next())
				{
					fecha = rsOpcion.getString("fecha");
				}
	
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" UPDATE semaforo  ");
				cadenaSQL.append(" set fecha = '" + fecha + "' ");
				
								if (r_tablaOrigen.contentEquals("e__cconj")) r_tablaOrigen="e__lconj";
				
				cadenaSQL.append(" where entrada = '" + r_tablaOrigen + "' ");
				
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + RutinasFechas.fechaActual());
	
				con= this.establecerConexion("MYSQL");			
			    preparedStatement = con.prepareStatement(cadenaSQL.toString());
			    preparedStatement.executeUpdate();
			    
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeSeguimiento(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					Notificaciones.getInstance().mensajeSeguimiento(ex.getMessage());
				}
			}
		}
		else
			Notificaciones.getInstance().mensajeSeguimiento("Sincronziacion manual");
	}

	private void actualizarSemaforoSGA(Connection r_conDestino, String r_tablaDestino)
	{
		this.tocarSemaforoSGA(r_conDestino, r_tablaDestino, 1);
	}
	
	private Object obtenerValor(ResultSetMetaData r_medataData, ResultSet r_rs, int r_col) throws SQLException
	{
		String cadena = null;
		try
		{
			if (r_rs.equals(null))
			{
				return "";
			}
//			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			switch (r_medataData.getColumnType(r_col)) 
			{
				case Types.LONGVARCHAR:
					if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "";
					}
	    			else
	    			{
	    				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
	    				return "'" + cadena.trim() + "'" ;
	    			}
				case Types.BIGINT:
					return r_rs.getLong(r_col);
		    	case Types.CHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "";
					}
	    			else
	    			{
	    				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
	    				return "'" + cadena.trim() + "'" ;
	    			}
		    	case Types.DECIMAL:
		    		if (objective && r_medataData.getScale(r_col)==0)
		    		{
		    			if (datoSincro.getTablaDestino().contentEquals("a_ubic_0"))
		    				return r_rs.getString(r_col);
		    			else
		    				RutinasNumericas.formatearDoubleDecimalesDouble(r_rs.getDouble(r_col),0);
		    		}
		    		else
		    			return r_rs.getDouble(r_col);
		    	case Types.TIMESTAMP:
		    		if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					Date ts = r_rs.getDate(r_col);
					String fecha = ts.toString();
					return "'" + fecha.trim() + "'";
		    	case Types.DATE:
					if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					if (r_rs.getDate(r_col)!=null)					
					{
						if (dest.contentEquals("GREENsys"))
						{
							return "'" + RutinasFechas.convertirDateToString(r_rs.getDate(r_col)) + "'" ;
						}
						else if (dest.toUpperCase().contentEquals("MYSQL"))
						{
							return "'" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_rs.getDate(r_col))) + "'" ;
						}
						else if (dest.toUpperCase().contentEquals("OBJECTIVE"))
						{
							return "'" + RutinasFechas.convertirDateToString(r_rs.getDate(r_col),"dd-MM-yyyy") + "'" ;
						}
						else return r_rs.getDate(r_col);
					}
					else return "";
		    	case Types.SMALLINT:
		    		return r_rs.getInt(r_col);
		    	case Types.REAL:
		    		return r_rs.getInt(r_col);
		    	case Types.INTEGER:
		    		return r_rs.getInt(r_col);
		    	case Types.FLOAT:
		    		return r_rs.getFloat(r_col);
		    	case Types.VARCHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "";
					}
	    			else
	    			{
	    				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
	    				return "'" + cadena.trim() + "'" ;
	    			}
		    	case Types.DOUBLE:
		    		return r_rs.getDouble(r_col);
		    	case Types.TIME:
		    		if (r_rs.getTime(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00";
					}
		    		return "'" + r_rs.getTime(r_col) +"'" ;
		    	case Types.BIT:
		    		return "'" + r_rs.getByte(r_col) +"'" ;		    				    			
		    		
			    default:
			    	Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			        throw new Error("Unknown column type");
		    }
		}
		catch (Exception sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Sincronizador: " + r_medataData.getColumnName(r_col) + " " + sqlEx.getMessage());
		}				
		return null;
	}
	private String obtenerValorGsys(ResultSetMetaData r_medataData, ResultSet r_rs, int r_col) throws SQLException
	{
		String cadena = null;
		try
		{
			if (r_rs.equals(null))
			{
				return "";
			}
//			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			switch (r_medataData.getColumnType(r_col)) 
			{
			case Types.LONGVARCHAR:
				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
				return cadena ;
			case Types.BIGINT:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getLong(r_col)),".",",");
			case Types.CHAR:
				if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
				{
					return "";
				}
				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
				return cadena;
			case Types.DECIMAL:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getDouble(r_col)),".",",");
			case Types.TIMESTAMP:
				if (r_rs.getDate(r_col)==null)					
				{
					return "";
				}
				return RutinasFechas.convertirDateToString(r_rs.getDate(r_col));
			case Types.DATE:
				if (r_rs.getDate(r_col)==null)					
				{
					return "";
				}
				return RutinasFechas.convertirDateToString(r_rs.getDate(r_col));
			case Types.SMALLINT:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getInt(r_col)),".",",");
			case Types.REAL:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getInt(r_col)),".",",");
			case Types.INTEGER:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getInt(r_col)),".",",");
			case Types.FLOAT:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getFloat(r_col)),".",",");
			case Types.VARCHAR:
				if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
				{
					return "";
				}
				cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
				return cadena;
			case Types.DOUBLE:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getDouble(r_col)),".",",");
			case Types.TIME:
				if (r_rs.getTime(r_col)==null)					
				{
					return "";
				}
				return r_rs.getTime(r_col).toString();
			case Types.BIT:
				return RutinasCadenas.reemplazar(String.valueOf(r_rs.getByte(r_col)),".",",");
				
			default:
				Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
				throw new Error("Unknown column type");
			}
		}
		catch (Exception sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Sincronizador: " + r_medataData.getColumnName(r_col) + " " + sqlEx.getMessage());
		}				
		return null;
	}
	
	private Connection establecerConexion(String r_conector) throws ClassNotFoundException, SQLException
	{
		
		Connection con = null;
		this.poolsize = new Double(25000);
		this.objective = false;
		
		if (r_conector.toUpperCase().equals("MYSQL"))
		{
			//esto rula para mysq seguro
			
				String dbUrl="jdbc:mysql://192.168.6.6:3306/laboratorio?characterEncoding=Cp437&autoReconnect=true";
				String user="root";
				String pass="compaq123";
				
				try
				{
						Class.forName("com.mysql.jdbc.Driver");
						con = DriverManager.getConnection(dbUrl, user, pass);				 
				}
				catch (ClassNotFoundException e) 
				{
					e.printStackTrace();
				}

		}
		else if (r_conector.toUpperCase().equals("OBJECTIVE"))
		{
			this.poolsize=new Double(1000);
			this.objective = true;
			
			//esto rula para mysq seguro
			con = connectionManager.getInstance().establecerConexionSQLInd();
		}
		
		else if (r_conector.toUpperCase().equals("TIENDA"))
		{
			//esto rula para mysq seguro
			con = connectionManager.getInstance().establecerConexionTienda();
		}
		else if (r_conector.toUpperCase().equals("ACCESS"))
		{
			//esto rula para access
			try {
				System.out.println(RutinasFechas.horaActual());
	            con = DriverManager.getConnection("jdbc:ucanaccess://" + LecturaProperties.rutaLecturaCV +"/" + this.bbdd + ";inactivityTimeout=0;skipIndexes=true;memory=true" , "", "");
	        } catch (OutOfMemoryError oome) {
	            //Log the info
	            System.err.println("Array size too large");
	            System.err.println("Max JVM memory: " + Runtime.getRuntime().maxMemory());
	        }
		}
		else if (r_conector.toUpperCase().equals("GREENSYS"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionInd();
		}
		else if (r_conector.toUpperCase().equals("BORJA"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionBorja();
		}
		else if (r_conector.toUpperCase().equals("POZUELO"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionPozuelo();
		}
		else if (r_conector.toUpperCase().equals("TABUENCA"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionTabuenca();
		}


		else
		{
			return null;
		}
		
		return con;
	}
	private Boolean comprobarDestino(String r_bbdd, String r_tabla) throws ClassNotFoundException, SQLException
	{
		PreparedStatement stDestino=null;
		Connection conDestino = null;

		try
		{
			
			conDestino=this.establecerConexion(r_bbdd);
			StringBuffer sql_destino = new StringBuffer();
			sql_destino.append("select count(*) from " + r_tabla);
			
			stDestino = conDestino.prepareStatement(sql_destino.toString());
			stDestino.executeQuery();
			return true;
		}
		catch (SQLException sqlEx)
		{
//			Notificaciones.getInstance().mensajeError("No Exsite la tabla destino: " + r_bbdd + " - " + r_tabla);
			return false;
		}		
	}
	public Boolean comprobarDestinoConDatos(String r_bbdd, String r_tabla) throws ClassNotFoundException, SQLException
	{
		Statement stDestino=null;
		ResultSet rsOpcion = null;		
		Connection conDestino = null;
		
		try
		{
			
			conDestino=this.establecerConexion(r_bbdd);
			StringBuffer sql_destino = new StringBuffer();
			sql_destino.append("select * from " + r_tabla);
			
			stDestino = conDestino.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion = stDestino.executeQuery(sql_destino.toString());
			
			while (rsOpcion.next()) 
			{
				return true;
			}
		}
		catch (SQLException sqlEx)
		{
//			Notificaciones.getInstance().mensajeError("No Exsite la tabla destino: " + r_bbdd + " - " + r_tabla);
			return false;
		}
		return false;
	}

	private Boolean crearTablaDestino(ResultSetMetaData r_medataData, String r_bbdd, String r_tabla) throws ClassNotFoundException, SQLException
	{
		PreparedStatement stDestino=null;		
		Connection conDestino = null;
		
		try
		{
			conDestino=this.establecerConexion(r_bbdd);
			StringBuffer sql_destino = new StringBuffer();
			sql_destino.append("create table " + r_tabla + "(");
			
			for (int j=1; j<r_medataData.getColumnCount();j++)
			{
				sql_destino.append(r_medataData.getColumnName(j) + " ");
				sql_destino.append(this.tipoDato(r_medataData.getColumnName(j), r_medataData.getColumnType(j), r_medataData.getPrecision(j), r_medataData.getScale(j)));
//				switch (r_medataData.getColumnType(j)) 
//				{
//					case Types.LONGVARCHAR:
//						sql_destino.append("VARCHAR(500) ");
//			    		break;
//					case Types.BIGINT:
//						sql_destino.append("LONG ");
//			    		break;
//			    	case Types.CHAR:
//			    		sql_destino.append("VARCHAR(" + r_medataData.getPrecision(j)+ ") ");
//			    		break;
//			    	case Types.DECIMAL:
//			    		sql_destino.append("DECIMAL(" + r_medataData.getPrecision(j)+ "," + r_medataData.getScale(j)+ ") ");
//			    		break;
//			    	case Types.TIMESTAMP:
//			    		sql_destino.append("DATE ");
//			    		break;
//			    	case Types.DATE:
//			    		sql_destino.append("DATE ");
//			    		break;
//		    		case Types.SMALLINT:
//		    			sql_destino.append("SMALLINT ");
//		    			break;
//			    	case Types.DOUBLE:
//			    		sql_destino.append("LONG ");
//			    		break;	    			
//			    	case Types.REAL:
//			    		sql_destino.append("LONG ");
//			    		break;
//			    	case Types.INTEGER:
//			    		sql_destino.append("INTEGER ");
//			    		break;
//			    	case Types.FLOAT:
//			    		sql_destino.append("FLOAT ");
//			    		break;
//			    	case Types.VARCHAR:
//			    		sql_destino.append("VARCHAR(" + r_medataData.getPrecision(j)+ ") ");
//			    		break;
//			    	case Types.TIME:
//			    		sql_destino.append("TIME ");
//			    		break;
//			    	case Types.BIT:
//			    		sql_destino.append("VARCHAR(1) ");
//			    		break;
//			    	
//			    	default:
//			    		Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(j));
//			    		Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(r_medataData.getColumnType(j)));
//	//		    		Notificaciones.getInstance().mensajeError("Sincronizador: " + "Campo de tipo desconocido " + r_medataData.getColumnName(j) + " " + r_medataData.getColumnType(j));
//			    		Notificaciones.getInstance().mensajeSeguimiento("Sincronizador: " + "Campo de tipo desconocido " + r_medataData.getColumnName(j) + " " + r_medataData.getColumnType(j));
//			    		return false;
//				}
				if (r_medataData.isNullable(j)==0)
				{
					sql_destino.append("NOT NULL, ");
				}
				else
				{
					sql_destino.append(", ");
				}
				
			}
			sql_destino.append(r_medataData.getColumnName(r_medataData.getColumnCount()) + " ");
			sql_destino.append(this.tipoDato(r_medataData.getColumnName(r_medataData.getColumnCount()), r_medataData.getColumnType(r_medataData.getColumnCount()), r_medataData.getPrecision(r_medataData.getColumnCount()), r_medataData.getScale(r_medataData.getColumnCount())));

			if (r_medataData.isNullable(r_medataData.getColumnCount())==0)
			{
				sql_destino.append("NOT NULL ");
			}
			sql_destino.append(")");
			
			stDestino = conDestino.prepareStatement(sql_destino.toString());
			stDestino.executeUpdate();
			return true;
		}
		catch (SQLException sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Sincronizador: " + "Error al buscar la tabla destino: " + r_bbdd + " - " + r_tabla);
			return false;
		}		
	}

	private String tipoDato(String r_nombreColumna, int r_tipo, int r_precision, int r_escala)
	{
		String rdo = null;
		
		switch (r_tipo) 
		{
			case Types.LONGVARCHAR:
				rdo = "BLOB ";
				break;
			case Types.BIGINT:
				rdo = "LONG ";
				break;
	    	case Types.CHAR:
	    		rdo = "VARCHAR(" + r_precision +  ") ";
	    		break;
	    	case Types.DECIMAL:
	    		rdo = "DECIMAL(" + r_precision+ "," + r_escala + ") ";
	    		break;
	    	case Types.TIMESTAMP:
	    		rdo = "DATE ";
	    		break;
	    	case Types.DATE:
	    		rdo = "DATE ";
	    		break;
    		case Types.SMALLINT:
    			rdo = "SMALLINT ";
    			break;
	    	case Types.DOUBLE:
	    		rdo = "LONG ";
	    		break;	    			
	    	case Types.REAL:
	    		rdo = "LONG ";
	    		break;
	    	case Types.INTEGER:
	    		rdo = "INTEGER ";
	    		break;
	    	case Types.FLOAT:
	    		rdo = "FLOAT ";
	    		break;
	    	case Types.VARCHAR:
	    		rdo = "VARCHAR(" + r_precision + ") ";
	    		break;
	    	case Types.TIME:
	    		rdo = "TIME ";
	    		break;
	    	case Types.BIT:		    		
	    		rdo = "VARCHAR(1) ";
	    		break;
	    	default:
//	    		Notificaciones.getInstance().mensajeSeguimiento(r_nombreColumna);
//	    		Notificaciones.getInstance().mensajeSeguimiento(String.valueOf(r_tipo));
//	    		Notificaciones.getInstance().mensajeError("Sincronizador: " + "Campo de tipo desconocido " + r_medataData.getColumnName(r_medataData.getColumnCount()) + " " + r_medataData.getColumnType(r_medataData.getColumnCount()));
	    		Notificaciones.getInstance().mensajeSeguimiento("Sincronizador: " + "Campo de tipo desconocido " + r_nombreColumna + " " + r_tipo);
	    		return "";
		}
		
		return rdo;
	}
	@Override
	public String semaforos() {
		return null;
	}

	public void tocarSemaforoSGA(Connection r_conDestino, String r_tabla, Integer r_valor)
	{
		Connection con =null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
		    cadenaSQL.append(" UPDATE dbo.SEMAFORO set ");
			cadenaSQL.append(" dbo.SEMAFORO.Estado =? ");
			cadenaSQL.append(" where dbo.SEMAFORO.Tabla = ? ");

			if (r_conDestino==null)
			{
				con= this.establecerConexion("Objective");
				preparedStatement = con.prepareStatement(cadenaSQL.toString());
			}
			else
				preparedStatement = r_conDestino.prepareStatement(cadenaSQL.toString());
		    
		    if (r_valor!=null)
		    {
		    	preparedStatement.setInt(1, r_valor);
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_tabla!=null)
		    {
		    	preparedStatement.setString(2, r_tabla);
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
	    	preparedStatement.executeUpdate();
		}
		catch (ClassNotFoundException clsEx)
		{
		}
		catch(SQLException sqlEx)
		{
			
		}
		catch (Exception ex)
		{
			
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				Notificaciones.getInstance().mensajeError(ex.getMessage());
			}
		}
	}

	private boolean sincronizar (String r_bbdd, String r_fecha, String r_hora, String r_articulo)
	{
		Connection conOrigen								=	null;
		Connection conDestino								=	null;
		PreparedStatement stDestino 						=	null;
		PreparedStatement stOrigen 							=	null;
		ResultSet rsOrigen 									= 	null;		
		ResultSet rsDestino									= 	null;		
		ResultSetMetaData rsMetaDataOrigen					= 	null;
		StringBuffer sql_origen 							= 	null;
		StringBuffer sql_destino 							= 	null;		

		Boolean destinoCorrecto;
		Boolean conErrores;
		Double contador = null;
		
		/*
		 * recorremos el array y vamos extrayendo los hashmaps
		 */
		contador=new Double(0);
		conErrores=false;
		destinoCorrecto=true;
		try
		{
			 /*
			  * Establecemos la conexion con el origen
			  */
			
			conOrigen=this.establecerConexion("access");
			sql_origen = new StringBuffer();
			sql_origen.append("select * from GeneralCounters");
			stOrigen = conOrigen.prepareStatement(sql_origen.toString());
			rsOrigen = stOrigen.executeQuery();
			rsMetaDataOrigen = rsOrigen.getMetaData();
			/*
			 * Me recoge del origen los datos sean totales o incrementales
			 */
			stOrigen = conOrigen.prepareStatement(sql_origen.toString());
			rsOrigen = stOrigen.executeQuery();
			rsMetaDataOrigen = rsOrigen.getMetaData();
				
			/*
			 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
			 * por otro los registros obtenidos
			 * 
			 * con todo esto montamos el SQL del insert en destino
			 */
			//	rsOrigen.last();
			conErrores=false;
			conDestino=this.establecerConexion("mysql");
				
			sql_destino = new StringBuffer();
			sql_destino.append("insert into qc_cv_generalCounters (id, fecha,hora,articulo,");
				
			// bucle para obtener los campos										
			for (int c=1; c<rsMetaDataOrigen.getColumnCount()-1;c++)
			{	
				sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
			}
			sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
			sql_destino.append(") values (null,'" + r_fecha + "','" + r_hora + "','" + r_articulo +"',");
				
			Boolean primero = true;
				
			while(rsOrigen.next())
			{
				if (!primero) 
				{
					sql_destino.append(",(null,'" + r_fecha + "','" + r_hora + "','" + r_articulo +"',");
				}
				else
				{
					primero=false;
				}
				contador++;
				//bucle para obtener los datos
					
				for (int j=1; j<rsMetaDataOrigen.getColumnCount()-1;j++)
				{
					Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , j+1);
					if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
					{
						rdo=null;
						conErrores=false;
					}						
					sql_destino.append(rdo + ",");
				}
				//Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
				Object rdo=this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
				if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
				{
					rdo=null;
				}
				sql_destino.append(rdo + ")");

				if (!conErrores)
				{
					stDestino = conDestino.prepareStatement(sql_destino.toString());
					stDestino.executeUpdate();
				}
				
				if (stDestino!=null)
				{
					stDestino.close();
				}
				if (conDestino!=null)
				{
					conDestino.close();
				}
				
			}
		}
		catch (Exception sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Error al sincronizar la camara " + sqlEx.getMessage());
			sqlEx.printStackTrace();
			return false;
		}
		finally
		{
			// al terminar las inserciones cierro las conexiones y los resultsets, statements, etc.				
			try
			{
				if (rsOrigen!=null)
				{
					rsOrigen.close();
					rsOrigen=null;
				}
				if (stOrigen!=null)
				{
					stOrigen.close();
					stOrigen=null;
				}
				if (conOrigen!=null)
				{
					conOrigen.close();
					conOrigen=null;;
				}
				System.out.println(Runtime.getRuntime().freeMemory());
				System.gc();
				System.out.println(Runtime.getRuntime().freeMemory());
			}
			catch (SQLException sqlEx)
			{
				Notificaciones.getInstance().mensajeError("Error al sincronizar la camara " + sqlEx.getMessage());	
				return false;
			}				
		}
		return true;
	}

}