package borsao.e_borsao.Modulos.INFORMATICA.sincronizador.trabajos;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.vaadin.server.VaadinRequest;

import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@DisallowConcurrentExecution
public class trabajos implements Job
{
	 public void execute(JobExecutionContext context) throws JobExecutionException 
	 {
		 new servidorSincronizadorBBDD(null,true);		 
	 } 
}