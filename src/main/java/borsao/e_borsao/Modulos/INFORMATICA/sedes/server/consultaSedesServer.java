package borsao.e_borsao.Modulos.INFORMATICA.sedes.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.INFORMATICA.sedes.modelo.MapeoSedes;

public class consultaSedesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaSedesServer instance;
	
	public consultaSedesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSedesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSedesServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoSedes> datosSedesGlobal(MapeoSedes r_mapeo)
	{
		ResultSet rsSedes = null;		
		ArrayList<MapeoSedes> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_sedes.idCodigo sed_id, ");
		cadenaSQL.append(" inf_sedes.identificacion sed_ide, ");
		cadenaSQL.append(" inf_sedes.descripcion sed_des ");
     	cadenaSQL.append(" FROM inf_sedes ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdentificacion()!=null && r_mapeo.getIdentificacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" identificacion = '" + r_mapeo.getIdentificacion() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idCodigo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsSedes.TYPE_SCROLL_SENSITIVE,rsSedes.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsSedes= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoSedes>();
			
			while(rsSedes.next())
			{
				MapeoSedes mapeoSedes = new MapeoSedes();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSedes.setIdCodigo(rsSedes.getInt("sed_id"));
				mapeoSedes.setIdentificacion(rsSedes.getString("sed_ide"));
				mapeoSedes.setDescripcion(rsSedes.getString("sed_des"));
				vector.add(mapeoSedes);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsSedes = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(inf_sedes.idCodigo) sed_sig ");
     	cadenaSQL.append(" FROM inf_sedes ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsSedes.TYPE_SCROLL_SENSITIVE,rsSedes.CONCUR_UPDATABLE);
			rsSedes= cs.executeQuery(cadenaSQL.toString());
			
			while(rsSedes.next())
			{
				return rsSedes.getInt("sed_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoSedes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO inf_sedes ( ");
    		cadenaSQL.append(" inf_sedes.idCodigo, ");
    		cadenaSQL.append(" inf_sedes.identificacion, ");
    		cadenaSQL.append(" inf_sedes.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getIdentificacion()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getIdentificacion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoSedes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE inf_sedes set ");
			cadenaSQL.append(" inf_sedes.identificacion=?, ");
		    cadenaSQL.append(" inf_sedes.descripcion=? ");
		    cadenaSQL.append(" WHERE inf_sedes.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setString(1, r_mapeo.getIdentificacion());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoSedes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM inf_sedes ");            
			cadenaSQL.append(" WHERE inf_sedes.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public Integer obtenerId(String r_identificacion)
	{

		ResultSet rsSedes = null;		
		Integer idObtenido = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_sedes.idCodigo sed_id, ");
		cadenaSQL.append(" inf_sedes.identificacion sed_ide, ");
		cadenaSQL.append(" inf_sedes.descripcion sed_des ");
     	cadenaSQL.append(" FROM inf_sedes ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_identificacion!=null && r_identificacion.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" identificacion = '" + r_identificacion + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsSedes.TYPE_SCROLL_SENSITIVE,rsSedes.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsSedes= cs.executeQuery(cadenaSQL.toString());
			
			while(rsSedes.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				idObtenido = rsSedes.getInt("sed_id");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return idObtenido;
	}
	public String obtenerDescId(Integer r_id)
	{

		ResultSet rsSedes = null;		
		String  ident= null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_sedes.idCodigo sed_id, ");
		cadenaSQL.append(" inf_sedes.identificacion sed_ide, ");
		cadenaSQL.append(" inf_sedes.descripcion sed_des ");
     	cadenaSQL.append(" FROM inf_sedes ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_id!=null && r_id>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idCodigo = " + r_id);
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsSedes.TYPE_SCROLL_SENSITIVE,rsSedes.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsSedes= cs.executeQuery(cadenaSQL.toString());
			
			while(rsSedes.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				ident = rsSedes.getString("sed_ide");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return ident;
	}
}