package borsao.e_borsao.Modulos.INFORMATICA.sedes.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoSedes extends MapeoGlobal
{
	private String identificacion;
	private String descripcion;

	public MapeoSedes()
	{
		this.setIdentificacion("");
		this.setDescripcion("");
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}