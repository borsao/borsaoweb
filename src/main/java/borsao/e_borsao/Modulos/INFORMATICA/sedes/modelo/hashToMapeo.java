package borsao.e_borsao.Modulos.INFORMATICA.sedes.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoSedes mapeo = null;
	 
	 public MapeoSedes convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoSedes();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setIdentificacion(r_hash.get("identificacion"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoSedes convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoSedes();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setIdentificacion(r_hash.get("identificacion"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoSedes r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());		 
		 this.hash.put("identificacion", r_mapeo.getIdentificacion());
		 return hash;		 
	 }
}