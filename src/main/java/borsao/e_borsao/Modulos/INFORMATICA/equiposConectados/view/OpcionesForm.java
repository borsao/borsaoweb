package borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.EquiposConectadosGrid;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.MapeoEquiposConectados;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.server.consultaEquiposConectadosServer;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.MapeoRacks;
import borsao.e_borsao.Modulos.INFORMATICA.racks.server.consultaRacksServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	
{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoEquiposConectados mapeoEquiposConectados = null;
	 
	 private consultaEquiposConectadosServer cus = null;	 
	 private equiposConectadosView uw = null;
	 private BeanFieldGroup<MapeoEquiposConectados> fieldGroup;
	 
	 public OpcionesForm(equiposConectadosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoEquiposConectados>(MapeoEquiposConectados.class);
        fieldGroup.bindMemberFields(this);

        this.cargarCombo();
        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{
		this.ip.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (ip.getValue().toString()!=null && isBusqueda()==false)
				{
					if (comprobar(ip.getValue().trim()))
					{
						Notificaciones.getInstance().mensajeError("IP ya existente. Revísalo por favor");
						ip.focus();	
					}
				}
			}
		});

	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoEquiposConectados = (MapeoEquiposConectados) uw.grid.getSelectedRow();
                eliminarEquipo(mapeoEquiposConectados);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoEquiposConectados = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearEquipo(mapeoEquiposConectados);
	 			}
	 			else
	 			{
	 				MapeoEquiposConectados mapeoEquipos_orig = (MapeoEquiposConectados) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoEquiposConectados = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarEquipo(mapeoEquiposConectados,mapeoEquipos_orig);
	 				hm=null;
	 			}
	 			if (((EquiposConectadosGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 				uw.grid.setEnabled(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	
        		btnGuardar.setCaption("Guardar");
        		btnEliminar.setEnabled(true);
                setEnabled(false);
                if (((EquiposConectadosGrid) uw.grid)!=null)
	 			{			
    				((equiposConectadosView) uw).grid.removeAllColumns();			
    				((equiposConectadosView) uw).barAndGridLayout.removeComponent(uw.grid);
    				((equiposConectadosView) uw).grid=null;
    				((equiposConectadosView) uw).setHayGrid(false);
     			}
                if (((equiposConectadosView) uw).opcionesEscogidas!=null) ((equiposConectadosView) uw).generarGrid(((equiposConectadosView) uw).opcionesEscogidas);
                
                uw.regresarDesdeForm();
                removeStyleName("visible");
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarEquipo(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.boca.getValue()!=null && this.boca.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("boca", this.boca.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("boca", "");
		}
		if (this.roseta.getValue()!=null && this.roseta.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("roseta", this.roseta.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("roseta", "");
		}
		if (this.ip.getValue()!=null && this.ip.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ip", this.ip.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ip", "");
		}
		
		if (this.direccionPublica.getValue()!=null && this.direccionPublica.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("dir", this.direccionPublica.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("dir", "");
		}

		if (this.identificador.getValue()!=null && this.identificador.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("identificador", this.identificador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("identificador", "");
		}
		
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		if (this.user.getValue()!=null && this.user.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("user", this.user.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("user", "");
		}
		if (this.pass.getValue()!=null && this.pass.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("pass", this.pass.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("pass", "");
		}
		if (this.cuenta.getValue()!=null && this.cuenta.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cuenta", this.cuenta.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cuenta", "");
		}

		if (this.descRack.getValue()!=null && this.descRack.getValue().toString().length()>0) 
		{
			consultaRacksServer css = consultaRacksServer.getInstance(CurrentUser.get());
	    	Integer id = css.obtenerId(this.descRack.getValue().toString());
			opcionesEscogidas.put("idRack", id.toString());
		}
		else
		{
			opcionesEscogidas.put("idRack", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarEquipo(null);
	}
	
	public void editarEquipo(MapeoEquiposConectados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoEquiposConectados(); 
		}
		else
		{
			uw.grid.setEnabled(false);
//			consultaRacksServer css = consultaRacksServer.getInstance(CurrentUser.get());
//	    	Integer id = css.obtenerId(this.identificador.getValue().toString());
//			opcionesEscogidas.put("idSede", id.toString());
			this.descRack.setValue(r_mapeo.getDescRack());
			this.identificador.setValue(r_mapeo.getIdentificador());
		}
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoEquiposConectados>(r_mapeo));
		if (r_mapeo!=null) this.descRack.setValue(r_mapeo.getDescRack());
		identificador.focus();
	}
	
	public void eliminarEquipo(MapeoEquiposConectados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = consultaEquiposConectadosServer.getInstance(CurrentUser.get());
		((EquiposConectadosGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoEquiposConectados>) ((EquiposConectadosGrid) uw.grid).vector).remove(r_mapeo);
		
		uw.regresarDesdeForm();
        removeStyleName("visible");
       	activarDesactivarControles(true);

	}
	
	public void modificarEquipo(MapeoEquiposConectados r_mapeo, MapeoEquiposConectados r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaEquiposConectadosServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			r_mapeo.setDescRack(this.descRack.getValue().toString());
			r_mapeo.setIdRack(r_mapeo_orig.getIdRack());
			cus.guardarCambios(r_mapeo);		
			((EquiposConectadosGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
			

			uw.regresarDesdeForm();
	        removeStyleName("visible");
	       	activarDesactivarControles(true);
		}
	}
	public void crearEquipo(MapeoEquiposConectados r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaEquiposConectadosServer(CurrentUser.get());
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			r_mapeo.setDescRack(this.descRack.getValue().toString());
			
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoEquiposConectados>(r_mapeo));
				if (((EquiposConectadosGrid) uw.grid)!=null)
				{
					((EquiposConectadosGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoEquiposConectados> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoEquiposConectados= item.getBean();
            if (this.mapeoEquiposConectados.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.descRack.getValue()==null || this.descRack.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el Switch");
    		return false;
    	}
    	if (this.boca.getValue()==null || this.boca.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la boca de conexión");
    		return false;
    	}
    	if ((this.roseta.getValue()==null || this.roseta.getValue().toString().length()==0) && (this.ip.getValue()==null || this.ip.getValue().toString().length()==0)) 
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la roseta y/o la ip del equipo que está conectado ");
    		return false;
    	}
    	return true;
    }

	private void cargarCombo()
	{
		consultaRacksServer crf = consultaRacksServer.getInstance(CurrentUser.get());
		ArrayList<MapeoRacks> racks = crf.datosRacksGlobal(null);
		if (racks!=null && racks.size()>0)
		{
			for (int i = 0; i<racks.size();i++)
			{
				MapeoRacks mapeo = (MapeoRacks) racks.get(i);
				this.descRack.addItem(mapeo.getIdentificacion());
			}
			
			this.descRack.setNullSelectionAllowed(false);
			this.descRack.setNewItemsAllowed(false);
		}
		racks=null;
		crf=null;
		
		this.identificador.addItem("Router");
		this.identificador.addItem("Datafono");
		this.identificador.addItem("Dect");
		this.identificador.addItem("Switch");
		this.identificador.addItem("Máquina");
		this.identificador.addItem("Servidor");
		this.identificador.addItem("Cabina");
		this.identificador.addItem("Teléfono");
		this.identificador.addItem("Ordenador");
		this.identificador.addItem("Impresora");
		this.identificador.addItem("Wifi");
		this.identificador.addItem("Telefonía");
		this.identificador.addItem("Otros");
		this.identificador.addItem("Firewall");
		this.identificador.addItem("");
		
		this.identificador.setNullSelectionAllowed(false);
		this.identificador.setNewItemsAllowed(false);
	}
	
	private boolean comprobar(String r_ip)
	{
		boolean duplicada = false;
		Integer cuantos=0;
		
		MapeoEquiposConectados mapeoEnvio = new MapeoEquiposConectados();
		mapeoEnvio.setIp(r_ip);
		
			consultaEquiposConectadosServer cecs = consultaEquiposConectadosServer.getInstance(CurrentUser.get());
			cuantos = cecs.comprobarIPDuplicada(mapeoEnvio);
					
			duplicada = cuantos>1;
		
		return duplicada;
	}
}
