package borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoEquiposConectados mapeo = null;
	 
	 public MapeoEquiposConectados convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquiposConectados();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("idRack")!=null && r_hash.get("idRack").length()>0) this.mapeo.setIdRack(new Integer(r_hash.get("idRack")));
		 
		 this.mapeo.setIdentificador(r_hash.get("identificador"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setBoca(r_hash.get("boca"));
		 this.mapeo.setRoseta(r_hash.get("roseta"));
		 this.mapeo.setIp(r_hash.get("ip"));
		 this.mapeo.setDireccionPublica(r_hash.get("dir"));
		 this.mapeo.setUser(r_hash.get("user"));
		 this.mapeo.setPass(r_hash.get("pass"));
		 this.mapeo.setCuenta(r_hash.get("cuenta"));
		 this.mapeo.setDescRack(r_hash.get("descRack"));
		 return mapeo;		 
	 }
	 
	 public MapeoEquiposConectados convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquiposConectados();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("idRack")!=null && r_hash.get("idRack").length()>0) this.mapeo.setIdRack(new Integer(r_hash.get("idRack")));
		 
		 this.mapeo.setIdentificador(r_hash.get("identificador"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setBoca(r_hash.get("boca"));
		 this.mapeo.setRoseta(r_hash.get("roseta"));
		 this.mapeo.setIp(r_hash.get("ip"));
		 this.mapeo.setDireccionPublica(r_hash.get("dir"));
		 this.mapeo.setUser(r_hash.get("user"));
		 this.mapeo.setPass(r_hash.get("pass"));
		 this.mapeo.setCuenta(r_hash.get("cuenta"));
		 this.mapeo.setDescRack(r_hash.get("descRack"));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoEquiposConectados r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());		 
		 if (r_mapeo.getIdRack()!=null) this.hash.put("idRack", r_mapeo.getIdRack().toString());

		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());		 
		 this.hash.put("identificador", r_mapeo.getIdentificador());
		 this.hash.put("observaciones", r_mapeo.getObservaciones());
		 this.hash.put("boca", r_mapeo.getBoca());		 
		 this.hash.put("roseta", r_mapeo.getRoseta());
		 this.hash.put("ip", r_mapeo.getIp());
		 this.hash.put("dir", r_mapeo.getDireccionPublica());
		 this.hash.put("user", r_mapeo.getUser());
		 this.hash.put("pass", r_mapeo.getPass());
		 this.hash.put("cuenta", r_mapeo.getCuenta());
		 this.hash.put("descRack", r_mapeo.getDescRack());
		 return hash;		 
	 }
}