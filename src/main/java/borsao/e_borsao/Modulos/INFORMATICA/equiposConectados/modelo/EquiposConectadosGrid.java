package borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.view.equiposConectadosView;
import borsao.e_borsao.Modulos.INFORMATICA.racks.view.pantallaEquiposConectados;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EquiposConectadosGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	public String rutaAnyDesk = null;
	private equiposConectadosView app = null;
	
    public EquiposConectadosGrid(equiposConectadosView r_app, ArrayList<MapeoEquiposConectados> r_vector) {
        
        this.vector=r_vector;
        this.app = r_app;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Equipos Conectados");		
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoEquiposConectados.class);        
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("idRack","estado", "bocas", "any", "descRack", "boca", "roseta", "ip", "direccionPublica", "identificador", "descripcion", "observaciones", "user", "pass","cuenta");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("estado").setHeaderCaption("");
    	this.getColumn("estado").setSortable(false);
    	this.getColumn("estado").setWidth(new Double(50));

    	this.getColumn("bocas").setHeaderCaption("");
    	this.getColumn("bocas").setSortable(false);
    	this.getColumn("bocas").setWidth(new Double(50));
    	
    	this.getColumn("any").setHeaderCaption("");
    	this.getColumn("any").setSortable(false);
    	this.getColumn("any").setWidth(new Double(50));
    	
    	
    	this.getColumn("descRack").setHeaderCaption("Switch");
    	this.getColumn("descRack").setSortable(true);
    	this.getColumn("descRack").setWidth(new Double(150));
    	this.getColumn("boca").setHeaderCaption("Boca");
    	this.getColumn("boca").setSortable(true);
    	this.getColumn("boca").setWidth(new Double(75));
    	this.getColumn("roseta").setHeaderCaption("Roseta");
    	this.getColumn("roseta").setSortable(true);
    	this.getColumn("roseta").setWidth(new Double(75));
    	this.getColumn("ip").setHeaderCaption("Ip");
    	this.getColumn("ip").setSortable(true);
    	this.getColumn("ip").setWidth(new Double(150));

    	this.getColumn("direccionPublica").setHeaderCaption("ANYDesk");
    	this.getColumn("direccionPublica").setSortable(true);
    	this.getColumn("direccionPublica").setWidth(new Double(150));

    	this.getColumn("identificador").setHeaderCaption("Nombre");
    	this.getColumn("descripcion").setHeaderCaption("Descripción");
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("user").setHeaderCaption("Usuario");
    	this.getColumn("pass").setHeaderCaption("Pass");
    	this.getColumn("cuenta").setHeaderCaption("Cuenta");
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idRack").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoEquiposConectados) {
                // The actual description text is depending on the application
                if ("estado".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Web Switch";
                }
                else if ("bocas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a dispositivos conectados";
                }
                else if ("any".equals(cell.getPropertyId()))
                {
                	descriptionText = "ANYDesk. Acceso remoto";
                }
            }
        }
        return descriptionText;
    }
	public void asignarEstilos() 
	{		
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	String estilo = null;
            	if ( "estado".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonRack";
            	}
            	else if ( "any".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonPC";
            	}

            	else if ( "bocas".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonBoca";
            	}
            	return estilo;
            }
        });
    	
    	
	}

	public void cargarListeners() 
	{		
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoEquiposConectados mapeo = (MapeoEquiposConectados) event.getItemId();
            		
            		if (event.getPropertyId().toString().equals("estado"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;

	            		if (mapeo.getDescRack()!=null)
	            		{
	            			String url = "http://" + mapeo.getDescRack();
	            			getUI().getPage().open(url, "_blank");
	            		}

	            	}
            		else if (event.getPropertyId().toString().equals("bocas"))
	            	{
            			activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getDescRack()!=null && mapeo.getDescRack().length()>0)
	            		{
	        	    		/*
	        	    		 * Abriremos ventana peticion parametros
	        	    		 * rellenaremos la variables publicas
	        	    		 * 		cajas y anada
	        	    		 * 		llamaremos al metodo generacionPdf
	        	    		 */
	            			pantallaEquiposConectados vtPeticion = new pantallaEquiposConectados("Switch " + mapeo.getDescRack(), mapeo.getDescRack());
	        	    		getUI().addWindow(vtPeticion);
	            		}
            		}
            		else if (event.getPropertyId().toString().equals("any"))
            		{
            			if (mapeo.getDireccionPublica()!=null && mapeo.getDireccionPublica().length()>0)
            			{
            				conexionRemota(mapeo.getDireccionPublica());
            			}
            			else
            			{
            				Notificaciones.getInstance().mensajeError("No se puede conectar a este dispositivo");
            			}
            		}
	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
            	}
            }
    	});
    	
	}

	private void conexionRemota(String r_dir)
	{
//		import java.io.IOException;
//	    import java.io.InputStream;
//	    import java.io.InputStreamReader;
//	    import java.util.Scanner;
//	     
//		r_ip =  "592937547";
		this.rutaAnyDesk = r_dir;
		
		
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this.app, "Debes tener anydesk.exe en la ruta c:\\tmp para poder conectarte al equipo", "Conectar", "Cancelar", "Conectar", null);
		getUI().addWindow(vt);
		
//        ProcessBuilder pb = new ProcessBuilder("c:\\tmp\\AnyDesk.exe", r_dir);
////	            pb.directory(new File("src"));
////	     
//	            try {
//					Process process = pb.start();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//	            IOThreadHandler outputHandler = new IOThreadHandler(process.getInputStream());
//	            outputHandler.start();
//	            process.waitFor();
	   
//	            Notificaciones.getInstance().mensajeSeguimiento(outputHandler.getOutput());
//	        }
//	     
//	        private static class IOThreadHandler extends Thread {
//	            private InputStream inputStream;
//	            private StringBuilder output = new StringBuilder();
//	     
//	            IOThreadHandler(InputStream inputStream) {
//	                this.inputStream = inputStream;
//	            }
//	     
//	            public void run() {
//	                Scanner br = null;
//	                try {
//	                    br = new Scanner(new InputStreamReader(inputStream));
//	                    String line = null;
//	                    while (br.hasNextLine()) {
//	                        line = br.nextLine();
//	                        output.append(line
//	                                + System.getProperty("line.separator"));
//	                    }
//	                } finally {
//	                    br.close();
//	                }
//	            }
//	     
//	            public StringBuilder getOutput() {
//	                return output;
//	            }
//	        }
//	    }
//	    //
	}
	@Override
	public void calcularTotal() {
		
	}
}
