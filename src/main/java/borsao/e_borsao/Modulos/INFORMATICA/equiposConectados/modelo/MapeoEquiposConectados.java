package borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEquiposConectados extends MapeoGlobal
{
	private String identificador;
	private String descripcion;
	private String observaciones;
	private String boca;
	private String roseta;
	private String ip;
	private String direccionPublica;
	private String user;
	private String pass;
	private String cuenta;
	private Integer idRack;
	private String descRack;
	
	private String estado="";
	private String bocas="";
	private String any="";
	
	public MapeoEquiposConectados()
	{
		this.setIdentificador("");
		this.setDescripcion("");
		this.setObservaciones("");
		this.setUser("");
		this.setIp("");
		this.setDireccionPublica("");
		this.setRoseta("");
		this.setBoca("");
		this.setPass("");
		this.setCuenta("");
		this.setDescRack("");
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getBoca() {
		return boca;
	}

	public void setBoca(String boca) {
		this.boca = boca;
	}

	public String getRoseta() {
		return roseta;
	}

	public void setRoseta(String roseta) {
		this.roseta = roseta;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Integer getIdRack() {
		return idRack;
	}

	public void setIdRack(Integer idRack) {
		this.idRack = idRack;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getDescRack() {
		return descRack;
	}

	public void setDescRack(String descRack) {
		this.descRack = descRack;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getBocas() {
		return bocas;
	}

	public void setBocas(String bocas) {
		this.bocas = bocas;
	}

	public String getAny() {
		return any;
	}

	public void setAny(String any) {
		this.any = any;
	}

	public String getDireccionPublica() {
		return direccionPublica;
	}

	public void setDireccionPublica(String direccionPublica) {
		this.direccionPublica = direccionPublica;
	}
}