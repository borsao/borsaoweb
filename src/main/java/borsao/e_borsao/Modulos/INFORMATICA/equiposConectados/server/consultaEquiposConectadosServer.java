package borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.MapeoEquiposConectados;

public class consultaEquiposConectadosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEquiposConectadosServer instance;
	
	public consultaEquiposConectadosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEquiposConectadosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEquiposConectadosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoEquiposConectados> datosEquiposConectadosGlobal(MapeoEquiposConectados r_mapeo)
	{
		ResultSet rsEquipos = null;		
		ArrayList<MapeoEquiposConectados> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_equipos.idCodigo equ_id, ");
		cadenaSQL.append(" inf_equipos.boca * 1 equ_boc, ");
		cadenaSQL.append(" inf_equipos.roseta equ_ros, ");
		cadenaSQL.append(" inf_equipos.ip equ_ip, ");
		cadenaSQL.append(" inf_equipos.direccionPublica equ_dir, ");
		cadenaSQL.append(" inf_equipos.identificador equ_ide, ");
		cadenaSQL.append(" inf_equipos.descripcion equ_des, ");
		cadenaSQL.append(" inf_equipos.observaciones equ_obs, ");
		cadenaSQL.append(" inf_equipos.usuario equ_usu, ");
		cadenaSQL.append(" inf_equipos.cuenta equ_cue, ");
		cadenaSQL.append(" inf_equipos.pass equ_pas, ");
		cadenaSQL.append(" inf_racks.idCodigo rac_id, ");
		cadenaSQL.append(" inf_racks.identificador rac_ide ");
     	cadenaSQL.append(" FROM inf_equipos ");
     	cadenaSQL.append(" inner join inf_racks on inf_racks.idCodigo = inf_equipos.idRack ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdentificador()!=null && r_mapeo.getIdentificador().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.identificador = '" + r_mapeo.getIdentificador() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.observaciones like '%" + r_mapeo.getObservaciones() + "%' ");
				}
				if (r_mapeo.getBoca()!=null && r_mapeo.getBoca().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" boca = '" + r_mapeo.getBoca() + "' ");
				}
				if (r_mapeo.getRoseta()!=null && r_mapeo.getRoseta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" roseta = '" + r_mapeo.getRoseta() + "' ");
				}
				if (r_mapeo.getIp()!=null && r_mapeo.getIp().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ip like '%" + r_mapeo.getIp() + "%' ");
				}
				if (r_mapeo.getUser()!=null && r_mapeo.getUser().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" usuario = '" + r_mapeo.getUser() + "' ");
				}
				if (r_mapeo.getPass()!=null && r_mapeo.getPass().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pass = '" + r_mapeo.getPass() + "' ");
				}
				if (r_mapeo.getCuenta()!=null && r_mapeo.getCuenta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cuenta = '" + r_mapeo.getCuenta() + "' ");
				}
				if (r_mapeo.getIdRack()!=null && r_mapeo.getIdRack()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.idCodigo = " + r_mapeo.getIdRack());
				}
				if (r_mapeo.getDescRack()!=null && r_mapeo.getDescRack().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.identificador = " + r_mapeo.getDescRack());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by 12 asc, 2  asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsEquipos= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoEquiposConectados>();
			
			while(rsEquipos.next())
			{
				MapeoEquiposConectados mapeoEquiposConectados = new MapeoEquiposConectados();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquiposConectados.setIdCodigo(rsEquipos.getInt("equ_id"));

				mapeoEquiposConectados.setBoca(rsEquipos.getString("equ_boc"));
				mapeoEquiposConectados.setRoseta(rsEquipos.getString("equ_ros"));
				mapeoEquiposConectados.setIp(rsEquipos.getString("equ_ip"));
				mapeoEquiposConectados.setDireccionPublica(rsEquipos.getString("equ_dir"));
				mapeoEquiposConectados.setIdentificador(rsEquipos.getString("equ_ide"));
				mapeoEquiposConectados.setDescripcion(rsEquipos.getString("equ_des"));
				mapeoEquiposConectados.setObservaciones(rsEquipos.getString("equ_obs"));
				mapeoEquiposConectados.setUser(rsEquipos.getString("equ_usu"));
				mapeoEquiposConectados.setPass(rsEquipos.getString("equ_pas"));
				mapeoEquiposConectados.setCuenta(rsEquipos.getString("equ_cue"));
				mapeoEquiposConectados.setIdRack(rsEquipos.getInt("rac_id"));
				mapeoEquiposConectados.setDescRack(rsEquipos.getString("rac_ide"));
				vector.add(mapeoEquiposConectados);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public MapeoEquiposConectados datosEquiposConectados(MapeoEquiposConectados r_mapeo)
	{
		ResultSet rsEquipos = null;		
		StringBuffer cadenaWhere =null;
		MapeoEquiposConectados mapeoEquiposConectados =null; 
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_equipos.idCodigo equ_id, ");
		cadenaSQL.append(" inf_equipos.boca equ_boc, ");
		cadenaSQL.append(" inf_equipos.roseta equ_ros, ");
		cadenaSQL.append(" inf_equipos.ip equ_ip, ");
		cadenaSQL.append(" inf_equipos.direccionPublica equ_dir, ");
		cadenaSQL.append(" inf_equipos.identificador equ_ide, ");
		cadenaSQL.append(" inf_equipos.descripcion equ_des, ");
		cadenaSQL.append(" inf_equipos.observaciones equ_obs, ");
		cadenaSQL.append(" inf_equipos.usuario equ_usu, ");
		cadenaSQL.append(" inf_equipos.cuenta equ_cue, ");
		cadenaSQL.append(" inf_equipos.pass equ_pas, ");
		cadenaSQL.append(" inf_racks.idCodigo rac_id, ");
		cadenaSQL.append(" inf_racks.identificador rac_ide ");
     	cadenaSQL.append(" FROM inf_equipos ");
     	cadenaSQL.append(" inner join inf_racks on inf_racks.idCodigo = inf_equipos.idRack ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdentificador()!=null && r_mapeo.getIdentificador().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.identificador = '" + r_mapeo.getIdentificador() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getBoca()!=null && r_mapeo.getBoca().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" boca = '" + r_mapeo.getBoca() + "' ");
				}
				if (r_mapeo.getRoseta()!=null && r_mapeo.getRoseta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" roseta = '" + r_mapeo.getRoseta() + "' ");
				}

				if (r_mapeo.getIp()!=null && r_mapeo.getIp().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ip = '" + r_mapeo.getIp() + "' ");
				}
				if (r_mapeo.getUser()!=null && r_mapeo.getUser().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" usuario = '" + r_mapeo.getUser() + "' ");
				}
				if (r_mapeo.getPass()!=null && r_mapeo.getPass().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pass = '" + r_mapeo.getPass() + "' ");
				}
				if (r_mapeo.getCuenta()!=null && r_mapeo.getCuenta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cuenta = '" + r_mapeo.getCuenta() + "' ");
				}
				if (r_mapeo.getIdRack()!=null && r_mapeo.getIdRack()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.idRack = " + r_mapeo.getIdRack());
				}
				if (r_mapeo.getDescRack()!=null && r_mapeo.getDescRack().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.identificador = '" + r_mapeo.getDescRack() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by inf_racks.identificador asc, inf_equipos.boca  asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsEquipos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquipos.next())
			{
				mapeoEquiposConectados = new MapeoEquiposConectados();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquiposConectados.setIdCodigo(rsEquipos.getInt("equ_id"));

				mapeoEquiposConectados.setBoca(rsEquipos.getString("equ_boc"));
				mapeoEquiposConectados.setRoseta(rsEquipos.getString("equ_ros"));
				mapeoEquiposConectados.setIp(rsEquipos.getString("equ_ip"));
				mapeoEquiposConectados.setDireccionPublica(rsEquipos.getString("equ_dir"));
				mapeoEquiposConectados.setIdentificador(rsEquipos.getString("equ_ide"));
				mapeoEquiposConectados.setDescripcion(rsEquipos.getString("equ_des"));
				mapeoEquiposConectados.setObservaciones(rsEquipos.getString("equ_obs"));
				mapeoEquiposConectados.setUser(rsEquipos.getString("equ_usu"));
				mapeoEquiposConectados.setPass(rsEquipos.getString("equ_pas"));
				mapeoEquiposConectados.setCuenta(rsEquipos.getString("equ_cue"));
				mapeoEquiposConectados.setIdRack(rsEquipos.getInt("rac_id"));
				mapeoEquiposConectados.setDescRack(rsEquipos.getString("rac_ide"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeoEquiposConectados;
	}
	
	public Integer comprobarIPDuplicada(MapeoEquiposConectados r_mapeo)
	{
		ResultSet rsEquipos = null;
		Integer cuantos = 0;
		StringBuffer cadenaWhere =null;
		MapeoEquiposConectados mapeoEquiposConectados =null; 
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_equipos.idCodigo equ_id, ");
		cadenaSQL.append(" inf_equipos.boca equ_boc, ");
		cadenaSQL.append(" inf_equipos.roseta equ_ros, ");
		cadenaSQL.append(" inf_equipos.ip equ_ip, ");
		cadenaSQL.append(" inf_equipos.direccionPublica equ_dir, ");
		cadenaSQL.append(" inf_equipos.identificador equ_ide, ");
		cadenaSQL.append(" inf_equipos.descripcion equ_des, ");
		cadenaSQL.append(" inf_equipos.observaciones equ_obs, ");
		cadenaSQL.append(" inf_equipos.usuario equ_usu, ");
		cadenaSQL.append(" inf_equipos.cuenta equ_cue, ");
		cadenaSQL.append(" inf_equipos.pass equ_pas, ");
		cadenaSQL.append(" inf_racks.idCodigo rac_id, ");
		cadenaSQL.append(" inf_racks.identificador rac_ide ");
     	cadenaSQL.append(" FROM inf_equipos ");
     	cadenaSQL.append(" inner join inf_racks on inf_racks.idCodigo = inf_equipos.idRack ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getIdentificador()!=null && r_mapeo.getIdentificador().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.identificador = '" + r_mapeo.getIdentificador() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_equipos.observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getBoca()!=null && r_mapeo.getBoca().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" boca = '" + r_mapeo.getBoca() + "' ");
				}
				if (r_mapeo.getRoseta()!=null && r_mapeo.getRoseta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" roseta = '" + r_mapeo.getRoseta() + "' ");
				}

				if (r_mapeo.getIp()!=null && r_mapeo.getIp().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ip = '" + r_mapeo.getIp() + "' ");
				}
				if (r_mapeo.getUser()!=null && r_mapeo.getUser().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" usuario = '" + r_mapeo.getUser() + "' ");
				}
				if (r_mapeo.getPass()!=null && r_mapeo.getPass().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pass = '" + r_mapeo.getPass() + "' ");
				}
				if (r_mapeo.getCuenta()!=null && r_mapeo.getCuenta().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cuenta = '" + r_mapeo.getCuenta() + "' ");
				}
				if (r_mapeo.getIdRack()!=null && r_mapeo.getIdRack()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.idRack = " + r_mapeo.getIdRack());
				}
				if (r_mapeo.getDescRack()!=null && r_mapeo.getDescRack().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.identificador = '" + r_mapeo.getDescRack() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by inf_racks.identificador asc, inf_equipos.boca  asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsEquipos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquipos.next())
			{
				cuantos=cuantos+1;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cuantos;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsEquipos = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(inf_equipos.idCodigo) equ_sig ");
     	cadenaSQL.append(" FROM inf_equipos ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsEquipos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquipos.next())
			{
				return rsEquipos.getInt("equ_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public Integer obtenerId(Integer r_idRack, String r_boca)
	{
		ResultSet rsEquipos = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT inf_equipos.idCodigo equ_id ");
     	cadenaSQL.append(" FROM inf_equipos ");
     	cadenaSQL.append(" where inf_equipos.idRack = " + r_idRack);
     	cadenaSQL.append(" and  inf_equipos.boca =  '" + r_boca + "' ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsEquipos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquipos.next())
			{
				return rsEquipos.getInt("equ_id");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoEquiposConectados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO inf_equipos ( ");
    		cadenaSQL.append(" inf_equipos.idCodigo, ");
    		cadenaSQL.append(" inf_equipos.boca, ");
    		cadenaSQL.append(" inf_equipos.roseta, ");
    		cadenaSQL.append(" inf_equipos.ip, ");
    		cadenaSQL.append(" inf_equipos.identificador, ");
    		cadenaSQL.append(" inf_equipos.descripcion, ");
    		cadenaSQL.append(" inf_equipos.observaciones, ");
    		cadenaSQL.append(" inf_equipos.usuario, ");
    		cadenaSQL.append(" inf_equipos.pass, ");
    		cadenaSQL.append(" inf_equipos.cuenta, ");
    		cadenaSQL.append(" inf_equipos.idRack, ");
    		cadenaSQL.append(" inf_equipos.direccionPublica) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    if (r_mapeo.getBoca()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getBoca());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getRoseta()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getRoseta());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getIp()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getIp());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getIdentificador()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getIdentificador());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getUser()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getUser());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getPass()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getPass());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getCuenta()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getCuenta());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getIdRack()!=null)
		    {		    	
		    	preparedStatement.setInt(11, r_mapeo.getIdRack());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getDireccionPublica()!=null)
		    {		    	
		    	preparedStatement.setString(12, r_mapeo.getDireccionPublica());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoEquiposConectados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE inf_equipos set ");
			cadenaSQL.append(" inf_equipos.boca=?, ");
			cadenaSQL.append(" inf_equipos.roseta=?, ");
			cadenaSQL.append(" inf_equipos.ip=?, ");
			cadenaSQL.append(" inf_equipos.identificador=?, ");
			cadenaSQL.append(" inf_equipos.descripcion=?, ");
		    cadenaSQL.append(" inf_equipos.observaciones=?, ");
		    cadenaSQL.append(" inf_equipos.usuario=?, ");
		    cadenaSQL.append(" inf_equipos.pass=?, ");
		    cadenaSQL.append(" inf_equipos.cuenta=?, ");
		    cadenaSQL.append(" inf_equipos.idRack= ?, ");
		    cadenaSQL.append(" inf_equipos.direccionPublica=? ");
		    cadenaSQL.append(" WHERE inf_equipos.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setString(1, r_mapeo.getBoca());
		    preparedStatement.setString(2, r_mapeo.getRoseta());
		    preparedStatement.setString(3, r_mapeo.getIp());
		    preparedStatement.setString(4, r_mapeo.getIdentificador());
		    preparedStatement.setString(5, r_mapeo.getDescripcion());
		    preparedStatement.setString(6, r_mapeo.getObservaciones());
		    preparedStatement.setString(7, r_mapeo.getUser());
		    preparedStatement.setString(8, r_mapeo.getPass());
		    preparedStatement.setString(9, r_mapeo.getCuenta());
		    preparedStatement.setInt(10, r_mapeo.getIdRack());
		    preparedStatement.setString(11, r_mapeo.getDireccionPublica());
		    preparedStatement.setInt(12, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoEquiposConectados r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM inf_equipos ");            
			cadenaSQL.append(" WHERE inf_equipos.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}