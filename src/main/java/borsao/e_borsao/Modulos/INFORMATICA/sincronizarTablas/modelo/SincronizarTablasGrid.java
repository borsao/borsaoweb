package borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.view.SincronizarTablasView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class SincronizarTablasGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	private GridViewRefresh app = null;
	
    public SincronizarTablasGrid(SincronizarTablasView r_app, ArrayList<MapeoSincronizarTablas> r_vector) {
    	this.app=r_app;
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("SincronizarTablas");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoSincronizarTablas.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("bbddOrigen", "tablaOrigen", "camposOrigen", "bbddDestino", "tablaDestino", "procesar", "sql", "camposGroupOrigen");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("bbddOrigen", "120px");
    	this.widthFiltros.put("bbddDestino", "120px");
    	this.widthFiltros.put("procesar", "70px");
    	this.getColumn("bbddOrigen").setHeaderCaption("Origen");
    	this.getColumn("tablaOrigen").setHeaderCaption("Tabla Origen");
    	this.getColumn("camposOrigen").setHeaderCaption("Campos Origen");
    	this.getColumn("bbddDestino").setHeaderCaption("Destino");
    	this.getColumn("tablaDestino").setHeaderCaption("Tabla Destino");
    	this.getColumn("procesar").setHeaderCaption("Procesar");
    	this.getColumn("sql").setHeaderCaption("SQL s/Origen");
    	this.getColumn("camposGroupOrigen").setHeaderCaption("Group by s/Origen");
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("tablaOrigen");
		this.camposNoFiltrar.add("tablaDestino");
		this.camposNoFiltrar.add("camposOrigen");
		this.camposNoFiltrar.add("camposGroupOrigen");
		this.camposNoFiltrar.add("sql");
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
    	if (this.footer==null) this.footer=this.appendFooterRow();
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");

	}
}
