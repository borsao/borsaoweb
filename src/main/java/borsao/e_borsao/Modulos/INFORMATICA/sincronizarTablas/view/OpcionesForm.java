package borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.SincronizarTablasGrid;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.server.consultaSincronizaTablasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoSincronizarTablas mapeoSincronizarTablas = null;
	 
	 private consultaSincronizaTablasServer cus = null;	 
	 private SincronizarTablasView uw = null;
	 private BeanFieldGroup<MapeoSincronizarTablas> fieldGroup;
	 
	 public OpcionesForm(SincronizarTablasView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoSincronizarTablas>(MapeoSincronizarTablas.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        this.cargarCombo();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoSincronizarTablas = (MapeoSincronizarTablas) uw.grid.getSelectedRow();
                eliminarSincronizarTablas(mapeoSincronizarTablas);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoSincronizarTablas = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearSincronizarTablas(mapeoSincronizarTablas);
	 			}
	 			else
	 			{
	 				MapeoSincronizarTablas mapeoSincronizarTablas_orig = (MapeoSincronizarTablas) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoSincronizarTablas= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarSincronizarTablas(mapeoSincronizarTablas,mapeoSincronizarTablas_orig);
	 				hm=null;
	 			}
	 			if (((SincronizarTablasGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarSincronizarTablas(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.tablaOrigen.getValue()!=null && this.tablaOrigen.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("tablaOrigen", this.tablaOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tablaOrigen", "");
		}
		
		if (this.tablaDestino.getValue()!=null && this.tablaDestino.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("tablaDestino", this.tablaDestino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tablaDestino", "");
		}
		if (this.camposOrigen.getValue()!=null && this.camposOrigen.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("camposOrigen", this.camposOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("camposOrigen", "");
		}
		
		if (this.camposGroupOrigen.getValue()!=null && this.camposGroupOrigen.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("camposGroupOrigen", this.camposGroupOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("camposGroupOrigen", "");
		}
		
		if (this.bbddOrigen.getValue()!=null && this.bbddOrigen.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("bbddOrigen", this.bbddOrigen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bbddOrigen", "");
		}
		
		if (this.bbddDestino.getValue()!=null && this.bbddDestino.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("bbddDestino", this.bbddDestino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bbddDestino", "");
		}
		if (this.sql.getValue()!=null && this.sql.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("sql", this.sql.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("sql", "");
		}
		if (this.procesar.getValue()!=null && this.procesar.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("procesar", this.procesar.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("procesar", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarSincronizarTablas(null);
	}
	
	public void editarSincronizarTablas(MapeoSincronizarTablas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoSincronizarTablas();
			fieldGroup.setItemDataSource(new BeanItem<MapeoSincronizarTablas>(r_mapeo));
			tablaOrigen.focus();
			btnGuardar.setEnabled(true);
		}
		else
		{
			setCreacion(false);
			btnGuardar.setEnabled(true);
			btnEliminar.setEnabled(true);
			fieldGroup.setItemDataSource(new BeanItem<MapeoSincronizarTablas>(r_mapeo));
		}
	}
	
	public void eliminarSincronizarTablas(MapeoSincronizarTablas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaSincronizaTablasServer(CurrentUser.get());
		((SincronizarTablasGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoSincronizarTablas>) ((SincronizarTablasGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarSincronizarTablas(MapeoSincronizarTablas r_mapeo, MapeoSincronizarTablas r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaSincronizaTablasServer(CurrentUser.get());
			cus.guardarCambios(r_mapeo);		
			((SincronizarTablasGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearSincronizarTablas(MapeoSincronizarTablas r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaSincronizaTablasServer(CurrentUser.get());
		if (todoEnOrden())
		{
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoSincronizarTablas>(r_mapeo));
				if (((SincronizarTablasGrid) uw.grid)!=null)
				{
					((SincronizarTablasGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoSincronizarTablas> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoSincronizarTablas= item.getBean();
            if (this.mapeoSincronizarTablas.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.tablaOrigen.getValue()==null || this.tablaOrigen.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la tabla Origen");
    		return false;
    	}
    	if (this.tablaDestino.getValue()==null || this.tablaDestino.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la tabla Destino");
    		return false;
    	}
    	if (this.bbddOrigen.getValue()==null || this.bbddOrigen.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la bbdd Origen");
    		return false;
    	}
    	if (this.bbddDestino.getValue()==null || this.bbddDestino.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la bbdd Destino");
    		return false;
    	}
    	if (this.procesar.getValue()==null || this.procesar.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el campo procesar s/n");
    		return false;
    	}
    	return true;
    }

    private void cargarCombo()
    {
    	this.procesar.addItem("S");
    	this.procesar.addItem("N");
    	
    	this.bbddDestino.addItem("Mysql");
    	this.bbddDestino.addItem("Tienda");
    	this.bbddDestino.addItem("GREENsys");
    	this.bbddDestino.addItem("Access");
    	this.bbddDestino.addItem("Objective");
    	
    	this.bbddOrigen.addItem("Mysql");
    	this.bbddOrigen.addItem("Objective");
    	this.bbddOrigen.addItem("Tienda");
    	this.bbddOrigen.addItem("GREENsys");
    	this.bbddOrigen.addItem("Access");
		this.bbddOrigen.addItem("Tabuenca");
		this.bbddOrigen.addItem("Pozuelo");
		this.bbddOrigen.addItem("Borja");
    }
}
