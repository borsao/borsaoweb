package borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoSincronizarTablas extends MapeoGlobal
{
	private String tablaOrigen;
	private String tablaDestino;
	private String camposOrigen;
	private String camposGroupOrigen;
	private String bbddOrigen;
	private String bbddDestino;
	private String sql;
	private String procesar;
	private boolean manual;
	
	
	public MapeoSincronizarTablas()
	{
		this.setTablaDestino("");
		this.setTablaOrigen("");
		this.setSql("");
		this.setManual(false);
	}

	public String getTablaOrigen() {
		return tablaOrigen;
	}

	public void setTablaOrigen(String tablaOrigen) {
		this.tablaOrigen = tablaOrigen;
	}

	public String getTablaDestino() {
		return tablaDestino;
	}

	public void setTablaDestino(String tablaDestino) {
		this.tablaDestino = tablaDestino;
	}

	public String getBbddOrigen() {
		return bbddOrigen;
	}

	public void setBbddOrigen(String bbddOrigen) {
		this.bbddOrigen = bbddOrigen;
	}

	public String getBbddDestino() {
		return bbddDestino;
	}

	public void setBbddDestino(String bbddDestino) {
		this.bbddDestino = bbddDestino;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getProcesar() {
		return procesar;
	}

	public void setProcesar(String procesar) {
		this.procesar = procesar;
	}

	public String getCamposOrigen() {
		return camposOrigen;
	}

	public void setCamposOrigen(String camposOrigen) {
		this.camposOrigen = camposOrigen;
	}

	public String getCamposGroupOrigen() {
		return camposGroupOrigen;
	}

	public void setCamposGroupOrigen(String camposGroupOrigen) {
		this.camposGroupOrigen = camposGroupOrigen;
	}

	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}
}