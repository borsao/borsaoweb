package borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoSincronizarTablas mapeo = null;
	 
	 public MapeoSincronizarTablas convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoSincronizarTablas();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setTablaOrigen(r_hash.get("tablaOrigen"));
		 this.mapeo.setTablaDestino(r_hash.get("tablaDestino"));
		 this.mapeo.setCamposOrigen(r_hash.get("camposOrigen"));
		 this.mapeo.setCamposGroupOrigen(r_hash.get("camposGroupOrigen"));
		 this.mapeo.setBbddOrigen(r_hash.get("bbddOrigen"));
		 this.mapeo.setBbddDestino(r_hash.get("bbddDestino"));
		 this.mapeo.setProcesar(r_hash.get("procesar"));
		 this.mapeo.setSql(r_hash.get("sql"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoSincronizarTablas convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoSincronizarTablas();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setTablaOrigen(r_hash.get("tablaOrigen"));
		 this.mapeo.setTablaDestino(r_hash.get("tablaDestino"));
		 this.mapeo.setCamposOrigen(r_hash.get("camposOrigen"));
		 this.mapeo.setCamposGroupOrigen(r_hash.get("camposGroupOrigen"));
		 this.mapeo.setBbddOrigen(r_hash.get("bbddOrigen"));
		 this.mapeo.setBbddDestino(r_hash.get("bbddDestino"));
		 this.mapeo.setProcesar(r_hash.get("procesar"));
		 this.mapeo.setSql(r_hash.get("sql"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoSincronizarTablas r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("bbddDestino", r_mapeo.getBbddDestino());
		 this.hash.put("bbddOrigen", r_mapeo.getBbddOrigen());
		 this.hash.put("tablaOrigen", r_mapeo.getTablaOrigen());
		 this.hash.put("tablaDestino", r_mapeo.getTablaDestino());
		 this.hash.put("camposOrigen", r_mapeo.getCamposOrigen());
		 this.hash.put("camposGroupOrigen", r_mapeo.getCamposGroupOrigen());
		 this.hash.put("procesar", r_mapeo.getProcesar());
		 this.hash.put("sql", r_mapeo.getSql());
		 return hash;		 
	 }
}