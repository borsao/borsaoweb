package borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;

public class consultaSincronizaTablasServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaSincronizaTablasServer instance;
	
	public consultaSincronizaTablasServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSincronizaTablasServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSincronizaTablasServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoSincronizarTablas> datosSincronizarTablasGlobal(MapeoSincronizarTablas r_mapeo, String r_sincro)
	{
		ResultSet rsSincronizarTablas = null;		
		ArrayList<MapeoSincronizarTablas> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_sincronizarTablas.bbddOrigen tab_bdo, ");
		cadenaSQL.append(" inf_sincronizarTablas.tablaOrigen tab_tbo, ");
		cadenaSQL.append(" inf_sincronizarTablas.camposOrigen tab_cbo, ");
		cadenaSQL.append(" inf_sincronizarTablas.bbddDestino tab_bdd, ");
		cadenaSQL.append(" inf_sincronizarTablas.tablaDestino tab_tbd, ");
		cadenaSQL.append(" inf_sincronizarTablas.procesar tab_pro, ");
		cadenaSQL.append(" inf_sincronizarTablas.sql tab_sql, ");
		cadenaSQL.append(" inf_sincronizarTablas.camposGroupOrigen tab_grp ");
     	cadenaSQL.append(" FROM inf_sincronizarTablas ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_sincro != null) cadenaWhere.append(" procesar = '" + r_sincro + "' ");

			if (r_mapeo!=null)
			{
				
				if (r_mapeo.getTablaOrigen()!=null && r_mapeo.getTablaOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tablaOrigen = '" + r_mapeo.getTablaOrigen() + "' ");
				}
				if (r_mapeo.getTablaDestino()!=null && r_mapeo.getTablaDestino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tablaDestino= '" + r_mapeo.getTablaDestino() + "' ");
				}
				if (r_mapeo.getBbddOrigen()!=null && r_mapeo.getBbddOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" bbddOrigen = '" + r_mapeo.getBbddOrigen() + "' ");
				}
				if (r_mapeo.getBbddDestino()!=null && r_mapeo.getBbddDestino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" bbddDestino = '" + r_mapeo.getBbddDestino() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			
			con= this.establecerConex();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsSincronizarTablas= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoSincronizarTablas>();
			
			while(rsSincronizarTablas.next())
			{
				MapeoSincronizarTablas mapeoSincronizarTablas = new MapeoSincronizarTablas();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSincronizarTablas.setTablaOrigen(rsSincronizarTablas.getString("tab_tbo"));
				mapeoSincronizarTablas.setTablaDestino(rsSincronizarTablas.getString("tab_tbd"));
				mapeoSincronizarTablas.setCamposOrigen(rsSincronizarTablas.getString("tab_cbo"));
				mapeoSincronizarTablas.setBbddOrigen(rsSincronizarTablas.getString("tab_bdo"));
				mapeoSincronizarTablas.setBbddDestino(rsSincronizarTablas.getString("tab_bdd"));
				mapeoSincronizarTablas.setSql(rsSincronizarTablas.getString("tab_sql"));
				mapeoSincronizarTablas.setCamposGroupOrigen(rsSincronizarTablas.getString("tab_grp"));
				mapeoSincronizarTablas.setProcesar(rsSincronizarTablas.getString("tab_pro"));
				vector.add(mapeoSincronizarTablas);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	public Connection establecerConex() throws SQLException
	{
		Connection con = null;
		String dbUrl="jdbc:mysql://192.168.6.6:3306/laboratorio?characterEncoding=Cp437&autoReconnect=true";
		String user="root";
		String pass="compaq123";
		
		try
		{
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection(dbUrl, user, pass);				 
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		return con;
	}

	public String guardarNuevo(MapeoSincronizarTablas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO inf_sincronizarTablas ( ");
			cadenaSQL.append(" inf_sincronizarTablas.bbddOrigen, ");
    		cadenaSQL.append(" inf_sincronizarTablas.tablaOrigen, ");
    		cadenaSQL.append(" inf_sincronizarTablas.camposOrigen, ");
			cadenaSQL.append(" inf_sincronizarTablas.bbddDestino, ");
			cadenaSQL.append(" inf_sincronizarTablas.tablaDestino, ");
			cadenaSQL.append(" inf_sincronizarTablas.sql, ");
			cadenaSQL.append(" inf_sincronizarTablas.procesar, ");
    		cadenaSQL.append(" inf_sincronizarTablas.camposGroupOrigen ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getBbddOrigen()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getBbddOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getTablaOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTablaOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCamposOrigen()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCamposOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getBbddDestino()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getBbddDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getTablaDestino()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTablaDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getSql()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getSql());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getProcesar()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getProcesar());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getCamposGroupOrigen()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getCamposGroupOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoSincronizarTablas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update inf_sincronizarTablas set ");
			cadenaSQL.append(" inf_sincronizarTablas.tablaDestino=?, ");
			cadenaSQL.append(" inf_sincronizarTablas.camposOrigen=?, ");
			cadenaSQL.append(" inf_sincronizarTablas.sql=?, ");
			cadenaSQL.append(" inf_sincronizarTablas.camposGroupOrigen=?, ");
    		cadenaSQL.append(" inf_sincronizarTablas.procesar =? ");
    		cadenaSQL.append(" where inf_sincronizarTablas.bbddOrigen=? ");
    		cadenaSQL.append(" and inf_sincronizarTablas.tablaOrigen=? ");
    		cadenaSQL.append(" and inf_sincronizarTablas.bbddDestino=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getTablaDestino()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getTablaDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCamposOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCamposOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSql()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSql());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCamposGroupOrigen()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCamposGroupOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getProcesar()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getProcesar());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }

		    if (r_mapeo.getBbddOrigen()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getBbddOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getTablaOrigen()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getTablaOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getBbddDestino()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getBbddDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }

		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoSincronizarTablas r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM inf_sincronizarTablas ");            
			cadenaSQL.append(" WHERE inf_sincronizarTablas.tablaOrigen = ?");
			cadenaSQL.append(" and inf_sincronizarTablas.bbddOrigen = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getTablaOrigen());
			preparedStatement.setString(2, r_mapeo.getBbddOrigen());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}