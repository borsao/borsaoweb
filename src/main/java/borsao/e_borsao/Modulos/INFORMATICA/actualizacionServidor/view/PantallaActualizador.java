package borsao.e_borsao.Modulos.INFORMATICA.actualizacionServidor.view;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;

public class PantallaActualizador extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbPeticion = null;
	
	private HorizontalLayout fila5 = null;
	private HorizontalLayout fila6 = null;
	
	private actualizadorView origen = null;
	private HorizontalLayout fila2 = null;
	private HorizontalLayout fila3 = null;
	private HorizontalLayout fila4 = null;
	
	private VerticalLayout controles = null;
	
	public PantallaActualizador(actualizadorView r_view)
	{
		this.origen=r_view;

		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.cmbPeticion =new ComboBox("Petición");
				this.cmbPeticion.setWidth("250px");
				this.cmbPeticion.setNullSelectionAllowed(false);
				fila1.addComponent(this.cmbPeticion);
				
		controles.addComponent(fila1);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición Actualizacion Servidor");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cargarCombo();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() 
		{
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				int processOutput = 0;
				String paroServidor = "/etc/init.d/servidorJetty stop";
				String revisoPermisos = "chmod -R 777 /home/datos/informatica/8080";
				String copiaPom = "cp /home/datos/informatica/8080/pom.xml /usr/share/jetty9/.";
				String copiaWeb = "cp /home/datos/informatica/8080/web.xml /usr/share/jetty9/webapps/root/WEB-INF/.";
				String copiaWar = "cp /home/datos/informatica/8080/borsaoWeb-1.0.war /usr/share/jetty9/webapps/.";
				String copiaClases = "cp -R /home/datos/informatica/8080/borsaoWeb-1.0/* /usr/share/jetty9/webapps/root/.";
				String lanzoServidor = "/etc/init.d/servidorJetty start";
				
				String reiniciarServidor = "sudo /etc/init.d/servidorJetty force-reload";
				String actualizarServidor = "sudo actualizacionBorsaoWeb";

				try
				{
					if (cmbPeticion.getValue().toString().toUpperCase().contentEquals("REINICIAR"))
					{
						RutinasFicheros.ejecutarComandoSinTraza(reiniciarServidor);	
					}
					else
					{
						RutinasFicheros.ejecutarComandoSinTraza(actualizarServidor);	
//						processOutput = RutinasFicheros.ejecutarComando(revisoPermisos);
//						if (processOutput==0)
//						{
//							processOutput = RutinasFicheros.ejecutarComando(copiaPom);
//							if (processOutput==0)
//							{
//								processOutput = RutinasFicheros.ejecutarComando(copiaWeb);
//								if (processOutput==0)
//								{
//									processOutput = RutinasFicheros.ejecutarComando(copiaWar);
//									if (processOutput==0)
//									{
//										processOutput = RutinasFicheros.ejecutarComando(copiaClases);
//										if (processOutput==0)
//										{
////											RutinasFicheros.ejecutarComandoSinTraza(reiniciarServidor);
//										}
//										else
//										{
//											Notificaciones.getInstance().mensajeSeguimiento("Error al copiar clases");
//										}
//									}
//									else
//									{
//										Notificaciones.getInstance().mensajeSeguimiento("Error al copiar War");
//									}
//								}
//								else
//								{
//									Notificaciones.getInstance().mensajeSeguimiento("Error al copiarWeb");
//								}
//							}
//							else
//							{
//								Notificaciones.getInstance().mensajeSeguimiento("Error al copiar pom");
//							}
//						}
//						else
//						{
//							Notificaciones.getInstance().mensajeSeguimiento("Error al revisar permisos");
//						}
					}
				}
				catch (Exception ex)
				{
					
				}
			}
		}
		);
		
		
		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			@Override
			public void buttonClick(ClickEvent event) 
			{
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	
	private void cargarCombo()
	{
		this.cmbPeticion.addItem("Reiniciar");
		this.cmbPeticion.addItem("Reinicicar actualizando programas");
	}

}