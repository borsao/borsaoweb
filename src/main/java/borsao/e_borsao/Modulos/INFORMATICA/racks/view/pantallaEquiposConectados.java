package borsao.e_borsao.Modulos.INFORMATICA.racks.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.MapeoEquiposConectados;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.server.consultaEquiposConectadosServer;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.MapeoSwitch;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.VisionBocasRackGrid;
import borsao.e_borsao.Modulos.INFORMATICA.racks.server.consultaRacksServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaEquiposConectados extends Window
{
	
	private Button btnBotonCVentana = null;
	private Button btnBotonActualizar = null;
	private Grid gridPadre= null;
	private VerticalLayout principal=null;
	private boolean hayGridPadre = false;
	private boolean modificando = false;
	private VerticalLayout frameCentral = null;	

	private String switchConsultado = null;
	private TextField txtBoca = null;
	private TextField txtIP = null;
	private TextField txtAnyDesk = null;
	private TextField txtRoseta = null;
	private ComboBox txtIdentificador = null;
	private TextArea txtDescripcion = null;
	private TextArea  txtObservaciones = null;
	private TextField txtUsuario = null;
	private TextField txtPasswd = null;
	private Button btnPing = null;
	private Button btnAnyDesk = null;
	private Integer tamano = null;
	private consultaRacksServer crs  = null;
	
	/*
	 * Entradas datos equipo
	 */
	public TextField cantidad= null;
	
	public pantallaEquiposConectados(String r_titulo, String r_switch)
	{
		
		this.setCaption(r_titulo);
		this.switchConsultado=r_switch;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.crs = new consultaRacksServer(CurrentUser.get());
    	this.tamano=this.crs.tamanoSwitch(this.switchConsultado.trim());

    	if (this.tamano==48)
    	{
    		this.setWidth("1875px");
    	}
    	else
    	{
    		this.setWidth("1175px");
    	}
    	this.setHeight("750px");
		
		this.cargarPantalla();
		this.llenarRegistros();
		this.cargarListeners();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
				btnBotonActualizar = new Button("Actualizar");
				btnBotonActualizar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			
			HorizontalLayout framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		btnAnyDesk.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				conectar();
			}
		});

		btnPing.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				ping();
			}
		});

		btnBotonActualizar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				actualizar();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoSwitch> r_vector=null;

    	r_vector=this.crs.datosSwitch(this.tamano, this.switchConsultado.trim());
    	
    	this.presentarGrid(tamano, r_vector);

	}

    private void presentarGrid(Integer r_tamano, ArrayList<MapeoSwitch> r_vector)
    {
    	
    	gridPadre = new VisionBocasRackGrid(this, r_vector,this.switchConsultado.trim(), r_tamano);
    	
    	if (((VisionBocasRackGrid) gridPadre).vector==null) 
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		setHayGridPadre(true);
    	}
    	
    	if (isHayGridPadre())
    	{
    		this.gridPadre.setHeight("135px");
    		this.gridPadre.setWidth("100%");
    			
    		
    		this.frameCentral.addComponent(gridPadre);
    		this.frameCentral.setExpandRatio(gridPadre,1 );
    		
    		/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			txtBoca = new TextField("Boca");
    			txtBoca.setWidth("100px");
    			txtRoseta = new TextField("Roseta");
    			txtRoseta.setWidth("100px");
    			txtIP = new TextField("IP");
    			btnPing = new Button("Ping");
    			txtAnyDesk = new TextField("ANYDesk");
    			txtAnyDesk.setIcon(FontAwesome.LAPTOP);
    			
    			btnAnyDesk = new Button(FontAwesome.LAPTOP);
    			btnAnyDesk.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
    			
    			txtIdentificador = new ComboBox("Identificador");
    			txtIdentificador.setWidth("300px");
    			
    			linea1.addComponent(this.txtBoca);
    			linea1.addComponent(this.txtRoseta);
    			linea1.addComponent(this.txtIP);
//    			linea1.addComponent(this.btnPing);
    			linea1.addComponent(this.txtAnyDesk);
    			linea1.addComponent(this.btnAnyDesk);
    			linea1.setComponentAlignment(this.btnAnyDesk, Alignment.BOTTOM_LEFT);
    			linea1.addComponent(this.txtIdentificador);
    			
			HorizontalLayout linea2 = new HorizontalLayout();
				linea2.setSpacing(true);
    			txtDescripcion = new TextArea("Descripcion");
    			txtDescripcion.setWidth("250px");
    			txtObservaciones = new TextArea("Observaciones");
    			txtObservaciones.setWidth("250px");
    			
    			linea2.addComponent(this.txtDescripcion);
    			linea2.addComponent(this.txtObservaciones);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    		linea3.setSpacing(true);
    			txtUsuario = new TextField("Usuario");
    			txtPasswd= new TextField("Passwd");
    			
    			linea3.addComponent(this.txtUsuario);
    			linea3.addComponent(this.txtPasswd);
    		
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		this.frameCentral.addComponent(this.btnBotonActualizar);
    		this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    		this.frameCentral.setComponentAlignment(this.btnBotonActualizar, Alignment.BOTTOM_LEFT);    			
    		this.frameCentral.setMargin(true);
    		
    		this.cargarCombo();
    		
    	}
    }

    private void activarEntradasDatos(boolean r_activar)
    {
    	txtBoca.setEnabled(r_activar);
    	txtIP.setEnabled(r_activar);
    	txtRoseta.setEnabled(r_activar);
    	txtIdentificador.setEnabled(r_activar);
    	txtDescripcion.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    	txtUsuario.setEnabled(r_activar);
    	txtPasswd.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	public void rellenarEntradasDatos(Integer r_reg, String r_switch, String r_bocaPulsada)
	{
		Integer boca = null;

		boca = new Integer(r_bocaPulsada.substring(4))*2;
		if (r_reg==1) boca = boca -1;
		
		txtBoca.setValue(boca.toString());
		txtBoca.setEnabled(false);
		/*
		 * con los datos del switch y la boca pulsada 
		 * buscaremos ne la parte servidora los datos asociados
		 * 
		 */
		consultaEquiposConectadosServer cecs = consultaEquiposConectadosServer.getInstance(CurrentUser.get());
		MapeoEquiposConectados mapeo = new MapeoEquiposConectados();
		
		mapeo.setBoca(boca.toString());
		mapeo.setDescRack(r_switch);
		
		mapeo = cecs.datosEquiposConectados(mapeo);
		
		
		if (mapeo!=null)
		{
			modificando=true;
			txtRoseta.setValue(mapeo.getRoseta());
			txtIP.setValue(mapeo.getIp());
			txtIdentificador.setValue(mapeo.getIdentificador());
			txtDescripcion.setValue(mapeo.getDescripcion());
			txtObservaciones.setValue(mapeo.getObservaciones());
			txtUsuario.setValue(mapeo.getUser());
			txtPasswd.setValue(mapeo.getPass());
			txtAnyDesk.setValue(mapeo.getDireccionPublica());
	//		txtCuenta.setValue(mapeo.getCuenta());
		}
		else
		{
			modificando=false;
			txtRoseta.setValue("");
			txtIP.setValue("");
			txtIdentificador.setValue("");
			txtDescripcion.setValue("");
			txtObservaciones.setValue("");
			txtUsuario.setValue("");
			txtPasswd.setValue("");
			txtAnyDesk.setValue("");
//			txtCuenta.setValue(mapeo.getCuenta());
			
			Notificaciones.getInstance().mensajeInformativo("Boca " + boca.toString() + " del switch " + r_switch + ". No hay datos asociados");
		}
		
	}
	
	private void actualizar()
	{
		String rdo = null;
		MapeoEquiposConectados mapeo = null;
		consultaEquiposConectadosServer cecs = consultaEquiposConectadosServer.getInstance(CurrentUser.get());
		consultaRacksServer crs = consultaRacksServer.getInstance(CurrentUser.get());
		
		if (todoEnOrden())
		{
			mapeo = new MapeoEquiposConectados();
			mapeo.setBoca(txtBoca.getValue());
			mapeo.setRoseta(txtRoseta.getValue());
	    	mapeo.setIp(txtIP.getValue());
	    	mapeo.setIdentificador(txtIdentificador.getValue().toString());
	    	mapeo.setDescripcion(txtDescripcion.getValue());
	    	mapeo.setObservaciones(txtObservaciones.getValue());
	    	mapeo.setUser(txtUsuario.getValue());
	    	mapeo.setPass(txtPasswd.getValue());
	    	mapeo.setAny(this.txtAnyDesk.getValue());
//	    	mapeo.setCuenta(txtCuenta.getValue());
	    	mapeo.setIdRack(crs.obtenerId(this.switchConsultado));
	    	
	    	if (modificando)
	    	{
	    		mapeo.setIdCodigo(cecs.obtenerId(mapeo.getIdRack(), mapeo.getBoca()));
	    		cecs.guardarCambios(mapeo);
	    	}
	    	else
	    	{
	    		rdo = cecs.guardarNuevo(mapeo);
	    	}
		}
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtBoca.getValue()==null || this.txtBoca.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la boca de conexión");
    		return false;
    	}
    	if ((this.txtRoseta.getValue()==null || this.txtRoseta.getValue().toString().length()==0) && (this.txtIP.getValue()==null || this.txtIP.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la roseta y/o la ip del equipo que está conectado ");
    		return false;
    	}
//    	if (this.txtIP.getValue()==null || this.txtIP.getValue().toString().length()==0)
//    	{
//    		Notificaciones.getInstance().mensajeError("Debes Indicar la IP / Extensión del equipo conectado");
//    		return false;
//    	}    	

    	return true;
    }
    
    private void ping()
    {
    	ProcessBuilder pb = new ProcessBuilder("cmd.exe", "ping -t ", this.txtIP.getValue());

    	try 
        {
        	pb.directory(new File("src"));

        	Process process = pb.start();
	        IOThreadHandler outputHandler = new IOThreadHandler(process.getInputStream());
	        outputHandler.start();
	        process.waitFor();
	        
	        Notificaciones.getInstance().mensajeSeguimiento(outputHandler.getOutput().toString());
	        
		} 
        catch (Exception e) 
        {
			e.printStackTrace();
		}
   	
    }
    
    private void conectar()
    {
    	if (this.txtAnyDesk.getValue()==null || this.txtAnyDesk.getValue().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("No has seleccionado dispositivo para conectarte");
    	}
    	else
    	{
    		ProcessBuilder pb = new ProcessBuilder("c:\\tmp\\AnyDesk.exe", this.txtAnyDesk.getValue());

	    	try 
	    	{
				Process process = pb.start();
			}
	    	catch (IOException e) 
	    	{
				e.printStackTrace();
				Notificaciones.getInstance().mensajeError("Problemas en la ejecución del software");
			}
    	}
    }
    
    private static class IOThreadHandler extends Thread 
	{
		private InputStream inputStream;
		private StringBuilder output = new StringBuilder();
	
		IOThreadHandler(InputStream inputStream) 
		{
			this.inputStream = inputStream;
		}
	
		public void run() {
			Scanner br = null;
			try 
			{
				br = new Scanner(new InputStreamReader(inputStream));
				String line = null;
				while (br.hasNextLine()) 
				{
					line = br.nextLine();
					output.append(line + System.getProperty("line.separator"));
				}
			} 	
			finally 
			{
				br.close();
			}
		}
	
		public StringBuilder getOutput() 
		{
			return output;
		}
	}
    
    private void cargarCombo()
    {
		this.txtIdentificador.addItem("Router");
		this.txtIdentificador.addItem("Datafono");
		this.txtIdentificador.addItem("Dect");
		this.txtIdentificador.addItem("Switch");
		this.txtIdentificador.addItem("Máquina");
		this.txtIdentificador.addItem("Servidor");
		this.txtIdentificador.addItem("Cabina");
		this.txtIdentificador.addItem("Teléfono");
		this.txtIdentificador.addItem("Ordenador");
		this.txtIdentificador.addItem("Impresora");
		this.txtIdentificador.addItem("Wifi");
		this.txtIdentificador.addItem("Telefonía");
		this.txtIdentificador.addItem("Otros");
		this.txtIdentificador.addItem("Firewall");
		this.txtIdentificador.addItem("");
		
		this.txtIdentificador.setNullSelectionAllowed(false);
		this.txtIdentificador.setNewItemsAllowed(false);
    }
}