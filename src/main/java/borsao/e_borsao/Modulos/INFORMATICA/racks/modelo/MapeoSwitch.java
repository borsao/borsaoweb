package borsao.e_borsao.Modulos.INFORMATICA.racks.modelo;

import com.vaadin.server.ThemeResource;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoSwitch extends MapeoGlobal
{
	private String identificacion;
	private ThemeResource boca1;
	private ThemeResource boca2;
	private ThemeResource boca3;
	private ThemeResource boca4;
	private ThemeResource boca5;
	private ThemeResource boca6;
	private ThemeResource boca7;
	private ThemeResource boca8;
	private ThemeResource boca9;
	private ThemeResource boca10;
	private ThemeResource boca11;
	private ThemeResource boca12;
	private ThemeResource boca13;
	private ThemeResource boca14;
	private ThemeResource boca15;
	private ThemeResource boca16;
	private ThemeResource boca17;
	private ThemeResource boca18;
	private ThemeResource boca19;
	private ThemeResource boca20;
	private ThemeResource boca21;
	private ThemeResource boca22;
	private ThemeResource boca23;
	private ThemeResource boca24;
	
	private String bc1;
	private String bc2;
	private String bc3;
	private String bc4;
	private String bc5;
	private String bc6;
	private String bc7;
	private String bc8;
	private String bc9;
	private String bc10;
	private String bc11;
	private String bc12;
	private String bc13;
	private String bc14;
	private String bc15;
	private String bc16;
	private String bc17;
	private String bc18;
	private String bc19;
	private String bc20;
	private String bc21;
	private String bc22;
	private String bc23;
	private String bc24;
//	private ThemeResource boca25;
//	private ThemeResource boca26;
//	private ThemeResource boca27;
//	private ThemeResource boca28;
//	private ThemeResource boca29;
//	private ThemeResource boca30;
//	private ThemeResource boca31;
//	private ThemeResource boca32;
//	private ThemeResource boca33;
//	private ThemeResource boca34;
//	private ThemeResource boca35;
//	private ThemeResource boca36;
//	private ThemeResource boca37;
//	private ThemeResource boca38;
//	private ThemeResource boca39;
//	private ThemeResource boca40;
//	private ThemeResource boca41;
//	private ThemeResource boca42;
//	private ThemeResource boca43;
//	private ThemeResource boca44;
//	private ThemeResource boca45;
//	private ThemeResource boca46;
//	private ThemeResource boca47;
//	private ThemeResource boca48;
	
	public MapeoSwitch()
	{
		this.setIdentificacion("");
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public ThemeResource getBoca1() 
	{
		return boca1;
	}

	public void setBoca1(String r_boca1) 
	{
		if (r_boca1 == null) r_boca1 ="img/down.png";  
		boca1 = new ThemeResource(r_boca1);	
	}

	public ThemeResource getBoca2() 
	{
		return boca2;
	}

	public void setBoca2(String r_boca2) 
	{		  
		if (r_boca2 == null) r_boca2 ="img/down.png";  
		boca2 = new ThemeResource(r_boca2);	
	}

	public ThemeResource getBoca3() {
		return boca3;
	}

	public void setBoca3(String r_boca3) 
	{		  
		if (r_boca3 == null) r_boca3 ="img/down.png";  
		boca3 = new ThemeResource(r_boca3);	
	}

	public ThemeResource getBoca4() {
		return boca4;
	}

	public void setBoca4(String r_boca4) 
	{		  
		if (r_boca4 == null) r_boca4 ="img/down.png";  
		boca4 = new ThemeResource(r_boca4);	
	}

	public ThemeResource getBoca5() {
		return boca5;
	}

	public void setBoca5(String r_boca5) 
	{		  
		if (r_boca5 == null) r_boca5 ="img/down.png";  
		boca5 = new ThemeResource(r_boca5);	
	}

	public ThemeResource getBoca6() {
		return boca6;
	}

	public void setBoca6(String r_boca6) 
	{		  
		if (r_boca6 == null) r_boca6 ="img/down.png";  
		boca6 = new ThemeResource(r_boca6);	
	}

	public ThemeResource getBoca7() {
		return boca7;
	}

	public void setBoca7(String r_boca7) 
	{		  
		if (r_boca7 == null) r_boca7 ="img/down.png";  
		boca7 = new ThemeResource(r_boca7);	
	}

	public ThemeResource getBoca8() {
		return boca8;
	}

	public void setBoca8(String r_boca8) 
	{		  
		if (r_boca8 == null) r_boca8 ="img/down.png";  
		boca8 = new ThemeResource(r_boca8);	
	}

	public ThemeResource getBoca9() {
		return boca9;
	}

	public void setBoca9(String r_boca9) 
	{		  
		if (r_boca9 == null) r_boca9 ="img/down.png";  
		boca9 = new ThemeResource(r_boca9);	
	}

	public ThemeResource getBoca10() {
		return boca10;
	}

	public void setBoca10(String r_boca10) {
		if (r_boca10 == null) r_boca10 ="img/down.png";  
		boca10 = new ThemeResource(r_boca10);	
	}
	
	public ThemeResource getBoca11() 
	{
		return boca11;
	}
	
	public void setBoca11(String r_boca11) 
	{
		if (r_boca11 == null) r_boca11 ="img/down.png";  
		boca11 = new ThemeResource(r_boca11);	
	}

	public ThemeResource getBoca12() 
	{
		return boca12;
	}

	public void setBoca12(String r_boca12) 
	{		  
		if (r_boca12 == null) r_boca12 ="img/down.png";  
		boca12 = new ThemeResource(r_boca12);	
	}

	public ThemeResource getBoca13() {
		return boca13;
	}

	public void setBoca13(String r_boca13) 
	{		  
		if (r_boca13 == null) r_boca13 ="img/down.png";  
		boca13 = new ThemeResource(r_boca13);	
	}

	public ThemeResource getBoca14() {
		return boca14;
	}

	public void setBoca14(String r_boca14) 
	{		  
		if (r_boca14 == null) r_boca14 ="img/down.png";  
		boca14 = new ThemeResource(r_boca14);	
	}

	public ThemeResource getBoca15() {
		return boca15;
	}

	public void setBoca15(String r_boca15) 
	{		  
		if (r_boca15 == null) r_boca15 ="img/down.png";  
		boca15 = new ThemeResource(r_boca15);	
	}

	public ThemeResource getBoca16() {
		return boca16;
	}

	public void setBoca16(String r_boca16) 
	{		  
		if (r_boca16 == null) r_boca16 ="img/down.png";  
		boca16 = new ThemeResource(r_boca16);	
	}

	public ThemeResource getBoca17() {
		return boca17;
	}

	public void setBoca17(String r_boca17) 
	{		  
		if (r_boca17 == null) r_boca17 ="img/down.png";  
		boca17 = new ThemeResource(r_boca17);	
	}

	public ThemeResource getBoca18() {
		return boca18;
	}

	public void setBoca18(String r_boca18) 
	{		  
		if (r_boca18 == null) r_boca18 ="img/down.png";  
		boca18 = new ThemeResource(r_boca18);	
	}

	public ThemeResource getBoca19() {
		return boca19;
	}

	public void setBoca19(String r_boca19) 
	{		  
		if (r_boca19 == null) r_boca19 ="img/down.png";  
		boca19 = new ThemeResource(r_boca19);	
	}

	public ThemeResource getBoca20() {
		return boca20;
	}

	public void setBoca20(String r_boca20) {
		if (r_boca20 == null) r_boca20 ="img/down.png";  
		boca20 = new ThemeResource(r_boca20);	
	}
	
	public ThemeResource getBoca21() 
	{
		return boca21;
	}
	
	public void setBoca21(String r_boca21) 
	{
		if (r_boca21 == null) r_boca21 ="img/down.png";  
		boca21 = new ThemeResource(r_boca21);	
	}
	
	public ThemeResource getBoca22() 
	{
		return boca22;
	}

	public void setBoca22(String r_boca22) 
	{		  
		if (r_boca22 == null) r_boca22 ="img/down.png";  
		boca22 = new ThemeResource(r_boca22);	
	}

	public ThemeResource getBoca23() {
		return boca23;
	}

	public void setBoca23(String r_boca23) 
	{		  
		if (r_boca23 == null) r_boca23 ="img/down.png";  
		boca23 = new ThemeResource(r_boca23);	
	}

	public ThemeResource getBoca24() {
		return boca24;
	}

	public void setBoca24(String r_boca24) 
	{		  
		if (r_boca24 == null) r_boca24 ="img/down.png";  
		boca24 = new ThemeResource(r_boca24);	
	}

	public String getBc1() {
		return bc1;
	}

	public void setBc1(String bc1) {
		this.bc1 = bc1;
	}

	public String getBc2() {
		return bc2;
	}

	public void setBc2(String bc2) {
		this.bc2 = bc2;
	}

	public String getBc3() {
		return bc3;
	}

	public void setBc3(String bc3) {
		this.bc3 = bc3;
	}

	public String getBc4() {
		return bc4;
	}

	public void setBc4(String bc4) {
		this.bc4 = bc4;
	}

	public String getBc5() {
		return bc5;
	}

	public void setBc5(String bc5) {
		this.bc5 = bc5;
	}

	public String getBc6() {
		return bc6;
	}

	public void setBc6(String bc6) {
		this.bc6 = bc6;
	}

	public String getBc7() {
		return bc7;
	}

	public void setBc7(String bc7) {
		this.bc7 = bc7;
	}

	public String getBc8() {
		return bc8;
	}

	public void setBc8(String bc8) {
		this.bc8 = bc8;
	}

	public String getBc9() {
		return bc9;
	}

	public void setBc9(String bc9) {
		this.bc9 = bc9;
	}

	public String getBc10() {
		return bc10;
	}

	public void setBc10(String bc10) {
		this.bc10 = bc10;
	}

	public String getBc11() {
		return bc11;
	}

	public void setBc11(String bc11) {
		this.bc11 = bc11;
	}

	public String getBc12() {
		return bc12;
	}

	public void setBc12(String bc12) {
		this.bc12 = bc12;
	}

	public String getBc13() {
		return bc13;
	}

	public void setBc13(String bc13) {
		this.bc13 = bc13;
	}

	public String getBc14() {
		return bc14;
	}

	public void setBc14(String bc14) {
		this.bc14 = bc14;
	}

	public String getBc15() {
		return bc15;
	}

	public void setBc15(String bc15) {
		this.bc15 = bc15;
	}

	public String getBc16() {
		return bc16;
	}

	public void setBc16(String bc16) {
		this.bc16 = bc16;
	}

	public String getBc17() {
		return bc17;
	}

	public void setBc17(String bc17) {
		this.bc17 = bc17;
	}

	public String getBc18() {
		return bc18;
	}

	public void setBc18(String bc18) {
		this.bc18 = bc18;
	}

	public String getBc19() {
		return bc19;
	}

	public void setBc19(String bc19) {
		this.bc19 = bc19;
	}

	public String getBc20() {
		return bc20;
	}

	public void setBc20(String bc20) {
		this.bc20 = bc20;
	}

	public String getBc21() {
		return bc21;
	}

	public void setBc21(String bc21) {
		this.bc21 = bc21;
	}

	public String getBc22() {
		return bc22;
	}

	public void setBc22(String bc22) {
		this.bc22 = bc22;
	}

	public String getBc23() {
		return bc23;
	}

	public void setBc23(String bc23) {
		this.bc23 = bc23;
	}

	public String getBc24() {
		return bc24;
	}

	public void setBc24(String bc24) {
		this.bc24 = bc24;
	}

//	public ThemeResource getBoca25() {
//		return boca25;
//	}
//
//	public void setBoca25(String r_boca25) 
//	{		  
//		new ThemeResource(r_boca25);	
//	}
//
//	public ThemeResource getBoca26() {
//		return boca26;
//	}
//
//	public void setBoca26(String r_boca26) 
//	{		  
//		new ThemeResource(r_boca26);	
//	}
//
//	public ThemeResource getBoca27() {
//		return boca27;
//	}
//
//	public void setBoca27(String r_boca27) 
//	{		  
//		new ThemeResource(r_boca27);	
//	}
//
//	public ThemeResource getBoca28() {
//		return boca28;
//	}
//
//	public void setBoca28(String r_boca28) 
//	{		  
//		new ThemeResource(r_boca28);	
//	}
//
//	public ThemeResource getBoca29() {
//		return boca29;
//	}
//
//	public void setBoca29(String r_boca29) 
//	{		  
//		new ThemeResource(r_boca29);	
//	}
//
//	public ThemeResource getBoca30() {
//		return boca30;
//	}
//
//	public void setBoca30(ThemeResource boca30) {
//		this.boca30 = boca30;
//	}
//	
//	public ThemeResource getBoca31() 
//	{
//		return boca31;
//	}
//	public void setBoca31(String r_boca31) 
//	{
//		if (r_boca31 == null) r_boca31 ="img/down.png";  
//		new ThemeResource(r_boca31);	
//	}
//
//	public ThemeResource getBoca32() 
//	{
//		return boca32;
//	}
//
//	public void setBoca32(String r_boca32) 
//	{		  
//		new ThemeResource(r_boca32);	
//	}
//
//	public ThemeResource getBoca33() {
//		return boca33;
//	}
//
//	public void setBoca33(String r_boca33) 
//	{		  
//		new ThemeResource(r_boca33);	
//	}
//
//	public ThemeResource getBoca34() {
//		return boca34;
//	}
//
//	public void setBoca34(String r_boca34) 
//	{		  
//		new ThemeResource(r_boca34);	
//	}
//
//	public ThemeResource getBoca35() {
//		return boca35;
//	}
//
//	public void setBoca35(String r_boca35) 
//	{		  
//		new ThemeResource(r_boca35);	
//	}
//
//	public ThemeResource getBoca36() {
//		return boca36;
//	}
//
//	public void setBoca36(String r_boca36) 
//	{		  
//		new ThemeResource(r_boca36);	
//	}
//
//	public ThemeResource getBoca37() {
//		return boca37;
//	}
//
//	public void setBoca37(String r_boca37) 
//	{		  
//		new ThemeResource(r_boca37);	
//	}
//
//	public ThemeResource getBoca38() {
//		return boca38;
//	}
//
//	public void setBoca38(String r_boca38) 
//	{		  
//		new ThemeResource(r_boca38);	
//	}
//
//	public ThemeResource getBoca39() {
//		return boca39;
//	}
//
//	public void setBoca39(String r_boca39) 
//	{		  
//		new ThemeResource(r_boca39);	
//	}
//
//	public ThemeResource getBoca40() {
//		return boca40;
//	}
//
//	public void setBoca40(ThemeResource boca40) {
//		this.boca40 = boca40;
//	}
//	
//	public ThemeResource getBoca41() 
//	{
//		return boca41;
//	}
//	
//	public void setBoca41(String r_boca41) 
//	{
//		if (r_boca41 == null) r_boca41 ="img/down.png";  
//		new ThemeResource(r_boca41);	
//	}
//
//	public ThemeResource getBoca42() 
//	{
//		return boca42;
//	}
//
//	public void setBoca42(String r_boca42) 
//	{		  
//		new ThemeResource(r_boca42);	
//	}
//
//	public ThemeResource getBoca43() {
//		return boca43;
//	}
//
//	public void setBoca43(String r_boca43) 
//	{		  
//		new ThemeResource(r_boca43);	
//	}
//
//	public ThemeResource getBoca44() {
//		return boca44;
//	}
//
//	public void setBoca44(String r_boca44) 
//	{		  
//		new ThemeResource(r_boca44);	
//	}
//
//	public ThemeResource getBoca45() {
//		return boca45;
//	}
//
//	public void setBoca45(String r_boca45) 
//	{		  
//		new ThemeResource(r_boca45);	
//	}
//
//	public ThemeResource getBoca46() {
//		return boca46;
//	}
//
//	public void setBoca46(String r_boca46) 
//	{		  
//		new ThemeResource(r_boca46);	
//	}
//
//	public ThemeResource getBoca47() {
//		return boca47;
//	}
//
//	public void setBoca47(String r_boca47) 
//	{		  
//		new ThemeResource(r_boca47);	
//	}
//
//	public ThemeResource getBoca48() {
//		return boca48;
//	}
//
//	public void setBoca48(String r_boca48) 
//	{		  
//		new ThemeResource(r_boca48);	
//	}
}