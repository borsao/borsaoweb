package borsao.e_borsao.Modulos.INFORMATICA.racks.modelo;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Image;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoRacks extends MapeoGlobal
{
	private String identificacion;
	private String descripcion;
	private String ubicacion;
	private Integer numBocas;
	private Integer idSede;
	private String descSede;
	private String estado="";
	private String bocas="";
//	private ThemeResource boca1;
//	private ThemeResource boca2;
	
	public MapeoRacks()
	{
		this.setIdentificacion("");
		this.setDescripcion("");
		this.setUbicacion("");
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Integer getIdSede() {
		return idSede;
	}

	public void setIdSede(Integer idSede) {
		this.idSede = idSede;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescSede() {
		return descSede;
	}

	public void setDescSede(String descSede) {
		this.descSede = descSede;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getBocas() {
		return bocas;
	}

	public void setBocas(String bocas) {
		this.bocas = bocas;
	}

	public Integer getNumBocas() {
		return numBocas;
	}

	public void setNumBocas(Integer numBocas) {
		this.numBocas = numBocas;
	}


//	public ThemeResource getBoca1() 
//	{
//		return new ThemeResource("img/down.png");
//	}
//
//	public ThemeResource getBoca2() 
//	{
//		return new ThemeResource("img/up.png");
//	}
//
//	public void setBoca1(Image boca1) {
//		
//	}

}