package borsao.e_borsao.Modulos.INFORMATICA.racks.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.MapeoRacks;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.RacksGrid;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.INFORMATICA.racks.server.consultaRacksServer;
import borsao.e_borsao.Modulos.INFORMATICA.sedes.modelo.MapeoSedes;
import borsao.e_borsao.Modulos.INFORMATICA.sedes.server.consultaSedesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	
{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoRacks mapeoRacks = null;
	 
	 private consultaRacksServer cus = null;	 
	 private racksView uw = null;
	 private BeanFieldGroup<MapeoRacks> fieldGroup;
	 
	 public OpcionesForm(racksView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoRacks>(MapeoRacks.class);
        fieldGroup.bindMemberFields(this);

        this.cargarCombo();
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoRacks = (MapeoRacks) uw.grid.getSelectedRow();
                eliminarRacks(mapeoRacks);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoRacks = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearRacks(mapeoRacks);
	 			}
	 			else
	 			{
	 				MapeoRacks mapeoRacks_orig = (MapeoRacks) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoRacks= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarRacks(mapeoRacks,mapeoRacks_orig);
	 				hm=null;
	 			}
	 			if (((RacksGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarRacks(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.identificacion.getValue()!=null && this.identificacion.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("identificacion", this.identificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("identificacion", "");
		}
		
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.ubicacion.getValue()!=null && this.ubicacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ubicacion", this.ubicacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ubicacion", "");
		}
		if (this.descSede.getValue()!=null && this.descSede.getValue().toString().length()>0) 
		{
			consultaSedesServer css = consultaSedesServer.getInstance(CurrentUser.get());
	    	Integer id = css.obtenerId(this.descSede.getValue().toString());
			opcionesEscogidas.put("idSede", id.toString());
		}
		else
		{
			opcionesEscogidas.put("idSede", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarRacks(null);
	}
	
	public void editarRacks(MapeoRacks r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoRacks(); 
		}
		else
		{
//			consultaSedesServer css = consultaSedesServer.getInstance(CurrentUser.get());
//	    	Integer id = css.obtenerId(this.idSede.getValue().toString());
//			opcionesEscogidas.put("idSede", id.toString());
		}
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoRacks>(r_mapeo));
		identificacion.focus();
	}
	
	public void eliminarRacks(MapeoRacks r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = consultaRacksServer.getInstance(CurrentUser.get());
		((RacksGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoRacks>) ((RacksGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarRacks(MapeoRacks r_mapeo, MapeoRacks r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaRacksServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((RacksGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearRacks(MapeoRacks r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaRacksServer(CurrentUser.get());
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			r_mapeo.setDescSede(this.descSede.getValue().toString());
			
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoRacks>(r_mapeo));
				if (((RacksGrid) uw.grid)!=null)
				{
					((RacksGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoRacks> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoRacks= item.getBean();
            if (this.mapeoRacks.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.identificacion.getValue()==null || this.identificacion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Identificación");
    		return false;
    	}
    	if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Descripción");
    		return false;
    	}
    	if (this.ubicacion.getValue()==null || this.ubicacion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Ubicación");
    		return false;
    	}    	
    	return true;
    }

	private void cargarCombo()
	{
		consultaSedesServer csf = consultaSedesServer.getInstance(CurrentUser.get());
		ArrayList<MapeoSedes> sedes = csf.datosSedesGlobal(null);
		if (sedes!=null && sedes.size()>0)
		{
			for (int i = 0; i<sedes.size();i++)
			{
				MapeoSedes mapeo = (MapeoSedes) sedes.get(i);
				this.descSede.addItem(mapeo.getIdentificacion());
			}
			
			this.descSede.setNullSelectionAllowed(false);
			this.descSede.setNewItemsAllowed(false);
		}
		sedes=null;
		csf=null;
	}
}
