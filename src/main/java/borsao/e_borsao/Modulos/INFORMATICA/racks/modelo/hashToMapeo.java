package borsao.e_borsao.Modulos.INFORMATICA.racks.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoRacks mapeo = null;
	 
	 public MapeoRacks convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoRacks();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("idSede")!=null && r_hash.get("idSede").length()>0) this.mapeo.setIdSede(new Integer(r_hash.get("idSede")));
		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setIdentificacion(r_hash.get("identificacion"));
		 this.mapeo.setUbicacion(r_hash.get("ubicacion"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoRacks convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoRacks();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 if (r_hash.get("idSede")!=null && r_hash.get("idSede").length()>0) this.mapeo.setIdSede(new Integer(r_hash.get("idSede")));
		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));		 
		 this.mapeo.setIdentificacion(r_hash.get("identificacion"));
		 this.mapeo.setUbicacion(r_hash.get("ubicacion"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoRacks r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());		 
		 if (r_mapeo.getIdSede()!=null) this.hash.put("idSede", r_mapeo.getIdSede().toString());
		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());		 
		 this.hash.put("identificacion", r_mapeo.getIdentificacion());
		 this.hash.put("ubicacion", r_mapeo.getUbicacion());
		 return hash;		 
	 }
}