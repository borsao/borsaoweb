package borsao.e_borsao.Modulos.INFORMATICA.racks.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.modelo.MapeoEquiposConectados;
import borsao.e_borsao.Modulos.INFORMATICA.equiposConectados.server.consultaEquiposConectadosServer;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.MapeoRacks;
import borsao.e_borsao.Modulos.INFORMATICA.racks.modelo.MapeoSwitch;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaRacksServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaRacksServer instance;
	private MapeoSwitch mapeo = null;
	
	public consultaRacksServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRacksServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRacksServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoRacks> datosRacksGlobal(MapeoRacks r_mapeo)
	{
		ResultSet rsRacks = null;		
		ArrayList<MapeoRacks> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT inf_racks.idCodigo rac_id, ");
		cadenaSQL.append(" inf_racks.identificador rac_ide, ");
		cadenaSQL.append(" inf_racks.descripcion rac_des, ");
		cadenaSQL.append(" inf_racks.ubicacion rac_ubi, ");
		cadenaSQL.append(" inf_racks.numBocas rac_boc, ");
		cadenaSQL.append(" inf_racks.idSede rac_sed, ");
		cadenaSQL.append(" inf_sedes.identificacion sed_sed ");
     	cadenaSQL.append(" FROM inf_racks ");
     	cadenaSQL.append(" inner join inf_sedes on inf_sedes.idCodigo = inf_racks.idSede ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNumBocas()!=null && r_mapeo.getNumBocas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" inf_racks.numBocas = " + r_mapeo.getNumBocas() + " ");
				}
				if (r_mapeo.getIdentificacion()!=null && r_mapeo.getIdentificacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" identificador = '" + r_mapeo.getIdentificacion() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getUbicacion()!=null && r_mapeo.getUbicacion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ubicacion = '" + r_mapeo.getUbicacion() + "' ");
				}
				if (r_mapeo.getIdSede()!=null && r_mapeo.getIdSede()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idSede = " + r_mapeo.getIdSede());
				}

			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by inf_racks.identificador asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsRacks.TYPE_SCROLL_SENSITIVE,rsRacks.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsRacks= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoRacks>();
			
			while(rsRacks.next())
			{
				MapeoRacks mapeoRacks = new MapeoRacks();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRacks.setIdCodigo(rsRacks.getInt("rac_id"));
				mapeoRacks.setIdentificacion(rsRacks.getString("rac_ide"));
				mapeoRacks.setDescripcion(rsRacks.getString("rac_des"));
				mapeoRacks.setUbicacion(rsRacks.getString("rac_ubi"));
				mapeoRacks.setNumBocas(rsRacks.getInt("rac_boc"));
				mapeoRacks.setIdSede(rsRacks.getInt("rac_sed"));
				mapeoRacks.setDescSede(rsRacks.getString("sed_sed"));
				vector.add(mapeoRacks);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer tamanoSwitch(String r_desc)
	{
		ResultSet rsRacks = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT inf_racks.numBocas rac_boc ");
     	cadenaSQL.append(" FROM inf_racks ");
     	cadenaSQL.append(" WHERE inf_racks.identificador = '" + r_desc + "' ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsRacks= cs.executeQuery(cadenaSQL.toString());
			
			while(rsRacks.next())
			{
				return rsRacks.getInt("rac_boc");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}
	
	public Integer obtenerId(String r_desc)
	{
		ResultSet rsRacks = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT inf_racks.idCodigo rac_id ");
     	cadenaSQL.append(" FROM inf_racks ");
     	cadenaSQL.append(" WHERE inf_racks.identificador = '" + r_desc + "' ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsRacks= cs.executeQuery(cadenaSQL.toString());
			
			while(rsRacks.next())
			{
				return rsRacks.getInt("rac_id");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsRacks = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(inf_racks.idCodigo) rac_sig ");
     	cadenaSQL.append(" FROM inf_racks ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsRacks.TYPE_SCROLL_SENSITIVE,rsRacks.CONCUR_UPDATABLE);
			rsRacks= cs.executeQuery(cadenaSQL.toString());
			
			while(rsRacks.next())
			{
				return rsRacks.getInt("rac_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public ArrayList<MapeoSwitch> datosSwitch(Integer r_tamano, String r_switch)
	{
		/*
		 * Conectados al API del switch devolvemos el estado de cada boca
		 */
		ArrayList<MapeoSwitch> vector = null;
  		URL url;
  		String boca=null; 
  		String[] valores =null;
  		String[] activo = null; 
  		String estado =null;
  		String numeroBocaReal =null;
  		
		try 
		{
//			this.prueba();
			
			url = new URL("http://" + r_switch.trim() + "/rest/v3/ports");
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8")); 
			
			vector = new ArrayList<MapeoSwitch>();			
			this.mapeo = new MapeoSwitch();
			
			this.mapeo.setIdentificacion(r_switch);
			
			String line = reader.readLine().replace("\"", "");
			
			if (line!=null)
			{
				
				/*
				 * indexOf( char ch, int start )
				 * 
				 * con esto buscaremos la llave "{" de la posicion que nos interesa
				 * luego buscaremos la posicion de la llave "}" que cierra la informacion de nuestra boca
				 * 
				 * substring(beginIndex, endIndex)
				 * 
				 * con esto extraemos toda la info de nuestra boca del switch
				 * 
				 */
				int pos = 0;
				int posf = 0;
				for (int i = 1; i<3;i++)
				{
					pos = line.indexOf("{",pos+1);
				}
				
				posf = line.indexOf("}",pos);
				boca = line.substring(pos, posf);
				valores = boca.split(",");
				activo = valores[4].split(":");
				estado = activo[1];
				rellenarMapeo(r_switch, 1,"1", estado);
				
				/*
				 *leer impares 
				 */
				int sec = 1;
				for (int b=2;b<r_tamano+1;b++)
				{
					pos = posf + 1;
					posf = line.indexOf("}",pos);
					boca = line.substring(pos, posf);
					valores = boca.split(",");
					
					activo = valores[2].split(":");
					numeroBocaReal = activo[1];
					
					if ((new Integer(numeroBocaReal)).intValue() % 2 != 0)
					{
						sec=sec + 1;
						activo = valores[5].split(":");
						estado = activo[1];
						rellenarMapeo(r_switch, sec,numeroBocaReal, estado);
					}
				}
				this.mapeo.setIdCodigo(1);
				vector.add(this.mapeo);
				
				this.mapeo = new MapeoSwitch();				
				this.mapeo.setIdentificacion(r_switch);
				
				pos = 0;
				posf = 0;
				for (int i = 1; i<3;i++)
				{
					pos = line.indexOf("{",pos+1);
				}
				posf = line.indexOf("}",pos);
				boca = line.substring(pos, posf);
				valores = boca.split(",");
				activo = valores[4].split(":");
				estado = activo[1];
				
				/*
				 * leer pares
				 */
				sec = 0;
				for (int b=2;b<r_tamano+1;b++)
				{
					pos = posf + 1;
					posf = line.indexOf("}",pos);
					boca = line.substring(pos, posf);
					valores = boca.split(",");
					
					activo = valores[2].split(":");
					numeroBocaReal = activo[1];

					if ((new Integer(numeroBocaReal)).intValue() % 2 == 0)
					{
						sec=sec + 1;
						activo = valores[5].split(":");
						estado = activo[1];
						rellenarMapeo(r_switch, sec,numeroBocaReal, estado);
					}
				}
				this.mapeo.setIdCodigo(2);
				vector.add(this.mapeo);
			}
			else
			{
				for (int j=1; j<3;j++)
				{
					this.mapeo = new MapeoSwitch();
					this.mapeo.setIdentificacion(r_switch);
	
					for (int i = 1;i<r_tamano/2+1; i++)
					{
						rellenarMapeo(r_switch, i,String.valueOf(i), "false");
					}
					this.mapeo.setIdCodigo(j);
					vector.add(this.mapeo);
				}
				Notificaciones.getInstance().mensajeError("Ruta del switch no alcanzada");
			}
		} 
		catch (MalformedURLException e) 
		{
//			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
//			e.printStackTrace();
		} 
		catch (ConnectException coex) {
//			coex.printStackTrace();
		}
		catch (IOException e) 
		{
//			e.printStackTrace();
		}
		finally {
			if (vector==null)
			{
				vector = new ArrayList<MapeoSwitch>();
				for (int j=1; j<3;j++)
				{
					this.mapeo = new MapeoSwitch();
					this.mapeo.setIdentificacion(r_switch);
	
					for (int i = 1;i<r_tamano/2+1; i++)
					{
						rellenarMapeo(r_switch, i,String.valueOf(i), "false");
					}
					this.mapeo.setIdCodigo(j);
					vector.add(this.mapeo);
				}
				Notificaciones.getInstance().mensajeError("Ruta del switch no alcanzada");
			}
		}

		return vector;
	}
	
	private void rellenarMapeo(String r_switch, Integer r_boca_grid, String r_boca_real, String r_valor)
	{
		String ruta = null;
		String activa = null;
		
		MapeoEquiposConectados map = new MapeoEquiposConectados();
		map.setDescRack(r_switch);
		map.setBoca(String.valueOf(r_boca_real));
		
		consultaEquiposConectadosServer cecs = consultaEquiposConectadosServer.getInstance(CurrentUser.get());
		MapeoEquiposConectados mapeoEncontrado = cecs.datosEquiposConectados(map);

		
		if (r_valor.contentEquals("true"))
		{
			ruta = "img/up.png";
			if (mapeoEncontrado== null || (mapeoEncontrado!=null && (mapeoEncontrado.getIdentificador()==null || (mapeoEncontrado.getIdentificador()!=null && mapeoEncontrado.getIdentificador().length()==0))))  ruta = "img/alert.png";
			activa="S";
		}
		else
		{
			ruta = "img/down.png";
			/*
			 * combrobar si en este switch y boca tengo datos almacenados, de ser asi cargo alert.png
			 */
			if (mapeoEncontrado!=null && mapeoEncontrado.getRoseta()!=null && mapeoEncontrado.getIdentificador()!=null && mapeoEncontrado.getIdentificador().length()>0)  ruta = "img/critical.png";
			
			activa="N";
		}
		
		switch (r_boca_grid)
		{
			case 1:
			{
				mapeo.setBoca1(ruta);
				mapeo.setBc1(activa);
				break;
			}
			case 2:
			{
				mapeo.setBoca2(ruta);
				mapeo.setBc2(activa);
				break;
			}
			case 3:
			{
				mapeo.setBoca3(ruta);
				mapeo.setBc3(activa);
				break;
			}
			case 4:
			{
				mapeo.setBoca4(ruta);
				mapeo.setBc4(activa);
				break;
			}
			case 5:
			{
				mapeo.setBoca5(ruta);
				mapeo.setBc5(activa);
				break;
			}
			case 6:
			{
				mapeo.setBoca6(ruta);
				mapeo.setBc6(activa);
				break;
			}
			case 7:
			{
				mapeo.setBoca7(ruta);
				mapeo.setBc7(activa);
				break;
			}
			case 8:
			{
				mapeo.setBoca8(ruta);
				mapeo.setBc8(activa);
				break;
			}
			case 9:
			{
				mapeo.setBoca9(ruta);
				mapeo.setBc9(activa);
				break;
			}
			case 10:
			{
				mapeo.setBoca10(ruta);
				mapeo.setBc10(activa);
				break;
			}
			case 11:
			{
				mapeo.setBoca11(ruta);
				mapeo.setBc11(activa);
				break;
			}
			case 12:
			{
				mapeo.setBoca12(ruta);
				mapeo.setBc12(activa);
				break;
			}
			case 13:
			{
				mapeo.setBoca13(ruta);
				mapeo.setBc13(activa);
				break;
			}
			case 14:
			{
				mapeo.setBoca14(ruta);
				mapeo.setBc14(activa);
				break;
			}
			case 15:
			{
				mapeo.setBoca15(ruta);
				mapeo.setBc15(activa);
				break;
			}
			case 16:
			{
				mapeo.setBoca16(ruta);
				mapeo.setBc16(activa);
				break;
			}
			case 17:
			{
				mapeo.setBoca17(ruta);
				mapeo.setBc17(activa);
				break;
			}
			case 18:
			{
				mapeo.setBoca18(ruta);
				mapeo.setBc18(activa);
				break;
			}
			case 19:
			{
				mapeo.setBoca19(ruta);
				mapeo.setBc19(activa);
				break;
			}
			case 20:
			{
				mapeo.setBoca20(ruta);
				mapeo.setBc20(activa);
				break;
			}
			case 21:
			{
				mapeo.setBoca21(ruta);
				mapeo.setBc21(activa);
				break;
			}
			case 22:
			{
				mapeo.setBoca22(ruta);
				mapeo.setBc22(activa);
				break;
			}
			case 23:
			{
				mapeo.setBoca23(ruta);
				mapeo.setBc23(activa);
				break;
			}
			case 24:
			{
				mapeo.setBoca24(ruta);
				mapeo.setBc24(activa);
				break;
			}
			
		}
	}
	public String datosEquipoConectado(String r_switch, String r_boca)
	{
		/*
		 * Llamamos al mantenimiento de equipos conectados para recoger los datos asociados a
		 * la boca del switch indicada.
		 */
		String rdo = null;
		return rdo;
	}
	
	public String guardarNuevo(MapeoRacks r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO inf_racks ( ");
    		cadenaSQL.append(" inf_racks.idCodigo, ");
    		cadenaSQL.append(" inf_racks.identificador, ");
    		cadenaSQL.append(" inf_racks.descripcion, ");
    		cadenaSQL.append(" inf_racks.ubicacion, ");
    		cadenaSQL.append(" inf_racks.numBocas, ");
    		cadenaSQL.append(" inf_racks.idSede ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getIdentificacion()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getIdentificacion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getUbicacion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getUbicacion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getNumBocas()!=null)
		    {		    	
		    	preparedStatement.setInt(5, r_mapeo.getNumBocas());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getIdSede()!=null)
		    {		    	
		    	preparedStatement.setInt(6, r_mapeo.getIdSede());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoRacks r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE inf_racks set ");
			cadenaSQL.append(" inf_racks.identificador=?, ");
			cadenaSQL.append(" inf_racks.descripcion=?, ");
		    cadenaSQL.append(" inf_racks.ubicacion=?, ");
		    cadenaSQL.append(" inf_racks.numBocas=?, ");
		    cadenaSQL.append(" inf_racks.idSede = ? ");
		    cadenaSQL.append(" WHERE inf_racks.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
	    		    preparedStatement.setString(1, r_mapeo.getIdentificacion());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setString(3, r_mapeo.getUbicacion());
		    preparedStatement.setInt(4, r_mapeo.getNumBocas());
		    preparedStatement.setInt(5, r_mapeo.getIdSede());
		    preparedStatement.setInt(6, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoRacks r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM inf_racks ");            
			cadenaSQL.append(" WHERE inf_racks.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	private void prueba()
	{
		
		
		try 
		{
		
		 String data = "username=tsol&password=Ts0lB0rsa02019";
	       URL url = new URL("http://192.168.30.21/rest/v1/login");
	       HttpURLConnection con = (HttpURLConnection) url.openConnection();
	       con.setRequestMethod("POST");
	       con.setDoOutput(true);
	        con.setRequestProperty("Content-Type", "application/json");
	       con.getOutputStream().write(data.getBytes("UTF-8"));
	       
	       
	       BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
	       String line = reader.readLine().replace("\"", "");		
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}
//		try
//		{
//		
//			ProcessBuilder pb = new ProcessBuilder(
//		               "curl",
//		               "-X",
//		               "POST https://192.168.30.21/rest/v10.04/login?username=tsol&password=Ts0lB0rsa02019 -H accept: */* ");
//	
//		       pb.directory(new File("/home	"));
//		       pb.redirectErrorStream(true);
//		       Process p = pb.start();
//		       
//		       pb.command("http://192.168.30.21/rest/v3/ports");
//		       p = pb.start();
//		       
//		       InputStream is = p.getInputStream();
//
//		       BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
//		       String line = reader.readLine().replace("\"", "");		       
//	
//		}
//		catch ( IOException io)
//		{
//			
//		}
	}
}