package borsao.e_borsao.Modulos.INFORMATICA.racks.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickListener;
import com.vaadin.ui.renderers.ImageRenderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.INFORMATICA.racks.view.pantallaEquiposConectados;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class VisionBocasRackGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private pantallaEquiposConectados app = null;
	private Integer tamano = null;
	
    public VisionBocasRackGrid(Window r_app, ArrayList<MapeoSwitch> r_vector, String r_rack, Integer r_tamano) 
    {
        this.vector=r_vector;
        this.app=(pantallaEquiposConectados) r_app;
        this.tamano= r_tamano;
        
		this.asignarTitulo("Switch " + r_rack);
		
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoSwitch.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	if (this.tamano==48)
    	setColumnOrder("idCodigo", "boca1", "boca2","boca3","boca4","boca5","boca6","boca7","boca8","boca9","boca10","boca11","boca12","boca13","boca14","boca15","boca16","boca17","boca18","boca19","boca20","boca21","boca22","boca23","boca24");
    	else
    	setColumnOrder("idCodigo", "boca1", "boca2","boca3","boca4","boca5","boca6","boca7","boca8","boca9","boca10","boca11","boca12");
    }
    
    public void establecerTitulosColumnas()
    {
    	RendererClickListener lis = new RendererClickListener() {

			@Override
			public void click(RendererClickEvent event) {
				if (event.getPropertyId()!=null)
            	{
            		MapeoSwitch mapeo = (MapeoSwitch) event.getItemId();
            		app.rellenarEntradasDatos(mapeo.getIdCodigo(), mapeo.getIdentificacion(), event.getPropertyId().toString());
            	}
			}
		};

    	this.getColumn("boca1").setHeaderCaption("1/2");
    	this.getColumn("boca1").setSortable(false);
    	this.getColumn("boca1").setWidth(new Double(75));    	
    	this.getColumn("boca1").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca2").setHeaderCaption("3/4");
    	this.getColumn("boca2").setSortable(false);
    	this.getColumn("boca2").setWidth(new Double(75));    	
    	this.getColumn("boca2").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca3").setHeaderCaption("5/6");
    	this.getColumn("boca3").setSortable(false);
    	this.getColumn("boca3").setWidth(new Double(75));    	
    	this.getColumn("boca3").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca4").setHeaderCaption("7/8");
    	this.getColumn("boca4").setSortable(false);
    	this.getColumn("boca4").setWidth(new Double(75));    	
    	this.getColumn("boca4").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca5").setHeaderCaption("9/10");
    	this.getColumn("boca5").setSortable(false);
    	this.getColumn("boca5").setWidth(new Double(75));    	
    	this.getColumn("boca5").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca6").setHeaderCaption("11/12");
    	this.getColumn("boca6").setSortable(false);
    	this.getColumn("boca6").setWidth(new Double(75));    	
    	this.getColumn("boca6").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca7").setHeaderCaption("13/14");
    	this.getColumn("boca7").setSortable(false);
    	this.getColumn("boca7").setWidth(new Double(75));    	
    	this.getColumn("boca7").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca8").setHeaderCaption("15/16");
    	this.getColumn("boca8").setSortable(false);
    	this.getColumn("boca8").setWidth(new Double(75));    	
    	this.getColumn("boca8").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca9").setHeaderCaption("17/18");
    	this.getColumn("boca9").setSortable(false);
    	this.getColumn("boca9").setWidth(new Double(75));    	
    	this.getColumn("boca9").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca10").setHeaderCaption("19/20");
    	this.getColumn("boca10").setSortable(false);
    	this.getColumn("boca10").setWidth(new Double(75));    	
    	this.getColumn("boca10").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca11").setHeaderCaption("21/22");
    	this.getColumn("boca11").setSortable(false);
    	this.getColumn("boca11").setWidth(new Double(75));    	
    	this.getColumn("boca11").setRenderer(new ImageRenderer(lis));
    	
    	this.getColumn("boca12").setHeaderCaption("23/24");
    	this.getColumn("boca12").setSortable(false);
    	this.getColumn("boca12").setWidth(new Double(75));    	
    	this.getColumn("boca12").setRenderer(new ImageRenderer(lis));
    	
    	if (this.tamano==48)
    	{
    		
    		this.getColumn("boca13").setHeaderCaption("25/26");
    		this.getColumn("boca13").setSortable(false);
    		this.getColumn("boca13").setWidth(new Double(75));    	
    		this.getColumn("boca13").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca14").setHeaderCaption("27/28");
    		this.getColumn("boca14").setSortable(false);
    		this.getColumn("boca14").setWidth(new Double(75));    	
    		this.getColumn("boca14").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca15").setHeaderCaption("29/30");
    		this.getColumn("boca15").setSortable(false);
    		this.getColumn("boca15").setWidth(new Double(75));    	
    		this.getColumn("boca15").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca16").setHeaderCaption("31/32");
    		this.getColumn("boca16").setSortable(false);
    		this.getColumn("boca16").setWidth(new Double(75));    	
    		this.getColumn("boca16").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca17").setHeaderCaption("33/34");
    		this.getColumn("boca17").setSortable(false);
    		this.getColumn("boca17").setWidth(new Double(75));    	
    		this.getColumn("boca17").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca18").setHeaderCaption("35/36");
    		this.getColumn("boca18").setSortable(false);
    		this.getColumn("boca18").setWidth(new Double(75));    	
    		this.getColumn("boca18").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca19").setHeaderCaption("37/38");
    		this.getColumn("boca19").setSortable(false);
    		this.getColumn("boca19").setWidth(new Double(75));    	
    		this.getColumn("boca19").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca20").setHeaderCaption("39/40");
    		this.getColumn("boca20").setSortable(false);
    		this.getColumn("boca20").setWidth(new Double(75));    	
    		this.getColumn("boca20").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca21").setHeaderCaption("41/42");
    		this.getColumn("boca21").setSortable(false);
    		this.getColumn("boca21").setWidth(new Double(75));    	
    		this.getColumn("boca21").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca22").setHeaderCaption("43/44");
    		this.getColumn("boca22").setSortable(false);
    		this.getColumn("boca22").setWidth(new Double(75));    	
    		this.getColumn("boca22").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca23").setHeaderCaption("45/46");
    		this.getColumn("boca23").setSortable(false);
    		this.getColumn("boca23").setWidth(new Double(75));    	
    		this.getColumn("boca23").setRenderer(new ImageRenderer(lis));
    		
    		this.getColumn("boca24").setHeaderCaption("47/48");
    		this.getColumn("boca24").setSortable(false);
    		this.getColumn("boca24").setWidth(new Double(75));    	
    		this.getColumn("boca24").setRenderer(new ImageRenderer(lis));
    	}
    	else
    	{
        	this.getColumn("boca13").setHidden(true);
        	this.getColumn("boca14").setHidden(true);
        	this.getColumn("boca15").setHidden(true);
        	this.getColumn("boca16").setHidden(true);
        	this.getColumn("boca17").setHidden(true);
        	this.getColumn("boca18").setHidden(true);
        	this.getColumn("boca19").setHidden(true);
        	this.getColumn("boca20").setHidden(true);
        	this.getColumn("boca21").setHidden(true);
        	this.getColumn("boca22").setHidden(true);
        	this.getColumn("boca23").setHidden(true);
        	this.getColumn("boca24").setHidden(true);
    	}
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("identificacion").setHidden(true);
    	this.getColumn("bc1").setHidden(true);
    	this.getColumn("bc2").setHidden(true);
    	this.getColumn("bc3").setHidden(true);
    	this.getColumn("bc4").setHidden(true);
    	this.getColumn("bc5").setHidden(true);
    	this.getColumn("bc6").setHidden(true);
    	this.getColumn("bc7").setHidden(true);
    	this.getColumn("bc8").setHidden(true);
    	this.getColumn("bc9").setHidden(true);
    	this.getColumn("bc10").setHidden(true);
    	this.getColumn("bc11").setHidden(true);
    	this.getColumn("bc12").setHidden(true);
    	this.getColumn("bc13").setHidden(true);
    	this.getColumn("bc14").setHidden(true);
    	this.getColumn("bc15").setHidden(true);
    	this.getColumn("bc16").setHidden(true);
    	this.getColumn("bc17").setHidden(true);
    	this.getColumn("bc18").setHidden(true);
    	this.getColumn("bc19").setHidden(true);
    	this.getColumn("bc20").setHidden(true);
    	this.getColumn("bc21").setHidden(true);
    	this.getColumn("bc22").setHidden(true);
    	this.getColumn("bc23").setHidden(true);
    	this.getColumn("bc24").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	return "cell-nativebuttonBocas" ;
            }
        });
    }
    
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoSwitch mapeo = (MapeoSwitch) event.getItemId();
            		app.rellenarEntradasDatos(mapeo.getIdCodigo(), mapeo.getIdentificacion(), event.getPropertyId().toString());
            	}
            }
        });
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoSwitch) {
                MapeoSwitch progRow = (MapeoSwitch)bean;
                if (cell.getPropertyId().toString().contains("oca"))
                {
                	// 	The actual description text is depending on the application
                	if (((ThemeResource) cell.getValue())!=null && ((ThemeResource) cell.getValue()).toString().contains("critical")) 
                	{
                		descriptionText = "Boca no activa con equipo identificado";
                	}
                	else if (((ThemeResource) cell.getValue())!=null && ((ThemeResource) cell.getValue()).toString().contains("alert")) 
                	{
                		descriptionText = "Boca activa con equipo no identificado";
                	}
                	else if (((ThemeResource) cell.getValue())!=null && ((ThemeResource) cell.getValue()).toString().contains("down")) 
                	{
                		descriptionText = "Boca no activa";
                	}	
                	else if (((ThemeResource) cell.getValue())!=null && ((ThemeResource) cell.getValue()).toString().contains("up")) 
                	{
                		descriptionText = "Boca activa";
                	}	
                }
                
            }
        }
        return descriptionText;
    }

	@Override
	public void calcularTotal() {
		
	}
	@Override
	public void establecerColumnasNoFiltro() 
	{
	}
}


