package borsao.e_borsao.Modulos.INFORMATICA.racks.modelo;

import java.util.ArrayList;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.INFORMATICA.racks.view.pantallaEquiposConectados;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class RacksGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	
    public RacksGrid(ArrayList<MapeoRacks> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Switches");		
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoRacks.class);        
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("estado", "bocas", "identificacion", "descripcion", "ubicacion", "numBocas", "idSede", "descSede");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("identificacion").setHeaderCaption("Nombre");
    	this.getColumn("descripcion").setHeaderCaption("Descripción");
    	this.getColumn("numBocas").setHeaderCaption("Tamaño");
    	this.getColumn("idSede").setHeaderCaption("Sede");
    	this.getColumn("descSede").setHeaderCaption("Nombre Sede");
    	
    	this.getColumn("estado").setHeaderCaption("");
    	this.getColumn("estado").setSortable(false);
    	this.getColumn("estado").setWidth(new Double(50));

    	this.getColumn("bocas").setHeaderCaption("");
    	this.getColumn("bocas").setSortable(false);
    	this.getColumn("bocas").setWidth(new Double(50));

//    	this.getColumn("boca1").setHeaderCaption("");
//    	this.getColumn("boca1").setSortable(false);
//    	this.getColumn("boca1").setWidth(new Double(50));    	
//    	this.getColumn("boca1").setRenderer(new ImageRenderer());
//    	
//    	this.getColumn("boca2").setHeaderCaption("");
//    	this.getColumn("boca2").setSortable(false);
//    	this.getColumn("boca2").setWidth(new Double(50));
//    	this.getColumn("boca2").setRenderer(new ImageRenderer());
    	
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoRacks) {
                // The actual description text is depending on the application
                if ("estado".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Web Switch";
                }
                else if ("bocas".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a dispositivos conectados";
                }
            }
        }
        return descriptionText;
    }
	public void asignarEstilos() 
	{		
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	String estilo = null;
            	if ( "estado".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonRack";
            	}
            	else if ( "bocas".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonBoca";
            	}
//            	else if ( "boca1".equals(cellReference.getPropertyId()))
//            	{
//            		estilo = "cell-nativebuttonBocas";
//            	}
//            	else if ( "boca2".equals(cellReference.getPropertyId()))
//            	{
//            		estilo = "cell-nativebuttonBocas";
//            	}
            	return estilo;
            }
        });
    	
    	
	}

	public void cargarListeners() 
	{		
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoRacks mapeo = (MapeoRacks) event.getItemId();
            		
            		if (event.getPropertyId().toString().equals("estado"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;

	            		if (mapeo.getIdentificacion()!=null)
	            		{
	            			String url = "http://" + mapeo.getIdentificacion();
	            			getUI().getPage().open(url, "_blank");
	            		}

	            	}
            		else if (event.getPropertyId().toString().equals("bocas"))
	            	{
            			activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getIdentificacion()!=null && mapeo.getIdentificacion().length()>0)
	            		{
	        	    		/*
	        	    		 * Abriremos ventana peticion parametros
	        	    		 * rellenaremos la variables publicas
	        	    		 * 		cajas y anada
	        	    		 * 		llamaremos al metodo generacionPdf
	        	    		 */
	            			pantallaEquiposConectados vtPeticion = new pantallaEquiposConectados("Switch " + mapeo.getIdentificacion(), mapeo.getIdentificacion());
	        	    		getUI().addWindow(vtPeticion);
	            		}
            		}
	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
            	}
            }
    	});
    	
	}

	@Override
	public void calcularTotal() {
		
	}
}
