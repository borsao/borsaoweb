package borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class PresentadoresGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public PresentadoresGrid(ArrayList<MapeoPresentadores> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Presentadores");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoPresentadores.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("nif", "nombre", "direccion","codigoPostal", "cae");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombre").setHeaderCaption("Nombre");
    	this.getColumn("codigoPostal").setHeaderCaption("C.P.");
    	this.getColumn("cae").setHeaderCaption("CAE");
    	this.getColumn("direccion").setHeaderCaption("Direccion");    	
    	this.getColumn("nif").setHeaderCaption("NIF");
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
