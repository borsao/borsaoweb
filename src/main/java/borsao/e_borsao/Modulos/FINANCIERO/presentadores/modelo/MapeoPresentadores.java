package borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoPresentadores extends MapeoGlobal
{
	private String nombre;
	
	private String direccion;
	private String codigoPostal;
	private String nif;
	private String cae;

	public MapeoPresentadores()
	{
		this.setNombre("");
		this.setDireccion("");
		this.setNif("");
		this.setCodigoPostal("");
		this.setCae("");
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getCae() {
		return cae;
	}

	public void setCae(String cae) {
		this.cae = cae;
	}


}