package borsao.e_borsao.Modulos.FINANCIERO.presentadores.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.MapeoPresentadores;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.PresentadoresGrid;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.server.consultaPresentadoresServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoPresentadores mapeoPresentadores = null;
	 
	 private consultaPresentadoresServer cus = null;	 
	 private PresentadoresView uw = null;
	 private BeanFieldGroup<MapeoPresentadores> fieldGroup;
	 
	 public OpcionesForm(PresentadoresView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoPresentadores>(MapeoPresentadores.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoPresentadores = (MapeoPresentadores) uw.grid.getSelectedRow();
                eliminarPresentadores(mapeoPresentadores);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoPresentadores = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearPresentadores(mapeoPresentadores);
	 			}
	 			else
	 			{
	 				MapeoPresentadores mapeoPresentadores_orig = (MapeoPresentadores) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoPresentadores= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarPresentadores(mapeoPresentadores,mapeoPresentadores_orig);
	 				hm=null;
	 			}
	 			if (((PresentadoresGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarPresentadores(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.codigoPostal.getValue()!=null && this.codigoPostal.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("codigoPostal", this.codigoPostal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoPostal", "");
		}
		
		if (this.direccion.getValue()!=null && this.direccion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("direccion", this.direccion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("direccion", "");
		}
		
		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.cae.getValue()!=null && this.cae.getValue().length()>0) 
		{
			opcionesEscogidas.put("cae", this.cae.getValue());
		}
		else
		{
			opcionesEscogidas.put("cae", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarPresentadores(null);
	}
	
	public void editarPresentadores(MapeoPresentadores r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoPresentadores();
		fieldGroup.setItemDataSource(new BeanItem<MapeoPresentadores>(r_mapeo));
		nombre.focus();
	}
	
	public void eliminarPresentadores(MapeoPresentadores r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((PresentadoresGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoPresentadores>) ((PresentadoresGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarPresentadores(MapeoPresentadores r_mapeo, MapeoPresentadores r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaPresentadoresServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((PresentadoresGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearPresentadores(MapeoPresentadores r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaPresentadoresServer(CurrentUser.get());
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoPresentadores>(r_mapeo));
				if (((PresentadoresGrid) uw.grid)!=null)
				{
					((PresentadoresGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoPresentadores> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoPresentadores= item.getBean();
            if (this.mapeoPresentadores.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.direccion.getValue()==null || this.direccion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Dirección");
    		return false;
    	}
    	if (this.codigoPostal.getValue()==null || this.codigoPostal.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Código Postal");
    		return false;
    	}
    	if (this.nombre.getValue()==null || this.nombre.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Nombre");
    		return false;
    	}
    	if (this.nif.getValue()==null || this.nif.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Nif");
    		return false;
    	}
    	if (this.cae.getValue()!=null & this.cae.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el CAE");
    		return false;
    	}
    	return true;
    }

}
