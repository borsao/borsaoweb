package borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoPresentadores mapeo = null;
	 
	 public MapeoPresentadores convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPresentadores();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigoPostal(r_hash.get("codigoPostal"));		 
		 this.mapeo.setDireccion(r_hash.get("direccion"));
		 this.mapeo.setNif(r_hash.get("nif"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setCae(r_hash.get("cae"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoPresentadores convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoPresentadores();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigoPostal(r_hash.get("codigoPostal"));		 
		 this.mapeo.setDireccion(r_hash.get("direccion"));
		 this.mapeo.setNif(r_hash.get("nif"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setCae(r_hash.get("cae"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoPresentadores r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("codigoPostal", r_mapeo.getCodigoPostal());		 
		 this.hash.put("direccion", r_mapeo.getDireccion());
		 this.hash.put("nombre", r_mapeo.getNombre());
		 this.hash.put("nif", r_mapeo.getNif());
		 this.hash.put("cae", r_mapeo.getCae());
		 return hash;		 
	 }
}