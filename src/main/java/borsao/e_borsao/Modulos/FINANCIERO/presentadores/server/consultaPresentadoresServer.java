package borsao.e_borsao.Modulos.FINANCIERO.presentadores.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.MapeoPresentadores;

public class consultaPresentadoresServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaPresentadoresServer instance;
	
	public consultaPresentadoresServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPresentadoresServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPresentadoresServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoPresentadores> datosPresentadoresGlobal(MapeoPresentadores r_mapeo)
	{
		ResultSet rsPresentadores = null;		
		ArrayList<MapeoPresentadores> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_presentadores.id pre_id, ");
		cadenaSQL.append(" fin_presentadores.nombre pre_nom, ");
		cadenaSQL.append(" fin_presentadores.direccion pre_dir, ");
		cadenaSQL.append(" fin_presentadores.codigoPostal pre_cod, ");
		cadenaSQL.append(" fin_presentadores.nif pre_nif, ");
		cadenaSQL.append(" fin_presentadores.cae pre_cae ");
     	cadenaSQL.append(" FROM fin_presentadores ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getDireccion()!=null && r_mapeo.getDireccion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" direccion = '" + r_mapeo.getDireccion() + "' ");
				}
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nif = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getCae()!=null && r_mapeo.getCae().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cae = '" + r_mapeo.getCae() + "' ");
				}
				if (r_mapeo.getCodigoPostal()!=null && r_mapeo.getCodigoPostal().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigoPostal = '" + r_mapeo.getCodigoPostal() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsPresentadores.TYPE_SCROLL_SENSITIVE,rsPresentadores.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsPresentadores= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoPresentadores>();
			
			while(rsPresentadores.next())
			{
				MapeoPresentadores mapeoPresentadores = new MapeoPresentadores();
				/*
				 * recojo mapeo operarios
				 */
				mapeoPresentadores.setIdCodigo(rsPresentadores.getInt("pre_id"));
				mapeoPresentadores.setNombre(rsPresentadores.getString("pre_nom"));
				mapeoPresentadores.setDireccion(rsPresentadores.getString("pre_dir"));
				mapeoPresentadores.setCodigoPostal(rsPresentadores.getString("pre_cod"));
				mapeoPresentadores.setNif(rsPresentadores.getString("pre_nif"));
				mapeoPresentadores.setCae(rsPresentadores.getString("pre_cae"));
				vector.add(mapeoPresentadores);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPresentadores!=null)
				{
					rsPresentadores.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		Connection con = null;
		Statement cs = null;

		ResultSet rsPresentadores = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_presentadores.id) pre_sig ");
     	cadenaSQL.append(" FROM fin_presentadores ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsPresentadores.TYPE_SCROLL_SENSITIVE,rsPresentadores.CONCUR_UPDATABLE);
			rsPresentadores= cs.executeQuery(cadenaSQL.toString());
			
			while(rsPresentadores.next())
			{
				return rsPresentadores.getInt("pre_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPresentadores!=null)
				{
					rsPresentadores.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return 0;
	}

	public Integer obtenerId(String r_titular)
	{
		ResultSet rsPresentadores = null;		
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT fin_presentadores.id pre_id ");
     	cadenaSQL.append(" FROM fin_presentadores ");
     	cadenaSQL.append(" WHERE fin_presentadores.cae ='" + r_titular + "' " );
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsPresentadores.TYPE_SCROLL_SENSITIVE,rsPresentadores.CONCUR_UPDATABLE);
			rsPresentadores= cs.executeQuery(cadenaSQL.toString());
			
			while(rsPresentadores.next())
			{
				return rsPresentadores.getInt("pre_id") ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsPresentadores!=null)
				{
					rsPresentadores.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public String guardarNuevo(MapeoPresentadores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_presentadores ( ");
    		cadenaSQL.append(" fin_presentadores.id, ");
    		cadenaSQL.append(" fin_presentadores.direccion, ");
    		cadenaSQL.append(" fin_presentadores.codigoPostal, ");
	        cadenaSQL.append(" fin_presentadores.cae, ");
		    cadenaSQL.append(" fin_presentadores.nif, ");
		    cadenaSQL.append(" fin_presentadores.nombre ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDireccion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCodigoPostal()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCodigoPostal());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCae()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCae());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getNombre());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }       
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return null;
	}
	
	public void guardarCambios(MapeoPresentadores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_presentadores set ");
			cadenaSQL.append(" fin_presentadores.direccion=?, ");
		    cadenaSQL.append(" fin_presentadores.codigoPostal=?, ");
		    cadenaSQL.append(" fin_presentadores.cae=?, ");
		    cadenaSQL.append(" fin_presentadores.nif=?, ");
		    cadenaSQL.append(" fin_presentadores.nombre=? ");
		    cadenaSQL.append(" WHERE fin_presentadores.id = ? ");
		    
		    con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getDireccion());
		    preparedStatement.setString(2, r_mapeo.getCodigoPostal());
		    preparedStatement.setString(3, r_mapeo.getCae());
		    preparedStatement.setString(4, r_mapeo.getNif());
		    preparedStatement.setString(5, r_mapeo.getNombre());
		    preparedStatement.setInt(6, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}
	
	public void eliminar(MapeoPresentadores r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_presentadores ");            
			cadenaSQL.append(" WHERE fin_presentadores.id = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}
}