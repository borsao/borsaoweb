package borsao.e_borsao.Modulos.FINANCIERO.consultaRegistrosIVA.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;

public class consultaRegistrosIvaServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaRegistrosIvaServer instance;
	
	
	public consultaRegistrosIvaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRegistrosIvaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRegistrosIvaServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoGlobal> datosOpcionesGlobal()
	{
		ArrayList<MapeoGlobal> vector = null;
		return vector;
	}
	
	public String generarInforme(ArrayList<MapeoGlobal> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer cuadre = null; 
		try
		{
			cadenaSQL.append(" SELECT min(c_iva.fecha_fra) iva_fra ");
	     	cadenaSQL.append(" FROM c_iva ");
	     	cadenaSQL.append(" where modalidad_iva = 'S' ");
	     	cadenaSQL.append(" and autoconsumo = 'N' ");

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			cuadre = 0;
			
			while(rsOpcion.next())
			{
				cuadre = rsOpcion.getInt("sa_sal");
			}
			
			if (cuadre!=0)
			{
				return "2";
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return "0";
	}

	public void eliminar()
	{
	}

	public String guardarNuevo(MapeoGlobal r_mapeo)
	{
		return null;
	}
	
}