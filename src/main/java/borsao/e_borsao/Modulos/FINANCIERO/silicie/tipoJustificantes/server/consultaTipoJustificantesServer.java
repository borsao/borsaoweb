package borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.modelo.MapeoTipoJustificantes;

public class consultaTipoJustificantesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTipoJustificantesServer instance;
	
	public consultaTipoJustificantesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTipoJustificantesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTipoJustificantesServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoTipoJustificantes> datosTipoJustificantesGlobal(MapeoTipoJustificantes r_mapeo)
	{
		ResultSet rsTipoJustificantes = null;		
		ArrayList<MapeoTipoJustificantes> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_jus.id jus_id, ");
		cadenaSQL.append(" fin_silicie_jus.codigo jus_cod, ");
		cadenaSQL.append(" fin_silicie_jus.descripcion jus_des ");
     	cadenaSQL.append(" FROM fin_silicie_jus ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsTipoJustificantes= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoTipoJustificantes>();
			
			while(rsTipoJustificantes.next())
			{
				MapeoTipoJustificantes mapeoTipoJustificantes = new MapeoTipoJustificantes();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTipoJustificantes.setIdCodigo(rsTipoJustificantes.getInt("jus_id"));
				mapeoTipoJustificantes.setCodigo(rsTipoJustificantes.getString("jus_cod"));
				mapeoTipoJustificantes.setDescripcion(rsTipoJustificantes.getString("jus_des"));
				vector.add(mapeoTipoJustificantes);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsTipoJustificantes = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_jus.id) jus_sig ");
     	cadenaSQL.append(" FROM fin_silicie_jus ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsTipoJustificantes= cs.executeQuery(cadenaSQL.toString());
			
			while(rsTipoJustificantes.next())
			{
				return rsTipoJustificantes.getInt("jus_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoTipoJustificantes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_jus ( ");
    		cadenaSQL.append(" fin_silicie_jus.id, ");
    		cadenaSQL.append(" fin_silicie_jus.codigo, ");
    		cadenaSQL.append(" fin_silicie_jus.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoTipoJustificantes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_jus set ");
			cadenaSQL.append(" fin_silicie_jus.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_jus.descripcion=? ");
		    cadenaSQL.append(" WHERE fin_silicie_jus.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoTipoJustificantes r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_jus ");            
			cadenaSQL.append(" WHERE fin_silicie_jus.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select codigo, descripcion from fin_silicie_jus order by codigo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("codigo"));
				mA.setDescripcion(rsArticulos.getString("descripcion"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

}