package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.CargaFichero;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionFormatoImpresion;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.modelo.MapeoMovimientosDeclaradoSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.server.consultaDeclaracionSilicieServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoInventarioSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MovimientosSilicieGrid;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class MovimientosSilicieView extends GridViewRefresh {

	public static final String VIEW_NAME = "Asientos Silicie";
	public static final Integer rowCSVLimit = 1000;
	
	private OpcionesForm form = null;
	public consultaMovimientosSilicieServer cmss =null;
	public MapeoMovimientosSilicie mapeoMovimientosSilicieEnlazado=null;
	public boolean modificando = false;

	public boolean conTotales = false;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private int intervaloRefresco = 0*60*1000; //milisegundos
	public String tituloGrid = "Movimientos Generados en ";
	
	public ComboBox cmbCaeTitular =null;	
	public TextField txtEjercicio = null;
	public ComboBox cmbMes = null;
	public ComboBox cmbEstado = null;
	public DateField fechaPresentacion = null;
	
	public String numeroAlmacen=null;
	public String almacenSeleccionado=null;
	
	private Button opcGenerar = null;
	private Button opcDeclarar = null;
	private Button opcValidar = null;
	private Button opcAeat = null;
	private Button opcVerificar = null;
	private Button opcAnular = null;
	private Button opcRectificar = null;
	private VentanaAceptarCancelar  vt = null;

	private Integer ejercicioDeclaracion = null;
	private Integer mesDeclaracion = null;
	private String fechaDeclaracion = null;
	private String titular = null;
	private String accion = null;
	
	private HashMap<String,String> filtrosRec = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public MovimientosSilicieView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	this.cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
    	
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		this.cmbCaeTitular= new ComboBox("Declarante");    		
		this.cmbCaeTitular.setNewItemsAllowed(false);
		this.cmbCaeTitular.setNullSelectionAllowed(false);
		this.cmbCaeTitular.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.txtEjercicio= new TextField("Ejercicio");    	
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		
		this.cmbMes= new ComboBox("Mes");    		
		this.cmbMes.setNewItemsAllowed(false);
		this.cmbMes.setNullSelectionAllowed(false);
		this.cmbMes.addStyleName(ValoTheme.COMBOBOX_TINY);
				
		this.cmbEstado= new ComboBox("Estado");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);
		
    	this.fechaPresentacion = new DateField("Fecha Presentacion");
    	this.fechaPresentacion.setEnabled(true);
    	this.fechaPresentacion.setDateFormat("dd/MM/yyyy");
    	this.fechaPresentacion.setVisible(false);
    	this.fechaPresentacion.addStyleName(ValoTheme.DATEFIELD_TINY);
    	
		this.cabLayout.addComponent(this.cmbCaeTitular);
		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.cmbMes);
		this.cabLayout.addComponent(this.cmbEstado);
		this.cabLayout.addComponent(this.fechaPresentacion);
		
		this.opcGenerar = new Button("Generacion");
		this.opcGenerar.setIcon(FontAwesome.COGS);
		this.opcGenerar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcGenerar.addStyleName(ValoTheme.BUTTON_TINY);

		this.opcDeclarar = new Button("Declaracion");
		this.opcDeclarar.setIcon(FontAwesome.COGS);
		this.opcDeclarar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcDeclarar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcDeclarar.setEnabled(false);
		
		this.opcValidar = new Button("Validar");
		this.opcValidar.setIcon(FontAwesome.COGS);
		this.opcValidar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcValidar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcValidar.setEnabled(false);

		this.opcAeat = new Button("Verificar AEAT");
		this.opcAeat.setIcon(FontAwesome.COGS);
		this.opcAeat.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcAeat.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcAeat.setEnabled(false);

		this.opcAnular = new Button("Anular Dec.");
		this.opcAnular.setIcon(FontAwesome.COGS);
		this.opcAnular.addStyleName(ValoTheme.BUTTON_DANGER);
		this.opcAnular.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcAnular.setEnabled(false);

		this.opcRectificar = new Button("Anular Astos.");
		this.opcRectificar.setIcon(FontAwesome.COGS);
		this.opcRectificar.addStyleName(ValoTheme.BUTTON_DANGER);
		this.opcRectificar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcRectificar.setEnabled(false);

		this.opcVerificar = new Button("Verificar Stock");
		this.opcVerificar.setIcon(FontAwesome.EXCHANGE);
		this.opcVerificar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcVerificar.addStyleName(ValoTheme.BUTTON_TINY);

		this.topLayoutL.addComponent(this.opcGenerar);
		this.topLayoutC.addComponent(this.opcDeclarar);
		this.topLayoutC.addComponent(this.opcValidar);
		this.topLayoutR.addComponent(this.opcVerificar);
		this.topLayoutR.addComponent(this.opcAeat);
		this.topLayoutR.addComponent(this.opcAnular);
		this.topLayoutR.addComponent(this.opcRectificar);
		
		this.topLayoutL.setComponentAlignment(this.opcGenerar, Alignment.MIDDLE_LEFT);
		
		this.cargarCombo();
		this.cargarListeners();
		this.establecerModo();
		
		lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoMovimientosSilicie> r_vector=null;
    	MapeoMovimientosSilicie mapeoMovimientosSilicie =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoMovimientosSilicie=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	mapeoMovimientosSilicie.setCaeTitular(this.cmbCaeTitular.getValue().toString());
    	mapeoMovimientosSilicie.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    	if (cmbMes.getValue()!=null && cmbMes.getValue()!="Todos") mapeoMovimientosSilicie.setMes(new Integer(this.cmbMes.getValue().toString()));
    	mapeoMovimientosSilicie.setEstado(this.cmbEstado.getValue().toString());

    	r_vector=this.cmss.datosMovimientosSilicieGlobal(mapeoMovimientosSilicie);
    	this.presentarGrid(r_vector);
    	
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }

    private void generacion()
    {
		PeticionRangosGeneracion vtr = new PeticionRangosGeneracion(this,this.cmbCaeTitular.getValue().toString(), this.txtEjercicio.getValue(), this.cmbMes.getValue().toString());
		getUI().addWindow(vtr);	            				
    }
    
    private void cargar()
    {
    	CargaFichero cf = new CargaFichero("basic", this, eBorsao.get().prop.rutaSilicie, null);
    	getUI().addWindow(cf);
    }
    
    public void validar(String r_separador, String r_path, String r_encabezados)
    {
    	if (accion.contentEquals("Validar"))
    	{
    		this.validarAsientosAeat(r_separador, r_path, r_encabezados);
    	}
    	else
    	{
    		this.chequearExistenciasAeat(r_separador, r_path, r_encabezados);
    	}
    }
    
    private void validarAsientosAeat(String r_separador, String r_path, String r_encabezados)
    {
    	ArrayList<MapeoMovimientosDeclaradoSilicie> vector =null;
    	String rdo = null;
    	consultaDeclaracionSilicieServer cdss = new consultaDeclaracionSilicieServer(CurrentUser.get());
    	
    	r_path = eBorsao.get().prop.rutaSilicie + r_path;
    	
    	vector = RutinasFicheros.leerCsvDeclaradoSilicie(r_separador, r_path, cmbEstado.getValue().toString().equals("Anulados"), r_encabezados);

    	if (vector !=null && vector.size()>0)
    	{
	    	if (cmbEstado.getValue().toString().equals("Anulados"))
	    	{
	    		rdo = cdss.actualizarMovimientosAnulacion(vector,this.cmbCaeTitular.getValue().toString(), this.cmbMes.getValue().toString());
	    	}
	    	else
	    	{
	    		rdo = cdss.actualizarMovimientosDeclarados(this.txtEjercicio.getValue(), vector,this.cmbCaeTitular.getValue().toString(), this.cmbMes.getValue().toString());
	    	}
			if (rdo==null)
			{
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				generarGrid(opcionesEscogidas);
			}
			else
			{
				Notificaciones.getInstance().mensajeError(rdo);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Problemas con la lectura del fichero. No devuleve datos");
		}    	
    }

    private void chequearExistenciasAeat(String r_separador, String r_path, String r_encabezados)
    {
    	ArrayList<MapeoInventarioSilicie> vector =null;
    	String rdo = null;
    	Integer rdoInt = null;
    	consultaDeclaracionSilicieServer cdss = new consultaDeclaracionSilicieServer(CurrentUser.get());
    	consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
    	
    	r_path = eBorsao.get().prop.rutaSilicie + r_path;
    	
    	vector = RutinasFicheros.leerCsvInventarioSilicieAeat(r_separador, r_path, r_encabezados);

    	if (vector !=null && vector.size()>0)
    	{
    		/*
    		 * Insertamos los datos en la tabla aeat_existencias
    		 */
    		rdo = cmss.cargarInventarioAeatSilicie(this.cmbCaeTitular.getValue().toString(), vector);
    		/*
    		 * Generamos los datos de existencias como si fueras a imprimir el inventario
    		 * Siempre stock actual
    		 */
    		if (rdo==null)
    		{
	    		rdoInt = cmss.generacionDatosTemporalExistencias("Articulos", this.txtEjercicio.getValue(),this.cmbMes.getValue().toString(), this.cmbCaeTitular.getValue().toString(),null);
	    		/*
	    		 * sacamos pantalla con el cuadre de existencias
	    		 * 
	    		 * La pantalla mostrará los articulos y su valor de inventario tanto en intranet como en AEAT
	    		 * Los que tengasn diferencias entre el stock de aeat y los de la intranet se visularizarán en rojo
	    		 * 
	    		 * Al arrancar presentara los datos y un mensaje al usuario indicando si hay diferencias o no, para mayor comodidad 
	    		 * 
	    		 */
	    		pantallaLineasChequeoInventarioAeat vt = new pantallaLineasChequeoInventarioAeat(this, this.cmbCaeTitular.getValue().toString(), "Chequeo Inventario AEAT");
	    		getUI().addWindow(vt);
    		}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Problemas con la lectura del fichero. No devuleve datos");
		}    	
    }

    private void declarar()
    {   
    	String mensaje="";
    	this.activarBotones(false);
    	
		ejercicioDeclaracion = new Integer(txtEjercicio.getValue());
		if (!cmbMes.getValue().toString().toUpperCase().equals("TODOS")) mesDeclaracion = new Integer(cmbMes.getValue().toString()); else mesDeclaracion=null;
		titular = cmbCaeTitular.getValue().toString().trim();
		
		try 
		{
			fechaDeclaracion = RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue()));
		} 
		catch (Exception e) 
		{
		}
		
		generarTabla(titular, ejercicioDeclaracion, mesDeclaracion,fechaDeclaracion);
		
		if (cmbEstado.getValue().toString().equals("Anulados"))
		{
			mensaje = "Pesentacion asientos anulados para el ejercicio, mes y almacen seleccionados? (Elige formato de presentacion CSV o XML))";
			
		}
		else
		{
			mensaje = "Declaracion de los movimientos para el ejercicio, mes y almacen seleccionados? (Elige formato de presentacion CSV o XML))";
		}
		
		this.vt = new VentanaAceptarCancelar(this, mensaje, "XML", "CSV", "XML", "CSV");			
    	getUI().addWindow(vt);
    	
    	this.botonesGenerales(true);
		this.navegacion(true);
		this.verBotones(true);
		this.activarBotones(true);
    }

    private void anularAsientos()
    {
    	/*
    	 * La primera vez que ejecutamos solo activa la seleccion multiple de registros en el grid
    	 * 
    	 */
    	if (grid.getSelectionModel() instanceof SingleSelectionModel)
		{
    		Notificaciones.getInstance().mensajeAdvertencia("Recuerda que tienes que declararlos el mismo día que los anulas.");
    		grid.setSelectionMode(SelectionMode.MULTI);
		}
    	/*
    	 * La segunda vez que ejecutamos empieza el proceso de declaracion de anulacion de asientos
    	 * 
    	 */
    	else if (grid.getSelectionModel() instanceof MultiSelectionModel)
		{
    		String rdo = null;
    		ArrayList<MapeoMovimientosSilicie> vector=null;
    		consultaDeclaracionSilicieServer cdss = new consultaDeclaracionSilicieServer(CurrentUser.get());
    		/*
    		 * Procesamos los registros para generar el dichero de anulacion
    		 * Guardamos los datos en la tabla nueva
    		 */
    		if (((MovimientosSilicieGrid) grid).getSelectedRows()!=null && ((MovimientosSilicieGrid) grid).getSelectedRows().size()!=0)
    		{
    			Iterator it = null;
    			
    			vector = new ArrayList<MapeoMovimientosSilicie>();
    			it = ((MovimientosSilicieGrid) grid).getSelectedRows().iterator();
    			
    			while (it.hasNext())
    			{		
    				MapeoMovimientosSilicie mapeo = (MapeoMovimientosSilicie) it.next();
    				rdo = cdss.guardarNuevoAnulados(mapeo);
    				if (rdo!=null)
    				{
    					Notificaciones.getInstance().mensajeError(rdo);
    					break;
    				}
    				else
    				{
    					vector.add(mapeo);
    				}
    			}
    		}
    		/*
    		 * dejamos el registro desvalidado y desdeclarado
    		 */
    		if (rdo==null)
    		{
    			rdo = cdss.actualizarMovimientosAnulados(vector);
    		}
    		else
    		{
    			Notificaciones.getInstance().mensajeError("Problemas al marcar los asientos a anular.");
    		}
    		
    		grid.setSelectionMode(SelectionMode.SINGLE);    		
    		if (form!=null) form.cerrar();
    		
    		if (rdo==null)
    		{
    			if (isHayGrid())
    			{			
    				grid.removeAllColumns();			
    				barAndGridLayout.removeComponent(grid);
    				grid=null;		
    				setHayGrid(false);
    			}
    			generarGrid(opcionesEscogidas);
    		}
		}
    }
    
    private void anular()
    {   
    	this.activarBotones(false);
    	
		ejercicioDeclaracion = new Integer(txtEjercicio.getValue());
		mesDeclaracion = new Integer(cmbMes.getValue().toString());
		titular = cmbCaeTitular.getValue().toString().trim();
		
		try 
		{
			fechaDeclaracion = RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue()));
			this.vt = new VentanaAceptarCancelar(this, "Quieres anular la declaracion de los movimientos para el ejercicio, mes y almacen seleccionados?", "Anular", "Cancelar", "Anular", "CancelarAnulacion");
			getUI().addWindow(vt);
		} 
		catch (Exception e) 
		{
			Notificaciones.getInstance().mensajeAdvertencia("Tienes que rellenar la fecha de la declaración a anular");
		}
		
    	this.botonesGenerales(true);
		this.navegacion(true);
		this.verBotones(true);
		this.activarBotones(true);
    }

    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);    		
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
    	if (this.grid==null || ((MovimientosSilicieGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((MovimientosSilicieGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((MovimientosSilicieGrid) this.grid).activadaVentanaPeticion && !((MovimientosSilicieGrid) this.grid).ordenando)
    	{
    		this.modificando=true;
    		this.verForm(false);
    		this.form.editarMovimientosSilicie((MapeoMovimientosSilicie) r_fila);
    	}    		
    }
    
    public void generarGrid(MapeoMovimientosSilicie r_mapeo)
    {
    	ArrayList<MapeoMovimientosSilicie> r_vector=null;
    	r_vector=this.cmss.datosMovimientosSilicieGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoMovimientosSilicie> r_vector)
    {
    	if (isHayGrid() && r_vector!=null)
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;
			setHayGrid(false);
		}
    	
    	this.grid = new MovimientosSilicieGrid(this,r_vector);
    	if (((MovimientosSilicieGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(true);
    		this.activarBotones(true);
    	}
    	else if (((MovimientosSilicieGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(true);
    		this.activarBotones(true);
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    		((MovimientosSilicieGrid) grid).establecerFiltros(filtrosRec);
    	}
    	
    	activarBotonesAccion();
    }
    
    private void cargarListeners()
    {
    	this.opcGenerar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				generacion();
			}
		});

    	this.opcVerificar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verificar();
			}
		});

    	this.opcValidar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				accion="Validar";
				cargar();
			}
		});

    	this.opcAeat.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				accion="Chequear";
				cargar();
			}
		});

    	this.opcAnular.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				anular();
			}
		});

    	this.opcRectificar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				anularAsientos();
			}
		});

    	this.opcDeclarar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (txtEjercicio.getValue()!=null)
				{
					declarar();
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Selecciona correctamente ejercicio y mes para la generacion de los datos.");
				}
			}
		});

    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				opcionesEscogidas.put("caeTitular", cmbCaeTitular.getValue().toString());
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				opcionesEscogidas.put("mes", cmbMes.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
			}
		});
    	
		this.cmbCaeTitular.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((MovimientosSilicieGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoMovimientosSilicie mapeo = new MapeoMovimientosSilicie();
				
				if (cmbCaeTitular.getValue()!=null) mapeo.setCaeTitular(cmbCaeTitular.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				if (cmbMes.getValue()!=null && cmbMes.getValue()!="Todos") mapeo.setMes(new Integer(cmbMes.getValue().toString()));
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				
				if (cmbEstado.getValue()!=null && (cmbEstado.getValue().equals("Declarados") || cmbEstado.getValue().equals("Validados")))
				{
					try 
					{
						if (fechaPresentacion.getValue()!=null) mapeo.setFechaPresentacion(fechaPresentacion.getValue());
						opcionesEscogidas.put("fechaPresentacion", RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue())));
					} 
					catch (Exception e) 
					{
					}
				}
				
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				
				if (!cmbCaeTitular.getValue().toString().equals("Todos"))
				{
					almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeTitular.getValue().toString());
					numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeTitular.getValue().toString());
				}
				else
				{
					almacenSeleccionado = "Todos";
				}
				
				
				tituloGrid = "Movimientos Generados en " + almacenSeleccionado;
				
				opcionesEscogidas.put("caeTitular", cmbCaeTitular.getValue().toString());
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				opcionesEscogidas.put("mes", cmbMes.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				
				generarGrid(mapeo);
				
			}
		});
		this.cmbMes.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (isHayGrid())
				{			
					filtrosRec = ((MovimientosSilicieGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoMovimientosSilicie mapeo = new MapeoMovimientosSilicie();
				
				if (cmbCaeTitular.getValue()!=null) mapeo.setCaeTitular(cmbCaeTitular.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				if (cmbMes.getValue()!=null && cmbMes.getValue()!="Todos") mapeo.setMes(new Integer(cmbMes.getValue().toString()));
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				
				opcionesEscogidas.put("caeTitular", cmbCaeTitular.getValue().toString());
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				opcionesEscogidas.put("mes", cmbMes.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				
				if (cmbEstado.getValue()!=null && (cmbEstado.getValue().equals("Declarados")||cmbEstado.getValue().equals("Validados")))
				{
					try 
					{
						if (fechaPresentacion.getValue()!=null) mapeo.setFechaPresentacion(fechaPresentacion.getValue());
						opcionesEscogidas.put("fechaPresentacion", RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue())));
					} 
					catch (Exception e) 
					{
					}
				}
				generarGrid(mapeo);
			}
		});
		
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((MovimientosSilicieGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoMovimientosSilicie mapeo = new MapeoMovimientosSilicie();
				if (cmbCaeTitular.getValue()!=null) mapeo.setCaeTitular(cmbCaeTitular.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				if (cmbMes.getValue()!=null && cmbMes.getValue()!="Todos") mapeo.setMes(new Integer(cmbMes.getValue().toString()));
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				
				opcionesEscogidas.put("caeTitular", cmbCaeTitular.getValue().toString());
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				opcionesEscogidas.put("mes", cmbMes.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				
				
				activarBotonesAccion();
				
				if (cmbEstado.getValue()!=null && (cmbEstado.getValue().equals("Declarados") || cmbEstado.getValue().equals("Validados")))
				{
					fechaPresentacion.setVisible(true);
				}				
				else
				{
					fechaPresentacion.setValue(null);
					fechaPresentacion.setVisible(false);
				}
				
				try 
				{
					if (fechaPresentacion.getValue()!=null)
					{
						mapeo.setFechaPresentacion(fechaPresentacion.getValue());
						opcionesEscogidas.put("fechaPresentacion", RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue())));
					}
				} 
				catch (Exception e) 
				{
				}
				generarGrid(mapeo);
			}
		});

		this.fechaPresentacion.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((MovimientosSilicieGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoMovimientosSilicie mapeo = new MapeoMovimientosSilicie();
				if (cmbCaeTitular.getValue()!=null) mapeo.setCaeTitular(cmbCaeTitular.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				if (cmbMes.getValue()!=null && cmbMes.getValue()!="Todos") mapeo.setMes(new Integer(cmbMes.getValue().toString()));
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				
				opcionesEscogidas.put("caeTitular", cmbCaeTitular.getValue().toString());
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				opcionesEscogidas.put("mes", cmbMes.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				
				if (cmbEstado.getValue()!=null && (cmbEstado.getValue().equals("Declarados") || cmbEstado.getValue().equals("Validados")))
				{
					try 
					{
						if (fechaPresentacion.getValue()!=null) mapeo.setFechaPresentacion(fechaPresentacion.getValue());
						opcionesEscogidas.put("fechaPresentacion", RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(fechaPresentacion.getValue())));
					} 
					catch (Exception e) 
					{
					}
				}				
				generarGrid(mapeo);
			}
		});

    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				imprimir();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 */
		this.verBotones(true);
		this.activarBotones(true);
		this.navegacion(true);
		this.activarBotonesAccion();
		
		this.opcNuevo.setVisible(true);
		this.opcNuevo.setEnabled(true);
		setSoloConsulta(this.soloConsulta);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcImprimir.setEnabled(r_activo);
		this.opcVerificar.setEnabled(r_activo);
	}

	private void activarBotonesAccion()
	{
		if (this.grid!=null && ((MovimientosSilicieGrid) this.grid).vector==null && ((MovimientosSilicieGrid) this.grid).vector.isEmpty())
		{
    		this.opcDeclarar.setEnabled(false);
    		this.opcValidar.setEnabled(false);
    		this.opcAeat.setEnabled(false);
    		this.opcVerificar.setEnabled(false);
    		this.opcAnular.setEnabled(false);
    		this.opcRectificar.setEnabled(false);
		}
		else
		{
			switch (cmbEstado.getValue().toString())
			{
				case "Pendientes":
				{
					this.opcGenerar.setEnabled(true);
		    		this.opcDeclarar.setEnabled(true);
		    		this.opcValidar.setEnabled(false);
		    		this.opcVerificar.setEnabled(true);
		    		this.opcAeat.setEnabled(true);
		    		this.opcAnular.setEnabled(false);
		    		this.opcRectificar.setEnabled(false);
					break;
				}
				case "Anulados":
				{
					this.opcGenerar.setEnabled(false);
		    		this.opcDeclarar.setEnabled(true);
		    		this.opcValidar.setEnabled(true);
		    		this.opcVerificar.setEnabled(false);
		    		this.opcAeat.setEnabled(false);
		    		this.opcAnular.setEnabled(false);
		    		this.opcRectificar.setEnabled(false);
					break;
				}
				case "Declarados":
				{
					this.opcGenerar.setEnabled(true);
		    		this.opcDeclarar.setEnabled(true);
		    		this.opcValidar.setEnabled(true);
		    		this.opcVerificar.setEnabled(true);
		    		this.opcAeat.setEnabled(true);
		    		this.opcAnular.setEnabled(true);
		    		this.opcRectificar.setEnabled(false);
					break;
				}
				case "Validados":
				{
					this.opcGenerar.setEnabled(false);
		    		this.opcDeclarar.setEnabled(false);
		    		this.opcValidar.setEnabled(false);
		    		this.opcVerificar.setEnabled(true);
		    		this.opcAeat.setEnabled(true);
		    		this.opcAnular.setEnabled(false);
		    		this.opcRectificar.setEnabled(true);
					break;
				}
				case "Todos":
				{
					this.opcGenerar.setEnabled(false);
		    		this.opcDeclarar.setEnabled(false);
		    		this.opcValidar.setEnabled(false);
		    		this.opcAeat.setEnabled(false);
		    		this.opcVerificar.setEnabled(false);
		    		this.opcAnular.setEnabled(false);
		    		this.opcRectificar.setEnabled(false);
					break;
				}
			}
		}
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcBuscar.setVisible(false);
		this.opcImprimir.setVisible(r_visibles);
	}
	
	private void imprimir()
	{
    	/*
    	 * llamar a pantalla de peticion formatos impresion
    	 */
//    	Notificaciones.getInstance().mensajeAdvertencia("Pendiente de implementar");
    	
    	PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion("MovimientosSilicie", txtEjercicio.getValue(),cmbMes.getValue().toString(), cmbCaeTitular.getValue().toString());
		getUI().addWindow(vtPeticion);
    	
    	this.botonesGenerales(true);		
	}
	
	private void verificar()
	{
		Boolean soloHayApertura = false;
		String mes = null;
		
		
		consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
		/*
		 * vamos a comprobar, si el mes es ENERO, que haya movimientos distintos al de apertura.
		 * Si solo hay apertura, pondremos mes = 0
		 * 
		 * En la generacion de los datos al ver mes 0 cogeremos para comparar el campo unid_inicio 
		 * de la tabla de existencias de greensys
		 * 
		 * En caso contrario compararemos como en la actualidad.
		 */
		if (this.cmbMes.getValue().toString().contentEquals("01"))
			soloHayApertura = cmss.comprobarSoloApertura(this.txtEjercicio.getValue(), this.cmbMes.getValue().toString(), this.cmbCaeTitular.getValue().toString());
		else
			soloHayApertura=false;
		
		if (soloHayApertura)
			mes="0";
		else
			mes = this.cmbMes.getValue().toString();
		
		Integer id = cmss.generacionDatosTemporalExistencias("Verificacion", this.txtEjercicio.getValue(),this.cmbMes.getValue().toString(), cmbCaeTitular.getValue().toString().trim(), null);
		cmss.eliminarVerificadosOk(id,this.txtEjercicio.getValue(),mes, cmbCaeTitular.getValue().toString().trim());
		String pdfGenerado  = cmss.imprimirGeneracion(this.txtEjercicio.getValue(),this.cmbMes.getValue().toString(), "ChequeoInventario", cmbCaeTitular.getValue().toString(), id, null, null, null);
		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
	}
	
    public void print() 
    {

    }
	
	private void establecerTableExportable()
	{
		
	}
	
	private void cargarCombo()
	{
		this.cmbMes.removeAllItems();
		for (int i = 1; i<=12; i++)
		{
			this.cmbMes.addItem(RutinasNumericas.formatearIntegerDigitosIzqda(i, 2));
		}
		this.cmbMes.addItem("Todos");
		this.cmbMes.setValue(RutinasFechas.mesActualMM());
		
		this.cmbEstado.removeAllItems();
		
		this.cmbEstado.addItem("Pendientes");
		this.cmbEstado.addItem("Anulados");
		this.cmbEstado.addItem("Declarados");
		this.cmbEstado.addItem("Validados");
		this.cmbEstado.addItem("Todos");
		this.cmbEstado.setValue("Todos");
		
		opcionesEscogidas.put("estado", cmbEstado.getValue().toString());

		this.cmbCaeTitular.removeAllItems();
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		String almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			if (caes.get(i)!=null && caes.get(i).length()>0) this.cmbCaeTitular.addItem(caes.get(i));
		}
		
		this.cmbCaeTitular.addItem("Todos");
		
		String caeDefecto = cas.obtenerCAEAlmacen(almacenDefecto);
		
		if (caeDefecto!=null)
		{
			
			this.cmbCaeTitular.setValue(caeDefecto.trim());
			this.numeroAlmacen = cas.obtenerAlmacenPorCAE(this.cmbCaeTitular.getValue().toString());
			this.almacenSeleccionado = cas.obtenerDescripcionAlmacen(this.numeroAlmacen);
			this.tituloGrid = this.tituloGrid + " " + this.almacenSeleccionado; 
			this.opcionesEscogidas.put("caeTitular", this.cmbCaeTitular.getValue().toString());
		}
	}
	
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;
			this.setHayGrid(false);
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
		switch (r_accion)
		{
			case "XML":
			{
//				FileResource ruta = RutinasFicheros.generarCsv("silicie");
				
//				if (ruta!=null)
//				{
//					Notificaciones.getInstance().mensajeAdvertencia("Declaración generada. " + ruta.getSourceFile().getAbsolutePath());
//				}
//				else
				{
					Notificaciones.getInstance().mensajeError("Pendiente de implementar la presentacion por xml.");
				}
				
				if (this.vt.isVisible()) this.vt.close();
				break;
			}

			case "Anular":
			{
				if (cmbMes.getValue().toString().contentEquals("01"))
				{
					this.vt = new VentanaAceptarCancelar(this, "Quieres anular la declaracion de los movimientos de apertura también? ", "Si", "No", "AnularApertura", "NoAnulacionApertura");
					getUI().addWindow(vt);
				}
				else
				{
					borrarTabla(titular, ejercicioDeclaracion, mesDeclaracion,fechaDeclaracion, false);
				}
				if (this.vt.isVisible()) this.vt.close();
				this.cerrarVentanaGeneracion();
				break;
			}
			case "AnularApertura":
			{
				borrarTabla(titular, ejercicioDeclaracion, mesDeclaracion,fechaDeclaracion,true);
				if (this.vt.isVisible()) this.vt.close();
				this.cerrarVentanaGeneracion();
				break;
			}
			case "NoAnularApertura":
			{
				borrarTabla(titular, ejercicioDeclaracion, mesDeclaracion,fechaDeclaracion,false);
				if (this.vt.isVisible()) this.vt.close();
				this.cerrarVentanaGeneracion();
				break;
			}
			case "Eliminar":
			{
				((MovimientosSilicieGrid) this.grid).remove(mapeoMovimientosSilicieEnlazado);
				cmss.eliminar(mapeoMovimientosSilicieEnlazado);
				this.form.cerrar();
				break;
			}
		}
	}

	public void cerrarVentanaGeneracion()
	{
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;		
			this.setHayGrid(false);
		}
		this.generarGrid(this.opcionesEscogidas);
	}

	@Override
	public void cancelarProceso(String r_accion) 
	{
		String ruta = null;
		if (r_accion == null) r_accion = "";
		
		switch (r_accion)
		{
			case "CSV":
			{
				if (mesDeclaracion==null) mesDeclaracion=0;
				if (cmbEstado.getValue().toString().equals("Anulados"))
				{
					ruta = RutinasFicheros.generarCsv("anulacionSilicie"+cmbCaeTitular.getValue().toString(), ejercicioDeclaracion, mesDeclaracion,rowCSVLimit, null);
				}
				else
				{
					
					ruta = RutinasFicheros.generarCsv("silicie"+cmbCaeTitular.getValue().toString(), ejercicioDeclaracion, mesDeclaracion,rowCSVLimit,null);
					
				}
				if (ruta!=null)
				{
					Notificaciones.getInstance().mensajeAdvertencia("Declaración generada. " + ruta);
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Problemas en la generacion del fichero.");
				}
				
				ruta=null;
				if (this.vt.isVisible()) this.vt.close();
				break;
			}
			default:
			{
				if (this.form!=null) this.form.cerrar();
				this.cerrarVentanaGeneracion();
			}
		}

	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void botonesGenerales(boolean r_activo)
	{
		this.opcBuscar.setEnabled(r_activo);
		this.opcSalir.setEnabled(r_activo);
		this.opcNuevo.setEnabled(r_activo);
		this.opcImprimir.setEnabled(r_activo);
		this.opcGenerar.setEnabled(r_activo);
		this.opcVerificar.setEnabled(r_activo);
		
		if ((MovimientosSilicieGrid) grid != null && ((MovimientosSilicieGrid) grid).vector!=null && !((MovimientosSilicieGrid) grid).vector.isEmpty() && cmbEstado.getValue()!=null) opcDeclarar.setEnabled(cmbEstado.getValue().toString().equals("Pendientes")||cmbEstado.getValue().toString().equals("Declarados"));
		if ((MovimientosSilicieGrid) grid != null && ((MovimientosSilicieGrid) grid).vector!=null && !((MovimientosSilicieGrid) grid).vector.isEmpty() && cmbEstado.getValue()!=null) opcValidar.setEnabled(cmbEstado.getValue().toString().equals("Declarados"));
		if ((MovimientosSilicieGrid) grid != null && ((MovimientosSilicieGrid) grid).vector!=null && !((MovimientosSilicieGrid) grid).vector.isEmpty() && cmbEstado.getValue()!=null) opcAnular.setEnabled(cmbEstado.getValue().toString().equals("Declarados"));

	}

	
	private void generarTabla(String r_titular, Integer r_ejercicio, Integer r_mes, String r_fecha)
	{
		String rdo = null;
		
		if (cmbEstado.getValue().toString().equals("Pendientes")||cmbEstado.getValue().toString().equals("Declarados")||cmbEstado.getValue().toString().equals("Anulados"))
		{
			rdo = cmss.generacionMovimientosDeclaracionSilicielobal(r_titular, r_ejercicio,r_mes,cmbEstado.getValue().toString(), r_fecha);
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Rango ya declarado");
		}
		
		if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
	}
	
	private void borrarTabla(String r_titular, Integer r_ejercicio, Integer r_mes, String r_fecha, boolean r_apertura)
	{
		String rdo = null;
		
		rdo = cmss.eliminarMovimientosDeclaracionSilicielobal(r_titular, r_ejercicio,r_mes, r_fecha, r_apertura);
		
		if (rdo!=null && rdo.trim().length()>0)
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
}
