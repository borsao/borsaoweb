package borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoCodigosNC mapeo = null;
	 
	 public MapeoCodigosNC convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoCodigosNC();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoCodigosNC convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoCodigosNC();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoCodigosNC r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("codigo", r_mapeo.getCodigo());		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 return hash;		 
	 }
}