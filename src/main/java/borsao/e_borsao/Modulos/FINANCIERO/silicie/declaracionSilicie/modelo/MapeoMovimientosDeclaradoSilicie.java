package borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoMovimientosDeclaradoSilicie extends MapeoGlobal
{
	
	private String documentoFalso;
	private String doc;
	
	private String numeroAsiento;
	private String numeroAsientoPrevio;
	private String tipoAsiento;
	
	private String numeroRefInterno;
	private Integer idMovimiento;
	
	private String mes;
//	private String esc;
//	private String idPrg;

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public MapeoMovimientosDeclaradoSilicie()
	{
		this.setDoc("");
	}

	public String getDocumentoFalso() {
		return documentoFalso;
	}

	public void setDocumentoFalso(String documentoFalso) {
		this.documentoFalso = documentoFalso;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(String numeroAsiento) {
		this.numeroAsiento = numeroAsiento;
	}

	public String getTipoAsiento() {
		return tipoAsiento;
	}

	public void setTipoAsiento(String tipoAsiento) {
		this.tipoAsiento = tipoAsiento;
	}

	public String getNumeroRefInterno() {
		return numeroRefInterno;
	}

	public void setNumeroRefInterno(String numeroRefInterno) {
		this.numeroRefInterno = numeroRefInterno;
	}

	public Integer getIdMovimiento() {
		return idMovimiento;
	}

	public void setIdMovimiento(Integer idMovimiento) {
		this.idMovimiento = idMovimiento;
	}

	public String getNumeroAsientoPrevio() {
		return numeroAsientoPrevio;
	}

	public void setNumeroAsientoPrevio(String numeroAsientoPrevio) {
		this.numeroAsientoPrevio = numeroAsientoPrevio;
	}

}


