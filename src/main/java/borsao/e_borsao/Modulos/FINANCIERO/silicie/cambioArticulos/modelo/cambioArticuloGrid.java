package borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.view.cambioArticulosView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class cambioArticuloGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private cambioArticulosView app = null;
	
    public cambioArticuloGrid(cambioArticulosView r_app, ArrayList<MapeoCambioArticulo> r_vector) {
        
        this.vector=r_vector;
        this.app = r_app;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Cambio Articulo");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoCambioArticulo.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo", "descripcion", "lote","nuevoNC", "nuevoArticulo", "nuevaDescripcion");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("lote").setHeaderCaption("Lote");
    	this.getColumn("nuevoNC").setHeaderCaption("Nuevo NC");
    	this.getColumn("nuevaDescripcion").setHeaderCaption("Nueva Desc.");
    	this.getColumn("nuevoArticulo").setHeaderCaption("Nuevo Art.");
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	public void calcularTotal() 
	{
		//"","","litros", "reservado", "prepedido", "stock_real"
//		Double totalLitros = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
//    	Indexed indexed = this.getContainerDataSource();
//        List<?> list = new ArrayList<Object>(indexed.getItemIds());
//        for(Object itemId : list)
//        {
//        	Item item = indexed.getItem(itemId);
//        	if (item!=null)
//        	{
//	        	Double q1Value = (Double) item.getItemProperty("litros").getValue();
//	        	String q1Sig = (String) item.getItemProperty("signo").getValue();
//	        	if (q1Value!=null && q1Sig!= null && q1Sig.equals("+")) totalLitros += q1Value;
//	        	if (q1Value!=null && q1Sig!= null && q1Sig.equals("-")) totalLitros -= q1Value;
//        	}
//        }
//        
//        footer.getCell("mes").setText("Totales");
//		footer.getCell("litros").setText(RutinasNumericas.formatearDouble(totalLitros).toString());
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
//		footer.getCell("litros").setStyleName("Rcell-pie");
	}}
