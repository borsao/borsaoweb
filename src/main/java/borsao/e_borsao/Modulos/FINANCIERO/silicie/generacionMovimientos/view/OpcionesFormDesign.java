package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view;

import com.vaadin.annotations.AutoGenerated;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;

/** 
 * !! DO NOT EDIT THIS FILE !!
 * 
 * This class is generated by Vaadin Designer and will be overwritten.
 * 
 * Please make a subclass with logic and additional interfaces as needed,
 * e.g class LoginView extends LoginDesign implements View { … }
 */
@DesignRoot
@AutoGenerated
@SuppressWarnings("serial")
public class OpcionesFormDesign extends CssLayout 
{
	protected ComboBox caeTitular;
	protected TextField ejercicio;
	protected TextField mes;
	protected DateField fechaMovimiento;
	protected DateField fechaRegistro;
	
	protected ComboBox descripcionRegimenFiscal;
	protected ComboBox descripcionOperacion;
	protected ComboBox descripcionJustificante;
	protected ComboBox descripcionDocumentoOd;
	protected ComboBox descripcionDiferenciaMenos;
	
	protected TextField descripcionMovimiento;
	protected TextField numeroJustificante;
	protected TextField codigoNC;
	protected TextField descripcion;
	protected TextField codigoArticulo;
	protected TextField descripcionArticulo;
	
	protected TextField litros;
	protected TextField grado;
	
	protected TextField cae_od;
	protected TextField numeroDocumentoIdOD;
	protected TextField razonSocialOD;
	
    protected Button btnGuardar = null;
	protected Button btnCancel= null;
	protected Button btnEliminar= null;

	protected Button btnArticulo= null;
	protected Button btnArticuloG= null;
	protected Button btnMovimiento= null;
	
    public OpcionesFormDesign() {
        Design.read(this);
    }
}