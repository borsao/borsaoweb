package borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.modelo.MapeoTiposMovimiento;

public class consultaTiposMovimientoServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTiposMovimientoServer instance;
	
	public consultaTiposMovimientoServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTiposMovimientoServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTiposMovimientoServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoTiposMovimiento> datosTiposMovimientoGlobal(MapeoTiposMovimiento r_mapeo)
	{
		ResultSet rsTiposMovimiento = null;		
		ArrayList<MapeoTiposMovimiento> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_mov.id mov_id, ");
		cadenaSQL.append(" fin_silicie_mov.codigo mov_cod, ");
		cadenaSQL.append(" fin_silicie_mov.descripcion mov_des, ");
		cadenaSQL.append(" fin_silicie_mov.signo mov_sig, ");
		cadenaSQL.append(" fin_silicie_mov.silicie_sn mov_sil ");
     	cadenaSQL.append(" FROM fin_silicie_mov ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getSigno()!=null && r_mapeo.getSigno().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" signo = '" + r_mapeo.getSigno() + "' ");
				}
				if (r_mapeo.getSilicie()!=null && r_mapeo.getSilicie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" silicie_sn = '" + r_mapeo.getSilicie() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsTiposMovimiento.TYPE_SCROLL_SENSITIVE,rsTiposMovimiento.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsTiposMovimiento= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoTiposMovimiento>();
			
			while(rsTiposMovimiento.next())
			{
				MapeoTiposMovimiento mapeoTiposMovimiento = new MapeoTiposMovimiento();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTiposMovimiento.setIdCodigo(rsTiposMovimiento.getInt("mov_id"));
				mapeoTiposMovimiento.setCodigo(rsTiposMovimiento.getString("mov_cod"));
				mapeoTiposMovimiento.setDescripcion(rsTiposMovimiento.getString("mov_des"));
				mapeoTiposMovimiento.setSigno(rsTiposMovimiento.getString("mov_sig"));
				mapeoTiposMovimiento.setSilicie(rsTiposMovimiento.getString("mov_sil"));
				vector.add(mapeoTiposMovimiento);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsTiposMovimiento = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_mov.id) mov_sig ");
     	cadenaSQL.append(" FROM fin_silicie_mov ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsTiposMovimiento.TYPE_SCROLL_SENSITIVE,rsTiposMovimiento.CONCUR_UPDATABLE);
			rsTiposMovimiento= cs.executeQuery(cadenaSQL.toString());
			
			while(rsTiposMovimiento.next())
			{
				return rsTiposMovimiento.getInt("mov_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoTiposMovimiento r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_mov ( ");
    		cadenaSQL.append(" fin_silicie_mov.id, ");
    		cadenaSQL.append(" fin_silicie_mov.codigo, ");
    		cadenaSQL.append(" fin_silicie_mov.descripcion, ");
	        cadenaSQL.append(" fin_silicie_mov.signo, ");
		    cadenaSQL.append(" fin_silicie_mov.silicie_sn ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSigno()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSigno());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getSilicie()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getSilicie());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoTiposMovimiento r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_mov set ");
			cadenaSQL.append(" fin_silicie_mov.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_mov.descripcion=?, ");
		    cadenaSQL.append(" fin_silicie_mov.signo=?, ");
		    cadenaSQL.append(" fin_silicie_mov.silicie_sn=? ");
		    cadenaSQL.append(" WHERE fin_silicie_mov.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setString(3, r_mapeo.getSigno());
		    preparedStatement.setString(4, r_mapeo.getSilicie());
		    preparedStatement.setInt(5, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoTiposMovimiento r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_mov ");            
			cadenaSQL.append(" WHERE fin_silicie_mov.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select codigo, descripcion from fin_silicie_mov order by codigo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("codigo"));
				mA.setDescripcion(rsArticulos.getString("descripcion"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

}