package borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo.MapeoCambioArticulo;

public class consultaCambioArticulosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCambioArticulosServer instance;
	
	public consultaCambioArticulosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCambioArticulosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCambioArticulosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoCambioArticulo> datosCambioArticuloGlobal(MapeoCambioArticulo r_mapeo)
	{
		ResultSet rsCambioArticulo = null;		
		ArrayList<MapeoCambioArticulo> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_sil_tune_art.idCodigo ca_id, ");
		cadenaSQL.append(" fin_sil_tune_art.articulo ca_art, ");
		cadenaSQL.append(" fin_sil_tune_art.descripcion ca_des, ");
		cadenaSQL.append(" fin_sil_tune_art.nuevoNC ca_nnc, ");
		cadenaSQL.append(" fin_sil_tune_art.nuevoArticulo ca_nar, ");
		cadenaSQL.append(" fin_sil_tune_art.nuevaDescripcion ca_nde, ");
		cadenaSQL.append(" fin_sil_tune_art.lote ca_lot ");
     	cadenaSQL.append(" FROM fin_sil_tune_art ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" Descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getNuevoNC()!=null && r_mapeo.getNuevoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" NuevoNC = '" + r_mapeo.getNuevoNC() + "' ");
				}
				if (r_mapeo.getNuevoArticulo()!=null && r_mapeo.getNuevoArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" NuevoArticulo = '" + r_mapeo.getNuevoArticulo() + "' ");
				}
				if (r_mapeo.getNuevaDescripcion()!=null && r_mapeo.getNuevaDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" NuevaDescripcion = '" + r_mapeo.getNuevaDescripcion() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" Lote = '" + r_mapeo.getLote() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idCodigo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsCambioArticulo.TYPE_SCROLL_SENSITIVE,rsCambioArticulo.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsCambioArticulo= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoCambioArticulo>();
			
			while(rsCambioArticulo.next())
			{
				MapeoCambioArticulo mapeoCambioArticulo = new MapeoCambioArticulo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoCambioArticulo.setIdCodigo(rsCambioArticulo.getInt("ca_id"));
				mapeoCambioArticulo.setArticulo(rsCambioArticulo.getString("ca_art"));
				mapeoCambioArticulo.setDescripcion(rsCambioArticulo.getString("ca_des"));
				mapeoCambioArticulo.setNuevoNC(rsCambioArticulo.getString("ca_nnc"));
				mapeoCambioArticulo.setNuevoArticulo(rsCambioArticulo.getString("ca_nar"));
				mapeoCambioArticulo.setNuevaDescripcion(rsCambioArticulo.getString("ca_nde"));
				mapeoCambioArticulo.setLote(rsCambioArticulo.getString("ca_lot"));
				vector.add(mapeoCambioArticulo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsCambioArticulo = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_sil_tune_art.idCodigo) ca_sig ");
     	cadenaSQL.append(" FROM fin_sil_tune_art ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsCambioArticulo.TYPE_SCROLL_SENSITIVE,rsCambioArticulo.CONCUR_UPDATABLE);
			rsCambioArticulo= cs.executeQuery(cadenaSQL.toString());
			
			while(rsCambioArticulo.next())
			{
				return rsCambioArticulo.getInt("ca_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public Integer obtenerId(String r_titular)
	{
		ResultSet rsCambioArticulo = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT fin_sil_tune_art.idCodigo ca_id ");
     	cadenaSQL.append(" FROM fin_sil_tune_art ");
     	cadenaSQL.append(" WHERE fin_sil_tune_art.cae ='" + r_titular + "' " );
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsCambioArticulo.TYPE_SCROLL_SENSITIVE,rsCambioArticulo.CONCUR_UPDATABLE);
			rsCambioArticulo= cs.executeQuery(cadenaSQL.toString());
			
			while(rsCambioArticulo.next())
			{
				return rsCambioArticulo.getInt("ca_id") ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String guardarNuevo(MapeoCambioArticulo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_sil_tune_art ( ");
    		cadenaSQL.append(" fin_sil_tune_art.idCodigo, ");
    		cadenaSQL.append(" fin_sil_tune_art.articulo, ");
    		cadenaSQL.append(" fin_sil_tune_art.descripcion, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevoArticulo, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevaDescripcion, ");
		    cadenaSQL.append(" fin_sil_tune_art.lote, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevoNC) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getNuevoArticulo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getNuevoArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getNuevaDescripcion()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getNuevaDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getNuevoNC()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getNuevoNC());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoCambioArticulo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_sil_tune_art set ");
			cadenaSQL.append(" fin_sil_tune_art.articulo=?, ");
		    cadenaSQL.append(" fin_sil_tune_art.descripcion=?, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevoArticulo=?, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevaDescripcion=?, ");
		    cadenaSQL.append(" fin_sil_tune_art.lote=?, ");
		    cadenaSQL.append(" fin_sil_tune_art.nuevoNC=? ");
		    cadenaSQL.append(" WHERE fin_sil_tune_art.idCodigo = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getNuevoArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getNuevoArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getNuevaDescripcion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getNuevaDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getNuevoNC()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getNuevoNC());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoCambioArticulo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_sil_tune_art ");            
			cadenaSQL.append(" WHERE fin_sil_tune_art.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}