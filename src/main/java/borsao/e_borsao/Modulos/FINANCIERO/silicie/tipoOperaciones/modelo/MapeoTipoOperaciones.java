package borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTipoOperaciones extends MapeoGlobal
{
	private String codigo;
	private String descripcion;

	public MapeoTipoOperaciones()
	{
		this.setCodigo("");
		this.setDescripcion("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}