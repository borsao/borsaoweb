package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.server.consultaCodigosNCServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.modelo.MapeoDiferenciasMenos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.server.consultaDiferenciasMenosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.modelo.MapeoDocumentosIdentificativos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.server.consultaDocumentosIdentificativosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MovimientosSilicieGrid;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.modelo.MapeoRegimenesFiscales;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.server.consultaRegimenesFiscalesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.modelo.MapeoTipoJustificantes;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.server.consultaTipoJustificantesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.modelo.MapeoTipoOperaciones;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.server.consultaTipoOperacionesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.server.consultaTiposMovimientoServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoMovimientosSilicie mapeo = null;
	 public String gradoNuevo = null;
	 public String litrosNuevo = null;

	 private ventanaAyuda vHelp  =null;
	 private consultaMovimientosSilicieServer cmss = null;
	 
	 private GridViewRefresh uw = null;
	 public MapeoMovimientosSilicie mapeoModificado = null;
	 public BeanFieldGroup<MapeoMovimientosSilicie> fieldGroup;
//	 private ArrayList<MapeoLineasAlbaranesCompras> vectorAlbaranes = null;
//	 private ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslados = null;
	 
	 public OpcionesForm(GridViewRefresh r_uw) {
		super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoMovimientosSilicie>(MapeoMovimientosSilicie.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fechaMovimiento.addStyleName("v-datefield-textfield");
        this.fechaMovimiento.setDateFormat("dd/MM/yyyy");
        this.fechaMovimiento.setShowISOWeekNumbers(true);
        this.fechaMovimiento.setResolution(Resolution.DAY);
        
        this.fechaRegistro.addStyleName("v-datefield-textfield");
        this.fechaRegistro.setDateFormat("dd/MM/yyyy");
        this.fechaRegistro.setShowISOWeekNumbers(true);
        this.fechaRegistro.setResolution(Resolution.DAY);
        
        this.btnArticulo.setCaption("Codigo NC");
        this.litros.setLocale(new Locale("es_ES"));
        this.grado.setLocale(new Locale("es_ES"));

        this.posicionarCampos(null);
    }   

	private void cargarValidators()
	{
//		this.grado.addValueChangeListener(new ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				// 
//				if (grado.getValue()!=null) gradoNuevo = grado.getValue().toString().replace(".", ",");	
//			}
//		});
//		this.litros.addValueChangeListener(new ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				// 
//				if (litros.getValue()!=null) litrosNuevo = litros.getValue().toString().replace(".", ",");	
//			}
//		});

		this.numeroJustificante.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (descripcionJustificante.getValue()!=null && descripcionJustificante.getValue().toString().contains("E-DA") &&  numeroJustificante.getValue()!=null && numeroJustificante.getValue().length()!=21) 
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. 21 Posiciones");
					numeroJustificante.focus();
				}
			}
		});
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoMovimientosSilicie) uw.grid.getSelectedRow();
                eliminarMovimientosSilicie(mapeo);
            }
        });  
        
        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes("Articulo");
            }
        });
        this.btnArticuloG.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes("ArticuloG");
            }
        });
        this.btnMovimiento.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes("Movimiento");
            }
        });
        
		this.codigoNC.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (codigoNC.getValue()!=null && codigoNC.getValue().length()>0)
				{
					consultaCodigosNCServer cncs  = new consultaCodigosNCServer(CurrentUser.get());
					String desc = cncs.obtenerDescripcionCodigoNC(codigoNC.getValue());
					descripcion.setValue(desc);
				}
			}
		});
		this.codigoArticulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (codigoArticulo.getValue()!=null && codigoArticulo.getValue().length()>0)
				{
					consultaArticulosServer cas  = new consultaArticulosServer(CurrentUser.get());
					String desc = cas.obtenerDescripcionArticulo(codigoArticulo.getValue());
					descripcionArticulo.setValue(desc);
					Double gr = cas.obtenerArticulosGrado(codigoArticulo.getValue());
					grado.setValue(gr.toString());
					String nc = cas.obtenerArticulosNC(codigoArticulo.getValue());
					codigoNC.setValue(nc);
					if (nc!=null)
					{
						consultaCodigosNCServer cncs  = new consultaCodigosNCServer(CurrentUser.get());
						desc = cncs.obtenerDescripcionCodigoNC(nc);
						descripcion.setValue(desc);
					}
					litros.focus();
				}
			}
		});
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				((MovimientosSilicieView) uw).modificando=false; 
	 				((MovimientosSilicieView) uw).opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 				uw.setHayGrid(false);
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				((MovimientosSilicieView) uw).modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);
		 				if (grado.getValue()!=null) mapeo.setGrado(Double.valueOf(grado.getValue().replace(",", "")));
		 				if (litros.getValue()!=null) mapeo.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		                crearMovimientosSilicie(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 			}
	 			else
	 			{
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoMovimientosSilicie mapeo_orig= (MapeoMovimientosSilicie) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				mapeo.setClaveTabla(mapeo_orig.getClaveTabla());
		 				mapeo.setSerie(mapeo_orig.getSerie());
		 				mapeo.setDocumento(mapeo_orig.getDocumento());
		 				mapeo.setDocumentoFalso(mapeo_orig.getDocumentoFalso());
		 				mapeo.setAlbaran(mapeo_orig.getAlbaran());
		 				mapeo.setPosicion(mapeo_orig.getPosicion());
		 				mapeo.setNumeroRefInterno(mapeo_orig.getNumeroRefInterno());
	 					mapeo.setGrado(Double.valueOf(grado.getValue().replace(",", "")));		 				
	 					mapeo.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		 				
		 				modificarMovimientosSilicie(mapeo,mapeo_orig);
		 				hm=null;
			           	removeStyleName("visible");
	 				}
	 			}
	 			if (((MovimientosSilicieGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 				uw.grid.setEnabled(true);
	 			}
 			}
 		});	
		
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		this.fechaMovimiento.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				fechaRegistro.setValue(fechaMovimiento.getValue());
			}
		});
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarMovimientosSilicie(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.cae_od.getValue()!=null && this.cae_od.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeOD", this.cae_od.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeOD", "");
		}
		if (this.caeTitular.getValue()!=null && this.caeTitular.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeTitular", this.caeTitular.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeTitular", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.mes.getValue()!=null && this.mes.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("mes", this.mes.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("mes", "");
		}

		if (this.fechaMovimiento.getValue()!=null && this.fechaMovimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fechaMovimiento.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.fechaRegistro.getValue()!=null && this.fechaRegistro.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaRegistro", RutinasFechas.convertirDateToString(this.fechaRegistro.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaRegistro", "");
		}
		if (this.numeroJustificante.getValue()!=null && this.numeroJustificante.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroJustificante", this.numeroJustificante.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroJustificante", "");
		}
		if (this.razonSocialOD.getValue()!=null && this.razonSocialOD.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("razonSocialOD", this.razonSocialOD.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("razonSocialOD", "");
		}
		if (this.numeroDocumentoIdOD.getValue()!=null && this.numeroDocumentoIdOD.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroDocumentoIdOD", this.numeroDocumentoIdOD.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroDocumentoIdOD", "");
		}
		if (this.descripcionMovimiento.getValue()!=null && this.descripcionMovimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionMovimiento", this.descripcionMovimiento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionMovimiento", "");
		}
		if (this.descripcionRegimenFiscal.getValue()!=null && this.descripcionRegimenFiscal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionRegimenFiscal", this.descripcionRegimenFiscal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionRegimenFiscal", "");
		}
		if (this.descripcionOperacion.getValue()!=null && this.descripcionOperacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionOperacion", this.descripcionOperacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionOperacion", "");
		}
		if (this.descripcionJustificante.getValue()!=null && this.descripcionJustificante.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionJustificante", this.descripcionJustificante.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionJustificante", "");
		}
		if (this.descripcionDocumentoOd.getValue()!=null && this.descripcionDocumentoOd.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionDocumentoOd", this.descripcionDocumentoOd.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionDocumentoOd", "");
		}
		if (this.descripcionDiferenciaMenos.getValue()!=null && this.descripcionDiferenciaMenos.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionDiferenciaMenos", this.descripcionDiferenciaMenos.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionDiferenciaMenos", "");
		}
		if (this.codigoNC.getValue()!=null && this.codigoNC.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoNC", this.codigoNC.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoNC", "");
		}
		if (this.codigoArticulo.getValue()!=null && this.codigoArticulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoArticulo", this.codigoArticulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoArticulo", "");
		}
		if (this.grado.getValue()!=null && this.grado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("grado", this.grado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("grado", "");
		}
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.descripcionArticulo.getValue()!=null && this.descripcionArticulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionArticulo", this.descripcionArticulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionArticulo", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.cae_od.getValue()!=null && this.cae_od.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeOD", this.cae_od.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeOD", "");
		}
		if (this.caeTitular.getValue()!=null && this.caeTitular.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeTitular", this.caeTitular.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeTitular", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.mes.getValue()!=null && this.mes.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("mes", this.mes.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("mes", "");
		}

		if (this.fechaMovimiento.getValue()!=null && this.fechaMovimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fechaMovimiento.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.fechaRegistro.getValue()!=null && this.fechaRegistro.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fechaRegistro", RutinasFechas.convertirDateToString(this.fechaRegistro.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fechaRegistro", "");
		}
		if (this.numeroJustificante.getValue()!=null && this.numeroJustificante.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroJustificante", this.numeroJustificante.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroJustificante", "");
		}
		if (this.razonSocialOD.getValue()!=null && this.razonSocialOD.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("razonSocialOD", this.razonSocialOD.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("razonSocialOD", "");
		}
		if (this.numeroDocumentoIdOD.getValue()!=null && this.numeroDocumentoIdOD.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroDocumentoIdOD", this.numeroDocumentoIdOD.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroDocumentoIdOD", "");
		}
		if (this.descripcionMovimiento.getValue()!=null && this.descripcionMovimiento.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionMovimiento", this.descripcionMovimiento.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionMovimiento", "");
		}
		if (this.descripcionRegimenFiscal.getValue()!=null && this.descripcionRegimenFiscal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionRegimenFiscal", this.descripcionRegimenFiscal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionRegimenFiscal", "");
		}
		if (this.descripcionOperacion.getValue()!=null && this.descripcionOperacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionOperacion", this.descripcionOperacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionOperacion", "");
		}
		if (this.codigoNC.getValue()!=null && this.codigoNC.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoNC", this.codigoNC.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoNC", "");
		}
		if (this.codigoArticulo.getValue()!=null && this.codigoArticulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoArticulo", this.codigoArticulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoArticulo", "");
		}
		if (this.grado.getValue()!=null && this.grado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("grado", this.grado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("grado", "");
		}
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.descripcionArticulo.getValue()!=null && this.descripcionArticulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionArticulo", this.descripcionArticulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionArticulo", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarMovimientosSilicie(null);
		}
	}
	
	public void editarMovimientosSilicie(MapeoMovimientosSilicie r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		Integer id = null;
		consultaContadoresEjercicioServer cont = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		consultaTipoJustificantesServer just = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
		
		cargarCombo();		
		posicionarCampos(r_mapeo);
		
		if (!isBusqueda())
		{
			/*
			 * establezco el numerador
			 */
			if (r_mapeo==null) 
			{
				r_mapeo=new MapeoMovimientosSilicie();				
				r_mapeo.setCaeTitular(((MovimientosSilicieView) uw).cmbCaeTitular.getValue().toString().trim());
				r_mapeo.setEjercicio(new Integer(((MovimientosSilicieView) uw).txtEjercicio.getValue().trim()));
				r_mapeo.setMes(new Integer(((MovimientosSilicieView) uw).cmbMes.getValue().toString().trim()));
				r_mapeo.setFechaMovimiento(new Date());
				r_mapeo.setFechaRegistro(new Date());
				r_mapeo.setNumeroJustificante(cont.recuperarContador(r_mapeo.getEjercicio(), "MANUAL SILICIE", ((MovimientosSilicieView) uw).cmbCaeTitular.getValue().toString().trim()).toString());

				MapeoTipoJustificantes mapJust = new MapeoTipoJustificantes();
				mapJust.setDescripcion("Otros");
				ArrayList<MapeoTipoJustificantes> vector = just.datosTipoJustificantesGlobal(mapJust);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	id = ((MapeoTipoJustificantes) vector.get(i)).getIdCodigo();
			    }

				r_mapeo.setIdCodigoJustificante(id);
				
				this.creacion=true;
			}
		}
		else
		{
			r_mapeo=new MapeoMovimientosSilicie();
		}
	
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoMovimientosSilicie>(r_mapeo));
//		((MovimientosSilicieView) uw).modificando=false;
		
		this.descripcionJustificante.setValue("Otros");
		desactivarCampos();

	}
	
	public void eliminarMovimientosSilicie(MapeoMovimientosSilicie r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		((MovimientosSilicieView) uw).mapeoMovimientosSilicieEnlazado=r_mapeo;
		
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(uw, "Seguro que deseas eliminar el registro?", "Si", "No", "Eliminar", null);			
		uw.getUI().addWindow(vt);
	}
	
	public void modificarMovimientosSilicie(MapeoMovimientosSilicie r_mapeo, MapeoMovimientosSilicie r_mapeo_orig)
	{
		String rdo = null;
				
		rdo = ((MovimientosSilicieView) uw).cmss.guardarCambios(r_mapeo,r_mapeo_orig);
		
		if (rdo==null)
		{
//			r_mapeo.setEstado(rdo);
		
			if (!((MovimientosSilicieGrid) uw.grid).vector.isEmpty())
			{
				
				ArrayList<MapeoMovimientosSilicie> r_vector = (ArrayList<MapeoMovimientosSilicie>) ((MovimientosSilicieGrid) uw.grid).vector;
				r_vector.remove(r_mapeo_orig);
				r_vector.add(r_mapeo);
				
				Indexed indexed = ((MovimientosSilicieGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	            uw.setHayGrid(false);
	            
	            ((MovimientosSilicieView) uw).presentarGrid(r_vector);

	            ((MovimientosSilicieGrid) uw.grid).setRecords(r_vector);
//	            ((MovimientosSilicieGrid) uw.grid).sort("numeroReferencia", SortDirection.DESCENDING);
				((MovimientosSilicieGrid) uw.grid).scrollTo(r_mapeo);
				((MovimientosSilicieGrid) uw.grid).select(r_mapeo);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		((MovimientosSilicieView) uw).modificando=false;
	}
	
	public void crearMovimientosSilicie(MapeoMovimientosSilicie r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		String rdo = null;
		rdo = ((MovimientosSilicieView) uw).cmss.guardarNuevo(r_mapeo);

		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoMovimientosSilicie>(r_mapeo));
			if (((MovimientosSilicieGrid) uw.grid)!=null )//&& )
			{
				if (!((MovimientosSilicieGrid) uw.grid).vector.isEmpty())
				{
					((ArrayList<MapeoMovimientosSilicie>) ((MovimientosSilicieGrid) uw.grid).vector).add(r_mapeo);
					((MovimientosSilicieGrid) uw.grid).setRecords((ArrayList<MapeoMovimientosSilicie>) ((MovimientosSilicieGrid) uw.grid).vector);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}

	public void desactivarCampos()
	{
		this.caeTitular.setEnabled(false);
	}
	
	public void cerrar()
	{
		vaciarCombo();
		mapeoModificado=null;
		((MovimientosSilicieView) uw).modificando=false; 
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        ((MovimientosSilicieView) uw).regresarDesdeForm();
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoMovimientosSilicie> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
//        btnEliminar.setEnabled(canRemoveProduct);
        
    }

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.caeTitular.getValue()==null || this.caeTitular.getValue().toString().length()==0)
			{ 
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Declarante");
				this.caeTitular.focus();
				return false;
			}
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Ejercicio ");
				this.ejercicio.focus();
				return false;
			}
			if (this.mes.getValue()==null || this.mes.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el mes");
				this.mes.focus();
				return false;
			}
			if (this.fechaMovimiento.getValue()==null || this.fechaMovimiento.getValue().toString().length()==0) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la Fecha Movimiento");
				this.fechaMovimiento.focus();
				return false;
			}
			if (this.fechaRegistro.getValue()==null || this.fechaRegistro.getValue().toString().length()==0) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la Fecha Registro");
				this.fechaRegistro.focus();
				return false;
			}
			if (this.descripcionMovimiento.getValue()==null || this.descripcionMovimiento.getValue().toString().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Movimiento");
				this.descripcionMovimiento.focus();
				return false;
			}
			if (this.descripcionRegimenFiscal.getValue()==null || this.descripcionRegimenFiscal.getValue().toString().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Regimen Fiscal");
				this.descripcionRegimenFiscal.focus();
				return false;
			}
//			if (this.operacion.getValue()==null || this.operacion.getValue().toString().length()==0)
//			{
//				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la operación");
//				this.operacion.focus();
//				return false;
//			}
			if (this.numeroJustificante.getValue()==null || this.numeroJustificante.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Numero Justificante");
				this.numeroJustificante.focus();
				return false;
			}
			if (this.codigoNC.getValue()==null || this.codigoNC.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el codigoNC");
				this.codigoNC.focus();
				return false;
			}
			if (this.descripcion.getValue()==null || this.descripcion.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la descripcion");
				this.descripcion.focus();
				return false;
			}
			if (this.codigoArticulo.getValue()==null || this.codigoArticulo.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el codigoArticulo");
				this.codigoArticulo.focus();
				return false;
			}
			if (this.descripcionArticulo.getValue()==null || this.descripcionArticulo.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la descripcionArticulo");
				this.descripcionArticulo.focus();
				return false;
			}
			if (this.litros.getValue()==null || this.litros.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar los litros");
				this.litros.focus();
				return false;
			}
			if (this.grado.getValue()==null || this.grado.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el grado alcoholico");
				this.grado.focus();
				return false;
			}

//			if (this.caeDestino.getValue()==null || this.caeDestino.getValue().toString().length()==0)
//			{
//				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el CAE de destino ");
//				this.caeDestino.focus();
//				return false;
//			}
		}
		
		return true;
	}
	
	private void cargarCombo()
	{
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			this.caeTitular.addItem(caes.get(i).trim());
		}
		this.caeTitular.setNewItemsAllowed(false);
	
		consultaRegimenesFiscalesServer crfs = consultaRegimenesFiscalesServer.getInstance(CurrentUser.get());
		ArrayList<MapeoRegimenesFiscales> vectorReg = crfs.datosRegimenesFiscalesGlobal(null);
		for (int i=0;i<vectorReg.size();i++)
		{
			MapeoRegimenesFiscales mapeo = (MapeoRegimenesFiscales) vectorReg.get(i); 
			this.descripcionRegimenFiscal.addItem(mapeo.getDescripcion().trim());
		}
		this.descripcionRegimenFiscal.setNewItemsAllowed(true);
		
		consultaTipoOperacionesServer cos = consultaTipoOperacionesServer.getInstance(CurrentUser.get());
		ArrayList<MapeoTipoOperaciones> vectorOp = cos.datosTipoOperacionesGlobal(null);
		for (int i=0;i<vectorOp.size();i++)
		{
			MapeoTipoOperaciones mapeo = (MapeoTipoOperaciones) vectorOp.get(i); 
			this.descripcionOperacion.addItem(mapeo.getDescripcion().trim());
		}
		this.descripcionOperacion.setNewItemsAllowed(true);
		
		consultaTipoJustificantesServer cjus = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
		ArrayList<MapeoTipoJustificantes> vectorJus = cjus.datosTipoJustificantesGlobal(null);
		for (int i=0;i<vectorJus.size();i++)
		{
			MapeoTipoJustificantes mapeo = (MapeoTipoJustificantes) vectorJus.get(i); 
			this.descripcionJustificante.addItem(mapeo.getDescripcion().trim());
		}
		this.descripcionJustificante.setNewItemsAllowed(false);
		
		consultaDocumentosIdentificativosServer cdocs = consultaDocumentosIdentificativosServer.getInstance(CurrentUser.get());
		ArrayList<MapeoDocumentosIdentificativos> vectorDoc = cdocs.datosDocumentosIdentificativosGlobal(null);
		for (int i=0;i<vectorDoc.size();i++)
		{
			MapeoDocumentosIdentificativos mapeo = (MapeoDocumentosIdentificativos) vectorDoc.get(i); 
			this.descripcionDocumentoOd.addItem(mapeo.getDescripcion().trim());
		}
		this.descripcionDocumentoOd.setNewItemsAllowed(true);
		
		consultaDiferenciasMenosServer cdifs = consultaDiferenciasMenosServer.getInstance(CurrentUser.get());
		ArrayList<MapeoDiferenciasMenos> vectorDif = cdifs.datosDiferenciasMenosGlobal(null);
		for (int i=0;i<vectorDif.size();i++)
		{
			MapeoDiferenciasMenos mapeo = (MapeoDiferenciasMenos) vectorDif.get(i); 
			this.descripcionDiferenciaMenos.addItem(mapeo.getDescripcion().trim());
		}
		this.descripcionDiferenciaMenos.setNewItemsAllowed(true);
	}
	
	private void posicionarCampos(MapeoMovimientosSilicie r_mapeo)
	{
		String cae = null;
		String titulo = null;
		String estado = null;
		
		cae = ((MovimientosSilicieView) uw).cmbCaeTitular.getValue().toString().trim();
		titulo = ((MovimientosSilicieView) uw).almacenSeleccionado.toUpperCase().trim();
		estado = ((MovimientosSilicieView) uw).cmbEstado.getValue().toString().toUpperCase().trim();
		
		this.caeTitular.setValue(cae);
		this.caeTitular.setCaption(titulo);

		this.ejercicio.setValue(((MovimientosSilicieView) uw).txtEjercicio.getValue().trim());
		this.mes.setValue(((MovimientosSilicieView) uw).cmbMes.getValue().toString().trim());
		
		if (r_mapeo!=null)
		{
			/*
			 * Relleno operacion, numeroJustificante, movimiento y regimen fiscal
			 */
			consultaMovimientosSilicieServer cmovs = consultaMovimientosSilicieServer.getInstance(CurrentUser.get());
			
			if (r_mapeo.getIdCodigoRegimenFiscal()!=null) this.descripcionRegimenFiscal.setValue(cmovs.obtenerDesc("Regimen", r_mapeo.getIdCodigoRegimenFiscal()));
			if (r_mapeo.getIdCodigoJustificante()!=null) this.descripcionJustificante.setValue(cmovs.obtenerDesc("Justificante", r_mapeo.getIdCodigoJustificante()));
			if (r_mapeo.getIdCodigoOperacion()!=null) this.descripcionOperacion.setValue(cmovs.obtenerDesc("Operacion", r_mapeo.getIdCodigoOperacion()));
			if (r_mapeo.getIdCodigoDocumentoOd()!=null) this.descripcionDocumentoOd.setValue(cmovs.obtenerDesc("DocumentoOd", r_mapeo.getIdCodigoDocumentoOd()));
			if (r_mapeo.getIdCodigoDiferenciaMenos()!=null) this.descripcionDiferenciaMenos.setValue(cmovs.obtenerDesc("DiferenciaMenos", r_mapeo.getIdCodigoDiferenciaMenos()));
			if (r_mapeo.getIdCodigoMovimiento()!=null) this.descripcionMovimiento.setValue(cmovs.obtenerDesc("Movimiento", r_mapeo.getIdCodigoMovimiento()));
		}
		
		if ((!this.busqueda && (estado.equals("PENDIENTES") || estado.equals("ANULADOS"))) || isCreacion())
		{
			this.btnGuardar.setEnabled(true);
			this.btnEliminar.setEnabled(true);
		}
		else
		{
			this.btnGuardar.setEnabled(false);
			this.btnEliminar.setEnabled(false);
		}
	}

	private void ventanaMargenes(String r_area)
	{
		ArrayList<MapeoAyudas> vector = null;
		switch (r_area)
		{
			case "Articulo":
			{
				consultaCodigosNCServer cncs = consultaCodigosNCServer.getInstance(CurrentUser.get());
				vector=cncs.vector();
				this.vHelp = new ventanaAyuda(this.codigoNC, this.descripcion, vector, "Codigos NC");
				break;
			}
			case "ArticuloG":
			{
				consultaArticulosServer cncs = consultaArticulosServer.getInstance(CurrentUser.get());
				vector=cncs.vector();
				this.vHelp = new ventanaAyuda(this.codigoArticulo, this.descripcionArticulo, vector, "Articulos");
				break;
			}
			case "Movimiento":
			{
				consultaTiposMovimientoServer cmov = consultaTiposMovimientoServer.getInstance(CurrentUser.get());
				vector=cmov.vector();
				this.vHelp = new ventanaAyuda(null, this.descripcionMovimiento, vector, "Movimientos");
				break;
			}
		}
		getUI().addWindow(this.vHelp);	
	}

	private void vaciarCombo()
	{
		this.caeTitular.removeAllItems();
		this.descripcionOperacion.removeAllItems();
		this.descripcionRegimenFiscal.removeAllItems();
	}
}
