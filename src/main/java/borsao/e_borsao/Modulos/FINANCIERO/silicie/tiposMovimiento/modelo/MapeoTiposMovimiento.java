package borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTiposMovimiento extends MapeoGlobal
{
	private String codigo;
	private String descripcion;
	private String signo;
	private String silicie;

	public MapeoTiposMovimiento()
	{
		this.setCodigo("");
		this.setDescripcion("");
		this.setSigno("");
		this.setSilicie("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	public String getSilicie() {
		return silicie;
	}

	public void setSilicie(String silicie) {
		this.silicie = silicie;
	}


}