package borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.modelo.MapeoTipoOperaciones;

public class consultaTipoOperacionesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTipoOperacionesServer instance;
	
	public consultaTipoOperacionesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTipoOperacionesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTipoOperacionesServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoTipoOperaciones> datosTipoOperacionesGlobal(MapeoTipoOperaciones r_mapeo)
	{
		ResultSet rsTipoOperaciones = null;		
		ArrayList<MapeoTipoOperaciones> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_op.id op_id, ");
		cadenaSQL.append(" fin_silicie_op.codigo op_cod, ");
		cadenaSQL.append(" fin_silicie_op.descripcion op_des ");
     	cadenaSQL.append(" FROM fin_silicie_op ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsTipoOperaciones= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoTipoOperaciones>();
			
			while(rsTipoOperaciones.next())
			{
				MapeoTipoOperaciones mapeoTipoOperaciones = new MapeoTipoOperaciones();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTipoOperaciones.setIdCodigo(rsTipoOperaciones.getInt("op_id"));
				mapeoTipoOperaciones.setCodigo(rsTipoOperaciones.getString("op_cod"));
				mapeoTipoOperaciones.setDescripcion(rsTipoOperaciones.getString("op_des"));
				vector.add(mapeoTipoOperaciones);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsTipoOperaciones = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_op.id) op_sig ");
     	cadenaSQL.append(" FROM fin_silicie_op ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsTipoOperaciones= cs.executeQuery(cadenaSQL.toString());
			
			while(rsTipoOperaciones.next())
			{
				return rsTipoOperaciones.getInt("op_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoTipoOperaciones r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_op ( ");
    		cadenaSQL.append(" fin_silicie_op.id, ");
    		cadenaSQL.append(" fin_silicie_op.codigo, ");
    		cadenaSQL.append(" fin_silicie_op.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoTipoOperaciones r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_op set ");
			cadenaSQL.append(" fin_silicie_op.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_op.descripcion=? ");
		    cadenaSQL.append(" WHERE fin_silicie_op.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoTipoOperaciones r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_op ");            
			cadenaSQL.append(" WHERE fin_silicie_op.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}