package borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo.MapeoCambioArticulo;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo.cambioArticuloGrid;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.server.consultaCambioArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoCambioArticulo mapeoCambioArticulo = null;
	 
	 private consultaCambioArticulosServer cus = null;	 
	 private cambioArticulosView uw = null;
	 private BeanFieldGroup<MapeoCambioArticulo> fieldGroup;
	 
	 public OpcionesForm(cambioArticulosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoCambioArticulo>(MapeoCambioArticulo.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.articulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// TODO Auto-generated method stub
				consultaArticulosServer cas =new consultaArticulosServer(CurrentUser.get());
				descripcion.setValue(cas.obtenerDescripcionArticulo(articulo.getValue()));
				nuevoNC.setValue(cas.obtenerArticulosNC(articulo.getValue()));
			}
		});
        
        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoCambioArticulo = (MapeoCambioArticulo) uw.grid.getSelectedRow();
                eliminarCambioArticulo(mapeoCambioArticulo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoCambioArticulo = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearCambioArticulo(mapeoCambioArticulo);
	 			}
	 			else
	 			{
	 				MapeoCambioArticulo mapeoCambioArticulo_orig = (MapeoCambioArticulo) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoCambioArticulo= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarCambioArticulo(mapeoCambioArticulo,mapeoCambioArticulo_orig);
	 				hm=null;
	 			}
	 			if (((cambioArticuloGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarCambioArticulo(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		
		if (this.nuevoNC.getValue()!=null && this.nuevoNC.getValue().length()>0) 
		{
			opcionesEscogidas.put("nuevoNc", this.nuevoNC.getValue());
		}
		else
		{
			opcionesEscogidas.put("nuevoNc", "");
		}
		if (this.nuevoArticulo.getValue()!=null && this.nuevoArticulo.getValue().length()>0) 
		{
			opcionesEscogidas.put("nuevoArticulo", this.nuevoArticulo.getValue());
		}
		else
		{
			opcionesEscogidas.put("nuevoArticulo", "");
		}
		if (this.nuevaDescripcion.getValue()!=null && this.nuevaDescripcion.getValue().length()>0) 
		{
			opcionesEscogidas.put("nuevaDescripcion", this.nuevaDescripcion.getValue());
		}
		else
		{
			opcionesEscogidas.put("nuevaDescripcion", "");
		}
		if (this.lote.getValue()!=null && this.lote.getValue().length()>0) 
		{
			opcionesEscogidas.put("lote", this.lote.getValue());
		}
		else
		{
			opcionesEscogidas.put("lote", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarCambioArticulo(null);
	}
	
	public void editarCambioArticulo(MapeoCambioArticulo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoCambioArticulo();
		fieldGroup.setItemDataSource(new BeanItem<MapeoCambioArticulo>(r_mapeo));
		articulo.focus();
	}
	
	public void eliminarCambioArticulo(MapeoCambioArticulo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((cambioArticuloGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoCambioArticulo>) ((cambioArticuloGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarCambioArticulo(MapeoCambioArticulo r_mapeo, MapeoCambioArticulo r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaCambioArticulosServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((cambioArticuloGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearCambioArticulo(MapeoCambioArticulo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaCambioArticulosServer(CurrentUser.get());
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoCambioArticulo>(r_mapeo));
				if (((cambioArticuloGrid) uw.grid)!=null)
				{
					((cambioArticuloGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoCambioArticulo> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoCambioArticulo= item.getBean();
            if (this.mapeoCambioArticulo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la descripcion");
    		return false;
    	}
    	if (this.nuevoNC.getValue()==null || this.nuevoNC.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el nuevoNC");
    		return false;
    	}
    	if (this.articulo.getValue()==null || this.articulo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Articulo");
    		return false;
    	}
    	if (this.nuevoArticulo.getValue()==null || this.nuevoArticulo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Nuevo Articulo");
    		return false;
    	}
    	if (this.nuevaDescripcion.getValue()==null || this.nuevaDescripcion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Nueva Descripcion");
    		return false;
    	}
    	if (this.lote.getValue()==null || this.lote.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Lote");
    		return false;
    	}
    	return true;
    }
}
