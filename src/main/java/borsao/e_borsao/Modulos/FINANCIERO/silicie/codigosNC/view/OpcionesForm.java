package borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.modelo.CodigosNCGrid;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.modelo.MapeoCodigosNC;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.server.consultaCodigosNCServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.codigosNC.modelo.hashToMapeo;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoCodigosNC mapeoCodigosNC = null;
	 
	 private consultaCodigosNCServer cus = null;	 
	 private CodigosNCView uw = null;
	 private BeanFieldGroup<MapeoCodigosNC> fieldGroup;
	 
	 public OpcionesForm(CodigosNCView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");

        cus = consultaCodigosNCServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoCodigosNC>(MapeoCodigosNC.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoCodigosNC = (MapeoCodigosNC) uw.grid.getSelectedRow();
                eliminarCodigosNC(mapeoCodigosNC);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoCodigosNC = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearCodigosNC(mapeoCodigosNC);
	 			}
	 			else
	 			{
	 				MapeoCodigosNC mapeoCodigosNC_orig = (MapeoCodigosNC) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoCodigosNC= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarCodigosNC(mapeoCodigosNC,mapeoCodigosNC_orig);
	 				hm=null;
	 			}
	 			if (((CodigosNCGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarCodigosNC(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.codigo.getValue()!=null && this.codigo.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("codigo", this.codigo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigo", "");
		}
		
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarCodigosNC(null);
	}
	
	public void editarCodigosNC(MapeoCodigosNC r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoCodigosNC();		
		fieldGroup.setItemDataSource(new BeanItem<MapeoCodigosNC>(r_mapeo));
		codigo.focus();
	}
	
	public void eliminarCodigosNC(MapeoCodigosNC r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((CodigosNCGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoCodigosNC>) ((CodigosNCGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarCodigosNC(MapeoCodigosNC r_mapeo, MapeoCodigosNC r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaCodigosNCServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((CodigosNCGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearCodigosNC(MapeoCodigosNC r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaCodigosNCServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoCodigosNC>(r_mapeo));
				if (((CodigosNCGrid) uw.grid)!=null)
				{
					((CodigosNCGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoCodigosNC> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoCodigosNC= item.getBean();
            if (this.mapeoCodigosNC.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.codigo.getValue()==null || this.codigo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Código");
    		return false;
    	}
    	if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar La Descripción");
    		return false;
    	}
    	return true;
    }

}
