package borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.modelo.MapeoDocumentosIdentificativos;

public class consultaDocumentosIdentificativosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDocumentosIdentificativosServer instance;
	
	public consultaDocumentosIdentificativosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDocumentosIdentificativosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDocumentosIdentificativosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoDocumentosIdentificativos> datosDocumentosIdentificativosGlobal(MapeoDocumentosIdentificativos r_mapeo)
	{
		ResultSet rsDocumentosIdentificativos = null;		
		ArrayList<MapeoDocumentosIdentificativos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_doc.id doc_id, ");
		cadenaSQL.append(" fin_silicie_doc.codigo doc_cod, ");
		cadenaSQL.append(" fin_silicie_doc.descripcion doc_des, ");
		cadenaSQL.append(" fin_silicie_doc.verifica_sn doc_ver ");
     	cadenaSQL.append(" FROM fin_silicie_doc ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getVerifica()!=null && r_mapeo.getVerifica().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" verifica_sn = '" + r_mapeo.getVerifica() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsDocumentosIdentificativos= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoDocumentosIdentificativos>();
			
			while(rsDocumentosIdentificativos.next())
			{
				MapeoDocumentosIdentificativos mapeoDocumentosIdentificativos = new MapeoDocumentosIdentificativos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoDocumentosIdentificativos.setIdCodigo(rsDocumentosIdentificativos.getInt("doc_id"));
				mapeoDocumentosIdentificativos.setCodigo(rsDocumentosIdentificativos.getString("doc_cod"));
				mapeoDocumentosIdentificativos.setDescripcion(rsDocumentosIdentificativos.getString("doc_des"));
				mapeoDocumentosIdentificativos.setVerifica(rsDocumentosIdentificativos.getString("doc_ver"));
				vector.add(mapeoDocumentosIdentificativos);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsDocumentosIdentificativos = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_doc.id) doc_sig ");
     	cadenaSQL.append(" FROM fin_silicie_doc ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsDocumentosIdentificativos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsDocumentosIdentificativos.next())
			{
				return rsDocumentosIdentificativos.getInt("doc_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoDocumentosIdentificativos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_doc ( ");
    		cadenaSQL.append(" fin_silicie_doc.id, ");
    		cadenaSQL.append(" fin_silicie_doc.codigo, ");
    		cadenaSQL.append(" fin_silicie_doc.descripcion, ");
		    cadenaSQL.append(" fin_silicie_doc.verifica_sn ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getVerifica()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getVerifica());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoDocumentosIdentificativos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_doc set ");
			cadenaSQL.append(" fin_silicie_doc.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_doc.descripcion=?, ");
		    cadenaSQL.append(" fin_silicie_doc.verifica_sn=? ");
		    cadenaSQL.append(" WHERE fin_silicie_doc.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setString(3, r_mapeo.getVerifica());
		    preparedStatement.setInt(4, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoDocumentosIdentificativos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_doc ");            
			cadenaSQL.append(" WHERE fin_silicie_doc.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}