package borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoRegimenesFiscales mapeo = null;
	 
	 public MapeoRegimenesFiscales convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoRegimenesFiscales();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoRegimenesFiscales convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoRegimenesFiscales();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoRegimenesFiscales r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("codigo", r_mapeo.getCodigo());		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 return hash;		 
	 }
}