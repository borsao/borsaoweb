package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionRangosGeneracion extends Ventana
{
	
	/*
	 * Parte Grafica
	 */
	private boolean chequeado = false;
	private boolean hayErrores = false;
	
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	private Button btnBotonChequeos = null;
	
	private CheckBox chkSaltarChequeos = null;
	
	private TextField txtDeclarante = null;
	private TextField txtEjercicio = null;
	private TextField txtMes = null;
	
	private CheckBox chkCargarInventarioInicial = null;
	
	private CheckBox chkComprobarCaes = null;
	private Label lblComprobarCaes = null;

	private CheckBox chkComprobarMes = null;
	private Label lblComprobarMes = null;
//	private CheckBox chkComprobarFechas = null;
//	private Label lblComprobarFechas = null;
	private CheckBox chkComprobarCodigoNC = null;
	private Label lblComprobarCodigoNC = null;
	private CheckBox chkComprobarGrado = null;
	private Label lblComprobarGrado = null;

	private CheckBox chkComprobarARC= null;
	private Label lblComprobarARC = null;
	private CheckBox chkComprobarNR= null;
	private Label lblComprobarNR = null;
	
	private CheckBox chkComprobarValorGrado = null;
	private Label lblComprobarValorGrado = null;

	private Label lblComprobarClientes = null;
	private CheckBox chkComprobarClientes = null;
	private CheckBox chkComprobarEntradasEMCS = null;
	private Label lblComprobarEntradas = null;
	private CheckBox chkComprobarFactorArticulos = null;
	private Label lblComprobarFactorArticulos = null;
	private CheckBox chkComprobarTrasladosEMCS = null;
	private Label lblComprobarTraslados = null;
	private CheckBox chkComprobarSalidasEMCS = null;
	private Label lblComprobarSalidas = null;
	
	private Label lblGenerandoInventarioInicial= null;
	private Label lblGenerandoEntradasCompras= null;
	private Label lblGenerandoEntradasTraslado = null;
	private Label lblGenerandoEntradasRegularizacion= null;
	private Label lblGenerandoSalidasRegularizacion = null;
	private Label lblGenerandoSalidasTraslado = null;
	private Label lblGenerandoSalidasVentas = null;
	private Label lblGenerandoFabricacion= null;
	private Label lblGenerandoUva= null;
	
	private consultaMovimientosSilicieServer cmss = null;
	private MovimientosSilicieView origen = null;
	
	private HorizontalLayout fila2= null;
	private VentanaAceptarCancelar vt=null;	
	private final static String rdoOK = "Ok";
	private final static String rdoKo = "Ko";
	
	public PeticionRangosGeneracion(MovimientosSilicieView r_view, String r_cae, String r_ejercicio, String r_mes)
	{
		this.origen=r_view;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
		HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtDeclarante = new TextField("Declarante");
				this.txtDeclarante.setValue(r_cae);
				this.txtDeclarante.setEnabled(false);
				
				this.txtEjercicio = new TextField("Ejercicio");
				this.txtEjercicio.setValue(r_ejercicio);
				this.txtEjercicio.setEnabled(false);
				
				this.txtMes = new TextField("Mes");
				this.txtMes.setValue(r_mes);
				this.txtMes.setEnabled(false);
				
				fila1.addComponent(this.txtDeclarante);
				fila1.addComponent(this.txtEjercicio);
				fila1.addComponent(this.txtMes);
				
			fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.chkComprobarMes = new CheckBox("Chequeo Mes no declarado" );
				this.chkComprobarMes.setValue(true);
				
				this.lblComprobarMes = new Label("?");
				this.lblComprobarMes.setStyleName("deColor");
				
				this.chkComprobarCaes = new CheckBox("CAE de Origen igual a Destino" );
				this.chkComprobarCaes.setValue(true);
				
				this.lblComprobarCaes = new Label("?");
				this.lblComprobarCaes.setStyleName("deColor");
				
				this.lblGenerandoEntradasCompras = new Label("?");
				this.lblGenerandoEntradasCompras.setStyleName("deColor");
				this.lblGenerandoEntradasCompras.setVisible(false);
				
				fila2.addComponent(this.chkComprobarMes);
				fila2.addComponent(this.lblComprobarMes);
				fila2.addComponent(this.chkComprobarCaes);
				fila2.addComponent(this.lblComprobarCaes);
				fila2.addComponent(this.lblGenerandoEntradasCompras);
				
				fila2.setComponentAlignment(this.chkComprobarMes,Alignment.MIDDLE_LEFT);
				fila2.setComponentAlignment(this.chkComprobarCaes,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.chkComprobarClientes= new CheckBox("Chequeo Clientes sin tipo EMCS" );
				this.chkComprobarClientes.setValue(true);
					
				this.lblComprobarClientes = new Label("?");
				this.lblComprobarClientes.setStyleName("deColor");

				this.chkComprobarValorGrado= new CheckBox("Chequeo Valor Grado Correcto" );
				this.chkComprobarValorGrado.setValue(true);
					
				this.lblComprobarValorGrado = new Label("?");
				this.lblComprobarValorGrado.setStyleName("deColor");

				fila3.addComponent(this.chkComprobarClientes);
				fila3.addComponent(this.lblComprobarClientes);
				fila3.addComponent(this.chkComprobarValorGrado);
				fila3.addComponent(this.lblComprobarValorGrado);
				fila3.setComponentAlignment(this.chkComprobarClientes,Alignment.MIDDLE_LEFT);
				fila3.setComponentAlignment(this.chkComprobarValorGrado,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.chkComprobarCodigoNC= new CheckBox("Chequeo Articulos sin CodigosNC" );
				this.chkComprobarCodigoNC.setValue(true);
					
				this.lblComprobarCodigoNC = new Label("?");
				this.lblComprobarCodigoNC.setStyleName("deColor");
				
				this.chkComprobarARC= new CheckBox("Chequeo Valores ARC" );
				this.chkComprobarARC.setValue(true);
					
				this.lblComprobarARC= new Label("?");
				this.lblComprobarARC.setStyleName("deColor");

				this.lblGenerandoFabricacion = new Label("?");
				this.lblGenerandoFabricacion.setStyleName("deColor");
				this.lblGenerandoFabricacion.setVisible(false);
				
				fila4.addComponent(this.chkComprobarCodigoNC);
				fila4.addComponent(this.lblComprobarCodigoNC);
				fila4.addComponent(this.chkComprobarARC);
				fila4.addComponent(this.lblComprobarARC);
				fila4.addComponent(this.lblGenerandoFabricacion);
				fila4.setComponentAlignment(this.chkComprobarCodigoNC,Alignment.MIDDLE_LEFT);
				fila4.setComponentAlignment(this.chkComprobarARC,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila5 = new HorizontalLayout();
			fila5.setWidthUndefined();
			fila5.setSpacing(true);
				this.chkComprobarGrado = new CheckBox("Chequeo Articulos sin Grado ");
				this.chkComprobarGrado.setValue(true);
					
				this.lblComprobarGrado = new Label("?");
				this.lblComprobarGrado.setStyleName("deColor");

				this.chkComprobarNR= new CheckBox("Chequeo Valores Numero Referencia" );
				this.chkComprobarNR.setValue(true);
					
				this.lblComprobarNR= new Label("?");
				this.lblComprobarNR.setStyleName("deColor");

				this.lblGenerandoEntradasRegularizacion = new Label("?");
				this.lblGenerandoEntradasRegularizacion.setStyleName("deColor");
				this.lblGenerandoEntradasRegularizacion.setVisible(false);
				
				this.lblGenerandoSalidasRegularizacion = new Label("?");
				this.lblGenerandoSalidasRegularizacion.setStyleName("deColor");
				this.lblGenerandoSalidasRegularizacion.setVisible(false);

				fila5.addComponent(this.chkComprobarGrado);
				fila5.addComponent(this.lblComprobarGrado);
				fila5.addComponent(this.chkComprobarNR);
				fila5.addComponent(this.lblComprobarNR);
				fila5.addComponent(this.lblGenerandoEntradasRegularizacion);
				fila5.addComponent(this.lblGenerandoSalidasRegularizacion);
				fila5.setComponentAlignment(this.chkComprobarGrado,Alignment.MIDDLE_LEFT);
				fila5.setComponentAlignment(this.chkComprobarNR,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila6 = new HorizontalLayout();
			fila6.setWidthUndefined();
			fila6.setSpacing(true);
				this.chkComprobarEntradasEMCS= new CheckBox("Chequeo EMCS Entradas");
				this.chkComprobarEntradasEMCS.setValue(true);
					
				this.lblComprobarEntradas = new Label("?");
				this.lblComprobarEntradas.setStyleName("deColor");

				this.chkComprobarFactorArticulos= new CheckBox("Chequeo Todos los Articulos con Volumen");
				this.chkComprobarFactorArticulos.setValue(true);
					
				this.lblComprobarFactorArticulos= new Label("?");
				this.lblComprobarFactorArticulos.setStyleName("deColor");

				this.lblGenerandoEntradasTraslado= new Label("?");
				this.lblGenerandoEntradasTraslado.setStyleName("deColor");
				this.lblGenerandoEntradasTraslado.setVisible(false);
				
				this.lblGenerandoSalidasTraslado= new Label("?");
				this.lblGenerandoSalidasTraslado.setStyleName("deColor");
				this.lblGenerandoSalidasTraslado.setVisible(false);

				fila6.addComponent(this.chkComprobarEntradasEMCS);
				fila6.addComponent(this.lblComprobarEntradas);
				fila6.addComponent(this.chkComprobarFactorArticulos);
				fila6.addComponent(this.lblComprobarFactorArticulos);
				fila6.addComponent(this.lblGenerandoEntradasTraslado);
				fila6.addComponent(this.lblGenerandoSalidasTraslado);
				fila6.setComponentAlignment(this.chkComprobarEntradasEMCS,Alignment.MIDDLE_LEFT);
				fila6.setComponentAlignment(this.chkComprobarFactorArticulos,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila7 = new HorizontalLayout();
			fila7.setWidthUndefined();
			fila7.setSpacing(true);
				this.chkComprobarTrasladosEMCS= new CheckBox("Chequeo EMCS Traslados");
				this.chkComprobarTrasladosEMCS.setValue(true);
					
				this.lblComprobarTraslados = new Label("?");
				this.lblComprobarTraslados.setStyleName("deColor");

				this.lblGenerandoSalidasVentas= new Label("?");
				this.lblGenerandoSalidasVentas.setStyleName("deColor");
				this.lblGenerandoSalidasVentas.setVisible(false);

				
				this.lblGenerandoUva = new Label("?");
				this.lblGenerandoUva.setStyleName("deColor");
				this.lblGenerandoUva.setVisible(false);

				fila7.addComponent(this.chkComprobarTrasladosEMCS);
				fila7.addComponent(this.lblComprobarTraslados);
				fila7.addComponent(this.lblGenerandoSalidasVentas);
				fila7.addComponent(this.lblGenerandoUva);
				fila7.setComponentAlignment(this.chkComprobarTrasladosEMCS,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila8 = new HorizontalLayout();
			fila8.setWidthUndefined();
			fila8.setSpacing(true);
				this.chkComprobarSalidasEMCS= new CheckBox("Chequeo EMCS Salidas");
				this.chkComprobarSalidasEMCS.setValue(true);
					
				this.lblComprobarSalidas = new Label("?");
				this.lblComprobarSalidas.setStyleName("deColor");
				
				this.lblGenerandoInventarioInicial = new Label("?");
				this.lblGenerandoInventarioInicial.setStyleName("deColor");
				this.lblGenerandoInventarioInicial.setVisible(false);
					
				fila8.addComponent(this.chkComprobarSalidasEMCS);
				fila8.addComponent(this.lblComprobarSalidas);
				fila8.addComponent(this.lblGenerandoInventarioInicial);
				fila8.setComponentAlignment(this.chkComprobarSalidasEMCS,Alignment.MIDDLE_LEFT);

			HorizontalLayout fila9 = new HorizontalLayout();
			fila9.setWidthUndefined();
			fila9.setSpacing(true);
				this.chkCargarInventarioInicial = new CheckBox("Cargar Existencias Iniciales");
				this.chkCargarInventarioInicial.setValue(false);
				this.chkCargarInventarioInicial.setVisible(r_mes.contentEquals("01"));
				
				fila9.addComponent(this.chkCargarInventarioInicial);
				fila9.setComponentAlignment(this.chkCargarInventarioInicial,Alignment.MIDDLE_LEFT);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		controles.addComponent(fila5);
		controles.addComponent(fila6);
		controles.addComponent(fila7);
		controles.addComponent(fila8);
		controles.addComponent(fila9);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("650px");
		this.center();

		
		this.chkSaltarChequeos= new CheckBox("Saltar Chequeos");
		this.chkSaltarChequeos.setValue(false);
		
		btnBotonChequeos = new Button("Chequear");
		btnBotonChequeos.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonAVentana = new Button("Generar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana.setEnabled(false);
		botonera.addComponent(btnBotonChequeos);
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);
		if (eBorsao.get().accessControl.getNombre().contains("Claveria")) botonera.addComponent(chkSaltarChequeos);
		
		botonera.setComponentAlignment(btnBotonChequeos, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		if (eBorsao.get().accessControl.getNombre().contains("Claveria")) botonera.setComponentAlignment(chkSaltarChequeos, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}

	private void cargarListeners()
	{

		
		chkSaltarChequeos.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				btnBotonAVentana.setEnabled(chkSaltarChequeos.getValue());
				chequeado=chkSaltarChequeos.getValue();
				if (txtMes.getValue().equals("01")) chkCargarInventarioInicial.setVisible(chequeado);
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				
				if (chequeado)
				{
					generar();
				}
				else
				{
					if (hayErrores) Notificaciones.getInstance().mensajeAdvertencia("No se puede generar la declaracion. Hay errores que corregir."); else
						Notificaciones.getInstance().mensajeAdvertencia("Tienes que pasar los chequeos antes de generar los datos.");
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		btnBotonChequeos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				chequeado=chequear(chkCargarInventarioInicial.getValue());
				btnBotonAVentana.setEnabled(chequeado);
			}
		});	
	}
	
    private void generar()
    {   
    	vt = new VentanaAceptarCancelar(this, "Seguro que quieres generar los movimientos para el ejercicio, mes y almacen seleccionados? (Se borrará todo lo no declarado del mes, salvo los movimientos manuales.)", "Generar", "Cancelar", "Generar", null);
    	getUI().addWindow(vt);
    }

	private boolean chequear(boolean r_apertura)
	{
		consultaArticulosServer cas = null;
		boolean resultado = true;
		consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
		MapeoMovimientosSilicie mapeoMovimientos = null;
		hayErrores=false;
		/*
		 * chequeo mes declarado
		 */
		if (chkComprobarMes.getValue())
		{
			this.lblComprobarMes.setValue("");
			this.lblComprobarMes.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			mapeoMovimientos = new MapeoMovimientosSilicie();
			mapeoMovimientos.setEstado("Declarados");
			mapeoMovimientos.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			mapeoMovimientos.setMes(new Integer(this.txtMes.getValue()));
			mapeoMovimientos.setCaeTitular(this.txtDeclarante.getValue());
			
			resultado = cmss.comprobarMesDeclarado(mapeoMovimientos, r_apertura);

			if (resultado)
			{
				this.lblComprobarMes.setValue(this.rdoOK);
				this.lblComprobarMes.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarMes.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
//				fila2.addLayoutClickListener(new LayoutClickListener() {			
//			          @Override
//			           public void layoutClick(LayoutClickEvent event) {
//			                	
//			                	if (event.getClickedComponent() instanceof Label) Notificaciones.getInstance().mensajeInformativo("HOLA");		
//			            }
//				}); 
				this.lblComprobarMes.setValue(this.rdoKo);
				this.lblComprobarMes.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarMes.setStyleName(ValoTheme.LABEL_FAILURE);

//				Notificaciones.getInstance().mensajeError("Este mes ya lo tienes declarado");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
	/*
	 * chequeo caes
	 */
		if (chkComprobarCaes.getValue())
		{
			this.lblComprobarCaes.setValue("");
			this.lblComprobarCaes.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			mapeoMovimientos = new MapeoMovimientosSilicie();
			mapeoMovimientos.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			mapeoMovimientos.setMes(new Integer(this.txtMes.getValue()));
			mapeoMovimientos.setCaeTitular(this.txtDeclarante.getValue());
			
			resultado = cmss.comprobarCaes(mapeoMovimientos);
			
			if (!resultado)
			{
				this.lblComprobarCaes.setValue(this.rdoOK);
				this.lblComprobarCaes.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarCaes.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarCaes.setValue(this.rdoKo);
				this.lblComprobarCaes.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarCaes.setStyleName(ValoTheme.LABEL_FAILURE);
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		
		/*
		 * chequeo fechas
		 */
			
		if (chkComprobarClientes.getValue())
		{
			this.lblComprobarClientes.setValue("");
			this.lblComprobarClientes.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			
//			mapeoMovimientos = new MapeoMovimientosSilicie();
//			mapeoMovimientos.setEstado("Pendientes");
//			mapeoMovimientos.setEjercicio(new Integer(this.txtEjercicio.getValue()));
//			mapeoMovimientos.setMes(new Integer(this.txtMes.getValue()));
//			mapeoMovimientos.setCaeTitular(this.txtDeclarante.getValue());
			
			consultaClientesServer ccs = consultaClientesServer.getInstance(CurrentUser.get());
			resultado = ccs.chequearClientesEMCS();
			
			if (!resultado)
			{
				this.lblComprobarClientes.setValue(this.rdoOK);
				this.lblComprobarClientes.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarClientes.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarClientes.setValue(this.rdoKo);
				this.lblComprobarClientes.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarClientes.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay movimientos registrados 24h más tarde de los previsto.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		/*
		 * chequeo caes
		 */
		if (chkComprobarValorGrado.getValue())
		{
			this.lblComprobarValorGrado.setValue("");
			this.lblComprobarValorGrado.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			mapeoMovimientos = new MapeoMovimientosSilicie();
			mapeoMovimientos.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			mapeoMovimientos.setMes(new Integer(this.txtMes.getValue()));
			mapeoMovimientos.setCaeTitular(this.txtDeclarante.getValue());
			
			resultado = cmss.comprobarGrado(mapeoMovimientos);
			
			if (!resultado)
			{
				this.lblComprobarValorGrado.setValue(this.rdoOK);
				this.lblComprobarValorGrado.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarValorGrado.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarValorGrado.setValue(this.rdoKo);
				this.lblComprobarValorGrado.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarValorGrado.setStyleName(ValoTheme.LABEL_FAILURE);
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}		
		/*
		 * chequeo codigosNC
		 */
		if (chkComprobarCodigoNC.getValue())
		{
			this.lblComprobarCodigoNC.setValue("");
			this.lblComprobarCodigoNC.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			resultado = cas.obtenerArticulosSinNC();
			
			if (!resultado)
			{
				this.lblComprobarCodigoNC.setValue(this.rdoOK);
				this.lblComprobarCodigoNC.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarCodigoNC.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarCodigoNC.setValue(this.rdoKo);
				this.lblComprobarCodigoNC.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarCodigoNC.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay articulos sin codigoNC en la ficha.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		/*
		 * chequeo codigosARC
		 */
		if (chkComprobarARC.getValue())
		{
			this.lblComprobarARC.setValue("");
			this.lblComprobarARC.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			resultado = cmss.comprobarARC(mapeoMovimientos);
			
			if (!resultado)
			{
				this.lblComprobarARC.setValue(this.rdoOK);
				this.lblComprobarARC.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarARC.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarARC.setValue(this.rdoKo);
				this.lblComprobarARC.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarARC.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay articulos sin codigoNC en la ficha.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		/*
		 * chequeo codigosNR
		 */
		if (chkComprobarNR.getValue())
		{
			this.lblComprobarNR.setValue("");
			this.lblComprobarNR.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			resultado = cmss.comprobarNR(mapeoMovimientos);
			
			if (!resultado)
			{
				this.lblComprobarNR.setValue(this.rdoOK);
				this.lblComprobarNR.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarNR.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarNR.setValue(this.rdoKo);
				this.lblComprobarNR.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarNR.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay articulos sin codigoNC en la ficha.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		
		
		/*
		 * chequeo grado
		 */
		if (chkComprobarGrado.getValue())
		{
			this.lblComprobarGrado.setValue("");
			this.lblComprobarGrado.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			resultado = cas.obtenerArticulosSinGrado();
			
			if (!resultado)
			{
				this.lblComprobarGrado.setValue(this.rdoOK);
				this.lblComprobarGrado.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarGrado.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarGrado.setValue(this.rdoKo);
				this.lblComprobarGrado.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarGrado.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay articulos sin grado en la ficha.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		/*
		 * chequeo entrada sin EMCS
		 */
		if (chkComprobarEntradasEMCS.getValue())
		{
			this.lblComprobarEntradas.setValue("");
			this.lblComprobarEntradas.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
			resultado = cacs.chequeoSinEmcs(new Integer(this.txtEjercicio.getValue()), this.txtDeclarante.getValue().trim(), new Integer(this.txtMes.getValue()));
			
			if (resultado)
			{
				this.lblComprobarEntradas.setValue(this.rdoOK);
				this.lblComprobarEntradas.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarEntradas.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarEntradas.setValue(this.rdoKo);
				this.lblComprobarEntradas.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarEntradas.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay entradas de Granel sin EMCS asociado.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}

		/*
		 * chequeo codigosNC
		 */
		if (chkComprobarFactorArticulos.getValue())
		{
			this.lblComprobarFactorArticulos.setValue("");
			this.lblComprobarFactorArticulos.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			
			cas = consultaArticulosServer.getInstance(CurrentUser.get());
			resultado = cas.obtenerArticulosSinFactor();
			
			if (!resultado)
			{
				this.lblComprobarFactorArticulos.setValue(this.rdoOK);
				this.lblComprobarFactorArticulos.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarFactorArticulos.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else 
			{
				this.lblComprobarFactorArticulos.setValue(this.rdoKo);
				this.lblComprobarFactorArticulos.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarFactorArticulos.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay articulos sin codigoNC en la ficha.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}		
		
		/*
		 * chequeo traslados sin EMCS
		 */
		if (chkComprobarTrasladosEMCS.getValue())
		{
			this.lblComprobarTraslados.setValue("");
			this.lblComprobarTraslados.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
			resultado = cats.chequeoEntradaSinEmcs(new Integer(this.txtEjercicio.getValue()), this.txtDeclarante.getValue().trim(), new Integer(this.txtMes.getValue()));
			
			if (resultado)
			{
				resultado = cats.chequeoSalidaSinEmcs(new Integer(this.txtEjercicio.getValue()), this.txtDeclarante.getValue().trim(), new Integer(this.txtMes.getValue()));
				
				if (resultado)
				{
					this.lblComprobarTraslados.setValue(this.rdoOK);
					this.lblComprobarTraslados.addStyleName(ValoTheme.LABEL_SUCCESS);
					this.lblComprobarTraslados.setStyleName(ValoTheme.LABEL_SUCCESS);
				}
				else
				{
					this.lblComprobarTraslados.setValue(this.rdoKo);
					this.lblComprobarTraslados.addStyleName(ValoTheme.LABEL_FAILURE);
					this.lblComprobarTraslados.setStyleName(ValoTheme.LABEL_FAILURE);
//					Notificaciones.getInstance().mensajeError("Hay envios de traslados sin EMCS asociado.");
					hayErrores=true;	
				}
			}
			else 
			{
				this.lblComprobarTraslados.setValue(this.rdoKo);
				this.lblComprobarTraslados.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarTraslados.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay recepciones de traslados sin EMCS asociado.");
				hayErrores=true;
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}			
		
		/*
		 * chequeo ventas sin EMCS
		 */
		if (chkComprobarSalidasEMCS.getValue())
		{
			this.lblComprobarSalidas.setValue("");
			this.lblComprobarSalidas.addStyleName(ValoTheme.LABEL_SPINNER);
			getUI().push();
			consultaAlbaranesVentasServer cacs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());
			resultado = cacs.chequeoSalidasSinEMCS(new Integer(this.txtEjercicio.getValue()), this.txtDeclarante.getValue().trim(), new Integer(this.txtMes.getValue()));
						
			if (resultado)
			{
				this.lblComprobarSalidas.setValue(this.rdoOK);
				this.lblComprobarSalidas.addStyleName(ValoTheme.LABEL_SUCCESS);
				this.lblComprobarSalidas.setStyleName(ValoTheme.LABEL_SUCCESS);
			}
			else
			{
				this.lblComprobarSalidas.setValue(this.rdoKo);
				this.lblComprobarSalidas.addStyleName(ValoTheme.LABEL_FAILURE);
				this.lblComprobarSalidas.setStyleName(ValoTheme.LABEL_FAILURE);
//				Notificaciones.getInstance().mensajeError("Hay salids por ventas sin EMCS asociado.");
				hayErrores=true;	
			}
			this.markAsDirty();
			this.markAsDirtyRecursive();
			getUI().push();
		}
		
		if (hayErrores) resultado = false;
		
		if (eBorsao.get().prop.pruebas) return true; else return resultado;
		
	}
	
	public void aceptarProceso(String r_accion)
	{
		String rdo = null;
		String declarante = null;
		Integer ejercicio = null;
		Integer mes = null;
		
		if (this.vt.isVisible()) this.vt.close();
		
		this.cmss = new consultaMovimientosSilicieServer(CurrentUser.get());
		/*
		 * Proceso de generacion de los datos
		 */
		
		this.chkComprobarCodigoNC.setVisible(false);
		this.chkComprobarClientes.setVisible(false);
		this.chkComprobarEntradasEMCS.setVisible(false);
		this.chkComprobarFactorArticulos.setVisible(false);
//		this.chkComprobarFechas.setVisible(false);
		this.chkComprobarGrado.setVisible(false);
		this.chkComprobarMes.setVisible(false);
		this.chkComprobarARC.setVisible(false);
		this.chkComprobarNR.setVisible(false);
		this.chkComprobarCaes.setVisible(false);
		this.chkComprobarValorGrado.setVisible(false);
		this.chkComprobarSalidasEMCS.setVisible(false);
		this.chkComprobarTrasladosEMCS.setVisible(false);
		this.chkCargarInventarioInicial.setVisible(false);
		
		this.lblComprobarMes.setVisible(false);
		this.lblComprobarCaes.setVisible(false);
		this.lblComprobarValorGrado.setVisible(false);
		this.lblComprobarARC.setVisible(false);
		this.lblComprobarNR.setVisible(false);
		this.lblComprobarCodigoNC.setVisible(false);
		this.lblComprobarFactorArticulos.setVisible(false);
		this.lblComprobarEntradas.setVisible(false);
		this.lblComprobarGrado.setVisible(false);
		this.lblComprobarClientes.setVisible(false);
//		this.lblComprobarFechas.setVisible(false);
		this.lblComprobarSalidas.setVisible(false);
		this.lblComprobarTraslados.setVisible(false);

		getUI().push();
		
//			Notificaciones.getInstance().mensajeAdvertencia("Aqui generamos los datos del mes, de golpe");
		if (this.txtEjercicio.getValue()!=null)
		{
			ejercicio = new Integer(this.txtEjercicio.getValue());
			mes = new Integer(this.txtMes.getValue());
			declarante = this.txtDeclarante.getValue().trim();
			
			rdo = this.cmss.eliminarGenerados(declarante, ejercicio, mes);
			
			if (rdo==null)
			{	
				
				if (chkCargarInventarioInicial.getValue())
				{
					this.lblGenerandoInventarioInicial.setValue("");
					this.lblGenerandoInventarioInicial.addStyleName(ValoTheme.LABEL_SPINNER);
					getUI().push();
					
					rdo = this.cmss.generacionMovimientosInventarioInicialSilicie(declarante, ejercicio);
					if (rdo==null)
					{
						this.lblGenerandoInventarioInicial.setValue(this.rdoOK);
						this.lblGenerandoInventarioInicial.addStyleName(ValoTheme.LABEL_SUCCESS);
						this.lblGenerandoInventarioInicial.setStyleName(ValoTheme.LABEL_SUCCESS);
					}
					else
					{
						this.lblGenerandoInventarioInicial.setValue(this.rdoKo);
						this.lblGenerandoInventarioInicial.addStyleName(ValoTheme.LABEL_FAILURE);
						this.lblGenerandoInventarioInicial.setStyleName(ValoTheme.LABEL_FAILURE);					
					}
					getUI().push();
				}
				else
				{
					this.lblGenerandoEntradasCompras.setVisible(true);
					this.lblGenerandoEntradasCompras.setCaption(" Entradas Compras ");
					this.lblGenerandoEntradasCompras.setValue("?");
					this.lblGenerandoFabricacion.setVisible(true);
					this.lblGenerandoFabricacion.setCaption(" Movimientos Fabricación ");
					this.lblGenerandoFabricacion.setValue("?");
					this.lblGenerandoEntradasRegularizacion.setVisible(true);
					this.lblGenerandoEntradasRegularizacion.setCaption(" Entradas Regularización ");
					this.lblGenerandoEntradasRegularizacion.setValue("?");
					this.lblGenerandoEntradasTraslado.setVisible(true);
					this.lblGenerandoEntradasTraslado.setCaption(" Entradas Traslado ");
					this.lblGenerandoEntradasTraslado.setValue("?");
					this.lblGenerandoSalidasTraslado.setVisible(true);
					this.lblGenerandoSalidasTraslado.setCaption(" Salidas Traslado ");
					this.lblGenerandoSalidasTraslado.setValue("?");
					this.lblGenerandoSalidasRegularizacion.setVisible(true);
					this.lblGenerandoSalidasRegularizacion.setCaption(" Salidas Regularización ");
					this.lblGenerandoSalidasRegularizacion.setValue("?");
					
					this.lblGenerandoSalidasVentas.setVisible(true);
					this.lblGenerandoSalidasVentas.setCaption(" Salidas Ventas ");
					this.lblGenerandoSalidasVentas.setValue("?");

					/*
					 * activar en AM
					 */
					this.lblGenerandoUva.setVisible(true);
					this.lblGenerandoUva.setCaption(" Elaboracion Vino ");
					this.lblGenerandoUva.setValue("?");
					
					if (chkCargarInventarioInicial.getValue())
					{
						this.lblGenerandoInventarioInicial.setVisible(true);
						this.lblGenerandoInventarioInicial.setCaption(" Existencias Iniciales ");
						this.lblGenerandoInventarioInicial.setValue("?");
					}
					
					getUI().push();
					
					this.lblGenerandoEntradasCompras.setValue("");
					this.lblGenerandoEntradasCompras.addStyleName(ValoTheme.LABEL_SPINNER);
					getUI().push();
					
					rdo = this.cmss.generacionMovimientosEntradasCompraSilicie(declarante, ejercicio, mes);

					if (rdo==null)
					{
						this.lblGenerandoEntradasCompras.setValue(this.rdoOK);
						this.lblGenerandoEntradasCompras.addStyleName(ValoTheme.LABEL_SUCCESS);
						this.lblGenerandoEntradasCompras.setStyleName(ValoTheme.LABEL_SUCCESS);
						
						this.lblGenerandoFabricacion.setValue("");
						this.lblGenerandoFabricacion.addStyleName(ValoTheme.LABEL_SPINNER);
						getUI().push();
						rdo = this.cmss.generacionMovimientosFabricacionSilicie(declarante, ejercicio, mes);
					
						if (rdo == null)
						{
							this.lblGenerandoFabricacion.setValue(this.rdoOK);
							this.lblGenerandoFabricacion.addStyleName(ValoTheme.LABEL_SUCCESS);
							this.lblGenerandoFabricacion.setStyleName(ValoTheme.LABEL_SUCCESS);
							
							this.lblGenerandoEntradasRegularizacion.setValue("");
							this.lblGenerandoEntradasRegularizacion.addStyleName(ValoTheme.LABEL_SPINNER);
							getUI().push();
							rdo = this.cmss.generacionMovimientosEntradasRegularizacionSilicie(declarante, ejercicio, mes);
						
							if (rdo==null)
							{
								this.lblGenerandoEntradasRegularizacion.setValue(this.rdoOK);
								this.lblGenerandoEntradasRegularizacion.addStyleName(ValoTheme.LABEL_SUCCESS);
								this.lblGenerandoEntradasRegularizacion.setStyleName(ValoTheme.LABEL_SUCCESS);
								
								this.lblGenerandoSalidasRegularizacion.setValue("");
								this.lblGenerandoSalidasRegularizacion.addStyleName(ValoTheme.LABEL_SPINNER);
								getUI().push();			
								rdo = this.cmss.generacionMovimientosSalidasRegularizacionSilicie(declarante, ejercicio, mes);
							
								if (rdo==null)
								{
									this.lblGenerandoSalidasRegularizacion.setValue(this.rdoOK);
									this.lblGenerandoSalidasRegularizacion.addStyleName(ValoTheme.LABEL_SUCCESS);
									this.lblGenerandoSalidasRegularizacion.setStyleName(ValoTheme.LABEL_SUCCESS);
									
									this.lblGenerandoEntradasTraslado.setValue("");
									this.lblGenerandoEntradasTraslado.addStyleName(ValoTheme.LABEL_SPINNER);
									getUI().push();
									rdo = this.cmss.generacionMovimientosEntradasTrasladoSilicie(declarante, ejercicio, mes);

									if (rdo==null)
									{
										this.lblGenerandoEntradasTraslado.setValue(this.rdoOK);
										this.lblGenerandoEntradasTraslado.addStyleName(ValoTheme.LABEL_SUCCESS);
										this.lblGenerandoEntradasTraslado.setStyleName(ValoTheme.LABEL_SUCCESS);
										
										this.lblGenerandoSalidasTraslado.setValue("");
										this.lblGenerandoSalidasTraslado.addStyleName(ValoTheme.LABEL_SPINNER);
										getUI().push();
										rdo = this.cmss.generacionMovimientosSalidasTrasladoSilicie(declarante, ejercicio, mes);
									
										if (rdo==null)
										{
											this.lblGenerandoSalidasTraslado.setValue(this.rdoOK);
											this.lblGenerandoSalidasTraslado.addStyleName(ValoTheme.LABEL_SUCCESS);
											this.lblGenerandoSalidasTraslado.setStyleName(ValoTheme.LABEL_SUCCESS);
											
											this.lblGenerandoSalidasVentas.setValue("");
											this.lblGenerandoSalidasVentas.addStyleName(ValoTheme.LABEL_SPINNER);
											getUI().push();
											rdo = this.cmss.generacionMovimientosSalidasVentaSilicie(declarante, ejercicio, mes);
										
											if (rdo==null)
											{
												this.lblGenerandoSalidasVentas.setValue(this.rdoOK);
												this.lblGenerandoSalidasVentas.addStyleName(ValoTheme.LABEL_SUCCESS);
												this.lblGenerandoSalidasVentas.setStyleName(ValoTheme.LABEL_SUCCESS);
											}
											else
											{
												this.lblGenerandoSalidasVentas.setValue(this.rdoKo);
												this.lblGenerandoSalidasVentas.addStyleName(ValoTheme.LABEL_FAILURE);
												this.lblGenerandoSalidasVentas.setStyleName(ValoTheme.LABEL_FAILURE);					
											}

										}
										else
										{
											this.lblGenerandoSalidasTraslado.setValue(this.rdoKo);
											this.lblGenerandoSalidasTraslado.addStyleName(ValoTheme.LABEL_FAILURE);
											this.lblGenerandoSalidasTraslado.setStyleName(ValoTheme.LABEL_FAILURE);					
										}
						

									}
									else
									{
										this.lblGenerandoEntradasTraslado.setValue(this.rdoKo);
										this.lblGenerandoEntradasTraslado.addStyleName(ValoTheme.LABEL_FAILURE);
										this.lblGenerandoEntradasTraslado.setStyleName(ValoTheme.LABEL_FAILURE);					
									}
					

								}
								else
								{
									this.lblGenerandoSalidasRegularizacion.setValue(this.rdoKo);
									this.lblGenerandoSalidasRegularizacion.addStyleName(ValoTheme.LABEL_FAILURE);
									this.lblGenerandoSalidasRegularizacion.setStyleName(ValoTheme.LABEL_FAILURE);					
								}

							}
							else
							{
								this.lblGenerandoEntradasRegularizacion.setValue(this.rdoKo);
								this.lblGenerandoEntradasRegularizacion.addStyleName(ValoTheme.LABEL_FAILURE);
								this.lblGenerandoEntradasRegularizacion.setStyleName(ValoTheme.LABEL_FAILURE);					
							}

						}
						else
						{
							this.lblGenerandoFabricacion.setValue(this.rdoKo);
							this.lblGenerandoFabricacion.addStyleName(ValoTheme.LABEL_FAILURE);
							this.lblGenerandoFabricacion.setStyleName(ValoTheme.LABEL_FAILURE);					
						}

					}
					else
					{
						this.lblGenerandoEntradasCompras.setValue(this.rdoKo);
						this.lblGenerandoEntradasCompras.addStyleName(ValoTheme.LABEL_FAILURE);
						this.lblGenerandoEntradasCompras.setStyleName(ValoTheme.LABEL_FAILURE);					
					}

	

					/*
					 * activar en AM
					 */
//				rdo = this.cmss.generacionMovimientosElaboracionVinoSilicie(declarante, ejercicio, mes);
//					
//					if (rdo==null)
//					{
//						this.lblGenerandoUva.setValue(this.rdoOK);
//						this.lblGenerandoUva.addStyleName(ValoTheme.LABEL_SUCCESS);
//						this.lblGenerandoUva.setStyleName(ValoTheme.LABEL_SUCCESS);
//					}
//					else
//					{
//						this.lblGenerandoUva.setValue(this.rdoKo);
//						this.lblGenerandoUva.addStyleName(ValoTheme.LABEL_FAILURE);
//						this.lblGenerandoUva.setStyleName(ValoTheme.LABEL_FAILURE);					
//					}
					
					
				}
			}		
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
			else
			{
				Notificaciones.getInstance().mensajeInformativo("Actualizando datos en pantalla.");
				if (origen.isHayGrid())
				{			
					origen.grid.removeAllColumns();			
					origen.barAndGridLayout.removeComponent(origen.grid);
					origen.grid=null;		
					origen.setHayGrid(false);
				}
				origen.generarGrid(origen.opcionesEscogidas);
				close();
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Selecciona correctamente ejercicio y mes para la generacion de los datos.");
		}
		
	}
	
	public void cancelarProceso(String r_accion)
	{
		
		this.chkComprobarCodigoNC.setVisible(true);
		this.chkComprobarEntradasEMCS.setVisible(true);
		this.chkComprobarFactorArticulos.setVisible(true);
//		this.chkComprobarFechas.setVisible(true);
		this.chkComprobarGrado.setVisible(true);
		this.chkComprobarMes.setVisible(true);
		this.chkComprobarARC.setVisible(true);
		this.chkComprobarNR.setVisible(true);
		this.chkComprobarCaes.setVisible(true);
		this.chkComprobarValorGrado.setVisible(true);
		this.chkComprobarSalidasEMCS.setVisible(true);
		this.chkComprobarTrasladosEMCS.setVisible(true);

		this.lblComprobarMes.setVisible(true);
		this.lblComprobarCaes.setVisible(true);
		this.lblComprobarARC.setVisible(true);
		this.lblComprobarNR.setVisible(true);
		this.lblComprobarFactorArticulos.setVisible(true);
		this.lblComprobarValorGrado.setVisible(true);
		this.lblComprobarCodigoNC.setVisible(true);
		this.lblComprobarEntradas.setVisible(true);
		this.lblComprobarGrado.setVisible(true);
//		this.lblComprobarFechas.setVisible(true);
		this.lblComprobarSalidas.setVisible(true);
		this.lblComprobarTraslados.setVisible(true);

		this.lblComprobarMes.setValue("?");
		this.lblComprobarMes.setStyleName("deColor");
		this.lblComprobarCodigoNC.setValue("?");
		this.lblComprobarCodigoNC.setStyleName("deColor");
		this.lblComprobarEntradas.setValue("?");
		this.lblComprobarEntradas.setStyleName("deColor");
		this.lblComprobarGrado.setValue("?");
		this.lblComprobarGrado.setStyleName("deColor");
		
		this.lblComprobarClientes.setValue("?");
		this.lblComprobarClientes.setStyleName("deColor");
		
//		this.lblComprobarFechas.setValue("?");
//		this.lblComprobarFechas.setStyleName("deColor");
		this.lblComprobarSalidas.setValue("?");
		this.lblComprobarSalidas.setStyleName("deColor");
		this.lblComprobarTraslados.setValue("?");
		this.lblComprobarTraslados.setStyleName("deColor");

		this.chkCargarInventarioInicial.setVisible(false);
		this.btnBotonAVentana.setEnabled(false);
		this.chequeado=false;

	}
}