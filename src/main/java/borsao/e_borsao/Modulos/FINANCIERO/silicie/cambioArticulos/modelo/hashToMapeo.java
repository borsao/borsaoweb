package borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoCambioArticulo mapeo = null;
	 
	 public MapeoCambioArticulo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoCambioArticulo();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setArticulo(r_hash.get("articulo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setNuevoNC(r_hash.get("nuevoNc"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setNuevoArticulo(r_hash.get("nuevoArticulo"));
		 this.mapeo.setNuevaDescripcion(r_hash.get("nuevaDescripcion"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoCambioArticulo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoCambioArticulo();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setArticulo(r_hash.get("articulo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setNuevoNC(r_hash.get("nuevoNc"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setNuevoArticulo(r_hash.get("nuevoArticulo"));
		 this.mapeo.setNuevaDescripcion(r_hash.get("nuevaDescripcion"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoCambioArticulo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("articulo", r_mapeo.getArticulo());		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 this.hash.put("nuevoNC", r_mapeo.getNuevoNC());
		 this.hash.put("lote", r_mapeo.getLote());
		 this.hash.put("nuevoArticulo", r_mapeo.getNuevoArticulo());
		 this.hash.put("nuevaDescripcion", r_mapeo.getNuevaDescripcion());
		 return hash;		 
	 }
}