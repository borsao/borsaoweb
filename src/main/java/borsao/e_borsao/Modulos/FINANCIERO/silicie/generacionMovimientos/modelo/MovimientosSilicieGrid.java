package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view.pantallaLineasAlbaranesTraslados;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.view.pantallaLineasAlbaranesCompra;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.pantallaLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view.MovimientosSilicieView;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.view.pantallaLineasAlbaranesFabricacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class MovimientosSilicieGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean conTotales = true;	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	private GridViewRefresh app = null;
	
    public MovimientosSilicieGrid(MovimientosSilicieView r_app, ArrayList<MapeoMovimientosSilicie> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo(((MovimientosSilicieView)this.app).tituloGrid);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoMovimientosSilicie.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    

    public void doEditItem() 
    {
    }
  
  @Override
  	public void doCancelEditor()
  	{
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("documentoFalso","idAnulacion", "doc","lin", "fechaPresentacion", "numeroAsiento", "ejercicio", "mes", "numeroRefInterno", "fechaMovimiento", "fechaRegistro", "signo", "descripcionMovimiento", "descripcionDiferenciaMenos", "descripcionRegimenFiscal", "documento", "albaran", "posicion","codigoArticulo", "descripcionArticulo", "codigoNC", "descripcion", "litros", "grado", "alcoholPuro", "numeroJustificante", "codigoDocumentoOd", "numeroDocumentoIdOD", "razonSocialOD", "caeOD", "numeroAsientoReentrada","codigoOperacionAnterior");	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("mes", "45");
    	this.widthFiltros.put("signo", "45");
    	this.widthFiltros.put("numeroJutificante", "85");
    	this.widthFiltros.put("documento", "40");
    	this.widthFiltros.put("albaran", "80");
    	this.widthFiltros.put("codigoNC", "80");
    	this.widthFiltros.put("descripcion", "200");
    	this.widthFiltros.put("codigoArticulo", "80");
    	this.widthFiltros.put("descripcionArticulo", "200");
    	this.widthFiltros.put("razonSocialOD", "260");
    	this.widthFiltros.put("caeOD", "140");    	
    	this.widthFiltros.put("descripcionMovimiento", "85");
    	this.widthFiltros.put("descripcionDiferenciaMenos", "85");
    	this.widthFiltros.put("descripcionRegimenFiscal", "85");
    	this.widthFiltros.put("numeroRefInterno", "110");
    	this.widthFiltros.put("codigoDocumentoOd", "85");
    	this.widthFiltros.put("numeroDocumentoIdOD", "110");
    	
    	this.getColumn("doc").setHeaderCaption("");
    	this.getColumn("doc").setSortable(false);
    	this.getColumn("doc").setWidth(new Double(50));
    	
    	this.getColumn("lin").setHeaderCaption("");
    	this.getColumn("lin").setSortable(false);
    	this.getColumn("lin").setWidth(new Double(50));

    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(95);

    	this.getColumn("mes").setHeaderCaption("Mes");
    	this.getColumn("mes").setSortable(false);
    	this.getColumn("mes").setWidth(new Double(85));

    	this.getColumn("numeroRefInterno").setHeaderCaption("Referencia Interna");
    	this.getColumn("numeroRefInterno").setSortable(false);
    	this.getColumn("numeroRefInterno").setWidth(new Double(150));

    	this.getColumn("fechaMovimiento").setHeaderCaption("FECHA");
    	this.getColumn("fechaMovimiento").setWidthUndefined();
    	this.getColumn("fechaMovimiento").setWidth(120);

    	this.getColumn("fechaRegistro").setHeaderCaption("Registro");
    	this.getColumn("fechaRegistro").setWidthUndefined();
    	this.getColumn("fechaRegistro").setWidth(120);
    	
    	this.getColumn("signo").setHeaderCaption("+/-");
    	this.getColumn("signo").setSortable(false);
    	this.getColumn("signo").setWidth(new Double(85));

    	this.getColumn("descripcionMovimiento").setHeaderCaption("Movimiento");
    	this.getColumn("descripcionMovimiento").setWidth(125);

    	this.getColumn("descripcionRegimenFiscal").setHeaderCaption("Regimen Fiscal");
    	this.getColumn("descripcionRegimenFiscal").setWidth(125);

    	this.getColumn("descripcionDiferenciaMenos").setHeaderCaption("Diferencia Menos");
    	this.getColumn("descripcionDiferenciaMenos").setWidth(125);

    	
    	this.getColumn("documento").setHeaderCaption("Doc.");
    	this.getColumn("documento").setSortable(false);
    	this.getColumn("documento").setWidth(new Double(80));
		
    	this.getColumn("albaran").setHeaderCaption("Codigo");
    	this.getColumn("albaran").setSortable(false);
    	this.getColumn("albaran").setWidth(new Double(120));

    	this.getColumn("posicion").setHeaderCaption("Posicion");
    	this.getColumn("posicion").setSortable(false);
    	this.getColumn("posicion").setWidth(new Double(80));

    	this.getColumn("codigoNC").setHeaderCaption("Codigo NC");
    	this.getColumn("codigoNC").setWidth(120);
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(240);
    	this.getColumn("codigoArticulo").setHeaderCaption("Articulo");
    	this.getColumn("codigoArticulo").setWidth(120);
    	this.getColumn("descripcionArticulo").setHeaderCaption("Descripcion");
    	this.getColumn("descripcionArticulo").setWidth(240);
    	this.getColumn("litros").setHeaderCaption("Litros");
    	this.getColumn("litros").setWidth(120);
    	this.getColumn("grado").setHeaderCaption("Grado");
    	this.getColumn("grado").setWidth(85);
    	this.getColumn("alcoholPuro").setHeaderCaption("Alcohol");
    	this.getColumn("alcoholPuro").setWidth(120);
    	
    	this.getColumn("caeOD").setHeaderCaption("CAE");
    	this.getColumn("caeOD").setWidth(180);
    	this.getColumn("razonSocialOD").setHeaderCaption("Tercero");
    	this.getColumn("razonSocialOD").setWidth(300);

    	this.getColumn("numeroJustificante").setHeaderCaption("N. Justificante");
    	this.getColumn("numeroJustificante").setWidth(250);

    	this.getColumn("codigoDocumentoOd").setHeaderCaption("Tipo");
    	this.getColumn("codigoDocumentoOd").setWidth(125);
    	
    	this.getColumn("numeroDocumentoIdOD").setHeaderCaption("NIF");
    	this.getColumn("numeroDocumentoIdOD").setWidth(125);
    	
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("epigrafe").setHidden(true);
    	this.getColumn("codigoEpigrafe").setHidden(true);
    	this.getColumn("clave").setHidden(true);
    	this.getColumn("numeroDocumentoIdREP").setHidden(true);
    	this.getColumn("razonSocialREP").setHidden(true);
    	this.getColumn("claveTabla").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("documentoFalso").setHidden(true);
    	this.getColumn("idAnulacion").setHidden(true);
    	this.getColumn("posicion").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("mes").setHidden(true);
    	
    	this.getColumn("descripcionDocumentoRep").setHidden(true);
    	this.getColumn("descripcionDocumentoOd").setHidden(true);
    	this.getColumn("descripcionJustificante").setHidden(true);
    	this.getColumn("caeTitular").setHidden(true);
    	this.getColumn("descripcionOperacion").setHidden(true);
    	this.getColumn("unidad_medida").setHidden(true);
		
    	this.getColumn("numeroAsientoReentrada").setHidden(true);
		this.getColumn("codigoOperacionAnterior").setHidden(true);
		
    	this.getColumn("idPresentador").setHidden(true);
		this.getColumn("idCodigoDocumentoRep").setHidden(true);
    	this.getColumn("idCodigoJustificante").setHidden(true);
    	this.getColumn("idCodigoRegimenFiscal").setHidden(true);
    	this.getColumn("idCodigoOperacion").setHidden(true);
		this.getColumn("idCodigoMovimiento").setHidden(true);
		this.getColumn("idCodigoDiferenciaMenos").setHidden(true);
		if (((MovimientosSilicieView) this.app).cmbEstado.getValue().toString().equals("Pendientes"))
		{
			this.getColumn("fechaPresentacion").setHidden(true);
			this.getColumn("numeroAsiento").setHidden(true);
		}
		else
		{
			this.getColumn("fechaPresentacion").setHidden(false);
			this.getColumn("numeroAsiento").setHidden(false);			
		}
		
//		this.getColumn("signo").setHidden(true);
		this.getColumn("codigoDocumentoRep").setHidden(true);
    	this.getColumn("idCodigoDocumentoOd").setHidden(true);
    	this.getColumn("codigoJustificante").setHidden(true);
    	this.getColumn("codigoRegimenFiscal").setHidden(true);
    	this.getColumn("codigoOperacion").setHidden(true);
		this.getColumn("codigoMovimiento").setHidden(true);
		this.getColumn("codigoDiferenciaMenos").setHidden(true);
		
    	this.getColumn("fechaMovimiento").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaRegistro").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaPresentacion").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoMovimientosSilicie) {

            	if ("doc".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Documento Origen";
                }
            	else if ("lin".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Datos Anulacion";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estilo = null;
    		String relleno = null;
    		String anulado = null;
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ( "documentoFalso".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().length()>0)
            			relleno = "S";
            		else
            			relleno = "N";
            	}
            	if ( "idAnulacion".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null &&  (Integer) cellReference.getValue()>0)
            			anulado = "S";
            		else
            			anulado = "N";
            	}
            	if ( "doc".equals(cellReference.getPropertyId()))
            	{
            		if (relleno=="S") estilo = "nativebuttonLineas";
            	}            	
            	else if ( "lin".equals(cellReference.getPropertyId()))
            	{
            		if (anulado=="S") estilo = "nativebuttonRed"; else estilo =  "cell-normal";
            	}            	
            	else if ( "alcoholPuro".equals(cellReference.getPropertyId()) || "litros".equals(cellReference.getPropertyId()) || "mes".equals(cellReference.getPropertyId()) || "grado".equals(cellReference.getPropertyId())|| "albaran".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId()))
            	{
            		estilo =  "Rcell-normal";
            	}
            	else estilo =  "cell-normal";
            	
            	return estilo;
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoMovimientosSilicie mapeo = (MapeoMovimientosSilicie) event.getItemId();
            	
            	if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("doc"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
            		String almacen = cas.obtenerAlmacenPorCAE(mapeo.getCaeTitular());
            		
            		pantallaLineasAlbaranesCompra vtc = null;
            		pantallaLineasAlbaranesFabricacion vtf = null;
            		pantallaLineasAlbaranesVentas vta = null;
            		pantallaLineasAlbaranesTraslados vtt = null;
            		pantallaLineasAlbaranesTraslados vtr = null;
            		
            		if (mapeo.getClaveTabla()!=null && mapeo.getClaveTabla().toUpperCase().equals("E") && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0)
            		{
            			vtc = new pantallaLineasAlbaranesCompra(null, mapeo.getEjercicio(), "Lineas Albaran Entrada " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getDocumento(), mapeo.getAlbaran(), almacen);
            			getUI().addWindow(vtc);	            			
            		}
            		else if (mapeo.getClaveTabla()!=null && mapeo.getClaveTabla().toUpperCase().equals("T") && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0)
            		{
            			vtt = new pantallaLineasAlbaranesTraslados(app, "Lineas Albaran Traslado " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
            			getUI().addWindow(vtt);	            			
            		}
            		else if (mapeo.getClaveTabla()!=null && (mapeo.getClaveTabla().toUpperCase().equals("A") || mapeo.getClaveTabla().toUpperCase().equals("F")) && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0)
            		{
            			vta = new pantallaLineasAlbaranesVentas(app, "Lineas Albaran Venta" + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
            			getUI().addWindow(vta);
            		}
            		if (mapeo.getClaveTabla()!=null && mapeo.getClaveTabla().toUpperCase().equals("B") && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0)
            		{
            			vtf = new pantallaLineasAlbaranesFabricacion("Lineas Fabricación " + mapeo.getDocumento() + " - " + mapeo.getFechaMovimiento(), mapeo.getDocumento(), mapeo.getFechaMovimiento(), almacen, mapeo.getCodigoNC());
            			getUI().addWindow(vtf);	            			
            		}
            		if (mapeo.getClaveTabla()!=null && mapeo.getClaveTabla().toUpperCase().equals("R") && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0)
            		{
            			vtr = new pantallaLineasAlbaranesTraslados(app, "Ajustes Fabricación " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
            			getUI().addWindow(vtr);	            			
            		}
            	}
            	else if (event.getPropertyId()!=null && event.getPropertyId().toString().equals("lin"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getIdAnulacion()!=null && mapeo.getIdAnulacion()>0) Notificaciones.getInstance().mensajeAdvertencia("Efectivamente, lo anulaste y ahora lo tienes que corregir y declarar");
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("doc");
		this.camposNoFiltrar.add("lin");
		this.camposNoFiltrar.add("fechaMovimiento");
		this.camposNoFiltrar.add("fechaPresentacion");
		this.camposNoFiltrar.add("fechaRegistro");
		this.camposNoFiltrar.add("posicion");
		this.camposNoFiltrar.add("litros");
		this.camposNoFiltrar.add("grado");
		this.camposNoFiltrar.add("alcoholPuro");
		this.camposNoFiltrar.add("tipoJustificante");		
		this.camposNoFiltrar.add("numeroJustificante");
		this.camposNoFiltrar.add("numeroAsiento");
//		this.camposNoFiltrar.add("numeroDocumentoIdOD");
//		this.camposNoFiltrar.add("razonSocialOD");
		this.camposNoFiltrar.add("CAEOD");
	}

	public void generacionPdf(MapeoMovimientosSilicie r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double totalLitros = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
	        	Double q1Value = (Double) item.getItemProperty("litros").getValue();
	        	String q1Sig = (String) item.getItemProperty("signo").getValue();
	        	if (q1Value!=null && q1Sig!= null && q1Sig.equals("+")) totalLitros += q1Value;
	        	if (q1Value!=null && q1Sig!= null && q1Sig.equals("-")) totalLitros -= q1Value;
        	}
        }
        
        footer.getCell("mes").setText("Totales");
		footer.getCell("litros").setText(RutinasNumericas.formatearDouble(totalLitros).toString());
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("litros").setStyleName("Rcell-pie");
	}
}



