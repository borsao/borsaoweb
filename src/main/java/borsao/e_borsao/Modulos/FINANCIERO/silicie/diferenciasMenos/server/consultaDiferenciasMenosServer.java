package borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.modelo.MapeoDiferenciasMenos;

public class consultaDiferenciasMenosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDiferenciasMenosServer instance;
	
	public consultaDiferenciasMenosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDiferenciasMenosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDiferenciasMenosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoDiferenciasMenos> datosDiferenciasMenosGlobal(MapeoDiferenciasMenos r_mapeo)
	{
		ResultSet rsDiferenciasMenos = null;		
		ArrayList<MapeoDiferenciasMenos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_dme.id dme_id, ");
		cadenaSQL.append(" fin_silicie_dme.codigo dme_cod, ");
		cadenaSQL.append(" fin_silicie_dme.descripcion dme_des ");
     	cadenaSQL.append(" FROM fin_silicie_dme ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsDiferenciasMenos= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoDiferenciasMenos>();
			
			while(rsDiferenciasMenos.next())
			{
				MapeoDiferenciasMenos mapeoDiferenciasMenos = new MapeoDiferenciasMenos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoDiferenciasMenos.setIdCodigo(rsDiferenciasMenos.getInt("dme_id"));
				mapeoDiferenciasMenos.setCodigo(rsDiferenciasMenos.getString("dme_cod"));
				mapeoDiferenciasMenos.setDescripcion(rsDiferenciasMenos.getString("dme_des"));
				vector.add(mapeoDiferenciasMenos);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsDiferenciasMenos = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_dme.id) dme_sig ");
     	cadenaSQL.append(" FROM fin_silicie_dme ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsDiferenciasMenos= cs.executeQuery(cadenaSQL.toString());
			
			while(rsDiferenciasMenos.next())
			{
				return rsDiferenciasMenos.getInt("dme_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoDiferenciasMenos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_dme ( ");
    		cadenaSQL.append(" fin_silicie_dme.id, ");
    		cadenaSQL.append(" fin_silicie_dme.codigo, ");
    		cadenaSQL.append(" fin_silicie_dme.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoDiferenciasMenos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_dme set ");
			cadenaSQL.append(" fin_silicie_dme.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_dme.descripcion=? ");
		    cadenaSQL.append(" WHERE fin_silicie_dme.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoDiferenciasMenos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_dme ");            
			cadenaSQL.append(" WHERE fin_silicie_dme.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}