package borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDiferenciasMenos extends MapeoGlobal
{
	private String codigo;
	private String descripcion;

	public MapeoDiferenciasMenos()
	{
		this.setCodigo("");
		this.setDescripcion("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}