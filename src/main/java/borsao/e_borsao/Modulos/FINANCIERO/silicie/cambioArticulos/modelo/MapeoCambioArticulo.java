package borsao.e_borsao.Modulos.FINANCIERO.silicie.cambioArticulos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCambioArticulo extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String nuevoNC;
	private String lote;
	private String nuevoArticulo;
	private String nuevaDescripcion;
	
	public MapeoCambioArticulo()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setNuevoNC("");
		this.setNuevoArticulo("");
		this.setLote("");
		this.setNuevaDescripcion("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNuevoNC() {
		return nuevoNC;
	}

	public void setNuevoNC(String nuevoNC) {
		this.nuevoNC = nuevoNC;
	}

	public String getNuevoArticulo() {
		return nuevoArticulo;
	}

	public void setNuevoArticulo(String nuevoArticulo) {
		this.nuevoArticulo = nuevoArticulo;
	}

	public String getNuevaDescripcion() {
		return nuevaDescripcion;
	}

	public void setNuevaDescripcion(String nuevaDescripcion) {
		this.nuevaDescripcion = nuevaDescripcion;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}


}