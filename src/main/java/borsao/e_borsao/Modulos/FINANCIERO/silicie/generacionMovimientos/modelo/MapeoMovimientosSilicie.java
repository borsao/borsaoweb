package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo;


import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoMovimientosSilicie extends MapeoGlobal
{
	
	private Integer mes;
	private String documentoFalso;
	private String doc;
	private String lin;
	private String numeroDocumentoIdREP;
	private String razonSocialREP;
	
	private Double grado;
	private Double litros;
	private Double alcoholPuro;
	private Integer ejercicio;
	private Integer posicion;
	private Integer albaran;
	private String numeroAsiento;
	private String numeroAsientoReentrada;
	private String numeroRefInterno;
	private String numeroJustificante;
	private String numeroDocumentoIdOD;
	private String razonSocialOD;
	private String caeOD;
	private String codigoNC;
	private String descripcion;
	private String codigoArticulo;
	private String descripcionArticulo;
	
	private String documento;
	private String claveTabla;
	private String serie;
	private String signo;
	
	private String epigrafe;
	private String codigoEpigrafe;
	private String clave;
	private String unidad_medida;
	private String estado;
	
//	private String esc;
//	private String idPrg;

	private Date fechaMovimiento;
	private Date fechaRegistro;
	private Date fechaPresentacion;
	
	private String caeTitular;
	
	private String codigoDocumentoRep;
	private String codigoDocumentoOd;
	private String codigoJustificante;
	private String codigoRegimenFiscal;
	private String codigoOperacionAnterior;
	private String codigoOperacion;
	private String codigoMovimiento;
	private String codigoDiferenciaMenos;
	
	private String descripcionDocumentoRep;
	private String descripcionDocumentoOd;
	private String descripcionJustificante;
	private String descripcionRegimenFiscal;
	private String descripcionOperacion;
	private String descripcionMovimiento;
	private String descripcionDiferenciaMenos;

	private Integer idCodigoDocumentoRep;
	private Integer idCodigoDocumentoOd;
	private Integer idCodigoJustificante;
	private Integer idCodigoRegimenFiscal;
	private Integer idPresentador;

	private Integer idCodigoOperacion;
	private Integer idCodigoMovimiento;
	private Integer idAnulacion;
	private Integer idCodigoDiferenciaMenos;

	public MapeoMovimientosSilicie()
	{
		this.setDoc("");
		
		this.setNumeroJustificante("");		
		this.setCaeOD("");
		this.setNumeroDocumentoIdOD("");
		this.setRazonSocialOD("");
		
		this.setNumeroDocumentoIdREP("");
		this.setRazonSocialREP("");

		this.setClave("");
		this.setCodigoNC("");		
		this.setDescripcion("");
		
		this.setCodigoEpigrafe("V0");		
		this.setEpigrafe("1");
		this.setUnidad_medida("LTR");
	}

	public Double getLitros() {
		return litros;
	}

	public void setLitros(Double litros) {
		this.litros = litros;
	}

	public Double getGrado() {
		return grado;
	}

	public void setGrado(Double grado) {
		this.grado = grado;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public String getNumeroJustificante() {
		return numeroJustificante;
	}

	public void setNumeroJustificante(String numeroJustificante) {
		this.numeroJustificante = numeroJustificante;
	}

	public String getNumeroDocumentoIdOD() {
		if (numeroDocumentoIdOD!=null && numeroDocumentoIdOD.length()>0)
			return numeroDocumentoIdOD;
		else
			return eBorsao.strCIF;
	}

	public void setNumeroDocumentoIdOD(String numeroDocumentoIdOD) {
		this.numeroDocumentoIdOD = numeroDocumentoIdOD;
	}

	public String getRazonSocialOD() {
		if (razonSocialOD!=null && razonSocialOD.length()>0)
			return razonSocialOD;
		else
			return eBorsao.strEmpresa;
	}

	public void setRazonSocialOD(String razonSocialOD) {
		this.razonSocialOD = razonSocialOD;
	}

	public String getCaeOD() {
		return caeOD;
	}

	public void setCaeOD(String caeOD) {
		this.caeOD = caeOD;
	}

	public String getNumeroDocumentoIdREP() {
		return numeroDocumentoIdREP;
	}

	public void setNumeroDocumentoIdREP(String numeroDocumentoIdREP) {
		this.numeroDocumentoIdREP = numeroDocumentoIdREP;
	}

	public String getRazonSocialREP() {
		return razonSocialREP;
	}

	public void setRazonSocialREP(String razonSocialREP) {
		this.razonSocialREP = razonSocialREP;
	}

	public String getEpigrafe() {
		return epigrafe;
	}

	public void setEpigrafe(String epigrafe) {
		this.epigrafe = epigrafe;
	}

	public String getCodigoEpigrafe() {
		return codigoEpigrafe;
	}

	public void setCodigoEpigrafe(String codigoEpigrafe) {
		this.codigoEpigrafe = codigoEpigrafe;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getCodigoNC() {
		return codigoNC;
	}

	public void setCodigoNC(String codigoNC) {
		this.codigoNC = codigoNC;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUnidad_medida() {
		return unidad_medida;
	}

	public void setUnidad_medida(String unidad_medida) {
		this.unidad_medida = unidad_medida;
	}

	public String getDocumentoFalso() {
		return documentoFalso;
	}

	public void setDocumentoFalso(String documentoFalso) {
		this.documentoFalso = documentoFalso;
	}

	public String getClaveTabla() {
		return claveTabla;
	}

	public void setClaveTabla(String claveTabla) {
		this.claveTabla = claveTabla;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getAlbaran() {
		return albaran;
	}

	public void setAlbaran(Integer albaran) {
		this.albaran = albaran;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public Date getFechaMovimiento() {
		return fechaMovimiento;
	}

	public Integer getIdCodigoDocumentoRep() {
		return idCodigoDocumentoRep;
	}

	public void setIdCodigoDocumentoRep(Integer idCodigoDocumentoRep) {
		this.idCodigoDocumentoRep = idCodigoDocumentoRep;
	}

	public Integer getIdCodigoDocumentoOd() {
		return idCodigoDocumentoOd;
	}

	public void setIdCodigoDocumentoOd(Integer idCodigoDocumentoOd) {
		this.idCodigoDocumentoOd = idCodigoDocumentoOd;
	}

	public Integer getIdCodigoJustificante() {
		return idCodigoJustificante;
	}

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	public void setIdCodigoJustificante(Integer idCodigoJustificante) {
		this.idCodigoJustificante = idCodigoJustificante;
	}

	public Integer getIdCodigoRegimenFiscal() {
		return idCodigoRegimenFiscal;
	}

	public void setIdCodigoRegimenFiscal(Integer idCodigoRegimenFiscal) {
		this.idCodigoRegimenFiscal = idCodigoRegimenFiscal;
	}

	public Integer getIdPresentador() {
		return idPresentador;
	}

	public void setIdPresentador(Integer idPresentador) {
		this.idPresentador = idPresentador;
	}

	public Integer getIdCodigoOperacion() {
		return idCodigoOperacion;
	}

	public void setIdCodigoOperacion(Integer idCodigoOperacion) {
		this.idCodigoOperacion = idCodigoOperacion;
	}

	public Integer getIdCodigoMovimiento() {
		return idCodigoMovimiento;
	}

	public void setIdCodigoMovimiento(Integer idCodigoMovimiento) {
		this.idCodigoMovimiento = idCodigoMovimiento;
	}

	public Integer getIdCodigoDiferenciaMenos() {
		return idCodigoDiferenciaMenos;
	}

	public void setIdCodigoDiferenciaMenos(Integer idCodigoDiferenciaMenos) {
		this.idCodigoDiferenciaMenos = idCodigoDiferenciaMenos;
	}

	public void setFechaMovimiento(Date fecha) {
		this.fechaMovimiento = fecha;
	}

	public String getCaeTitular() {
		return caeTitular;
	}

	public void setCaeTitular(String caeExpedidor) {
		this.caeTitular = caeExpedidor;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaPresentacion() {
		return fechaPresentacion;
	}

	public void setFechaPresentacion(Date fechaPresentacion) {
		this.fechaPresentacion = fechaPresentacion;
	}

	public String getDescripcionDocumentoRep() {
		return descripcionDocumentoRep;
	}

	public void setDescripcionDocumentoRep(String codigoDocumentoRep) {
		this.descripcionDocumentoRep = codigoDocumentoRep;
	}

	public String getDescripcionDocumentoOd() {
		return descripcionDocumentoOd;
	}

	public void setDescripcionDocumentoOd(String codigoDocumentoOd) {
		this.descripcionDocumentoOd = codigoDocumentoOd;
	}

	public String getDescripcionJustificante() {
		return descripcionJustificante;
	}

	public void setDescripcionJustificante(String codigoJustificante) {
		this.descripcionJustificante = codigoJustificante;
	}

	public String getDescripcionRegimenFiscal() {
		return descripcionRegimenFiscal;
	}

	public void setDescripcionRegimenFiscal(String codigoRegimenFiscal) {
		this.descripcionRegimenFiscal = codigoRegimenFiscal;
	}

	public String getDescripcionOperacion() {
		return descripcionOperacion;
	}

	public void setDescripcionOperacion(String codigoOperacion) {
		this.descripcionOperacion = codigoOperacion;
	}

	public String getDescripcionMovimiento() {
		return descripcionMovimiento;
	}

	public void setDescripcionMovimiento(String codigoMovimiento) {
		this.descripcionMovimiento = codigoMovimiento;
	}

	public String getDescripcionDiferenciaMenos() {
		return descripcionDiferenciaMenos;
	}

	public void setDescripcionDiferenciaMenos(String codigoDiferenciaMenos) {
		this.descripcionDiferenciaMenos = codigoDiferenciaMenos;
	}

	public String getNumeroRefInterno() {
		return numeroRefInterno;
	}

	public void setNumeroRefInterno(String numeroRefInterno) {
		this.numeroRefInterno = numeroRefInterno;
	}

	public String getCodigoDocumentoRep() {
		return codigoDocumentoRep;
	}

	public void setCodigoDocumentoRep(String codigoDocumentoRep) {
		this.codigoDocumentoRep = codigoDocumentoRep;
	}

	public String getCodigoDocumentoOd() {
		if (codigoDocumentoOd!=null && codigoDocumentoOd.length()>0)
			return codigoDocumentoOd;
		else
			return "3";
	}

	public void setCodigoDocumentoOd(String codigoDocumentoOd) {
		this.codigoDocumentoOd = codigoDocumentoOd;
	}

	public String getCodigoJustificante() {
		return codigoJustificante;
	}

	public void setCodigoJustificante(String codigoJustificante) {
		this.codigoJustificante = codigoJustificante;
	}

	public String getCodigoRegimenFiscal() {
		return codigoRegimenFiscal;
	}

	public void setCodigoRegimenFiscal(String codigoRegimenFiscal) {
		this.codigoRegimenFiscal = codigoRegimenFiscal;
	}

	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	public String getCodigoMovimiento() {
		return codigoMovimiento;
	}

	public void setCodigoMovimiento(String codigoMovimiento) {
		this.codigoMovimiento = codigoMovimiento;
	}

	public Integer getIdAnulacion() {
		return idAnulacion;
	}

	public void setIdAnulacion(Integer idAnulacion) {
		this.idAnulacion = idAnulacion;
	}

	public String getCodigoDiferenciaMenos() {
		return codigoDiferenciaMenos;
	}

	public void setCodigoDiferenciaMenos(String codigoDiferenciaMenos) {
		this.codigoDiferenciaMenos = codigoDiferenciaMenos;
	}

	public Double getAlcoholPuro() {
		return alcoholPuro;
	}

	public void setAlcoholPuro(Double alcoholPuro) {
		this.alcoholPuro = alcoholPuro;
	}

	public String getNumeroAsiento() {
		return numeroAsiento;
	}

	public void setNumeroAsiento(String numeroAsiento) {
		this.numeroAsiento = numeroAsiento;
	}

	public String getCodigoArticulo() {
		return codigoArticulo;
	}

	public void setCodigoArticulo(String codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}

	public String getDescripcionArticulo() {
		return descripcionArticulo;
	}

	public void setDescripcionArticulo(String descripcionArticulo) {
		this.descripcionArticulo = descripcionArticulo;
	}

	public String getLin() {
		return lin;
	}

	public void setLin(String lin) {
		this.lin = lin;
	}
	public String getNumeroAsientoReentrada() {
		return numeroAsientoReentrada;
	}

	public void setNumeroAsientoReentrada(String numeroAsientoReentrada) {
		this.numeroAsientoReentrada = numeroAsientoReentrada;
	}

	public String getCodigoOperacionAnterior() {
		return codigoOperacionAnterior;
	}

	public void setCodigoOperacionAnterior(String codigoOperacionAnterior) {
		this.codigoOperacionAnterior = codigoOperacionAnterior;
	}



}


