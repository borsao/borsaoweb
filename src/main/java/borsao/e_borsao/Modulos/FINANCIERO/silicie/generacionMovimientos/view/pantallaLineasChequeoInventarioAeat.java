package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoInventarioSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasChequeoInventarioAeat extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	public boolean multiSelect = false;
	private MovimientosSilicieView app=null; 
	
	public pantallaLineasChequeoInventarioAeat(MovimientosSilicieView r_app, String r_cae, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		this.app = r_app;
		
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		consultaMovimientosSilicieServer cse = new consultaMovimientosSilicieServer(CurrentUser.get());
		ArrayList<MapeoInventarioSilicie> vector = cse.chequearInventarioAeat(r_cae);

		if (vector==null ||vector.isEmpty())
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado descuadres de inventario con AEAT");
		}
		else
		{
			this.llenarRegistros(vector);

			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setHeight("650");
			this.gridDatos.setFrozenColumnCount(3);
			
			principal.addComponent(this.gridDatos);
		}
		
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		
		if (gridDatos!=null) principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		
		this.setContent(principal);

	}
	
	private void llenarRegistros(ArrayList<MapeoInventarioSilicie> r_vector)
	{
		Iterator iterator = null;
		MapeoInventarioSilicie mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("CAE", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Borsao", Double.class, null);
		container.addContainerProperty("Aeat", Double.class, null);
		container.addContainerProperty("Cuadre", Double.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoInventarioSilicie) iterator.next();
			file.getItemProperty("CAE").setValue(RutinasCadenas.conversion(mapeoAyuda.getCae()));
    		file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getCodigo()));
    		file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
    		
    		file.getItemProperty("Borsao").setValue(mapeoAyuda.getEntradas());
    		file.getItemProperty("Aeat").setValue(mapeoAyuda.getSalidas());
    		file.getItemProperty("Cuadre").setValue(mapeoAyuda.getFinales());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.gridDatos.sort("Articulo", SortDirection.ASCENDING);
		this.asignarEstilos();
		this.calcularTotal();
	}
	
	private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
//		this.gridDatos.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
//	private String getCellDescription(CellReference cell) 
//	{
//		
//		String strEstado = "";
//		String descriptionText = "";
//			                
//				
//		return descriptionText;
//	}

	public void asignarEstilos()
    {
//		this.asignarTooltips();
		
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Borsao".equals(cellReference.getPropertyId()) || "Aeat".equals(cellReference.getPropertyId()) || "Cuadre".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void cerrar()
	{
		close();
	}
	

	public void calcularTotal() 
	{
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double totalCuadre = new Double(0) ;
    	
    	if (this.gridDatos.getFooterRowCount()==0) this.gridDatos.appendFooterRow();
    	
    	Indexed indexed = this.gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
	        	Double q1Value = (Double) item.getItemProperty("Cuadre").getValue();
	        	if (q1Value!=null ) totalCuadre += q1Value;
        	}
        }
        
        this.gridDatos.getFooterRow(0).getCell("Articulo").setText("Totales");
        this.gridDatos.getFooterRow(0).getCell("Cuadre").setText(RutinasNumericas.formatearDouble(totalCuadre).toString());
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		this.gridDatos.getFooterRow(0).getCell("Cuadre").setStyleName("Rcell-pie");
	}
}