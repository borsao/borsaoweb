package borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoTiposMovimiento mapeo = null;
	 
	 public MapeoTiposMovimiento convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoTiposMovimiento();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setSigno(r_hash.get("signo"));		 
		 this.mapeo.setSilicie(r_hash.get("silicie"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoTiposMovimiento convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoTiposMovimiento();
		 
		 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
		 
		 this.mapeo.setCodigo(r_hash.get("codigo"));		 
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setSigno(r_hash.get("signo"));		 
		 this.mapeo.setSilicie(r_hash.get("silicie"));
		 
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoTiposMovimiento r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 this.hash.put("codigo", r_mapeo.getCodigo());		 
		 this.hash.put("descripcion", r_mapeo.getDescripcion());
		 this.hash.put("signo", r_mapeo.getSigno());
		 this.hash.put("silicie", r_mapeo.getSilicie());
		 return hash;		 
	 }
}