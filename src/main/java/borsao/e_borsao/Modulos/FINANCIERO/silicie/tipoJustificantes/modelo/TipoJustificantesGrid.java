package borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class TipoJustificantesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public TipoJustificantesGrid(ArrayList<MapeoTipoJustificantes> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Tipo Justificantes");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoTipoJustificantes.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("codigo", "descripcion");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("codigo").setHeaderCaption("Codigo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
