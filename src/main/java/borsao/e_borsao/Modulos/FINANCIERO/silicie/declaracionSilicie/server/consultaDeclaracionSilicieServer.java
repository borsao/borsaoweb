package borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.server;

 import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.MapeoPresentadores;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.server.consultaPresentadoresServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.modelo.MapeoMovimientosDeclaradoSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.modelo.MapeoDiferenciasMenos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.server.consultaDiferenciasMenosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.modelo.MapeoDocumentosIdentificativos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.server.consultaDocumentosIdentificativosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server.consultaMovimientosSilicieServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.modelo.MapeoRegimenesFiscales;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.server.consultaRegimenesFiscalesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.modelo.MapeoTipoJustificantes;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.server.consultaTipoJustificantesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.modelo.MapeoTipoOperaciones;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.server.consultaTipoOperacionesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.modelo.MapeoTiposMovimiento;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.server.consultaTiposMovimientoServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaDeclaracionSilicieServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDeclaracionSilicieServer instance;
	private final boolean actualizarGreensys = true;
//	private final String tableLectura = "a_lin_";
	private final String tableOrigen = "fin_sil_registros";
	private final String tableDeclaracion = "fin_sil_declarados";
	private final String tableAnulacion = "fin_sil_declarados_anulados";
	private final String identificadorTabla = "id";
	
	public consultaDeclaracionSilicieServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDeclaracionSilicieServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDeclaracionSilicieServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoMovimientosSilicie> datosDeclaracionSilicieGlobal(MapeoMovimientosSilicie r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientosSilicie> vector = null;
		StringBuffer cadenaWhere = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id, ");
		cadenaSQL.append(" a.clave_tabla reg_clt, ");
		cadenaSQL.append(" a.documento reg_doc, ");
		cadenaSQL.append(" a.serie reg_ser, ");
		cadenaSQL.append(" a.codigo reg_alb, ");
		cadenaSQL.append(" a.posicion reg_pos, ");
		
		cadenaSQL.append(" a.numero_justificante reg_jus,");		
		cadenaSQL.append(" a.numero_documento_id_od reg_ide,");
		cadenaSQL.append(" a.razon_social_od reg_nod,");
		cadenaSQL.append(" a.CAE_od reg_cae,");
		cadenaSQL.append(" a.numero_documento_id_rep reg_idr,");
		cadenaSQL.append(" a.razon_social_rep reg_nre,");
		
		cadenaSQL.append(" a.epigrafe reg_epi,");
		cadenaSQL.append(" a.codigo_epigrafe reg_cep,");
		cadenaSQL.append(" a.clave reg_cl,");
		
		cadenaSQL.append(" a.codigoNC reg_art,");
		cadenaSQL.append(" a.descripcion reg_des,");
		
		cadenaSQL.append(" a.unidad_medida reg_ud,");
		cadenaSQL.append(" a.cantidad reg_ltr,");
		cadenaSQL.append(" a.grado_alcoholico reg_grd,");
		cadenaSQL.append(" a.alcoholPuro reg_alc,");
		
		cadenaSQL.append(" year(a.fecha_movimiento) reg_eje, ");
		cadenaSQL.append(" month(a.fecha_movimiento) reg_mes, ");
		cadenaSQL.append(" a.fecha_movimiento reg_fec, ");
		cadenaSQL.append(" a.fecha_registro reg_fer, ");
		
		cadenaSQL.append(" dre.codigo dre_cod,");
		cadenaSQL.append(" dre.descripcion dre_des, ");
		cadenaSQL.append(" dod.codigo dod_cod, ");
		cadenaSQL.append(" dod.descripcion dod_des, ");
		cadenaSQL.append(" op.codigo op_cod, ");
		cadenaSQL.append(" op.descripcion op_des, ");
		cadenaSQL.append(" mov.codigo mov_cod, ");
		cadenaSQL.append(" mov.descripcion mov_des, ");
		cadenaSQL.append(" mov.signo mov_sig, ");
		cadenaSQL.append(" jus.codigo jus_cod, ");
		cadenaSQL.append(" jus.descripcion jus_des, ");
		cadenaSQL.append(" rf.codigo rf_cod, ");
		cadenaSQL.append(" rf.descripcion rf_des, ");
		cadenaSQL.append(" pre.nombre pre_nom, ");
		cadenaSQL.append(" pre.cae pre_cae, ");
		cadenaSQL.append(" dme.codigo dme_cod, ");
		cadenaSQL.append(" dme.descripcion dme_des, ");
		if (!r_mapeo.getEstado().equals("Pendientes") && !r_mapeo.getEstado().equals("Anulados"))
		{
			cadenaSQL.append(" dcl.fechaPresentacion dcl_fec, ");
			cadenaSQL.append(" dcl.numeroAsiento dcl_as, ");
		}
		cadenaSQL.append(" a.refInterna reg_ref ");
		
		cadenaSQL.append(" from " + this.tableOrigen + " a ");
		
		if (r_mapeo.getEstado().equals("Declarados") || r_mapeo.getEstado().equals("Validados")) cadenaSQL.append(" inner join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		if (r_mapeo.getEstado().equals("Todos")) cadenaSQL.append(" left outer join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		
		cadenaSQL.append(" inner join fin_silicie_rf rf on rf.id = a.id_rf ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov ");
		
		cadenaSQL.append(" left outer join fin_silicie_doc dre on dre.id = a.id_doc_rep ");
		cadenaSQL.append(" left outer join fin_silicie_doc dod on dod.id = a.id_doc_od ");
		cadenaSQL.append(" left outer join fin_silicie_jus jus on jus.id = a.id_jus ");
		cadenaSQL.append(" left outer join fin_silicie_op op on op.id = a.id_op ");
		cadenaSQL.append(" left outer join fin_silicie_dme dme on dme.id = a.id_dme ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha_movimiento) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha_movimiento) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCodigoNC()!=null && r_mapeo.getCodigoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigoNC = '" + r_mapeo.getCodigoNC() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.clave_tabla = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pre.cae = '" + r_mapeo.getCaeTitular() + "' ");
				}

				if (r_mapeo.getEstado()!=null && (r_mapeo.getEstado().equals("Declarados") || r_mapeo.getEstado().equals("Validados")) && r_mapeo.getFechaPresentacion()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dcl.fechaPresentacion = '"+ RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaPresentacion())) + "' ");
				}
				
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Pendientes"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id not in (select idRegistroDeclarado from fin_sil_declarados) ");
				}
				else if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Anulados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados_anulados where numeroAsiento = '') ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by a.codigoNC, a.refInterna ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoMovimientosSilicie>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMovimientosSilicie.setIdCodigo(rsOpcion.getInt("reg_id"));
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("reg_clt"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("reg_ser"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setDocumentoFalso(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("reg_alb"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("reg_pos"));

				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("reg_jus"));				
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("reg_ide"));
				mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("reg_nod"));
				mapeoMovimientosSilicie.setCaeOD(rsOpcion.getString("reg_cae"));
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(rsOpcion.getString("reg_idr"));
				mapeoMovimientosSilicie.setRazonSocialREP(rsOpcion.getString("reg_nre"));
				mapeoMovimientosSilicie.setEpigrafe(rsOpcion.getString("reg_epi"));
				mapeoMovimientosSilicie.setCodigoEpigrafe(rsOpcion.getString("reg_cep"));
				mapeoMovimientosSilicie.setClave(rsOpcion.getString("reg_cl"));				

				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("reg_art"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("reg_des"));				
				mapeoMovimientosSilicie.setUnidad_medida(rsOpcion.getString("reg_ud"));
				mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("reg_ltr"));
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("reg_grd"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("reg_alc"));
				
				mapeoMovimientosSilicie.setEjercicio(rsOpcion.getInt("reg_eje"));
				mapeoMovimientosSilicie.setMes(rsOpcion.getInt("reg_mes"));
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("reg_fec")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("reg_fer")));				
				
				mapeoMovimientosSilicie.setCaeTitular(rsOpcion.getString("pre_cae"));
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(rsOpcion.getString("dre_des"));
				mapeoMovimientosSilicie.setDescripcionDocumentoOd(rsOpcion.getString("dod_des"));
				mapeoMovimientosSilicie.setDescripcionJustificante(rsOpcion.getString("jus_des"));
				mapeoMovimientosSilicie.setDescripcionRegimenFiscal(rsOpcion.getString("rf_des"));
				mapeoMovimientosSilicie.setDescripcionOperacion(rsOpcion.getString("op_des"));
				mapeoMovimientosSilicie.setDescripcionMovimiento(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(rsOpcion.getString("dme_des"));
				
				mapeoMovimientosSilicie.setCodigoDocumentoRep(rsOpcion.getString("dre_cod"));
				mapeoMovimientosSilicie.setCodigoDocumentoOd(rsOpcion.getString("dod_cod"));
				mapeoMovimientosSilicie.setCodigoJustificante(rsOpcion.getString("jus_cod"));
				mapeoMovimientosSilicie.setCodigoRegimenFiscal(rsOpcion.getString("rf_cod"));
				mapeoMovimientosSilicie.setCodigoOperacion(rsOpcion.getString("op_cod"));
				mapeoMovimientosSilicie.setCodigoMovimiento(rsOpcion.getString("mov_cod"));
				mapeoMovimientosSilicie.setCodigoDiferenciaMenos(rsOpcion.getString("dme_cod"));
				
				mapeoMovimientosSilicie.setSigno(rsOpcion.getString("mov_sig"));
				mapeoMovimientosSilicie.setNumeroRefInterno(rsOpcion.getString("reg_ref"));
				
				if (!r_mapeo.getEstado().equals("Pendientes") && !r_mapeo.getEstado().equals("Anulados")) 
				{
					mapeoMovimientosSilicie.setFechaPresentacion(RutinasFechas.conversion(rsOpcion.getDate("dcl_fec")));
					mapeoMovimientosSilicie.setNumeroAsiento(rsOpcion.getString("dcl_as"));
				}
				
				vector.add(mapeoMovimientosSilicie);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String actualizarMovimientosDeclarados(String r_ejercicio, ArrayList<MapeoMovimientosDeclaradoSilicie> r_vector, String r_titular, String r_mes)
	{
		String rdo = null;
		consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());		
		
		for (int i=0;i<r_vector.size();i++)
		{
			MapeoMovimientosDeclaradoSilicie mapeo = (MapeoMovimientosDeclaradoSilicie) r_vector.get(i);
			
			Integer id = cmss.obtenerIdRefInterna(mapeo.getNumeroRefInterno());
			if (id!=null)
			{
				mapeo.setIdMovimiento(id);
				rdo = this.guardarCambios(mapeo, r_titular, r_mes);
			}
			else
				Notificaciones.getInstance().mensajeSeguimiento("Problemas para encontrar el registro interno " + mapeo.getNumeroRefInterno() + " del silicie ");
		}
		if (actualizarGreensys) cerrarDeclaracion(r_ejercicio, r_titular, r_mes);
		return rdo;
	}

	public String actualizarMovimientosAnulacion(ArrayList<MapeoMovimientosDeclaradoSilicie> r_vector, String r_titular, String r_mes)
	{
		String rdo = null;
		consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());		
		
		for (int i=0;i<r_vector.size();i++)
		{
			MapeoMovimientosDeclaradoSilicie mapeo = (MapeoMovimientosDeclaradoSilicie) r_vector.get(i);
			rdo = this.guardarCambiosAnulacion(mapeo);
			
			/*
			 * Si es correcta la anulacion y la validacion, busco el registro y anulo el dato de la referencia Interna
			 * lo tengo que modificar para darle un nuevo valor, si no no me dejara presentarlo de nuevo
			 */
			if (rdo==null)
			{
				Integer idReg = this.obtenerIdAsientoPrevio(mapeo.getNumeroAsientoPrevio());
				String ref = this.obtenerRefInterna(idReg);
				rdo = cmss.actualizarReferenciaIntera(ref,idReg);
				if (rdo!=null) Notificaciones.getInstance().mensajeError("Error actualizando referencia Interna " + rdo );
			}
		}
		return rdo;
	}

	private Integer obtenerIdAsientoPrevio(String r_asientoPrevio)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.idRegistroDeclarado reg_id ");
		cadenaSQL.append(" from " + this.tableAnulacion + " a ");
		cadenaSQL.append(" where a.numeroAsientoPrevio = '" + r_asientoPrevio+ "' " );
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("reg_id");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	private String obtenerRefInterna(Integer r_id)
	{
		ResultSet rsOpcion = null;
		String refRdo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.refInterna reg_ref ");
		cadenaSQL.append(" from fin_sil_registros a ");
		cadenaSQL.append(" where a.id = " + r_id+ " " );
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				refRdo = "C-" + rsOpcion.getString("reg_ref");
				return refRdo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String actualizarMovimientosAnulados(ArrayList<MapeoMovimientosSilicie> r_vector)
	{
		String rdo = null;
		consultaMovimientosSilicieServer cmss = new consultaMovimientosSilicieServer(CurrentUser.get());		
		
		for (int i=0;i<r_vector.size();i++)
		{
			MapeoMovimientosSilicie mapeo = (MapeoMovimientosSilicie) r_vector.get(i);
			
			Integer id = cmss.obtenerIdRefInterna(mapeo.getNumeroRefInterno());
			if (id!=null)
			{
				mapeo.setIdCodigo(id);
				rdo = this.anularAsientoDeclarado(mapeo);
			}
			else
				Notificaciones.getInstance().mensajeSeguimiento("Problemas para encontrar el registro interno " + mapeo.getNumeroRefInterno() + " del silicie ");
		}
		return rdo;
	}

	public ArrayList<MapeoMovimientosSilicie> datosMovimientosDeclaradosSilicieGlobal(MapeoMovimientosSilicie r_mapeo, boolean r_anulados)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientosSilicie> vector = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT refInterna reg_ref, ");
		cadenaSQL.append("asientoPrevio reg_asto, ");
		cadenaSQL.append("fecha_movimiento reg_fec, ");
		cadenaSQL.append("fecha_registro reg_fer, ");
		cadenaSQL.append("tipo_movimiento mov_cod, ");
		cadenaSQL.append("diferencia_menos dme_cod, ");
		cadenaSQL.append("regimen rf_cod, ");
		cadenaSQL.append("tipo_transformacion op_cod, ");
		cadenaSQL.append("numOpTransformacion op_num, ");
		cadenaSQL.append("unidadFabricacionProcDesc, ");
		cadenaSQL.append("unidadFabricacionProcCod, ");
		cadenaSQL.append("tipo_justificante jus_cod, ");
		cadenaSQL.append("numero_justificante reg_jus, ");
		cadenaSQL.append("tipo_documento_od dod_cod, ");
		cadenaSQL.append("numero_documento_od reg_ide, ");  
		cadenaSQL.append("razon_social_od reg_nod, ");    
		cadenaSQL.append("cae_od reg_cae, ");    
		cadenaSQL.append("tipo_documento_rep, ");
		cadenaSQL.append("numero_documento_rep, ");  
		cadenaSQL.append("razon_social_rep, ");    
		cadenaSQL.append("epigrafe reg_epi, ");  
		cadenaSQL.append("codigo_epigrafe reg_cep, ");  
		cadenaSQL.append("codigoNC reg_art, ");  
		cadenaSQL.append("clave reg_cl, ");  
		cadenaSQL.append("cantidad reg_ltr, ");  
		cadenaSQL.append("unidad_medida reg_ud, ");  
		cadenaSQL.append("descripcion reg_des, ");  
		cadenaSQL.append("refProducto ref_art, ");  
		cadenaSQL.append("densidad, ");  
		cadenaSQL.append("grado_alcoholico reg_grd, ");
		cadenaSQL.append("cantidad_absoluta reg_alc, ");
		cadenaSQL.append("extractoPorcentaje, ");  
		cadenaSQL.append("extractoKg, ");
		cadenaSQL.append("gradoPlatoMedio, ");
		cadenaSQL.append("gradoAcetico, ");
		cadenaSQL.append("tipoEnvase, ");
		cadenaSQL.append("capacidad, ");
		cadenaSQL.append("numeroEnvases, ");
		cadenaSQL.append("observaciones ");

		
		cadenaSQL.append(" from fin_sil_declarados_csv ");
		
		try
		{
			
			cadenaSQL.append(" order by refInterna");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoMovimientosSilicie>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();
				   
				if (r_anulados) mapeoMovimientosSilicie.setNumeroAsiento(rsOpcion.getString("reg_asto"));
			    mapeoMovimientosSilicie.setNumeroRefInterno(rsOpcion.getString("reg_ref"));
			    mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("reg_fec")));
			    mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("reg_fer")));				
			    mapeoMovimientosSilicie.setCodigoMovimiento(rsOpcion.getString("mov_cod"));
			    mapeoMovimientosSilicie.setCodigoDiferenciaMenos(rsOpcion.getString("dme_cod"));
			    mapeoMovimientosSilicie.setCodigoRegimenFiscal(rsOpcion.getString("rf_cod"));
			    mapeoMovimientosSilicie.setCodigoJustificante(rsOpcion.getString("jus_cod"));
			    mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("reg_jus"));				
			    mapeoMovimientosSilicie.setCodigoDocumentoOd(rsOpcion.getString("dod_cod"));
			    mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("reg_ide"));
			    mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("reg_nod"));
			    mapeoMovimientosSilicie.setCaeOD(rsOpcion.getString("reg_cae"));
			    mapeoMovimientosSilicie.setEpigrafe(rsOpcion.getString("reg_epi"));
			    mapeoMovimientosSilicie.setCodigoEpigrafe(rsOpcion.getString("reg_cep"));
			    mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("reg_art"));
			    mapeoMovimientosSilicie.setClave(rsOpcion.getString("reg_cl"));				
			    mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("reg_ltr"));
			    mapeoMovimientosSilicie.setUnidad_medida(rsOpcion.getString("reg_ud"));
			    mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("reg_des"));
			    mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("ref_art"));
			    mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("reg_grd"));
			    mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("reg_alc"));
			    
			    mapeoMovimientosSilicie.setCodigoOperacion(rsOpcion.getString("op_cod"));
			    mapeoMovimientosSilicie.setIdCodigoOperacion(rsOpcion.getInt("op_num"));
				mapeoMovimientosSilicie.setCaeTitular("");
				mapeoMovimientosSilicie.setDescripcionDocumentoRep("");
				mapeoMovimientosSilicie.setDescripcionDocumentoOd("");
				mapeoMovimientosSilicie.setDescripcionJustificante("");
				mapeoMovimientosSilicie.setDescripcionRegimenFiscal("");
				mapeoMovimientosSilicie.setDescripcionOperacion("");
				mapeoMovimientosSilicie.setDescripcionMovimiento("");
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos("");
				
				vector.add(mapeoMovimientosSilicie);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		Integer rdo = null;
		try
		{
			con= this.conManager.establecerConexion();
			rdo=  this.obtenerSiguienteCodigoInterno(con, this.tableDeclaracion, this.identificadorTabla);
		}
		catch (Exception ex)
		{
			rdo=1;
		}
		return rdo;
	}

	public String guardarNuevo(MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO fin_sil_declarados( ");
			cadenaSQL.append("id, ");
			cadenaSQL.append("test, ");
			cadenaSQL.append("identificadorMensaje, ");
			cadenaSQL.append("csv, ");
			cadenaSQL.append("fechaPresentacion, ");
			cadenaSQL.append("numeroAsiento, ");
			cadenaSQL.append("tipoAsiento, ");
			cadenaSQL.append("idPresentador, ");
			cadenaSQL.append("idRegistroDeclarado ) VALUES (");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?) ");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, r_mapeo.getIdCodigo().toString());
		    preparedStatement.setString(2, "S");
		    preparedStatement.setString(3, this.obtenerSiguienteCodigoInterno(this.con, this.tableDeclaracion, "id").toString());
		    preparedStatement.setString(4, "");
		    preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaPresentacion())));
		    preparedStatement.setString(6, "");
		    preparedStatement.setString(7, "");
		    preparedStatement.setInt(8, r_mapeo.getIdPresentador());
		    preparedStatement.setInt(9, r_mapeo.getIdCodigo());
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarNuevoAnulados(MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO fin_sil_declarados_anulados( ");
			cadenaSQL.append("id, ");
			cadenaSQL.append("test, ");
			cadenaSQL.append("identificadorMensaje, ");
			cadenaSQL.append("csv, ");
			cadenaSQL.append("fechaPresentacion, ");
			cadenaSQL.append("numeroAsiento, ");
			cadenaSQL.append("numeroAsientoPrevio, ");
			cadenaSQL.append("fechaPresentacionPrevio, ");
			cadenaSQL.append("tipoAsiento, ");
			cadenaSQL.append("motivoAnulacion, ");
			cadenaSQL.append("idPresentador, ");
			cadenaSQL.append("idRegistroDeclarado ) VALUES ( ");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdPresentador(this.obtenerId("Presentador", r_mapeo.getCaeTitular()));
		    
		    preparedStatement.setString(1,  this.obtenerSiguienteCodigoInterno(this.con, this.tableAnulacion, "id").toString());
		    preparedStatement.setString(2, "N");
		    preparedStatement.setString(3, this.obtenerSiguienteCodigoInterno(this.con, this.tableDeclaracion, "id").toString());
		    preparedStatement.setString(4, "");
		    preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()));
		    preparedStatement.setString(6, "");
		    preparedStatement.setString(7, r_mapeo.getNumeroAsiento());
		    preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaPresentacion())));
		    preparedStatement.setInt(9, 1);
		    preparedStatement.setInt(10, 3);
		    preparedStatement.setInt(11, r_mapeo.getIdPresentador());
		    preparedStatement.setInt(12, r_mapeo.getIdCodigo());

		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarCambios(MapeoMovimientosDeclaradoSilicie r_mapeo, String r_titular, String r_mes)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" UPDATE fin_sil_declarados set ");
			cadenaSQL.append("numeroAsiento =  ?, ");
			cadenaSQL.append("tipoAsiento = ? ");
			cadenaSQL.append("where idRegistroDeclarado = ?");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getNumeroAsiento()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getNumeroAsiento());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
		    if (r_mapeo.getTipoAsiento()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getTipoAsiento());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
		    if (r_mapeo.getIdMovimiento()!=null)
			{
				preparedStatement.setInt(3, r_mapeo.getIdMovimiento());
			}
			else
			{
				preparedStatement.setInt(3, new Integer(null));
			}
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}

	public String guardarCambiosAnulacion(MapeoMovimientosDeclaradoSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" UPDATE fin_sil_declarados_anulados set ");
			cadenaSQL.append(" numeroAsiento =  ? ");
			cadenaSQL.append(" where numeroAsientoPrevio = ?");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getNumeroAsiento()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getNumeroAsiento());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
		    if (r_mapeo.getNumeroAsientoPrevio()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getNumeroAsientoPrevio());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}


	public String anularAsientoDeclarado(MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" delete from fin_sil_declarados ");
			cadenaSQL.append(" where id = ? ");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(1, new Integer(null));
			}
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}

	private void cerrarDeclaracion(String r_ejercicio, String r_cae, String r_mes)
	{
		ResultSet rsOpcion = null;
		consultaAlmacenesServer cas =null;
		Integer cuantos = 0;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(*) reg_cuantos ");
		cadenaSQL.append(" from " + this.tableOrigen + " a ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador and pre.cae = '" + r_cae + "' ");
		
		cadenaSQL.append(" where a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento is null) ");		
		cadenaSQL.append(" and month(a.fecha_movimiento)= '" + r_mes + "' ");
		
		try
		{
			cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
			String almacen = cas.obtenerAlmacenPorCAE(r_cae);
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("reg_cuantos")==0)
				{
					/*
					 * updateo greensys
					 */
					PreparedStatement preparedStatement = null;
					cadenaSQL = new StringBuffer();  
					
					String ejer = "0";
					if (r_ejercicio!=null && r_ejercicio.equals(RutinasFechas.añoActualYYYY())) ejer = "0"; else ejer = String.valueOf(new Integer(RutinasFechas.añoActualYYYY()) - new Integer(r_ejercicio));
					
					cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
		    		cadenaSQL.append(" observacion[2] = 'D' ");
			        cadenaSQL.append(" where month(fecha_documento) = '" + r_mes + "' ");
				    
					if (almacen.equals("1"))
					{
						cadenaSQL.append(" and almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
					}
					else if (!almacen.equals("1"))
					{
						cadenaSQL.append(" and almacen not in (" + consultaAlmacenesServer.capuchinos + ") ");		
					}			
					
					Connection cone= this.conManager.establecerConexionGestion();	
					preparedStatement = cone.prepareStatement(cadenaSQL.toString()); 
					
					preparedStatement.executeUpdate();
					
//					con= this.conManager.establecerConexion();	
//					preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
//					
//					preparedStatement.executeUpdate();

					Notificaciones.getInstance().mensajeAdvertencia("Cerrada Presentacion " + r_cae + " del mes " + r_mes);
					
				}
				   
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}	
	}

	public String guardarNuevoDeclaracionCSV(MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO fin_sil_declarados_csv( ");
			cadenaSQL.append("refInterna, ");
			cadenaSQL.append("asientoPrevio, ");
			cadenaSQL.append("fecha_movimiento, ");
			cadenaSQL.append("fecha_registro, ");
			cadenaSQL.append("tipo_movimiento, ");
			cadenaSQL.append("diferencia_menos, ");
			cadenaSQL.append("regimen, ");
			cadenaSQL.append("tipo_transformacion, ");
			cadenaSQL.append("numOpTransformacion, ");
			cadenaSQL.append("unidadFabricacionProcDesc, ");
			cadenaSQL.append("unidadFabricacionProcCod, ");
			cadenaSQL.append("tipo_justificante, ");
			cadenaSQL.append("numero_justificante, ");
			cadenaSQL.append("tipo_documento_od, ");
			cadenaSQL.append("numero_documento_od, ");  
			cadenaSQL.append("razon_social_od, ");    
			cadenaSQL.append("cae_od, ");    
			cadenaSQL.append("tipo_documento_rep, ");
			cadenaSQL.append("numero_documento_rep, ");  
			cadenaSQL.append("razon_social_rep, ");    
			cadenaSQL.append("epigrafe, ");  
			cadenaSQL.append("codigo_epigrafe, ");  
			cadenaSQL.append("codigoNC, ");  
			cadenaSQL.append("clave, ");  
			cadenaSQL.append("cantidad, ");  
			cadenaSQL.append("unidad_medida, ");  
			cadenaSQL.append("descripcion, ");  
			cadenaSQL.append("refProducto, ");  
			cadenaSQL.append("densidad, ");  
			cadenaSQL.append("grado_alcoholico, ");
			cadenaSQL.append("cantidad_absoluta, ");
			cadenaSQL.append("extractoPorcentaje, ");  
			cadenaSQL.append("extractoKg, ");
			cadenaSQL.append("gradoPlatoMedio, ");
			cadenaSQL.append("gradoAcetico, ");
			cadenaSQL.append("tipoEnvase, ");
			cadenaSQL.append("capacidad, ");
			cadenaSQL.append("numeroEnvases, ");
			cadenaSQL.append("observaciones ) VALUES (");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setString(1, r_mapeo.getNumeroRefInterno());
		    preparedStatement.setString(2, "");
		    preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaMovimiento())));
		    preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaRegistro())));
		    preparedStatement.setString(5, r_mapeo.getCodigoMovimiento());
		    preparedStatement.setString(6, r_mapeo.getCodigoDiferenciaMenos());
		    preparedStatement.setString(7, r_mapeo.getCodigoRegimenFiscal());
		    if (r_mapeo.getCodigoOperacion()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getCodigoOperacion());
		    	preparedStatement.setString(9, r_mapeo.getIdCodigoOperacion().toString()); 
		    }
		    else
		    {
		    	preparedStatement.setString(8, "");
		    	preparedStatement.setString(9, "");
		    }
		    preparedStatement.setString(10, "");
		    preparedStatement.setString(11, "");
		    preparedStatement.setString(12, r_mapeo.getCodigoJustificante());
		    if (r_mapeo.getNumeroJustificante()!=null)  preparedStatement.setString(13, r_mapeo.getNumeroJustificante()); else preparedStatement.setString(13, null);
		    preparedStatement.setString(14, r_mapeo.getCodigoDocumentoOd());
		    preparedStatement.setString(15, r_mapeo.getNumeroDocumentoIdOD());
		    preparedStatement.setString(16, r_mapeo.getRazonSocialOD());
		    if (r_mapeo.getCaeOD()!= null) preparedStatement.setString(17, r_mapeo.getCaeOD()); else preparedStatement.setString(17, "");
		    preparedStatement.setString(18, "");
		    preparedStatement.setString(19, "");
		    preparedStatement.setString(20, "");
		    preparedStatement.setString(21, r_mapeo.getEpigrafe());
		    preparedStatement.setString(22, r_mapeo.getCodigoEpigrafe());
		    preparedStatement.setString(23, r_mapeo.getCodigoNC());
		    preparedStatement.setString(24, r_mapeo.getClave());
		    preparedStatement.setDouble(25, r_mapeo.getLitros());
		    preparedStatement.setString(26, r_mapeo.getUnidad_medida());
		    preparedStatement.setString(27, r_mapeo.getDescripcion());
		    preparedStatement.setString(28, r_mapeo.getCodigoArticulo());
		    preparedStatement.setString(29, null);
		    preparedStatement.setDouble(30, r_mapeo.getGrado());
		    preparedStatement.setDouble(31, r_mapeo.getAlcoholPuro());
		    preparedStatement.setString(32, null);
		    preparedStatement.setString(33, null);
		    preparedStatement.setString(34, null);
		    preparedStatement.setString(35, null);
		    preparedStatement.setString(36, "");
		    preparedStatement.setString(37, null);
		    preparedStatement.setString(38, null);
		    preparedStatement.setString(39, "");
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarNuevoDeclaracionCSVAnulados(MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = null;  

		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO fin_sil_declarados_csv( ");
			cadenaSQL.append("refInterna, ");
			cadenaSQL.append("asientoPrevio, ");
			cadenaSQL.append("fecha_movimiento, ");
			cadenaSQL.append("fecha_registro, ");
			cadenaSQL.append("tipo_movimiento, ");
			cadenaSQL.append("diferencia_menos, ");
			cadenaSQL.append("regimen, ");
			cadenaSQL.append("tipo_transformacion, ");
			cadenaSQL.append("numOpTransformacion, ");
			cadenaSQL.append("unidadFabricacionProcDesc, ");
			cadenaSQL.append("unidadFabricacionProcCod, ");
			cadenaSQL.append("tipo_justificante, ");
			cadenaSQL.append("numero_justificante, ");
			cadenaSQL.append("tipo_documento_od, ");
			cadenaSQL.append("numero_documento_od, ");  
			cadenaSQL.append("razon_social_od, ");    
			cadenaSQL.append("cae_od, ");    
			cadenaSQL.append("tipo_documento_rep, ");
			cadenaSQL.append("numero_documento_rep, ");  
			cadenaSQL.append("razon_social_rep, ");    
			cadenaSQL.append("epigrafe, ");  
			cadenaSQL.append("codigo_epigrafe, ");  
			cadenaSQL.append("codigoNC, ");  
			cadenaSQL.append("clave, ");  
			cadenaSQL.append("cantidad, ");  
			cadenaSQL.append("unidad_medida, ");  
			cadenaSQL.append("descripcion, ");  
			cadenaSQL.append("refProducto, ");  
			cadenaSQL.append("densidad, ");  
			cadenaSQL.append("grado_alcoholico, ");
			cadenaSQL.append("cantidad_absoluta, ");
			cadenaSQL.append("extractoPorcentaje, ");  
			cadenaSQL.append("extractoKg, ");
			cadenaSQL.append("gradoPlatoMedio, ");
			cadenaSQL.append("gradoAcetico, ");
			cadenaSQL.append("tipoEnvase, ");
			cadenaSQL.append("capacidad, ");
			cadenaSQL.append("numeroEnvases, ");
			cadenaSQL.append("observaciones ) VALUES (");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setString(1, r_mapeo.getNumeroRefInterno());
		    preparedStatement.setString(2, r_mapeo.getNumeroAsiento());
		    preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaMovimiento())));
		    preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaRegistro())));
		    preparedStatement.setString(5, r_mapeo.getCodigoMovimiento());
		    preparedStatement.setString(6, r_mapeo.getCodigoDiferenciaMenos());
		    preparedStatement.setString(7, r_mapeo.getCodigoRegimenFiscal());
		    if (r_mapeo.getCodigoOperacion()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getCodigoOperacion());
		    	preparedStatement.setString(9, r_mapeo.getIdCodigoOperacion().toString()); 
		    }
		    else
		    {
		    	preparedStatement.setString(8, "");
		    	preparedStatement.setString(9, "");
		    }
		    preparedStatement.setString(10, "");
		    preparedStatement.setString(11, "");
		    preparedStatement.setString(12, r_mapeo.getCodigoJustificante());
		    if (r_mapeo.getNumeroJustificante()!=null)  preparedStatement.setString(13, r_mapeo.getNumeroJustificante()); else preparedStatement.setString(13, null);
		    preparedStatement.setString(14, r_mapeo.getCodigoDocumentoOd());
		    preparedStatement.setString(15, r_mapeo.getNumeroDocumentoIdOD());
		    preparedStatement.setString(16, r_mapeo.getRazonSocialOD());
		    if (r_mapeo.getCaeOD()!= null) preparedStatement.setString(17, r_mapeo.getCaeOD()); else preparedStatement.setString(17, "");
		    preparedStatement.setString(18, "");
		    preparedStatement.setString(19, "");
		    preparedStatement.setString(20, "");
		    preparedStatement.setString(21, r_mapeo.getEpigrafe());
		    preparedStatement.setString(22, r_mapeo.getCodigoEpigrafe());
		    preparedStatement.setString(23, r_mapeo.getCodigoNC());
		    preparedStatement.setString(24, r_mapeo.getClave());
		    preparedStatement.setDouble(25, r_mapeo.getLitros());
		    preparedStatement.setString(26, r_mapeo.getUnidad_medida());
		    preparedStatement.setString(27, r_mapeo.getDescripcion());
		    preparedStatement.setString(28, r_mapeo.getCodigoArticulo());
		    preparedStatement.setString(29, null);
		    preparedStatement.setDouble(30, r_mapeo.getGrado());
		    preparedStatement.setDouble(31, r_mapeo.getAlcoholPuro());
		    preparedStatement.setString(32, null);
		    preparedStatement.setString(33, null);
		    preparedStatement.setString(34, null);
		    preparedStatement.setString(35, null);
		    preparedStatement.setString(36, "");
		    preparedStatement.setString(37, null);
		    preparedStatement.setString(38, null);
		    preparedStatement.setString(39, "");
		    
		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		return null;
	}

	public void eliminar(MapeoMovimientosSilicie r_mapeo)
	{

		try
		{
			con= this.conManager.establecerConexion();
			boolean declarado = this.comprobarDeclarado(r_mapeo);
			if (!declarado) this.eliminarRegistro(con, this.tableDeclaracion, this.identificadorTabla, r_mapeo.getIdCodigo());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public String eliminarGeneradosCSV()
	{

		/*
		 * Borra todos los movimientos generados desde greensys pendientes de declarar del ejercicio y mes indicados
		 */
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;
		String rdo = null;
		
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" delete from fin_sil_declarados_csv ");
			this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();	
		}
		return rdo;
	}

	public boolean comprobarMesCerrado(Integer r_ejercicio, String r_cae, Integer r_mes)
	{
		boolean cerrado = false;
		ResultSet rsOpcion = null;
		consultaAlmacenesServer cas =null;
//		Integer cuantos = 0;
		
		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio!=null)
		{
			if (r_ejercicio.intValue() == ejercicioActual.intValue()) digito="0";
			if (r_ejercicio.intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_ejercicio);
		}

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(*) reg_cuantos ");
		cadenaSQL.append(" from a_lin_" + digito + "  a ");
		
		cadenaSQL.append(" where month(a.fecha_documento)= '" + r_mes + "' ");
		cadenaSQL.append(" and a.observacion[2]='D' ");
		
		try
		{
			cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
			String almacen = cas.obtenerAlmacenPorCAE(r_cae);

			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}			

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("reg_cuantos")!=0)
				{
					return true;
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}	
		
		return cerrado;
	}
	public String eliminarDeclarados(String r_fecha,Integer r_idPresentador,Integer r_mes, boolean r_apertura)
	{

		/*
		 * Borra todos los movimientos generados desde greensys pendientes de declarar del ejercicio y mes indicados
		 */
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;
		String rdo = null;
		
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" delete from fin_sil_declarados where fechaPresentacion = '" + r_fecha + "' and idPresentador = " + r_idPresentador);
			
			if(r_mes==1 && !r_apertura)
				cadenaSQL.append(" and idRegistroDeclarado in (select id from fin_sil_registros where id_mov<>1 and month(fecha_movimiento) = " + r_mes + " and idPresentador = " + r_idPresentador + ") " );
			else
				cadenaSQL.append(" and idRegistroDeclarado in (select id from fin_sil_registros where month(fecha_movimiento) = " + r_mes + " and idPresentador = " + r_idPresentador + ") " );
				
			if (this.con==null) this.con = this.conManager.establecerConexion();			
		    preparedStatement = this.con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.executeUpdate();

		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();	
		}
		return rdo;
	}
	
	public String eliminarGenerados(String r_titular, Integer r_ejercicio, Integer r_mes)
	{

		/*
		 * Borra todos los movimientos generados desde greensys pendientes de declarar del ejercicio y mes indicados
		 */
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + this.tableDeclaracion);            
			cadenaSQL.append(" WHERE year(fecha_movimiento) = ? ");
			cadenaSQL.append(" and month(fecha_movimiento) = ? ");
			cadenaSQL.append(" and id_presentador in (select id from fin_presentadores where cae ='" + r_titular.trim() + "') ");
			cadenaSQL.append(" and id not in (select idRegistroDeclarado from fin_sil_declarados) ");
			cadenaSQL.append(" and documento is not null  ");
			
			con= this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_ejercicio);
			preparedStatement.setInt(2, r_mes);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();	
		}
		return rdo;
	}

	private void actualizarLineaGreensys(String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Integer r_posicion)
	{
		// actualizar campo observacion a_lin_0 con el dato D observacion[2]="D"
		// actualizaremos todos los movimientos del CAE correspondiente en funcion del mes declarado
		
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.marcarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			case "E":
			{
//				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.marcarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			case "R":
			{
//				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.marcarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			case "B":
			{
//				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.marcarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			case "A":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.marcarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);
			}

			default:
			{
				consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
//				cacs.marcarCabeceraAlbaranEMCS(r_albaran);
			}
		}
			
	}
	
//	private void liberarGreensys(String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran, Integer r_posicion)
//	{
//		switch (r_clave_tabla)
//		{
//			case "T":
//			case "t":
//			{
//				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
//				cats.liberarCabeceraAlbaranEMCS(r_clave_tabla, r_documento, r_serie, r_albaran);				
//			}
//			default:
//			{
//				consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
//				cacs.liberarCabeceraAlbaranEMCS(r_albaran);
//			}
//		}
//	}

	private Integer obtenerId(String r_area, String r_valor)
	{
		Integer resultado = null;
		switch (r_area)
		{
			case "Justificante":
			{
			    consultaTipoJustificantesServer cjus = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
			    MapeoTipoJustificantes mapeo = new MapeoTipoJustificantes();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoTipoJustificantes> vector = cjus.datosTipoJustificantesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTipoJustificantes) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "Operacion":
			{
			    consultaTipoOperacionesServer cop = consultaTipoOperacionesServer.getInstance(CurrentUser.get());
			    MapeoTipoOperaciones mapeo = new MapeoTipoOperaciones();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoTipoOperaciones> vector = cop.datosTipoOperacionesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTipoOperaciones) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "DocumentoOd":
			{
			    consultaDocumentosIdentificativosServer cdoc = consultaDocumentosIdentificativosServer.getInstance(CurrentUser.get());
			    MapeoDocumentosIdentificativos mapeo = new MapeoDocumentosIdentificativos();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoDocumentosIdentificativos> vector = cdoc.datosDocumentosIdentificativosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDocumentosIdentificativos) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}

			case "Movimiento":
			{
			    consultaTiposMovimientoServer cmov = consultaTiposMovimientoServer.getInstance(CurrentUser.get());
			    MapeoTiposMovimiento mapeo = new MapeoTiposMovimiento();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoTiposMovimiento> vector = cmov.datosTiposMovimientoGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTiposMovimiento) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "DiferenciaMenos":
			{
			    consultaDiferenciasMenosServer cdif = consultaDiferenciasMenosServer.getInstance(CurrentUser.get());
			    MapeoDiferenciasMenos mapeo = new MapeoDiferenciasMenos();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoDiferenciasMenos> vector = cdif.datosDiferenciasMenosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDiferenciasMenos) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}

			case "Regimen":
			{
			    consultaRegimenesFiscalesServer cmov = consultaRegimenesFiscalesServer.getInstance(CurrentUser.get());
			    MapeoRegimenesFiscales mapeo = new MapeoRegimenesFiscales();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoRegimenesFiscales> vector = cmov.datosRegimenesFiscalesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoRegimenesFiscales) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "Presentador":
			{
			    consultaPresentadoresServer cmov = consultaPresentadoresServer.getInstance(CurrentUser.get());
			    MapeoPresentadores mapeo = new MapeoPresentadores();
			    mapeo.setCae(r_valor);
			    ArrayList<MapeoPresentadores> vector = cmov.datosPresentadoresGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoPresentadores) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
		}
		return resultado;
	}

	public String obtenerDesc(String r_area, Integer r_valor)
	{
		String resultado = null;
		switch (r_area)
		{
			case "Justificante":
			{
			    consultaTipoJustificantesServer cjus = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
			    MapeoTipoJustificantes mapeo = new MapeoTipoJustificantes();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoTipoJustificantes> vector = cjus.datosTipoJustificantesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTipoJustificantes) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "DocumentoOd":
			{
			    consultaDocumentosIdentificativosServer cdoc = consultaDocumentosIdentificativosServer.getInstance(CurrentUser.get());
			    MapeoDocumentosIdentificativos mapeo = new MapeoDocumentosIdentificativos();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoDocumentosIdentificativos> vector = cdoc.datosDocumentosIdentificativosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDocumentosIdentificativos) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}

			case "Movimiento":
			{
			    consultaTiposMovimientoServer cmov = consultaTiposMovimientoServer.getInstance(CurrentUser.get());
			    MapeoTiposMovimiento mapeo = new MapeoTiposMovimiento();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoTiposMovimiento> vector = cmov.datosTiposMovimientoGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTiposMovimiento) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "DiferenciaMenos":
			{
			    consultaDiferenciasMenosServer cdif = consultaDiferenciasMenosServer.getInstance(CurrentUser.get());
			    MapeoDiferenciasMenos mapeo = new MapeoDiferenciasMenos();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoDiferenciasMenos> vector = cdif.datosDiferenciasMenosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDiferenciasMenos) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			
			case "Regimen":
			{
			    consultaRegimenesFiscalesServer cmov = consultaRegimenesFiscalesServer.getInstance(CurrentUser.get());
			    MapeoRegimenesFiscales mapeo = new MapeoRegimenesFiscales();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoRegimenesFiscales> vector = cmov.datosRegimenesFiscalesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoRegimenesFiscales) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "Presentador":
			{
			    consultaPresentadoresServer cmov = consultaPresentadoresServer.getInstance(CurrentUser.get());
			    MapeoPresentadores mapeo = new MapeoPresentadores();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoPresentadores> vector = cmov.datosPresentadoresGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoPresentadores) vector.get(i)).getCae();
			    }
			    
				break;
			}
		}
		return resultado.trim();
	}

	
	public boolean comprobarDeclarado(MapeoMovimientosSilicie r_mapeo)
	{
		boolean encontrado=false;
		
		try 
		{
			con= this.conManager.establecerConexion();
			encontrado = this.comprobarRegistro(con,this.tableDeclaracion,"idRegistroDeclarado",r_mapeo.getIdCodigo());
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return encontrado;
	}
	
	@Override
	public String semaforos() 
	{
		String semaforo= "0";
		
		consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		semaforo = cats.semaforos();
		
		if (semaforo.equals("0"))
		{
			consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
			semaforo = cacs.semaforos();
		}
		return semaforo;
	}

//	private boolean silicieActivo()
//	{
//		ResultSet rsOpcion = null;
//		StringBuffer cadenaSQL = new StringBuffer();  
//		
//		try
//		{
//	        
//			cadenaSQL.append(" select valor_actual from e_l_parm where grupo='BORSAO' and codigo='SILICIE' ");
//		    
//			con= this.conManager.establecerConexionGestion();			
//			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
//			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			
//			while(rsOpcion.next())
//			{
//				if (new Integer(rsOpcion.getString("valor_actual").trim())>0) return true; else return false;
//			}
//		}
//	    catch (Exception ex)
//	    {
//	    	serNotif.mensajeError(ex.getMessage());
//	    	return false;
//	    }
//		return false;
//	}
}