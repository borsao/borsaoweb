package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.server;

 import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server.consultaRecepcionEMCSServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.modelo.MapeoPresentadores;
import borsao.e_borsao.Modulos.FINANCIERO.presentadores.server.consultaPresentadoresServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.declaracionSilicie.server.consultaDeclaracionSilicieServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.modelo.MapeoDiferenciasMenos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.diferenciasMenos.server.consultaDiferenciasMenosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.modelo.MapeoDocumentosIdentificativos;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.server.consultaDocumentosIdentificativosServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoInventarioSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo.MapeoMovimientosSilicie;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.view.MovimientosSilicieView;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.modelo.MapeoRegimenesFiscales;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.server.consultaRegimenesFiscalesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.modelo.MapeoTipoJustificantes;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoJustificantes.server.consultaTipoJustificantesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.modelo.MapeoTipoOperaciones;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tipoOperaciones.server.consultaTipoOperacionesServer;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.modelo.MapeoTiposMovimiento;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.tiposMovimiento.server.consultaTiposMovimientoServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaMovimientosSilicieServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaMovimientosSilicieServer instance;
	private final String tableLectura = "a_lin_";
	private final String tableGeneracion = "fin_sil_registros";
	private final String identificadorTabla = "id";
//	public static int litrosMinimos=90;
	
	public consultaMovimientosSilicieServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaMovimientosSilicieServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaMovimientosSilicieServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoMovimientosSilicie> datosMovimientosSilicieGlobal(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientosSilicie> vector = null;
		StringBuffer cadenaWhere = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id, ");
		cadenaSQL.append(" a.clave_tabla reg_clt, ");
		cadenaSQL.append(" a.documento reg_doc, ");
		cadenaSQL.append(" a.serie reg_ser, ");
		cadenaSQL.append(" a.codigo reg_alb, ");
		cadenaSQL.append(" a.posicion reg_pos, ");
		
		cadenaSQL.append(" a.numero_justificante reg_jus,");		
		cadenaSQL.append(" a.numero_documento_id_od reg_ide,");
		cadenaSQL.append(" a.razon_social_od reg_nod,");
		cadenaSQL.append(" a.CAE_od reg_cae,");
		cadenaSQL.append(" a.numero_documento_id_rep reg_idr,");
		cadenaSQL.append(" a.razon_social_rep reg_nre,");
		
		cadenaSQL.append(" a.epigrafe reg_epi,");
		cadenaSQL.append(" a.codigo_epigrafe reg_cep,");
		cadenaSQL.append(" a.clave reg_cl,");
		
		cadenaSQL.append(" a.codigoNC reg_art,");
		cadenaSQL.append(" a.descripcion reg_des,");
		cadenaSQL.append(" a.articulo reg_arg,");
		cadenaSQL.append(" a.descripcionArticulo reg_deg,");

		cadenaSQL.append(" a.unidad_medida reg_ud,");
		cadenaSQL.append(" a.cantidad reg_ltr,");
		cadenaSQL.append(" a.grado_alcoholico reg_grd,");
		cadenaSQL.append(" a.alcoholPuro reg_alc,");
		
		cadenaSQL.append(" year(a.fecha_movimiento) reg_eje, ");
		cadenaSQL.append(" month(a.fecha_movimiento) reg_mes, ");
		cadenaSQL.append(" a.fecha_movimiento reg_fec, ");
		cadenaSQL.append(" a.fecha_registro reg_fer, ");
		
		cadenaSQL.append(" dre.codigo dre_cod,");
		cadenaSQL.append(" dre.descripcion dre_des, ");
		cadenaSQL.append(" dod.codigo dod_cod, ");
		cadenaSQL.append(" dod.descripcion dod_des, ");
		cadenaSQL.append(" op.codigo op_cod, ");
		cadenaSQL.append(" op.descripcion op_des, ");
		cadenaSQL.append(" mov.codigo mov_cod, ");
		cadenaSQL.append(" mov.descripcion mov_des, ");
		cadenaSQL.append(" mov.signo mov_sig, ");
		cadenaSQL.append(" jus.codigo jus_cod, ");
		cadenaSQL.append(" jus.descripcion jus_des, ");
		cadenaSQL.append(" rf.codigo rf_cod, ");
		cadenaSQL.append(" rf.descripcion rf_des, ");
		cadenaSQL.append(" pre.nombre pre_nom, ");
		cadenaSQL.append(" pre.cae pre_cae, ");
		cadenaSQL.append(" dme.codigo dme_cod, ");
		cadenaSQL.append(" dme.descripcion dme_des, ");
		if (!r_mapeo.getEstado().equals("Pendientes") && !r_mapeo.getEstado().equals("Anulados"))
		{
			cadenaSQL.append(" dcl.fechaPresentacion dcl_fec, ");
			cadenaSQL.append(" dcl.numeroAsiento dcl_as, ");
		}
		cadenaSQL.append(" dcla.id rda_id, ");
		cadenaSQL.append(" a.refInterna reg_ref ");
		
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		
		
		if (r_mapeo.getEstado().equals("Declarados") || r_mapeo.getEstado().equals("Validados") ) cadenaSQL.append(" inner join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		if (r_mapeo.getEstado().equals("Todos")) cadenaSQL.append(" left outer join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		
		cadenaSQL.append(" left outer join fin_sil_declarados_anulados dcla on dcla.idRegistroDeclarado = a.id ");
		
		cadenaSQL.append(" inner join fin_silicie_rf rf on rf.id = a.id_rf ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov ");
		
		cadenaSQL.append(" left outer join fin_silicie_doc dre on dre.id = a.id_doc_rep ");
		cadenaSQL.append(" left outer join fin_silicie_doc dod on dod.id = a.id_doc_od ");
		cadenaSQL.append(" left outer join fin_silicie_jus jus on jus.id = a.id_jus ");
		cadenaSQL.append(" left outer join fin_silicie_op op on op.id = a.id_op ");
		cadenaSQL.append(" left outer join fin_silicie_dme dme on dme.id = a.id_dme ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha_movimiento) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0 )
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha_movimiento) = " + r_mapeo.getMes() + " ");
				}
				
				if (r_mapeo.getCodigoNC()!=null && r_mapeo.getCodigoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigoNC = '" + r_mapeo.getCodigoNC() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}

				if (r_mapeo.getCodigoArticulo()!=null && r_mapeo.getCodigoArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.articulo = '" + r_mapeo.getCodigoArticulo() + "' ");
				}
				if (r_mapeo.getDescripcionArticulo()!=null && r_mapeo.getDescripcionArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcionArticulo = '" + r_mapeo.getDescripcionArticulo() + "' ");
				}
				
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.clave_tabla = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pre.cae = '" + r_mapeo.getCaeTitular() + "' ");
				}

				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Declarados") && r_mapeo.getFechaPresentacion()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dcl.fechaPresentacion = '"+ RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaPresentacion())) + "' ");
				}
				
				
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Pendientes"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id not in (select idRegistroDeclarado from fin_sil_declarados) ");
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id not in (select idRegistroDeclarado from fin_sil_declarados_anulados where numeroAsiento = '') ");
				}
				else if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Declarados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento = '') ");
				}
				else if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Validados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento <>'') ");
				}
				else if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Anulados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados_anulados where numeroAsiento = '') ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by a.codigoNC, a.refInterna ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoMovimientosSilicie>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMovimientosSilicie.setIdCodigo(rsOpcion.getInt("reg_id"));
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("reg_clt"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("reg_ser"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setDocumentoFalso(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("reg_alb"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("reg_pos"));

				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("reg_jus"));				
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("reg_ide"));
				mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("reg_nod"));
				mapeoMovimientosSilicie.setCaeOD(rsOpcion.getString("reg_cae"));
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(rsOpcion.getString("reg_idr"));
				mapeoMovimientosSilicie.setRazonSocialREP(rsOpcion.getString("reg_nre"));
				mapeoMovimientosSilicie.setEpigrafe(rsOpcion.getString("reg_epi"));
				mapeoMovimientosSilicie.setCodigoEpigrafe(rsOpcion.getString("reg_cep"));
				mapeoMovimientosSilicie.setClave(rsOpcion.getString("reg_cl"));				

				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("reg_art"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("reg_des"));				
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("reg_arg"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("reg_deg"));				
				
				mapeoMovimientosSilicie.setUnidad_medida(rsOpcion.getString("reg_ud"));
				mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("reg_ltr"));
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("reg_grd"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("reg_alc"));
				
				mapeoMovimientosSilicie.setEjercicio(rsOpcion.getInt("reg_eje"));
				mapeoMovimientosSilicie.setMes(rsOpcion.getInt("reg_mes"));
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("reg_fec")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("reg_fer")));				
				
				mapeoMovimientosSilicie.setCaeTitular(rsOpcion.getString("pre_cae"));
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(rsOpcion.getString("dre_des"));
				mapeoMovimientosSilicie.setDescripcionDocumentoOd(rsOpcion.getString("dod_des"));
				mapeoMovimientosSilicie.setDescripcionJustificante(rsOpcion.getString("jus_des"));
				mapeoMovimientosSilicie.setDescripcionRegimenFiscal(rsOpcion.getString("rf_des"));
				mapeoMovimientosSilicie.setDescripcionOperacion(rsOpcion.getString("op_des"));
				mapeoMovimientosSilicie.setDescripcionMovimiento(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(rsOpcion.getString("dme_des"));
				
				mapeoMovimientosSilicie.setCodigoDocumentoRep(rsOpcion.getString("dre_cod"));
				mapeoMovimientosSilicie.setCodigoDocumentoOd(rsOpcion.getString("dod_cod"));
				mapeoMovimientosSilicie.setCodigoJustificante(rsOpcion.getString("jus_cod"));
				mapeoMovimientosSilicie.setCodigoRegimenFiscal(rsOpcion.getString("rf_cod"));
				mapeoMovimientosSilicie.setCodigoOperacion(rsOpcion.getString("op_cod"));
				mapeoMovimientosSilicie.setCodigoMovimiento(rsOpcion.getString("mov_cod"));
				mapeoMovimientosSilicie.setCodigoDiferenciaMenos(rsOpcion.getString("dme_cod"));
				mapeoMovimientosSilicie.setIdAnulacion(rsOpcion.getInt("rda_id"));
				mapeoMovimientosSilicie.setSigno(rsOpcion.getString("mov_sig"));
				mapeoMovimientosSilicie.setNumeroRefInterno(rsOpcion.getString("reg_ref"));
				
				if (r_mapeo.getEstado().equals("Declarados")) 
				{
					mapeoMovimientosSilicie.setFechaPresentacion(RutinasFechas.conversion(rsOpcion.getDate("dcl_fec")));
				}
				else if (r_mapeo.getEstado().equals("Validados") || r_mapeo.getEstado().equals("Todos"))
				{
					mapeoMovimientosSilicie.setFechaPresentacion(RutinasFechas.conversion(rsOpcion.getDate("dcl_fec")));
					mapeoMovimientosSilicie.setNumeroAsiento(rsOpcion.getString("dcl_as"));
				}
				
				vector.add(mapeoMovimientosSilicie);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public Integer obtenerIdRefInterna(String r_referenciaInterna)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id ");
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		cadenaSQL.append(" where a.refInterna = '" + r_referenciaInterna + "' " );
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("reg_id");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}

	public ArrayList<MapeoMovimientosSilicie> datosMovimientosDeclararSilicieGlobal(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		ArrayList<MapeoMovimientosSilicie> vector = null;
		StringBuffer cadenaWhere = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id, ");
		cadenaSQL.append(" a.clave_tabla reg_clt, ");
		cadenaSQL.append(" a.documento reg_doc, ");
		cadenaSQL.append(" a.serie reg_ser, ");
		cadenaSQL.append(" a.codigo reg_alb, ");
		cadenaSQL.append(" a.posicion reg_pos, ");
		
		cadenaSQL.append(" a.numero_justificante reg_jus,");		
		cadenaSQL.append(" a.numero_documento_id_od reg_ide,");
		cadenaSQL.append(" a.razon_social_od reg_nod,");
		cadenaSQL.append(" a.CAE_od reg_cae,");
		cadenaSQL.append(" a.numero_documento_id_rep reg_idr,");
		cadenaSQL.append(" a.razon_social_rep reg_nre,");
		
		cadenaSQL.append(" a.epigrafe reg_epi,");
		cadenaSQL.append(" a.codigo_epigrafe reg_cep,");
		cadenaSQL.append(" a.clave reg_cl,");
		
		cadenaSQL.append(" a.codigoNC reg_art,");
		cadenaSQL.append(" a.descripcion reg_des,");
		cadenaSQL.append(" a.articulo reg_arg,");
		cadenaSQL.append(" a.descripcionArticulo reg_deg,");
		
		cadenaSQL.append(" a.unidad_medida reg_ud,");
		cadenaSQL.append(" a.cantidad reg_ltr,");
		cadenaSQL.append(" a.grado_alcoholico reg_grd,");
		cadenaSQL.append(" a.alcoholPuro reg_alc,");
		
		cadenaSQL.append(" year(a.fecha_movimiento) reg_eje, ");
		cadenaSQL.append(" month(a.fecha_movimiento) reg_mes, ");
		cadenaSQL.append(" a.fecha_movimiento reg_fec, ");
		cadenaSQL.append(" a.fecha_registro reg_fer, ");
		
		cadenaSQL.append(" dre.codigo dre_cod,");
		cadenaSQL.append(" dre.descripcion dre_des, ");
		cadenaSQL.append(" dod.codigo dod_cod, ");
		cadenaSQL.append(" dod.descripcion dod_des, ");
		cadenaSQL.append(" op.id op_id, ");
		cadenaSQL.append(" op.codigo op_cod, ");
		cadenaSQL.append(" op.descripcion op_des, ");
		cadenaSQL.append(" mov.codigo mov_cod, ");
		cadenaSQL.append(" mov.descripcion mov_des, ");
		cadenaSQL.append(" mov.signo mov_sig, ");
		cadenaSQL.append(" jus.codigo jus_cod, ");
		cadenaSQL.append(" jus.descripcion jus_des, ");
		cadenaSQL.append(" rf.codigo rf_cod, ");
		cadenaSQL.append(" rf.descripcion rf_des, ");
		cadenaSQL.append(" pre.nombre pre_nom, ");
		cadenaSQL.append(" pre.cae pre_cae, ");
		cadenaSQL.append(" dme.codigo dme_cod, ");
		cadenaSQL.append(" dme.descripcion dme_des, ");
		if (r_mapeo.getEstado().equals("Anulados")) cadenaSQL.append(" anu.numeroAsientoPrevio anu_ast, ");
		
		cadenaSQL.append(" a.refInterna reg_ref ");
		
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		
		
		cadenaSQL.append(" inner join fin_silicie_rf rf on rf.id = a.id_rf ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov and mov.silicie_sn = 'S' ");
		
		if (r_mapeo.getEstado().equals("Anulados"))
		{
			cadenaSQL.append(" inner join fin_sil_declarados_anulados anu on anu.idRegistroDeclarado = a.id and numeroAsiento = '' ");
		}
		
		cadenaSQL.append(" left outer join fin_silicie_doc dre on dre.id = a.id_doc_rep ");
		cadenaSQL.append(" left outer join fin_silicie_doc dod on dod.id = a.id_doc_od ");
		cadenaSQL.append(" left outer join fin_silicie_jus jus on jus.id = a.id_jus ");
		cadenaSQL.append(" left outer join fin_silicie_op op on op.id = a.id_op ");
		cadenaSQL.append(" left outer join fin_silicie_dme dme on dme.id = a.id_dme ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha_movimiento) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha_movimiento) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCodigoNC()!=null && r_mapeo.getCodigoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigoNC = '" + r_mapeo.getCodigoNC() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				
				if (r_mapeo.getCodigoArticulo()!=null && r_mapeo.getCodigoArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.articulo = '" + r_mapeo.getCodigoArticulo() + "' ");
				}
				if (r_mapeo.getDescripcionArticulo()!=null && r_mapeo.getDescripcionArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcionArticulo = '" + r_mapeo.getDescripcionArticulo() + "' ");
				}
				
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.clave_tabla = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pre.cae = '" + r_mapeo.getCaeTitular() + "' ");
				}

				if (r_mapeo.getEstado().equals("Pendientes"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id not in (select idRegistroDeclarado from fin_sil_declarados) ");
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id not in (select idRegistroDeclarado from fin_sil_declarados_anulados) ");
				}
				else if (r_mapeo.getEstado().equals("Declarados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getFechaPresentacion()!=null)
					{
						cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento = '' and fechaPresentacion = '" + RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaPresentacion().toString()) + "') ");
					}
					else
					{
						cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento = '' ) ");
					}
				}
				else if (r_mapeo.getEstado().equals("Validados"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getFechaPresentacion()!=null)
					{
						cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento <>'' and fechaPresentacion = '" + RutinasFechas.convertirAFechaMysql(r_mapeo.getFechaPresentacion().toString()) + "') ");
					}
					else
					{
						cadenaWhere.append(" a.id in (select idRegistroDeclarado from fin_sil_declarados where numeroAsiento <>'' ) ");
					}
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by a.codigoNC, a.refInterna ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoMovimientosSilicie>();
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();
				/*
				 * recojo mapeo operarios
				 */
				mapeoMovimientosSilicie.setIdCodigo(rsOpcion.getInt("reg_id"));
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("reg_clt"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("reg_ser"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setDocumentoFalso(rsOpcion.getString("reg_doc"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("reg_alb"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("reg_pos"));
				if (r_mapeo.getEstado().equals("Anulados"))  mapeoMovimientosSilicie.setNumeroAsiento(rsOpcion.getString("anu_ast"));
				mapeoMovimientosSilicie.setNumeroAsientoReentrada(null);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("reg_jus"));				
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("reg_ide"));
				mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("reg_nod"));
				mapeoMovimientosSilicie.setCaeOD(rsOpcion.getString("reg_cae"));
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(rsOpcion.getString("reg_idr"));
				mapeoMovimientosSilicie.setRazonSocialREP(rsOpcion.getString("reg_nre"));
				mapeoMovimientosSilicie.setEpigrafe(rsOpcion.getString("reg_epi"));
				mapeoMovimientosSilicie.setCodigoEpigrafe(rsOpcion.getString("reg_cep"));
				mapeoMovimientosSilicie.setClave(rsOpcion.getString("reg_cl"));				

				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("reg_art"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("reg_des"));				
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("reg_arg"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("reg_deg"));		
				
				mapeoMovimientosSilicie.setUnidad_medida(rsOpcion.getString("reg_ud"));
				mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("reg_ltr"));
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("reg_grd"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("reg_alc"));
				
				mapeoMovimientosSilicie.setEjercicio(rsOpcion.getInt("reg_eje"));
				mapeoMovimientosSilicie.setMes(rsOpcion.getInt("reg_mes"));
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("reg_fec")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("reg_fec")));				
				
				mapeoMovimientosSilicie.setCaeTitular(rsOpcion.getString("pre_cae"));
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(rsOpcion.getString("dre_des"));
				mapeoMovimientosSilicie.setDescripcionDocumentoOd(rsOpcion.getString("dod_des"));
				mapeoMovimientosSilicie.setDescripcionJustificante(rsOpcion.getString("jus_des"));
				mapeoMovimientosSilicie.setDescripcionRegimenFiscal(rsOpcion.getString("rf_des"));
				mapeoMovimientosSilicie.setDescripcionOperacion(rsOpcion.getString("op_des"));
				mapeoMovimientosSilicie.setDescripcionMovimiento(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(rsOpcion.getString("dme_des"));
				
				mapeoMovimientosSilicie.setCodigoDocumentoRep(rsOpcion.getString("dre_cod"));
				mapeoMovimientosSilicie.setCodigoDocumentoOd(rsOpcion.getString("dod_cod"));
				mapeoMovimientosSilicie.setCodigoJustificante(rsOpcion.getString("jus_cod"));
				mapeoMovimientosSilicie.setCodigoRegimenFiscal(rsOpcion.getString("rf_cod"));
				mapeoMovimientosSilicie.setCodigoOperacionAnterior(null);
				mapeoMovimientosSilicie.setCodigoOperacion(rsOpcion.getString("op_cod"));
				mapeoMovimientosSilicie.setCodigoMovimiento(rsOpcion.getString("mov_cod"));
				mapeoMovimientosSilicie.setCodigoDiferenciaMenos(rsOpcion.getString("dme_cod"));
				
				mapeoMovimientosSilicie.setSigno(rsOpcion.getString("mov_sig"));
				mapeoMovimientosSilicie.setNumeroRefInterno(rsOpcion.getString("reg_ref"));
				
				
				mapeoMovimientosSilicie.setIdCodigoOperacion(rsOpcion.getInt("op_id"));
				
				
				vector.add(mapeoMovimientosSilicie);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	

	public String generacionMovimientosEntradasCompraSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		MapeoRecepcionEMCS mapeoRecepcionEnvio = null;
		MapeoRecepcionEMCS mapeoRecepcionRecibo = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		
		cadenaSQL.append(" p.nombre_fiscal pro_nom,");
		cadenaSQL.append(" p.cif pro_nif,");
		cadenaSQL.append(" p.nom_abreviado pro_cae ,");
		cadenaSQL.append(" p.pais pro_pai,");
		cadenaSQL.append(" p.provincia pro_pro,");
		cadenaSQL.append(" p.estadistico_b2 pro_b2,");
		
		cadenaSQL.append(" y.descripcion mov_des ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume d on d.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and d.clave_tabla in ('E','M') " );		 
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
		cadenaSQL.append(" LEFT OUTER join e_provee p on p.proveedor COLLATE latin1_spanish_ci = " + "a.receptor_emisor COLLATE latin1_spanish_ci ");
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and (y.codigo<>'' and y.codigo<> 'FALSO') " );
		
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and (a.observacion is null or (substring(a.observacion,1,1) <> 'N' and substring(a.observacion,2,1) <> 'D')) " );
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}			
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

				/*
				 * EN CASO DE SER GRANEL cogeremos datos de la asignacion de emcs
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101"))
				{
					consultaRecepcionEMCSServer crs = consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
					
					mapeoRecepcionEnvio = new MapeoRecepcionEMCS();
						mapeoRecepcionEnvio.setDocumento(rsOpcion.getString("mov_doc"));
						mapeoRecepcionEnvio.setClaveTabla(rsOpcion.getString("mov_cl"));
						mapeoRecepcionEnvio.setSerie(rsOpcion.getString("mov_ser"));
						mapeoRecepcionEnvio.setAlbaran(rsOpcion.getInt("mov_cod"));
					
					mapeoRecepcionRecibo = crs.datosRecepcionEMCS(mapeoRecepcionEnvio);
				}
/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				
	
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				if (mapeoRecepcionRecibo!=null)
				{
					//arc emcs
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoRecepcionRecibo.getArc());
					/*
					 * ORIGEN_DESTINO
					 */	
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(mapeoRecepcionRecibo.getNif());
					mapeoMovimientosSilicie.setRazonSocialOD(mapeoRecepcionRecibo.getDestinatario());
					mapeoMovimientosSilicie.setCaeOD(mapeoRecepcionRecibo.getOrigen());

				}
				else
				{
					// otros
					mapeoMovimientosSilicie.setIdCodigoJustificante(11);
					mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
					
					/*
					 * ORIGEN_DESTINO
					 */				
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("pro_nif"));
					mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("pro_nom"));
					mapeoMovimientosSilicie.setCaeOD(rsOpcion.getString("pro_cae"));
				}
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);

/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	

				if (rsOpcion.getString("pro_b2").equals("06"))
				{
					// otro
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(3);
					// Entrada Importacion
					mapeoMovimientosSilicie.setIdCodigoMovimiento(6);
				}
				else if (rsOpcion.getString("pro_pai").equals("000"))
				{
					// NIF
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
					if (rsOpcion.getString("pro_pro").equals("35") || rsOpcion.getString("pro_pro").equals("38"))
					{
						// Entrada Canarias
						mapeoMovimientosSilicie.setIdCodigoMovimiento(2);
					}
					else
					{
						// Entrada Interior
						mapeoMovimientosSilicie.setIdCodigoMovimiento(2);
					}
				}
				else
				{
					// VAT
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(2);
					// Entrada Europea
					mapeoMovimientosSilicie.setIdCodigoMovimiento(4);
				}
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				if (mapeoRecepcionRecibo!=null)
				{
					// Suspensivo
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
				}
				else
				{
					// Impuesto Devengado tipo pleno
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(4);
				}

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);					
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 1 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
				
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			if (mapeoMovimientosSilicie!=null)
			{
				Notificaciones.getInstance().mensajeError("SILICIE 1a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
			else 
				Notificaciones.getInstance().mensajeError("SILICIE 1a Error: " + ex.getMessage());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosFabricacionSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
//		cadenaSQL.append(" a.codigo mov_cod, ");
//		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" sum(a.unidades) mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des, ");
		cadenaSQL.append(" w.encargado pre_cae ,");
		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (b.articulo like '0101%' or b.articulo like '0102%' or b.articulo like '0103%' or b.articulo like '0111%') " );
		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );

		
		try
		{
			cadenaSQL.append(" where (a.observacion is null or ( substring(a.observacion,2,1)<>'D')) " );
			cadenaSQL.append(" and a.clave_tabla in ('B') " );
			cadenaSQL.append(" and a.clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and month(a.fecha_documento) = " + r_mes);
			
			cadenaSQL.append(" group by " );
			cadenaSQL.append(" a.ejercicio,");
			cadenaSQL.append(" a.documento, ");
			cadenaSQL.append(" a.clave_tabla, ");
			cadenaSQL.append(" a.serie, ");
//			cadenaSQL.append(" a.codigo mov_cod, ");
//			cadenaSQL.append(" a.posicion mov_pos, ");
			cadenaSQL.append(" a.fecha_ult_modif,");
			cadenaSQL.append(" a.fecha_documento,");
			cadenaSQL.append(" a.articulo, ");
			cadenaSQL.append(" b.descrip_articulo, ");
			cadenaSQL.append(" b.marca, ");
			cadenaSQL.append(" b.vl_caract_2, ");
			cadenaSQL.append(" b.tipo_articulo, ");
			cadenaSQL.append(" b.precio_compra2, ");
			cadenaSQL.append(" a.receptor_emisor,");
			cadenaSQL.append(" a.movimiento,");
			cadenaSQL.append(" t.nuevoArticulo, ");
			cadenaSQL.append(" t.nuevaDescripcion, ");
			cadenaSQL.append(" t.nuevoNC, ");
			cadenaSQL.append(" s.descripcion, ");
			cadenaSQL.append(" y.descripcion, ");
			cadenaSQL.append(" w.encargado,");
			cadenaSQL.append(" z.id");


			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(null);
				mapeoMovimientosSilicie.setPosicion(null);
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}

/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+RutinasFechas.convertirDateToString(rsOpcion.getDate("mov_fed")));
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);
				
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("A50889955");

				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
				if (rsOpcion.getString("mov_mov").equals("49"))
				{
					// otro
					// Entrada Importacion
					mapeoMovimientosSilicie.setIdCodigoMovimiento(15);
				}
				else
				{
					// VAT
					// Entrada Europea
					mapeoMovimientosSilicie.setIdCodigoMovimiento(14);
				}
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(rsOpcion.getInt("pre_id"));				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "EMBOTELLADO / ENVASADO"));
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 2 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 2a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosEntradasTrasladoSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		MapeoRecepcionEMCS mapeoRecepcionEnvio = null;
		MapeoRecepcionEMCS mapeoRecepcionRecibo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des, ");
		cadenaSQL.append(" w.encargado pre_cae ,");
		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('T','t') " );
		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
		cadenaSQL.append(" inner join e_almac c on c.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and c.encargado <>'' " );
		cadenaSQL.append(" inner join e_almac d on d.almacen COLLATE latin1_spanish_ci = " + "a.receptor_emisor COLLATE latin1_spanish_ci and d.encargado <>'' " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );
		try
		{
			cadenaSQL.append(" where clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and month(a.fecha_documento) = " + r_mes );
			cadenaSQL.append(" and (a.observacion is null or ( substring(a.observacion,2,1)<>'D' )) " );
			cadenaSQL.append(" and a.movimiento = '22' ");
			
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

				consultaRecepcionEMCSServer crs = consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
				
				mapeoRecepcionEnvio = new MapeoRecepcionEMCS();
					mapeoRecepcionEnvio.setDocumento(rsOpcion.getString("mov_doc"));
					mapeoRecepcionEnvio.setClaveTabla(rsOpcion.getString("mov_cl"));
					mapeoRecepcionEnvio.setSerie(rsOpcion.getString("mov_ser"));
					mapeoRecepcionEnvio.setAlbaran(rsOpcion.getInt("mov_cod"));
					mapeoRecepcionEnvio.setEjercicio(r_ejercicio);
				
				mapeoRecepcionRecibo = crs.datosRecepcionEMCS(mapeoRecepcionEnvio);
/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				if (mapeoRecepcionRecibo!=null)
				{
					//arc emcs
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoRecepcionRecibo.getArc());
					/*
					 * ORIGEN_DESTINO
					 */	
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(mapeoRecepcionRecibo.getNif());
					mapeoMovimientosSilicie.setRazonSocialOD(mapeoRecepcionRecibo.getOrigen());
					mapeoMovimientosSilicie.setCaeOD(mapeoRecepcionRecibo.getCaeDestino());
					
					/*
					 * Suspensivo
					 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);

				}
				else
				{
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoMovimientosSilicie.getDocumento() + " " + mapeoMovimientosSilicie.getAlbaran().toString());
					/*
					 * suspensivo
					 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
				}
				
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));

				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_uds")*rsOpcion.getDouble("mov_gr")/100);
				
/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
				/*
				 * Entrada interior
				 */
				mapeoMovimientosSilicie.setIdCodigoMovimiento(2);
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(rsOpcion.getInt("pre_id"));				
				
				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				mapeoMovimientosSilicie.setIdCodigoOperacion(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 3 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 3a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosSalidasTrasladoSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		MapeoSeguimientoEMCS mapeoSeguimientoEnvio = null;
		MapeoSeguimientoEMCS mapeoSeguimientoRecibo = null;
//		consultaArticulosServer cas = null;
		consultaSeguimientoEMCSServer crs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des, ");
		cadenaSQL.append(" w.encargado pre_cae ,");
		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('T','t') " );
		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
		cadenaSQL.append(" inner join e_almac c on c.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and c.encargado <>'' " );
		cadenaSQL.append(" inner join e_almac d on d.almacen COLLATE latin1_spanish_ci = " + "a.receptor_emisor COLLATE latin1_spanish_ci and d.encargado <>'' " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );
		try
		{
			cadenaSQL.append(" where clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and (a.observacion is null or ( substring(a.observacion,2,1)<>'D')) " );
			cadenaSQL.append(" and a.movimiento = '62' ");
			
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			crs = consultaSeguimientoEMCSServer.getInstance(CurrentUser.get());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

				
				mapeoSeguimientoEnvio = new MapeoSeguimientoEMCS();
					mapeoSeguimientoEnvio.setDocumento(rsOpcion.getString("mov_doc"));
					mapeoSeguimientoEnvio.setClaveTabla(rsOpcion.getString("mov_cl"));
					mapeoSeguimientoEnvio.setSerie(rsOpcion.getString("mov_ser"));
					mapeoSeguimientoEnvio.setAlbaran(rsOpcion.getInt("mov_cod"));
				
				mapeoSeguimientoRecibo = crs.datosSeguimientoEMCS(mapeoSeguimientoEnvio);
/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				if (mapeoSeguimientoRecibo!=null)
				{
					//arc emcs
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoSeguimientoRecibo.getArc());
					/*
					 * ORIGEN_DESTINO
					 */	
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(mapeoSeguimientoRecibo.getNif());
					mapeoMovimientosSilicie.setRazonSocialOD(mapeoSeguimientoRecibo.getDestinatario());
					mapeoMovimientosSilicie.setCaeOD(mapeoSeguimientoRecibo.getExciseNumber());
					
					
					// Suspensivo
					if (mapeoSeguimientoRecibo.getCodigoIdentificacion()==1)
					{
						
						mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
						/*
						 * Salida interior
						 */
						mapeoMovimientosSilicie.setIdCodigoMovimiento(8);
					}
					else
					{
						/*
						 * Salida Exportacion
						 */
						mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(mapeoSeguimientoRecibo.getCodigoIdentificacion());
						mapeoMovimientosSilicie.setIdCodigoMovimiento(11);
						mapeoMovimientosSilicie.setCaeOD(null);
					}
				}
				else
				{
					/*
					 * justificante y origen destino internos
					 */
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoMovimientosSilicie.getDocumento() + " " + mapeoMovimientosSilicie.getAlbaran().toString());
					/*
					 * suspensivo
					 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
					mapeoMovimientosSilicie.setIdCodigoMovimiento(8);
				}
					
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_uds")*rsOpcion.getDouble("mov_gr")/100);
				
	/*
	 * PRESENTADOR
	 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(rsOpcion.getInt("pre_id"));				
				
				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				mapeoMovimientosSilicie.setIdCodigoOperacion(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 4 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 4a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosSalidasVentaSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		MapeoSeguimientoEMCS mapeoSeguimientoEnvio = null;
		MapeoSeguimientoEMCS mapeoSeguimientoRecibo = null;
		boolean abono = false;
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		
		cadenaSQL.append(" p.nombre_fiscal cli_nom,");
		cadenaSQL.append(" p.cif cli_nif,");
		cadenaSQL.append(" p.pais cli_pai,");
		cadenaSQL.append(" p.provincia cli_pro,");
		cadenaSQL.append(" p.estadistico_b2 cli_b2,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des ");
		
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci and b.vl_caract_2 <> 'FALSO' " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
		cadenaSQL.append(" left outer join e_client p on p.cliente COLLATE latin1_spanish_ci = " + "a.receptor_emisor COLLATE latin1_spanish_ci ");
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci " );
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and a.clave_tabla in ('A','F') ");
			cadenaSQL.append(" and (a.observacion is null or (substring(a.observacion,1,1) <> 'N' and substring(a.observacion,2,1) <> 'D')) " );
			cadenaSQL.append(" and a.clave_movimiento in ('T','A') " );
			cadenaSQL.append("  " );
			
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			
			if (!almacen.equals("1")) cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes +") " );
			
			cadenaSQL.append(" order by fecha_documento  ");
		
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

				/*
				 * EN CASO DE SER GRANEL cogeremos datos de la asignacion de emcs
				 */
				consultaSeguimientoEMCSServer crs = consultaSeguimientoEMCSServer.getInstance(CurrentUser.get());
				
				mapeoSeguimientoEnvio = new MapeoSeguimientoEMCS();
					mapeoSeguimientoEnvio.setDocumento(rsOpcion.getString("mov_doc"));
					mapeoSeguimientoEnvio.setClaveTabla(rsOpcion.getString("mov_cl"));
					mapeoSeguimientoEnvio.setSerie(rsOpcion.getString("mov_ser"));
					mapeoSeguimientoEnvio.setAlbaran(rsOpcion.getInt("mov_cod"));
					mapeoSeguimientoEnvio.setEstado("Todos");
					
					mapeoSeguimientoRecibo = crs.datosSeguimientoEMCS(mapeoSeguimientoEnvio);
					
/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}	
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				if (mapeoSeguimientoRecibo!=null)
				{
					//arc emcs
					mapeoMovimientosSilicie.setIdCodigoJustificante(1);
					mapeoMovimientosSilicie.setNumeroJustificante(mapeoSeguimientoRecibo.getArc());
					/*
					 * ORIGEN_DESTINO
					 */	
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(mapeoSeguimientoRecibo.getNif());
					mapeoMovimientosSilicie.setRazonSocialOD(mapeoSeguimientoRecibo.getDestinatario());
					mapeoMovimientosSilicie.setCaeOD(mapeoSeguimientoRecibo.getAuthorized());

				}
				else
				{
					// otros
					mapeoMovimientosSilicie.setIdCodigoJustificante(11);
					mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
					/*
					 * ORIGEN_DESTINO
					 */				
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD(rsOpcion.getString("cli_nif"));
					mapeoMovimientosSilicie.setRazonSocialOD(rsOpcion.getString("cli_nom"));
					mapeoMovimientosSilicie.setCaeOD(null);
				}

				/*
				 * DATOS PRODUCTO
				 */
				mapeoMovimientosSilicie.setLitros(Math.abs(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac"))));					
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(Math.abs(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100));
/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
				abono = (rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")))<0;
				
				if (rsOpcion.getString("cli_pai").equals("000"))
				{
					// NIF
					if (mapeoSeguimientoRecibo!=null)
					{
						mapeoMovimientosSilicie.setIdCodigoDocumentoOd(mapeoSeguimientoRecibo.getCodigoIdentificacion());
					}
					else if (rsOpcion.getString("cli_nif")!=null && rsOpcion.getString("cli_nif").trim().contains("000000A"))
					{
						mapeoMovimientosSilicie.setIdCodigoDocumentoOd(3);
					}
					else
					{
						mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
					}
					
					if (rsOpcion.getString("cli_pro").equals("35") || rsOpcion.getString("cli_pro").equals("38"))
					{
						// salida Canarias
						mapeoMovimientosSilicie.setIdCodigoMovimiento(11);
						if (abono) mapeoMovimientosSilicie.setIdCodigoMovimiento(2);
					}
					else
					{
						// Salida Interior
						mapeoMovimientosSilicie.setIdCodigoMovimiento(8);
						if (abono) mapeoMovimientosSilicie.setIdCodigoMovimiento(2);
					}
				}
				else if (rsOpcion.getString("cli_b2").equals("06"))
				{
					// otro
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(3);
					// Salida Exportacion
					mapeoMovimientosSilicie.setIdCodigoMovimiento(11);
					if (abono) mapeoMovimientosSilicie.setIdCodigoMovimiento(6);
				}
				else
				{
					// VAT
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(2);
					// Salida Europea
					mapeoMovimientosSilicie.setIdCodigoMovimiento(10);
					if (abono) mapeoMovimientosSilicie.setIdCodigoMovimiento(4);
				}
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				if (mapeoSeguimientoRecibo!=null)
				{
					// Suspensivo
					if (mapeoSeguimientoRecibo.getCodigoIdentificacion()==1) 
						mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
					else
					{
						mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(mapeoSeguimientoRecibo.getCodigoIdentificacion());
						mapeoMovimientosSilicie.setIdCodigoMovimiento(11);
						mapeoMovimientosSilicie.setCaeOD(null);
					}
				}
				else
				{
					// Impuesto Devengado tipo pleno
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(4);
				}

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);					
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null)  Notificaciones.getInstance().mensajeSeguimiento("SILICIE 5 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
				
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 5a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosEntradasRegularizacionSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des ");
//		cadenaSQL.append(" w.encargado pre_cae ,");
//		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('R') " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
//		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
//		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and (a.observacion is null or substring(a.observacion,2,1)<>'D') " );
			cadenaSQL.append(" and a.movimiento between '00' and '49' ");
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);
				
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("");
				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
//				if (rsOpcion.getString("mov_art").startsWith("0101"))
//				{
//					// otro
//					// diferencia en mas por almacenamiento
//					mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "ELABORACION DE VINO"));
//				}
//				else
//				{
//					// otro
//					// diferencia en mas por almacenamiento
//					mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "EMBOTELLADO/ ENVASADO"));
//				}
				mapeoMovimientosSilicie.setIdCodigoMovimiento(31);
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("A50889955");
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 6 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 6a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosElaboracionVinoSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		String rdo = null;
		rdo = this.generacionMovimientosEntradaUvaSilicia(r_titular, r_ejercicio, r_mes);
		if (rdo ==null)
		{
			rdo = this.generacionMovimientosEntradasElaboracionVinoSilicie(r_titular, r_ejercicio, r_mes);
			if (rdo ==null)
			{
				rdo = this.generacionMovimientosSalidasElaboracionVinoSilicie(r_titular, r_ejercicio, r_mes);
			}
		}
		return rdo;
	}
	

	public String generacionMovimientosEntradaUvaSilicia(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
//		cadenaSQL.append(" a.codigo mov_cod, ");
//		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" month(a.fecha_documento) mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" sum(a.unidades) mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" y.descripcion mov_des, ");
		cadenaSQL.append(" w.encargado pre_cae ,");
		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('B') and p.documento in ('DC') " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (b.articulo like '012%' ) " );
		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		
		try
		{
			cadenaSQL.append(" where (a.observacion is null or ( substring(a.observacion,2,1)<>'D')) " );
			cadenaSQL.append(" and a.clave_movimiento in ('T','A') " );
			
			cadenaSQL.append(" group by " );
			cadenaSQL.append(" a.ejercicio,");
			cadenaSQL.append(" a.documento, ");
			cadenaSQL.append(" a.clave_tabla, ");
			cadenaSQL.append(" a.serie, ");
//			cadenaSQL.append(" a.codigo mov_cod, ");
//			cadenaSQL.append(" a.posicion mov_pos, ");
			cadenaSQL.append(" month(a.fecha_documento),");
			cadenaSQL.append(" a.articulo, ");
			cadenaSQL.append(" b.descrip_articulo, ");
			cadenaSQL.append(" b.marca, ");
			cadenaSQL.append(" b.vl_caract_2, ");
			cadenaSQL.append(" b.tipo_articulo, ");
			cadenaSQL.append(" b.precio_compra2, ");
			cadenaSQL.append(" a.receptor_emisor,");
			cadenaSQL.append(" a.movimiento,");
			cadenaSQL.append(" y.descripcion, ");
			cadenaSQL.append(" w.encargado,");
			cadenaSQL.append(" z.id");

			cadenaSQL.append(" having month(a.fecha_documento) = " + r_mes);
			

			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(null);
				mapeoMovimientosSilicie.setPosicion(null);
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversionDeString(RutinasFechas.restarDiasFecha("01/"+ (r_mes + 1)  + "/" + r_ejercicio.toString(), 1)));
				mapeoMovimientosSilicie.setFechaRegistro(mapeoMovimientosSilicie.getFechaMovimiento());
				
/*
 * DATOS PRODUCTO				
 */
				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));

/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+RutinasFechas.restarDiasFecha("01/"+ (r_mes + 1)  + "/" + r_ejercicio.toString(), 1));
				/*
				 * DATOS PRODUCTO
				 */
				mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds"));
				
				mapeoMovimientosSilicie.setGrado(null);
				mapeoMovimientosSilicie.setAlcoholPuro(null);
				mapeoMovimientosSilicie.setEpigrafe(null);
				mapeoMovimientosSilicie.setUnidad_medida("KGR");

				
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD(null);
				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
				if (rsOpcion.getString("mov_mov").equals("49"))
				{
					// otro
					// Entrada uva
					mapeoMovimientosSilicie.setIdCodigoMovimiento(14);
				}
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(rsOpcion.getInt("pre_id"));				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(1);

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "ELABORACION DE VINO"));
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 9 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 9a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String generacionMovimientosEntradasElaboracionVinoSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" y.descripcion mov_des ");
//		cadenaSQL.append(" w.encargado pre_cae ,");
//		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('R') and p.documento in ('V1') " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '012%' or a.articulo like '013%') " );
//		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
//		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and (a.observacion is null or substring(a.observacion,2,1)<>'D') " );
			cadenaSQL.append(" and a.movimiento between '00' and '49' ");
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101"))
				{
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds"));
					mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
					mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);
				/*
				 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
				 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);
				}
				
				else 
				{
					mapeoMovimientosSilicie.setGrado(null);
					mapeoMovimientosSilicie.setAlcoholPuro(null);
					mapeoMovimientosSilicie.setEpigrafe(null);
					mapeoMovimientosSilicie.setUnidad_medida("KGR");

					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds"));
				/*
				 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
				 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(1);
					
				}
				
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("");
				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

				/*
				 * MOVIMIENTO Y TIPO DE IDENTIFICACION
				 */	
				if (rsOpcion.getString("mov_art").startsWith("0101") || rsOpcion.getString("mov_art").startsWith("013"))
				{
					mapeoMovimientosSilicie.setIdCodigoMovimiento(15);
					mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
					mapeoMovimientosSilicie.setNumeroDocumentoIdOD("A50889955");
				}
				else
				{
					mapeoMovimientosSilicie.setIdCodigoMovimiento(14);
				}
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 10 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 10a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	public String generacionMovimientosSalidasElaboracionVinoSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" y.descripcion mov_des ");
//		cadenaSQL.append(" w.encargado pre_cae ,");
//		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('R') and p.documento in ('V1') " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '012%' or a.articulo like '013%') " );
//		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
//		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and (a.observacion is null or substring(a.observacion,2,1)<>'D') " );
			cadenaSQL.append(" and a.movimiento between '50' and '99' ");
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
				/*
				 * DATOS PRODUCTO
				 */
				mapeoMovimientosSilicie.setGrado(null);
				mapeoMovimientosSilicie.setAlcoholPuro(null);
				mapeoMovimientosSilicie.setEpigrafe(null);
				mapeoMovimientosSilicie.setUnidad_medida("KGR");

				mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds"));
				/*
				 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
				 */
					mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(1);
					
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("");
				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

				/*
				 * MOVIMIENTO Y TIPO DE IDENTIFICACION
				 */	
				mapeoMovimientosSilicie.setIdCodigoMovimiento(14);
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 10 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 10a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public String generacionMovimientosInventarioInicialSilicie(String r_titular, Integer r_ejercicio)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		String art = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		Long contador = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		consultaContadoresEjercicioServer cont = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		
		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = String.valueOf((new Integer(RutinasFechas.añoActualYYYY())-r_ejercicio));
		
		cadenaSQL.append(" SELECT a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		
		cadenaSQL.append(" sum(a.unid_inicio) mov_uds, ");
		
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" y.descripcion mov_des ");
		
		cadenaSQL.append(" from a_exis_" + digito + " a ");
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci " );
		
		if (almacen.equals("1"))
		{
			cadenaSQL.append(" where a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			cadenaSQL.append(" and b.vl_caract_2 <> 'FALSO' ");
			
		}
		else if (!almacen.equals("1"))
		{
			cadenaSQL.append(" where a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");
			cadenaSQL.append(" and b.vl_caract_2 <> 'FALSO' ");
		}
		
		try
		{
			cadenaSQL.append(" group by 1,2,4,5,6,7,8 ");
			cadenaSQL.append(" order by a.articulo ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();
				
				if (rsOpcion.getString("mov_art")!=null)
				{
					art=rsOpcion.getString("mov_art");
				}
				
				if (art.trim().equals("0101500"))
				{
					System.out.println("HECES");
				}
				if (art.trim().equals("0102052"))
				{
					System.out.println("monte oton");
				}
/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(null);
				mapeoMovimientosSilicie.setDocumento(null);
				mapeoMovimientosSilicie.setSerie(null);
				mapeoMovimientosSilicie.setAlbaran(null);
				mapeoMovimientosSilicie.setPosicion(null);
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(RutinasFechas.conversionDeString("01/01/"+ r_ejercicio)));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(RutinasFechas.conversionDeString("01/01/"+ r_ejercicio)));
				
/*
 * DATOS PRODUCTO				
 */
				mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
				mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
				mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
				mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(12);
				contador = cont.recuperarContador(new Integer(r_ejercicio), "MANUAL SILICIE", r_titular);
				mapeoMovimientosSilicie.setNumeroJustificante(contador.toString());
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);
				
				/*
				 * ORIGEN_DESTINO
				 */				
				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("A50889955");
				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

/*
 * MOVIMIENTO Y TIPO DE OPERACION
 */	
				mapeoMovimientosSilicie.setIdCodigoMovimiento(1);
				mapeoMovimientosSilicie.setIdCodigoOperacion(null);
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				if (rdo!=null) Notificaciones.getInstance().mensajeSeguimiento("SILICIE 7 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(art);
			rdo = ex.getMessage();			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	public String generacionMovimientosSalidasRegularizacionSilicie(String r_titular, Integer r_ejercicio, Integer r_mes)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
				
		StringBuffer cadenaSQL = new StringBuffer();
		String digito = null;
		String rdo = null;
		
		consultaPresentadoresServer cps = consultaPresentadoresServer.getInstance(CurrentUser.get());
		Integer idPresentador = cps.obtenerId(r_titular);

		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacen = cas.obtenerAlmacenPorCAE(r_titular);

		if (r_ejercicio!=null && r_ejercicio.toString().equals(RutinasFechas.añoActualYYYY())) digito = "0"; else digito = r_ejercicio.toString();
		
		cadenaSQL.append(" SELECT a.ejercicio mov_eje,");
		cadenaSQL.append(" a.documento mov_doc, ");
		cadenaSQL.append(" a.clave_tabla mov_cl, ");
		cadenaSQL.append(" a.serie mov_ser, ");
		cadenaSQL.append(" a.codigo mov_cod, ");
		cadenaSQL.append(" a.posicion mov_pos, ");
		cadenaSQL.append(" a.fecha_ult_modif mov_fer,");
		cadenaSQL.append(" a.fecha_documento mov_fed,");
		cadenaSQL.append(" a.articulo mov_art, ");
		cadenaSQL.append(" b.descrip_articulo lin_des, ");
		cadenaSQL.append(" a.unidades mov_uds, ");
		cadenaSQL.append(" b.marca mov_fac, ");
		cadenaSQL.append(" b.vl_caract_2 mov_nc, ");
		cadenaSQL.append(" b.tipo_articulo mov_tip, ");
		cadenaSQL.append(" b.precio_compra2 mov_gr, ");
		cadenaSQL.append(" a.receptor_emisor mov_ter,");
		cadenaSQL.append(" a.movimiento mov_mov,");
		
		cadenaSQL.append(" t.nuevoArticulo cmb_art,");
		cadenaSQL.append(" t.nuevaDescripcion cmb_des,");
		cadenaSQL.append(" t.nuevoNC cmb_nc,");
		cadenaSQL.append(" s.descripcion cmb_ncd, ");
		
		cadenaSQL.append(" y.descripcion mov_des ");
//		cadenaSQL.append(" w.encargado pre_cae ,");
//		cadenaSQL.append(" z.id pre_id ");
		
		cadenaSQL.append(" from " + this.tableLectura + digito + " a ");
		cadenaSQL.append(" inner join e_docume p on p.documento COLLATE latin1_spanish_ci = a.documento COLLATE latin1_spanish_ci and p.clave_tabla in ('R') " );
		cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = " + "a.articulo COLLATE latin1_spanish_ci " ); 
		cadenaSQL.append(" and (a.articulo like '0101%' or a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') " );
//		cadenaSQL.append(" inner join e_almac w on w.almacen COLLATE latin1_spanish_ci = " + "a.almacen COLLATE latin1_spanish_ci and w.encargado = '" + r_titular + "'");
//		cadenaSQL.append(" inner join fin_presentadores z on z.cae COLLATE latin1_spanish_ci = w.encargado COLLATE latin1_spanish_ci" );
		cadenaSQL.append(" left outer join fin_silicie_nc y on y.codigo COLLATE latin1_spanish_ci = b.vl_caract_2 COLLATE latin1_spanish_ci and y.codigo<> 'FALSO' " );
		
		cadenaSQL.append(" left outer join fin_sil_tune_art t on t.articulo COLLATE latin1_spanish_ci = a.articulo COLLATE latin1_spanish_ci and t.lote = a.numero_lote " );
		cadenaSQL.append(" left outer join fin_silicie_nc s on s.codigo COLLATE latin1_spanish_ci = t.nuevoNC COLLATE latin1_spanish_ci " );
		try
		{
			cadenaSQL.append(" where month(a.fecha_documento) = " + r_mes);
			cadenaSQL.append(" and clave_movimiento in ('T','A') " );
			cadenaSQL.append(" and a.movimiento between '50' and '99' ");
			cadenaSQL.append(" and (a.observacion is null or substring(a.observacion,2,1)<>'D') " );
			
			if (almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
			}
			else if (!almacen.equals("1"))
			{
				cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");		
			}

			
			cadenaSQL.append(" order by fecha_documento  ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoMovimientosSilicie = new MapeoMovimientosSilicie();

/*
 * DATOS IDENTIFICATIVOS MOVIMIENTO - ERP				
 */
				mapeoMovimientosSilicie.setClaveTabla(rsOpcion.getString("mov_cl"));
				mapeoMovimientosSilicie.setDocumento(rsOpcion.getString("mov_doc"));
				mapeoMovimientosSilicie.setSerie(rsOpcion.getString("mov_ser"));
				mapeoMovimientosSilicie.setAlbaran(rsOpcion.getInt("mov_cod"));
				mapeoMovimientosSilicie.setPosicion(rsOpcion.getInt("mov_pos"));
/*
 * FECHAS MOVIMIENTO Y REGISTRO				
 */
				mapeoMovimientosSilicie.setFechaMovimiento(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				mapeoMovimientosSilicie.setFechaRegistro(RutinasFechas.conversion(rsOpcion.getDate("mov_fed")));
				
/*
 * DATOS PRODUCTO				
 */
				if (rsOpcion.getString("cmb_nc")!=null && rsOpcion.getString("cmb_nc").length()>0)
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("cmb_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("cmb_ncd"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("cmb_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("cmb_des"));					
				}
				else
				{
					mapeoMovimientosSilicie.setCodigoNC(rsOpcion.getString("mov_nc"));
					mapeoMovimientosSilicie.setDescripcion(rsOpcion.getString("mov_des"));
					mapeoMovimientosSilicie.setCodigoArticulo(rsOpcion.getString("mov_art"));
					mapeoMovimientosSilicie.setDescripcionArticulo(rsOpcion.getString("lin_des"));
				}
				
/*
 * JUSTIFICANTE e-da, albaran, etc...
 */
				// otros
				mapeoMovimientosSilicie.setIdCodigoJustificante(11);
				mapeoMovimientosSilicie.setNumeroJustificante(rsOpcion.getString("mov_doc")+rsOpcion.getInt("mov_cod")+rsOpcion.getInt("mov_pos"));
				/*
				 * DATOS PRODUCTO
				 */
				if (rsOpcion.getString("mov_art").startsWith("0101")) 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")); 
				else 
					mapeoMovimientosSilicie.setLitros(rsOpcion.getDouble("mov_uds")*RutinasNumericas.formatearDoubleDeESP(rsOpcion.getString("mov_fac")));
				
				mapeoMovimientosSilicie.setGrado(rsOpcion.getDouble("mov_gr"));
				mapeoMovimientosSilicie.setAlcoholPuro(rsOpcion.getDouble("mov_gr")*mapeoMovimientosSilicie.getLitros()/100);
				
				/*
				 * ORIGEN_DESTINO
				 */				
				mapeoMovimientosSilicie.setIdCodigoDocumentoOd(1);
				mapeoMovimientosSilicie.setNumeroDocumentoIdOD("A50889955");

				mapeoMovimientosSilicie.setRazonSocialOD("");
				mapeoMovimientosSilicie.setCaeOD("");

/*
 * MOVIMIENTO Y TIPO DE IDENTIFICACION
 */	
				if (rsOpcion.getString("mov_art").startsWith("0101"))
				{
					// otro
					// diferencia en menos por almacenamiento
					mapeoMovimientosSilicie.setIdCodigoDiferenciaMenos(7); //causadas por el embotellado
//					mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "ELABORACION DE VINO"));
				}
				else
				{
					// otro
					// diferencia en menos por almacenamiento
					if (!rsOpcion.getString("lin_des").contains("S/E")) mapeoMovimientosSilicie.setIdCodigoDiferenciaMenos(5); else mapeoMovimientosSilicie.setIdCodigoDiferenciaMenos(3);
//					mapeoMovimientosSilicie.setIdCodigoOperacion(this.obtenerId("Operacion", "EMBOTELLADO/ ENVASADO"));
				}
				mapeoMovimientosSilicie.setIdCodigoMovimiento(30);			
/*
 * PRESENTADOR
 */				
				// idPresentador
				mapeoMovimientosSilicie.setCaeTitular(r_titular);
				mapeoMovimientosSilicie.setIdPresentador(idPresentador);				
				
/*
 * REGIMEN FISCAL. Si viene con guia será Suspensivo en cualquier otro caso con iva
 */
				mapeoMovimientosSilicie.setIdCodigoRegimenFiscal(2);

				/*
				 * NO APLICAN
				 */
				mapeoMovimientosSilicie.setDescripcionDocumentoRep(null);
				mapeoMovimientosSilicie.setNumeroDocumentoIdREP(null);
				mapeoMovimientosSilicie.setRazonSocialREP(null);	
				mapeoMovimientosSilicie.setDescripcionOperacion(null);
				mapeoMovimientosSilicie.setDescripcionDiferenciaMenos(null);
				
				/*
				 * Llamamos a guardar nuevo registro				
				 */
				rdo = this.guardarNuevoGeneracion(con, mapeoMovimientosSilicie);
				
				if (rdo!=null) 
					Notificaciones.getInstance().mensajeSeguimiento("SILICIE 8 Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
			}
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();
			Notificaciones.getInstance().mensajeError("SILICIE 8a Error: " + mapeoMovimientosSilicie.getSerie() + " " + mapeoMovimientosSilicie.getAlbaran() + " " + mapeoMovimientosSilicie.getCodigoArticulo() + " " + mapeoMovimientosSilicie.getCaeTitular() + " " + mapeoMovimientosSilicie.getMes());
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public Integer obtenerSiguiente()
	{
		Connection con = null;	

		Integer rdo = null;
		try
		{
			con= this.conManager.establecerConexionInd();
			rdo=  obtenerSiguienteCodigoInterno(con, this.tableGeneracion, this.identificadorTabla);
		}
		catch (Exception ex)
		{
			rdo=1;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}

	private Integer obtenerSiguienteTemporal()
	{
		Connection con = null;	

		Integer rdo = null;
		try
		{
			con= this.conManager.establecerConexionInd();
			rdo=  obtenerSiguienteCodigoInterno(con, "tmp_existencias", "idCodigo");
		}
		catch (Exception ex)
		{
			rdo=1;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}

	public String actualizarReferenciaIntera(String r_ref, Integer r_id)
	{
		Connection con = null;	

		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = null;
//		String rdo = null;
		
		cadenaSQL = new StringBuffer();
		cadenaSQL.append(" UPDATE fin_sil_registros set ");
		cadenaSQL.append("refInterna =  '" + r_ref + "' ");
		cadenaSQL.append("where id = " + r_id);
		
	    
	    try
	    {
	    	con = this.conManager.establecerConexionInd();
	    	preparedStatement = con.prepareStatement(cadenaSQL.toString());
	    	
	        preparedStatement.executeUpdate();
	        
		}
		catch (Exception ex)
		{
			return ex.getMessage();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return null;

	}
	
	public String guardarNuevo(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	

		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO " + this.tableGeneracion + "( ");
			cadenaSQL.append("id,");			
			cadenaSQL.append("clave_tabla, ");
			cadenaSQL.append("documento, ");
			cadenaSQL.append("serie, ");
			cadenaSQL.append("codigo, ");
			cadenaSQL.append("posicion, ");
			cadenaSQL.append("numero_justificante,");		
			cadenaSQL.append("numero_documento_id_od,");
			cadenaSQL.append("razon_social_od,");
			cadenaSQL.append("CAE_od,");
			cadenaSQL.append("numero_documento_id_rep,");
			cadenaSQL.append("razon_social_rep,");
			cadenaSQL.append("epigrafe,");
			cadenaSQL.append("codigo_epigrafe,");
			cadenaSQL.append("clave,");
			cadenaSQL.append("codigoNC,");
			cadenaSQL.append("descripcion,");			
			cadenaSQL.append("unidad_medida,");
			cadenaSQL.append("cantidad,");
			cadenaSQL.append("grado_alcoholico,");
			cadenaSQL.append("alcoholPuro,");
			cadenaSQL.append("fecha_movimiento, ");
			cadenaSQL.append("fecha_registro, ");	
			
			cadenaSQL.append("id_doc_rep, ");
			cadenaSQL.append("id_doc_od, ");
			cadenaSQL.append("id_jus, ");
			cadenaSQL.append("id_rf, ");
			cadenaSQL.append("id_presentador, ");
			cadenaSQL.append("id_op, ");
			cadenaSQL.append("id_mov, ");
			cadenaSQL.append("id_dme, ");
			cadenaSQL.append("refInterna, ");
			cadenaSQL.append("articulo,");
			cadenaSQL.append("descripcionArticulo ) VALUES (");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    Integer idReg = this.obtenerSiguiente();
		    r_mapeo.setIdCodigo(idReg);
		    
		    preparedStatement.setInt(1, idReg);
		    String fecha = "";

		    Integer id = this.obtenerId("Justificante", r_mapeo.getDescripcionJustificante());
		 	r_mapeo.setIdCodigoJustificante(id);
		 	id = this.obtenerId("Movimiento", r_mapeo.getDescripcionMovimiento());
		 	r_mapeo.setIdCodigoMovimiento(id);
		 	id = this.obtenerId("Regimen", r_mapeo.getDescripcionRegimenFiscal());
		 	r_mapeo.setIdCodigoRegimenFiscal(id);
		 	
		 	if (r_mapeo.getDescripcionDiferenciaMenos()!=null && r_mapeo.getDescripcionDiferenciaMenos().length()>0)
		 	{
		 		id = this.obtenerId("DiferenciaMenos", r_mapeo.getDescripcionDiferenciaMenos());
		 		r_mapeo.setIdCodigoDiferenciaMenos(id);
		 	}
		 	else
		 	{
		 		r_mapeo.setIdCodigoDiferenciaMenos(null);
		 	}
		 	id = this.obtenerId("Presentador", r_mapeo.getCaeTitular());
		 	r_mapeo.setIdPresentador(id);
			id = this.obtenerId("DocumentoOd", r_mapeo.getDescripcionDocumentoOd());
			r_mapeo.setIdCodigoDocumentoOd(id);

			
		    if (r_mapeo.getClaveTabla()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClaveTabla());
		    }
		    else
		    {
		    	preparedStatement.setString(2, "M");
		    }
		    
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getAlbaran()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getAlbaran());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getPosicion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getPosicion());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getNumeroJustificante()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getNumeroJustificante());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getNumeroDocumentoIdOD()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getNumeroDocumentoIdOD());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getRazonSocialOD()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getRazonSocialOD());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getCaeOD()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getCaeOD());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getNumeroDocumentoIdREP()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getNumeroDocumentoIdREP());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getRazonSocialREP()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getRazonSocialREP());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getEpigrafe()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getEpigrafe());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }		    
		    if (r_mapeo.getCodigoEpigrafe()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getCodigoEpigrafe());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getClave()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getClave());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getCodigoNC()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getCodigoNC());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getUnidad_medida()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getUnidad_medida());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }				
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(19, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setDouble(19, 0);
		    }
		    if (r_mapeo.getGrado()!=null)
		    {
		    	preparedStatement.setDouble(20, r_mapeo.getGrado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(20, 0);
		    }
		    if (r_mapeo.getGrado()!=null && r_mapeo.getLitros()!=null)
		    {
		    	r_mapeo.setAlcoholPuro(r_mapeo.getGrado()*r_mapeo.getLitros()/100);
		    	preparedStatement.setDouble(21, r_mapeo.getAlcoholPuro());
		    }
		    else
		    {
		    	preparedStatement.setDouble(21, 0);
		    }
		    
		    
		    if (r_mapeo.getFechaMovimiento()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaMovimiento());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(22, RutinasFechas.convertirAFechaMysql(fecha));
		    if (r_mapeo.getFechaRegistro()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaRegistro());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(23, RutinasFechas.convertirAFechaMysql(fecha));

		    if (r_mapeo.getIdCodigoDocumentoRep()!=null)
		    {
		    	preparedStatement.setInt(24, r_mapeo.getIdCodigoDocumentoRep());
		    }
		    else
		    {
		    	preparedStatement.setInt(24, 0);
		    }
		    if (r_mapeo.getIdCodigoDocumentoOd()!=null)
		    {
		    	preparedStatement.setInt(25, r_mapeo.getIdCodigoDocumentoOd());
		    }
		    else
		    {
		    	preparedStatement.setInt(25, 0);
		    }
		    if (r_mapeo.getIdCodigoJustificante()!=null)
		    {
		    	preparedStatement.setInt(26, r_mapeo.getIdCodigoJustificante());
		    }
		    else
		    {
		    	preparedStatement.setInt(26, 0);
		    }
		    if (r_mapeo.getIdCodigoRegimenFiscal()!=null)
		    {
		    	preparedStatement.setInt(27, r_mapeo.getIdCodigoRegimenFiscal());
		    }
		    else
		    {
		    	preparedStatement.setInt(27, 0);
		    }
		    if (r_mapeo.getIdPresentador()!=null)
		    {
		    	preparedStatement.setInt(28, r_mapeo.getIdPresentador());
		    }
		    else
		    {
		    	preparedStatement.setInt(28, 0);
		    }
		    if (r_mapeo.getIdCodigoOperacion()!=null)
		    {
		    	preparedStatement.setInt(29, r_mapeo.getIdCodigoOperacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(29, 0);
		    }
		    if (r_mapeo.getIdCodigoMovimiento()!=null)
		    {
		    	preparedStatement.setInt(30, r_mapeo.getIdCodigoMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setInt(30, 0);
		    }
		    if (r_mapeo.getIdCodigoDiferenciaMenos()!=null)
		    {
		    	preparedStatement.setInt(31, r_mapeo.getIdCodigoDiferenciaMenos());
		    }
		    else
		    {
		    	preparedStatement.setInt(31, 0);
		    }
		    
		    String ref="";
		    ref = RutinasFechas.añoFecha(r_mapeo.getFechaMovimiento());
		    ref = ref.concat("-");
		    ref = ref.concat(RutinasFechas.mesFecha(r_mapeo.getFechaMovimiento()));
		    ref = ref.concat("-");
		    ref = ref.concat(RutinasNumericas.formatearIntegerDigitosIzqda(idReg, 5));
		    preparedStatement.setString(32, ref);
		    r_mapeo.setNumeroRefInterno(ref);
		    
		    if (r_mapeo.getCodigoArticulo()!=null)
		    {
		    	preparedStatement.setString(33, r_mapeo.getCodigoArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(33, null);
		    }
		    if (r_mapeo.getDescripcionArticulo()!=null)
		    {
		    	preparedStatement.setString(34, RutinasCadenas.conversion(r_mapeo.getDescripcionArticulo()));
		    }
		    else
		    {
		    	preparedStatement.setString(34, null);
		    }

		    consultaContadoresEjercicioServer cont = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		    
		    if (!cont.comprobarContador(new Long(r_mapeo.getNumeroJustificante()), r_mapeo.getEjercicio(), "MANUAL SILICIE", r_mapeo.getCaeTitular()))
		    {
		    	cont.actualizarContador(r_mapeo.getEjercicio(), new Double(r_mapeo.getNumeroJustificante()), "MANUAL SILICIE" , r_mapeo.getCaeTitular());		    	
		    	preparedStatement.executeUpdate();
		    }
		    else
		    {
		    	return "Numerador";
		    }
		}
	    catch (Exception ex)
	    {
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarNuevoGeneracion(Connection r_con, MapeoMovimientosSilicie r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO " + this.tableGeneracion + "( ");
			cadenaSQL.append("id,");			
			cadenaSQL.append("clave_tabla, ");
			cadenaSQL.append("documento, ");
			cadenaSQL.append("serie, ");
			cadenaSQL.append("codigo, ");
			cadenaSQL.append("posicion, ");
			cadenaSQL.append("numero_justificante,");		
			cadenaSQL.append("numero_documento_id_od,");
			cadenaSQL.append("razon_social_od,");
			cadenaSQL.append("CAE_od,");
			cadenaSQL.append("numero_documento_id_rep,");
			cadenaSQL.append("razon_social_rep,");
			cadenaSQL.append("epigrafe,");
			cadenaSQL.append("codigo_epigrafe,");
			cadenaSQL.append("clave,");
			cadenaSQL.append("codigoNC,");
			cadenaSQL.append("descripcion,");
			cadenaSQL.append("unidad_medida,");
			cadenaSQL.append("cantidad,");
			cadenaSQL.append("grado_alcoholico,");
			cadenaSQL.append("alcoholPuro,");
			cadenaSQL.append("fecha_movimiento, ");
			cadenaSQL.append("fecha_registro, ");	
			
			cadenaSQL.append("id_doc_rep, ");
			cadenaSQL.append("id_doc_od, ");
			cadenaSQL.append("id_jus, ");
			cadenaSQL.append("id_rf, ");
			cadenaSQL.append("id_presentador, ");
			cadenaSQL.append("id_op, ");
			cadenaSQL.append("id_mov, ");
			cadenaSQL.append("id_dme, ");
			cadenaSQL.append("refInterna, ");
			cadenaSQL.append("articulo,");
			cadenaSQL.append("descripcionArticulo ) VALUES (");
			cadenaSQL.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    preparedStatement = r_con.prepareStatement(cadenaSQL.toString()); 
		    Integer id = this.obtenerSiguiente();
		    preparedStatement.setInt(1, id);
		    String fecha = "";

		    if (r_mapeo.getClaveTabla()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClaveTabla());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getAlbaran()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getAlbaran());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getPosicion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getPosicion());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getNumeroJustificante()!=null)
		    {
		    	if (r_mapeo.getNumeroJustificante().length()>0)
		    		preparedStatement.setString(7, r_mapeo.getNumeroJustificante().trim());
		    	else
		    		preparedStatement.setString(7, r_mapeo.getNumeroJustificante());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getNumeroDocumentoIdOD()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getNumeroDocumentoIdOD().trim());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getRazonSocialOD()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getRazonSocialOD());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getCaeOD()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getCaeOD().trim());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getNumeroDocumentoIdREP()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getNumeroDocumentoIdREP());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getRazonSocialREP()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getRazonSocialREP());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getEpigrafe()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getEpigrafe());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }		    
		    if (r_mapeo.getCodigoEpigrafe()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getCodigoEpigrafe());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getClave()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getClave());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getCodigoNC()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getCodigoNC());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(17, r_mapeo.getCodigoNC());
		    }
		    if (r_mapeo.getUnidad_medida()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getUnidad_medida());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }				
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(19, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setDouble(19, 0);
		    }
		    if (r_mapeo.getGrado()!=null)
		    {
		    	preparedStatement.setDouble(20, r_mapeo.getGrado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(20, 0);
		    }
		    if (r_mapeo.getAlcoholPuro()!=null)
		    {
		    	preparedStatement.setDouble(21, r_mapeo.getAlcoholPuro());
		    }
		    else
		    {
		    	preparedStatement.setDouble(21, 0);
		    }
		    if (r_mapeo.getFechaMovimiento()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaMovimiento());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(22, RutinasFechas.convertirAFechaMysql(fecha));
		    if (r_mapeo.getFechaRegistro()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaRegistro());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(23, RutinasFechas.convertirAFechaMysql(fecha));

		    if (r_mapeo.getIdCodigoDocumentoRep()!=null)
		    {
		    	preparedStatement.setInt(24, r_mapeo.getIdCodigoDocumentoRep());
		    }
		    else
		    {
		    	preparedStatement.setInt(24, 0);
		    }
		    if (r_mapeo.getIdCodigoDocumentoOd()!=null)
		    {
		    	preparedStatement.setInt(25, r_mapeo.getIdCodigoDocumentoOd());
		    }
		    else
		    {
		    	preparedStatement.setInt(25, 0);
		    }
		    if (r_mapeo.getIdCodigoJustificante()!=null)
		    {
		    	preparedStatement.setInt(26, r_mapeo.getIdCodigoJustificante());
		    }
		    else
		    {
		    	preparedStatement.setInt(26, 0);
		    }
		    if (r_mapeo.getIdCodigoRegimenFiscal()!=null)
		    {
		    	preparedStatement.setInt(27, r_mapeo.getIdCodigoRegimenFiscal());
		    }
		    else
		    {
		    	preparedStatement.setInt(27, 0);
		    }
		    if (r_mapeo.getIdPresentador()!=null)
		    {
		    	preparedStatement.setInt(28, r_mapeo.getIdPresentador());
		    }
		    else
		    {
		    	preparedStatement.setInt(28, 0);
		    }
		    if (r_mapeo.getIdCodigoOperacion()!=null)
		    {
		    	preparedStatement.setInt(29, r_mapeo.getIdCodigoOperacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(29, 0);
		    }
		    if (r_mapeo.getIdCodigoMovimiento()!=null)
		    {
		    	preparedStatement.setInt(30, r_mapeo.getIdCodigoMovimiento());
		    }
		    else
		    {
		    	preparedStatement.setInt(30, 0);
		    }
		    if (r_mapeo.getIdCodigoDiferenciaMenos()!=null)
		    {
		    	preparedStatement.setInt(31, r_mapeo.getIdCodigoDiferenciaMenos());
		    }
		    else
		    {
		    	preparedStatement.setInt(31, 0);
		    }

		    String ref=null;
		    ref = RutinasFechas.añoFecha(r_mapeo.getFechaMovimiento());
		    ref = ref.concat("-");
		    ref = ref.concat(RutinasFechas.mesFecha(r_mapeo.getFechaMovimiento()));
		    ref = ref.concat("-");
		    ref = ref.concat(RutinasNumericas.formatearIntegerDigitosIzqda(id, 5));
		    preparedStatement.setString(32, ref);
		    
		    if (r_mapeo.getCodigoArticulo()!=null)
		    {
		    	preparedStatement.setString(33, r_mapeo.getCodigoArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(33, null);
		    }
		    if (r_mapeo.getDescripcionArticulo()!=null)
		    {
		    	preparedStatement.setString(34, RutinasCadenas.conversion(r_mapeo.getDescripcionArticulo()));
		    }
		    else
		    {
		    	preparedStatement.setString(34, null);
		    }
		    preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	Notificaciones.getInstance().mensajeSeguimiento("ERROR Generacion Movimiento: " + r_mapeo.getCodigoArticulo() + " " + ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return null;
	}

	public String guardarCambios(MapeoMovimientosSilicie r_mapeo, MapeoMovimientosSilicie r_mapeo_orig)
	{
		Connection con = null;	
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
		try
		{
			
			boolean declarado = cds.comprobarDeclarado(r_mapeo_orig);
			
			if (!declarado)
			{
				
				cadenaSQL.append(" UPDATE "+ this.tableGeneracion + " set ");
				cadenaSQL.append("clave_tabla = ?,  ");
				cadenaSQL.append("documento = ?,  ");
				cadenaSQL.append("serie = ?,  ");
				cadenaSQL.append("codigo = ?,  ");
				cadenaSQL.append("posicion = ?,  ");
				cadenaSQL.append("numero_justificante = ?, ");		
				cadenaSQL.append("numero_documento_id_od = ?, ");
				cadenaSQL.append("razon_social_od = ?, ");
				cadenaSQL.append("CAE_od = ?, ");
				cadenaSQL.append("numero_documento_id_rep = ?, ");
				cadenaSQL.append("razon_social_rep = ?, ");
				cadenaSQL.append("epigrafe = ?, ");
				cadenaSQL.append("codigo_epigrafe = ?, ");
				cadenaSQL.append("clave = ?, ");
				cadenaSQL.append("codigoNC = ?, ");
				cadenaSQL.append("descripcion = ?, ");
				cadenaSQL.append("unidad_medida = ?, ");
				cadenaSQL.append("cantidad = ?, ");
				cadenaSQL.append("grado_alcoholico = ?, ");
				cadenaSQL.append("alcoholPuro = ?, ");
				cadenaSQL.append("fecha_movimiento = ?,  ");
				cadenaSQL.append("fecha_registro = ?,  ");				
				cadenaSQL.append("id_doc_rep = ?,  ");
				cadenaSQL.append("id_doc_od = ?,  ");
				cadenaSQL.append("id_jus = ?,  ");
				cadenaSQL.append("id_rf = ?,  ");
				cadenaSQL.append("id_presentador = ?,  ");
				cadenaSQL.append("id_op = ?,  ");
				cadenaSQL.append("id_mov = ?,  ");	
				cadenaSQL.append("id_dme = ?,  ");	
				cadenaSQL.append("articulo = ?,  ");	
				cadenaSQL.append("descripcionArticulo = ? ");
				cadenaSQL.append(" WHERE " + this.tableGeneracion+ ".id = ? " );
				
				con= this.conManager.establecerConexionInd();	
				preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
				
				String fecha = "";
				
				Integer id = this.obtenerId("Justificante", r_mapeo.getDescripcionJustificante());
				r_mapeo.setIdCodigoJustificante(id);
				id = this.obtenerId("Movimiento", r_mapeo.getDescripcionMovimiento());
				r_mapeo.setIdCodigoMovimiento(id);
				id = this.obtenerId("Regimen", r_mapeo.getDescripcionRegimenFiscal());
				r_mapeo.setIdCodigoRegimenFiscal(id);
				id = this.obtenerId("Presentador", r_mapeo.getCaeTitular());
				r_mapeo.setIdPresentador(id);
			 	id = this.obtenerId("DiferenciaMenos", r_mapeo.getDescripcionDiferenciaMenos());
			 	r_mapeo.setIdCodigoDiferenciaMenos(id);				
				id = this.obtenerId("DocumentoOd", r_mapeo.getDescripcionDocumentoOd());
				r_mapeo.setIdCodigoDocumentoOd(id);
				
				if (r_mapeo.getClaveTabla()!=null)
				{
					preparedStatement.setString(1, r_mapeo.getClaveTabla());
				}
				else
				{
					preparedStatement.setString(1, null);
				}
				
				if (r_mapeo.getDocumento()!=null)
				{
					preparedStatement.setString(2, r_mapeo.getDocumento());
				}
				else
				{
					preparedStatement.setString(2, null);
				}
				if (r_mapeo.getSerie()!=null)
				{
					preparedStatement.setString(3, r_mapeo.getSerie());
				}
				else
				{
					preparedStatement.setString(3, null);
				}
				if (r_mapeo.getAlbaran()!=null)
				{
					preparedStatement.setInt(4, r_mapeo.getAlbaran());
				}
				else
				{
					preparedStatement.setInt(4, 0);
				}
				if (r_mapeo.getPosicion()!=null)
				{
					preparedStatement.setInt(5, r_mapeo.getPosicion());
				}
				else
				{
					preparedStatement.setInt(5, 0);
				}
				if (r_mapeo.getNumeroJustificante()!=null)
				{
					preparedStatement.setString(6, r_mapeo.getNumeroJustificante());
				}
				else
				{
					preparedStatement.setString(6, null);
				}
				if (r_mapeo.getNumeroDocumentoIdOD()!=null)
				{
					preparedStatement.setString(7, r_mapeo.getNumeroDocumentoIdOD());
				}
				else
				{
					preparedStatement.setString(7, null);
				}
				if (r_mapeo.getRazonSocialOD()!=null)
				{
					preparedStatement.setString(8, r_mapeo.getRazonSocialOD());
				}
				else
				{
					preparedStatement.setString(8, null);
				}
				if (r_mapeo.getCaeOD()!=null)
				{
					preparedStatement.setString(9, r_mapeo.getCaeOD());
				}
				else
				{
					preparedStatement.setString(9, null);
				}
				if (r_mapeo.getNumeroDocumentoIdREP()!=null)
				{
					preparedStatement.setString(10, r_mapeo.getNumeroDocumentoIdREP());
				}
				else
				{
					preparedStatement.setString(10, null);
				}
				if (r_mapeo.getRazonSocialREP()!=null)
				{
					preparedStatement.setString(11, r_mapeo.getRazonSocialREP());
				}
				else
				{
					preparedStatement.setString(11, null);
				}
				if (r_mapeo.getEpigrafe()!=null)
				{
					preparedStatement.setString(12, r_mapeo.getEpigrafe());
				}
				else
				{
					preparedStatement.setString(12, null);
				}		    
				if (r_mapeo.getCodigoEpigrafe()!=null)
				{
					preparedStatement.setString(13, r_mapeo.getCodigoEpigrafe());
				}
				else
				{
					preparedStatement.setString(13, null);
				}
				if (r_mapeo.getClave()!=null)
				{
					preparedStatement.setString(14, r_mapeo.getClave());
				}
				else
				{
					preparedStatement.setString(14, null);
				}
				if (r_mapeo.getCodigoNC()!=null)
				{
					preparedStatement.setString(15, r_mapeo.getCodigoNC());
				}
				else
				{
					preparedStatement.setString(15, null);
				}
				if (r_mapeo.getDescripcion()!=null)
				{
					preparedStatement.setString(16, r_mapeo.getDescripcion());
				}
				else
				{
					preparedStatement.setString(16, null);
				}
				if (r_mapeo.getUnidad_medida()!=null)
				{
					preparedStatement.setString(17, r_mapeo.getUnidad_medida());
				}
				else
				{
					preparedStatement.setString(17, null);
				}				
				if (r_mapeo.getLitros()!=null)
				{
					preparedStatement.setDouble(18, r_mapeo.getLitros());
				}
				else
				{
					preparedStatement.setDouble(18, 0);
				}
				if (r_mapeo.getGrado()!=null)
				{
					preparedStatement.setDouble(19, r_mapeo.getGrado());
				}
				else
				{
					preparedStatement.setDouble(19, 0);
				}
				if (r_mapeo.getGrado()!=null && r_mapeo.getLitros()!=null)
				{
					r_mapeo.setAlcoholPuro(r_mapeo.getGrado()*r_mapeo.getLitros()/100);
					preparedStatement.setDouble(20, r_mapeo.getAlcoholPuro());
				}
				else
				{
					preparedStatement.setDouble(20, 0);
				}
				
				
				if (r_mapeo.getFechaMovimiento()!=null)
				{
					fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaMovimiento());
				}
				else
				{
					fecha = RutinasFechas.convertirDateToString(new Date());
				}
				preparedStatement.setString(21, RutinasFechas.convertirAFechaMysql(fecha));
				if (r_mapeo.getFechaRegistro()!=null)
				{
					fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaRegistro());
				}
				else
				{
					fecha = RutinasFechas.convertirDateToString(new Date());
				}
				preparedStatement.setString(22, RutinasFechas.convertirAFechaMysql(fecha));
				
				if (r_mapeo.getIdCodigoDocumentoRep()!=null)
				{
					preparedStatement.setInt(23, r_mapeo.getIdCodigoDocumentoRep());
				}
				else
				{
					preparedStatement.setInt(23, 0);
				}
				if (r_mapeo.getIdCodigoDocumentoOd()!=null)
				{
					preparedStatement.setInt(24, r_mapeo.getIdCodigoDocumentoOd());
				}
				else
				{
					preparedStatement.setInt(24, 0);
				}
				if (r_mapeo.getIdCodigoJustificante()!=null)
				{
					preparedStatement.setInt(25, r_mapeo.getIdCodigoJustificante());
				}
				else
				{
					preparedStatement.setInt(25, 0);
				}
				if (r_mapeo.getIdCodigoRegimenFiscal()!=null)
				{
					preparedStatement.setInt(26, r_mapeo.getIdCodigoRegimenFiscal());
				}
				else
				{
					preparedStatement.setInt(26, 0);
				}
				if (r_mapeo.getIdPresentador()!=null)
				{
					preparedStatement.setInt(27, r_mapeo.getIdPresentador());
				}
				else
				{
					preparedStatement.setInt(27, 0);
				}
				if (r_mapeo.getIdCodigoOperacion()!=null)
				{
					preparedStatement.setInt(28, r_mapeo.getIdCodigoOperacion());
				}
				else
				{
					preparedStatement.setInt(28, 0);
				}
				if (r_mapeo.getIdCodigoMovimiento()!=null)
				{
					preparedStatement.setInt(29, r_mapeo.getIdCodigoMovimiento());
				}
				else
				{
					preparedStatement.setInt(29, 0);
				}
				if (r_mapeo.getIdCodigoDiferenciaMenos()!=null)
				{
					preparedStatement.setInt(30, r_mapeo.getIdCodigoDiferenciaMenos());
				}
				else
				{
					preparedStatement.setInt(30, 0);
				}
				
				if (r_mapeo.getCodigoArticulo()!=null)
				{
					preparedStatement.setString(31, r_mapeo.getCodigoArticulo());
				}
				else
				{
					preparedStatement.setString(31, null);
				}
				if (r_mapeo.getDescripcionArticulo()!=null)
				{
					preparedStatement.setString(32, r_mapeo.getDescripcionArticulo());
				}
				else
				{
					preparedStatement.setString(32, null);
				}
				preparedStatement.setInt(33, r_mapeo_orig.getIdCodigo());
				
				preparedStatement.executeUpdate();
			}
			else
			{
				return "Registro declarado, no se puede modificar";
			}
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	

		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
		try
		{
			con= this.conManager.establecerConexionInd();
			boolean declarado = cds.comprobarDeclarado(r_mapeo);
			if (!declarado) eliminarRegistro(con, this.tableGeneracion, this.identificadorTabla, r_mapeo.getIdCodigo());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String generacionMovimientosDeclaracionSilicielobal(String r_titular, Integer r_ejercicio, Integer r_mes, String r_estado, String r_fecha)
	{
		
		String rdo=null;
		Integer idPresentador = null;
		ArrayList<MapeoMovimientosSilicie> vector = null;
		
		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
		
		MapeoMovimientosSilicie mapeo = new MapeoMovimientosSilicie();
		mapeo.setCaeTitular(r_titular);
		
    	mapeo.setEjercicio(r_ejercicio);
    	mapeo.setMes(r_mes);
    	mapeo.setEstado(r_estado);
    	mapeo.setFechaPresentacion(RutinasFechas.conversionDeString(r_fecha));
    	
    	vector = this.datosMovimientosDeclararSilicieGlobal(mapeo);
    	
    	rdo = cds.eliminarGeneradosCSV();
    	
    	if (rdo==null)
    	{
    		idPresentador = this.obtenerId("Presentador", r_titular);
    		
    		if (r_estado.equals("Declarados"))
    		{
    			rdo = cds.eliminarDeclarados(r_fecha, idPresentador,r_mes,true);
    		}
    		    		
	    	for (int i = 0; i< vector.size();i++)
	    	{
	    		MapeoMovimientosSilicie mapeoObtenido = (MapeoMovimientosSilicie) vector.get(i);
	    			mapeoObtenido.setFechaPresentacion(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));
	    			mapeoObtenido.setIdPresentador(idPresentador);
	    		if (rdo==null && r_estado=="Anulados")
	    		{
	    			mapeoObtenido.setNumeroRefInterno("A-" + mapeoObtenido.getNumeroRefInterno());
	    			rdo = cds.guardarNuevoDeclaracionCSVAnulados(mapeoObtenido);
	    		}
	    		else
	    		{
	    			rdo = cds.guardarNuevo(mapeoObtenido);	    		
	    			if (rdo==null) rdo = cds.guardarNuevoDeclaracionCSV(mapeoObtenido);
	    		}
	    		if (rdo!=null) break;
	    	}
    	}
    	
		return rdo;
	}

	public String eliminarMovimientosDeclaracionSilicielobal(String r_titular, Integer r_ejercicio, Integer r_mes, String r_fecha, boolean r_apertura)
	{
		String rdo=null;
		Integer idPresentador = null;
		
		consultaDeclaracionSilicieServer cds = consultaDeclaracionSilicieServer.getInstance(CurrentUser.get());
		
		idPresentador = this.obtenerId("Presentador", r_titular);
		
		if (!cds.comprobarMesCerrado(r_ejercicio,r_titular, r_mes))
		{
			
	    	rdo = cds.eliminarGeneradosCSV();
	    	
	    	if (rdo==null)
	    	{
	    		rdo = cds.eliminarDeclarados(r_fecha, idPresentador, r_mes, r_apertura);
	    	}
		}
		else
		{
			rdo = "Mes Cerrado";
		}
		return rdo;
	}
	
	public String eliminarGenerados(String r_titular, Integer r_ejercicio, Integer r_mes)
	{

		/*
		 * Borra todos los movimientos generados desde greensys pendientes de declarar del ejercicio y mes indicados
		 */
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		String rdo = null;
		
		try
		{
			cadenaSQL.append(" DELETE FROM " + this.tableGeneracion);            
			cadenaSQL.append(" WHERE year(fecha_movimiento) = ? ");
			cadenaSQL.append(" and month(fecha_movimiento) = ? ");
			cadenaSQL.append(" and id_presentador in (select id from fin_presentadores where cae ='" + r_titular.trim() + "') ");
			cadenaSQL.append(" and id not in (select idRegistroDeclarado from fin_sil_declarados) ");
			cadenaSQL.append(" and (clave_tabla is null or clave_tabla <> 'M' )  ");
			
			con= this.conManager.establecerConexionInd();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_ejercicio);
			preparedStatement.setInt(2, r_mes);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			rdo = ex.getMessage();	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public boolean comprobarFechasRegistro(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
//		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id, ");
		cadenaSQL.append(" a.clave_tabla reg_clt, ");
		cadenaSQL.append(" a.documento reg_doc, ");
		cadenaSQL.append(" a.serie reg_ser, ");
		cadenaSQL.append(" a.codigo reg_alb, ");
		cadenaSQL.append(" a.posicion reg_pos, ");
		
		cadenaSQL.append(" a.numero_justificante reg_jus,");		
		cadenaSQL.append(" a.numero_documento_id_od reg_ide,");
		cadenaSQL.append(" a.razon_social_od reg_nod,");
		cadenaSQL.append(" a.CAE_od reg_cae,");
		cadenaSQL.append(" a.numero_documento_id_rep reg_idr,");
		cadenaSQL.append(" a.razon_social_rep reg_nre,");
		
		cadenaSQL.append(" a.epigrafe reg_epi,");
		cadenaSQL.append(" a.codigo_epigrafe reg_cep,");
		cadenaSQL.append(" a.clave reg_cl,");
		
		cadenaSQL.append(" a.codigoNC reg_art,");
		cadenaSQL.append(" a.descripcion reg_des,");
		cadenaSQL.append(" a.articulo lin_art,");
		cadenaSQL.append(" a.descripcionArticulo lin_des,");
		
		cadenaSQL.append(" a.unidad_medida reg_ud,");
		cadenaSQL.append(" a.cantidad reg_ltr,");
		cadenaSQL.append(" a.grado_alcoholico reg_grd,");
		cadenaSQL.append(" a.alcoholPuro reg_alc,");
		
		cadenaSQL.append(" year(a.fecha_movimiento) reg_eje, ");
		cadenaSQL.append(" month(a.fecha_movimiento) reg_mes, ");
		cadenaSQL.append(" a.fecha_movimiento reg_fec, ");
		cadenaSQL.append(" a.fecha_registro reg_fer, ");
		
		cadenaSQL.append(" dre.codigo dre_cod,");
		cadenaSQL.append(" dre.descripcion dre_des, ");
		cadenaSQL.append(" dod.codigo dod_cod, ");
		cadenaSQL.append(" dod.descripcion dod_des, ");
		cadenaSQL.append(" op.codigo op_cod, ");
		cadenaSQL.append(" op.descripcion op_des, ");
		cadenaSQL.append(" mov.codigo mov_cod, ");
		cadenaSQL.append(" mov.descripcion mov_des, ");
		cadenaSQL.append(" mov.signo mov_sig, ");
		cadenaSQL.append(" jus.codigo jus_cod, ");
		cadenaSQL.append(" jus.descripcion jus_des, ");
		cadenaSQL.append(" rf.codigo rf_cod, ");
		cadenaSQL.append(" rf.descripcion rf_des, ");
		cadenaSQL.append(" pre.nombre pre_nom, ");
		cadenaSQL.append(" pre.cae pre_cae, ");
		cadenaSQL.append(" dme.codigo dme_cod, ");
		cadenaSQL.append(" dme.descripcion dme_des, ");
		if (!r_mapeo.getEstado().equals("Pendientes"))
		{
			cadenaSQL.append(" dcl.fechaPresentacion dcl_fec, ");
			cadenaSQL.append(" dcl.numeroAsiento dcl_as, ");
		}
		cadenaSQL.append(" a.refInterna reg_ref ");
		
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		
		if (r_mapeo.getEstado().equals("Declarados")) cadenaSQL.append(" inner join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		if (r_mapeo.getEstado().equals("Todos")) cadenaSQL.append(" left outer join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		
		cadenaSQL.append(" inner join fin_silicie_rf rf on rf.id = a.id_rf ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov ");
		
		cadenaSQL.append(" left outer join fin_silicie_doc dre on dre.id = a.id_doc_rep ");
		cadenaSQL.append(" left outer join fin_silicie_doc dod on dod.id = a.id_doc_od ");
		cadenaSQL.append(" left outer join fin_silicie_jus jus on jus.id = a.id_jus ");
		cadenaSQL.append(" left outer join fin_silicie_op op on op.id = a.id_op ");
		cadenaSQL.append(" left outer join fin_silicie_dme dme on dme.id = a.id_dme ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha_movimiento) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha_movimiento) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCodigoNC()!=null && r_mapeo.getCodigoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigoNC = '" + r_mapeo.getCodigoNC() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCodigoArticulo()!=null && r_mapeo.getCodigoArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.articulo = '" + r_mapeo.getCodigoArticulo() + "' ");
				}
				if (r_mapeo.getDescripcionArticulo()!=null && r_mapeo.getDescripcionArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcionArticulo = '" + r_mapeo.getDescripcionArticulo() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.clave_tabla = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pre.cae = '" + r_mapeo.getCaeTitular() + "' ");
				}

				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Declarados") && r_mapeo.getFechaPresentacion()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dcl.fechaPresentacion = '"+ RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaPresentacion())) + "' ");
				}
				
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Pendientes"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.fecha_registro - a.fecha_movimiento > 1  ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by a.codigoNC, a.refInterna ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return false;		
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;	
	}

	public boolean comprobarSoloApertura(String r_ejercicio,String r_mes, String r_titular)
	{
		Connection con = null;	
		Statement cs = null;
		
		ResultSet rsOpcion = null;
//		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(a.id) reg_cuantos ");
		
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov and mov.codigo <> 'A01' ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		
		try
		{
			if (r_ejercicio!=null && r_ejercicio.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" year(a.fecha_movimiento) = " + r_ejercicio + " ");
			}
			if (r_ejercicio!=null && r_ejercicio.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" month(a.fecha_movimiento) = 1 ");
			}
			if (r_titular!=null && r_titular.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" pre.cae = '" + r_titular + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getInt("reg_cuantos")>0)
					return false;
				else
					return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;	
	}
	
	public boolean comprobarMesDeclarado(MapeoMovimientosSilicie r_mapeo, boolean r_apertura)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
//		MapeoMovimientosSilicie mapeoMovimientosSilicie = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.id reg_id, ");
		cadenaSQL.append(" a.clave_tabla reg_clt, ");
		cadenaSQL.append(" a.documento reg_doc, ");
		cadenaSQL.append(" a.serie reg_ser, ");
		cadenaSQL.append(" a.codigo reg_alb, ");
		cadenaSQL.append(" a.posicion reg_pos, ");
		
		cadenaSQL.append(" a.numero_justificante reg_jus,");		
		cadenaSQL.append(" a.numero_documento_id_od reg_ide,");
		cadenaSQL.append(" a.razon_social_od reg_nod,");
		cadenaSQL.append(" a.CAE_od reg_cae,");
		cadenaSQL.append(" a.numero_documento_id_rep reg_idr,");
		cadenaSQL.append(" a.razon_social_rep reg_nre,");
		
		cadenaSQL.append(" a.epigrafe reg_epi,");
		cadenaSQL.append(" a.codigo_epigrafe reg_cep,");
		cadenaSQL.append(" a.clave reg_cl,");
		
		cadenaSQL.append(" a.codigoNC reg_art,");
		cadenaSQL.append(" a.descripcion reg_des,");
		cadenaSQL.append(" a.articulo lin_art,");
		cadenaSQL.append(" a.descripcionArticulo lin_des,");
		
		cadenaSQL.append(" a.unidad_medida reg_ud,");
		cadenaSQL.append(" a.cantidad reg_ltr,");
		cadenaSQL.append(" a.grado_alcoholico reg_grd,");
		cadenaSQL.append(" a.alcoholPuro reg_alc,");
		
		cadenaSQL.append(" year(a.fecha_movimiento) reg_eje, ");
		cadenaSQL.append(" month(a.fecha_movimiento) reg_mes, ");
		cadenaSQL.append(" a.fecha_movimiento reg_fec, ");
		cadenaSQL.append(" a.fecha_registro reg_fer, ");
		
		cadenaSQL.append(" dre.codigo dre_cod,");
		cadenaSQL.append(" dre.descripcion dre_des, ");
		cadenaSQL.append(" dod.codigo dod_cod, ");
		cadenaSQL.append(" dod.descripcion dod_des, ");
		cadenaSQL.append(" op.codigo op_cod, ");
		cadenaSQL.append(" op.descripcion op_des, ");
		cadenaSQL.append(" mov.codigo mov_cod, ");
		cadenaSQL.append(" mov.descripcion mov_des, ");
		cadenaSQL.append(" mov.signo mov_sig, ");
		cadenaSQL.append(" jus.codigo jus_cod, ");
		cadenaSQL.append(" jus.descripcion jus_des, ");
		cadenaSQL.append(" rf.codigo rf_cod, ");
		cadenaSQL.append(" rf.descripcion rf_des, ");
		cadenaSQL.append(" pre.nombre pre_nom, ");
		cadenaSQL.append(" pre.cae pre_cae, ");
		cadenaSQL.append(" dme.codigo dme_cod, ");
		cadenaSQL.append(" dme.descripcion dme_des, ");
		if (!r_mapeo.getEstado().equals("Pendientes"))
		{
			cadenaSQL.append(" dcl.fechaPresentacion dcl_fec, ");
			cadenaSQL.append(" dcl.numeroAsiento dcl_as, ");
		}
		cadenaSQL.append(" a.refInterna reg_ref ");
		
		cadenaSQL.append(" from " + this.tableGeneracion + " a ");
		
		if (r_mapeo.getEstado().equals("Declarados")) cadenaSQL.append(" inner join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		if (r_mapeo.getEstado().equals("Todos")) cadenaSQL.append(" left outer join fin_sil_declarados dcl on dcl.idRegistroDeclarado = a.id ");
		
		cadenaSQL.append(" inner join fin_silicie_rf rf on rf.id = a.id_rf ");
		cadenaSQL.append(" inner join fin_presentadores pre on pre.id = a.id_presentador ");
		
		if (r_apertura)
			cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov and mov.codigo='A01' ");
		else
			cadenaSQL.append(" inner join fin_silicie_mov mov on mov.id = a.id_mov and mov.codigo!='A01' ");
		
		cadenaSQL.append(" left outer join fin_silicie_doc dre on dre.id = a.id_doc_rep ");
		cadenaSQL.append(" left outer join fin_silicie_doc dod on dod.id = a.id_doc_od ");
		cadenaSQL.append(" left outer join fin_silicie_jus jus on jus.id = a.id_jus ");
		cadenaSQL.append(" left outer join fin_silicie_op op on op.id = a.id_op ");
		cadenaSQL.append(" left outer join fin_silicie_dme dme on dme.id = a.id_dme ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha_movimiento) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha_movimiento) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCodigoNC()!=null && r_mapeo.getCodigoNC().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigoNC = '" + r_mapeo.getCodigoNC() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCodigoArticulo()!=null && r_mapeo.getCodigoArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.articulo = '" + r_mapeo.getCodigoArticulo() + "' ");
				}
				if (r_mapeo.getDescripcionArticulo()!=null && r_mapeo.getDescripcionArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descripcionArticulo = '" + r_mapeo.getDescripcionArticulo() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.clave_tabla = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" pre.cae = '" + r_mapeo.getCaeTitular() + "' ");
				}

				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Pendientes"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.fecha_registro - a.fecha_movimiento > 1  ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by a.codigoNC, a.refInterna ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return false;		
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;	
	}


	public boolean comprobarGrado(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		String guia = null;
		boolean entradasErroneas = false;
//		boolean salidasErroneas = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT numeroReferencia ter_ref ");
		cadenaSQL.append(" from fin_emcs_terceros a ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeDestino = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and (a.grado < 10 or a.grado > 20) ");
			}
			else
			{
				cadenaSQL.append(" where (a.grado < 10 or a.grado > 20) ");
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				entradasErroneas = true;
				if (guia==null) guia="";
				guia = guia + "-" + rsOpcion.getString("ter_ref");
			}
			
			
			if (entradasErroneas) Notificaciones.getInstance().mensajeError("Hay guias de entrada con el grado Erroneo " + guia);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return entradasErroneas;	
	}

	public boolean comprobarARC(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		String art = null;
		boolean entradasErroneas = false;
		boolean salidasErroneas = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT arc ter_ref ");
		cadenaSQL.append(" from fin_emcs_terceros a ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeDestino = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and (a.arc = 0 or a.arc= '' or length(arc)>21) ");
			}
			else
			{
				cadenaSQL.append(" where (a.arc = 0 or a.arc= '' or length(arc)>21) ");
				
			}
			
			cadenaSQL.append(" and a.albaran > 0  ");
//			cadenaSQL.append(" and a.litros > " + litrosMinimos);
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				entradasErroneas = true;
				if (art==null) art="";
				art = art + "-" + rsOpcion.getString("ter_ref");
			}
			
			
			
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT arc ter_ref ");
			cadenaSQL.append(" from fin_emcs_borsao a ");
			
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and (a.arc = 0 or a.arc= '' or length(arc)>21) ");
			}
			else
			{
				cadenaSQL.append(" where (a.arc = 0 or a.arc= '' or length(arc)>21) ");
				
			}
			cadenaSQL.append(" and a.albaran > 0  ");
//			cadenaSQL.append(" and a.litros > " + litrosMinimos);
			
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion.close();
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				salidasErroneas = true;
				if (art==null) art="";
				art = art + "-" + rsOpcion.getString("ter_ref");
			}
			
			
			if (entradasErroneas) Notificaciones.getInstance().mensajeError("Hay Entradas con Codigos ARC erroneos " + art);
			if (salidasErroneas) Notificaciones.getInstance().mensajeError("Hay Salidas con Codigos ARC erroneos " + art);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		if (entradasErroneas || salidasErroneas) return true; else return false;	
	}
	
	public boolean comprobarNR(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		boolean entradasErroneas = false;
		boolean salidasErroneas = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT numeroReferencia ter_ref ");
		cadenaSQL.append(" from fin_emcs_terceros a ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeDestino = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and a.numeroReferencia = 0 ");
			}
			else
			{
				cadenaSQL.append(" where a.numeroReferencia = 0 ");
				
			}
			
			cadenaSQL.append(" and a.albaran > 0  ");
//			cadenaSQL.append(" and a.litros > " + litrosMinimos);
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				entradasErroneas = true;
			}
			
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT numeroReferencia ter_ref ");
			cadenaSQL.append(" from fin_emcs_borsao a ");
			
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and a.numeroReferencia = 0 ");
			}
			else
			{
				cadenaSQL.append(" where a.numeroReferencia = 0 ");
				
			}
			cadenaSQL.append(" and a.albaran > 0  ");
//			cadenaSQL.append(" and a.litros > " + litrosMinimos);
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion.close();
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				salidasErroneas = true;
			}
			if (entradasErroneas) Notificaciones.getInstance().mensajeError("Hay Entradas con nunmeroReferencia = 0 " );
			if (salidasErroneas) Notificaciones.getInstance().mensajeError("Hay Salidas con nunmeroReferencia = 0 " );
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		if (entradasErroneas || salidasErroneas) return true; else return false;	
	}

	public boolean comprobarCaes(MapeoMovimientosSilicie r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		boolean entradasErroneas = false;
		boolean salidasErroneas = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT id ter_ct ");
		cadenaSQL.append(" from fin_emcs_terceros a ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeDestino = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and a.caeExpedidor COLLATE latin1_spanish_ci = a.caeDestino COLLATE latin1_spanish_ci");
			}
			else
			{
				cadenaSQL.append(" where a.caeExpedidor COLLATE latin1_spanish_ci = a.caeDestino COLLATE latin1_spanish_ci");
			}
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				entradasErroneas = true;;		
			}
			
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT id bor_ct ");
			cadenaSQL.append(" from fin_emcs_borsao a ");
			
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" year(a.fecha) = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getMes()!=null && r_mapeo.getMes()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" month(a.fecha) = " + r_mapeo.getMes() + " ");
				}
				if (r_mapeo.getCaeTitular()!=null && r_mapeo.getCaeTitular().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getCaeTitular() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
				cadenaSQL.append(" and a.caeExpedidor = a.exciseNumber ");
			}
			else
			{
				cadenaSQL.append(" where a.caeExpedidor = a.exciseNumber ");
			}
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion.close();
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				salidasErroneas = true;		
			}
			if (entradasErroneas) Notificaciones.getInstance().mensajeError("Hay guias de entrada con el mismo CAE de Origen que de Destino");
			if (salidasErroneas) Notificaciones.getInstance().mensajeError("Hay salidas con el mismo CAE de Origen que de Destino");
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		if (entradasErroneas || salidasErroneas) return true; else return false;	
	}
	
	public void eliminarVerificadosOk(Integer r_id, String r_ejercicio, String r_mes, String r_titular)
	{
		Connection conG = null;
		Connection con = null;	
		Statement cs = null;

		HashMap<String, Double> hashSilicie = null;
		ResultSet rsOpcion = null;
		PreparedStatement preparedStatement = null;

		StringBuffer cadenaSQL = new StringBuffer();
		String digito ="0";
		
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (new Integer(r_ejercicio) >= ejercicioActual) digito="0";
		if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(ejercicioActual-new Integer(r_ejercicio));

		cadenaSQL = new StringBuffer();
    	cadenaSQL.append(" select ");
    	cadenaSQL.append(" tmp_existencias.idCodigo id, ");
    	cadenaSQL.append(" tmp_existencias.articulo art, ");
    	cadenaSQL.append(" tmp_existencias.finales fin ");
    	cadenaSQL.append(" from tmp_existencias ");
    	cadenaSQL.append(" where tmp_existencias.idCodigo = " + r_id);
    	
    	try 
    	{
    		/*
    		 * Relleno las existencias calculadas por el silicie
    		 */
    		con= this.conManager.establecerConexionInd();			
    		Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
    		cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
    		rsOpcion= cs.executeQuery(cadenaSQL.toString());
    		hashSilicie = new HashMap<String, Double>();
    		while(rsOpcion.next())
    		{
    			hashSilicie.put(rsOpcion.getString("art").trim(), rsOpcion.getDouble("fin"));
    		}

    		cs.close();
    		rsOpcion.close();
    		cs = null;
    		rsOpcion=null;
    		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
    		String almacen = cas.obtenerAlmacenPorCAE2(r_titular);
    		
    		cadenaSQL = new StringBuffer();

    		cadenaSQL.append(" select ");
        	cadenaSQL.append(" e.articulo art, ");
        	
        	if (!r_mes.contentEquals("0"))
        		cadenaSQL.append(" sum(a.stock_" + new Integer(r_mes.trim()) + "* e.marca) fin ");
        	else
        		cadenaSQL.append(" sum(a.unid_inicio * e.marca) fin ");
        	
        	cadenaSQL.append(" from a_exis_" + digito + " a, e_articu e ");
        	cadenaSQL.append(" where a.articulo = e.articulo ");
        	cadenaSQL.append(" and (a.articulo matches '0101*' or a.articulo matches '0102*' or a.articulo matches '0111*' or a.articulo matches '0103*') ");
        	
        	if (almacen.equals("1")) cadenaSQL.append(" and a.almacen in (" + consultaAlmacenesServer.capuchinos + ") ");
        	if (!almacen.equals("1")) cadenaSQL.append(" and a.almacen not in (" + consultaAlmacenesServer.restoAlmacenes + ") ");

        	cadenaSQL.append(" group by 1 ");
        	/*
        	 * Relleno los datos de greensys
        	 */
        	conG= this.conManager.establecerConexionGestionInd();			
    		Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
    		cs = conG.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
    		rsOpcion= cs.executeQuery(cadenaSQL.toString());
    		
    		while(rsOpcion.next())
    		{
    			Double fin = hashSilicie.get(rsOpcion.getString("art").trim());
    			
    			if (fin!=null && fin.compareTo(rsOpcion.getDouble("fin"))==0)
    			{
					cadenaSQL = new StringBuffer();
					cadenaSQL.append(" delete from tmp_existencias  ");
					cadenaSQL.append(" where cae = '" + r_titular + "' ");
					cadenaSQL.append(" and idCodigo = '" + r_id + "' ");
					cadenaSQL.append(" and articulo = '" + rsOpcion.getString("art").trim() + "' ");
					
					preparedStatement = con.prepareStatement(cadenaSQL.toString());
					preparedStatement.executeUpdate();	
    			}
    			else
    			{
    				if (fin==null) fin = new Double(0);
					cadenaSQL = new StringBuffer();
					cadenaSQL.append(" update tmp_existencias  ");
					cadenaSQL.append(" set cuadre = '" + (fin - ((Double) rsOpcion.getDouble("fin")).doubleValue()) + "' ");
					cadenaSQL.append(" where cae = '" + r_titular + "' ");
					cadenaSQL.append(" and idCodigo = '" + r_id + "' ");
					cadenaSQL.append(" and articulo = '" + rsOpcion.getString("art").trim() + "' ");
					
					preparedStatement = con.prepareStatement(cadenaSQL.toString());
					preparedStatement.executeUpdate();
    			}
    		}
    	}
    	catch (Exception ex)
    	{
    		ex.printStackTrace();
    	}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conG!=null)
				{
					conG.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

	}
	
	public Integer generacionDatosTemporalExistencias(String r_area, String r_ejercicio,String r_mes, String r_titular, String r_articulo)
	{
		Connection con = null;	
		Statement cs = null;

		Integer idReg =null;
		boolean traerDesc = false;
		String alias = null;
		ArrayList<MapeoInventarioSilicie> vector = null;
		MapeoInventarioSilicie mapeo = null;
		ResultSet rsOpcion = null;
		PreparedStatement preparedStatement = null;
		HashMap<String, String> hashDescripciones=null;
		StringBuffer cadenaSQL = new StringBuffer();
		String campo1 = null;
		String campo2 = null;
		
		/*
		 * 		Partimos de la tabla de movimientos
		 * 
		 * 1.- 	Teniendo en cuenta el presentador que recibimos r_titular
		 * 2.- 	Teniendo en cuenta el ejercicio y mes que recibimos r_ejercicio. r_mes
		 * 
		 * 3.- 	Opcionalmente consideraremos el artciulo concreto consultado recibido en r_articulo
		 * 
		 * NOTA: Todos los movimientos a tener en cuenta tienen que estar declarados o validados
		 * 
		 * 		Proceso de generacion
		 * 
		 * 1.- 	vector de articulos declarados obteniendo apertura
		 * 2.- 	vector de articulos declarados obteniendo la suma de entradas
		 * 3.- 	vector de articulos declarados obteniendo la suma de salidas
		 * 4.- 	generacion de un vector que incluya la informacion completa por articulo y calculando el resultante final = + apertura + entradas - salidas
		 * 
		 * 		Tabla
		 * 
		 * 1.-	Almacenaremos todos los datos del vector calculado finalmente en el paso anterior
		 * 2.-	Para todos los registros guardaremos el mismo id. Id que devolveremos para pasarselo al report
		 * 
		 */
		


		try
		{
			
			if (!r_area.contains("NC"))
			{
				campo1 = "articulo";
				traerDesc = true;

				cadenaSQL.append(" SELECT articulo reg_art,");
				cadenaSQL.append(" descrip_articulo reg_des ");
				cadenaSQL.append(" from e_articu ");
				cadenaSQL.append(" where articulo like '0101%' or articulo like '0103%' or articulo like '0111%' or articulo like '0102%' ");
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				hashDescripciones = new HashMap<String, String>();
				while(rsOpcion.next())
				{
					hashDescripciones.put(rsOpcion.getString("reg_art").trim(), rsOpcion.getString("reg_des"));
				}
			}
			else
			{
				campo1 = "codigoNC";
				campo2 = "descripcion";
			}

			
			if (r_mes!=null && r_mes.toUpperCase().equals("TODOS")) r_mes = "12";
			/*
			 * cojo los datos de apertura
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT fin_sil_registros." + campo1 + " reg_art, ");
			if (r_area.contains("NC")) cadenaSQL.append(" fin_sil_registros.descripcion reg_des, ");
			cadenaSQL.append(" SUM( CASE WHEN fin_silicie_mov.codigo = 'A01' THEN fin_sil_registros.cantidad ELSE 0 END ) ape, "); 
			cadenaSQL.append(" SUM( CASE WHEN fin_silicie_mov.codigo <> 'A01' and fin_silicie_mov.signo = '-' THEN fin_sil_registros.cantidad ELSE 0 END ) sal, "); 
			cadenaSQL.append(" SUM( CASE WHEN fin_silicie_mov.codigo <> 'A01' and fin_silicie_mov.signo = '+' THEN fin_sil_registros.cantidad ELSE 0 END ) ent ");
			cadenaSQL.append(" from fin_sil_registros");
			cadenaSQL.append(" inner join fin_presentadores on fin_presentadores.id = fin_sil_registros.id_presentador and fin_presentadores.cae = '" + r_titular + "' ");
			cadenaSQL.append(" inner join fin_silicie_mov on fin_silicie_mov.id = fin_sil_registros.id_mov ");
			cadenaSQL.append(" where year(fin_sil_registros.fecha_movimiento) = '" + r_ejercicio + "' ");
			cadenaSQL.append(" and month(fin_sil_registros.fecha_movimiento) <= '" + r_mes + "' ");
			if (r_articulo!=null) cadenaSQL.append(" and fin_sil_registros." + campo1 + " = '" + r_articulo + "'");
			
			if (r_area.contains("NC")) cadenaSQL.append(" group by 1,2");
			else cadenaSQL.append(" group by 1");
			
			cadenaSQL.append(" order by 1 ");
			
			rsOpcion.close();
			rsOpcion=null;
			cs.close();
			cs=null;
			con.close();
			con=null;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoInventarioSilicie>();
			
			while(rsOpcion.next())
			{

				mapeo = new MapeoInventarioSilicie();
				mapeo.setCodigo(rsOpcion.getString("reg_art").trim());
				if (traerDesc) mapeo.setDescripcion(hashDescripciones.get(rsOpcion.getString("reg_art").trim()));
					else mapeo.setDescripcion(rsOpcion.getString("reg_des").trim());
				
				mapeo.setIniciales(rsOpcion.getDouble("ape"));
				mapeo.setSalidas(rsOpcion.getDouble("sal"));
				mapeo.setEntradas(rsOpcion.getDouble("ent"));
				vector.add(mapeo);
			}
	
			
			/*
			 * Llegados a este punto, si tenemos datos que insertar vamos a proceder a borrar los datos
			 * que tengamos de generaciones previas de este cae, ejercicio y mes
			 */
			if (vector!=null && vector.size()>0)
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" delete from tmp_existencias  ");
				cadenaSQL.append(" where cae = '" + r_titular + "' ");
				
				preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    preparedStatement.executeUpdate();
			    /*
			     * LLamo al proceso de guardar la temporal con los datos del vector calculado
			     */
			    idReg = this.obtenerSiguienteTemporal();
			    if (idReg==null) idReg=1;
			    
			    for (int i=0;i<vector.size();i++)
			    {
			    	MapeoInventarioSilicie mapeoGuardar =(MapeoInventarioSilicie) vector.get(i);
			    	
			    	cadenaSQL = new StringBuffer();
			    	cadenaSQL.append(" INSERT INTO tmp_existencias ( ");
			    	cadenaSQL.append("idCodigo, ");
			    	cadenaSQL.append("cae, ");
			    	cadenaSQL.append("articulo, ");
			    	cadenaSQL.append("descripcion, ");
			    	cadenaSQL.append("iniciales, ");
			    	cadenaSQL.append("entradas, ");
			    	cadenaSQL.append("salidas, ");
			    	cadenaSQL.append("finales, ");
			    	cadenaSQL.append("cuadre ) VALUES (");
			    	cadenaSQL.append("?,?,?,?,?,?,?,?,?) ");
			    	
			    	preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    	
			    	preparedStatement.setInt(1, idReg);
			    	preparedStatement.setString(2, r_titular);
			    	
			    	if (mapeoGuardar.getCodigo()!=null)
			    	{
			    		preparedStatement.setString(3, mapeoGuardar.getCodigo());
			    	}
			    	else
			    	{
			    		preparedStatement.setString(3, null);
			    	}
			    	if (mapeoGuardar.getDescripcion()!=null)
			    	{
			    		preparedStatement.setString(4, mapeoGuardar.getDescripcion());
			    	}
			    	else
			    	{
			    		preparedStatement.setString(4, null);
			    	}
			    	
			    	double ent = 0;
			    	double sal = 0;
			    	double ini = 0;
			    	
			    	if (mapeoGuardar.getIniciales()==null)
			    		preparedStatement.setDouble(5, 0);
			    	else
			    	{
			    		ini = mapeoGuardar.getIniciales();
			    		preparedStatement.setDouble(5, mapeoGuardar.getIniciales());
			    	}
			    	if (mapeoGuardar.getEntradas()==null)
			    		preparedStatement.setDouble(6, 0);
			    	else
			    	{
			    		ent = mapeoGuardar.getEntradas();
			    		preparedStatement.setDouble(6, mapeoGuardar.getEntradas());
			    	}
			    	if (mapeoGuardar.getSalidas()==null)
			    		preparedStatement.setDouble(7, 0);
			    	else
			    	{
			    		sal= mapeoGuardar.getSalidas();
			    		preparedStatement.setDouble(7, mapeoGuardar.getSalidas());
			    	}
			    	
			    	
			    	Double finales = ini + ent - sal;
			    	
			    	preparedStatement.setDouble(8, finales);
			    	preparedStatement.setDouble(9, new Double(-1));
			    	preparedStatement.executeUpdate();
			    }
			    
			}
			else
			{
				/*
				 * No hemos encontrado datos para generar
				 */
			}
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return idReg;		
	}
	
	public ArrayList<MapeoInventarioSilicie> chequearInventarioAeat(String r_cae)
	{
		/*
		 * Este metodo se encarga de buscar las diferencias de inventario y presentar los articulos en pantalla
		 */
		Connection con = null;	
		Statement cs = null;

		boolean localizado = false;
		String rdo=null;
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = null;
		ArrayList<MapeoInventarioSilicie> vectorResultado =null;
		MapeoInventarioSilicie mapeo = null;
		MapeoInventarioSilicie mapeoAeat = null;

		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT articulo reg_art,");
			cadenaSQL.append(" cae reg_cae, ");
			cadenaSQL.append(" descripcion reg_des, ");
			cadenaSQL.append(" finales reg_fin ");
			cadenaSQL.append(" from aeat_existencias ");
			cadenaSQL.append(" where cae = '" + r_cae + "' ");
			cadenaSQL.append(" and finales != 0 ");
			cadenaSQL.append(" and concat(articulo, finales) not in (select concat(articulo, finales) from tmp_existencias) ");
			
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			vectorResultado = new ArrayList<MapeoInventarioSilicie>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoInventarioSilicie();
				mapeo.setCae(rsOpcion.getString("reg_cae"));
				mapeo.setCodigo(rsOpcion.getString("reg_art"));
				mapeo.setDescripcion(rsOpcion.getString("reg_des"));
				mapeo.setSalidas(rsOpcion.getDouble("reg_fin"));
				mapeo.setEntradas(new Double(0));
				mapeo.setFinales(mapeo.getEntradas().doubleValue()-mapeo.getSalidas().doubleValue());
				vectorResultado.add(mapeo);
			}

			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT articulo reg_art,");
			cadenaSQL.append(" cae reg_cae, ");
			cadenaSQL.append(" descripcion reg_des, ");
			cadenaSQL.append(" finales reg_fin ");
			cadenaSQL.append(" from tmp_existencias ");
			cadenaSQL.append(" where cae = '" + r_cae + "' ");
			cadenaSQL.append(" and finales != 0 ");
			cadenaSQL.append(" and concat(articulo, finales) not in (select concat(articulo, finales) from aeat_existencias) ");
			
			cs.close();
			rsOpcion.close();
			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				for (int i=0; i<vectorResultado.size();i++)
				{
					mapeoAeat = (MapeoInventarioSilicie) vectorResultado.get(i);
					
					if (mapeoAeat.getCodigo().contentEquals(rsOpcion.getString("reg_art")))
					{
						localizado = true;
						mapeoAeat.setEntradas(rsOpcion.getDouble("reg_fin"));
						mapeoAeat.setFinales(mapeoAeat.getEntradas().doubleValue()-mapeoAeat.getSalidas().doubleValue());
						vectorResultado.remove(i);
						vectorResultado.add(i, mapeoAeat);
						break;
					}
				}
				
				if (!localizado)
				{
					mapeo = new MapeoInventarioSilicie();
					mapeo.setCodigo(rsOpcion.getString("reg_art"));
					mapeo.setCae(rsOpcion.getString("reg_cae"));
					mapeo.setDescripcion(rsOpcion.getString("reg_des"));
					mapeo.setEntradas(rsOpcion.getDouble("reg_fin"));
					mapeo.setSalidas(new Double(0));
					mapeo.setFinales(mapeo.getEntradas().doubleValue()-mapeo.getSalidas().doubleValue());
					
					vectorResultado.add(mapeo);
				}
				else
				{
					localizado=false;
				}
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vectorResultado;
	}
	public String cargarInventarioAeatSilicie(String r_titular, ArrayList<MapeoInventarioSilicie> r_vector)
	{
		Connection con = null;	

		String rdo=null;
		ArrayList<MapeoInventarioSilicie> vector = null;
		PreparedStatement preparedStatement = null;
		HashMap<String, String> hashDescripciones=null;
		StringBuffer cadenaSQL = new StringBuffer();

		try
		{
			
			if (r_vector!=null && r_vector.size()>0)
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" delete from aeat_existencias  ");
				cadenaSQL.append(" where cae = '" + r_titular + "' ");
				
				
				con= this.conManager.establecerConexionInd();			
				preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    preparedStatement.executeUpdate();
			    /*
			     * LLamo al proceso de guardar la temporal con los datos del vector calculado
			     */
			    
			    for (int i=0;i<r_vector.size();i++)
			    {
			    	MapeoInventarioSilicie mapeoGuardar =(MapeoInventarioSilicie) r_vector.get(i);
			    	
			    	cadenaSQL = new StringBuffer();
			    	cadenaSQL.append(" INSERT INTO aeat_existencias ( ");
			    	cadenaSQL.append("cae, ");
			    	cadenaSQL.append("articulo, ");
			    	cadenaSQL.append("descripcion, ");
			    	cadenaSQL.append("finales ) VALUES (");
			    	cadenaSQL.append("?,?,?,?) ");
			    	
			    	preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    	
			    	preparedStatement.setString(1, r_titular);
			    	if (mapeoGuardar.getCodigo()!=null)
			    	{
			    		preparedStatement.setString(2, mapeoGuardar.getCodigo());
			    	}
			    	else
			    	{
			    		preparedStatement.setString(2, null);
			    	}
			    	if (mapeoGuardar.getDescripcion()!=null)
			    	{
			    		preparedStatement.setString(3, mapeoGuardar.getDescripcion());
			    	}
			    	else
			    	{
			    		preparedStatement.setString(3, null);
			    	}
			    	
			    	if (mapeoGuardar.getFinales()==null)
			    		preparedStatement.setDouble(4, 0);
			    	else
			    	{
			    		preparedStatement.setDouble(4, mapeoGuardar.getFinales());
			    	}
			    	preparedStatement.executeUpdate();
			    }
			    
			}
			else
			{
				/*
				 * No hemos encontrado datos para generar
				 */
			}
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public String imprimirGeneracion(String r_ejercicio,String r_mes, String r_formato, String r_titular, Integer r_id, String r_articulo, String r_nif, String r_tercero)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("silicie");
		
		switch (r_formato)
		{
			case "InventarioNC":
			{
				/*
				 * generaremos tabla temporal y recogeremos un id
				 */
				if(r_id!=null)
				{
					libImpresion.setCodigoTxt(r_id.toString());
					libImpresion.setCodigo2Txt(r_ejercicio);
					libImpresion.setCodigo3Txt(r_mes);
					libImpresion.setCodigo4Txt(r_articulo);
					
					libImpresion.setArchivoPlantilla("ListadoInventarioSilicie.jrxml");
					libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzul.jpg");
					libImpresion.setArchivoDefinitivo("/inventarioSiclicie" + RutinasFechas.horaActualSinSeparador() + ".pdf");
					
					resultadoGeneracion = libImpresion.generacionInformeTxtXML();
				}
				else
				{
					resultadoGeneracion=null;
				}
				break;
			}
			case "InventarioArticulo":
			{
				/*
				 * generaremos tabla temporal y recogeremos un id
				 */
				if(r_id!=null)
				{
					libImpresion.setCodigoTxt(r_id.toString());
					libImpresion.setCodigo2Txt(r_ejercicio);
					libImpresion.setCodigo3Txt(r_mes);
					libImpresion.setCodigo4Txt(r_articulo);
					
					libImpresion.setArchivoPlantilla("ListadoInventarioSilicie.jrxml");
					libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzul.jpg");
					libImpresion.setArchivoDefinitivo("/inventarioSiclicie" + RutinasFechas.horaActualSinSeparador() + ".pdf");
					
					resultadoGeneracion = libImpresion.generacionInformeTxtXML();
				}
				else
				{
					resultadoGeneracion=null;
				}
				break;
			}
			case "ChequeoInventario":
			{
				/*
				 * generaremos tabla temporal y recogeremos un id
				 */
				if(r_id!=null)
				{
					libImpresion.setCodigoTxt(r_id.toString());
					libImpresion.setCodigo2Txt(r_ejercicio);
					libImpresion.setCodigo3Txt(r_mes);
					libImpresion.setCodigo4Txt(r_articulo);
					
					libImpresion.setArchivoPlantilla("ListadoChequeoInventarioSilicie.jrxml");
					libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzul.jpg");
					libImpresion.setArchivoDefinitivo("/chequeoInventarioSiclicie" + RutinasFechas.horaActualSinSeparador() + ".pdf");
					
					resultadoGeneracion = libImpresion.generacionInformeTxtXML();
				}
				else
				{
					resultadoGeneracion=null;
				}
				break;
			}
			case "MovimientosNC":
			{
				/*
				 * Estableceremos SQL para generar los datos y no tener que rellenar mas tablas
				 */
				if (r_mes!=null && r_mes.toUpperCase().equals("TODOS")) r_mes = "1,2,3,4,5,6,7,8,9,10,11,12";
				
				libImpresion.setCodigoTxt(r_ejercicio);
				libImpresion.setCodigo2Txt(r_mes);
				libImpresion.setCodigo3Txt(r_titular.trim());
				if (r_articulo==null || r_articulo.length()==0) r_articulo = "%"; else r_articulo="%"+r_articulo+"%";
				if (r_nif==null || r_nif.length()==0) r_nif= "%"; else r_nif="%"+r_nif+"%";
				if (r_tercero==null || r_tercero.length()==0) r_tercero = "%"; else r_tercero="%"+r_tercero+"%";
				
				libImpresion.setCodigo4Txt(r_articulo);
				libImpresion.setCodigo5Txt(r_nif);
				libImpresion.setCodigo6Txt(r_tercero);
				
				libImpresion.setArchivoPlantilla("ListadoMovimientosCodigoNC.jrxml");
				libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
				libImpresion.setArchivoDefinitivo("/movimientosNC" + RutinasFechas.horaActualSinSeparador() + ".pdf");
				
				resultadoGeneracion = libImpresion.generacionInformeTxtXML();
				break;
			}
			case "MovimientosArticulo":
			{
				/*
				 * Estableceremos SQL para generar los datos y no tener que rellenar mas tablas
				 */
				if (r_mes!=null && r_mes.toUpperCase().equals("TODOS")) r_mes = "1,2,3,4,5,6,7,8,9,10,11,12";
				libImpresion.setCodigoTxt(r_ejercicio);
				libImpresion.setCodigo2Txt(r_mes);
				libImpresion.setCodigo3Txt(r_titular.trim());
				if (r_articulo==null || r_articulo.length()==0) r_articulo = "%"; else r_articulo="%"+r_articulo+"%";
				if (r_nif==null || r_nif.length()==0) r_nif= "%"; else r_nif="%"+r_nif+"%";
				if (r_tercero==null || r_tercero.length()==0) r_tercero = "%"; else r_tercero="%"+r_tercero+"%";
				
				libImpresion.setCodigo4Txt(r_articulo);
				libImpresion.setCodigo5Txt(r_nif);
				libImpresion.setCodigo6Txt(r_tercero);
						
				libImpresion.setArchivoPlantilla("ListadoMovimientosArticulo.jrxml");
				libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
				libImpresion.setArchivoDefinitivo("/movimientosArticulo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
				
				resultadoGeneracion = libImpresion.generacionInformeTxtXML();
				break;
			}
		}
		

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}
	
	private Integer obtenerId(String r_area, String r_valor)
	{
		Integer resultado = null;
		switch (r_area)
		{
			case "Justificante":
			{
			    consultaTipoJustificantesServer cjus = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
			    MapeoTipoJustificantes mapeo = new MapeoTipoJustificantes();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoTipoJustificantes> vector = cjus.datosTipoJustificantesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTipoJustificantes) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "Operacion":
			{
				if (r_valor!=null && r_valor.length()>0)
				{			    	

				    consultaTipoOperacionesServer cop = consultaTipoOperacionesServer.getInstance(CurrentUser.get());
				    MapeoTipoOperaciones mapeo = new MapeoTipoOperaciones();
				    mapeo.setDescripcion(r_valor);
				    ArrayList<MapeoTipoOperaciones> vector = cop.datosTipoOperacionesGlobal(mapeo);
				    
				    for (int i = 0; i<vector.size();i++)
				    {
				    	resultado = ((MapeoTipoOperaciones) vector.get(i)).getIdCodigo();
				    }
				}			    
				break;
			}
			case "DocumentoOd":
			{
				if (r_valor!=null && r_valor.length()>0)
				{			    	

				    consultaDocumentosIdentificativosServer cdoc = consultaDocumentosIdentificativosServer.getInstance(CurrentUser.get());
				    MapeoDocumentosIdentificativos mapeo = new MapeoDocumentosIdentificativos();
				    mapeo.setDescripcion(r_valor);
				    ArrayList<MapeoDocumentosIdentificativos> vector = cdoc.datosDocumentosIdentificativosGlobal(mapeo);
				    
				    for (int i = 0; i<vector.size();i++)
				    {
				    	resultado = ((MapeoDocumentosIdentificativos) vector.get(i)).getIdCodigo();
				    }
				}			    
				break;
			}

			case "Movimiento":
			{
			    consultaTiposMovimientoServer cmov = consultaTiposMovimientoServer.getInstance(CurrentUser.get());
			    MapeoTiposMovimiento mapeo = new MapeoTiposMovimiento();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoTiposMovimiento> vector = cmov.datosTiposMovimientoGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTiposMovimiento) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "DiferenciaMenos":
			{
				if (r_valor!=null && r_valor.length()>0)
				{			    	
				    consultaDiferenciasMenosServer cdif = consultaDiferenciasMenosServer.getInstance(CurrentUser.get());
				    MapeoDiferenciasMenos mapeo = new MapeoDiferenciasMenos();
				    mapeo.setDescripcion(r_valor);
			    
				    ArrayList<MapeoDiferenciasMenos> vector = cdif.datosDiferenciasMenosGlobal(mapeo);
				    
				    for (int i = 0; i<vector.size();i++)
				    {
				    	resultado = ((MapeoDiferenciasMenos) vector.get(i)).getIdCodigo();
				    }
			    }			    
				break;
			}

			case "Regimen":
			{
			    consultaRegimenesFiscalesServer cmov = consultaRegimenesFiscalesServer.getInstance(CurrentUser.get());
			    MapeoRegimenesFiscales mapeo = new MapeoRegimenesFiscales();
			    mapeo.setDescripcion(r_valor);
			    ArrayList<MapeoRegimenesFiscales> vector = cmov.datosRegimenesFiscalesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoRegimenesFiscales) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
			case "Presentador":
			{
			    consultaPresentadoresServer cmov = consultaPresentadoresServer.getInstance(CurrentUser.get());
			    MapeoPresentadores mapeo = new MapeoPresentadores();
			    mapeo.setCae(r_valor);
			    ArrayList<MapeoPresentadores> vector = cmov.datosPresentadoresGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoPresentadores) vector.get(i)).getIdCodigo();
			    }
			    
				break;
			}
		}
		return resultado;
	}

	public String obtenerDesc(String r_area, Integer r_valor)
	{
		String resultado = null;
		switch (r_area)
		{
			case "Justificante":
			{
			    consultaTipoJustificantesServer cjus = consultaTipoJustificantesServer.getInstance(CurrentUser.get());
			    MapeoTipoJustificantes mapeo = new MapeoTipoJustificantes();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoTipoJustificantes> vector = cjus.datosTipoJustificantesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTipoJustificantes) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "DocumentoOd":
			{
			    consultaDocumentosIdentificativosServer cdoc = consultaDocumentosIdentificativosServer.getInstance(CurrentUser.get());
			    MapeoDocumentosIdentificativos mapeo = new MapeoDocumentosIdentificativos();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoDocumentosIdentificativos> vector = cdoc.datosDocumentosIdentificativosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDocumentosIdentificativos) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}

			case "Movimiento":
			{
			    consultaTiposMovimientoServer cmov = consultaTiposMovimientoServer.getInstance(CurrentUser.get());
			    MapeoTiposMovimiento mapeo = new MapeoTiposMovimiento();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoTiposMovimiento> vector = cmov.datosTiposMovimientoGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoTiposMovimiento) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "DiferenciaMenos":
			{
			    consultaDiferenciasMenosServer cdif = consultaDiferenciasMenosServer.getInstance(CurrentUser.get());
			    MapeoDiferenciasMenos mapeo = new MapeoDiferenciasMenos();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoDiferenciasMenos> vector = cdif.datosDiferenciasMenosGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoDiferenciasMenos) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			
			case "Regimen":
			{
			    consultaRegimenesFiscalesServer cmov = consultaRegimenesFiscalesServer.getInstance(CurrentUser.get());
			    MapeoRegimenesFiscales mapeo = new MapeoRegimenesFiscales();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoRegimenesFiscales> vector = cmov.datosRegimenesFiscalesGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoRegimenesFiscales) vector.get(i)).getDescripcion();
			    }
			    
				break;
			}
			case "Presentador":
			{
			    consultaPresentadoresServer cmov = consultaPresentadoresServer.getInstance(CurrentUser.get());
			    MapeoPresentadores mapeo = new MapeoPresentadores();
			    mapeo.setIdCodigo(r_valor);
			    ArrayList<MapeoPresentadores> vector = cmov.datosPresentadoresGlobal(mapeo);
			    
			    for (int i = 0; i<vector.size();i++)
			    {
			    	resultado = ((MapeoPresentadores) vector.get(i)).getCae();
			    }
			    
				break;
			}
		}
		return resultado.trim();
	}

	@Override
	public String semaforos() 
	{
		String semaforo= "0";
		
		consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		semaforo = cats.semaforos();
		
		if (semaforo.equals("0"))
		{
			consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
			semaforo = cacs.semaforos();
		}
		return semaforo;
	}

//	private boolean silicieActivo()
//	{
//		ResultSet rsOpcion = null;
//		StringBuffer cadenaSQL = new StringBuffer();  
//		
//		try
//		{
//	        
//			cadenaSQL.append(" select valor_actual from e_l_parm where grupo='BORSAO' and codigo='SILICIE' ");
//		    
//			con= this.conManager.establecerConexionGestion();			
//			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
//			rsOpcion= cs.executeQuery(cadenaSQL.toString());
//			
//			while(rsOpcion.next())
//			{
//				if (new Integer(rsOpcion.getString("valor_actual").trim())>0) return true; else return false;
//			}
//		}
//	    catch (Exception ex)
//	    {
//	    	serNotif.mensajeError(ex.getMessage());
//	    	return false;
//	    }
//		return false;
//	}
}