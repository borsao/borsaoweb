package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoMovimientosSilicie mapeo = null;
	 
	 public MapeoMovimientosSilicie convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoMovimientosSilicie();
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("mes")!=null && r_hash.get("mes").length()>0 && r_hash.get("mes")!="Todos") this.mapeo.setMes(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("mes"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("litros"))));
		 if (r_hash.get("grado")!=null && r_hash.get("grado").length()>0) this.mapeo.setGrado(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("grado"))));
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
		 if (r_hash.get("albaran")!=null && r_hash.get("albaran").length()>0) this.mapeo.setAlbaran(new Integer(r_hash.get("albaran")));
		 if (r_hash.get("posicion")!=null && r_hash.get("posicion").length()>0) this.mapeo.setPosicion(new Integer(r_hash.get("posicion")));
		 

		 this.mapeo.setCaeTitular(r_hash.get("caeTitular"));
		 this.mapeo.setCaeOD(r_hash.get("caeOD"));
		 this.mapeo.setNumeroDocumentoIdOD(r_hash.get("numeroDocumentoIdOD"));
		 this.mapeo.setRazonSocialOD(r_hash.get("razonSocialOD"));
		 this.mapeo.setNumeroJustificante(r_hash.get("numeroJustificante"));
		 
		 this.mapeo.setCodigoNC(r_hash.get("codigoNC"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setCodigoArticulo(r_hash.get("codigoArticulo"));
		 this.mapeo.setDescripcionArticulo(r_hash.get("descripcionArticulo"));
		 
		 this.mapeo.setDocumento(r_hash.get("documento"));		 
		 this.mapeo.setClaveTabla(r_hash.get("claveTabla"));
		 this.mapeo.setSerie(r_hash.get("serie"));
		 
		 this.mapeo.setEstado(r_hash.get("estado"));

		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFechaMovimiento(RutinasFechas.convertirADate(r_hash.get("fecha")));
			 if (r_hash.get("fechaRegistro")!=null) this.mapeo.setFechaRegistro(RutinasFechas.convertirADate(r_hash.get("fechaRegistro")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError(ex.getMessage());
		 }
		 
		 
		 this.mapeo.setDescripcionDocumentoRep(r_hash.get("descripcionDocumentoRep"));
		 this.mapeo.setDescripcionDocumentoOd(r_hash.get("descripcionDocumentoOd"));
		 this.mapeo.setDescripcionJustificante(r_hash.get("descripcionJustificante"));
		 this.mapeo.setDescripcionRegimenFiscal(r_hash.get("descripcionRegimenFiscal"));
		 this.mapeo.setDescripcionOperacion(r_hash.get("descripcionOperacion"));
		 this.mapeo.setDescripcionMovimiento(r_hash.get("descripcionMovimiento"));
		 this.mapeo.setDescripcionDiferenciaMenos(r_hash.get("descripcionDiferenciaMenos"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoMovimientosSilicie convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoMovimientosSilicie();
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("mes")!=null && r_hash.get("mes").length()>0 && r_hash.get("mes")!="Todos") this.mapeo.setMes(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("mes"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("litros"))));
		 if (r_hash.get("grado")!=null && r_hash.get("grado").length()>0) this.mapeo.setGrado(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("grado"))));
		 if (r_hash.get("albaran")!=null && r_hash.get("albaran").length()>0) this.mapeo.setAlbaran(new Integer(r_hash.get("albaran")));
		 if (r_hash.get("posicion")!=null && r_hash.get("posicion").length()>0) this.mapeo.setPosicion(new Integer(r_hash.get("posicion")));
		 
		 this.mapeo.setCaeTitular(r_hash.get("caeTitular"));
		 this.mapeo.setCaeOD(r_hash.get("caeOD"));
		 this.mapeo.setNumeroDocumentoIdOD(r_hash.get("numeroDocumentoIdOD"));
		 this.mapeo.setRazonSocialOD(r_hash.get("razonSocialOD"));
		 this.mapeo.setNumeroJustificante(r_hash.get("numeroJustificante"));
		 
		 this.mapeo.setCodigoNC(r_hash.get("codigoNC"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setCodigoArticulo(r_hash.get("codigoArticulo"));
		 this.mapeo.setDescripcionArticulo(r_hash.get("descripcionArticulo"));
		 
		 this.mapeo.setDocumento(r_hash.get("documento"));		 
		 this.mapeo.setClaveTabla(r_hash.get("claveTabla"));
		 this.mapeo.setSerie(r_hash.get("serie"));
		 
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFechaMovimiento(RutinasFechas.convertirADate(r_hash.get("fecha")));
			 if (r_hash.get("fechaRegistro")!=null) this.mapeo.setFechaRegistro(RutinasFechas.convertirADate(r_hash.get("fechaRegistro")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError(ex.getMessage());
		 }

		 this.mapeo.setDescripcionDocumentoRep(r_hash.get("descripcionDocumentoRep"));
		 this.mapeo.setDescripcionDocumentoOd(r_hash.get("descripcionDocumentoOd"));
		 this.mapeo.setDescripcionJustificante(r_hash.get("descripcionJustificante"));
		 this.mapeo.setDescripcionRegimenFiscal(r_hash.get("descripcionRegimenFiscal"));
		 this.mapeo.setDescripcionOperacion(r_hash.get("descripcionOperacion"));
		 this.mapeo.setDescripcionMovimiento(r_hash.get("descripcionMovimiento"));
		 this.mapeo.setDescripcionDiferenciaMenos(r_hash.get("descripcionDiferenciaMenos"));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoMovimientosSilicie r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("id", String.valueOf(this.mapeo.getIdCodigo()));
		 if (this.mapeo.getEjercicio()!=null) this.hash.put("ejercicio", String.valueOf(this.mapeo.getEjercicio()));
		 if (this.mapeo.getMes()!=null) this.hash.put("mes", String.valueOf(this.mapeo.getMes()));
		 
		 if (this.mapeo.getLitros()!=null) this.hash.put("litros", String.valueOf(this.mapeo.getLitros()));
		 if (this.mapeo.getGrado()!=null) this.hash.put("grado", String.valueOf(this.mapeo.getGrado()));
		 if (this.mapeo.getAlbaran()!=null)
		 {
			 this.hash.put("albaran", String.valueOf(this.mapeo.getAlbaran()));
			 this.hash.put("posicion", String.valueOf(this.mapeo.getPosicion()));
			 this.hash.put("documento", this.mapeo.getDocumento());
			 this.hash.put("claveTabla", this.mapeo.getClaveTabla());
			 this.hash.put("serie", this.mapeo.getSerie());
		 }
		 

		 this.hash.put("caeTitular", this.mapeo.getCaeTitular());
		 this.hash.put("caeOD", this.mapeo.getCaeOD());
		 this.hash.put("numeroDocumentoOD", this.mapeo.getNumeroDocumentoIdOD());
		 this.hash.put("razonSocialOD", this.mapeo.getRazonSocialOD());
		 this.hash.put("numeroJustificante", this.mapeo.getNumeroJustificante());
		 this.hash.put("codigoNC", this.mapeo.getCodigoNC());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("codigoArticulo", this.mapeo.getCodigoArticulo());
		 this.hash.put("descripcionArticulo", this.mapeo.getDescripcionArticulo());
		 
		 this.hash.put("estado", this.mapeo.getEstado());
		 
		 if (r_mapeo.getFechaMovimiento()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFechaMovimiento().toString());
			 
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }
		 if (r_mapeo.getFechaRegistro()!=null)
		 {
			 this.hash.put("fechaRegistro", r_mapeo.getFechaRegistro().toString());
			 
		 }
		 else
		 {
			 this.hash.put("fechaRegistro", null);
		 }

		 this.hash.put("descripcionDocumentoRep",this.mapeo.getDescripcionDocumentoRep());
		 this.hash.put("descripcionDocumentoOd",this.mapeo.getDescripcionDocumentoOd());
		 this.hash.put("descripcionJustificante", this.mapeo.getDescripcionJustificante());
		 this.hash.put("descripcionRegimenFiscal", this.mapeo.getDescripcionRegimenFiscal());
		 this.hash.put("descripcionOperacion", this.mapeo.getDescripcionOperacion());
		 this.hash.put("descripcionMovimiento", this.mapeo.getDescripcionMovimiento());
		 this.hash.put("descripcionDiferenciaMenos", this.mapeo.getDescripcionDiferenciaMenos());
		 
		 return hash;		 
	 }
}