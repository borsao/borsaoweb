package borsao.e_borsao.Modulos.FINANCIERO.silicie.generacionMovimientos.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioSilicie extends MapeoGlobal
{
	
	private Double iniciales;
	private Double entradas;
	private Double salidas;
	private Double finales;
	
	private String codigo;
	private String descripcion;
	private String cae;
	
	public MapeoInventarioSilicie()
	{
	}

	public Double getIniciales() {
		return iniciales;
	}

	public void setIniciales(Double iniciales) {
		this.iniciales = iniciales;
	}

	public Double getEntradas() {
		return entradas;
	}

	public void setEntradas(Double entradas) {
		this.entradas = entradas;
	}

	public Double getSalidas() {
		return salidas;
	}

	public void setSalidas(Double salidas) {
		this.salidas = salidas;
	}

	public Double getFinales() {
		return finales;
	}

	public void setFinales(Double finales) {
		this.finales = finales;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCae() {
		return cae;
	}

	public void setCae(String cae) {
		this.cae = cae;
	}
}


