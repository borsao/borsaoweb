package borsao.e_borsao.Modulos.FINANCIERO.silicie.documentosIdentificativos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDocumentosIdentificativos extends MapeoGlobal
{
	private String codigo;
	private String descripcion;
	private String verifica;

	public MapeoDocumentosIdentificativos()
	{
		this.setCodigo("");
		this.setDescripcion("");
		this.setVerifica("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getVerifica() {
		return verifica;
	}

	public void setVerifica(String verifica) {
		this.verifica = verifica;
	}


}