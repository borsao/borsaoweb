package borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.silicie.regimenesFiscales.modelo.MapeoRegimenesFiscales;

public class consultaRegimenesFiscalesServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaRegimenesFiscalesServer instance;
	
	public consultaRegimenesFiscalesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRegimenesFiscalesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRegimenesFiscalesServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoRegimenesFiscales> datosRegimenesFiscalesGlobal(MapeoRegimenesFiscales r_mapeo)
	{
		ResultSet rsRegimenesFiscales = null;		
		ArrayList<MapeoRegimenesFiscales> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_silicie_rf.id rf_id, ");
		cadenaSQL.append(" fin_silicie_rf.codigo rf_cod, ");
		cadenaSQL.append(" fin_silicie_rf.descripcion rf_des ");
     	cadenaSQL.append(" FROM fin_silicie_rf ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsRegimenesFiscales= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoRegimenesFiscales>();
			
			while(rsRegimenesFiscales.next())
			{
				MapeoRegimenesFiscales mapeoRegimenesFiscales = new MapeoRegimenesFiscales();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRegimenesFiscales.setIdCodigo(rsRegimenesFiscales.getInt("rf_id"));
				mapeoRegimenesFiscales.setCodigo(rsRegimenesFiscales.getString("rf_cod"));
				mapeoRegimenesFiscales.setDescripcion(rsRegimenesFiscales.getString("rf_des"));
				vector.add(mapeoRegimenesFiscales);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsRegimenesFiscales = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_silicie_rf.id) rf_sig ");
     	cadenaSQL.append(" FROM fin_silicie_rf ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsRegimenesFiscales= cs.executeQuery(cadenaSQL.toString());
			
			while(rsRegimenesFiscales.next())
			{
				return rsRegimenesFiscales.getInt("rf_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoRegimenesFiscales r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_silicie_rf ( ");
    		cadenaSQL.append(" fin_silicie_rf.id, ");
    		cadenaSQL.append(" fin_silicie_rf.codigo, ");
    		cadenaSQL.append(" fin_silicie_rf.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoRegimenesFiscales r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_silicie_rf set ");
			cadenaSQL.append(" fin_silicie_rf.codigo=?, ");
		    cadenaSQL.append(" fin_silicie_rf.descripcion=? ");
		    cadenaSQL.append(" WHERE fin_silicie_rf.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoRegimenesFiscales r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_silicie_rf ");            
			cadenaSQL.append(" WHERE fin_silicie_rf.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}