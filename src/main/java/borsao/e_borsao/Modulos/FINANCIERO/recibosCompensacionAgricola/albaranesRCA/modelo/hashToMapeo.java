package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoAlbaranesRCA mapeo = null;
	 
	 public MapeoAlbaranesRCA convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoAlbaranesRCA();
		 
		 this.mapeo.setPrecio(r_hash.get("precio"));
		 this.mapeo.setAlbaran(r_hash.get("albaran"));
		 this.mapeo.setSocio(r_hash.get("socio"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setEjercicio(r_hash.get("ejercicio"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoAlbaranesRCA convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoAlbaranesRCA();
		 
		 this.mapeo.setPrecio(r_hash.get("precio"));
		 this.mapeo.setAlbaran(r_hash.get("albaran"));
		 this.mapeo.setSocio(r_hash.get("socio"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setEjercicio(r_hash.get("ejercicio"));

		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoAlbaranesRCA r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("precio", r_mapeo.getPrecio());
		 this.hash.put("albaran", r_mapeo.getAlbaran());
		 this.hash.put("socio", r_mapeo.getSocio());
		 this.hash.put("bodega", r_mapeo.getBodega());
		 this.hash.put("ejercicio", r_mapeo.getEjercicio());
		 return hash;		 
	 }
}