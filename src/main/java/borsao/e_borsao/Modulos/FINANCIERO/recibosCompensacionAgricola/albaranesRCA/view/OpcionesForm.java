package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo.AlbaranesRCAGrid;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo.MapeoAlbaranesRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.server.consultaAlbaranesRCAServer;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.MapeoParametrosRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.server.consultaParametrosRCAServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoAlbaranesRCA mapeoAlbaranesRCA = null;
	 
	 private consultaAlbaranesRCAServer cus = null;	 
	 private AlbaranesRCAView uw = null;
	 private BeanFieldGroup<MapeoAlbaranesRCA> fieldGroup;
	 
	 public OpcionesForm(AlbaranesRCAView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        this.cargarCombo();
        cus=consultaAlbaranesRCAServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoAlbaranesRCA>(MapeoAlbaranesRCA.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    
		ValueChangeListener val = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (isCreacion() && bodega.getValue()!=null && ejercicio.getValue()!=null && albaran.getValue()!=null)
				{
					cargarDatos();
				}
			}
		};
		this.albaran.addValueChangeListener(val);
		
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoAlbaranesRCA = (MapeoAlbaranesRCA) uw.grid.getSelectedRow();
                eliminarAlbaranesRCA(mapeoAlbaranesRCA);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoAlbaranesRCA = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearAlbaranesRCA(mapeoAlbaranesRCA);
	 			}
	 			else
	 			{
	 				MapeoAlbaranesRCA mapeoAlbaranesRCA_orig = (MapeoAlbaranesRCA) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoAlbaranesRCA= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarAlbaranesRCA(mapeoAlbaranesRCA,mapeoAlbaranesRCA_orig);
	 				hm=null;
	 			}
	 			if (((AlbaranesRCAGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarAlbaranesRCA(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.bodega.getValue()!=null && this.bodega.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("bodega", this.bodega.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bodega", "");
		}
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.albaran.getValue()!=null && this.albaran.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("albaran", this.albaran.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("albaran", "");
		}
		if (this.socio.getValue()!=null && this.socio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("socio", this.socio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("socio", "");
		}
		if (this.precio.getValue()!=null && this.precio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("precio", this.precio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("precio", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarAlbaranesRCA(null);
	}
	
	public void editarAlbaranesRCA(MapeoAlbaranesRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoAlbaranesRCA();		
		fieldGroup.setItemDataSource(new BeanItem<MapeoAlbaranesRCA>(r_mapeo));
		bodega.focus();
	}
	
	public void eliminarAlbaranesRCA(MapeoAlbaranesRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((AlbaranesRCAGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoAlbaranesRCA>) ((AlbaranesRCAGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarAlbaranesRCA(MapeoAlbaranesRCA r_mapeo, MapeoAlbaranesRCA r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaAlbaranesRCAServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((AlbaranesRCAGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearAlbaranesRCA(MapeoAlbaranesRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaAlbaranesRCAServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoAlbaranesRCA>(r_mapeo));
				if (((AlbaranesRCAGrid) uw.grid)!=null)
				{
					((AlbaranesRCAGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoAlbaranesRCA> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoAlbaranesRCA= item.getBean();
            if (this.mapeoAlbaranesRCA.getAlbaran()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.bodega.getValue()==null || this.bodega.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Bodega");
    		return false;
    	}
    	if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el ejercicio");
    		return false;
    	}
    	if (this.albaran.getValue()==null || this.albaran.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el albaran");
    		return false;
    	}
    	if (this.socio.getValue()==null || this.socio.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Socio");
    		return false;
    	}
    	if (this.precio.getValue()==null || this.precio.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el precio");
    		return false;
    	}
    	return true;
    }

    private void cargarCombo()
    {
    	this.bodega.addItem("Borja");
    	this.bodega.addItem("Pozuelo");
    	this.bodega.addItem("Tabuenca");
    	
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.ejercicio.addItem((String) vector.get(i));
		}
		
		this.ejercicio.setValue(vector.get(0).toString());
    }

    private void cargarDatos()
    {
		ArrayList<MapeoAlbaranesRCA> vector = null;
		
		consultaAlbaranesRCAServer carca = consultaAlbaranesRCAServer.getInstance(CurrentUser.get());
		
		MapeoAlbaranesRCA mapDefecto = new MapeoAlbaranesRCA();
		MapeoAlbaranesRCA mapObtenido = new MapeoAlbaranesRCA();
		MapeoAlbaranesRCA map = new MapeoAlbaranesRCA();
		map.setBodega(this.bodega.getValue().toString());
		map.setEjercicio(this.ejercicio.getValue().toString());
		map.setAlbaran(this.albaran.getValue().toString());
		
		mapObtenido = carca.datosAlbaranesImportados(map);
		
		this.socio.setValue(mapObtenido.getSocio());
		this.precio.setValue(mapObtenido.getPrecio());
		
		if ((new Double(mapObtenido.getPrecio())).compareTo(new Double(0))==0)
		{
			consultaParametrosRCAServer cprca = consultaParametrosRCAServer.getInstance(CurrentUser.get());
			MapeoParametrosRCA mapeoPar = new MapeoParametrosRCA();
			mapeoPar.setBodega(map.getBodega());
			
			this.precio.setValue(cprca.datosParametrosRCAPrecio(mapeoPar));
		}
	}
}
