package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo.MapeoAlbaranesRCA;

public class consultaAlbaranesRCAServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaAlbaranesRCAServer instance;
	
	public consultaAlbaranesRCAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlbaranesRCAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlbaranesRCAServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoAlbaranesRCA> datosAlbaranesRCAGlobal(MapeoAlbaranesRCA r_mapeo)
	{
		ResultSet rsAlbaranesRCA = null;		
		ArrayList<MapeoAlbaranesRCA> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_ccs.coop rca_bod, ");
		cadenaSQL.append(" rca_ccs.ejercicio rca_eje, ");
		cadenaSQL.append(" rca_ccs.codigo rca_alb, ");
		cadenaSQL.append(" rca_ccs.socio rca_soc, ");
		cadenaSQL.append(" rca_ccs.precio rca_pre ");
     	cadenaSQL.append(" FROM rca_ccs ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getPrecio()!=null && r_mapeo.getPrecio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" precio = '" + r_mapeo.getPrecio() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getAlbaran() + "' ");
				}
				if (r_mapeo.getSocio()!=null && r_mapeo.getSocio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" socio = '" + r_mapeo.getSocio() + "' ");
				}
				if (r_mapeo.getBodega()!=null && r_mapeo.getBodega().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" coop = '" + r_mapeo.getBodega() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsAlbaranesRCA.TYPE_SCROLL_SENSITIVE,rsAlbaranesRCA.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsAlbaranesRCA= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoAlbaranesRCA>();
			
			while(rsAlbaranesRCA.next())
			{
				MapeoAlbaranesRCA mapeoAlbaranesRCA = new MapeoAlbaranesRCA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoAlbaranesRCA.setBodega(rsAlbaranesRCA.getString("rca_bod"));
				mapeoAlbaranesRCA.setEjercicio(rsAlbaranesRCA.getString("rca_eje"));
				mapeoAlbaranesRCA.setSocio(rsAlbaranesRCA.getString("rca_soc"));
				mapeoAlbaranesRCA.setAlbaran(rsAlbaranesRCA.getString("rca_alb"));
				mapeoAlbaranesRCA.setPrecio(rsAlbaranesRCA.getString("rca_pre"));
				vector.add(mapeoAlbaranesRCA);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoAlbaranesRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO rca_ccs ( ");
    		cadenaSQL.append(" rca_ccs.coop, ");
	        cadenaSQL.append(" rca_ccs.ejercicio, ");
	        cadenaSQL.append(" rca_ccs.codigo, ");
	        cadenaSQL.append(" rca_ccs.socio, ");
		    cadenaSQL.append(" rca_ccs.precio ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getBodega()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getBodega());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getAlbaran()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getAlbaran());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getSocio()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getSocio());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getPrecio()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getPrecio());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoAlbaranesRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE rca_ccs set ");
			cadenaSQL.append(" rca_ccs.socio=?, ");
		    cadenaSQL.append(" rca_ccs.precio=? ");
		    cadenaSQL.append(" WHERE rca_ccs.coop=? ");
		    cadenaSQL.append(" and rca_ccs.ejercicio=? ");
		    cadenaSQL.append(" and rca_ccs.codigo=? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getSocio());
		    preparedStatement.setString(2, r_mapeo.getPrecio());
		    preparedStatement.setString(3, r_mapeo.getBodega());
		    preparedStatement.setString(4, r_mapeo.getEjercicio());
		    preparedStatement.setString(5, r_mapeo.getAlbaran());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoAlbaranesRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM rca_ccs ");            
		    cadenaSQL.append(" WHERE rca_ccs.coop=? ");
		    cadenaSQL.append(" and rca_ccs.ejercicio=? ");
		    cadenaSQL.append(" and rca_ccs.codigo=? ");
 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getBodega());
		    preparedStatement.setString(2, r_mapeo.getEjercicio());
		    preparedStatement.setString(3, r_mapeo.getAlbaran());

			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public MapeoAlbaranesRCA datosAlbaranesImportados(MapeoAlbaranesRCA r_mapeo)
	{
		ResultSet rsAlbaranesRCA = null;		
		String tablaOrigen=null;
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaWhere =null;
		
		if (r_mapeo.getBodega().toString().trim().contentEquals("Borja")) tablaOrigen = "_bor";
		if (r_mapeo.getBodega().toString().trim().contentEquals("Pozuelo")) tablaOrigen = "_poz";
		if (r_mapeo.getBodega().toString().trim().contentEquals("Tabuenca")) tablaOrigen = "_tab";

		cadenaSQL.append(" SELECT e__ccs" + tablaOrigen + ".ejercicio rca_eje, ");
		cadenaSQL.append(" e__ccs" + tablaOrigen + ".codigo rca_alb, ");
		cadenaSQL.append(" e__ccs" + tablaOrigen + ".socio rca_soc, ");
		cadenaSQL.append(" e__ccs" + tablaOrigen + ".valor_cosecha / e__ccs" + tablaOrigen + ".kgs_neto_liq rca_pre ");
     	cadenaSQL.append(" FROM e__ccs" + tablaOrigen);

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = '" + r_mapeo.getEjercicio() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getAlbaran() + "' ");
				}
				if (r_mapeo.getSocio()!=null && r_mapeo.getSocio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" socio = '" + r_mapeo.getSocio() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsAlbaranesRCA.TYPE_SCROLL_SENSITIVE,rsAlbaranesRCA.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsAlbaranesRCA= cs.executeQuery(cadenaSQL.toString());
			
			while(rsAlbaranesRCA.next())
			{
				MapeoAlbaranesRCA mapeoAlbaranesRCA = new MapeoAlbaranesRCA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoAlbaranesRCA.setBodega(r_mapeo.getBodega());
				mapeoAlbaranesRCA.setEjercicio(rsAlbaranesRCA.getString("rca_eje"));
				mapeoAlbaranesRCA.setSocio(rsAlbaranesRCA.getString("rca_soc"));
				mapeoAlbaranesRCA.setAlbaran(rsAlbaranesRCA.getString("rca_alb"));
				mapeoAlbaranesRCA.setPrecio(rsAlbaranesRCA.getString("rca_pre"));
				return mapeoAlbaranesRCA;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
}