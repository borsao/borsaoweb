package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAlbaranesRCA extends MapeoGlobal
{
	private String bodega;
	private String ejercicio;
	private String socio;
	private String albaran;
	private String precio;

	public MapeoAlbaranesRCA()
	{
		this.setBodega("");
		this.setPrecio("");
		this.setAlbaran("");
	}

	public String getBodega() {
		return bodega;
	}

	public String getEjercicio() {
		return ejercicio;
	}

	public String getAlbaran() {
		return albaran;
	}

	public String getPrecio() {
		return precio;
	}

	public void setBodega(String bodega) {
		this.bodega = bodega;
	}

	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}

	public void setAlbaran(String albaran) {
		this.albaran = albaran;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getSocio() {
		return socio;
	}

	public void setSocio(String socio) {
		this.socio = socio;
	}

}