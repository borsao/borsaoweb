package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.sincronizadorRCA.server;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo.MapeoAlbaranesRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo.MapeoEquivalenciasRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.server.consultaEquivalenciasRCAServer;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.MapeoParametrosRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.server.consultaParametrosRCAServer;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.LABORATORIO.Cosecha.modelo.MapeoEntradasCosecha;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class servidorSincronizadorRCA extends serverBasico
{
	public static Double poolsize = new Double(25000);
	private String orig = null;
	private String dest = null;

	private String ejercicio= null;
	private String bodega= null;
	private Integer valorDiferencial = 0;  //valido para sumar al socio, al proveedor y al codigo de albaran
	private Double precio = 0.0; // precio_compra1 para el articulo
	private String cuentaContable= null;  //valido para el socio y para el proveedor
	private String formaPago= null;  //valido para el proveedor
	private String cuentaContableBaseImponible= null;  //valido para el articulo

	private Double kilos = new Double(0);

	public servidorSincronizadorRCA(String r_bodega)
	{
		this.bodega=r_bodega;
		this.obtenerValoresBodega();		
	}

	public servidorSincronizadorRCA(String r_bodega, ArrayList<MapeoSincronizarTablas> r_sincroRecibo, String r_ejercicio)
	{
		
		Notificaciones.getInstance().mensajeSeguimiento("Inicio sincro" + new java.util.Date());
		this.bodega=r_bodega;
		this.ejercicio = r_ejercicio;
		this.obtenerValoresBodega();
		this.sincronizar(r_sincroRecibo);
		
		Notificaciones.getInstance().mensajeSeguimiento("Fin sincro" + new java.util.Date());
	}

	private void sincronizar (ArrayList<MapeoSincronizarTablas> sincroRecibo)
	{
		Connection conOrigen = null;
		Connection conDestino = null;
		PreparedStatement stDestino = null;
		PreparedStatement stOrigen = null;
		ResultSet rsOrigen = null;		
		ResultSet rsDestino	= null;		
		ResultSetMetaData rsMetaDataOrigen = null;
		StringBuffer sql_origen = null;
		StringBuffer sql_destino = null;		
		Iterator<MapeoSincronizarTablas> sincronizaciones = null;
		Boolean destinoCorrecto;
		Boolean conErrores;
		Double contador = null;
		
		/*
		 * recorremos el array y vamos extrayendo los hashmaps
		 */
		contador=new Double(0);
		conErrores=false;
		destinoCorrecto=true;
		sincronizaciones = sincroRecibo.iterator();
		while (sincronizaciones.hasNext())
		{
			MapeoSincronizarTablas datoSincro = (MapeoSincronizarTablas) sincronizaciones.next();
			try
			{
				 /*
				  * Establecemos la conexion con el origen
				  */
				
				conOrigen=this.establcerConexion(datoSincro.getBbddOrigen().toString().trim());
				orig = datoSincro.getBbddOrigen();
				if (conOrigen!=null)
				{
					sql_origen = new StringBuffer();
					sql_origen.append("select * from " + datoSincro.getTablaOrigen().trim());
					
					if (datoSincro.getSql()!=null && datoSincro.getSql().length()>0) sql_origen.append(" " + datoSincro.getSql().trim());
					
					stOrigen = conOrigen.prepareStatement(sql_origen.toString());
					rsOrigen = stOrigen.executeQuery();
					rsMetaDataOrigen = rsOrigen.getMetaData();
					
					/*
					 * Comprobamos la tabla destino
					 */
					destinoCorrecto = this.comprobarDestino(datoSincro.getBbddDestino().trim(), datoSincro.getTablaDestino().trim());
					if (!destinoCorrecto)
					{	
//						this.crearTablaDestino(rsMetaDataOrigen, datoSincro.getBbddDestino().toString(), datoSincro.getTablaDestino());
					}
					/*
					 * Al ser copia total borro primero los datos de la tabla de destino
					 */
					conDestino=this.establcerConexion(datoSincro.getBbddDestino());
					dest = datoSincro.getBbddDestino();
					sql_destino = new StringBuffer();
					
					if (datoSincro.getTablaDestino().contentEquals("e_provee"))
					{
						sql_destino.append("delete from " + datoSincro.getTablaDestino() + " where apartado = '" + this.bodega + "' ");
					}
					else if (datoSincro.getTablaDestino().contentEquals("e__socio"))
					{
						sql_destino.append("delete from " + datoSincro.getTablaDestino() + " where cta_proveedor = '" + this.cuentaContable + "' ");
					}
					else if (datoSincro.getTablaDestino().contentEquals("e__ccs"))
					{
						sql_destino.append("delete from " + datoSincro.getTablaDestino() + " where ejercicio = '" + this.ejercicio + "' and socio in (select proveedor from e_provee where apartado = '"+ this.bodega + "') ");
					}

					if (sql_destino.length()>0)
					{
						stDestino = conDestino.prepareStatement(sql_destino.toString());
						stDestino.executeUpdate();
					}
					
					/*
					 * Me recoge del origen los datos sean totales o incrementales
					 */
					stOrigen = conOrigen.prepareStatement(sql_origen.toString());
					rsOrigen = stOrigen.executeQuery();
					rsMetaDataOrigen = rsOrigen.getMetaData();
					/*
					 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
					 * por otro los registros obtenidos
					 * 
					 * con todo esto montamos el SQL del insert en destino
					 */
					//	rsOrigen.last();
					conErrores=false;
//					conDestino=this.establcerConexion(datoSincro.getBbddDestino());
					
					sql_destino = new StringBuffer();
//					sql_destino.append("insert into " + datoSincro.getTablaDestino() + "(");
//					
//					// bucle para obtener los campos										
//					for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
//					{	
//						sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
//					}
//					sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
//					sql_destino.append(") values (");
//					
//					Boolean primero = true;
//					Boolean hayDatos = false;
					while(rsOrigen.next())
					{
//						hayDatos=true;
//						if (contador.doubleValue()>= this.poolsize || sql_destino.toString().length()>300000)
//						{
////							conDestino=this.establcerConexion(datoSincro.getBbddDestino());
//							sql_destino = new StringBuffer();
						
						
						/*
						 * Si hayDatos = false, directamente insertamos los datos en la tabla de destino
						 * En cambio si es = True solo tenemos que meter los datos que falten
						 */
						
						
						sql_destino = new StringBuffer();
						sql_destino.append("insert into " + datoSincro.getTablaDestino() + "(");
							
							// bucle para obtener los campos										
							for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
							{	
								sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
							}
							sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
							
							if ((this.bodega.contentEquals("Tabuenca")||this.bodega.contentEquals("Pozuelo")) && datoSincro.getTablaDestino().contentEquals("e__ccs"))
							{
								sql_destino.append(",cajas_llenas,cajas_vacias,cajas_peso,cajas_finca,cajas_pendiente) values (");
							}
							else
							sql_destino.append(") values (");
							
//							primero = true;
//							contador=new Double(0);
							
//						}
//						if (!primero) 
//						{
//							sql_destino.append(",(");
//						}
//						else
//						{
//							primero=false;
//						}
						
//						contador++;
						//bucle para obtener los datos
						
						for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
						{
							Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , j+1);
							if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
							{
								rdo="";
								conErrores=false;
							}						
							sql_destino.append(rdo + ",");
						}
						//Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
						Object rdo=this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
						if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
						{
							rdo="";
						}
						if ((this.bodega.contentEquals("Tabuenca")||this.bodega.contentEquals("Pozuelo")) && datoSincro.getTablaDestino().contentEquals("e__ccs"))
						{
							sql_destino.append(rdo + ",0.0,0.0,0.0,0,'');");
						}
						else
						sql_destino.append(rdo + ");");
						
//						if (contador.doubleValue()>= this.poolsize || sql_destino.toString().length()>300000)
//						{
//							stDestino = conDestino.prepareStatement(sql_destino.toString());
//							stDestino.executeUpdate();
//						}

						if (!conErrores )
						{
							stDestino = conDestino.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
						}
					}				
				}
				else
				{
					Notificaciones.getInstance().mensajeSeguimiento("Sincronizador: " + datoSincro.getBbddOrigen().toString().trim() + " No pudo establecerse la conexion");
				}
			}
			catch (Exception sqlEx)
			{  
				sqlEx.printStackTrace();
				Notificaciones.getInstance().mensajeError("Sincronizador - Tabla Error : " + datoSincro.getTablaDestino());
			}
		}
		/*
		 * - Preparamos el SQL de actualizacion
		 * insert into e_provee(proveedor,nombre_comercial,nombre_fiscal,direccion,poblacion,apartado,cod_postal,pais,provincia,cif,telefono1,telefono2,telefono3,fax,gremio,cod_pago,modalidad_iva,tipo_impuesto,num_cuenta,banco,agencia,cuenta_corriente,bloq_facturar,bloq_pedir,fecha_alta,moneda,porcentaje1,porcentaje2,porcentaje3,porcentaje4,porcentaje5,porcentaje6,estadistico_a1,estadistico_a2,estadistico_a3,estadistico_b1,estadistico_b2,estadistico_b3,varios1,varios2,varios3,usuario,fecha_ult_mod,hora_ult_mod,tty,clave_modif,idioma,medio_envio,tarifa_part,tarifa_part_cadu,tarifa_ofer,tarifa_ofer_cadu,grupo_compra,pedido_a,num_viajante,fecha_ult_compra,cta_gastos,tipo_iva,tipo_irpf,nom_abreviado,cod_pueblo,centro_pago,clave_portes) values (40413,'MARECA CHUECA JOSE MIGUEL              ','MARECA CHUECA JOSE MIGUEL              ','PUERTA DE LA VILLA 3                   ','TABUENCA                           ','Tabuenca','50547','000','50','25449312L           ','976-865890  ',,,,'0 ',81,1,'I','40010343',2085,5401,'0330392231          ','N','N','17/12/2001',0,0.0,0.0,0.0,0.0,0.0,0.0,'EA',,,,,'N ',,,,'gestion','11/12/2014','16:47:04','pts-0  ',' ','C ',1,0,,0,,40413,'P',0,,,4,2,,,0,);
		 * - Ejecutamos el SQL
		 */
	}
	private Object obtenerValor(ResultSetMetaData r_medataData, ResultSet r_rs, int r_col) throws SQLException
	{
		String cadena = null;
		String cuenta = null;
		Integer valor=0;
		
		try
		{
			if (r_rs.equals(null))
			{
				return "";
			}
			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnTypeName(r_col));
			
			if (r_medataData.getTableName(r_col).startsWith("e__ccs"))
			{
				if (r_medataData.getColumnName(r_col).contentEquals("codigo") || r_medataData.getColumnName(r_col).contentEquals("desde_codigo") || r_medataData.getColumnName(r_col).contentEquals("hasta_codigo"))
				{
					valor = this.valorDiferencial * 10;
					return r_rs.getInt(r_col) + valor;
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("socio"))
				{
					valor = this.valorDiferencial;
					return r_rs.getInt(r_col) + valor;
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("kgs_neto_liq"))
				{
					this.kilos = r_rs.getDouble(r_col);
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("valor_cosecha"))
				{
					return kilos*precio;
				}
			}
			else if (r_medataData.getTableName(r_col).startsWith("e__socio"))
			{
				if (r_medataData.getColumnName(r_col).contentEquals("socio"))
				{
					valor = this.valorDiferencial;
					return r_rs.getInt(r_col) + valor;
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("cta_proveedor"))
				{
					return "'" + this.cuentaContable + "'";
				}
			}
			else if (r_medataData.getTableName(r_col).startsWith("e_provee"))
			{
				if (r_medataData.getColumnName(r_col).contentEquals("proveedor") || r_medataData.getColumnName(r_col).contentEquals("grupo_compra"))
				{
					valor = this.valorDiferencial;
					return r_rs.getInt(r_col) + valor;
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("num_cuenta"))
				{
					return "'" + this.cuentaContable + "'";
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("apartado"))
				{
					return "'" + this.bodega.trim() + "'";
				}
				else if (r_medataData.getColumnName(r_col).contentEquals("cod_pago"))
				{
					return this.formaPago.trim();
				}
				
			}
			else if (r_medataData.getTableName(r_col).startsWith("e_articu"))
			{
				if (r_medataData.getColumnName(r_col).contentEquals("precio_compra1"))
				{
					return this.precio;
				}
			}
			
			switch (r_medataData.getColumnType(r_col)) 
			{
				case Types.LONGVARCHAR:
					cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		return "'" + cadena + "'" ;
				case Types.BIGINT:
					return r_rs.getLong(r_col);
		    	case Types.CHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "''";
					}
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(cadena,","," "));
		    		return "'" + cadena + "'" ;
		    	case Types.DECIMAL:
		    		return r_rs.getDouble(r_col);
		    	case Types.TIMESTAMP:
		    		if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					Date ts = r_rs.getDate(r_col);
					String fecha = ts.toString();
					return "'" + fecha + "'";
		    	case Types.DATE:
					if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					if (dest.contentEquals("GREENsys"))
					{
						if (r_rs.getDate(r_col)==null) return "''";
						
						return "'" + RutinasFechas.convertirDateToString(r_rs.getDate(r_col)) + "'" ;
					}
					else return "'" + r_rs.getDate(r_col) + "'" ;
		    	case Types.SMALLINT:
		    		return r_rs.getInt(r_col);
		    	case Types.REAL:
		    		return r_rs.getInt(r_col);
		    	case Types.INTEGER:
		    		return r_rs.getInt(r_col) ;
		    	case Types.FLOAT:
		    		return r_rs.getFloat(r_col);
		    	case Types.VARCHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "''";
					}
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(cadena,","," "));
		    		return "'" + cadena + "'" ;
		    	case Types.DOUBLE:
		    		return r_rs.getDouble(r_col);
		    	case Types.TIME:
		    		if (r_rs.getTime(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00";
					}
					if (dest.contentEquals("GREENsys"))
					{
						if (r_rs.getTime(r_col)==null) return "''";
						
						return "'" + r_rs.getTime(r_col) +"'" ;
					}
					else return "'" + r_rs.getTime(r_col) +"'" ;
		    	case Types.BIT:
		    		return "'" + r_rs.getByte(r_col) +"'" ;		    				    			
		    		
			    default:
			    	Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			        throw new Error("Unknown column type");
		    }
		}
		catch (Exception sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Sincronizador: " + r_medataData.getColumnName(r_col) + " " + sqlEx.getMessage());
		}				
		return null;
	}
	
	private Object obtenerValorGreensys(ResultSetMetaData r_medataData, ResultSet r_rs, int r_col) throws SQLException
	{
		String cadena = null;
		String cuenta = null;
		Integer valor=0;
		
		try
		{
			if (r_rs.equals(null))
			{
				return "";
			}
//			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
//			Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnTypeName(r_col));
			
			switch (r_medataData.getColumnType(r_col)) 
			{
				case Types.LONGVARCHAR:
					cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		return "'" + cadena + "'" ;
				case Types.BIGINT:
					return r_rs.getLong(r_col);
		    	case Types.CHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "''";
					}
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(cadena,","," "));
		    		return "'" + cadena.trim() + "'" ;
		    	case Types.DECIMAL:
		    		return r_rs.getDouble(r_col);
		    	case Types.TIMESTAMP:
		    		if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					Date ts = r_rs.getDate(r_col);
					String fecha = ts.toString();
					return "'" + fecha + "'";
		    	case Types.DATE:
					if (r_rs.getDate(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00 00:00";
					}
					if (dest.contentEquals("GREENsys"))
					{
						if (r_rs.getDate(r_col)==null) return "''";
						
						return "'" + RutinasFechas.convertirDateToString(r_rs.getDate(r_col)) + "'" ;
					}
					else return "'" + r_rs.getDate(r_col) + "'" ;
		    	case Types.SMALLINT:
		    		return r_rs.getInt(r_col);
		    	case Types.REAL:
		    		return r_rs.getInt(r_col);
		    	case Types.INTEGER:
		    		return r_rs.getInt(r_col) ;
		    	case Types.FLOAT:
		    		return r_rs.getFloat(r_col);
		    	case Types.VARCHAR:
		    		if ((r_rs.getString(r_col)==null) || (r_rs.getString(r_col)!=null && r_rs.getString(r_col).length()==0))					
					{
						return "''";
					}
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(r_rs.getString(r_col),"'","´"));
		    		cadena = RutinasCadenas.conversion(RutinasCadenas.reemplazar(cadena,","," "));
		    		return "'" + cadena.trim() + "'" ;
		    	case Types.DOUBLE:
		    		return r_rs.getDouble(r_col);
		    	case Types.TIME:
		    		if (r_rs.getTime(r_col)==null && r_medataData.isNullable(r_col)==0)					
					{
						return "00:00:00";
					}
					if (dest.contentEquals("GREENsys"))
					{
						if (r_rs.getTime(r_col)==null) return "''";
						
						return "'" + r_rs.getTime(r_col) +"'" ;
					}
					else return "'" + r_rs.getTime(r_col) +"'" ;
		    	case Types.BIT:
		    		return "'" + r_rs.getByte(r_col) +"'" ;		    				    			
		    		
			    default:
			    	Notificaciones.getInstance().mensajeSeguimiento(r_medataData.getColumnName(r_col));
			        throw new Error("Unknown column type");
		    }
		}
		catch (Exception sqlEx)
		{
			Notificaciones.getInstance().mensajeError("Sincronizador: " + r_medataData.getColumnName(r_col) + " " + sqlEx.getMessage());
		}				
		return null;
	}
	private Connection establcerConexion(String r_conector) throws ClassNotFoundException, SQLException
	{
		
		Connection con = null;

		if (r_conector.toUpperCase().equals("MYSQL"))
		{
			//esto rula para mysq seguro
			
				String dbUrl="jdbc:mysql://192.168.6.6:3306/laboratorio?characterEncoding=Cp437&autoReconnect=true";
				String user="root";
				String pass="compaq123";
				
				try
				{
						Class.forName("com.mysql.jdbc.Driver");
						con = DriverManager.getConnection(dbUrl, user, pass);				 
				}
				catch (ClassNotFoundException e) 
				{
					e.printStackTrace();
				}

		}
		else if (r_conector.toUpperCase().equals("TIENDA"))
		{
			//esto rula para mysq seguro
			con = connectionManager.getInstance().establecerConexionTienda();
		}
		else if (r_conector.toUpperCase().equals("ACCESS"))
		{
			//esto rula para access
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=//192.168.6.15/Sr/emp01/Bodega.mdb", "","");
		}
		else if (r_conector.toUpperCase().equals("GREENSYS"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestion();
		}
		else if (r_conector.toUpperCase().equals("BORJA"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionBorja();
		}
		else if (r_conector.toUpperCase().equals("POZUELO"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionPozuelo();
		}
		else if (r_conector.toUpperCase().equals("TABUENCA"))
		{
			//esto rula con GREENsys
			con = connectionManager.getInstance().establecerConexionGestionTabuenca();
		}


		else
		{
			return null;
		}
		
		return con;
	}
	private Boolean comprobarDestino(String r_bbdd, String r_tabla) throws ClassNotFoundException, SQLException
	{
		PreparedStatement stDestino=null;
		Connection conDestino = null;

		try
		{
			
			conDestino=this.establcerConexion(r_bbdd);
			StringBuffer sql_destino = new StringBuffer();
			sql_destino.append("select count(*) from " + r_tabla);
			
			stDestino = conDestino.prepareStatement(sql_destino.toString());
			stDestino.executeQuery();
			return true;
		}
		catch (SQLException sqlEx)
		{
//			Notificaciones.getInstance().mensajeError("No Exsite la tabla destino: " + r_bbdd + " - " + r_tabla);
			return false;
		}		
	}
	
	private void obtenerValoresBodega()
	{
		ArrayList<MapeoParametrosRCA> vector = null;
		consultaParametrosRCAServer cprca = consultaParametrosRCAServer.getInstance(CurrentUser.get());
		
		MapeoParametrosRCA map = new MapeoParametrosRCA();
		map.setBodega(this.bodega);
		
		vector = cprca.datosParametrosRCAGlobal(map);
		
		Iterator iter = vector.iterator();
		
		while (iter.hasNext())
		{
			MapeoParametrosRCA mapeo = new MapeoParametrosRCA();
			mapeo = (MapeoParametrosRCA) iter.next();
			
			
			if (mapeo.getParametro().contentEquals("codigo")) 
				this.valorDiferencial = new Integer(mapeo.getValor());
			else if (mapeo.getParametro().contentEquals("precio")) 
				this.precio = new Double(mapeo.getValor().replace(",", "."));
			else if (mapeo.getParametro().contentEquals("cuenta")) 
				this.cuentaContable= mapeo.getValor();
			else if (mapeo.getParametro().contentEquals("formaPago")) 
				this.formaPago= mapeo.getValor();
			else if (mapeo.getParametro().contentEquals("base")) 
				this.cuentaContableBaseImponible = mapeo.getValor();

		}
	}
	
	@Override
	public String semaforos() {
		return null;
	}
	
	public boolean comprobarProveedores(String r_conexion)
	{
		boolean hayDatos = false;
		
		String rdo = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT count(*) prov_tot");
		cadenaSQL.append(" from e_provee");			
		cadenaSQL.append(" where apartado = '" + r_conexion + "'");
		
		try
		{

			conex= this.establcerConexion("GREENsys");			
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				while(rsOpcion.next())
				{
					if (rsOpcion.getInt("prov_tot")!=0 && rsOpcion.getInt("prov_tot")>10) hayDatos=true; else hayDatos=false;
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());		
			return false;
		}

		return hayDatos;
	}

	public boolean sincronizarSocios(String r_conexion, String r_ejercicio, StringBuffer r_sql)
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conMysql = null;
		HashMap<Integer, Integer> hasSocios = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT socio + " + valorDiferencial + " soc_real, ");
		cadenaSQL.append(" socio - " + valorDiferencial + " soc_origen");
		cadenaSQL.append(" from e__socio");			
		cadenaSQL.append(" where cta_proveedor = '" + this.cuentaContable + "'");
		
		try
		{

			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			conex= this.establcerConexion("GREENsys");			
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				hasSocios = new HashMap<Integer, Integer>();
				
				while(rsOpcion.next())
				{
					hasSocios.put(rsOpcion.getInt("soc_origen"),rsOpcion.getInt("soc_real"));
				}
			}
			/*
			 * Ahora recorro en mysql los proveedores de esa bodega y los que no estén en el hash los incorporo a greensys
			 */
			conMysql= this.establcerConexion("MySQL");
			if (conMysql!=null)
			{
				if (r_conexion.toString().trim().contentEquals("Borja")) tablaOrigen = "_bor";
				if (r_conexion.toString().trim().contentEquals("Pozuelo")) tablaOrigen = "_poz";
				if (r_conexion.toString().trim().contentEquals("Tabuenca")) tablaOrigen = "_tab";
	
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" SELECT * from e__socio" + tablaOrigen );
				cadenaSQL.append(" where socio in (select socio from e__ccs" + tablaOrigen + " where ejercicio = " + r_ejercicio + " and cultivo = 1 and " + r_sql.toString() +  ") ");

				
				PreparedStatement stOrigen = conMysql.prepareStatement(cadenaSQL.toString());
				ResultSet rsOrigen = stOrigen.executeQuery();
				ResultSetMetaData rsMetaDataOrigen = rsOrigen.getMetaData();
				
				/*
				 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
				 * por otro los registros obtenidos
				 * 
				 * con todo esto montamos el SQL del insert en destino
				 */
				boolean conErrores=false;
				
				StringBuffer sql_destino = new StringBuffer();

				while(rsOrigen.next())
				{
					Integer soc = hasSocios.get(rsOrigen.getInt("socio"));
					
					if (soc==null)
					{
						sql_destino = new StringBuffer();
						sql_destino.append("insert into e__socio (");
							
						// bucle para obtener los campos										
						for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
						{	
							sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
						}
						sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
						sql_destino.append(") values (");
						
						
						for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
						{
							Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , j+1);
							if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
							{
								rdo="";
								conErrores=false;
							}						
							sql_destino.append(rdo + ",");
						}
						Object rdo=this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
						if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
						{
							rdo="";
						}
						sql_destino.append(rdo + ");");
						
						if (!conErrores )
						{
							PreparedStatement stDestino = conex.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
						}
					}
				}			
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
			return false;
		}

		return true;
	}
	
	public boolean aplicarEquivalencias(String r_conexion)
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conMysql = null;
		HashMap<Integer, Integer> hasProveedores = null;
		String campo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			conex= this.establcerConexion("GREENsys");

			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			consultaEquivalenciasRCAServer ceq = consultaEquivalenciasRCAServer.getInstance(CurrentUser.get());
			ArrayList<MapeoEquivalenciasRCA> vec = ceq.datosEquivalenciasRCAGlobal(null);
			Iterator<MapeoEquivalenciasRCA> iter = vec.iterator();
			
			while(iter.hasNext())
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append("update e_provee ");

				MapeoEquivalenciasRCA mapEq = iter.next();
				if (mapEq.getConcepto().toUpperCase().contentEquals("REGIMEN"))
				{
					campo = "estadistico_a1";
				}
				else if (mapEq.getConcepto().toUpperCase().contentEquals("MODALIDAD"))
				{
					campo = "modalidad_iva";
				}
				else if (mapEq.getConcepto().toUpperCase().contentEquals("IVA"))
				{
					campo = "tipo_iva";
				}
				else if (mapEq.getConcepto().toUpperCase().contentEquals("IRPF"))
				{
					campo = "tipo_irpf";
				}
				cadenaSQL.append("set " + campo + " = '" + mapEq.getDestino() + "' ");
				cadenaSQL.append("where " + campo  + " = '" + mapEq.getOrigen() + "' ");
				cadenaSQL.append("and apartado = '" + r_conexion.trim() + "' ");
				/*
				 * Ahora recorro en mysql los proveedores de esa bodega y los que no estén en el hash los incorporo a greensys
				 */
				if (conex!=null)
				{
					PreparedStatement stDestino = conex.prepareStatement(cadenaSQL.toString());
					stDestino.executeUpdate();
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
			return false;
		}

		return true;
	}
	public boolean actualizarArticulos()
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conMysql = null;
		HashMap<Integer, Integer> hasProveedores = null;
		String campo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			conex= this.establcerConexion("GREENsys");

			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			cadenaSQL.append(" update e_articu ");
			cadenaSQL.append(" set precio_compra1 = " + this.precio);
			cadenaSQL.append(" , cta_compras = '" + this.cuentaContableBaseImponible  + "' " );
			cadenaSQL.append(" , vl_caract_1 = '01199'");
			cadenaSQL.append(" where articulo in (select articulo from e__ccs) ");
			
			/*
			 * Ahora recorro en mysql los proveedores de esa bodega y los que no estén en el hash los incorporo a greensys
			 */
			if (conex!=null)
			{
				
				PreparedStatement stDestino = conex.prepareStatement(cadenaSQL.toString());
				stDestino.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());		
			return false;
		}

		return true;
	}
	public boolean actualizarValorCosecha()
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conMysql = null;
		HashMap<Integer, Integer> hasProveedores = null;
		String campo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try
		{
			conex= this.establcerConexion("GREENsys");
			
			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			cadenaSQL.append(" update e__ccs ");
			cadenaSQL.append(" set fecha_valorac = '" + RutinasFechas.fechaActual() + "' ");
			cadenaSQL.append(" where fecha_valorac is null ");
			
			/*
			 * Ahora recorro en mysql los proveedores de esa bodega y los que no estén en el hash los incorporo a greensys
			 */
			if (conex!=null)
			{
				
				PreparedStatement stDestino = conex.prepareStatement(cadenaSQL.toString());
				stDestino.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());		
			return false;
		}
		
		return true;
	}
	
	public boolean aplicarCambios(String r_conexion, Integer r_ejercicio)
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		boolean rdo = false;

		cadenaSQL.append(" SELECT a.coop rca_bod, ");
		cadenaSQL.append(" a.precio rca_pre, ");
		cadenaSQL.append(" a.socio rca_soc, ");
		cadenaSQL.append(" a.codigo rca_alb ");
		cadenaSQL.append(" from rca_ccs a");
		cadenaSQL.append(" where a.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and a.coop = '" + r_conexion + "' ");

		try
		{
			con= connectionManager.getInstance().establecerConexion();			
			if (con!=null)
			{
				/*
				 * recojo todos los albaranes a modificar
				 */
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				
				while(rsOpcion.next())
				{
					MapeoAlbaranesRCA map = new MapeoAlbaranesRCA();
					
					map.setPrecio(rsOpcion.getString("rca_pre"));
					map.setSocio(rsOpcion.getString("rca_soc"));
					map.setBodega(rsOpcion.getString("rca_bod"));
					map.setAlbaran(rsOpcion.getString("rca_alb"));
					map.setEjercicio(r_ejercicio.toString());
					
					String rrdo = this.guardarAplcacionCambios(map, r_conexion);
					if (rrdo!=null) return false;
					
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError("No se ha podido establecer la conexion con: " + r_conexion);
				return false;
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());			
		}
		return true;
	}
	
	private String guardarAplcacionCambios(MapeoAlbaranesRCA r_mapeo, String r_conexion)
	{
		
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Integer socio =null;
		try
		{
			/*
			 * hago update en greensys
			 * buscando el albaran, modificando su codigo en funcion de la bodega,
			 * para aplicar el precio y el valor cosecha con él
			 */
			
			this.bodega=r_conexion;
			this.obtenerValoresBodega();
			socio = (new Integer(r_mapeo.getSocio()) + this.valorDiferencial);
			
			cadenaSQL.append(" UPDATE e__ccs set ");
			cadenaSQL.append(" socio = " + socio.toString() + ", ");
		    cadenaSQL.append(" valor_cosecha = kgs_neto_liq * " + new Double(r_mapeo.getPrecio()));
		    cadenaSQL.append(" where codigo = ? ");
		    cadenaSQL.append(" and ejercicio = ? ");
			
		    con= connectionManager.getInstance().establecerConexionGestionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setInt(1, new Integer(r_mapeo.getAlbaran()) + (valorDiferencial*10));
		    preparedStatement.setInt(2, new Integer(r_mapeo.getEjercicio()));
		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			Notificaciones.getInstance().mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally {
			con=null;
			preparedStatement=null;
		}
		return null;
	}
	public boolean sincronizarProveedores(String r_conexion, String r_ejercicio, StringBuffer r_sql)
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conMysql = null;
		HashMap<Integer, Integer> hasProveedores = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT proveedor + " + valorDiferencial + " prov_real, ");
		cadenaSQL.append(" proveedor - " + valorDiferencial + " prov_origen");
		cadenaSQL.append(" from e_provee");			
		cadenaSQL.append(" where apartado = '" + r_conexion.trim() + "'");
		
		try
		{

			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			conex= this.establcerConexion("GREENsys");			
			dest = "GREENsys";
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				hasProveedores = new HashMap<Integer, Integer>();
				
				while(rsOpcion.next())
				{
					hasProveedores.put(rsOpcion.getInt("prov_origen"),rsOpcion.getInt("prov_real"));
				}
			}
			/*
			 * Ahora recorro en mysql los proveedores de esa bodega y los que no estén en el hash los incorporo a greensys
			 */
			conMysql= this.establcerConexion("MySQL");
			if (conMysql!=null)
			{
				if (r_conexion.toString().trim().contentEquals("Borja")) tablaOrigen = "_bor";
				if (r_conexion.toString().trim().contentEquals("Pozuelo")) tablaOrigen = "_poz";
				if (r_conexion.toString().trim().contentEquals("Tabuenca")) tablaOrigen = "_tab";
	
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" SELECT * from e_provee" + tablaOrigen );
				cadenaSQL.append(" where proveedor in (select socio from e__ccs" + tablaOrigen + " where ejercicio = " + r_ejercicio + " and cultivo = 1 and " + r_sql.toString() + ") ");
				
				PreparedStatement stOrigen = conMysql.prepareStatement(cadenaSQL.toString());
				ResultSet rsOrigen = stOrigen.executeQuery();
				ResultSetMetaData rsMetaDataOrigen = rsOrigen.getMetaData();
				
				/*
				 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
				 * por otro los registros obtenidos
				 * 
				 * con todo esto montamos el SQL del insert en destino
				 */				
				
				boolean conErrores=false;
				
				StringBuffer sql_destino = new StringBuffer();

				while(rsOrigen.next())
				{
					Integer prov = hasProveedores.get(rsOrigen.getInt("proveedor"));
					
					if (prov==null)
					{
						sql_destino = new StringBuffer();
						sql_destino.append("insert into e_provee (");
							
						// bucle para obtener los campos										
						for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
						{	
							sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
						}
						sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
						sql_destino.append(") values (");
						
						
						for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
						{
							Object rdo = this.obtenerValor(rsMetaDataOrigen, rsOrigen , j+1);
							if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
							{
								rdo="";
								conErrores=false;
							}						
							sql_destino.append(rdo + ",");
						}
						Object rdo=this.obtenerValor(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
						if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
						{
							rdo="";
						}
						sql_destino.append(rdo + ");");
						
						if (!conErrores )
						{
							PreparedStatement stDestino = conex.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
						}
					}
				}			
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());		
			return false;
		}

		return true;
	}
	
	public boolean sincronizarArticulos(String r_conexion)
	{
		String tablaOrigen = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		Connection conOrigen = null;
		HashMap<String, String> hasArticulos = null;
		dest = "GREENsys";
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a.articulo  art_real, ");
		cadenaSQL.append(" b.articulo art_sup");
		cadenaSQL.append(" from e_articu a, e_articu b");
		cadenaSQL.append(" where a.cod_superior = b.articulo ");
		cadenaSQL.append(" and b.descrip_articulo matches 'UVA*' ");
		cadenaSQL.append(" and b.grado = 2 ");
		
		try
		{

			/*
			 * Me conecto a la base de datos de destino y relleno un hash con los proveedores que tengo creados de esa bodega
			 */
			conex= this.establcerConexion("GREENsys");			
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				hasArticulos = new HashMap<String, String>();
				
				while(rsOpcion.next())
				{
					hasArticulos.put(rsOpcion.getString("art_real").trim(),rsOpcion.getString("art_sup").trim());
				}
			}
			/*
			 * Ahora recorro los articulos de esa bodega y los que no estén en el hash los incorporo a greensys
			 */
			conOrigen= this.establcerConexion(r_conexion.toString().trim());
			if (conOrigen!=null)
			{
				tablaOrigen = "e_articu";
	
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" SELECT a.* from " + tablaOrigen + " a, " + tablaOrigen + " b ");
				cadenaSQL.append(" where a.cod_superior = b.articulo ");
				cadenaSQL.append(" and b.descrip_articulo matches 'UVA*' ");
				cadenaSQL.append(" and b.grado = 2 ");
				
				PreparedStatement stOrigen = conOrigen.prepareStatement(cadenaSQL.toString());
				ResultSet rsOrigen = stOrigen.executeQuery();
				ResultSetMetaData rsMetaDataOrigen = rsOrigen.getMetaData();
				
				/*
				 * con rsOrigen y su metaData sacamos la estrcutra de campos por un lado
				 * por otro los registros obtenidos
				 * 
				 * con todo esto montamos el SQL del insert en destino
				 */
				boolean conErrores=false;
				
				StringBuffer sql_destino = new StringBuffer();
				
				while(rsOrigen.next())
				{
					String art = hasArticulos.get(rsOrigen.getString("articulo").trim());
					
					if (art==null)
					{
						System.out.println("Articulo con error " + rsOrigen.getString("articulo").trim());
						sql_destino = new StringBuffer();
						sql_destino.append("insert into e_articu (");
							
						// bucle para obtener los campos										
						for (int c=0; c<rsMetaDataOrigen.getColumnCount()-1;c++)
						{	
							sql_destino.append(rsMetaDataOrigen.getColumnName(c+1) + ",");
						}
						sql_destino.append(rsMetaDataOrigen.getColumnName(rsMetaDataOrigen.getColumnCount()));
						sql_destino.append(") values (");
						
						
						for (int j=0; j<rsMetaDataOrigen.getColumnCount()-1;j++)
						{
							Object rdo = this.obtenerValorGreensys(rsMetaDataOrigen, rsOrigen , j+1);
							if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
							{
								rdo="";
								conErrores=false;
							}						
							sql_destino.append(rdo + ",");
						}
						Object rdo=this.obtenerValorGreensys(rsMetaDataOrigen, rsOrigen , rsMetaDataOrigen.getColumnCount());
						if (rdo==null ||  rdo.equals(null) || rdo.equals("null") || rdo.equals("'null'") || rdo.equals(""))
						{
							rdo="";
						}
						sql_destino.append(rdo + ");");
						
						if (!conErrores )
						{
							PreparedStatement stDestino = conex.prepareStatement(sql_destino.toString());
							stDestino.executeUpdate();
						}
					}
				}			
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());		
			return false;
		}

		return true;
	}

	public boolean comprobarSocio(String r_conexion)
	{
		boolean hayDatos = false;
		
		String rdo = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT count(*) soc_tot");
		cadenaSQL.append(" from e__socio");			
		cadenaSQL.append(" where cta_proveedor = '" + this.cuentaContable + "'");
		
		try
		{

			conex= this.establcerConexion("GREENsys");			
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				while(rsOpcion.next())
				{
					if (rsOpcion.getInt("soc_tot")!=0 && rsOpcion.getInt("soc_tot")>10 ) hayDatos=true; else hayDatos=false;
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());			
		}

		return hayDatos;
	}
	
	public boolean comprobarAlbaranes(String r_conexion)
	{
		boolean hayDatos = false;
		
		String rdo = null;
		ResultSet rsOpcion = null;
		Connection conex = null;
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT count(*) ent_tot");
		cadenaSQL.append(" from e__ccs");			
		cadenaSQL.append(" where socio in (select proveedor from e_provee where apartado = '" + r_conexion + "') ");
		
		try
		{

			conex= this.establcerConexion(r_conexion);			
			if (conex!=null)
			{
				Statement cs = conex.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new java.util.Date());
				
				while(rsOpcion.next())
				{
					if (rsOpcion.getInt("soc_tot")!=0 && rsOpcion.getInt("soc_tot")>10) hayDatos=true; else hayDatos=false;
				}
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());			
		}

		return hayDatos;
	}
}

