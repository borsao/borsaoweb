package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoParametrosRCA mapeo = null;
	 
	 public MapeoParametrosRCA convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoParametrosRCA();
		 
		 this.mapeo.setValor(r_hash.get("valor"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setParametro(r_hash.get("parametro"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoParametrosRCA convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoParametrosRCA();
		 
		 this.mapeo.setValor(r_hash.get("valor"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setParametro(r_hash.get("parametro"));

		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoParametrosRCA r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("valor", r_mapeo.getValor());
		 this.hash.put("bodega", r_mapeo.getBodega());
		 this.hash.put("parametro", r_mapeo.getParametro());
		 return hash;		 
	 }
}