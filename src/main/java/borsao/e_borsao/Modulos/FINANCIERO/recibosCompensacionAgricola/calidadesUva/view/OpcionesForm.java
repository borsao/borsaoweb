package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.modelo.CalidadesUvaGrid;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.modelo.MapeoCalidadesUva;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.server.consultaCalidadesUvaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoCalidadesUva mapeoCalidadesUva = null;
	 
	 private consultaCalidadesUvaServer cus = null;	 
	 private CalidadesUvaView uw = null;
	 private BeanFieldGroup<MapeoCalidadesUva> fieldGroup;
	 
	 public OpcionesForm(CalidadesUvaView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");

        cus = consultaCalidadesUvaServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoCalidadesUva>(MapeoCalidadesUva.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoCalidadesUva = (MapeoCalidadesUva) uw.grid.getSelectedRow();
                eliminarCalidadesUva(mapeoCalidadesUva);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoCalidadesUva = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearCalidadesUva(mapeoCalidadesUva);
	 			}
	 			else
	 			{
	 				MapeoCalidadesUva mapeoCalidadesUva_orig = (MapeoCalidadesUva) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoCalidadesUva= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarCalidadesUva(mapeoCalidadesUva,mapeoCalidadesUva_orig);
	 				hm=null;
	 			}
	 			if (((CalidadesUvaGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarCalidadesUva(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.codigo.getValue()!=null && this.codigo.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("codigo", this.codigo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigo", "");
		}
		
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarCalidadesUva(null);
	}
	
	public void editarCalidadesUva(MapeoCalidadesUva r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoCalidadesUva();		
		fieldGroup.setItemDataSource(new BeanItem<MapeoCalidadesUva>(r_mapeo));
		codigo.focus();
	}
	
	public void eliminarCalidadesUva(MapeoCalidadesUva r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((CalidadesUvaGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoCalidadesUva>) ((CalidadesUvaGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarCalidadesUva(MapeoCalidadesUva r_mapeo, MapeoCalidadesUva r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaCalidadesUvaServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((CalidadesUvaGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearCalidadesUva(MapeoCalidadesUva r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaCalidadesUvaServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoCalidadesUva>(r_mapeo));
				if (((CalidadesUvaGrid) uw.grid)!=null)
				{
					((CalidadesUvaGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoCalidadesUva> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoCalidadesUva= item.getBean();
            if (this.mapeoCalidadesUva.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.codigo.getValue()==null || this.codigo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Código");
    		return false;
    	}
    	if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar La Descripción");
    		return false;
    	}
    	return true;
    }

}
