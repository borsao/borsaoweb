package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCalidadesUva extends MapeoGlobal
{
	private String codigo;
	private String descripcion;

	public MapeoCalidadesUva()
	{
		this.setCodigo("");
		this.setDescripcion("");
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}