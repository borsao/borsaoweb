package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ParametrosRCAGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public ParametrosRCAGrid(ArrayList<MapeoParametrosRCA> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Parametros RCA");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoParametrosRCA.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("bodega", "parametro", "valor");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("bodega", "150");
    	this.widthFiltros.put("parametro", "150");
    	
    	this.getColumn("bodega").setHeaderCaption("Bodega");
    	this.getColumn("parametro").setHeaderCaption("Parametro");
    	this.getColumn("valor").setHeaderCaption("Valor");    	
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("valor");
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
