package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.MapeoParametrosRCA;

public class consultaParametrosRCAServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaParametrosRCAServer instance;
	
	public consultaParametrosRCAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaParametrosRCAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaParametrosRCAServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoParametrosRCA> datosParametrosRCAGlobal(MapeoParametrosRCA r_mapeo)
	{
		ResultSet rsParametrosRCA = null;		
		ArrayList<MapeoParametrosRCA> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_parametros.id rca_id, ");
		cadenaSQL.append(" rca_parametros.bodega rca_bod, ");
		cadenaSQL.append(" rca_parametros.parametro rca_par, ");
		cadenaSQL.append(" rca_parametros.valor rca_val ");
     	cadenaSQL.append(" FROM rca_parametros ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor = '" + r_mapeo.getValor() + "' ");
				}
				if (r_mapeo.getParametro()!=null && r_mapeo.getParametro().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" parametro = '" + r_mapeo.getParametro() + "' ");
				}
				if (r_mapeo.getBodega()!=null && r_mapeo.getBodega().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" bodega = '" + r_mapeo.getBodega() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsParametrosRCA.TYPE_SCROLL_SENSITIVE,rsParametrosRCA.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsParametrosRCA= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoParametrosRCA>();
			
			while(rsParametrosRCA.next())
			{
				MapeoParametrosRCA mapeoParametrosRCA = new MapeoParametrosRCA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoParametrosRCA.setIdCodigo(rsParametrosRCA.getInt("rca_id"));
				mapeoParametrosRCA.setBodega(rsParametrosRCA.getString("rca_bod"));
				mapeoParametrosRCA.setParametro(rsParametrosRCA.getString("rca_par"));
				mapeoParametrosRCA.setValor(rsParametrosRCA.getString("rca_val"));
				vector.add(mapeoParametrosRCA);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public String datosParametrosRCAPrecio(MapeoParametrosRCA r_mapeo)
	{
		ResultSet rsParametrosRCA = null;		
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_parametros.id rca_id, ");
		cadenaSQL.append(" rca_parametros.bodega rca_bod, ");
		cadenaSQL.append(" rca_parametros.parametro rca_par, ");
		cadenaSQL.append(" rca_parametros.valor rca_val ");
		cadenaSQL.append(" FROM rca_parametros ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getValor()!=null && r_mapeo.getValor().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor = '" + r_mapeo.getValor() + "' ");
				}
				if (r_mapeo.getParametro()!=null && r_mapeo.getParametro().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" parametro = '" + r_mapeo.getParametro() + "' ");
				}
				if (r_mapeo.getBodega()!=null && r_mapeo.getBodega().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" bodega = '" + r_mapeo.getBodega() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsParametrosRCA.TYPE_SCROLL_SENSITIVE,rsParametrosRCA.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsParametrosRCA= cs.executeQuery(cadenaSQL.toString());
			
			while(rsParametrosRCA.next())
			{
				MapeoParametrosRCA mapeoParametrosRCA = new MapeoParametrosRCA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoParametrosRCA.setBodega(rsParametrosRCA.getString("rca_bod"));
				mapeoParametrosRCA.setParametro(rsParametrosRCA.getString("rca_par"));
				mapeoParametrosRCA.setValor(rsParametrosRCA.getString("rca_val"));
				
				if (mapeoParametrosRCA.getParametro().contentEquals("precio")) 
					return mapeoParametrosRCA.getValor();
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsParametrosRCA = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(rca_parametros.id) rca_sig ");
     	cadenaSQL.append(" FROM rca_parametros ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsParametrosRCA.TYPE_SCROLL_SENSITIVE,rsParametrosRCA.CONCUR_UPDATABLE);
			rsParametrosRCA= cs.executeQuery(cadenaSQL.toString());
			
			while(rsParametrosRCA.next())
			{
				return rsParametrosRCA.getInt("rca_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoParametrosRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO rca_parametros ( ");
    		cadenaSQL.append(" rca_parametros.id, ");
    		cadenaSQL.append(" rca_parametros.bodega, ");
	        cadenaSQL.append(" rca_parametros.parametro, ");
		    cadenaSQL.append(" rca_parametros.valor ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getBodega()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getBodega());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getParametro()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getParametro());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getValor()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getValor());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoParametrosRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE rca_parametros set ");
		    cadenaSQL.append(" rca_parametros.bodega=?, ");
		    cadenaSQL.append(" rca_parametros.parametro=?, ");
		    cadenaSQL.append(" rca_parametros.valor=? ");
		    cadenaSQL.append(" WHERE rca_parametros.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getBodega());
		    preparedStatement.setString(2, r_mapeo.getParametro());
		    preparedStatement.setString(3, r_mapeo.getValor());
		    preparedStatement.setInt(4, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoParametrosRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM rca_parametros ");            
			cadenaSQL.append(" WHERE rca_parametros.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}