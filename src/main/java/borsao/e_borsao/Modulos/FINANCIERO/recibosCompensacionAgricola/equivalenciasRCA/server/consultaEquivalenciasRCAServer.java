package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo.MapeoEquivalenciasRCA;

public class consultaEquivalenciasRCAServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEquivalenciasRCAServer instance;
	
	public consultaEquivalenciasRCAServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEquivalenciasRCAServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEquivalenciasRCAServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoEquivalenciasRCA> datosEquivalenciasRCAGlobal(MapeoEquivalenciasRCA r_mapeo)
	{
		ResultSet rsEquivalenciasRCA = null;		
		ArrayList<MapeoEquivalenciasRCA> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_equivalencias.id rca_id, ");
		cadenaSQL.append(" rca_equivalencias.bodega rca_bod, ");
		cadenaSQL.append(" rca_equivalencias.concepto rca_con, ");
		cadenaSQL.append(" rca_equivalencias.origen rca_ori, ");
		cadenaSQL.append(" rca_equivalencias.destino rca_des ");
     	cadenaSQL.append(" FROM rca_equivalencias ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" origen = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getDestino()!=null && r_mapeo.getDestino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destino = '" + r_mapeo.getDestino() + "' ");
				}
				if (r_mapeo.getConcepto()!=null && r_mapeo.getConcepto().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" concepto = '" + r_mapeo.getConcepto() + "' ");
				}
				if (r_mapeo.getBodega()!=null && r_mapeo.getBodega().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" bodega = '" + r_mapeo.getBodega() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquivalenciasRCA.TYPE_SCROLL_SENSITIVE,rsEquivalenciasRCA.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsEquivalenciasRCA= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoEquivalenciasRCA>();
			
			while(rsEquivalenciasRCA.next())
			{
				MapeoEquivalenciasRCA mapeoEquivalenciasRCA = new MapeoEquivalenciasRCA();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquivalenciasRCA.setIdCodigo(rsEquivalenciasRCA.getInt("rca_id"));
				mapeoEquivalenciasRCA.setBodega(rsEquivalenciasRCA.getString("rca_bod"));
				mapeoEquivalenciasRCA.setConcepto(rsEquivalenciasRCA.getString("rca_con"));
				mapeoEquivalenciasRCA.setOrigen(rsEquivalenciasRCA.getString("rca_ori"));
				mapeoEquivalenciasRCA.setDestino(rsEquivalenciasRCA.getString("rca_des"));
				vector.add(mapeoEquivalenciasRCA);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsEquivalenciasRCA = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(rca_equivalencias.id) rca_sig ");
     	cadenaSQL.append(" FROM rca_equivalencias ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquivalenciasRCA.TYPE_SCROLL_SENSITIVE,rsEquivalenciasRCA.CONCUR_UPDATABLE);
			rsEquivalenciasRCA= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquivalenciasRCA.next())
			{
				return rsEquivalenciasRCA.getInt("rca_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoEquivalenciasRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO rca_equivalencias ( ");
    		cadenaSQL.append(" rca_equivalencias.id, ");
    		cadenaSQL.append(" rca_equivalencias.bodega, ");
	        cadenaSQL.append(" rca_equivalencias.concepto, ");
	        cadenaSQL.append(" rca_equivalencias.origen, ");
		    cadenaSQL.append(" rca_equivalencias.destino ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getBodega()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getBodega());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getConcepto()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getConcepto());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDestino()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoEquivalenciasRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE rca_equivalencias set ");
		    cadenaSQL.append(" rca_equivalencias.bodega=?, ");
		    cadenaSQL.append(" rca_equivalencias.concepto=?, ");
		    cadenaSQL.append(" rca_equivalencias.origen=?, ");
		    cadenaSQL.append(" rca_equivalencias.destino=? ");
		    cadenaSQL.append(" WHERE rca_equivalencias.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setString(1, r_mapeo.getBodega());
		    preparedStatement.setString(2, r_mapeo.getConcepto());
		    preparedStatement.setString(3, r_mapeo.getOrigen());
		    preparedStatement.setString(4, r_mapeo.getDestino());
		    preparedStatement.setInt(5, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoEquivalenciasRCA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM rca_equivalencias ");            
			cadenaSQL.append(" WHERE rca_equivalencias.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}