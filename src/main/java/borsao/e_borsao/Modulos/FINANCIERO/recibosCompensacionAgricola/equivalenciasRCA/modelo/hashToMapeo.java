package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoEquivalenciasRCA mapeo = null;
	 
	 public MapeoEquivalenciasRCA convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquivalenciasRCA();
		 
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setDestino(r_hash.get("destino"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setConcepto(r_hash.get("concepto"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoEquivalenciasRCA convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 this.mapeo = new MapeoEquivalenciasRCA();
		 
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setDestino(r_hash.get("destino"));
		 this.mapeo.setBodega(r_hash.get("bodega"));		 
		 this.mapeo.setConcepto(r_hash.get("concepto"));

		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoEquivalenciasRCA r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("origen", r_mapeo.getOrigen());
		 this.hash.put("destino", r_mapeo.getDestino());
		 this.hash.put("bodega", r_mapeo.getBodega());
		 this.hash.put("concepto", r_mapeo.getConcepto());
		 return hash;		 
	 }
}