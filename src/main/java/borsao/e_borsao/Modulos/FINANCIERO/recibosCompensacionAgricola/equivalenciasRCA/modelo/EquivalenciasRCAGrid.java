package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EquivalenciasRCAGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public EquivalenciasRCAGrid(ArrayList<MapeoEquivalenciasRCA> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Equivalencias RCA");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoEquivalenciasRCA.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("bodega", "concepto", "origen", "destino");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("bodega", "150");
    	this.widthFiltros.put("concepto", "150");
    	
    	this.getColumn("bodega").setHeaderCaption("Bodega");
    	this.getColumn("concepto").setHeaderCaption("Concepto");
    	this.getColumn("origen").setHeaderCaption("Origen");
    	this.getColumn("destino").setHeaderCaption("Destino");
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("origen");
		this.camposNoFiltrar.add("destino");
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
