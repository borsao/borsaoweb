package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.sincronizadorRCA.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.server.consultaCalidadesUvaServer;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.sincronizadorRCA.server.servidorSincronizadorRCA;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PantallaSincronizadorRCA extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private TextField txtEjercicio = null;
	private ComboBox cmbBodega = null;
	private CheckBox chkProveedores = null;
	private CheckBox chkSocios= null;
	private CheckBox chkAlbaranes = null;
	
	private TextField txtSQL= null;

	
	private SincronizadorRCAView origen = null;
	
	private VerticalLayout controles = null;
	
	public PantallaSincronizadorRCA(SincronizadorRCAView r_view)
	{
		this.origen=r_view;

		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);

				this.txtEjercicio = new TextField("Ejercicio");
				Integer ejercicio = new Integer(RutinasFechas.añoActualYYYY())-1;
				this.txtEjercicio.setValue(ejercicio.toString());
				this.cmbBodega = new ComboBox("Bodega");
				this.cmbBodega.setNullSelectionAllowed(false);

				fila1.addComponent(this.txtEjercicio);
				fila1.addComponent(this.cmbBodega);
			
				VerticalLayout checks = new VerticalLayout();
					checks.setSpacing(true);
					this.chkProveedores= new CheckBox("Proveedores");
					this.chkSocios= new CheckBox("Socios");
					this.chkAlbaranes = new CheckBox("Albaranes");
					
					checks.addComponent(chkProveedores);
					checks.addComponent(chkSocios);
					checks.addComponent(chkAlbaranes);
				
				fila1.addComponent(checks);
				
		controles.addComponent(fila1);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición Sincronización");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
		this.cargarCombo();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() 
		{
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				servidorSincronizadorRCA ss = null;
				ArrayList<MapeoSincronizarTablas> arr = null;
				MapeoSincronizarTablas mapeo = null;
				StringBuffer cadenaSQL = null;
				boolean resultado = true;
				
				if (todoEnOrden())
				{
					String terminacion = "";
					arr = new ArrayList<MapeoSincronizarTablas>();
					
					servidorSincronizadorRCA ssrca = new servidorSincronizadorRCA(cmbBodega.getValue().toString().trim());

					consultaCalidadesUvaServer ccus = new consultaCalidadesUvaServer(CurrentUser.get());					
					cadenaSQL = ccus.obtenerCalidadesUvaGlobal(null, cmbBodega.getValue().toString().trim());
							
					if (cmbBodega.getValue().toString().trim().contentEquals("Borja")) terminacion = "bor";
					if (cmbBodega.getValue().toString().trim().contentEquals("Pozuelo")) terminacion = "poz";
					if (cmbBodega.getValue().toString().trim().contentEquals("Tabuenca")) terminacion = "tab";
			
					if (chkAlbaranes.getValue() && resultado)
					{
						mapeo = new MapeoSincronizarTablas();
							mapeo.setBbddOrigen("MySQL");
							mapeo.setBbddDestino("GREENsys");
						
							mapeo.setTablaOrigen("e__ccs_"+ terminacion);
							mapeo.setTablaDestino("e__ccs");
							mapeo.setSql("where ejercicio = "+ txtEjercicio.getValue().trim() + " and cultivo = 1 and kgs_neto_liq>0 and " + cadenaSQL.toString() );
							
						arr.add(mapeo);
					}		
					if (chkProveedores.getValue() && resultado)
					{
						if (!ssrca.comprobarProveedores(cmbBodega.getValue().toString().trim()))
						{
							mapeo = new MapeoSincronizarTablas();					
								mapeo.setBbddOrigen("MySQL");
								mapeo.setBbddDestino("GREENsys");
							
								mapeo.setTablaOrigen("e_provee_"+ terminacion);
								mapeo.setTablaDestino("e_provee");
								mapeo.setSql("where proveedor in (select socio from e__ccs_"+ terminacion + " where ejercicio = "+ txtEjercicio.getValue().trim() + " and cultivo = 1 and " + cadenaSQL.toString() +  ") ");
					
							arr.add(mapeo);
						}
						else
						{
							resultado = ssrca.sincronizarProveedores(cmbBodega.getValue().toString().trim(),txtEjercicio.getValue().trim(), cadenaSQL);
							if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problema con el traspaso de proveedores");
						}
					}
					
					if (resultado)
					{
						resultado = ssrca.sincronizarArticulos(cmbBodega.getValue().toString().trim());
						if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problemas con el traspaso de articulos");
					}

					if (chkSocios.getValue() && resultado)
					{
						if (!ssrca.comprobarSocio(cmbBodega.getValue().toString().trim()))
						{
							mapeo = new MapeoSincronizarTablas();
								mapeo.setBbddOrigen("MySQL");
								mapeo.setBbddDestino("GREENsys");
							
								mapeo.setTablaOrigen("e__socio_"+ terminacion);
								mapeo.setTablaDestino("e__socio");
								mapeo.setSql("where socio in (select socio from e__ccs_"+ terminacion + " where ejercicio = "+ txtEjercicio.getValue().trim() + " and cultivo = 1 and " + cadenaSQL.toString() +  ") ");
								
							arr.add(mapeo);
						}
						else
						{
							resultado = ssrca.sincronizarSocios(cmbBodega.getValue().toString().trim(),txtEjercicio.getValue().trim(), cadenaSQL);
							if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problema con la sincronizacion de socios ");							
						}
					}
							
					if (!arr.isEmpty() && resultado ) ss = new servidorSincronizadorRCA(cmbBodega.getValue().toString().trim(), arr, txtEjercicio.getValue().trim());
					
					/*
					 * Aplicamos equivalencias en Proveedores
					 */
					if (resultado)
					{
						resultado = ssrca.aplicarEquivalencias(cmbBodega.getValue().toString().trim());
						if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problemas al aplicar Equivalencias");
					}
					/*
					 * actualizamos precios articulos
					 */
					if (resultado)
					{
						resultado = ssrca.actualizarArticulos();
						if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problema al actualizar los Precios y cuentas de los articulos");
					}
					
					/*
					 * actualizamos fecha_Valoracion cosecha
					 */
					if (resultado)
					{
						resultado = ssrca.actualizarValorCosecha();
						if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problema al actualizar la fecha valoracion cosecha");
						else
						{
							resultado = ssrca.aplicarCambios(cmbBodega.getValue().toString(),new Integer(txtEjercicio.getValue()));
							if (!resultado) Notificaciones.getInstance().mensajeInformativo("Problema al actualizar los albaranes a modificar");
						}
					}
					
					if (!resultado) Notificaciones.getInstance().mensajeInformativo("No se realizo la sincronizacion"); else Notificaciones.getInstance().mensajeInformativo("Sincronizacion Terminada");
					
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		
		
		btnBotonCVentana.addClickListener(new ClickListener() 
		{
			@Override
			public void buttonClick(ClickEvent event) 
			{
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		
		if (this.cmbBodega.getValue()==null || this.cmbBodega.getValue().toString().length()==0)
		{
			devolver = false;
			this.cmbBodega.focus();
		}
		else if (this.txtEjercicio.getValue()==null || this.txtEjercicio.getValue().length()==0) 
		{
			devolver =false;			
			txtEjercicio.focus();
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		this.cmbBodega.addItem("Tabuenca");
		this.cmbBodega.addItem("Pozuelo");
		this.cmbBodega.addItem("Borja");
	}

}