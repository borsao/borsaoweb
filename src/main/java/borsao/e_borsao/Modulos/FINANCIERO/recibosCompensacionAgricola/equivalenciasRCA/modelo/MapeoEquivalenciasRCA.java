package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEquivalenciasRCA extends MapeoGlobal
{
	private String bodega;
	private String concepto;
	private String origen;
	private String destino;

	public MapeoEquivalenciasRCA()
	{
		this.setBodega("");
		this.setOrigen("");
		this.setDestino("");
		this.setConcepto("");
	}

	public String getBodega() {
		return bodega;
	}

	public void setBodega(String bodega) {
		this.bodega = bodega;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

}