package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.MapeoParametrosRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.ParametrosRCAGrid;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.server.consultaParametrosRCAServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoParametrosRCA mapeoParametrosRCA = null;
	 
	 private consultaParametrosRCAServer cus = null;	 
	 private ParametrosRCAView uw = null;
	 private BeanFieldGroup<MapeoParametrosRCA> fieldGroup;
	 
	 public OpcionesForm(ParametrosRCAView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        this.cargarCombo();
        
        cus=consultaParametrosRCAServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoParametrosRCA>(MapeoParametrosRCA.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoParametrosRCA = (MapeoParametrosRCA) uw.grid.getSelectedRow();
                eliminarParametrosRCA(mapeoParametrosRCA);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoParametrosRCA = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearParametrosRCA(mapeoParametrosRCA);
	 			}
	 			else
	 			{
	 				MapeoParametrosRCA mapeoParametrosRCA_orig = (MapeoParametrosRCA) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoParametrosRCA= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarParametrosRCA(mapeoParametrosRCA,mapeoParametrosRCA_orig);
	 				hm=null;
	 			}
	 			if (((ParametrosRCAGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarParametrosRCA(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.bodega.getValue()!=null && this.bodega.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("bodega", this.bodega.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bodega", "");
		}
		
		if (this.parametro.getValue()!=null && this.parametro.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("parametro", this.parametro.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("parametro", "");
		}
		if (this.valor.getValue()!=null && this.valor.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("valor", this.valor.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("valor", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarParametrosRCA(null);
	}
	
	public void editarParametrosRCA(MapeoParametrosRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoParametrosRCA();		
		fieldGroup.setItemDataSource(new BeanItem<MapeoParametrosRCA>(r_mapeo));
		bodega.focus();
	}
	
	public void eliminarParametrosRCA(MapeoParametrosRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((ParametrosRCAGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoParametrosRCA>) ((ParametrosRCAGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarParametrosRCA(MapeoParametrosRCA r_mapeo, MapeoParametrosRCA r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaParametrosRCAServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((ParametrosRCAGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearParametrosRCA(MapeoParametrosRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaParametrosRCAServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoParametrosRCA>(r_mapeo));
				if (((ParametrosRCAGrid) uw.grid)!=null)
				{
					((ParametrosRCAGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoParametrosRCA> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoParametrosRCA= item.getBean();
            if (this.mapeoParametrosRCA.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.bodega.getValue()==null || this.bodega.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Bodega");
    		return false;
    	}
    	if (this.parametro.getValue()==null || this.parametro.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el parametro");
    		return false;
    	}
    	if (this.valor.getValue()==null || this.valor.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el valor");
    		return false;
    	}
    	return true;
    }

    private void cargarCombo()
    {
    	this.bodega.addItem("Borja");
    	this.bodega.addItem("Pozuelo");
    	this.bodega.addItem("Tabuenca");
    	
    	this.parametro.addItem("codigo");
    	this.parametro.addItem("cuenta");
    	this.parametro.addItem("formaPago");
    	this.parametro.addItem("precio");
    	this.parametro.addItem("base");
    }
}
