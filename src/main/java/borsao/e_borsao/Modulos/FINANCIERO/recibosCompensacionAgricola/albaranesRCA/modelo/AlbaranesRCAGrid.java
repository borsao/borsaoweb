package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.albaranesRCA.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AlbaranesRCAGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public AlbaranesRCAGrid(ArrayList<MapeoAlbaranesRCA> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Albaranes RCA");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoAlbaranesRCA.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("bodega", "ejercicio", "socio", "albaran", "precio");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("bodega", "150");
    	this.widthFiltros.put("ejercicio", "120");
    	this.widthFiltros.put("socio", "120");
    	this.widthFiltros.put("albaran", "120");
    	
    	this.getColumn("bodega").setHeaderCaption("Bodega");
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("socio").setHeaderCaption("Socio");    	
    	this.getColumn("albaran").setHeaderCaption("Albaran");    	
    	this.getColumn("precio").setHeaderCaption("Precio");    	
    	this.getColumn("idCodigo").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("precio");
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
