package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.calidadesUva.modelo.MapeoCalidadesUva;

public class consultaCalidadesUvaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCalidadesUvaServer instance;
	
	public consultaCalidadesUvaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCalidadesUvaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCalidadesUvaServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoCalidadesUva> datosCalidadesUvaGlobal(MapeoCalidadesUva r_mapeo)
	{
		ResultSet rsCalidadesUva = null;		
		ArrayList<MapeoCalidadesUva> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_calidades_uva.id op_id, ");
		cadenaSQL.append(" rca_calidades_uva.codigo op_cod, ");
		cadenaSQL.append(" rca_calidades_uva.descripcion op_des ");
     	cadenaSQL.append(" FROM rca_calidades_uva ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsCalidadesUva= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoCalidadesUva>();
			
			while(rsCalidadesUva.next())
			{
				MapeoCalidadesUva mapeoCalidadesUva = new MapeoCalidadesUva();
				/*
				 * recojo mapeo operarios
				 */
				mapeoCalidadesUva.setIdCodigo(rsCalidadesUva.getInt("op_id"));
				mapeoCalidadesUva.setCodigo(rsCalidadesUva.getString("op_cod"));
				mapeoCalidadesUva.setDescripcion(rsCalidadesUva.getString("op_des"));
				vector.add(mapeoCalidadesUva);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public StringBuffer obtenerCalidadesUvaGlobal(MapeoCalidadesUva r_mapeo, String r_origen)
	{
		ResultSet rsCalidadesUva = null;		
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaObtenida =null;
		String comando = "like";
		String comodin = "%";
		int cuantas = 0;
		
		if (r_origen.contentEquals("GREENsys"))
		{
			comando = "matches";
			comodin = "*";
		}
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT rca_calidades_uva.id op_id, ");
		cadenaSQL.append(" rca_calidades_uva.codigo op_cod, ");
		cadenaSQL.append(" rca_calidades_uva.descripcion op_des ");
     	cadenaSQL.append(" FROM rca_calidades_uva ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + r_mapeo.getCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsCalidadesUva= cs.executeQuery(cadenaSQL.toString());
			
			cadenaObtenida = new StringBuffer();
			cadenaObtenida.append("(");
			
			while(rsCalidadesUva.next())
			{
				/*
				 * recojo mapeo operarios
				 */
//				if (r_origen.contentEquals("Tabuenca") && rsCalidadesUva.getString("op_cod").contentEquals("889"))
//				{
//					// no lo tengo en cuenta y paso al siguiente
//				}
//				else
//				{
					if (cuantas==0)
					{
						cadenaObtenida.append("articulo " + comando + " '" + comodin + rsCalidadesUva.getString("op_cod") + comodin + "' ");
					}
					else
					{
						cadenaObtenida.append("or articulo " + comando + " '" + comodin + rsCalidadesUva.getString("op_cod") + comodin + "' ");
					}
					cuantas += 1;
//				}
				
			}
			cadenaObtenida.append(")");
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cadenaObtenida;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsCalidadesUva = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(rca_calidades_uva.id) op_sig ");
     	cadenaSQL.append(" FROM rca_calidades_uva ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsCalidadesUva= cs.executeQuery(cadenaSQL.toString());
			
			while(rsCalidadesUva.next())
			{
				return rsCalidadesUva.getInt("op_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoCalidadesUva r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO rca_calidades_uva ( ");
    		cadenaSQL.append(" rca_calidades_uva.id, ");
    		cadenaSQL.append(" rca_calidades_uva.codigo, ");
    		cadenaSQL.append(" rca_calidades_uva.descripcion ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getCodigo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCodigo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoCalidadesUva r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE rca_calidades_uva set ");
			cadenaSQL.append(" rca_calidades_uva.codigo=?, ");
		    cadenaSQL.append(" rca_calidades_uva.descripcion=? ");
		    cadenaSQL.append(" WHERE rca_calidades_uva.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getCodigo());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoCalidadesUva r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM rca_calidades_uva ");            
			cadenaSQL.append(" WHERE rca_calidades_uva.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}