package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.parametrosRCA.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoParametrosRCA extends MapeoGlobal
{
	private String valor;
	private String bodega;
	private String parametro;

	public MapeoParametrosRCA()
	{
		this.setBodega("");
		this.setValor("");
		this.setParametro("");
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getBodega() {
		return bodega;
	}

	public void setBodega(String bodega) {
		this.bodega = bodega;
	}

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}



}