package borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo.EquivalenciasRCAGrid;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo.MapeoEquivalenciasRCA;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.recibosCompensacionAgricola.equivalenciasRCA.server.consultaEquivalenciasRCAServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoEquivalenciasRCA mapeoEquivalenciasRCA = null;
	 
	 private consultaEquivalenciasRCAServer cus = null;	 
	 private EquivalenciasRCAView uw = null;
	 private BeanFieldGroup<MapeoEquivalenciasRCA> fieldGroup;
	 
	 public OpcionesForm(EquivalenciasRCAView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        this.cargarCombo();
        
        cus=consultaEquivalenciasRCAServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoEquivalenciasRCA>(MapeoEquivalenciasRCA.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoEquivalenciasRCA = (MapeoEquivalenciasRCA) uw.grid.getSelectedRow();
                eliminarEquivalenciasRCA(mapeoEquivalenciasRCA);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoEquivalenciasRCA = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearEquivalenciasRCA(mapeoEquivalenciasRCA);
	 			}
	 			else
	 			{
	 				MapeoEquivalenciasRCA mapeoEquivalenciasRCA_orig = (MapeoEquivalenciasRCA) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoEquivalenciasRCA= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarEquivalenciasRCA(mapeoEquivalenciasRCA,mapeoEquivalenciasRCA_orig);
	 				hm=null;
	 			}
	 			if (((EquivalenciasRCAGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarEquivalenciasRCA(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.bodega.getValue()!=null && this.bodega.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("bodega", this.bodega.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("bodega", "");
		}
		
		if (this.concepto.getValue()!=null && this.concepto.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("concepto", this.concepto.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("concepto", "");
		}
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.destino.getValue()!=null && this.destino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destino", this.destino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destino", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarEquivalenciasRCA(null);
	}
	
	public void editarEquivalenciasRCA(MapeoEquivalenciasRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoEquivalenciasRCA();		
		fieldGroup.setItemDataSource(new BeanItem<MapeoEquivalenciasRCA>(r_mapeo));
		bodega.focus();
	}
	
	public void eliminarEquivalenciasRCA(MapeoEquivalenciasRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((EquivalenciasRCAGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoEquivalenciasRCA>) ((EquivalenciasRCAGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarEquivalenciasRCA(MapeoEquivalenciasRCA r_mapeo, MapeoEquivalenciasRCA r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaEquivalenciasRCAServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((EquivalenciasRCAGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearEquivalenciasRCA(MapeoEquivalenciasRCA r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaEquivalenciasRCAServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			r_mapeo.setIdCodigo(cus.obtenerSiguiente());
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoEquivalenciasRCA>(r_mapeo));
				if (((EquivalenciasRCAGrid) uw.grid)!=null)
				{
					((EquivalenciasRCAGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoEquivalenciasRCA> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoEquivalenciasRCA= item.getBean();
            if (this.mapeoEquivalenciasRCA.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.bodega.getValue()==null || this.bodega.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la Bodega");
    		return false;
    	}
    	if (this.concepto.getValue()==null || this.concepto.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el concepto");
    		return false;
    	}
    	if (this.destino.getValue()==null || this.destino.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el destino");
    		return false;
    	}
    	if (this.origen.getValue()==null || this.origen.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el origen");
    		return false;
    	}
    	return true;
    }

    private void cargarCombo()
    {
    	this.bodega.addItem("Borja");
    	this.bodega.addItem("Pozuelo");
    	this.bodega.addItem("Tabuenca");
    	
    	this.concepto.addItem("iva");
    	this.concepto.addItem("irpf");
    	this.concepto.addItem("regimen");
    	this.concepto.addItem("modalidad");
    }
}
