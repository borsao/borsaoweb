package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.SeguimientoEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class AsignacionSalidasEMCSView extends GridViewRefresh {

	public static final String VIEW_NAME = "Asignacion Salidas EMCS";
	public consultaSeguimientoEMCSServer cus =null;
	public boolean modificando = false;
	public String tituloGrid = "Salida de ";
	public DateField fecha = null;
	public boolean conTotales = false;
	public MapeoSeguimientoEMCS mapeoSeguimientoEMCSEnlazado=null;
	public ComboBox cmbCaeDestino = null;	
	public ComboBox cmbEstado = null;
	public ComboBox cmbArea = null;
	public String almacenSeleccionado=null;
	public String numeroAlmacen=null;
	public TextField txtEjercicio = null;
	
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private OpcionesForm form = null;
	private pantallaAsignacionDatosEMCSAlbaran vt = null;
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public AsignacionSalidasEMCSView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
    	this.cus = new consultaSeguimientoEMCSServer(CurrentUser.get());
    	
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		this.txtEjercicio = new TextField("Ejercicio");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
				
		this.cmbArea= new ComboBox("Tipo");    		
		this.cmbArea.setNewItemsAllowed(false);
		this.cmbArea.setNullSelectionAllowed(false);
		this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cmbEstado= new ComboBox("Area");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cmbCaeDestino= new ComboBox("Origen");    		
		this.cmbCaeDestino.setNewItemsAllowed(false);
		this.cmbCaeDestino.setNullSelectionAllowed(false);
		this.cmbCaeDestino.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.fecha = new DateField();
		this.fecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.fecha.setValue(new Date());
		this.fecha.setDateFormat("dd/MM/yyyy");
		this.fecha.setShowISOWeekNumbers(true);
		
		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.cmbEstado);
		this.cabLayout.addComponent(this.cmbArea);
		this.cabLayout.addComponent(this.cmbCaeDestino);
		this.cabLayout.addComponent(this.fecha);
		this.cabLayout.setComponentAlignment(this.fecha, Alignment.BOTTOM_LEFT);
		
		this.cargarCombo();
		this.cargarListeners();
		this.establecerModo();
		
		opcionesEscogidas.put("ejercicio", this.txtEjercicio.getValue());
		opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		opcionesEscogidas.put("area", this.cmbArea.getValue().toString());
		
		lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSeguimientoEMCS> r_vector=null;
    	MapeoSeguimientoEMCS mapeoSeguimientoEMCS =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoSeguimientoEMCS=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	mapeoSeguimientoEMCS.setOrigen(this.cmbCaeDestino.getValue().toString());
    	
    	r_vector=this.cus.datosAsignacionSalidasEMCSGlobal(mapeoSeguimientoEMCS);
    	
    	this.presentarGrid(r_vector);
    }
    
    public void generarGrid(MapeoSeguimientoEMCS r_mapeo)
    {
    	ArrayList<MapeoSeguimientoEMCS> r_vector=null;
    	r_vector=this.cus.datosAsignacionSalidasEMCSGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
		
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(!this.modificando);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
    	if (this.grid==null || ((SeguimientoEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((SeguimientoEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	MapeoSeguimientoEMCS mapeo = (MapeoSeguimientoEMCS) r_fila;
    	
    	if (!((SeguimientoEMCSGrid) this.grid).activadaVentanaPeticion && !((SeguimientoEMCSGrid) this.grid).ordenando)
    	{
    		this.modificando=false;
    		this.verForm(false);
    		this.form.editarSeguimientoEMCS(mapeo);
    	}    		
    }
    
    public void presentarGrid(ArrayList<MapeoSeguimientoEMCS> r_vector)
    {
    	if (isHayGrid() && grid!=null)
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
		}
    	
    	this.grid = new SeguimientoEMCSGrid(this,r_vector);
    	if (((SeguimientoEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((SeguimientoEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;		
	 				setHayGrid(false);
				}
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("origen", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				
				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setOrigen(cmbCaeDestino.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));

				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Seguimiento en " + almacenSeleccionado;
				
				generarGrid(mapeo);
			}
		});

    	this.fecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;		
	 				setHayGrid(false);
				}
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("origen", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setOrigen(cmbCaeDestino.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));				


				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Seguimiento en " + almacenSeleccionado;
				
				generarGrid(mapeo);
			}
		});
    	
		this.cmbCaeDestino.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setOrigen(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Seguimiento en " + almacenSeleccionado;
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("origen", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setOrigen(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("origen", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbArea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setOrigen(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("origen", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				
				generarGrid(mapeo);
			}
		});
		
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 */
		this.verBotones(true);
		this.activarBotones(true);
		this.navegacion(true);
				
		setSoloConsulta(this.soloConsulta);
		this.opcBuscar.setVisible(false);
		this.opcBuscar.setEnabled(false);	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	public void imprimirSeguimientoEMCS()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
	}
	
	private void cargarCombo()
	{
		this.cmbArea.removeAllItems();
		this.cmbArea.addItem("EMCS Europa");
		this.cmbArea.addItem("EMCS Interno");

		this.cmbCaeDestino.removeAllItems();
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		String almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			if (caes.get(i)!=null && caes.get(i).length()>0) this.cmbCaeDestino.addItem(caes.get(i).trim());
		}

		this.cmbEstado.removeAllItems();		
		this.cmbEstado.addItem("Traslados");
		this.cmbEstado.addItem("Ventas");
		this.cmbEstado.addItem("Todas");
		
		if (eBorsao.get().accessControl.getDepartamento().equals("Almacen"))
		{
			if (caes!=null && caes.size()>1) this.cmbEstado.setValue("Traslados"); else this.cmbEstado.setValue("Ventas");
		}
		else if (eBorsao.get().accessControl.getDepartamento().equals("Comercial"))
		{
			this.cmbEstado.setValue("Ventas");
		}
		else
		{
			this.cmbEstado.setValue("Todas");
		}

		this.cmbArea.setValue("EMCS Interno");
				
		opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
		opcionesEscogidas.put("area", cmbArea.getValue().toString());

		String caeDefecto = cas.obtenerCAEAlmacen(almacenDefecto);
		
		if (caeDefecto!=null)
		{
			
			this.cmbCaeDestino.setValue(caeDefecto.trim());
			this.almacenSeleccionado=cas.obtenerDescripcionAlmacen(almacenDefecto);
			this.numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
			this.tituloGrid = this.tituloGrid + " " + this.almacenSeleccionado; 
			this.opcionesEscogidas.put("origen", this.cmbCaeDestino.getValue().toString());
		}
	}
	
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
		switch (r_accion)
		{
			case "Guardar":
			{
				this.cerrarVentanaBusquedaGuardarNuevo(mapeoSeguimientoEMCSEnlazado);
				break;
			}
			case "Eliminar":
			{
				consultaSeguimientoEMCSServer cus = new consultaSeguimientoEMCSServer(CurrentUser.get());	
				((SeguimientoEMCSGrid) this.grid).remove(mapeoSeguimientoEMCSEnlazado);
				cus.eliminar(mapeoSeguimientoEMCSEnlazado);
				this.form.cerrar();
				break;
			}
		}
	}

	public void cerrarVentanaBusqueda( String r_clave_tabla, String r_documento, String r_serie, Integer r_codigo, String r_arc, Double r_litros)
	{
		MapeoSeguimientoEMCS mapeo_orig = null;
		MapeoSeguimientoEMCS mapeo = (MapeoSeguimientoEMCS) this.grid.getSelectedRow();


		mapeo_orig = this.cus.copiar(mapeo, false);
		mapeo_orig.setClaveTabla(mapeo.getClaveTabla());
		mapeo_orig.setDocumento(mapeo.getDocumento());
		mapeo_orig.setSerie(mapeo.getSerie());
		mapeo_orig.setAlbaran(mapeo.getAlbaran());
		mapeo_orig.setArc(mapeo.getArc());	
		mapeo_orig.setLitros(mapeo.getLitros());	
		mapeo_orig.setDestinatario(mapeo.getDestinatario());
		
		mapeo.setClaveTabla(r_clave_tabla);
		mapeo.setDocumento(r_documento);
		mapeo.setSerie(r_serie);
		mapeo.setAlbaran(r_codigo);
		mapeo.setArc(r_arc);
		mapeo.setLitros(r_litros);
		
		this.cus.guardarCambios(mapeo,mapeo_orig);
	}

	public void cerrarVentanaBusquedaGuardarNuevo(MapeoSeguimientoEMCS r_mapeo)
	{
		String rdo = null;
		Integer id = this.cus.obtenerSiguiente();
		r_mapeo.setIdCodigo(id);
		if (r_mapeo!=null) rdo = this.cus.guardarNuevo(r_mapeo);
		
		if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		
		if (this.vt.isVisible()) this.vt.close();
	}
	
	public void cerrarVentanaMarcar()
	{
		MapeoSeguimientoEMCS mapeo = (MapeoSeguimientoEMCS) this.grid.getSelectedRow();
		
		if (mapeo!=null) this.cus.marcarAlbaranGreensys(mapeo.getEjercicio(), mapeo.getClaveTabla(),mapeo.getDocumento(),mapeo.getSerie(), mapeo.getAlbaran());
		
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		if (this.form!=null) this.form.cerrar();
	}
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	
}


