package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaSeguimientoEMCSServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaSeguimientoEMCSServer instance;
	private final String tableName = "fin_emcs_borsao";
	private final String identificadorTabla = "id";
	
	public consultaSeguimientoEMCSServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSeguimientoEMCSServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSeguimientoEMCSServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoSeguimientoEMCS> datosSeguimientoEMCSGlobal(MapeoSeguimientoEMCS r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;
		ArrayList<MapeoSeguimientoEMCS> vector = null;
		StringBuffer cadenaWhere = null;
		MapeoSeguimientoEMCS mapeoSeguimientoEMCS = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT id seg_id, ");
		cadenaSQL.append(" ejercicio seg_eje,");
		cadenaSQL.append(" area seg_are,");
		cadenaSQL.append(" numeroReferencia seg_num,");
		cadenaSQL.append(" caeExpedidor seg_ori,");
		cadenaSQL.append(" fecha seg_fec,");
		cadenaSQL.append(" destinatario seg_des,");
		cadenaSQL.append(" nif seg_nif,");
		cadenaSQL.append(" exciseNumber seg_cae,");
		cadenaSQL.append(" agenteAduanas seg_adu,");
		cadenaSQL.append(" registeredConsignee seg_reg,");
		cadenaSQL.append(" authorizedWHKeeper seg_aut,");
		cadenaSQL.append(" taxWarehouse seg_tax,");
		cadenaSQL.append(" transporte seg_tte,");
		cadenaSQL.append(" cliente seg_cli,");
		cadenaSQL.append(" codigo seg_cod,");
		cadenaSQL.append(" arc seg_arc,");
		cadenaSQL.append(" clave_tabla seg_ct,");
		cadenaSQL.append(" documento seg_doc,");
		cadenaSQL.append(" serie seg_ser,");
		cadenaSQL.append(" albaran seg_alb,");
		cadenaSQL.append(" litros seg_ltr,");
		cadenaSQL.append(" observaciones seg_obs ");
		
		cadenaSQL.append(" from " + this.tableName + " ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0 && !r_mapeo.getOrigen().equals("Todos"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getNumeroReferencia()!=null && r_mapeo.getNumeroReferencia()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numeroReferencia = " + r_mapeo.getNumeroReferencia() + " ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getDestinatario()!=null && r_mapeo.getDestinatario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destinatario = '" + r_mapeo.getDestinatario() + "' ");
				}
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nif = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getExciseNumber()!=null && r_mapeo.getExciseNumber().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" exciseNumber = '" + r_mapeo.getExciseNumber() + "' ");
				}
				if (r_mapeo.getAgenteAduanas()!=null && r_mapeo.getAgenteAduanas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" agenteAduanas = '" + r_mapeo.getAgenteAduanas() + "' ");
				}
				if (r_mapeo.getRegistered()!=null && r_mapeo.getRegistered().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" registeredConsignee = '" + r_mapeo.getRegistered() + "' ");
				}
				if (r_mapeo.getTaxwarehouse()!=null && r_mapeo.getTaxwarehouse().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" taxWarehouse = '" + r_mapeo.getTaxwarehouse() + "' ");
				}
				if (r_mapeo.getAuthorized()!=null && r_mapeo.getAuthorized().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" authorizedWHKeeper = '" + r_mapeo.getAuthorized() + "' ");
				}
				if (r_mapeo.getNombreTransporte()!=null && r_mapeo.getNombreTransporte().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" transporte = '" + r_mapeo.getNombreTransporte() + "' ");
				}
				if (r_mapeo.getNombreCliente()!=null && r_mapeo.getNombreCliente().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cliente = '" + r_mapeo.getNombreCliente() + "' ");
				}
				if (r_mapeo.getCodigoIdentificacion()!=null && r_mapeo.getCodigoIdentificacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()) + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getEstado().equals("Borrador"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" (arc is null or litros is null or documento is null) ");
				}
				else
				{
					if (r_mapeo.getLitros()!=null && r_mapeo.getLitros()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" litros = " + r_mapeo.getLitros() + " ");
					}
					if (r_mapeo.getArc()!=null && r_mapeo.getArc().toString().length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" arc = '" + r_mapeo.getArc() + "' ");
					}					
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha desc , numeroREferencia desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoSeguimientoEMCS>();
			
			while(rsOpcion.next())
			{
				mapeoSeguimientoEMCS = new MapeoSeguimientoEMCS();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSeguimientoEMCS.setIdCodigo(rsOpcion.getInt("seg_id"));
				mapeoSeguimientoEMCS.setEjercicio(rsOpcion.getInt("seg_eje"));
				mapeoSeguimientoEMCS.setLitros(rsOpcion.getDouble("seg_ltr"));
				
				mapeoSeguimientoEMCS.setCodigoIdentificacion(rsOpcion.getInt("seg_cod"));
				mapeoSeguimientoEMCS.setDescripcionIdentificacion(this.obtenerCodigo(rsOpcion.getString("seg_are"), rsOpcion.getInt("seg_cod")));
				
				mapeoSeguimientoEMCS.setNumeroReferencia(rsOpcion.getLong("seg_num"));				
				mapeoSeguimientoEMCS.setArea(rsOpcion.getString("seg_are"));
				mapeoSeguimientoEMCS.setOrigen(rsOpcion.getString("seg_ori"));
				mapeoSeguimientoEMCS.setDestinatario(rsOpcion.getString("seg_des"));
				mapeoSeguimientoEMCS.setNombreCliente(rsOpcion.getString("seg_cli"));
				mapeoSeguimientoEMCS.setAgenteAduanas(rsOpcion.getString("seg_adu"));
				mapeoSeguimientoEMCS.setExciseNumber(rsOpcion.getString("seg_cae"));
				mapeoSeguimientoEMCS.setRegistered(rsOpcion.getString("seg_reg"));
				mapeoSeguimientoEMCS.setAuthorized(rsOpcion.getString("seg_aut"));
				mapeoSeguimientoEMCS.setNombreTransporte(rsOpcion.getString("seg_tte"));
				mapeoSeguimientoEMCS.setTaxwarehouse(rsOpcion.getString("seg_tax"));
				mapeoSeguimientoEMCS.setArc(rsOpcion.getString("seg_arc"));
				mapeoSeguimientoEMCS.setNif(rsOpcion.getString("seg_nif"));
				mapeoSeguimientoEMCS.setFecha(RutinasFechas.conversion(rsOpcion.getDate("seg_fec")));
				mapeoSeguimientoEMCS.setObservaciones(rsOpcion.getString("seg_obs"));
				mapeoSeguimientoEMCS.setClaveTabla(rsOpcion.getString("seg_ct"));
				mapeoSeguimientoEMCS.setDocumento(rsOpcion.getString("seg_doc"));
				mapeoSeguimientoEMCS.setSerie(rsOpcion.getString("seg_ser"));
				mapeoSeguimientoEMCS.setAlbaran(rsOpcion.getInt("seg_alb"));
				
				vector.add(mapeoSeguimientoEMCS);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public MapeoSeguimientoEMCS datosSeguimientoEMCS(MapeoSeguimientoEMCS r_mapeo)
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		MapeoSeguimientoEMCS mapeoSeguimientoEMCS = null;
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT id seg_id, ");
		cadenaSQL.append(" ejercicio seg_eje,");
		cadenaSQL.append(" area seg_are,");
		cadenaSQL.append(" numeroReferencia seg_num,");
		cadenaSQL.append(" caeExpedidor seg_ori,");
		cadenaSQL.append(" fecha seg_fec,");
		cadenaSQL.append(" destinatario seg_des,");
		cadenaSQL.append(" nif seg_nif,");
		cadenaSQL.append(" exciseNumber seg_cae,");
		cadenaSQL.append(" agenteAduanas seg_adu,");
		cadenaSQL.append(" registeredConsignee seg_reg,");
		cadenaSQL.append(" authorizedWHKeeper seg_aut,");
		cadenaSQL.append(" taxWarehouse seg_tax,");
		cadenaSQL.append(" transporte seg_tte,");
		cadenaSQL.append(" cliente seg_cli,");
		cadenaSQL.append(" codigo seg_cod,");
		cadenaSQL.append(" arc seg_arc,");
		cadenaSQL.append(" clave_tabla seg_ct,");
		cadenaSQL.append(" documento seg_doc,");
		cadenaSQL.append(" serie seg_ser,");
		cadenaSQL.append(" albaran seg_alb,");
		cadenaSQL.append(" litros seg_ltr,");
		cadenaSQL.append(" observaciones seg_obs ");
		
		cadenaSQL.append(" from " + this.tableName + " ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0 && !r_mapeo.getOrigen().equals("Todos"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getNumeroReferencia()!=null && r_mapeo.getNumeroReferencia()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numeroReferencia = " + r_mapeo.getNumeroReferencia() + " ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getDestinatario()!=null && r_mapeo.getDestinatario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destinatario = '" + r_mapeo.getDestinatario() + "' ");
				}
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nif = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getExciseNumber()!=null && r_mapeo.getExciseNumber().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" exciseNumber = '" + r_mapeo.getExciseNumber() + "' ");
				}
				if (r_mapeo.getAgenteAduanas()!=null && r_mapeo.getAgenteAduanas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" agenteAduanas = '" + r_mapeo.getAgenteAduanas() + "' ");
				}
				if (r_mapeo.getRegistered()!=null && r_mapeo.getRegistered().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" registeredConsignee = '" + r_mapeo.getRegistered() + "' ");
				}
				if (r_mapeo.getTaxwarehouse()!=null && r_mapeo.getTaxwarehouse().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" taxWarehouse = '" + r_mapeo.getTaxwarehouse() + "' ");
				}
				if (r_mapeo.getAuthorized()!=null && r_mapeo.getAuthorized().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" authorizedWHKeeper = '" + r_mapeo.getAuthorized() + "' ");
				}
				if (r_mapeo.getNombreTransporte()!=null && r_mapeo.getNombreTransporte().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" transporte = '" + r_mapeo.getNombreTransporte() + "' ");
				}
				if (r_mapeo.getNombreCliente()!=null && r_mapeo.getNombreCliente().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cliente = '" + r_mapeo.getNombreCliente() + "' ");
				}
				if (r_mapeo.getCodigoIdentificacion()!=null && r_mapeo.getCodigoIdentificacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()) + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" clave_tabla = '" + r_mapeo.getClaveTabla() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" albaran = " + r_mapeo.getAlbaran());
				}

				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Borrador"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" (arc is null or litros is null or documento is null) ");
				}
				else
				{
					if (r_mapeo.getLitros()!=null && r_mapeo.getLitros()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" litros = " + r_mapeo.getLitros() + " ");
					}
					if (r_mapeo.getArc()!=null && r_mapeo.getArc().toString().length()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" arc = '" + r_mapeo.getArc() + "' ");
					}					
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha desc , numeroREferencia desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
//			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoSeguimientoEMCS = new MapeoSeguimientoEMCS();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSeguimientoEMCS.setIdCodigo(rsOpcion.getInt("seg_id"));
				mapeoSeguimientoEMCS.setEjercicio(rsOpcion.getInt("seg_eje"));
				mapeoSeguimientoEMCS.setLitros(rsOpcion.getDouble("seg_ltr"));
				
				mapeoSeguimientoEMCS.setCodigoIdentificacion(rsOpcion.getInt("seg_cod"));
				mapeoSeguimientoEMCS.setDescripcionIdentificacion(this.obtenerCodigo(rsOpcion.getString("seg_are"), rsOpcion.getInt("seg_cod")));
				
				mapeoSeguimientoEMCS.setNumeroReferencia(rsOpcion.getLong("seg_num"));				
				mapeoSeguimientoEMCS.setArea(rsOpcion.getString("seg_are"));
				mapeoSeguimientoEMCS.setOrigen(rsOpcion.getString("seg_ori"));
				mapeoSeguimientoEMCS.setDestinatario(rsOpcion.getString("seg_des"));
				mapeoSeguimientoEMCS.setNombreCliente(rsOpcion.getString("seg_cli"));
				mapeoSeguimientoEMCS.setAgenteAduanas(rsOpcion.getString("seg_adu"));
				mapeoSeguimientoEMCS.setExciseNumber(rsOpcion.getString("seg_cae"));
				mapeoSeguimientoEMCS.setRegistered(rsOpcion.getString("seg_reg"));
				mapeoSeguimientoEMCS.setAuthorized(rsOpcion.getString("seg_aut"));
				mapeoSeguimientoEMCS.setNombreTransporte(rsOpcion.getString("seg_tte"));
				mapeoSeguimientoEMCS.setTaxwarehouse(rsOpcion.getString("seg_tax"));
				mapeoSeguimientoEMCS.setArc(rsOpcion.getString("seg_arc"));
				mapeoSeguimientoEMCS.setNif(rsOpcion.getString("seg_nif"));
				mapeoSeguimientoEMCS.setFecha(RutinasFechas.conversion(rsOpcion.getDate("seg_fec")));
				mapeoSeguimientoEMCS.setObservaciones(rsOpcion.getString("seg_obs"));
				mapeoSeguimientoEMCS.setClaveTabla(rsOpcion.getString("seg_ct"));
				mapeoSeguimientoEMCS.setDocumento(rsOpcion.getString("seg_doc"));
				mapeoSeguimientoEMCS.setSerie(rsOpcion.getString("seg_ser"));
				mapeoSeguimientoEMCS.setAlbaran(rsOpcion.getInt("seg_alb"));
				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeoSeguimientoEMCS;
	}
	
	public ArrayList<MapeoSeguimientoEMCS> datosAsignacionSalidasEMCSGlobal(MapeoSeguimientoEMCS r_mapeo)
	{
		Integer almacenInt = null;
		ArrayList<MapeoSeguimientoEMCS> vector = null;
		ArrayList<MapeoLineasAlbaranesVentas> vectorVentas = null;
		ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslado = null;
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
    	String almacen = cas.obtenerAlmacenPorCAE(r_mapeo.getOrigen());
    	if(almacen!=null) almacenInt = new Integer(almacen);
    	
    	vector = new ArrayList<MapeoSeguimientoEMCS>();
    	
    	if (r_mapeo.getEstado().equals("Ventas") || r_mapeo.getEstado().equals("Todas") )
    	{
			vectorVentas = this.obtenerDocumentosVentas(r_mapeo.getEjercicio(), r_mapeo.getArea(), null, r_mapeo.getFecha(), almacenInt);
			
			if (vectorVentas!=null && !vectorVentas.isEmpty())
			{
				for (int i = 0; i< vectorVentas.size();i++)
				{
					MapeoLineasAlbaranesVentas mapeoAlbaran = vectorVentas.get(i);			
					MapeoSeguimientoEMCS mapeoSeguimientoEMCS = new MapeoSeguimientoEMCS();
					
					mapeoSeguimientoEMCS.setArea(r_mapeo.getArea());
					mapeoSeguimientoEMCS.setOrigen(r_mapeo.getOrigen());
					mapeoSeguimientoEMCS.setEjercicio(mapeoAlbaran.getEjercicio());			
					mapeoSeguimientoEMCS.setFecha(RutinasFechas.conversionDeString(mapeoAlbaran.getFechaDocumento()));
					
					mapeoSeguimientoEMCS.setClaveTabla(mapeoAlbaran.getClaveTabla());
					mapeoSeguimientoEMCS.setDocumento(mapeoAlbaran.getDocumento());
					mapeoSeguimientoEMCS.setSerie(mapeoAlbaran.getSerie());
					mapeoSeguimientoEMCS.setAlbaran(mapeoAlbaran.getIdCodigoVenta());
					
					mapeoSeguimientoEMCS.setNif(mapeoAlbaran.getNif());
					mapeoSeguimientoEMCS.setDestinatario(mapeoAlbaran.getNombreCliente());
					
					mapeoSeguimientoEMCS.setTipo("PT");  
					
					if (mapeoAlbaran.getUnidades()!=null) mapeoSeguimientoEMCS.setLitros(mapeoAlbaran.getUnidades().doubleValue());
					
					mapeoSeguimientoEMCS.setCodigoIdentificacion(1);
					mapeoSeguimientoEMCS.setDescripcionIdentificacion("con CAE");
					
					mapeoSeguimientoEMCS.setNumeroReferencia(null);				
					mapeoSeguimientoEMCS.setExciseNumber(null);
					mapeoSeguimientoEMCS.setArc(null);
					mapeoSeguimientoEMCS.setObservaciones(null);
					
					vector.add(mapeoSeguimientoEMCS);
				}
			}
    	}
    	
    	if (r_mapeo.getArea().toString().equals("EMCS Interno") && (r_mapeo.getEstado().equals("Traslados") || r_mapeo.getEstado().equals("Todas") ))
    	{
	    	vectorTraslado= this.obtenerDocumentosTraslado(r_mapeo.getEjercicio(), r_mapeo.getFecha(), almacen, null);
	    	if (vectorTraslado!=null && !vectorTraslado.isEmpty())
			{
				for (int i = 0; i< vectorTraslado.size();i++)
				{
					MapeoLineasAlbaranesTraslado mapeoAlbaranTr = vectorTraslado.get(i);
					MapeoSeguimientoEMCS mapeoAsignacionEMCS = new MapeoSeguimientoEMCS();
		
					mapeoAsignacionEMCS.setArea(r_mapeo.getArea());
					mapeoAsignacionEMCS.setEjercicio(mapeoAlbaranTr.getEjercicio());			
					mapeoAsignacionEMCS.setOrigen(r_mapeo.getOrigen());
					mapeoAsignacionEMCS.setDestinatario(mapeoAlbaranTr.getNombreAlmacen());
					mapeoAsignacionEMCS.setExciseNumber(mapeoAlbaranTr.getCae());
					mapeoAsignacionEMCS.setNif(mapeoAlbaranTr.getNif());
					mapeoAsignacionEMCS.setDocumento(mapeoAlbaranTr.getDocumento());
					mapeoAsignacionEMCS.setFecha(RutinasFechas.conversion(mapeoAlbaranTr.getFechaDocumento()));			
					mapeoAsignacionEMCS.setAlbaran(mapeoAlbaranTr.getIdCodigoTraslado());
					mapeoAsignacionEMCS.setTipo(mapeoAlbaranTr.getArticulo());
					mapeoAsignacionEMCS.setClaveTabla("T");
					mapeoAsignacionEMCS.setSerie("t");
					mapeoAsignacionEMCS.setLitros(mapeoAlbaranTr.getUnidades().doubleValue());			
					mapeoAsignacionEMCS.setCodigoIdentificacion(1);
					mapeoAsignacionEMCS.setDescripcionIdentificacion("con CAE");
					mapeoAsignacionEMCS.setNumeroReferencia(null);				
					mapeoAsignacionEMCS.setArc(null);
					mapeoAsignacionEMCS.setObservaciones(null);
					
					vector.add(mapeoAsignacionEMCS);
				}
			}
    	}
//    	if (vector.isEmpty()) vector = null;
		return vector;
	}
	
	public MapeoSeguimientoEMCS copiar(MapeoSeguimientoEMCS r_mapeo, boolean r_nuevo)
	{
		MapeoSeguimientoEMCS mapeoGenerado = new MapeoSeguimientoEMCS();
		Long nr =null;
		/*
		 * obtencion de datos que cambian
		 * 
		 * id interno
		 * ejercicio
		 * fecha
		 * numerador
		 * arc
		 * litros
		 * observaciones
		 * 
		 */
		Date fecha=null;
		Integer ejer = null;
		
		if (r_nuevo)
		{
			fecha = new Date();
			nr = this.obtenerNumerador(new Integer(RutinasFechas.añoActualYYYY()), r_mapeo.getArea(), r_mapeo.getOrigen());
			ejer = new Integer(RutinasFechas.añoActualYYYY());
		}
		else
		{
			ejer = r_mapeo.getEjercicio();
		}
		
		/*
		 * datos del nuevo registro que actualizamos
		 */
		mapeoGenerado.setFecha(fecha);
		mapeoGenerado.setNumeroReferencia(nr);
		mapeoGenerado.setEjercicio(ejer);
		
		mapeoGenerado.setArc(null);
		mapeoGenerado.setObservaciones(null);
		mapeoGenerado.setLitros(new Double(0));
		mapeoGenerado.setAlbaran(0);
		mapeoGenerado.setClaveTabla(null);
		mapeoGenerado.setDocumento(null);
		mapeoGenerado.setSerie(null);
		
		/*
		 * Datos que mantenemos del origen
		 */
		mapeoGenerado.setCodigoIdentificacion(r_mapeo.getCodigoIdentificacion());
		mapeoGenerado.setDescripcionIdentificacion(r_mapeo.getDescripcionIdentificacion());
		mapeoGenerado.setArea(r_mapeo.getArea());
		mapeoGenerado.setOrigen(r_mapeo.getOrigen());
		mapeoGenerado.setDestinatario(r_mapeo.getDestinatario());
		mapeoGenerado.setNombreCliente(r_mapeo.getNombreCliente());
		mapeoGenerado.setAgenteAduanas(r_mapeo.getAgenteAduanas());
		mapeoGenerado.setExciseNumber(r_mapeo.getExciseNumber());
		mapeoGenerado.setRegistered(r_mapeo.getRegistered());
		mapeoGenerado.setAuthorized(r_mapeo.getAuthorized());
		mapeoGenerado.setNombreTransporte(r_mapeo.getNombreTransporte());
		mapeoGenerado.setTaxwarehouse(r_mapeo.getTaxwarehouse());
		mapeoGenerado.setNif(r_mapeo.getNif());
		
		return mapeoGenerado;
	}
	
	public Integer obtenerSiguiente()
	{
		Connection con =null;
		Integer rdo = null;
		try
		{
			con= this.conManager.establecerConexionInd();
			rdo=  obtenerSiguienteCodigoInterno(con, this.tableName, this.identificadorTabla);
		}
		catch (Exception ex)
		{
			rdo=1;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}

	public Long obtenerNumerador(Integer r_ejercicio, String r_area, String r_origen)
	{
		
		consultaContadoresEjercicioServer ccs = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		Long numerador = ccs.recuperarContador(r_ejercicio, r_area, r_origen);
				
		return numerador;
	}

	private boolean comprobarNumerador(Long r_numerador, Integer r_ejercicio, String r_area,String r_origen)
	{
		
		consultaContadoresEjercicioServer ccs = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		boolean numerador = ccs.comprobarContador(r_numerador, r_ejercicio, r_area, r_origen);
				
		return numerador;
	}

	private void actualizarNumerador(Long r_numerador, Integer r_ejercicio, String r_area, String r_origen)
	{
		
		consultaContadoresEjercicioServer ccs = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
		ccs.actualizarContador(r_ejercicio, new Double(r_numerador),  r_area, r_origen);
				
	}
	
	public String guardarNuevo(MapeoSeguimientoEMCS r_mapeo)
	{
		boolean actualizarGreensys = false;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con=null;
		try
		{
			
			cadenaSQL.append(" INSERT INTO " + this.tableName + "( ");
			cadenaSQL.append("id,");			
			cadenaSQL.append("ejercicio, ");			
			cadenaSQL.append("area, ");
			cadenaSQL.append("numeroReferencia, ");
			cadenaSQL.append("fecha, ");
			cadenaSQL.append("caeExpedidor, ");
			cadenaSQL.append("destinatario, ");
			cadenaSQL.append("nif, ");
			cadenaSQL.append("exciseNumber, ");
			cadenaSQL.append("agenteAduanas, ");
			
			cadenaSQL.append("registeredConsignee,");
			cadenaSQL.append("authorizedWHKeeper,");
			cadenaSQL.append("taxWarehouse,");
			cadenaSQL.append("transporte,");
			
			cadenaSQL.append("cliente,");
			cadenaSQL.append("codigo,");
			
			cadenaSQL.append("arc,");
			cadenaSQL.append("clave_tabla,");
			cadenaSQL.append("documento,");
			cadenaSQL.append("serie,");
			cadenaSQL.append("albaran,");			
			cadenaSQL.append("litros,");
			
			cadenaSQL.append("observaciones ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
//			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    String fecha = "";
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getNumeroReferencia()!=null)
		    {
		    	preparedStatement.setLong(4, r_mapeo.getNumeroReferencia());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(fecha));
		    
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }		    
		    if (r_mapeo.getDestinatario()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getDestinatario());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getExciseNumber()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getExciseNumber());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getAgenteAduanas()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getAgenteAduanas());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getRegistered()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getRegistered());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getAuthorized()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getAuthorized());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getTaxwarehouse()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getTaxwarehouse());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getNombreTransporte()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getNombreTransporte());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getNombreCliente()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getNombreCliente());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getDescripcionIdentificacion()!=null)
		    {
		    	r_mapeo.setCodigoIdentificacion(this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()));		    	
		    	preparedStatement.setInt(16, this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()));
		    	
//		    	preparedStatement.setInt(16, r_mapeo.getCodigoIdentificacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(16, 0);
		    }
		    if (r_mapeo.getArc()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getArc());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    
		    if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran()>0)
		    {
		    	preparedStatement.setString(18, r_mapeo.getClaveTabla());
		    	preparedStatement.setString(19, r_mapeo.getDocumento());
		    	preparedStatement.setString(20, r_mapeo.getSerie());
		    	preparedStatement.setInt(21, r_mapeo.getAlbaran());
		    	
//		    	serNotif.mensajeAdvertencia("Recuerda activar la actualizacion a greensys");
		    	actualizarGreensys=true;
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    	preparedStatement.setString(19, null);
		    	preparedStatement.setString(20, null);
		    	preparedStatement.setInt(21, 0);
		    }
		    
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(22, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(22, 0);
		    }

		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(23, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(23, null);
		    }
		    
		    if (!this.comprobarNumerador(r_mapeo.getNumeroReferencia(), r_mapeo.getEjercicio(), r_mapeo.getArea(), r_mapeo.getOrigen()))
		    {
		    	this.actualizarNumerador(r_mapeo.getNumeroReferencia(), r_mapeo.getEjercicio(), r_mapeo.getArea(), r_mapeo.getOrigen());		    	
		    	preparedStatement.executeUpdate();
		    	if (actualizarGreensys) this.actualizarGreensys(r_mapeo.getEjercicio(), r_mapeo.getClaveTabla(), r_mapeo.getDocumento(), r_mapeo.getSerie(),r_mapeo.getAlbaran());
		    }
		    else
		    {
		    	return "Numerador";
		    }
	        
		}
	    catch (Exception ex)
	    {
//	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoSeguimientoEMCS r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con=null;
		try
		{
			
			cadenaSQL.append(" UPDATE "+ this.tableName + " set ");
			cadenaSQL.append("fecha = ?, ");
			cadenaSQL.append("caeExpedidor = ?, ");
			cadenaSQL.append("destinatario = ?, ");
			cadenaSQL.append("nif = ?, ");
			cadenaSQL.append("exciseNumber = ?, ");
			cadenaSQL.append("agenteAduanas = ?, ");
			
			cadenaSQL.append("registeredConsignee = ?,");
			cadenaSQL.append("authorizedWHKeeper = ?,");
			cadenaSQL.append("taxWarehouse = ?,");
			cadenaSQL.append("transporte = ?,");
			
			cadenaSQL.append("cliente = ?,");
			cadenaSQL.append("codigo = ?,");
			
			cadenaSQL.append("arc = ?,");
			
			cadenaSQL.append("litros = ?,");
			cadenaSQL.append("observaciones  = ? ");
			cadenaSQL.append(" WHERE " + this.tableName + ".id = ? " );
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    String fecha =null;
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(1, RutinasFechas.convertirAFechaMysql(fecha));
		    
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDestinatario()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDestinatario());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getExciseNumber()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getExciseNumber());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getAgenteAduanas()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getAgenteAduanas());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    
		    if (r_mapeo.getRegistered()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getRegistered());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getAuthorized()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getAuthorized());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getTaxwarehouse()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getTaxwarehouse());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getNombreTransporte()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getNombreTransporte());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }

		    
		    if (r_mapeo.getNombreCliente()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getNombreCliente());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getDescripcionIdentificacion()!=null)
		    {
		    	r_mapeo.setCodigoIdentificacion(this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()));
			    preparedStatement.setInt(12, this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getDescripcionIdentificacion()));
			    
//		    	preparedStatement.setInt(12, r_mapeo.getCodigoIdentificacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getArc()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getArc());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(14, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setDouble(14, 0);
		    }

		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    preparedStatement.setInt(16, r_mapeo.getIdCodigo());

		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	
	
	public String guardarCambios(MapeoSeguimientoEMCS r_mapeo, MapeoSeguimientoEMCS r_mapeo_orig)
	{
		boolean actualizarGreensys = false;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		String rdoGreensys = null;
		Connection con=null;
		try
		{
			
			cadenaSQL.append(" UPDATE "+ this.tableName + " set ");
			
			cadenaSQL.append("arc = ?,");
			cadenaSQL.append("clave_tabla = ?,");
			cadenaSQL.append("documento = ?,");
			cadenaSQL.append("serie = ?,");
			cadenaSQL.append("albaran = ?,");
			cadenaSQL.append("litros = ? ");
			cadenaSQL.append(" WHERE " + this.tableName + ".id = ? " );
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    if (r_mapeo.getArc()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArc());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    
		    if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran()>0)
		    {
		    	preparedStatement.setString(2, r_mapeo.getClaveTabla());
		    	preparedStatement.setString(3, r_mapeo.getDocumento());
		    	preparedStatement.setString(4, r_mapeo.getSerie());
		    	preparedStatement.setInt(5, r_mapeo.getAlbaran());
		    	
//		    	serNotif.mensajeAdvertencia("Seguimiento EMCS server. Recuerda activar la conexion con GREENsys");		    	
		    	actualizarGreensys=true;
		    	
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    	preparedStatement.setString(3, null);
		    	preparedStatement.setString(4, null);
		    	preparedStatement.setInt(5, 0);
		    	if (r_mapeo_orig!=null && r_mapeo_orig.getAlbaran() != 0) rdoGreensys = this.liberarGreensys(r_mapeo_orig.getEjercicio(), r_mapeo_orig.getClaveTabla(), r_mapeo_orig.getDocumento(), r_mapeo_orig.getSerie(), r_mapeo_orig.getAlbaran());
		    }

		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }

		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());

		    if (rdoGreensys==null)
		    	preparedStatement.executeUpdate();
		    else return rdoGreensys;
		    
		    if (actualizarGreensys)
		    {
		    	if (!r_mapeo.getAlbaran().equals(r_mapeo_orig.getAlbaran()))
		    	{
		    		if (r_mapeo_orig.getAlbaran()!=null && r_mapeo_orig.getAlbaran() != 0)  this.liberarGreensys(r_mapeo_orig.getEjercicio(), r_mapeo_orig.getClaveTabla(), r_mapeo_orig.getDocumento(), r_mapeo_orig.getSerie(), r_mapeo_orig.getAlbaran());
		    		this.actualizarGreensys(r_mapeo.getEjercicio(), r_mapeo.getClaveTabla(), r_mapeo.getDocumento(), r_mapeo.getSerie(), r_mapeo.getAlbaran());
		    	}
		    }
		    
		    
		}
		catch (Exception ex)
	    {
	    	serNotif.mensajeSeguimiento("Metodo guardar cambios fin_emcs_borsao");
	    	serNotif.mensajeSeguimiento("mapeo normal");
	    	serNotif.mensajeSeguimiento("Arc " + r_mapeo.getArc() );
	    	serNotif.mensajeSeguimiento("Ejercicio " + String.valueOf(r_mapeo.getEjercicio()) );
	    	serNotif.mensajeSeguimiento("Clave tabla " + r_mapeo.getClaveTabla() );
	    	serNotif.mensajeSeguimiento("Documento " + r_mapeo.getDocumento() );
	    	serNotif.mensajeSeguimiento("Serie " + r_mapeo.getSerie());
	    	serNotif.mensajeSeguimiento("Codigo " + String.valueOf(r_mapeo.getAlbaran()) );
	    	
	    	serNotif.mensajeSeguimiento("mapeo orig");
	    	serNotif.mensajeSeguimiento("Ejercicio " + String.valueOf(r_mapeo_orig.getEjercicio()) );
	    	serNotif.mensajeSeguimiento("Clave tabla " + r_mapeo_orig.getClaveTabla() );
	    	serNotif.mensajeSeguimiento("Documento " + r_mapeo_orig.getDocumento() );
	    	serNotif.mensajeSeguimiento("Serie " + r_mapeo_orig.getSerie());
	    	serNotif.mensajeSeguimiento("Codigo " + String.valueOf(r_mapeo_orig.getAlbaran()) );

			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoSeguimientoEMCS r_mapeo)
	{
		Connection con = null;
		String rdo = null;
		try
		{
			con= this.conManager.establecerConexionInd();	          
			rdo = eliminarRegistro(con, this.tableName, this.identificadorTabla, r_mapeo.getIdCodigo());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	@Override
	public String semaforos() 
	{
		String semaforo= "0";
		
		consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		semaforo = cats.semaforosSalida();
		
		if (semaforo.equals("0"))
		{
			consultaAlbaranesVentasServer cacs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());
			semaforo = cacs.semaforos();
		}
		return semaforo;
	}
	
	private Integer obtenerCodigo(String r_area, String r_codigo)
	{

		switch (r_area)
		{
			case "EMCS Interno":
			{
				switch (r_codigo)
				{
					case "con CAE":
					{
						return 1;
					}
					case "sin CAE":
					{
						return 2;
					}
					case "Exportación":
					{
						return 3;
					}
					case "Desconocido (art 29.B.1. Reglamento IIEE)":
					{
						return 4;
					}
				}
			}
			case "EMCS Europa":
			{
				switch (r_codigo)
				{
					case "Depósifo Fiscal":
					{
						return 1;
					}
					case "Destinatario Registrado":
					{
						return 2;
					}
					case "Destinatario Registrado Ocasional":
					{
						return 3;
					}
					case "Entrega Directa Autorizada":
					{
						return 4;
					}
					case "Destinatario Exento":
					{
						return 5;
					}
					case "Exportación":
					{
						return 6;
					}
					case "Desconocido (art 30.A.8. Reglamento)":
					{
						return 8;
					}
				}
			}
		}
		return null;
	}
	
	private String obtenerCodigo(String r_area, Integer r_codigo)
	{

		switch (r_area)
		{
			case "EMCS Interno":
			{
				switch (r_codigo)
				{
					case 1:
					{
						return "con CAE";
					}
					case 2:
					{
						return "sin CAE";
					}
					case 3:
					{
						return "Exportación";
					}
					case 4:
					{
						return "Desconocido (art 29.B.1. Reglamento IIEE)";
					}
				}
			}
			case "EMCS Europa":
			{
				switch (r_codigo)
				{
					case 1:
					{
						return "Depósifo Fiscal";
					}
					case 2:
					{
						return "Destinatario Registrado";
					}
					case 3:
					{
						return "Destinatario Registrado Ocasional";
					}
					case 4:
					{
						return "Entrega Directa Autorizada";
					}
					case 5:
					{
						return "Destinatario Exento";
					}
					case 6:
					{
						return "Exportación";
					}
					case 8:
					{
						return "Desconocido (art 30.A.8. Reglamento)";
					}
				}
			}
		}
		return null;
	}
	
	private void actualizarGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				cats.marcarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);
				break;
			}
			default:
			{
				consultaAlbaranesVentasServer cacs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());
				cacs.marcarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);			
				break;
			}
		}
			
	}

	public void marcarAlbaranGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				cats.marcarAlbaranSilicie(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);
				break;
			}
			default:
			{
				consultaAlbaranesVentasServer cacs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());				
				cacs.marcarAlbaranSilicie(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);			
				break;
			}
		}
			
	}

	private String liberarGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		String rdo = null;
		Boolean actualizado = false;
		
		switch (r_clave_tabla)
		{
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				actualizado = cats.liberarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);				
				break;
			}
			case "T":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				String marcada = cats.estadoCabeceraAlbaranEMCS(r_ejercicio, r_documento, r_serie, r_albaran);
				
				if (!marcada.equals("S"))
				{
					actualizado = cats.liberarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);				
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Hay que liberar la recepcion del traslado primero");
					rdo = "error al liberar el traslado";
				}
				break;
			}
			default:
			{
				consultaAlbaranesVentasServer cacs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());
				actualizado = cacs.liberarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);
				break;
			}
		}
		if (!actualizado && rdo==null) rdo = "error en greensys";
		if (actualizado) return null; else return rdo; 
	}


	public ArrayList<MapeoLineasAlbaranesVentas> obtenerDocumentosVentas(Integer r_ejercicio, String r_area, String r_nif, Date r_fecha, Integer r_almacen)
	{
		ArrayList<MapeoLineasAlbaranesVentas> vector = null;
		MapeoLineasAlbaranesVentas mapeo = null;
		
		consultaAlbaranesVentasServer cac = new consultaAlbaranesVentasServer(CurrentUser.get());
		
		mapeo=new MapeoLineasAlbaranesVentas();
		
		mapeo.setAlmacen(new Integer(r_almacen));
		mapeo.setFechaDocumento(r_fecha);
		
		vector = cac.datosOpcionesGlobalSinEMCS(r_area, r_ejercicio , "A", null, null, null, r_fecha, r_almacen);
		
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> obtenerDocumentosTraslado(Integer r_ejercicio, Date r_fecha, String r_almacen,String r_almacenOrigen)
	{
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		consultaAlbaranesTrasladoServer cac = new consultaAlbaranesTrasladoServer(CurrentUser.get());
		
		vector = cac.datosOpcionesGlobalSumaSinEMCS(r_ejercicio,"T","T1", null, null,r_fecha,r_almacen, r_almacenOrigen);
		
		return vector;
	}


}
