package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.SeguimientoEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoSeguimientoEMCS mapeo = null;
	 public String litrosNuevo = null;
	 
	 private GridViewRefresh uw = null;
	 public MapeoSeguimientoEMCS mapeoModificado = null;
	 public BeanFieldGroup<MapeoSeguimientoEMCS> fieldGroup;
	 
	 public OpcionesForm(GridViewRefresh r_uw) {
		super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoSeguimientoEMCS>(MapeoSeguimientoEMCS.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fecha.addStyleName("v-datefield-textfield");
        this.fecha.setDateFormat("dd/MM/yyyy");
        this.fecha.setShowISOWeekNumbers(true);
        this.fecha.setResolution(Resolution.DAY);
        
        this.litros.setLocale(new Locale("es_ES"));

    }   

	private void cargarValidators()
	{
		this.litros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (litros.getValue()!=null)
				{
					litrosNuevo = litros.getValue().toString().replace(".", ",");	
				}
			}
		});
		this.arc.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (arc.getValue()!=null && arc.getValue().length()>0 && arc.getValue().length()!=21)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. 21 Posiciones");					
					arc.focus();	
				}
			}
		});
		
		this.numeroReferencia.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (numeroReferencia.getValue()!=null && numeroReferencia.getValue().length()>0 && numeroReferencia.getValue().replace(".", "").length()!=11)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. 11 Posiciones");
					numeroReferencia.focus();	
				}
			}
		});
		
		this.authorized.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (authorized.getValue()!=null && authorized.getValue().length()>0 && authorized.getValue().replace(".", "").length()>13)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. Máx. 13 Posiciones");
					authorized.focus();	
				}
			}
		});
		this.exciseNumber.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (exciseNumber.getValue()!=null && exciseNumber.getValue().length()>0 && exciseNumber.getValue().replace(".", "").length()>13)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. Máx. 13 Posiciones");
					exciseNumber.focus();	
				}
			}
		});

	}
	
	private void cargarListeners()
	{
		descripcionIdentificacion.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (area.getValue().toString().contains("Europa"))
				{
					switch (descripcionIdentificacion.getValue().toString())
					{
						case "Depósifo Fiscal":
						{
							nif.setVisible(false);
							exciseNumber.setVisible(false);
							agenteAduanas.setVisible(false);
							authorized.setVisible(true);
							registered.setVisible(false);
							taxwarehouse.setVisible(true);
							nombreTransporte.setVisible(true);
							break;
						}
						case "Destinatario Registrado":
						{
							nif.setVisible(false);
							exciseNumber.setVisible(false);
							agenteAduanas.setVisible(false);
							authorized.setVisible(false);
							registered.setVisible(true);
							taxwarehouse.setVisible(false);
							nombreTransporte.setVisible(true);
							break;
						}
						case "Exportación":
						{
							nif.setVisible(true);						
							exciseNumber.setVisible(true);
							exciseNumber.setCaption("Aduana Salida");
							agenteAduanas.setVisible(true);
							authorized.setVisible(false);
							registered.setVisible(false);
							taxwarehouse.setVisible(false);
							nombreTransporte.setVisible(true);
							break;
						}
						default:
						{
							nif.setVisible(true);			
							exciseNumber.setVisible(true);
							exciseNumber.setCaption("Aduana Salida");
							agenteAduanas.setVisible(true);
							authorized.setVisible(true);
							registered.setVisible(true);
							taxwarehouse.setVisible(true);
							nombreTransporte.setVisible(true);
							break;
						}
					}
				}
				else if (area.getValue().toString().contains("Interno"))
				{
					authorized.setVisible(false);
					registered.setVisible(false);
					taxwarehouse.setVisible(false);
					nombreTransporte.setVisible(false);
					nif.setVisible(true);
					exciseNumber.setVisible(true);
					agenteAduanas.setVisible(true);
				}

//				getUI().push();
			}
		});
		
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoSeguimientoEMCS) uw.grid.getSelectedRow();
                eliminarSeguimientoEMCS(mapeo);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				if (uw instanceof SeguimientoEMCSView) 
	 				{
	 					((SeguimientoEMCSView) uw).modificando=false;
	 				}
	 				else
	 				{
	 					((AsignacionSalidasEMCSView) uw).modificando=false;
	 				}

	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				if (uw instanceof SeguimientoEMCSView) 
	 				{
	 					((SeguimientoEMCSView) uw).modificando=false;
	 				}
	 				else
	 				{
	 					((AsignacionSalidasEMCSView) uw).modificando=false;
	 				}

	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				

		 				mapeoModificado = hm.convertirHashAMapeo(opcionesEscogidas);

		 				if (uw instanceof AsignacionSalidasEMCSView)
		 				{
		 					mapeoModificado.setClaveTabla(mapeo.getClaveTabla());
		 					mapeoModificado.setDocumento(mapeo.getDocumento());
		 					mapeoModificado.setSerie(mapeo.getSerie());
		 					mapeoModificado.setAlbaran(mapeo.getAlbaran());
		 					mapeoModificado.setTipo(mapeo.getTipo());		 					
		 				}
		 				if (litros.getValue()!=null) mapeoModificado.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		                crearSeguimientoEMCS(mapeoModificado);
			           	removeStyleName("visible");
//			           	uw.newForm();
	 				}
	 			}
	 			else
	 			{
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoSeguimientoEMCS mapeo_orig= (MapeoSeguimientoEMCS) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				if (litros.getValue()!=null) mapeo.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		 				modificarSeguimientoEMCS(mapeo,mapeo_orig);
		 				hm=null;
			           	removeStyleName("visible");
	 				}
	 			}
	 			if (((SeguimientoEMCSGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 				uw.grid.setEnabled(true);
	 			}
 			}
 		});	
		
		
		this.fecha.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				ejercicio.setValue(RutinasFechas.añoFecha(fecha.getValue()));
				if (isCreacion()) numeroReferencia.setValue(String.valueOf(obtenerContador()));
			}
		});
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarSeguimientoEMCS(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.numeroReferencia.getValue()!=null && this.numeroReferencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroReferencia", this.numeroReferencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroReferencia", "");
		}
		if (this.destinatario.getValue()!=null && this.destinatario.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinatario", this.destinatario.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinatario", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.descripcionIdentificacion.getValue()!=null && this.descripcionIdentificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionIdentificacion", this.descripcionIdentificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionIdentificacion", "");
		}
		if (this.exciseNumber.getValue()!=null && this.exciseNumber.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("exciseNumber", this.exciseNumber.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("exciseNumber", "");
		}
		if (this.agenteAduanas.getValue()!=null && this.agenteAduanas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("agenteAduanas", this.agenteAduanas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("agenteAduanas", "");
		}
		
		
		if (this.registered.getValue()!=null && this.registered.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("registered", this.registered.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("registered", "");
		}
		if (this.authorized.getValue()!=null && this.authorized.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("authorized", this.authorized.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("authorized", "");
		}
		if (this.taxwarehouse.getValue()!=null && this.taxwarehouse.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("taxwarehouse", this.taxwarehouse.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("taxwarehouse", "");
		}
		if (this.nombreTransporte.getValue()!=null && this.nombreTransporte.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombreTransporte", this.nombreTransporte.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombreTransporte", "");
		}
		
		if (this.nombreCliente.getValue()!=null && this.nombreCliente.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombreCliente", this.nombreCliente.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombreCliente", "");
		}
		if (this.arc.getValue()!=null && this.arc.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("arc", this.arc.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("arc", "");
		}
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.numeroReferencia.getValue()!=null && this.numeroReferencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroReferencia", this.numeroReferencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroReferencia", "");
		}
		if (this.destinatario.getValue()!=null && this.destinatario.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinatario", this.destinatario.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinatario", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.descripcionIdentificacion.getValue()!=null && this.descripcionIdentificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionIdentificacion", this.descripcionIdentificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionIdentificacion", "");
		}
		if (this.exciseNumber.getValue()!=null && this.exciseNumber.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("exciseNumber", this.exciseNumber.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("exciseNumber", "");
		}
		if (this.agenteAduanas.getValue()!=null && this.agenteAduanas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("agenteAduanas", this.agenteAduanas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("agenteAduanas", "");
		}
		if (this.nombreCliente.getValue()!=null && this.nombreCliente.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombreCliente", this.nombreCliente.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombreCliente", "");
		}
		if (this.registered.getValue()!=null && this.registered.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("registered", this.registered.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("registered", "");
		}
		if (this.authorized.getValue()!=null && this.authorized.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("authorized", this.authorized.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("authorized", "");
		}
		if (this.taxwarehouse.getValue()!=null && this.taxwarehouse.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("taxwarehouse", this.taxwarehouse.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("taxwarehouse", "");
		}
		if (this.nombreTransporte.getValue()!=null && this.nombreTransporte.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombreTransporte", this.nombreTransporte.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombreTransporte", "");
		}
		
		if (this.arc.getValue()!=null && this.arc.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("arc", this.arc.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("arc", "");
		}
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarSeguimientoEMCS(null);
		}
	}
	
	public void editarSeguimientoEMCS(MapeoSeguimientoEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cargarCombo();
		
		posicionarCampos();
		
		if (!isBusqueda())
		{
			/*
			 * establezco el numerador
			 */
			if (r_mapeo==null) 
			{
				r_mapeo=new MapeoSeguimientoEMCS();			
				r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
				r_mapeo.setFecha(new Date());
				r_mapeo.setArea(this.area.getValue().toString());
				if (isCreacion())
				{
					Long nr = obtenerContador();
					r_mapeo.setNumeroReferencia(nr);
				}
				if (this.origen.getValue()!=null) r_mapeo.setOrigen(this.origen.getValue().toString());
			}
			else
			{
				if (r_mapeo.getClaveTabla()!=null && this.numeroReferencia.getValue()!=null && this.numeroReferencia.getValue().length()>0 && r_mapeo.getNumeroReferencia()==null) r_mapeo.setNumeroReferencia(new Long(RutinasCadenas.quitarPuntoMiles(this.numeroReferencia.getValue())));
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().toUpperCase().equals("T"))
 				{
 					this.destinatario.setValue("Bodegas Borsao, S.A.");
 				}
				
//				this.mapeo=r_mapeo;
			}
		}
		else
		{
			r_mapeo=new MapeoSeguimientoEMCS();
		}
	
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoSeguimientoEMCS>(r_mapeo));
		if (uw instanceof SeguimientoEMCSView) 
		{
			((SeguimientoEMCSView) this.uw).modificando=false;
		}
		else
		{
			((AsignacionSalidasEMCSView) this.uw).modificando=false;
		}

		
		desactivarCampos(r_mapeo.getAlbaran());

	}
	
	public void eliminarSeguimientoEMCS(MapeoSeguimientoEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		if (uw instanceof SeguimientoEMCSView) 
		{
			((SeguimientoEMCSView) this.uw).mapeoSeguimientoEMCSEnlazado=r_mapeo;
		}
		else
		{
			((AsignacionSalidasEMCSView) this.uw).mapeoSeguimientoEMCSEnlazado=r_mapeo;
		}
		
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(uw, "Seguro que deseas eliminar el registro?", "Si", "No", "Eliminar", null);			
		uw.getUI().addWindow(vt);
	}
	
	public void modificarSeguimientoEMCS(MapeoSeguimientoEMCS r_mapeo, MapeoSeguimientoEMCS r_mapeo_orig)
	{
		String rdo = null;
		boolean reasignacion = false;
		
		if (uw instanceof SeguimientoEMCSView)
			if (r_mapeo_orig!=null && r_mapeo_orig.getAlbaran() != null && r_mapeo_orig.getAlbaran()!=0)
			{
				reasignacion=true;
				rdo = ((SeguimientoEMCSView) uw).cus.guardarCambios(r_mapeo,r_mapeo_orig);
			}
			else
				rdo = ((SeguimientoEMCSView) uw).cus.guardarCambios(r_mapeo);
		else
		{
			reasignacion=true;
			rdo = ((AsignacionSalidasEMCSView) uw).cus.guardarCambios(r_mapeo,r_mapeo_orig);
		}
		
		if (rdo==null)
		{
//			r_mapeo.setEstado(rdo);
		
			if (!((SeguimientoEMCSGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector).remove(r_mapeo_orig);
				if (r_mapeo_orig.getOrigen().equals(r_mapeo.getOrigen())) ((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoSeguimientoEMCS> r_vector = (ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector;
				
				HashMap<String,String> filtrosRec = null;
				
				if (uw instanceof SeguimientoEMCSView) 
				{
					filtrosRec = ((SeguimientoEMCSGrid) uw.grid).recogerFiltros();
				}
				
				Indexed indexed = ((SeguimientoEMCSGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            if (uw instanceof SeguimientoEMCSView) 
				{
	            	if (((SeguimientoEMCSView) this.uw).isHayGrid())
	            	{
	            		((SeguimientoEMCSView) this.uw).grid.removeAllColumns();
	            		((SeguimientoEMCSView) this.uw).barAndGridLayout.removeComponent(uw.grid);
	            		((SeguimientoEMCSView) this.uw).grid=null;
	            		((SeguimientoEMCSView) this.uw).setHayGrid(false);
	            	}
					((SeguimientoEMCSView) this.uw).presentarGrid(r_vector);
		            ((SeguimientoEMCSGrid) uw.grid).setRecords((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector);
		            ((SeguimientoEMCSGrid) uw.grid).sort("numeroReferencia", SortDirection.DESCENDING);
		            if (r_mapeo_orig.getOrigen().equals(r_mapeo.getOrigen())) ((SeguimientoEMCSGrid) uw.grid).scrollTo(r_mapeo);
		            if (r_mapeo_orig.getOrigen().equals(r_mapeo.getOrigen())) ((SeguimientoEMCSGrid) uw.grid).select(r_mapeo);
		            
		            ((SeguimientoEMCSGrid) uw.grid).establecerFiltros(filtrosRec);

				}
				else
				{
					
					if (((AsignacionSalidasEMCSView) this.uw).isHayGrid())
	            	{
	            		((AsignacionSalidasEMCSView) this.uw).grid.removeAllColumns();
	            		((AsignacionSalidasEMCSView) this.uw).barAndGridLayout.removeComponent(uw.grid);
	            		((AsignacionSalidasEMCSView) this.uw).grid=null;
	            		((AsignacionSalidasEMCSView) this.uw).setHayGrid(false);
	            	}
					((AsignacionSalidasEMCSView) this.uw).presentarGrid(r_vector);
//		            ((AsignacionSalidasEMCSGrid) uw.grid).setRecords((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector);
//		            ((AsignacionSalidasEMCSGrid) uw.grid).sort("numeroReferencia", SortDirection.DESCENDING);
//					((AsignacionSalidasEMCSGrid) uw.grid).scrollTo(r_mapeo);
//					((AsignacionSalidasEMCSGrid) uw.grid).select(r_mapeo);

				}
			}
			if (reasignacion) Notificaciones.getInstance().mensajeAdvertencia("Tienes que revisar la asociación del albarán o repetirla");
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		if (uw instanceof SeguimientoEMCSView) 
		{
			((SeguimientoEMCSView) this.uw).modificando=false;
		}
		else
		{
			((AsignacionSalidasEMCSView) this.uw).modificando=false;
		}

	}
	
	public void crearSeguimientoEMCS(MapeoSeguimientoEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		String rdo = null;
		MapeoSeguimientoEMCS mapeo_orig = null;
		
		if (((SeguimientoEMCSGrid) uw.grid)!=null && !((SeguimientoEMCSGrid) uw.grid).vector.isEmpty() && !this.isCreacion())
		{
			mapeo_orig= (MapeoSeguimientoEMCS) ((SeguimientoEMCSGrid) uw.grid).getSelectedRow();
			if (mapeo_orig!=null) r_mapeo.setTipo(mapeo_orig.getTipo());
		}
		
		if (uw instanceof SeguimientoEMCSView)
		{
			r_mapeo.setIdCodigo(((SeguimientoEMCSView) uw).cus.obtenerSiguiente());
			rdo = ((SeguimientoEMCSView) uw).cus.guardarNuevo(r_mapeo);
		}
		else
		{
			r_mapeo.setIdCodigo(((AsignacionSalidasEMCSView) uw).cus.obtenerSiguiente());
			rdo = ((AsignacionSalidasEMCSView) uw).cus.guardarNuevo(r_mapeo);
		}

		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoSeguimientoEMCS>(r_mapeo));
			if (((SeguimientoEMCSGrid) uw.grid)!=null )//&& )
			{
				if (!((SeguimientoEMCSGrid) uw.grid).vector.isEmpty() && !this.isCreacion() )
				{
					((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector).remove(mapeo_orig);
					((SeguimientoEMCSGrid) uw.grid).setRecords((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector);
				}
				else if (!((SeguimientoEMCSGrid) uw.grid).vector.isEmpty() && this.isCreacion() )
				{
					((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector).add(r_mapeo);
					((SeguimientoEMCSGrid) uw.grid).setRecords((ArrayList<MapeoSeguimientoEMCS>) ((SeguimientoEMCSGrid) uw.grid).vector);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			if (this.isCreacion()) this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			if (rdo=="Numerador" || rdo.contains("Duplicate"))
			{
				if (uw instanceof SeguimientoEMCSView) 
				{
					((SeguimientoEMCSView) this.uw).mapeoSeguimientoEMCSEnlazado=r_mapeo;
				}
				else
				{
					((AsignacionSalidasEMCSView) this.uw).mapeoSeguimientoEMCSEnlazado=r_mapeo;
				}

				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this.uw, "El numerador está asignado. Pulsa reintentar para obtener otro.", "Obtener Nuevo", "Cancelar", "Reintentar", null);
				uw.getUI().addWindow(vt);
			}
			else
				Notificaciones.getInstance().mensajeError(rdo);
		}
	}

	public void desactivarCampos(Integer r_albaran)
	{
		if (this.origen.getValue()!=null && r_albaran!=null && r_albaran>0)  this.origen.setEnabled(false);
		if (this.area.getValue()!=null) this.area.setEnabled(false);
		
		if (!isBusqueda()) this.numeroReferencia.setEnabled(false);
		
		
		if (this.area.getValue().toString().contains("Europa"))
		{
			this.nif.setVisible(true);			
			this.exciseNumber.setVisible(true);
			this.exciseNumber.setCaption("Aduana Salida");
			this.agenteAduanas.setVisible(true);
			this.authorized.setVisible(true);
			this.registered.setVisible(true);
			this.taxwarehouse.setVisible(true);
			this.nombreTransporte.setVisible(true);
		}
		else if (this.area.getValue().toString().contains("Interno"))
		{
			this.authorized.setVisible(false);
			this.registered.setVisible(false);
			this.taxwarehouse.setVisible(false);
			this.nombreTransporte.setVisible(false);
			this.nif.setVisible(true);
			this.exciseNumber.setVisible(true);
			this.agenteAduanas.setVisible(true);

		}
		if (uw instanceof AsignacionSalidasEMCSView) ((AsignacionSalidasEMCSView) this.uw).grid.setEnabled(false);
	}
	
	public void cerrar()
	{
//		if (isCreacion()) vaciarCombo();

		mapeoModificado=null;

		if (uw instanceof SeguimientoEMCSView) 
		{
			((SeguimientoEMCSView) this.uw).modificando=false;
			
			if (((SeguimientoEMCSView) this.uw).isHayGrid() && ((SeguimientoEMCSView) this.uw).grid!=null)
 			{			
				((SeguimientoEMCSView) this.uw).grid.removeAllColumns();			
				((SeguimientoEMCSView) this.uw).barAndGridLayout.removeComponent(uw.grid);
				((SeguimientoEMCSView) this.uw).grid=null;
				((SeguimientoEMCSView) this.uw).setHayGrid(false);
 			}
			((SeguimientoEMCSView) this.uw).generarGrid(((SeguimientoEMCSView) this.uw).opcionesEscogidas);
		}
		else
		{
			((AsignacionSalidasEMCSView) this.uw).modificando=false;
			
			if (((AsignacionSalidasEMCSView) this.uw).isHayGrid() && ((AsignacionSalidasEMCSView) this.uw).grid!=null)
 			{			
				((AsignacionSalidasEMCSView) this.uw).grid.removeAllColumns();			
				((AsignacionSalidasEMCSView) this.uw).barAndGridLayout.removeComponent(uw.grid);
				((AsignacionSalidasEMCSView) this.uw).grid=null;	
				((AsignacionSalidasEMCSView) this.uw).setHayGrid(false);
 			}
			
			((AsignacionSalidasEMCSView) this.uw).generarGrid(((AsignacionSalidasEMCSView) this.uw).opcionesEscogidas);
			
//			MapeoSeguimientoEMCS mapeo_orig= (MapeoSeguimientoEMCS) uw.grid.getSelectedRow();
		}
		uw.regresarDesdeForm();
		
		removeStyleName("visible");
//		btnGuardar.setCaption("Guardar");
//		btnEliminar.setEnabled(true);
//        setEnabled(false);
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoSeguimientoEMCS> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
        
    }

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.origen.getValue()==null || this.origen.getValue().toString().length()==0)
			{ 
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Expedidor");
				this.origen.focus();
				return false;
			}
			if (this.origen.getValue()!=null && this.exciseNumber.getValue()!=null && this.origen.getValue().toString().equals(this.exciseNumber.getValue()))
			{ 
				Notificaciones.getInstance().mensajeAdvertencia("Origen y destino no pueden ser iguales");
				this.origen.focus();
				return false;
			}
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Ejercicio ");
				this.ejercicio.focus();
				return false;
			}
			if (this.fecha.getValue()==null || this.fecha.getValue().toString().length()==0) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la Fecha ");
				this.fecha.focus();
				return false;
			}
			if (this.destinatario.getValue()==null || this.destinatario.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Destinatario ");
				this.destinatario.focus();
				return false;
			}
			
			if (this.area.getValue().equals("EMCS Interno"))
			{
				switch (this.descripcionIdentificacion.getValue().toString())
				{
					case "con CAE":
					{
						if (this.exciseNumber.getValue()==null || this.exciseNumber.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el CAE ");
							this.exciseNumber.focus();
							return false;
						}
						break;
					}
					case "sin CAE":
					{
						break;
					}
					case "Exportación":
					{
						if (this.agenteAduanas.getValue()==null || this.agenteAduanas.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Agente de Aduanas");
							this.agenteAduanas.focus();
							return false;
						}
						if (this.nombreCliente.getValue()==null || this.nombreCliente.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Nombre del Cliente");
							this.nombreCliente.focus();
							return false;
						}
						break;
					}
				}
			}
			else if (this.area.getValue().equals("EMCS Europa"))
			{
				switch (this.descripcionIdentificacion.getValue().toString())
				{
					case "Depósifo Fiscal":
					{
						if (this.authorized.getValue()==null || this.authorized.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Operador");
							this.authorized.focus();
							return false;
						}
						if (this.taxwarehouse.getValue()==null || this.taxwarehouse.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar TaxWaarehouse");
							this.taxwarehouse.focus();
							return false;
						}
						break;
					}
					case "Destinatario Registrado":
					{
						if (this.registered.getValue()==null || this.registered.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Registered Consignee");
							this.registered.focus();
							return false;
						}
						break;
					}
					case "Exportacion":
					{
						nif.setVisible(true);						
						exciseNumber.setVisible(true);
						agenteAduanas.setVisible(true);
						if (this.nif.getValue()==null || this.nif.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el NIF");
							this.nif.focus();
							return false;
						}
						if (this.exciseNumber.getValue()==null || this.exciseNumber.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la Aduana de Salida");
							this.exciseNumber.focus();
							return false;
						}
						if (this.agenteAduanas.getValue()==null || this.agenteAduanas.getValue().length()==0)
						{
							Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Agente de Aduanas");
							this.agenteAduanas.focus();
							return false;
						}
						break;
					}
					case "Desconocido (art 30.A.8. Reglamento)":
					{
						break;
					}
				}
			}
		}
		
		return true;
	}
	
	private void cargarCombo()
	{
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			this.origen.addItem(caes.get(i).trim());
		}
		
		this.origen.setNewItemsAllowed(false);
		
		this.area.addItem("EMCS Interno");
		this.area.addItem("EMCS Europa");
		this.area.setNewItemsAllowed(false);
	}
	
	private Long obtenerContador()
	{
		consultaSeguimientoEMCSServer cse = new consultaSeguimientoEMCSServer(CurrentUser.get());
		Long nr = cse.obtenerNumerador(new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue().toString())), area.getValue().toString(), origen.getValue().toString());
		this.numeroReferencia.setValue(nr.toString());
		return nr;
	}
	
	private void posicionarCampos()
	{

		if (uw instanceof SeguimientoEMCSView) 
		{
			if (((SeguimientoEMCSGrid) uw.grid)!=null)
 			{
 				uw.setHayGrid(true);
 				uw.grid.setEnabled(false);
 			}

			this.origen.setValue(((SeguimientoEMCSView) uw).cmbOrigen.getValue().toString());
			this.area.setValue(((SeguimientoEMCSView) uw).cmbArea.getValue().toString());
			this.origen.setCaption(((SeguimientoEMCSView) uw).almacenSeleccionado.toUpperCase());
		}
		else
		{
			this.origen.setValue(((AsignacionSalidasEMCSView) uw).cmbCaeDestino.getValue().toString());
			this.area.setValue(((AsignacionSalidasEMCSView) uw).cmbArea.getValue().toString());
			this.origen.setCaption(((AsignacionSalidasEMCSView) uw).almacenSeleccionado.toUpperCase());			
		}

		this.ejercicio.setValue(RutinasFechas.añoActualYYYY());		
		
		if (this.area!=null && this.area.getValue()!=null && this.area.getValue().toString().equals("EMCS Interno"))
		{
			this.descripcionIdentificacion.addItem("con CAE");
			this.descripcionIdentificacion.addItem("sin CAE");
			this.descripcionIdentificacion.addItem("Exportación");
			this.descripcionIdentificacion.addItem("Desconocido (art 29.B.1. Reglamento IIEE)");
		}
		if (this.area!=null && this.area.getValue()!=null && this.area.getValue().toString().equals("EMCS Europa"))
		{
			this.descripcionIdentificacion.addItem("Depósifo Fiscal");
			this.descripcionIdentificacion.addItem("Destinatario Registrado");
			this.descripcionIdentificacion.addItem("Destinatario Registrado Ocasional");
			this.descripcionIdentificacion.addItem("Entrega Directa Autorizada");
			this.descripcionIdentificacion.addItem("Destinatario Exento");
			this.descripcionIdentificacion.addItem("Exportación");
			this.descripcionIdentificacion.addItem("Desconocido (art 30.A.8. Reglamento)");
		}
		this.descripcionIdentificacion.setNewItemsAllowed(false);
	}
	
//	private void vaciarCombo()
//	{
//		this.origen.removeAllItems();
//		this.area.removeAllItems();
//		this.descripcionIdentificacion.removeAllItems();
//
//	}
}
