package borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoDestinatarios extends MapeoGlobal
{
    private Integer idDestinatario;
    private Integer codigo;
    
    private String area;
	private String nombre;
	private String nif;
	private String codigoIdentificador;
	private String numero_identificador;

	public MapeoDestinatarios()
	{
		this.setNombre("");
		this.setCodigo(null);
		this.setArea("");
		this.setNif("");
		this.setNombre("");
		this.setNumero_identificador("");
		this.setCodigoIdentificador("");
	}

	public Integer getIdDestinatario() {
		return idDestinatario;
	}

	public void setIdDestinatario(Integer idDestinatario) {
		this.idDestinatario = idDestinatario;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNumero_identificador() {
		return numero_identificador;
	}

	public void setNumero_identificador(String numero_identificador) {
		this.numero_identificador = numero_identificador;
	}

	public String getCodigoIdentificador() {
		return codigoIdentificador;
	}

	public void setCodigoIdentificador(String codigoIdentificador) {
		this.codigoIdentificador = codigoIdentificador;
	}


}