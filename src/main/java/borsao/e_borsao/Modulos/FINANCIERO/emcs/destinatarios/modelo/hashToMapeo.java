package borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoDestinatarios mapeo = null;
	 
	 public MapeoDestinatarios convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoDestinatarios();
		 
		 if (r_hash.get("idDdestinatario")!=null) this.mapeo.setIdDestinatario(new Integer(r_hash.get("idDestinatiario")));
		 if (r_hash.get("codigo")!=null && r_hash.get("codigo").length() >0 ) this.mapeo.setCodigoIdentificador(r_hash.get("codigo"));
		 
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setNif(r_hash.get("nif"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setNumero_identificador(r_hash.get("numero_identificador"));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoDestinatarios convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoDestinatarios();
		 
		 if (r_hash.get("idDdestinatario")!=null) this.mapeo.setIdDestinatario(new Integer(r_hash.get("idDestinatario")));
		 if (r_hash.get("codigo")!=null && r_hash.get("codigo").length() >0 ) this.mapeo.setCodigoIdentificador(r_hash.get("codigo"));
		 
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setNif(r_hash.get("nif"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 this.mapeo.setNumero_identificador(r_hash.get("numero_identificador"));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoDestinatarios r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdDestinatario()!=null) this.hash.put("idDestinatario", r_mapeo.getIdDestinatario().toString());
		 if (r_mapeo.getCodigoIdentificador()!=null && r_mapeo.getCodigoIdentificador().length() >0 ) this.hash.put("codigo", r_mapeo.getCodigoIdentificador());
		 
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("nombre", r_mapeo.getNombre());
		 this.hash.put("nif", r_mapeo.getNif());
		 this.hash.put("numero_identificador", r_mapeo.getNumero_identificador());
		 return hash;		 
	 }
}