package borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo.MapeoDestinatarios;

public class consultaDestinatariosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaDestinatariosServer instance;
	
	public consultaDestinatariosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaDestinatariosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaDestinatariosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoDestinatarios> datosDestinatariosGlobal(MapeoDestinatarios r_mapeo)
	{
		ResultSet rsDestinatarios = null;		
		ArrayList<MapeoDestinatarios> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT fin_destinatarios_emcs.id des_id, ");
		cadenaSQL.append(" fin_destinatarios_emcs.area des_are, ");
		cadenaSQL.append(" fin_destinatarios_emcs.codigo des_cod, ");
		cadenaSQL.append(" fin_destinatarios_emcs.num_identificacion des_num, ");
		cadenaSQL.append(" fin_destinatarios_emcs.nif des_nif, ");
        cadenaSQL.append(" fin_destinatarios_emcs.nombre des_nom ");
     	cadenaSQL.append(" FROM fin_destinatarios_emcs ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdDestinatario()!=null && r_mapeo.getIdDestinatario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = '" + r_mapeo.getIdDestinatario() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" area = '" + r_mapeo.getArea() + "' ");
					
					if (r_mapeo.getCodigo()!=null && r_mapeo.getCodigo()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" codigo = '" + this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getCodigoIdentificador()) + "' ");
					}
				}
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nif = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getNumero_identificador()!=null && r_mapeo.getNumero_identificador().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" num_identificacion = '" + r_mapeo.getNumero_identificador() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by area,id asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsDestinatarios.TYPE_SCROLL_SENSITIVE,rsDestinatarios.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsDestinatarios= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoDestinatarios>();
			
			while(rsDestinatarios.next())
			{
				MapeoDestinatarios mapeoDestinatarios = new MapeoDestinatarios();
				/*
				 * recojo mapeo operarios
				 */
				mapeoDestinatarios.setIdDestinatario(rsDestinatarios.getInt("des_id"));
				mapeoDestinatarios.setArea(rsDestinatarios.getString("des_are"));
				mapeoDestinatarios.setCodigo(rsDestinatarios.getInt("des_cod"));
				mapeoDestinatarios.setCodigoIdentificador(this.obtenerCodigo(rsDestinatarios.getString("des_are"), rsDestinatarios.getInt("des_cod")));
				mapeoDestinatarios.setNombre(rsDestinatarios.getString("des_nom"));
				mapeoDestinatarios.setNif(rsDestinatarios.getString("des_nif"));
				mapeoDestinatarios.setNumero_identificador(rsDestinatarios.getString("des_num"));
				vector.add(mapeoDestinatarios);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsDestinatarios = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fin_destinatarios_emcs.id) des_sig ");
     	cadenaSQL.append(" FROM fin_destinatarios_emcs ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsDestinatarios.TYPE_SCROLL_SENSITIVE,rsDestinatarios.CONCUR_UPDATABLE);
			rsDestinatarios= cs.executeQuery(cadenaSQL.toString());
			
			while(rsDestinatarios.next())
			{
				return rsDestinatarios.getInt("des_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoDestinatarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO fin_destinatarios_emcs ( ");
    		cadenaSQL.append(" fin_destinatarios_emcs.id, ");
    		cadenaSQL.append(" fin_destinatarios_emcs.area, ");
    		cadenaSQL.append(" fin_destinatarios_emcs.codigo, ");
	        cadenaSQL.append(" fin_destinatarios_emcs.num_identificacion, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.nif, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.nombre ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCodigoIdentificador()!=null)
		    {
		    	r_mapeo.setCodigo(this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getCodigoIdentificador()));		    	
		    	preparedStatement.setInt(3, this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getCodigoIdentificador()));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getNumero_identificador()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getNumero_identificador());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getNombre());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoDestinatarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_destinatarios_emcs set ");
			cadenaSQL.append(" fin_destinatarios_emcs.area=?, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.codigo=?, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.num_identificacion=?, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.nif=?, ");
		    cadenaSQL.append(" fin_destinatarios_emcs.nombre=? ");
		    cadenaSQL.append(" WHERE fin_destinatarios_emcs.id = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    
		    preparedStatement.setString(1, r_mapeo.getArea());
		    r_mapeo.setCodigo(this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getCodigoIdentificador()));
		    preparedStatement.setInt(2, this.obtenerCodigo(r_mapeo.getArea(), r_mapeo.getCodigoIdentificador()));
		    preparedStatement.setString(3, r_mapeo.getNumero_identificador());
		    preparedStatement.setString(4, r_mapeo.getNif());
		    preparedStatement.setString(5, r_mapeo.getNombre());
		    preparedStatement.setInt(6, r_mapeo.getIdDestinatario());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoDestinatarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_destinatarios_emcs ");            
			cadenaSQL.append(" WHERE fin_destinatarios_emcs.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdDestinatario());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	private Integer obtenerCodigo(String r_area, String r_codigo)
	{

		switch (r_area)
		{
			case "EMCS Interno":
			{
				switch (r_codigo)
				{
					case "con CAE":
					{
						return 1;
					}
					case "sin CAE":
					{
						return 2;
					}
					case "Exportación":
					{
						return 3;
					}
					case "Desconocido (art 29.B.1. Reglamento IIEE":
					{
						return 4;
					}
				}
			}
			case "EMCS Europa":
			{
				switch (r_codigo)
				{
					case "Depósifo Fiscal":
					{
						return 1;
					}
					case "Destinatario Registrado":
					{
						return 2;
					}
					case "Destinatario Registrado Ocasional":
					{
						return 3;
					}
					case "Entrega Directa Autorizada":
					{
						return 4;
					}
					case "Exportación":
					{
						return 5;
					}
					case "Desconocido (art 30.A.8. Reglamento ":
					{
						return 6;
					}
				}
			}
		}
		return null;
	}
	
	private String obtenerCodigo(String r_area, Integer r_codigo)
	{

		switch (r_area)
		{
			case "EMCS Interno":
			{
				switch (r_codigo)
				{
					case 1:
					{
						return "con CAE";
					}
					case 2:
					{
						return "sin CAE";
					}
					case 3:
					{
						return "Exportación";
					}
					case 4:
					{
						return "Desconocido (art 29.B.1. Reglamento IIEE";
					}
				}
			}
			case "EMCS Europa":
			{
				switch (r_codigo)
				{
					case 1:
					{
						return "Depósifo Fiscal";
					}
					case 2:
					{
						return "Destinatario Registrado";
					}
					case 3:
					{
						return "Destinatario Registrado Ocasional";
					}
					case 4:
					{
						return "Entrega Directa Autorizada";
					}
					case 5:
					{
						return "Exportación";
					}
					case 6:
					{
						return "Desconocido (art 30.A.8. Reglamento ";
					}
				}
			}
		}
		return null;
	}
}