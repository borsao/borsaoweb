package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoRecepcionEMCS mapeo = null;
	 
	 public MapeoRecepcionEMCS convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoRecepcionEMCS();
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("numeroReferencia")!=null && r_hash.get("numeroReferencia").length()>0) this.mapeo.setNumeroReferencia(new Long(RutinasCadenas.quitarPuntoMiles(r_hash.get("numeroReferencia"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros"))));
		 if (r_hash.get("grado")!=null && r_hash.get("grado").length()>0) this.mapeo.setGrado(new Double(RutinasNumericas.formatearIntegerDeESP(r_hash.get("grado"))));
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
//		 if (r_hash.get("codigoIdentificacion")!=null && r_hash.get("codigoIdentificacion").length() >0 ) this.mapeo.setCodigoIdentificacion(new Integer(r_hash.get("codigoIdentificacion")));
		 
//		 this.mapeo.setDescripcionIdentificacion(r_hash.get("descripcionIdentificacion"));
		 this.mapeo.setDestino(r_hash.get("destino"));
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setCaeDestino(r_hash.get("caeDestino"));
		 this.mapeo.setNif(r_hash.get("nif"));
		 this.mapeo.setDireccion(r_hash.get("direccion"));
		 this.mapeo.setCodigoPostal(r_hash.get("codigoPostal"));
		 
		 this.mapeo.setDestinatario(r_hash.get("destinatario"));
		 this.mapeo.setArc(r_hash.get("arc"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 if (r_hash.get("albaran")!=null && r_hash.get("albaran").length()>0) this.mapeo.setAlbaran(new Integer(r_hash.get("albaran")));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Rececpcion Guias: valor fecha erroneo. " + ex.getMessage());
		 }
		 return mapeo;		 
	 }
	 
	 public MapeoRecepcionEMCS convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoRecepcionEMCS();
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("numeroReferencia")!=null && r_hash.get("numeroReferencia").length()>0) this.mapeo.setNumeroReferencia(new Long(RutinasCadenas.quitarPuntoMiles(r_hash.get("numeroReferencia"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros"))));
		 if (r_hash.get("grado")!=null && r_hash.get("grado").length()>0) this.mapeo.setGrado(new Double(RutinasNumericas.formatearDoubleDeESP(r_hash.get("grado"))));
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
		 if (r_hash.get("codigoIdentificacion")!=null && r_hash.get("codigoIdentificacion").length() >0 ) this.mapeo.setCodigoIdentificacion(new Integer(r_hash.get("codigoIdentificacion")));
		 if (r_hash.get("albaran")!=null && r_hash.get("albaran").length() >0 )
		 {
			 this.mapeo.setAlbaran(new Integer(r_hash.get("albaran")));
		 }		 
		 this.mapeo.setDescripcionIdentificacion(r_hash.get("descripcionIdentificacion"));
		 this.mapeo.setDestino(r_hash.get("destino"));
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setCaeDestino(r_hash.get("caeDestino"));
		 this.mapeo.setNif(r_hash.get("nif"));
		 this.mapeo.setDireccion(r_hash.get("direccion"));
		 this.mapeo.setCodigoPostal(r_hash.get("codigoPostal"));
		 
		 this.mapeo.setDestinatario(r_hash.get("destinatario"));
		 this.mapeo.setArc(r_hash.get("arc"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Rececpcion Guias: valor fecha erroneo. " + ex.getMessage());
		 }
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoRecepcionEMCS r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getFecha()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFecha().toString());
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }
		 this.hash.put("destino", this.mapeo.getDestino());
		 this.hash.put("anada", this.mapeo.getAnada());
		 this.hash.put("origen", this.mapeo.getOrigen());
		 this.hash.put("destinatario", this.mapeo.getDestinatario());
		 

		 this.hash.put("nif", this.mapeo.getNif());
		 this.hash.put("caeDestino", this.mapeo.getCaeDestino());
		 this.hash.put("direccion", this.mapeo.getDireccion());
		 
		 this.hash.put("codigoPostal", this.mapeo.getCodigoPostal());
		 
		 this.hash.put("arc", this.mapeo.getArc());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 
		 this.hash.put("estado", this.mapeo.getEstado());
		 this.hash.put("descripcionIdentificacion", this.mapeo.getDescripcionIdentificacion());
		 
		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("id", String.valueOf(this.mapeo.getIdCodigo()));
		 if (this.mapeo.getCodigoIdentificacion()!=null) this.hash.put("codigoIdentificacion", String.valueOf(this.mapeo.getCodigoIdentificacion()));
		 if (this.mapeo.getNumeroReferencia()!=null) this.hash.put("numeroReferencia", String.valueOf(this.mapeo.getNumeroReferencia()));
		 if (this.mapeo.getLitros()!=null) this.hash.put("litros", String.valueOf(this.mapeo.getLitros()));
		 if (this.mapeo.getGrado()!=null) this.hash.put("grado", String.valueOf(this.mapeo.getGrado()));
		 if (this.mapeo.getAlbaran()!=null)
		 {
			 this.hash.put("albaran", String.valueOf(this.mapeo.getAlbaran()));
			 this.hash.put("documento", "E1");
		 }
		 
		 return hash;		 
	 }
}