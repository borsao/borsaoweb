package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.AyudaLineasTrasladosGrid;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo.AyudaLineasComprasGrid;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo.MapeoLineasAlbaranesCompras;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAsignacionDatosRecepcionEMCSAlbaran extends Window
{
	private RecepcionEMCSView app = null;
	private Button btnBotonCVentana = null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	
	private ComboBox claveTabla = null;
	private ComboBox documento = null;
	
	private TextField numero= null;
	private TextField grado= null;
	private TextField destino= null;
	private TextField anada= null;
	private TextField arc= null;
	private TextField litros= null;
	private DateField fecha = null;
	
	private Integer ejercicio = null;
	private String almacen = null;
	private String almacenDestino=null;
	private Date fechaSeguimiento = null;
	
	public ComboBox cmbOrigen = null;

	private boolean hayGridComponentes= false;
	private VerticalLayout frameCentral = null;	

	private ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslado = null;
	private ArrayList<MapeoLineasAlbaranesCompras> vectorCompras = null;
	
	private HorizontalLayout entradasDatos = null;
	private HorizontalLayout entradasDatosSeleccion = null;
	
	public pantallaAsignacionDatosRecepcionEMCSAlbaran(RecepcionEMCSView r_app, Integer r_ejercicio, String r_titulo, Date r_fecha, String r_almacen, String r_almacenDestino)
	{
		this.app=r_app;
		this.ejercicio=r_ejercicio;
		this.setCaption(r_titulo);
		this.almacen=r_almacen;
		this.almacenDestino = r_almacenDestino;
		this.fechaSeguimiento = r_fecha;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarCombo();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			this.frameCentral.setMargin(true);
			
				entradasDatosSeleccion = new HorizontalLayout();
				entradasDatosSeleccion.setCaption("Seleccion de Datos");
				entradasDatosSeleccion.setMargin(true);
				entradasDatosSeleccion.setWidth("100%");
				
					this.cmbOrigen= new ComboBox("Origen");    		
					this.cmbOrigen.setNewItemsAllowed(false);
					this.cmbOrigen.setNullSelectionAllowed(false);
					this.cmbOrigen.addItem("Externos");
					this.cmbOrigen.addItem("Internos");
					this.cmbOrigen.setValue("Externos");
				
					this.fecha = new DateField("Fecha");
					this.fecha.setEnabled(true);
					this.fecha.setDateFormat("dd/MM/yyyy");
					this.fecha.setValue(this.fechaSeguimiento);
					
					this.claveTabla= new ComboBox("Area");
					this.documento= new ComboBox("Documentos");
				
				entradasDatosSeleccion.addComponent(this.cmbOrigen);
				entradasDatosSeleccion.addComponent(this.claveTabla);
				entradasDatosSeleccion.addComponent(this.documento);
				entradasDatosSeleccion.addComponent(this.fecha);

				entradasDatos = new HorizontalLayout();
				entradasDatos.setCaption("Entrada de Datos");
				entradasDatos.setMargin(true);
				entradasDatos.setWidth("100%");
				
					this.numero=new TextField();
					this.numero.setEnabled(true);
					this.numero.addStyleName("rightAligned");
					this.numero.setCaption("Numero Referencia");
					
					this.arc=new TextField();
					this.arc.setEnabled(true);
					this.arc.addStyleName("rightAligned");
					this.arc.setCaption("ARC");
					this.arc.setRequired(true);

					this.litros=new TextField();
					this.litros.setEnabled(true);
					this.litros.addStyleName("rightAligned");
					this.litros.setCaption("Litros");
					this.litros.setRequired(true);
					
					this.grado=new TextField();
					this.grado.setEnabled(true);
					this.grado.addStyleName("rightAligned");
					this.grado.setCaption("Grado");
					this.grado.setRequired(true);
					
					this.destino=new TextField();
					this.destino.setEnabled(true);
					this.destino.setCaption("Destino");
					this.destino.setRequired(true);

					this.anada=new TextField();
					this.anada.setEnabled(true);
					this.anada.setCaption("Añada");
					this.anada.setRequired(true);
					
				entradasDatos.addComponent(this.arc);
				entradasDatos.addComponent(this.litros);
				entradasDatos.addComponent(this.grado);
				entradasDatos.addComponent(this.destino);
				entradasDatos.addComponent(this.anada);
				entradasDatos.addComponent(this.numero);
				entradasDatos.setComponentAlignment(this.arc, Alignment.MIDDLE_LEFT);
				entradasDatos.setComponentAlignment(this.litros, Alignment.MIDDLE_LEFT);
				entradasDatos.setComponentAlignment(this.grado, Alignment.MIDDLE_LEFT);
				entradasDatos.setComponentAlignment(this.destino, Alignment.MIDDLE_LEFT);
				entradasDatos.setComponentAlignment(this.numero, Alignment.MIDDLE_LEFT);
				
			frameCentral.addComponent(this.entradasDatosSeleccion);
			frameCentral.addComponent(this.entradasDatos);
				
			HorizontalLayout framePie = new HorizontalLayout();
		
				btnBotonCVentana = new Button("Seleccionar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.cmbOrigen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event)  
			{
				cargarCombo();
				if (cmbOrigen.getValue().toString().equals("Internos"))
				{
					claveTabla.setValue("t");
					documento.setValue("T1");
				}
				else
				{
					claveTabla.setValue("E");
					documento.setValue("E1");
				}
				if (isHayGridComponentes())
				{
					gridComponentes.removeAllColumns();			
					frameCentral.removeComponent(gridComponentes);
					gridComponentes=null;
				}		
				llenarRegistros();
				
			}
		});
		
		this.fecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (fecha.getValue()!=null && fecha.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		this.documento.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (documento.getValue()!=null && documento.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		this.claveTabla.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (claveTabla.getValue()!=null && claveTabla.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				boolean guardar = false;
				MapeoRecepcionEMCS mapeoRecepcion = new MapeoRecepcionEMCS();
				Integer litrosAlbaran=null;
				if (gridComponentes!=null && gridComponentes.getSelectedRow()!=null) 
				{
					if (arc.getValue()!=null && arc.getValue().length()==21 && litros.getValue()!=null && litros.getValue().length()>0 
							&& grado.getValue()!=null && grado.getValue().length()>0 && numero.getValue()!=null && numero.getValue().length()==11)
					{
						try
						{
							mapeoRecepcion.setClaveTabla(claveTabla.getValue().toString());
							mapeoRecepcion.setDocumento(documento.getValue().toString());
							mapeoRecepcion.setArc(arc.getValue());
							mapeoRecepcion.setDestino(destino.getValue());
							mapeoRecepcion.setAnada(anada.getValue());
						
							mapeoRecepcion.setNumeroReferencia(new Long(numero.getValue()));
							mapeoRecepcion.setLitros(new Double(litros.getValue()));
							mapeoRecepcion.setGrado(new Double(grado.getValue()));
								
							if (!claveTabla.getValue().toString().toUpperCase().equals("T"))
							{
								MapeoLineasAlbaranesCompras mapeo=(MapeoLineasAlbaranesCompras) ((AyudaLineasComprasGrid) gridComponentes).getSelectedRow();
								if (mapeo!=null && mapeo.getIdCodigoCompra()!=null && mapeo.getAlias()!=null && mapeo.getAlias().length()>0) // && mapeo.getDireccion()!=null && mapeo.getDireccion().length()>0)
								{
									mapeoRecepcion.setAlbaran(mapeo.getIdCodigoCompra());
									mapeoRecepcion.setSerie(mapeo.getSerie());
									mapeoRecepcion.setCaeDestino(app.cmbCaeDestino.getValue().toString());
									mapeoRecepcion.setCodigoIdentificacion(1);
									mapeoRecepcion.setDescripcionIdentificacion("con CAE");
									mapeoRecepcion.setDestinatario(mapeo.getNombreProveedor());
									mapeoRecepcion.setDireccion(mapeo.getDireccion());
									mapeoRecepcion.setCodigoPostal(mapeo.getCodigoPostal());
									mapeoRecepcion.setEjercicio(mapeo.getEjercicio());
									mapeoRecepcion.setFecha(mapeo.getFechaDocumento());
									mapeoRecepcion.setNif(mapeo.getNif());
									mapeoRecepcion.setOrigen(mapeo.getAlias());
									mapeoRecepcion.setTipo("Granel");
									
									if (mapeo.getUnidades()!=null && mapeo.getUnidades().intValue()!= mapeoRecepcion.getLitros().intValue()) litrosAlbaran=mapeo.getUnidades().intValue();
									guardar=true;
								}
							}
							else
							{
								MapeoLineasAlbaranesTraslado mapeo=(MapeoLineasAlbaranesTraslado) ((AyudaLineasTrasladosGrid) gridComponentes).getSelectedRow();
								if (mapeo!=null && mapeo.getIdCodigoTraslado()!=null && mapeo.getCae()!=null && mapeo.getCae().length()>0) // && mapeo.getDireccion()!=null && mapeo.getDireccion().length()>0)
								{
									mapeoRecepcion.setAlbaran(mapeo.getIdCodigoTraslado());
									mapeoRecepcion.setSerie(mapeo.getSerie());
									mapeoRecepcion.setCaeDestino(app.cmbCaeDestino.getValue().toString());
									mapeoRecepcion.setCodigoIdentificacion(1);
									mapeoRecepcion.setDescripcionIdentificacion("con CAE");
									mapeoRecepcion.setCodigoPostal(mapeo.getCodigoPostal());
									mapeoRecepcion.setDestinatario(mapeo.getNombreAlmacen());
									mapeoRecepcion.setDireccion(mapeo.getDireccion());
									mapeoRecepcion.setEjercicio(mapeo.getEjercicio());
									mapeoRecepcion.setFecha(mapeo.getFechaDocumento());
									mapeoRecepcion.setNif(mapeo.getNif());
									mapeoRecepcion.setOrigen(mapeo.getCae());
									
									if (mapeo.getArticulo()!=null && mapeo.getArticulo().startsWith("0101")) mapeoRecepcion.setTipo("Granel"); else mapeoRecepcion.setTipo("PT");
									if (mapeo.getUnidades()!=null && mapeo.getUnidades().intValue()!= mapeoRecepcion.getLitros().intValue()) litrosAlbaran=mapeo.getUnidades();
									guardar=true;
								}				
							}
							if (guardar)
							{
								if (litrosAlbaran!=null)
								{
									app.mapeoRecepcionEMCSEnlazado=mapeoRecepcion;
									VentanaAceptarCancelar vt = new VentanaAceptarCancelar(app, "No coinciden los litros. Aceptas la diferencia (" + litros.getValue() + " has introducido y segun el albaran son " + litrosAlbaran.toString(), "Si", "No", "Guardar", null);			
									app.getUI().addWindow(vt);
								}
								else
								{
									app.cerrarVentanaBusquedaGuardarNuevo(mapeoRecepcion);
									close();
								}
							}
							else
							{
								Notificaciones.getInstance().mensajeError("El albaran seleccionado no proporciona razon social y cae de origen. Revisa los datos del proveedor / almacen ");
							}
						}
						catch (NumberFormatException nf)
						{
							Notificaciones.getInstance().mensajeError("Debes introducir un valor correcto en los campos litros y Grado");
						}
						catch (Exception ex)
						{
//							Notificaciones.getInstance().mensajeError(ex.getMessage());   
						}
					}
					else
					{
						Notificaciones.getInstance().mensajeError("Tienes que rellenar el numero de ARC, los litros totales, el grado y la referencia. ");
					}	
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Debes Seleccionar un registro. ");
				}
			}
		});
		
	}
	
	private void llenarRegistros()
	{
		this.vectorTraslado = null;
		this.vectorCompras = null;
		
    	if (this.claveTabla.getValue().toString().toUpperCase().equals("T"))
    	{
    		consultaAlbaranesTrasladoServer cats = new consultaAlbaranesTrasladoServer(CurrentUser.get());
    		this.vectorTraslado =cats.datosOpcionesGlobalSumaSinEMCS(this.ejercicio, this.claveTabla.getValue().toString(), this.documento.getValue().toString(),null,null, this.fecha.getValue(), this.almacen, this.almacenDestino);
    		
    		this.presentarGrid();    		
    	}
    	else
    	{
    		consultaAlbaranesComprasServer cavs = new consultaAlbaranesComprasServer(CurrentUser.get());
    		this.vectorCompras = cavs.datosOpcionesGlobalSinEMCS(this.ejercicio, this.claveTabla.getValue().toString(), this.documento.getValue().toString(),null,null, this.fecha.getValue(),this.almacenDestino);
    		
    		this.presentarGrid();
    	}
    	

	}

    private void presentarGrid()
    {
    	
    	if (this.vectorTraslado!=null && this.vectorTraslado.size()>0)
    	{
			gridComponentes = new AyudaLineasTrasladosGrid(this,vectorTraslado);
			if (((AyudaLineasTrasladosGrid) gridComponentes).vector==null)
	    	{
	    		setHayGridComponentes(false);
	    	}
	    	else
	    	{
	    		setHayGridComponentes(true);
	    	}
    	}
    	else if (this.vectorCompras!=null && this.vectorCompras.size()>0)
    	{
			gridComponentes = new AyudaLineasComprasGrid(vectorCompras);
			if (((AyudaLineasComprasGrid) gridComponentes).vector==null)
	    	{
	    		setHayGridComponentes(false);
	    	}
	    	else
	    	{
	    		setHayGridComponentes(true);
	    	}
			
    	}
    	else
    	{
    		setHayGridComponentes(false);
    	}
    	
    	if (isHayGridComponentes())
    	{
    		this.gridComponentes.setHeight("300px");
    		this.gridComponentes.setWidth("100%");
    		this.frameCentral.addComponent(gridComponentes);
    		this.gridComponentes.setCaption("Albaranes Encontrados" );
    		
    		this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
    	}
    }

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
	
	public void close() {
		super.close();
	}

	public void filaSeleccionada (Object r_fila)
	{
		/*
		 * ir a buscar los datos del traslado al seguimiento de salida y traerme los datos
		 */
		MapeoSeguimientoEMCS mapeoSeguimiento = new MapeoSeguimientoEMCS();
		consultaSeguimientoEMCSServer css = consultaSeguimientoEMCSServer.getInstance(CurrentUser.get());
		MapeoLineasAlbaranesTraslado mapeo = (MapeoLineasAlbaranesTraslado) r_fila;
		
		mapeoSeguimiento.setAlbaran(mapeo.getIdCodigoTraslado());
		mapeoSeguimiento.setClaveTabla("T");
		mapeoSeguimiento.setDocumento(mapeo.getDocumento());
		mapeoSeguimiento.setEstado("Todos");
		
		mapeoSeguimiento= css.datosSeguimientoEMCS(mapeoSeguimiento);
		
		if(mapeoSeguimiento!=null)
		{
			if (mapeoSeguimiento.getArc()!=null) this.arc.setValue(mapeoSeguimiento.getArc());
			if (mapeoSeguimiento.getNumeroReferencia()!=null) this.numero.setValue(mapeoSeguimiento.getNumeroReferencia().toString());
			if (mapeoSeguimiento.getLitros()!=null) this.litros.setValue(mapeoSeguimiento.getLitros().toString());
		}
		
	}

	private void cargarCombo()
	{  
		
		if (this.cmbOrigen.getValue().toString().equals("Internos") && (almacenDestino!=null) )
		{
			this.claveTabla.addItem("t");
			this.claveTabla.removeItem("E");
			this.claveTabla.removeItem("M");
			this.claveTabla.removeItem("T");
			
			this.claveTabla.setValue("t");
			
			this.documento.addItem("T1");
			this.documento.removeItem("E1");
			this.documento.removeItem("M1");
			
			this.documento.setValue("T1");
		}
		else
		{
			this.claveTabla.addItem("E");
			this.claveTabla.addItem("M");
			this.claveTabla.removeItem("t");
			
			this.claveTabla.setValue("E");
			
			this.documento.addItem("E1");
			this.documento.addItem("M1");
			this.documento.removeItem("T1");
			
			this.documento.setValue("E1");
		}
		
		this.claveTabla.setNullSelectionAllowed(false);
		this.claveTabla.setNewItemsAllowed(false);
		
		this.documento.setNullSelectionAllowed(false);
		this.documento.setNewItemsAllowed(false);
	}
}