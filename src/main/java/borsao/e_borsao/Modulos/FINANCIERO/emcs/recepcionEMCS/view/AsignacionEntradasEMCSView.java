package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.RecepcionEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server.consultaRecepcionEMCSServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class AsignacionEntradasEMCSView extends GridViewRefresh {

	public static final String VIEW_NAME = "Asignacion Entradas EMCS";
	public consultaRecepcionEMCSServer cus =null;
	public boolean modificando = false;
	public String tituloGrid = "Recepcion en ";
	public DateField fecha = null;
	public boolean conTotales = false;
	public MapeoRecepcionEMCS mapeoRecepcionEMCSEnlazado=null;
	public ComboBox cmbCaeDestino = null;	
	public ComboBox cmbEstado = null;
	public String almacenSeleccionado=null;
	public String numeroAlmacen=null;
	public TextField txtEjercicio = null;
	
	private String titulo = "Asignacion Entradas EMCS";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true ;
	private OpcionesForm form = null;
	private pantallaAsignacionDatosRecepcionEMCSAlbaran vt = null;
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public AsignacionEntradasEMCSView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
    	this.cus = new consultaRecepcionEMCSServer(CurrentUser.get());
    	
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		this.txtEjercicio= new TextField("Ejercicio");
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		
		this.cmbEstado= new ComboBox("Area");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cmbCaeDestino= new ComboBox("Destino");    		
		this.cmbCaeDestino.setNewItemsAllowed(false);
		this.cmbCaeDestino.setNullSelectionAllowed(false);
		this.cmbCaeDestino.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.fecha = new DateField();
		this.fecha.setValue(new Date());
		this.fecha.setDateFormat("dd/MM/yyyy");
		this.fecha.setShowISOWeekNumbers(true);
		this.fecha.addStyleName(ValoTheme.DATEFIELD_TINY);
		
		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.cmbEstado);
		this.cabLayout.addComponent(this.cmbCaeDestino);
		this.cabLayout.addComponent(this.fecha);
		this.cabLayout.setComponentAlignment(this.fecha, Alignment.BOTTOM_LEFT);
		
		this.cargarCombo();
		this.cargarListeners();
		this.establecerModo();
		
		opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		opcionesEscogidas.put("ejercicio", this.txtEjercicio.getValue());
		
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoRecepcionEMCS> r_vector=null;
    	MapeoRecepcionEMCS mapeoRecepcionEMCS =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoRecepcionEMCS=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	mapeoRecepcionEMCS.setCaeDestino(this.cmbCaeDestino.getValue().toString());
    	
    	
    	r_vector=this.cus.datosAsignacionEntradasEMCSGlobal(mapeoRecepcionEMCS);
    	
    	this.presentarGrid(r_vector);
    }
    
    public void generarGrid(MapeoRecepcionEMCS r_mapeo)
    {
    	ArrayList<MapeoRecepcionEMCS> r_vector=null;
    	r_vector=this.cus.datosAsignacionEntradasEMCSGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
		
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
    	if (this.grid==null || ((RecepcionEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RecepcionEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}
    	

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((RecepcionEMCSGrid) this.grid).activadaVentanaPeticion && !((RecepcionEMCSGrid) this.grid).ordenando)
    	{
        	this.modificando=false;
    		this.verForm(false);
    		
    		MapeoRecepcionEMCS map = (MapeoRecepcionEMCS) r_fila;
    				
    		if (map!=null && map.getClaveTabla()!=null && map.getClaveTabla().equals("t"))
    		{
	    		MapeoSeguimientoEMCS mapeoSeguimiento = new MapeoSeguimientoEMCS();
	    		consultaSeguimientoEMCSServer css = consultaSeguimientoEMCSServer.getInstance(CurrentUser.get());
	    		
	    		mapeoSeguimiento.setAlbaran(map.getAlbaran());
	    		mapeoSeguimiento.setClaveTabla("T");
	    		mapeoSeguimiento.setDocumento(map.getDocumento());
	    		mapeoSeguimiento.setEstado("Todos");
	    		
	    		mapeoSeguimiento= css.datosSeguimientoEMCS(mapeoSeguimiento);
	    		if (mapeoSeguimiento!=null)
	    		{
	    			map.setArc(mapeoSeguimiento.getArc());
	    			map.setNumeroReferencia(mapeoSeguimiento.getNumeroReferencia());
	    			map.setLitros(mapeoSeguimiento.getLitros());
	    		}
    		}
    		this.form.editarRecepcionEMCS(map);
    	}    		
    }
    
    public void presentarGrid(ArrayList<MapeoRecepcionEMCS> r_vector)
    {
    	if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
		}
    	
    	this.grid = new RecepcionEMCSGrid(this,r_vector);
    	if (((RecepcionEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RecepcionEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;		
	 				setHayGrid(false);
				}
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());
				
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));

				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Recepcion en " + almacenSeleccionado;
				
				generarGrid(mapeo);
			}
		});
    	
    	this.fecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
	 				grid.removeAllColumns();			
	 				barAndGridLayout.removeComponent(grid);
	 				grid=null;		
	 				setHayGrid(false);
				}
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());				
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));

				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Recepcion en " + almacenSeleccionado;
				
				generarGrid(mapeo);
			}
		});
    	
		this.cmbCaeDestino.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());				
				almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				tituloGrid = "Recepcion en " + almacenSeleccionado;
				
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());  				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (fecha.getValue()!=null) mapeo.setFecha(fecha.getValue());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (txtEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
				opcionesEscogidas.put("ejercicio", txtEjercicio.getValue());				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(fecha.getValue()));
				
				generarGrid(mapeo);
			}
		});
		
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 */
		this.verBotones(true);
		this.activarBotones(true);
		this.navegacion(true);
				
		this.opcBuscar.setVisible(false);
		this.opcBuscar.setEnabled(false);
		setSoloConsulta(this.soloConsulta);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	public void imprimirRecepcionEMCS()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		

		this.cmbCaeDestino.removeAllItems();
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		String almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			if (caes.get(i)!=null && caes.get(i).length()>0) this.cmbCaeDestino.addItem(caes.get(i).trim());
		}

		if (caes!=null && caes.size()> 1) this.cmbEstado.addItem("Traslados");
		this.cmbEstado.addItem("Compras");
		this.cmbEstado.addItem("Todas");
		this.cmbEstado.setValue("Todas");
		opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
		
		String caeDefecto = cas.obtenerCAEAlmacen(almacenDefecto);
		
		if (caeDefecto!=null)
		{
			
			this.cmbCaeDestino.setValue(caeDefecto.trim());
			this.almacenSeleccionado=cas.obtenerDescripcionAlmacen(almacenDefecto);
			this.numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
			this.tituloGrid = this.tituloGrid + " " + this.almacenSeleccionado; 
			this.opcionesEscogidas.put("caeDestino", this.cmbCaeDestino.getValue().toString());
		}
	}
	
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
		switch (r_accion)
		{
			case "Guardar":
			{
				this.cerrarVentanaBusquedaGuardarNuevo(mapeoRecepcionEMCSEnlazado);
				break;
			}
			case "Eliminar":
			{
				consultaRecepcionEMCSServer cus = new consultaRecepcionEMCSServer(CurrentUser.get());	
				((RecepcionEMCSGrid) this.grid).remove(mapeoRecepcionEMCSEnlazado);
				cus.eliminar(mapeoRecepcionEMCSEnlazado);
				this.form.cerrar();
				break;
			}
		}
	}

	public void cerrarVentanaBusqueda( String r_clave_tabla, String r_documento, String r_serie, Integer r_codigo, String r_arc, Double r_litros, Double r_grado, String r_destino)
	{
		MapeoRecepcionEMCS mapeo_orig = null;
		MapeoRecepcionEMCS mapeo = (MapeoRecepcionEMCS) this.grid.getSelectedRow();


		mapeo_orig = this.cus.copiar(mapeo, false);
		mapeo_orig.setClaveTabla(mapeo.getClaveTabla());
		mapeo_orig.setDocumento(mapeo.getDocumento());
		mapeo_orig.setSerie(mapeo.getSerie());
		mapeo_orig.setAlbaran(mapeo.getAlbaran());
		mapeo_orig.setArc(mapeo.getArc());	
		mapeo_orig.setLitros(mapeo.getLitros());	
		mapeo_orig.setGrado(mapeo.getGrado());
		mapeo_orig.setDestino(mapeo.getDestino());
		
		mapeo.setClaveTabla(r_clave_tabla);
		mapeo.setDocumento(r_documento);
		mapeo.setSerie(r_serie);
		mapeo.setAlbaran(r_codigo);
		mapeo.setArc(r_arc);
		mapeo.setLitros(r_litros);
		mapeo.setGrado(r_grado);
		mapeo.setDestino(r_destino);
		
		this.cus.guardarCambios(mapeo,mapeo_orig);
	}

	public void cerrarVentanaBusquedaGuardarNuevo(MapeoRecepcionEMCS r_mapeo)
	{
		String rdo = null;
		Integer id = this.cus.obtenerSiguiente();
		r_mapeo.setIdCodigo(id);
		if (r_mapeo!=null) rdo = this.cus.guardarNuevo(r_mapeo);
		
		if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		
		if (this.vt.isVisible()) this.vt.close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		if (this.form!=null) this.form.cerrar();
	}
	
	public void cerrarVentanaMarcar()
	{
		String rdo = null;
		MapeoRecepcionEMCS mapeo = (MapeoRecepcionEMCS) this.grid.getSelectedRow();
		
		if (mapeo!=null) this.cus.marcarAlbaranGreensys(mapeo.getEjercicio(), mapeo.getClaveTabla(),mapeo.getDocumento(),mapeo.getSerie(), mapeo.getAlbaran());
		
		if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			setHayGrid(false);
			generarGrid(opcionesEscogidas);
		}
	}

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	
}


