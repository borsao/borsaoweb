package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.AyudaLineasTrasladosGrid;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.AyudaLineasVentasGrid;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.modelo.MapeoLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAsignacionDatosEMCSAlbaran extends Ventana
{
	private SeguimientoEMCSView app = null;
	private Button btnBotonCVentana = null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	
//	private ComboBox claveTabla = null;
	private ComboBox documento = null;
	
//	private TextField numero= null;
	private TextField arc= null;
//	private TextField litros= null;
	private DateField fecha = null;
	
//	private Integer litrosMapeo = null;
	private Integer ejercicio = null;
	private String arcMapeo = null;
	private String almacen = null;
	private String almacenDestino=null;
	private Date fechaSeguimiento = null;

	private boolean hayGridComponentes= false;
	private VerticalLayout frameCentral = null;	

	private ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslado = null;
	private ArrayList<MapeoLineasAlbaranesVentas> vectorVentas = null;
	
	private HorizontalLayout entradasDatos = null;
	
	public pantallaAsignacionDatosEMCSAlbaran(SeguimientoEMCSView r_app, Integer r_ejercicio, String r_titulo, Date r_fecha, String r_almacen, String r_almacenDestino, String r_arc, Double r_litros)
	{
		this.app=r_app;
		this.ejercicio=r_ejercicio;
		this.setCaption(r_titulo);
		this.almacen=r_almacen;
		this.almacenDestino = r_almacenDestino;
		this.fechaSeguimiento = new Date();
		this.arcMapeo=r_arc;
//		this.litrosMapeo=r_litros;

		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarCombo();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			this.frameCentral.setMargin(true);
			
				entradasDatos= new HorizontalLayout();
				entradasDatos.setMargin(true);
				entradasDatos.setWidth("100%");
				
					this.fecha = new DateField("Fecha");
					this.fecha.setEnabled(true);
					this.fecha.setDateFormat("dd/MM/yyyy");
					this.fecha.setValue(this.fechaSeguimiento);

					this.documento = new ComboBox("Documentos");
					this.documento.setVisible(false);
					this.documento.setNewItemsAllowed(false);
					this.documento.setNullSelectionAllowed(false);
					
					this.arc=new TextField();
					this.arc.setEnabled(true);
					this.arc.addStyleName("rightAligned");
					this.arc.setCaption("ARC");
					this.arc.setRequired(true);
					if (this.arcMapeo!=null) this.arc.setValue(this.arcMapeo);

					
				entradasDatos.addComponent(this.fecha);
				entradasDatos.addComponent(this.documento);
				entradasDatos.addComponent(this.arc);
				entradasDatos.setComponentAlignment(this.arc, Alignment.MIDDLE_LEFT);
				entradasDatos.setComponentAlignment(this.fecha, Alignment.MIDDLE_LEFT);
				
			frameCentral.addComponent(this.entradasDatos);
				
			HorizontalLayout framePie = new HorizontalLayout();
		
				btnBotonCVentana = new Button("Seleccionar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.fecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (fecha.getValue()!=null && fecha.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		this.documento.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (documento.getValue()!=null && documento.getValue().toString()=="Venta") 
				{
					almacenDestino=null;
					rellenar();
				}
				else
				{
					rellenar();
				}
			}
		}); 

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				String clave_tabla = null;
				String doc = null;
				String serie = null;
				Integer codigo = null;
				String arcIntroducido = null;
				Double litros=null;
				
				if (gridComponentes!=null && gridComponentes.getSelectedRow()!=null) 
				{
					if (arc.getValue()!=null && arc.getValue().length()==21)//) && litros.getValue()!=null && litros.getValue().length()>0)
					{
						arcIntroducido = arc.getValue();
//						litrosIntroducidos = new Integer(litros.getValue());
								
						if (almacenDestino==null)
						{
							MapeoLineasAlbaranesVentas mapeo=(MapeoLineasAlbaranesVentas) ((AyudaLineasVentasGrid) gridComponentes).getSelectedRow();
							if (mapeo!=null && mapeo.getIdCodigoVenta()!=null)
							{
								codigo = mapeo.getIdCodigoVenta();
								serie = mapeo.getSerie();
								doc = mapeo.getDocumento();
								clave_tabla = mapeo.getClaveTabla();
								litros = mapeo.getUnidades().doubleValue();
							}
						}
						else
						{
							MapeoLineasAlbaranesTraslado mapeo=(MapeoLineasAlbaranesTraslado) ((AyudaLineasTrasladosGrid) gridComponentes).getSelectedRow();
							if (mapeo!=null && mapeo.getIdCodigoTraslado()!=null)
							{
								codigo = mapeo.getIdCodigoTraslado();
								serie = mapeo.getSerie();			
								doc = mapeo.getDocumento();
								clave_tabla = "T";
								litros=mapeo.getUnidades().doubleValue();
							}				
						}
						app.cerrarVentanaBusqueda(clave_tabla, doc, serie, codigo, arcIntroducido, litros);
						close();
					}
					else
					{
						Notificaciones.getInstance().mensajeError("Tienes que rellenar correctamente el numero de ARC. ");
					}	
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Debes Seleccionar un registro. ");
				}
			}
		});
		
	}
	
	private void llenarRegistros()
	{
		consultaAlmacenesServer cams= consultaAlmacenesServer.getInstance(CurrentUser.get());
		if (this.almacenDestino!=null)
		{
			boolean externo = cams.obtenerAlmacenExterno(almacenDestino);
			if (externo) 
			{
				this.documento.setVisible(true);
			}
			else
			{
				this.rellenar();
			}
		}
		else
		{
			this.rellenar();
		}
	}

    private void presentarGrid()
    {
    	
    	if (this.vectorTraslado!=null && this.vectorTraslado.size()>0)
    	{
			gridComponentes = new AyudaLineasTrasladosGrid(vectorTraslado);
			if (((AyudaLineasTrasladosGrid) gridComponentes).vector==null)
	    	{
	    		setHayGridComponentes(false);
	    	}
	    	else
	    	{
	    		setHayGridComponentes(true);
	    	}
    	}
    	else if (this.vectorVentas!=null && this.vectorVentas.size()>0)
    	{
			gridComponentes = new AyudaLineasVentasGrid(vectorVentas);
			if (((AyudaLineasVentasGrid) gridComponentes).vector==null)
	    	{
	    		setHayGridComponentes(false);
	    	}
	    	else
	    	{
	    		setHayGridComponentes(true);
	    	}
			
    	}
    	else
    	{
    		setHayGridComponentes(false);
    	}
    	
    	if (isHayGridComponentes())
    	{
    		this.gridComponentes.setHeight("500px");
    		this.gridComponentes.setWidth("100%");
    		this.frameCentral.addComponent(gridComponentes);
    		this.gridComponentes.setCaption("Albaranes Encontrados" );
    		
    		this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
    	}
    }

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
	
	public void close() {
		super.close();
	}
	
	private void cargarCombo()
	{

		this.documento.addItem("Venta");
		this.documento.addItem("Traslado");
		
//		if (this.almacenDestino!=null)
//		{
//			this.claveTabla.addItem("T");
//			this.claveTabla.addItem("t");
//			this.claveTabla.setValue("T");
//			this.documento.addItem("T1");
//			this.documento.setValue("T1");
//		}
//		else
//		{
//			this.claveTabla.addItem("A");
//			this.claveTabla.addItem("F");
//			
//			this.claveTabla.setValue("A");
//			
//			this.documento.addItem("e");
//			this.documento.addItem("n");
//			this.documento.addItem("u");
//			this.documento.addItem("E");
//			this.documento.addItem("N");
//			this.documento.addItem("U");
//			
//			if (this.app.cmbArea.getValue().toString().contains("Europa"))
//			{
//				this.documento.setValue("e");
//			}
//			else
//			{
//				this.documento.setValue("n");
//			}
//		}
//		
//		this.claveTabla.setNullSelectionAllowed(false);
//		this.claveTabla.setNewItemsAllowed(false);
//		
//		this.documento.setNullSelectionAllowed(false);
//		this.documento.setNewItemsAllowed(false);
	}

	@Override
	public void aceptarProceso(String r_accion) {
	}

	@Override
	public void cancelarProceso(String r_accion) {
		almacenDestino=null;
	}
	
	private void rellenar()
	{
    	if (this.almacenDestino!=null)
    	{
    		consultaAlbaranesTrasladoServer cats = new consultaAlbaranesTrasladoServer(CurrentUser.get());
    		this.vectorTraslado =cats.datosOpcionesGlobalSumaSinEMCS(this.ejercicio, "T", null,null,null, this.fecha.getValue(), this.almacen, this.almacenDestino);
    	}
    	else
    	{
    		consultaAlbaranesVentasServer cavs = new consultaAlbaranesVentasServer(CurrentUser.get());
    		this.vectorVentas = cavs.datosOpcionesGlobalSinEMCS(this.app.cmbArea.getValue().toString(), this.ejercicio, null, null,null,null, this.fecha.getValue(),new Integer(this.almacen));
    	}
    	this.presentarGrid();

	}
}