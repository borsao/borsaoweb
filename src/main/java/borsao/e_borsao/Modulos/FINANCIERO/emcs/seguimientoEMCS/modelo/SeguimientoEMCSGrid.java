package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view.pantallaLineasAlbaranesTraslados;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.view.pantallaLineasAlbaranesVentas;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.AsignacionSalidasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.SeguimientoEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view.pantallaAsignacionDatosEMCSAlbaran;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class SeguimientoEMCSGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean conTotales = true;	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	private GridViewRefresh app = null;
	
    public SeguimientoEMCSGrid(SeguimientoEMCSView r_app, ArrayList<MapeoSeguimientoEMCS> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
        this.asignarTitulo(((SeguimientoEMCSView)this.app).tituloGrid);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public SeguimientoEMCSGrid(AsignacionSalidasEMCSView r_app, ArrayList<MapeoSeguimientoEMCS> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo(((AsignacionSalidasEMCSView) this.app).tituloGrid);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoSeguimientoEMCS.class);
		this.setRecords(this.vector);
//		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    

    public void doEditItem() 
    {
    }
  
  @Override
  	public void doCancelEditor()
  	{
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	if (this.app instanceof SeguimientoEMCSView)
    	{
    		setColumnOrder("documentoFalso", "doc", "tipo", "origen", "ejercicio", "mes", "fecha","numeroReferencia" , "destinatario","nombreCliente", "codigoIdentificacion",  "exciseNumber", "nif",  "agenteAduanas", "registered", "authorized", "taxwarehouse" ,   "nombreTransporte", "arc", "litros", "observaciones", "descripcionIdentificacion", "documento", "albaran" );
    	}
    	else
    	{
    		setColumnOrder("documentoFalso", "doc", "tipo", "ejercicio", "mes", "documento", "claveTabla", "serie", "albaran" , "fecha", "numeroReferencia" , "destinatario",   "nombreCliente","codigoIdentificacion",  "exciseNumber", "nif",  "agenteAduanas", "registered", "authorized", "taxwarehouse" , "nombreTransporte", "arc", "litros", "observaciones", "descripcionIdentificacion");
    	}
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("tipo", "55");
    	this.widthFiltros.put("mes", "35");
    	this.widthFiltros.put("origen", "125");
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("numeroReferencia", "110");
    	this.widthFiltros.put("destinatario", "260");
    	this.widthFiltros.put("nombreCliente", "200");
    	this.widthFiltros.put("codigoIdentificacion", "35");
    	this.widthFiltros.put("documento", "60");
    	this.widthFiltros.put("albaran", "80");
    	this.widthFiltros.put("arc", "280");
    	
    	this.getColumn("doc").setHeaderCaption("");
    	this.getColumn("doc").setSortable(false);
    	this.getColumn("doc").setWidth(new Double(50));

    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setSortable(false);
    	this.getColumn("tipo").setWidth(new Double(95));

    	this.getColumn("mes").setHeaderCaption("Mes");
    	this.getColumn("mes").setSortable(false);
    	this.getColumn("mes").setWidth(new Double(85));

//    	this.getColumn("idPrg").setHeaderCaption("");
//    	this.getColumn("idPrg").setSortable(false);
//    	this.getColumn("idPrg").setWidth(new Double(40));
//    	this.getColumn("esc").setHeaderCaption("");
//    	this.getColumn("esc").setSortable(false);
//    	this.getColumn("esc").setWidth(new Double(40));

    	this.getColumn("origen").setHeaderCaption("Expedidor");
    	this.getColumn("origen").setWidthUndefined();
    	this.getColumn("origen").setWidth(120);
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(95);
    	this.getColumn("fecha").setHeaderCaption("FECHA");
    	this.getColumn("fecha").setWidthUndefined();
    	this.getColumn("fecha").setWidth(120);

    	this.getColumn("documento").setHeaderCaption("Doc");
    	this.getColumn("documento").setWidthUndefined();
    	this.getColumn("documento").setWidth(100);
    	this.getColumn("albaran").setHeaderCaption("Numero");
    	this.getColumn("albaran").setWidthUndefined();
    	this.getColumn("albaran").setWidth(120);

    	this.getColumn("numeroReferencia").setHeaderCaption("NR");
    	this.getColumn("numeroReferencia").setWidth(150);
    	this.getColumn("destinatario").setHeaderCaption("Destinatario");
    	this.getColumn("destinatario").setWidth(300);
    	this.getColumn("codigoIdentificacion").setHeaderCaption("Code");
    	this.getColumn("codigoIdentificacion").setWidth(75);
    	
    	
    	if ((this.app instanceof SeguimientoEMCSView && ((SeguimientoEMCSView)this.app).cmbArea.getValue().toString().contains("Interno"))  || (this.app instanceof AsignacionSalidasEMCSView && ((AsignacionSalidasEMCSView) this.app).cmbArea.getValue().toString().contains("Interno")))
    	{
    		this.widthFiltros.put("exciseNumber", "110");
    		this.widthFiltros.put("nif", "80");
    		this.widthFiltros.put("agenteAduanas", "200");
    		
	    	this.getColumn("exciseNumber").setHeaderCaption("Excise Number");
	    	this.getColumn("exciseNumber").setWidth(150);
	    	this.getColumn("nif").setHeaderCaption("NIF");
	    	this.getColumn("nif").setWidth(120);
	    	this.getColumn("agenteAduanas").setHeaderCaption("Agente ADUANAS");
	    	this.getColumn("agenteAduanas").setWidth(240);
	    	
	    	this.getColumn("registered").setHidden(true);
    		this.getColumn("authorized").setHidden(true);
    		this.getColumn("taxwarehouse").setHidden(true);
    		this.getColumn("nombreTransporte").setHidden(true);
    		
    	}
    	else if ((this.app instanceof SeguimientoEMCSView && ((SeguimientoEMCSView)this.app).cmbArea.getValue().toString().contains("Europa")) || (this.app instanceof AsignacionSalidasEMCSView && ((AsignacionSalidasEMCSView) this.app).cmbArea.getValue().toString().contains("Europa")))
    	{
    		this.widthFiltros.put("nombreTransporte", "200");
    		this.widthFiltros.put("registered", "110");
    		this.widthFiltros.put("authorized", "110");
    		this.widthFiltros.put("taxwarehouse", "110");
    		
    		this.getColumn("registered").setHeaderCaption("Registered Consignee");
    		this.getColumn("registered").setWidth(150);
    		this.getColumn("authorized").setHeaderCaption("Authorized WH Keepr");
    		this.getColumn("authorized").setWidth(150);
    		this.getColumn("taxwarehouse").setHeaderCaption("TAX Warehouse");
    		this.getColumn("taxwarehouse").setWidth(150);
    		this.getColumn("nombreTransporte").setHeaderCaption("Transporte");
    		this.getColumn("nombreTransporte").setWidth(240);
    		
    		this.getColumn("exciseNumber").setHidden(true);
    		this.getColumn("nif").setHidden(true);
    		this.getColumn("agenteAduanas").setHidden(true);    		
    	}    	
    	
    	this.getColumn("nombreCliente").setHeaderCaption("Cliente");
    	this.getColumn("nombreCliente").setWidth(240);
    	this.getColumn("arc").setHeaderCaption("ARC");
    	this.getColumn("arc").setWidth(300);
    	this.getColumn("litros").setHeaderCaption("Litros");
    	this.getColumn("litros").setWidth(125);
    	this.getColumn("observaciones").setHeaderCaption("OBSERVACIONES");
    	this.getColumn("observaciones").setWidth(500);
    	
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("origen").setHidden(true);
    	this.getColumn("area").setHidden(true);
    	this.getColumn("claveTabla").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	if (this.app instanceof SeguimientoEMCSView) this.getColumn("tipo").setHidden(true);
//    	if (this.app instanceof SeguimientoEMCSView) this.getColumn("documento").setHidden(true);
//    	if (this.app instanceof SeguimientoEMCSView) this.getColumn("albaran").setHidden(true);
    	this.getColumn("documentoFalso").setHidden(true);
    	
    	this.getColumn("litros").setRenderer((Renderer) new NumberRenderer(new DecimalFormat("#,##0.00")));    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
//    	this.getColumn("idRetrabajo").setRenderer(new NumberRenderer());
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoSeguimientoEMCS) {
//                MapeoSeguimientoEMCS progRow = (MapeoSeguimientoEMCS)bean;
                // The actual description text is depending on the application
//                if ("idRetrabajo".equals(cell.getPropertyId()))
//                {
//                	switch (progRow.getEstado())
//                	{
//                		case "C":
//                		{
//                			descriptionText = "Cerrada";
//                			break;
//                		}
//                		case "P":
//                		{
//                			descriptionText = "Programada";
//                			break;
//                		}
//                		case "A":
//                		{
//                			descriptionText = "Pendiente";
//                			break;
//                		}
//                	}
//                	
//                }
//                else if ("esc".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Ayuda produccion";
//                }
//                else if ("idPrg".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Programación";
//                }
            	if ("doc".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Albaran";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estilo = null;
    		String relleno = null;
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "documentoFalso".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().length()>0)
            			relleno = "S";
            		else
            			relleno = "N";
            	}
            	if ( "doc".equals(cellReference.getPropertyId()))
            	{
            		if (relleno=="S") estilo = "nativebuttonLineas";
            		if (relleno=="N") estilo = "nativebuttonLineasPdte";
            	}            	
            	else if ( "litros".equals(cellReference.getPropertyId()) || "mes".equals(cellReference.getPropertyId()))
            	{
            		estilo =  "Rcell-normal";
            	}
            	else estilo =  "cell-normal";
            	return estilo;
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoSeguimientoEMCS mapeo = (MapeoSeguimientoEMCS) event.getItemId();
            	
//            	if (event.getPropertyId().toString().equals("idPrg"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
//            		}
//            		else if (mapeo.getIdProgramacionEnvasadora()!=null && mapeo.getIdProgramacionEnvasadora()!=0)
//            		{
//            			pantallaLineasProgramacionesEnvasadora vt = null;
//        	    		vt= new pantallaLineasProgramacionesEnvasadora(mapeo.getIdProgramacionEnvasadora(),"Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
//            		}
//            		else
//            		{
//            			PeticionAsignacionProgramacion vt = null;
//        	    		vt= new PeticionAsignacionProgramacion(app, mapeo, "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
//            		}
//            	}
//            	else if (event.getPropertyId().toString().equals("esc"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		pantallaAyudaProduccion vt = null;
//            		
//            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
//            		{
//            			vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo());
//            			getUI().addWindow(vt);	            			
//            		}	            		
//            	}
//            	else
//            	{
//            		activadaVentanaPeticion=false;
//            		ordenando = false;
//	            }
            	if (event.getPropertyId().toString().equals("doc"))
            	{
            		
            		pantallaLineasAlbaranesTraslados vtt = null;
            		pantallaLineasAlbaranesVentas vt = null;
            		pantallaAsignacionDatosEMCSAlbaran v = null;
            		String caeDestino = null;
            		
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		select(mapeo);
            		if (mapeo.getArea().contains("Europa")) caeDestino = mapeo.getTaxwarehouse(); else caeDestino = mapeo.getExciseNumber();
            		
            		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
            		String almacen = cas.obtenerAlmacenPorCAE(mapeo.getOrigen());
            		
            		String almacenDestino = null;
            		almacenDestino = cas.obtenerAlmacenPorCAE(caeDestino);
            		
            		if ((almacenDestino==null || almacenDestino.length()==0) && caeDestino !=null && caeDestino.length()>0)
            		{
            			almacenDestino = cas.obtenerAlmacenPorCAEDestino(caeDestino.substring(0, 4));
            		}
        			if (mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0 && !mapeo.getClaveTabla().toUpperCase().equals("T"))
        			{
        				vt = new pantallaLineasAlbaranesVentas(app, " Albaran Venta " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
        				getUI().addWindow(vt);
        			}
        			else if (mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0 && mapeo.getClaveTabla().toUpperCase().equals("T"))
        			{
        				vtt = new pantallaLineasAlbaranesTraslados(app, " Albaran Traslado " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
        				getUI().addWindow(vtt);            				
        			}
        			else
        			{
        				v = new pantallaAsignacionDatosEMCSAlbaran((SeguimientoEMCSView) app, mapeo.getEjercicio(), "Asignacion ", mapeo.getFecha(), almacen, almacenDestino, mapeo.getArc(), mapeo.getLitros());
        				getUI().addWindow(v);
        			}
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fecha");
		
//		this.camposNoFiltrar.add("nif");
//		this.camposNoFiltrar.add("exciseNumber");
//		this.camposNoFiltrar.add("agenteAduanas");
//		this.camposNoFiltrar.add("registered");
//		this.camposNoFiltrar.add("authorized");
//		this.camposNoFiltrar.add("taxwarehouse");
		
//		this.camposNoFiltrar.add("arc");
		this.camposNoFiltrar.add("descripcionIdentificacion");
		this.camposNoFiltrar.add("litros");
		this.camposNoFiltrar.add("observaciones");
//		if (this.app instanceof SeguimientoEMCSView) this.camposNoFiltrar.add("documento");
//		if (this.app instanceof SeguimientoEMCSView) this.camposNoFiltrar.add("albaran");
		this.camposNoFiltrar.add("doc");
	}

	public void generacionPdf(MapeoSeguimientoEMCS r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	
    	
//    	consultaSeguimientoEMCSServer cps = new consultaSeguimientoEMCSServer(CurrentUser.get());
//    	pdfGenerado = cps.generarInforme(r_mapeo);
//    	if(pdfGenerado.length()>0)
//    	{
//			try
//            { 
//				if (r_impresora.toUpperCase().equals("PANTALLA"))
//				{
//					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//				}
//				else
//				{
//					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
//					
//					RutinasFicheros.imprimir(archivo, r_impresora);
//				    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
//				}
//				
//            }
//			catch(Exception e)
//			{ 
//                 Notificaciones.getInstance().mensajeSeguimiento(e); 
//			}
//    	}
//    	else
//    		Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
		Double totalLitros = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	Double q1Value = (Double) item.getItemProperty("litros").getValue();
        	if (q1Value!=null) totalLitros += q1Value;

        }
        
        footer.getCell("mes").setText("Totales");
		footer.getCell("litros").setText(RutinasNumericas.formatearDouble(totalLitros));
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("litros").setStyleName("Rcell-pie");

	}
}



