package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server;

 import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.modelo.MapeoLineasAlbaranesTraslado;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.server.consultaAlbaranesTrasladoServer;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.modelo.MapeoLineasAlbaranesCompras;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.server.consultaAlbaranesComprasServer;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCSVariedades;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.server.consultaVariedadesDgaServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaRecepcionEMCSServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaRecepcionEMCSServer instance;
	private final String tableName = "fin_emcs_terceros";
	private final String identificadorTabla = "id";
	
	public consultaRecepcionEMCSServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRecepcionEMCSServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRecepcionEMCSServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoRecepcionEMCS> datosRecepcionEMCSGlobal(MapeoRecepcionEMCS r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoRecepcionEMCS> vector = null;
		StringBuffer cadenaWhere = null;
		MapeoRecepcionEMCS mapeoRecepcionEMCS = null;
		Connection con = null;
		Statement cs = null;

			
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT id ter_id, ");
		cadenaSQL.append(" ejercicio ter_eje,");
		cadenaSQL.append(" numeroReferencia ter_num,");
		cadenaSQL.append(" fecha ter_fec,");
		cadenaSQL.append(" caeExpedidor ter_ori,");
		cadenaSQL.append(" nombreExpedidor ter_nom,");
		cadenaSQL.append(" direccionExpedidor ter_dir,");
		cadenaSQL.append(" cpExpedidor ter_cp,");
		cadenaSQL.append(" nifExpedidor ter_nif,");
		cadenaSQL.append(" caeDestino ter_cae,");
		cadenaSQL.append(" codigo ter_cod,");
		cadenaSQL.append(" arc ter_arc,");
		cadenaSQL.append(" litros ter_ltr,");
		cadenaSQL.append(" grado ter_gra,");
		cadenaSQL.append(" destino ter_des,");
		cadenaSQL.append(" observaciones ter_obs, ");
		cadenaSQL.append(" documento ter_doc, ");
		cadenaSQL.append(" claveTabla ter_cl, ");
		cadenaSQL.append(" serie ter_ser, ");
		cadenaSQL.append(" albaran ter_alb, ");
		cadenaSQL.append(" anada ter_ana, ");
		cadenaSQL.append(" tipo ter_tip ");
		
		cadenaSQL.append(" from " + this.tableName + " ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getDestino()!=null && r_mapeo.getDestino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" destino = '" + r_mapeo.getDestino() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeExpedidor = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getCaeDestino()!=null && r_mapeo.getCaeDestino().length()>0 && !r_mapeo.getCaeDestino().equals("Todos"))
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caeDestino = '" + r_mapeo.getCaeDestino() + "' ");
				}
				if (r_mapeo.getNumeroReferencia()!=null && r_mapeo.getNumeroReferencia()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" numeroReferencia = " + r_mapeo.getNumeroReferencia() + " ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getDestinatario()!=null && r_mapeo.getDestinatario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombreExpedidor = '" + r_mapeo.getDestinatario() + "' ");
				}
				if (r_mapeo.getNif()!=null && r_mapeo.getNif().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nifExpedidor = '" + r_mapeo.getNif() + "' ");
				}
				if (r_mapeo.getCodigoIdentificacion()!=null && r_mapeo.getCodigoIdentificacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" codigo = '" + this.obtenerCodigo(r_mapeo.getDescripcionIdentificacion()) + "' ");
				}
				if (r_mapeo.getLitros()!=null && r_mapeo.getLitros()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" litros = " + r_mapeo.getLitros() + " ");
				}
				if (r_mapeo.getGrado()!=null && r_mapeo.getGrado()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" grado = " + r_mapeo.getGrado() + " ");
				}
				if (r_mapeo.getArc()!=null && r_mapeo.getArc().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" arc = '" + r_mapeo.getArc() + "' ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" albaran = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().equals("Pendientes"))
				{
//					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
//					cadenaWhere.append(" (albaran is null or albaran = 0 or arc is null) ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha desc , numeroReferencia desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoRecepcionEMCS>();
			
			while(rsOpcion.next())
			{
				mapeoRecepcionEMCS = new MapeoRecepcionEMCS();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRecepcionEMCS.setIdCodigo(rsOpcion.getInt("ter_id"));
				mapeoRecepcionEMCS.setEjercicio(rsOpcion.getInt("ter_eje"));
				mapeoRecepcionEMCS.setLitros(rsOpcion.getDouble("ter_ltr"));
				mapeoRecepcionEMCS.setGrado(rsOpcion.getDouble("ter_gra"));
				
				mapeoRecepcionEMCS.setCodigoIdentificacion(rsOpcion.getInt("ter_cod"));
				mapeoRecepcionEMCS.setDescripcionIdentificacion(this.obtenerCodigo(rsOpcion.getInt("ter_cod")));
				
				mapeoRecepcionEMCS.setNumeroReferencia(rsOpcion.getLong("ter_num"));				
				mapeoRecepcionEMCS.setOrigen(rsOpcion.getString("ter_ori"));
				mapeoRecepcionEMCS.setDestinatario(rsOpcion.getString("ter_nom"));
				mapeoRecepcionEMCS.setDireccion(rsOpcion.getString("ter_dir"));
				mapeoRecepcionEMCS.setCaeDestino(rsOpcion.getString("ter_cae"));
				mapeoRecepcionEMCS.setCodigoPostal(rsOpcion.getString("ter_cp"));
				mapeoRecepcionEMCS.setArc(rsOpcion.getString("ter_arc"));
				mapeoRecepcionEMCS.setDestino(rsOpcion.getString("ter_des"));
				mapeoRecepcionEMCS.setAnada(rsOpcion.getString("ter_ana"));
				mapeoRecepcionEMCS.setNif(rsOpcion.getString("ter_nif"));
				mapeoRecepcionEMCS.setFecha(RutinasFechas.conversion(rsOpcion.getDate("ter_fec")));
				mapeoRecepcionEMCS.setObservaciones(rsOpcion.getString("ter_obs"));
				
				mapeoRecepcionEMCS.setClaveTabla(rsOpcion.getString("ter_cl"));
				mapeoRecepcionEMCS.setSerie(rsOpcion.getString("ter_ser"));
				mapeoRecepcionEMCS.setDocumento(rsOpcion.getString("ter_doc"));
				mapeoRecepcionEMCS.setAlbaran(rsOpcion.getInt("ter_alb"));
				mapeoRecepcionEMCS.setTipo(rsOpcion.getString("ter_tip"));
				
				vector.add(mapeoRecepcionEMCS);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}
	
	public MapeoRecepcionEMCS datosRecepcionEMCS(MapeoRecepcionEMCS r_mapeo)
	{
		
		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		MapeoRecepcionEMCS mapeoRecepcionEMCS = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT id ter_id, ");
		cadenaSQL.append(" ejercicio ter_eje,");
		cadenaSQL.append(" numeroReferencia ter_num,");
		cadenaSQL.append(" fecha ter_fec,");
		cadenaSQL.append(" caeExpedidor ter_ori,");
		cadenaSQL.append(" nombreExpedidor ter_nom,");
		cadenaSQL.append(" direccionExpedidor ter_dir,");
		cadenaSQL.append(" cpExpedidor ter_cp,");
		cadenaSQL.append(" nifExpedidor ter_nif,");
		cadenaSQL.append(" caeDestino ter_cae,");
		cadenaSQL.append(" codigo ter_cod,");
		cadenaSQL.append(" arc ter_arc,");
		cadenaSQL.append(" litros ter_ltr,");
		cadenaSQL.append(" grado ter_gra,");
		cadenaSQL.append(" destino ter_des,");
		cadenaSQL.append(" observaciones ter_obs, ");
		cadenaSQL.append(" documento ter_doc, ");
		cadenaSQL.append(" claveTabla ter_cl, ");
		cadenaSQL.append(" serie ter_ser, ");
		cadenaSQL.append(" albaran ter_alb, ");
		cadenaSQL.append(" anada ter_ana, ");
		cadenaSQL.append(" tipo ter_tip ");
		
		cadenaSQL.append(" from " + this.tableName + " ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();				
			
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClaveTabla()!=null && r_mapeo.getClaveTabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" claveTabla = '" + r_mapeo.getClaveTabla() + "' ");
				}
				if (r_mapeo.getSerie()!=null && r_mapeo.getSerie().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" serie = '" + r_mapeo.getSerie() + "' ");
				}
				if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" albaran = " + r_mapeo.getAlbaran());
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha desc , numeroReferencia desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				mapeoRecepcionEMCS = new MapeoRecepcionEMCS();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRecepcionEMCS.setIdCodigo(rsOpcion.getInt("ter_id"));
				mapeoRecepcionEMCS.setEjercicio(rsOpcion.getInt("ter_eje"));
				mapeoRecepcionEMCS.setLitros(rsOpcion.getDouble("ter_ltr"));
				mapeoRecepcionEMCS.setGrado(rsOpcion.getDouble("ter_gra"));
				
				mapeoRecepcionEMCS.setCodigoIdentificacion(rsOpcion.getInt("ter_cod"));
				mapeoRecepcionEMCS.setDescripcionIdentificacion(this.obtenerCodigo(rsOpcion.getInt("ter_cod")));
				
				mapeoRecepcionEMCS.setNumeroReferencia(rsOpcion.getLong("ter_num"));				
				mapeoRecepcionEMCS.setOrigen(rsOpcion.getString("ter_ori"));
				mapeoRecepcionEMCS.setDestinatario(rsOpcion.getString("ter_nom"));
				mapeoRecepcionEMCS.setDireccion(rsOpcion.getString("ter_dir"));
				mapeoRecepcionEMCS.setCaeDestino(rsOpcion.getString("ter_cae"));
				mapeoRecepcionEMCS.setCodigoPostal(rsOpcion.getString("ter_cp"));
				mapeoRecepcionEMCS.setArc(rsOpcion.getString("ter_arc"));
				mapeoRecepcionEMCS.setDestino(rsOpcion.getString("ter_des"));
				mapeoRecepcionEMCS.setAnada(rsOpcion.getString("ter_ana"));
				mapeoRecepcionEMCS.setNif(rsOpcion.getString("ter_nif"));
				mapeoRecepcionEMCS.setFecha(RutinasFechas.conversion(rsOpcion.getDate("ter_fec")));
				mapeoRecepcionEMCS.setObservaciones(rsOpcion.getString("ter_obs"));
				
				mapeoRecepcionEMCS.setClaveTabla(rsOpcion.getString("ter_cl"));
				mapeoRecepcionEMCS.setSerie(rsOpcion.getString("ter_ser"));
				mapeoRecepcionEMCS.setDocumento(rsOpcion.getString("ter_doc"));
				mapeoRecepcionEMCS.setAlbaran(rsOpcion.getInt("ter_alb"));
				mapeoRecepcionEMCS.setTipo(rsOpcion.getString("ter_tip"));
				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return mapeoRecepcionEMCS;
	}
	
	public ArrayList<MapeoRecepcionEMCS> datosAsignacionEntradasEMCSGlobal(MapeoRecepcionEMCS r_mapeo)
	{
		ArrayList<MapeoRecepcionEMCS> vector = null;
		ArrayList<MapeoLineasAlbaranesCompras> vectorCompras = null;
		ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslado = null;
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
    	String almacen= cas.obtenerAlmacenPorCAE(r_mapeo.getCaeDestino());

    	vector = new ArrayList<MapeoRecepcionEMCS>();
    	
    	if (r_mapeo.getEstado().equals("Compras") || r_mapeo.getEstado().equals("Todas") )
    	{
			vectorCompras = this.obtenerDocumentosCompras(r_mapeo.getEjercicio(), null, r_mapeo.getFecha(), almacen);
			
			for (int i = 0; i< vectorCompras.size();i++)
			{
				MapeoLineasAlbaranesCompras mapeoAlbaran = vectorCompras.get(i);			
				MapeoRecepcionEMCS mapeoRecepcionEMCS = new MapeoRecepcionEMCS();
				
				mapeoRecepcionEMCS.setEjercicio(mapeoAlbaran.getEjercicio());			
				mapeoRecepcionEMCS.setOrigen(mapeoAlbaran.getAlias());
				mapeoRecepcionEMCS.setDestinatario(mapeoAlbaran.getNombreProveedor());
				mapeoRecepcionEMCS.setDireccion(mapeoAlbaran.getDireccion());			
				mapeoRecepcionEMCS.setCaeDestino(r_mapeo.getCaeDestino());
				mapeoRecepcionEMCS.setCodigoPostal(mapeoAlbaran.getCodigoPostal());			
				mapeoRecepcionEMCS.setNif(mapeoAlbaran.getNif());
				mapeoRecepcionEMCS.setFecha(RutinasFechas.conversion(mapeoAlbaran.getFechaDocumento()));			
				mapeoRecepcionEMCS.setClaveTabla(mapeoAlbaran.getClave_tabla());
				mapeoRecepcionEMCS.setSerie(mapeoAlbaran.getSerie());
				mapeoRecepcionEMCS.setDocumento(mapeoAlbaran.getDocumento());
				mapeoRecepcionEMCS.setAlbaran(mapeoAlbaran.getIdCodigoCompra());
				
				if(mapeoAlbaran.getArticulo()!=null & mapeoAlbaran.getArticulo().startsWith("0101")) mapeoRecepcionEMCS.setTipo("Granel"); else mapeoRecepcionEMCS.setTipo("PT");  
				
				if (mapeoAlbaran.getUnidades()!=null) mapeoRecepcionEMCS.setLitros(mapeoAlbaran.getUnidades().doubleValue());			
				mapeoRecepcionEMCS.setGrado(null);
				mapeoRecepcionEMCS.setCodigoIdentificacion(1);
				mapeoRecepcionEMCS.setDescripcionIdentificacion("con CAE");
				mapeoRecepcionEMCS.setNumeroReferencia(null);				
				mapeoRecepcionEMCS.setArc(null);
				mapeoRecepcionEMCS.setDestino(null);
				mapeoRecepcionEMCS.setAnada(null);
				mapeoRecepcionEMCS.setObservaciones(null);
				
				vector.add(mapeoRecepcionEMCS);
			}
    	}
    	
    	if (r_mapeo.getEstado().equals("Traslados") || r_mapeo.getEstado().equals("Todas") )
    	{
	    	vectorTraslado= this.obtenerDocumentosTraslado(r_mapeo.getEjercicio(), r_mapeo.getFecha(), null, almacen);
			
			for (int i = 0; i< vectorTraslado.size();i++)
			{
				MapeoLineasAlbaranesTraslado mapeoAlbaranTr = vectorTraslado.get(i);
				MapeoRecepcionEMCS mapeoAsignacionEMCS = new MapeoRecepcionEMCS();
	
				mapeoAsignacionEMCS.setEjercicio(mapeoAlbaranTr.getEjercicio());			
				mapeoAsignacionEMCS.setOrigen(mapeoAlbaranTr.getCae());
				mapeoAsignacionEMCS.setDestinatario(mapeoAlbaranTr.getNombreAlmacen());
				mapeoAsignacionEMCS.setDireccion(mapeoAlbaranTr.getDireccion());			
				mapeoAsignacionEMCS.setCaeDestino(r_mapeo.getCaeDestino());
				mapeoAsignacionEMCS.setCodigoPostal(mapeoAlbaranTr.getCodigoPostal());			
				mapeoAsignacionEMCS.setNif(mapeoAlbaranTr.getNif());
				mapeoAsignacionEMCS.setDocumento(mapeoAlbaranTr.getDocumento());
				mapeoAsignacionEMCS.setFecha(RutinasFechas.conversion(mapeoAlbaranTr.getFechaDocumento()));			
				mapeoAsignacionEMCS.setAlbaran(mapeoAlbaranTr.getIdCodigoTraslado());
				mapeoAsignacionEMCS.setTipo(mapeoAlbaranTr.getArticulo());
				
				mapeoAsignacionEMCS.setClaveTabla("t");
				mapeoAsignacionEMCS.setSerie("t");
				mapeoAsignacionEMCS.setLitros(mapeoAlbaranTr.getUnidades().doubleValue());			
				mapeoAsignacionEMCS.setGrado(null);
				mapeoAsignacionEMCS.setCodigoIdentificacion(1);
				mapeoAsignacionEMCS.setDescripcionIdentificacion("con CAE");
				mapeoAsignacionEMCS.setNumeroReferencia(null);				
				mapeoAsignacionEMCS.setArc(null);
				mapeoAsignacionEMCS.setDestino(null);
				mapeoAsignacionEMCS.setAnada(null);
				mapeoAsignacionEMCS.setObservaciones(null);
				
				vector.add(mapeoAsignacionEMCS);
			}
    	}
//    	if (vector.isEmpty()) vector = null;
		return vector;
	}
	
	public MapeoRecepcionEMCS copiar(MapeoRecepcionEMCS r_mapeo)
	{
		MapeoRecepcionEMCS mapeoGenerado = new MapeoRecepcionEMCS();
		
		/*
		 * obtencion de datos que cambian
		 * 
		 * id interno
		 * ejercicio
		 * fecha
		 * numerador
		 * arc
		 * litros
		 * observaciones
		 * 
		 */
		Date fecha = new Date();
		Long nr = null;
		
		/*
		 * datos del nuevo registro que actualizamos
		 */
		mapeoGenerado.setFecha(fecha);
		mapeoGenerado.setNumeroReferencia(nr);
		mapeoGenerado.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
		mapeoGenerado.setArc("");
		mapeoGenerado.setObservaciones("");
		mapeoGenerado.setDestino("");
		mapeoGenerado.setAnada("");
		mapeoGenerado.setLitros(new Double(0));
		mapeoGenerado.setGrado(new Double(0));
		mapeoGenerado.setAlbaran(null);
		mapeoGenerado.setDocumento("");
		mapeoGenerado.setClaveTabla("");
		mapeoGenerado.setSerie("");
		mapeoGenerado.setTipo("");
		
		/*
		 * Datos que mantenemos del origen
		 */
		mapeoGenerado.setCodigoIdentificacion(r_mapeo.getCodigoIdentificacion());
		mapeoGenerado.setDescripcionIdentificacion(r_mapeo.getDescripcionIdentificacion());
		mapeoGenerado.setOrigen(r_mapeo.getOrigen());
		mapeoGenerado.setDestinatario(r_mapeo.getDestinatario());
		mapeoGenerado.setDireccion(r_mapeo.getDireccion());
		mapeoGenerado.setCodigoPostal(r_mapeo.getCodigoPostal());
		mapeoGenerado.setCaeDestino(r_mapeo.getCaeDestino());
		mapeoGenerado.setNif(r_mapeo.getNif());
		
		return mapeoGenerado;
	}
	
	public Integer obtenerSiguiente()
	{
		Connection con = null;
		Integer rdo = null;
		try
		{
			con= this.conManager.establecerConexionInd();
			rdo=  obtenerSiguienteCodigoInterno(con, this.tableName, this.identificadorTabla);
		}
		catch (Exception ex)
		{
			rdo=1;
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return rdo;
	}

	public String guardarNuevo(MapeoRecepcionEMCS r_mapeo)
	{
		boolean actualizarGreensys = false;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO " + this.tableName + "( ");
			cadenaSQL.append("id,");			
			cadenaSQL.append("ejercicio, ");			
			cadenaSQL.append("numeroReferencia, ");
			cadenaSQL.append("fecha, ");
			cadenaSQL.append("caeExpedidor, ");
			cadenaSQL.append("nombreExpedidor, ");
			cadenaSQL.append("direccionExpedidor, ");
			cadenaSQL.append("nifExpedidor, ");
			cadenaSQL.append("caeDestino, ");			
			cadenaSQL.append("cpExpedidor,");
			cadenaSQL.append("codigo,");
			cadenaSQL.append("arc,");
			cadenaSQL.append("grado,");
			cadenaSQL.append("litros,");
			cadenaSQL.append("destino, ");
			cadenaSQL.append("observaciones, ");
			cadenaSQL.append("documento, ");
			cadenaSQL.append("serie, ");
			cadenaSQL.append("claveTabla, ");
			cadenaSQL.append("albaran, ");
			cadenaSQL.append("tipo, ");
			cadenaSQL.append("anada ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    String fecha = "";
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getNumeroReferencia()!=null)
		    {
		    	preparedStatement.setLong(3, r_mapeo.getNumeroReferencia());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(fecha));
		    
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }		    
		    if (r_mapeo.getDestinatario()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDestinatario());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getDireccion()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getDireccion());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getCaeDestino()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getCaeDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getCodigoPostal()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getCodigoPostal());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcionIdentificacion()!=null)
		    {
		    	r_mapeo.setCodigoIdentificacion(this.obtenerCodigo(r_mapeo.getDescripcionIdentificacion()));		    	
		    	preparedStatement.setInt(11, this.obtenerCodigo(r_mapeo.getDescripcionIdentificacion()));
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getArc()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getArc());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getGrado()!=null)
		    {
		    	preparedStatement.setDouble(13, r_mapeo.getGrado());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(14, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    
		    if (r_mapeo.getDestino()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }

		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getClaveTabla()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getClaveTabla());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran() >0)
		    {
		    	preparedStatement.setInt(20, r_mapeo.getAlbaran());
//		    	serNotif.mensajeAdvertencia("Recepcion EMCS server. Recuerda activar la conexion con GREENsys");		    	
		    	actualizarGreensys=true;
		    }
		    else
		    {
		    	preparedStatement.setInt(20, 0);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(21, null);
		    }
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(22, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(22, null);
		    }

		    preparedStatement.executeUpdate();
	        if (actualizarGreensys) this.actualizarGreensys(r_mapeo.getEjercicio(), r_mapeo.getClaveTabla(), r_mapeo.getDocumento(), r_mapeo.getSerie(), r_mapeo.getAlbaran());
	        
		}
	    catch (Exception ex)
	    {
//	    	serNotif.mensajeError(ex.getMessage());
	    	ex.printStackTrace();
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	private void actualizarGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				cats.marcarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			default:
			{
				consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
				cacs.marcarCabeceraAlbaranEMCS(r_ejercicio, r_albaran);
			}
		}
			
	}
	
	public void marcarAlbaranGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				cats.marcarAlbaranSilicie(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);
			}
			default:
			{
				consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
				cacs.marcarAlbaranSilicie(r_ejercicio, r_albaran);
			}
		}
			
	}
	
	private void liberarGreensys(Integer r_ejercicio, String r_clave_tabla, String r_documento, String r_serie, Integer r_albaran)
	{
		switch (r_clave_tabla)
		{
			case "T":
			case "t":
			{
				consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
				cats.liberarCabeceraAlbaranEMCS(r_ejercicio, r_clave_tabla, r_documento, r_serie, r_albaran);				
			}
			default:
			{
				consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
				cacs.liberarCabeceraAlbaranEMCS(r_ejercicio, r_albaran);
			}
		}
	}
	
	public String guardarCambios(MapeoRecepcionEMCS r_mapeo, MapeoRecepcionEMCS r_mapeo_orig)
	{
		boolean actualizarGreensys = false;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		try
		{
			
			cadenaSQL.append(" UPDATE "+ this.tableName + " set ");
			cadenaSQL.append(" fecha = ?, ");
			cadenaSQL.append(" caeExpedidor = ?, ");
			cadenaSQL.append(" nombreExpedidor = ?, ");
			cadenaSQL.append(" direccionExpedidor = ?, ");			
			cadenaSQL.append(" cpExpedidor = ?,");
			cadenaSQL.append(" caeDestino = ?, ");
			cadenaSQL.append(" nifExpedidor = ?, ");

			cadenaSQL.append(" codigo = ?,");
			cadenaSQL.append(" numeroReferencia = ?,");			
			cadenaSQL.append(" arc = ?,");
			cadenaSQL.append(" grado = ?,");
			cadenaSQL.append(" litros = ?,");
			cadenaSQL.append(" destino = ?,");
			cadenaSQL.append(" observaciones = ?,");
			cadenaSQL.append(" documento  = ?,");
			cadenaSQL.append(" serie = ?,");
			cadenaSQL.append(" claveTabla  = ?,");
			cadenaSQL.append(" albaran = ?, ");
			cadenaSQL.append(" tipo = ?, ");
			cadenaSQL.append(" anada = ? ");
			cadenaSQL.append(" WHERE " + this.tableName + ".id = ? " );
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    String fecha =null;
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(1, RutinasFechas.convertirAFechaMysql(fecha));
		    
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDestinatario()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDestinatario());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDireccion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDireccion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    
		    if (r_mapeo.getCodigoPostal()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getCodigoPostal());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCaeDestino()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getCaeDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getNif()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getNif());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getDescripcionIdentificacion()!=null)
		    {
		    	r_mapeo.setCodigoIdentificacion(this.obtenerCodigo(r_mapeo.getDescripcionIdentificacion()));
		    	preparedStatement.setInt(8, this.obtenerCodigo(r_mapeo.getDescripcionIdentificacion()));
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getNumeroReferencia()!=null)
		    {
		    	preparedStatement.setLong(9, r_mapeo.getNumeroReferencia());
		    }
		    else
		    {
		    	preparedStatement.setLong(9, 0);
		    }

		    if (r_mapeo.getArc()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArc());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    
		    
		    if (r_mapeo.getGrado()!=null)
		    {
		    	preparedStatement.setDouble(11, r_mapeo.getGrado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(11,0);
		    }

		    
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setDouble(12, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }

		    if (r_mapeo.getDestino()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getDestino());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }

		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    
		    if (r_mapeo.getDocumento()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getDocumento());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getClaveTabla()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getClaveTabla());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    
		    if (r_mapeo.getAlbaran()!=null && r_mapeo.getAlbaran() >0)
		    {
		    	preparedStatement.setInt(18, r_mapeo.getAlbaran());
		    	
//		    	serNotif.mensajeAdvertencia("Recepcion EMCS server. Recuerda activar la conexion con GREENsys");		    	
		    	actualizarGreensys=true;
		    }
		    else
		    {
		    	preparedStatement.setInt(18, 0);
		    	if (r_mapeo_orig!=null && r_mapeo_orig.getAlbaran() != 0) this.liberarGreensys(r_mapeo_orig.getEjercicio(), r_mapeo_orig.getClaveTabla(), r_mapeo_orig.getDocumento(), r_mapeo_orig.getSerie(), r_mapeo_orig.getAlbaran());
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    preparedStatement.setInt(21, r_mapeo.getIdCodigo());

		    
		    preparedStatement.executeUpdate();
		    
		    if (actualizarGreensys)
		    {
		    	if (!r_mapeo.getAlbaran().equals(r_mapeo_orig.getAlbaran()))
		    	{
		    		if (r_mapeo_orig!=null && r_mapeo_orig.getAlbaran() != 0) this.liberarGreensys(r_mapeo_orig.getEjercicio(), r_mapeo_orig.getClaveTabla(), r_mapeo_orig.getDocumento(), r_mapeo_orig.getSerie(), r_mapeo_orig.getAlbaran());
		    		this.actualizarGreensys(r_mapeo.getEjercicio(), r_mapeo.getClaveTabla(), r_mapeo.getDocumento(), r_mapeo.getSerie(), r_mapeo.getAlbaran());
		    	}
		    }
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoRecepcionEMCS r_mapeo)
	{
		Connection con = null;
		try
		{
			con= this.conManager.establecerConexionInd();	          

			if (r_mapeo!=null && r_mapeo.getAlbaran() != 0) this.liberarGreensys(r_mapeo.getEjercicio(), r_mapeo.getClaveTabla(), r_mapeo.getDocumento(), r_mapeo.getSerie(), r_mapeo.getAlbaran());
			
			this.eliminarRegistro(con, "fin_emcs_ter_var", "idEntrada", r_mapeo.getIdCodigo());
			this.eliminarRegistro(con, this.tableName, this.identificadorTabla, r_mapeo.getIdCodigo());
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public ArrayList<MapeoLineasAlbaranesCompras> obtenerDocumentosCompras(Integer r_ejercicio, String r_nif, Date r_fecha, String r_almacen)
	{
		ArrayList<MapeoLineasAlbaranesCompras> vector = null;
		MapeoLineasAlbaranesCompras mapeo = null;
		
		consultaAlbaranesComprasServer cac = new consultaAlbaranesComprasServer(CurrentUser.get());
		
		mapeo=new MapeoLineasAlbaranesCompras();
		
		mapeo.setNif(r_nif);
		mapeo.setAlmacen(r_almacen);
		mapeo.setFechaDocumento(r_fecha);
		
		vector = cac.datosOpcionesGlobalSinEMCS(r_ejercicio, "E", "E1", "E", null, r_fecha, r_almacen);
		
		return vector;
	}
	
	public ArrayList<MapeoLineasAlbaranesTraslado> obtenerDocumentosTraslado(Integer r_ejercicio, Date r_fecha, String r_almacen,String r_almacenDestino)
	{
		ArrayList<MapeoLineasAlbaranesTraslado> vector = null;
		consultaAlbaranesTrasladoServer cac = new consultaAlbaranesTrasladoServer(CurrentUser.get());
		
		vector = cac.datosOpcionesGlobalSumaSinEMCS(r_ejercicio,"t","T1", null, null,r_fecha,r_almacen, r_almacenDestino);
		
		return vector;
	}

	@Override
	public String semaforos() 
	{
		String semaforo= "0";
		
		consultaAlbaranesTrasladoServer cats = consultaAlbaranesTrasladoServer.getInstance(CurrentUser.get());
		semaforo = cats.semaforos();
		
		if (semaforo.equals("0"))
		{
			consultaAlbaranesComprasServer cacs = consultaAlbaranesComprasServer.getInstance(CurrentUser.get());
			semaforo = cacs.semaforos();
		}
		return semaforo;
	}
	
	private Integer obtenerCodigo(String r_codigo)
	{

		switch (r_codigo)
		{
			case "con CAE":
			{
				return 1;
			}
			case "sin CAE":
			{
				return 2;
			}
			case "Exportación":
			{
				return 3;
			}
			case "Desconocido (art 29.B.1. Reglamento IIEE)":
			{
				return 4;
			}
		}
		return null;
	}
	
	private String obtenerCodigo(Integer r_codigo)
	{

		switch (r_codigo)
		{
			case 1:
			{
				return "con CAE";
			}
			case 2:
			{
				return "sin CAE";
			}
			case 3:
			{
				return "Exportación";
			}
			case 4:
			{
				return "Desconocido (art 29.B.1. Reglamento IIEE)";
			}
		}
		return null;
	}
	
	public MapeoRecepcionEMCS copiar(MapeoRecepcionEMCS r_mapeo, boolean r_nuevo)
	{
		MapeoRecepcionEMCS mapeoGenerado = new MapeoRecepcionEMCS();
		Long nr =null;
		/*
		 * obtencion de datos que cambian
		 * 
		 * id interno
		 * ejercicio
		 * fecha
		 * numerador
		 * arc
		 * litros
		 * observaciones
		 * 
		 */
		Date fecha=null;
		
		if (r_nuevo)
		{
			fecha = new Date();
		}
		
		/*
		 * datos del nuevo registro que actualizamos
		 */
		mapeoGenerado.setFecha(fecha);
		mapeoGenerado.setNumeroReferencia(nr);
		mapeoGenerado.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
		mapeoGenerado.setArc(null);
		mapeoGenerado.setObservaciones(null);
		mapeoGenerado.setLitros(new Double(0));
		mapeoGenerado.setAlbaran(0);
		mapeoGenerado.setClaveTabla(null);
		mapeoGenerado.setDocumento(null);
		mapeoGenerado.setSerie(null);
		mapeoGenerado.setTipo(null);
		mapeoGenerado.setDestino(null);
		mapeoGenerado.setAnada(null);
		
		/*
		 * Datos que mantenemos del origen
		 */
		mapeoGenerado.setCodigoIdentificacion(r_mapeo.getCodigoIdentificacion());
		mapeoGenerado.setDescripcionIdentificacion(r_mapeo.getDescripcionIdentificacion());
//		mapeoGenerado.setArea(r_mapeo.getArea());
		mapeoGenerado.setOrigen(r_mapeo.getOrigen());
		mapeoGenerado.setDestinatario(r_mapeo.getDestinatario());
		mapeoGenerado.setCodigoPostal(r_mapeo.getCodigoPostal());
		mapeoGenerado.setCaeDestino(r_mapeo.getCaeDestino());
		mapeoGenerado.setDireccion(r_mapeo.getDireccion());
		mapeoGenerado.setNif(r_mapeo.getNif());
		
		return mapeoGenerado;
	}
	
	public ArrayList<MapeoRecepcionEMCSVariedades> datosEenlaceEntradaVariedad(MapeoRecepcionEMCSVariedades r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoRecepcionEMCSVariedades> vector = null;
		StringBuffer cadenaWhere = null;
		Connection con = null;
		Statement cs = null;

			
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT fin_emcs_ter_var.id ter_id, ");
		cadenaSQL.append(" fin_emcs_ter_var.idEntrada ter_ent,");
		cadenaSQL.append(" fin_emcs_ter_var.idVariedad ter_var,");
		cadenaSQL.append(" fin_emcs_ter_var.porcentaje ter_por, ");
		cadenaSQL.append(" fin_emcs_ter_var.litros ter_lit, ");
		cadenaSQL.append(" dga_variedades_uva.descripcion var_nom ");
		cadenaSQL.append(" from fin_emcs_ter_var ");
		cadenaSQL.append(" inner join dga_variedades_uva on fin_emcs_ter_var.idVariedad = dga_variedades_uva.id ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fin_emcs_ter_var.id = " + r_mapeo.getIdCodigo() + " ");
				}
				if (r_mapeo.getIdEntrada()!=null && r_mapeo.getIdEntrada()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fin_emcs_ter_var.idEntrada = " + r_mapeo.getIdEntrada() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by porcentaje desc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoRecepcionEMCSVariedades>();
			
			while(rsOpcion.next())
			{
				MapeoRecepcionEMCSVariedades mapeo = new MapeoRecepcionEMCSVariedades();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("ter_id"));
				mapeo.setIdEntrada(rsOpcion.getInt("ter_ent"));
				mapeo.setIdVariedad(rsOpcion.getInt("ter_var"));
				mapeo.setPorcentaje(rsOpcion.getString("ter_por"));
				mapeo.setLitros(rsOpcion.getString("ter_lit"));
				mapeo.setNombre(rsOpcion.getString("var_nom"));
				
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());		
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}
	public String guardarEnlaceGranelVariedad(MapeoRecepcionEMCSVariedades r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		try
		{
			
			cadenaSQL.append(" INSERT INTO fin_emcs_ter_var( ");
			cadenaSQL.append("id,");			
			cadenaSQL.append("idEntrada, ");			
			cadenaSQL.append("idVariedad, ");
			cadenaSQL.append("porcentaje, ");
			cadenaSQL.append("litros ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer id = obtenerSiguienteCodigoInterno(con, "fin_emcs_ter_var", "id");
		    preparedStatement.setInt(1, id);
		    
		    if (r_mapeo.getIdEntrada()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getIdEntrada());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setInt(3, obtenerIdVariedad(r_mapeo.getNombre()));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getPorcentaje()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getPorcentaje());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }		    
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }		    

		    preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	ex.printStackTrace();
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambiosEnlaceEntradaVariedad(MapeoRecepcionEMCSVariedades r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		try
		{
			
			cadenaSQL.append(" UPDATE fin_emcs_ter_var set ");
			cadenaSQL.append(" porcentaje = ?,");
			cadenaSQL.append(" litros = ?");
			cadenaSQL.append(" WHERE fin_emcs_ter_var.id = ? " );
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setString(1, r_mapeo.getPorcentaje());
		    preparedStatement.setString(2, r_mapeo.getLitros());
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());

		    preparedStatement.executeUpdate();
		    
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	public void eliminarEnlaceEntradaVariedad(MapeoRecepcionEMCSVariedades r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM fin_emcs_ter_var ");            
			cadenaSQL.append(" WHERE fin_emcs_ter_var.idEntrada = ?"); 
			cadenaSQL.append(" and fin_emcs_ter_var.idVariedad = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdEntrada());
			preparedStatement.setInt(2, obtenerIdVariedad(r_mapeo.getNombre()));
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	private Integer obtenerIdVariedad(String r_nombre)
	{
		consultaVariedadesDgaServer cvs = consultaVariedadesDgaServer.getInstance(CurrentUser.get());
		Integer var = cvs.obtenerIdVariedad(r_nombre);
		return var;
	}
}


//private boolean silicieActivo()
//{
//	return true;
	/*
	 * obs_envio2
	 * establecemos el valor "S" cuando hemos enviado el mail
	 * 
	 */
//	ResultSet rsOpcion = null;
//	StringBuffer cadenaSQL = new StringBuffer();  
//	
//	try
//	{
//        
//		cadenaSQL.append(" select valor_actual from e_l_parm where grupo='BORSAO' and codigo='SILICIE' ");
//	    
//		con= this.conManager.establecerConexionGestion();			
//		cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
//		rsOpcion= cs.executeQuery(cadenaSQL.toString());
//		
//		while(rsOpcion.next())
//		{
//			if (new Integer(rsOpcion.getString("valor_actual").trim())>0) return true; else return false;
//		}
//		
//		
//	}
//    catch (Exception ex)
//    {
//    	serNotif.mensajeError(ex.getMessage());
//    	return false;
//    }
//	return false;
//}