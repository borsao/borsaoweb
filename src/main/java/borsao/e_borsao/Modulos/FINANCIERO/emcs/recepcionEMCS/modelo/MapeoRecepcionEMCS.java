package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoRecepcionEMCS extends MapeoGlobal
{
	private Integer ejercicio;
	private Long numeroReferencia;
	private Integer codigoIdentificacion;
	
	private Double litros;
	private Double grado;
	
	private Integer mes;	
	private String origen;
	private String caeDestino;
	private String destinatario;
	private String direccion;
	private String codigoPostal;
	private String nif;
	private String destino;
	private String observaciones;
	private String arc;
	private String descripcionIdentificacion;
	private String tipo;
	private String serie;
	private String claveTabla;
	private String anada;

	private String documento;
	private String documentoFalso;
	private Integer albaran;

	private String estado;
	private String doc;
	private String var;


//	private String esc;
//	private String idPrg;

	private Date fecha;
	
	public MapeoRecepcionEMCS()
	{
		this.setDoc("");
		this.setDestinatario("");
		this.setNif("");
		this.setCaeDestino("");
		this.setDireccion("");
		this.setCodigoPostal("");
		this.setObservaciones("");
		this.setDestino("");
		this.setArc("");		
		this.setDescripcionIdentificacion("con CAE");
		this.setCodigoIdentificacion(1);
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Long getNumeroReferencia() {
		return numeroReferencia;
	}

	public void setNumeroReferencia(Long numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

//	public Integer getIdDestinatario() {
//		return idDestinatario;
//	}
//
//	public void setIdDestinatario(Integer idDestinatario) {
//		this.idDestinatario = idDestinatario;
//	}

	public Integer getCodigoIdentificacion() {
		return codigoIdentificacion;
	}

	public void setCodigoIdentificacion(Integer codigoIdentificacion) {
		this.codigoIdentificacion = codigoIdentificacion;
	}

	public Double getLitros() {
		return litros;
	}

	public void setLitros(Double litros) {
		this.litros = litros;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getCaeDestino() {
		return caeDestino;
	}

	public void setCaeDestino(String caeDestino) {
		this.caeDestino = caeDestino;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getArc() {
		return arc;
	}

	public void setArc(String arc) {
		this.arc = arc;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDescripcionIdentificacion() {
		return descripcionIdentificacion;
	}

	public void setDescripcionIdentificacion(String descripcionIdentificacion) {
		this.descripcionIdentificacion = descripcionIdentificacion;
	}

	public Double getGrado() {
		return grado;
	}

	public void setGrado(Double grado) {
		this.grado = grado;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getDocumento() {
		return documento;
	}

	public String getDocumentoFalso() {
		return documento;
	}
	
	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getAlbaran() {
		return albaran;
	}

	public void setAlbaran(Integer albaran) {
		this.albaran = albaran;
	}

	public String getDoc() {
		return doc;
	}

	public String getVar() {
		return var;
	}

	public Integer getMes() {
		return new Integer(RutinasFechas.mesFecha(getFecha()));
	}


	public void setDoc(String doc) {
		this.doc = doc;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getClaveTabla() {
		return claveTabla;
	}

	public void setClaveTabla(String claveTabla) {
		this.claveTabla = claveTabla;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAnada() {
		return anada;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}
}


