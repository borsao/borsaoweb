package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.MapeoSeguimientoEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.SeguimientoEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.server.consultaSeguimientoEMCSServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SeguimientoEMCSView extends GridViewRefresh {

	public static final String VIEW_NAME = "Seguimiento EMCS";
	public consultaSeguimientoEMCSServer cus =null;
	public boolean modificando = false;
	public String tituloGrid = "Seguimiento";
	public boolean conTotales = false;
	public MapeoSeguimientoEMCS mapeoSeguimientoEMCSEnlazado=null;
	public ComboBox cmbEstado = null;
	public ComboBox cmbOrigen = null;	
	public ComboBox cmbArea = null;
	public ComboBox cmbEjercicio = null;
	public String almacenSeleccionado=null;
	
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private OpcionesForm form = null;
	
	private HashMap<String,String> filtrosRec = null;
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
	private Button opcCopiar = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public SeguimientoEMCSView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
    	this.cus = new consultaSeguimientoEMCSServer(CurrentUser.get());
    	
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);

		this.cmbEjercicio= new ComboBox("Ejercicio");    		
		this.cmbEjercicio.setNewItemsAllowed(false);
		this.cmbEjercicio.setNullSelectionAllowed(false);
		this.cmbEjercicio.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cmbEstado= new ComboBox("Estado");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);
	
		this.opcCopiar= new Button("Copiar");    	
		this.opcCopiar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcCopiar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCopiar.setIcon(FontAwesome.COPY);

		this.cmbArea= new ComboBox("Tipo");    		
		this.cmbArea.setNewItemsAllowed(false);
		this.cmbArea.setNullSelectionAllowed(false);
		this.cmbArea.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cmbOrigen= new ComboBox("Expedidor");    		
		this.cmbOrigen.setNewItemsAllowed(false);
		this.cmbOrigen.setNullSelectionAllowed(false);
		this.cmbOrigen.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cabLayout.addComponent(this.cmbEjercicio);
		this.cabLayout.addComponent(this.cmbEstado);	
		this.cabLayout.addComponent(this.cmbArea);
		this.cabLayout.addComponent(this.cmbOrigen);
		this.cabLayout.addComponent(this.opcCopiar);
		this.cabLayout.setComponentAlignment(this.opcCopiar,Alignment.BOTTOM_LEFT);
		
		this.cargarCombo();
		this.cargarComboEjercicio();
		this.cargarListeners();
		this.establecerModo();
		
		lblTitulo.setValue(SeguimientoEMCSView.VIEW_NAME);//, ContentMode.HTML);
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSeguimientoEMCS> r_vector=null;
    	MapeoSeguimientoEMCS mapeoSeguimientoEMCS =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoSeguimientoEMCS=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	mapeoSeguimientoEMCS.setOrigen(this.cmbOrigen.getValue().toString());
    	mapeoSeguimientoEMCS.setEjercicio(new Integer(this.cmbEjercicio.getValue().toString()));

    	r_vector=this.cus.datosSeguimientoEMCSGlobal(mapeoSeguimientoEMCS);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((SeguimientoEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((SeguimientoEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		this.establecerModo();
//    		setHayGrid(true);    		
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((SeguimientoEMCSGrid) this.grid).activadaVentanaPeticion && !((SeguimientoEMCSGrid) this.grid).ordenando)
    	{
    		this.modificando=true;
    		this.verForm(false);
    		this.form.editarSeguimientoEMCS((MapeoSeguimientoEMCS) r_fila);
    	}    		
    }
    
    public void generarGrid(MapeoSeguimientoEMCS r_mapeo)
    {
    	ArrayList<MapeoSeguimientoEMCS> r_vector=null;
    	r_vector=this.cus.datosSeguimientoEMCSGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoSeguimientoEMCS> r_vector)
    {
    	grid = new SeguimientoEMCSGrid(this,r_vector);
    	if (((SeguimientoEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((SeguimientoEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    		((SeguimientoEMCSGrid) grid).establecerFiltros(filtrosRec);
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
		this.cmbOrigen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
				}
				if (form!=null && form.isVisible()) removeStyleName("visible");
//				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (cmbOrigen.getValue()!=null) mapeo.setOrigen(cmbOrigen.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				
				if (!cmbOrigen.getValue().toString().equals("Todos"))
				{
					almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbOrigen.getValue().toString());
				}
				else
				{
					almacenSeleccionado = "Todos";
				}
				
				tituloGrid = "Seguimiento " + cmbArea.getValue().toString() + " " + almacenSeleccionado;
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("origen", cmbOrigen.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
				}
				if (form!=null && form.isVisible()) removeStyleName("visible");
//				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (cmbOrigen.getValue()!=null) mapeo.setOrigen(cmbOrigen.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("origen", cmbOrigen.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
				}
				if (form!=null && form.isVisible()) removeStyleName("visible");
//				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (cmbOrigen.getValue()!=null) mapeo.setOrigen(cmbOrigen.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("origen", cmbOrigen.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		}); 

		this.cmbArea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{	
					filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
				}
				if (form!=null && form.isVisible()) removeStyleName("visible");
//				if (form!=null) form.cerrar();
				MapeoSeguimientoEMCS mapeo = new MapeoSeguimientoEMCS();
				if (cmbOrigen.getValue()!=null) mapeo.setOrigen(cmbOrigen.getValue().toString());
				if (cmbArea.getValue()!=null) mapeo.setArea(cmbArea.getValue().toString());
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				if (!cmbOrigen.getValue().toString().equals("Todos"))
				{
					almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbOrigen.getValue().toString());
				}
				else
				{
					almacenSeleccionado = "Todos";
				}
				
				tituloGrid = "Seguimiento " + cmbArea.getValue().toString() + " " + almacenSeleccionado;
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("area", cmbArea.getValue().toString());
				opcionesEscogidas.put("origen", cmbOrigen.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		}); 
		
    	this.opcCopiar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				if (form!=null) form.cerrar();
				if (form!=null && form.isVisible()) removeStyleName("visible");
				MapeoSeguimientoEMCS mapeo=(MapeoSeguimientoEMCS) grid.getSelectedRow();
				MapeoSeguimientoEMCS mapeoGenerado = null;
				
				if (mapeo!=null)
				{
					mapeoGenerado = cus.copiar(mapeo,true);

					if (mapeoGenerado!=null)
					{
						modificando=false;
						verForm(false);
						grid.setEnabled(false);
						form.editarSeguimientoEMCS(mapeoGenerado);
						form.creacion=true;
					}
					else
						Notificaciones.getInstance().mensajeError("No ha podido realizarse la copia");
				}			
				else
				{
					Notificaciones.getInstance().mensajeError("No se han encontrado datos a copiar");
				}
			}
		});
    	
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 */
		this.verBotones(true);
		this.activarBotones(true);
		this.navegacion(true);
				
		this.opcNuevo.setVisible(true);
		this.opcNuevo.setEnabled(true);
		setSoloConsulta(this.soloConsulta);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCopiar.setEnabled(r_activo);
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCopiar.setVisible(r_visibles);
	}
	
	public void imprimirSeguimientoEMCS()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
	}
	
	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicio.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicio.setValue(vector.get(0).toString());
	}

	private void cargarCombo()
	{
		
		this.cmbEstado.removeAllItems();
		
		this.cmbEstado.addItem("Borrador");
		this.cmbEstado.addItem("Todos");
		this.cmbEstado.setValue("Borrador");
		this.opcionesEscogidas.put("estado", cmbEstado.getValue().toString());

		this.cmbArea.removeAllItems();
		this.cmbArea.addItem("EMCS Europa");
		this.cmbArea.addItem("EMCS Interno");
				
		this.cmbArea.setValue("EMCS Interno");
		this.opcionesEscogidas.put("area", this.cmbArea.getValue().toString());
		
		
		
		this.tituloGrid = this.tituloGrid + " " + "EMCS Interno"; 
		
		this.cmbOrigen.removeAllItems();
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		String almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			if (caes.get(i)!=null && caes.get(i).length()>0) this.cmbOrigen.addItem(caes.get(i).trim());
		}
		
		this.cmbOrigen.addItem("Todos");
		
		String caeDefecto = cas.obtenerCAEAlmacen(almacenDefecto);
		
		if (caeDefecto!=null)
		{
			
			this.cmbOrigen.setValue(caeDefecto.trim());
			this.almacenSeleccionado=cas.obtenerDescripcionAlmacen(almacenDefecto);
			this.tituloGrid = this.tituloGrid + " " + this.almacenSeleccionado; 
			this.opcionesEscogidas.put("origen", this.cmbOrigen.getValue().toString());
		}
	}
	
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
		switch (r_accion)
		{
			case "Eliminar":
			{
				consultaSeguimientoEMCSServer cus = new consultaSeguimientoEMCSServer(CurrentUser.get());	
				((SeguimientoEMCSGrid) this.grid).remove(mapeoSeguimientoEMCSEnlazado);
				cus.eliminar(mapeoSeguimientoEMCSEnlazado);
				this.form.cerrar();
				break;
			}
			case "Reintentar":
			{
				consultaSeguimientoEMCSServer cse = new consultaSeguimientoEMCSServer(CurrentUser.get());
				Long nr = cse.obtenerNumerador(new Integer(RutinasCadenas.quitarPuntoMiles(mapeoSeguimientoEMCSEnlazado.getEjercicio().toString())), mapeoSeguimientoEMCSEnlazado.getArea(), mapeoSeguimientoEMCSEnlazado.getOrigen());
				this.form.numeroReferencia.setValue(nr.toString());
				mapeoSeguimientoEMCSEnlazado.setNumeroReferencia(nr);
		    	this.form.addStyleName("visible");
		    	this.form.setEnabled(true);
		    	this.form.creacion=true;
		    	this.form.fieldGroup.setItemDataSource(new BeanItem<MapeoSeguimientoEMCS>(mapeoSeguimientoEMCSEnlazado));
		    	this.form.desactivarCampos(mapeoSeguimientoEMCSEnlazado.getAlbaran());

				break;
			}
		}
	}

	public void cerrarVentanaBusqueda(String r_clave_tabla, String r_documento, String r_serie, Integer r_codigo, String r_arc, Double r_litros)
	{
		MapeoSeguimientoEMCS mapeo = (MapeoSeguimientoEMCS) this.grid.getSelectedRow();

		if (mapeo!=null)
		{
			MapeoSeguimientoEMCS mapeo_orig = this.cus.copiar(mapeo, false);
		
			mapeo_orig.setClaveTabla(mapeo.getClaveTabla());
			mapeo_orig.setDocumento(mapeo.getDocumento());
			mapeo_orig.setSerie(mapeo.getSerie());
			mapeo_orig.setAlbaran(mapeo.getAlbaran());
			mapeo_orig.setArc(mapeo.getArc());	
			mapeo_orig.setLitros(mapeo.getLitros());	
			
			mapeo.setClaveTabla(r_clave_tabla);
			mapeo.setDocumento(r_documento);
			mapeo.setSerie(r_serie);
			mapeo.setAlbaran(r_codigo);
			mapeo.setArc(r_arc);
			mapeo.setLitros(r_litros);
			
			this.cus.guardarCambios(mapeo,mapeo_orig);
			
			if (isHayGrid())
			{
				filtrosRec = ((SeguimientoEMCSGrid) grid).recogerFiltros();
				grid.removeAllColumns();			
				barAndGridLayout.removeComponent(grid);
				grid=null;
				setHayGrid(false);
				generarGrid(opcionesEscogidas);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Ocurrió un poroblema al seleccionar el registro original");
		}
	}
	
	@Override
	public void cancelarProceso(String r_accion) {
		this.form.cerrar();
	}
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(SeguimientoEMCSView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(SeguimientoEMCSView.VIEW_NAME, this.getClass());
	}
	
}


