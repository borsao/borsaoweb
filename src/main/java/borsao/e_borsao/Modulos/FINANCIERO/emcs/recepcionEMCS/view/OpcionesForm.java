package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.RecepcionEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.RetrabajosGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoRecepcionEMCS mapeo = null;
	 public String gradoNuevo = null;
	 public String litrosNuevo = null;
	 
	 private GridViewRefresh uw = null;
	 public MapeoRecepcionEMCS mapeoModificado = null;
	 public BeanFieldGroup<MapeoRecepcionEMCS> fieldGroup;
//	 private ArrayList<MapeoLineasAlbaranesCompras> vectorAlbaranes = null;
//	 private ArrayList<MapeoLineasAlbaranesTraslado> vectorTraslados = null;
	 
	 public OpcionesForm(GridViewRefresh r_uw) {
		super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoRecepcionEMCS>(MapeoRecepcionEMCS.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fecha.addStyleName("v-datefield-textfield");
        this.fecha.setDateFormat("dd/MM/yyyy");
        this.fecha.setShowISOWeekNumbers(true);
        this.fecha.setResolution(Resolution.DAY);

//        this.grado.setBuffered(true);
        this.litros.setLocale(new Locale("es_ES"));
        this.grado.setLocale(new Locale("es_ES"));
        this.direccion.setVisible(false);
        this.codigoPostal.setVisible(false);
        
//        this.descripcionIdentificacion.setVisible(false);
    }   

	private void cargarValidators()
	{
		this.grado.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (grado.getValue()!=null) gradoNuevo = grado.getValue().toString().replace(".", ",");	
			}
		});
		this.litros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (litros.getValue()!=null) litrosNuevo = litros.getValue().toString().replace(".", ",");	
			}
		});
		
		this.arc.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (arc.getValue()!=null && arc.getValue()!="0" && arc.getValue().length()>0 && arc.getValue().length()!=21)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. 21 Posiciones");					
					arc.focus();	
				}
			}
		});
		
		this.numeroReferencia.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				// 
				if (numeroReferencia.getValue()!=null && numeroReferencia.getValue() != "0" && numeroReferencia.getValue().length()>0 && numeroReferencia.getValue().replace(".", "").length()!=11)
				{
					Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. 11 Posiciones");
					numeroReferencia.focus();	
				}	
			}
		});
//		
//		this.grado.addValidator(new Validator() {
//			
//			@Override
//			public void validate(Object value) throws InvalidValueException {
//				grado.getValue().toString().replace(".", ",");		
//			}
//		});
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoRecepcionEMCS) uw.grid.getSelectedRow();
                if (mapeo.getAlbaran()!=null && mapeo.getAlbaran()!=0) Notificaciones.getInstance().mensajeError("No se puede eliminar. Albaran asociado"); else eliminarRecepcionEMCS(mapeo);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).modificando=false; else ((AsignacionEntradasEMCSView) uw).modificando=false; 
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 				uw.setHayGrid(false);
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).modificando=false; else ((AsignacionEntradasEMCSView) uw).modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);
		 				if (grado.getValue()!=null) mapeo.setGrado(Double.valueOf(grado.getValue().replace(",", "")));
		 				if (litros.getValue()!=null) mapeo.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		                crearRecepcionEMCS(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 			}
	 			else
	 			{
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoRecepcionEMCS mapeo_orig= (MapeoRecepcionEMCS) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo();
		 				
		 				if (mapeo_orig!=null)
		 				{
		 					HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 					mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 					
		 					mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 					mapeo.setClaveTabla(mapeo_orig.getClaveTabla());
		 					mapeo.setSerie(mapeo_orig.getSerie());
		 					mapeo.setDocumento(mapeo_orig.getDocumento());
		 					mapeo.setTipo(mapeo_orig.getTipo());
		 					mapeo.setGrado(Double.valueOf(grado.getValue().replace(",", "")));		 				
		 					mapeo.setLitros(Double.valueOf(litros.getValue().replace(",", "")));
		 					modificarRecepcionEMCS(mapeo,mapeo_orig);
		 				}
		 				hm=null;
			           	removeStyleName("visible");
	 				}
	 			}
	 			if (((RecepcionEMCSGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 				uw.grid.setEnabled(true);
	 			}
 			}
 		});	
		
//		if (uw instanceof RecepcionEMCSView)
//		{
//			this.arc.addValueChangeListener(new ValueChangeListener() {
//				
//				@Override
//				public void valueChange(ValueChangeEvent event) {
//					cargarComboAlbaranes();
//				}
//			});
//		}
//		
//		if (uw instanceof RecepcionEMCSView)
//		{
//			this.litros.addValueChangeListener(new ValueChangeListener() {
//				
//				@Override
//				public void valueChange(ValueChangeEvent event) {
//					Integer litrosAlbaran = new Integer(0);
//					if (albaran.getValue()!=null && (Integer) albaran.getValue()>0)
//					{
//						litrosAlbaran=obtenerLitrosAlbaranSeleccionado();
//						if (!new Integer(RutinasNumericas.formatearIntegerDeESP(litros.getValue())).equals(litrosAlbaran)) Notificaciones.getInstance().mensajeAdvertencia("No coinciden los litros introducidos con los del albararán indicado");
//					}
//				}
//			});
//		}
		
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarRecepcionEMCS(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.destino.getValue()!=null && this.destino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destino", this.destino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destino", "");
		}
		if (this.anada.getValue()!=null && this.anada.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("anada", this.anada.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("anada", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.numeroReferencia.getValue()!=null && this.numeroReferencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroReferencia", this.numeroReferencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroReferencia", "");
		}
		if (this.destinatario.getValue()!=null && this.destinatario.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinatario", this.destinatario.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinatario", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.descripcionIdentificacion.getValue()!=null && this.descripcionIdentificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionIdentificacion", this.descripcionIdentificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionIdentificacion", "");
		}
		if (this.caeDestino.getValue()!=null && this.caeDestino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeDestino", this.caeDestino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeDestino", "");
		}
		if (this.direccion.getValue()!=null && this.direccion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("direccion", this.direccion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("direccion", "");
		}
		
		
		if (this.codigoPostal.getValue()!=null && this.codigoPostal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoPostal", this.codigoPostal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoPostal", "");
		}
		if (this.grado.getValue()!=null && this.grado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("grado", this.grado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("grado", "");
		}
		if (this.arc.getValue()!=null && this.arc.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("arc", this.arc.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("arc", "");
		}
		if (this.litros.getValue()!=null && this.litros.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("litros", this.litros.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("litros", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		
		if (this.albaran.getValue()!=null && ((Integer) this.albaran.getValue())>0) 
		{
			opcionesEscogidas.put("albaran", this.albaran.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("albaran", "");
			opcionesEscogidas.put("documento", "");
		}
		
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.destino.getValue()!=null && this.destino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destino", this.destino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destino", "");
		}
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.numeroReferencia.getValue()!=null && this.numeroReferencia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("numeroReferencia", this.numeroReferencia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("numeroReferencia", "");
		}
		if (this.destinatario.getValue()!=null && this.destinatario.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("destinatario", this.destinatario.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("destinatario", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.descripcionIdentificacion.getValue()!=null && this.descripcionIdentificacion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcionIdentificacion", this.descripcionIdentificacion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcionIdentificacion", "");
		}
		if (this.caeDestino.getValue()!=null && this.caeDestino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caeDestino", this.caeDestino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caeDestino", "");
		}
		if (this.direccion.getValue()!=null && this.direccion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("direccion", this.direccion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("direccion", "");
		}
		if (this.codigoPostal.getValue()!=null && this.codigoPostal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("codigoPostal", this.codigoPostal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigoPostal", "");
		}
		if (this.grado.getValue()!=null && this.grado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("grado", this.grado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("grado", "");
		}
		if (this.arc.getValue()!=null && this.arc.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("arc", this.arc.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("arc", "");
		}
		if (this.albaran.getValue()!=null && this.albaran.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("albaran", this.albaran.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("albaran", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarRecepcionEMCS(null);
		}
	}
	
	public void editarRecepcionEMCS(MapeoRecepcionEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cargarCombo();
		
		posicionarCampos();
		
		if (!isBusqueda())
		{
			/*
			 * establezco el numerador
			 */
			if (r_mapeo==null) 
			{
				r_mapeo=new MapeoRecepcionEMCS();			
				r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
				r_mapeo.setFecha(new Date());
			}
			else
			{
				if (uw instanceof AsignacionEntradasEMCSView)
				{
					this.creacion=true;
					
					this.albaran.addItem(r_mapeo.getAlbaran());
					this.albaran.setValue(r_mapeo.getAlbaran());
					r_mapeo.setObservaciones("");
					
					r_mapeo.setDestino("");
					r_mapeo.setAnada("");
					r_mapeo.setGrado(new Double(0));
					if (r_mapeo.getArc()==null || r_mapeo.getArc().length()==0) r_mapeo.setArc("0");
					if (r_mapeo.getNumeroReferencia()==null || r_mapeo.getNumeroReferencia()==0) r_mapeo.setNumeroReferencia(new Long(0));
				}
			}
		}
		else
		{
			r_mapeo=new MapeoRecepcionEMCS();
		}
	
//		r_mapeo.setCaeDestino(this.caeDestino.getValue().toString());
		
		
		fieldGroup.setItemDataSource(new BeanItem<MapeoRecepcionEMCS>(r_mapeo));
		if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).modificando=true; else ((AsignacionEntradasEMCSView) uw).modificando=false;
		
		desactivarCampos(r_mapeo.getAlbaran());

	}
	
	public void eliminarRecepcionEMCS(MapeoRecepcionEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).mapeoRecepcionEMCSEnlazado=r_mapeo; else ((AsignacionEntradasEMCSView) uw).mapeoRecepcionEMCSEnlazado=r_mapeo;
		
		
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(uw, "Seguro que deseas eliminar el registro?", "Si", "No", "Eliminar", null);			
		uw.getUI().addWindow(vt);
	}
	
	public void modificarRecepcionEMCS(MapeoRecepcionEMCS r_mapeo, MapeoRecepcionEMCS r_mapeo_orig)
	{
		String rdo = null;
				
		if (uw instanceof RecepcionEMCSView) 
			rdo = ((RecepcionEMCSView) uw).cus.guardarCambios(r_mapeo,r_mapeo_orig);
		else
			rdo = ((AsignacionEntradasEMCSView) uw).cus.guardarCambios(r_mapeo,r_mapeo_orig);
		
		if (rdo==null)
		{
//			r_mapeo.setEstado(rdo);
		
			if (!((RecepcionEMCSGrid) uw.grid).vector.isEmpty())
			{
				HashMap<String,String> filtrosRec = ((RecepcionEMCSGrid) uw.grid).recogerFiltros();
				
				((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector).remove(r_mapeo_orig);
				if (r_mapeo_orig.getCaeDestino().equals(r_mapeo.getCaeDestino())) ((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoRecepcionEMCS> r_vector = (ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector;
				
				Indexed indexed = ((RecepcionEMCSGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	            uw.setHayGrid(false);
	            
	            if (uw instanceof RecepcionEMCSView) 
	    			((RecepcionEMCSView) uw).presentarGrid(r_vector);
	    		else
	    			((AsignacionEntradasEMCSView) uw).presentarGrid(r_vector);

	            ((RecepcionEMCSGrid) uw.grid).setRecords((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector);
	            ((RecepcionEMCSGrid) uw.grid).sort("numeroReferencia", SortDirection.DESCENDING);
	            if (r_mapeo_orig.getCaeDestino().equals(r_mapeo.getCaeDestino())) ((RecepcionEMCSGrid) uw.grid).scrollTo(r_mapeo);
	            if (r_mapeo_orig.getCaeDestino().equals(r_mapeo.getCaeDestino())) ((RecepcionEMCSGrid) uw.grid).select(r_mapeo);
	            
	            ((RecepcionEMCSGrid) uw.grid).establecerFiltros(filtrosRec);	            
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).modificando=false; else ((AsignacionEntradasEMCSView) uw).modificando=false;
	}
	
	public void crearRecepcionEMCS(MapeoRecepcionEMCS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		String rdo = null;
		MapeoRecepcionEMCS mapeo_orig = null;
		
		if (((RecepcionEMCSGrid) uw.grid)!=null && !((RecepcionEMCSGrid) uw.grid).vector.isEmpty())
		{
			mapeo_orig= (MapeoRecepcionEMCS) ((RecepcionEMCSGrid) uw.grid).getSelectedRow();
			r_mapeo.setTipo(mapeo_orig.getTipo());
			r_mapeo.setDocumento(mapeo_orig.getDocumento());
			r_mapeo.setClaveTabla(mapeo_orig.getClaveTabla());
			r_mapeo.setSerie(mapeo_orig.getSerie());
		}
		
		if (uw instanceof RecepcionEMCSView)
		{
			r_mapeo.setIdCodigo(((RecepcionEMCSView) uw).cus.obtenerSiguiente());
			rdo = ((RecepcionEMCSView) uw).cus.guardarNuevo(r_mapeo);
		}
		else
		{
			r_mapeo.setIdCodigo(((AsignacionEntradasEMCSView) uw).cus.obtenerSiguiente());
			rdo = ((AsignacionEntradasEMCSView) uw).cus.guardarNuevo(r_mapeo);
		}

		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoRecepcionEMCS>(r_mapeo));
			if (((RecepcionEMCSGrid) uw.grid)!=null )//&& )
			{
				if (!((RecepcionEMCSGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((RecepcionEMCSGrid) uw.grid).recogerFiltros();
					
					((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector).remove(mapeo_orig);
//					((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector).add(r_mapeo);
					((RecepcionEMCSGrid) uw.grid).setRecords((ArrayList<MapeoRecepcionEMCS>) ((RecepcionEMCSGrid) uw.grid).vector);
//					if (uw instanceof RecepcionEMCSView)
//					{
//						((RecepcionEMCSGrid) uw.grid).sort("numeroReferencia", SortDirection.DESCENDING);
//					}
//					else
//					{
//						((RecepcionEMCSGrid) uw.grid).sort("albaran", SortDirection.ASCENDING);
//					}
//					((RecepcionEMCSGrid) uw.grid).scrollTo(r_mapeo);
//					((RecepcionEMCSGrid) uw.grid).select(r_mapeo);
					
					((RecepcionEMCSGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}

	public void desactivarCampos(Integer r_albaran)
	{
		if (r_albaran!=null && r_albaran >0) this.caeDestino.setEnabled(false);
	}
	
	public void cerrar()
	{
		vaciarCombo();
		mapeoModificado=null;
		if (uw instanceof RecepcionEMCSView) ((RecepcionEMCSView) uw).modificando=false; else ((AsignacionEntradasEMCSView) uw).modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoRecepcionEMCS> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
        
    }

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (arc.getValue()!=null && arc.getValue().length()>0 && arc.getValue().length()!=21)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Introduce el valor correctamente. El arc debe tener 21 posiciones");
				this.arc.focus();
				return false;
			}
			if (numeroReferencia.getValue()!=null && numeroReferencia.getValue().length()>0 && numeroReferencia.getValue().replace(".", "").length()!=11)
			{
				Notificaciones.getInstance().mensajeError("Introduce el valor correctamente. El numero de referencia debe tener 11 Posiciones");
				this.numeroReferencia.focus();
				return false;
			}
			if (this.origen.getValue()==null || this.origen.getValue().length()==0)
			{ 
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Expedidor");
				this.origen.focus();
				return false;
			}
			if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Ejercicio ");
				this.ejercicio.focus();
				return false;
			}
			if (this.fecha.getValue()==null || this.fecha.getValue().toString().length()==0) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar la Fecha ");
				this.fecha.focus();
				return false;
			}
			if (this.destinatario.getValue()==null || this.destinatario.getValue().length()==0)
			{
				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el Destinatario ");
				this.destinatario.focus();
				return false;
			}
//			if (this.caeDestino.getValue()==null || this.caeDestino.getValue().toString().length()==0)
//			{
//				Notificaciones.getInstance().mensajeAdvertencia("Es obligatorio indicar el CAE de destino ");
//				this.caeDestino.focus();
//				return false;
//			}
		}
		
		return true;
	}
	
	private void cargarCombo()
	{
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			this.caeDestino.addItem(caes.get(i).trim());
		}
		
		this.caeDestino.setNewItemsAllowed(false);
		
		this.descripcionIdentificacion.addItem("con CAE");
		this.descripcionIdentificacion.addItem("sin CAE");
		this.descripcionIdentificacion.addItem("Exportación");
		this.descripcionIdentificacion.addItem("Desconocido (art 29.B.1. Reglamento IIEE)");
		this.descripcionIdentificacion.setNewItemsAllowed(false);
		
		this.albaran.setNewItemsAllowed(true);
	}
	
	private void posicionarCampos()
	{
		String cae = null;
		String titulo = null;
		
		if (uw instanceof RecepcionEMCSView)
		{
			if (((RecepcionEMCSGrid) uw.grid)!=null)
 			{
 				uw.setHayGrid(true);
 				uw.grid.setEnabled(false);
 			}
			cae = ((RecepcionEMCSView) uw).cmbCaeDestino.getValue().toString();
			titulo = ((RecepcionEMCSView) uw).almacenSeleccionado.toUpperCase();
		}
		else
		{
			cae = ((AsignacionEntradasEMCSView) uw).cmbCaeDestino.getValue().toString();
			titulo = ((AsignacionEntradasEMCSView) uw).almacenSeleccionado.toUpperCase();
		}

		this.caeDestino.setValue(cae);
		this.caeDestino.setCaption(titulo);

		this.ejercicio.setValue(RutinasFechas.añoActualYYYY());		
		
		this.descripcionIdentificacion.setValue("con CAE");
	}
	
	private void vaciarCombo()
	{
		this.caeDestino.removeAllItems();
		this.descripcionIdentificacion.removeAllItems();

	}
	
//	private void cargarComboAlbaranes()
//	{
//		String almacen = null;
//		String almacenOrigen = null;
//		
//		this.albaran.removeAllItems();
//		
//		if (this.albaran.getValue()==null || (Integer) this.albaran.getValue()==0)
//		{
//			consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());			
//			if (this.caeDestino.getValue()!=null) almacen = cas.obtenerAlmacenPorCAE(this.caeDestino.getValue().toString());
//			if (this.origen.getValue()!=null) almacenOrigen = cas.obtenerAlmacenPorCAE(this.origen.getValue().toString());
//			
//			consultaRecepcionEMCSServer cres = new consultaRecepcionEMCSServer(CurrentUser.get());
//			
//			if(almacenOrigen==null)
//			{
//				this.vectorAlbaranes = cres.obtenerDocumentosCompras(this.nif.getValue(), this.fecha.getValue(), almacen);
//				
//				if (this.vectorAlbaranes!=null)
//				{
//					for (int i=0;i<this.vectorAlbaranes.size();i++)
//					{
//						MapeoLineasAlbaranesCompras mapeo = (MapeoLineasAlbaranesCompras) this.vectorAlbaranes.get(i);
//						this.albaran.addItem(mapeo.getIdCodigoCompra());
//					}
//				}
//			}
//			else
//			{
//				this.vectorTraslados = cres.obtenerDocumentosTraslado(this.fecha.getValue(), almacen,almacenOrigen);
//				
//				if (this.vectorTraslados!=null)
//				{
//					for (int i=0;i<this.vectorTraslados.size();i++)
//					{
//						MapeoLineasAlbaranesTraslado mapeo = (MapeoLineasAlbaranesTraslado) this.vectorTraslados.get(i);
//						this.albaran.addItem(mapeo.getIdCodigoTraslado());
//					}
//				}
//			}
//			this.albaran.setNewItemsAllowed(false);
//		}		
//	}
//	
//	private Integer obtenerLitrosAlbaranSeleccionado()
//	{
//		Integer litrosObtenidos = null;
//		
//		if (this.vectorAlbaranes!=null && !this.vectorAlbaranes.isEmpty())
//		{
//			for (int i=0;i<this.vectorAlbaranes.size();i++)
//			{
//				MapeoLineasAlbaranesCompras mapeo = (MapeoLineasAlbaranesCompras) this.vectorAlbaranes.get(i);
//				if (mapeo.getIdCodigoCompra().equals((Integer) this.albaran.getValue()))
//				{
//					litrosObtenidos=mapeo.getUnidades().intValue();
//					break;
//				}
//				
//			}
//		}
//		else if (this.vectorTraslados!=null && !this.vectorTraslados.isEmpty())
//		{
//			for (int i=0;i<this.vectorTraslados.size();i++)
//			{
//				MapeoLineasAlbaranesTraslado mapeo = (MapeoLineasAlbaranesTraslado) this.vectorTraslados.get(i);
//				if (mapeo.getIdCodigoTraslado().equals((Integer) this.albaran.getValue()))
//				{
//					litrosObtenidos=mapeo.getUnidades().intValue();
//					break;
//				}
//			}
//		}
//		
//		
//		return litrosObtenidos;
//	}
}
