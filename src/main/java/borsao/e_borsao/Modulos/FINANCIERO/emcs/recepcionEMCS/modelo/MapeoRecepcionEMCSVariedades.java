package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class MapeoRecepcionEMCSVariedades extends MapeoGlobal
{
	
	private Integer idEntrada;	
	private Integer idVariedad;	
	private String nombre;
	private String porcentaje;
	private String litros;
	
	public MapeoRecepcionEMCSVariedades()
	{
	}

	public Integer getIdEntrada() {
		return idEntrada;
	}

	public Integer getIdVariedad() {
		return idVariedad;
	}

	public String getNombre() {
		return nombre;
	}

	public String getPorcentaje() {
		if (porcentaje!=null)
			return RutinasNumericas.formatearDouble(RutinasNumericas.formatearDouble(porcentaje));
		else
			return null;
	}

	public void setIdEntrada(Integer idEntrada) {
		this.idEntrada = idEntrada;
	}

	public void setIdVariedad(Integer idVariedad) {
		this.idVariedad = idVariedad;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPorcentaje(String porcentaje) {
		if (porcentaje!=null)
			this.porcentaje = RutinasNumericas.formatearDouble(RutinasCadenas.reemplazarPuntoMiles(porcentaje));
		else
			this.porcentaje =  null;
	}

	public String getLitros() {
		return litros;
	}

	public void setLitros(String litros) {
		this.litros = litros;
	}
}


