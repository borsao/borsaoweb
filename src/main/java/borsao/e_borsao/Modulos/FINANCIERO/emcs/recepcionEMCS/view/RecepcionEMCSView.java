package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasEjercicios;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.RecepcionEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server.consultaRecepcionEMCSServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class RecepcionEMCSView extends GridViewRefresh {

	public static final String VIEW_NAME = "Control Guías Entrada";
	public consultaRecepcionEMCSServer cus =null;
	public boolean modificando = false;
	public String tituloGrid = "Recepcion en ";
	public boolean conTotales = false;
	public MapeoRecepcionEMCS mapeoRecepcionEMCSEnlazado=null;
	public ComboBox cmbCaeDestino = null;	
	public ComboBox cmbEstado = null;
	public String almacenSeleccionado=null;
	public String numeroAlmacen=null;
	
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private OpcionesForm form = null;
	private pantallaAsignacionDatosRecepcionEMCSAlbaran vt = null;
	public ComboBox cmbEjercicio = null;
	private HashMap<String,String> filtrosRec = null;
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
	private Button opcCopiar = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public RecepcionEMCSView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
    	this.cus = new consultaRecepcionEMCSServer(CurrentUser.get());
    	
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		
		this.cmbEjercicio= new ComboBox("Ejercicio");    		
		this.cmbEjercicio.setNewItemsAllowed(false);
		this.cmbEjercicio.setNullSelectionAllowed(false);
		this.cmbEjercicio.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.opcCopiar= new Button("Copiar");    	
		this.opcCopiar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcCopiar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcCopiar.setIcon(FontAwesome.COPY);

		this.cmbEstado= new ComboBox("Estado");    		
		this.cmbEstado.setNewItemsAllowed(false);
		this.cmbEstado.setNullSelectionAllowed(false);
		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.cmbCaeDestino= new ComboBox("Destino");    		
		this.cmbCaeDestino.setNewItemsAllowed(false);
		this.cmbCaeDestino.setNullSelectionAllowed(false);
		this.cmbCaeDestino.addStyleName(ValoTheme.COMBOBOX_TINY);
		
//		this.cabLayout.addComponent(this.cmbEstado);
		this.cabLayout.addComponent(this.cmbEjercicio);
		this.cabLayout.addComponent(this.cmbCaeDestino);
		this.cabLayout.addComponent(this.opcCopiar);
		this.cabLayout.setComponentAlignment(this.opcCopiar,Alignment.BOTTOM_LEFT);
		
		this.cargarCombo();
		this.cargarComboEjercicio();
		this.cargarListeners();
		this.establecerModo();
		
		lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoRecepcionEMCS> r_vector=null;
    	MapeoRecepcionEMCS mapeoRecepcionEMCS =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoRecepcionEMCS=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	mapeoRecepcionEMCS.setCaeDestino(this.cmbCaeDestino.getValue().toString());
    	mapeoRecepcionEMCS.setEjercicio(new Integer(this.cmbEjercicio.getValue().toString()));

    	r_vector=this.cus.datosRecepcionEMCSGlobal(mapeoRecepcionEMCS);
    	this.presentarGrid(r_vector);
    	
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	
    	
    	vt = new pantallaAsignacionDatosRecepcionEMCSAlbaran(this, new Integer(RutinasFechas.añoActualYYYY()), "Creación Guias Entrada ", null, null, this.numeroAlmacen);
    	
    	getUI().addWindow(vt);
    	
    	this.generarGrid(opcionesEscogidas);

    	this.botonesGenerales(true);
		this.navegacion(true);
		this.verBotones(true);
		this.activarBotones(true);
		
//    	if (this.form==null)
//    	{
//    		this.form = new OpcionesForm(this);
//    		addComponent(this.form);
//    	}
//    	this.modificando=false;
//    	this.form.setCreacion(true);
//    	this.form.setBusqueda(false);
//    	this.form.btnGuardar.setCaption("Guardar");
//    	this.form.btnEliminar.setEnabled(false);    	
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
    	if (this.grid==null || ((RecepcionEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RecepcionEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((RecepcionEMCSGrid) this.grid).activadaVentanaPeticion && !((RecepcionEMCSGrid) this.grid).ordenando)
    	{
    		this.modificando=true;
    		this.verForm(false);
    		this.form.editarRecepcionEMCS((MapeoRecepcionEMCS) r_fila);
    	}    		
    }
    
    public void generarGrid(MapeoRecepcionEMCS r_mapeo)
    {
    	ArrayList<MapeoRecepcionEMCS> r_vector=null;
    	r_vector=this.cus.datosRecepcionEMCSGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoRecepcionEMCS> r_vector)
    {
    	if (isHayGrid())
		{			
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			setHayGrid(false);
		}
    	
    	this.grid = new RecepcionEMCSGrid(this,r_vector);
    	if (((RecepcionEMCSGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RecepcionEMCSGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    		((RecepcionEMCSGrid) grid).establecerFiltros(filtrosRec);
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//    				setHayGrid(false);
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
		this.cmbCaeDestino.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{	
					filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;		
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
				
				if (!cmbCaeDestino.getValue().toString().equals("Todos"))
				{
					almacenSeleccionado=cas.obtenerNombreAlmacenPorCAE(cmbCaeDestino.getValue().toString());
					numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
				}
				else
				{
					almacenSeleccionado = "Todos";
				}
				tituloGrid = "Recepcion en " + almacenSeleccionado;
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		});
		
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		});

		this.cmbEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo = new MapeoRecepcionEMCS();
				if (cmbEstado.getValue()!=null) mapeo.setEstado(cmbEstado.getValue().toString());
				if (cmbCaeDestino.getValue()!=null) mapeo.setCaeDestino(cmbCaeDestino.getValue().toString());
				if (cmbEjercicio.getValue()!=null) mapeo.setEjercicio(new Integer(cmbEjercicio.getValue().toString()));
				
				opcionesEscogidas.put("estado", cmbEstado.getValue().toString());
				opcionesEscogidas.put("caeDestino", cmbCaeDestino.getValue().toString());
				opcionesEscogidas.put("ejercicio", cmbEjercicio.getValue().toString());
				
				generarGrid(mapeo);
			}
		}); 

    	this.opcCopiar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (form!=null) form.cerrar();
				MapeoRecepcionEMCS mapeo=(MapeoRecepcionEMCS) grid.getSelectedRow();
				MapeoRecepcionEMCS mapeoGenerado = null;
				
				if (mapeo!=null)
				{
					mapeoGenerado = cus.copiar(mapeo);

					if (mapeoGenerado!=null)
					{
						modificando=false;
						verForm(false);
						grid.setEnabled(false);
						form.editarRecepcionEMCS(mapeoGenerado);
						form.creacion=true;
					}
					else
						Notificaciones.getInstance().mensajeError("No ha podido realizarse la copia");
				}			
				else
				{
					Notificaciones.getInstance().mensajeError("No se han encontrado datos a copiar");
				}
			}
		});
    	
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 */
		this.verBotones(true);
		this.activarBotones(true);
		this.navegacion(true);
				
		this.opcNuevo.setVisible(true);
		this.opcNuevo.setEnabled(true);
		setSoloConsulta(this.soloConsulta);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCopiar.setEnabled(r_activo);
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCopiar.setVisible(r_visibles);
	}
	
	public void imprimirRecepcionEMCS()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		
		this.cmbEstado.addItem("Pendientes");
		this.cmbEstado.addItem("Todas");
		this.cmbEstado.setValue("Todas");
		opcionesEscogidas.put("estado", cmbEstado.getValue().toString());

		this.cmbCaeDestino.removeAllItems();
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());		
		String almacenDefecto = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
		
		ArrayList<String> caes = cas.obtenerCAEAlmacenes();
		
		for (int i=0;i<caes.size();i++)
		{
			if (caes.get(i)!=null && caes.get(i).length()>0) this.cmbCaeDestino.addItem(caes.get(i).trim());
		}
		
		this.cmbCaeDestino.addItem("Todos");
		
		String caeDefecto = cas.obtenerCAEAlmacen(almacenDefecto);
		
		if (caeDefecto!=null)
		{
			
			this.cmbCaeDestino.setValue(caeDefecto.trim());
			this.almacenSeleccionado=cas.obtenerDescripcionAlmacen(almacenDefecto);
			this.numeroAlmacen = cas.obtenerAlmacenPorCAE(cmbCaeDestino.getValue().toString());
			this.tituloGrid = this.tituloGrid + " " + this.almacenSeleccionado; 
			this.opcionesEscogidas.put("caeDestino", this.cmbCaeDestino.getValue().toString());
		}
	}
	
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			setHayGrid(false);
			generarGrid(opcionesEscogidas);
		}
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
		switch (r_accion)
		{
			case "Guardar":
			{
				this.cerrarVentanaBusquedaGuardarNuevo(mapeoRecepcionEMCSEnlazado);
				break;
			}
			case "Eliminar":
			{
				consultaRecepcionEMCSServer cus = new consultaRecepcionEMCSServer(CurrentUser.get());	
				((RecepcionEMCSGrid) this.grid).remove(mapeoRecepcionEMCSEnlazado);
				cus.eliminar(mapeoRecepcionEMCSEnlazado);
				this.form.cerrar();
				break;
			}
		}
	}

	public void cerrarVentanaBusqueda( String r_clave_tabla, String r_documento, String r_serie, Integer r_codigo, String r_arc, Long r_referencia, Integer r_litros, Double r_grado, String r_destino)
	{
//		MapeoRecepcionEMCS mapeo_orig = null;
		MapeoRecepcionEMCS mapeo = (MapeoRecepcionEMCS) this.grid.getSelectedRow();

		if (mapeo!=null)
		{
			this.cus.eliminar(mapeo);
//			mapeo_orig = this.cus.copiar(mapeo, false);
//			mapeo_orig.setClaveTabla(mapeo.getClaveTabla());
//			mapeo_orig.setDocumento(mapeo.getDocumento());
//			mapeo_orig.setSerie(mapeo.getSerie());
//			mapeo_orig.setAlbaran(mapeo.getAlbaran());
//			mapeo_orig.setNumeroReferencia(mapeo.getNumeroReferencia());
//			mapeo_orig.setArc(mapeo.getArc());	
//			mapeo_orig.setLitros(mapeo.getLitros());	
//			mapeo_orig.setGrado(mapeo.getGrado());
//			mapeo_orig.setDestino(mapeo.getDestino());
//			
//			mapeo.setClaveTabla(r_clave_tabla);
//			mapeo.setDocumento(r_documento);
//			mapeo.setSerie(r_serie);
//			mapeo.setAlbaran(r_codigo);
//			mapeo.setArc(r_arc);
//			mapeo.setNumeroReferencia(r_referencia);
//			mapeo.setLitros(r_litros);
//			mapeo.setGrado(r_grado);
//			mapeo.setDestino(r_destino);
//			
//			this.cus.guardarCambios(mapeo,mapeo_orig);
			
			if (isHayGrid())
			{
				filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
				grid.removeAllColumns();			
				barAndGridLayout.removeComponent(grid);
				grid=null;
				setHayGrid(false);
				generarGrid(opcionesEscogidas);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No tienes ningun registro seleccionado");
		}
	}

	public void cerrarVentanaBusqueda( String r_clave_tabla, String r_documento, String r_serie, Integer r_codigo, String r_arc, Long r_referencia)
	{
		MapeoRecepcionEMCS mapeo_orig = null;
		MapeoRecepcionEMCS mapeo = (MapeoRecepcionEMCS) this.grid.getSelectedRow();

		if (mapeo!=null)
		{
//			this.cus.eliminar(mapeo);
			mapeo_orig = this.cus.copiar(mapeo, false);
			mapeo_orig.setClaveTabla(mapeo.getClaveTabla());
			mapeo_orig.setDocumento(mapeo.getDocumento());
			mapeo_orig.setSerie(mapeo.getSerie());
			mapeo_orig.setAlbaran(mapeo.getAlbaran());
			mapeo_orig.setNumeroReferencia(mapeo.getNumeroReferencia());
			mapeo_orig.setArc(mapeo.getArc());	
			mapeo_orig.setLitros(mapeo.getLitros());	
			mapeo_orig.setGrado(mapeo.getGrado());
			mapeo_orig.setDestino(mapeo.getDestino());
			
			mapeo.setClaveTabla(r_clave_tabla);
			mapeo.setDocumento(r_documento);
			mapeo.setSerie(r_serie);
			mapeo.setAlbaran(r_codigo);
			mapeo.setArc(r_arc);
			mapeo.setNumeroReferencia(r_referencia);
			
			this.cus.guardarCambios(mapeo,mapeo_orig);
			
			if (isHayGrid())
			{
				filtrosRec = ((RecepcionEMCSGrid) grid).recogerFiltros();
				grid.removeAllColumns();			
				barAndGridLayout.removeComponent(grid);
				grid=null;
				setHayGrid(false);
				generarGrid(opcionesEscogidas);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeError("No tienes ningun registro seleccionado");
		}
	}

	public void cerrarVentanaBusquedaGuardarNuevo(MapeoRecepcionEMCS r_mapeo)
	{
		String rdo = null;
		Integer id = this.cus.obtenerSiguiente();
		r_mapeo.setIdCodigo(id);
		if (r_mapeo!=null) rdo = this.cus.guardarNuevo(r_mapeo);
		
		if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		else
			generarGrid(opcionesEscogidas);
			
//		if (this.vt.isVisible()) this.vt.close();
		
//		if (isHayGrid())
//		{
//			grid.removeAllColumns();			
//			barAndGridLayout.removeComponent(grid);
//			grid=null;
//			setHayGrid(false);
//		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		if (this.form!=null) this.form.cerrar();
	}
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	private void cargarComboEjercicio()
	{
		RutinasEjercicios rej = new RutinasEjercicios();
		ArrayList<String> vector = rej.cargarEjercicios();
		
		for (int i=0;i<vector.size();i++)
		{
			if (((String) vector.get(i)).length()>0)  this.cmbEjercicio.addItem((String) vector.get(i));
		}
		
		this.cmbEjercicio.setValue(vector.get(0).toString());
	}
}


