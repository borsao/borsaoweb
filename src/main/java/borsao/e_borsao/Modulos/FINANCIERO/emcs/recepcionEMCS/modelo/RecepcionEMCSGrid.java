package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.albaranesTraslado.view.pantallaLineasAlbaranesTraslados;
import borsao.e_borsao.Modulos.COMPRAS.AlbaranesCompras.view.pantallaLineasAlbaranesCompra;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.AsignacionEntradasEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.RecepcionEMCSView;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view.pantallaVariedades;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class RecepcionEMCSGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private boolean conTotales = true;	
	private boolean editable = false;
	private boolean conFiltro = true;
	
	private GridViewRefresh app = null;
	
    public RecepcionEMCSGrid(RecepcionEMCSView r_app, ArrayList<MapeoRecepcionEMCS> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo(((RecepcionEMCSView)this.app).tituloGrid);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public RecepcionEMCSGrid(AsignacionEntradasEMCSView r_app, ArrayList<MapeoRecepcionEMCS> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo(((AsignacionEntradasEMCSView) this.app).tituloGrid);

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    

    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoRecepcionEMCS.class);
		this.setRecords(this.vector);
//		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    

    public void doEditItem() 
    {
    }
  
  @Override
  	public void doCancelEditor()
  	{
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	if (this.app instanceof RecepcionEMCSView)
    	{
    		setColumnOrder("documentoFalso","var", "doc","tipo", "caeDestino", "ejercicio", "mes", "fecha","numeroReferencia" ,"origen", "destinatario",   "codigoIdentificacion", "direccion", "codigoPostal", "arc", "litros", "grado", "destino", "anada", "nif", "observaciones", "descripcionIdentificacion", "documento", "albaran" );	
    	}
    	else if (this.app instanceof AsignacionEntradasEMCSView)
    	{
    		setColumnOrder("documentoFalso", "var", "doc", "tipo", "ejercicio", "mes", "documento", "claveTabla", "serie", "albaran" , "fecha", "origen", "destinatario", "litros", "codigoIdentificacion", "direccion", "codigoPostal", "numeroReferencia" , "arc", "grado", "destino", "anada", "nif", "observaciones", "descripcionIdentificacion" ,"caeDestino" );	
    	}
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("caeDestino", "110");
    	this.widthFiltros.put("tipo", "55");
    	this.widthFiltros.put("anada", "55");
    	this.widthFiltros.put("ejercicio", "55");
    	this.widthFiltros.put("mes", "35");
    	this.widthFiltros.put("numeroReferencia", "85");
    	this.widthFiltros.put("origen", "110");
    	this.widthFiltros.put("destinatario", "260");
    	this.widthFiltros.put("nif", "80");
    	this.widthFiltros.put("codigoIdentificacion", "35");
    	this.widthFiltros.put("destino", "80");
    	this.widthFiltros.put("arc", "210");
    	this.widthFiltros.put("documento", "80");
    	this.widthFiltros.put("albaran", "210");
    	
    	this.getColumn("var").setHeaderCaption("");
    	this.getColumn("var").setSortable(false);
    	this.getColumn("var").setWidth(new Double(50));

    	this.getColumn("doc").setHeaderCaption("");
    	this.getColumn("doc").setSortable(false);
    	this.getColumn("doc").setWidth(new Double(50));

    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setSortable(false);
    	this.getColumn("tipo").setWidth(new Double(95));

    	this.getColumn("anada").setHeaderCaption("Añada");
    	this.getColumn("anada").setSortable(false);
    	this.getColumn("anada").setWidth(new Double(95));

    	this.getColumn("mes").setHeaderCaption("Mes");
    	this.getColumn("mes").setSortable(false);
    	this.getColumn("mes").setWidth(new Double(85));

    	this.getColumn("origen").setHeaderCaption("Expedidor");
    	this.getColumn("origen").setWidthUndefined();
    	this.getColumn("origen").setWidth(150);
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(95);
    	this.getColumn("fecha").setHeaderCaption("FECHA");
    	this.getColumn("fecha").setWidthUndefined();
    	this.getColumn("fecha").setWidth(120);
    	this.getColumn("numeroReferencia").setHeaderCaption("NR");
    	this.getColumn("numeroReferencia").setWidth(125);
    	this.getColumn("destinatario").setHeaderCaption("Proveedor");
    	this.getColumn("destinatario").setWidth(300);
    	this.getColumn("codigoIdentificacion").setHeaderCaption("Code");
    	this.getColumn("codigoIdentificacion").setWidth(75);
    	
		
    	this.getColumn("caeDestino").setHeaderCaption("CAE Recepcion");
    	this.getColumn("caeDestino").setWidth(150);
    	this.getColumn("nif").setHeaderCaption("NIF");
    	this.getColumn("nif").setWidth(120);
    	this.getColumn("direccion").setHeaderCaption("Direccion");
    	this.getColumn("direccion").setWidth(240);
    	this.getColumn("codigoPostal").setHeaderCaption("Codigo Postal");
    	this.getColumn("codigoPostal").setWidth(240);
    	this.getColumn("arc").setHeaderCaption("ARC");
    	this.getColumn("arc").setWidth(250);
    	this.getColumn("litros").setHeaderCaption("Litros");
    	this.getColumn("litros").setWidth(120);
    	this.getColumn("grado").setHeaderCaption("Grado");
    	this.getColumn("grado").setWidth(85);
    	this.getColumn("destino").setHeaderCaption("Destino");
    	this.getColumn("destino").setWidth(120);

    	this.getColumn("observaciones").setHeaderCaption("OBSERVACIONES");
    	this.getColumn("observaciones").setWidth(500);
    	
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("codigoIdentificacion").setHidden(true);
    	this.getColumn("descripcionIdentificacion").setHidden(true);
    	this.getColumn("direccion").setHidden(true);
    	this.getColumn("codigoPostal").setHidden(true);
    	this.getColumn("claveTabla").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("documentoFalso").setHidden(true);
    	if (this.app instanceof AsignacionEntradasEMCSView) this.getColumn("var").setHidden(true);
    	
    	this.getColumn("caeDestino").setHidden(this.app instanceof AsignacionEntradasEMCSView);
//    	this.getColumn("albaran").setHidden(this.app instanceof RecepcionEMCSView);
//    	this.getColumn("documento").setHidden(this.app instanceof RecepcionEMCSView);
    	
    	this.getColumn("litros").setRenderer((Renderer) new NumberRenderer(new DecimalFormat("#,##0.00")));
    	this.getColumn("grado").setRenderer((Renderer) new NumberRenderer(new DecimalFormat("#,##0.00")));
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoRecepcionEMCS) {

            	if ("doc".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Albaran";
                }
            	else if ("var".equals(cell.getPropertyId()))
            	{
            		descriptionText = "Acceso a Variedades";
            	}
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estilo = null;
    		String relleno = null;
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	
            	if ( "documentoFalso".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().length()>0)
            			relleno = "S";
            		else
            			relleno = "N";
            	}
            	if ( "doc".equals(cellReference.getPropertyId()))
            	{
            		if (relleno=="S") estilo = "nativebuttonLineas";
            	}            	
            	else if ( "var".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonBiblia";
            	}            	
            	else if ( "litros".equals(cellReference.getPropertyId()) || "mes".equals(cellReference.getPropertyId()))
            	{
            		estilo =  "Rcell-normal";
            	}
            	else estilo =  "cell-normal";
            	
            	return estilo;
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoRecepcionEMCS mapeo = (MapeoRecepcionEMCS) event.getItemId();

            	if (event.getPropertyId().toString().equals("doc"))
            	{
            		
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
            		String almacen = cas.obtenerAlmacenPorCAE(mapeo.getCaeDestino());
            		
            		pantallaLineasAlbaranesCompra vt = null;
            		pantallaLineasAlbaranesTraslados vtt = null;
//            		
            		if (mapeo.getClaveTabla()!=null && !mapeo.getClaveTabla().toUpperCase().equals("T") && mapeo.getDocumento()!=null && mapeo.getDocumento().length()>0 && mapeo.getAlbaran()!=null && mapeo.getAlbaran()>0)
            		{
            			
            	    	if (app instanceof RecepcionEMCSView)
            	    	{
            	    		vt = new pantallaLineasAlbaranesCompra(app, new Integer(((RecepcionEMCSView) app).cmbEjercicio.getValue().toString()), "Lineas Albaran Entrada " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getDocumento(), mapeo.getAlbaran(), almacen);
            	    	}
            	    	else if (app instanceof AsignacionEntradasEMCSView)
            	    	{
            	    		vt = new pantallaLineasAlbaranesCompra(app, new Integer(((AsignacionEntradasEMCSView) app).txtEjercicio.getValue().toString()), "Lineas Albaran Entrada " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getDocumento(), mapeo.getAlbaran(), almacen);
            	    	}

            			getUI().addWindow(vt);	            			
            		}
            		else
            		{
            			vtt = new pantallaLineasAlbaranesTraslados(app, "Lineas Albaran Traslado " + mapeo.getDocumento() + " - " + mapeo.getAlbaran(), mapeo.getEjercicio(), mapeo.getClaveTabla(), mapeo.getDocumento(), mapeo.getSerie(), mapeo.getAlbaran());
            			getUI().addWindow(vtt);	            			
            		}
            		
            	}
            	else if (event.getPropertyId().toString().equals("var"))
            	{
            		
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		pantallaVariedades vt = null;
//            		
            		vt = new pantallaVariedades("Variedades Entrada " + mapeo.getIdCodigo() + " - " + mapeo.getArc(), mapeo);
        	    	getUI().addWindow(vt);	            			
            		
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fecha");
		
		this.camposNoFiltrar.add("descripcionIdentificacion");
		this.camposNoFiltrar.add("direccion");
		this.camposNoFiltrar.add("codigoPostal");		
		this.camposNoFiltrar.add("litros");
		this.camposNoFiltrar.add("grado");
		this.camposNoFiltrar.add("observaciones");
		this.camposNoFiltrar.add("doc");
	}

	public void generacionPdf(MapeoRecepcionEMCS r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	
    	
//    	consultaRecepcionEMCSServer cps = new consultaRecepcionEMCSServer(CurrentUser.get());
//    	pdfGenerado = cps.generarInforme(r_mapeo);
//    	if(pdfGenerado.length()>0)
//    	{
//			try
//            { 
//				if (r_impresora.toUpperCase().equals("PANTALLA"))
//				{
//					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//				}
//				else
//				{
//					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
//					
//					RutinasFicheros.imprimir(archivo, r_impresora);
//				    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
//				}
//				
//            }
//			catch(Exception e)
//			{ 
//                 Notificaciones.getInstance().mensajeSeguimiento(e); 
//			}
//    	}
//    	else
//    		Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double totalLitros = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	Double q1Value = (Double) item.getItemProperty("litros").getValue();
        	if (q1Value!=null) totalLitros += q1Value;

        }
        
        footer.getCell("mes").setText("Totales");
		footer.getCell("litros").setText(RutinasNumericas.formatearDouble(totalLitros));
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("litros").setStyleName("Rcell-pie");
	}
}



