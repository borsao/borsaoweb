package borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo.DestinatariosGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo.MapeoDestinatarios;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.server.consultaDestinatariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoDestinatarios mapeoDestinatarios = null;
	 
	 private consultaDestinatariosServer cus = null;	 
	 private DestinatariosView uw = null;
	 private BeanFieldGroup<MapeoDestinatarios> fieldGroup;
	 
	 public OpcionesForm(DestinatariosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.area,"Area");
        this.cargarCombos(this.codigoIdentificador,"codigo");
        
        fieldGroup = new BeanFieldGroup<MapeoDestinatarios>(MapeoDestinatarios.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();
        
        this.area.setValue("EMCS Interno");
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoDestinatarios = (MapeoDestinatarios) uw.grid.getSelectedRow();
                eliminarDestinatario(mapeoDestinatarios);
//                removeStyleName("visible");
//                btnGuardar.setCaption("Guardar");
//                btnEliminar.setEnabled(true);
//            	setEnabled(false);
            }
        });        
        
        
        this.area.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				codigoIdentificador.removeAllItems();
				cargarCombos(codigoIdentificador, "codigo");				
			}
		});
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoDestinatarios = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearDestinatario(mapeoDestinatarios);
	 			}
	 			else
	 			{
	 				MapeoDestinatarios mapeoDestinatarios_orig = (MapeoDestinatarios) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoDestinatarios= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarDestinatario(mapeoDestinatarios,mapeoDestinatarios_orig);
	 				hm=null;
	 			}
	 			if (((DestinatariosGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		switch (r_nombre)
		{
			case "codigo":
			{
				if (this.area!=null && this.area.getValue()!=null && this.area.getValue().toString().equals("EMCS Interno"))
				{
					r_combo.addItem("con CAE");
					r_combo.addItem("sin CAE");
					r_combo.addItem("Exportación");
					r_combo.addItem("Desconocido (art 29.B.1. Reglamento IIEE");
				}
				if (this.area!=null && this.area.getValue()!=null && this.area.getValue().toString().equals("EMCS Europa"))
				{
					r_combo.addItem("Depósifo Fiscal");
					r_combo.addItem("Destinatario Registrado");
					r_combo.addItem("Destinatario Registrado Ocasional");
					r_combo.addItem("Entrega Directa Autorizada");
					r_combo.addItem("Exportación");
					r_combo.addItem("Desconocido (art 30.A.8. Reglamento ");
				}
				break;
			}
			case "Area":
			{
				r_combo.addItem("EMCS Interno");
				r_combo.addItem("EMCS Europa");
				break;
			}
		}
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarDestinatario(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.codigoIdentificador.getValue()!=null && this.codigoIdentificador.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("codigo", this.codigoIdentificador.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("codigo", "");
		}
		
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.nif.getValue()!=null && this.nif.getValue().length()>0) 
		{
			opcionesEscogidas.put("nif", this.nif.getValue());
		}
		else
		{
			opcionesEscogidas.put("nif", "");
		}
		if (this.numero_identificador.getValue()!=null && this.numero_identificador.getValue().length()>0) 
		{
			opcionesEscogidas.put("numero_identificador", this.numero_identificador.getValue());
		}
		else
		{
			opcionesEscogidas.put("numero_identificador", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarDestinatario(null);
	}
	
	public void editarDestinatario(MapeoDestinatarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoDestinatarios();
		fieldGroup.setItemDataSource(new BeanItem<MapeoDestinatarios>(r_mapeo));
		nombre.focus();
	}
	
	public void eliminarDestinatario(MapeoDestinatarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		uw.mapeoDestinatariosEliminar=r_mapeo;
		
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(uw, "Seguro que deseas eliminar el registro?", "Si", "No", null,null);			
		uw.getUI().addWindow(vt);
	}
	
	public void modificarDestinatario(MapeoDestinatarios r_mapeo, MapeoDestinatarios r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaDestinatariosServer(CurrentUser.get());
			r_mapeo.setIdDestinatario(r_mapeo_orig.getIdDestinatario());
			cus.guardarCambios(r_mapeo);		
			((DestinatariosGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearDestinatario(MapeoDestinatarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaDestinatariosServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoDestinatarios>(r_mapeo));
				if (((DestinatariosGrid) uw.grid)!=null)
				{
					((DestinatariosGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoDestinatarios> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoDestinatarios= item.getBean();
            if (this.mapeoDestinatarios.getIdDestinatario()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.area.getValue()==null || this.area.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Area");
    		return false;
    	}
    	if (this.codigoIdentificador.getValue()==null || this.codigoIdentificador.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Codigo");
    		return false;
    	}
    	if (this.nombre.getValue()==null || this.nombre.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Nombre");
    		return false;
    	}
    	if (this.nif.getValue()==null || this.nif.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el Nif");
    		return false;
    	}
    	if ((this.numero_identificador.getValue()==null || this.numero_identificador.getValue().toString().length()==0) && this.codigoIdentificador.getValue()!=null & this.codigoIdentificador.getValue().toString()!="Exportacion")
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el NÚmero de Identificación");
    		return false;
    	}
    	return true;
    }

}
