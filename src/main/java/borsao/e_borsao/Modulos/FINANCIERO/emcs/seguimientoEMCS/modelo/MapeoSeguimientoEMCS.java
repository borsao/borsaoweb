package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoSeguimientoEMCS extends MapeoGlobal
{
	private Integer ejercicio;
	private Long numeroReferencia;
	private Integer codigoIdentificacion;
	private Double litros;
	private Integer mes;
	
	private String area;
	private String origen;
	private String destinatario;
	private String nif;
	private String exciseNumber;
	private String agenteAduanas;
	private String authorized;
	private String registered;
	private String taxwarehouse;
	private String nombreCliente;
	private String nombreTransporte;
	private String observaciones;
	private String arc;
	private String descripcionIdentificacion;
	private String tipo;
	
	private String documento;
	private String documentoFalso;

	private Integer albaran;
	private String claveTabla;
	private String serie;
	
	private String estado;
	private String doc;
//	private String esc;
//	private String idPrg;

	private Date fecha;
	
	public MapeoSeguimientoEMCS()
	{
		this.setArea("");
		this.setDestinatario("");
		this.setNif("");
		this.setExciseNumber("");
		this.setAgenteAduanas("");
		this.setAuthorized("");
		this.setRegistered("");
		this.setTaxwarehouse("");
		this.setNombreCliente("");
		this.setNombreTransporte("");
		this.setObservaciones("");
		this.setArc("");
		this.setDoc("");
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Long getNumeroReferencia() {
		return numeroReferencia;
	}

	public void setNumeroReferencia(Long numeroReferencia) {
		this.numeroReferencia = numeroReferencia;
	}

//	public Integer getIdDestinatario() {
//		return idDestinatario;
//	}
//
//	public void setIdDestinatario(Integer idDestinatario) {
//		this.idDestinatario = idDestinatario;
//	}

	public Integer getCodigoIdentificacion() {
		return codigoIdentificacion;
	}

	public void setCodigoIdentificacion(Integer codigoIdentificacion) {
		this.codigoIdentificacion = codigoIdentificacion;
	}

	public Double getLitros() {
		return litros;
	}

	public void setLitros(Double litros) {
		this.litros = litros;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getExciseNumber() {
		return exciseNumber;
	}

	public void setExciseNumber(String exciseNumber) {
		this.exciseNumber = exciseNumber;
	}

	public String getAgenteAduanas() {
		return agenteAduanas;
	}

	public void setAgenteAduanas(String agenteAduanas) {
		this.agenteAduanas = agenteAduanas;
	}

	public String getAuthorized() {
		return authorized;
	}

	public void setAuthorized(String authorized) {
		this.authorized = authorized;
	}

	public String getRegistered() {
		return registered;
	}

	public void setRegistered(String registered) {
		this.registered = registered;
	}

	public String getTaxwarehouse() {
		return taxwarehouse;
	}

	public void setTaxwarehouse(String taxwarehouse) {
		this.taxwarehouse = taxwarehouse;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getNombreTransporte() {
		return nombreTransporte;
	}

	public void setNombreTransporte(String nombreTransporte) {
		this.nombreTransporte = nombreTransporte;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getArc() {
		return arc;
	}

	public void setArc(String arc) {
		this.arc = arc;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDescripcionIdentificacion() {
		return descripcionIdentificacion;
	}

	public void setDescripcionIdentificacion(String descripcionIdentificacion) {
		this.descripcionIdentificacion = descripcionIdentificacion;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Integer getAlbaran() {
		return albaran;
	}

	public void setAlbaran(Integer albaran) {
		this.albaran = albaran;
	}

	public String getClaveTabla() {
		return claveTabla;
	}

	public void setClaveTabla(String claveTabla) {
		this.claveTabla = claveTabla;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getDocumentoFalso() {
		return documento;
	}
	public Integer getMes() {
		return new Integer(RutinasFechas.mesFecha(getFecha()));
	}
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}


