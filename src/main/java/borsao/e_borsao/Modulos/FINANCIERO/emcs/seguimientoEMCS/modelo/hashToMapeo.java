package borsao.e_borsao.Modulos.FINANCIERO.emcs.seguimientoEMCS.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoSeguimientoEMCS mapeo = null;
	 
	 public MapeoSeguimientoEMCS convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoSeguimientoEMCS();
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("numeroReferencia")!=null && r_hash.get("numeroReferencia").length()>0) this.mapeo.setNumeroReferencia(new Long(RutinasCadenas.quitarPuntoMiles(r_hash.get("numeroReferencia"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros"))));
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
		 if (r_hash.get("codigoIdentificacion")!=null && r_hash.get("codigoIdentificacion").length() >0 ) this.mapeo.setCodigoIdentificacion(new Integer(r_hash.get("codigoIdentificacion")));
		 
		 this.mapeo.setDescripcionIdentificacion(r_hash.get("descripcionIdentificacion"));
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setExciseNumber(r_hash.get("exciseNumber"));
		 this.mapeo.setNif(r_hash.get("nif"));
		 this.mapeo.setAgenteAduanas(r_hash.get("agenteAduanas"));
		 this.mapeo.setNombreCliente(r_hash.get("nombreCliente"));
		 
		 this.mapeo.setRegistered(r_hash.get("registered"));
		 this.mapeo.setAuthorized(r_hash.get("authorized"));
		 this.mapeo.setTaxwarehouse(r_hash.get("taxwarehouse"));
		 this.mapeo.setNombreTransporte(r_hash.get("nombreTransporte"));
		 
		 this.mapeo.setDestinatario(r_hash.get("destinatario"));
		 this.mapeo.setArc(r_hash.get("arc"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Seguimiento EMCS: fecha erronea. " + ex.getMessage());
		 }
		 return mapeo;		 
	 }
	 
	 public MapeoSeguimientoEMCS convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoSeguimientoEMCS();
		 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0 ) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));
		 if (r_hash.get("numeroReferencia")!=null && r_hash.get("numeroReferencia").length()>0) this.mapeo.setNumeroReferencia(new Long(RutinasCadenas.quitarPuntoMiles(r_hash.get("numeroReferencia"))));
		 if (r_hash.get("litros")!=null && r_hash.get("litros").length()>0) this.mapeo.setLitros(new Double(RutinasNumericas.formatearIntegerDeESP(r_hash.get("litros"))));
		 if (r_hash.get("id")!=null && r_hash.get("id").length()>0) this.mapeo.setIdCodigo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idCodigo")));
		 if (r_hash.get("codigoIdentificacion")!=null && r_hash.get("codigoIdentificacion").length() >0 ) this.mapeo.setCodigoIdentificacion(new Integer(r_hash.get("codigoIdentificacion")));
		 
		 this.mapeo.setDescripcionIdentificacion(r_hash.get("descripcionIdentificacion"));
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setExciseNumber(r_hash.get("exciseNumber"));
		 this.mapeo.setNif(r_hash.get("nif"));
		 this.mapeo.setAgenteAduanas(r_hash.get("agenteAduanas"));
		 this.mapeo.setNombreCliente(r_hash.get("nombreCliente"));
		 this.mapeo.setRegistered(r_hash.get("registered"));
		 this.mapeo.setAuthorized(r_hash.get("authorized"));
		 this.mapeo.setTaxwarehouse(r_hash.get("taxwarehouse"));
		 this.mapeo.setNombreTransporte(r_hash.get("nombreTransporte"));

		 this.mapeo.setDestinatario(r_hash.get("destinatario"));
		 this.mapeo.setArc(r_hash.get("arc"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null) this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Seguimiento EMCS: fecha erronea. " + ex.getMessage());
		 }
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoSeguimientoEMCS r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getFecha()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFecha().toString());
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }
		 this.hash.put("area", this.mapeo.getArea());
		 this.hash.put("origen", this.mapeo.getOrigen());
		 this.hash.put("destinatario", this.mapeo.getDestinatario());
		 

		 this.hash.put("nif", this.mapeo.getNif());
		 this.hash.put("exciseNumber", this.mapeo.getExciseNumber());
		 this.hash.put("agenteAduanas", this.mapeo.getAgenteAduanas());
		 
		 this.hash.put("registered", this.mapeo.getRegistered());
		 this.hash.put("authorized", this.mapeo.getAuthorized());
		 this.hash.put("taxwarehouse", this.mapeo.getTaxwarehouse());
		 this.hash.put("nombreTransporte", this.mapeo.getNombreTransporte());
		 
		 this.hash.put("nombreCliente", this.mapeo.getNombreCliente());
		 this.hash.put("arc", this.mapeo.getArc());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 
		 this.hash.put("estado", this.mapeo.getEstado());
		 this.hash.put("descripcionIdentificacion", this.mapeo.getDescripcionIdentificacion());
		 
		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("id", String.valueOf(this.mapeo.getIdCodigo()));
		 if (this.mapeo.getCodigoIdentificacion()!=null) this.hash.put("codigoIdentificacion", String.valueOf(this.mapeo.getCodigoIdentificacion()));
		 if (this.mapeo.getNumeroReferencia()!=null) this.hash.put("numeroReferencia", String.valueOf(this.mapeo.getNumeroReferencia()));
		 if (this.mapeo.getLitros()!=null) this.hash.put("litros", String.valueOf(this.mapeo.getLitros()));
		 
		 return hash;		 
	 }
}