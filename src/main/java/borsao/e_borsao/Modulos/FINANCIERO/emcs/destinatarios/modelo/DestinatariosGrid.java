package borsao.e_borsao.Modulos.FINANCIERO.emcs.destinatarios.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class DestinatariosGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public DestinatariosGrid(ArrayList<MapeoDestinatarios> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Destinatarios");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoDestinatarios.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("area", "nif", "nombre", "codigo","codigoIdentificador", "numero_identificador");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombre").setHeaderCaption("Nombre");
    	this.getColumn("codigo").setHeaderCaption("Code");
    	this.getColumn("codigoIdentificador").setHeaderCaption("Codigo Identifiador");
    	this.getColumn("area").setHeaderCaption("Area");    	
    	this.getColumn("nif").setHeaderCaption("NIF");
    	this.getColumn("numero_identificador").setHeaderCaption("Num. Identificación");
    	this.getColumn("idDestinatario").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
//    	this.getColumn("codigoIdentificador").setHidden(true);
    	
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
