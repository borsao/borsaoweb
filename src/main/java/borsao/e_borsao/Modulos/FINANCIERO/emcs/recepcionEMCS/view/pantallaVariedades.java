package borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.controles.controlProductoElaborar.modelo.MapeoControlProductoElaborar;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCS;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.MapeoRecepcionEMCSVariedades;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.modelo.RecepcionEMCSGrid;
import borsao.e_borsao.Modulos.FINANCIERO.emcs.recepcionEMCS.server.consultaRecepcionEMCSServer;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.modelo.MapeoVariedadesDga;
import borsao.e_borsao.Modulos.LABORATORIO.LibrosGranel.variedadesDga.server.consultaVariedadesDgaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaVariedades extends Ventana
{
	MapeoRecepcionEMCS mapeoRecepcion = null;
	MapeoRecepcionEMCSVariedades mapeoRecepcionEmcsVariedades = null;
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private Combo cmbVariedad = null;
	private TextField txtPorcentaje = null;
	private TextField txtLitros = null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;

	private RecepcionEMCSGrid app=null;
	private Integer idEntrada = null;

	/*
	 * Entradas datos equipo
	 */
	
	public pantallaVariedades(String r_titulo, MapeoRecepcionEMCS r_entrada)
	{
		this.setCaption(r_titulo);
//		this.app=r_app;
		this.mapeoRecepcion =r_entrada;
		this.idEntrada = mapeoRecepcion.getIdCodigo();
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.cmbVariedad= new Combo("Variedad");    		
        		this.cmbVariedad.setNewItemsAllowed(false);
        		this.cmbVariedad.setEnabled(false);
        		this.cmbVariedad.setWidth("250px");
        		this.cmbVariedad.addStyleName(ValoTheme.COMBOBOX_TINY);

        		this.txtLitros= new TextField("Litros");    		
        		this.txtLitros.setEnabled(true);
        		this.txtLitros.setWidth("130px");
        		this.txtLitros.addStyleName(ValoTheme.TEXTFIELD_TINY);
        		this.txtLitros.addStyleName("rightAligned");
        		
        		this.txtPorcentaje= new TextField("Porcentaje");    		
        		this.txtPorcentaje.setEnabled(true);
        		this.txtPorcentaje.setWidth("130px");
        		this.txtPorcentaje.addStyleName(ValoTheme.TEXTFIELD_TINY);
        		this.txtPorcentaje.addStyleName("rightAligned");
        		
				linea1.addComponent(this.cmbVariedad);
				linea1.addComponent(this.txtLitros);
				linea1.addComponent(this.txtPorcentaje);
    			
    		HorizontalLayout linea6 = new HorizontalLayout();
    			linea6.setSpacing(true);
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

    			
    			linea6.addComponent(this.btnLimpiar);
    			linea6.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea6.addComponent(this.btnInsertar);
    			linea6.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea6.addComponent(this.btnEliminar);
    			linea6.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea6);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		this.setCreacion(true);
		limpiarCampos();
	}
	
	private void eliminar()
	{
		consultaRecepcionEMCSServer crs= consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
		MapeoRecepcionEMCSVariedades mapeo = new MapeoRecepcionEMCSVariedades();
		mapeo.setIdEntrada(idEntrada);
		mapeo.setNombre(cmbVariedad.getValue().toString());
		
		crs.eliminarEnlaceEntradaVariedad(mapeo);
		
		if (gridDatos!=null)
		{
			gridDatos.removeAllColumns();
			gridDatos=null;
			frameGrid.removeComponent(panelGrid);
			panelGrid=null;
		}
		llenarRegistros();
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			consultaRecepcionEMCSServer crs = consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
			MapeoRecepcionEMCSVariedades mapeo = new MapeoRecepcionEMCSVariedades();
			
			mapeo.setIdEntrada(this.mapeoRecepcion.getIdCodigo());
			mapeo.setPorcentaje(this.txtPorcentaje.getValue());
			mapeo.setLitros(this.txtLitros.getValue());
			mapeo.setNombre(this.cmbVariedad.getValue().toString());
			
			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = crs.guardarEnlaceGranelVariedad(mapeo);
				if (rdo==null)
				{
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoRecepcionEmcsVariedades.getIdCodigo());
				rdo = crs.guardarCambiosEnlaceEntradaVariedad(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoRecepcionEMCSVariedades> vector=null;
    	consultaRecepcionEMCSServer crs = consultaRecepcionEMCSServer.getInstance(CurrentUser.get());
    	
    	MapeoRecepcionEMCSVariedades mapeoPreop = new MapeoRecepcionEMCSVariedades();
    	mapeoPreop.setIdEntrada(this.mapeoRecepcion.getIdCodigo());
    	
    	vector = crs.datosEenlaceEntradaVariedad(mapeoPreop);

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoRecepcionEMCSVariedades r_mapeo)
	{
		this.mapeoRecepcion = new MapeoRecepcionEMCS();
		this.mapeoRecepcionEmcsVariedades = new MapeoRecepcionEMCSVariedades();
		
		this.mapeoRecepcion.setIdCodigo(r_mapeo.getIdEntrada());
		this.mapeoRecepcionEmcsVariedades.setIdVariedad(r_mapeo.getIdVariedad());
		this.mapeoRecepcionEmcsVariedades.setIdEntrada(r_mapeo.getIdEntrada());
		this.mapeoRecepcionEmcsVariedades.setIdCodigo(r_mapeo.getIdCodigo());
		
		this.txtPorcentaje.setValue(r_mapeo.getPorcentaje());
		this.txtLitros.setValue(r_mapeo.getLitros());
		this.cmbVariedad.setValue(r_mapeo.getNombre());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtPorcentaje.setEnabled(r_activar);
    	txtLitros.setEnabled(r_activar);
    	cmbVariedad.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.cmbVariedad.setValue(null);
		this.txtLitros.setValue("");
		this.txtPorcentaje.setValue("");
	}
	
    private boolean todoEnOrden()
    {
    	if (this.cmbVariedad.getValue()==null || this.cmbVariedad.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar la variedad");
    		return false;
    	}
    	if ((this.txtPorcentaje.getValue()==null || this.txtPorcentaje.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el porcentaje");
    		return false;
    	}
    	if ((this.txtLitros.getValue()==null || this.txtLitros.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar los litros");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboVariedad();
    }

    
    private void cargarComboVariedad()
    {
    	consultaVariedadesDgaServer cvs = consultaVariedadesDgaServer.getInstance(CurrentUser.get());
    	ArrayList<MapeoVariedadesDga> vectorVariedades = cvs.datosVariedadesDgaGlobal(null);
    	
    	for (int i = 0; i<vectorVariedades.size();i++)
    	{
    		MapeoVariedadesDga mapeo = vectorVariedades.get(i); 
    		this.cmbVariedad.addItem(mapeo.getDescripcion());
    	}
    }
 
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoRecepcionEMCSVariedades) r_fila)!=null)
    	{
    		MapeoRecepcionEMCSVariedades mapeoGrifo = (MapeoRecepcionEMCSVariedades) r_fila;
    		if (mapeoGrifo!=null) this.llenarEntradasDatos(mapeoGrifo);
    	}    			
    	else 
    	{
    		this.setCreacion(true);
    		this.limpiarCampos();
    	}
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoRecepcionEMCSVariedades> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Variedades Seleccionadas ");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoRecepcionEMCSVariedades> container = new BeanItemContainer<MapeoRecepcionEMCSVariedades>(MapeoRecepcionEMCSVariedades.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("nombre").setHeaderCaption("Variedad");
			    	getColumn("nombre").setWidth(new Double(220));
			    	getColumn("porcentaje").setHeaderCaption("Porcentaje");
			    	getColumn("porcentaje").setWidth(new Double(100));

					getColumn("idCodigo").setHidden(true);
					getColumn("idEntrada").setHidden(true);
					getColumn("idVariedad").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
						setColumnOrder("nombre", "porcentaje");
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
			panelGrid.setContent(gridDatos);
			this.setCreacion(false);
		}
		else
		{
			this.setCreacion(true);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}