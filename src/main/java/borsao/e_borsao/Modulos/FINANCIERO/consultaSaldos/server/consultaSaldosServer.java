package borsao.e_borsao.Modulos.FINANCIERO.consultaSaldos.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;

public class consultaSaldosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
//	private Connection conMysql = null;
	private Statement cs = null;
	private static consultaSaldosServer instance;
	
	
	public consultaSaldosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSaldosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSaldosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoGlobal> datosOpcionesGlobal()
	{
		ArrayList<MapeoGlobal> vector = null;
		return vector;
	}
	
	public String generarInforme(ArrayList<MapeoGlobal> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer cuadre = null; 
		try
		{
			cadenaSQL.append(" SELECT sum(c_con_0.saldo) sa_sal ");
	     	cadenaSQL.append(" FROM c_con_0 ");
	     	cadenaSQL.append(" where grado = 1 ");

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			cuadre = 0;
			
			while(rsOpcion.next())
			{
				cuadre = rsOpcion.getInt("sa_sal");
			}
			
			if (cuadre!=0)
			{
				return "2";
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return "0";
	}

	public void eliminar()
	{
	}

	public String guardarNuevo(MapeoGlobal r_mapeo)
	{
		return null;
	}
	
}