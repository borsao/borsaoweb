package borsao.e_borsao.Modulos.FINANCIERO.consultaApuntes.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;

public class consultaApuntesServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaApuntesServer instance;
	
	
	public consultaApuntesServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaApuntesServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaApuntesServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoGlobal> datosOpcionesGlobal()
	{
		return null;
	}
	



	
	public String generarInforme(ArrayList<MapeoGlobal> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer cuadre = null; 
		try
		{
			cadenaSQL.append(" SELECT c_his_0.tipo ap_tip, ");
	        cadenaSQL.append(" Sum(c_his_0.importe) ap_imp ");
	        
	     	cadenaSQL.append(" FROM c_his_0 ");
	     	cadenaSQL.append(" GROUP BY tipo ");

			con= this.conManager.establecerConexionGestion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			cuadre = 0;
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("ap_tip").equals("D"))
				{
					cuadre = cuadre + rsOpcion.getInt("ap_imp");
				}
				else
				{
					cuadre = cuadre - rsOpcion.getInt("ap_imp");
				}
			}
			
			if (cuadre!=0)
			{
				return "2";
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return "0";
	}

	public void eliminar()
	{
	}

	public String guardarNuevo(MapeoGlobal r_mapeo)
	{
		
		return null;
	}
	
}