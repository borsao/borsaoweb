package borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.modelo.MapeoEtiquetasPalets;

public class consultaEtiquetasPaletsServer 
{
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private Connection con = null;
	private Statement cs = null;
	private static consultaEtiquetasPaletsServer instance;

	public consultaEtiquetasPaletsServer(String r_usuario) 
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasPaletsServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEtiquetasPaletsServer(r_usuario);
		}
		return instance;
	}

	public boolean generarDatosEtiquetas(MapeoEtiquetasPalets r_mapeo) {
		String datosGenerados = null;

		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		MapeoEtiquetasPalets mapeoPalets = new MapeoEtiquetasPalets();

		mapeoPalets.setReferencia(r_mapeo.getReferencia());
		mapeoPalets.setDestino(r_mapeo.getDestino());
		mapeoPalets.setArticulo(r_mapeo.getArticulo());
		mapeoPalets.setDescripcion(r_mapeo.getDescripcion());
		mapeoPalets.setSscc(r_mapeo.getSscc());
		mapeoPalets.setCajas(r_mapeo.getCajas());
		mapeoPalets.setCliente(r_mapeo.getCliente());

		try {
			/*
			 * Borro datos anteriores si los hubiera
			 */
			this.eliminar(mapeoPalets.getCliente());
			/*
			 * Genero los datos para la impresion
			 */
			datosGenerados = this.guardarNuevo(mapeoPalets);

			if (datosGenerados == null)
			{
				return true;
			}
			else
				return false;
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}

		return false;
	}

	public void eliminar(String r_cod) {
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		try {
			cadenaSQL.append(" DELETE FROM prd_etiquetas_palets ");
			cadenaSQL.append(" WHERE prd_etiquetas_palets.cliente = ?");
			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_cod);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}
	}

	public String guardarNuevo(MapeoEtiquetasPalets r_mapeo) {
		PreparedStatement preparedStatement = null;

		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO prd_etiquetas_palets ( ");
			cadenaSQL.append(" prd_etiquetas_palets.cliente,");
			cadenaSQL.append(" prd_etiquetas_palets.articulo,");
			cadenaSQL.append(" prd_etiquetas_palets.referencia,");
			cadenaSQL.append(" prd_etiquetas_palets.destino,");
			cadenaSQL.append(" prd_etiquetas_palets.descripcion ,");
			cadenaSQL.append(" prd_etiquetas_palets.code ,");
			cadenaSQL.append(" prd_etiquetas_palets.sscc,");
			cadenaSQL.append(" prd_etiquetas_palets.cajas) VALUES (");
			
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");

			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());

			if (r_mapeo.getCliente() != null) {
				preparedStatement.setString(1, r_mapeo.getCliente());
			} else {
				preparedStatement.setString(1, null);
			}
			
			if (r_mapeo.getArticulo() != null) {
				preparedStatement.setString(2, r_mapeo.getArticulo());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getReferencia() != null) {
				preparedStatement.setString(3, r_mapeo.getReferencia());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getDestino() != null) {
				preparedStatement.setString(4, r_mapeo.getDestino());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getDescripcion() != null) {
				preparedStatement.setString(5, r_mapeo.getDescripcion());
			} else {
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getCode() != null) {
				preparedStatement.setString(6, r_mapeo.getCode());
			} else {
				preparedStatement.setString(6, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(7, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(7, null);
			}
			preparedStatement.setInt(8, r_mapeo.getCajas());

			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		return null;
	}

	public String generarInforme(String r_id) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_id);
		libImpresion.setArchivoDefinitivo("/etiqueta" + r_id.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("etiquetasPalets.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasPalets");
		libImpresion.setBackGround(null);

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}
	
	
	
}