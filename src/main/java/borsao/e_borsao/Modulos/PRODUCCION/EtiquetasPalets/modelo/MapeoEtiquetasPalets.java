package borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasPalets extends MapeoGlobal
{
	private String cliente; 
	private String articulo; 
	private String destino;
	private String code;
	private String sscc;
	private String referencia; 
	private Integer cajas;
	private String descripcion;
	
	
	public MapeoEtiquetasPalets()
	{
		
	}


	public Integer getCajas() {
		return cajas;
	}


	public void setCajas(Integer ejercicio) {
		this.cajas = ejercicio;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String documento) {
		this.articulo = documento;
	}


	public String getDestino() {
		return destino;
	}


	public void setDestino(String numero) {
		this.destino = numero;
	}


	public String getSscc() {
		return sscc;
	}


	public void setSscc(String sscc) {
		this.sscc = sscc;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getCliente() {
		return cliente;
	}


	public void setCliente(String nombre) {
		this.cliente = nombre;
	}


	public String getReferencia() {
		return referencia;
	}


	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String sscc1) {
		this.descripcion = sscc1;
	}


}