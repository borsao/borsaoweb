package borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.view;

import java.util.ArrayList;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.modelo.MapeoEtiquetasPalets;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasPalets.server.consultaEtiquetasPaletsServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasPalets extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtPedido=null;
	private TextField txtDestino=null;
	private TextField txtArticulo=null;
	private TextField txtDescripcion=null;
	private TextField txtSSCC=null;
	private TextField txtCajas=null;
	private ComboBox cmbPrinter = null;
	
	private consultaEtiquetasPaletsServer ceps = null;
	private EtiquetasPaletsView origen = null;
	
	public PeticionEtiquetasPalets(EtiquetasPaletsView r_view)
	{
		ceps = new consultaEtiquetasPaletsServer(CurrentUser.get());
		this.origen=r_view;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtPedido=new TextField("Pedido");
				this.txtPedido.setRequired(true);
				this.txtPedido.setWidth("200px");
				fila1.addComponent(txtPedido);

				this.txtDestino=new TextField("Destino");
				this.txtDestino.setRequired(true);
				this.txtDestino.setWidth("300px");
				fila1.addComponent(txtDestino);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setRequired(true);
				this.txtArticulo.setWidth("200px");
				fila2.addComponent(txtArticulo);

				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setRequired(true);
				this.txtDescripcion.setWidth("300px");
				fila2.addComponent(txtDescripcion);


			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtSSCC= new TextField("SSCC:");
				this.txtSSCC.setWidth("200px");
				this.txtSSCC.setRequired(true);
				fila3.addComponent(txtSSCC);

				this.txtCajas=new TextField();
				this.txtCajas.setRequired(true);
				this.txtCajas.setWidth("100px");
				this.txtCajas.setCaption("Cajas");
				fila3.addComponent(txtCajas);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				fila4.addComponent(cmbPrinter);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.cargarListeners();
	}

	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				MapeoEtiquetasPalets mapeo = null;
				if (todoEnOrden())
				{
					mapeo=rellenarMapeo();
					boolean generado = ceps.generarDatosEtiquetas(mapeo);
					if (generado)
					{
						String pdfGenerado = ceps.generarInforme(mapeo.getCliente());
						
						if(pdfGenerado.length()>0)
						{
							if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
							{
								RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
							}
							else
							{
								String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
								RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
							    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
							}
						}
				    	else
				    		Notificaciones.getInstance().mensajeError("Error en la generacion");
					}
					
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	
	private MapeoEtiquetasPalets rellenarMapeo()
	{
		MapeoEtiquetasPalets mapeo = new MapeoEtiquetasPalets();
		
		mapeo.setReferencia(txtPedido.getValue().trim());
		mapeo.setDestino(txtDestino.getValue().trim());
		mapeo.setArticulo(txtArticulo.getValue().trim());
		mapeo.setDescripcion(txtDescripcion.getValue().trim());
		mapeo.setSscc(txtSSCC.getValue().trim());
		mapeo.setCajas(new Integer(txtCajas.getValue().trim()));
		mapeo.setCliente("AUCHAN");
		return mapeo;
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.txtPedido.getValue()==null || this.txtPedido.getValue().toString().length()==0)
		{
			devolver = false;
			txtPedido.focus();
		}
		else if (this.txtDestino.getValue()==null || this.txtDestino.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtDestino.focus();
		}
		else if (this.txtArticulo.getValue()==null || this.txtArticulo.getValue().toString().length()==0) 
		{
			devolver =false;
			txtArticulo.focus();
		}
		else if (this.txtDescripcion.getValue()==null || this.txtDescripcion.getValue().toString().length()==0) 
		{
			devolver =false;
			txtDescripcion.focus();
		}
		else if (this.txtCajas.getValue()==null || this.txtCajas.getValue().toString().length()==0) 
		{
			devolver =false;
			txtCajas.focus();
		}
		else if (this.txtSSCC.getValue()==null || this.txtSSCC.getValue().toString().length()==0) 
		{
			devolver =false;
			txtSSCC.focus();
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

}