package borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server.consultaArticulosSMPTServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaSituacionEmbotelladoServer
{
	private static Integer minimoUnidades = 4200;
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private MapeoSituacionEmbotellado mapeoTotales = null;
	private static consultaSituacionEmbotelladoServer instance;
	private ArrayList<MapeoSituacionEmbotellado> vectorPreparados= null;	
	private ArrayList<MapeoSituacionEmbotellado> vectorJaulones= null;
	private ArrayList<MapeoSituacionEmbotellado> vectorReservado = null;
	private ArrayList<MapeoSituacionEmbotellado> vectorConfirmado = null;
	private ArrayList<String> vectorRetrabajo= null;
	private ArrayList<MapeoProgramacion> vectorProgramado = null;
	private ArrayList<MapeoProgramacionEnvasadora> vectorProgramadoMesa = null;
	private Double semanasPrevision = null;
	
	public consultaSituacionEmbotelladoServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSituacionEmbotelladoServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSituacionEmbotelladoServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoSituacionEmbotellado> datosOpcionesGlobal(boolean r_soloPendiente, boolean r_noCeros, Double r_previsionSemanas)
	{
		Connection conGreensys = null;	
		Statement csGREENsys = null;
		Connection conMysql = null;
		Statement csMysql = null;

		semanasPrevision = r_previsionSemanas;
		Integer comparativo = null;
		Integer mediaVentas = 0;
		ResultSet rsOpcion = null;
		HashMap<String, Integer> hashPrepedido = null;
		HashMap<String, Integer> hashReservado = null;
		HashMap<String, Integer> hashJaulon = null;
		HashMap<String, Integer> hashConfirmado = null;
		HashMap<String, String> hashVino = null;
		HashMap<String, String> hashBotella = null;
		HashMap<String, String> hashTapon = null;
		HashMap<String, Integer> hashVentasActual = null;
		HashMap<String, Integer> hashVentasAnterior = null;
		HashMap<String, Double> hashCuantos = null;
		
		String almacenesCalculo=null;
		ArrayList<MapeoSituacionEmbotellado> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());
		almacenesCalculo = cas.obtenerAlmacenesCalculoPendienteServir();
		
		cadenaSQL.append(" SELECT c.descripcion se_tip, ");
		cadenaSQL.append(" a.articulo se_art, ");		 
        cadenaSQL.append(" b.descrip_articulo se_des, ");
        cadenaSQL.append(" sum(a.existencias) se_exa, ");
        cadenaSQL.append(" sum(a.stock_minimo) se_exm, ");
        cadenaSQL.append(" sum(a.lote_pedido) se_lp, ");
        cadenaSQL.append(" sum(a.pdte_servir) se_pse, ");
//        cadenaSQL.append(" sum(a.sal_deposito) se_dep, ");
        cadenaSQL.append(" sum(a.pdte_recibir) se_pre  ");        
     	cadenaSQL.append(" FROM a_exis_0 a, e__grp_c c, e__grp_l l, e_articu b");
     	cadenaSQL.append(" where (a.articulo = b.articulo and a.almacen in " + almacenesCalculo + ") ");     	
     	cadenaSQL.append(" and b.articulo = l.codigo ");
     	cadenaSQL.append(" and b.color = 'S' ");
     	cadenaSQL.append(" and l.grupo = c.grupo ");     	
     	cadenaSQL.append(" and (a.articulo matches '0102*' or a.articulo matches '0111*') " );
     	cadenaSQL.append(" and b.tipo_articulo='PT' ");     	
//     	cadenaSQL.append(" and a.articulo='0102006' ");
     	cadenaSQL.append(" and c.grupo<800 ");
     	cadenaSQL.append(" group by 1,2,3 ");
     	cadenaSQL.append(" order by 1,2 ");
		try
		{
			conMysql = this.conManager.establecerConexionInd();			
			csMysql = conMysql.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);

			conGreensys= this.conManager.establecerConexionGestionInd();
			csGREENsys = conGreensys.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			
			hashPrepedido = this.recogerPrepedido();
			hashReservado = this.recogerReservado();
			hashJaulon = this.recogerJaulon();
			hashConfirmado = this.recogerConfirmado();
			
			
			hashVino = this.recogerMP("0101");
			hashBotella = this.recogerMP("0104");
			hashTapon = this.recogerMP("0105");
			
			this.vectorRetrabajo = crs.comprobarRetrabajos();
			this.recogerProgramado();
			this.recogerProgramadoMesa();
			
			if (eBorsao.get().accessControl.getNombre().contains("Claveria") &&  eBorsao.superUsuario)
			{
				hashVentasActual = this.recuperarMovimientosVentasArticuloEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
				hashVentasAnterior = this.recuperarMovimientosVentasArticuloEjercicio(new Integer(RutinasFechas.añoActualYYYY())-1);
				hashCuantos = this.recuperarArticuloCuantosMeses();
			}
			
			rsOpcion= csGREENsys.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoSituacionEmbotellado>();
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("se_art").trim().contentEquals("0102049"))
				{
					mediaVentas = 0;
				}
				mediaVentas = 0;
				comparativo = 0;
				if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
				{
					if (hashVentasActual!=null && hashVentasActual.get(rsOpcion.getString("se_art").trim())!=null) 
					{
						mediaVentas = hashVentasActual.get(rsOpcion.getString("se_art").trim()).intValue();
					}
					if (hashVentasAnterior!=null && hashVentasAnterior.get(rsOpcion.getString("se_art").trim())!=null) 
					{
						mediaVentas = mediaVentas + hashVentasAnterior.get(rsOpcion.getString("se_art").trim()).intValue();
					}
					
					mediaVentas = mediaVentas / 12;
					
//					if (mediaVentas < this.minimoUnidades.intValue()) mediaVentas=0;
					
					comparativo = rsOpcion.getInt("se_pse") + mediaVentas;
				}
				else
				{
					comparativo = rsOpcion.getInt("se_pse");
				}
				if (comparativo!=0)
				{
					MapeoSituacionEmbotellado mapeo = new MapeoSituacionEmbotellado();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setArticulo(rsOpcion.getString("se_art"));
					mapeo.setDescripcion(rsOpcion.getString("se_des"));
					
					mapeo.setPdte_recibir(rsOpcion.getInt("se_pre"));
					mapeo.setPdte_servir(rsOpcion.getInt("se_pse"));
					mapeo.setStock_actual(rsOpcion.getInt("se_exa"));				
					mapeo.setStock_minimo(mediaVentas);
					
					if  (hashPrepedido.get(mapeo.getArticulo().trim())==null ) mapeo.setPrepedido(0); else mapeo.setPrepedido(hashPrepedido.get(mapeo.getArticulo().trim()));
					if  (hashReservado.get(mapeo.getArticulo().trim())==null ) mapeo.setReservado(0); else mapeo.setReservado(hashReservado.get(mapeo.getArticulo().trim()));
					if  (hashJaulon.get(mapeo.getArticulo().trim())==null ) mapeo.setStock_Jaulon(0); else mapeo.setStock_Jaulon(hashJaulon.get(mapeo.getArticulo().trim()));
					if  (hashConfirmado.get(mapeo.getArticulo().trim())==null ) mapeo.setConfirmado(0); else mapeo.setConfirmado(hashConfirmado.get(mapeo.getArticulo().trim()));

					if  (hashBotella.get(mapeo.getArticulo().trim())==null ) mapeo.setBotella(""); else mapeo.setBotella(hashBotella.get(mapeo.getArticulo().trim()));
					if  (hashTapon.get(mapeo.getArticulo().trim())==null ) mapeo.setTapon(""); else mapeo.setTapon(hashTapon.get(mapeo.getArticulo().trim()));						
					
					if  (hashVino.get(mapeo.getArticulo().trim())==null ) mapeo.setTipoVino(rsOpcion.getString("se_tip")); else mapeo.setTipoVino(hashVino.get(mapeo.getArticulo().trim()));

					mapeo.setRetrabajo(this.obtenerRetrabajo(mapeo.getArticulo()));
					mapeo.setStock_real(0);
				
					Double meses = new Double(0);
					boolean necesito = false;
					boolean llenado = false;
					
					Integer progr = new Integer(0);
					Integer calculoNecesidad = new Integer(0);
					
					if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
					{
						MapeoProgramacion map = this.obtenerProgramado(mapeo.getArticulo().trim());
						if (map!=null && map.getUnidades()!=null) progr = map.getUnidades();
						
						if (hashCuantos.get(mapeo.getArticulo().trim())!=null && new Double(hashCuantos.get(mapeo.getArticulo().trim()))!=0)
						{
							meses = hashCuantos.get(mapeo.getArticulo().trim());
							if (((mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()+progr-mapeo.getPdte_servir() - ((comparativo/meses)-mapeo.getPdte_servir())*semanasPrevision)< comparativo))
							{
								necesito = true;
								calculoNecesidad = (comparativo- mapeo.getPdte_servir()) - new Double(mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()+progr-mapeo.getPdte_servir() - ((comparativo/meses)-mapeo.getPdte_servir())*semanasPrevision).intValue();
							}
							
						}
						else
						{
							if ((mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()+progr) <  comparativo)
							{
								necesito = true;
								calculoNecesidad = (comparativo) - new Double(mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()+progr).intValue();
							}
						}
						if (calculoNecesidad<0) mapeo.setStock_real(0); else mapeo.setStock_real(calculoNecesidad);
					}
					else
					{
						if ((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()) > 0)
						{
							necesito = true;
							mapeo.setStock_real((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()));
						}
					}
					
					if (necesito)
					{
						
						if (eBorsao.get().accessControl.getNombre().contains("Claveria")&& eBorsao.superUsuario)
						{
							if (calculoNecesidad > 0 && calculoNecesidad > mapeo.getStock_Jaulon()) llenado=true; 
							if (calculoNecesidad > 0 && calculoNecesidad <= mapeo.getStock_Jaulon()) llenado=false;
						}
						else
						{
							if ((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()) > mapeo.getStock_Jaulon()) llenado=true; else llenado=false;
						}
					
//					if ((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()) > 0)
//						mapeo.setStock_real((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()));
//						if ((comparativo) - (mapeo.getStock_actual()+mapeo.getPrepedido()+mapeo.getReservado()) > mapeo.getStock_Jaulon())
						if (llenado)
						{
							/*
							 * embotellar
							 */
							if (eBorsao.get().accessControl.getNombre().contains("Claveria")&& eBorsao.superUsuario)
							{
								if ((mapeo.getStock_actual()-mapeo.getPdte_servir()+mapeo.getPrepedido()+mapeo.getReservado() > 0) && mapeo.getStock_real()>0)
								{
									mapeo.setNegativo("sb"); // contra stock embotellando
								}
								else
									mapeo.setNegativo("rb");
							}
							else mapeo.setNegativo("rb");
						}
						else
						{
							/*
							 * etiquetar
							 */
							if (eBorsao.get().accessControl.getNombre().contains("Claveria")&& eBorsao.superUsuario)
							{
								if ((mapeo.getStock_actual()-mapeo.getPdte_servir()+mapeo.getPrepedido()+mapeo.getReservado() > 0) && mapeo.getStock_real()>0 && mapeo.getStock_Jaulon()>0)
								{
									mapeo.setNegativo("se"); // contra stock etiquetando
								}
								else
									mapeo.setNegativo("vb");
							}
							else
							mapeo.setNegativo("vb");
						}
					}
					else
					{
						
						mapeo.setNegativo("");
					}
					
					if (mapeo.getArticulo().startsWith("0102"))
					{
						MapeoProgramacion map = this.obtenerProgramado(mapeo.getArticulo());
						if (map!=null)
						{
							mapeo.setProgramado("SI");
							mapeo.setProgramadas(map.getUnidades());
							
							if (map.getTipo().equals("EMBOTELLADO") && !mapeo.getNegativo().contentEquals("sb"))
							{
								if (map.getUnidades()>=mapeo.getStock_real() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mb");
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() >= mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mc");							
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() < mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mr");
								else
									mapeo.setNegativo("");
									
							}
							else if (map.getTipo().equals("ETIQUETADO") && !mapeo.getNegativo().contentEquals("se"))
							{
								if (map.getUnidades()>=mapeo.getStock_real() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ab");
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() >= mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ac");														
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() < mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ar");
								else
									mapeo.setNegativo("");
							}
						}
						else
						{
							mapeo.setProgramado("NO");
							mapeo.setProgramadas(new Integer(0));
						}
					}
					else if (mapeo.getArticulo().startsWith("0111"))
					{
						MapeoProgramacionEnvasadora map = this.obtenerProgramadoEnvasadora(mapeo.getArticulo());
						if (map!=null)
						{
							mapeo.setProgramado("SI");
							mapeo.setProgramadas(map.getUnidades());
							if (map.getTipo().equals("EMBOTELLADO") && !mapeo.getNegativo().contentEquals("sb"))
							{
								if (map.getUnidades()>=mapeo.getStock_real() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mb");
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() >= mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mc");							
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() < mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("mr");
								else
									mapeo.setNegativo("");
									
							}
							else if (map.getTipo().equals("ETIQUETADO") && !mapeo.getNegativo().contentEquals("se"))
							{
								if (map.getUnidades()>=mapeo.getStock_real() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ab");
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() >= mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ac");														
								else if (map.getUnidades()<mapeo.getStock_real() && map.getUnidades() < mapeo.getConfirmado() && mapeo.getStock_real()!=0)
									mapeo.setNegativo("ar");
								else
									mapeo.setNegativo("");
							}
						}
						else
						{
							mapeo.setProgramado("NO");
							mapeo.setProgramadas(new Integer(0));
						}
					}
					
					if (eBorsao.get().accessControl.getNombre().contains("Claveria")&& eBorsao.superUsuario)
					{
						
						if (mapeo.getStock_real().intValue()==0)
						{
							if (!r_noCeros) vector.add(mapeo);	
						}
						else
						{
							if (r_soloPendiente && mapeo.getProgramado().contentEquals("NO")) vector.add(mapeo);
							else if (r_soloPendiente && !mapeo.getNegativo().contains("mb") && !mapeo.getNegativo().contains("ab")) vector.add(mapeo);
							else if (!r_soloPendiente) vector.add(mapeo);
						}
					}
					else
						vector.add(mapeo);
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (csGREENsys!=null)
				{
					csGREENsys.close();
				}
				if (csMysql!=null)
				{
					csMysql.close();
				}
				if (conGreensys!=null)
				{
					conGreensys.close();
				}
				if (conMysql!=null)
				{
					conMysql.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		vectorPreparados= null;	
		vectorJaulones= null;
		vectorReservado = null;
		return vector;
	}
	
	private void totales(MapeoSituacionEmbotellado r_mapeo)
	{
		this.mapeoTotales.setStock_actual(this.mapeoTotales.getStock_actual()+r_mapeo.getStock_actual());
		this.mapeoTotales.setProgramadas(this.mapeoTotales.getProgramadas()+r_mapeo.getProgramadas());
		this.mapeoTotales.setStock_real(this.mapeoTotales.getStock_real()+r_mapeo.getStock_real());
//		this.mapeoTotales.setStock_Jaulon(this.mapeoTotales.getStock_actual()+r_mapeo.getStock_Jaulon());
		this.mapeoTotales.setPdte_recibir(this.mapeoTotales.getPdte_recibir()+r_mapeo.getPdte_recibir());
		this.mapeoTotales.setPdte_servir(this.mapeoTotales.getPdte_servir()+r_mapeo.getPdte_servir());
		this.mapeoTotales.setProgramado(this.mapeoTotales.getProgramado()+r_mapeo.getProgramado());
		this.mapeoTotales.setPrepedido(this.mapeoTotales.getPrepedido()+r_mapeo.getPrepedido());
//		this.mapeoTotales.setDeposito(this.mapeoTotales.getDeposito()+r_mapeo.getDeposito());
		this.mapeoTotales.setConfirmado(this.mapeoTotales.getConfirmado()+r_mapeo.getConfirmado());
		this.mapeoTotales.setReservado(this.mapeoTotales.getReservado()+r_mapeo.getReservado());
		this.mapeoTotales.setNegativo("");
		this.mapeoTotales.setProgramado("NO");
		
	}
	public String generarInforme(ArrayList<MapeoGlobal> r_vector, boolean regenerar, String r_destino)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

		if (regenerar)
		{
			this.eliminar();
			
			this.mapeoTotales = new MapeoSituacionEmbotellado();
			this.mapeoTotales.setDescripcion("TOTALES........");
			this.mapeoTotales.setStock_actual(0);
			this.mapeoTotales.setStock_real(0);
			this.mapeoTotales.setStock_Jaulon(0);
			this.mapeoTotales.setPdte_recibir(0);
			this.mapeoTotales.setPdte_servir(0);
			this.mapeoTotales.setPrepedido(0);
			this.mapeoTotales.setDeposito(0);
			this.mapeoTotales.setConfirmado(0);
			this.mapeoTotales.setReservado(0);
			this.mapeoTotales.setProgramadas(0);
			
			for (int i=0; i< r_vector.size();i++)
			{
				MapeoSituacionEmbotellado mapeo = (MapeoSituacionEmbotellado) r_vector.get(i);
				this.totales(mapeo);
				if (r_destino.contentEquals("Laboratorio"))
				{
//					if (mapeo.getTipoVino().trim().contentEquals("OTROS")) break;
					if (mapeo.getTapon().contains("CORCHO")) mapeo.setTapon("CORCHO"); else mapeo.setTapon("ROSCA");
					if (mapeo.getNegativo().contentEquals("rb") || mapeo.getNegativo().contentEquals("mb") || mapeo.getNegativo().contentEquals("mr") || mapeo.getNegativo().contentEquals("mc") || mapeo.getNegativo().contentEquals("sb"))
						this.guardarNuevo(mapeo);	
				}
				else
					this.guardarNuevo(mapeo);
			}
			if (!r_destino.contentEquals("Laboratorio")) this.guardarNuevo(this.mapeoTotales);
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	switch (r_destino)
    	{
	    	case "Laboratorio":
	    	{
	    		libImpresion.setArchivoDefinitivo("/previsionPlanificacion" + String.valueOf((new Date()).getTime()) + ".pdf");
	    		libImpresion.setArchivoPlantilla("planificacionBodega.jrxml");

	    		break;
	    	}
	    	case "Produccion":
	    	{
	    		libImpresion.setArchivoDefinitivo("/previsionPlanificacion" + String.valueOf((new Date()).getTime()) + ".pdf");
	    		libImpresion.setArchivoPlantilla("planificacionProduccion.jrxml");

	    		break;
	    	}
	    	case "Produccion Et.":
	    	{
	    		libImpresion.setArchivoDefinitivo("/previsionPlanificacion" + String.valueOf((new Date()).getTime()) + ".pdf");
	    		libImpresion.setArchivoPlantilla("planificacionProduccionEtiquetados.jrxml");

	    		break;
	    	}

	    	case "Planificacion":
	    	{
	    		libImpresion.setArchivoDefinitivo("/previsionPlanificacion" + String.valueOf((new Date()).getTime()) + ".pdf");
	    		libImpresion.setArchivoPlantilla("planificacionSemana.jrxml");

	    		break;
	    	}
	    	
	    	case "General":
	    	{
	    		libImpresion.setArchivoDefinitivo("/calculoNecesidades" + String.valueOf((new Date()).getTime()) + ".pdf");
	    		libImpresion.setArchivoPlantilla("calculoNecesidades.jrxml");
	    		break;
	    	}
    	}

    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado("calculoNecesidades.jasper");
    	libImpresion.setCarpeta("existencias");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	public String guardarNuevo(MapeoSituacionEmbotellado r_mapeo)
	{
		Connection con= null;
		PreparedStatement preparedStatement = null;
		Integer codigoInterno = 1;
		consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_calculo_necesidades( ");
			cadenaSQL.append(" prd_calculo_necesidades.idprd_calculo_necesidades,");
			cadenaSQL.append(" prd_calculo_necesidades.articulo ,");
			cadenaSQL.append(" prd_calculo_necesidades.descripcion ,");
			cadenaSQL.append(" prd_calculo_necesidades.tipoVino ,");
			cadenaSQL.append(" prd_calculo_necesidades.botella,");
			cadenaSQL.append(" prd_calculo_necesidades.tapon,");
			cadenaSQL.append(" prd_calculo_necesidades.stockActual,");
			cadenaSQL.append(" prd_calculo_necesidades.pdteServir ,");
			cadenaSQL.append(" prd_calculo_necesidades.deposito ,");
			cadenaSQL.append(" prd_calculo_necesidades.prepedido ,");
			cadenaSQL.append(" prd_calculo_necesidades.reservado,");
			cadenaSQL.append(" prd_calculo_necesidades.stockJaulon ,");
			cadenaSQL.append(" prd_calculo_necesidades.stockReal ,");
			cadenaSQL.append(" prd_calculo_necesidades.negativo ,");
			cadenaSQL.append(" prd_calculo_necesidades.programado ,");
			cadenaSQL.append(" prd_calculo_necesidades.programadas ,");
			cadenaSQL.append(" prd_calculo_necesidades.stockMinimo ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getDescripcion().contains("TOTALES"))
		    {
		    	r_mapeo.setTipoVino("ZZZZZZZZ");
		    }

		    preparedStatement.setInt(1, codigoInterno);
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getTipoVino()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTipoVino());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getBotella()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getBotella());	
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getTapon()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTapon());	
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getStock_actual()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getStock_actual());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getPdte_servir()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getPdte_servir());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getConfirmado()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getConfirmado());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getPrepedido()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getPrepedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getReservado()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getReservado());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getStock_Jaulon()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getStock_Jaulon());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getStock_real()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getStock_real());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getNegativo()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getNegativo());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getProgramado()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getProgramado());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setInt(16, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setInt(16, 0);
		    }
		    if (r_mapeo.getStock_minimo()!=null)
		    {
		    	preparedStatement.setInt(17, r_mapeo.getStock_minimo());
		    }
		    else
		    {
		    	preparedStatement.setInt(17, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }       
     	finally
     	{
     		if (preparedStatement!=null)
     		{
     			preparedStatement=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}

		return null;
	}
	
	public void eliminar()
	{
		Connection con= null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_calculo_necesidades ");            
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
     	finally
     	{
     		if (preparedStatement!=null)
     		{
     			preparedStatement=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}

	}

	
	private void recogerProgramado()
	{
		MapeoProgramacion mapeo = null;
		
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		mapeo = new MapeoProgramacion();
		mapeo.setEstado("X");
		
		vectorProgramado = cps.datosProgramacionGlobal(mapeo);
     	
	}

	private void recogerProgramadoMesa()
	{
		MapeoProgramacionEnvasadora mapeo = null;
		
		consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		mapeo = new MapeoProgramacionEnvasadora();
		mapeo.setEstado("A");
		
		vectorProgramadoMesa = cps.datosProgramacionGlobal(mapeo);
     	
	}

	private MapeoProgramacion obtenerProgramado(String r_articulo)
	{
		MapeoProgramacion mapeoDevolver = new MapeoProgramacion();
		mapeoDevolver.setUnidades(0);
		for (int i=0;i<vectorProgramado.size();i++)
		{
			MapeoProgramacion map = (MapeoProgramacion) vectorProgramado.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				mapeoDevolver.setArticulo(r_articulo);
				mapeoDevolver.setTipo(map.getTipo());
				mapeoDevolver.setUnidades(mapeoDevolver.getUnidades()+map.getUnidades());
			}
		}
		if (mapeoDevolver.getArticulo()!=null)
			return mapeoDevolver;
		else
			return null;
	}

	private MapeoProgramacionEnvasadora obtenerProgramadoEnvasadora(String r_articulo)
	{
		MapeoProgramacionEnvasadora mapeoDevolver = new MapeoProgramacionEnvasadora();
		mapeoDevolver.setUnidades(0);
		for (int i=0;i<vectorProgramadoMesa.size();i++)
		{
			MapeoProgramacionEnvasadora map = (MapeoProgramacionEnvasadora) vectorProgramadoMesa.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				mapeoDevolver.setArticulo(r_articulo);
				mapeoDevolver.setTipo(map.getTipo());
				mapeoDevolver.setUnidades(mapeoDevolver.getUnidades()+map.getUnidades());
			}
		}
		if (mapeoDevolver.getArticulo()!=null)
			return mapeoDevolver;
		else
			return null;
	}

	private HashMap<String, Integer> recogerConfirmado()
	{
		Connection con= null;
		Statement cs= null;

		ResultSet rsConfirmado = null;
		HashMap<String, Integer> hash = null;
		/*
		 * saco informacion prepedidos
		 */
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(a.unidades-unidades_serv) se_res, ");
		cadenaSQL.append(" a.articulo se_art ");
     	cadenaSQL.append(" FROM a_vp_l_0 a, a_vp_c_0 b, e_finali c ");
     	cadenaSQL.append(" where a.cumplimentacion is null ");
     	cadenaSQL.append(" and c.finalidad = b.finalidad ");
     	cadenaSQL.append(" and c.procesar = 'S' ");
     	cadenaSQL.append(" and a.clave_tabla = b.clave_tabla ");
     	cadenaSQL.append(" and a.documento = b.documento ");
     	cadenaSQL.append(" and a.serie = b.serie ");
     	cadenaSQL.append(" and a.codigo = b.codigo ");
     	cadenaSQL.append(" and a.movimiento = '51' ");
     	cadenaSQL.append(" group by 2 ");
     	

     	try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
     		Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsConfirmado = cs.executeQuery(cadenaSQL.toString());
			
			hash = new HashMap<String, Integer>();
			
			while(rsConfirmado.next())
			{
				hash.put(rsConfirmado.getString("se_art").trim(),rsConfirmado.getInt("se_res"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsConfirmado!=null)
     		{
     			rsConfirmado=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
     	return hash;
	}
	
	private HashMap<String , Integer> recogerReservado()
	{
		Connection con= null;
		Statement cs= null;

		ResultSet rsPrepedidos = null;
		HashMap<String , Integer> hash = null;
		/*
		 * saco informacion prepedidos
		 */
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(a.unidades-unidades_serv) se_res, ");
		cadenaSQL.append(" a.articulo se_art ");
     	cadenaSQL.append(" FROM a_vp_l_0 a ");
     	cadenaSQL.append(" where a.cumplimentacion is null ");
     	cadenaSQL.append(" and a.plazo_entrega = '31/12/2999' ");
     	cadenaSQL.append(" and a.movimiento = '51' ");
     	cadenaSQL.append(" group by 2 ");
     	

     	try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos = cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String , Integer> ();
			
			while(rsPrepedidos.next())
			{
				hash.put(rsPrepedidos.getString("se_art").trim(), rsPrepedidos.getInt("se_res") );
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsPrepedidos!=null)
     		{
     			rsPrepedidos=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
     	
     	return hash;
	}
	

	
	private Integer obtenerReservado(String r_articulo)
	{
		Integer rdo = null;
		
		rdo = 0;
		
		for (int i=0;i<vectorReservado.size();i++)
		{
			MapeoSituacionEmbotellado map = (MapeoSituacionEmbotellado) vectorReservado.get(i);
			if (map.getArticulo().equals(r_articulo.trim()))
			{
				rdo =map.getReservado();
				break;
			}
		}
		return rdo;
	}
	
	private String obtenerRetrabajo(String r_articulo)
	{
		String rdo = "";
		
		if (vectorRetrabajo.contains(r_articulo.trim()))
		{
			rdo = "Si";
		}
		
		return rdo;
	}

	private HashMap<String, Integer> recogerPrepedido()
	{
		Connection con= null;
		Statement cs= null;

		ResultSet rsPrepedidos = null;
		HashMap<String, Integer> hash = null;
		/*
		 * saco informacion prepedidos
		 */
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT a.existencias se_exa, ");
		cadenaSQL.append(" a.articulo se_art ");
     	cadenaSQL.append(" FROM a_exis_0 a ");
     	cadenaSQL.append(" where a.almacen in (6,12) ");
     	cadenaSQL.append(" and a.existencias <> 0 ");

     	try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsPrepedidos = cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String, Integer>();
			
			while(rsPrepedidos.next())
			{
				
				hash.put(rsPrepedidos.getString("se_art").trim(),rsPrepedidos.getInt("se_exa"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsPrepedidos!=null)
     		{
     			rsPrepedidos=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
     	return hash;
	}
	
	private Integer obtenerPreparado(String r_articulo)
	{
		Integer rdo = null;
		
		rdo = 0;
		
		for (int i=0;i<vectorPreparados.size();i++)
		{
			MapeoSituacionEmbotellado map = (MapeoSituacionEmbotellado) vectorPreparados.get(i);
			if (map.getArticulo().equals(r_articulo.trim()))
			{
				rdo =map.getPrepedido();
				break;
			}
		}
		return rdo;
	}

	private Integer obtenerConfirmado(String r_articulo)
	{
		Integer rdo = null;
		
		rdo = 0;
		
		for (int i=0;i<vectorConfirmado.size();i++)
		{
			MapeoSituacionEmbotellado map = (MapeoSituacionEmbotellado) vectorConfirmado.get(i);
			if (map.getArticulo().equals(r_articulo.trim()))
			{
				rdo =map.getConfirmado();
				break;
			}
		}
		return rdo;
	}

	private HashMap<String , Integer> recogerJaulon()
	{
		Connection con= null;
		Statement cs= null;

		ResultSet rsJaulon = null;
		HashMap<String , Integer> hash = null;
		/*
		 * busco el codigo de articulo del jaulon
		 */
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT sum(a.existencias) se_exa, ");
		cadenaSQL.append(" a.articulo se_jau, ");
		cadenaSQL.append(" b.padre se_pad ");
     	cadenaSQL.append(" FROM a_exis_0 a, e__lconj b, e_articu c ");     	
     	cadenaSQL.append(" where a.articulo = b.hijo ");
     	cadenaSQL.append(" and a.articulo = c.articulo ");
     	cadenaSQL.append(" and c.articulo = b.hijo ");
     	cadenaSQL.append(" and c.tipo_articulo = 'PS' ");
     	cadenaSQL.append(" and a.almacen in (1,4) ");
     	cadenaSQL.append(" and a.existencias <> 0 ");
     	cadenaSQL.append(" group by 2,3 ");
     	
     	hash = new HashMap<String , Integer>();
     	
     	try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs =con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
     		
			rsJaulon = cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsJaulon.next())
			{

				hash.put(rsJaulon.getString("se_pad").substring(0, 7),rsJaulon.getInt("se_exa"));

			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsJaulon!=null)
     		{
     			rsJaulon=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
     	return hash;
	}
	
	private HashMap<String , String> recogerMP(String r_mascara)
	{
		Connection con= null;
		Statement cs= null;

		ResultSet rsJaulon = null;
		HashMap<String , String> hash = null;
		/*
		 * busco el codigo de articulo del jaulon
		 */
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT a.padre se_pad, ");
		cadenaSQL.append(" a.hijo se_hij, ");
		cadenaSQL.append(" b.descrip_articulo se_bot ");
     	cadenaSQL.append(" FROM e__lconj a, e_articu b ");     	
     	cadenaSQL.append(" where a.hijo = b.articulo ");
     	cadenaSQL.append(" and a.hijo matches '" + r_mascara + "*'");
     	
     	hash = new HashMap<String , String>();
     	if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
     	{
	     	try
			{
				con= this.conManager.establecerConexionGestionInd();			
				cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	     		
				rsJaulon = cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				
				while(rsJaulon.next())
				{
					hash.put(rsJaulon.getString("se_pad").substring(0, 7), rsJaulon.getString("se_hij").substring(0, 7) + " " + rsJaulon.getString("se_bot"));
				}
	
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
				ex.printStackTrace();
			}
	     	finally
	     	{
	     		if (rsJaulon!=null)
	     		{
	     			rsJaulon=null;
	     		}
	     		if (cs!=null)
	     		{
	     			cs=null;
	     		}
	     		if (con!=null)
	     		{
	     			con=null;
	     		}
	     	}
     	}
     	return hash;
	}
	
	public String recogerArticuloJaulon(String r_padre)
	{
		Connection con= null;
		Statement cs= null;
		ResultSet rsJaulon = null;
		/*
		 * busco el codigo de articulo del jaulon
		 */
		
		StringBuffer cadenaSQL = new StringBuffer();
		cadenaSQL.append(" SELECT a.existencias se_exa, ");
		cadenaSQL.append(" a.articulo se_jau, ");
		cadenaSQL.append(" b.padre se_pad ");
     	cadenaSQL.append(" FROM a_exis_0 a, e__lconj b ");     	
     	cadenaSQL.append(" where a.articulo = b.hijo ");
     	cadenaSQL.append(" and a.almacen in (1,4) ");
     	cadenaSQL.append(" and a.existencias <> 0 ");
     	cadenaSQL.append(" and (b.hijo matches '0102*' or b.hijo matches '0111*') ");
     	cadenaSQL.append(" and b.padre = '" + r_padre.trim() + "-1'" );
     	
     	try
		{
			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(rsJaulon.TYPE_SCROLL_SENSITIVE,rsJaulon.CONCUR_READ_ONLY);
			rsJaulon = cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsJaulon.next())
			{
				return rsJaulon.getString("se_jau");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
     	finally
     	{
     		if (rsJaulon!=null)
     		{
     			rsJaulon=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
     	return null; 
	}
	
	private Integer obtenerJaulon(String r_articulo)
	{
		Integer rdo = null;
		
		rdo = 0;
		
		for (int i=0;i<vectorJaulones.size();i++)
		{
			MapeoSituacionEmbotellado map = (MapeoSituacionEmbotellado) vectorJaulones.get(i);
			if (map.getArticulo().equals(r_articulo.trim()))
			{
				rdo =map.getStock_Jaulon();
				break;
			}
		}
		return rdo;
	}

	private HashMap<String,	Integer> recuperarMovimientosVentasArticuloEjercicio(Integer r_ejercicio)
	{
		ResultSet rsMovimientos = null;
		HashMap<String, Integer> hash =null;
		
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT a_lin_" + digito + ".articulo as art, ");
			sql.append(" SUM(a_lin_" + digito + ".unidades*prd_smpt.cuantos) as total ");
			  
			sql.append(" FROM a_lin_" + digito );
			sql.append(" inner join prd_smpt on prd_smpt.articulo = a_lin_" + digito + ".articulo");
			sql.append(" WHERE clave_tabla in ('A','F') ");
			sql.append(" and (a_lin_" + digito + ".articulo like '0102%' or a_lin_" + digito + ".articulo like '0111%') ");
			sql.append(" and movimiento > '49' ");
//			sql.append(" and articulo in (select articulo from prd_smpt) ");
			if (digito == "0") sql.append(" and month(fecha_documento) <=  " + (new Integer(RutinasFechas.mesActualMM()).intValue()-1));
			else sql.append(" and month(fecha_documento) >  " + (new Integer(RutinasFechas.mesActualMM()).intValue()-1));
			
			sql.append(" GROUP BY a_lin_" + digito + ".articulo ");
			
//			if (digito == "0") sql.append(" having sum(unidades) >= " + (minimoUnidades*(new Integer(RutinasFechas.mesActualMM()).intValue()-1)) );
//			else sql.append(" having sum(unidades) > " + (minimoUnidades*(12 - (new Integer(RutinasFechas.mesActualMM()).intValue()-1))) );
				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			int tot = new Integer(0);
			hash = new HashMap<String, Integer>();
			while(rsMovimientos.next())
			{
//				if (digito == "0") tot = rsMovimientos.getInt("total")/(new Integer(RutinasFechas.mesActualMM()).intValue()-1);
//				else
//					tot = rsMovimientos.getInt("total")/(12 - (new Integer(RutinasFechas.mesActualMM()).intValue()-1));
				
				hash.put(rsMovimientos.getString("art").trim(), rsMovimientos.getInt("total"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
     	{
     		if (rsMovimientos!=null)
     		{
     			rsMovimientos=null;
     		}
     		if (cs!=null)
     		{
     			cs=null;
     		}
     		if (con!=null)
     		{
     			con=null;
     		}
     	}
		return hash;
	}
	
	private HashMap<String,	Double> recuperarArticuloCuantosMeses()
	{
		HashMap<String, Double> hash =null;
		
		consultaArticulosSMPTServer casmp = new consultaArticulosSMPTServer(CurrentUser.get());
		hash=casmp.hashCuantos();
		
		return hash;
	}
}