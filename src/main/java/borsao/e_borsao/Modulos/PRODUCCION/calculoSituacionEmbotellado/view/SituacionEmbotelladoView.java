package borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.server.consultaSituacionEmbotelladoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SituacionEmbotelladoView extends GridViewRefresh {

	public static final String VIEW_NAME = "Situacion Embotellado";
	private final String titulo = "Calculo Necesidades";
	private final int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private HashMap<String,String> filtrosRec = null;
	private Double semanasPrevision = null;
	private boolean generado = false;
	private CheckBox noCeros = null;
	private CheckBox soloPendiente = null;
	private TextField semanasPrev = null;
	
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public SituacionEmbotelladoView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
//    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	this.noCeros = new CheckBox("Ver solo los distintos de 0? ");
    	this.noCeros.setValue(false);
    	this.soloPendiente = new CheckBox("Ver solo pendiente? ");
    	this.soloPendiente.setValue(false);
    	this.semanasPrev = new TextField("Semanas Prevision");
    	this.semanasPrev .setStyleName(ValoTheme.TEXTFIELD_TINY);
    	this.semanasPrev.setValue("0");

    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
    	{
    		this.topLayoutC.addComponent(this.noCeros);
    		this.topLayoutC.setComponentAlignment(this.noCeros, Alignment.BOTTOM_LEFT);
    		this.topLayoutC.addComponent(this.soloPendiente);
    		this.topLayoutC.setComponentAlignment(this.soloPendiente, Alignment.BOTTOM_LEFT);
    		this.topLayoutC.addComponent(this.semanasPrev);
    		this.topLayoutC.setComponentAlignment(this.semanasPrev, Alignment.BOTTOM_LEFT);

    	}
    	this.cargarListeners();
    	this.generarGrid(null);
    	this.barAndGridLayout.removeComponent(this.cabLayout);
    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSituacionEmbotellado> r_vector=null;
//    	hashToMapeo hm = new hashToMapeo();
    	
//    	MapeoPedidosCompras=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaSituacionEmbotelladoServer cse = new consultaSituacionEmbotelladoServer(CurrentUser.get());
    	if (this.semanasPrev.getValue()!=null && this.semanasPrev.getValue().length()>0)
    	{
    		if (this.semanasPrev.getValue()!="0")
    			semanasPrevision = new Double(this.semanasPrev.getValue())/4;
    		else
    			semanasPrevision = new Double(0);
    	}
    	r_vector=cse.datosOpcionesGlobal(this.soloPendiente.getValue(), this.noCeros.getValue(),semanasPrevision);
    	
    	grid = new OpcionesGrid(r_vector,this);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    		
    		((OpcionesGrid) grid).establecerFiltros(filtrosRec);
    	}
    }

    private void cargarListeners()
    {
    	this.noCeros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;	
					setHayGrid(false);
				}
				generarGrid(null);
			}
		});
    	this.soloPendiente.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;	
					setHayGrid(false);
				}
				generarGrid(null);
			}
		});
    }
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    public void actualizarDatos()
    {
		if (isHayGrid())
		{
			filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;	
			setHayGrid(false);
		}
		generarGrid(null);

    }
    
    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(true);
    }

    public void print() 
    {

    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
