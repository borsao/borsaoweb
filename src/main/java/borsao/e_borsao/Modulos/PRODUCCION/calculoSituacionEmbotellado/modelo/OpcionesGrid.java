package borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.sort.Sort;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionFormatoImpresion;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionAsignacionProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.server.consultaSituacionEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view.SituacionEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view.pantallaCalculoNecesidades;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private boolean conTotales=true;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
	private SituacionEmbotelladoView padre =null;
	private pantallaCalculoNecesidades padreWin =null;
	
    public OpcionesGrid(ArrayList<MapeoSituacionEmbotellado> r_vector, SituacionEmbotelladoView r_app) 
    {
        
        this.vector=r_vector;
        this.padre = r_app;
        
		this.asignarTitulo("Cálculo Necesidades");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    public OpcionesGrid(ArrayList<MapeoSituacionEmbotellado> r_vector, pantallaCalculoNecesidades r_app) 
    {
        
        this.vector=r_vector;
        this.padreWin = r_app;
        
		this.asignarTitulo("Cálculo Necesidades");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("ver");
    	this.camposNoFiltrar.add("biblia");
    	this.camposNoFiltrar.add("mail");
    	this.camposNoFiltrar.add("negativo");
    	this.camposNoFiltrar.add("retrabajo");
    	this.camposNoFiltrar.add("preparado");
    	this.camposNoFiltrar.add("stock_actual");
    	this.camposNoFiltrar.add("stock_real");
    	this.camposNoFiltrar.add("stock_minimo");
    	this.camposNoFiltrar.add("stock_Jaulon");
    	this.camposNoFiltrar.add("pdte_servir");
    	this.camposNoFiltrar.add("pdte_recibir");
    	this.camposNoFiltrar.add("reservado");
    	this.camposNoFiltrar.add("confirmado");
    	this.camposNoFiltrar.add("prepedido");
    	this.camposNoFiltrar.add("programado");
    }
    
    private void generarGrid()
    {
    	this.setConTotales(this.conTotales);
//		this.actualizar = false;
		this.crearGrid(MapeoSituacionEmbotellado.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(7);
		if (eBorsao.get().accessControl.getNombre().contains("Claveria"))//&& eBorsao.superUsuario)
		{
			this.setFrozenColumnCount(11);
		}
		this.calcularTotal();
		this.sort(Sort.by("tipoVino", SortDirection.ASCENDING)
	              .then("botella", SortDirection.ASCENDING)
	              .then("tapon", SortDirection.ASCENDING)
	              .then("stock_real", SortDirection.DESCENDING));
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("programado", "negativo", "retrabajo",  "mail", "biblia", "ver",   "tipoVino", "botella", "tapon", "articulo","descripcion","stock_minimo", "stock_actual","pdte_servir","confirmado", "deposito", "reservado", "prepedido", "stock_real","stock_Jaulon", "programadas");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.widthFiltros.put("tipoVino", "75");
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "250");
    	this.widthFiltros.put("botella", "80");
    	this.widthFiltros.put("tapon", "80");
    	
    	this.getColumn("mail").setHeaderCaption("");
    	this.getColumn("mail").setSortable(false);
    	this.getColumn("mail").setWidth(new Double(50));
    	this.getColumn("mail").setHidden(true);
    	this.getColumn("deposito").setHidden(true);
    	
    	this.getColumn("ver").setHeaderCaption("");
    	this.getColumn("ver").setSortable(false);
    	this.getColumn("ver").setWidth(new Double(50));

    	this.getColumn("biblia").setHeaderCaption("");
    	this.getColumn("biblia").setSortable(false);
    	this.getColumn("biblia").setWidth(new Double(50));

    	this.getColumn("pdte_recibir").setHeaderCaption("");
    	this.getColumn("pdte_recibir").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("negativo").setHidden(true);
    	this.getColumn("retrabajo").setHidden(true);
    	this.getColumn("programado").setHidden(true);
    	
    	this.getColumn("tipoVino").setHeaderCaption("Vino");
    	this.getColumn("tipoVino").setWidth(new Double(120));
    	this.getColumn("botella").setHeaderCaption("Botella");
    	this.getColumn("botella").setWidth(new Double(120));
    	this.getColumn("tapon").setHeaderCaption("Tapon");
    	this.getColumn("tapon").setWidth(new Double(120));    	

    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_minimo").setHeaderCaption("Minimo");
    	this.getColumn("stock_minimo").setWidth(new Double(120));
    	this.getColumn("stock_actual").setHeaderCaption("Existencias");
    	this.getColumn("stock_actual").setWidth(new Double(120));
    	this.getColumn("pdte_servir").setHeaderCaption("Pdte. Servir");
    	this.getColumn("pdte_servir").setWidth(new Double(120));
    	this.getColumn("confirmado").setHeaderCaption("Confirmado");
    	this.getColumn("confirmado").setWidth(new Double(120));
    	this.getColumn("reservado").setHeaderCaption("Reservado");
    	this.getColumn("reservado").setWidth(new Double(120));
    	this.getColumn("prepedido").setHeaderCaption("Preparado");
    	this.getColumn("prepedido").setWidth(new Double(120));
    	this.getColumn("stock_real").setHeaderCaption("Stock Real");
    	this.getColumn("stock_real").setWidth(new Double(120));
    	this.getColumn("stock_Jaulon").setHeaderCaption("Semiterminado");
    	this.getColumn("stock_Jaulon").setWidth(new Double(120));
    	
    	this.getColumn("biblia").setHidden(true);
//    	this.getColumn("ver").setHidden(true);
    	this.getColumn("tapon").setHidden(true);
    	this.getColumn("botella").setHidden(true);
    	this.getColumn("stock_minimo").setHidden(true);
    	this.getColumn("cantidad").setHidden(true);
    	this.getColumn("factor").setHidden(true);
    	this.getColumn("observaciones").setHidden(true);
    	this.getColumn("operacion").setHidden(true);
    	this.getColumn("programadas").setHidden(true);
    	
    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
		{
    		this.getColumn("ver").setHidden(false);
    		this.getColumn("biblia").setHidden(false);
        	this.getColumn("tapon").setHidden(false);
        	this.getColumn("botella").setHidden(false);
        	this.getColumn("stock_minimo").setHidden(false);
		}
	}
	
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoSituacionEmbotellado mapeo = (MapeoSituacionEmbotellado) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("mail"))
            	{
//            		MapeoSituacionEnvasadora mapeo = (MapeoSituacionEnvasadora) event.getItemId();
//            		
//	    			boolean generado = traerPedidoSeleccionado(mapeo.getArticulo(), mapeo.getStock_actual());
//	    			
//	    			if (generado)
//	    			{
//	    				enviadoMail=generacionPdf(mapeo, true, true);
//	    				if (enviadoMail)
//	    				{
//	    					mapeo.setEnviado("S");
//	    					boolean send = actualizarPedidoEnviado(mapeo);
//	    					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar el pedido como enviado en GREENsys.");
//	    					refresh(mapeo, mapeo);
//	    				}
//	    			}
//					else
//					{
//						Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoCompra().toString() + " e insertarlo en la plataforma.");
//					}
            	}
            	else if (event.getPropertyId().toString().equals("biblia"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		String host = UI.getCurrent().getPage().getLocation().getHost().trim();
            		
            		if (host.equals("localhost")) host = "192.168.6.6";
            		
            		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + mapeo.getArticulo().trim();
            		getUI().getPage().open(url, "_blank");
            		
            	}

            	else if (event.getPropertyId().toString().equals("ver"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
//            		if (mapeo.getProgramado()!=null && mapeo.getProgramado().contentEquals("SI"))
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
//        	    		getUI().addWindow(vt);
//            		}
//            		else
//            		{
            		if (padre!=null)
            		{
            			PeticionAsignacionProgramacion vt = null;
            			vt= new PeticionAsignacionProgramacion(padre, mapeo, "Programar Cantidad de " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
            			getUI().addWindow(vt);
            		}
            		else
            		{
            			Notificaciones.getInstance().mensajeAdvertencia("No puedes programar desde aqui");
            		}
            	}

            	else if (event.getPropertyId().toString().equals("articulo"))
            	{
            		/*
            		 * acceso a ayuda produccion
            		 */
            		
            		pantallaAyudaProduccion vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getStock_real().toString(), mapeo.getStock_real(), mapeo.getArticulo());
        			getUI().addWindow(vt);
            		
            	}

            	else if (event.getPropertyId().toString().equals("stock_actual"))
            	{
            		/*
            		 * acceso a ayuda produccion
            		 */
            		
            		if (mapeo.getStock_actual()!=0)
            		{
            			pantallaStocksLotes vt = new pantallaStocksLotes("Lineas Stock Lotes del Articulo " +  mapeo.getArticulo() + " Stock total: " + mapeo.getStock_actual(),  mapeo.getArticulo(),null);
            			getUI().addWindow(vt);
            		}
            		
            	}
            	else if (event.getPropertyId().toString().equals("pdte_servir"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		
            		pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(mapeo.getArticulo(), mapeo.getStock_actual(), "Lineas Pedidos Venta del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
        			getUI().addWindow(vt);
            		
            	}
            	else if (event.getPropertyId().toString().equals("stock_real"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		
            		if (mapeo.getProgramado().equals("SI") && mapeo.getArticulo().startsWith("0102"))
            		{
            			pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(mapeo.getArticulo(), "Lineas Programadas del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " Necesidad global: " + mapeo.getStock_real(), "");
            			getUI().addWindow(vt);
            		}
            		else if (mapeo.getProgramado().equals("SI") && mapeo.getArticulo().startsWith("0111"))
            		{
            			pantallaLineasProgramaciones vtt = new pantallaLineasProgramaciones(mapeo.getArticulo(), "Lineas Programadas del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " Necesidad global: " + mapeo.getStock_real(),"Mesa");
            			getUI().addWindow(vtt);
            		}            		

            	}
            	else if (event.getPropertyId().toString().equals("stock_Jaulon"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		if (mapeo.getStock_Jaulon()!=0)
            		{
            			consultaSituacionEmbotelladoServer cses = new consultaSituacionEmbotelladoServer(CurrentUser.get());
            			String articuloJaulon = cses.recogerArticuloJaulon(mapeo.getArticulo());
            			pantallaStocksLotes vt = new pantallaStocksLotes("Lineas Stock Lotes del Articulo " + articuloJaulon + " Stock total: " + mapeo.getStock_Jaulon(), articuloJaulon,null);
            			cses=null;
            			getUI().addWindow(vt);
            		}
            		
            	}
            }
        });
    }
    
    private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    public String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            
            if (bean instanceof MapeoSituacionEmbotellado) {
            	MapeoSituacionEmbotellado progRow = (MapeoSituacionEmbotellado)bean;
                // The actual description text is depending on the application
            	/*
            	 * pendiente servir
            	 * 
            	 * 		si amarillo Retrabajos
            	 * 
            	 * stock real
            	 * 
            	 * 		rojo 	embotellar
            	 * 		morado 	programado embotellado
            	 * 		azul	programado etiquetado
            	 * 		verde	etiquetar
            	 */
            	if ("stock_real".equals(cell.getPropertyId()))
            	{
                	if (progRow.getNegativo()!=null)
                	{
	                	switch (progRow.getNegativo().trim())
	                	{
	                		case "rb":
	                		{
	                			descriptionText = "Hay que Embotellar";
	                			break;
	                		}
	                		case "vb":
	                		{
	                			descriptionText = "Se puede Etiquetar";
	                			break;
	                		}
	                		case "mb":
	                		{
	                			descriptionText = "Embotellado Programado";
	                			break;
	                		}
	                		case "mr":
	                		{
	                			descriptionText = "Embotellado Programado pero no cubre las necesidades";
	                			break;
	                		}
	                		case "mc":
	                		{
	                			descriptionText = "Embotellado Programado cubriendo la cantidad confirmada";
	                			break;
	                		}
	                		case "ab":
	                		{
	                			descriptionText = "Etiquetado Programado";
	                			break;
	                		}
	                		case "ar":
	                		{
	                			descriptionText = "Etiquetado Programado pero no cubre las necesidades";
	                			break;
	                		}
	                		case "ac":
	                		{
	                			descriptionText = "Etiquetado Programado cubriendo la cantidad confirmada";
	                			break;
	                		}
	                		case "se":
	                		{
	                			descriptionText = "Etiquetar contra stock";
	                			break;
	                		}
	                		case "sb":
	                		{
	                			descriptionText = "Embotellar contra stock";
	                			break;
	                		}

	                		default:
	                		{
	                			descriptionText = "";
	                			break;
	                		}
	                	}
                	}
                	else
                	{
                		descriptionText = "";
                	}
            	}
            	else if ("articulo".equals(cell.getPropertyId()))
            	{
            		consultaNotasArticulosServer cns = consultaNotasArticulosServer.getInstance(CurrentUser.get());
        			MapeoNotasArticulos mapeoNotas = new MapeoNotasArticulos();
        			mapeoNotas.setArticulo(cell.getValue().toString().trim());
        			if (cns.hayNotas(mapeoNotas))
    				{
        				descriptionText=cns.verNotas(cell.getValue().toString().trim());
    				}
            	}
                else if ("biblia".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ficha de Producto";
                }
                else if ("Programado".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Programación";
                }

            	else if ("pdte_servir".equals(cell.getPropertyId()))
                {
            		if (progRow.getRetrabajo()!=null)
                	{
	                	switch (progRow.getRetrabajo().trim())
	                	{
	                		case "Si":
	                		{
	                			descriptionText = "Hay retrabajos";
	                			break;
	                		}
	                		default:
	                		{
	                			descriptionText = "";
	                			break;
	                		}
	                	}
                	}
                	else
                	{
                		descriptionText = "";
                	}
                }
            	else if ("tipoVino".equals(cell.getPropertyId()))
                {
            		descriptionText = traerTotal(progRow.getTipoVino(), null, null);
                }
            	else if ("botella".equals(cell.getPropertyId()))
                {
            		descriptionText = traerTotal(progRow.getTipoVino(), cell.getValue().toString(),null);
                }
            	else if ("tapon".equals(cell.getPropertyId()))
                {
            		descriptionText = traerTotal(progRow.getTipoVino(), progRow.getBotella(), cell.getValue().toString());
                }
            }
        }
        return descriptionText;
    }

    private String traerTotal(String r_vino , String r_botella, String r_tapon)
    {
    	String resultado = null;
    	Integer cuantas = new Integer(0);
    	String vino = null;
    	String botella = null;
    	String tapon = null;
    	
    	vino = r_vino;
    	botella=r_botella;
    	tapon=r_tapon;

    	for (int i = 0; i< this.vector.size();i++)
    	{
    		MapeoSituacionEmbotellado map = (MapeoSituacionEmbotellado) this.vector.get(i);
    		
    		if (r_vino!=null && r_botella == null)
    		{
    			if (map.getTipoVino().contentEquals(vino)) cuantas = cuantas + map.getStock_real();
    		}
    		else if (r_vino!=null && r_botella != null && r_tapon == null)
    		{
    			if (map.getBotella().contentEquals(botella) && map.getTipoVino().contentEquals(vino)) cuantas = cuantas + map.getStock_real(); 
    		}
    		else if (r_vino!=null && r_botella != null && r_tapon != null)
    		{
    			if (map.getBotella().contentEquals(botella) && map.getTipoVino().contentEquals(vino) && map.getTapon().contentEquals(tapon)) cuantas = cuantas + map.getStock_real();
    		}
    	}
    	
		if (r_vino!=null && r_botella == null)
		{
			resultado = "Total " + vino + ": "; 
		}
		else if (r_vino!=null && r_botella != null && r_tapon == null)
		{
			resultado = "Total " + vino + " " + botella + ": "; 
		}
		else if (r_vino!=null && r_botella != null && r_tapon != null)
		{
			resultado = "Total " + vino + " " + botella + " " + tapon + ": ";
		}
    	resultado = resultado + RutinasNumericas.formatearIntegerDigitosIzqda(cuantas,0);
    	
    	
    	return resultado;
    	
    }
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estado = null;
    		String programado = null;
    		String retrabajar = null;
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("programado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue().toString().equals("SI"))            			
            			programado="SI";
            		else
            			programado = "NO";
            	}
            	if ("negativo".equals(cellReference.getPropertyId()))
            	{
            		estado=cellReference.getValue().toString();
            	}
            	if ("retrabajo".equals(cellReference.getPropertyId()))
            	{
            		retrabajar=cellReference.getValue().toString();
            	}
            	if ("stock_actual".equals(cellReference.getPropertyId()) || "stock_real".equals(cellReference.getPropertyId()) || "pdte_servir".equals(cellReference.getPropertyId()) || "stock_Jaulon".equals(cellReference.getPropertyId())
            			|| "confirmado".equals(cellReference.getPropertyId()) || "reservado".equals(cellReference.getPropertyId()) || "prepedido".equals(cellReference.getPropertyId()) || "stock_minimo".equals(cellReference.getPropertyId())) 
            	{
            		if ("stock_real".equals(cellReference.getPropertyId()))
            		{
            			String estilo = traerEstilo(programado, estado);
            			return estilo;
        			}
        			else if ("stock_actual".equals(cellReference.getPropertyId()) || "stock_Jaulon".equals(cellReference.getPropertyId()))
        			{
        				return "Rcell-pointer";
        			}
        			else if ("pdte_servir".equals(cellReference.getPropertyId()))
        			{
        				if (retrabajar.equals("Si"))
                			return "Rcell-warningP";
            			else
            				return "Rcell-pointer";
        			}
        			else 
        			{
        				return "Rcell-normal";
        			}
            	}
            	else if ( "biblia".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else if ("articulo".equals(cellReference.getPropertyId()))
            	{
            			if (traerNotas(cellReference))
            				return "cell-warningP";
            			else
        				return "cell-pointer";
            	}
            	else if ("mail".equals(cellReference.getPropertyId()))
            	{
            		return "nativebutton";
            	}
            	else if ( "ver".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonProgramacion";
            	}
            	else
            	{
    				return "cell-normal";
    			}
            }
        });
    }

    private boolean traerNotas(CellReference cell)
    {
    	consultaNotasArticulosServer cns = consultaNotasArticulosServer.getInstance(CurrentUser.get());
		MapeoNotasArticulos mapeoNotas = new MapeoNotasArticulos();
		mapeoNotas.setArticulo(cell.getValue().toString().trim());
		return cns.hayNotas(mapeoNotas);
    }
    
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	String destinoImpresion =  null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaSituacionEmbotelladoServer cses = consultaSituacionEmbotelladoServer.getInstance(CurrentUser.get());
    	
    	
    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") )// eBorsao.superUsuario)
    	{
    		/*
    		 * Pediremos formato de impresion para sacar datos agrupados
    		 */
			PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion((ArrayList<MapeoGlobal>) this.vector, "Necesidades");
			getUI().addWindow(vtPeticion);

    	}
    	else
    	{
    		destinoImpresion = "General";
    		pdfGenerado = cses.generarInforme((ArrayList<MapeoGlobal>) this.vector, regenerar,destinoImpresion);
    		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
    	}
		
		pdfGenerado = null;

    }
    
    private String traerEstilo(String r_programado, String r_estado)
    {
    	String estilo= "";
    	
    	switch (r_estado)
    	{
	    	case "rb":
	    	{
	    		estilo="Rcell-error";
	    		break;
	    	}
	    	case "vb":
	    	{
	    		estilo="Rcell-green";
	    		break;
	    	}
	    	case "mb":
	    	{
	    		estilo="Rcell-7b0065P";
	    		break;
	    	}
	    	case "mc":
	    	{
	    		estilo="Rcell-7b0065YP";
	    		break;
	    	}
	    	case "mr":
	    	{
	    		estilo="Rcell-7b0065RP";
	    		break;
	    	}
	    	case "ab":
	    	{
	    		estilo="Rcell-07fff8P";
	    		break;
	    	}
	    	case "ac":
	    	{
	    		estilo="Rcell-07fff8YP";
	    		break;
	    	}
	    	case "ar":
	    	{
	    		estilo="Rcell-07fff8RP";
	    		break;
	    	}
	    	case "se":
	    	{
	    		if (r_programado.contentEquals("SI")) estilo="Rcell-yellowP "; else estilo="Rcell-yellow";
	    		break;
	    	}
	    	case "sb":
	    	{
	    		if (r_programado.contentEquals("SI")) estilo="Rcell-pinkP "; else estilo="Rcell-pink";
	    		break;
	    	}
	    	default:
	    	{
	    		estilo="Rcell-normal";
	    		break;
	    	}
    	}
    	return estilo;
    }

	@Override
	public void calcularTotal() 
	{
		//"","","deposito", "reservado", "prepedido", "stock_real"
		Long totalExistencias = new Long(0) ;
		Long totalPdteServir = new Long(0) ;
		Long totalDeposito= new Long(0) ;
		Long totalReservado = new Long(0) ;
		Long totalPrepedido = new Long(0) ;
		Long totalReal = new Long(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	Integer q1Value = (Integer) item.getItemProperty("stock_actual").getValue();
        	Integer q2Value = (Integer) item.getItemProperty("pdte_servir").getValue();
        	Integer q3Value = (Integer) item.getItemProperty("confirmado").getValue();
        	Integer q4Value = (Integer) item.getItemProperty("reservado").getValue();
        	Integer q5Value = (Integer) item.getItemProperty("prepedido").getValue();
        	Integer q6Value = (Integer) item.getItemProperty("stock_real").getValue();
        	if (q1Value!=null) totalExistencias += q1Value.longValue();
			if (q2Value!=null) totalPdteServir +=q2Value.longValue();
			if (q3Value!=null) totalDeposito +=q3Value.longValue();
			if (q4Value!=null) totalReservado +=q4Value.longValue();
			if (q5Value!=null) totalPrepedido +=q5Value.longValue();
			if (q3Value!=null) totalReal +=q6Value.longValue();

        }
        
        footer.getCell("articulo").setText("Totales");
		footer.getCell("stock_actual").setText(RutinasNumericas.formatearDouble(totalExistencias.toString()));
		footer.getCell("pdte_servir").setText(RutinasNumericas.formatearDouble(totalPdteServir.toString()));
		footer.getCell("confirmado").setText(RutinasNumericas.formatearDouble(totalDeposito.toString()));
		footer.getCell("reservado").setText(RutinasNumericas.formatearDouble(totalReservado.toString()));
		footer.getCell("prepedido").setText(RutinasNumericas.formatearDouble(totalPrepedido.toString()));
		footer.getCell("stock_real").setText(RutinasNumericas.formatearDouble(totalReal.toString()));
		
		if (this.padre!= null)
		{
			this.padre.barAndGridLayout.setWidth("100%");
			this.padre.barAndGridLayout.setHeight((this.padre.getHeight()-this.padre.cabLayout.getHeight()-this.padre.lblSeparador.getHeight()-this.padre.topLayout.getHeight()-10)+"%");
		}
		if (this.padreWin!= null)
		{
			this.padreWin.barAndGridLayout.setWidth("100%");
			this.padreWin.barAndGridLayout.setHeight(this.padreWin.getHeight()-this.padreWin.cabLayout.getHeight()+"%");
		}
		
//		footer.setStyleName("smallgrid");
		footer.getCell("stock_actual").setStyleName("Rcell-pie");
		footer.getCell("stock_real").setStyleName("Rcell-pie");
		footer.getCell("pdte_servir").setStyleName("Rcell-pie");
		footer.getCell("confirmado").setStyleName("Rcell-pie");
		footer.getCell("reservado").setStyleName("Rcell-pie");
		footer.getCell("prepedido").setStyleName("Rcell-pie");

		
	}
}
