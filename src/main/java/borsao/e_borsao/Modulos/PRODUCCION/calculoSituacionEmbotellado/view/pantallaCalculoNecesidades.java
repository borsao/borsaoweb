package borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.server.consultaSituacionEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo.consultaSituacionEmbotelladoGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.	
 */
public class pantallaCalculoNecesidades extends Window 
{

	public VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	
	public Grid grid2 = null;
	private boolean hayGrid2 = false;
	
	private Double semanasPrevision = null;
	private boolean generado = false;
	private CheckBox noCeros = null;
	private CheckBox soloPendiente = null;
	private TextField semanasPrev = null;
	private HashMap<String,String> filtrosRec = null;

	/*
     * METODOS PROPIOS PERO GENERICOS
     */    
    public pantallaCalculoNecesidades(String r_titulo)
    {
    	
		this.setCaption(r_titulo);
		this.center();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);
    }

    private void cargarPantalla()
    {

    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);

    	this.noCeros = new CheckBox("Ver solo los distintos de 0? ");
    	this.noCeros.setValue(false);
    	this.soloPendiente = new CheckBox("Ver solo pendiente? ");
    	this.soloPendiente.setValue(false);
    	this.semanasPrev = new TextField("Semanas Prevision");
    	this.semanasPrev .setStyleName(ValoTheme.TEXTFIELD_TINY);
    	this.semanasPrev.setValue("0");

    	if (eBorsao.get().accessControl.getNombre().contains("Claveria") && eBorsao.superUsuario)
    	{
    		this.cabLayout.addComponent(this.noCeros);
    		this.cabLayout.setComponentAlignment(this.noCeros, Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.soloPendiente);
    		this.cabLayout.setComponentAlignment(this.soloPendiente, Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.semanasPrev);
    		this.cabLayout.setComponentAlignment(this.semanasPrev, Alignment.BOTTOM_LEFT);
    	}

		this.barAndGridLayout.addComponent(this.cabLayout);

		this.generarGrid();

    }
    public void generarGrid()
    {
    	ArrayList<MapeoSituacionEmbotellado> r_vector=null;
    	consultaSituacionEmbotelladoServer cse = new consultaSituacionEmbotelladoServer(CurrentUser.get());
    	
    	if (this.semanasPrev.getValue()!=null && this.semanasPrev.getValue().length()>0)
    	{
    		if (this.semanasPrev.getValue()!="0")
    			semanasPrevision = new Double(this.semanasPrev.getValue())/4;
    		else
    			semanasPrevision = new Double(0);
    	}
    	r_vector=cse.datosOpcionesGlobal(this.soloPendiente.getValue(), this.noCeros.getValue(),semanasPrevision);
    	
    	grid2 = new OpcionesGrid(r_vector,this);
    	if (((OpcionesGrid) grid2).vector==null && ((OpcionesGrid) grid2).vector.isEmpty())
    	{
    		setHayGrid2(false);
    	}
    	else
    	{
    		generado=true;
    		setHayGrid2(true);
    		((OpcionesGrid) grid2).establecerFiltros(filtrosRec);
    	}

    	if (isHayGrid2())
		{
			barAndGridLayout.addComponent(grid2);
    		barAndGridLayout.setExpandRatio(grid2,3 );
    		barAndGridLayout.setMargin(true);
		}
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void cargarListeners()
    {

    	this.noCeros.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid2())
				{
					filtrosRec = ((OpcionesGrid) grid2).recogerFiltros();
					grid2.removeAllColumns();			
					barAndGridLayout.removeComponent(grid2);
					grid2=null;	
					setHayGrid2(false);
				}
				generarGrid();
			}
		});
    	this.soloPendiente.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid2())
				{
					filtrosRec = ((OpcionesGrid) grid2).recogerFiltros();
					grid2.removeAllColumns();			
					barAndGridLayout.removeComponent(grid2);
					grid2=null;	
					setHayGrid2(false);
				}
				generarGrid();
			}
		});
			
    }
    

    public void print() 
    {
    }
    
	public void destructor()
	{
	}
	
    public boolean isHayGrid2() {
    	return hayGrid2;
    }
    
    public void setHayGrid2(boolean hayGrid2) {
    	this.hayGrid2 = hayGrid2;
    }
    
}
