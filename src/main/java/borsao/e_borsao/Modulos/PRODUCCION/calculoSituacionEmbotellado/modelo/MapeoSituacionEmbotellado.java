package borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoSituacionEmbotellado extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String tipoVino;
	private String botella;
	private String tapon;
	private String operacion;
	private String programado;
	private Integer cantidad;
	private Integer factor;
	
	private Integer stock_actual;
	private Integer programadas;
	private Integer stock_minimo;
	private Integer pdte_servir;
	private Integer pdte_recibir;
	private Integer deposito;
	private Integer prepedido;
	private Integer reservado;
	private Integer confirmado;
	private Integer stock_Jaulon;
	private Integer stock_real;
	
	private String observaciones = " ";
	
	private String ver = " ";
	private String biblia = " ";
	private String mail = " ";
	private String negativo = " ";
	private String retrabajo = " ";

	public MapeoSituacionEmbotellado()
	{
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getStock_actual() {		
		return stock_actual;
	}


	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}


	public Integer getPdte_servir() {
		return pdte_servir;
	}


	public void setPdte_servir(Integer pdte_servir) {
		this.pdte_servir = pdte_servir;
	}


	public Integer getStock_real() {
		return stock_real;
	}


	public void setStock_real(Integer stock_real) {
		this.stock_real = stock_real;
	}


	public Integer getPdte_recibir() {
		return pdte_recibir;
	}


	public void setPdte_recibir(Integer pdte_recibir) {
		this.pdte_recibir = pdte_recibir;
	}


	public String getVer() {
		return ver;
	}


	public void setVer(String ver) {
		this.ver = ver;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getNegativo() {
		return negativo;
	}

	public void setNegativo(String negativo) {
		this.negativo = negativo;
	}

	public Integer getStock_Jaulon() {
		return stock_Jaulon;
	}


	public void setStock_Jaulon(Integer stock_Jaulon) {
		this.stock_Jaulon = stock_Jaulon;
	}


	public String getTipoVino() {
		return tipoVino;
	}


	public void setTipoVino(String tipoVino) {
		this.tipoVino = tipoVino;
	}


	public Integer getDeposito() {
		return deposito;
	}


	public void setDeposito(Integer deposito) {
		this.deposito = deposito;
	}


	public Integer getPrepedido() {
		return prepedido;
	}


	public void setPrepedido(Integer prepedido) {
		this.prepedido = prepedido;
	}


	public Integer getReservado() {
		return reservado;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public void setReservado(Integer reservado) {
		this.reservado = reservado;
	}


	public String getProgramado() {
		return programado;
	}


	public void setProgramado(String programado) {
		this.programado = programado;
	}


	public String getRetrabajo() {
		return retrabajo;
	}


	public void setRetrabajo(String retrabajo) {
		this.retrabajo = retrabajo;
	}


	public Integer getConfirmado() {
		return confirmado;
	}


	public void setConfirmado(Integer confirmado) {
		this.confirmado = confirmado;
	}


	public String getBotella() {
		return botella;
	}


	public void setBotella(String botella) {
		this.botella = botella;
	}


	public String getTapon() {
		return tapon;
	}


	public void setTapon(String tapon) {
		this.tapon = tapon;
	}


	public Integer getStock_minimo() {
		return stock_minimo;
	}


	public void setStock_minimo(Integer stock_minimo) {
		this.stock_minimo = stock_minimo;
	}


	public String getBiblia() {
		return biblia;
	}


	public void setBiblia(String biblia) {
		this.biblia = biblia;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public Integer getFactor() {
		return factor;
	}


	public void setFactor(Integer factor) {
		this.factor = factor;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public Integer getProgramadas() {
		return programadas;
	}


	public void setProgramadas(Integer programadas) {
		this.programadas = programadas;
	}


}