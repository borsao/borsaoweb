package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEquiposSemana extends MapeoGlobal
{
    private Integer equipo;
    private String ejercicio;
    private String semana;
    private String area;
	private String jefe;
	private String operario1;
	private String operario2;
	private String operario3;
	private String operario4;
	private String turno;

	public MapeoEquiposSemana()
	{
		this.setEjercicio("");
		this.setJefe("");
		this.setSemana("");
		this.setArea("");
		this.setOperario1("");
		this.setOperario2("");
		this.setOperario3("");
		this.setOperario4("");
		this.setTurno("");
	}

	public Integer getEquipo() {
		return equipo;
	}

	public void setEquipo(Integer idEquipo) {
		this.equipo = idEquipo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getJefe() {
		return jefe;
	}

	public void setJefe(String jefe) {
		this.jefe = jefe;
	}

	public String getOperario1() {
		return operario1;
	}

	public void setOperario1(String operario1) {
		this.operario1 = operario1;
	}

	public String getOperario2() {
		return operario2;
	}

	public void setOperario2(String operario2) {
		this.operario2 = operario2;
	}

	public String getOperario3() {
		return operario3;
	}

	public void setOperario3(String operario3) {
		this.operario3 = operario3;
	}

	public String getOperario4() {
		return operario4;
	}

	public void setOperario4(String operario4) {
		this.operario4 = operario4;
	}

	public String getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	
}