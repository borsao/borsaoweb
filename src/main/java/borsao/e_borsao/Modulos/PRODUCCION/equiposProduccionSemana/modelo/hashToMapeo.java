package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoEquiposSemana mapeo = null;
	 
	 public MapeoEquiposSemana convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquiposSemana();
		 this.mapeo.setSemana(r_hash.get("semana"));
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setJefe(r_hash.get("jefe"));
		 this.mapeo.setOperario1(r_hash.get("botellas"));		 
		 this.mapeo.setOperario2(r_hash.get("cajas"));
		 this.mapeo.setOperario3(r_hash.get("toro"));
		 this.mapeo.setTurno(r_hash.get("turno"));
		 if (r_hash.get("equipo")!=null)
		 {
			 this.mapeo.setEquipo(new Integer(r_hash.get("equipo")));
		 }
		 if (r_hash.get("ejercicio")!=null)
		 {
			 this.mapeo.setEjercicio(r_hash.get("ejercicio"));
		 }
		 return mapeo;		 
	 }
	 
	 public MapeoEquiposSemana convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquiposSemana();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setSemana(r_hash.get("semana"));
		 this.mapeo.setJefe(r_hash.get("jefe"));
		 this.mapeo.setOperario1(r_hash.get("botellas"));		 
		 this.mapeo.setOperario2(r_hash.get("cajas"));
		 this.mapeo.setOperario3(r_hash.get("toro"));
		 this.mapeo.setTurno(r_hash.get("turno"));
		 if (r_hash.get("equipo")!=null)
		 {
			 this.mapeo.setEquipo(new Integer(r_hash.get("equipo")));
		 }
		 if (r_hash.get("ejercicio")!=null)
		 {
			 this.mapeo.setEjercicio(r_hash.get("ejercicio"));
		 }
		 return mapeo;			 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoEquiposSemana r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("semana", r_mapeo.getSemana());
		 this.hash.put("jefe", r_mapeo.getJefe());
		 this.hash.put("botellas", r_mapeo.getOperario1());
		 this.hash.put("cajas", r_mapeo.getOperario2());
		 this.hash.put("toro", r_mapeo.getOperario3());
		 this.hash.put("turno", r_mapeo.getTurno());
		 if (r_mapeo.getEquipo()!=null)
		 {
			 this.hash.put("equipo", r_mapeo.getEquipo().toString());
		 }
		 if (r_mapeo.getEjercicio()!=null)
		 {
			 this.hash.put("ejercicio", r_mapeo.getEjercicio().toString());
		 }
		 return hash;		 
	 }
}