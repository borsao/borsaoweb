package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.server.consultaEquiposProduccionSemanaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EquiposSemanaGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	
    public EquiposSemanaGrid(ArrayList<MapeoEquiposSemana> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Equipos Producción Semana");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoEquiposSemana.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "semana", "equipo", "area", "jefe","operario1","operario2","operario3","operario4");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "100");
    	this.widthFiltros.put("semana", "100");
    	this.widthFiltros.put("turno", "100");
    	this.widthFiltros.put("area", "200");
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("semana").setHeaderCaption("Semana");
    	this.getColumn("equipo").setHeaderCaption("Equipo");
    	this.getColumn("area").setHeaderCaption("Area Producción");    	
    	this.getColumn("jefe").setHeaderCaption("Jefe Turno");
    	this.getColumn("operario1").setHeaderCaption("Botellas/Garrafas");
    	this.getColumn("operario2").setHeaderCaption("Cajas");
    	this.getColumn("operario3").setHeaderCaption("Carretillero");
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("operario4").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("jefe");
		this.camposNoFiltrar.add("operario1");
		this.camposNoFiltrar.add("operario2");
		this.camposNoFiltrar.add("operario3");
		this.camposNoFiltrar.add("operario4");
		this.camposNoFiltrar.add("equipo");
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	 public void generacionPdf(boolean regenerar) 
	    {
	    	String pdfGenerado = null;
	    	
	    	/*
	    	 * generacion del pdf
	    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
	    	 */
	    	consultaEquiposProduccionSemanaServer ces = consultaEquiposProduccionSemanaServer.getInstance(CurrentUser.get());
	    	pdfGenerado = ces.generarInforme((ArrayList<MapeoEquiposSemana>) this.vector);
	    	
	    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
			
			pdfGenerado = null;

	    }
	@Override
	public void calcularTotal() {
		
	}
}
