package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo.MapeoTurnos;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.server.consultaTurnosServer;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo.MapeoEquipos;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.server.consultaEquiposProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.EquiposSemanaGrid;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.MapeoEquiposSemana;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.server.consultaEquiposProduccionSemanaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoEquiposSemana mapeoEquiposSemana = null;
	 
	 private ventanaAyuda vHelp =null;
	 private consultaEquiposProduccionSemanaServer cus = null;	 
	 private EquiposProduccionSemanaView uw = null;
	 private BeanFieldGroup<MapeoEquiposSemana> fieldGroup;
	 
	 public OpcionesForm(EquiposProduccionSemanaView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        
        
        fieldGroup = new BeanFieldGroup<MapeoEquiposSemana>(MapeoEquiposSemana.class);
        fieldGroup.bindMemberFields(this);

        this.cargarCombos(this.area,"Area");
        this.cargarCombos(this.semana,"Semana");
        
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoEquiposSemana = (MapeoEquiposSemana) uw.grid.getSelectedRow();
                eliminarEquipo(mapeoEquiposSemana);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoEquiposSemana = hm.convertirHashAMapeo(uw.opcionesEscogidas);
//	 				mapeoEquiposSemana.setTurno(recuperarIdentificadorTurno(mapeoEquiposSemana.getTurno()));
	                crearEquipo(mapeoEquiposSemana);
	 			}
	 			else
	 			{
	 				MapeoEquiposSemana mapeoEquipos_orig = (MapeoEquiposSemana) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoEquiposSemana = hm.convertirHashAMapeo(uw.opcionesEscogidas);
//	 				mapeoEquiposSemana.setTurno(recuperarIdentificadorTurno(mapeoEquiposSemana.getTurno()));
	 				modificarEquipo(mapeoEquiposSemana,mapeoEquipos_orig);
	 				hm=null;
	 			}
	 			if (((EquiposSemanaGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
		this.btnEquipos.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaAyudaEquipos(area.getValue().toString());
            }
        });
		
		ValueChangeListener valueTeam = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (equipo.getValue()!=null && equipo.getValue().length()>0) recuperarDatosEquipo(area.getValue().toString(), equipo.getValue());
			}
		};
		this.equipo.addValueChangeListener(valueTeam);
		
		ValueChangeListener valueLis = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				cargarCombos(jefe,area.getValue().toString());
		        cargarCombos(operario1,area.getValue().toString());
		        cargarCombos(operario2,area.getValue().toString());
		        cargarCombos(operario3,area.getValue().toString());
		        cargarCombos(turno,"Turno");
		        btnEquipos.setEnabled(true);
			}
		};
		this.area.addValueChangeListener(valueLis);

	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		
		switch (r_nombre)
		{
			case "Turno":
			{
				r_combo.removeAllItems();
				consultaTurnosServer cts = consultaTurnosServer.getInstance(CurrentUser.get());
				ArrayList<MapeoTurnos> vectorTurnos = cts.datosOpcionesGlobal(null);
				for (int i = 0; i< vectorTurnos.size();i++)
				{
					r_combo.addItem(((MapeoTurnos) vectorTurnos.get(i)).getDescripcion());
				}
				break;
			}
			case "Area":
			{
				r_combo.addItem("General");
				r_combo.addItem("Almacen");
				r_combo.addItem("Embotelladora");
				r_combo.addItem("Envasadora");
				break;
			}
			case "Semana":
			{
				if (this.ejercicio.getValue()==null || this.ejercicio.getValue().length()==0)
				{
					this.ejercicio.setValue(RutinasFechas.añoActualYYYY());
				}
				else
				{
					this.semana.removeAllItems();
				}

				if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().length()>0) 
				{	
					int semanas = RutinasFechas.semanasAño(this.ejercicio.getValue());
					
					for (int i=1; i<=semanas; i++)
					{
						this.semana.addItem(String.valueOf(i));
					}
					this.semana.setValue(RutinasFechas.semanaActual(this.ejercicio.getValue()));
				}
				break;
			}
			case "Embotelladora":
			{
				if (r_nombre==null || r_nombre.length()==0) break;
				r_combo.removeAllItems();
				consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
				MapeoOperarios mapeoOp = new MapeoOperarios();
//				mapeoOp.setArea(r_nombre);
				ArrayList<MapeoOperarios> oper = cos.datosOperariosGlobal(mapeoOp);
				for (int i = 0; i< oper.size();i++)
				{
					MapeoOperarios map = oper.get(i);
					r_combo.addItem(map.getNombre());
				}
				cos=null;
				break;
			}
		}		
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;		
		editarEquipo(null);
//		if (creacion) cargarNumerador();
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		opcionesEscogidas.put("ejercicio", this.ejercicio.getValue());
		opcionesEscogidas.put("equipo", this.equipo.getValue());
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.jefe.getValue()!=null && this.jefe.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("jefe", this.jefe.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("jefe", "");
		}
		if (this.operario1.getValue()!=null && this.operario1.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("botellas", this.operario1.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("botellas", "");
		}
		if (this.operario2.getValue()!=null && this.operario2.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cajas", this.operario2.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cajas", "");
		}
		if (this.operario3.getValue()!=null && this.operario3.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("toro", this.operario3.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("toro", "");
		}
		if (this.turno.getValue()!=null && this.turno.getValue().toString().length()>0) 
		{
			String turno = this.recuperarIdentificadorTurno(this.turno.getValue().toString());
			opcionesEscogidas.put("turno", turno);
		}
		else
		{
			opcionesEscogidas.put("turno", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarEquipo(null);
	}
	
	public void editarEquipo(MapeoEquiposSemana r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoEquiposSemana();
			r_mapeo.setEjercicio(this.ejercicio.getValue());
			r_mapeo.setSemana(String.valueOf(RutinasFechas.semanaActual(this.ejercicio.getValue())));
		}
		if (r_mapeo.getTurno()!=null && r_mapeo.getTurno().length()==1) r_mapeo.setTurno(this.recuperarDescripcionTurno(r_mapeo.getTurno()));
		fieldGroup.setItemDataSource(new BeanItem<MapeoEquiposSemana>(r_mapeo));
		ejercicio.focus();
//		if (r_mapeo!=null) this.area.setValue(r_mapeo.getArea());
//		if (r_mapeo!=null) this.semana.setValue(r_mapeo.getSemana());
	}
	
	public void eliminarEquipo(MapeoEquiposSemana r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaEquiposProduccionSemanaServer(CurrentUser.get());
		((EquiposSemanaGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarEquipo(MapeoEquiposSemana r_mapeo, MapeoEquiposSemana r_mapeo_orig)
	{
		cus  = new consultaEquiposProduccionSemanaServer(CurrentUser.get());
	    if (r_mapeo.getEquipo()!=null) r_mapeo.setEquipo(new Integer(r_mapeo_orig.getEquipo()));
		cus.guardarCambios(r_mapeo);		
		((EquiposSemanaGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearEquipo(MapeoEquiposSemana r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaEquiposProduccionSemanaServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoEquiposSemana>(r_mapeo));
			if (((EquiposSemanaGrid) uw.grid)!=null)
			{
				((EquiposSemanaGrid) uw.grid).refresh(r_mapeo,null);
			}
			else
			{
				uw.generarGrid(uw.opcionesEscogidas);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoEquiposSemana> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoEquiposSemana= item.getBean();
            if (this.mapeoEquiposSemana.getEquipo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    
    private void recuperarDatosEquipo(String r_area, String r_equipo)
    {
		MapeoEquipos mapeoEquipos= null;
		
		MapeoEquipos map = new MapeoEquipos();
		consultaEquiposProduccionServer cps  = new consultaEquiposProduccionServer(CurrentUser.get());
		
		map.setArea(r_area);
		map.setEquipo(new Integer(r_equipo));
		mapeoEquipos =cps.datosEquiposProduccion(map);
		
		if (mapeoEquipos!=null)
		{
			this.operario1.setValue(mapeoEquipos.getOperario1());
			this.operario2.setValue(mapeoEquipos.getOperario2());
			this.operario3.setValue(mapeoEquipos.getOperario3());
		}
    	
    }
    
    private void ventanaAyudaEquipos(String r_area)
    {
		ArrayList<MapeoAyudas> vectorEquipos= null;
		consultaEquiposProduccionServer cps  = new consultaEquiposProduccionServer(CurrentUser.get());
		vectorEquipos =cps.ayuda(r_area);
		this.vHelp = new ventanaAyuda(this.equipo, this.jefe, vectorEquipos, "Equipos");
		getUI().addWindow(this.vHelp);	

    }
    
    private String recuperarIdentificadorTurno(String r_descripcion)
    {
		consultaTurnosServer cts = consultaTurnosServer.getInstance(CurrentUser.get());
		MapeoTurnos map = new MapeoTurnos();
		map.setDescripcion(r_descripcion);
		
		ArrayList<MapeoTurnos> vectorTurnos = cts.datosOpcionesGlobal(map);
		
		if (vectorTurnos!=null && vectorTurnos.size()>0)
		{
			return ((MapeoTurnos) vectorTurnos.get(0)).getTurno();
		}
		else
		{
			return null;
		}    	
    }
    
    private String recuperarDescripcionTurno(String r_descripcion)
    {
		consultaTurnosServer cts = consultaTurnosServer.getInstance(CurrentUser.get());
		MapeoTurnos map = new MapeoTurnos();
		map.setTurno(r_descripcion);
		ArrayList<MapeoTurnos> vectorTurnos = cts.datosOpcionesGlobal(map);
		
		if (vectorTurnos!=null && vectorTurnos.size()>0)
		{
			return ((MapeoTurnos) vectorTurnos.get(0)).getDescripcion();
		}
		else
		{
			return null;
		}
    	
    }
}
