package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.EquiposSemanaGrid;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.MapeoEquiposSemana;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.server.consultaEquiposProduccionSemanaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class EquiposProduccionSemanaView extends GridViewRefresh {

	public static final String VIEW_NAME = "Turnos Produccion";
	private final String titulo = "EQUIPOS SEMANALES";
	private final int intervaloRefresco = 500;
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private boolean generado = false;
	
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoEquiposSemana> r_vector=null;
    	MapeoEquiposSemana mapeoEquiposSemana =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoEquiposSemana=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaEquiposProduccionSemanaServer cus = new consultaEquiposProduccionSemanaServer(CurrentUser.get());
    	r_vector=cus.datosEquiposProduccionSemanaGlobal(mapeoEquiposSemana);
    	
    	
    	
    	if (r_vector!=null && r_vector.size()>0)
    	{
    		grid = new EquiposSemanaGrid(r_vector);
			setHayGrid(true);
			this.opcImprimir.setVisible(true);
			generado=true;
    	}
    	else
    	{
    		setHayGrid(false);
    		this.opcImprimir.setVisible(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    	
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public EquiposProduccionSemanaView() 
    {
    	opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);
    }

    public void newForm()
    {   
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarEquipo((MapeoEquiposSemana) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	if (isHayGrid()) this.opcImprimir.setEnabled(true);
    }

    public void print() {
    	((EquiposSemanaGrid) this.grid).generacionPdf(generado);
    	this.regresarDesdeForm();
    	generado=false;

    }

    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }

    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
    
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
