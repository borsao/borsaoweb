package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo.MapeoTurnos;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.server.consultaTurnosServer;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccionSemana.modelo.MapeoEquiposSemana;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEquiposProduccionSemanaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEquiposProduccionSemanaServer instance;
	
	public consultaEquiposProduccionSemanaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEquiposProduccionSemanaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEquiposProduccionSemanaServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoEquiposSemana> datosEquiposProduccionSemanaGlobal(MapeoEquiposSemana r_mapeo)
	{
		ResultSet rsEquiposProduccionSemana = null;		
		ArrayList<MapeoEquiposSemana> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_equipos_semana.idprd_equipos_semana equ_id, ");
		cadenaSQL.append(" prd_equipos_semana.prd_ejercicio equ_eje, ");
        cadenaSQL.append(" prd_equipos_semana.prd_semana equ_sem, ");
        cadenaSQL.append(" prd_equipos_semana.prd_area equ_are, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_jefe_turno equ_jef, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_operario1 equ_op1, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_operario2 equ_op2, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_operario3 equ_op3, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_operario4 equ_op4, ");
	    cadenaSQL.append(" prd_equipos_semana.prd_turno equ_tur ");
     	cadenaSQL.append(" FROM prd_equipos_semana ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();

				if (r_mapeo.getEquipo()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_equipos_semana = " + r_mapeo.getEquipo() + " ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ejercicio = '" + new Integer(r_mapeo.getEjercicio()) + "' ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_semana = '" + r_mapeo.getSemana() + "' ");
				}				
				
				if (r_mapeo.getJefe()!=null && r_mapeo.getJefe().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_jefe_turno = '" + r_mapeo.getJefe() + "' ");
				}				
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOperario1()!=null && r_mapeo.getOperario1().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario1 = '" + r_mapeo.getOperario1() + "' ");
				}
				if (r_mapeo.getOperario2()!=null && r_mapeo.getOperario2().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario2 = '" + r_mapeo.getOperario2() + "' ");
				}
				if (r_mapeo.getOperario3()!=null && r_mapeo.getOperario3().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario3 = '" + r_mapeo.getOperario3() + "' ");
				}
				if (r_mapeo.getOperario4()!=null && r_mapeo.getOperario4().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario4 = '" + r_mapeo.getOperario4() + "' ");
				}
				if (r_mapeo.getTurno()!=null && r_mapeo.getTurno().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_turno = '" + r_mapeo.getTurno() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_equipos_semana asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquiposProduccionSemana.TYPE_SCROLL_SENSITIVE,rsEquiposProduccionSemana.CONCUR_UPDATABLE);
			rsEquiposProduccionSemana= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoEquiposSemana>();
			
			while(rsEquiposProduccionSemana.next())
			{
				MapeoEquiposSemana mapeoEquiposSemana = new MapeoEquiposSemana();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquiposSemana.setEquipo(rsEquiposProduccionSemana.getInt("equ_id"));
				mapeoEquiposSemana.setEjercicio(rsEquiposProduccionSemana.getString("equ_eje"));
				mapeoEquiposSemana.setSemana(rsEquiposProduccionSemana.getString("equ_sem"));
				mapeoEquiposSemana.setJefe(rsEquiposProduccionSemana.getString("equ_jef"));
				mapeoEquiposSemana.setArea(rsEquiposProduccionSemana.getString("equ_are"));
				mapeoEquiposSemana.setOperario1(rsEquiposProduccionSemana.getString("equ_op1"));
				mapeoEquiposSemana.setOperario2(rsEquiposProduccionSemana.getString("equ_op2"));
				mapeoEquiposSemana.setOperario3(rsEquiposProduccionSemana.getString("equ_op3"));
				mapeoEquiposSemana.setOperario4(rsEquiposProduccionSemana.getString("equ_op4"));
				String desc = this.recuperarDescripcionTurno(rsEquiposProduccionSemana.getString("equ_tur"));
				mapeoEquiposSemana.setTurno(desc);
				vector.add(mapeoEquiposSemana);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsEquiposProduccion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_equipos_semana.idprd_equipos_semana) equ_sig ");
     	cadenaSQL.append(" FROM prd_equipos_semana ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquiposProduccion.TYPE_SCROLL_SENSITIVE,rsEquiposProduccion.CONCUR_UPDATABLE);
			rsEquiposProduccion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquiposProduccion.next())
			{
				return rsEquiposProduccion.getInt("equ_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoEquiposSemana r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_equipos_semana ( ");
    		cadenaSQL.append(" prd_equipos_semana.idprd_equipos_semana, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_ejercicio, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_Semana, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_area, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_jefe_turno, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario1, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario2, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario3, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario4, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_turno) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getEquipo());

		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2,  new Integer(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getJefe()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getJefe());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getOperario1()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getOperario1());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getOperario2()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getOperario2());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getOperario3()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getOperario3());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getOperario4()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getOperario4());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getTurno());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarNuevoTemporal(MapeoEquiposSemana r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO tmp_equipos_semana ( ");
    		cadenaSQL.append(" tmp_equipos_semana.idprd_equipos_semana, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_ejercicio, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_Semana, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_area, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_jefe_turno, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_operario1, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_operario2, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_operario3, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_operario4, ");
    		cadenaSQL.append(" tmp_equipos_semana.prd_turno) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, r_mapeo.getEquipo());

		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2,  new Integer(r_mapeo.getEjercicio()));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getJefe()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getJefe());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getOperario1()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getOperario1());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getOperario2()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getOperario2());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getOperario3()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getOperario3());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getOperario4()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getOperario4());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	if (r_mapeo.getTurno().equals("M")) preparedStatement.setString(10, "1");
		    	if (r_mapeo.getTurno().equals("T")) preparedStatement.setString(10, "2");
		    	if (r_mapeo.getTurno().equals("N")) preparedStatement.setString(10, "3");
		    }
		    else
		    {
		    	preparedStatement.setString(10, "");
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoEquiposSemana r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_equipos_semana set ");
			cadenaSQL.append(" prd_equipos_semana.prd_ejercicio  = ?, ");
			cadenaSQL.append(" prd_equipos_semana.prd_semana = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_area  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_jefe_turno  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario1  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario2  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario3  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_operario4  = ?, ");
    		cadenaSQL.append(" prd_equipos_semana.prd_turno  = ? ");
		    cadenaSQL.append(" WHERE prd_equipos_semana.idprd_equipos_semana = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    if (r_mapeo.getEjercicio()!=null)
		    	preparedStatement.setInt(1, new Integer(r_mapeo.getEjercicio()));
		    else
		    	preparedStatement.setInt(1, 0);
		    preparedStatement.setString(2, r_mapeo.getSemana());
		    preparedStatement.setString(3, r_mapeo.getArea());
		    preparedStatement.setString(4, r_mapeo.getJefe());
		    preparedStatement.setString(5, r_mapeo.getOperario1());
		    preparedStatement.setString(6, r_mapeo.getOperario2());
		    preparedStatement.setString(7, r_mapeo.getOperario3());
		    preparedStatement.setString(8, r_mapeo.getOperario4());
		    preparedStatement.setString(9, r_mapeo.getTurno());
		    if (r_mapeo.getEquipo()!=null)
		    	preparedStatement.setInt(10, new Integer(r_mapeo.getEquipo()));
		    else
		    	preparedStatement.setInt(10, 0);
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoEquiposSemana r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_equipos_semana ");            
			cadenaSQL.append(" WHERE prd_equipos_semana.idprd_equipos_semana = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    if (r_mapeo.getEquipo()!=null)
		    	preparedStatement.setInt(1, new Integer(r_mapeo.getEquipo()));
		    else
		    	preparedStatement.setInt(1, 0);

			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public void eliminarTemporal()
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM tmp_equipos_semana ");            
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	private String recuperarDescripcionTurno(String r_turno)
    {
		consultaTurnosServer cts = consultaTurnosServer.getInstance(CurrentUser.get());
		MapeoTurnos map = new MapeoTurnos();
		map.setTurno(r_turno);
		
		ArrayList<MapeoTurnos> vectorTurnos = cts.datosOpcionesGlobal(map);
		
		for (int i = 0; i< vectorTurnos.size();i++)
		{
			return ((MapeoTurnos) vectorTurnos.get(i)).getDescripcion();
		}
		return null;
    	
    }
	
	public String generarInforme(ArrayList<MapeoEquiposSemana> r_vector)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		ArrayList<Integer> codigos = null;
		
		codigos=new ArrayList<Integer>();
		this.eliminarTemporal();
		
		for (int i=0; i< r_vector.size();i++)
		{
			MapeoEquiposSemana mapeo = (MapeoEquiposSemana) r_vector.get(i);
			if (mapeo.getTurno().length()>1) mapeo.setTurno(this.recuperarIdentificadorTurno(mapeo.getTurno()));
			this.guardarNuevoTemporal(mapeo);
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/turnosSemana" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("turnosSemana.jrxml");
    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado("turnosSemana.jasper");
    	libImpresion.setCarpeta("produccion");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
    private String recuperarIdentificadorTurno(String r_descripcion)
    {
		consultaTurnosServer cts = consultaTurnosServer.getInstance(CurrentUser.get());
		MapeoTurnos map = new MapeoTurnos();
		map.setDescripcion(r_descripcion);
		
		ArrayList<MapeoTurnos> vectorTurnos = cts.datosOpcionesGlobal(map);
		
		for (int i = 0; i< vectorTurnos.size();i++)
		{
			return ((MapeoTurnos) vectorTurnos.get(i)).getTurno();
		}
		return null;
    	
    }

}