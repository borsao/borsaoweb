package borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.MapeoTareasEscandallo;

public class consultaTareasEscandalloServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTareasEscandalloServer instance;
	
	public consultaTareasEscandalloServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTareasEscandalloServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTareasEscandalloServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoTareasEscandallo> datosTareasEscandalloGlobal(MapeoTareasEscandallo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoTareasEscandallo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoTareasEscandallo mapeoTareasEscandallo=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_tareas_escandallo.idCodigo tar_id,");
		cadenaSQL.append(" prd_tareas_escandallo.tarea tar_tar,");
		cadenaSQL.append(" prd_tareas_escandallo.orden tar_ord ");
		cadenaSQL.append(" from prd_tareas_escandallo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getTarea()!=null && r_mapeo.getTarea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tarea = '" + r_mapeo.getTarea() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoTareasEscandallo>();
			
			while(rsOpcion.next())
			{
				mapeoTareasEscandallo = new MapeoTareasEscandallo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTareasEscandallo.setIdCodigo(rsOpcion.getInt("tar_id"));
				mapeoTareasEscandallo.setOrden(rsOpcion.getInt("tar_ord"));
				mapeoTareasEscandallo.setTarea(rsOpcion.getString("tar_tar"));
				vector.add(mapeoTareasEscandallo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public boolean comprobarTareasEscandallo(String r_tarea)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT prd_tareas_escandallo.orden tar_ord ");
		cadenaSQL.append(" from prd_tareas_escandallo ");
		cadenaSQL.append(" where tarea = '" + r_tarea + "' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String imprimirTarea(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_escandallo.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_tareas_escandallo ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public Integer obtenerSiguienteOrden()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_escandallo.orden) tar_ord ");
     	cadenaSQL.append(" FROM prd_tareas_escandallo ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_ord") + 10 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}


	public String guardarNuevo(MapeoTareasEscandallo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_tareas_escandallo( ");
			cadenaSQL.append(" prd_tareas_escandallo.idCodigo,");			
			cadenaSQL.append(" prd_tareas_escandallo.tarea, ");			
			cadenaSQL.append(" prd_tareas_escandallo.orden ) VALUES (");
			cadenaSQL.append(" ?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getTarea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTarea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoTareasEscandallo r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_tareas_escandallo set ");
		    cadenaSQL.append(" prd_tareas_escandallo.tarea =?,");
		    cadenaSQL.append(" prd_tareas_escandallo.orden =? ");
			cadenaSQL.append(" where prd_tareas_escandallo.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getTarea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getTarea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoTareasEscandallo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_tareas_escandallo ");            
			cadenaSQL.append(" WHERE prd_tareas_escandallo.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoTareasEscandallo r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/TareasEscandallo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("TareasEscandallo.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("TareasEscandallo");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}