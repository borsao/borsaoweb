package borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoTareasEscandallo mapeo = null;
	 
	 public MapeoTareasEscandallo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoTareasEscandallo();

		 this.mapeo.setTarea(r_hash.get("tarea"));
		 
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden"))));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoTareasEscandallo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoTareasEscandallo();

		 this.mapeo.setTarea(r_hash.get("tarea"));
		 
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden"))));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoTareasEscandallo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("tarea", this.mapeo.getTarea());
		 
		 this.hash.put("orden", String.valueOf(this.mapeo.getOrden()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}