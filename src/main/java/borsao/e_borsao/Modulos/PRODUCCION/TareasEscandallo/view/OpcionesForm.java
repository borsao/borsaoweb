package borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.MapeoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.TareasEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.server.consultaTareasEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoTareasEscandallo mapeo = null;
	 
	 private consultaTareasEscandalloServer cps = null;
	 private TareasEscandalloView uw = null;
	 public MapeoTareasEscandallo mapeoModificado = null;
	 private BeanFieldGroup<MapeoTareasEscandallo> fieldGroup;
	 
	 public OpcionesForm(TareasEscandalloView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoTareasEscandallo>(MapeoTareasEscandallo.class);
        fieldGroup.bindMemberFields(this);
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoTareasEscandallo) uw.grid.getSelectedRow();
                eliminarTarea(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;	
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearTarea(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoTareasEscandallo mapeo_orig= (MapeoTareasEscandallo) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				
		 				modificarTarea(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((TareasEscandalloGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarTarea(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.orden.getValue()!=null && this.orden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("orden", this.orden.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("orden", "");
		}
		if (this.tarea.getValue()!=null && this.tarea.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tarea", this.tarea.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tarea", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.orden.getValue()!=null && this.orden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("orden", this.orden.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("orden", "");
		}
		if (this.tarea.getValue()!=null && this.tarea.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tarea", this.tarea.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tarea", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarTarea(null);
		}
	}
	
	public void editarTarea(MapeoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoTareasEscandallo();
			
			if (!isBusqueda())
			{
				cps = consultaTareasEscandalloServer.getInstance(CurrentUser.get());
				establecerCamposObligatoriosCreacion(true);
				r_mapeo.setOrden(cps.obtenerSiguienteOrden());
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoTareasEscandallo>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarTarea(MapeoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((TareasEscandalloGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((TareasEscandalloGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
			HashMap<String,String> filtrosRec = ((TareasEscandalloGrid) uw.grid).recogerFiltros();
			ArrayList<MapeoTareasEscandallo> r_vector = (ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector;
			
			Indexed indexed = ((TareasEscandalloGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((TareasEscandalloGrid) uw.grid).sort("orden");
			((TareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
		}
	}
	
	public void modificarTarea(MapeoTareasEscandallo r_mapeo, MapeoTareasEscandallo r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo==null)
		{
			if (!((TareasEscandalloGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoTareasEscandallo> r_vector = (ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((TareasEscandalloGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((TareasEscandalloGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	//            indexed.addItemAt(list.size()+1, r_mapeo);
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	//            uw.generarGrid(uw.opcionesEscogidas);
	            uw.presentarGrid(r_vector);
	            
				((TareasEscandalloGrid) uw.grid).setRecords((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector);
	////			((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);
				((TareasEscandalloGrid) uw.grid).sort("orden");
				((TareasEscandalloGrid) uw.grid).scrollTo(r_mapeo);
				((TareasEscandalloGrid) uw.grid).select(r_mapeo);
				
				((TareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearTarea(MapeoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdCodigo(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoTareasEscandallo>(r_mapeo));
			if (((TareasEscandalloGrid) uw.grid)!=null )//&& )
			{
				if (!((TareasEscandalloGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((TareasEscandalloGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((TareasEscandalloGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector).add(r_mapeo);
					((TareasEscandalloGrid) uw.grid).setRecords((ArrayList<MapeoTareasEscandallo>) ((TareasEscandalloGrid) uw.grid).vector);
					((TareasEscandalloGrid) uw.grid).sort("orden");
					((TareasEscandalloGrid) uw.grid).scrollTo(r_mapeo);
					((TareasEscandalloGrid) uw.grid).select(r_mapeo);
					
					((TareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoTareasEscandallo> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
//		this.articulo.setRequired(r_obliga);
//		this.numerador.setRequired(r_obliga);
//		this.origen.setRequired(r_obliga);
//		this.notificador.setRequired(r_obliga);
//		this.cantidad.setRequired(r_obliga);
//		this.fechaNotificacion.setRequired(r_obliga);
//		this.fechaNotificacion.setEnabled(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.orden.getValue()==null || this.orden.getValue().toString().length()==0) return false;
			if (this.tarea.getValue()==null || this.tarea.getValue().toString().length()==0) return false;
		}
		
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}

}
