package borsao.e_borsao.Modulos.PRODUCCION.TareasEscandallo.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTareasEscandallo extends MapeoGlobal
{
	private Integer orden;

	private String tarea;
	
	
	public MapeoTareasEscandallo()
	{ 
	}


	public Integer getOrden() {
		return orden;
	}


	public void setOrden(Integer orden) {
		this.orden = orden;
	}


	public String getTarea() {
		return tarea;
	}


	public void setTarea(String tarea) {
		this.tarea = tarea;
	}

}