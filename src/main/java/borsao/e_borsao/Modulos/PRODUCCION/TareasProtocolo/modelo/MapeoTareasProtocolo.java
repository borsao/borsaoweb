package borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTareasProtocolo extends MapeoGlobal
{
	private Integer orden;
	private String responsable;
	private String mail;
	private String tarea;
	
	
	
	public MapeoTareasProtocolo()
	{ 
	}


	public Integer getOrden() {
		return orden;
	}


	public void setOrden(Integer orden) {
		this.orden = orden;
	}


	public String getTarea() {
		return tarea;
	}


	public void setTarea(String tarea) {
		this.tarea = tarea;
	}


	public String getResponsable() {
		return responsable;
	}


	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}

}