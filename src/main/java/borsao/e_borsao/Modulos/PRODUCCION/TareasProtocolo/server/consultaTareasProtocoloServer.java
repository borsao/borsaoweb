package borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.modelo.MapeoTareasProtocolo;

public class consultaTareasProtocoloServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTareasProtocoloServer instance;
	
	public consultaTareasProtocoloServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTareasProtocoloServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTareasProtocoloServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoTareasProtocolo> datosTareasProtocoloGlobal(MapeoTareasProtocolo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoTareasProtocolo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoTareasProtocolo mapeoTareasProtocolo=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_tareas_protocolo.idCodigo tar_id,");
		cadenaSQL.append(" prd_tareas_protocolo.tarea tar_tar,");
		cadenaSQL.append(" prd_tareas_protocolo.mail tar_mai,");
		cadenaSQL.append(" prd_tareas_protocolo.responsable tar_res,");
		cadenaSQL.append(" prd_tareas_protocolo.orden tar_ord ");
		cadenaSQL.append(" from prd_tareas_protocolo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getTarea()!=null && r_mapeo.getTarea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tarea = '" + r_mapeo.getTarea() + "' ");
				}
				if (r_mapeo.getMail()!=null && r_mapeo.getMail().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" mail = '" + r_mapeo.getMail() + "' ");
				}
				if (r_mapeo.getResponsable()!=null && r_mapeo.getResponsable().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" responsable = '" + r_mapeo.getResponsable() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoTareasProtocolo>();
			
			while(rsOpcion.next())
			{
				mapeoTareasProtocolo = new MapeoTareasProtocolo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTareasProtocolo.setIdCodigo(rsOpcion.getInt("tar_id"));
				mapeoTareasProtocolo.setOrden(rsOpcion.getInt("tar_ord"));
				mapeoTareasProtocolo.setTarea(rsOpcion.getString("tar_tar"));
				mapeoTareasProtocolo.setMail(rsOpcion.getString("tar_mai"));
				mapeoTareasProtocolo.setResponsable(rsOpcion.getString("tar_res"));
				vector.add(mapeoTareasProtocolo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public boolean comprobarTareasProtocolo(String r_tarea)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT prd_tareas_protocolo.orden tar_ord ");
		cadenaSQL.append(" from prd_tareas_protocolo ");
		cadenaSQL.append(" where tarea = '" + r_tarea + "' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String imprimirTarea(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_protocolo.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_tareas_protocolo ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public Integer obtenerSiguienteOrden()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tareas_protocolo.orden) tar_ord ");
     	cadenaSQL.append(" FROM prd_tareas_protocolo ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_ord") + 10 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}


	public String guardarNuevo(MapeoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_tareas_protocolo( ");
			cadenaSQL.append(" prd_tareas_protocolo.idCodigo,");			
			cadenaSQL.append(" prd_tareas_protocolo.tarea, ");			
			cadenaSQL.append(" prd_tareas_protocolo.responsable, ");
			cadenaSQL.append(" prd_tareas_protocolo.mail, ");			
			cadenaSQL.append(" prd_tareas_protocolo.orden ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getTarea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTarea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getResponsable()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getMail()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getMail());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_tareas_protocolo set ");
		    cadenaSQL.append(" prd_tareas_protocolo.tarea =?,");
		    cadenaSQL.append(" prd_tareas_protocolo.responsable =?,");
		    cadenaSQL.append(" prd_tareas_protocolo.mail =?,");
		    cadenaSQL.append(" prd_tareas_protocolo.orden =? ");
			cadenaSQL.append(" where prd_tareas_protocolo.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getTarea()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getTarea());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getResponsable()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getResponsable());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getMail()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getMail());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_tareas_protocolo ");            
			cadenaSQL.append(" WHERE prd_tareas_protocolo.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoTareasProtocolo r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/TareasProtocolo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("TareasProtocolo.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("TareasProtocolo");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}