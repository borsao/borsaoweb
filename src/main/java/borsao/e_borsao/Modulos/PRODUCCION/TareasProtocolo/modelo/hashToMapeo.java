package borsao.e_borsao.Modulos.PRODUCCION.TareasProtocolo.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoTareasProtocolo mapeo = null;
	 
	 public MapeoTareasProtocolo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoTareasProtocolo();

		 this.mapeo.setTarea(r_hash.get("tarea"));
		 this.mapeo.setMail(r_hash.get("mail"));
		 this.mapeo.setResponsable(r_hash.get("responsable"));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden"))));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoTareasProtocolo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoTareasProtocolo();

		 this.mapeo.setTarea(r_hash.get("tarea"));
		 this.mapeo.setMail(r_hash.get("mail"));
		 this.mapeo.setResponsable(r_hash.get("responsable"));
		 if (r_hash.get("orden")!=null && r_hash.get("orden").length()>0) this.mapeo.setOrden(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden"))));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoTareasProtocolo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("tarea", this.mapeo.getTarea());
		 this.hash.put("mail", this.mapeo.getMail());
		 this.hash.put("responsable", this.mapeo.getResponsable());
		 
		 this.hash.put("orden", String.valueOf(this.mapeo.getOrden()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}