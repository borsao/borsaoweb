package borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo.MapeoOpcionesMS;

public class consultaOpcionesMateriaSecaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaOpcionesMateriaSecaServer instance;
	
	public consultaOpcionesMateriaSecaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaOpcionesMateriaSecaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaOpcionesMateriaSecaServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoOpcionesMS> datosOpcionesGlobal(MapeoOpcionesMS r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoOpcionesMS> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_matseca.idprd_matseca opc_id, ");
        cadenaSQL.append(" prd_matseca.valor opc_nom, ");
	    cadenaSQL.append(" prd_matseca.color opc_col ");
     	cadenaSQL.append(" FROM prd_matseca ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdOpcion()!=null && r_mapeo.getIdOpcion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_matseca = '" + r_mapeo.getIdOpcion() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor = '" + r_mapeo.getNombre() + "' ");
				}
//				if (r_mapeo.getColor()!=null && r_mapeo.getColor().length()>0)
//				{
//					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
//					cadenaWhere.append(" color = '" + r_mapeo.getColor() + "' ");
//				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_matseca asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOpcionesMS>();
			
			while(rsOpcion.next())
			{
				MapeoOpcionesMS mapeoOperarios = new MapeoOpcionesMS();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOperarios.setIdOpcion(rsOpcion.getInt("opc_id"));
				mapeoOperarios.setNombre(rsOpcion.getString("opc_nom").toUpperCase());
				mapeoOperarios.setColor(rsOpcion.getString("opc_col"));
				vector.add(mapeoOperarios);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_matseca.idprd_matseca) opc_sig ");
     	cadenaSQL.append(" FROM prd_matseca ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("opc_sig")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String obtenerColor(String r_valor)
	{
		ResultSet rsOpcion= null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_matseca.color opc_col ");
     	cadenaSQL.append(" FROM prd_matseca ");
     	cadenaSQL.append(" where prd_matseca.valor = '" + r_valor + "'");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getString("opc_col");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String guardarNuevo(MapeoOpcionesMS r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_matseca ( ");
    		cadenaSQL.append(" prd_matseca.idprd_matseca, ");
	        cadenaSQL.append(" prd_matseca.valor, ");
		    cadenaSQL.append(" prd_matseca.color ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getNombre().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getColor()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getColor());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoOpcionesMS r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_matseca set ");
	        cadenaSQL.append(" prd_matseca.valor=?, ");
		    cadenaSQL.append(" prd_matseca.color=? ");
		    cadenaSQL.append(" WHERE prd_matseca.idprd_matseca = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getNombre().toUpperCase());
		    preparedStatement.setString(2, r_mapeo.getColor());
		    preparedStatement.setInt(3, r_mapeo.getIdOpcion());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoOpcionesMS r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_matseca ");            
			cadenaSQL.append(" WHERE prd_matseca.idprd_matseca = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdOpcion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public ArrayList<String> datosOpcionesCombo()
	{
		ResultSet rsOpcion = null;		
		ArrayList<String> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_matseca.valor opc_nom ");	    
     	cadenaSQL.append(" FROM prd_matseca ");

		try
		{
			cadenaSQL.append(" order by idprd_matseca asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<String>();
			
			while(rsOpcion.next())
			{
				vector.add(rsOpcion.getString("opc_nom").toUpperCase());				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
}