package borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoOpcionesMS  extends MapeoGlobal
{
    private Integer idOpcion;
	private String nombre;
	private String color;

	public MapeoOpcionesMS()
	{
		this.setNombre("");
		this.setColor("");
	}

	public Integer getIdOpcion() {
		return idOpcion;
	}

	public void setIdOpcion(Integer idOpcion) {
		this.idOpcion = idOpcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String r_nombre) {
		this.nombre = r_nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String r_color) {
		this.color= r_color;
	}
}