package borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.RutinasColores;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo.MapeoOpcionesMS;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.server.consultaOpcionesMateriaSecaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoOpcionesMS mapeoOpcionesMS = null;
	 
	 private consultaOpcionesMateriaSecaServer cus = null;	 
	 private OpcionesView uw = null;
	 private BeanFieldGroup<MapeoOpcionesMS> fieldGroup;
	 
	 public OpcionesForm(OpcionesView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoOpcionesMS>(MapeoOpcionesMS.class);
        fieldGroup.bindMemberFields(this);

        this.aviso.setCaptionAsHtml(true);
        this.aviso.setCaption("<p> AVISO </p>");
        this.aviso.setStyleName("bordeado");
        this.avisoTexto.setCaptionAsHtml(true);
        this.avisoTexto.setStyleName("bordeado");
        this.avisoTexto.setCaption("<p>Al crear o modificar el color de cualquier registro </p>" +
                                   "<p>poneros en contacto con informatica para su </p>" +
                                    "<p> implentacion </p>");

        this.color.setPosition(100, 100);
        
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoOpcionesMS = (MapeoOpcionesMS) uw.grid.getSelectedRow();
                eliminarOpcion(mapeoOpcionesMS);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoOpcionesMS = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOpcion(mapeoOpcionesMS);
	 			}
	 			else
	 			{
	 				MapeoOpcionesMS mapeoOpcionesMS_orig = (MapeoOpcionesMS) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoOpcionesMS= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarOpcion(mapeoOpcionesMS,mapeoOpcionesMS_orig);
	 				hm=null;
	 			}
	 			
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.color.getColor()!=null && this.color.getColor().getCSS()!="")
		{
			opcionesEscogidas.put("color", String.valueOf(this.color.getColor().getCSS()));
		}
		else
		{
			opcionesEscogidas.put("color", "");
		}
		
		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoOpcionesMS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoOpcionesMS();
		fieldGroup.setItemDataSource(new BeanItem<MapeoOpcionesMS>(r_mapeo));
		if (r_mapeo!=null && r_mapeo.getColor().length()>0) this.color.setColor(new Color(RutinasColores.convertirHexaRGB(r_mapeo.getColor())));		
		nombre.focus();
	}
	
	public void eliminarOpcion(MapeoOpcionesMS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaOpcionesMateriaSecaServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarOpcion(MapeoOpcionesMS r_mapeo, MapeoOpcionesMS r_mapeo_orig)
	{
		cus  = new consultaOpcionesMateriaSecaServer(CurrentUser.get());
		r_mapeo.setIdOpcion(r_mapeo_orig.getIdOpcion());
		cus.guardarCambios(r_mapeo);		
		((OpcionesGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearOpcion(MapeoOpcionesMS r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaOpcionesMateriaSecaServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoOpcionesMS>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null)
			{
				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoOpcionesMS> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoOpcionesMS= item.getBean();
            if (this.mapeoOpcionesMS.getIdOpcion()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

}
