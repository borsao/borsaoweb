package borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoOpcionesMS mapeo = null;
	 
	 public MapeoOpcionesMS convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoOpcionesMS();
		 this.mapeo.setColor(r_hash.get("color"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdOpcion(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public MapeoOpcionesMS convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoOpcionesMS();
		 this.mapeo.setColor(r_hash.get("color"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdOpcion(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoOpcionesMS r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("color", r_mapeo.getColor());
		 this.hash.put("nombre", this.mapeo.getNombre());
		 this.hash.put("id", this.mapeo.getIdOpcion().toString());
		 return hash;		 
	 }
}