package borsao.e_borsao.Modulos.PRODUCCION.Programacion.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view.pantallaLineasDetalleMezclaVinos;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server.consultaTiposOFServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasProgramaciones extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private String articuloSeleccionado = null;
	private String estado = null;
	private Integer id = null;
	private Integer semana = null;
	private Integer ejercicio = null;
	private String articuloMP = null;
	private ArrayList<String> padresSeleccionados = null;
	private pantallaLineasDetalleMezclaVinos app = null;
	private String area = null;
	private boolean global = false;
	
	public pantallaLineasProgramaciones(String r_articulo, String r_titulo, String r_area)
	{
		this.articuloSeleccionado=r_articulo;
		this.area = r_area;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public pantallaLineasProgramaciones(Integer r_id, String r_titulo)
	{
		this.id=r_id;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasProgramaciones(Integer r_ejercicio, String r_semana, String r_titulo)
	{
		this.ejercicio=r_ejercicio;
		this.semana = new Integer(r_semana);
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasProgramaciones(String r_articulo, Integer r_ejercicio, String r_titulo, boolean r_global)
	{
		this.ejercicio=r_ejercicio;
		this.articuloSeleccionado = r_articulo;
		this.global=r_global;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public pantallaLineasProgramaciones(pantallaLineasDetalleMezclaVinos r_window, SelectionMode r_select, Integer r_ejercicio, Integer r_semana, boolean r_consulta)
	{
		this.articuloSeleccionado=null;
		this.semana = r_semana;
		this.ejercicio=r_ejercicio;
		this.app= r_window;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption("Lineas Programaciones");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(r_select);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				if (!r_consulta) cerrar(); else close(); 
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasProgramaciones(ArrayList<String> r_padres, String r_titulo, String r_articulo, String r_area, Integer r_ejercicio )
	{
		this.padresSeleccionados=r_padres;
		this.articuloMP = r_articulo;
		this.ejercicio=r_ejercicio;
		this.area = r_area;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasProgramaciones(String r_estado, ArrayList<String> r_padres, String r_titulo, String r_articulo, String r_area, Integer r_ejercicio)
	{
		this.padresSeleccionados=r_padres;
		this.articuloMP = r_articulo;
		this.area = r_area;
		this.ejercicio=r_ejercicio;
		this.estado=r_estado;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}

	
	private void llenarRegistros(SelectionMode r_selecet)
	{
		Iterator iterator = null;
		boolean etiquetados = true;
		String origen = "";
		
		MapeoProgramacion mapeo = null;
		ArrayList<MapeoProgramacion> vector = null;		
		consultaProgramacionServer cps = null;
		
		MapeoProgramacionEnvasadora mapeoEnvasadora = null;
		ArrayList<MapeoProgramacionEnvasadora> vectorEnvasadora = null;
		consultaProgramacionEnvasadoraServer cpes = null;
		
		container = new IndexedContainer();
		container.addContainerProperty("idProg", Integer.class, null);
		container.addContainerProperty("ejercicio", Integer.class, null);
		container.addContainerProperty("semana", String.class, null);
		
		if (area!=null && area.equals("Mesa"))
		{			
			mapeoEnvasadora = new MapeoProgramacionEnvasadora();
			cpes = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
			
			if (this.ejercicio!=null) mapeoEnvasadora.setEjercicio(this.ejercicio);
			if (this.semana!=null) mapeoEnvasadora.setSemana(this.semana.toString());

			if (this.articuloSeleccionado!=null)
			{
				mapeoEnvasadora.setArticulo(this.articuloSeleccionado);
				mapeoEnvasadora.setEstado("A");
				origen="produccion";
				vectorEnvasadora = cpes.datosProgramacionGlobal(mapeoEnvasadora);
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
			}
			else if (this.articuloMP!=null)
			{
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
				container.addContainerProperty("articulo", String.class, null);
				container.addContainerProperty("descripcion", String .class, null);
				
				origen = "padres";
				vectorEnvasadora = cpes.datosProgramacionGlobal(this.padresSeleccionados,mapeoEnvasadora, true, true);
			}
			
			mapeoEnvasadora=null;
			iterator = vectorEnvasadora.iterator();
			
			
			container.addContainerProperty("cajas", Integer.class, null);
			container.addContainerProperty("factor", Integer.class, null);
			container.addContainerProperty("unidades", Integer.class, null);
	        container.addContainerProperty("observaciones", String.class, null);
	        
			while (iterator.hasNext())
			{
				Object newItemId = container.addItem();
				Item file = container.getItem(newItemId);
				
				mapeoEnvasadora = (MapeoProgramacionEnvasadora) iterator.next();
				
				file.getItemProperty("idProg").setValue(mapeoEnvasadora.getIdProgramacion());
	    		file.getItemProperty("ejercicio").setValue(mapeoEnvasadora.getEjercicio());
	    		file.getItemProperty("semana").setValue(mapeoEnvasadora.getSemana());
	    		
	    		if (origen.equals("padres"))
	    		{
	    			file.getItemProperty("dia").setValue(mapeoEnvasadora.getDia());
	        		file.getItemProperty("articulo").setValue(mapeoEnvasadora.getArticulo());
	        		file.getItemProperty("descripcion").setValue(mapeoEnvasadora.getDescripcion());
	    		}
	    		else if (origen.equals("produccion"))
	    		{
	        		file.getItemProperty("dia").setValue(mapeoEnvasadora.getDia());
	        		
	    		}
	    		
	    		file.getItemProperty("tipoOperacion").setValue(mapeoEnvasadora.getTipo());
	    		file.getItemProperty("unidades").setValue(mapeoEnvasadora.getUnidades());
	    		file.getItemProperty("cajas").setValue(mapeoEnvasadora.getCajas());
	    		file.getItemProperty("factor").setValue(mapeoEnvasadora.getFactor());
	    		file.getItemProperty("observaciones").setValue(RutinasCadenas.conversion(mapeoEnvasadora.getObservaciones()));
			}
		}
		else
		{
			mapeo = new MapeoProgramacion();
			cps = new consultaProgramacionServer(CurrentUser.get());
		
			if (this.ejercicio!=null) mapeo.setEjercicio(this.ejercicio);
			if (this.semana!=null) mapeo.setSemana(this.semana.toString());
				
			if (this.articuloSeleccionado!=null)
			{
				mapeo.setArticulo(this.articuloSeleccionado);
				mapeo.setEstado("X");
				if (global) mapeo.setEstado(null);
				origen="produccion";
				vector = cps.datosProgramacionGlobalNuevo(mapeo);
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
			}
			else if (this.id!=null)
			{
				mapeo.setIdProgramacion(this.id);
				vector = cps.datosProgramacionGlobalNuevo(mapeo);
				origen="calidad";
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
				container.addContainerProperty("articulo", String.class, null);
				container.addContainerProperty("descripcion", String .class, null);
	
			}
			else if (this.articuloMP!=null)
			{
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
				container.addContainerProperty("articulo", String.class, null);
				container.addContainerProperty("descripcion", String .class, null);
				
				if (this.articuloMP.startsWith("0104") || this.articuloMP.startsWith("0105") || this.articuloMP.startsWith("0101")) etiquetados = false;
				origen = "padres";
				vector = cps.datosProgramacionGlobal(this.padresSeleccionados, this.estado, etiquetados, this.ejercicio);
			}
			else
			{
				container.addContainerProperty("dia", String.class, null);
				container.addContainerProperty("tipoOperacion", String.class, null);
				container.addContainerProperty("cantidad", Integer.class, null);
				container.addContainerProperty("vino", String .class, null);
				container.addContainerProperty("articulo", String.class, null);
				container.addContainerProperty("descripcion", String .class, null);			
				origen="laboratorio";
				vector = cps.datosProgramacionGlobal(mapeo);
			}
			
			mapeo=null;
			iterator = vector.iterator();
			
			
			container.addContainerProperty("cajas", Integer.class, null);
			container.addContainerProperty("factor", Integer.class, null);
			container.addContainerProperty("unidades", Integer.class, null);
			container.addContainerProperty("pallets", Integer.class, null);
	        container.addContainerProperty("observaciones", String.class, null);
	        
			while (iterator.hasNext())
			{
				Object newItemId = container.addItem();
				Item file = container.getItem(newItemId);
				
				mapeo= (MapeoProgramacion) iterator.next();
				
				file.getItemProperty("idProg").setValue(mapeo.getIdProgramacion());
	    		file.getItemProperty("ejercicio").setValue(mapeo.getEjercicio());
	    		file.getItemProperty("semana").setValue(mapeo.getSemana());
	    		file.getItemProperty("dia").setValue(mapeo.getDia());
	    		
	    		if (origen.equals("padres"))
	    		{
	        		file.getItemProperty("articulo").setValue(mapeo.getArticulo());
	        		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
	    		}
	    		else if (origen.equals("produccion"))
	    		{
	        		
	    		}
	    		else if (origen.equals("laboratorio"))
	    		{
	        		file.getItemProperty("cantidad").setValue(mapeo.getCantidad());
	        		file.getItemProperty("vino").setValue(mapeo.getVino());
	        		file.getItemProperty("articulo").setValue(mapeo.getArticulo());
	        		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
	    		}
	    		else if (origen.equals("calidad"))
	    		{
	    			file.getItemProperty("articulo").setValue(mapeo.getArticulo());
	        		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
	    		}
	    		
	    		
	    		file.getItemProperty("tipoOperacion").setValue(mapeo.getTipo());
	    		file.getItemProperty("unidades").setValue(mapeo.getUnidades());
	    		file.getItemProperty("cajas").setValue(mapeo.getCajas());
	    		file.getItemProperty("factor").setValue(mapeo.getFactor());
	    		file.getItemProperty("pallets").setValue(this.obtenerCantidadPallets(mapeo.getArticulo(), mapeo.getUnidades()));
	    		
	    		file.getItemProperty("observaciones").setValue(RutinasCadenas.conversion(mapeo.getObservaciones()));
			}

		}
		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(r_selecet);

		this.asignarEstilos();
	}
	
	private Integer obtenerCantidadPallets(String r_articulo, Integer r_unidades)
	{
		String sufijo=null;
		Double cantidad = new Double(0);
		
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		if (r_articulo.contentEquals("0103004") || r_articulo.contentEquals("0103104"))
			sufijo = "28";
		else if (r_articulo.contentEquals("0102006") || r_articulo.contentEquals("0102091") ||
				r_articulo.contentEquals("0103005") || r_articulo.contentEquals("0103012") || r_articulo.contentEquals("0103105")
				|| r_articulo.contentEquals("0103112"))
			sufijo="38";
		else if (r_articulo.contentEquals("0103001") || r_articulo.contentEquals("0103010") || r_articulo.contentEquals("0103002")
				|| r_articulo.contentEquals("0103003") || r_articulo.contentEquals("0103011") || r_articulo.contentEquals("0103004"))
			sufijo="58";
		else
			sufijo ="18";
		
			
		String paletizacionArticulo=cas.obtenerCantidadEan(r_articulo, sufijo);
		
		if (paletizacionArticulo!=null)
		{
			Double paletizacion = new Double(paletizacionArticulo);				
			cantidad = cantidad + Math.ceil(r_unidades/paletizacion);
		}
		else
			cantidad=new Double(0);
		
		return cantidad.intValue();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.getColumn("idProg").setHidden(true);
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("tipoOperacion".equals(cellReference.getPropertyId())) 
            	{
            		consultaTiposOFServer tiposOF = consultaTiposOFServer.getInstance(CurrentUser.get());
            		String color = tiposOF.obtenerColor(cellReference.getValue().toString());
            		return "cell-" + color.substring(1, 7);
                }
            	if ("unidades".equals(cellReference.getPropertyId()) || "cajas".equals(cellReference.getPropertyId()) || "factor".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void cerrar()
	{
		
		MapeoProgramacion mapeo = new MapeoProgramacion();
		Integer fila = (Integer) this.gridDatos.getSelectedRow();
		Item item = this.gridDatos.getContainerDataSource().getItem(fila);
		
		mapeo.setIdProgramacion(new Integer(item.getItemProperty("idProg").getValue().toString()));
		mapeo.setSemana(item.getItemProperty("semana").getValue().toString());
		mapeo.setEjercicio(new Integer(item.getItemProperty("ejercicio").getValue().toString()));
		mapeo.setDia(item.getItemProperty("dia").getValue().toString());
		mapeo.setVino(item.getItemProperty("vino").getValue().toString());
		mapeo.setTipo(item.getItemProperty("tipoOperacion").getValue().toString());
		mapeo.setCantidad(new Integer(item.getItemProperty("cantidad").getValue().toString()));
		
		this.app.insertarDatos(mapeo);
		close();
	}
}

