package borsao.e_borsao.Modulos.PRODUCCION.Programacion.view;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.view.SituacionEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view.RetrabajosView;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionAsignacionProgramacion extends Window
{
	private static Integer numeroPalets= 5;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonAsignar = null;
	private Button btnBotonCVentana = null;

	public Window padre = null;
	public TextField txtEjercicio= null;
	
	public ComboBox cmbSemana = null;
	public ComboBox cmbDestino = null;
	public ComboBox cmbOperacion = null;
	private MapeoGlobal mapeo = null;

	public TextField txtUnidades= null;
	public TextField txtCantidad= null;
	public TextField txtVelocidad= null;
	public TextField txtFactor= null;
	public TextField txtObservaciones= null;
	public Label lblUnidadesPalet= null;
	public Label  lblProduccionMinima= null;
	
	public Button opcPedidos = null;
	public Button opcIntranet = null;
	
	private String semana = null;
	private String area = null;
	private String idsPedidos = null;
	private GridViewRefresh app=null;
	/*
	 * Valores devueltos
	 */
	
	public PeticionAsignacionProgramacion(RetrabajosView r_app, MapeoRetrabajos r_mapeo, String r_titulo)
	{
		this.app=r_app;
		this.padre = this;
		this.mapeo=r_mapeo;
		this.area = "Retrabajos";
		
		this.setCaption(r_titulo);
		this.cargarPantalla();

  		this.cargarCombo(null);
  		this.cargarValoresPorDefecto();
		
		this.habilitarCampos();
		this.cargarListeners();
	}
	
	
	public PeticionAsignacionProgramacion(SituacionEmbotelladoView r_app, MapeoSituacionEmbotellado r_mapeo, String r_titulo)
	{
		this.padre = this;
		this.app=r_app;
		this.mapeo=r_mapeo;
		this.area = "Necesidades";
		
		this.cargarPantalla();
		this.setCaption(r_titulo);

  		this.cargarCombo(null);
		
		this.cargarValoresPorDefecto();
		this.habilitarCampos();
		this.cargarListeners();
	}

	private void cargarValoresPorDefecto()
	{
		switch (this.area)
		{
			case "Necesidades":
			{
			    consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			    consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			    
			    Integer palet = ces.obtenerCantidadPaletEmbotelladora(((MapeoSituacionEmbotellado) this.mapeo).getArticulo());
			    Integer factor = ces.recogerTipoCaja(((MapeoSituacionEmbotellado) this.mapeo).getArticulo());
			    Integer vel = cas.obtenerVelocidadArticulo(((MapeoSituacionEmbotellado) this.mapeo).getArticulo());
			    Integer prodMin = palet * this.numeroPalets;
			    Integer cantidad = (((MapeoSituacionEmbotellado) mapeo).getStock_real()/factor.intValue());
				this.txtCantidad.setValue(cantidad.toString());
				this.txtVelocidad.setValue(vel.toString());
				this.txtFactor.setValue(factor.toString());
				this.txtUnidades.setValue(((MapeoSituacionEmbotellado) mapeo).getStock_real().toString());
				this.lblUnidadesPalet.setValue(palet.toString());
				this.lblProduccionMinima.setValue(prodMin.toString());
				
				switch (((MapeoSituacionEmbotellado) mapeo).getNegativo().trim())
			    {
			    	case "rb":
			    	case "mb":
			    	case "mr":
			    	case "mc":
			    	case "sb":
			    	{
			    		this.cmbOperacion.setValue("Embotellado");
			    		break;
			    	}
			    	
			    	case "vb":
			    	case "ar":
			    	case "ab":
			    	case "ac":
			    	case "se":
			    	{
			    		this.cmbOperacion.setValue("Etiquetado");
			    		break;
			    	}
			    	
			    }
				break;
			}
			case "Retrabajos":
			{
				this.cmbOperacion.setValue("Retrabajo");
				this.txtCantidad.setValue(((MapeoRetrabajos) mapeo).getCantidad().toString());
				this.txtFactor.setValue(((MapeoRetrabajos) mapeo).getFactor().toString());
				this.txtUnidades.setValue(((MapeoRetrabajos) mapeo).getUnidades().toString());
				break;
			}
		}
		
		this.cmbSemana.setValue(this.semana);
	}
	
	private void cargarPantalla()
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		
		HorizontalLayout fila2 = new HorizontalLayout();
		fila2.setWidthUndefined();
		fila2.setSpacing(true);
		
		HorizontalLayout fila3 = new HorizontalLayout();
		fila3.setWidthUndefined();
		fila3.setSpacing(true);

		HorizontalLayout fila4 = new HorizontalLayout();
		fila4.setWidthUndefined();
		fila4.setSpacing(true);

		HorizontalLayout fila5 = new HorizontalLayout();
		fila5.setWidthUndefined();
		fila5.setSpacing(true);

		HorizontalLayout fila6 = new HorizontalLayout();
		fila6.setWidthUndefined();
		fila6.setSpacing(true);

		this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setEnabled(false);
		
		fila2.addComponent(this.txtEjercicio);
		
		this.cmbSemana= new ComboBox("Semana");    		
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setNullSelectionAllowed(false);
		this.cargarCombo(null);
		
		fila2.addComponent(cmbSemana);
		
		this.cmbDestino= new ComboBox("Programacion Destino");    		
		this.cmbDestino.setNewItemsAllowed(false);
		this.cmbDestino.setNullSelectionAllowed(false);
		this.cmbDestino.addItem("Embotelladora");
		this.cmbDestino.addItem("Envasadora");
		this.cmbDestino.setValue("Embotelladora");
		
		fila2.addComponent(cmbDestino);
		
		controles.addComponent(fila2);
	
		this.cmbOperacion= new ComboBox("Tipo Operacion");    		
		this.cmbOperacion.setNewItemsAllowed(false);
		this.cmbOperacion.setNullSelectionAllowed(false);
		this.cmbOperacion.addItem("Embotellado");
		this.cmbOperacion.addItem("Etiquetado");
		this.cmbOperacion.addItem("Retrabajo");

		this.txtVelocidad=new TextField("Velocidad");		
		this.txtVelocidad.setEnabled(false);

		fila3.addComponent(this.cmbOperacion);
		fila3.addComponent(this.txtVelocidad);
		controles.addComponent(fila3);
		
		this.txtCantidad=new TextField("Cantidad");		
		this.txtCantidad.setEnabled(false);

		this.txtFactor=new TextField("Factor");
		this.txtFactor.setEnabled(false);

		this.txtUnidades=new TextField("Unidades");
		this.txtUnidades.setEnabled(false);

		fila4.addComponent(this.txtCantidad);
		fila4.addComponent(this.txtFactor);
		fila4.addComponent(this.txtUnidades);
		controles.addComponent(fila4);
		
		this.txtObservaciones=new TextField("Observaciones");
		this.txtObservaciones.setEnabled(false);
		this.txtObservaciones.setWidth("450px");

		this.opcPedidos = new Button("Pedidos");
		this.opcPedidos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcPedidos.addStyleName(ValoTheme.BUTTON_TINY);

		this.opcIntranet= new Button();
		this.opcIntranet.setIcon(FontAwesome.BOOK);
		this.opcIntranet.addStyleName(ValoTheme.BUTTON_PRIMARY);
		this.opcIntranet.addStyleName(ValoTheme.BUTTON_TINY);

		fila5.addComponent(this.txtObservaciones);		
		fila5.addComponent(this.opcPedidos);
		fila5.setComponentAlignment(this.opcPedidos, Alignment.BOTTOM_LEFT);
		fila5.addComponent(this.opcIntranet);
		fila5.setComponentAlignment(this.opcIntranet, Alignment.BOTTOM_LEFT);
		controles.addComponent(fila5);
		
		this.lblUnidadesPalet = new Label();
		this.lblUnidadesPalet.setCaption("Paletización: ");
		
		this.lblProduccionMinima = new Label();
		this.lblProduccionMinima.setCaption("Produccion Mínima: ");
		
		fila6.addComponent(this.lblUnidadesPalet);
		fila6.addComponent(this.lblProduccionMinima);
		
		if (this.area.contentEquals("Necesidades")) controles.addComponent(fila6);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Salir");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_DANGER);
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Nuevo");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAsignar = new Button("Asignar");
		btnBotonAsignar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		btnBotonAsignar.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonAsignar);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonAsignar, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		
	}
	
	private void habilitarCampos()
	{
	
		this.cmbSemana.setEnabled(true);
		this.cmbDestino.setEnabled(true);
		this.txtEjercicio.setEnabled(true);
		
		this.txtCantidad.setEnabled(true);
		this.txtVelocidad.setEnabled(true);
		this.txtFactor.setEnabled(true);
		this.txtUnidades.setEnabled(true);
		this.txtObservaciones.setEnabled(true);
		
		consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
		consultaProgramacionEnvasadoraServer ces = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
		
		boolean prog = false;
		
		
		if (this.cmbDestino.getValue().toString().equals("Embotelladora"))
		{
			if (this.area.contentEquals("Retrabajos"))
				prog =cps.comprobarProgramado( ((MapeoRetrabajos)this.mapeo).getArticulo().trim(),this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
			else if (this.area.contentEquals("Necesidades"))
				prog =cps.comprobarProgramado( ((MapeoSituacionEmbotellado)this.mapeo).getArticulo().trim(),this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
			
		}
		else if (this.cmbDestino.getValue().toString().equals("Envasadora"))
		{
			if (this.area.contentEquals("Retrabajos"))
				prog =ces.comprobarProgramado( ((MapeoRetrabajos)this.mapeo).getArticulo(),this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
			else if (this.area.contentEquals("Necesidades"))
				prog =ces.comprobarProgramado( ((MapeoSituacionEmbotellado)this.mapeo).getArticulo(),this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
		}
		
		this.btnBotonAsignar.setEnabled(prog);
	}
	
	private void cargarListeners()
	{

		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				semana = cmbSemana.getValue().toString();
				habilitarCampos();
			}
		}); 

		this.cmbDestino.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				habilitarCampos();
			}
		}); 
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (cmbOperacion.getValue()!=null)
				{
					if (txtEjercicio.getValue()!=null && semana!=null && cmbDestino.getValue().equals("Embotelladora"))
					{
				    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
				    	
				    	String rdo = null;
				    	if (area.contentEquals("Retrabajos"))
				    	{
				    		if (txtCantidad!=null) ((MapeoRetrabajos) mapeo).setCantidad(new Integer(txtCantidad.getValue()));
				    		if (txtFactor!=null) ((MapeoRetrabajos) mapeo).setFactor(new Integer(txtFactor.getValue()));
				    		if (txtUnidades!=null) ((MapeoRetrabajos) mapeo).setUnidades(new Integer(txtUnidades.getValue()));
				    		if (txtObservaciones!=null) ((MapeoRetrabajos) mapeo).setObservaciones(txtObservaciones.getValue());
				    		
				    		rdo = cps.programarNuevoRetrabajo((MapeoRetrabajos) mapeo,txtEjercicio.getValue(),semana,idsPedidos);
				    	}
				    	else if (area.contentEquals("Necesidades"))
				    	{
				    		if (txtCantidad!=null) ((MapeoSituacionEmbotellado) mapeo).setCantidad(new Integer(txtCantidad.getValue()));
				    		if (txtFactor!=null) ((MapeoSituacionEmbotellado) mapeo).setFactor(new Integer(txtFactor.getValue()));
				    		if (txtUnidades!=null) ((MapeoSituacionEmbotellado) mapeo).setStock_real(new Integer(txtUnidades.getValue()));
				    		if (txtObservaciones!=null) ((MapeoSituacionEmbotellado) mapeo).setObservaciones(txtObservaciones.getValue());
				    		if (cmbOperacion.getValue()!=null) ((MapeoSituacionEmbotellado) mapeo).setOperacion(cmbOperacion.getValue().toString());
				    		
				    		rdo = cps.programarDesdeNecesidades((MapeoSituacionEmbotellado) mapeo,txtEjercicio.getValue(),semana, idsPedidos, new Integer(txtVelocidad.getValue()));
				    		
				    	}
				    	if (rdo==null) 
				    		cerrar(); 
				    	else 
				    		Notificaciones.getInstance().mensajeInformativo(rdo); 
					}
					else if (txtEjercicio.getValue()!=null && semana!=null && cmbDestino.getValue().equals("Envasadora"))
					{
						consultaProgramacionEnvasadoraServer ces = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
						
				    	String rdo = null;
				    	if (area.contentEquals("Retrabajos"))
				    	{
				    		rdo = ces.programarNuevoRetrabajo((MapeoRetrabajos) mapeo,txtEjercicio.getValue(),semana);
				    	}
				    	
				    	if (rdo==null) 
				    		cerrar(); 
				    	else 
				    		Notificaciones.getInstance().mensajeInformativo(rdo);
					}
					else
						Notificaciones.getInstance().mensajeInformativo("No hay datos a guardar.");
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena el tipo de operacion.");
				}
		}});
		
		
		opcIntranet.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				
        		String host = UI.getCurrent().getPage().getLocation().getHost().trim();        		
        		if (host.equals("localhost")) host = "192.168.6.6";
        		String articulo = null;
        		
        		if (area.contentEquals("Retrabajos"))
		    	{
		    		articulo = ((MapeoRetrabajos) mapeo).getArticulo();
		    	}
        		else if (area.contentEquals("Necesidades"))
		    	{
		    		articulo = ((MapeoSituacionEmbotellado) mapeo).getArticulo();
		    	}
        		
        		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + articulo.trim();
        		getUI().getPage().open(url, "_blank");
				
			}
		});
		opcPedidos.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				
        		pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(padre, ((MapeoSituacionEmbotellado) mapeo).getArticulo(), ((MapeoSituacionEmbotellado) mapeo).getStock_actual(), "Lineas Pedidos Venta del Articulo " + ((MapeoSituacionEmbotellado) mapeo).getArticulo() + " " + ((MapeoSituacionEmbotellado) mapeo).getDescripcion(), true );
    			getUI().addWindow(vt);
		}});
		
		btnBotonAsignar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				
				if (txtEjercicio.getValue()!=null && semana!=null && cmbDestino.getValue().equals("Embotelladora"))
				{
			    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
			    	MapeoProgramacion mapeoPr = new MapeoProgramacion();
			    	
			    	String rdo = null;
			    	if (area.contentEquals("Retrabajos"))
			    	{
			    		mapeoPr = cps.recogerProgramado(((MapeoRetrabajos) mapeo).getArticulo(),txtEjercicio.getValue(),semana);
			    		((MapeoRetrabajos) mapeo).setIdProgramacion(mapeoPr.getIdProgramacion());
			    		rdo = cps.asignarRetrabajo(((MapeoRetrabajos) mapeo),txtEjercicio.getValue(),semana);
			    	
				    	if (rdo==null)
				    	{
					    	mapeoPr.setObservaciones(mapeoPr.getObservaciones() + " " + ((MapeoRetrabajos) mapeo).getObservaciones());				    	
					    	rdo = cps.actualizarProgramacion(mapeoPr,idsPedidos);
					    	if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);				    	
					    	cerrar();
				    	}
				    	else 
				    		Notificaciones.getInstance().mensajeInformativo(rdo); 
			    	}
			    	else if (area.contentEquals("Necesidades"))
			    	{
			    		mapeoPr = cps.recogerProgramado(((MapeoSituacionEmbotellado) mapeo).getArticulo(),txtEjercicio.getValue(),semana);
			    	
				    	mapeoPr.setObservaciones(mapeoPr.getObservaciones() + " " + ((MapeoSituacionEmbotellado) mapeo).getObservaciones());
				    	mapeoPr.setCantidad(mapeoPr.getCantidad() + ((MapeoSituacionEmbotellado) mapeo).getCantidad());
				    	mapeoPr.setUnidades(mapeoPr.getUnidades() + ((MapeoSituacionEmbotellado) mapeo).getStock_real());
				    	rdo = cps.actualizarProgramacion(mapeoPr,idsPedidos);
				    	if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);				    	
				    	cerrar();
			    	}			    	

				}
				else if (txtEjercicio.getValue()!=null && semana!=null && cmbDestino.getValue().equals("Envasadora"))
				{
			    	consultaProgramacionEnvasadoraServer ces = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
			    	MapeoProgramacionEnvasadora mapeoPr = new MapeoProgramacionEnvasadora();
			    	String rdo = null;
			    	
			    	if (area.contentEquals("Retrabajos"))
			    	{
			    	
				    	mapeoPr = ces.recogerProgramado(((MapeoRetrabajos) mapeo).getArticulo(),txtEjercicio.getValue(),semana);
				    	((MapeoRetrabajos) mapeo).setIdProgramacionEnvasadora(mapeoPr.getIdProgramacion());
				    	
				    	rdo = ces.asignarRetrabajo(((MapeoRetrabajos) mapeo),txtEjercicio.getValue(),semana);
				    	
				    	if (rdo==null)
				    	{
					    	mapeoPr.setObservaciones(mapeoPr.getObservaciones() + " " + ((MapeoRetrabajos) mapeo).getObservaciones());
					    	rdo = ces.actualizarObservacionesProgramacion(mapeoPr);
					    	if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
					    	cerrar();
				    	}
				    	else 
				    		Notificaciones.getInstance().mensajeInformativo(rdo); 
			    	}
				}
				else
					Notificaciones.getInstance().mensajeInformativo("No hay datos a asignar.");
		}});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
	}

	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}

	private void cerrar()
	{
		if(this.area.contentEquals("Retrabajos"))
		{
			((RetrabajosView) this.app).actualizarDatos();
		}
		else if(this.area.contentEquals("Necesidades"))
		{
			if (this.app!=null) ((SituacionEmbotelladoView) this.app).actualizarDatos();
		}
		close();
	}
	
	public void recogerIds(String r_ids)
	{
		this.idsPedidos= r_ids;
		Notificaciones.getInstance().mensajeAdvertencia("Ids Seleccionados: " + r_ids);
	}
}