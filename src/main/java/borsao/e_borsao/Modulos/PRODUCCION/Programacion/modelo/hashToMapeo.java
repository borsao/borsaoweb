package borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoProgramacion mapeo = null;
	 
	 public MapeoProgramacion convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoProgramacion();		 
		 if (r_hash.get("id")!=null) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("id")));
		 if (r_hash.get("ejercicio")!=null) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio").replace(".", "")));
		 this.mapeo.setSemana(r_hash.get("semana"));
		 this.mapeo.setDia(r_hash.get("dia"));
		 this.mapeo.setTipo(r_hash.get("tipoOperacion"));
		 this.mapeo.setVino(r_hash.get("vino"));
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setLote_pt(r_hash.get("lotePT"));
		 this.mapeo.setLote_jaulon_1(r_hash.get("lote_jaulon_1"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFrontales(r_hash.get("frontales"));
		 this.mapeo.setContras(r_hash.get("contras"));
		 this.mapeo.setCapsulas(r_hash.get("capsulas"));
		 this.mapeo.setTapones(r_hash.get("tapones"));
		 this.mapeo.setCaja(r_hash.get("caja"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 if (r_hash.get("cantidad")!=null) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad").replace(".", "")));
		 if (r_hash.get("velocidad")!=null) this.mapeo.setVelocidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("velocidad").replace(".", "")));
		 if (r_hash.get("cajas")!=null) this.mapeo.setCajas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cajas").replace(".", "")));
		 if (r_hash.get("factor")!=null) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor").replace(".", "")));
		 if (r_hash.get("unidades")!=null) this.mapeo.setUnidades(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades").replace(".", "")));
		 if (r_hash.get("status")!=null) this.mapeo.setStatus(RutinasNumericas.formatearIntegerDeESP(r_hash.get("status")));
		 if (r_hash.get("orden")!=null) this.mapeo.setOrden(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden").replace(".", "")));
		 this.mapeo.setNuevo(r_hash.get("nuevo"));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null)
			 {
				 this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
				 if (this.mapeo.getTipo().toUpperCase().contentEquals("LINEA"))
				 {
					 this.mapeo.setDia(RutinasFechas.obtenerDiaDeLaSemanaLetra(RutinasFechas.convertirADate(r_hash.get("fecha"))).toUpperCase() + " " + RutinasFechas.diaFecha(RutinasFechas.convertirADate(r_hash.get("fecha"))) + "/" + RutinasFechas.mesFecha(RutinasFechas.convertirADate(r_hash.get("fecha"))));
				 }
			 }
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Programacion: fecha erronea. " + ex.getMessage());
		 }

		 return mapeo;		 
	 }
	 
	 public MapeoProgramacion convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoProgramacion();		 
		 if (r_hash.get("id")!=null) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("id")));
		 if (r_hash.get("ejercicio")!=null) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio").replace(".", "")));
		 this.mapeo.setSemana(r_hash.get("semana"));
		 
		 
		 
		 this.mapeo.setTipo(r_hash.get("tipoOperacion"));
		 this.mapeo.setVino(r_hash.get("vino"));
		 this.mapeo.setAnada(r_hash.get("anada"));
		 this.mapeo.setLote_jaulon_1(r_hash.get("lote_jaulon_1"));
		 this.mapeo.setLote_pt(r_hash.get("lotePT"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFrontales(r_hash.get("frontales"));
		 this.mapeo.setContras(r_hash.get("contras"));
		 this.mapeo.setCapsulas(r_hash.get("capsulas"));
		 this.mapeo.setTapones(r_hash.get("tapones"));
		 this.mapeo.setCaja(r_hash.get("caja"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 this.mapeo.setNuevo(r_hash.get("nuevo"));
		 if (r_hash.get("cantidad")!=null) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad").replace(".", "")));
		 if (r_hash.get("velocidad")!=null) this.mapeo.setVelocidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("velocidad").replace(".", "")));
		 if (r_hash.get("cajas")!=null) this.mapeo.setCajas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cajas").replace(".", "")));
		 if (r_hash.get("factor")!=null) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor").replace(".", "")));
		 if (r_hash.get("unidades")!=null) this.mapeo.setUnidades(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades").replace(".", "")));
		 if (r_hash.get("status")!=null) this.mapeo.setStatus(RutinasNumericas.formatearIntegerDeESP(r_hash.get("status").replace(".", "")));
		 if (r_hash.get("orden")!=null) this.mapeo.setOrden(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden")));
		 
		 try
		 {
			 if (r_hash.get("fecha")!=null)
			 {
				 this.mapeo.setFecha(RutinasFechas.convertirADate(r_hash.get("fecha")));
				 if (this.mapeo.getTipo().toUpperCase().contentEquals("LINEA"))
				 {
					 this.mapeo.setDia(RutinasFechas.obtenerDiaDeLaSemanaLetra(RutinasFechas.convertirADate(r_hash.get("fecha"))).toUpperCase() + " " + RutinasFechas.diaFecha(RutinasFechas.convertirADate(r_hash.get("fecha"))) + "/" + RutinasFechas.mesFecha(RutinasFechas.convertirADate(r_hash.get("fecha"))));
				 }
			 }
		 }
		 catch (Exception ex)
		 {
			 Notificaciones.getInstance().mensajeError("Programacion: fecha erronea. " + ex.getMessage());
		 }
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoProgramacion r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 
		 if (r_mapeo.getFecha()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFecha().toString());
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }

		 this.hash.put("id", this.mapeo.getIdProgramacion().toString());		 
		 this.hash.put("ejercicio", this.mapeo.getEjercicio().toString());
		 this.hash.put("semana", "Semana " + this.mapeo.getSemana());
		 this.hash.put("dia", this.mapeo.getDia());
		 this.hash.put("tipoOperacion", this.mapeo.getTipo());
		 this.hash.put("vino", this.mapeo.getVino());
		 this.hash.put("anada", this.mapeo.getAnada());
		 this.hash.put("lotePT", this.mapeo.getLote_pt());
		 this.hash.put("lote_jaulon_1", this.mapeo.getLote_jaulon_1());
		 this.hash.put("lote", this.mapeo.getLote());
		 this.hash.put("articulo", this.mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 this.hash.put("frontales", this.mapeo.getFrontales());
		 this.hash.put("contras", this.mapeo.getContras());
		 this.hash.put("capsulas", this.mapeo.getCapsulas());
		 this.hash.put("tapones", this.mapeo.getTapones());
		 this.hash.put("caja", this.mapeo.getCaja());
		 this.hash.put("estado", this.mapeo.getEstado());
		 this.hash.put("nuevo", this.mapeo.getNuevo());
		 
		 this.hash.put("velocidad", this.mapeo.getVelocidad().toString());
		 this.hash.put("cantidad", this.mapeo.getCantidad().toString());
		 this.hash.put("cajas", this.mapeo.getCajas().toString());
		 this.hash.put("factor", this.mapeo.getFactor().toString());
		 this.hash.put("unidades", this.mapeo.getUnidades().toString());
		 this.hash.put("status", this.mapeo.getStatus().toString());
		 this.hash.put("orden", this.mapeo.getOrden().toString());
		 
		 
		 return hash;		 
	 }
}