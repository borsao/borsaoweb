package borsao.e_borsao.Modulos.PRODUCCION.Programacion.view;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.ProgramacionGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasBotella extends Window
{
	private ProgramacionGrid App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbTipoEtiqueta = null;
	private ComboBox cmbFormatoEtiqueta = null;
	private ComboBox cmbPrinter = null;
	private TextField txtArticulo = null;
	private TextField txtDescripcion= null;
	private TextField txtCuantas= null;
	/*
	 * Valores devueltos
	 */
	private Integer cuantas = null;
	
	public PeticionEtiquetasBotella(GridPropio r_App, MapeoProgramacion r_mapeo, String r_titulo)
	{
		this.App = (ProgramacionGrid) r_App;
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setValue(r_mapeo.getArticulo());
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setRequired(true);
				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setValue(r_mapeo.getDescripcion());
				this.txtDescripcion.setWidth("400px");
				this.txtDescripcion.setRequired(true);
				fila1.addComponent(txtArticulo);
				fila1.addComponent(txtDescripcion);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);

				this.cmbTipoEtiqueta = new ComboBox("Selecion Tipo");
				this.cmbTipoEtiqueta.addItem("Frontales");
				this.cmbTipoEtiqueta.addItem("Contras");
				this.cmbTipoEtiqueta.addItem("Tirillas");
				
				this.cmbTipoEtiqueta.setInvalidAllowed(false);
				this.cmbTipoEtiqueta.setNewItemsAllowed(false);
				this.cmbTipoEtiqueta.setNullSelectionAllowed(false);
				this.cmbTipoEtiqueta.setRequired(true);
				this.cmbTipoEtiqueta.setWidth("200px");
				
				this.cmbFormatoEtiqueta = new ComboBox("Selecion Formato");				
				this.cmbTipoEtiqueta.setInvalidAllowed(false);
				this.cmbTipoEtiqueta.setNewItemsAllowed(false);
				this.cmbTipoEtiqueta.setNullSelectionAllowed(false);
				this.cmbTipoEtiqueta.setRequired(true);
				this.cmbTipoEtiqueta.setWidth("300px");
				
				fila2.addComponent(cmbTipoEtiqueta);
				fila2.addComponent(cmbFormatoEtiqueta);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.txtCuantas =new TextField("Nº Etiquetas");
				this.txtCuantas.setWidth("200px");
				this.txtCuantas.setRequired(true);
				
				fila3.addComponent(txtCuantas);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				fila4.addComponent(cmbPrinter);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
	}

	private void habilitarCampos()
	{
	
		this.txtArticulo.setEnabled(true);
		this.txtDescripcion.setEnabled(true);
		this.txtCuantas.setEnabled(true);
		
		this.cmbTipoEtiqueta.setEnabled(true);
		this.cmbPrinter.setEnabled(true);
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					cuantas=new Integer(txtCuantas.getValue());
					
					App.impresionEtiqueta("/" + cmbTipoEtiqueta.getValue().toString() + "/" + cmbFormatoEtiqueta.getValue().toString(), cuantas, cmbPrinter.getValue().toString());
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
		this.cmbTipoEtiqueta.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (cmbTipoEtiqueta.getValue()!=null && cmbTipoEtiqueta.getValue().toString().length()>0) 
				{
					cargarFormatos(cmbTipoEtiqueta.getValue().toString());
				}
			}
		}); 
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.cmbTipoEtiqueta.getValue()==null || this.cmbTipoEtiqueta.getValue().toString().length()==0)
		{
			devolver = false;
			cmbTipoEtiqueta.focus();
		}
		else if (this.cmbPrinter.getValue()==null || this.cmbPrinter.getValue().toString().length()==0)
		{
			devolver = false;
			cmbPrinter.focus();
		}
		else if (this.txtCuantas.getValue()==null || this.txtCuantas.getValue().length()==0) 
		{
			devolver =false;			
			txtCuantas.focus();
		}
		return devolver;
	}

	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private void cargarFormatos(String r_carpeta)
	{
		this.cmbFormatoEtiqueta.removeAllItems();
        File folderFile = new File(VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/germark/" + r_carpeta);
        if ((folderFile.exists())) {
            File[] files = folderFile.listFiles(new FilenameFilter() {
                public boolean accept(File folderFile, String name) {
                    return name.toLowerCase().startsWith(txtArticulo.getValue().toString().trim());
                }});
            
            for (File file : files) {
                boolean isFolder = file.isDirectory();
                if (!isFolder) this.cmbFormatoEtiqueta.addItem(file.getName());
            }
        }
	}
}