package borsao.e_borsao.Modulos.PRODUCCION.Programacion.server;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasEmbotellado.server.consultaEtiquetasEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.Modulos.PRODUCCION.produccionDiariaSGA.modelo.MapeoProduccionDiariaSGA;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaProgramacionServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaProgramacionServer instance;
	
	public consultaProgramacionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProgramacionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProgramacionServer(r_usuario);			
		}
		return instance;
	}
	
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
	
	public Integer obtenerVelocidadArticulo(String r_articulo)
	{
		Integer rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerVelocidadArticulo(r_articulo);
		return rdo;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobal(MapeoProgramacion r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.lote_pt pro_lpt,");
		cadenaSQL.append(" prd_programacion.lote_jaulon_1 pro_lja,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}

				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVelocidad()!=null && r_mapeo.getVelocidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" velocidad = " + r_mapeo.getVelocidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N','L') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setFecha(RutinasFechas.conversion(rsOpcion.getDate("pro_fec")));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setAnada(rsOpcion.getString("pro_ana"));
				
//				if (rsOpcion.getString("pro_ope").toUpperCase().contentEquals("ETIQUETADO"))
//				{
//					if (rsOpcion.getString("pro_art")!=null && rsOpcion.getString("pro_art").length()>0 && rsOpcion.getString("pro_lja")!=null && rsOpcion.getString("pro_lja").length()>0)
//						mapeoProgramacion.setLote_pt(rsOpcion.getString("pro_lja").substring(0, (rsOpcion.getString("pro_lja").length()-3))+ rsOpcion.getString("pro_art").substring(rsOpcion.getString("pro_art").length()-3, rsOpcion.getString("pro_art").length()));
//				}
//					
//				else
					mapeoProgramacion.setLote_pt(rsOpcion.getString("pro_lpt"));
				
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setLote_jaulon_1(rsOpcion.getString("pro_lja"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				String of = mapeoProgramacion.getEjercicio().toString() + RutinasCadenas.formatCerosIzquierda(mapeoProgramacion.getSemana(), 2) + RutinasCadenas.formatCerosIzquierda(mapeoProgramacion.getOrden().toString(), 3); 
				mapeoProgramacion.setOrdenFabricacion(new Long(of));
				
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public ArrayList<MapeoControlesProcesoProductivo> datosProgramacionGlobalArticulos(MapeoControlesProcesoProductivo r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlesProcesoProductivo> vector = null;
		StringBuffer cadenaWhere =null;
		String lote ="";
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
			}

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}

			if (cadenaWhere.length()>0) cadenaSQL.append(" and "); else cadenaSQL.append(" where ");
			cadenaSQL.append(" ( unidades <> 0  or prd_programacion.dia <> '' ) ");
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoControlesProcesoProductivo>();
			
			while(rsOpcion.next())
			{
				MapeoControlesProcesoProductivo mapeoProgramacion = new MapeoControlesProcesoProductivo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				
					
				if (!rsOpcion.getString("pro_ope").toUpperCase().contains("LINEA"))
				{
					mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				
					if (rsOpcion.getString("pro_lot")!=null && rsOpcion.getString("pro_lot").length()>0)
						lote = rsOpcion.getString("pro_lot");
					mapeoProgramacion.setLote(lote);
					mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
					mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
					mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				}					
				else mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public Integer cuantosProgramacionGlobal(MapeoProgramacion r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		Integer cuantos = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(prd_programacion.idprd_programacion) pro_num ");
		cadenaSQL.append(" from prd_programacion ");
		try
		{
			cadenaWhere = new StringBuffer();
			cadenaWhere.append(" prd_programacion.tipoOperacion <> 'RETRABAJO' ");

			if (r_mapeo!=null)
			{
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N','L') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			cuantos = 0;
			while(rsOpcion.next())
			{
				cuantos = rsOpcion.getInt("pro_num");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cuantos;
	}
	public MapeoProgramacion datosProgramacionGlobal(Integer r_id)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		MapeoProgramacion mapeoProgramacion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_id!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_id!=null && r_id.toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_id + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setFecha(RutinasFechas.conversion(rsOpcion.getDate("pro_fec")));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
					
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setAnada(rsOpcion.getString("pro_ana"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return mapeoProgramacion;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobalNuevo(MapeoProgramacion r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getSemana()==null) cadenaWhere.append(" ejercicio >= " + r_mapeo.getEjercicio() + " "); else cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVelocidad()!=null && r_mapeo.getVelocidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" velocidad = " + r_mapeo.getVelocidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N','L') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setFecha(RutinasFechas.conversion(rsOpcion.getDate("pro_fec")));
				String dia = "";
				if (rsOpcion.getString("pro_dia")==null || (rsOpcion.getString("pro_dia")!=null && rsOpcion.getString("pro_dia").length()==0))
				{
					dia = this.obtenerDia(rsOpcion.getInt("pro_ej"), rsOpcion.getString("pro_sem"), rsOpcion.getInt("pro_ord"));
				}

//				dia = rsOpcion.getString("pro_dia");
				mapeoProgramacion.setDia(dia);
					
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setAnada(rsOpcion.getString("pro_ana"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public ArrayList<MapeoProgramacion> datosProgramacionTotal(Integer r_ejercicio, String r_semana)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where ((prd_programacion.ejercicio = " + r_ejercicio + " and semana >= " + new Integer(r_semana) + ")  ") ;
		cadenaSQL.append(" or (prd_programacion.ejercicio > " + r_ejercicio + "))  ") ;
		cadenaSQL.append(" and estado in ('A','N','L')  ") ;
		
		try
		{
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setFecha(RutinasFechas.conversion(rsOpcion.getDate("pro_fec")));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setAnada(rsOpcion.getString("pro_ana"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobal(ArrayList<String> r_articulos, String r_estado, boolean r_etiquetados, Integer r_ejercicio)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		boolean jaulon = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion.vino pro_vin,");
		cadenaSQL.append(" prd_programacion.anada pro_ana,");
		cadenaSQL.append(" prd_programacion.lote pro_lot,");
		cadenaSQL.append(" prd_programacion.orden pro_ord,");
		cadenaSQL.append(" prd_programacion.articulo pro_art,");
		cadenaSQL.append(" prd_programacion.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion.factor pro_fac,");
		cadenaSQL.append(" prd_programacion.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion.contras pro_con,");
		cadenaSQL.append(" prd_programacion.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion.estado pro_es, ");
		cadenaSQL.append(" prd_programacion.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_estado==null || r_estado.contentEquals("X"))
				cadenaSQL.append(" WHERE ejercicio >= " + r_ejercicio);
			else
				cadenaSQL.append(" WHERE ejercicio = " + r_ejercicio);
			
     		String articulos="";
     		for (int i=0; i<r_articulos.size();i++)
     		{
//     			if (r_articulos.get(i).trim().endsWith("-1")) jaulon=true;
     			if (articulos!="") articulos = articulos + ",";
     			articulos =articulos + "'" + r_articulos.get(i).trim().substring(0, 7) + "'";
     		}
     		if (articulos!="") cadenaSQL.append(" and articulo in (" + articulos + ") ");

     		if (r_etiquetados || jaulon) cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO','ETIQUETADO') ");
     		else cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' ");
     		
     		if (r_estado==null || r_estado.contentEquals("X"))
     			cadenaSQL.append(" and estado in ('A','N','L') ");
     		else if (!r_estado.contentEquals("X"))
     			cadenaSQL.append(" and estado = '" + r_estado + "'");
     		
			cadenaSQL.append(" order by ejercicio, semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setAnada(rsOpcion.getString("pro_ana"));
				mapeoProgramacion.setFecha(RutinasFechas.conversion(rsOpcion.getDate("pro_fec")));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				if (rsOpcion.getString("pro_dia")==null || (rsOpcion.getString("pro_dia")!=null && rsOpcion.getString("pro_dia").trim().length()==0))
				{
					mapeoProgramacion.setDia(this.buscoDiaSemana(con, mapeoProgramacion));
				}
				else
				{
					mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				}
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	private String buscoDiaSemana(Connection r_con, MapeoProgramacion r_mapeo)
	{
		Statement css = null;
		ResultSet rsOpcions = null;

		String diaSemana = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion.semana pro_sem,");
		cadenaSQL.append(" prd_programacion.dia pro_dia,");
		cadenaSQL.append(" prd_programacion.fecha pro_fec,");
		cadenaSQL.append(" prd_programacion.orden pro_ord ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			
     		cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
     		cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
     		cadenaSQL.append(" and orden <= " + r_mapeo.getOrden());
     		cadenaSQL.append(" and length(dia) > 0  ");
			cadenaSQL.append(" order by ejercicio, semana, orden desc");
			
			css = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcions= css.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcions.next())
			{
				diaSemana = rsOpcions.getString("pro_dia");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcions!= null)
				{
					rsOpcions.close();
					rsOpcions=null;
				}
				if (css !=null)
				{
					css.close();
					css=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return diaSemana;
	}

	public boolean buscoEmbotelladosDiaSemana(MapeoProgramacion r_mapeo)
	{
		Connection con = null;
		Statement css = null;
		ResultSet rsOpcions = null;
		Integer orden2 = null;
		Integer orden = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		try
		{
		
			cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
			cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
			cadenaSQL.append(" prd_programacion.semana pro_sem,");
			cadenaSQL.append(" prd_programacion.dia pro_dia,");
			cadenaSQL.append(" prd_programacion.orden pro_ord ");
			cadenaSQL.append(" from prd_programacion ");
			cadenaSQL.append(" where idprd_programacion = " + r_mapeo.getIdProgramacion());

			con= this.conManager.establecerConexionInd();
			css = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcions= css.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcions.next())
			{
				orden = rsOpcions.getInt("pro_ord");
				break;
			}

			if (orden!=null)
			{
				cadenaSQL = new StringBuffer();
	
				cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id,");
				cadenaSQL.append(" prd_programacion.ejercicio pro_ej,");
				cadenaSQL.append(" prd_programacion.semana pro_sem,");
				cadenaSQL.append(" prd_programacion.dia pro_dia,");
				cadenaSQL.append(" prd_programacion.orden pro_ord ");
				cadenaSQL.append(" from prd_programacion ");
				cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
				cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
				cadenaSQL.append(" and orden > " + orden);
				cadenaSQL.append(" and length(dia) > 0  ");
				cadenaSQL.append(" order by ejercicio, semana, orden asc");
	
				rsOpcions.close();
				rsOpcions= css.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcions.next())
				{
					orden2 = rsOpcions.getInt("pro_ord");
					break;
				}
				
				if (orden2!=null)
				{
					cadenaSQL = new StringBuffer();
					
					cadenaSQL.append(" SELECT prd_programacion.tipoOperacion pro_op");
					cadenaSQL.append(" from prd_programacion ");
					cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
					cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
					cadenaSQL.append(" and orden between " + orden + " and " + orden2);

					rsOpcions.close();
					rsOpcions= css.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		
					while(rsOpcions.next())
					{
						if (rsOpcions.getString("pro_op").toUpperCase().contentEquals("EMBOTELLADO")) return true;
					}
				}
				else
				{
					cadenaSQL = new StringBuffer();
					
					cadenaSQL.append(" SELECT prd_programacion.tipoOperacion pro_op");
					cadenaSQL.append(" from prd_programacion ");
					cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
					cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
					cadenaSQL.append(" and orden > " + orden);

					rsOpcions.close();
					rsOpcions= css.executeQuery(cadenaSQL.toString());
					Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		
					while(rsOpcions.next())
					{
						if (rsOpcions.getString("pro_op").toUpperCase().contentEquals("EMBOTELLADO")) return true;
					}
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcions!= null)
				{
					rsOpcions.close();
					rsOpcions=null;
				}
				if (css !=null)
				{
					css.close();
					css=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	public Double obtenerSumaProgramacion(String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;
		Double cantidad = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana = " + new Integer(r_semana) + " ");

		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
					cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			if (con==null)
				con= this.conManager.establecerConexionInd();
			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cantidad;		
	}
	
	public Double obtenerSumaProgramacion(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres, String r_tipoLinea)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where ((ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana >= " + new Integer(r_semana) + ") ");
		cadenaSQL.append(" or (ejercicio >= '" + r_ejercicio + "')) ");
		cadenaSQL.append(" and estado in ('A','N','L') ");
		if (r_tipoLinea=="EMBOTELLADO")	cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' " );
		if (r_tipoLinea=="ETIQUETADO") cadenaSQL.append(" and tipoOperacion = 'ETIQUETADO' " );
		
		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cantidad;		
	}

	public Integer obtenerSumaProgramacionPallet(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres)
	{
		Integer cantidad = null;
		Double calculo = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT articulo pro_art, prd_programacion.unidades pro_ud ");		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where ((ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana >= " + new Integer(r_semana) + ") ");
		cadenaSQL.append(" or (ejercicio >= '" + r_ejercicio + "')) ");
		cadenaSQL.append(" and estado in ('A','N','L') ");
		
		try
		{
			cantidad = 0;
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
						
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
				}
				if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			
			cs = r_con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			String sufijo = null;
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_art").contentEquals("0103004")|| rsOpcion.getString("pro_art").contentEquals("0103104"))
					sufijo = "28";
				else if (rsOpcion.getString("pro_art").contentEquals("0102006") || rsOpcion.getString("pro_art").contentEquals("0102091") 
						|| rsOpcion.getString("pro_art").contentEquals("0103005") || rsOpcion.getString("pro_art").contentEquals("0103012")
						|| rsOpcion.getString("pro_art").contentEquals("0103105") || rsOpcion.getString("pro_art").contentEquals("0103112"))
					sufijo="38";
				else
					sufijo ="18";
					
				String paletizacionArticulo=cas.obtenerCantidadEan(r_con, rsOpcion.getString("pro_art"), sufijo);
				calculo = new Double(0);
				
				if (paletizacionArticulo!=null)
				{
					Double paletizacion = new Double(paletizacionArticulo);				
					calculo = calculo + Math.ceil(rsOpcion.getInt("pro_ud")/paletizacion);
					cantidad = cantidad + calculo.intValue();
				}
				else
					calculo=new Double(0);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cantidad.intValue();		
	}
	
	public Double obtenerSumaProgramacionAlmacen(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres, String r_tipoLinea)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and estado in ('A','N','L') ");
		cadenaSQL.append(" and semana = " + new Integer(r_semana) + " ");
		if (r_tipoLinea=="EMBOTELLADO")	cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' " );
		if (r_tipoLinea=="ETIQUETADO") cadenaSQL.append(" and tipoOperacion = 'ETIQUETADO' " );
		
		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cantidad;		
	}
	
	public String generarInforme(MapeoProgramacion r_mapeo, String r_lote, String r_tipo, Integer r_cajas, String r_anada, String r_cantidad, boolean r_paletCompleto)
	{
		boolean generado = false;
		String informe = "";
		
		consultaEtiquetasEmbotelladoServer ces = consultaEtiquetasEmbotelladoServer.getInstance(CurrentUser.get());
		generado = ces.generarDatosEtiquetas(r_mapeo, r_lote, r_tipo, r_cajas, r_anada,r_cantidad, r_paletCompleto);
		
		if (generado) informe = ces.generarInforme(r_mapeo.getIdProgramacion());
		
		
		return informe;
		
	}

	public String imprimirProgramacion(String r_ejercicio, String r_semana, String r_formato)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio);
		libImpresion.setCodigo2Txt(r_semana);
		libImpresion.setArchivoDefinitivo("/programacion" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		if (r_formato.equals("Laboratorio"))
		{
			libImpresion.setArchivoPlantilla("ListadoProgramacion.jrxml");
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzul.jpg");
		}
		else
		{
			libImpresion.setArchivoPlantilla("ListadoProgramacionNormal.jrxml");
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
		}
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("programacion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public String imprimirProgramacion(String r_valores)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_valores);
		libImpresion.setArchivoDefinitivo("/programacion" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("ListadoProgramacionSelector.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("programacion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion.idprd_programacion) pro_sig ");
     	cadenaSQL.append(" FROM prd_programacion ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return 1;
	}


	public Integer obtenerOrden(Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion.orden) pro_ord ");
     	cadenaSQL.append(" FROM prd_programacion ");
     	cadenaSQL.append(" WHERE ejercicio =  " + r_ejercicio);
     	cadenaSQL.append(" AND semana = " + new Integer(r_semana) );
     	
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_ord") + 10 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return 10;
	}

	public String guardarNuevo(MapeoProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			Integer permisos = new Integer(this.comprobarAccesos());		    
			if (permisos<99 && permisos>70 ) 
				r_mapeo.setEstado("P");
			else 
				if (r_mapeo.getNuevo()!=null && r_mapeo.getNuevo().equals("S")) r_mapeo.setEstado("N");
				else
				r_mapeo.setEstado("A");
			
			cadenaSQL.append(" INSERT INTO prd_programacion( ");
			cadenaSQL.append(" prd_programacion.idprd_programacion,");
			cadenaSQL.append(" prd_programacion.ejercicio ,");
			cadenaSQL.append(" prd_programacion.semana ,");
			cadenaSQL.append(" prd_programacion.dia ,");
			cadenaSQL.append(" prd_programacion.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion.cantidad ,");
			cadenaSQL.append(" prd_programacion.vino ,");
			cadenaSQL.append(" prd_programacion.lote ,");
			cadenaSQL.append(" prd_programacion.orden ,");
			cadenaSQL.append(" prd_programacion.articulo ,");
			cadenaSQL.append(" prd_programacion.descripcion ,");
			cadenaSQL.append(" prd_programacion.cajas ,");
			cadenaSQL.append(" prd_programacion.factor ,");
			cadenaSQL.append(" prd_programacion.unidades ,");
			cadenaSQL.append(" prd_programacion.observaciones ,");
			cadenaSQL.append(" prd_programacion.frontales ,");
			cadenaSQL.append(" prd_programacion.contras ,");
			cadenaSQL.append(" prd_programacion.capsulas ,");
			cadenaSQL.append(" prd_programacion.tapon ,");
			cadenaSQL.append(" prd_programacion.caja, ");
			cadenaSQL.append(" prd_programacion.estado, ");
			cadenaSQL.append(" prd_programacion.status, ");
			cadenaSQL.append(" prd_programacion.velocidad, ");
			cadenaSQL.append(" prd_programacion.fecha, ");
			cadenaSQL.append(" prd_programacion.lote_pt, ");
			cadenaSQL.append(" prd_programacion.lote_jaulon_1, ");
			cadenaSQL.append(" prd_programacion.anada ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    

		    preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_mapeo.getSemana()));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0 && r_mapeo.getTipo().toUpperCase().contentEquals("LINEA"))
		    {
		    	preparedStatement.setString(4, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getContras()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getContras().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getCapsulas()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getCapsulas().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getTapones()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getTapones().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(21, null);
		    }
		    
		    preparedStatement.setInt(22, 0);
		    if (r_mapeo.getVelocidad()!=null)
		    {
		    	preparedStatement.setInt(23, r_mapeo.getVelocidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(23, 0);
		    }
		    String fecha = "";
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(24, RutinasFechas.convertirAFechaMysql(fecha));
		    preparedStatement.setString(25, this.lotePT(r_mapeo));
		    if (r_mapeo.getLote_jaulon_1()!=null)
		    {
		    	preparedStatement.setString(26, r_mapeo.getLote_jaulon_1());
		    }
		    else
		    {
		    	preparedStatement.setString(26, null);
		    }
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(27, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(27, null);
		    }

		    
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	private String lotePT(MapeoProgramacion r_mapeo)
	{
		String loteCalculado=null;
		/*
		 * Lo usaremos para calcular el lote PT de cada linea y almacenarlo en la tabla
		 */
		
		if (r_mapeo.getTipo().toUpperCase().equals("EMBOTELLADO"))
		{
			if ((r_mapeo.getLote()==null || r_mapeo.getLote().length()==0))
			{
				String lote = null;
				lote = this.buscarLote(r_mapeo);
				if (lote!=null)						
				{
					loteCalculado = lote.substring(2) + r_mapeo.getArticulo().substring(7-(10-lote.substring(2).length()));
				}
				else
				{
					loteCalculado = RutinasFechas.añoActualYY() + RutinasFechas.mesActualMM() + RutinasFechas.diaActualDD() + r_mapeo.getArticulo().substring(3);
				}
			}
			else
			{
				loteCalculado = r_mapeo.getLote().substring(2) + r_mapeo.getArticulo().substring(7-(10-r_mapeo.getLote().substring(2).length()));
			}
		}
		else if (r_mapeo.getTipo().toUpperCase().equals("ETIQUETADO"))
		{
			if ((r_mapeo.getLote_jaulon_1()!=null && r_mapeo.getLote_jaulon_1().length()>0))
			{
				loteCalculado = (r_mapeo.getLote_jaulon_1().substring(0, (r_mapeo.getLote_jaulon_1().length()-3))+ r_mapeo.getArticulo().substring(r_mapeo.getArticulo().length()-3, r_mapeo.getArticulo().length()));
			}
		}
		return loteCalculado;
	}
	
	public String programarNuevoRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana, String r_sql)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	

		String rdo = null;
		StringBuffer cadenaSQL = new StringBuffer();
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());

		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion( ");
			cadenaSQL.append(" prd_programacion.idprd_programacion,");
			cadenaSQL.append(" prd_programacion.ejercicio ,");
			cadenaSQL.append(" prd_programacion.semana ,");
			cadenaSQL.append(" prd_programacion.dia ,");
			cadenaSQL.append(" prd_programacion.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion.cantidad ,");
			cadenaSQL.append(" prd_programacion.vino ,");
			cadenaSQL.append(" prd_programacion.lote ,");
			cadenaSQL.append(" prd_programacion.orden ,");
			cadenaSQL.append(" prd_programacion.articulo ,");
			cadenaSQL.append(" prd_programacion.descripcion ,");
			cadenaSQL.append(" prd_programacion.cajas ,");
			cadenaSQL.append(" prd_programacion.factor ,");
			cadenaSQL.append(" prd_programacion.unidades ,");
			cadenaSQL.append(" prd_programacion.observaciones ,");
			cadenaSQL.append(" prd_programacion.frontales ,");
			cadenaSQL.append(" prd_programacion.contras ,");
			cadenaSQL.append(" prd_programacion.capsulas ,");
			cadenaSQL.append(" prd_programacion.tapon ,");
			cadenaSQL.append(" prd_programacion.caja, ");
			cadenaSQL.append(" prd_programacion.estado, ");
			cadenaSQL.append(" prd_programacion.status, ");
			cadenaSQL.append(" prd_programacion.velocidad, ");
			cadenaSQL.append(" prd_programacion.fecha, ");
			cadenaSQL.append(" prd_programacion.lote_pt, ");
			cadenaSQL.append(" prd_programacion.lote_jaulon_1, ");
			cadenaSQL.append(" prd_programacion.anada ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer idNuevo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idNuevo);		    
		    if (r_ejercicio!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_ejercicio));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_semana!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_semana));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    //dia
		    preparedStatement.setString(4, null);

		    //tipo y cantidad 
		    preparedStatement.setString(5, "RETRABAJO");
		    preparedStatement.setInt(6, 0);
		    //VINO, LOTE
		    preparedStatement.setString(7, null);
		    preparedStatement.setString(8, null);
		    
		    preparedStatement.setInt(9, this.obtenerOrden(new Integer(r_ejercicio), r_semana));
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    //Frontales,Contras, Capsulas, Tapones, Cajas, Estado
		    preparedStatement.setString(16, "SI");
		    preparedStatement.setString(17, "SI");
		    preparedStatement.setString(18, "SI");
		    preparedStatement.setString(19, "SI");
		    preparedStatement.setString(20, "SI");
		    preparedStatement.setString(21, "A");
		    
		    preparedStatement.setInt(22, 0);
		    preparedStatement.setInt(23, 0);
		    String fecha = "";
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(24, RutinasFechas.convertirAFechaMysql(fecha));
		    preparedStatement.setString(25, null);
		    preparedStatement.setString(26, null);
		    preparedStatement.setString(27, null);

	        preparedStatement.executeUpdate();
	        r_mapeo.setIdProgramacion(idNuevo);
	        rdo = crs.asociarProgramacion(r_mapeo);
	        
        	if (r_sql!=null && r_sql.length()>0 )
        	{
        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
        		cps.actualizarPedidoProgramado(idNuevo, r_sql);
        	}

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdo;
		
	}
	
	public String programarDesdeNecesidades(MapeoSituacionEmbotellado r_mapeo,String r_ejercicio,String r_semana, String r_sql, Integer r_velocidad)
	{
		String rdo = null;
		Connection con = null;	
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion( ");
			cadenaSQL.append(" prd_programacion.idprd_programacion,");
			cadenaSQL.append(" prd_programacion.ejercicio ,");
			cadenaSQL.append(" prd_programacion.semana ,");
			cadenaSQL.append(" prd_programacion.dia ,");
			cadenaSQL.append(" prd_programacion.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion.cantidad ,");
			cadenaSQL.append(" prd_programacion.vino ,");
			cadenaSQL.append(" prd_programacion.lote ,");
			cadenaSQL.append(" prd_programacion.orden ,");
			cadenaSQL.append(" prd_programacion.articulo ,");
			cadenaSQL.append(" prd_programacion.descripcion ,");
			cadenaSQL.append(" prd_programacion.cajas ,");
			cadenaSQL.append(" prd_programacion.factor ,");
			cadenaSQL.append(" prd_programacion.unidades ,");
			cadenaSQL.append(" prd_programacion.observaciones ,");
			cadenaSQL.append(" prd_programacion.frontales ,");
			cadenaSQL.append(" prd_programacion.contras ,");
			cadenaSQL.append(" prd_programacion.capsulas ,");
			cadenaSQL.append(" prd_programacion.tapon ,");
			cadenaSQL.append(" prd_programacion.caja, ");
			cadenaSQL.append(" prd_programacion.estado, ");
			cadenaSQL.append(" prd_programacion.status, ");
			cadenaSQL.append(" prd_programacion.velocidad ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer idNuevo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idNuevo);		    
		    if (r_ejercicio!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_ejercicio));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_semana!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_semana));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    //dia
		    preparedStatement.setString(4, null);

		    //tipo y cantidad
		    
		    preparedStatement.setString(5, r_mapeo.getOperacion());
		    
		    preparedStatement.setInt(6, 0);
		    //VINO, LOTE
		    preparedStatement.setString(7, null);
		    preparedStatement.setString(8, null);
		    
		    preparedStatement.setInt(9, this.obtenerOrden(new Integer(r_ejercicio), r_semana)+10);
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getStock_real()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getStock_real());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    //observaciones
		    
		    preparedStatement.setString(15, r_mapeo.getObservaciones());
		    
		    //Frontales,Contras, Capsulas, Tapones, Cajas, Estado
		    preparedStatement.setString(16, "SI");
		    preparedStatement.setString(17, "SI");
		    preparedStatement.setString(18, "SI");
		    preparedStatement.setString(19, "SI");
		    preparedStatement.setString(20, "SI");
		    preparedStatement.setString(21, "A");

		    
		    preparedStatement.setInt(22, 0);
		    preparedStatement.setInt(23, r_velocidad);
		    
	        preparedStatement.executeUpdate();
	        
	        
	        if (r_mapeo.getPdte_servir()!=null && r_mapeo.getPdte_servir()!=0)
	        {
	        	/*
	        	 * Actualizamos los pedidos de greensys de este articulo al haberlos programado
	        	 */
	        	if (r_sql!=null && r_sql.length()>0 )
	        	{
	        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
	        		cps.actualizarPedidoProgramado(idNuevo, r_sql);
	        	}
	        }
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdo;
		
	}
	
	public String asignarRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana)
	{
		String rdo = null;
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());

		try
		{
	        rdo = crs.asociarProgramacion(r_mapeo);
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        

		return rdo;
		
	}
	public String guardarCambios(MapeoProgramacion r_mapeo)
	{
		String rdo = null;
		Connection con = null;	
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion set ");
			cadenaSQL.append(" prd_programacion.ejercicio =?,");
			cadenaSQL.append(" prd_programacion.semana =?,");
			cadenaSQL.append(" prd_programacion.dia =?,");
			cadenaSQL.append(" prd_programacion.tipoOperacion =?,");
			cadenaSQL.append(" prd_programacion.cantidad =?,");
			cadenaSQL.append(" prd_programacion.vino =?,");
			cadenaSQL.append(" prd_programacion.lote =?,");
			cadenaSQL.append(" prd_programacion.orden =?,");
			cadenaSQL.append(" prd_programacion.articulo =?,");
			cadenaSQL.append(" prd_programacion.descripcion =?,");
			cadenaSQL.append(" prd_programacion.cajas =?,");
			cadenaSQL.append(" prd_programacion.factor =?,");
			cadenaSQL.append(" prd_programacion.unidades =?,");
			cadenaSQL.append(" prd_programacion.observaciones =?,");
			cadenaSQL.append(" prd_programacion.frontales =?,");
			cadenaSQL.append(" prd_programacion.contras =?,");
			cadenaSQL.append(" prd_programacion.capsulas =?,");
			cadenaSQL.append(" prd_programacion.tapon =?,");
			cadenaSQL.append(" prd_programacion.caja = ?, ");
			cadenaSQL.append(" prd_programacion.estado = ?, ");
			cadenaSQL.append(" prd_programacion.status = ?, ");
			cadenaSQL.append(" prd_programacion.velocidad = ?, ");
			cadenaSQL.append(" prd_programacion.fecha = ?, ");
			cadenaSQL.append(" prd_programacion.lote_pt = ?, ");
			cadenaSQL.append(" prd_programacion.lote_jaulon_1 = ?, ");
			cadenaSQL.append(" prd_programacion.anada =? ");
			cadenaSQL.append(" where prd_programacion.idprd_programacion = ? ");
			
			con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_mapeo.getSemana()));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0 && r_mapeo.getTipo().toUpperCase().contentEquals("LINEA"))
		    {
		    	preparedStatement.setString(3, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLote()!=null && !r_mapeo.getTipo().toUpperCase().contentEquals("ETIQUETADO"))
		    {
		    	preparedStatement.setString(7, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getContras()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getContras().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getCapsulas()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getCapsulas().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getTapones()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getTapones().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    if (r_mapeo.getStatus()!=null)
		    {
		    	preparedStatement.setInt(21, r_mapeo.getStatus()+1);
		    }
		    else
		    {
		    	preparedStatement.setInt(21, 0);
		    }
		    if (r_mapeo.getVelocidad()!=null)
		    {
		    	preparedStatement.setInt(22, r_mapeo.getVelocidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(22, 0);
		    }
		    String fecha =null;
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(23, RutinasFechas.convertirAFechaMysql(fecha));
		    preparedStatement.setString(24, this.lotePT(r_mapeo));
		    
		    if (r_mapeo.getLote_jaulon_1()!=null)
		    {
		    	preparedStatement.setString(25, r_mapeo.getLote_jaulon_1());
		    }
		    else
		    {
		    	preparedStatement.setString(25, null);
		    }
		    if (r_mapeo.getAnada()!=null)
		    {
		    	preparedStatement.setString(26, r_mapeo.getAnada());
		    }
		    else
		    {
		    	preparedStatement.setString(26, null);
		    }
		    preparedStatement.setInt(27, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()) || statusActual==null)
    		{
		    	preparedStatement.executeUpdate();
		    	
		    	if (r_mapeo.getEstado().contentEquals("T"))
		    	{
		    		/*
		    		 * TODO: Liberar Pedido
		    		 * Liberar el pedido de greensys
		    		 * poniendo el orden_fabric a nulos
		    		 * 
		    		 */
		    		consultaPedidosVentasServer cpvs = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		    		cpvs.liberarPedidoProgramado(r_mapeo.getIdProgramacion());
		    	}
		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdo;
	}

	public String modificarEstadoProgramacion(Integer r_id, String r_estado, Integer r_status)
	{
		String rdo = null;
		Connection con = null;	
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_programacion set ");
			cadenaSQL.append(" prd_programacion.estado = ? ");
			cadenaSQL.append(" where prd_programacion.idprd_programacion = ? ");
			
			con= this.conManager.establecerConexionInd();	 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			
			if (r_estado!=null)
			{
				preparedStatement.setString(1, r_estado);
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			preparedStatement.setInt(2, r_id);
			

			Integer statusActual = this.obtenerStatus(r_id);
			
			if (statusActual.compareTo(r_status)==0 || statusActual==null)
			{
				preparedStatement.executeUpdate();
				
				if (r_estado.contentEquals("T"))
				{
					if (LecturaProperties.automatizarProduccionSGA)
					{
						this.sincronizarProduccionLPN();
						this.eliminarLpnSincronizado(null);
					}
					/*
					 * TODO: Liberar Pedido
					 * Liberar el pedido de greensys
					 * poniendo el orden_fabric a nulos
					 * 
					 */
					consultaPedidosVentasServer cpvs = consultaPedidosVentasServer.getInstance(CurrentUser.get());
					cpvs.liberarPedidoProgramado(r_id);
				}
				rdo=null;
			}
			else
			{
				rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	    	
		}
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdo;
	}

	public String actualizarProgramacion(MapeoProgramacion r_mapeo, String r_sql)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		Connection con = null;	

		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion set ");
		    cadenaSQL.append(" prd_programacion.cantidad = ?, ");
		    cadenaSQL.append(" prd_programacion.unidades = ?, ");
			cadenaSQL.append(" prd_programacion.observaciones = ? ");
			cadenaSQL.append(" where prd_programacion.idprd_programacion = ? ");
			
			con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setInt(1, r_mapeo.getCantidad());
		    preparedStatement.setInt(2, r_mapeo.getUnidades());
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    preparedStatement.setInt(4, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()))
    		{
		    	preparedStatement.executeUpdate();
	        	if (r_sql!=null && r_sql.length()>0 )
	        	{
	        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
	        		cps.actualizarPedidoProgramado(r_mapeo.getIdProgramacion(), r_sql);
	        	}

		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdo;
	}

	public void eliminar(MapeoProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	

		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_programacion ");            
			cadenaSQL.append(" WHERE prd_programacion.idprd_programacion = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void eliminarLpnSincronizado(String r_sscc)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM a__pd ");
			if (r_sscc != null)
				cadenaSQL.append(" WHERE a__pd.sscc = ?");
			
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_sscc);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	private void eliminarLpnSincronizadoSGA(String r_sscc)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM produccionDiaria ");            
			cadenaSQL.append(" WHERE produccionDiaria.sscc = ?"); 
			con= this.conManager.establecerConexionSQLInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_sscc);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	

	/*
	 * Devuelve el nivel de acceso a programaciones del usuario
	 * Recibe como parametro el nombre del usuario con el que buscamos a su operario equivalente
	 * 
	 * Devuelve:  0 - Sin permisos
	 * 			 10 - Laboratorio
	 * 			 20 - Solo Consulta
	 * 			 30 - Papeles 
	 * 			 40 - Completar
	 * 			 50 - Cambio Observaciones
	 * 			 70 - Cambio observaciones y materia seca
	 * 			 80 - Cambio observaciones y materia seca y Creacion provisional
	 *  			 
	 * 			 99 - Acceso total 
	 */
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Embotelladora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("BIB");
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="0";
				}
			}
		}
		return permisos;
	}
	
	public Integer obtenerStatus(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.status pro_st ");
     	cadenaSQL.append(" FROM prd_programacion ");
     	cadenaSQL.append(" where prd_programacion.idprd_programacion = " + r_id);
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_st");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}
	
	public String buscarLote (MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaWhere =null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.lote pro_lot ");
		cadenaSQL.append(" from prd_programacion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()));
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden < " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden desc ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_lot")!=null && rsOpcion.getString("pro_lot").length()>0)
				{
					return rsOpcion.getString("pro_lot").trim();
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}		
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}

	public boolean comprobarOperacionAnteriorCompletada (MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaWhere =null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.estado pro_est ");
		cadenaSQL.append(" from prd_programacion ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					Integer semAc = new Integer(r_mapeo.getSemana());
					Integer semAn = new Integer(r_mapeo.getSemana())-1;
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana in(" + semAc + "," + semAn + ") ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden < " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" and articulo <> '' ");
			cadenaSQL.append(" and tipoOperacion <> 'RETRABAJO' ");
			cadenaSQL.append(" order by orden desc ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_est")!=null && rsOpcion.getString("pro_est").length()>0 && !rsOpcion.getString("pro_est").contentEquals("T"))
				{
					return false;
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}		
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return true;
	}

	public boolean comprobarProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;

		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean comprobarProgramadoSGA(String r_ejercicio, String r_semana, String r_orden)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;	
		Statement cs = null;

		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id, ");
		cadenaSQL.append(" prd_programacion.tipoOperacion pro_ope, ");
		cadenaSQL.append(" prd_programacion.lote_jaulon_1 pro_lps, ");
		cadenaSQL.append(" prd_programacion.lote_pt pro_lpt ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion.orden = '" + r_orden + "' ");

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_ope").toUpperCase().contentEquals("EMBOTELLADO") && (rsOpcion.getString("pro_lpt")==null || rsOpcion.getString("pro_lpt").length()==0 )) return false;				
				if (rsOpcion.getString("pro_ope").toUpperCase().contentEquals("ETIQUETADO") && (rsOpcion.getString("pro_lpt")==null || rsOpcion.getString("pro_lpt").length()==0  
																							||  rsOpcion.getString("pro_lps")==null || rsOpcion.getString("pro_lps").length()==0 )) return false;				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return true;
	}
	
	public MapeoProgramacion recogerProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		MapeoProgramacion mapeo = null;
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id, ");
		cadenaSQL.append(" prd_programacion.cantidad pro_can, ");
		cadenaSQL.append(" prd_programacion.unidades pro_uds, ");
		cadenaSQL.append(" prd_programacion.observaciones pro_obs, ");
		cadenaSQL.append(" prd_programacion.status pro_st ");
		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			mapeo=new MapeoProgramacion();
			
			while(rsOpcion.next())
			{
				mapeo.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeo.setCantidad(rsOpcion.getInt("pro_can"));
				mapeo.setUnidades(rsOpcion.getInt("pro_uds"));
				mapeo.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeo.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return mapeo;
	}
	
	private String obtenerDia(Integer r_ejercicio, String r_semana, Integer r_orden)
	{
	
		String dia = "";
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.dia pro_dia, ");
		cadenaSQL.append(" prd_programacion.orden pro_ord ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_programacion.semana = '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion.orden <= " + r_orden);
		cadenaSQL.append(" and (prd_programacion.dia is not null and prd_programacion.dia<>'') ");
		cadenaSQL.append(" order by prd_programacion.orden desc ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				dia = rsOpcion.getString("pro_dia");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return dia;
	}
	

	public HashMap<String, String> hayObservacionesCalidad(ArrayList<MapeoProgramacion> r_mapeo)
	{
		boolean hayObservaciones = false;
		HashMap<String, String> obs = null;
		ArrayList<MapeoLineasEscandallo> vector = null;
		String art=null;
		File fl =null;
		obs = new HashMap<String, String>();
		
		if (r_mapeo!=null)
		{
			for (int j=0; j<r_mapeo.size();j++)
			{
				
				MapeoProgramacion mapeo = r_mapeo.get(j);
				if (mapeo.getArticulo()!=null)
				{
	
					art = mapeo.getArticulo().trim();
				
					fl =new File(LecturaProperties.basePdfPath + "/calidad/pt/" + art.trim() + ".pdf");
			    	
			    	if (fl.exists())
			    	{
			    		obs.put(art,"S");
			    	}
			    	else
			    	{
						consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
						vector = ces.recuperarModificacionesEscandalloInterno(art);
						String valor= "N";
						for (int i = 0; i<vector.size();i++)
						{
							MapeoLineasEscandallo map = (MapeoLineasEscandallo) vector.get(i);
							fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + map.getArticulo().trim() + ".pdf");
							if (fl.exists())
							{
								valor="S";
								break;
							}
						}
						obs.put(art,valor);
			    	}
				}
			}
		}
		return obs;
	}

	public boolean hayObservacionesCalidad(String r_articulo)
	{
		boolean hayObservaciones = false;
		ArrayList<MapeoLineasEscandallo> vector = null;
		
		File fl =new File(LecturaProperties.basePdfPath + "/calidad/pt/" + r_articulo.trim() + ".pdf");
    	
    	if (fl.exists())
    	{
    		hayObservaciones = true;
    	}
    	else
    	{
			consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
			vector = ces.recuperarModificacionesEscandalloInterno(r_articulo);
			
			for (int i = 0; i<vector.size();i++)
			{
				MapeoLineasEscandallo mapeo = (MapeoLineasEscandallo) vector.get(i);
				fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + mapeo.getArticulo().trim() + ".pdf");
				if (fl.exists())
				{
					hayObservaciones= true;
					break;
				}
			}
    	}
		return hayObservaciones;
	}
	
	public String recogerArticuloProgramacion(Integer r_idProgramacion)
	{
		String articuloEncontrado = null;
		
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.articulo pro_art ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.idprd_programacion = " + r_idProgramacion);
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				articuloEncontrado= rsOpcion.getString("pro_art");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return articuloEncontrado;
	}
	
	public HashMap<Integer, String> obtenerHashArticulos(String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<Integer, String> hash = null;
		int contador = 0;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT distinct prd_programacion.articulo pro_art ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('ETIQUETADO', 'EMBOTELLADO') "); 
//		cadenaSQL.append(" and estado not in ('T') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<Integer, String>();
			
			while(rsOpcion.next())
			{
				hash.put(contador, rsOpcion.getString("pro_art").trim());
				contador++;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	public HashMap<String, Double> obtenerHashProducciones(String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion.articulo pro_art, ");
		cadenaSQL.append(" sum(prd_programacion.unidades) pro_uds ");
		cadenaSQL.append(" from prd_programacion ");

		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('ETIQUETADO', 'EMBOTELLADO') "); 
//		cadenaSQL.append(" and estado not in ('T') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try
		{
			cadenaSQL.append(" group by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("pro_art").trim(), rsOpcion.getDouble("pro_uds"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	public HashMap<String, Double> obtenerHashVelocidades(String r_ejercicio, String r_semana, String r_vista, boolean r_prevision)
	{
		HashMap<String, Double> hash = null;
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		int desde = 1;
		int hasta = 53;
		
		cadenaSQL.append(" SELECT prd_programacion.articulo pro_art, ");
		cadenaSQL.append(" convert(1/(sum(prd_programacion.unidades*convert(prd_programacion.velocidad,decimal(8,0)))/sum(unidades)),decimal(20,12)) pro_vel_media ");
		
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('ETIQUETADO', 'EMBOTELLADO') "); 
		
		if (!r_prevision)
			cadenaSQL.append(" and estado in ('T') ");
//		else
//			cadenaSQL.append(" and estado not in ('T') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try
		{
			cadenaSQL.append(" group by articulo ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("pro_art").trim(), rsOpcion.getDouble("pro_vel_media"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public String sincronizarProduccionLPN()
	{
		String rdo = null;
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;
		consultaArticulosServer cas = null;
		ArrayList<MapeoProduccionDiariaSGA> vector = null;
		StringBuffer cadenaSQL = null;

		
		boolean existe = false;
		cas = consultaArticulosServer.getInstance(CurrentUser.get());
		cadenaSQL = new StringBuffer();
		/*
		 * recojo lpn's generados y no procesados
		 */
		cadenaSQL.append(" SELECT a__pd.articulo pd_art,");
		cadenaSQL.append(" a__pd.cantidad pd_can,");
		cadenaSQL.append(" a__pd.fecha pd_fec,");
		cadenaSQL.append(" a__pd.hora pd_hor,");
		cadenaSQL.append(" a__pd.anada pd_ana,");
		cadenaSQL.append(" a__pd.sscc pd_lpn,");
		cadenaSQL.append(" a__pd.lote pd_lot,");
		cadenaSQL.append(" a__pd.operacion pd_ope");
		cadenaSQL.append(" from a__pd ");

		try
		{
			cadenaSQL.append(" order by sscc asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiariaSGA>();
			
			while(rsOpcion.next())
			{
				MapeoProduccionDiariaSGA  mapeoProduccionDiariaSGA = new MapeoProduccionDiariaSGA();
				/*
				 * recojo mapeo etiquetas
				 */
				mapeoProduccionDiariaSGA.setArticulo(rsOpcion.getString("pd_art"));
				mapeoProduccionDiariaSGA.setCantidad(rsOpcion.getInt("pd_can"));
				mapeoProduccionDiariaSGA.setFecha(rsOpcion.getString("pd_fec"));
				mapeoProduccionDiariaSGA.setHora(rsOpcion.getString("pd_hor"));
				mapeoProduccionDiariaSGA.setAnada(rsOpcion.getString("pd_ana"));
				mapeoProduccionDiariaSGA.setSscc(rsOpcion.getString("pd_lpn"));
				mapeoProduccionDiariaSGA.setLote(rsOpcion.getString("pd_lot"));
				mapeoProduccionDiariaSGA.setOperacion(rsOpcion.getString("pd_ope").toUpperCase());
				
				MapeoProduccionDiariaSGA mapeoProduccionDiariaLeidaSGA = lpnLeidoSGA(mapeoProduccionDiariaSGA.getSscc(), cas.obtenerTipoArticulo(mapeoProduccionDiariaSGA.getArticulo().trim()), mapeoProduccionDiariaSGA.getArticulo()); 
				if (mapeoProduccionDiariaLeidaSGA != null)
				{
					mapeoProduccionDiariaSGA.setFecha(mapeoProduccionDiariaLeidaSGA.getFecha());
					mapeoProduccionDiariaSGA.setHora(mapeoProduccionDiariaLeidaSGA.getHora());
					this.guardarProduccionLeidaGsys(mapeoProduccionDiariaSGA);
					this.eliminarLpnSincronizado(mapeoProduccionDiariaSGA.getSscc());
					this.eliminarLpnSincronizadoSGA(mapeoProduccionDiariaLeidaSGA.getSscc());
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		return rdo;
	}
	private MapeoProduccionDiariaSGA lpnLeidoSGA(String r_sscc, String r_ptps, String r_articulo)
	{
		MapeoProduccionDiariaSGA mapeoLpnLeido = null;
		String rdo = null;
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProduccionDiariaSGA> vector = null;
		StringBuffer cadenaSQL = null;

		
		boolean existe = false;
		cadenaSQL = new StringBuffer();
		
		/*
		 * recojo lpn's generados y no procesados
		 */
		cadenaSQL.append(" SELECT produccionDiaria.articulo pd_art,");
		cadenaSQL.append(" produccionDiaria.cantidad pd_can,");
		cadenaSQL.append(" produccionDiaria.fecha pd_fec,");
		cadenaSQL.append(" produccionDiaria.hora pd_hor,");
		cadenaSQL.append(" produccionDiaria.anada pd_ana,");
		cadenaSQL.append(" produccionDiaria.sscc pd_lpn,");
		cadenaSQL.append(" produccionDiaria.lote pd_lot,");
		cadenaSQL.append(" produccionDiaria.operacion pd_ope");
		cadenaSQL.append(" from produccionDiaria ");
		
		if (r_ptps.contentEquals("PT"))
			cadenaSQL.append(" where produccionDiaria.sscc = " + r_sscc);
		else
			cadenaSQL.append(" where produccionDiaria.articulo = " + r_articulo);

		try
		{
			cadenaSQL.append(" order by sscc asc");
			con= this.conManager.establecerConexionSQLInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			while (rsOpcion.next())
			{
				mapeoLpnLeido = new MapeoProduccionDiariaSGA();
				mapeoLpnLeido.setSscc(rsOpcion.getString("pd_lpn"));
				mapeoLpnLeido.setArticulo(rsOpcion.getString("pd_art"));
				mapeoLpnLeido.setCantidad(rsOpcion.getInt("pd_can"));
				mapeoLpnLeido.setFecha(rsOpcion.getString("pd_fec"));
				mapeoLpnLeido.setHora(rsOpcion.getString("pd_hor"));
				mapeoLpnLeido.setAnada(rsOpcion.getString("pd_ana"));
				mapeoLpnLeido.setLote(rsOpcion.getString("pd_lot"));
				mapeoLpnLeido.setOperacion(rsOpcion.getString("pd_ope").toUpperCase());
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		return mapeoLpnLeido;
	}
	
	private void guardarProduccionLeidaGsys(MapeoProduccionDiariaSGA r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection conGsys = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO a__pd ( ");
			cadenaSQL.append(" a__pd.articulo ,");
			cadenaSQL.append(" a__pd.cantidad ,");
			cadenaSQL.append(" a__pd.fecha ,");
			cadenaSQL.append(" a__pd.hora, ");
			cadenaSQL.append(" a__pd.anada, ");
			cadenaSQL.append(" a__pd.sscc ,");
			cadenaSQL.append(" a__pd.lote ,");
			cadenaSQL.append(" a__pd.operacion ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");

			conGsys = this.conManager.establecerConexionGestionInd();
			preparedStatement = conGsys.prepareStatement(cadenaSQL.toString());

			if (r_mapeo.getArticulo() != null) 
			{
				preparedStatement.setString(1, r_mapeo.getArticulo());
			} 
			else 
			{
				preparedStatement.setString(1, null);
			}
			if (r_mapeo.getCantidad() != null) 
			{
				preparedStatement.setInt(2, r_mapeo.getCantidad());
			} 
			else 
			{
				preparedStatement.setInt(2, 0);
			}
			if (r_mapeo.getFecha() != null) 
			{
				preparedStatement.setString(3, RutinasFechas.convertirDeFecha(r_mapeo.getFecha()));
			} 
			else 
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getHora() != null) 
			{
				preparedStatement.setString(4, r_mapeo.getHora());
			} 
			else 
			{
				preparedStatement.setInt(4, 0);
			}
			if (r_mapeo.getAnada() != null) 
			{
				preparedStatement.setString(5, r_mapeo.getAnada());
			} 
			else 
			{
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getSscc() != null) 
			{
				preparedStatement.setString(6, r_mapeo.getSscc());
			} 
			else 
			{
				preparedStatement.setString(6, null);
			}
			if (r_mapeo.getLote() != null) 
			{
				preparedStatement.setString(7, r_mapeo.getLote());
			} 
			else 
			{
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getOperacion() != null)
			{
				preparedStatement.setString(8, r_mapeo.getOperacion());
			}
			else
			{
				preparedStatement.setString(8, null);
			}

			preparedStatement.executeUpdate();
		} 
		catch (Exception ex) 
		{
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (conGsys!=null)
				{
					conGsys.close();
					conGsys=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

	}

}

