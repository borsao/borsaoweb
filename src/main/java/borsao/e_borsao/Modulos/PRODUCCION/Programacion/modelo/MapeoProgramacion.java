package borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoProgramacion  extends MapeoGlobal
{
    private Integer idProgramacion;
	private Integer ejercicio;
	private String semana;
	private String dia;
	private String tipo;
	private String anada;
	private Integer cantidad;
	private Integer velocidad;
	private Integer orden;
	private Long ordenFabricacion;
	private String vino;
	private String lote;
	private String lote_pt;
	private String lote_jaulon_1;
	private String articulo;
	private String descripcion;
	private Integer cajas;
	private Integer factor;
	private Integer unidades;
	private String observaciones;
	private String frontales;
	private String contras;
	private String capsulas;
	private String tapones;
	private String caja;
	private String estado;
	private String ojo;
	private Integer status;
	private String papel=" ";
	private String etiqueta=" ";
	private String biblia=" ";
	private String esc=" ";
	private String cambios=" ";
	private String subir=" ";
	private String bajar=" ";
	private String nuevo;
	
	private Date fecha;

	public MapeoProgramacion()
	{
//		this.setArticulo("");
//		this.setVino("");
//		this.setLote("");
//		this.setDia("");
//		this.setObservaciones("");
//		this.setDescripcion("");
//		this.setCantidad(0);
//		this.setOrden(0);
//		this.setCajas(0);
//		this.setFactor(0);
//		this.setUnidades(0);
		
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getSemana() {
		if (semana!=null && new Integer(semana)==0) semana = "";
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getVino() {
		return vino;
	}

	public void setVino(String vino) {
		this.vino = vino;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getArticulo() {
		if (articulo!=null) return articulo.trim();
		else return null;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = RutinasCadenas.conversion(descripcion);
	}

	public Integer getCajas() {
		return cajas;
	}

	public void setCajas(Integer cajas) {
		this.cajas = cajas;
	}

	public Integer getFactor() {
		return factor;
	}

	public void setFactor(Integer factor) {
		this.factor = factor;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getFrontales() {
		return frontales;
	}

	public void setFrontales(String frontales) {
		this.frontales = frontales;
	}

	public String getContras() {
		return contras;
	}

	public void setContras(String contras) {
		this.contras = contras;
	}

	public String getCapsulas() {
		return capsulas;
	}

	public void setCapsulas(String capsulas) {
		this.capsulas = capsulas;
	}

	public String getTapones() {
		return tapones;
	}

	public void setTapones(String tapones) {
		this.tapones = tapones;
	}

	public String getCaja() {
		return caja;
	}

	public void setCaja(String caja) {
		this.caja = caja;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPapel() {
		return papel;
	}

	public void setPapel(String papel) {
		this.papel = papel;
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getOjo() {
		return ojo;
	}

	public void setOjo(String ojo) {
		this.ojo = ojo;
	}

	public String getSubir() {
		return subir;
	}

	public void setSubir(String subir) {
		this.subir = subir;
	}

	public String getEsc() {
		return esc;
	}

	public void setEsc(String esc) {
		this.esc = esc;
	}

	public String getCambios() {
		return cambios;
	}

	public void setCambios(String cambios) {
		this.cambios = cambios;
	}

	public String getBajar() {
		return bajar;
	}

	public void setBajar(String bajar) {
		this.bajar = bajar;
	}

	public String getBiblia() {
		return biblia;
	}

	public void setBiblia(String biblia) {
		this.biblia = biblia;
	}

	public String getNuevo() {
		return nuevo;
	}

	public void setNuevo(String nuevo) {
		this.nuevo = nuevo;
	}

	public Integer getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(Integer velocidad) {
		this.velocidad = velocidad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getLote_pt() {
		return lote_pt;
	}

	public String getLote_jaulon_1() {
		return lote_jaulon_1;
	}

	public void setLote_pt(String lote_pt) {
		this.lote_pt = lote_pt;
	}

	public void setLote_jaulon_1(String lote_jaulon_1) {
		this.lote_jaulon_1 = lote_jaulon_1;
	}

	public String getAnada() {
		return anada;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public Long getOrdenFabricacion() {
		return ordenFabricacion;
	}

	public void setOrdenFabricacion(Long ordenFabricacion) {
		this.ordenFabricacion = ordenFabricacion;
	}
}