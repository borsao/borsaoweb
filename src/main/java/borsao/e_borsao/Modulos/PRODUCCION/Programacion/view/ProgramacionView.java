package borsao.e_borsao.Modulos.PRODUCCION.Programacion.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.vaadin.haijian.ExcelExporter;
import org.vaadin.teemusa.gridextensions.wrappinggrid.WrappingGrid;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.PeticionFormatoImpresion;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaMermasPtServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.PeticionMermasPt;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view.pantallaNotasArticulo;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizador.server.servidorSincronizadorBBDD;
import borsao.e_borsao.Modulos.INFORMATICA.sincronizarTablas.modelo.MapeoSincronizarTablas;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.PantallaControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.ProgramacionGrid;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.pantallaSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.pantallaUbicacionMateriales;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ProgramacionView extends GridViewRefresh {

	public static final String VIEW_NAME = "Programacion Embotellado";
	public consultaProgramacionServer cus =null;
	public ComboBox cmbSemana = null;
	public TextField txtEjercicio= null;
	public TextField txtUnidades= null;
	public TextField txtLitros= null;
	public boolean modificando = false;
	
	private WrappingGrid  wrap =null;
	private CheckBox recortar = null;
	
	private consultaEscandalloServer ces = null;	
	private final String titulo = "PROGRAMACION LINEA EMBOTELLADO";
	private int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = false;
	private Integer permisos = 0;	
	private String semana ="";
	private OpcionesForm form = null;
	private Button opcCompletar = null;
	private Button opcRechazo = null;
	private Button opcControl= null;
	private Button opcBajar = null;
	private Button opcSubir = null;
	private Button opcImprimir = null;
	private Button opcMail = null;
	private Button opcSumar = null;
	private Button opcMateriales = null;
	private Button opcMes = null;
	private Button opcTotal = null;
	private ExcelExporter opcExcel = null;
	private Button excelImportador = null;
	
	private Button opcMenos= null;
	private Button opcMas= null;
	private ProgramacionView app = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ProgramacionView() 
    {
    	app = this;
    	opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	
    	this.ces = new consultaEscandalloServer(CurrentUser.get());
		this.cus= consultaProgramacionServer.getInstance(CurrentUser.get());
		this.permisos = new Integer(this.cus.comprobarAccesos());
		
		if (this.permisos==99) this.intervaloRefresco=0;
    	
		
    	if (this.permisos==0)
    	{
    		Notificaciones.getInstance().mensajeError("No tienes acceso a este programa");
    		this.destructor();
    	}
    	else
    	{
    		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    		
    		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);

    		this.opcSubir= new Button();    	
    		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_UP);
    		
    		this.opcBajar= new Button();    	
    		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

    		this.opcMas= new Button();    	
    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
    		
    		this.opcMenos= new Button();    	
    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
    		
    		this.opcImprimir= new Button();    	
    		this.opcImprimir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcImprimir.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcImprimir.setIcon(FontAwesome.PRINT);
    		this.opcImprimir.setDescription("Imprimir");

    		this.opcMail= new Button();    	
    		this.opcMail.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMail.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMail.setIcon(FontAwesome.ENVELOPE_O);

    		this.opcExcel = new ExcelExporter();    	  	
    		this.opcExcel.setDateFormat("yyyy-MM-dd");
    		this.opcExcel.setIcon(FontAwesome.FILE_EXCEL_O);
    		this.opcExcel.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcExcel.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcExcel.setDownloadFileName("programacion");
    		
    		this.opcSumar= new Button();    	
    		this.opcSumar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcSumar.setIcon(FontAwesome.PLUS_SQUARE);
    		this.opcSumar.setDescription("Seleccion Lineas");

    		this.opcMateriales= new Button();    	
    		this.opcMateriales.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMateriales.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMateriales.setIcon(FontAwesome.CUBES);
    		this.opcMateriales.setVisible(false);
    		this.opcMateriales.setDescription("Explosión Materiales");

    		this.opcMes= new Button();    	
    		this.opcMes.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMes.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMes.setIcon(FontAwesome.UPLOAD);
    		this.opcMes.setVisible(false);
    		this.opcMes.setDescription("Lanzar Orden");
    		
    		this.opcTotal= new Button();    	
    		this.opcTotal.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcTotal.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcTotal.setIcon(FontAwesome.CALCULATOR);
    		this.opcTotal.setVisible(false);
    		this.opcTotal.setDescription("Caluclo Litros/botellas");
    		
    		this.opcCompletar= new Button("Completar");    	
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcCompletar.setIcon(FontAwesome.CHECK_CIRCLE);
    		
    		this.opcRechazo= new Button();    	
    		this.opcRechazo.addStyleName(ValoTheme.BUTTON_DANGER);
    		this.opcRechazo.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcRechazo.setIcon(FontAwesome.THUMBS_DOWN);
    		this.opcRechazo.setDescription("Mermas");
    		
    		this.opcControl= new Button();    	
    		this.opcControl.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcControl.addStyleName(ValoTheme.BUTTON_TINY );
    		this.opcControl.setIcon(FontAwesome.LIST_ALT);
    		this.opcControl.setDescription("Control Embotellado");
    		
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
    		
    		this.txtUnidades=new TextField("Unidades");
    		this.txtUnidades.setVisible(false);
    		this.txtUnidades.setWidth("150px");
    		this.txtUnidades.addStyleName(ValoTheme.TEXTFIELD_TINY);
    		this.txtUnidades.addStyleName("rightAligned");
    		this.txtLitros=new TextField("Litros");
    		this.txtLitros.setVisible(false);
    		this.txtLitros.setWidth("150px");
    		this.txtLitros.addStyleName("rightAligned");
    		this.txtLitros.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		
    		this.cmbSemana= new ComboBox("Semana");    	
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.setNullSelectionAllowed(false);
    		
    		this.recortar=new CheckBox("Wrap Texto");
    		this.recortar.setValue(false);
    		
    		this.cabLayout.addComponent(this.opcSubir);
    		this.cabLayout.setComponentAlignment(this.opcSubir,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcBajar);
    		this.cabLayout.setComponentAlignment(this.opcBajar,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcRechazo);
    		this.cabLayout.setComponentAlignment(this.opcRechazo,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcControl);
    		this.cabLayout.setComponentAlignment(this.opcControl,Alignment.BOTTOM_LEFT);    		
    		this.cabLayout.addComponent(this.opcImprimir);
    		this.cabLayout.setComponentAlignment(this.opcImprimir,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcMail);
    		this.cabLayout.setComponentAlignment(this.opcMail,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcExcel);
    		this.cabLayout.setComponentAlignment(this.opcExcel, Alignment.BOTTOM_LEFT);

    		this.cabLayout.addComponent(this.txtEjercicio);
    		this.cabLayout.addComponent(this.opcMenos);
    		this.cabLayout.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.cmbSemana);
    		this.cabLayout.addComponent(this.opcMas);
    		this.cabLayout.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);

//    		this.cabLayout.addComponent(this.recortar);
    		this.cabLayout.addComponent(this.opcCompletar);
    		this.cabLayout.setComponentAlignment(this.opcCompletar,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcSumar);
    		this.cabLayout.setComponentAlignment(this.opcSumar,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcMateriales);
    		this.cabLayout.setComponentAlignment(this.opcMateriales,Alignment.BOTTOM_LEFT);
    		
    		this.cabLayout.addComponent(this.opcTotal);
    		this.cabLayout.setComponentAlignment(this.opcTotal,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.txtUnidades);
    		this.cabLayout.setComponentAlignment(this.txtUnidades,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.txtLitros);
    		this.cabLayout.setComponentAlignment(this.txtLitros,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcMes);
    		this.cabLayout.setComponentAlignment(this.opcMes,Alignment.BOTTOM_LEFT);
    		
    		this.excelImportador= new Button();    	
    		this.excelImportador.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.excelImportador.setIcon(FontAwesome.CLOUD_UPLOAD);
    		this.excelImportador.setVisible(false);

    		this.cabLayout.addComponent(this.excelImportador);
    		this.cabLayout.setComponentAlignment(this.excelImportador,Alignment.BOTTOM_LEFT);
    		
    		this.cargarCombo(null);
    		this.cargarListeners();
    		this.establecerModo();
    		this.cmbSemana.setValue(this.semana);
    		
    		opcionesEscogidas.put("ejercicio", this.txtEjercicio.getValue());
    		opcionesEscogidas.put("semana", this.semana);
    		this.generarGrid(opcionesEscogidas);
    	}
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoProgramacion> r_vector=null;
    	MapeoProgramacion mapeoProgramacion =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoProgramacion=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	r_vector=this.cus.datosProgramacionGlobal(mapeoProgramacion);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (permisos==99 && isHayGrid())
    	{
    		this.opcBajar.setEnabled(true);
    		this.opcBajar.setVisible(true);
    		this.opcSubir.setEnabled(true);
    		this.opcSubir.setVisible(true);

    	}
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((ProgramacionGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((ProgramacionGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
	@Override
	public void mostrarFilas(Collection<Object> r_filas) 
	{
		Iterator it = null;
		Double unidades = new Double(0);
		Double litros = new Double(0);
		
		it = r_filas.iterator();
		
		while (it.hasNext())
		{			
			MapeoProgramacion mapeo = (MapeoProgramacion) it.next();
			unidades = unidades + mapeo.getUnidades();
			Double factor = ces.recuperarCantidadComponenteMascara(mapeo.getArticulo(), "0101");
			litros = litros + mapeo.getUnidades() * factor;
		}

		if (unidades!=null)
		{
			txtUnidades.setValue(unidades.toString());			
			txtLitros.setValue(litros.toString());			
		}
	}
	
	
	public void mostrarMateriales(Collection<Object> r_filas) 
	{
		Iterator it = null;
		ArrayList<String> listaArticulosSeleccionados = null;
		ArrayList<String> listaOrdenesSeleccionados = null;
		it = r_filas.iterator();

		listaArticulosSeleccionados = new ArrayList<String>();
		listaOrdenesSeleccionados = new ArrayList<String>();
		
		MapeoProgramacion mapeo =null;
		while (it.hasNext())
		{			
			mapeo = (MapeoProgramacion) it.next();

			if (mapeo.getArticulo()!= null && mapeo.getArticulo().length()>0)
			{
				if (mapeo.getTipo().toUpperCase().equals("ETIQUETADO"))
				{
					if(mapeo.getArticulo().contentEquals("0102006") || mapeo.getArticulo().contentEquals("0102091"))
					{
						listaArticulosSeleccionados.add(mapeo.getArticulo() + "-2");
					}
					else
					{
						listaArticulosSeleccionados.add(mapeo.getArticulo() + "-1");
					}
				}
				else 
				{
					listaArticulosSeleccionados.add(mapeo.getArticulo());
				}
				listaOrdenesSeleccionados.add(mapeo.getEjercicio().toString() + RutinasCadenas.formatCerosIzquierda(mapeo.getSemana(), 2) + RutinasCadenas.formatCerosIzquierda(mapeo.getOrden().toString(), 3));
			}
		}

		/*
		 * Llamamos a la pantalla de explosion de materiales para su consulta e impresion
		 */
		
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		pantallaSituacionEmbotellado vtPantallaMateriales = new pantallaSituacionEmbotellado(listaOrdenesSeleccionados, listaArticulosSeleccionados, this.txtEjercicio.getValue(), this.cmbSemana.getValue().toString(), "Botella", mapeo.getLote_jaulon_1());
		getUI().addWindow(vtPantallaMateriales);
	}
	
	public void mostrarMateriales(MapeoProgramacion r_mapeo) 
	{
		Iterator it = null;
		ArrayList<String> listaArticulosSeleccionados = null;
		ArrayList<String> listaOrdenesSeleccionados = null;

		listaArticulosSeleccionados = new ArrayList<String>();
		listaOrdenesSeleccionados = new ArrayList<String>();
		
		MapeoProgramacion mapeo = r_mapeo;

		if (mapeo.getArticulo()!= null && mapeo.getArticulo().length()>0)
		{
			if (mapeo.getTipo().toUpperCase().equals("ETIQUETADO"))
			{
				if(mapeo.getArticulo().contentEquals("0102006") || mapeo.getArticulo().contentEquals("0102091"))
				{
					listaArticulosSeleccionados.add(mapeo.getArticulo() + "-2");
				}
				else
				{
					listaArticulosSeleccionados.add(mapeo.getArticulo() + "-1");
				}
			}
			else 
			{
				listaArticulosSeleccionados.add(mapeo.getArticulo());
			}
			listaOrdenesSeleccionados.add(mapeo.getEjercicio().toString() + RutinasCadenas.formatCerosIzquierda(mapeo.getSemana(), 2) + RutinasCadenas.formatCerosIzquierda(mapeo.getOrden().toString(), 3));
		}

		String pdfGenerado = null;
		
    	consultaControlEmbotelladoServer cps = new consultaControlEmbotelladoServer(CurrentUser.get());
    	
    	if (eBorsao.get().accessControl.getNombre().contains("carretillero"))
    	{
    		/*
    		 * pantalla explosion mp lpn's uno a uno
    		 */
    		pantallaUbicacionMateriales vtPantallaMateriales = new pantallaUbicacionMateriales(listaOrdenesSeleccionados, mapeo.getArticulo(), mapeo.getDescripcion(), this.txtEjercicio.getValue(), this.cmbSemana.getValue().toString(), "Botella", mapeo.getLote_jaulon_1(), mapeo.getOrdenFabricacion(), mapeo.getTipo().toUpperCase());
    		getUI().addWindow(vtPantallaMateriales);

    	}
    	else
    	{
    	
	    	pdfGenerado = cps.imprimirExplosionSGA(listaOrdenesSeleccionados,listaArticulosSeleccionados, mapeo.getLote_jaulon_1());
	    	
			if(pdfGenerado!=null && pdfGenerado.length()>0)
			{
				RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
			}
			else
				Notificaciones.getInstance().mensajeInformativo("No hay datos a imrpimir.");
    	}

	}
	
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((ProgramacionGrid) this.grid).activadaVentanaPeticion && !((ProgramacionGrid) this.grid).ordenando)
    	{
	    	switch (this.permisos)
	    	{
		    	case 0:
		    		break;
		    	case 10:
		    		break;
		    	case 20:
		    		break;
		    	case 30:
		    		mostrarMateriales((MapeoProgramacion) r_fila);
		    		break;
		    	case 40:
		    		this.establecerBotonAccion(r_fila);
		    		break;
		    	case 50:
		    		break;
		    	case 70:
		    		break;
		    	case 80:
		    		break;
		    	case 99:
		    		this.establecerBotonAccion(r_fila);
		    		this.modificando=true;
		    		this.verForm(false);
			    	this.form.editarProgramacion((MapeoProgramacion) r_fila);
		    		break;
	    	}
    	}    		
    }
    
    public void generarGrid(MapeoProgramacion r_mapeo)
    {
    	ArrayList<MapeoProgramacion> r_vector=null;
    	r_vector=this.cus.datosProgramacionGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    public void presentarGrid(ArrayList<MapeoProgramacion> r_vector)
    {
    	StringBuffer cadenaArticulos = new StringBuffer();
    	MapeoProgramacion mapeoProgramacion = null;
    	HashMap<String, String> obs = null;
    	obs = this.cus.hayObservacionesCalidad(r_vector);
    	
    	
    	if (this.permisos==99 && r_vector.size()>0)
    	{
	    	cadenaArticulos.append("('");
	    	
			for (int i=0; i< r_vector.size();i++)
			{
				mapeoProgramacion = r_vector.get(i);
				
				if (mapeoProgramacion.getArticulo()!=null && mapeoProgramacion.getArticulo().length()>0)
				{
					cadenaArticulos.append(mapeoProgramacion.getArticulo());
					cadenaArticulos.append("','");
				}
			}
	    	cadenaArticulos.append("')");
	
	    	consultaNotasArticulosServer cns = consultaNotasArticulosServer.getInstance(CurrentUser.get());
	    	ArrayList<MapeoNotasArticulos> vector = cns.verNotas(cadenaArticulos);
	    	
			if (vector.size()>0)
			{
				pantallaNotasArticulo vt = new pantallaNotasArticulo("Notas de Articulos ", vector);
				getUI().addWindow(vt);
			}
    	}    	
    	
    	grid = new ProgramacionGrid(this.permisos,r_vector, obs);
    	
    	if (((ProgramacionGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((ProgramacionGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		wrap = WrappingGrid.extend(grid);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});

    	this.recortar.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{
					wrap.setWrapping(recortar.getValue());
				}
				
			}
		});
    	
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
				
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;
//					setHayGrid(false);
//				}
//				MapeoProgramacion mapeo = new MapeoProgramacion();
//				mapeo.setSemana(cmbSemana.getValue().toString());
//				mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
//				
//				if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
//				}
//				if (txtEjercicio.getValue()!=null && txtEjercicio.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
//				}
//				
//				generarGrid(mapeo);


			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));

				
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;
//					setHayGrid(false);
//				}
//				
//				MapeoProgramacion mapeo = new MapeoProgramacion();
//				mapeo.setSemana(cmbSemana.getValue().toString());
//				mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
//				
//				if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
//				}
//				if (txtEjercicio.getValue()!=null && txtEjercicio.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
//				}
//				
//				generarGrid(mapeo);

			}
		});
    	
		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoProgramacion mapeo = new MapeoProgramacion();
					mapeo.setSemana(cmbSemana.getValue().toString());
					mapeo.setEjercicio(new Integer(txtEjercicio.getValue().toString()));
					
					if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
					{
						opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
					}
					if (txtEjercicio.getValue()!=null && txtEjercicio.getValue().toString().length()>0) 
					{
						opcionesEscogidas.put("ejercicio", txtEjercicio.getValue().toString());
					}
					
					generarGrid(mapeo);
				}
			}
		}); 

    	this.opcCompletar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacion mapeo=(MapeoProgramacion) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					if (opcCompletar.getCaption().equals("Completar"))
					{
	 					mapeo.setEstado("T");
					}
					else if (opcCompletar.getCaption().equals("Reabrir"))
					{
						mapeo.setEstado("A");
					}
					else if (opcCompletar.getCaption().equals("Aceptar"))
					{
						mapeo.setEstado("A");
					}
					else if (opcCompletar.getCaption().equals("Validar"))
					{
						mapeo.setEstado("A");
					}
					
					cus.modificarEstadoProgramacion(mapeo.getIdProgramacion(), mapeo.getEstado(), mapeo.getStatus());
					if (isHayGrid())
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;
						generarGrid(opcionesEscogidas);
					}
				}				
			}
		});
    	
    	this.opcRechazo.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacion mapeo=(MapeoProgramacion) grid.getSelectedRow();
				
				if (mapeo!=null && mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
				{
					rechazoMateriales(mapeo);
				}				
			}
		});

    	this.opcControl.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacion mapeo=(MapeoProgramacion) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					controlEmbotellado(mapeo);
				}				
			}
		});

    	this.opcSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("subir");
			}
		});

    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("bajar");
			}
		});

    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				imprimirProgramacion(false, false);
			}
		});
    	this.opcMail.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				imprimirProgramacion(false,true);
			}
		});
    	this.opcSumar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (grid.getSelectionModel() instanceof SingleSelectionModel)
				{
					opcSumar.addStyleName(ValoTheme.BUTTON_DANGER);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(false);
					txtUnidades.setVisible(true);
					txtUnidades.setValue("");
					txtLitros.setVisible(true);
					txtLitros.setValue("");
					botonesGenerales(false);
					activarBotones(false);
					opcSumar.setEnabled(true);					
					grid.setSelectionMode(SelectionMode.MULTI);
					opcTotal.setVisible(true);					
					opcMateriales.setVisible(true);
//					opcMes.setVisible(false);
				}
				else
				{					
					opcSumar.setStyleName(ValoTheme.BUTTON_FRIENDLY);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(true);
					txtUnidades.setVisible(false);
					txtLitros.setVisible(false);
					botonesGenerales(true);
					activarBotones(true);
					opcSumar.setEnabled(true);
					opcTotal.setVisible(false);
					opcMateriales.setVisible(false);
//					opcMes.setVisible(false);
					grid.deselectAll();
					grid.setSelectionMode(SelectionMode.SINGLE);
				}
			}
		});
    	this.excelImportador.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				importarExcel();
			}
		});
    	
    	this.opcTotal.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				mostrarFilas(grid.getSelectedRows());
			}
		});
    	this.opcMateriales.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				if (permisos.compareTo(40)==0 || permisos.compareTo(30)==0)
				{
					mostrarMateriales((MapeoProgramacion) grid.getSelectedRow());
				}
				else
					mostrarMateriales(grid.getSelectedRows());
			}
		});
    	this.opcMes.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) 
    		{
    			VentanaAceptarCancelar vt = new VentanaAceptarCancelar(app, "Quieres Lanzar la Orden de producción ?", "Si", "No", "OF", null);
    			getUI().addWindow(vt);
    			
    		}
    	});
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
		this.opcImprimir.setVisible(true);
		this.opcImprimir.setEnabled(true);

		switch (this.permisos)
		{
			case 10:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				break;
			case 20:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(true);
				this.activarBotones(false);
				break;
			case 30:
				this.setSoloConsulta(true);
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				this.opcMateriales.setEnabled(true);
				this.opcMateriales.setVisible(true);
				break;
			case 40:
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				this.opcMateriales.setEnabled(true);
				this.opcMateriales.setVisible(true);
				this.opcMes.setEnabled(true);
				this.opcMes.setVisible(true);
				this.opcCompletar.setEnabled(true);
				this.opcCompletar.setVisible(true);
				this.opcRechazo.setEnabled(true);
				this.opcRechazo.setVisible(true);				
				this.opcControl.setEnabled(true);			
				this.opcControl.setVisible(true);
				
				break;
			case 50:
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 70:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				break;
			case 80:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				this.opcRechazo.setEnabled(true);
				this.opcRechazo.setVisible(true);				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				this.opcSumar.setVisible(true);
				this.opcSumar.setEnabled(true);
				break;				
			case 99:
				this.verBotones(true);
				this.activarBotones(true);
				this.navegacion(true);
				this.opcMes.setEnabled(true);
				this.opcMes.setVisible(true);
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				setSoloConsulta(this.soloConsulta);
				break;			
		}
	}

	private void navegacion(boolean r_navegar)
	{
		this.cmbSemana.setVisible(r_navegar);
		this.txtEjercicio.setVisible(r_navegar);
		this.cmbSemana.setEnabled(r_navegar);
		this.txtEjercicio.setEnabled(r_navegar);	
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCompletar.setEnabled(r_activo);
//		this.opcImprimir.setEnabled(r_activo);
		this.opcSubir.setEnabled(r_activo);
		this.opcBajar.setEnabled(r_activo);
		this.opcMail.setEnabled(r_activo);
		this.opcSumar.setEnabled(r_activo);
		this.opcExcel.setEnabled(r_activo);
		this.opcRechazo.setEnabled(r_activo);
		this.opcControl.setEnabled(r_activo);

	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCompletar.setVisible(r_visibles);
		this.opcSubir.setVisible(r_visibles);
//		this.opcImprimir.setEnabled(r_visibles);
		this.opcBajar.setVisible(r_visibles);
		this.opcMail.setVisible(r_visibles);
		this.opcSumar.setVisible(r_visibles);
		this.opcExcel.setVisible(r_visibles);
		this.opcRechazo.setVisible(r_visibles);
		this.opcControl.setVisible(r_visibles);

	}
	
	private void establecerBotonAccion(Object r_fila)
	{
		this.opcCompletar.setVisible(true);

    	if (r_fila!=null && ((MapeoProgramacion) r_fila).getEstado()!=null)
    	{
    		this.opcCompletar.setEnabled(true);
	    	if (((MapeoProgramacion) r_fila).getEstado().equals("A") || ((MapeoProgramacion) r_fila).getEstado().equals("L") )
	    	{
	    		this.opcCompletar.setCaption("Completar");    		
	    	}
	    	else if (((MapeoProgramacion) r_fila).getEstado().equals("T"))
	    	{
	    		this.opcCompletar.setCaption("Reabrir");    		
	    	}
	    	else if (((MapeoProgramacion) r_fila).getEstado().equals("N"))
	    	{
	    		this.opcCompletar.setCaption("Validar");    		
	    	}
	    	else 
	    	{
	    		if (this.permisos==99)
	    		{
	    			this.opcCompletar.setCaption("Aceptar");
	    		}
	    		else
	    		{
	    			this.opcCompletar.setCaption("");
	    			this.opcCompletar.setEnabled(false);
	    		}
	    	}
    	}
	}

	private int obtenerIndice(MapeoProgramacion r_mapeo)
	{
		
		for (int i = 0; i<((ProgramacionGrid) grid).getContainerDataSource().getItemIds().size();i++)
		{
			MapeoProgramacion mapeoLeido = (MapeoProgramacion) ((ProgramacionGrid) grid).getContainer().getIdByIndex(i);
			if (mapeoLeido.equals(r_mapeo))
			{
				return i;
			}
		}

		return -1;
	}

	private void ordenarFilas(String r_direccion)
	{
		int filaOrigen = -1;
		int filaDestino = -1;
		
		if (form!=null) form.cerrar();
		
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = true;
		
		if (grid.getSelectedRow()!=null)
		{
			MapeoProgramacion mapeoOrigen=(MapeoProgramacion) grid.getSelectedRow();
			
			filaOrigen = obtenerIndice(mapeoOrigen);
			
			if (r_direccion.equals("subir")) 
			{
				filaDestino = filaOrigen - 1;
			}
			else
			{
				filaDestino = filaOrigen + 1;
			}
			
    		try
    		{
    			
        		MapeoProgramacion mapeoDestino = (MapeoProgramacion)  (MapeoProgramacion) ((ProgramacionGrid) grid).getContainer().getIdByIndex(filaDestino);
        		
        		Integer origen = mapeoOrigen.getOrden();
//        		String origenLote =mapeoOrigen.getLote();
//        		String origenVino =mapeoOrigen.getVino();
//        		Integer origenCantidad =mapeoOrigen.getCantidad();
        		
        		Integer destino = mapeoDestino.getOrden();
//        		String destinoLote =mapeoDestino.getLote();
//        		String destinoVino =mapeoDestino.getVino();
//        		Integer destinoCantidad =mapeoDestino.getCantidad();
        		
        		mapeoOrigen.setOrden(destino);
//        		mapeoOrigen.setLote(destinoLote);
//        		mapeoOrigen.setVino(destinoVino);
//        		mapeoOrigen.setCantidad(destinoCantidad);
        		
        		mapeoDestino.setOrden(origen);
//        		mapeoDestino.setLote(origenLote);
//        		mapeoDestino.setVino(origenVino);
//        		mapeoDestino.setCantidad(origenCantidad);
        		
        		((ProgramacionGrid) grid).guardarCambios(mapeoOrigen, mapeoDestino);
        		((ProgramacionGrid) grid).select(grid.getContainerDataSource().getIdByIndex(filaDestino));
        		((ProgramacionGrid) grid).scrollTo(mapeoOrigen);

        		
    		}
			catch (IndexOutOfBoundsException ex)
			{
				
			}
    		
    		this.enviarMail(true);
		}
		
		
		
	}
	
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}
	public void enviarMail(boolean r_mail)
	{
		
		if (eBorsao.get().accessControl.getNombre().contains("Claveria"))
		{
			Integer sem = new Integer(this.cmbSemana.getValue().toString());
			if (sem < RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY())+2) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Recuerda enviar mail a todos");
	
				//			VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Enviamos mail notificando cambios?", "Si", "No", "mail", null);
				//			getUI().addWindow(vt);
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("Recuerda enviar mail a todos");
			aceptarProceso("mail");
		}
	}
	public void imprimirProgramacion(boolean r_completa, boolean r_mail)
	{
		String pdfGenerado = null;
		String listaLineasSeleccionados = "";
		
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		if (!r_completa && ((ProgramacionGrid) grid).getSelectedRows()!=null && ((ProgramacionGrid) grid).getSelectedRows().size()!=0)
		{
			Iterator it = null;
			
			it = ((ProgramacionGrid) grid).getSelectedRows().iterator();
			
			while (it.hasNext())
			{			
				MapeoProgramacion mapeo = (MapeoProgramacion) it.next();
				if (listaLineasSeleccionados.length()==0)
				{
					listaLineasSeleccionados = mapeo.getIdProgramacion().toString();
				}
				else
				{
					listaLineasSeleccionados = listaLineasSeleccionados + "," + mapeo.getIdProgramacion().toString();
				}
			}
			pdfGenerado = cps.imprimirProgramacion(listaLineasSeleccionados);
			
			if(pdfGenerado!=null && pdfGenerado.length()>0)
	    	{
				RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
	    	}	    	
		}
		else
		{
			if (this.txtEjercicio.getValue()!=null && this.cmbSemana.getValue()!=null)
			{
				if (r_mail)
				{
					pdfGenerado = cps.imprimirProgramacion(this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString(), "");
					
					if(pdfGenerado.length()>0)
					{
						HashMap adjuntos = new HashMap();
						
						adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
						adjuntos.put("nombre", "programacion semana - " + this.cmbSemana.getValue().toString() + ".pdf");
						ArrayList<String> destinatariosProgramacion = new ArrayList<String>();

						//produccion
						destinatariosProgramacion.add("j.gracia@bodegasborsao.com");
						destinatariosProgramacion.add("embotelladora@bodegasborsao.com");
						//calidad
						destinatariosProgramacion.add("c.gracia@bodegasborsao.com");
						destinatariosProgramacion.add("f.sebastian@bodegasborsao.com");
						//compras
						destinatariosProgramacion.add("compras@bodegasborsao.com");
						//almacen
						destinatariosProgramacion.add("almacen@bodegasborsao.com");
						//comercial
						destinatariosProgramacion.add("manuel@bodegasborsao.com");
						
						ClienteMail.getInstance().envioMail("j.claveria@bodegasborsao.com",destinatariosProgramacion,"Programacion Semana" + this.cmbSemana.getValue().toString(), "Adjunto envío programacion prevista", adjuntos, false);
					}
					else
						Notificaciones.getInstance().mensajeInformativo("No hay datos a imrpimir.");
				}
				else
				{
					PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion("Programacion", txtEjercicio.getValue(), cmbSemana.getValue().toString());
					getUI().addWindow(vtPeticion);
				}
			}
			else
				Notificaciones.getInstance().mensajeInformativo("Cumplimenta correctamente los datos.");
		}

	}
	
	public void rechazoMateriales(MapeoProgramacion r_mapeo)
	{
		 
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		consultaMermasPtServer cms = consultaMermasPtServer.getInstance(CurrentUser.get());
		
		String permisos = cms.comprobarAccesos();
		if (permisos!=null )
		{
			int perm = new Integer(permisos);
			
			if (perm>=40)
			{
				PeticionMermasPt vtMermasPt = new PeticionMermasPt(r_mapeo);
				getUI().addWindow(vtMermasPt);
			}
		}

	}

	public void sincronizarOF(MapeoProgramacion r_mapeo)
	{
		ArrayList<MapeoSincronizarTablas> arr =null;
		servidorSincronizadorBBDD ss = null;
		
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		if (form!=null) form.cerrar();
		
		if (r_mapeo.getEstado().contentEquals("A") || r_mapeo.getEstado().contentEquals("N"))
		{
			if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
			{
				boolean correcto = this.cus.comprobarProgramadoSGA(r_mapeo.getEjercicio().toString().trim(), r_mapeo.getSemana().trim(), r_mapeo.getOrden().toString().trim());
				if (!correcto)
					Notificaciones.getInstance().mensajeError("No sincronizo. Comprueba los lotes de fabricación");
				else
				{
					arr = new ArrayList<MapeoSincronizarTablas>(); 
					MapeoSincronizarTablas mapeo = this.rellenarMapeoProgramacion(r_mapeo.getEjercicio().toString().trim(), r_mapeo.getSemana().trim(), r_mapeo.getOrden().toString().trim(), false);
					mapeo.setBbddDestino("Objective");
					mapeo.setManual(true);
					arr.add(mapeo);
					
					ss = new servidorSincronizadorBBDD(arr,false,true);
					
					if (ss.exito)
					{
						for (int i = 0; i < arr.size(); i++)
						{
							mapeo = (MapeoSincronizarTablas) arr.get(i);
							ss.tocarSemaforoSGA(null,mapeo.getTablaDestino(), 1);
							Integer est = this.cus.obtenerStatus(r_mapeo.getIdProgramacion());
							this.cus.modificarEstadoProgramacion(r_mapeo.getIdProgramacion(), "L", est);
						}
					}
				}
				lanzarWebSincronizacionMes();
			}
		}
		else
		{
			if (r_mapeo.getEstado().contentEquals("T"))
				Notificaciones.getInstance().mensajeError("Esta orden no se puede lanzar, está terminada.");
			else if (r_mapeo.getEstado().contentEquals("L"))
				Notificaciones.getInstance().mensajeError("Esta orden ya está lanzada.");
			else if (r_mapeo.getEstado().contentEquals("P"))
				Notificaciones.getInstance().mensajeError("Esta orden está pendiente de validar por planificacion.");
		}
	}

	private void lanzarWebSincronizacionMes()
	{
		String url = "http://192.168.6.22:1880/ui/#!/7?socketid=HI6K93npucE1xKZvAAJ3";
		getUI().getPage().open(url, "_blank");

	}

	public void controlEmbotellado(MapeoProgramacion r_mapeo)
	{
		
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		PantallaControlEmbotellado vtPeticionContrEmb = new PantallaControlEmbotellado(r_mapeo, "Control Embotellado " + r_mapeo.getDescripcion() );
		getUI().addWindow(vtPeticionContrEmb);

	}

	public void importarExcel()
	{
		
		((ProgramacionGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
//		PeticionImportacionExcel vtPeticion = new PeticionImportacionExcel();
//		getUI().addWindow(vtPeticion);
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
		Table table = new Table();
		table.setContainerDataSource(this.grid.getContainerDataSource());		
		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
		table.setVisibleColumns(columns);
		
		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	public void destructor()
	{
		ces=null;
		
		if (eBorsao.get().accessControl.getNombre().contains("carretillero"))
		{
            VaadinSession.getCurrent().getSession().invalidate();
            Page.getCurrent().reload();
            System.out.println("Usuario desconectado: " + eBorsao.get().accessControl.getNombre() + " " + new Date());
		}
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		switch (r_accion)
		{
			case "mail":
			{
				consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
//				String pdfGenerado = cps.imprimirProgramacion(this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString(), "");
				
//				if(pdfGenerado.length()>0)
				{
					HashMap adjuntos = new HashMap();
					
//					adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
//					adjuntos.put("nombre", "programacion semana - " + this.cmbSemana.getValue().toString() + ".pdf");
					ArrayList<String> destinatariosProgramacion = new ArrayList<String>();
					
					//produccion
					destinatariosProgramacion.add("j.gracia@bodegasborsao.com");
					destinatariosProgramacion.add("embotelladora@bodegasborsao.com");
					//calidad
					destinatariosProgramacion.add("f.sebastian@bodegasborsao.com");
					//compras
					destinatariosProgramacion.add("compras@bodegasborsao.com");
					//almacen
					destinatariosProgramacion.add("almacen@bodegasborsao.com");
					//comercial
					destinatariosProgramacion.add("manuel@bodegasborsao.com");

					ClienteMail.getInstance().envioMailPrueba("j.claveria@bodegasborsao.com",null,"Programacion Semana" + this.cmbSemana.getValue().toString(), "Adjunto envío programacion prevista semanal por " + CurrentUser.get(), null, false);
				}
				break;
			}
			case "OF":
			{
				sincronizarOF((MapeoProgramacion) grid.getSelectedRow());
			}
			default:
			{
				if (this.form!=null)
				{
					form.removeStyleName("visible");
					form.cerrar();
				}
				break;
			}
				
		}
	}

	private MapeoSincronizarTablas rellenarMapeoProgramacion(String r_valor, String r_valor2, String r_valor3, boolean r_volcarTodo)
	{
		MapeoSincronizarTablas mapeo =new MapeoSincronizarTablas();
		String campoId="ejercicio";
		String campoId2="semana";
		String campoId3="orden";
		mapeo.setTablaOrigen("prd_programacion");
		mapeo.setBbddOrigen("MySQL");
		mapeo.setTablaDestino("INTRANET");

		mapeo.setCamposOrigen("concat(ejercicio, lpad(semana,2,'0'),lpad(orden,3,'0')) as idprd_programacion,semana, fecha, tipoOperacion, lote_pt, articulo, unidades, velocidad, lote_jaulon_1 as lote_jaulon, anada, (case when articulo in ('0102091','0102006') then '1.5' else '1' end) as maquina");
		mapeo.setSql("");
		if (!r_volcarTodo)
			if (r_valor3!=null && r_valor3.length()>0)
				mapeo.setSql(" where " + campoId + " = (" + r_valor.trim() + ")  and " + campoId2 + " = (" + r_valor2 + ") and " + campoId3 + " = (" + r_valor3 + ") and estado in ('A','N') and length(articulo)>0 ");
			else
				mapeo.setSql(" where " + campoId + " = (" + r_valor.trim() + ")  and " + campoId2 + " = (" + r_valor2 + ") and estado in ('A','N') and length(articulo)>0 ");
		else
			mapeo.setSql(" where estado in ('A','N') and length(articulo)>0");

		return mapeo;
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	/*
	 * TODO. A los datos de la semana devolver siempre los retrabajos pendientes, sean de la semana que sean
	 * 
	 */
}



/*
 * update a_lin_0 set familia ="100",sub_familia="010" 
	where articulo in (select codigo   from e__grp_l 
	where grupo  = "110")
 * 	
 * update e_articu set familia ="100",sub_familia="030" 
	where articulo in (select codigo   from e__grp_l 
	where grupo  = "110") 
 */
