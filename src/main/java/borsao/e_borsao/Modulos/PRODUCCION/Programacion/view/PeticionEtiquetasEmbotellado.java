package borsao.e_borsao.Modulos.PRODUCCION.Programacion.view;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.ProgramacionGrid;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasEmbotellado extends Window
{
	private MapeoProgramacion mapeoProgramacion = null;
	private ProgramacionGrid App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private ComboBox cmbTipoOrden = null;
	private ComboBox cmbPrinter = null;
	private ComboBox cmbPaletCompleto = null;
	private TextField txtArticulo = null;
	private TextField txtDescripcion= null;
	private TextField txtLote= null;
	private TextField txtAnada = null;
	private TextField txtCajas = null;
	private TextField txtFactor = null;
	private TextField txtCopias = null;
	/*
	 * Valores devueltos
	 */
	private Integer cajas = null;
	private String cantidad = null;
	private String anada = null;
	private boolean paletCompleto = false;
	
	public PeticionEtiquetasEmbotellado(GridPropio r_App, MapeoProgramacion r_mapeo, String r_titulo)
	{
		this.mapeoProgramacion = r_mapeo;
		this.App = (ProgramacionGrid) r_App;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtArticulo=new TextField("Articulo");
				this.txtArticulo.setValue(r_mapeo.getArticulo());
				this.txtArticulo.setWidth("200px");
				this.txtArticulo.setRequired(true);
				this.txtDescripcion=new TextField("Descripcion");
				this.txtDescripcion.setValue(r_mapeo.getDescripcion());
				this.txtDescripcion.setWidth("400px");
				this.txtDescripcion.setRequired(true);
				fila1.addComponent(txtArticulo);
				fila1.addComponent(txtDescripcion);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.cmbTipoOrden=new ComboBox("Tipo");
				this.cmbTipoOrden.addItem("EMBOTELLADO");
				this.cmbTipoOrden.addItem("ETIQUETADO");
				this.cmbTipoOrden.addItem("PALETA");
				this.cmbTipoOrden.addItem("ET. PALETA");				
				this.cmbTipoOrden.setInvalidAllowed(false);
				this.cmbTipoOrden.setNewItemsAllowed(false);
				this.cmbTipoOrden.setNullSelectionAllowed(false);
				this.cmbTipoOrden.setRequired(true);
				if (!r_mapeo.getDescripcion().startsWith("1/2"))
				{
					this.cmbTipoOrden.setValue(r_mapeo.getTipo().toUpperCase().trim());
				}
				else
				{
					if (r_mapeo.getTipo().toUpperCase().equals("EMBOTELLADO"))
						this.cmbTipoOrden.setValue("PALETA");
					else
						this.cmbTipoOrden.setValue("ET. PALETA");
				}
				this.cmbTipoOrden.setWidth("200px");
				this.txtCajas=new TextField("Cajas ");				
				this.txtCajas.setWidth("150px");
				this.txtCajas.setRequired(true);
				this.txtFactor=new TextField("Bot. / Caja ");
				this.txtFactor.setValue(this.recogerTipoCaja().toString().trim());
				this.txtFactor.setWidth("150px");
				fila2.addComponent(cmbTipoOrden);
				fila2.addComponent(txtCajas);
				fila2.addComponent(txtFactor);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
				this.txtLote=new TextField("Lote");
//				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
//				{
//					this.txtLote.setValue(r_mapeo.getLote().substring(2) + r_mapeo.getArticulo().substring(7-(10-r_mapeo.getLote().substring(2).length())));
//				}
//				else
//				{
//				}
				
				if (r_mapeo.getTipo().toUpperCase().equals("EMBOTELLADO"))
				{
					if ((r_mapeo.getLote()==null || r_mapeo.getLote().length()==0))
					{
						String lote = null;
						lote = this.buscarLote(this.mapeoProgramacion);
						if (lote!=null)						
						{
							
							this.txtLote.setValue(lote.substring(2) + r_mapeo.getArticulo().substring(7-(10-lote.substring(2).length())));
						}
						else
						{
							this.txtLote.setValue(RutinasFechas.añoActualYY() + RutinasFechas.mesActualMM() + RutinasFechas.diaActualDD() + r_mapeo.getArticulo().substring(3));
						}
					}
					else
					{
						this.txtLote.setValue(r_mapeo.getLote().substring(2) + r_mapeo.getArticulo().substring(7-(10-r_mapeo.getLote().substring(2).length())));
					}
				}
				
				this.txtLote.setWidth("200px");
				this.txtLote.setRequired(true);
				this.txtAnada=new TextField("Añada");
				this.txtAnada.setWidth("150px");
				this.txtAnada.setRequired(true);
				this.cmbPaletCompleto=new ComboBox("Palet Completo / Palet Parcial");
				this.cmbPaletCompleto.addItem("COMPLETO");
				this.cmbPaletCompleto.addItem("PARCIAL");
				this.cmbPaletCompleto.setWidth("200px");
				this.cmbPaletCompleto.setRequired(true);
				this.cmbPaletCompleto.setNullSelectionAllowed(false);
				this.cmbPaletCompleto.setInvalidAllowed(false);
				this.cmbPaletCompleto.setNewItemsAllowed(false);
				
				fila3.addComponent(txtLote);
				fila3.addComponent(txtAnada);
				fila3.addComponent(cmbPaletCompleto);

			HorizontalLayout fila4 = new HorizontalLayout();
			fila4.setWidthUndefined();
			fila4.setSpacing(true);
				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);				
				this.cmbPrinter.setWidth("200px");
				
				this.txtCopias=new TextField("Copias ");				
				this.txtCopias.setWidth("70px");
				this.txtCopias.setRequired(true);
				this.txtCopias.setValue("1");
				if (comprobarArticulo(txtArticulo.getValue()))
				{
					txtCopias.setValue("2");
				}
				fila4.addComponent(cmbPrinter);
				fila4.addComponent(txtCopias);
				
		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		controles.addComponent(fila4);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
		this.txtCajas.focus();
	}

	private void habilitarCampos()
	{
	
		this.txtArticulo.setEnabled(true);
		this.txtDescripcion.setEnabled(true);
		
		this.txtFactor.setEnabled(true);
		this.cmbTipoOrden.setEnabled(true);
		this.cmbPrinter.setEnabled(true);
		this.txtCajas.setEnabled(true);
		this.txtLote.setEnabled(true);			
		this.txtAnada.setEnabled(true);			
	}
	
	private void cargarListeners()
	{

		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					cajas=new Integer(txtCajas.getValue());
					anada=txtAnada.getValue();
					
					if (cmbPaletCompleto.getValue().equals("COMPLETO")) paletCompleto=true; else paletCompleto=false;
					
					MapeoProgramacion newMapeo = new MapeoProgramacion();
					
					newMapeo.setArticulo(txtArticulo.getValue());
					newMapeo.setDescripcion(txtDescripcion.getValue());
					newMapeo.setFactor(new Integer(txtFactor.getValue()));
					
					newMapeo.setBajar(mapeoProgramacion.getBajar());
					newMapeo.setBiblia(mapeoProgramacion.getBiblia());
					newMapeo.setCaja(mapeoProgramacion.getCaja());
					newMapeo.setCajas(mapeoProgramacion.getCajas());
					newMapeo.setCantidad(mapeoProgramacion.getCantidad());
					newMapeo.setCapsulas(mapeoProgramacion.getCapsulas());
					newMapeo.setContras(mapeoProgramacion.getContras());
					newMapeo.setDia(mapeoProgramacion.getDia());
					newMapeo.setEjercicio(mapeoProgramacion.getEjercicio());
					newMapeo.setEsc(mapeoProgramacion.getEsc());
					newMapeo.setEstado(mapeoProgramacion.getEstado());
					newMapeo.setFrontales(mapeoProgramacion.getFrontales());
					newMapeo.setIdCodigo(mapeoProgramacion.getIdCodigo());
					newMapeo.setIdProgramacion(mapeoProgramacion.getIdProgramacion());
					newMapeo.setLote(mapeoProgramacion.getLote());
					newMapeo.setNuevo(mapeoProgramacion.getNuevo());
					newMapeo.setObservaciones(mapeoProgramacion.getObservaciones());
					newMapeo.setOjo(mapeoProgramacion.getOjo());
					newMapeo.setPapel(mapeoProgramacion.getPapel());
					newMapeo.setSemana(mapeoProgramacion.getSemana());
					newMapeo.setStatus(mapeoProgramacion.getStatus());
					newMapeo.setSubir(mapeoProgramacion.getSubir());
					newMapeo.setTapones(mapeoProgramacion.getTapones());
					newMapeo.setTipo(mapeoProgramacion.getTipo());
					newMapeo.setUnidades(mapeoProgramacion.getUnidades());
					newMapeo.setVino(mapeoProgramacion.getVino());
					App.generacionPdf(newMapeo, txtLote.getValue(), cmbTipoOrden.getValue().toString(), cajas, anada, cantidad, paletCompleto, true, cmbPrinter.getValue().toString(),new Integer(txtCopias.getValue()));
					/*
					 * tras llamar a la generacion cierro la ventana
					 */
//					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
		
		ValueChangeListener lis = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtCajas.getValue()!="" & txtFactor.getValue()!="")
				{
					if (txtCajas.getValue().equals("1"))
					{
						txtFactor.setEnabled(true);
//						cmbPaletCompleto.setValue("COMPLETO");
						cantidad= String.valueOf(new Integer(txtCajas.getValue())*new Integer(txtFactor.getValue()));
						cantidad = RutinasCadenas.formatCerosIzquierda(cantidad, 4);
					}
					else
					{
//						if (App.permisos!=99) txtFactor.setEnabled(false);
						txtFactor.setValue(recogerTipoCaja().toString());
						cantidad= String.valueOf(new Integer(txtCajas.getValue())*new Integer(txtFactor.getValue()));
						cantidad = RutinasCadenas.formatCerosIzquierda(cantidad, 4);
						
						if (esPaletCompleto(mapeoProgramacion.getArticulo(), new Integer(cantidad)))
						{
//							cmbPaletCompleto.setValue("COMPLETO");
						}
						else
						{
//							cmbPaletCompleto.setValue("PARCIAL");
							if (cmbPaletCompleto.getValue()!=null && cmbPaletCompleto.getValue().equals("COMPLETO"))
								Notificaciones.getInstance().mensajeAdvertencia("Ojo!! No coincide la cantidad con la paletización");
						}
					}
				}
			}
		};
		
		txtCajas.addValueChangeListener(lis);
		
		ValueChangeListener lisFac = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtFactor.getValue()!="" & txtCajas.getValue()!="")
				{
					cantidad= String.valueOf(new Integer(txtCajas.getValue())*new Integer(txtFactor.getValue()));
					cantidad = RutinasCadenas.formatCerosIzquierda(cantidad, 4);
//					cmbPaletCompleto.setValue("COMPLETO");
					
					if (esPaletCompleto(mapeoProgramacion.getArticulo(), new Integer(cantidad)))
					{
//						cmbPaletCompleto.setValue("COMPLETO");
					}
					else
					{
//						cmbPaletCompleto.setValue("PARCIAL");
						if (cmbPaletCompleto.getValue()!=null && cmbPaletCompleto.getValue().equals("COMPLETO"))
							Notificaciones.getInstance().mensajeAdvertencia("Ojo!! No coincide la cantidad con la paletización");
					}
				}
				
			}
		};
		txtFactor.addValueChangeListener(lisFac);
		
		ValueChangeListener lisArt = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtArticulo.getValue()!="" & txtArticulo.getValue()!="")
				{
					consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
					txtDescripcion.setValue(cas.obtenerDescripcionArticulo(txtArticulo.getValue()));	
					
					txtFactor.setValue(recogerTipoCaja().toString());
					if (txtLote!=null)
					{
						if (txtLote.getValue().substring(6).startsWith("2"))
						{
							txtLote.setValue(txtLote.getValue().substring(0, 6) + txtArticulo.getValue().substring(3));
						}
						else
						{
							txtLote.setValue(txtLote.getValue().substring(0, 7) + txtArticulo.getValue().substring(4));
						}
					}
					if (comprobarArticulo(txtArticulo.getValue()))
					{
						txtCopias.setValue("2");
					}
				}
			}
		};
		txtArticulo.addValueChangeListener(lisArt);
		
		ValueChangeListener lisLote = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (txtLote.getValue()!="" & txtLote.getValue()!="")
				{
					if (txtLote!=null && (!txtLote.getValue().endsWith(txtArticulo.getValue().substring(5))))
					{
						Notificaciones.getInstance().mensajeError("El lote no es correcto");
						txtLote.focus();
					}
				}
			}
		};
		txtLote.addValueChangeListener(lisLote);

		ValueChangeListener lisPaletizacion = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbPaletCompleto.getValue()!="" && txtFactor.getValue()!="" && txtCajas.getValue()!="" )
				{
					cantidad= String.valueOf(new Integer(txtCajas.getValue())*new Integer(txtFactor.getValue()));
					cantidad = RutinasCadenas.formatCerosIzquierda(cantidad, 4);
					
					if (esPaletCompleto(mapeoProgramacion.getArticulo(), new Integer(cantidad)))
					{
//						cmbPaletCompleto.setValue("COMPLETO");
					}
					else
					{
//						cmbPaletCompleto.setValue("PARCIAL");
						if (cmbPaletCompleto.getValue()!=null && cmbPaletCompleto.getValue().equals("COMPLETO"))
							Notificaciones.getInstance().mensajeAdvertencia("Ojo!! No coincide la cantidad con la paletización");
					}
				}
			}
		};
		cmbPaletCompleto.addValueChangeListener(lisPaletizacion);

	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.cmbTipoOrden.getValue()==null || this.cmbTipoOrden.getValue().toString().length()==0)
		{
			devolver = false;
			cmbTipoOrden.focus();
		}
		else if (this.cmbPrinter.getValue()==null || this.cmbPrinter.getValue().toString().length()==0)
		{
			devolver = false;
			cmbPrinter.focus();
		}
		else if (this.txtCajas.getValue()==null || this.txtCajas.getValue().length()==0) 
		{
			devolver =false;			
			txtCajas.focus();
		}
		else if (this.txtLote.getValue()==null || this.txtLote.getValue().length()==0) 
		{
			devolver =false;
			txtLote.focus();
		}
		else if (!this.txtLote.getValue().endsWith(this.txtArticulo.getValue().substring(5)))
		{
			devolver =false;
			txtLote.focus();			
		}
		else if (this.txtAnada.getValue()==null || this.txtAnada.getValue().length()==0)
		{
			devolver =false;
			txtAnada.focus();
		}
		else if (this.cmbPaletCompleto.getValue()==null || this.cmbPaletCompleto.getValue().toString().length()==0) 
		{
			devolver =false;
			cmbPaletCompleto.focus();
		}
		return devolver;
	}

	/*
	 * Metodo que nos dirá si estamos en palet completo o en palet parcial
	 * 
	 * devuelve: 	true en caso de palet completo
	 * 				false en caso de palet parcial 
	 */
	private boolean esPaletCompleto(String r_articulo, Integer r_cantidad)
	{
		Integer cantidadReferencia = null;
		
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		cantidadReferencia = ces.obtenerCantidadPaletEmbotelladora(r_articulo);
		this.recogerTipoPalet();
		if (r_cantidad.equals(cantidadReferencia)) return true; else return false;
	}

	private Integer recogerTipoCaja()
	{
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		return ces.recogerTipoCaja(this.txtArticulo.getValue());
	}
	
	private void recogerTipoPalet()
	{
		Integer tipo=0;
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		tipo= ces.recogerTipoPalet(this.mapeoProgramacion.getArticulo());
		if (tipo!=null && tipo==2) this.cmbTipoOrden.setValue("PALET");
	}

	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		
		this.cmbPrinter.setValue(defecto);
	}

	private String buscarLote (MapeoProgramacion r_mapeo)
	{
		String lote = null;
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		lote = cps.buscarLote(r_mapeo);
		
		return lote;
	}
	
	private boolean comprobarArticulo(String r_articulo)
	{
		consultaArticulosPTEnvasadoraServer captes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
		return captes.articuloEnvasadora(r_articulo);
	}
}