package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.view.OpcionesPTEView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = true;
	private GridViewRefresh app = null;
	
    public OpcionesGrid(OpcionesPTEView r_app, ArrayList<MapeoArticulosEnvasadora> r_vector) 
    {
    	this.app = r_app;
        
        this.vector=r_vector;
		this.asignarTitulo("Articulos Producto Termiando Envasadora");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoArticulosEnvasadora.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo","descripcion", "alias", "calculaCon", "peso","dias");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "300");
    	this.widthFiltros.put("alias", "300");
    	this.widthFiltros.put("calculaCon", "70");
    	this.widthFiltros.put("dias", "70");
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("alias").setHeaderCaption("Alias");
    	this.getColumn("calculaCon").setHeaderCaption("Calcula Con");
    	this.getColumn("dias").setHeaderCaption("Dias Bodega");
    	this.getColumn("peso").setHeaderCaption("Peso");
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "cell-normal";
            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
		this.camposNoFiltrar.add("peso");
	}

	@Override
	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		if (this.footer==null) this.footer=this.appendFooterRow();
		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");

	}

}
