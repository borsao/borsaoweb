package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTiemposEnvasadora extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	private Integer idProgramacion;
	private String tipo;
	private String descripcion;
	private String observaciones;
	private String turno;
	private Date fecha;
	private Double tiempo;
	private boolean eliminar;
	

	public MapeoTiemposEnvasadora()
	{
	}


	public Integer getIdProgramacion() {
		return idProgramacion;
	}


	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public Double getTiempo() {
		return tiempo;
	}


	public void setTiempo(Double tiempo) {
		this.tiempo = tiempo;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public Integer getSemana() {
		return semana;
	}


	public void setSemana(Integer semana) {
		this.semana = semana;
	}


	public String getTurno() {
		return turno;
	}


	public void setTurno(String turno) {
		this.turno = turno;
	}

}