package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view.ControlTurnoEnvasadoraView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = true;
	public boolean actualizar = false;
	private GridViewRefresh app = null;	
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
    public OpcionesGrid(ControlTurnoEnvasadoraView r_app, ArrayList<MapeoControlTurnoEnvasadora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();

        this.asignarTitulo("Horas Turno");
        
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}

    }
    
    private void generarGrid()
    {
    	
        this.crearGrid(MapeoControlTurnoEnvasadora.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
//		this.addStyleName("minigrid");
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	/*
    	 * 	private String operario;
			private Double horas;
			private Double fueraLina;
			private Double ausencia;
			private String conceptoFueraLinea;
			private String conceptoAusencia;

    	 */
    	setColumnOrder("dia", "fecha", "turno", "operario", "horas", "fueraLina","conceptoFueraLinea", "ausencia", "conceptoAusencia");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("dia", "80");
    	this.widthFiltros.put("turno", "60");
    	
    	this.getColumn("dia").setHeaderCaption("Dia");
    	this.getColumn("dia").setWidth(new Double(120));
    	
    	this.getColumn("dia").setHeaderCaption("Dia");
    	this.getColumn("dia").setWidth(new Double(120));
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(120));
    	this.getColumn("turno").setHeaderCaption("Turno");
    	this.getColumn("turno").setWidth(new Double(100));
    	this.getColumn("operario").setHeaderCaption("Operario");
    	this.getColumn("operario").setWidth(new Double(200));
    	this.getColumn("horas").setHeaderCaption("Horas");
    	this.getColumn("horas").setWidth(new Double(80));
    	this.getColumn("fueraLina").setHeaderCaption("Fuera Linea");
    	this.getColumn("fueraLina").setWidth(new Double(80));
    	this.getColumn("conceptoFueraLinea").setHeaderCaption("Descripcion");
    	this.getColumn("conceptoFueraLinea").setWidth(new Double(250));
    	this.getColumn("ausencia").setHeaderCaption("Ausencia");
    	this.getColumn("ausencia").setWidth(new Double(80));
    	this.getColumn("conceptoAusencia").setHeaderCaption("Concepto");
    	this.getColumn("conceptoAusencia").setWidth(new Double(250));
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("mapeo").setHidden(true);
    	this.getColumn("eliminar").setHidden(true);
    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    }

    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("horas".equals(cellReference.getPropertyId()) || "fueraLina".equals(cellReference.getPropertyId()) || "ausencia".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
        			return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	/*
            	 * al seleccionar registro mostraremos paradas de linea del dia y turno seleccionados
            	 */
    		}
        });
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("fecha");
		this.camposNoFiltrar.add("horas");
		this.camposNoFiltrar.add("fueraLina");
		this.camposNoFiltrar.add("ausencia");
		this.camposNoFiltrar.add("operario");
		this.camposNoFiltrar.add("conceptoFueraLinea");
		this.camposNoFiltrar.add("conceptoAusencia");
	}

	@Override
	public void calcularTotal() {
		
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-20)+"%");
	}
	
}

