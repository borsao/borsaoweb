package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.server.consultaInventarioAlmacenServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.InventarioAlmacenView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.PantallaCargasVinoMesa;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.PeticionEtiquetasPalletsMercadona;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view.pantallaPedidosSemana;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private GridViewRefresh app = null;
	private PantallaCargasVinoMesa win = null;	
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
    public OpcionesGrid(InventarioAlmacenView r_app, ArrayList<MapeoInventarioAlmacen> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.generarGrid();
    }
    
    
    public OpcionesGrid(PantallaCargasVinoMesa r_app, ArrayList<MapeoInventarioAlmacen> r_vector) 
    {
        this.vector=r_vector;
        this.win=r_app;
        this.setSizeFull();
		this.generarGrid();
    }
    
    private void generarGrid()
    {
        this.crearGrid(MapeoInventarioAlmacen.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(5);
		this.addStyleName("minigrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("cumplimentado", "ejercicio", "semana", "idPallet", "referencia","fechaDocumento", "tinto5", "rosa5", "tinto72", "rosa72", "media5", "tinto2", "rosa2", "tinto60", "rosa60", "media2", "oro","mediaOro","mitica", "mediaMitica", "campellas","bole","borsao", "mallacan", "tresla", "cliente", "kilos", "codigoPedido");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("idPallet").setHeaderCaption("");
    	this.getColumn("idPallet").setWidth(new Double(50));
    	this.getColumn("referencia").setHeaderCaption("Pedido");
    	this.getColumn("referencia").setWidth(new Double(70));
    	this.getColumn("fechaDocumento").setHeaderCaption("Fecha");
    	this.getColumn("fechaDocumento").setWidth(new Double(80));    	
    	this.getColumn("tinto5").setHeaderCaption("Tinto 5L");
    	this.getColumn("tinto5").setWidth(new Double(55));
    	this.getColumn("rosa5").setHeaderCaption("Rosa 5L");
    	this.getColumn("rosa5").setWidth(new Double(55));
    	this.getColumn("tinto72").setHeaderCaption("Tinto 72C");
    	this.getColumn("tinto72").setWidth(new Double(65));
    	this.getColumn("rosa72").setHeaderCaption("Rosa 72C");
    	this.getColumn("rosa72").setWidth(new Double(65));
    	this.getColumn("media5").setHeaderCaption("1/2 PALETA");
    	this.getColumn("media5").setWidth(new Double(75));
    	
    	this.getColumn("tinto2").setHeaderCaption("Tinto 2L");
    	this.getColumn("tinto2").setWidth(new Double(55));
    	this.getColumn("rosa2").setHeaderCaption("Rosa 2L");
    	this.getColumn("rosa2").setWidth(new Double(55));
    	this.getColumn("tinto60").setHeaderCaption("Tinto 60C");
    	this.getColumn("tinto60").setWidth(new Double(65));
    	this.getColumn("rosa60").setHeaderCaption("Rosa 60C");
    	this.getColumn("rosa60").setWidth(new Double(65));
    	this.getColumn("media2").setHeaderCaption("1/2 PALETA");
    	this.getColumn("media2").setWidth(new Double(75));
    	
    	this.getColumn("oro").setHeaderCaption("ORO");
    	this.getColumn("oro").setWidth(new Double(55));
    	this.getColumn("mediaOro").setHeaderCaption("1/2 ORO");
    	this.getColumn("mediaOro").setWidth(new Double(65));
    	
    	this.getColumn("mitica").setHeaderCaption("MITICA");
    	this.getColumn("mitica").setWidth(new Double(60));
    	this.getColumn("mediaMitica").setHeaderCaption("1/2 MITICA");
    	this.getColumn("mediaMitica").setWidth(new Double(75));
    	
    	this.getColumn("campellas").setHeaderCaption("CAMPE");
    	this.getColumn("campellas").setWidth(new Double(55));
    	this.getColumn("bole").setHeaderCaption("BOLE");
    	this.getColumn("bole").setWidth(new Double(55));
    	this.getColumn("borsao").setHeaderCaption("BORS");
    	this.getColumn("borsao").setWidth(new Double(55));
    	this.getColumn("mallacan").setHeaderCaption("MALLA");
    	this.getColumn("mallacan").setWidth(new Double(55));
    	this.getColumn("tresla").setHeaderCaption("TRESLA");
    	this.getColumn("tresla").setWidth(new Double(55));

    	
    	this.getColumn("cliente").setHeaderCaption("CLIENTE");
    	this.getColumn("cliente").setWidth(new Double(175));
    	this.getColumn("kilos").setHeaderCaption("KILOS");
    	this.getColumn("kilos").setWidth(new Double(70));
    	this.getColumn("codigoPedido").setHeaderCaption("Pedido BOR");
    	this.getColumn("codigoPedido").setWidth(new Double(85));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("semana").setHidden(true);
    	this.getColumn("cumplimentado").setHidden(true);
    	this.getColumn("numeroAlmacen").setHidden(true);
    	this.getColumn("nombreAlmacen").setHidden(true);
    	this.getColumn("idCliente").setHidden(true);
    	
    	this.getColumn("fechaDocumento").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    }

    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		
            boolean cargado = false;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("cumplimentado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().length()>0)
            			cargado=true;
            		else
            			cargado=false;
            	}
            	if ("tresla".equals(cellReference.getPropertyId()) || "mallacan".equals(cellReference.getPropertyId()) || "tinto5".equals(cellReference.getPropertyId()) || "rosa5".equals(cellReference.getPropertyId()) || "tinto72".equals(cellReference.getPropertyId()) || "rosa72".equals(cellReference.getPropertyId()) || "media5".equals(cellReference.getPropertyId())
            		|| "tinto2".equals(cellReference.getPropertyId()) || "rosa2".equals(cellReference.getPropertyId()) || "tinto60".equals(cellReference.getPropertyId()) || "rosa60".equals(cellReference.getPropertyId()) || "media2".equals(cellReference.getPropertyId())
            		|| "oro".equals(cellReference.getPropertyId()) || "mediaOro".equals(cellReference.getPropertyId()) || "mitica".equals(cellReference.getPropertyId()) || "mediaMitica".equals(cellReference.getPropertyId()) 
            		|| "codigoPedido".equals(cellReference.getPropertyId()) || "campellas".equals(cellReference.getPropertyId()) || "bole".equals(cellReference.getPropertyId()) || "borsao".equals(cellReference.getPropertyId()) )
            	{
            		if (cargado)
            			return "Rcell-yellowP";
            		else
            			return "Rcell-pointer";
            	}
            	else if ( "kilos".equals(cellReference.getPropertyId()))
            	{
            		if (cargado)
            			return "Rcell-yellow";
            		else
            			return "Rcell-normal";           		
            	}
            	else if ( "idPallet".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEan";
            	}
            	else
            	{
            		if (cargado)
            			return "cell-warning";
            		else
            			return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoInventarioAlmacen mapeo = (MapeoInventarioAlmacen) event.getItemId();
            	if (event.getPropertyId()!=null)
            	{
            		if (event.getPropertyId().toString().contentEquals("codigoPedido"))
            		{
            			activadaVentanaPeticion=true;
            			ordenando = false;
            			
            			pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(mapeo.getCodigoPedido(), "Lineas Pedido: " + mapeo.getCodigoPedido().toString());
    					getUI().addWindow(vt);
            		}
            		else if (event.getPropertyId().toString().contentEquals("idPallet"))
            		{
            			activadaVentanaPeticion=true;
            			ordenando = false;
            			
            			PeticionEtiquetasPalletsMercadona vt = new PeticionEtiquetasPalletsMercadona((InventarioAlmacenView) app, mapeo);
    					getUI().addWindow(vt);
            		}
            		else
            		{
            			
            			String articulos = null;
            			consultaInventarioAlmacenServer cias = consultaInventarioAlmacenServer.getInstance(CurrentUser.get());
            			
            			switch (event.getPropertyId().toString())
            			{
            			case ("tinto5"):
            			{
            				articulos = "('0103001','0103006')";
            				break;
            			}
            			case ("rosa5"):
            			{
            				articulos = "('0103010')";
            				break;
            			}
            			case ("tinto72"):
            			{
            				articulos = "('0103002','0103003','0103005')";
            				break;
            			}
            			case ("rosa72"):
            			{
            				articulos = "('0103011','0103012')";
            				break;
            			}
            			case ("media5"):
            			{
            				articulos = "('0103004')";
            				break;
            			}
            			
            			case ("tinto2"):
            			{
            				articulos = "('0103100','0103106')";
            				break;
            			}
            			case ("rosa2"):
            			{
            				articulos = "('0103110')";
            				break;
            			}
            			case ("tinto60"):
            			{
            				articulos = "('0103102','0103101')";
            				break;
            			}
            			case ("rosa60"):
            			{
            				articulos = "('0103112','0103111')";
            				break;
            			}
            			case ("media2"):
            			{
            				articulos = "('0103104')";
            				break;
            			}
            			case ("oro"):
            			{
            				articulos = "('0102454','0102463')";
            				break;
            			}
            			case ("mediaOro"):
            			{
            				articulos = "('0102091')";
            				break;
            			}
            			case ("mallacan"):
            			{
            				articulos = "('0102297')";
            				break;
            			}
            			case ("tresla"):
            			{
            				articulos = "('0102340')";
            				break;
            			}
            			case ("mitica"):
            			{
            				articulos = "('0102003','0102464')";
            				break;
            			}
            			case ("bole"):
            			{
            				articulos = "('0102208')";
            				break;
            			}
            			case ("campellas"):
            			{
            				articulos = "('0102246')";
            				break;
            			}
            			case ("borsao"):
            			{
            				articulos = "('0102100')";
            				break;
            			}
            			case ("mediaMitica"):
            			{
            				articulos = "('0102006')";
            				break;
            			}
            			}
            			
            			if (articulos!=null && articulos.length()>0)
            			{
            				String ejer = null;
            				String week = null;
            				
            				if (app!=null)
            				{
            					ejer = ((InventarioAlmacenView)app).txtEjercicio.getValue();
            					week = ((InventarioAlmacenView)app).cmbSemana.getValue().toString();
            				}
            				else
            				{
            					ejer = ((PantallaCargasVinoMesa)win).txtEjercicio.getValue();
            					week = ((PantallaCargasVinoMesa)win).cmbSemana.getValue().toString();            					
            				}
            				HashMap<String, Double> hashPedidos = cias.recuperarMovimientosPedidosEjercicio(new Integer(ejer), articulos);
            				HashMap<String, Double> hashPedidosAnterior = cias.recuperarMovimientosPedidosEjercicio(new Integer(ejer)-1, articulos);
            				
            				/*
            				 * cargo pantalla medias semanales pedidoas
            				 * 
            				 */
            				if (!hashPedidos.isEmpty())
            				{
            					activadaVentanaPeticion=true;
            					ordenando = false;
            					
            					pantallaPedidosSemana vt =  new pantallaPedidosSemana(ejer, week, hashPedidos, hashPedidosAnterior, "Cargas semanales " + event.getPropertyId().toString());
            					getUI().addWindow(vt);
            				}
            				else
            				{
            					activadaVentanaPeticion=false;
            					ordenando = false;
            				}
            			}
            		}
        		}
    		}
        });
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() {
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double total = new Double(0);
		Double totalTinto5 = new Double(0);
		Double totalRosa5 = new Double(0);
		Double totalTinto72 = new Double(0);
		Double totalRosa72 = new Double(0);
		Double totalMP5 = new Double(0);
		
		Double totalTinto2 = new Double(0);
		Double totalRosa2 = new Double(0);
		Double totalTinto60 = new Double(0);
		Double totalRosa60 = new Double(0);
		Double totalMP2 = new Double(0);
		
		Double totalOro = new Double(0);
		Double totalMediaOro = new Double(0);
		Double totalMallacan = new Double(0);
		Double totalMitica = new Double(0);
		Double totalMediaMitica = new Double(0);
		
		Double totalCampellas = new Double(0);
		Double totalTresla = new Double(0);
		Double totalBole = new Double(0);
		Double totalBorsao = new Double(0);
		
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
        		Double q1Value = (Double) item.getItemProperty("tinto5").getValue();
        		Double q2Value = (Double) item.getItemProperty("rosa5").getValue();
        		Double q3Value = (Double) item.getItemProperty("tinto72").getValue();
        		Double q4Value = (Double) item.getItemProperty("rosa72").getValue();
        		Double q5Value = (Double) item.getItemProperty("media5").getValue();
	        	
        		Double q6Value = (Double) item.getItemProperty("tinto2").getValue();
        		Double q7Value = (Double) item.getItemProperty("rosa2").getValue();
        		Double q8Value = (Double) item.getItemProperty("tinto60").getValue();
        		Double q9Value = (Double) item.getItemProperty("rosa60").getValue();
        		Double q10Value = (Double) item.getItemProperty("media2").getValue();
	        	
        		Double q11Value = (Double) item.getItemProperty("oro").getValue();
        		Double q12Value = (Double) item.getItemProperty("mediaOro").getValue();
        		Double q13Value = (Double) item.getItemProperty("mitica").getValue();
        		Double q14Value = (Double) item.getItemProperty("mediaMitica").getValue();
        		
        		Double q15Value = (Double) item.getItemProperty("campellas").getValue();
        		Double q16Value = (Double) item.getItemProperty("bole").getValue();
        		Double q17Value = (Double) item.getItemProperty("borsao").getValue();
        		Double q18Value = (Double) item.getItemProperty("mallacan").getValue();
        		Double q19Value = (Double) item.getItemProperty("tresla").getValue();
	        	
	        	if (q1Value!=null)
	        	{
	        		total+= q1Value;
	        		totalTinto5 += q1Value;
	        	}
	        	if (q2Value!=null)
	        	{
	        		total+= q2Value;
	        		totalRosa5 += q2Value;
	        	}
	        	if (q3Value!=null)
	        	{
	        		total+= q3Value;
	        		totalTinto72 += q3Value;
	        	}
	        	if (q4Value!=null)
	        	{
	        		total+= q4Value;
	        		totalRosa72 += q4Value;
	        	}
	        	if (q5Value!=null)
	        	{
	        		total+= q5Value;
	        		totalMP5 += q5Value;
	        	}
	        	
	        	if (q6Value!=null)
	        	{
	        		total+= q6Value;
	        		totalTinto2 += q6Value;
	        	}
	        	if (q7Value!=null)
	        	{
	        		total+= q7Value;
	        		totalRosa2 += q7Value;
	        	}
	        	if (q8Value!=null)
	        	{
	        		total+= q8Value;
	        		totalTinto60 += q8Value;
	        	}
	        	if (q9Value!=null)
	        	{
	        		total+= q9Value;
	        		totalRosa60 += q9Value;
	        	}
	        	if (q10Value!=null)
	        	{
	        		total+= q10Value;
	        		totalMP2 += q10Value;
	        	}
	        	
	        	if (q11Value!=null)
	        	{
	        		total+= q11Value;
	        		totalOro += q11Value;
	        	}
	        	if (q12Value!=null)
	        	{
	        		total+= q12Value;
	        		totalMediaOro += q12Value;
	        	}
	        	if (q13Value!=null)
	        	{
	        		total+= q13Value;
	        		totalMitica += q13Value;
	        	}
	        	if (q14Value!=null)
	        	{
	        		total+= q14Value;
	        		totalMediaMitica += q14Value;
	        	}
	        	if (q15Value!=null)
	        	{
	        		total+= q15Value;
	        		totalCampellas += q15Value;
	        	}
	        	if (q16Value!=null)
	        	{
	        		total+= q16Value;
	        		totalBole += q16Value;
	        	}
	        	if (q17Value!=null)
	        	{
	        		total+= q17Value;
	        		totalBorsao += q17Value;
	        	}
	        	if (q18Value!=null)
	        	{
	        		total+= q18Value;
	        		totalMallacan += q18Value;
	        	}
	        	if (q19Value!=null)
	        	{
	        		total+= q19Value;
	        		totalTresla+= q19Value;
	        	}
        	}
        }
        footer.getCell("fechaDocumento").setText("TOTALES");
		footer.getCell("tinto5").setText(RutinasNumericas.formatearDoubleDecimales(totalTinto5,2).toString());
		footer.getCell("rosa5").setText(RutinasNumericas.formatearDoubleDecimales(totalRosa5,2).toString());
		footer.getCell("tinto72").setText(RutinasNumericas.formatearDoubleDecimales(totalTinto72,2).toString());
		footer.getCell("rosa72").setText(RutinasNumericas.formatearDoubleDecimales(totalRosa72,2).toString());
		footer.getCell("media5").setText(RutinasNumericas.formatearDoubleDecimales(totalMP5,2).toString());
		
		footer.getCell("tinto2").setText(RutinasNumericas.formatearDoubleDecimales(totalTinto2,2).toString());
		footer.getCell("rosa2").setText(RutinasNumericas.formatearDoubleDecimales(totalRosa2,2).toString());
		footer.getCell("tinto60").setText(RutinasNumericas.formatearDoubleDecimales(totalTinto60,2).toString());
		footer.getCell("rosa60").setText(RutinasNumericas.formatearDoubleDecimales(totalRosa60,2).toString());
		footer.getCell("media2").setText(RutinasNumericas.formatearDoubleDecimales(totalMP2,2).toString());
		
		footer.getCell("oro").setText(RutinasNumericas.formatearDoubleDecimales(totalOro,2).toString());
		footer.getCell("mediaOro").setText(RutinasNumericas.formatearDoubleDecimales(totalMediaOro,2).toString());
		footer.getCell("mitica").setText(RutinasNumericas.formatearDoubleDecimales(totalMitica,2).toString());
		footer.getCell("mediaMitica").setText(RutinasNumericas.formatearDoubleDecimales(totalMediaMitica,2).toString());
		
		footer.getCell("campellas").setText(RutinasNumericas.formatearDoubleDecimales(totalCampellas,2).toString());
		footer.getCell("bole").setText(RutinasNumericas.formatearDoubleDecimales(totalBole,2).toString());
		footer.getCell("borsao").setText(RutinasNumericas.formatearDoubleDecimales(totalBorsao,2).toString());
		footer.getCell("mallacan").setText(RutinasNumericas.formatearDoubleDecimales(totalMallacan,2).toString());
		footer.getCell("tresla").setText(RutinasNumericas.formatearDoubleDecimales(totalTresla,2).toString());
		
		footer.getCell("cliente").setText(RutinasNumericas.formatearDoubleDecimales(total,2).toString());
		
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setWidth("100%");
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		}
		else if (this.win!=null)
		{
			this.win.barAndGridLayout.setWidth("100%");
			this.win.barAndGridLayout.setHeight((this.win.getHeight()-this.win.cabLayout.getHeight())+"%");
		}	

		//		footer.setStyleName("smallgrid");
		footer.getCell("tinto5").setStyleName("Rcell-pie");
		footer.getCell("rosa5").setStyleName("Rcell-pie");
		footer.getCell("tinto72").setStyleName("Rcell-pie");
		footer.getCell("rosa72").setStyleName("Rcell-pie");
		footer.getCell("media5").setStyleName("Rcell-pie");
		footer.getCell("tinto2").setStyleName("Rcell-pie");
		footer.getCell("rosa2").setStyleName("Rcell-pie");
		footer.getCell("tinto60").setStyleName("Rcell-pie");
		footer.getCell("rosa60").setStyleName("Rcell-pie");
		footer.getCell("media2").setStyleName("Rcell-pie");
		footer.getCell("oro").setStyleName("Rcell-pie");
		footer.getCell("mediaOro").setStyleName("Rcell-pie");
		footer.getCell("mitica").setStyleName("Rcell-pie");
		footer.getCell("mediaMitica").setStyleName("Rcell-pie");
		footer.getCell("cliente").setStyleName("Rcell-pie");
		footer.getCell("campellas").setStyleName("Rcell-pie");
		footer.getCell("bole").setStyleName("Rcell-pie");
		footer.getCell("borsao").setStyleName("Rcell-pie");
		footer.getCell("mallacan").setStyleName("Rcell-pie");
		footer.getCell("tresla").setStyleName("Rcell-pie");
	}
	
}
