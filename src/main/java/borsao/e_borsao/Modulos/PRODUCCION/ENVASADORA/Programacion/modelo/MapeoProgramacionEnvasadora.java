package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoProgramacionEnvasadora  extends MapeoGlobal
{
    private Integer idProgramacion;
	private Integer ejercicio;
	private String semana;
	private String dia;
	private String horario;
	private String tipo;
	private Integer cantidad;
	private Integer orden;
	private String vino;
	private String lote;
	private String articulo;
	private String descripcion;
	private Integer cajas;
	private Integer factor;
	private Integer unidades;
	private String observaciones;
	private String frontales;
	private String caja;
	private String estado;
	private String ojo;
	private Integer status;
	private String papel=" ";
	private String subir=" ";
	private String biblia=" ";
	private String bajar=" ";
	private String esc=" ";

	public MapeoProgramacionEnvasadora()
	{
//		this.setArticulo("");
//		this.setVino("");
//		this.setLote("");
//		this.setDia("");
//		this.setObservaciones("");
//		this.setDescripcion("");
//		this.setCantidad(0);
//		this.setOrden(0);
//		this.setCajas(0);
//		this.setFactor(0);
//		this.setUnidades(0);
		
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getVino() {
		return vino;
	}

	public void setVino(String vino) {
		this.vino = vino;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = RutinasCadenas.conversion(descripcion);
	}

	public Integer getCajas() {
		return cajas;
	}

	public void setCajas(Integer cajas) {
		this.cajas = cajas;
	}

	public Integer getFactor() {
		return factor;
	}

	public void setFactor(Integer factor) {
		this.factor = factor;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getFrontales() {
		return frontales;
	}

	public void setFrontales(String frontales) {
		this.frontales = frontales;
	}

	public String getCaja() {
		return caja;
	}

	public void setCaja(String caja) {
		this.caja = caja;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPapel() {
		return papel;
	}

	public void setPapel(String papel) {
		this.papel = papel;
	}

	public String getOjo() {
		return ojo;
	}

	public void setOjo(String ojo) {
		this.ojo = ojo;
	}

	public String getSubir() {
		return subir;
	}

	public void setSubir(String subir) {
		this.subir = subir;
	}

	public String getBajar() {
		return bajar;
	}

	public void setBajar(String bajar) {
		this.bajar = bajar;
	}

	public String getEsc() {
		return esc;
	}

	public void setEsc(String esc) {
		this.esc = esc;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public String getBiblia() {
		return biblia;
	}

	public void setBiblia(String biblia) {
		this.biblia = biblia;
	}
}