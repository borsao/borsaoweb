package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CALIDAD.controlesCalidadProceso.modelo.MapeoControlesProcesoProductivo;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.EtiquetasEnvasado.server.consultaEtiquetasEnvasadoServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaProgramacionEnvasadoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaProgramacionEnvasadoraServer instance;
	private static Double velocidadBib =  new Double(325);
	
	public consultaProgramacionEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProgramacionEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProgramacionEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		Connection con = null;	
		Statement cs = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where articulo between '0103000' and '0103999' order by articulo asc" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
	
	public ArrayList<MapeoProgramacionEnvasadora> datosProgramacionGlobal(MapeoProgramacionEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		ArrayList<MapeoProgramacionEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idPrdProgramacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = '" + r_mapeo.getSemana() + "' ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getHorario()!=null && r_mapeo.getHorario().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" horario = '" + r_mapeo.getHorario() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacionEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacionEnvasadora mapeoProgramacion = new MapeoProgramacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setHorario(rsOpcion.getString("pro_hor"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	

	public ArrayList<MapeoControlesProcesoProductivo> datosProgramacionGlobalArticulosEnv(MapeoControlesProcesoProductivo r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		ArrayList<MapeoControlesProcesoProductivo> vector = null;
		StringBuffer cadenaWhere =null;
		String lote = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idPrdProgramacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = '" + r_mapeo.getSemana() + "' ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			if (cadenaWhere.length()>0) cadenaSQL.append(" and "); else cadenaSQL.append(" where ");			
			cadenaSQL.append(" ( (unidades <> 0 and articulo like '0103%') or prd_programacion_envasadora.dia <> '' ) ");
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoControlesProcesoProductivo>();
			
			while(rsOpcion.next())
			{
				MapeoControlesProcesoProductivo mapeoProgramacion = new MapeoControlesProcesoProductivo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				
				if (!rsOpcion.getString("pro_ope").toUpperCase().contains("LINEA"))
				{
					mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
					if (rsOpcion.getString("pro_lot").length()>0)
						lote = rsOpcion.getString("pro_lot");
					
					mapeoProgramacion.setLote(lote);
					mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
					mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
					mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				}
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public ArrayList<MapeoControlesProcesoProductivo> datosProgramacionGlobalArticulosBIB(MapeoControlesProcesoProductivo r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		ArrayList<MapeoControlesProcesoProductivo> vector = null;
		StringBuffer cadenaWhere =null;
		String lote = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idPrdProgramacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = '" + r_mapeo.getSemana() + "' ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			if (cadenaWhere.length()>0) cadenaSQL.append(" and "); else cadenaSQL.append(" where ");			
			cadenaSQL.append(" ( (unidades <> 0 and articulo like '0111%') or prd_programacion_envasadora.dia <> '' ) ");
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoControlesProcesoProductivo>();
			
			while(rsOpcion.next())
			{
				MapeoControlesProcesoProductivo mapeoProgramacion = new MapeoControlesProcesoProductivo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				
				if (!rsOpcion.getString("pro_ope").toUpperCase().contains("LINEA"))
				{
					mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
					if (rsOpcion.getString("pro_lot").length()>0)
						lote = rsOpcion.getString("pro_lot");
					
					mapeoProgramacion.setLote(lote);
					mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
					mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
					mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				}
				vector.add(mapeoProgramacion);				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProgramacionEnvasadora> datosProgramacionGlobal(Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		ArrayList<MapeoProgramacionEnvasadora> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where ((prd_programacion_envasadora.ejercicio = " + r_ejercicio + " and semana >= " + new Integer(r_semana) + ")  ") ;
		cadenaSQL.append(" or (prd_programacion_envasadora.ejercicio > " + r_ejercicio + "))  ") ;
		cadenaSQL.append(" and estado in ('A','N')  ") ;

		try
		{
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacionEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacionEnvasadora mapeoProgramacion = new MapeoProgramacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setHorario(rsOpcion.getString("pro_hor"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public MapeoProgramacionEnvasadora datosProgramacionGlobal(Integer r_id)
	{
		MapeoProgramacionEnvasadora mapeoProgramacion = null;
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.idPrdProgramacion = " + r_id);

		try
		{
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoProgramacion = new MapeoProgramacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setHorario(rsOpcion.getString("pro_hor"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeoProgramacion;
	}

	public ArrayList<MapeoProgramacionEnvasadora> datosProgramacionGlobal(ArrayList<String> r_articulos, MapeoProgramacionEnvasadora r_mapeo, boolean r_etiquetados, boolean r_buscoDia)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		ArrayList<MapeoProgramacionEnvasadora> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idPrdProgramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.horario pro_hor,");
		cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_envasadora.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_envasadora.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_envasadora.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_envasadora.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_envasadora.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_envasadora.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_envasadora.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_envasadora.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_envasadora.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_envasadora.contras pro_con,");
		cadenaSQL.append(" prd_programacion_envasadora.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_envasadora.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");

		try
		{
     		String articulos="";
     		for (int i=0; i<r_articulos.size();i++)
     		{
     			if (articulos!="") articulos = articulos + ",";
     			articulos =articulos + "'" + r_articulos.get(i).trim().substring(0, 7) + "'";
     		}
     		if (articulos!="") cadenaSQL.append(" where articulo in (" + articulos + ") ");

     		

     		if (r_etiquetados) cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO','ETIQUETADO') ");
     		else cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' ");
     		
     		if (r_mapeo.getEstado()==null || r_mapeo.getEstado().contentEquals("X"))
     			cadenaSQL.append(" and estado in ('A','N') ");
     		else if (!r_mapeo.getEstado().contentEquals("X"))
     			cadenaSQL.append(" and estado = '" + r_mapeo.getEstado() + "'");

     		if (r_mapeo.getEstado()==null || r_mapeo.getEstado().contentEquals("X"))
				cadenaSQL.append(" and ejercicio >= " + r_mapeo.getEjercicio());
     		else if (!r_mapeo.getEstado().contentEquals("X"))
				cadenaSQL.append(" and ejercicio = " + r_mapeo.getEjercicio());

//     		if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()!=0)
//     			cadenaSQL.append(" and ejercicio = " + r_mapeo.getEjercicio());

			cadenaSQL.append(" order by ejercicio, semana, orden asc");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacionEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacionEnvasadora mapeoProgramacion = new MapeoProgramacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setHorario(rsOpcion.getString("pro_hor"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des").toUpperCase());
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				
				if (rsOpcion.getString("pro_dia")!=null && rsOpcion.getString("pro_dia").trim().length()==0 && r_buscoDia)
				{
					mapeoProgramacion.setDia(this.buscoDiaSemana(con, mapeoProgramacion));
				}
				else
				{
					mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				}
				
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public String generarInforme(MapeoProgramacionEnvasadora r_mapeo, String r_lote, String r_tipo, Integer r_cajas, String r_anada, String r_cantidad, boolean r_paletCompleto)
	{
		boolean generado = false;
		String informe = "";
		
		consultaEtiquetasEnvasadoServer ces = consultaEtiquetasEnvasadoServer.getInstance(CurrentUser.get());
		generado = ces.generarDatosEtiquetas(r_mapeo, r_lote, r_tipo, r_cajas, r_anada,r_cantidad, r_paletCompleto);
		
		if (generado) informe = ces.generarInforme(r_mapeo.getIdProgramacion());
		
		
		return informe;
		
	}

	public String imprimirProgramacion(String r_ejercicio, String r_semana)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio);
		libImpresion.setCodigo2Txt(r_semana);
		libImpresion.setArchivoDefinitivo("/programacion Vino Mesa  " + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("ListadoProgramacionEnvasad.jrxml");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("programacion");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}
	public Integer obtenerSumaProgramacionPallet(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres)
	{
		Integer cantidad = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		Double calculo = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT articulo pro_art, prd_programacion_envasadora.unidades pro_ud ");		
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where ((ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana >= " + new Integer(r_semana) + ") ");
		cadenaSQL.append(" or (ejercicio >= '" + r_ejercicio + "')) ");
		cadenaSQL.append(" and estado in ('A','N') ");
		
		try
		{
			cantidad =0;
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
						
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
				}
				if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			
			cs = r_con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			String sufijo = null;
			while(rsOpcion.next())
			{
				
//				if (rsOpcion.getString("pro_art").contentEquals("0103004") || rsOpcion.getString("pro_art").contentEquals("0103104"))
//					sufijo = "28";
//				else if (rsOpcion.getString("pro_art").contentEquals("0102006") || rsOpcion.getString("pro_art").contentEquals("0102091") ||
//						rsOpcion.getString("pro_art").contentEquals("0103005") || rsOpcion.getString("pro_art").contentEquals("0103012") || rsOpcion.getString("pro_art").contentEquals("0103105")
//						|| rsOpcion.getString("pro_art").contentEquals("0103112"))
//					sufijo="38";
//				else if (rsOpcion.getString("pro_art").contentEquals("0103001") || rsOpcion.getString("pro_art").contentEquals("0103010") || rsOpcion.getString("pro_art").contentEquals("0103002")
//						|| rsOpcion.getString("pro_art").contentEquals("0103003") || rsOpcion.getString("pro_art").contentEquals("0103011") || rsOpcion.getString("pro_art").contentEquals("0103004"))
//					sufijo="58";
//				else
//					sufijo ="18";
				if (rsOpcion.getString("pro_art").contentEquals("0103004")|| rsOpcion.getString("pro_art").contentEquals("0103104") || rsOpcion.getString("pro_art").contentEquals("0103008") || rsOpcion.getString("pro_art").contentEquals("0103018"))
					sufijo = "28";
				else if (rsOpcion.getString("pro_art").contentEquals("0102006") || rsOpcion.getString("pro_art").contentEquals("0102091") 
						|| rsOpcion.getString("pro_art").contentEquals("0103005") || rsOpcion.getString("pro_art").contentEquals("0103012")
						|| rsOpcion.getString("pro_art").contentEquals("0103105") || rsOpcion.getString("pro_art").contentEquals("0103112"))
					sufijo="38";
				else
					sufijo ="18";
				
				calculo = new Double(0);
				String paletizacionArticulo=cas.obtenerCantidadEan(r_con, rsOpcion.getString("pro_art"), sufijo);
				
				if (paletizacionArticulo!=null)
				{
					Double paletizacion = new Double(paletizacionArticulo);				
					calculo = calculo + Math.ceil(rsOpcion.getInt("pro_ud")/paletizacion);
					cantidad = cantidad + calculo.intValue();
				}
				else
					calculo=new Double(0);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return cantidad.intValue();		
	}
	
	public Double obtenerSumaProgramacion(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		
		cadenaSQL.append(" SELECT sum(prd_programacion_envasadora.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where ((ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana >= " + new Integer(r_semana) + ") ");
		cadenaSQL.append(" or (ejercicio >= '" + r_ejercicio + "')) ");
		cadenaSQL.append(" and estado in ('A','N') ");

		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
					cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			
			cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return cantidad;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion_envasadora.idPrdProgramacion) pro_sig ");
     	cadenaSQL.append(" FROM prd_programacion_envasadora ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 1;
	}

	public Integer obtenerOrden(Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion_envasadora.orden) pro_ord ");
     	cadenaSQL.append(" FROM prd_programacion_envasadora ");
     	cadenaSQL.append(" WHERE ejercicio =  " + r_ejercicio);
     	cadenaSQL.append(" AND semana = '" + r_semana + "'");
     	
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_ord") + 10 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return 10;
	}

	public String guardarNuevo(MapeoProgramacionEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			Integer permisos = new Integer(this.comprobarAccesos());		    
			if (permisos<99 && permisos>70 ) 
				r_mapeo.setEstado("P");
			else 
				r_mapeo.setEstado("A");
			
			cadenaSQL.append(" INSERT INTO prd_programacion_envasadora( ");
			cadenaSQL.append(" prd_programacion_envasadora.idPrdProgramacion,");
			cadenaSQL.append(" prd_programacion_envasadora.ejercicio ,");
			cadenaSQL.append(" prd_programacion_envasadora.semana ,");
			cadenaSQL.append(" prd_programacion_envasadora.dia ,");
			cadenaSQL.append(" prd_programacion_envasadora.horario ,");
			cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion_envasadora.cantidad ,");
			cadenaSQL.append(" prd_programacion_envasadora.vino ,");
			cadenaSQL.append(" prd_programacion_envasadora.lote ,");
			cadenaSQL.append(" prd_programacion_envasadora.orden ,");
			cadenaSQL.append(" prd_programacion_envasadora.articulo ,");
			cadenaSQL.append(" prd_programacion_envasadora.descripcion ,");
			cadenaSQL.append(" prd_programacion_envasadora.cajas ,");
			cadenaSQL.append(" prd_programacion_envasadora.factor ,");
			cadenaSQL.append(" prd_programacion_envasadora.unidades ,");
			cadenaSQL.append(" prd_programacion_envasadora.observaciones ,");
			cadenaSQL.append(" prd_programacion_envasadora.frontales ,");
			cadenaSQL.append(" prd_programacion_envasadora.contras ,");
			cadenaSQL.append(" prd_programacion_envasadora.caja, ");
			cadenaSQL.append(" prd_programacion_envasadora.estado, ");
			cadenaSQL.append(" prd_programacion_envasadora.status ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    

		    preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDia()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getHorario()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getHorario());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }

		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(15, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(15, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    {
		    	preparedStatement.setString(18, "SI");
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    
		    preparedStatement.setInt(21, 0);
		    
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoProgramacionEnvasadora r_mapeo)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;	
		Statement cs = null;

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion_envasadora set ");
			cadenaSQL.append(" prd_programacion_envasadora.ejercicio =?,");
			cadenaSQL.append(" prd_programacion_envasadora.semana =?,");
			cadenaSQL.append(" prd_programacion_envasadora.dia =?,");
			cadenaSQL.append(" prd_programacion_envasadora.horario =?,");
			cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion =?,");
			cadenaSQL.append(" prd_programacion_envasadora.cantidad =?,");
			cadenaSQL.append(" prd_programacion_envasadora.vino =?,");
			cadenaSQL.append(" prd_programacion_envasadora.lote =?,");
			cadenaSQL.append(" prd_programacion_envasadora.orden =?,");
			cadenaSQL.append(" prd_programacion_envasadora.articulo =?,");
			cadenaSQL.append(" prd_programacion_envasadora.descripcion =?,");
			cadenaSQL.append(" prd_programacion_envasadora.cajas =?,");
			cadenaSQL.append(" prd_programacion_envasadora.factor =?,");
			cadenaSQL.append(" prd_programacion_envasadora.unidades =?,");
			cadenaSQL.append(" prd_programacion_envasadora.observaciones =?,");
			cadenaSQL.append(" prd_programacion_envasadora.frontales =?,");
			cadenaSQL.append(" prd_programacion_envasadora.contras =?,");
			cadenaSQL.append(" prd_programacion_envasadora.caja = ?, ");
			cadenaSQL.append(" prd_programacion_envasadora.estado = ?, ");
			cadenaSQL.append(" prd_programacion_envasadora.status = ? ");
			cadenaSQL.append(" where prd_programacion_envasadora.idPrdProgramacion = ? ");
			
		    con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDia()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getHorario()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getHorario());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    {
		    	preparedStatement.setString(17, "SI");
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getStatus()!=null)
		    {
		    	preparedStatement.setInt(20, r_mapeo.getStatus()+1);
		    }
		    else
		    {
		    	preparedStatement.setInt(20, 0);
		    }

		    preparedStatement.setInt(21, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()))
    		{
		    	preparedStatement.executeUpdate();
		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public void eliminar(MapeoProgramacionEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;	
		Statement cs = null;

		try
		{
			cadenaSQL.append(" DELETE FROM prd_programacion_envasadora ");            
			cadenaSQL.append(" WHERE prd_programacion_envasadora.idPrdProgramacion = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	public String programarNuevoRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());
		Connection con = null;	
		Statement cs = null;

		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion_envasadora( ");
			cadenaSQL.append(" prd_programacion_envasadora.idPrdProgramacion,");
			cadenaSQL.append(" prd_programacion_envasadora.ejercicio ,");
			cadenaSQL.append(" prd_programacion_envasadora.semana ,");
			cadenaSQL.append(" prd_programacion_envasadora.dia ,");
			cadenaSQL.append(" prd_programacion_envasadora.horario ,");
			cadenaSQL.append(" prd_programacion_envasadora.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion_envasadora.cantidad ,");
			cadenaSQL.append(" prd_programacion_envasadora.vino ,");
			cadenaSQL.append(" prd_programacion_envasadora.lote ,");
			cadenaSQL.append(" prd_programacion_envasadora.orden ,");
			cadenaSQL.append(" prd_programacion_envasadora.articulo ,");
			cadenaSQL.append(" prd_programacion_envasadora.descripcion ,");
			cadenaSQL.append(" prd_programacion_envasadora.cajas ,");
			cadenaSQL.append(" prd_programacion_envasadora.factor ,");
			cadenaSQL.append(" prd_programacion_envasadora.unidades ,");
			cadenaSQL.append(" prd_programacion_envasadora.observaciones ,");
			cadenaSQL.append(" prd_programacion_envasadora.frontales ,");
			cadenaSQL.append(" prd_programacion_envasadora.contras ,");
			cadenaSQL.append(" prd_programacion_envasadora.caja, ");
			cadenaSQL.append(" prd_programacion_envasadora.estado, ");
			cadenaSQL.append(" prd_programacion_envasadora.status ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer idNuevo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idNuevo);		    
		    if (r_ejercicio!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_ejercicio));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_semana!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_semana));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    //dia
		    preparedStatement.setString(4, null);
		    //horario
		    preparedStatement.setString(5, null);
		    //tipo y cantidad 
		    preparedStatement.setString(6, "RETRABAJO");
		    preparedStatement.setInt(7, 0);
		    //VINO, LOTE
		    preparedStatement.setString(8, null);
		    preparedStatement.setString(9, null);
		    
		    preparedStatement.setInt(10, this.obtenerOrden(new Integer(r_ejercicio), r_semana));
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(15, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(15, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    //Frontales,Contras, Capsulas, Tapones, Cajas, Estado
		    preparedStatement.setString(17, "SI");
		    preparedStatement.setString(18, "SI");
		    preparedStatement.setString(19, "SI");
		    preparedStatement.setString(20, "A");
		    
		    preparedStatement.setInt(21, 0);
		    
	        preparedStatement.executeUpdate();
	        r_mapeo.setIdProgramacionEnvasadora(idNuevo);
	        rdo = crs.asociarProgramacion(r_mapeo);
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
		
	}
	
	public String asignarRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana)
	{
		String rdo = null;
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());

		try
		{
	        rdo = crs.asociarProgramacion(r_mapeo);
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        

		return rdo;
		
	}

	public String actualizarObservacionesProgramacion(MapeoProgramacionEnvasadora r_mapeo)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;	
		Statement cs = null;

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion_envasadora set ");
			cadenaSQL.append(" prd_programacion_envasadora.observaciones =? ");
			cadenaSQL.append(" where prd_programacion_envasadora.idPrdProgramacion = ? ");
			
		    con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }

		    preparedStatement.setInt(2, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()))
    		{
		    	preparedStatement.executeUpdate();
		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		finally
		{
			try
			{
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}


	/*
	 * Devuelve el nivel de acceso a programaciones del usuario
	 * Recibe como parametro el nombre del usuario con el que buscamos a su operario equivalente
	 * 
	 * Devuelve:  0 - Sin permisos
	 * 			 10 - Laboratorio
	 * 			 20 - Solo Consulta
	 * 			 30 - Papeles 
	 * 			 40 - Completar
	 * 			 50 - Cambio Observaciones
	 * 			 70 - Cambio observaciones y materia seca
	 * 			 80 - Cambio observaciones y materia seca y Creacion provisional
	 *  			 
	 * 			 99 - Acceso total 
	 */
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Envasadora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos(null);
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";
			}
			else
			{
				permisos="0";
			}
		}
		return permisos;
	}
	
	public Integer obtenerStatus(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.status pro_st ");
     	cadenaSQL.append(" FROM prd_programacion_envasadora ");
     	cadenaSQL.append(" where prd_programacion_envasadora.idPrdProgramacion = " + r_id);
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_st");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String buscarLote (MapeoProgramacionEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaWhere =null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.lote pro_lot ");
		cadenaSQL.append(" from prd_programacion_envasadora ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()));
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden < " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden desc ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_lot")!=null && rsOpcion.getString("pro_lot").length()>0)
				{
					return rsOpcion.getString("pro_lot").trim();
					
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public MapeoProgramacionEnvasadora recogerProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		MapeoProgramacionEnvasadora mapeo = null;
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idprdProgramacion pro_id, ");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion_envasadora.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion_envasadora.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			mapeo=new MapeoProgramacionEnvasadora();
			
			while(rsOpcion.next())
			{
				mapeo.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeo.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeo.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeo;
	}

	public boolean comprobarProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idprdProgramacion pro_id, ");
		cadenaSQL.append(" prd_programacion_envasadora.observaciones pro_obs, ");
		cadenaSQL.append(" prd_programacion_envasadora.status pro_st ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion_envasadora.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion_envasadora.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}

	private String buscoDiaSemana(Connection r_con, MapeoProgramacionEnvasadora r_mapeo)
	{
		String diaSemana = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		ResultSet rsOpcion = null;
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.idprdprogramacion pro_id,");
		cadenaSQL.append(" prd_programacion_envasadora.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_envasadora.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_envasadora.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_envasadora.orden pro_ord ");
		cadenaSQL.append(" from prd_programacion_envasadora ");

		try
		{
			
     		cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
     		cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
     		cadenaSQL.append(" and orden <= " + r_mapeo.getOrden());
     		cadenaSQL.append(" and length(dia) > 0  ");
			cadenaSQL.append(" order by ejercicio, semana, orden desc");
			
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				diaSemana = rsOpcion.getString("pro_dia");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return diaSemana;
	}
	
	public String recogerArticuloProgramacion(Integer r_idProgramacion)
	{
		String articuloEncontrado = null;
		
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.articulo pro_art ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.idPrdProgramacion = " + r_idProgramacion);
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				articuloEncontrado= rsOpcion.getString("pro_art");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return articuloEncontrado;
	}
	
	public HashMap<String, Double> obtenerHashProducciones(String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.articulo pro_art, ");
		cadenaSQL.append(" sum(prd_programacion_envasadora.unidades) pro_uds ");
		cadenaSQL.append(" from prd_programacion_envasadora ");

		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO') "); 
		cadenaSQL.append(" and articulo like ('0111%') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion_envasadora.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion_envasadora.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try
		{
			cadenaSQL.append(" group by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("pro_art").trim(), rsOpcion.getDouble("pro_uds"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public HashMap<Integer, HashMap<Integer, Double>> obtenerHashProgramacion(String r_ejercicio, String r_articulo, String r_campo)
	{
		HashMap<Integer, HashMap<Integer, Double>> hash = null;
		HashMap<Integer, Double> hashSemanas = null;
		HashMap<Integer, Double> hashUnidades = null;
		int contador = 0;
		String campo = "";
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		if (r_campo.contentEquals("Litros")) campo = "e_articu.marca"; else campo="1";
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.semana pro_sem, ");
		cadenaSQL.append(" sum(prd_programacion_envasadora.unidades*" + campo + ") pro_uds ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" inner join e_articu on prd_programacion_envasadora.articulo COLLATE latin1_spanish_ci  = e_articu.articulo COLLATE latin1_spanish_ci ");
		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and prd_programacion_envasadora.tipoOperacion in ('EMBOTELLADO') ");
		cadenaSQL.append(" and prd_programacion_envasadora.estado in ('A','N') ");
		cadenaSQL.append(" and prd_programacion_envasadora.articulo ='" + r_articulo + "' " );

		try
		{
			cadenaSQL.append(" group by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hashSemanas = new HashMap<Integer, Double>();
			hashUnidades = new HashMap<Integer, Double>();
			hash = new HashMap<Integer,HashMap<Integer, Double>>();
			
			while(rsOpcion.next())
			{
				hashSemanas.put(contador, rsOpcion.getDouble("pro_sem"));
				hashUnidades.put(contador, rsOpcion.getDouble("pro_uds"));
				contador++;
			}
			
			if (contador>0)
			{
				hash.put(0,hashSemanas);
				hash.put(1,hashUnidades);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}

	public HashMap<String, Double> obtenerHashVelocidades(String r_ejercicio, String r_semana, String r_vista, boolean r_prevision)
	{
		HashMap<String, Double> hash = null;
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		int desde = 1;
		int hasta = 53;
		
		cadenaSQL.append(" SELECT prd_programacion_envasadora.articulo pro_art, ");
		cadenaSQL.append(" convert(1/(sum(prd_programacion_envasadora.unidades*convert(" + velocidadBib + ",decimal(8,0)))/sum(unidades)),decimal(20,12)) pro_vel_media ");
		
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO') ");
		cadenaSQL.append(" and prd_programacion_envasadora.articulo like ('0111%') ");
		
		if (!r_prevision)
			cadenaSQL.append(" and estado in ('T') ");
//		else
//			cadenaSQL.append(" and estado not in ('T') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion_envasadora.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion_envasadora.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try
		{
			cadenaSQL.append(" group by articulo ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("pro_art").trim(), rsOpcion.getDouble("pro_vel_media"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	public HashMap<Integer, String> obtenerHashArticulos(String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<Integer, String> hash = null;
		int contador = 0;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT distinct prd_programacion_envasadora.articulo pro_art ");
		cadenaSQL.append(" from prd_programacion_envasadora ");
		cadenaSQL.append(" where prd_programacion_envasadora.ejercicio = '" + r_ejercicio + "' "); 
		cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO') ");
		cadenaSQL.append(" and prd_programacion_envasadora.articulo like ('0111%') "); 
//		cadenaSQL.append(" and estado not in ('T') ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and prd_programacion_envasadora.semana= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and prd_programacion_envasadora.semana<= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual"))
		{
			try {
				int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
				int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
				int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
				if (desde>hasta) desde = 1;
				cadenaSQL.append(" and semana between " + desde + " and " + hasta);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<Integer, String>();
			
			while(rsOpcion.next())
			{
				hash.put(contador, rsOpcion.getString("pro_art").trim());
				contador++;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
}