package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoControlTurnoEnvasadora;

public class consultaControlTurnoEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaControlTurnoEnvasadoraServer instance;
	
	public consultaControlTurnoEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaControlTurnoEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaControlTurnoEnvasadoraServer(r_usuario);			
		}
		return instance;
	}


	public ArrayList<MapeoControlTurnoEnvasadora> datosOpcionesGlobal(MapeoControlTurnoEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlTurnoEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_turnos_env.idCodigo tur_id, ");
		cadenaSQL.append(" prd_turnos_env.operario tur_ope, ");
		cadenaSQL.append(" prd_turnos_env.horas tur_hor, ");
		cadenaSQL.append(" prd_turnos_env.fuera_linea tur_fue, ");
		cadenaSQL.append(" prd_turnos_env.motivo_fuera_linea tur_mf, ");
		cadenaSQL.append(" prd_turnos_env.ausencia tur_aus, ");
		cadenaSQL.append(" prd_turnos_env.motivo_ausencia tur_ms, ");
		cadenaSQL.append(" prd_produccion_env.idCodigo ttr_id, ");
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM prd_turnos_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = prd_turnos_env.idTurno ");
     	
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdTurno()!=null && r_mapeo.getIdTurno()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_produccion_env.idCodigo= " + r_mapeo.getIdTurno());
				}

			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
//			cadenaSQL.append(" order by ejercicio, semana, fecha,  turno asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoControlTurnoEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoControlTurnoEnvasadora mapeo = new MapeoControlTurnoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("tur_id"));
				mapeo.setOperario(rsOpcion.getString("tur_ope"));
				mapeo.setHoras(rsOpcion.getDouble("tur_hor"));
				mapeo.setFueraLina(rsOpcion.getDouble("tur_fue"));
				mapeo.setConceptoFueraLinea(rsOpcion.getString("tur_mf"));
				mapeo.setAusencia(rsOpcion.getDouble("tur_aus"));
				mapeo.setConceptoAusencia(rsOpcion.getString("tur_ms"));
				mapeo.setIdTurno(rsOpcion.getInt("ttr_id"));
				
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				
				mapeo.setFecha(mapeo.getMapeo().getFecha());
				mapeo.setTurno(mapeo.getMapeo().getTurno());
				
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoControlTurnoEnvasadora> datosOpcionesGlobal(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlTurnoEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_turnos_env.idCodigo tur_id, ");
		cadenaSQL.append(" prd_turnos_env.operario tur_ope, ");
		cadenaSQL.append(" prd_turnos_env.horas tur_hor, ");
		cadenaSQL.append(" prd_turnos_env.fuera_linea tur_fue, ");
		cadenaSQL.append(" prd_turnos_env.motivo_fuera_linea tur_mf, ");
		cadenaSQL.append(" prd_turnos_env.ausencia tur_aus, ");
		cadenaSQL.append(" prd_turnos_env.motivo_ausencia tur_ms, ");
		cadenaSQL.append(" prd_produccion_env.idCodigo ttr_id, ");
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM prd_turnos_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = prd_turnos_env.idTurno ");
     	
		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_ejercicio!=null && r_ejercicio>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.ejercicio = " + r_ejercicio);
			}
			if (r_semana!=null && r_semana>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.semana = " + r_semana);
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana, fecha,  turno asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoControlTurnoEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoControlTurnoEnvasadora mapeo = new MapeoControlTurnoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("tur_id"));
				mapeo.setOperario(rsOpcion.getString("tur_ope"));
				mapeo.setHoras(rsOpcion.getDouble("tur_hor"));
				mapeo.setFueraLina(rsOpcion.getDouble("tur_fue"));
				mapeo.setConceptoFueraLinea(rsOpcion.getString("tur_mf"));
				mapeo.setAusencia(rsOpcion.getDouble("tur_aus"));
				mapeo.setConceptoAusencia(rsOpcion.getString("tur_ms"));
				mapeo.setIdTurno(rsOpcion.getInt("ttr_id"));
				
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				
				mapeo.setFecha(mapeo.getMapeo().getFecha());
				mapeo.setTurno(mapeo.getMapeo().getTurno());
				mapeo.setDia(RutinasFechas.obtenerDiaDeLaSemanaLetra(mapeo.getMapeo().getFecha()));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoControlTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_turnos_env ( ");
			cadenaSQL.append(" prd_turnos_env.idCodigo, ");
			cadenaSQL.append(" prd_turnos_env.operario, ");
			cadenaSQL.append(" prd_turnos_env.horas, ");
			cadenaSQL.append(" prd_turnos_env.fuera_linea, ");
    		cadenaSQL.append(" prd_turnos_env.motivo_fuera_linea, ");
			cadenaSQL.append(" prd_turnos_env.ausencia, ");
			cadenaSQL.append(" prd_turnos_env.motivo_ausencia, ");
    		cadenaSQL.append(" prd_turnos_env.idTurno) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_turnos_env", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getOperario()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOperario());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getFueraLina()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getFueraLina());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getConceptoFueraLinea()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getConceptoFueraLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getAusencia()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getAusencia());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getConceptoAusencia()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getConceptoAusencia());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoControlTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_turnos_env SET ");
			cadenaSQL.append(" prd_turnos_env.operario=?, ");
    		cadenaSQL.append(" prd_turnos_env.horas=?, ");
    		cadenaSQL.append(" prd_turnos_env.fuera_linea=?, ");
    		cadenaSQL.append(" prd_turnos_env.motivo_fuera_linea=?, ");
    		cadenaSQL.append(" prd_turnos_env.ausencia=?, ");
    		cadenaSQL.append(" prd_turnos_env.motivo_ausencia=?, ");
    		cadenaSQL.append(" prd_turnos_env.idTurno=? ");
	        cadenaSQL.append(" WHERE prd_turnos_env.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getOperario()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getOperario());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    if (r_mapeo.getFueraLina()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getFueraLina());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getConceptoFueraLinea()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getConceptoFueraLinea());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getAusencia()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getAusencia());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getConceptoAusencia()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getConceptoAusencia());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    preparedStatement.setInt(8, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoControlTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_turnos_env ");            
			cadenaSQL.append(" WHERE prd_turnos_env.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}