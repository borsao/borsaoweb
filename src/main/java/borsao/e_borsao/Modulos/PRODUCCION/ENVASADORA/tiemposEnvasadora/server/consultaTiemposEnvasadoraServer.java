package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.MapeoTiemposEnvasadora;

public class consultaTiemposEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTiemposEnvasadoraServer instance;
	
	public consultaTiemposEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTiemposEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTiemposEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoTiemposEnvasadora> datosOpcionesGlobal(MapeoTiemposEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoTiemposEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_tiempos_envasadora.idCodigo tie_id, ");
		cadenaSQL.append(" prd_tiempos_envasadora.ejercicio tie_eje, ");
		cadenaSQL.append(" prd_tiempos_envasadora.semana tie_sem, ");
		cadenaSQL.append(" prd_tiempos_envasadora.turno tie_tur, ");
		cadenaSQL.append(" prd_tiempos_envasadora.fecha tie_fec, ");
		cadenaSQL.append(" prd_tiempos_envasadora.tipo tie_tip, ");
		cadenaSQL.append(" prd_tiempos_envasadora.descripcion tie_des, ");
		cadenaSQL.append(" prd_tiempos_envasadora.tiempo tie_tie, ");
		cadenaSQL.append(" prd_tiempos_envasadora.observaciones tie_obs, ");
		cadenaSQL.append(" prd_tiempos_envasadora.idProgramacion tie_prg ");
     	cadenaSQL.append(" FROM prd_tiempos_envasadora ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idProgramacion = " + r_mapeo.getIdProgramacion());
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipo = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getFecha()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())) + "' ");
				}
				if (r_mapeo.getTurno()!=null && r_mapeo.getTurno().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					
					if (r_mapeo.getTurno().contentEquals("Mañana")) cadenaWhere.append(" turno = 'M' " );
					else if (r_mapeo.getTurno().contentEquals("Tarde")) cadenaWhere.append(" turno = 'T' " );
					else if (r_mapeo.getTurno().contentEquals("Noche")) cadenaWhere.append(" turno = 'N' " );
					else cadenaWhere.append(" turno = '" + r_mapeo.getTurno() + "' " );
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha, tipo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoTiemposEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoTiemposEnvasadora mapeo = new MapeoTiemposEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setEjercicio(rsOpcion.getInt("tie_eje"));
				mapeo.setSemana(rsOpcion.getInt("tie_sem"));
				mapeo.setIdCodigo(rsOpcion.getInt("tie_id"));
				mapeo.setFecha(rsOpcion.getDate("tie_fec"));
				mapeo.setIdProgramacion(rsOpcion.getInt("tie_prg"));
				mapeo.setTipo(rsOpcion.getString("tie_tip"));
				mapeo.setTurno(rsOpcion.getString("tie_tur"));
				mapeo.setDescripcion(rsOpcion.getString("tie_des"));
				mapeo.setTiempo(rsOpcion.getDouble("tie_tie"));
				mapeo.setObservaciones(rsOpcion.getString("tie_obs"));
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public String guardarNuevo(MapeoTiemposEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_tiempos_envasadora ( ");
			cadenaSQL.append(" prd_tiempos_envasadora.idCodigo, ");
			cadenaSQL.append(" prd_tiempos_envasadora.ejercicio, ");
			cadenaSQL.append(" prd_tiempos_envasadora.semana, ");
			cadenaSQL.append(" prd_tiempos_envasadora.turno, ");
			cadenaSQL.append(" prd_tiempos_envasadora.fecha, ");
			cadenaSQL.append(" prd_tiempos_envasadora.tipo, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.descripcion, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.tiempo, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.observaciones, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.idProgramacion) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_tiempos_envasadora", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	String turno = null;
		    	if (r_mapeo.getTurno().contentEquals("Mañana")) turno="M";
		    	else if (r_mapeo.getTurno().contentEquals("Tarde")) turno="T";
		    	else if (r_mapeo.getTurno().contentEquals("Noche")) turno="N";
		    	
		    	preparedStatement.setString(4, turno);
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setDouble(10, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setDouble(10, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoTiemposEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_tiempos_envasadora SET ");
			cadenaSQL.append(" prd_tiempos_envasadora.ejercicio=?, ");
			cadenaSQL.append(" prd_tiempos_envasadora.semana=?, ");
			cadenaSQL.append(" prd_tiempos_envasadora.turno=?, ");
			cadenaSQL.append(" prd_tiempos_envasadora.fecha=?, ");
			cadenaSQL.append(" prd_tiempos_envasadora.tipo=?, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.descripcion=?, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.tiempo=?, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.observaciones=?, ");
    		cadenaSQL.append(" prd_tiempos_envasadora.idProgramacion=? ");
	        cadenaSQL.append(" WHERE prd_tiempos_envasadora.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getTurno());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    preparedStatement.setInt(10, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoTiemposEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_tiempos_envasadora ");            
			cadenaSQL.append(" WHERE prd_tiempos_envasadora.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}