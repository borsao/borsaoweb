package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo.MapeoSituacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.server.consultaSituacionEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaSituacionEnvasadora extends Window{

	public consultaSituacionEnvasadoraServer cses =null;
	private ComboBox cmbAlmacen = null;
	private EntradaDatosFecha fechaCarga = null;
	
	private VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	private Grid grid = null;
	private final String titulo = "SITUACION ENVASADORA";
	private boolean hayGrid=false;
	public String almacenConsulta = "1";
	private HashMap<String,String> filtrosRec = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaSituacionEnvasadora() 
    {
		this.setCaption(titulo);
		this.center();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);
    	
    }

    public void cargarPantalla() 
    {
    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);
    	
    	this.cmbAlmacen= new ComboBox("Almacen de Consulta");    		
		this.cmbAlmacen.setNewItemsAllowed(false);
		this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbAlmacen.setNullSelectionAllowed(false);
		this.cmbAlmacen.addItem("Almacen Capuchinos");
		this.cmbAlmacen.addItem("Bodega Nueva");
		this.cmbAlmacen.addItem("Todos");
		this.cmbAlmacen.setValue("Almacen Capuchinos");

		this.fechaCarga = new EntradaDatosFecha("Fecha de Carga");
		this.fechaCarga.addStyleName(ValoTheme.DATEFIELD_TINY);
		this.fechaCarga.setValue(new Date());
		
		this.cabLayout.addComponent(this.cmbAlmacen);
		this.cabLayout.addComponent(this.fechaCarga);
		
		this.barAndGridLayout.addComponent(this.cabLayout);
		
		this.generarGrid();
    }

    public void generarGrid()
    {
    	ArrayList<MapeoSituacionEnvasadora> r_vector=null;
    	consultaSituacionEnvasadoraServer cse = new consultaSituacionEnvasadoraServer(CurrentUser.get());
    	r_vector=cse.datosOpcionesGlobal(almacenConsulta,RutinasFechas.convertirDateToString(this.fechaCarga.getValue()));
    	this.presentarGrid(r_vector);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoSituacionEnvasadora> r_vector)
    {
    	
    	grid = new OpcionesGrid(this, r_vector,RutinasFechas.convertirDateToString(this.fechaCarga.getValue()));
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    		((OpcionesGrid) grid).establecerFiltros(filtrosRec);
    	}

    }
    
    private void cargarListeners()
    {

		this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Almacen Capuchinos")) 
					{
						almacenConsulta = "1";
					}
					else if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Bodega Nueva")) 
					{
						almacenConsulta = "4";
					}
					else 
					{
						almacenConsulta = "";
					}
					
					generarGrid();
				}
			}
		}); 
		this.fechaCarga.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Almacen Capuchinos")) 
					{
						almacenConsulta = "1";
					}
					else if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Bodega Nueva")) 
					{
						almacenConsulta = "4";
					}
					else 
					{
						almacenConsulta = "";
					}
					
					generarGrid();
				}
				
			}
		});
    }
    
    
	private boolean isHayGrid() {
		return hayGrid;
	}

	private void setHayGrid(boolean r_hayGrid) {
		this.hayGrid = r_hayGrid;
		if (r_hayGrid)
		{
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);

			grid.addSelectionListener(new SelectionListener() {
				
				@Override
				public void select(SelectionEvent event) {
						
//					if (grid.getSelectionModel() instanceof SingleSelectionModel)
//						filaSeleccionada(grid.getSelectedRow());
//					else
//						mostrarFilas(grid.getSelectedRows());
				}
			});
		}
		else
		{
		}
	}
}
