package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view.PantallaSituacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view.SituacionEnvasadoraView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = true;
	private SituacionEnvasadoraView app = null;
	private PantallaSituacionEnvasadora win = null;
	private String fechaConsulta=null;
	
    public OpcionesGrid(SituacionEnvasadoraView r_App, ArrayList<MapeoSituacionEnvasadora> r_vector, String r_fecha) 
    {
    	this.app=r_App;
        this.vector=r_vector;
        this.fechaConsulta=r_fecha;
		this.asignarTitulo("Situacion Envasadora");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    public OpcionesGrid(PantallaSituacionEnvasadora r_app, ArrayList<MapeoSituacionEnvasadora> r_vector, String r_fecha) 
    {
    	this.win=r_app;
        this.vector=r_vector;
        this.fechaConsulta=r_fecha;
        
		this.asignarTitulo("Situacion Envasadora");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("ver");
    	this.camposNoFiltrar.add("mail");
    	this.camposNoFiltrar.add("negativo");
    	this.camposNoFiltrar.add("stock_actual");
    	this.camposNoFiltrar.add("stock_servible");
    	this.camposNoFiltrar.add("stock_real");
    	this.camposNoFiltrar.add("pdte_servir");
    	this.camposNoFiltrar.add("pales");
    	this.camposNoFiltrar.add("palesPedidos");
    	this.camposNoFiltrar.add("palesStock");
    	this.camposNoFiltrar.add("palesServibles");
    	this.camposNoFiltrar.add("primerPlazo");
    	this.camposNoFiltrar.add("nombre");
    	this.camposNoFiltrar.add("loteSalidas");
//    	this.camposNoFiltrar.add("articulo");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoSituacionEnvasadora.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(5);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("negativo", "mail", "ver", "articulo","descripcion","stock_actual","palesStock","pdte_servir","palesPedidos","pdte_recibir", "stock_servible", "palesServibles", "stock_real","pales", "loteSalidas", "primerPlazo", "nombre");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("articulo", "70");
    	this.widthFiltros.put("descripcion", "300");

    	this.getColumn("mail").setHeaderCaption("");
    	this.getColumn("mail").setSortable(false);
    	this.getColumn("mail").setWidth(new Double(50));
    	this.getColumn("mail").setHidden(true);
    	
    	this.getColumn("ver").setHeaderCaption("");
    	this.getColumn("ver").setSortable(false);
    	this.getColumn("ver").setWidth(new Double(50));

    	this.getColumn("pdte_recibir").setHeaderCaption("");
    	this.getColumn("pdte_recibir").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("negativo").setHidden(true);
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(110));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_actual").setHeaderCaption("Existencias");
    	this.getColumn("stock_actual").setWidth(new Double(125));
    	this.getColumn("stock_servible").setHeaderCaption("Ex. Servible");
    	this.getColumn("stock_servible").setWidth(new Double(125));
    	this.getColumn("pdte_servir").setHeaderCaption("Pdte. Servir");
    	this.getColumn("pdte_servir").setWidth(new Double(125));
    	this.getColumn("stock_real").setHeaderCaption("Stock Real");
    	this.getColumn("stock_real").setWidth(new Double(125));
    	this.getColumn("primerPlazo").setHeaderCaption("Fecha Carga");
    	this.getColumn("primerPlazo").setWidth(new Double(125));
    	this.getColumn("pales").setHeaderCaption("Resto Pales");
    	this.getColumn("pales").setWidth(new Double(125));
    	this.getColumn("palesPedidos").setHeaderCaption("Pales Pedidos");
    	this.getColumn("palesPedidos").setWidth(new Double(125));
    	this.getColumn("palesServibles").setHeaderCaption("Stock Pales");
    	this.getColumn("palesServibles").setWidth(new Double(125));
    	this.getColumn("palesStock").setHeaderCaption("Stock Pales");
    	this.getColumn("palesStock").setWidth(new Double(125));
    	this.getColumn("loteSalidas").setHeaderCaption("Lote");
    	this.getColumn("loteSalidas").setWidth(new Double(125));

    	this.getColumn("nombre").setHeaderCaption("Cliente");
    	this.getColumn("nombre").setWidth(new Double(400));

	}
	
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("mail"))
            	{
//            		MapeoSituacionEnvasadora mapeo = (MapeoSituacionEnvasadora) event.getItemId();
//            		
//	    			boolean generado = traerPedidoSeleccionado(mapeo.getArticulo(), mapeo.getStock_actual());
//	    			
//	    			if (generado)
//	    			{
//	    				enviadoMail=generacionPdf(mapeo, true, true);
//	    				if (enviadoMail)
//	    				{
//	    					mapeo.setEnviado("S");
//	    					boolean send = actualizarPedidoEnviado(mapeo);
//	    					if (!send) Notificaciones.getInstance().mensajeError("Error al marcar el pedido como enviado en GREENsys.");
//	    					refresh(mapeo, mapeo);
//	    				}
//	    			}
//					else
//					{
//						Notificaciones.getInstance().mensajeError("Error al recoger los datos del pedido de GREENsys : " + mapeo.getIdCodigoCompra().toString() + " e insertarlo en la plataforma.");
//					}
            	}
            	else if (event.getPropertyId().toString().equals("articulo"))
            	{
            		MapeoSituacionEnvasadora mapeo = (MapeoSituacionEnvasadora) event.getItemId();
            		
            		if (app!=null)
            		{
            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CM Articulo de " + mapeo.getArticulo() + " " + mapeo.getDescripcion(),mapeo.getArticulo() );
            			getUI().addWindow(vt);
            		}
            		else if (win!=null)
            		{
            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CM Articulo de " + mapeo.getArticulo() + " " + mapeo.getDescripcion(),mapeo.getArticulo() );
            			getUI().addWindow(vt);
            		}
            	}
            	else if (event.getPropertyId().toString().equals("ver"))
            	{
            		MapeoSituacionEnvasadora mapeo = (MapeoSituacionEnvasadora) event.getItemId();

            		if (app!=null)
            		{
            			pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(app.almacenConsulta, mapeo.getArticulo(), mapeo.getStock_actual(), "Lineas Pedidos Venta del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
            			getUI().addWindow(vt);
            		}
            		else if (win!=null)
            		{
            			pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(win.almacenConsulta, mapeo.getArticulo(), mapeo.getStock_actual(), "Lineas Pedidos Venta del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
            			getUI().addWindow(vt);            			
            		}
            	}
            	else if (event.getPropertyId().toString().equals("stock_actual") || event.getPropertyId().toString().equals("stock_servible"))
            	{
            		MapeoSituacionEnvasadora mapeo = (MapeoSituacionEnvasadora) event.getItemId();
            		
            		pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + mapeo.getArticulo().trim(), mapeo.getArticulo().trim(),fechaConsulta);
        			getUI().addWindow(vt);
            	}
            }
        });
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estado = null;
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("negativo".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue().toString().equals("Error"))
            			estado="Error";
            		else if (cellReference.getValue().toString().equals("Advertencia"))
            			estado = "Advertencia";
            		else
            			estado = "Ok";
            	}
            	
            	if ( "stock_real".equals(cellReference.getPropertyId()) || "pdte_servir".equals(cellReference.getPropertyId()) 
            			|| "pales".equals(cellReference.getPropertyId()) || "palesPedidos".equals(cellReference.getPropertyId()) || "palesStock".equals(cellReference.getPropertyId()))
            	{
//            		if (estado.equals("Error"))
//            		{
//            			return "Rcell-error";
//            		}
//        			else if (estado.equals("Advertencia"))
//        			{
//        				return "Rcell-warning";
//        			}
//        			else 
//        			{
        				return "Rcell-normal";
//        			}
            		
            	}
            	else if ( "stock_servible".equals(cellReference.getPropertyId()) || "palesServibles".equals(cellReference.getPropertyId()))
            	{
            		if (estado.equals("Error"))
            		{
            			return "Rcell-error";
            		}
        			else 
        			{
        				return "Rcell-pointer-green";            		
        			}
            	}
            	else if ("stock_actual".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-pointer";
            	}
            	else if ("mail".equals(cellReference.getPropertyId()))
            	{
            		return "nativebutton";
            	}
            	else if ( "ver".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonBars";
            	}
            	else
            	{
//            		if (estado.equals("Error"))
//            		{
//            			return "cell-error";
//            		}
//        			else if (estado.equals("Advertencia"))
//        			{
//        				return "cell-warning";
//        			}
//        			else 
//        			{
        				return "cell-normal";
//        			}
            	}
            }
        });
    }

	@Override
	public void calcularTotal() {
		
	}
    
//    private boolean generacionPdf(MapeoSituacionEnvasadora r_mapeo, boolean r_EnviarMail, boolean r_eliminar) 
//    {
//    	String pdfGenerado = null;
//    	String cuentaDestino = null;
//    	String cuentaCopia = null;
//    	String cuentaCopia2 = null;
//    	String asunto = null;
//    	String cuerpo = null;
//    	HashMap adjuntos = null;
//    	boolean envio = false;
//    	/*
//    	 * generacion del pdf
//    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
//    	 */
//    	consultaPedidosComprasServer cps = new consultaPedidosComprasServer(CurrentUser.get());
//    	pdfGenerado = cps.generarInforme(r_mapeo.getIdCodigo(),r_mapeo.getIdCodigoCompra());
//    	
//		if (r_EnviarMail)
//		{    			
//			cuentaDestino = r_mapeo.getMail();
//			cuentaCopia = r_mapeo.getMail2();
//			cuentaCopia2 = r_mapeo.getMail3();
//			
//			asunto = "Envío Pedido Compra " + r_mapeo.getIdCodigoCompra().toString();
//			cuerpo = "Adjunto enviamos el pedido: " + r_mapeo.getIdCodigoCompra().toString();
//			
//			adjuntos = new HashMap();
//			
//			adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
//			adjuntos.put("nombre", "pedido - " + r_mapeo.getIdCodigoCompra().toString() + ".pdf");
//			
//    		envio=ClienteMail.getInstance().envioMail(cuentaDestino, cuentaCopia, cuentaCopia2, asunto , cuerpo, adjuntos, r_eliminar);
//    		return envio;
//		}
//		else
//		{
//			RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
//		}
//		
//		pdfGenerado = null;
//    	cuentaDestino = null;
//    	cuentaCopia = null;
//    	asunto = null;
//    	cuerpo = null;
//    	adjuntos = null;
//    	
//    	return envio;
//    }
}
