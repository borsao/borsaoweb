package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.modelo.MapeoPrimerPlazo;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.CRM.albaranesVenta.server.consultaAlbaranesVentasServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo.MapeoArticulosEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo.MapeoSituacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaSituacionEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaSituacionEnvasadoraServer instance;
	
	
	public consultaSituacionEnvasadoraServer(String r_usuario) 
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSituacionEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSituacionEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoSituacionEnvasadora> datosOpcionesGlobal(String r_almacen, String r_fecha)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoSituacionEnvasadora> vector = null;
		Connection con = null;
		Statement cs = null;		

		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT a.articulo se_art, ");
        cadenaSQL.append(" b.descrip_articulo se_des, ");
        cadenaSQL.append(" sum(a.existencias) se_exa, ");	    
        cadenaSQL.append(" sum(a.pdte_servir) se_pse, ");
        cadenaSQL.append(" sum(a.pdte_recibir) se_pre ");
        
     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
     	cadenaSQL.append(" where a.articulo = b.articulo ");
     	if (r_almacen.equals("1")) cadenaSQL.append(" and (a.almacen = '1') ");
     	if (r_almacen.equals("4")) cadenaSQL.append(" and (a.almacen = '4') ");
     	if (r_almacen.equals("")) cadenaSQL.append(" and (a.almacen in  ('1','4')) ");
     	
     	cadenaSQL.append(this.obtenerWhereArticulos());
     	
     	cadenaSQL.append(" group by 1,2 ");
     	
     	cadenaSQL.append(" order by 1 ");
		try
		{
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoSituacionEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoSituacionEnvasadora mapeoSituacionEnvasadora = new MapeoSituacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSituacionEnvasadora.setArticulo(rsOpcion.getString("se_art"));
				mapeoSituacionEnvasadora.setDescripcion(rsOpcion.getString("se_des"));
				mapeoSituacionEnvasadora.setPdte_servir(rsOpcion.getInt("se_pse"));
				mapeoSituacionEnvasadora.setPdte_recibir(rsOpcion.getInt("se_pre"));
				mapeoSituacionEnvasadora.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoSituacionEnvasadora.setStock_real(rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse"));
				
				mapeoSituacionEnvasadora.setStock_servible(cis.recuperarExistenciasLotesServibles(rsOpcion.getString("se_art").trim(),r_almacen, r_fecha).intValue());
				
				/*
				 * Conversion de existencias a pales
				 */
				Double capacidadPales = this.obtenerPales(mapeoSituacionEnvasadora.getArticulo());
				
				if (capacidadPales!=null && capacidadPales !=0)
				{
					mapeoSituacionEnvasadora.setPales(new Double(mapeoSituacionEnvasadora.getStock_real()/capacidadPales));
					mapeoSituacionEnvasadora.setPalesPedidos(new Double(mapeoSituacionEnvasadora.getPdte_servir()/capacidadPales));
					mapeoSituacionEnvasadora.setPalesStock(new Double(mapeoSituacionEnvasadora.getStock_actual()/capacidadPales));
					mapeoSituacionEnvasadora.setPalesServibles(new Double(mapeoSituacionEnvasadora.getStock_servible()/capacidadPales));
				}
				else
				{
					mapeoSituacionEnvasadora.setPales(null);
					mapeoSituacionEnvasadora.setPalesPedidos(null);
					mapeoSituacionEnvasadora.setPalesStock(null);
					mapeoSituacionEnvasadora.setPalesServibles(null);
				}
				/*
				 * llamaremos a buscar el plazo de entrega 
				 * 
				 * si menor de 5 dias asignamos error
				 * si mayor de 5 dias asignamos advertencia
				 * si no nada
				 */
				if (mapeoSituacionEnvasadora.getPalesPedidos()!=null)
				{
					if (mapeoSituacionEnvasadora.getPalesPedidos().compareTo(mapeoSituacionEnvasadora.getPalesServibles())>0)
					{
						mapeoSituacionEnvasadora.setNegativo("Error");
					}
				}
				else if (mapeoSituacionEnvasadora.getStock_real()<0)
				{
					consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
					MapeoPrimerPlazo mapeoPlazo = cps.traerPrimerPlazoEntrega(con, rsOpcion.getString("se_art"),rsOpcion.getInt("se_exa"), r_almacen);
					
					if (mapeoPlazo!=null)
					{
						mapeoSituacionEnvasadora.setPrimerPlazo(mapeoPlazo.getPrimerPlazo());
						mapeoSituacionEnvasadora.setNombre(mapeoPlazo.getNombreCliente());
						int dias = RutinasFechas.restarFechas(new Date(),mapeoPlazo.getPrimerPlazo());
						
						if (dias < 5)
						{
							mapeoSituacionEnvasadora.setNegativo("Error");
						}
						else 
						{
							mapeoSituacionEnvasadora.setNegativo("Advertencia");
						}
					}
					else
					{
						mapeoSituacionEnvasadora.setPrimerPlazo(null);
						mapeoSituacionEnvasadora.setNombre("");
						mapeoSituacionEnvasadora.setNegativo("Ok");
					}

				}				
				else
				{
					mapeoSituacionEnvasadora.setNegativo("Ok");
					mapeoSituacionEnvasadora.setPrimerPlazo(null);
				}
				/*
				 * vamos a buscar la ultima salida por articulo para mostrar el lote
				 */
				consultaAlbaranesVentasServer cvs = consultaAlbaranesVentasServer.getInstance(CurrentUser.get());
				String numeroLote = cvs.traerUltimoLoteSalida(rsOpcion.getString("se_art"));
				mapeoSituacionEnvasadora.setLoteSalidas(numeroLote);
				vector.add(mapeoSituacionEnvasadora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	private String obtenerWhereArticulos()
	{
		String condicion = null;
		condicion =" and (a.articulo matches '0103*' or a.articulo = '0102003'  or a.articulo = '0102006' or a.articulo = '0102007' or a.articulo = '0102008' or a.articulo = '0102463' or a.articulo = '0102464' " +
				" or a.articulo = '0102454' or a.articulo = '0102455'  or a.articulo = '0102100' or a.articulo = '0102246') " ;

		StringBuffer condicionBuffer = null;
		
		condicionBuffer = new StringBuffer();
		
		consultaArticulosPTEnvasadoraServer cpts = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
		ArrayList<MapeoArticulosEnvasadora> vector = cpts.datosOpcionesGlobal(null);
		if (vector.size()>0)
		{
			condicionBuffer.append("and (");
			
			for (int i=0; i<vector.size();i++)
			{
				MapeoArticulosEnvasadora mp = (MapeoArticulosEnvasadora) vector.get(i);
				if (mp.getArticulo()!=null && i>0) condicionBuffer.append(" or ");
				
				if (mp.getArticulo().trim().length()==7) condicionBuffer.append(" a.articulo = '" + mp.getArticulo().trim() + "' ");
				else if (mp.getArticulo().trim().length()<7) condicionBuffer.append(" a.articulo matches '" + mp.getArticulo().trim() + "*' ");
			}
			condicionBuffer.append(") ");
		}
		else
		{
			return condicion;
		}
			
		return condicionBuffer.toString();
	}
	public String generarInforme(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(codigoInterno);
    	libImpresion.setArchivoDefinitivo("/pedido - " + numeroDocumento.toString() + ".pdf");
    	libImpresion.setArchivoPlantilla("impresoPedido.jrxml");
    	libImpresion.setArchivoPlantillaDetalle("impresoPedido_subreport0.jrxml");
    	libImpresion.setArchivoPlantillaDetalleCompilado("impresoPedido_subreport0.jasper");
    	libImpresion.setArchivoPlantillaNotas("impresoPedido_notas.jrxml");
    	libImpresion.setArchivoPlantillaNotasCompilado("impresoPedido_notas.jasper");
    	libImpresion.setCarpeta("pedidosCompra");
    	libImpresion.setBackGround("/fondoA4PedidoCompra.jpg");
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		ResultSet rsOpcion = null;
		String errores = "0";
//		Integer saldo = null;
		StringBuffer cadenaSQL =null;
		Connection con = null;
		Statement cs = null;		

		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" sum(a.existencias) se_exa, ");	    
	        cadenaSQL.append(" sum(a.pdte_servir) se_pse, ");
	        cadenaSQL.append(" sum(a.pdte_recibir) se_pre ");
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and (a.almacen ='1') ");
	     	
	     	cadenaSQL.append(this.obtenerWhereArticulos());
	     	
//	     	cadenaSQL.append(" and (a.articulo matches '0103*' or a.articulo = '0102003'  or a.articulo = '0102006' or a.articulo = '0102007' or a.articulo = '0102008' or a.articulo = '0102463' or a.articulo = '0102464' " );
//	     	cadenaSQL.append(" or a.articulo = '0102454' or a.articulo = '0102455'  or a.articulo = '0102100' or a.articulo = '0102246') " );
	     	cadenaSQL.append(" group by 1,2 ");
	     	cadenaSQL.append(" order by 1 ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsOpcion.next())
			{
				MapeoSituacionEnvasadora mapeoSituacionEnvasadora = new MapeoSituacionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSituacionEnvasadora.setArticulo(rsOpcion.getString("se_art"));
				mapeoSituacionEnvasadora.setDescripcion(rsOpcion.getString("se_des"));
				mapeoSituacionEnvasadora.setPdte_servir(rsOpcion.getInt("se_pse"));
				mapeoSituacionEnvasadora.setPdte_recibir(rsOpcion.getInt("se_pre"));
				mapeoSituacionEnvasadora.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoSituacionEnvasadora.setStock_real(rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse"));
				
				/*
				 * Conversion de existencias a pales
				 */
				Double capacidadPales = this.obtenerPales(mapeoSituacionEnvasadora.getArticulo());
				
				if (capacidadPales!=null && capacidadPales !=0)
				{
					mapeoSituacionEnvasadora.setPales(new Double(mapeoSituacionEnvasadora.getStock_real()/capacidadPales));
					mapeoSituacionEnvasadora.setPalesPedidos(new Double(mapeoSituacionEnvasadora.getPdte_servir()/capacidadPales));
					mapeoSituacionEnvasadora.setPalesStock(new Double(mapeoSituacionEnvasadora.getStock_actual()/capacidadPales));
				}
				else
				{
					mapeoSituacionEnvasadora.setPales(null);
					mapeoSituacionEnvasadora.setPalesPedidos(null);
					mapeoSituacionEnvasadora.setPalesStock(null);
				}
				/*
				 * llamaremos a buscar el plazo de entrega 
				 * 
				 * si menor de 5 dias asignamos error
				 * si mayor de 5 dias asignamos advertencia
				 * si no nada
				 */
				
				if (mapeoSituacionEnvasadora.getStock_real()<0)
				{
					consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
					MapeoPrimerPlazo mapeoPlazo = cps.traerPrimerPlazoEntrega(con, mapeoSituacionEnvasadora.getArticulo().trim(),mapeoSituacionEnvasadora.getStock_real(),"1");

					if (mapeoPlazo!=null)
					{
						mapeoSituacionEnvasadora.setPrimerPlazo(mapeoPlazo.getPrimerPlazo());
						mapeoSituacionEnvasadora.setNombre(mapeoPlazo.getNombreCliente());
						
						int dias = RutinasFechas.restarFechas(new Date(), RutinasFechas.conversionDeString(mapeoSituacionEnvasadora.getPrimerPlazo()));
						
						if (dias < 5)
						{
							mapeoSituacionEnvasadora.setNegativo("Error");
						}
						else
						{
							mapeoSituacionEnvasadora.setNegativo("Advertencia");
						}
					}
					else
					{
						mapeoSituacionEnvasadora.setPrimerPlazo(null);
						mapeoSituacionEnvasadora.setNombre("");
						mapeoSituacionEnvasadora.setNegativo("Ok");
					}
				}				
				else
				{
					mapeoSituacionEnvasadora.setNegativo("Ok");
					mapeoSituacionEnvasadora.setPrimerPlazo(null);
				}
				
//				saldo = rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse");
				
				if (mapeoSituacionEnvasadora.getNegativo().equals("Error"))
				{
					mapeoSituacionEnvasadora=null;
					errores = "2";					
				}
				else if (mapeoSituacionEnvasadora.getNegativo().equals("Advertencia"))
				{
					if (errores.equals("0")) errores = "1";
				}
				else
				{
					if (errores.equals("0")) errores="0";
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return errores ;
	}
	
	
	private Double obtenerPales(String r_articulo)
	{
		Double palesObtenidos = null;
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		palesObtenidos = ces.obtenerCantidadPaletEnvasadora(r_articulo);
		return palesObtenidos;
	}
}