package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server.consultaTiposOFServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasProgramacionesEnvasadora extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private String articuloSeleccionado = null;
	private ArrayList<String> vectorPadres = null;
	private Integer id = null;
	private Integer semana = null;
	private Integer ejercicio = null;
	private boolean global = false;
	private String estado = null;
	
	public pantallaLineasProgramacionesEnvasadora(Integer r_id, String r_titulo)
	{
		this.id=r_id;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasProgramacionesEnvasadora(Integer r_ejercicio, String r_semana, String r_titulo)
	{
		this.ejercicio=r_ejercicio;
		this.semana = new Integer(r_semana);
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	
	public pantallaLineasProgramacionesEnvasadora(String r_articulo, Integer r_ejercicio, boolean r_global)
	{
		this.id=null;
		this.articuloSeleccionado=r_articulo;
		this.ejercicio=r_ejercicio;
		this.global=r_global;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption("Programacion");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	public pantallaLineasProgramacionesEnvasadora(ArrayList<String>r_articulos, String r_estado, Integer r_ejercicio, boolean r_global)
	{
		this.id=null;
		this.articuloSeleccionado=null;
		this.vectorPadres=r_articulos;
		this.ejercicio=r_ejercicio;
		this.global=r_global;
		this.estado = r_estado;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption("Programacion");
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	private void llenarRegistros(SelectionMode r_selecet)
	{
		Iterator iterator = null;
		MapeoProgramacionEnvasadora mapeo = null;
		ArrayList<MapeoProgramacionEnvasadora> vector = null;		
		consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		String origen = "";
		
		mapeo = new MapeoProgramacionEnvasadora();
		
		
		container = new IndexedContainer();
		container.addContainerProperty("idProg", Integer.class, null);
		container.addContainerProperty("ejercicio", Integer.class, null);
		container.addContainerProperty("semana", String.class, null);
		
		
		if (this.ejercicio!=null) mapeo.setEjercicio(this.ejercicio);
		if (this.semana!=null) mapeo.setSemana(this.semana.toString());
			
		if (this.articuloSeleccionado!=null)
		{
			mapeo.setArticulo(this.articuloSeleccionado);
			if (!global) mapeo.setEstado("A");		
			if (this.ejercicio!=null) mapeo.setEjercicio(this.ejercicio);
			vector = cps.datosProgramacionGlobal(mapeo);
		}
		else if (this.vectorPadres!=null)
		{
			if (this.ejercicio!=null) mapeo.setEjercicio(this.ejercicio);
			mapeo.setEstado(this.estado);
			vector = cps.datosProgramacionGlobal(vectorPadres, mapeo,true,false);
			origen="crm";
			container.addContainerProperty("articulo", String.class, null);
			container.addContainerProperty("descripcion", String .class, null);
		}
		else if (this.id!=null)
		{
			mapeo.setIdProgramacion(this.id);
			vector = cps.datosProgramacionGlobal(mapeo);
			origen="calidad";
			container.addContainerProperty("articulo", String.class, null);
			container.addContainerProperty("descripcion", String .class, null);
		}
		else
		{
			container.addContainerProperty("dia", String.class, null);
			container.addContainerProperty("cantidad", Integer.class, null);
			container.addContainerProperty("vino", String .class, null);
			container.addContainerProperty("articulo", String.class, null);
			container.addContainerProperty("descripcion", String .class, null);			
			origen="crm";
			vector = cps.datosProgramacionGlobal(mapeo);
		}
		
		mapeo=null;
		iterator = vector.iterator();
		
		container.addContainerProperty("tipoOperacion", String.class, null);
		container.addContainerProperty("cajas", Integer.class, null);
		container.addContainerProperty("factor", Integer.class, null);
		container.addContainerProperty("unidades", Integer.class, null);
        container.addContainerProperty("observaciones", String.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoProgramacionEnvasadora) iterator.next();
			
			file.getItemProperty("idProg").setValue(mapeo.getIdProgramacion());
    		file.getItemProperty("ejercicio").setValue(mapeo.getEjercicio());
    		file.getItemProperty("semana").setValue(mapeo.getSemana());

    		if (origen.equals("calidad")||origen.equals("crm"))
    		{
        		file.getItemProperty("articulo").setValue(mapeo.getArticulo());
        		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
        		file.getItemProperty("dia").setValue(mapeo.getDia());
        		file.getItemProperty("cantidad").setValue(mapeo.getCantidad());
        		file.getItemProperty("vino").setValue(mapeo.getVino());
    		}
    		file.getItemProperty("tipoOperacion").setValue(mapeo.getTipo());
    		file.getItemProperty("unidades").setValue(mapeo.getUnidades());
    		file.getItemProperty("cajas").setValue(mapeo.getCajas());
    		file.getItemProperty("factor").setValue(mapeo.getFactor());
    		file.getItemProperty("observaciones").setValue(RutinasCadenas.conversion(mapeo.getObservaciones()));
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(r_selecet);

		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.getColumn("idProg").setHidden(true);
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("tipoOperacion".equals(cellReference.getPropertyId())) 
            	{
            		consultaTiposOFServer tiposOF = consultaTiposOFServer.getInstance(CurrentUser.get());
            		String color = tiposOF.obtenerColor(cellReference.getValue().toString());
            		return "cell-" + color.substring(1, 7);
                }
            	if ("unidades".equals(cellReference.getPropertyId()) || "cajas".equals(cellReference.getPropertyId()) || "factor".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void cerrar()
	{
		
//		MapeoProgramacionEnvasadora mapeo = new MapeoProgramacionEnvasadora();
//		Integer fila = (Integer) this.gridDatos.getSelectedRow();
//		Item item = this.gridDatos.getContainerDataSource().getItem(fila);
//		
//		mapeo.setIdProgramacion(new Integer(item.getItemProperty("idProg").getValue().toString()));
//		mapeo.setSemana(item.getItemProperty("semana").getValue().toString());
//		mapeo.setEjercicio(new Integer(item.getItemProperty("ejercicio").getValue().toString()));
//		mapeo.setDia(item.getItemProperty("dia").getValue().toString());
//		mapeo.setVino(item.getItemProperty("vino").getValue().toString());
//		mapeo.setTipo(item.getItemProperty("tipoOperacion").getValue().toString());
//		mapeo.setCantidad(new Integer(item.getItemProperty("cantidad").getValue().toString()));
//		
//		this.app.insertarDatos(mapeo);
		close();
	}
}

