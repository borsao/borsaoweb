package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoArticulosEnvasadora extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private String alias;
	private String calculaCon;
	private Integer dias;
	private Integer peso;

	public MapeoArticulosEnvasadora()
	{
		this.setArticulo("");
		this.setDescripcion("");
		this.setAlias("");
		this.setCalculaCon("");
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getCalculaCon() {
		return calculaCon;
	}

	public void setCalculaCon(String calculaCon) {
		this.calculaCon = calculaCon;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	
}