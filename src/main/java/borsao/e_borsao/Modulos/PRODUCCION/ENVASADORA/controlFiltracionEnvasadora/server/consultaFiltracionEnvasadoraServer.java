package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo.MapeoControlFiltracionEnvasadora;

public class consultaFiltracionEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaFiltracionEnvasadoraServer  instance;
	
	public consultaFiltracionEnvasadoraServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaFiltracionEnvasadoraServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaFiltracionEnvasadoraServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlFiltracionEnvasadora datosOpcionesGlobal(MapeoControlFiltracionEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		MapeoControlFiltracionEnvasadora mapeo = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_filtro_env.idCodigo fil_id, ");
		cadenaSQL.append(" qc_filtro_env.hora fil_hora, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro1 fil_pf1, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro1 fil_vf1, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro2 fil_pf2, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro2 fil_vf2, ");
		cadenaSQL.append(" qc_filtro_env.integridad fil_ein, ");
		cadenaSQL.append(" qc_filtro_env.valor_integridad fil_vin, ");
		cadenaSQL.append(" qc_filtro_env.conductividad fil_ecn, ");
		cadenaSQL.append(" qc_filtro_env.valor_conductividad fil_vcn, ");
		cadenaSQL.append(" qc_filtro_env.tipoLimpieza fil_tip, ");
		cadenaSQL.append(" qc_filtro_env.realizado fil_rea, ");
		cadenaSQL.append(" qc_filtro_env.observaciones fil_obs, ");
		cadenaSQL.append(" qc_filtro_env.idTurno fil_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_filtro_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_filtro_env.idTurno ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdTurno()!=null && r_mapeo.getIdTurno()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_produccion_env.idCodigo= " + r_mapeo.getIdTurno());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlFiltracionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("fil_id"));
				mapeo.setHora(rsOpcion.getString("fil_hora"));
				mapeo.setIdTurno(rsOpcion.getInt("fil_idt"));
				mapeo.setEstadoFiltro1(rsOpcion.getString("fil_pf1"));
				mapeo.setValorFiltro1(rsOpcion.getDouble("fil_vf1"));
				mapeo.setEstadoFiltro2(rsOpcion.getString("fil_pf2"));
				mapeo.setValorFiltro2(rsOpcion.getDouble("fil_vf2"));
				mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
				mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
				mapeo.setEstadoConductividad(rsOpcion.getString("fil_ecn"));
				mapeo.setValorConductividad(rsOpcion.getDouble("fil_vcn"));
				mapeo.setTipoLimpieza(rsOpcion.getString("fil_tip"));
				mapeo.setRealizado(rsOpcion.getString("fil_rea"));
				mapeo.setObservaciones(rsOpcion.getString("fil_obs"));
				
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				mapeo.setFecha(mapeo.getMapeo().getFecha());
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlFiltracionEnvasadora> datosOpcionesGlobal(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlFiltracionEnvasadora> vector = null;
		MapeoControlFiltracionEnvasadora mapeo = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_filtro_env.idCodigo fil_id, ");
		cadenaSQL.append(" qc_filtro_env.hora fil_hora, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro1 fil_pf1, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro1 fil_vf1, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro2 fil_pf2, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro2 fil_vf2, ");
		cadenaSQL.append(" qc_filtro_env.integridad fil_ein, ");
		cadenaSQL.append(" qc_filtro_env.valor_integridad fil_vin, ");
		cadenaSQL.append(" qc_filtro_env.conductividad fil_ecn, ");
		cadenaSQL.append(" qc_filtro_env.valor_conductividad fil_vcn, ");
		cadenaSQL.append(" qc_filtro_env.tipoLimpieza fil_tip, ");
		cadenaSQL.append(" qc_filtro_env.realizado fil_rea, ");
		cadenaSQL.append(" qc_filtro_env.observaciones fil_obs, ");
		cadenaSQL.append(" qc_filtro_env.idTurno fil_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_filtro_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_filtro_env.idTurno ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_ejercicio!=null && r_ejercicio>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.ejercicio = " + r_ejercicio);
			}
			if (r_semana!=null && r_semana>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.semana = " + r_semana);
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoControlFiltracionEnvasadora>();
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlFiltracionEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("fil_id"));
				mapeo.setHora(rsOpcion.getString("fil_hora"));
				mapeo.setIdTurno(rsOpcion.getInt("fil_idt"));
				mapeo.setEstadoFiltro1(rsOpcion.getString("fil_pf1"));
				mapeo.setValorFiltro1(rsOpcion.getDouble("fil_vf1"));
				mapeo.setEstadoFiltro2(rsOpcion.getString("fil_pf2"));
				mapeo.setValorFiltro2(rsOpcion.getDouble("fil_vf2"));
				mapeo.setEstadoIntegridad(rsOpcion.getString("fil_ein"));
				mapeo.setValorIntegridad(rsOpcion.getDouble("fil_vin"));
				mapeo.setEstadoConductividad(rsOpcion.getString("fil_ecn"));
				mapeo.setValorConductividad(rsOpcion.getDouble("fil_vcn"));
				mapeo.setTipoLimpieza(rsOpcion.getString("fil_tip"));
				mapeo.setRealizado(rsOpcion.getString("fil_rea"));
				mapeo.setObservaciones(rsOpcion.getString("fil_obs"));
				
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				mapeo.setFecha(mapeo.getMapeo().getFecha());
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public boolean comprobarFiltracion(Date r_fecha)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_filtro_env.idCodigo fil_id, ");
		cadenaSQL.append(" qc_filtro_env.hora fil_hora, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro1 fil_pf1, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro1 fil_vf1, ");
		cadenaSQL.append(" qc_filtro_env.presion_filtro2 fil_pf2, ");
		cadenaSQL.append(" qc_filtro_env.valor_filtro2 fil_vf2, ");
		cadenaSQL.append(" qc_filtro_env.integridad fil_ein, ");
		cadenaSQL.append(" qc_filtro_env.valor_integridad fil_vin, ");
		cadenaSQL.append(" qc_filtro_env.conductividad fil_ecn, ");
		cadenaSQL.append(" qc_filtro_env.valor_conductividad fil_vcn, ");
		cadenaSQL.append(" qc_filtro_env.tipoLimpieza fil_tip, ");
		cadenaSQL.append(" qc_filtro_env.realizado fil_rea, ");
		cadenaSQL.append(" qc_filtro_env.observaciones fil_obs, ");
		cadenaSQL.append(" qc_filtro_env.idTurno fil_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_filtro_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_filtro_env.idTurno ");

		try
		{
			cadenaWhere = new StringBuffer();
				
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" prd_produccion_env.fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_fecha)) + "' ");
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				return true;				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String guardarNuevo(MapeoControlFiltracionEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_filtro_env ( ");
			
			cadenaSQL.append(" qc_filtro_env.idCodigo , ");
			cadenaSQL.append(" qc_filtro_env.hora , ");
			cadenaSQL.append(" qc_filtro_env.presion_filtro1 , ");
			cadenaSQL.append(" qc_filtro_env.valor_filtro1 , ");
			cadenaSQL.append(" qc_filtro_env.presion_filtro2 , ");
			cadenaSQL.append(" qc_filtro_env.valor_filtro2 , ");
			cadenaSQL.append(" qc_filtro_env.integridad , ");
			cadenaSQL.append(" qc_filtro_env.valor_integridad , ");
			cadenaSQL.append(" qc_filtro_env.conductividad , ");
			cadenaSQL.append(" qc_filtro_env.valor_conductividad , ");
			cadenaSQL.append(" qc_filtro_env.tipoLimpieza , ");
			cadenaSQL.append(" qc_filtro_env.realizado , ");
			cadenaSQL.append(" qc_filtro_env.observaciones , ");
			cadenaSQL.append(" qc_filtro_env.idTurno ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "qc_filtro_env", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getEstadoFiltro1()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getEstadoFiltro1());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getValorFiltro1()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getValorFiltro1());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }

		    if (r_mapeo.getEstadoFiltro2()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getEstadoFiltro2());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getValorFiltro2()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getValorFiltro2());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getEstadoIntegridad()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getEstadoIntegridad());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getValorIntegridad()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getValorIntegridad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getEstadoConductividad()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getEstadoConductividad());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getValorConductividad()!=null)
		    {
		    	preparedStatement.setDouble(10, r_mapeo.getValorConductividad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(10, 0);
		    }
		    if (r_mapeo.getTipoLimpieza()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getTipoLimpieza());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(13, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(13, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoControlFiltracionEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE qc_filtro_env set ");
			
			cadenaSQL.append(" qc_filtro_env.hora =?, ");
			cadenaSQL.append(" qc_filtro_env.presion_filtro1 =?, ");
			cadenaSQL.append(" qc_filtro_env.valor_filtro1 =?, ");
			cadenaSQL.append(" qc_filtro_env.presion_filtro2 =?, ");
			cadenaSQL.append(" qc_filtro_env.valor_filtro2 =?, ");
			cadenaSQL.append(" qc_filtro_env.integridad =?, ");
			cadenaSQL.append(" qc_filtro_env.valor_integridad =?, ");
			cadenaSQL.append(" qc_filtro_env.conductividad =?, ");
			cadenaSQL.append(" qc_filtro_env.valor_conductividad =?, ");
			cadenaSQL.append(" qc_filtro_env.tipoLimpieza =?, ");
			cadenaSQL.append(" qc_filtro_env.realizado =?, ");
			cadenaSQL.append(" qc_filtro_env.observaciones =?, ");
			cadenaSQL.append(" qc_filtro_env.idTurno =? " );
			
	        cadenaSQL.append(" WHERE qc_filtro_env.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getEstadoFiltro1()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getEstadoFiltro1());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getValorFiltro1()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getValorFiltro1());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }

		    if (r_mapeo.getEstadoFiltro2()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getEstadoFiltro2());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getValorFiltro2()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getValorFiltro2());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getEstadoIntegridad()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getEstadoIntegridad());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getValorIntegridad()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getValorIntegridad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getEstadoConductividad()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getEstadoConductividad());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getValorConductividad()!=null)
		    {
		    	preparedStatement.setDouble(9, r_mapeo.getValorConductividad());
		    }
		    else
		    {
		    	preparedStatement.setDouble(9, 0);
		    }
		    if (r_mapeo.getTipoLimpieza()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getTipoLimpieza());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    preparedStatement.setInt(14, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoControlFiltracionEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_filtro_env ");            
			cadenaSQL.append(" WHERE qc_filtro_env.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}