package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoResumenSemanaInventarioAlmacen extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	
	private Integer vino;
	
	public MapeoResumenSemanaInventarioAlmacen()
	{
		
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getVino() {
		return vino;
	}

	public void setVino(Integer vino) {
		this.vino = vino;
	}

}