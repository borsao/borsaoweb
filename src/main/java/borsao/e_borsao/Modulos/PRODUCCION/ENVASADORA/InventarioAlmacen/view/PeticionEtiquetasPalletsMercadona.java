package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view;

import java.util.ArrayList;

import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo.MapeoInventarioAlmacen;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.server.consultaInventarioAlmacenServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class PeticionEtiquetasPalletsMercadona extends Window
{
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;

	private ComboBox cmbPrinter = null;
	private TextField txtBultos = null;

	private TextField txtNumeroPedido= null;
	private TextField txtNumeroAlmacen= null;
	private TextField txtNombreAlmacen= null;
	
	private MapeoInventarioAlmacen mapeo = null;
	
	
	private InventarioAlmacenView origen = null;
	
	public PeticionEtiquetasPalletsMercadona(InventarioAlmacenView r_view, MapeoInventarioAlmacen r_mapeo)
	{
		this.origen=r_view;
		this.mapeo=r_mapeo;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
				
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.txtNumeroPedido=new TextField("Numero Pedido");
				this.txtNumeroPedido.setValue(this.mapeo.getReferencia());
				this.txtNumeroAlmacen=new TextField("Almacen");
				this.txtNumeroAlmacen.setValue(this.mapeo.getNumeroAlmacen());
				this.txtNombreAlmacen=new TextField("Nombre Almacen");
				this.txtNumeroAlmacen.setValue(this.mapeo.getNombreAlmacen());
				this.txtNumeroPedido.setEnabled(false);
				this.txtNumeroAlmacen.setEnabled(false);
				this.txtNombreAlmacen.setEnabled(false);
				
				fila1.addComponent(this.txtNumeroPedido);
				fila1.addComponent(this.txtNumeroAlmacen);
				fila1.addComponent(this.txtNombreAlmacen);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtBultos = new TextField("TOTAL BULTOS");
				this.txtBultos.setWidth("100px");
				this.txtBultos.setRequired(true);

				this.cmbPrinter=new ComboBox("Impresora");
				this.cargarCombo();
				this.cmbPrinter.setInvalidAllowed(false);
				this.cmbPrinter.setNewItemsAllowed(false);
				this.cmbPrinter.setNullSelectionAllowed(false);
				this.cmbPrinter.setRequired(true);
				this.cmbPrinter.setWidth("200px");
				
				fila2.addComponent(this.txtBultos);
				fila2.addComponent(cmbPrinter);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption("Petición");
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("450px");
		this.center();
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);
		this.cargarDatosAlmacen(this.mapeo);
		this.cargarListeners();
	}

	private void cargarListeners()
	{
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				MapeoInventarioAlmacen mapeo = null;
				if (todoEnOrden())
				{
					mapeo = new MapeoInventarioAlmacen();
					mapeo.setReferencia(txtNumeroPedido.getValue());
					mapeo.setNumeroAlmacen(txtNumeroAlmacen.getValue());
					mapeo.setNombreAlmacen(txtNombreAlmacen.getValue());
					
					consultaInventarioAlmacenServer cis = consultaInventarioAlmacenServer.getInstance(CurrentUser.get());

					for (int i = 0;i< new Integer(txtBultos.getValue()); i++)
					{
						String pdfGenerado = cis.generarInforme(mapeo, String.valueOf(i+1), txtBultos.getValue());
						
						if(pdfGenerado.length()>0) 
						{
							if (cmbPrinter.getValue().toString().toUpperCase().equals("PANTALLA"))
							{
								RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
								break;
							}
							else
							{
								String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
								RutinasFicheros.imprimir(archivo, cmbPrinter.getValue().toString());
							    RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
							}
						}
				    	else
				    		Notificaciones.getInstance().mensajeError("Error en la generacion");
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				origen.opcSalir.click();
				close();
			}
		});	
		
	}
	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		if (this.txtBultos.getValue()==null || this.txtBultos.getValue().toString().length()==0) 
		{
			devolver =false;			
			txtBultos.focus();
		}
		return devolver;
	}
	
	private void cargarCombo()
	{
		String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
			this.cmbPrinter.addItem(((MapeoImpresorasUsuarios) vector.get(i)).getImpresora());
		}
		this.cmbPrinter.setValue(defecto);
	}

	private void cargarDatosAlmacen(MapeoInventarioAlmacen r_mapeo)
	{
		consultaInventarioAlmacenServer cis = consultaInventarioAlmacenServer.getInstance(CurrentUser.get());
		mapeo.setNumeroAlmacen(cis.recuperarDatosAlmacen(this.mapeo.getIdCliente(),"Numero"));
		this.txtNumeroAlmacen.setValue(mapeo.getNumeroAlmacen());
		mapeo.setNombreAlmacen(cis.recuperarDatosAlmacen(this.mapeo.getIdCliente(),"Nombre"));
		this.txtNombreAlmacen.setValue(mapeo.getNombreAlmacen());
	}
}