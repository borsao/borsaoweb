package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.EtiquetasEnvasado.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasCodigoBarras;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Contadores.server.consultaContadoresServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasEmbotellado.modelo.MapeoEtiquetasEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEtiquetasEnvasadoServer {
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private Connection con = null;
	private static consultaEtiquetasEnvasadoServer instance;
	private static int añosCaducidad = 1; 

	public consultaEtiquetasEnvasadoServer(String r_usuario) {
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEtiquetasEnvasadoServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaEtiquetasEnvasadoServer(r_usuario);
		}
		return instance;
	}

	public String generarInforme(Integer r_id) {

		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigo(r_id);
		libImpresion.setArchivoDefinitivo("/etiqueta" + r_id.toString() + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("etiquetasEnvasadora.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasEnvasadora");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;
	}

	public boolean generarDatosEtiquetas(MapeoProgramacionEnvasadora r_mapeo,String r_lote, String r_tipo, Integer r_cajas, String r_anada, String r_cantidad, boolean r_paletCompleto) {
		String datosGenerados = null;
		String emp_ean = null;
		String prefijo=null;
		String fechaCaducidad=null;
		/*
		 * Aqui viene el codigo de access que genere los datos correctamente en
		 * cierta tabla Posteriormente el listado tirará de esta tabla para
		 * imprimir
		 */
		MapeoEtiquetasEmbotellado mapeoEtiquetas = new MapeoEtiquetasEmbotellado();

		if (r_mapeo.getDescripcion().toUpperCase().contains("1/2"))
		{
			prefijo="28";
		}
		else if (r_mapeo.getDescripcion().toUpperCase().contains("PEQ"))
		{
			prefijo="38";
			if (r_cajas==76)
			{
				prefijo="68";
				r_paletCompleto=true;
			}
		}
		else if (r_mapeo.getDescripcion().toUpperCase().contains("CAN"))
		{
			prefijo="48";
		}
		else if (r_cajas==95)
		{
			prefijo="58";
			r_paletCompleto=true;
		}
		else
		{
			prefijo="18";
		}

		String codigo = this.recuperarEan(r_mapeo.getArticulo(), prefijo);
		Integer contador = this.recuperarContador();
		
		emp_ean = "08412423120" + RutinasNumericas.formatearIntegerDigitos(contador,6);
		emp_ean = RutinasCodigoBarras.calcula_ean(emp_ean);
		

		mapeoEtiquetas.setIdPrdProgramacion(r_mapeo.getIdProgramacion());
		mapeoEtiquetas.setArticulo(r_mapeo.getArticulo());
		mapeoEtiquetas.setDescripcion(r_mapeo.getDescripcion());
		mapeoEtiquetas.setCantidad(r_mapeo.getFactor());
		mapeoEtiquetas.setCajas(r_cajas);
		mapeoEtiquetas.setBotellas(r_mapeo.getFactor() * r_cajas);
		mapeoEtiquetas.setLote(RutinasCadenas.formatCerosIzquierda(r_lote, 9));
		mapeoEtiquetas.setAnada(r_anada);
		
		mapeoEtiquetas.setCode(RutinasCodigoBarras.calcula_codigo(codigo.substring(0, 13)));
		
		/*
		 * Modificamos la fecha para calcular la caducidad de 1 año dese el lote
		 */
		Date fecha = new Date();
		fecha = RutinasFechas.conversionDeString(mapeoEtiquetas.getLote().substring(4, 6) + "/" +  mapeoEtiquetas.getLote().substring(2, 4) + "/" + mapeoEtiquetas.getLote().substring(0, 2));
		try
		{
			fechaCaducidad= RutinasFechas.fechaSumandoAños(fecha, añosCaducidad);
			if (fechaCaducidad==null || fechaCaducidad.length()==0 || new Integer(fechaCaducidad.substring(6, 10)) < new Integer(r_anada) )
			{
				mapeoEtiquetas.setFecha(RutinasFechas.fechaActualSumandoAños(añosCaducidad));
			}
			else
			{
				mapeoEtiquetas.setFecha(fechaCaducidad);
			}
		}
		catch (Exception ex)
		{
			mapeoEtiquetas.setFecha(RutinasFechas.fechaActualSumandoAños(añosCaducidad));
			Notificaciones.getInstance().mensajeAdvertencia("Revisar fecha Caducidad");
		}
		
		
		mapeoEtiquetas.setHora(RutinasFechas.horaActual());
		mapeoEtiquetas.setSscc(emp_ean);
		mapeoEtiquetas.setSscc1("00" + emp_ean + "17" + mapeoEtiquetas.getFecha().substring(8) + mapeoEtiquetas.getFecha().substring(3,5) + mapeoEtiquetas.getFecha().substring(0,2));
		mapeoEtiquetas.setSscc1t("(00)" + emp_ean + "(17)" + mapeoEtiquetas.getFecha().substring(8) + mapeoEtiquetas.getFecha().substring(3,5) + mapeoEtiquetas.getFecha().substring(0,2));
		mapeoEtiquetas.setPresscc1t("00");
		if (r_paletCompleto)
		{
			mapeoEtiquetas.setCodigo("01" + mapeoEtiquetas.getCode() + "10" + mapeoEtiquetas.getLote());
			mapeoEtiquetas.setCodigot("(01)" + mapeoEtiquetas.getCode() + "(10)" + mapeoEtiquetas.getLote());
			mapeoEtiquetas.setPrecodigot("01");
		}
		else
		{
			mapeoEtiquetas.setCodigo("02" + mapeoEtiquetas.getCode() + "10" + mapeoEtiquetas.getLote() + "37" + RutinasCadenas.formatCerosIzquierda(r_cantidad, 4));
			mapeoEtiquetas.setCodigot("(02)" + mapeoEtiquetas.getCode() + "(10)" + mapeoEtiquetas.getLote() + "(37)" + RutinasCadenas.formatCerosIzquierda(r_cantidad, 4));
			mapeoEtiquetas.setPrecodigot("02");
		}

		try {
			/*
			 * Borro datos anteriores si los hubiera
			 */
			this.eliminar(r_mapeo.getIdProgramacion());
			/*
			 * Genero los datos para la impresion
			 */
			datosGenerados = this.guardarNuevo(mapeoEtiquetas);

			if (datosGenerados == null)
			{
				this.actualizarContador(contador);
				return true;
			}
			else
				return false;
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}

		return false;
	}

	public String guardarNuevo(MapeoEtiquetasEmbotellado r_mapeo) {
		PreparedStatement preparedStatement = null;

		StringBuffer cadenaSQL = new StringBuffer();

		try {

			cadenaSQL.append(" INSERT INTO prd_etiquetas_envasadora ( ");
			cadenaSQL.append(" prd_etiquetas_envasadora.idPrdProgramacion,");
			cadenaSQL.append(" prd_etiquetas_envasadora.articulo ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.descripcion ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.sscc ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.code ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.cantidad ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.lote ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.fecha ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.hora, ");
			cadenaSQL.append(" prd_etiquetas_envasadora.cajas ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.botellas ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.codigo ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.sscc1 ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.codigot ,");
			cadenaSQL.append(" prd_etiquetas_envasadora.sscc1t, ");
			cadenaSQL.append(" prd_etiquetas_envasadora.anada, ");
			cadenaSQL.append(" prd_etiquetas_envasadora.presscc1t, ");
			cadenaSQL.append(" prd_etiquetas_envasadora.precodigot) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());

			preparedStatement.setInt(1, r_mapeo.getIdPrdProgramacion());

			if (r_mapeo.getArticulo() != null) {
				preparedStatement.setString(2, r_mapeo.getArticulo());
			} else {
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getDescripcion() != null) {
				preparedStatement.setString(3, r_mapeo.getDescripcion());
			} else {
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getSscc() != null) {
				preparedStatement.setString(4, r_mapeo.getSscc());
			} else {
				preparedStatement.setString(4, null);
			}
			if (r_mapeo.getCode() != null) {
				preparedStatement.setString(5, r_mapeo.getCode());
			} else {
				preparedStatement.setString(5, null);
			}
			if (r_mapeo.getCantidad() != null) {
				preparedStatement.setInt(6, r_mapeo.getCantidad());
			} else {
				preparedStatement.setInt(6, 0);
			}
			if (r_mapeo.getLote() != null) {
				preparedStatement.setString(7, r_mapeo.getLote());
			} else {
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getFecha() != null) {
				preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(r_mapeo.getFecha()));
			} else {
				preparedStatement.setString(8, null);
			}
			if (r_mapeo.getHora() != null) {
				preparedStatement.setString(9, r_mapeo.getHora());
			} else {
				preparedStatement.setInt(9, 0);
			}
			if (r_mapeo.getCajas() != null) {
				preparedStatement.setInt(10, r_mapeo.getCajas());
			} else {
				preparedStatement.setInt(10, 0);
			}
			if (r_mapeo.getBotellas() != null) {
				preparedStatement.setInt(11, r_mapeo.getBotellas());
			} else {
				preparedStatement.setInt(11, 0);
			}

			if (r_mapeo.getCodigo() != null) {
				preparedStatement.setString(12, r_mapeo.getCodigo());
			} else {
				preparedStatement.setString(12, null);
			}
			if (r_mapeo.getSscc1() != null) {
				preparedStatement.setString(13, r_mapeo.getSscc1());
			} else {
				preparedStatement.setString(13, null);
			}
			if (r_mapeo.getCodigot() != null) {
				preparedStatement.setString(14, r_mapeo.getCodigot());
			} else {
				preparedStatement.setString(14, null);
			}
			if (r_mapeo.getSscc1t() != null) {
				preparedStatement.setString(15, r_mapeo.getSscc1t());
			} else {
				preparedStatement.setString(15, null);
			}
			if (r_mapeo.getAnada() != null) {
				preparedStatement.setString(16, r_mapeo.getAnada());
			} else {
				preparedStatement.setString(16, null);
			}
			if (r_mapeo.getPresscc1t() != null) {
				preparedStatement.setString(17, r_mapeo.getPresscc1t());
			} else {
				preparedStatement.setString(17, null);
			}
			if (r_mapeo.getPrecodigot() != null) {
				preparedStatement.setString(18, r_mapeo.getPrecodigot());
			} else {
				preparedStatement.setString(18, null);
			}



			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}
		return null;
	}

	public void eliminar(Integer r_id) {
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try {
			cadenaSQL.append(" DELETE FROM prd_etiquetas_envasadora ");
			cadenaSQL.append(" WHERE prd_etiquetas_envasadora.idPrdProgramacion = ?");
			con = this.conManager.establecerConexion();
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		} catch (Exception ex) {
			serNotif.mensajeError(ex.getMessage());
		}
	}

	public String obtenerDescripcionArticulo(String r_articulo) 
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		
		return rdo;
	}

	private String recuperarEan(String r_articulo, String r_prefijo)
	{
		String ean_obtenido = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		ean_obtenido = cas.obtenerEan(r_articulo, r_prefijo);
		return ean_obtenido;
	}
	
	private Integer recuperarContador()
	{
		Integer contador_obtenido = null;
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
//		contador_obtenido = ccs.recuperarContador("Envasadora");
		contador_obtenido = ccs.recuperarContador("Programacion");
		
		return contador_obtenido + 1 ;
	}
	
	private void actualizarContador(Integer r_contador)
	{
		consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
//		ccs.actualizarContador(r_contador, "Envasadora");
		ccs.actualizarContador(r_contador, "Programacion");
	}

	private String recuperarDescripcion(String r_articulo, String r_sufijo)
	{
		String rdo_obtenido = null;
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
		rdo_obtenido= ces.recuperarDescripcion(r_articulo, r_sufijo);
		return rdo_obtenido;
	}

}