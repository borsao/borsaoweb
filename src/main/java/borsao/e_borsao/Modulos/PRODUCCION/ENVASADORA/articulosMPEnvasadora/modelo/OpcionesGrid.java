package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public OpcionesGrid(ArrayList<MapeoArticulos> r_vector) {
        
        this.vector=r_vector;
		this.asignarTitulo("Articulos Materia Prima Envasadora");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoArticulos.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo","descripcion");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "cell-normal";
            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
	}

	@Override
	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}

}
