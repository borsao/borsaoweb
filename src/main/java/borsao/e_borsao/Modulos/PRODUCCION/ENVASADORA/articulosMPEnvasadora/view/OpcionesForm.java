package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.view;

 import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.server.consultaArticulosMPEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoArticulos mapeoArticulosEnvasadora = null;
	 
	 private consultaArticulosMPEnvasadoraServer ser = null;	 
	 private OpcionesMPEView uw = null;
	 private BeanFieldGroup<MapeoArticulos> fieldGroup;
	 
	 public OpcionesForm(OpcionesMPEView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        fieldGroup = new BeanFieldGroup<MapeoArticulos>(MapeoArticulos.class);
        fieldGroup.bindMemberFields(this);

        this.descripcion.setCaptionAsHtml(true);
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoArticulosEnvasadora = (MapeoArticulos) uw.grid.getSelectedRow();
                eliminarOpcion(mapeoArticulosEnvasadora);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoArticulosEnvasadora = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOpcion(mapeoArticulosEnvasadora);
	 			}
	 			else
	 			{
	 			}
	 			
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0 )
				{
					ser = new consultaArticulosMPEnvasadoraServer(CurrentUser.get());
					String desc = ser.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
			}
		});
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOpcion(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.articulo.getValue()!=null && this.articulo.getValue().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue());
			opcionesEscogidas.put("descripcion", this.descripcion.getValue());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
			opcionesEscogidas.put("descripcion", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOpcion(null);
	}
	
	public void editarOpcion(MapeoArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoArticulos();
		fieldGroup.setItemDataSource(new BeanItem<MapeoArticulos>(r_mapeo));
		articulo.focus();
	}
	
	public void eliminarOpcion(MapeoArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaArticulosMPEnvasadoraServer(CurrentUser.get());
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		ser.eliminar(r_mapeo);
		
	}
	
	public void crearOpcion(MapeoArticulos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		ser = new consultaArticulosMPEnvasadoraServer(CurrentUser.get());
		String rdo = ser.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoArticulos>(r_mapeo));
			if (((OpcionesGrid) uw.grid)!=null)
			{
				((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();	
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoArticulos> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoArticulosEnvasadora= item.getBean();
            if (this.mapeoArticulosEnvasadora.getArticulo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
}
