package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaProduccionTurnoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.DesgloseTiemposEnvasadoraGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.MapeoTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.server.consultaTiemposEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaTiemposEnvasadora extends Window
{
	
	private Button btnBotonCVentana = null;
	private Button btnBotonActualizar = null;
	private Grid gridPadre= null;
	private VerticalLayout principal=null;
	private boolean hayGridPadre = false;
	
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout framePie = null;
	
	private EntradaDatosFecha txtFecha = null;
	
	private Combo cmbTipo = null;
	private Combo cmbDescripcion = null;
	private Combo cmbTurno = null;
	private TextField txtTiempo= null;
	private TextArea txtObservaciones = null;
	
	private consultaTiemposEnvasadoraServer cncs  = null;
	private consultaProduccionTurnoEnvasadoraServer cpncs  = null;
	public MapeoProgramacionEnvasadora mapeoEnvasadora = null;
	public MapeoTiemposEnvasadora mapeoTiemposEnvasadora = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaTiemposEnvasadora(String r_titulo, MapeoProgramacionEnvasadora r_mapeo)
	{
		this.setCaption(r_titulo);
		this.mapeoEnvasadora =r_mapeo;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaTiemposEnvasadoraServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.llenarRegistros();
		
		this.cargarListeners();
		this.setContent(principal);
	}

	public pantallaTiemposEnvasadora(String r_titulo, MapeoTiemposEnvasadora r_mapeo)
	{
		this.setCaption(r_titulo);
		this.mapeoTiemposEnvasadora =r_mapeo;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaTiemposEnvasadoraServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.llenarRegistros();
		
		this.setContent(principal);
		this.cargarListeners();
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha Consulta");
				if (this.mapeoTiemposEnvasadora!=null) this.txtFecha.setValue(this.mapeoTiemposEnvasadora.getFecha()); else this.txtFecha.setValue(new Date());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
    			cmbTurno= new Combo("Turno");
    			cmbTurno.setWidth("150px");
    			cmbTurno.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			cmbTurno.setNewItemsAllowed(false);
    			cmbTurno.setNullSelectionAllowed(false);

    			linea1.addComponent(this.txtFecha);
    			linea1.addComponent(this.cmbTurno);

    		
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);
    			cmbTipo= new Combo("Tipo");
    			cmbTipo.setWidth("150px");
    			cmbTipo.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			cmbTipo.setNewItemsAllowed(false);
    			cmbTipo.setNullSelectionAllowed(false);
    			cmbDescripcion= new Combo("Motivo");
    			cmbDescripcion.setWidth("220px");
    			cmbDescripcion.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			cmbDescripcion.setNewItemsAllowed(false);
    			cmbDescripcion.setNullSelectionAllowed(false);
    			
    			
    			txtTiempo = new TextField("Tiempo (Minutos)");
    			txtTiempo.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			txtTiempo.setWidth("120px");
    			
    			txtObservaciones= new TextArea("Observaciones");
    			txtObservaciones.setWidth("300px");
    			
    			linea2.addComponent(this.cmbTipo);
    			linea2.addComponent(this.cmbDescripcion);
    			linea2.addComponent(this.txtTiempo);
    			linea2.addComponent(this.txtObservaciones);
    			
    			btnBotonActualizar = new Button("Insertar");
    			btnBotonActualizar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnBotonActualizar.addStyleName(ValoTheme.BUTTON_TINY);
    			
				
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(this.btnBotonActualizar);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.cmbTipo.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
				cargarComboMotivos();
			}
		});
		txtFecha.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				llenarRegistros();
				
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		btnBotonActualizar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				actualizar();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	
		if (isHayGridPadre())
		{
			gridPadre.removeAllColumns();
			frameCentral.removeComponent(gridPadre);
			gridPadre=null;
			setHayGridPadre(false);
		}
    	
    	this.presentarGrid();
    	this.activarEntradasDatos(true);
	}

	
    private void presentarGrid()
    {
    	if (isHayGridPadre())
		{
			gridPadre.removeAllColumns();
			frameCentral.removeComponent(gridPadre);
			gridPadre=null;
			setHayGridPadre(false);
		}
    	
    	MapeoTiemposEnvasadora mapeo = null;
    	
    	if (this.mapeoTiemposEnvasadora==null)
    	{
	    	mapeo = new MapeoTiemposEnvasadora();
	    	mapeo.setIdProgramacion(this.mapeoEnvasadora.getIdProgramacion());
    	}
    	else
    		mapeo = this.mapeoTiemposEnvasadora;
    	
    	ArrayList<MapeoTiemposEnvasadora>r_vector=this.cncs.datosOpcionesGlobal(mapeo);

    	gridPadre = new DesgloseTiemposEnvasadoraGrid(this, r_vector);
    	
    	if (((DesgloseTiemposEnvasadoraGrid) gridPadre).vector==null) 
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		gridPadre.setWidth("100%");
    		gridPadre.setHeight("250px");
    		this.frameCentral.addComponent(gridPadre);
    		this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    		this.frameCentral.setMargin(true);
    		setHayGridPadre(true);
    	}
    	
    }

    private void activarEntradasDatos(boolean r_activar)
    {
    	txtFecha.setEnabled(r_activar);
    	cmbDescripcion.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    	txtTiempo.setEnabled(r_activar);
    	cmbTipo.setEnabled(r_activar);
    	cmbTurno.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	public void rellenarEntradasDatos(Integer r_reg)
	{
		/*
		 * con los datos del switch y la boca pulsada 
		 * buscaremos ne la parte servidora los datos asociados
		 * 
		 */
//		consultaNoConformidadesServer cecs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
//		MapeoDesgloseValoracionNoConformidades mapeo = new MapeoDesgloseValoracionNoConformidades();
//		
//		mapeo = cecs.datosDesgloseValoracionNoConformidadesGlobal(r_reg);
//		
//		
//		if (mapeo!=null)
//		{
//			modificando=true;
//			txtRoseta.setValue(mapeo.getRoseta());
//			txtIP.setValue(mapeo.getIp());
//			txtIdentificador.setValue(mapeo.getIdentificador());
//			txtDescripcion.setValue(mapeo.getDescripcion());
//			txtObservaciones.setValue(mapeo.getObservaciones());
//			txtUsuario.setValue(mapeo.getUser());
//			txtPasswd.setValue(mapeo.getPass());
//			txtAnyDesk.setValue(mapeo.getDireccionPublica());
//	//		txtCuenta.setValue(mapeo.getCuenta());
//		}
//		else
//		{
//			modificando=false;
//			txtRoseta.setValue("");
//			txtIP.setValue("");
//			txtIdentificador.setValue("");
//			txtDescripcion.setValue("");
//			txtObservaciones.setValue("");
//			txtUsuario.setValue("");
//			txtPasswd.setValue("");
//			txtAnyDesk.setValue("");
////			txtCuenta.setValue(mapeo.getCuenta());
//			
//			Notificaciones.getInstance().mensajeInformativo("Boca " + boca.toString() + " del switch " + r_switch + ". No hay datos asociados");
//		}
		
	}
	
	private void actualizar()
	{
		String rdo = null;
		MapeoTiemposEnvasadora mapeo = null;
		consultaTiemposEnvasadoraServer cecs = consultaTiemposEnvasadoraServer.getInstance(CurrentUser.get());
		
		if (todoEnOrden())
		{
			mapeo = new MapeoTiemposEnvasadora();
			if (this.mapeoEnvasadora.getIdProgramacion()!=null) mapeo.setIdProgramacion(this.mapeoEnvasadora.getIdProgramacion());
			mapeo.setEjercicio(this.mapeoEnvasadora.getEjercicio());
			mapeo.setSemana(new Integer(this.mapeoEnvasadora.getSemana()));
			mapeo.setTipo(cmbTipo.getValue().toString());
	    	mapeo.setDescripcion(cmbDescripcion.getValue().toString());
	    	mapeo.setTiempo(new Double(txtTiempo.getValue()));
	    	mapeo.setFecha(txtFecha.getValue());
	    	mapeo.setTurno(cmbTurno.getValue().toString());
	    	mapeo.setObservaciones(txtObservaciones.getValue());
	    	
	    	rdo = cecs.guardarNuevo(mapeo);
		}
		limpiarCampos();
		if (isHayGridPadre())
		{
			gridPadre.removeAllColumns();
			frameCentral.removeComponent(gridPadre);
			gridPadre=null;
			setHayGridPadre(false);
		}
		this.presentarGrid();
	}
	
	private void limpiarCampos()
	{
		txtTiempo.setValue("");
		cmbDescripcion.setValue(null);
		txtObservaciones.setValue("");
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la fecha");
    		return false;
    	}
    	if ((this.cmbTipo.getValue()==null || this.cmbTipo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tiempo");
    		return false;
    	}
    	if ((this.cmbTurno.getValue()==null || this.cmbTurno.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el Turno");
    		return false;
    	}
    	if ((this.cmbDescripcion.getValue()==null || this.cmbDescripcion.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el motivo parada");
    		return false;
    	}
    	if ((this.txtTiempo.getValue()==null || this.txtTiempo.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tiempo");
    		return false;
    	}
    	return true;
    }
    
    private void cargarCombos()
    {
    	this.cargarComboTipos();
    	this.cargarComboMotivos();
    	this.cargarComboTurnos();
    }
    
    private void cargarComboTipos()
    {
    	this.cmbTipo.addItem("Incidencia");
    	this.cmbTipo.addItem("Parada / Cambio");
    	
    	this.cmbTipo.setValue("Parada / Cambio");
    }

    private void cargarComboTurnos()
    {
    	this.cmbTurno.addItem("Mañana");
    	this.cmbTurno.addItem("Tarde");
    	this.cmbTurno.addItem("Noche");
    	
    	if (this.mapeoTiemposEnvasadora!=null)
    	{
    		this.cmbTurno.setValue(this.mapeoTiemposEnvasadora.getTurno());
    	}
    	else
    	{
	    	String franja = RutinasFechas.obtenerFranjaFecha(new Date());
	    	
	    	if (franja.contentEquals("1"))
	    		this.cmbTurno.setValue("Mañana");
	    	else if (franja.contentEquals("2"))
	    		this.cmbTurno.setValue("Tarde");
	    	else if (franja.contentEquals("3"))
	    		this.cmbTurno.setValue("Noche");
	    	else
	    		this.cmbTurno.setValue("Mañana");
    	}
    }

    private void cargarComboMotivos()
    {
    	this.cmbDescripcion.removeAllItems();
    	
    	switch (this.cmbTipo.getValue().toString())
    	{
    		case "Incidencia":
    		{
    			
    			this.cmbDescripcion.addItem("Sin Materiales");				
    			this.cmbDescripcion.addItem("Cambio formato palets");				
    			this.cmbDescripcion.addItem("Cambio formato garrafa");				
    			this.cmbDescripcion.addItem("Cambio de articulo (vino)");
    			this.cmbDescripcion.addItem("MicroFiltracion");
    			this.cmbDescripcion.addItem("Posimat");
    			this.cmbDescripcion.addItem("Llenadora");
    			this.cmbDescripcion.addItem("Taponadora");
    			this.cmbDescripcion.addItem("Camara Vision Art.");
    			this.cmbDescripcion.addItem("Etiquetadora");
    			this.cmbDescripcion.addItem("Formadora");
    			this.cmbDescripcion.addItem("Encajonadora");
    			this.cmbDescripcion.addItem("Cerradora");
    			this.cmbDescripcion.addItem("Robot");
    			this.cmbDescripcion.addItem("Paletizador");
    			this.cmbDescripcion.addItem("Enfardador");
    			this.cmbDescripcion.addItem("Limpiar");
    			this.cmbDescripcion.addItem("Otros");
    			break;
    		}
    		default:
    		{
    			this.cmbDescripcion.addItem("Esperar Vino");
    			this.cmbDescripcion.addItem("Esperar Ok Vino");
    			this.cmbDescripcion.addItem("Cambio formato palets");				
    			this.cmbDescripcion.addItem("Cambio formato garrafa");				
    			this.cmbDescripcion.addItem("Cambio de articulo (vino)");				
    			this.cmbDescripcion.addItem("Posimat");
    			this.cmbDescripcion.addItem("Llenadora");
    			this.cmbDescripcion.addItem("Taponadora");
    			this.cmbDescripcion.addItem("Camara Vision Art.");
    			this.cmbDescripcion.addItem("Etiquetadora");
    			this.cmbDescripcion.addItem("Formadora");
    			this.cmbDescripcion.addItem("Encajonadora");
    			this.cmbDescripcion.addItem("Cerradora");
    			this.cmbDescripcion.addItem("Robot");
    			this.cmbDescripcion.addItem("Paletizador");
    			this.cmbDescripcion.addItem("Enfardador");
    			this.cmbDescripcion.addItem("Limpiar");
    			this.cmbDescripcion.addItem("Bocadillo");
    			this.cmbDescripcion.addItem("Otros");
    			break;
    		}
    	}
    }
}