package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo;

import java.util.HashMap;

import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulosEnvasadora mapeo = null;
	 
	 public MapeoArticulosEnvasadora convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEnvasadora();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setAlias(r_hash.get("alias"));
		 this.mapeo.setCalculaCon(r_hash.get("calculaCon"));
		 
		 if (r_hash.get("dias")!=null && r_hash.get("dias").length()>0) this.mapeo.setDias(new Integer(r_hash.get("dias")));
		 if (r_hash.get("peso")!=null && r_hash.get("peso").length()>0) this.mapeo.setPeso(new Integer(r_hash.get("peso")));
		 return mapeo;		 
	 }
	 
	 public MapeoArticulosEnvasadora convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulosEnvasadora();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 this.mapeo.setAlias(r_hash.get("alias"));
		 this.mapeo.setCalculaCon(r_hash.get("calculaCon"));
		 
		 if (r_hash.get("dias")!=null && r_hash.get("dias").length()>0) this.mapeo.setDias(new Integer(r_hash.get("dias")));
		 if (r_hash.get("peso")!=null && r_hash.get("peso").length()>0) this.mapeo.setPeso(new Integer(r_hash.get("peso")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulosEnvasadora r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("alias", this.mapeo.getAlias());
		 this.hash.put("calculaCon", this.mapeo.getCalculaCon());
		 if (this.mapeo.getDias()!= null) this.hash.put("dias", this.mapeo.getDias().toString());
		 if (this.mapeo.getPeso()!= null) this.hash.put("peso", this.mapeo.getPeso().toString());
		 return hash;		 
	 }
}