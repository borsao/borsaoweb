package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoProduccionTurnoEnvasadora extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	private String turno;
	private Date fecha;
	private Double buenas;
	private Double malas;
	
	public MapeoProduccionTurnoEnvasadora()
	{
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Double getBuenas() {
		return buenas;
	}

	public void setBuenas(Double buenas) {
		this.buenas = buenas;
	}

	public Double getMalas() {
		return malas;
	}

	public void setMalas(Double malas) {
		this.malas = malas;
	}


}