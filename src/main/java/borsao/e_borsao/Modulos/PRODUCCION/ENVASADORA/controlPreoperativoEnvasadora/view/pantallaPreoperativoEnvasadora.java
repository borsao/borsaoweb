package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.modelo.MapeoControlPreoperativoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.server.consultaPreoperativoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaControlTurnoEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaPreoperativoEnvasadora extends Ventana
{
	public MapeoProgramacionEnvasadora mapeoEnvasadora = null;
	private MapeoProduccionTurnoEnvasadora mapeoProduccion = null;
	private MapeoControlPreoperativoEnvasadora mapeoControlPreoprativo = null;
	private consultaPreoperativoEnvasadoraServer cncs  = null;
	private consultaControlTurnoEnvasadoraServer ccts = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout framePie = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	
	private EntradaDatosFecha txtFecha = null;
	
	private Combo cmbTurno = null;
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	private Button btnBuscar = null;	
	
	private TextField txtHoras= null;
	private CheckBox chkSalaOk = null;
	private CheckBox chkMaquinaLimpia = null;
	private CheckBox chkAusenciaCuerpos = null;
	private CheckBox chkMaquinaOk = null;
	private CheckBox chkLineaLimpia = null;
	
	private TextField txtRealizado= null;
	private TextField txtVerificado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaPreoperativoEnvasadora(String r_titulo, MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		this.setCaption(r_titulo);
		this.mapeoProduccion = r_mapeo;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaPreoperativoEnvasadoraServer(CurrentUser.get());
    	this.ccts = new consultaControlTurnoEnvasadoraServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("550px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(false);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
    			this.cmbTurno= new Combo("Turno");
    			this.cmbTurno.setWidth("150px");
    			this.cmbTurno.setEnabled(false);
    			this.cmbTurno.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbTurno.setNewItemsAllowed(false);
    			this.cmbTurno.setNullSelectionAllowed(false);

    			btnBuscar= new Button("Buscar Partes");
    			btnBuscar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnBuscar.addStyleName(ValoTheme.BUTTON_TINY);

    			linea1.addComponent(this.txtEjercicio);
    			linea1.addComponent(this.cmbSemana);
    			linea1.addComponent(this.txtFecha);
    			linea1.addComponent(this.cmbTurno);
    			linea1.addComponent(this.btnBuscar);
    			linea1.setComponentAlignment(this.btnBuscar, Alignment.BOTTOM_LEFT);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");
    			
    			this.chkSalaOk= new CheckBox();
    			this.chkSalaOk.setCaption("Sala Limpia ?");

    			this.chkMaquinaOk= new CheckBox();
    			this.chkMaquinaOk.setCaption("Maquinas en OK ?");
    			
				this.chkAusenciaCuerpos= new CheckBox();
    			this.chkAusenciaCuerpos.setCaption("Ausencia Cuerops Extraños ?");

				this.chkMaquinaLimpia= new CheckBox();
    			this.chkMaquinaLimpia.setCaption("Maquinas Limpias ?");

				this.chkLineaLimpia= new CheckBox();
    			this.chkLineaLimpia.setCaption("Linea Limpias ?");

    			linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.chkSalaOk);
    			linea2.setComponentAlignment(this.chkSalaOk, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkMaquinaOk);
    			linea2.setComponentAlignment(this.chkMaquinaOk, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkAusenciaCuerpos);
    			linea2.setComponentAlignment(this.chkAusenciaCuerpos, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkMaquinaLimpia);
    			linea2.setComponentAlignment(this.chkMaquinaLimpia, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkLineaLimpia);
    			linea2.setComponentAlignment(this.chkLineaLimpia, Alignment.BOTTOM_LEFT);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.txtRealizado= new TextField();
    			this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");
				
    			this.txtVerificado= new TextField();
    			this.txtVerificado.setCaption("Verificado Por?");
    			this.txtVerificado.setWidth("180");
    			this.txtVerificado.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			linea3.addComponent(this.txtRealizado);
    			linea3.addComponent(this.txtVerificado);
    			linea3.addComponent(this.txtObservaciones);
				
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
				framePie.addComponent(btnBotonAVentana);		
				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBuscar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				llenarRegistros();
			}
		});

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlPreoperativoEnvasadora mapeo = new MapeoControlPreoperativoEnvasadora();
			
			if (chkAusenciaCuerpos.getValue()) mapeo.setAusencia_cuerpos("S"); else mapeo.setAusencia_cuerpos("N");
			if (chkMaquinaLimpia.getValue()) mapeo.setMaquina_limpia("S"); else mapeo.setMaquina_limpia("N");
			if (chkMaquinaOk.getValue()) mapeo.setMaquina_ok("S"); else mapeo.setMaquina_ok("N");
			if (chkLineaLimpia.getValue()) mapeo.setLinea_ok("S"); else mapeo.setLinea_ok("N");
			if (chkSalaOk.getValue()) mapeo.setSala_ok("S"); else mapeo.setSala_ok("N");
			
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setVerificado(this.txtVerificado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdTurno(this.mapeoProduccion.getIdCodigo());

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo!=null)
				{
					this.mapeoControlPreoprativo = new MapeoControlPreoperativoEnvasadora();
					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlPreoprativo.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
			}
			
			if (rdo!=null)
				Notificaciones.getInstance().mensajeError(rdo);
			else
			{
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", null, null);
				getUI().addWindow(vt);
			}
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlPreoperativoEnvasadora> r_vector=null;

    	MapeoControlPreoperativoEnvasadora mapeoPreop = new MapeoControlPreoperativoEnvasadora();
    	mapeoPreop.setIdTurno(this.mapeoProduccion.getIdCodigo());
    	
    	mapeoPreop = this.cncs.datosOpcionesGlobal(mapeoPreop);
    	
    	if (mapeoPreop!=null) this.llenarEntradasDatos(mapeoPreop);
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);
	}

	private void llenarEntradasDatos(MapeoControlPreoperativoEnvasadora r_mapeo)
	{
		this.mapeoControlPreoprativo = new MapeoControlPreoperativoEnvasadora();
		this.mapeoControlPreoprativo.setIdCodigo(r_mapeo.getIdCodigo());
		setCreacion(false);
		if (r_mapeo.getAusencia_cuerpos().contentEquals("S")) chkAusenciaCuerpos.setValue(true);
		if (r_mapeo.getMaquina_limpia().contentEquals("S")) chkMaquinaLimpia.setValue(true);
		if (r_mapeo.getMaquina_ok().contentEquals("S")) chkMaquinaOk.setValue(true);
		if (r_mapeo.getLinea_ok().contentEquals("S")) chkLineaLimpia.setValue(true);
		if (r_mapeo.getSala_ok().contentEquals("S")) chkSalaOk.setValue(true);
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtVerificado.setValue(r_mapeo.getVerificado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		
		
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
//    	this.cmbTurno.setEnabled(r_activar);
//    	this.cmbSemana.setEnabled(r_activar);
//    	this.txtEjercicio.setEnabled(r_activar);
//    	this.txtFecha.setEnabled(r_activar);
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtVerificado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.chkAusenciaCuerpos.setValue(false);
		this.chkSalaOk.setValue(false);
		this.chkMaquinaLimpia.setValue(false);
		this.chkMaquinaOk.setValue(false);
		this.chkLineaLimpia.setValue(false);
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtVerificado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Si has hecho cambios no se guardarán. Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboTurnos();
    	this.cargarComboSemana();
    }

    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
   
    private void cargarComboTurnos()
    {
    	this.cmbTurno.addItem("Mañana");
    	this.cmbTurno.addItem("Tarde");
    	this.cmbTurno.addItem("Noche");
    	
    	this.cmbTurno.setValue(this.mapeoProduccion.getTurno().toString());
    	
    }

}