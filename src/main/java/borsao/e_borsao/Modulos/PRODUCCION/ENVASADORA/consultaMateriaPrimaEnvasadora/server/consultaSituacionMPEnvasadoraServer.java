package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.modelo.MapeoInventario;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo.MapeoMPPrimerPlazo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo.MapeoSituacionMPEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaSituacionMPEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaSituacionMPEnvasadoraServer instance;
	
	
	public consultaSituacionMPEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaSituacionMPEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaSituacionMPEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoSituacionMPEnvasadora> datosOpcionesGlobal()
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoSituacionMPEnvasadora> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;		

		try
		{
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" sum(a.existencias) se_exa, ");	    
	        cadenaSQL.append(" sum(a.pdte_servir) se_pse, ");
	        cadenaSQL.append(" sum(a.pdte_recibir) se_pre ");
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.almacen = 1 ");
	     	cadenaSQL.append(" and a.articulo in (select hijo from e__lconj where padre matches '0103*' ");
	     	cadenaSQL.append(" and hijo between '0104000' and '0119999') ");
	     	cadenaSQL.append(" group by 1,2 ");
	     	cadenaSQL.append(" order by 1 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoSituacionMPEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoSituacionMPEnvasadora mapeoSituacionMPEnvasadora = new MapeoSituacionMPEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSituacionMPEnvasadora.setArticulo(rsOpcion.getString("se_art"));
				mapeoSituacionMPEnvasadora.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				
//				Double pendiente = this.recogerPendienteServir(con, rsOpcion.getString("se_art"));				
//				consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());				
//				int factor = ces.recuperarCantidadComponente(con, rsOpcion.getString("se_art").trim()).intValue();
				mapeoSituacionMPEnvasadora.setPdte_servir(rsOpcion.getInt("se_pse"));//+(pendiente.intValue()*factor));
				
				mapeoSituacionMPEnvasadora.setPdte_recibir(rsOpcion.getInt("se_pre"));
				mapeoSituacionMPEnvasadora.setStock_actual(rsOpcion.getInt("se_exa"));
				mapeoSituacionMPEnvasadora.setStock_real(rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse"));//-pendiente.intValue());
				
				/*
				 * llamaremos a buscar el plazo de entrega 
				 * 
				 * si menor de 5 dias asignamos error
				 * si mayor de 5 dias asignamos advertencia
				 * si no nada
				 */
				
				
				if (mapeoSituacionMPEnvasadora.getStock_real()<0)
				{
					consultaPedidosComprasServer cps =  consultaPedidosComprasServer.getInstance(CurrentUser.get());
					
					MapeoMPPrimerPlazo mapeoPlazo = cps.traerPrimerPlazo(con, rsOpcion.getString("se_art"));
					if (mapeoPlazo!=null)
					{
						mapeoSituacionMPEnvasadora.setPrimerPlazo(mapeoPlazo.getPrimerPlazo());
						int dias = RutinasFechas.restarFechas(new Date(), RutinasFechas.conversionDeString(mapeoSituacionMPEnvasadora.getPrimerPlazo()));
						
						if (dias < 5)
						{
							mapeoSituacionMPEnvasadora.setNegativo("Error");
						}
						else
						{
							mapeoSituacionMPEnvasadora.setNegativo("Advertencia");
						}
					}
					else
					{
						mapeoSituacionMPEnvasadora.setNegativo("Error");
						mapeoSituacionMPEnvasadora.setPrimerPlazo(null);
					}
					
				}				
				else
				{
					mapeoSituacionMPEnvasadora.setNegativo("Ok");
					mapeoSituacionMPEnvasadora.setPrimerPlazo(null);
				}

				vector.add(mapeoSituacionMPEnvasadora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	private Double recogerPendienteServir(Connection r_con, String r_articuloMP)
	{
		Double factor = new Double(0);
		
		ArrayList<String> padres = null;
		consultaAyudaProduccionServer caps = consultaAyudaProduccionServer.getInstance(CurrentUser.get());
				
		Double cantidad = caps.datosPendienteServir(r_con,r_articuloMP.trim());
		
		return cantidad;
	}

	public String generarInforme(ArrayList<MapeoSituacionMPEnvasadora> r_vector, boolean regenerar)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

		if (regenerar)
		{
			this.eliminar();
			
			for (int i=0; i< r_vector.size();i++)
			{
				MapeoSituacionMPEnvasadora mapeo = (MapeoSituacionMPEnvasadora) r_vector.get(i);
				this.guardarNuevo(mapeo);	
			}
		}		
		
    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setCodigo(1);
    	libImpresion.setArchivoDefinitivo("/calculoInventario" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setArchivoPlantilla("inventarioEnvasadora.jrxml");
    	libImpresion.setArchivoPlantillaDetalle(null);
    	libImpresion.setArchivoPlantillaDetalleCompilado(null);
    	libImpresion.setArchivoPlantillaNotas(null);
    	libImpresion.setArchivoPlantillaNotasCompilado("calculoInventario.jasper");
    	libImpresion.setCarpeta("existencias");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	public String semaforos()
	{
		String errores= "0";
		ResultSet rsOpcion = null;		
		String valores = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;		

		cadenaSQL.append(" SELECT articulo mp_art ");
		cadenaSQL.append(" from prd_mpenv ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			valores = "";
			
			while(rsOpcion.next())
			{
				if (valores.length()==0)
				{
					valores = valores + "('" + rsOpcion.getString("mp_art") + "'";
				}
				else
				{
					valores = valores + ",'" + rsOpcion.getString("mp_art") + "'";
				}
			}
			valores = valores + ")";
			
			cadenaSQL = new StringBuffer();
		
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" sum(a.existencias) se_exa, ");	    
	        cadenaSQL.append(" sum(a.pdte_servir) se_pse, ");
	        cadenaSQL.append(" sum(a.pdte_recibir) se_pre ");
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.almacen = 1 ");
	     	
	     	cadenaSQL.append(" and a.articulo in " + valores );
	     	cadenaSQL.append(" group by 1,2 ");
	     	cadenaSQL.append(" order by 1 ");

	     	rsOpcion.close();
	     	con.close();
	     	cs.close();
	     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				MapeoSituacionMPEnvasadora mapeoSituacionMPEnvasadora = new MapeoSituacionMPEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoSituacionMPEnvasadora.setStock_real(rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse"));
				
				/*
				 * llamaremos a buscar el plazo de entrega 
				 * 
				 * si menor de 5 dias asignamos error
				 * si mayor de 5 dias asignamos advertencia
				 * si no nada
				 */
				
				if (mapeoSituacionMPEnvasadora.getStock_real()<0)
				{
					mapeoSituacionMPEnvasadora.setNegativo("Error");
				}				
				else
				{
					mapeoSituacionMPEnvasadora.setNegativo("Ok");
				}
//				saldo = rsOpcion.getInt("se_exa") + rsOpcion.getInt("se_pre") - rsOpcion.getInt("se_pse");
				
				if (mapeoSituacionMPEnvasadora.getNegativo().equals("Error"))
				{
					errores="2";
					mapeoSituacionMPEnvasadora=null;
					break;
				}
			}
			con.close();
			con=null;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return errores;
	}

	public void eliminar()
	{
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		cis.eliminar();
	}

	public String guardarNuevo(MapeoSituacionMPEnvasadora r_mapeo)
	{
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		MapeoInventario mp = new MapeoInventario();
		mp.setArticulo(r_mapeo.getArticulo());
		mp.setDescripcion(r_mapeo.getDescripcion());
		mp.setStock_actual(r_mapeo.getStock_actual());
		return cis.guardarNuevo(mp);
	}
	
}