package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.view.PreoperativoEnvasadoraView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private GridViewRefresh app = null;	
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
    public OpcionesGrid(PreoperativoEnvasadoraView r_app, ArrayList<MapeoControlPreoperativoEnvasadora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
        
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}

    }
    
    private void generarGrid()
    {
    	
        this.crearGrid(MapeoControlPreoperativoEnvasadora.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
//		this.addStyleName("minigrid");
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	/*
    	 * 	private String hora;
			private String sala_ok;
			private String maquina_limpia;
			private String ausencia_cuerpos;
			private String maquina_ok;
			private String linea_ok;
			private String realizado;
			private String verificado;
			private String observaciones;

    	 */
    	setColumnOrder("fecha", "hora", "sala_ok", "maquina_limpia","ausencia_cuerpos", "maquina_ok", "linea_ok", "realizado", "verificado", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(120));
    	this.getColumn("hora").setHeaderCaption("Hora");
    	this.getColumn("hora").setWidth(new Double(100));
    	this.getColumn("sala_ok").setHeaderCaption("Sala Limpia");
    	this.getColumn("sala_ok").setWidth(new Double(120));
    	this.getColumn("maquina_limpia").setHeaderCaption("Maquina Limpia");
    	this.getColumn("maquina_limpia").setWidth(new Double(130));
    	this.getColumn("ausencia_cuerpos").setHeaderCaption("Cuerpos Extr.");
    	this.getColumn("ausencia_cuerpos").setWidth(new Double(120));
    	this.getColumn("maquina_ok").setHeaderCaption("Maquinas OK");
    	this.getColumn("maquina_ok").setWidth(new Double(120));
    	this.getColumn("linea_ok").setHeaderCaption("Linea buen Estado");
    	this.getColumn("linea_ok").setWidth(new Double(145));
    	
    	this.getColumn("realizado").setHeaderCaption("Realizado");
    	this.getColumn("realizado").setWidth(new Double(155));
    	this.getColumn("verificado").setHeaderCaption("Verificado");
    	this.getColumn("verificado").setWidth(new Double(155));
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("mapeo").setHidden(true);
    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    }

    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		
            boolean cargado = false;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("sala_ok".equals(cellReference.getPropertyId()) || "maquina_ok".equals(cellReference.getPropertyId()) || "maquina_limpia".equals(cellReference.getPropertyId()) || "ausencia_cuerpos".equals(cellReference.getPropertyId())
            			|| "linea_ok".equals(cellReference.getPropertyId()))
            	{
	            	if (cellReference!=null && cellReference.getValue()!=null && cellReference.getValue().toString().contentEquals("N"))
	            	{
	            		return "cell-warning";
	            	}
	        		else
	        		{
	        			return "cell-normal";
	            	}
            	}
            	else
        		{
        			return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
    		}
        });
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() {
		
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
	}
	
}
