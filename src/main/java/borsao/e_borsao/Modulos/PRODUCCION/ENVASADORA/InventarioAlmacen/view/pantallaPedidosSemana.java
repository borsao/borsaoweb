package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view;

import java.util.HashMap;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.CellReference;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaPedidosSemana extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	public boolean multiSelect = false;
	private Window pantallaPadre=null;
	
	public pantallaPedidosSemana(String r_ejercicio, String r_semana, HashMap<String, Double> r_cantidadesPedidas, HashMap<String, Double> r_cantidadesPedidasAnterior, String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750");
		this.setHeight("750px");
		

		this.llenarRegistros(r_ejercicio, r_semana, r_cantidadesPedidas, r_cantidadesPedidasAnterior);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	

	private void llenarRegistros(String r_ejercicio, String r_semana, HashMap<String, Double> r_vector, HashMap<String, Double> r_vectorAnterior)
	{
		container = new IndexedContainer();
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Semana", String.class, null);
		container.addContainerProperty("Unidades", Double.class, null);
        
		Integer semana = new Integer(r_semana);
		for (int i=semana+1;i<=53;i++)
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
    		file.getItemProperty("Ejercicio").setValue(new Integer(r_ejercicio)-1);
    		file.getItemProperty("Semana").setValue(String.valueOf(i));
    		file.getItemProperty("Unidades").setValue(r_vectorAnterior.get(String.valueOf(i)));
		}
		for (int i=1;i<=semana;i++)
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
    		file.getItemProperty("Ejercicio").setValue(new Integer(r_ejercicio));
    		file.getItemProperty("Semana").setValue(String.valueOf(i));
    		file.getItemProperty("Unidades").setValue(r_vector.get(String.valueOf(i)));
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
//		this.gridDatos.getColumn("estado").setHidden(true);
		this.asignarEstilos();
	}
	
	private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
		this.gridDatos.setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
	private String getCellDescription(CellReference cell) 
	{
		
		String strEstado = "";
		String descriptionText = "";
			                
		Item file = cell.getItem();
				
				
		return descriptionText;
	}

	public void asignarEstilos()
    {
		this.asignarTooltips();
		
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("unidades".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}	
            }
        });
    }
	
	private void cerrar()
	{
		close();
	}
}