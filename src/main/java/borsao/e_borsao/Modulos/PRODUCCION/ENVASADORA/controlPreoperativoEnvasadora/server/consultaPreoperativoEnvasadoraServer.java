package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.modelo.MapeoControlPreoperativoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoControlTurnoEnvasadora;

public class consultaPreoperativoEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaPreoperativoEnvasadoraServer  instance;
	
	public consultaPreoperativoEnvasadoraServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaPreoperativoEnvasadoraServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaPreoperativoEnvasadoraServer (r_usuario);			
		}
		return instance;
	}


	public MapeoControlPreoperativoEnvasadora datosOpcionesGlobal(MapeoControlPreoperativoEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		MapeoControlPreoperativoEnvasadora mapeo = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_preop_env.idCodigo pre_id, ");
		cadenaSQL.append(" qc_preop_env.hora pre_hora, ");
		cadenaSQL.append(" qc_preop_env.sala_ok pre_sok, ");
		cadenaSQL.append(" qc_preop_env.maq_limpia pre_mli, ");
		cadenaSQL.append(" qc_preop_env.ausencia_cuerpos pre_aus, ");
		cadenaSQL.append(" qc_preop_env.maq_ok pre_mok, ");
		cadenaSQL.append(" qc_preop_env.linea_ok pre_lok, ");
		cadenaSQL.append(" qc_preop_env.realizado pre_rea, ");
		cadenaSQL.append(" qc_preop_env.verificado pre_ver, ");
		cadenaSQL.append(" qc_preop_env.observaciones pre_obs, ");
		cadenaSQL.append(" qc_preop_env.idTurno pre_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_preop_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_preop_env.idTurno ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdTurno()!=null && r_mapeo.getIdTurno()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_produccion_env.idCodigo= " + r_mapeo.getIdTurno());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlPreoperativoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
				mapeo.setSala_ok(rsOpcion.getString("pre_sok"));
				mapeo.setMaquina_limpia(rsOpcion.getString("pre_mli"));
				mapeo.setAusencia_cuerpos(rsOpcion.getString("pre_aus"));
				mapeo.setMaquina_ok(rsOpcion.getString("pre_mok"));
				mapeo.setLinea_ok(rsOpcion.getString("pre_lok"));
				mapeo.setRealizado(rsOpcion.getString("pre_rea"));
				mapeo.setVerificado(rsOpcion.getString("pre_ver"));
				mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
				mapeo.setHora(rsOpcion.getString("pre_hora"));
				mapeo.setIdTurno(rsOpcion.getInt("pre_idt"));
				
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlPreoperativoEnvasadora> datosOpcionesGlobal(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoControlPreoperativoEnvasadora> vector = null;
		MapeoControlPreoperativoEnvasadora mapeo = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_preop_env.idCodigo pre_id, ");
		cadenaSQL.append(" qc_preop_env.hora pre_hora, ");
		cadenaSQL.append(" qc_preop_env.sala_ok pre_sok, ");
		cadenaSQL.append(" qc_preop_env.maq_limpia pre_mli, ");
		cadenaSQL.append(" qc_preop_env.ausencia_cuerpos pre_aus, ");
		cadenaSQL.append(" qc_preop_env.maq_ok pre_mok, ");
		cadenaSQL.append(" qc_preop_env.linea_ok pre_lok, ");
		cadenaSQL.append(" qc_preop_env.realizado pre_rea, ");
		cadenaSQL.append(" qc_preop_env.verificado pre_ver, ");
		cadenaSQL.append(" qc_preop_env.observaciones pre_obs, ");
		cadenaSQL.append(" qc_preop_env.idTurno pre_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_preop_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_preop_env.idTurno ");

		try
		{
			cadenaWhere = new StringBuffer();
				
			if (r_ejercicio!=null && r_ejercicio>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.ejercicio = " + r_ejercicio);
			}
			if (r_semana!=null && r_semana>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_produccion_env.semana = " + r_semana);
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoControlPreoperativoEnvasadora>();
			
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlPreoperativoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("pre_id"));
				mapeo.setSala_ok(rsOpcion.getString("pre_sok"));
				mapeo.setMaquina_limpia(rsOpcion.getString("pre_mli"));
				mapeo.setAusencia_cuerpos(rsOpcion.getString("pre_aus"));
				mapeo.setMaquina_ok(rsOpcion.getString("pre_mok"));
				mapeo.setLinea_ok(rsOpcion.getString("pre_lok"));
				mapeo.setRealizado(rsOpcion.getString("pre_rea"));
				mapeo.setVerificado(rsOpcion.getString("pre_ver"));
				mapeo.setObservaciones(rsOpcion.getString("pre_obs"));
				mapeo.setHora(rsOpcion.getString("pre_hora"));
				mapeo.setIdTurno(rsOpcion.getInt("pre_idt"));
				mapeo.getMapeo().setEjercicio(rsOpcion.getInt("tur_eje"));
				mapeo.getMapeo().setSemana(rsOpcion.getInt("tur_sem"));
				mapeo.getMapeo().setTurno(rsOpcion.getString("tur_tur"));
				mapeo.getMapeo().setFecha(rsOpcion.getDate("tur_fec"));
				mapeo.setFecha(mapeo.getMapeo().getFecha());
				
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public boolean comprobarPreoperativo(Date r_fecha)
	{
		ResultSet rsOpcion = null;		
		MapeoControlPreoperativoEnvasadora mapeo = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT qc_preop_env.idCodigo pre_id, ");
		cadenaSQL.append(" qc_preop_env.hora pre_hora, ");
		cadenaSQL.append(" qc_preop_env.sala_ok pre_sok, ");
		cadenaSQL.append(" qc_preop_env.maq_limpia pre_mli, ");
		cadenaSQL.append(" qc_preop_env.ausencia_cuerpos pre_aus, ");
		cadenaSQL.append(" qc_preop_env.maq_ok pre_mok, ");
		cadenaSQL.append(" qc_preop_env.linea_ok pre_lok, ");
		cadenaSQL.append(" qc_preop_env.realizado pre_rea, ");
		cadenaSQL.append(" qc_preop_env.verificado pre_ver, ");
		cadenaSQL.append(" qc_preop_env.observaciones pre_obs, ");
		cadenaSQL.append(" qc_preop_env.idTurno pre_idt," );
		
		cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
		cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
		
     	cadenaSQL.append(" FROM qc_preop_env ");
     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_preop_env.idTurno ");

		try
		{
			cadenaWhere = new StringBuffer();
				
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" prd_produccion_env.fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_fecha)) + "' ");
						
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				return true;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	
	public String guardarNuevo(MapeoControlPreoperativoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO qc_preop_env ( ");
			cadenaSQL.append(" qc_preop_env.idCodigo, ");
			cadenaSQL.append(" qc_preop_env.hora, ");
			cadenaSQL.append(" qc_preop_env.sala_ok , ");
			cadenaSQL.append(" qc_preop_env.maq_limpia , ");
			cadenaSQL.append(" qc_preop_env.ausencia_cuerpos , ");
			cadenaSQL.append(" qc_preop_env.maq_ok , ");
			cadenaSQL.append(" qc_preop_env.linea_ok , ");
			cadenaSQL.append(" qc_preop_env.realizado , ");
			cadenaSQL.append(" qc_preop_env.verificado, ");
			cadenaSQL.append(" qc_preop_env.observaciones , ");
			cadenaSQL.append(" qc_preop_env.idTurno ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "qc_preop_env", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSala_ok()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSala_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getMaquina_limpia()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getMaquina_limpia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getAusencia_cuerpos()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getAusencia_cuerpos());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getMaquina_ok()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getMaquina_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLinea_ok()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getLinea_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	

	
	public String guardarCambios(MapeoControlPreoperativoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE qc_preop_env set ");
			cadenaSQL.append(" qc_preop_env.hora=?, ");
			cadenaSQL.append(" qc_preop_env.sala_ok =?, ");
			cadenaSQL.append(" qc_preop_env.maq_limpia=?, ");
			cadenaSQL.append(" qc_preop_env.ausencia_cuerpos =?, ");
			cadenaSQL.append(" qc_preop_env.maq_ok =?, ");
			cadenaSQL.append(" qc_preop_env.linea_ok =?, ");
			cadenaSQL.append(" qc_preop_env.realizado =?, ");
			cadenaSQL.append(" qc_preop_env.verificado =?, ");
			cadenaSQL.append(" qc_preop_env.observaciones =?, ");
			cadenaSQL.append(" qc_preop_env.idTurno =? ");
	        cadenaSQL.append(" WHERE qc_preop_env.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getHora()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getHora());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getSala_ok()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getSala_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getMaquina_limpia()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getMaquina_limpia());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    if (r_mapeo.getAusencia_cuerpos()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getAusencia_cuerpos());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getMaquina_ok()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getMaquina_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getLinea_ok()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getLinea_ok());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getRealizado()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getRealizado());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getVerificado()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getVerificado());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    preparedStatement.setInt(11, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarVerificacion(MapeoControlPreoperativoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			ResultSet rsOpcion = null;		
			MapeoControlPreoperativoEnvasadora mapeo = null;
			StringBuffer cadenaWhere =null;
			
			
			cadenaSQL.append(" SELECT qc_preop_env.idCodigo pre_id, ");
			cadenaSQL.append(" qc_preop_env.hora pre_hora, ");
			cadenaSQL.append(" qc_preop_env.sala_ok pre_sok, ");
			cadenaSQL.append(" qc_preop_env.maq_limpia pre_mli, ");
			cadenaSQL.append(" qc_preop_env.ausencia_cuerpos pre_aus, ");
			cadenaSQL.append(" qc_preop_env.maq_ok pre_mok, ");
			cadenaSQL.append(" qc_preop_env.linea_ok pre_lok, ");
			cadenaSQL.append(" qc_preop_env.realizado pre_rea, ");
			cadenaSQL.append(" qc_preop_env.verificado pre_ver, ");
			cadenaSQL.append(" qc_preop_env.observaciones pre_obs, ");
			cadenaSQL.append(" qc_preop_env.idTurno pre_idt," );
			
			cadenaSQL.append(" prd_produccion_env.ejercicio tur_eje, ");
			cadenaSQL.append(" prd_produccion_env.semana tur_sem, ");
			cadenaSQL.append(" prd_produccion_env.turno tur_tur,  ");
			cadenaSQL.append(" prd_produccion_env.fecha tur_fec ");
			
	     	cadenaSQL.append(" FROM qc_preop_env ");
	     	cadenaSQL.append(" inner join prd_produccion_env on prd_produccion_env.idCodigo = qc_preop_env.idTurno ");

	     	cadenaSQL.append(" where prd_produccion_env.ejercicio = " + r_mapeo.getMapeo().getEjercicio());
	     	cadenaSQL.append(" and prd_produccion_env.semana = " + r_mapeo.getMapeo().getSemana());
	     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while (rsOpcion.next())
			{
			
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" UPDATE qc_preop_env set ");
				cadenaSQL.append(" qc_preop_env.verificado =? ");
		        cadenaSQL.append(" WHERE qc_preop_env.idCodigo=? ");
			    
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
			    if (r_mapeo.getVerificado()!=null)
			    {
			    	preparedStatement.setString(1, r_mapeo.getVerificado());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    preparedStatement.setInt(2, rsOpcion.getInt("pre_id"));
		        preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoControlTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_preop_env ");            
			cadenaSQL.append(" WHERE qc_preop_env.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}