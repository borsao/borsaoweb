package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlTurnoEnvasadora extends MapeoGlobal
{
	private String operario;
	private Double horas;
	private Double fueraLina;
	private Double ausencia;
	private String conceptoFueraLinea;
	private String conceptoAusencia;
	private Date fecha;
	private String turno;
	private String dia;
	private Integer idTurno;
	
	private MapeoProduccionTurnoEnvasadora mapeo = null;
	
	private boolean eliminar;
	

	public MapeoControlTurnoEnvasadora()
	{
		this.mapeo= new MapeoProduccionTurnoEnvasadora();
	}


	public String getOperario() {
		return operario;
	}


	public void setOperario(String operario) {
		this.operario = operario;
	}


	public Double getHoras() {
		return horas;
	}


	public void setHoras(Double horas) {
		this.horas = horas;
	}


	public Double getFueraLina() {
		return fueraLina;
	}


	public void setFueraLina(Double fueraLina) {
		this.fueraLina = fueraLina;
	}


	public Double getAusencia() {
		return ausencia;
	}


	public void setAusencia(Double ausencia) {
		this.ausencia = ausencia;
	}


	public String getConceptoFueraLinea() {
		return conceptoFueraLinea;
	}


	public void setConceptoFueraLinea(String conceptoFueraLinea) {
		this.conceptoFueraLinea = conceptoFueraLinea;
	}


	public String getConceptoAusencia() {
		return conceptoAusencia;
	}


	public void setConceptoAusencia(String conceptoAusencia) {
		this.conceptoAusencia = conceptoAusencia;
	}


	public Integer getIdTurno() {
		return idTurno;
	}


	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}


	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


	public MapeoProduccionTurnoEnvasadora getMapeo() {
		return mapeo;
	}


	public void setMapeo(MapeoProduccionTurnoEnvasadora mapeo) {
		this.mapeo = mapeo;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public String getTurno() {
		return turno;
	}


	public void setTurno(String turno) {
		this.turno = turno;
	}


	public String getDia() {
		return dia;
	}


	public void setDia(String dia) {
		this.dia = dia;
	}

}