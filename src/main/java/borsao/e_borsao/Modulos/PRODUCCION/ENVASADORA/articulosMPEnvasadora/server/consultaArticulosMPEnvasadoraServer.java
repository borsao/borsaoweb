package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosMPEnvasadoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosMPEnvasadoraServer instance;
	
	public consultaArticulosMPEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosMPEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosMPEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoArticulos> datosOpcionesGlobal(MapeoArticulos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_mpenv.articulo mpe_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo mpe_des ");
     	cadenaSQL.append(" FROM prd_mpenv ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_mpenv.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_mpenv.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_mpenv.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulos>();
			
			while(rsOpcion.next())
			{
				MapeoArticulos mapeoArticulosEnvasadora = new MapeoArticulos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosEnvasadora.setArticulo(rsOpcion.getString("mpe_art"));
				mapeoArticulosEnvasadora.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("mpe_des")));
				vector.add(mapeoArticulosEnvasadora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public String guardarNuevo(MapeoArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_mpenv ( ");
    		cadenaSQL.append(" prd_mpenv.articulo) VALUES (");
		    cadenaSQL.append(" ?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoArticulos r_mapeo)
	{
	}
	
	public void eliminar(MapeoArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_mpenv ");            
			cadenaSQL.append(" WHERE prd_mpenv.articulo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}
}