package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.modelo.MapeoArticulosEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosPTEnvasadoraServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaArticulosPTEnvasadoraServer instance;
	
	public consultaArticulosPTEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosPTEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosPTEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoArticulosEnvasadora> datosOpcionesGlobal(MapeoArticulosEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulosEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_ptenv.articulo pte_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo pte_des, ");
		cadenaSQL.append(" prd_ptenv.nombreMostrar pte_ali, ");
		cadenaSQL.append(" prd_ptenv.calculaCon pte_con, ");
		cadenaSQL.append(" prd_ptenv.diasBodega pte_dia, ");
		cadenaSQL.append(" prd_ptenv.peso pte_pes ");
     	cadenaSQL.append(" FROM prd_ptenv ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_ptenv.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptenv.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptenv.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulosEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoArticulosEnvasadora mapeoArticulosEnvasadora = new MapeoArticulosEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosEnvasadora.setArticulo(rsOpcion.getString("pte_art"));
				mapeoArticulosEnvasadora.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("pte_des")));
				mapeoArticulosEnvasadora.setAlias(rsOpcion.getString("pte_ali"));
				mapeoArticulosEnvasadora.setCalculaCon(rsOpcion.getString("pte_con"));
				mapeoArticulosEnvasadora.setDias(rsOpcion.getInt("pte_dia"));
				mapeoArticulosEnvasadora.setPeso(rsOpcion.getInt("pte_pes"));
				vector.add(mapeoArticulosEnvasadora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public boolean articuloEnvasadora(String r_articulo)
	{
		boolean rdo = false;
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_ptenv.articulo pte_art ");
		cadenaSQL.append(" FROM prd_ptenv ");
		cadenaSQL.append(" where prd_ptenv.articulo = '" + r_articulo.trim() + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				rdo=true;				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo;
	}
	
	public HashMap<String , Integer> datosPesosGlobal(MapeoArticulosEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<String , Integer> hashPesos = null;
		ArrayList<MapeoArticulosEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_ptenv.articulo pte_art, ");
		cadenaSQL.append(" prd_ptenv.peso pte_pes ");
     	cadenaSQL.append(" FROM prd_ptenv ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_ptenv.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptenv.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptenv.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			hashPesos = new HashMap<String , Integer>(); 
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				hashPesos.put(rsOpcion.getString("pte_art").trim(), rsOpcion.getInt("pte_pes"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hashPesos;
	}

	public HashMap<String , Integer> datosDiasGlobal(MapeoArticulosEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<String , Integer> hashPesos = null;
		ArrayList<MapeoArticulosEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_ptenv.articulo pte_art, ");
		cadenaSQL.append(" prd_ptenv.diasBodega pte_dia ");
		cadenaSQL.append(" FROM prd_ptenv ");
		cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_ptenv.articulo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptenv.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptenv.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hashPesos = new HashMap<String , Integer>(); 
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				hashPesos.put(rsOpcion.getString("pte_art").trim(), rsOpcion.getInt("pte_dia"));
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hashPesos;
	}
	
	public HashMap<String , Integer> datosPaletizacionesGlobal(MapeoArticulosEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		HashMap<String , Integer> hashPesos = null;
		ArrayList<MapeoArticulosEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		Connection con = null;
		Statement cs = null;		

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_ptenv.articulo pte_art ");
     	cadenaSQL.append(" FROM prd_ptenv ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_ptenv.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_ptenv.articulo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());

			hashPesos = new HashMap<String , Integer>(); 
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				hashPesos.put(rsOpcion.getString("pte_art").trim(), this.obtenerPales(rsOpcion.getString("pte_art").trim()));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return hashPesos;
	}
	
	public String guardarNuevo(MapeoArticulosEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		Statement cs = null;		

		try
		{
			cadenaSQL.append(" INSERT INTO prd_ptenv ( ");
			cadenaSQL.append(" prd_ptenv.articulo, ");
			cadenaSQL.append(" prd_ptenv.nombreMostrar, ");
			cadenaSQL.append(" prd_ptenv.calculaCon, ");
			cadenaSQL.append(" prd_ptenv.diasBodega, ");
    		cadenaSQL.append(" prd_ptenv.peso) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getAlias()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getAlias());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCalculaCon()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCalculaCon());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDias()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getDias());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getPeso()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getPeso());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public String guardarCambios(MapeoArticulosEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;

		try
		{
			cadenaSQL.append(" update prd_ptenv  SET ");
			cadenaSQL.append(" prd_ptenv.nombreMostrar=?, ");
			cadenaSQL.append(" prd_ptenv.calculaCon=?, ");
			cadenaSQL.append(" prd_ptenv.diasBodega=?, ");
    		cadenaSQL.append(" prd_ptenv.peso=? ");
    		cadenaSQL.append(" WHERE prd_ptenv.articulo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getAlias()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getAlias());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCalculaCon()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCalculaCon());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDias()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getDias());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getPeso()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getPeso());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }

		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	
	public void eliminar(MapeoArticulosEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		Connection con = null;

		try
		{
			cadenaSQL.append(" DELETE FROM prd_ptenv ");            
			cadenaSQL.append(" WHERE prd_ptenv.articulo = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}
	
	private Integer obtenerPales(String r_articulo)
	{
		Double palesObtenidos = null;
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		palesObtenidos = ces.obtenerCantidadPaletEnvasadora(r_articulo);
		return palesObtenidos.intValue();
	}

}