package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo.MapeoInventarioAlmacen;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.server.consultaInventarioAlmacenServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaCargasVinoMesa extends Window {

	private Grid grid = null;
	private Button opcSubir = null;
	private Button opcBajar = null;
	private boolean hayGrid=false;

	
	public VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	public TextField txtEjercicio= null;
	public ComboBox cmbSemana = null;
	public ComboBox cmbAlmacen = null;
	public String semana = null;


	public void generarGrid(HashMap<String, String> opcionesEscogidas) 
	{
		
    	ArrayList<MapeoInventarioAlmacen> r_vector=null;
    	consultaInventarioAlmacenServer cis = new consultaInventarioAlmacenServer(CurrentUser.get());
    	
    	Integer ejercicio = new  Integer(this.txtEjercicio.getValue());
    	Integer semana = new  Integer(this.cmbSemana.getValue().toString());
    	String almacen  = null;
		if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Almacen Capuchinos")) 
		{
			almacen= "1";
		}
		else if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Bodega Nueva")) 
		{
			almacen= "4";
		}
		else 
		{
			almacen= "";
		}

    	
    	r_vector = cis.recuperarMovimientosPedidos(ejercicio,semana,almacen);
    	
    	if (r_vector!=null && r_vector.size()>0)
    	{    	
	    	grid = new OpcionesGrid(this, r_vector);
			setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaCargasVinoMesa(String r_titulo) 
    {
    	
		this.setCaption(r_titulo);
		this.center();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);

    }

    public void cargarPantalla() 
    {
    	
    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);

    	this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setEnabled(true);
		this.txtEjercicio.setWidth("125px");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		
		this.cmbSemana= new ComboBox("Semana");    	
		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setWidth("125px");
		this.cmbSemana.setNullSelectionAllowed(false);
		
    	this.cmbAlmacen= new ComboBox("Almacen de Consulta");    		
		this.cmbAlmacen.setNewItemsAllowed(false);
		this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbAlmacen.setNullSelectionAllowed(false);
		this.cmbAlmacen.addItem("Almacen Capuchinos");
		this.cmbAlmacen.addItem("Bodega Nueva");
		this.cmbAlmacen.addItem("Todos");
		this.cmbAlmacen.setValue("Almacen Capuchinos");

		this.opcSubir= new Button();    	
		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		
		this.opcBajar= new Button();    	
		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);

    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
    	
    	this.cabLayout.addComponent(this.txtEjercicio);
    	this.cabLayout.addComponent(this.opcBajar);
		this.cabLayout.addComponent(this.cmbSemana);
		this.cabLayout.addComponent(this.opcSubir);
		this.cabLayout.setComponentAlignment(this.opcBajar, Alignment.BOTTOM_LEFT);
		this.cabLayout.setComponentAlignment(this.opcSubir, Alignment.BOTTOM_LEFT);
//		this.cabLayout.addComponent(this.cmbAlmacen);
		
		this.barAndGridLayout.addComponent(this.cabLayout);

		this.cargarListeners();
		this.generarGrid(null);
//	
    }

    private void cargarListeners()
    {
    	this.opcSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);

			}
		});

    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);

			}
		});

    	this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);
			}
		});
		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);
			}
		});
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }
    
    public void print() {
    	
    }

	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}


	private boolean isHayGrid() {
		return hayGrid;
	}

	private void setHayGrid(boolean r_hayGrid) {
		this.hayGrid = r_hayGrid;
		if (r_hayGrid)
		{
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);

			grid.addSelectionListener(new SelectionListener() {
				
				@Override
				public void select(SelectionEvent event) {
						
//						filaSeleccionada(grid.getSelectedRow());
//					else
//						mostrarFilas(grid.getSelectedRows());
				}
			});
		}
		else
		{
		}
	}

}
