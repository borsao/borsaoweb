package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.EtiquetasEnvasado.modelo;


import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEtiquetasEnvasado extends MapeoGlobal
{
	private Integer idPrdProgramacion;
	private String articulo; 
	private String descripcion;
	private String sscc;
	private String code;
	private Integer cantidad;
	private String lote; 
	private String fecha; 
	private String hora; 
	private Integer cajas; 
	private Integer botellas;
	private String codigo;
	private String sscc1; 
	private String codigot;
	private String precodigot;
	private String sscc1t;
	private String presscc1t;
	private String anada;
	
	public MapeoEtiquetasEnvasado()
	{
		
	}

	public Integer getIdPrdProgramacion() {
		return idPrdProgramacion;
	}

	public void setIdPrdProgramacion(Integer idPrdProgramacion) {
		this.idPrdProgramacion = idPrdProgramacion;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSscc() {
		return sscc;
	}

	public void setSscc(String sscc) {
		this.sscc = sscc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Integer getCajas() {
		return cajas;
	}

	public void setCajas(Integer cajas) {
		this.cajas = cajas;
	}

	public Integer getBotellas() {
		return botellas;
	}

	public void setBotellas(Integer botellas) {
		this.botellas = botellas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getSscc1() {
		return sscc1;
	}

	public void setSscc1(String sscc1) {
		this.sscc1 = sscc1;
	}

	public String getCodigot() {
		return codigot;
	}

	public void setCodigot(String codigot) {
		this.codigot = codigot;
	}

	public String getSscc1t() {
		return sscc1t;
	}

	public void setSscc1t(String sscc1t) {
		this.sscc1t = sscc1t;
	}

	public String getAnada() {
		return anada;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public String getPrecodigot() {
		return precodigot;
	}

	public void setPrecodigot(String precodigot) {
		this.precodigot = precodigot;
	}

	public String getPresscc1t() {
		return presscc1t;
	}

	public void setPresscc1t(String presscc1t) {
		this.presscc1t = presscc1t;
	}
}