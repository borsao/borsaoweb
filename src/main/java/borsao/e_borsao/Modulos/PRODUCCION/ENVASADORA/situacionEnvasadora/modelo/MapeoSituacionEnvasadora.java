package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoSituacionEnvasadora extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer stock_actual;
	private Integer pdte_servir;
	private Integer pdte_recibir;
	private Integer stock_real;
	private Integer stock_servible;
	private String ver = " ";
	private String mail = " ";
	private String negativo = " ";
	private Date primerPlazo = null;
	private Double palesStock = null;
	private Double palesPedidos = null;
	private Double palesServibles = null;
	private Double pales = null;
	private String nombre;
	private String loteSalidas;
	

	public String getLoteSalidas() {
		return loteSalidas;
	}


	public void setLoteSalidas(String loteSalidas) {
		this.loteSalidas = loteSalidas;
	}


	public MapeoSituacionEnvasadora()
	{
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getStock_actual() {
		return stock_actual;
	}


	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}


	public Integer getPdte_servir() {
		return pdte_servir;
	}


	public void setPdte_servir(Integer pdte_servir) {
		this.pdte_servir = pdte_servir;
	}


	public Integer getStock_real() {
		return stock_real;
	}


	public void setStock_real(Integer stock_real) {
		this.stock_real = stock_real;
	}


	public Integer getPdte_recibir() {
		return pdte_recibir;
	}


	public void setPdte_recibir(Integer pdte_recibir) {
		this.pdte_recibir = pdte_recibir;
	}


	public String getVer() {
		return ver;
	}


	public void setVer(String ver) {
		this.ver = ver;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getNegativo() {
		return negativo;
	}

	public Integer getStock_servible() {
		return stock_servible;
	}


	public void setStock_servible(Integer stock_servible) {
		this.stock_servible = stock_servible;
	}


	public void setNegativo(String negativo) {
		this.negativo = negativo;
	}


	public String getPrimerPlazo() {
		if (primerPlazo!=null)
			return RutinasFechas.convertirDateToString(primerPlazo);
		else
			return null;
	}


	public void setPrimerPlazo(Date primerPlazo) {
		this.primerPlazo = primerPlazo;
	}

	public Double getPales() {
		return pales;
	}


	public void setPales(Double pales) {
		this.pales = pales;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Double getPalesStock() {
		return palesStock;
	}


	public void setPalesStock(Double palesStock) {
		this.palesStock = palesStock;
	}


	public Double getPalesPedidos() {
		return palesPedidos;
	}


	public void setPalesPedidos(Double palesPedidos) {
		this.palesPedidos = palesPedidos;
	}


	public Double getPalesServibles() {
		return palesServibles;
	}


	public void setPalesServibles(Double palesServibles) {
		this.palesServibles = palesServibles;
	}


}