package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.view.FiltracionEnvasadoraView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private GridViewRefresh app = null;	
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	
    public OpcionesGrid(FiltracionEnvasadoraView r_app, ArrayList<MapeoControlFiltracionEnvasadora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
        
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}

    }
    
    private void generarGrid()
    {
    	
        this.crearGrid(MapeoControlFiltracionEnvasadora.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(2);
//		this.addStyleName("minigrid");
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	/*
    	 * 	private Integer idTurno;
			private String hora;
			private String estadoFiltro1;
			private String estadoFiltro2;
			private String estadoIntegridad;
			private String estadoConductividad;
			private String tipoLimpieza;	
			private String realizado;
			private String observaciones;
			
			private Double valorFiltro1;
			private Double valorFiltro2;
			private Double valorIntegridad;
			private Double valorConductividad;

    	 */
    	setColumnOrder("fecha", "hora", "estadoFiltro1", "valorFiltro1", "estadoFiltro2","valorFiltro2", "estadoIntegridad", "valorIntegridad", "estadoConductividad", "valorConductividad", "tipoLimpieza", "realizado", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setWidth(new Double(120));
    	this.getColumn("hora").setHeaderCaption("Hora");
    	this.getColumn("hora").setWidth(new Double(100));
    	this.getColumn("estadoFiltro1").setHeaderCaption("Filtro 1");
    	this.getColumn("estadoFiltro1").setWidth(new Double(120));
    	this.getColumn("valorFiltro1").setHeaderCaption("Valor F1");
    	this.getColumn("valorFiltro1").setWidth(new Double(130));
    	this.getColumn("estadoFiltro2").setHeaderCaption("Filtro 2");
    	this.getColumn("estadoFiltro2").setWidth(new Double(120));
    	this.getColumn("valorFiltro2").setHeaderCaption("Valor F2");
    	this.getColumn("valorFiltro2").setWidth(new Double(130));
    	this.getColumn("estadoIntegridad").setHeaderCaption("Integridad");
    	this.getColumn("estadoIntegridad").setWidth(new Double(120));
    	this.getColumn("valorIntegridad").setHeaderCaption("Valor Int.");
    	this.getColumn("valorIntegridad").setWidth(new Double(130));
    	this.getColumn("estadoConductividad").setHeaderCaption("Conductividad");
    	this.getColumn("estadoConductividad").setWidth(new Double(120));
    	this.getColumn("valorConductividad").setHeaderCaption("Valor Cond.");
    	this.getColumn("valorConductividad").setWidth(new Double(130));
    	this.getColumn("tipoLimpieza").setHeaderCaption("Limpiado con");
    	this.getColumn("tipoLimpieza").setWidth(new Double(120));
    	
    	this.getColumn("realizado").setHeaderCaption("Realizado");
    	this.getColumn("realizado").setWidth(new Double(155));
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("mapeo").setHidden(true);
    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	
    }

    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		
            boolean cargado = false;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("estadoFiltro1".equals(cellReference.getPropertyId()) || "estadoFiltro2".equals(cellReference.getPropertyId()) || "estadoIntegridad".equals(cellReference.getPropertyId()) || "estadoConductividad".equals(cellReference.getPropertyId()))
            	{
	            	if (cellReference!=null && cellReference.getValue()!=null && cellReference.getValue().toString().contentEquals("KO"))
	            	{
	            		return "cell-warning";
	            	}
	        		else
	        		{
	        			return "cell-normal";
	            	}
            	}
            	else if ("valorFiltro1".equals(cellReference.getPropertyId()) || "valorFiltro2".equals(cellReference.getPropertyId()) || "valorIntegridad".equals(cellReference.getPropertyId()) || "estadoConductividad".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
        			return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
    		}
        });
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() {
		
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
	}
	
}
