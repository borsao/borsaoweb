package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo.MapeoInventarioAlmacen;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosPTEnvasadora.server.consultaArticulosPTEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaInventarioAlmacenServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaInventarioAlmacenServer instance;
	public HashMap<String, Integer> hashPedidosSemana = null;
	
	public consultaInventarioAlmacenServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaInventarioAlmacenServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaInventarioAlmacenServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoInventarioAlmacen> recuperarMovimientosPedidos(Integer r_ejercicio,Integer r_semana, String r_almacen)
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsMovimientos = null;

		Integer pedido_old = null;
		Integer peso = null;
		ArrayList<MapeoInventarioAlmacen> vector = null;
		MapeoInventarioAlmacen mapeo =null;
		StringBuffer sql = null;
		HashMap<String, Integer> hashPesos = null;
		HashMap<String, Integer> hashPalets = null;
		
		try
		{
			
			pedido_old=new Integer(0);
			String digito = "0";
			
			/*
			 * Relleno el hash de pesos por articulo
			 */
			consultaArticulosPTEnvasadoraServer cptes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
			hashPesos = cptes.datosPesosGlobal(null); 
			/*
			 * relleno el hash de paletizaciones
			 */
			hashPalets = cptes.datosPaletizacionesGlobal(null);
			/*
			 * Me relleno el hash de cantidades pedidas por semana
			 */
			
			
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio >= ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(a.plazo_entrega) ped_eje, ");
			sql.append(" b.ref_cliente ped_ref, ");
			sql.append(" a.plazo_entrega ped_fec, ");
			sql.append(" a.articulo ped_art, " );
			sql.append(" a.codigo ped_cod, ");
			sql.append(" b.cliente ped_cli, ");
			sql.append(" b.nombre_comercial ped_nom, ");
			sql.append(" b.cumplimentacion ped_cum, ");
			sql.append(" sum(a.unidades)  ped_uds ");
			sql.append(" FROM a_vp_l_" + digito + " a, a_vp_c_" + digito + " b ");
			sql.append(" where a.clave_tabla = b.clave_tabla ");
			sql.append(" and a.documento = b.documento ");
			sql.append(" and a.serie = b.serie ");
			sql.append(" and a.codigo = b.codigo ");
			
//			if (r_semana.intValue()==RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY()))
//			{
			if (digito.contentEquals("0"))	sql.append(" and a.plazo_entrega between '" + RutinasFechas.primerDiaSemana(r_ejercicio, r_semana) + "' and '" + RutinasFechas.ultimoDiaSemana(r_ejercicio, r_semana) + "'  " );
			else sql.append(" and a.plazo_entrega between '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.primerDiaSemana(r_ejercicio, r_semana)) + "' and '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.ultimoDiaSemana(r_ejercicio, r_semana)) + "'  " );
//			}
//			else
//			{
//				sql.append(" or (a.cumplimentacion is not null and a.fecha_ult_entrega between '" + RutinasFechas.primerDiaSemana(r_ejercicio, r_semana) + "' and '" + RutinasFechas.ultimoDiaSemana(r_ejercicio, r_semana) + "')) " );
//			}
			sql.append(" and b.pais = '000' ");
			if (r_almacen!=null && r_almacen.length()>0) sql.append(" and a.almacen = " + r_almacen);
			sql.append(" and a.clave_movimiento='T'");
			
			if (digito.contentEquals("0")) sql.append(" and (a.articulo matches '0102*' or a.articulo matches '0103*' or a.articulo matches '0111*') ");
			else sql.append(" and (a.articulo like '0102%' or a.articulo like '0103%' or a.articulo like '0111%') ");
				
			sql.append(" GROUP BY 1,2,3,4,5,6,7,8 ");
			sql.append(" order by 1,3,5 ");
			
			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestionInd();
			else con= this.conManager.establecerConexionInd();
			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoInventarioAlmacen>();
			mapeo = new MapeoInventarioAlmacen();
			mapeo.setKilos(new Integer(0));
			peso = new Integer(0);
			
			while(rsMovimientos.next())
			{
				
				if (hashPesos.containsKey(rsMovimientos.getString("ped_art").trim()))
				{
					if (pedido_old.intValue()!=0 && pedido_old.intValue()!= rsMovimientos.getInt("ped_cod"))
					{
						vector.add(mapeo);
						peso = new Integer(0);
						mapeo = new MapeoInventarioAlmacen();
						mapeo.setKilos(new Integer(0));
					}
					pedido_old=rsMovimientos.getInt("ped_cod");
					mapeo.setCumplimentado(rsMovimientos.getString("ped_cum"));
					mapeo.setIdCliente(rsMovimientos.getInt("ped_cli"));
					mapeo.setCliente(rsMovimientos.getString("ped_nom"));
					mapeo.setCodigoPedido(rsMovimientos.getInt("ped_cod"));
					mapeo.setEjercicio(rsMovimientos.getInt("ped_eje"));
					mapeo.setFechaDocumento(rsMovimientos.getDate("ped_fec"));
					mapeo.setReferencia(rsMovimientos.getString("ped_ref"));
					
					
					Double valor = new Double(0);
					
					peso = hashPesos.get(rsMovimientos.getString("ped_art").trim());
					
					Integer paletizacion = hashPalets.get(rsMovimientos.getString("ped_art").trim());
					valor = rsMovimientos.getDouble("ped_uds");
					valor = valor / paletizacion;
	
					Double pesoTotal = peso * valor;
					
					mapeo.setKilos(mapeo.getKilos()+pesoTotal.intValue());
	
					switch(rsMovimientos.getString("ped_art").trim())
					{
						case "0103001":
						case "0103006":
						{
							if (mapeo.getTinto5()!= null)
							{
								valor = valor + mapeo.getTinto5();
							}
							
							mapeo.setTinto5(valor);
							
							break;
						}
						case "0103010":
						{
							if (mapeo.getRosa5()!= null)
							{
								valor = valor + mapeo.getRosa5();
							}
							mapeo.setRosa5(valor);
							break;
						}
						case "0103004":
						{
							if (mapeo.getMedia5()!= null)
							{
								valor = valor + mapeo.getMedia5();
							}
							mapeo.setMedia5(valor);
							break;
						}
						case "0103002":
						case "0103003":
						case "0103005":
						{
							if (mapeo.getTinto72()!= null)
							{
								valor = valor + mapeo.getTinto72();
							}
							mapeo.setTinto72(valor);
							break;
						}
						case "0103012":
						case "0103011":
						{
							if (mapeo.getRosa72()!= null)
							{
								valor = valor + mapeo.getRosa72();
							}
							mapeo.setRosa72(valor);
							break;
						}
						
						case "0103100":
						case "0103106":
						{
							if (mapeo.getTinto2()!= null)
							{
								valor = valor + mapeo.getTinto2();
							}
							mapeo.setTinto2(valor);
							break;
						}
						case "0103110":
						{
							if (mapeo.getRosa2()!= null)
							{
								valor = valor + mapeo.getRosa2();
							}
							mapeo.setRosa2(valor);
							break;
						}
						case "0103104":
						{
							if (mapeo.getMedia2()!= null)
							{
								valor = valor + mapeo.getMedia2();
							}
							mapeo.setMedia2(valor);
							break;
						}
						case "0103102":
						case "0103101":
						{
							if (mapeo.getTinto60()!= null)
							{
								valor = valor + mapeo.getTinto60();
							}
							mapeo.setTinto60(valor);
							break;
						}
						case "0103112":
						case "0103111":
						{
							if (mapeo.getRosa60()!= null)
							{
								valor = valor + mapeo.getRosa60();
							}
							mapeo.setRosa60(valor);
							break;
						}
						
						case "0102003":
						case "0102464":
						{
							if (mapeo.getMitica()!= null)
							{
								valor = valor + mapeo.getMitica();
							}
							mapeo.setMitica(valor);
							break;
						}
						case "0102208":
						{
							if (mapeo.getBole()!= null)
							{
								valor = valor + mapeo.getBole();
							}
							mapeo.setBole(valor);
							break;
						}
						case "0102246":
						{
							if (mapeo.getCampellas()!= null)
							{
								valor = valor + mapeo.getCampellas();
							}
							mapeo.setCampellas(valor);
							break;
						}
						case "0102100":
						{
							if (mapeo.getBorsao()!= null)
							{
								valor = valor + mapeo.getBorsao();
							}
							mapeo.setBorsao(valor);
							break;
						}
						case "0102006":
						{
							if (mapeo.getMediaMitica()!= null)
							{
								valor = valor + mapeo.getMediaMitica();
							}
							mapeo.setMediaMitica(valor);
							break;
						}
						case "0102454":
						case "0102463":
						{
							if (mapeo.getOro()!= null)
							{
								valor = valor + mapeo.getOro();
							}
							mapeo.setOro(valor);
							break;
						}
						case "0102091":
						{
							if (mapeo.getMediaOro()!= null)
							{
								valor = valor + mapeo.getMediaOro();
							}
							mapeo.setMediaOro(valor);
							break;
						}
						case "0102297": // mallacan
						case "0102515": // malacan syrahz
						{
							if (mapeo.getMallacan()!= null)
							{
								valor = valor + mapeo.getMallacan();
							}
							mapeo.setMallacan(valor);
							break;
						}
						case "0102533": // coleccion parcelas
						case "0102340":
						{
							if (mapeo.getTresla()!= null)
							{
								valor = valor + mapeo.getTresla();
							}
							mapeo.setTresla(valor);
							break;
						}
					}
					
				}
			}
			vector.add(mapeo);
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public HashMap<String,	Double> recuperarMovimientosPedidosEjercicio(Integer r_ejercicio, String r_articulos)
	{
		Connection con = null;
		Statement cs = null;
		ResultSet rsMovimientos = null;
		
		HashMap<String, Double> hash =null;
		HashMap<String, Integer> hashPalets = null;
		StringBuffer sql = null;
		try
		{
			
			consultaArticulosPTEnvasadoraServer cptes = consultaArticulosPTEnvasadoraServer.getInstance(CurrentUser.get());
			hashPalets = cptes.datosPaletizacionesGlobal(null);
			
			String digito = "0";
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_ejercicio >= ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);
			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(a.plazo_entrega) as ejercicio, ");
			for (int i = 1;i<=52;i++)
			{
				sql.append(" SUM( CASE week(a.plazo_entrega,3) WHEN '" + i + "' THEN a.unidades ELSE 0 END ) AS '" + i + "', ");
			}
			sql.append(" SUM( CASE week(a.plazo_entrega,3) WHEN '53' THEN a.unidades ELSE 0 END ) AS '53' ");
			  
			sql.append(" FROM a_vp_l_" + digito + " a, a_vp_c_" + digito + " b ");
			sql.append(" where a.clave_tabla = b.clave_tabla ");
			sql.append(" and a.documento = b.documento ");
			sql.append(" and a.serie = b.serie ");
			sql.append(" and a.codigo = b.codigo ");
			sql.append(" and b.pais = '000' ");
			sql.append(" and a.almacen = 1 ");
			sql.append(" and a.clave_movimiento='T'");
			sql.append(" and a.articulo in " + r_articulos + " " );
			
			sql.append(" GROUP BY ejercicio ");

//			if (digito.contentEquals("0")) con= this.conManager.establecerConexionGestion();
//			else 
				con= this.conManager.establecerConexionInd();
				
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			Double valor = new Double(0);
			Integer paletizacion = hashPalets.get(r_articulos.substring(2, 9));
			
			hash = new HashMap<String, Double>();
			while(rsMovimientos.next())
			{
				
				if (r_ejercicio.intValue()==rsMovimientos.getInt("ejercicio"))
				{
					for (int i = 1; i<=53;i++)
					{
						valor = rsMovimientos.getDouble(String.valueOf(i));
						valor = valor / paletizacion;
	
						hash.put(String.valueOf(i), valor.doubleValue());
					}
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos != null)
				{
					rsMovimientos.close();
					rsMovimientos=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return hash;
	}

	public String recuperarDatosAlmacen(Integer r_cliente, String r_campo)
	{
		String rdo = null;
		consultaClientesServer ccs = consultaClientesServer.getInstance(CurrentUser.get());
		rdo = ccs.recuperarDatosAlmacen(r_cliente, r_campo);
		return rdo;
	}
	
	public String generarInforme(MapeoInventarioAlmacen r_mapeo, String r_bulto, String r_bultos)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_mapeo.getReferencia());
		libImpresion.setCodigo2Txt(r_mapeo.getNumeroAlmacen());
		libImpresion.setCodigo3Txt(r_mapeo.getNombreAlmacen());
		libImpresion.setCodigo4Txt(r_bulto);
		libImpresion.setCodigo5Txt(r_bultos);
		
		libImpresion.setArchivoDefinitivo("/etiquetasMercadona" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("etiquetaPalletMercadona.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlancoH.jpg");
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("etiquetasMercadona");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	

}
