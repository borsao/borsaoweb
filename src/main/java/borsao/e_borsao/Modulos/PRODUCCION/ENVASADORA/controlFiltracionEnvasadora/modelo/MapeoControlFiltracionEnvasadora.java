package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;

public class MapeoControlFiltracionEnvasadora extends MapeoGlobal
{
	private Integer idTurno;
	private String hora;
	private String estadoFiltro1;
	private String estadoFiltro2;
	private String estadoIntegridad;
	private String estadoConductividad;
	private String tipoLimpieza;	
	private String realizado;
	private String observaciones;
	
	private Double valorFiltro1;
	private Double valorFiltro2;
	private Double valorIntegridad;
	private Double valorConductividad;
	
	private Date fecha =null;
	
	private MapeoProduccionTurnoEnvasadora mapeo = null;
	
	public MapeoControlFiltracionEnvasadora()
	{
		this.mapeo= new MapeoProduccionTurnoEnvasadora();
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstadoFiltro1() {
		return estadoFiltro1;
	}

	public void setEstadoFiltro1(String estadoFiltro1) {
		this.estadoFiltro1 = estadoFiltro1;
	}

	public String getEstadoFiltro2() {
		return estadoFiltro2;
	}

	public void setEstadoFiltro2(String estadoFiltro2) {
		this.estadoFiltro2 = estadoFiltro2;
	}

	public String getEstadoIntegridad() {
		return estadoIntegridad;
	}

	public void setEstadoIntegridad(String estadoIntegridad) {
		this.estadoIntegridad = estadoIntegridad;
	}

	public String getEstadoConductividad() {
		return estadoConductividad;
	}

	public void setEstadoConductividad(String estadoConductividad) {
		this.estadoConductividad = estadoConductividad;
	}

	public String getRealizado() {
		return realizado;
	}

	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Double getValorFiltro1() {
		return valorFiltro1;
	}

	public void setValorFiltro1(Double valorFiltro1) {
		this.valorFiltro1 = valorFiltro1;
	}

	public Double getValorFiltro2() {
		return valorFiltro2;
	}

	public void setValorFiltro2(Double valorFiltro2) {
		this.valorFiltro2 = valorFiltro2;
	}

	public Double getValorIntegridad() {
		return valorIntegridad;
	}

	public void setValorIntegridad(Double valorIntegridad) {
		this.valorIntegridad = valorIntegridad;
	}

	public Double getValorConductividad() {
		return valorConductividad;
	}

	public void setValorConductividad(Double valorConductividad) {
		this.valorConductividad = valorConductividad;
	}

	public MapeoProduccionTurnoEnvasadora getMapeo() {
		return mapeo;
	}

	public void setMapeo(MapeoProduccionTurnoEnvasadora mapeo) {
		this.mapeo = mapeo;
	}

	public String getTipoLimpieza() {
		return tipoLimpieza;
	}

	public void setTipoLimpieza(String tipoLimpieza) {
		this.tipoLimpieza = tipoLimpieza;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	
}