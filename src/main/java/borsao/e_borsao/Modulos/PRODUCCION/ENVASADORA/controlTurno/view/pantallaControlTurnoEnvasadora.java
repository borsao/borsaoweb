package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.view.pantallaFiltracionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.view.pantallaPreoperativoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.DesgloseControlTurnoEnvasadoraGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoControlTurnoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaControlTurnoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaProduccionTurnoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.MapeoTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.view.pantallaTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/************************************************************************************************************************
 * 																														*
 * 		DOCUMENTACION REFERIDA A LA PANTALLA DE CONTROL DE TURNO PARA LA PRODUCCION DEL VINO DE MESA					*
 * 																														*
 ************************************************************************************************************************
 * 																														*
 *  Este proceso es accesible desde la pantalla de la programacion														*
 *																														*
 *	Junto con este tendremos una consulta en el que dado un mes nos devolverá el resumen de horas						*
 *	e incidencias producidas durante el proceso productivo *															*
 *																														*
 *	Asi mismo tendremos sendos informes para obtener datos acerca de los controles de calidad							*
 *																														*
 *  Todos los procesos y programas se lanzan desde este mismo, a excepcion del control de tiempos de parada que se 		*
 *  lanza tambien desde la pantalla de la programacion 																	*
 *																														*
 *																														*
 ************************************************************************************************************************
 *  												TABLAS																*
 ************************************************************************************************************************
 *  																													*
 *  prd_produccion_en 																									*
 *  	Tabla principal del turno. Fecha, produccion y enlace al resto de tablas										*
 *  																													*
 * 	prd_turnos_env																										*	
 * 		Tabla para almacenar los integrantes del turno y las horas realziadas y en que se han invertido					*
 * 		Horas produccion, horas fuera linea, y horas ausencia															*
 * 																														*
 * 	prd_tiempos_envasadora																								*
 * 		Tabla deonde se almacenará todos los tiempos de parada  por incidencias o cualquier otro motivo					*
 *  																													*
 * 	qc_preop_env																										*	
 * 		Tabla para almacenar todo lo referente al preoperativo de calidad												*
 * 																														*
 * 	qc_filtro_env																										*
 * 		Tabla para almacenar todo lo referente al control del equipo de microfiltracion antes de iniciar la produccion	*
 * 																														* 
 *																														*	
 ************************************************************************************************************************
 */

public class pantallaControlTurnoEnvasadora extends Ventana
{
	public MapeoProgramacionEnvasadora mapeoEnvasadora = null;
	private MapeoProduccionTurnoEnvasadora mapeoProduccion = null;
	private consultaControlTurnoEnvasadoraServer cncs  = null;
	private consultaProduccionTurnoEnvasadoraServer cpcs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout framePie = null;

	private Grid gridPadre= null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnBuscar = null;
	private Button btnCopiar = null;
	private Button btnInsertar = null;
	private Button btnPrepoerativo = null;
	private Button btnFiltracion= null;
	private Button btnParadas = null;
	
	private EntradaDatosFecha txtFecha = null;
	
	private Combo cmbTurno = null;
	private Combo cmbSemana = null;
	
	private TextField txtEjercicio = null;
	private TextField txtBuenas = null;
	private TextField txtMalas = null;
	private Combo cmbOperario = null;
	private TextField txtHoras= null;
	
	private String year = null;
	private String week = null;
	private boolean hayGridPadre = false;
	private boolean creacion = false;
	private Integer idTurno = null;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaControlTurnoEnvasadora(String r_titulo, String r_ejercicio, String r_semana)
	{
		this.setCaption(r_titulo);
	
		this.year=r_ejercicio;
		this.week=r_semana;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaControlTurnoEnvasadoraServer(CurrentUser.get());
    	this.cpcs = new consultaProduccionTurnoEnvasadoraServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("750px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);

			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
        		this.txtEjercicio.setValue(this.year);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setValue(new Date());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
    			this.cmbTurno= new Combo("Turno");
    			this.cmbTurno.setWidth("150px");
    			this.cmbTurno.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbTurno.setNewItemsAllowed(false);
    			this.cmbTurno.setNullSelectionAllowed(false);

    			btnBuscar= new Button("Buscar Partes");
    			btnBuscar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnBuscar.addStyleName(ValoTheme.BUTTON_TINY);

    			linea1.addComponent(this.txtEjercicio);
    			linea1.addComponent(this.cmbSemana);
    			linea1.addComponent(this.txtFecha);
    			linea1.addComponent(this.cmbTurno);
    			linea1.addComponent(this.btnBuscar);
    			linea1.setComponentAlignment(this.btnBuscar, Alignment.BOTTOM_LEFT);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtBuenas = new TextField();
    			this.txtBuenas.setCaption("Produccion Buenas");
    			this.txtBuenas.setWidth("120px");
				this.txtBuenas.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtBuenas.addStyleName("rightAligned");
    			
    			this.txtMalas= new TextField();
    			this.txtMalas.setCaption("Produccion Malas");
    			this.txtMalas.setWidth("120px");
				this.txtMalas.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtMalas.addStyleName("rightAligned");

    			btnPrepoerativo= new Button("Preoperativo");
    			btnPrepoerativo.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnPrepoerativo.addStyleName(ValoTheme.BUTTON_TINY);
    			
    			btnFiltracion= new Button("Filtración");
    			btnFiltracion.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnFiltracion.addStyleName(ValoTheme.BUTTON_TINY);

    			btnParadas= new Button("Paradas de Línea");
    			btnParadas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnParadas.addStyleName(ValoTheme.BUTTON_TINY);

    			linea2.addComponent(this.txtBuenas);
    			linea2.addComponent(this.txtMalas);
    			linea2.addComponent(this.btnPrepoerativo);
    			linea2.setComponentAlignment(this.btnPrepoerativo, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.btnFiltracion);
    			linea2.setComponentAlignment(this.btnFiltracion, Alignment.BOTTOM_LEFT);
    			
    			if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).contentEquals("Master"))
    			{
    				linea2.addComponent(this.btnParadas);
    				linea2.setComponentAlignment(this.btnParadas, Alignment.BOTTOM_LEFT);
    			}

    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.cmbOperario= new Combo();
    			this.cmbOperario.setCaption("Operario");
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
				this.cmbOperario.addStyleName(ValoTheme.COMBOBOX_TINY);

    			this.txtHoras= new TextField();
    			this.txtHoras.setCaption("Horas");
    			this.txtHoras.setWidth("80px");
    			this.txtHoras.addStyleName("rightAligned");
    			this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			
    			btnInsertar= new Button("Insertar Operarios");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_PRIMARY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnCopiar= new Button("Copiar Operarios");
    			btnCopiar.addStyleName(ValoTheme.BUTTON_PRIMARY);
    			btnCopiar.addStyleName(ValoTheme.BUTTON_TINY);

    			linea3.addComponent(this.cmbOperario);
    			linea3.addComponent(this.txtHoras);
    			linea3.addComponent(this.btnInsertar);
//    			linea3.addComponent(this.btnCopiar);
    			linea3.setComponentAlignment(this.btnInsertar, Alignment.BOTTOM_LEFT);
//    			linea3.setComponentAlignment(this.btnCopiar, Alignment.BOTTOM_LEFT);
				
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
				framePie.addComponent(btnBotonAVentana);		
				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnParadas.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verParadas();
			}
		});
		
		btnPrepoerativo.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verPreoperativo();
			}
		});

		btnFiltracion.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				verEquipoFiltracion();
			}
		});

		this.cmbTurno.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
			}
		});

		btnBuscar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				llenarRegistros();
			}
		});


		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar(false);
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				insertarOperarios();
			}
		});
		btnCopiar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				copiarOperarios();
			}
		});
		
	}
	
	private void guardar(boolean r_lanzado)
	{
		String rdo = null;
		consultaProduccionTurnoEnvasadoraServer cpts = new consultaProduccionTurnoEnvasadoraServer(CurrentUser.get());
		
		if (todoEnOrdenProduccion())
		{
			if (this.mapeoProduccion == null) this.mapeoProduccion = new MapeoProduccionTurnoEnvasadora();			
			if (idTurno!=null) this.mapeoProduccion.setIdCodigo(idTurno);
			
			this.mapeoProduccion.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			this.mapeoProduccion.setSemana(new Integer(this.cmbSemana.getValue().toString()));
			this.mapeoProduccion.setTurno(this.cmbTurno.getValue().toString());
			this.mapeoProduccion.setFecha(this.txtFecha.getValue());
			this.mapeoProduccion.setBuenas(new Double(this.txtBuenas.getValue()));
			this.mapeoProduccion.setMalas(new Double(this.txtMalas.getValue()));
			
			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cpts.guardarNuevo(this.mapeoProduccion);				
				this.idTurno=this.mapeoProduccion.getIdCodigo();
				setCreacion(false);
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				rdo = cpts.guardarCambios(this.mapeoProduccion);
			}
			
			if (rdo!=null)
			{
				Notificaciones.getInstance().mensajeError(rdo);
			}	
			
		}
		
		if (!r_lanzado)
		{
			VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
			getUI().addWindow(vt);
		}
	}


	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlTurnoEnvasadora> r_vector=null;

    	if (isHayGridPadre())
    	{
    		gridPadre.removeAllColumns();
    		frameCentral.removeComponent(gridPadre);
    		gridPadre=null;
    		setHayGridPadre(false);
    	}
    	
    	MapeoProduccionTurnoEnvasadora mapeoProd = new MapeoProduccionTurnoEnvasadora();
    	mapeoProd.setFecha(this.txtFecha.getValue());
    	mapeoProd.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    	mapeoProd.setSemana(new Integer(this.cmbSemana.getValue().toString()));
    	mapeoProd.setTurno(this.cmbTurno.getValue().toString());
    	
    	mapeoProd = this.cpcs.datosOpcionesGlobal(mapeoProd);
    	
    	this.comprobarEstadoControlesCalidad();
    	
    	if (mapeoProd!=null) this.llenarEntradasDatos(mapeoProd);
    	else this.limpiarCampos();
		
    	this.presentarGrid(r_vector);
    	
    	this.activarEntradasDatos(true);
	}
	
	private void llenarEntradasDatos(MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		this.mapeoProduccion = new MapeoProduccionTurnoEnvasadora();
		this.mapeoProduccion.setIdCodigo(r_mapeo.getIdCodigo());
		this.idTurno  = r_mapeo.getIdCodigo();
		
		setCreacion(false);
		if (r_mapeo.getBuenas()!=null) this.txtBuenas.setValue(r_mapeo.getBuenas().toString()); else this.txtBuenas.setValue("0");
		if (r_mapeo.getMalas()!=null) this.txtMalas.setValue(r_mapeo.getMalas().toString()); else this.txtMalas.setValue("0");
		
		if (r_mapeo.getTurno()!=null)
		{
			if (r_mapeo.getTurno().contentEquals("M")) this.cmbTurno.setValue("Mañana");
			else if (r_mapeo.getTurno().contentEquals("T")) this.cmbTurno.setValue("Tarde");
			else if (r_mapeo.getTurno().contentEquals("N")) this.cmbTurno.setValue("Noche");
			else
			{
				if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("1")) this.cmbTurno.setValue("Mañana");
				else if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("2")) this.cmbTurno.setValue("Tarde");
				else if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("3")) this.cmbTurno.setValue("Noche");
				else this.cmbTurno.setValue("Mañana");
			}
		}
		
			
	}
	
    private void presentarGrid(ArrayList<MapeoControlTurnoEnvasadora> r_vector)
    {
    	
    	
    	MapeoControlTurnoEnvasadora mapeo = new MapeoControlTurnoEnvasadora();

    	if (this.idTurno!=null)
    	{
    		mapeo.setIdTurno(this.idTurno);
	    	
	    	r_vector=this.cncs.datosOpcionesGlobal(mapeo);
	    	
	    	gridPadre = new DesgloseControlTurnoEnvasadoraGrid(this, r_vector);
	    	
	    	if (((DesgloseControlTurnoEnvasadoraGrid) gridPadre).vector==null) 
	    	{
	    		setHayGridPadre(false);
	    	}
	    	else 
	    	{
	    		gridPadre.setWidth("100%");
	    		gridPadre.setHeight("250px");
	    		this.frameCentral.addComponent(gridPadre);
	    		this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
	    		this.frameCentral.setMargin(true);
	    		setHayGridPadre(true);
	    	}
    	}
    }

    private void activarEntradasDatos(boolean r_activar)
    {
    	txtEjercicio.setEnabled(r_activar);
    	cmbSemana.setEnabled(r_activar);
    	txtFecha.setEnabled(r_activar);
    	cmbTurno.setEnabled(r_activar);
    	txtBuenas.setEnabled(r_activar);
    	txtMalas.setEnabled(r_activar);
    	cmbOperario.setEnabled(r_activar);
    	txtHoras.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void insertarOperarios()
	{

		String rdo = null;
		MapeoControlTurnoEnvasadora mapeo = null;
		consultaControlTurnoEnvasadoraServer cecs = consultaControlTurnoEnvasadoraServer.getInstance(CurrentUser.get());
		
		if (this.idTurno==null)
		{
			this.guardar(true);
		}
		if (this.idTurno!=null)
		{
			if (todoEnOrden())
			{
				mapeo = new MapeoControlTurnoEnvasadora();
				mapeo.setIdTurno(this.idTurno);
				
				mapeo.setOperario(this.cmbOperario.getValue().toString());
				mapeo.setHoras(new Double(this.txtHoras.getValue()));
				mapeo.setFueraLina(new Double(0));
				mapeo.setAusencia(new Double(0));
				mapeo.setConceptoAusencia("");
				mapeo.setConceptoFueraLinea("");
				
				
				rdo = cecs.guardarNuevo(mapeo);
			}
			
			this.cmbOperario.setValue("");
			
			if (isHayGridPadre())
			{
				gridPadre.removeAllColumns();
				frameCentral.removeComponent(gridPadre);
				gridPadre=null;
				setHayGridPadre(false);
			}
			this.presentarGrid(null);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Has de guardar los datos del turno.");
		}

		
	}

	private void copiarOperarios()
	{

		String rdo = null;
		MapeoControlTurnoEnvasadora mapeo = null;
		consultaControlTurnoEnvasadoraServer cecs = consultaControlTurnoEnvasadoraServer.getInstance(CurrentUser.get());
		
		/*
		 * Pantalla peticion dia y turno para seleccionarlo y copiarlo 
		 */
		/*
		 * TODO Pantalla peticion dia y hora copia turno
		 */
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		this.idTurno=null;
//		if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("1")) this.cmbTurno.setValue("Mañana");
//		else if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("2")) this.cmbTurno.setValue("Tarde");
//		else if (RutinasFechas.obtenerFranjaFecha(new Date()).contentEquals("3")) this.cmbTurno.setValue("Noche");
//		else this.cmbTurno.setValue("Mañana");
		
		this.cmbOperario.setValue("");
		this.txtBuenas.setValue("0");
		this.txtMalas.setValue("0");
		this.txtHoras.setValue("8");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.cmbOperario.getValue()==null || this.cmbOperario.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el operario");
    		return false;
    	}
    	if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la fecha");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el tiempo");
    		return false;
    	}
    	if ((this.cmbTurno.getValue()==null || this.cmbTurno.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el Turno");
    		return false;
    	}
    	if ((this.cmbSemana.getValue()==null || this.cmbSemana.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la semana ");
    		return false;
    	}
    	if ((this.txtEjercicio.getValue()==null || this.txtEjercicio.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el ejercicio");
    		return false;
    	}
    	return true;
    }

    private boolean todoEnOrdenProduccion()
    {
    	if ((this.txtEjercicio.getValue()==null || this.txtEjercicio.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el ejercicio");
    		return false;
    	}
    	if ((this.cmbSemana.getValue()==null || this.cmbSemana.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la semana ");
    		return false;
    	}
    	if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar la fecha");
    		return false;
    	}
    	if ((this.cmbTurno.getValue()==null || this.cmbTurno.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el turno");
    		return false;
    	}
    	if ((this.txtBuenas.getValue()==null || this.txtBuenas.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar valor en produccion buenas");
    		return false;
    	}
    	if ((this.txtMalas.getValue()==null || this.txtMalas.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar valor en produccion malas");
    		return false;
    	}

    	return true;
    }

    private void cargarCombos()
    {
    	this.cargarComboTurnos();
    	this.cargarComboSemana();
    	this.cargarComboOperarios();
    }
    
    private void cargarComboOperarios()
    {
    	consultaOperariosServer cos = new consultaOperariosServer(CurrentUser.get());
    	ArrayList<MapeoOperarios> vector = null;
    	
    	MapeoOperarios mapeo = new MapeoOperarios();
    	mapeo.setArea("Envasadora");
    	vector = cos.datosOperariosGlobal(mapeo);
    	
    	for (int i = 0; i< vector.size();i++)
    	{
    		mapeo=vector.get(i);
    		cmbOperario.addItem(mapeo.getNombre());
    	}
    }
    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.week);
    }
   
    private void cargarComboTurnos()
    {
    	this.cmbTurno.addItem("Mañana");
    	this.cmbTurno.addItem("Tarde");
    	this.cmbTurno.addItem("Noche");
    	
    	String franja = RutinasFechas.obtenerFranjaFecha(new Date());
    	
    	if (franja.contentEquals("1"))
    		this.cmbTurno.setValue("Mañana");
    	else if (franja.contentEquals("2"))
    		this.cmbTurno.setValue("Tarde");
    	else if (franja.contentEquals("3"))
    		this.cmbTurno.setValue("Noche");
    	else
    		this.cmbTurno.setValue("Mañana");
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Si has hecho cambios no se guardarán. Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		switch (r_accion)
		{
			case "Salir":
			{
				close();
			}
			case "Copiar":
			{
				/*
				 * ejecuto el insert en mi turno
				 * y actualizo el grid
				 */

				llenarRegistros();

				break;
			}
		}
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private void verEquipoFiltracion()
	{

		if (this.idTurno==null)
		{
			this.guardar(true);
		}
		if (this.idTurno!=null)
		{
			mapeoProduccion = new MapeoProduccionTurnoEnvasadora();
			mapeoProduccion.setIdCodigo(this.idTurno);
			mapeoProduccion.setSemana(new Integer(this.cmbSemana.getValue().toString()));
			mapeoProduccion.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			mapeoProduccion.setTurno(this.cmbTurno.getValue().toString());
			mapeoProduccion.setFecha(this.txtFecha.getValue());
			
			pantallaFiltracionEnvasadora vt = new pantallaFiltracionEnvasadora("REGISTRO EQUIPO FILTRACION" , mapeoProduccion);
			getUI().addWindow(vt);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Has de guardar los datos del turno.");
		}
	}

	private void verPreoperativo()
	{

		if (this.idTurno==null)
		{
			this.guardar(true);
		}
		if (this.idTurno!=null)
		{
			mapeoProduccion = new MapeoProduccionTurnoEnvasadora();
			mapeoProduccion.setIdCodigo(this.idTurno);
			mapeoProduccion.setSemana(new Integer(this.cmbSemana.getValue().toString()));
			mapeoProduccion.setEjercicio(new Integer(this.txtEjercicio.getValue()));
			mapeoProduccion.setTurno(this.cmbTurno.getValue().toString());
			mapeoProduccion.setFecha(this.txtFecha.getValue());
			
			pantallaPreoperativoEnvasadora vt = new pantallaPreoperativoEnvasadora("REGISTRO PREOPERATVIO" , mapeoProduccion);
			getUI().addWindow(vt);
		}
		else
		{
			Notificaciones.getInstance().mensajeError("Has de guardar los datos del turno.");
		}
	}
	
	private void verParadas()
	{
    	MapeoTiemposEnvasadora mapeoParadas = new MapeoTiemposEnvasadora();
    	mapeoParadas.setFecha(this.txtFecha.getValue());
    	mapeoParadas.setEjercicio(new Integer(this.txtEjercicio.getValue()));
    	mapeoParadas.setSemana(new Integer(this.cmbSemana.getValue().toString()));
    	mapeoParadas.setTurno(this.cmbTurno.getValue().toString());

		pantallaTiemposEnvasadora vt = new pantallaTiemposEnvasadora("Paradas / Cambios" , mapeoParadas);
		getUI().addWindow(vt);

	}


	private void comprobarEstadoControlesCalidad()
	{
    	boolean hayPreoperativo = this.cpcs.comprobarPreoperativo(this.txtFecha.getValue());
    	
    	if (hayPreoperativo)
    	{
    		this.btnPrepoerativo.removeStyleName(ValoTheme.BUTTON_DANGER);
    		this.btnPrepoerativo.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	}
    	else
    	{
    		this.btnPrepoerativo.addStyleName(ValoTheme.BUTTON_DANGER);
			this.btnPrepoerativo.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    	}
    	boolean hayFiltracion = this.cpcs.comprobarFiltracion(this.txtFecha.getValue());
    	
    	if (hayFiltracion)
    	{
    		this.btnFiltracion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.btnFiltracion.removeStyleName(ValoTheme.BUTTON_DANGER);
    	}
    	else
    	{
    		this.btnFiltracion.addStyleName(ValoTheme.BUTTON_DANGER);
    		this.btnFiltracion.removeStyleName(ValoTheme.BUTTON_FRIENDLY);
    	}
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
	}

}