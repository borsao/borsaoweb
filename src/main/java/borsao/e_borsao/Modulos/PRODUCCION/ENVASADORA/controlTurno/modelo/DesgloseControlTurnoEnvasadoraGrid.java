package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaControlTurnoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view.pantallaControlTurnoEnvasadora;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class DesgloseControlTurnoEnvasadoraGrid extends GridPropio 
{
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	private boolean editable = false;
	private boolean conFiltro = false;
	
	private MapeoControlTurnoEnvasadora mapeo=null;
	private MapeoControlTurnoEnvasadora mapeoOrig=null;
	private pantallaControlTurnoEnvasadora app = null;
	
    public DesgloseControlTurnoEnvasadoraGrid(pantallaControlTurnoEnvasadora r_app , ArrayList<MapeoControlTurnoEnvasadora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("Desglose Turno " );

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoControlTurnoEnvasadora.class);
		this.setRecords(this.vector);
//		this.setFrozenColumnCount(1);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setEditorEnabled(true);
		this.setSeleccion(SelectionMode.SINGLE);
    }

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	try
    	{
	    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
	    	
	    	this.mapeoOrig = new MapeoControlTurnoEnvasadora();
			this.mapeo=((MapeoControlTurnoEnvasadora) getEditedItemId());
	    	this.mapeo.setMapeo(null);
			this.mapeoOrig.setIdCodigo(this.mapeo.getIdCodigo());
			this.mapeoOrig.setIdTurno(this.mapeo.getIdTurno());
			this.mapeoOrig.setOperario(this.mapeo.getOperario());
			this.mapeoOrig.setHoras(this.mapeo.getHoras());
			this.mapeoOrig.setFueraLina(this.mapeo.getFueraLina());
			this.mapeoOrig.setConceptoFueraLinea(this.mapeo.getConceptoFueraLinea());
			this.mapeoOrig.setAusencia(this.mapeo.getAusencia());
			this.mapeoOrig.setConceptoAusencia(this.mapeo.getConceptoAusencia());
	    	super.doEditItem();
    	}
    	catch (Exception ex)
    	{
    		
    	}
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	  refreshAllRows();
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("operario", "horas", "fueraLina","conceptoFueraLinea", "ausencia", "conceptoAusencia", "eliminar");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("operario").setHeaderCaption("Operario");
    	this.getColumn("operario").setWidthUndefined();
    	this.getColumn("operario").setWidth(250);
    	this.getColumn("horas").setHeaderCaption("Horas");
    	this.getColumn("horas").setWidth(120);
    	this.getColumn("fueraLina").setHeaderCaption("Fuera Linea");
    	this.getColumn("fueraLina").setWidth(120);
    	this.getColumn("ausencia").setHeaderCaption("Absentismo");
    	this.getColumn("ausencia").setWidth(120);
    	
    	this.getColumn("conceptoFueraLinea").setHeaderCaption("Motivo");
    	this.getColumn("conceptoFueraLinea").setWidthUndefined();
    	
    	this.getColumn("conceptoAusencia").setHeaderCaption("Motivo Abs.");
    	this.getColumn("conceptoAusencia").setWidthUndefined();
    	
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	this.getColumn("eliminar").setWidth(80);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("mapeo").setHidden(true);
    	this.getColumn("fecha").setHidden(true);
    	this.getColumn("turno").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoControlTurnoEnvasadora) {
                MapeoControlTurnoEnvasadora progRow = (MapeoControlTurnoEnvasadora)bean;
                // The actual description text is depending on the application
//                if ("numerador".equals(cell.getPropertyId()))
//                {
//                	switch (progRow.getEstado())
//                	{
//                		case "C":
//                		{
//                			descriptionText = "Cerrada";
//                			break;
//                		}
//                	}
//                	
//                }
//                else if ("programacion".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Programación";
//                }
//                else if ("euro".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "ControlTurno Perdidos";
//                }                
//                else if ("imagen".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Adjunar Imagenes";
//                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
 		   @Override
 		   protected String getTrueString() {
 		      return "";
 		   }
 		   @Override
 		   protected String getFalseString() {
 		      return "";
 		   }
 		});

    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "horas".equals(cellReference.getPropertyId()) || "fueraLina".equals(cellReference.getPropertyId()) || "ausencia".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    
    public void cargarListeners()
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	actualizar=true;
	        	
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	
	        	if (mapeo.getHoras().doubleValue()+mapeo.getFueraLina().doubleValue()+mapeo.getAusencia().doubleValue()<8)
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeError("Rellena bien las horas");	        		
	        	}
	        	if (mapeo.getAusencia().doubleValue()!=0 && (mapeo.getConceptoAusencia()==null || mapeo.getConceptoAusencia().length()==0))
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeError("Rellena bien el motivo de la ausencia");
	        	}
	        	if (mapeo.getFueraLina().doubleValue()!=0 && (mapeo.getConceptoFueraLinea()==null || mapeo.getConceptoFueraLinea().length()==0))
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeError("Rellena bien el motivo de estar fuera de línea");
	        	}
	        	if (mapeo.getHoras().doubleValue()!=0 && (mapeo.getOperario()==null|| mapeo.getOperario().length()==0))
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeError("Rellena el operario");
	        	}
	        	if (mapeo.getHoras()==null && mapeo.getOperario()!=null)
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeError("Rellena las horas de todos los operarios");
	        	}

	        	if (!actualizar)
	        	{
    				mapeo.setHoras(mapeoOrig.getHoras());
    				mapeo.setFueraLina(mapeoOrig.getFueraLina());
        			mapeo.setAusencia(mapeoOrig.getAusencia());
        			doCancelEditor();
	        	}
	        	else 
	        	{
	        		
	        		mapeo = (MapeoControlTurnoEnvasadora) getEditedItemId();
	        		
	        		consultaControlTurnoEnvasadoraServer cs = new consultaControlTurnoEnvasadoraServer(CurrentUser.get());
		        
        			if (mapeo.isEliminar())
        			{
        				cs.eliminar(mapeo);
        				((ArrayList<MapeoControlTurnoEnvasadora>) vector).remove(mapeo);
        				remove(mapeo);
		    			refreshAllRows();
        			}
        			else
        			{
        				mapeo.setIdCodigo(mapeoOrig.getIdCodigo());
        				mapeo.setIdTurno(mapeoOrig.getIdTurno());
	        			mapeo.setOperario(mapeoOrig.getOperario());

	        			cs.guardarCambios(mapeo);
		        		
		        		((ArrayList<MapeoControlTurnoEnvasadora>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoControlTurnoEnvasadora>) vector).add(mapeo);

		    			refreshAllRows();
		    			scrollTo(mapeo);
		    			select(mapeo);
        			}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoControlTurnoEnvasadora mapeo = (MapeoControlTurnoEnvasadora) event.getItemId();
            	
//            	if (event.getPropertyId().toString().equals("programacion"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//            	}
//            	else if (event.getPropertyId().toString().equals("imagen"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaImagenesNoConformidades vt = null;
//        	    		vt= new pantallaImagenesNoConformidades(mapeo, "Imagenes No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//
//            	}            	
//            	else if (event.getPropertyId().toString().equals("euro"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaValoracionNoConformidades vt = null;
//        	    		vt= new pantallaValoracionNoConformidades(mapeo.getIdqc_incidencias());
//        	    		getUI().addWindow(vt);
//            		}
//            	}            	
//            	else
//            	{
//            		activadaVentanaPeticion=false;
//            		ordenando = false;
//	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	public void generacionPdf(MapeoControlTurnoEnvasadora r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
	
}


