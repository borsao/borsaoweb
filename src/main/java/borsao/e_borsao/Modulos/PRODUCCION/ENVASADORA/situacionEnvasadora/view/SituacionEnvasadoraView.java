package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo.MapeoSituacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.situacionEnvasadora.server.consultaSituacionEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class SituacionEnvasadoraView extends GridViewRefresh {

	public static final String VIEW_NAME = "Situacion Envasadora";
	private final String titulo = "Situacion Envasadora";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = true;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private ComboBox cmbAlmacen = null;
	private EntradaDatosFecha fechaCarga = null;
	private HashMap<String,String> filtrosRec = null;
	public String almacenConsulta = "1";
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSituacionEnvasadora> r_vector=null;
//    	hashToMapeo hm = new hashToMapeo();
    	
//    	MapeoPedidosCompras=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	consultaSituacionEnvasadoraServer cse = new consultaSituacionEnvasadoraServer(CurrentUser.get());
    	r_vector=cse.datosOpcionesGlobal(almacenConsulta, RutinasFechas.convertirDateToString(this.fechaCarga.getValue()));	
    	
    	grid = new OpcionesGrid(this, r_vector, RutinasFechas.convertirDateToString(this.fechaCarga.getValue()));
    	
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		setHayGrid(true);
    		((OpcionesGrid) grid).establecerFiltros(filtrosRec);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public SituacionEnvasadoraView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
	    	this.cmbAlmacen= new ComboBox("Almacen de Consulta");    		
			this.cmbAlmacen.setNewItemsAllowed(false);
			this.cmbAlmacen.addStyleName(ValoTheme.COMBOBOX_TINY);
			this.cmbAlmacen.setNullSelectionAllowed(false);
			this.cmbAlmacen.addItem("Almacen Capuchinos");
			this.cmbAlmacen.addItem("Bodega Nueva");
			this.cmbAlmacen.addItem("Todos");
			this.cmbAlmacen.setValue("Almacen Capuchinos");
			
			this.fechaCarga = new EntradaDatosFecha("Fecha de Carga");
			this.fechaCarga.addStyleName(ValoTheme.DATEFIELD_TINY);
			this.fechaCarga.setValue(new Date());
			
    	this.cabLayout.addComponent(this.cmbAlmacen);
    	this.cabLayout.addComponent(this.fechaCarga);

    	this.cargarListeners();
    	this.generarGrid(null);
    }

    
    private void cargarListeners()
    {
		this.cmbAlmacen.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Almacen Capuchinos")) 
					{
						almacenConsulta = "1";
					}
					else if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Bodega Nueva")) 
					{
						almacenConsulta = "4";
					}
					else 
					{
						almacenConsulta = "";
					}
					
					generarGrid(null);
				}
			}
		}); 
		this.fechaCarga.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					
					if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Almacen Capuchinos")) 
					{
						almacenConsulta = "1";
					}
					else if (cmbAlmacen.getValue()!=null && cmbAlmacen.getValue().toString().equals("Bodega Nueva")) 
					{
						almacenConsulta = "4";
					}
					else 
					{
						almacenConsulta = "";
					}
					
					generarGrid(null);
				}
				
			}
		});
    }
    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    @Override
    public void reestablecerPantalla() {
    }

    public void print() {
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
