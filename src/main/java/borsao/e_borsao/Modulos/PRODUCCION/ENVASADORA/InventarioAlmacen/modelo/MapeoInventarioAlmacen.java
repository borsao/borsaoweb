package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.InventarioAlmacen.modelo;

import java.sql.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoInventarioAlmacen extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	private Integer codigoPedido;
	private Date fechaDocumento;
	private String referencia;
	private String cumplimentado;
	
	private Double tinto5;
	private Double rosa5;
	private Double tinto72;
	private Double rosa72;
	private Double media5;
	
	private Double tinto2;
	private Double rosa2;
	private Double tinto60;
	private Double rosa60;
	private Double media2;
	
	private Double oro;
	private Double mediaOro;
	private Double mallacan;
	private Double tresla;
	private Double mitica;
	private Double mediaMitica;
	
	private Double campellas;
	private Double bole;
	private Double borsao;
	
	private Integer kilos;
	private String cliente;
	private Integer idCliente;
	
	private String idPallet;
	private String numeroAlmacen;
	private String nombreAlmacen;
	
	
	public MapeoInventarioAlmacen()
	{
		this.setIdPallet("");
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}

	public Integer getSemana() {
		return semana;
	}

	public void setSemana(Integer semana) {
		this.semana = semana;
	}

	public Integer getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(Integer codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public Date getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getCumplimentado() {
		return cumplimentado;
	}

	public void setCumplimentado(String cumplimentado) {
		this.cumplimentado = cumplimentado;
	}

	public Double getTinto5() {
		return tinto5;
	}

	public void setTinto5(Double tinto5) {
		this.tinto5 = tinto5;
	}

	public Double getRosa5() {
		return rosa5;
	}

	public void setRosa5(Double rosa5) {
		this.rosa5 = rosa5;
	}

	public Double getTinto72() {
		return tinto72;
	}

	public Double getTresla() {
		return tresla;
	}

	public void setTresla(Double tresla) {
		this.tresla = tresla;
	}

	public void setTinto72(Double tinto72) {
		this.tinto72 = tinto72;
	}

	public Double getRosa72() {
		return rosa72;
	}

	public void setRosa72(Double rosa72) {
		this.rosa72 = rosa72;
	}

	public Double getMedia5() {
		return media5;
	}

	public void setMedia5(Double media5) {
		this.media5 = media5;
	}

	public Double getTinto2() {
		return tinto2;
	}

	public void setTinto2(Double tinto2) {
		this.tinto2 = tinto2;
	}

	public Double getRosa2() {
		return rosa2;
	}

	public void setRosa2(Double rosa2) {
		this.rosa2 = rosa2;
	}

	public Double getTinto60() {
		return tinto60;
	}

	public void setTinto60(Double tinto60) {
		this.tinto60 = tinto60;
	}

	public Double getRosa60() {
		return rosa60;
	}

	public void setRosa60(Double rosa60) {
		this.rosa60 = rosa60;
	}

	public Double getMedia2() {
		return media2;
	}

	public void setMedia2(Double media2) {
		this.media2 = media2;
	}

	public Double getOro() {
		return oro;
	}

	public void setOro(Double oro) {
		this.oro = oro;
	}

	public Double getMediaOro() {
		return mediaOro;
	}

	public void setMediaOro(Double mediaOro) {
		this.mediaOro = mediaOro;
	}

	public Double getMitica() {
		return mitica;
	}

	public void setMitica(Double mitica) {
		this.mitica = mitica;
	}

	public Double getMediaMitica() {
		return mediaMitica;
	}

	public void setMediaMitica(Double mediaMitica) {
		this.mediaMitica = mediaMitica;
	}

	public Integer getKilos() {
		return kilos;
	}

	public void setKilos(Integer kilos) {
		this.kilos = kilos;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Double getCampellas() {
		return campellas;
	}

	public void setCampellas(Double campellas) {
		this.campellas = campellas;
	}

	public Double getBole() {
		return bole;
	}

	public void setBole(Double bole) {
		this.bole = bole;
	}

	public Double getBorsao() {
		return borsao;
	}

	public void setBorsao(Double borsao) {
		this.borsao = borsao;
	}

	public Double getMallacan() {
		return mallacan;
	}

	public void setMallacan(Double mallacan) {
		this.mallacan = mallacan;
	}

	public String getIdPallet() {
		return idPallet;
	}

	public void setIdPallet(String idPallet) {
		this.idPallet = idPallet;
	}

	public String getNumeroAlmacen() {
		return numeroAlmacen;
	}

	public void setNumeroAlmacen(String numeroAlmacen) {
		this.numeroAlmacen = numeroAlmacen;
	}

	public String getNombreAlmacen() {
		return nombreAlmacen;
	}

	public void setNombreAlmacen(String nombreAlmacen) {
		this.nombreAlmacen = nombreAlmacen;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	
}