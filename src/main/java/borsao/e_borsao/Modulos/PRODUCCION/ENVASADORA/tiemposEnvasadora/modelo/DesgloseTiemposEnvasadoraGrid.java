package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextArea;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view.ControlTurnoEnvasadoraView;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.server.consultaTiemposEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.view.pantallaTiemposEnvasadora;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class DesgloseTiemposEnvasadoraGrid extends GridPropio 
{
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;

	private boolean editable = false;
	private boolean conFiltro = true;
	
	private MapeoTiemposEnvasadora mapeo=null;
	private MapeoTiemposEnvasadora mapeoOrig=null;
	private pantallaTiemposEnvasadora app = null;
	
    public DesgloseTiemposEnvasadoraGrid(pantallaTiemposEnvasadora r_app , ArrayList<MapeoTiemposEnvasadora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
        if (r_app!= null && r_app.mapeoEnvasadora != null && r_app.mapeoEnvasadora.getDescripcion()!=null)
        	this.asignarTitulo("Desglose Tiempos paradas " + r_app.mapeoEnvasadora.getDescripcion());
        else
        	this.asignarTitulo("Desglose Tiempos paradas ");
        
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public DesgloseTiemposEnvasadoraGrid(ControlTurnoEnvasadoraView r_app , ArrayList<MapeoTiemposEnvasadora> r_vector) 
    {
        this.vector=r_vector;
    	this.asignarTitulo("Incidencias / Paradas Turno");
        
		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoTiemposEnvasadora.class);
		this.setRecords(this.vector);
//		this.setFrozenColumnCount(1);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setEditorEnabled(true);
		this.setSeleccion(SelectionMode.SINGLE);
    }

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	
    	this.mapeoOrig = new MapeoTiemposEnvasadora();
		this.mapeo=((MapeoTiemposEnvasadora) getEditedItemId());
    	
		this.mapeoOrig.setIdCodigo(this.mapeo.getIdCodigo());
		this.mapeoOrig.setTurno(this.mapeo.getTurno());
		this.mapeoOrig.setEjercicio(this.mapeo.getEjercicio());
		this.mapeoOrig.setSemana(this.mapeo.getSemana());
		this.mapeoOrig.setIdProgramacion(this.mapeo.getIdProgramacion());
		this.mapeoOrig.setTipo(this.mapeo.getTipo());
		this.mapeoOrig.setDescripcion(this.mapeo.getDescripcion());
		this.mapeoOrig.setTiempo(this.mapeo.getTiempo());
		this.mapeoOrig.setObservaciones(this.mapeo.getObservaciones());
		this.mapeoOrig.setFecha(this.mapeo.getFecha());
    	
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("turno", "tipo", "descripcion","tiempo", "observaciones", "eliminar");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("turno", "40");
    	this.widthFiltros.put("tipo", "85");
    	this.widthFiltros.put("descripcion", "185");
    	
    	this.getColumn("turno").setHeaderCaption("Turno");
    	this.getColumn("turno").setWidthUndefined();
    	this.getColumn("turno").setWidth(80);
    	this.getColumn("tipo").setHeaderCaption("Tipo");
    	this.getColumn("tipo").setWidthUndefined();
    	this.getColumn("tipo").setWidth(175);
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(220);
    	this.getColumn("tiempo").setHeaderCaption("Tiempo ");
    	this.getColumn("tiempo").setWidth(120);
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("observaciones").setWidthUndefined();
    	this.getColumn("observaciones").setEditorField(getTextArea());
    	
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	this.getColumn("eliminar").setWidth(120);

    	this.getColumn("idProgramacion").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("fecha").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);
    	this.getColumn("semana").setHidden(true);
    }
    
    private void asignarTooltips()
    {
    	
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoTiemposEnvasadora) {
                MapeoTiemposEnvasadora progRow = (MapeoTiemposEnvasadora)bean;
                // The actual description text is depending on the application
//                if ("numerador".equals(cell.getPropertyId()))
//                {
//                	switch (progRow.getEstado())
//                	{
//                		case "C":
//                		{
//                			descriptionText = "Cerrada";
//                			break;
//                		}
//                	}
//                	
//                }
//                else if ("programacion".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Acceso a Programación";
//                }
//                else if ("euro".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Tiempos Perdidos";
//                }                
//                else if ("imagen".equals(cell.getPropertyId()))
//                {
//                	descriptionText = "Adjunar Imagenes";
//                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
 		   @Override
 		   protected String getTrueString() {
 		      return "";
 		   }
 		   @Override
 		   protected String getFalseString() {
 		      return "";
 		   }
 		});

    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "tiempo".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    
    private Field<?> getTextArea() {
		TextArea textArea = new TextArea();	
		textArea.setWidth("400px");
		textArea.setStyleName("areablanca");
		
		return textArea;
	}
    
    public void cargarListeners()
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	actualizar=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		mapeo = (MapeoTiemposEnvasadora) getEditedItemId();
	        		
	        		consultaTiemposEnvasadoraServer cs = new consultaTiemposEnvasadoraServer(CurrentUser.get());
		        
	        		if (mapeo.getIdProgramacion()!=null)
		        	{
	        			if (mapeo.isEliminar())
	        			{
	        				cs.eliminar(mapeo);
	        				((ArrayList<MapeoTiemposEnvasadora>) vector).remove(mapeo);
	        				remove(mapeo);
			    			refreshAllRows();
	        			}
	        			else
	        			{
	        				mapeo.setEjercicio(mapeoOrig.getEjercicio());
	        				mapeo.setSemana(mapeoOrig.getSemana());
		        			mapeo.setTipo(mapeoOrig.getTipo());
		        			mapeo.setTurno(mapeoOrig.getTurno());
	        				mapeo.setDescripcion(mapeoOrig.getDescripcion());
	        				mapeo.setFecha(mapeoOrig.getFecha());
	        				mapeo.setIdProgramacion(mapeoOrig.getIdProgramacion());
	        				mapeo.setIdCodigo(mapeoOrig.getIdCodigo());
			        		cs.guardarCambios(mapeo);
			        		
			        		((ArrayList<MapeoTiemposEnvasadora>) vector).remove(mapeoOrig);
			    			((ArrayList<MapeoTiemposEnvasadora>) vector).add(mapeo);
	
			    			refreshAllRows();
			    			scrollTo(mapeo);
			    			select(mapeo);
	        			}
		        	}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoTiemposEnvasadora mapeo = (MapeoTiemposEnvasadora) event.getItemId();
            	
//            	if (event.getPropertyId().toString().equals("programacion"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
//            		{
//            			pantallaLineasProgramaciones vt = null;
//        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//            	}
//            	else if (event.getPropertyId().toString().equals("imagen"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaImagenesNoConformidades vt = null;
//        	    		vt= new pantallaImagenesNoConformidades(mapeo, "Imagenes No Conformidad");
//        	    		getUI().addWindow(vt);
//            		}
//
//            	}            	
//            	else if (event.getPropertyId().toString().equals("euro"))
//            	{
//            		activadaVentanaPeticion=true;
//            		ordenando = false;
//            		
//            		if (mapeo.getIdqc_incidencias()!=null && mapeo.getIdqc_incidencias()!=0)
//            		{
//            			pantallaValoracionNoConformidades vt = null;
//        	    		vt= new pantallaValoracionNoConformidades(mapeo.getIdqc_incidencias());
//        	    		getUI().addWindow(vt);
//            		}
//            	}            	
//            	else
//            	{
//            		activadaVentanaPeticion=false;
//            		ordenando = false;
//	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("tiempo");
		this.camposNoFiltrar.add("eliminar");
		this.camposNoFiltrar.add("observaciones");
	}

	public void generacionPdf(MapeoTiemposEnvasadora r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
}


