package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Grid.SingleSelectionModel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.ClienteMail;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.PeticionNoConformidades;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.modelo.MapeoNotasArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.server.consultaNotasArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.NotasArticulos.view.pantallaNotasArticulo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.ProgramacionEnvasadoraGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view.pantallaControlTurnoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.view.pantallaTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view.pantallaSituacionEmbotellado;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ProgramacionEnvasadoraView extends GridViewRefresh {

	public static final String VIEW_NAME = "Programacion Envasadora";
	public consultaProgramacionEnvasadoraServer cus =null;
	private consultaEscandalloServer ces = null;
	
	public ComboBox cmbSemana = null;
	public TextField txtEjercicio= null;
	
	private final String titulo = "PROGRAMACION LINEA ENVASADORA";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private Integer permisos = 0;	
	private String semana ="";
	
	public TextField txtUnidades= null;
	public TextField txtLitros= null;

	private OpcionesForm form = null;
	private Button opcRechazo = null;
	private Button opcCompletar = null;
	private Button opcBajar = null;
	private Button opcTurno = null;
	private Button opcParadas = null;
	private Button opcSubir = null;
	private Button opcImprimir = null;
	private Button opcMail = null;
	private Button opcSumar = null;
	private Button opcMateriales = null;
	private Button opcTotal = null;    
	
	private Button opcMenos= null;
	private Button opcMas= null;
	
	
	/*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ProgramacionEnvasadoraView() 
    {
    	opcionesEscogidas = new HashMap<String , String>();
    }

    public void cargarPantalla() 
    {
    	this.ces = new consultaEscandalloServer(CurrentUser.get());
		this.cus= consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	this.permisos = new Integer(this.cus.comprobarAccesos());
    	
    	if (this.permisos==0)
    	{
    		Notificaciones.getInstance().mensajeError("No tienes acceso a este programa");
    		this.destructor();
    	}
    	else
    	{
    		
    		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    		
    		this.opcTurno= new Button();    	
    		this.opcTurno.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcTurno.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcTurno.setIcon(FontAwesome.USERS);
    		this.opcTurno.setDescription("Control Turno");

    		this.opcParadas= new Button();    	
    		this.opcParadas.addStyleName(ValoTheme.BUTTON_PRIMARY);
    		this.opcParadas.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcParadas.setIcon(FontAwesome.WARNING);
    		this.opcParadas.setDescription("Paradas / Cambios / Incidencias");
    		
    		this.opcSubir= new Button();    	
    		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_UP);
    		
    		this.opcBajar= new Button();    	
    		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_DOWN);

    		this.opcMas= new Button();    	
    		this.opcMas.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMas.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMas.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
    		
    		this.opcMenos= new Button();    	
    		this.opcMenos.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMenos.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMenos.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);

    		this.opcImprimir= new Button();    	
    		this.opcImprimir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcImprimir.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcImprimir.setIcon(FontAwesome.PRINT);
    		
    		this.opcSumar= new Button();    	
    		this.opcSumar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcSumar.setIcon(FontAwesome.PLUS_SQUARE);
    		this.opcSumar.setDescription("Seleccion Lineas");

    		this.opcMateriales= new Button();    	
    		this.opcMateriales.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMateriales.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMateriales.setIcon(FontAwesome.CUBES);
    		this.opcMateriales.setVisible(false);
    		this.opcMateriales.setDescription("Explosión Materiales");

    		this.opcTotal= new Button();    	
    		this.opcTotal.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcTotal.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcTotal.setIcon(FontAwesome.CALCULATOR);
    		this.opcTotal.setVisible(false);
    		this.opcTotal.setDescription("Caluclo Litros/botellas");

    		this.opcMail= new Button();    	
    		this.opcMail.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcMail.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcMail.setIcon(FontAwesome.ENVELOPE_O);

    		this.opcRechazo= new Button();    	
    		this.opcRechazo.addStyleName(ValoTheme.BUTTON_DANGER);
    		this.opcRechazo.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcRechazo.setIcon(FontAwesome.THUMBS_DOWN);
    		this.opcRechazo.setDescription("Indicar No Conformidad");

    		this.opcCompletar= new Button("Completar");    	
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcCompletar.setIcon(FontAwesome.CHECK_CIRCLE);
    		
    		this.txtEjercicio=new TextField("Ejercicio");
    		this.txtEjercicio.setEnabled(false);
    		this.txtEjercicio.setWidth("100px");
    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
    		
    		this.txtUnidades=new TextField("Unidades");
    		this.txtUnidades.setVisible(false);
    		this.txtUnidades.setWidth("150px");
    		this.txtUnidades.addStyleName(ValoTheme.TEXTFIELD_TINY);
    		this.txtUnidades.addStyleName("rightAligned");
    		this.txtLitros=new TextField("Litros");
    		this.txtLitros.setVisible(false);
    		this.txtLitros.setWidth("150px");
    		this.txtLitros.addStyleName("rightAligned");
    		this.txtLitros.addStyleName(ValoTheme.TEXTFIELD_TINY);

    		this.cmbSemana= new ComboBox("Semana");    		
    		this.cmbSemana.setNewItemsAllowed(false);
    		this.cmbSemana.setNullSelectionAllowed(false);
    		this.cmbSemana.setWidth("100px");
    		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
    		
//    		this.cabLayout.addComponent(this.opcTurno);
//    		this.cabLayout.setComponentAlignment(this.opcTurno,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcSubir);
    		this.cabLayout.setComponentAlignment(this.opcSubir,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcBajar);
    		this.cabLayout.setComponentAlignment(this.opcBajar,Alignment.BOTTOM_LEFT);
//    		this.cabLayout.addComponent(this.opcParadas);
//    		this.cabLayout.setComponentAlignment(this.opcParadas,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcRechazo);
    		this.cabLayout.setComponentAlignment(this.opcRechazo,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcImprimir);
    		this.cabLayout.setComponentAlignment(this.opcImprimir,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcMail);
    		this.cabLayout.setComponentAlignment(this.opcMail,Alignment.BOTTOM_LEFT);    		
    		this.cabLayout.addComponent(this.txtEjercicio);

    		this.cabLayout.addComponent(this.opcMenos);
    		this.cabLayout.setComponentAlignment(this.opcMenos,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.cmbSemana);
    		this.cabLayout.addComponent(this.opcMas);
    		this.cabLayout.setComponentAlignment(this.opcMas,Alignment.BOTTOM_LEFT);

    		this.cabLayout.addComponent(this.opcCompletar);
    		this.cabLayout.setComponentAlignment(this.opcCompletar,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcSumar);
    		this.cabLayout.setComponentAlignment(this.opcSumar,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.opcMateriales);
    		this.cabLayout.setComponentAlignment(this.opcMateriales,Alignment.BOTTOM_LEFT);
    		
    		this.cabLayout.addComponent(this.opcTotal);
    		this.cabLayout.setComponentAlignment(this.opcTotal,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.txtUnidades);
    		this.cabLayout.setComponentAlignment(this.txtUnidades,Alignment.BOTTOM_LEFT);
    		this.cabLayout.addComponent(this.txtLitros);
    		this.cabLayout.setComponentAlignment(this.txtLitros,Alignment.BOTTOM_LEFT);

    		this.cargarCombo(null);
    		this.cargarListeners();
    		this.establecerModo();
    		this.cmbSemana.setValue(this.semana);
    		
    		opcionesEscogidas.put("ejercicio", this.txtEjercicio.getValue());
    		opcionesEscogidas.put("semana", this.semana);
    		this.generarGrid(opcionesEscogidas);
    	}
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoProgramacionEnvasadora> r_vector=null;
    	MapeoProgramacionEnvasadora mapeoProgramacion =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoProgramacion=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	r_vector=this.cus.datosProgramacionGlobal(mapeoProgramacion);
    	this.presentarGrid(r_vector);
    }
    
    public void newForm()
    {    	
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}

    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (permisos==99 && isHayGrid())
    	{
    		this.opcBajar.setEnabled(true);
    		this.opcBajar.setVisible(true);
    		this.opcSubir.setEnabled(true);
    		this.opcSubir.setVisible(true);    		
    	}
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((ProgramacionEnvasadoraGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((ProgramacionEnvasadoraGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((ProgramacionEnvasadoraGrid) this.grid).activadaVentanaPeticion && !((ProgramacionEnvasadoraGrid) this.grid).ordenando)
    	{
	    	switch (this.permisos)
	    	{
		    	case 0:
		    		break;
		    	case 10:
		    		break;
		    	case 20:
		    		break;
		    	case 30:
		    		break;
		    	case 40:
		    		this.establecerBotonAccion(r_fila);
		    		break;
		    	case 50:
		    		break;
		    	case 70:
		    		break;
		    	case 80:
		    		break;
		    	case 99:
		    		this.establecerBotonAccion(r_fila);
		    		this.verForm(false);
			    	this.form.editarProgramacion((MapeoProgramacionEnvasadora) r_fila);
		    		break;
	    	}
    	}    		
    }
    
    public void generarGrid(MapeoProgramacionEnvasadora r_mapeo)
    {
    	ArrayList<MapeoProgramacionEnvasadora> r_vector=null;
    	r_vector=this.cus.datosProgramacionGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    }
    
    private void presentarGrid(ArrayList<MapeoProgramacionEnvasadora> r_vector)
    {
    	StringBuffer cadenaArticulos = new StringBuffer();
    	MapeoProgramacionEnvasadora mapeoProgramacion = null;

    	if (this.permisos==99 && r_vector.size()>0)
    	{
	    	cadenaArticulos.append("('");
	    	
			for (int i=0; i< r_vector.size();i++)
			{
				mapeoProgramacion = r_vector.get(i);
				
				if (mapeoProgramacion.getArticulo()!=null && mapeoProgramacion.getArticulo().length()>0)
				{
					cadenaArticulos.append(mapeoProgramacion.getArticulo());
					cadenaArticulos.append("','");
				}
			}
	    	cadenaArticulos.append("')");
	
	    	consultaNotasArticulosServer cns = consultaNotasArticulosServer.getInstance(CurrentUser.get());
	    	ArrayList<MapeoNotasArticulos> vector = cns.verNotas(cadenaArticulos);
	    	
			if (vector.size()>0)
			{
				pantallaNotasArticulo vt = new pantallaNotasArticulo("Notas de Articulos ", vector);
				getUI().addWindow(vt);
			}
    	}
    	
    	grid = new ProgramacionEnvasadoraGrid(this.permisos,r_vector);
    	if (((ProgramacionEnvasadoraGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((ProgramacionEnvasadoraGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
    	
    	
    	this.opcMas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
				
//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;
//					setHayGrid(false);
//				}
//				MapeoProgramacionEnvasadora mapeo = new MapeoProgramacionEnvasadora();
//				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
//				mapeo.setSemana(cmbSemana.getValue().toString());
//				if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
//				}
//				
//				generarGrid(mapeo);


			}
		});

    	this.opcMenos.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));

//				if (isHayGrid())
//				{			
//					grid.removeAllColumns();			
//					barAndGridLayout.removeComponent(grid);
//					grid=null;
//					setHayGrid(false);
//				}
//				
//				MapeoProgramacionEnvasadora mapeo = new MapeoProgramacionEnvasadora();
//				mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
//				mapeo.setSemana(cmbSemana.getValue().toString());
//				if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
//				{
//					opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
//				}
//				
//				generarGrid(mapeo);

			}
		});

		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoProgramacionEnvasadora mapeo = new MapeoProgramacionEnvasadora();
					mapeo.setEjercicio(new Integer(txtEjercicio.getValue()));
					mapeo.setSemana(cmbSemana.getValue().toString());
					if (cmbSemana.getValue()!=null && cmbSemana.getValue().toString().length()>0) 
					{
						opcionesEscogidas.put("semana", cmbSemana.getValue().toString());
					}
					
					generarGrid(mapeo);
				}
			}
		}); 

    	this.opcSumar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				
				if (grid.getSelectionModel() instanceof SingleSelectionModel)
				{
					opcSumar.addStyleName(ValoTheme.BUTTON_DANGER);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(false);
					txtUnidades.setVisible(true);
					txtUnidades.setValue("");
					txtLitros.setVisible(true);
					txtLitros.setValue("");
					botonesGenerales(false);
					activarBotones(false);
					opcSumar.setEnabled(true);					
					grid.setSelectionMode(SelectionMode.MULTI);
					opcTotal.setVisible(true);					
					opcMateriales.setVisible(true);
				}
				else
				{					
					opcSumar.setStyleName(ValoTheme.BUTTON_FRIENDLY);
					opcSumar.addStyleName(ValoTheme.BUTTON_TINY);
					opcRefresh.setEnabled(true);
					txtUnidades.setVisible(false);
					txtLitros.setVisible(false);
					botonesGenerales(true);
					activarBotones(true);
					opcSumar.setEnabled(true);
					opcTotal.setVisible(false);
					opcMateriales.setVisible(false);
					grid.deselectAll();
					grid.setSelectionMode(SelectionMode.SINGLE);
				}
			}
		});

    	this.opcCompletar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacionEnvasadora mapeo=(MapeoProgramacionEnvasadora) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					if (opcCompletar.getCaption().equals("Completar"))
					{
	 					mapeo.setEstado("T");
					}
					else if (opcCompletar.getCaption().equals("Reabrir"))
					{
						mapeo.setEstado("A");
					}
					else if (opcCompletar.getCaption().equals("Aceptar"))
					{
						mapeo.setEstado("A");
					}
					
					cus.guardarCambios(mapeo);
					if (isHayGrid())
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;
						generarGrid(opcionesEscogidas);
					}
				}				
			}
		});

    	this.opcRechazo.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacionEnvasadora mapeo=(MapeoProgramacionEnvasadora) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					rechazoMateriales(mapeo);
				}				
			}
		});
    	
    	this.opcParadas.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoProgramacionEnvasadora mapeo=(MapeoProgramacionEnvasadora) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
					paradas(mapeo);
				}				
			}
		});
    	
    	this.opcTurno.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
					turno();
			}
		});
    	
    	this.opcSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("subir");
			}
		});

    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				ordenarFilas("bajar");
			}
		});

    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				imprimirProgramacion(false);
			}
		});
    	this.opcMail.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				imprimirProgramacion(true);
			}
		});
    	this.opcTotal.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				mostrarFilas(grid.getSelectedRows());
			}
		});
    	this.opcMateriales.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) 
			{
				mostrarMateriales(grid.getSelectedRows());
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
		this.opcImprimir.setVisible(true);
		this.opcImprimir.setEnabled(true);
		
		switch (this.permisos)
		{
			case 10:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				break;
			case 20:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(true);
				this.activarBotones(false);
				break;
			case 30:
				this.setSoloConsulta(true);
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 40:
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				this.opcRechazo.setEnabled(true);
				this.opcRechazo.setVisible(true);				
				this.opcCompletar.setEnabled(true);
				this.opcCompletar.setVisible(true);
				this.opcTurno.setEnabled(true);
				this.opcTurno.setVisible(true);
				this.opcParadas.setEnabled(true);
				this.opcParadas.setVisible(true);			
				break;
			case 50:
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				this.opcTurno.setEnabled(true);
				this.opcTurno.setVisible(true);
				this.opcParadas.setEnabled(true);
				this.opcParadas.setVisible(true);			
				break;
			case 70:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 80:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				this.opcRechazo.setEnabled(true);
				this.opcRechazo.setVisible(true);				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				this.opcTurno.setEnabled(true);
				this.opcTurno.setVisible(true);
				this.opcParadas.setEnabled(true);
				this.opcParadas.setVisible(true);							
				break;				
			case 99:
				this.verBotones(true);
				this.activarBotones(true);
				this.navegacion(true);
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				setSoloConsulta(this.soloConsulta);
			
		}
	}

	private void navegacion(boolean r_navegar)
	{
		this.cmbSemana.setVisible(r_navegar);
		this.txtEjercicio.setVisible(r_navegar);
		this.cmbSemana.setEnabled(r_navegar);
		this.txtEjercicio.setEnabled(r_navegar);	
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCompletar.setEnabled(r_activo);
		this.opcSubir.setEnabled(r_activo);
		this.opcBajar.setEnabled(r_activo);
		this.opcRechazo.setEnabled(r_activo);
		
		this.opcTurno.setEnabled(r_activo);
		this.opcParadas.setEnabled(r_activo);

//		this.opcImprimir.setEnabled(r_activo);
		this.opcMail.setEnabled(r_activo);
		this.opcSumar.setEnabled(r_activo);
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCompletar.setVisible(r_visibles);
		this.opcSubir.setVisible(r_visibles);
		this.opcBajar.setVisible(r_visibles);
		this.opcRechazo.setVisible(r_visibles);
		
		this.opcTurno.setEnabled(r_visibles);
		this.opcParadas.setEnabled(r_visibles);

		this.opcImprimir.setVisible(r_visibles);
		this.opcMail.setVisible(r_visibles);
		this.opcSumar.setVisible(r_visibles);
	}
	
	private void establecerBotonAccion(Object r_fila)
	{
		this.opcCompletar.setVisible(true);

    	if (r_fila!=null && ((MapeoProgramacionEnvasadora) r_fila).getEstado()!=null)
    	{
    		this.opcCompletar.setEnabled(true);
	    	if (((MapeoProgramacionEnvasadora) r_fila).getEstado().equals("A"))
	    	{
	    		this.opcCompletar.setCaption("Completar");    		
	    	}
	    	else if (((MapeoProgramacionEnvasadora) r_fila).getEstado().equals("T"))
	    	{
	    		this.opcCompletar.setCaption("Reabrir");    		
	    	}
	    	else 
	    	{
	    		if (this.permisos==99)
	    		{
	    			this.opcCompletar.setCaption("Aceptar");
	    		}
	    		else
	    		{
	    			this.opcCompletar.setCaption("");
	    			this.opcCompletar.setEnabled(false);
	    		}
	    	}
    	}
	}

	private int obtenerIndice(MapeoProgramacionEnvasadora r_mapeo)
	{
		
		for (int i = 0; i<((ProgramacionEnvasadoraGrid) grid).getContainerDataSource().getItemIds().size();i++)
		{
			MapeoProgramacionEnvasadora mapeoLeido = (MapeoProgramacionEnvasadora) ((ProgramacionEnvasadoraGrid) grid).getContainer().getIdByIndex(i);
			if (mapeoLeido.equals(r_mapeo))
			{
				return i;
			}
		}

		return -1;
	}

	private void ordenarFilas(String r_direccion)
	{
		int filaOrigen = -1;
		int filaDestino = -1;
		
		if (form!=null) form.cerrar();
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = true;
		
		if (grid.getSelectedRow()!=null)
		{
			MapeoProgramacionEnvasadora mapeoOrigen=(MapeoProgramacionEnvasadora) grid.getSelectedRow();
			
			filaOrigen = obtenerIndice(mapeoOrigen);
			
			if (r_direccion.equals("subir")) 
			{
				filaDestino = filaOrigen - 1;
			}
			else
			{
				filaDestino = filaOrigen + 1;
			}
			
    		try
    		{
    			
        		MapeoProgramacionEnvasadora mapeoDestino = (MapeoProgramacionEnvasadora)  (MapeoProgramacionEnvasadora) ((ProgramacionEnvasadoraGrid) grid).getContainer().getIdByIndex(filaDestino);
        		
        		Integer origen = mapeoOrigen.getOrden();
        		Integer destino = mapeoDestino.getOrden();
        		
        		mapeoOrigen.setOrden(destino);
        		mapeoDestino.setOrden(origen);
        		
        		((ProgramacionEnvasadoraGrid) grid).guardarCambios(mapeoOrigen, mapeoDestino);
        		((ProgramacionEnvasadoraGrid) grid).select(grid.getContainerDataSource().getIdByIndex(filaDestino));
        		((ProgramacionEnvasadoraGrid) grid).scrollTo(mapeoOrigen);

        		
    		}
			catch (IndexOutOfBoundsException ex)
			{
				
			}
		}
		enviarMail(true);
	}
	
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}

	private void imprimirProgramacion(boolean r_mail)
	{
		String pdfGenerado = null;
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = false;

		if (form!=null) form.cerrar();
		
		if (grid!=null)
		{
			if (this.txtEjercicio.getValue()!=null && this.cmbSemana.getValue()!=null)
			{
		    	consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		    	pdfGenerado = cps.imprimirProgramacion(this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
		    	
		    	if(pdfGenerado.length()>0)
		    	{
		    		if (r_mail)
		    		{
			    		HashMap adjuntos = new HashMap();
						
						adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
						adjuntos.put("nombre", "programacion semana - " + this.cmbSemana.getValue().toString() + ".pdf");
	
						ArrayList<String> destinatariosProgramacion = new ArrayList<String>();
						
						//produccion
						destinatariosProgramacion.add("j.gracia@bodegasborsao.com");
						destinatariosProgramacion.add("envasadora@bodegasborsao.com");
						//calidad
						destinatariosProgramacion.add("c.gracia@bodegasborsao.com");
						destinatariosProgramacion.add("f.sebastian@bodegasborsao.com");
						//compras
						destinatariosProgramacion.add("compras@bodegasborsao.com");
						//almacen
						destinatariosProgramacion.add("almacen@bodegasborsao.com");
						destinatariosProgramacion.add("almacap@bodegasborsao.com");						
						
						ClienteMail.getInstance().envioMail("j.claveria@bodegasborsao.com",destinatariosProgramacion,"Programacion ENVASADORA S-" + this.cmbSemana.getValue().toString(), "Adjunto envío programacion semanal", adjuntos, false);
		    		}
		    		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		    	}	
		    	else
		    		Notificaciones.getInstance().mensajeError("Error en la generacion");
			}
			else
				Notificaciones.getInstance().mensajeInformativo("No hay datos a imrpimir.");
		}
		else
			Notificaciones.getInstance().mensajeInformativo("No hay datos a imrpimir.");
	}
	
    public void print() {
    	
    }

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		switch (r_accion)
		{
			case "mail":
			{
		    	consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		    	String pdfGenerado = cps.imprimirProgramacion(this.txtEjercicio.getValue(),this.cmbSemana.getValue().toString());
		    	
		    	if(pdfGenerado.length()>0)
		    	{
		    		HashMap adjuntos = new HashMap();
					
					adjuntos.put("archivo", LecturaProperties.basePdfPath + "/" + pdfGenerado);
					adjuntos.put("nombre", "programacion semana - " + this.cmbSemana.getValue().toString() + ".pdf");

					ArrayList<String> destinatariosProgramacion = new ArrayList<String>();
//					//produccion
//					destinatariosProgramacion.add("j.gracia@bodegasborsao.com");
//					destinatariosProgramacion.add("envasadora@bodegasborsao.com");
//					//calidad
//					destinatariosProgramacion.add("c.gracia@bodegasborsao.com");
//					destinatariosProgramacion.add("f.sebastian@bodegasborsao.com");
//					//compras
//					destinatariosProgramacion.add("compras@bodegasborsao.com");
//					//almacen
//					destinatariosProgramacion.add("almacen@bodegasborsao.com");
//					destinatariosProgramacion.add("almacap@bodegasborsao.com");						
					
					ClienteMail.getInstance().envioMailPrueba("j.claveria@bodegasborsao.com",destinatariosProgramacion,"Programacion ENVASADORA S-" + this.cmbSemana.getValue().toString(), "Adjunto envío programacion semanal por " + CurrentUser.get(), null, false);
				}
				break;
			}
			default:
			{
				if (this.form!=null)
				{
					form.removeStyleName("visible");
					form.cerrar();
				}
				break;
			}
				
		}

	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	@Override
	public void mostrarFilas(Collection<Object> r_filas) 
	{
		Iterator it = null;
		Double unidades = new Double(0);
		Double litros = new Double(0);
		
		it = r_filas.iterator();
		
		while (it.hasNext())
		{			
			MapeoProgramacionEnvasadora mapeo = (MapeoProgramacionEnvasadora) it.next();
			unidades = unidades + mapeo.getUnidades();
			Double factor = ces.recuperarCantidadComponenteMascara(mapeo.getArticulo(), "0101");
			litros = litros + mapeo.getUnidades() * factor;
		}

		if (unidades!=null)
		{
			txtUnidades.setValue(unidades.toString());			
			txtLitros.setValue(litros.toString());			
		}
	}
	
	public void mostrarMateriales(Collection<Object> r_filas) 
	{
		Iterator it = null;
		ArrayList<String> listaArticulosSeleccionados = null;
		it = r_filas.iterator();
		MapeoProgramacionEnvasadora mapeo = null;
		listaArticulosSeleccionados = new ArrayList<String>();
		while (it.hasNext())
		{			
			mapeo = (MapeoProgramacionEnvasadora) it.next();
			
    		if (mapeo.getTipo().toUpperCase().equals("ETIQUETADO"))
    		{
    			listaArticulosSeleccionados.add(mapeo.getArticulo() + "-1");
    		}
    		else 
    		{
    			listaArticulosSeleccionados.add(mapeo.getArticulo());
    		}
		}

		/*
		 * Llamamos a la pantalla de explosion de materiales para su consulta e impresion
		 */
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		pantallaSituacionEmbotellado vtPantallaMateriales = new pantallaSituacionEmbotellado(null, listaArticulosSeleccionados, this.txtEjercicio.getValue(), this.cmbSemana.getValue().toString(), "Mesa", null);
		getUI().addWindow(vtPantallaMateriales);
	}
	
	public void rechazoMateriales(MapeoProgramacionEnvasadora r_mapeo)
	{
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		PeticionNoConformidades vtPeticionNoConf = new PeticionNoConformidades(r_mapeo);
		getUI().addWindow(vtPeticionNoConf);

	}

	public void paradas(MapeoProgramacionEnvasadora r_mapeo)
	{
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		pantallaTiemposEnvasadora vt = new pantallaTiemposEnvasadora("Paradas / Cambios" , r_mapeo);
		getUI().addWindow(vt);

	}

	public void turno()
	{
		
		((ProgramacionEnvasadoraGrid) grid).activadaVentanaPeticion=false;
		((ProgramacionEnvasadoraGrid) grid).ordenando = false;
		
		if (form!=null) form.cerrar();
		
		pantallaControlTurnoEnvasadora vt = new pantallaControlTurnoEnvasadora("Control Turno Envasadora" , this.txtEjercicio.getValue(), this.cmbSemana.getValue().toString());
		getUI().addWindow(vt);

	}

	public void enviarMail(boolean r_mail)
	{
		
		if (eBorsao.get().accessControl.getNombre().contains("Claveria"))
		{
			Integer sem = new Integer(this.cmbSemana.getValue().toString());
			if (sem < RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY())+2) 
			{
				Notificaciones.getInstance().mensajeAdvertencia("Recuerda enviar mail a todos");
	
				//			VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Enviamos mail notificando cambios?", "Si", "No", "mail", null);
				//			getUI().addWindow(vt);
			}
		}
		else
		{
			aceptarProceso("mail");
		}
	}

}
