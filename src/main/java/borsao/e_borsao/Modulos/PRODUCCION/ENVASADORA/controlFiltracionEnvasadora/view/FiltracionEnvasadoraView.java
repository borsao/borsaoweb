package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo.MapeoControlFiltracionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.server.consultaFiltracionEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class FiltracionEnvasadoraView extends GridViewRefresh {

	public static final String VIEW_NAME = "Controles Filtración Envasadora";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;

	private Button opcSubir = null;
	private Button opcBajar = null;
	
	public TextField txtEjercicio= null;
	public ComboBox cmbSemana = null;
	public String semana = null;

	public void generarGrid(HashMap<String, String> opcionesEscogidas) 
	{
		
    	ArrayList<MapeoControlFiltracionEnvasadora> r_vector=null;
    	consultaFiltracionEnvasadoraServer cfs = new consultaFiltracionEnvasadoraServer(CurrentUser.get());
    	
    	Integer ejercicio = new  Integer(this.txtEjercicio.getValue());
    	Integer semana = new  Integer(this.cmbSemana.getValue().toString());

    	r_vector = cfs.datosOpcionesGlobal(ejercicio,semana);
    	
    	grid = new OpcionesGrid(this, r_vector);
    	
    	if (((OpcionesGrid) grid).vector==null)
    	{    	
			setHayGrid(false);
    	}
    	else if (((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public FiltracionEnvasadoraView() 
    {
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	
    	this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setEnabled(true);
		this.txtEjercicio.setWidth("125px");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		
		this.cmbSemana= new ComboBox("Semana");    	
		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setWidth("125px");
		this.cmbSemana.setNullSelectionAllowed(false);
		
		this.opcSubir= new Button();    	
		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		this.opcSubir.setDescription("Semana posterior");
		
		this.opcBajar= new Button();    	
		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		this.opcBajar.setDescription("Semana anterior");
		
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
    	
    	this.cabLayout.addComponent(this.txtEjercicio);
    	this.cabLayout.addComponent(this.opcBajar);
		this.cabLayout.addComponent(this.cmbSemana);
		this.cabLayout.addComponent(this.opcSubir);
		this.cabLayout.setComponentAlignment(this.opcBajar, Alignment.BOTTOM_LEFT);
		this.cabLayout.setComponentAlignment(this.opcSubir, Alignment.BOTTOM_LEFT);
		this.cargarListeners();
		
		this.opcBuscar.setVisible(false);
		this.opcNuevo.setVisible(false);
		
		this.generarGrid(null);
//	
    }

    private void cargarListeners()
    {
    	this.opcSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);

			}
		});
    	
    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);

			}
		});

		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				generarGrid(null);
			}
		});
    }
    
    public void newForm()
    {

    }
    
    public void verForm(boolean r_busqueda)
    {    	

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    }

    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(FiltracionEnvasadoraView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(FiltracionEnvasadoraView.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {

	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
	}


}
