package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.server.consultaSituacionMPEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
//	public boolean actualizar = false;	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public OpcionesGrid(ArrayList<MapeoSituacionMPEnvasadora> r_vector) 
    {
        
        this.vector=r_vector;
        
		this.asignarTitulo("Situacion Materias Primas Envasadora");
		
    	if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
    	else
    	{
    		this.generarGrid();    		
    	}    	
    	
    }

    /*
     * METODOS PUBLICOS
     */

    public void establecerColumnasNoFiltro()
    {    	
    	this.camposNoFiltrar.add("ver");
    	this.camposNoFiltrar.add("negativo");
    	this.camposNoFiltrar.add("stock_actual");
    	this.camposNoFiltrar.add("stock_real");
    	this.camposNoFiltrar.add("pdte_servir");
    	this.camposNoFiltrar.add("primerPlazo");
    }
    
    private void generarGrid()
    {
//		this.actualizar = false;
		this.crearGrid(MapeoSituacionMPEnvasadora.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
		this.setFrozenColumnCount(3);
		
    }
    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("negativo", "ver", "articulo","descripcion","stock_actual","pdte_servir","pdte_recibir", "stock_real","primerPlazo");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("ver").setHeaderCaption("");
    	this.getColumn("ver").setSortable(false);
    	this.getColumn("ver").setWidth(new Double(50));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("negativo").setHidden(true);
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidth(new Double(120));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("stock_actual").setHeaderCaption("Existencias");
    	this.getColumn("stock_actual").setWidth(new Double(125));
    	this.getColumn("pdte_servir").setHeaderCaption("Pdte. Servir");
    	this.getColumn("pdte_servir").setWidth(new Double(125));
    	this.getColumn("pdte_recibir").setHeaderCaption("Pdte. Recibir");
    	this.getColumn("pdte_recibir").setWidth(new Double(125));
    	this.getColumn("stock_real").setHeaderCaption("Stock Real");
    	this.getColumn("stock_real").setWidth(new Double(125));
    	this.getColumn("primerPlazo").setHeaderCaption("Fecha Rcepción");
    	this.getColumn("primerPlazo").setWidth(new Double(125));
	}
	
    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("ver"))
            	{
            		MapeoSituacionMPEnvasadora mapeo = (MapeoSituacionMPEnvasadora) event.getItemId();
            		pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(mapeo.getArticulo(), mapeo.getStock_real(), "Lineas Pedidos Compra del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
            		getUI().addWindow(vt);
            	}
            }
        });
    }
    
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
    		String estado = null;
    		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("negativo".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue().toString().equals("Error"))
            			estado="Error";
            		else if (cellReference.getValue().toString().equals("Advertencia"))
            			estado = "Advertencia";
            		else
            			estado = "Ok";
            	}
            	
            	
            	if ("stock_actual".equals(cellReference.getPropertyId()) || "stock_real".equals(cellReference.getPropertyId()) || "pdte_servir".equals(cellReference.getPropertyId()) || "pdte_recibir".equals(cellReference.getPropertyId())) 
            	{
            		if (estado.equals("Error"))
            		{
            			return "Rcell-error";
            		}
        			else if (estado.equals("Advertencia"))
        			{
        				return "Rcell-warning";
        			}
        			else 
        			{
        				return "Rcell-normal";
        			}
            		
            	}
            	else if ( "ver".equals(cellReference.getPropertyId()))
            	{
            		return "nativebuttonBars";
            	}
            	else
            	{
            		if (estado.equals("Error"))
            		{
            			return "cell-error";
            		}
        			else if (estado.equals("Advertencia"))
        			{
        				return "cell-warning";
        			}
        			else 
        			{
        				return "cell-normal";
        			}
            	}
            }
        });
    }
    public void generacionPdf(boolean regenerar, boolean r_eliminar) 
    {
    	String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaSituacionMPEnvasadoraServer cmes = consultaSituacionMPEnvasadoraServer.getInstance(CurrentUser.get());
    	pdfGenerado = cmes.generarInforme((ArrayList<MapeoSituacionMPEnvasadora>) this.vector, regenerar);
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
		
		pdfGenerado = null;

    }

	@Override
	public void calcularTotal() {
		
	}
}
