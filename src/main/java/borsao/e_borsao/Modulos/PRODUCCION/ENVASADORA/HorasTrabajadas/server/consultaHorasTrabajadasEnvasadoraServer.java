package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.HorasTrabajadas.modelo.MapeoHorasTrabajadasEnvasadora;

public class consultaHorasTrabajadasEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaHorasTrabajadasEnvasadoraServer instance;
	
	public consultaHorasTrabajadasEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaHorasTrabajadasEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaHorasTrabajadasEnvasadoraServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoHorasTrabajadasEnvasadora> datosOpcionesGlobal(MapeoHorasTrabajadasEnvasadora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoHorasTrabajadasEnvasadora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_horas_env.idCodigo hor_id, ");
		cadenaSQL.append(" prd_horas_env.ejercicio hor_eje, ");
		cadenaSQL.append(" prd_horas_env.semana hor_sem, ");
		cadenaSQL.append(" prd_horas_env.formato hor_for, ");
		cadenaSQL.append(" prd_horas_env.horas hor_hor, ");
		cadenaSQL.append(" prd_horas_env.horasEtt hor_hor_ett, ");
		cadenaSQL.append(" prd_horas_env.programados hor_prg, ");
		cadenaSQL.append(" prd_horas_env.mermas hor_mer, ");
        cadenaSQL.append(" prd_horas_env.incidencias hor_inc, ");
        cadenaSQL.append(" prd_horas_env.produccion5L hor_p5L, ");
        cadenaSQL.append(" prd_horas_env.produccion2L hor_p2L, ");
        cadenaSQL.append(" prd_horas_env.produccion5LR hor_p5LR, ");
        cadenaSQL.append(" prd_horas_env.produccion2LR hor_p2LR ");
     	cadenaSQL.append(" FROM prd_horas_env ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + r_mapeo.getSemana());
				}
				if (r_mapeo.getFormato()!=null && r_mapeo.getFormato()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" formato= " + r_mapeo.getFormato());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoHorasTrabajadasEnvasadora>();
			
			while(rsOpcion.next())
			{
				MapeoHorasTrabajadasEnvasadora mapeo = new MapeoHorasTrabajadasEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("hor_id"));
				mapeo.setEjercicio(rsOpcion.getInt("hor_eje"));
				mapeo.setFormato(rsOpcion.getInt("hor_for"));
				mapeo.setSemana(rsOpcion.getInt("hor_sem"));
				mapeo.setHoras(rsOpcion.getDouble("hor_hor"));
				mapeo.setHorasEtt(rsOpcion.getDouble("hor_hor_ett"));
				mapeo.setProgramadas(rsOpcion.getDouble("hor_prg"));
				mapeo.setMermas(rsOpcion.getDouble("hor_mer"));
				mapeo.setIncidencias(rsOpcion.getDouble("hor_inc"));
				mapeo.setProduccion5L(rsOpcion.getInt("hor_p5L"));
				mapeo.setProduccion2L(rsOpcion.getInt("hor_p2L"));
				mapeo.setProduccion5LR(rsOpcion.getInt("hor_p5LR"));
				mapeo.setProduccion2LR(rsOpcion.getInt("hor_p2LR"));

				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public Double recuperarHoras(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horas) as horas from prd_horas_env where ejercicio = " + r_ejercicio;
			
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}
			
			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarHorasEtt(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horasEtt) as horas from prd_horas_env where ejercicio = " + r_ejercicio;
			
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}
			
			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	
	public Double recuperarIncidencias(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(incidencias) as horas from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}
	
	public Double recuperarProgramadas(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(programados) as horas from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public HashMap<String,Double> recuperarHorasFormatoSemanas(Integer r_ejercicio, Integer r_semana, String r_formato)
	{
		HashMap<String,Double> horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select semana, sum(horas) as horas from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0) sql = sql + " and semana <= " + r_semana ; else if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) sql = sql + " and semana < " + RutinasFechas.semanaActual(r_ejercicio.toString());
			sql = sql + " and formato = '" + r_formato + "' " ;
			sql = sql + " group by semana" ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			horas_obtenido = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				horas_obtenido.put(rsHoras.getString("semana"),rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarMermas(Integer r_ejercicio, Integer r_semana, Integer r_formato)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(mermas) as horas from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ; else if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) sql = sql + " and semana < " + RutinasFechas.semanaActual(r_ejercicio.toString());
			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarProduccion5L(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double prod_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(produccion5L) as p5L from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				prod_obtenido = rsHoras.getDouble("p5L");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return prod_obtenido;
	}

	public Double recuperarProduccion2L(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double prod_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(produccion2L) as p2L from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				prod_obtenido = rsHoras.getDouble("p2L");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return prod_obtenido;
	}
	public Double recuperarProduccion5LR(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double prod_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(produccion5LR) as p5LR from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				prod_obtenido = rsHoras.getDouble("p5LR");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return prod_obtenido;
	}

	public Double recuperarProduccion2LR(Integer r_ejercicio, Integer r_semana, Integer r_formato, String r_vista)
	{
		Double prod_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(produccion2LR) as p2LR from prd_horas_env where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0)
			{
				if (r_vista.contentEquals("Semanal"))
					sql = sql + " and semana = " + r_semana;
				else
					sql = sql + " and semana <= " + r_semana;
			}

			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				prod_obtenido = rsHoras.getDouble("p2LR");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return prod_obtenido;
	}
	public boolean comprobarHoras(Integer r_ejercicio, Integer r_semana, Integer r_formato)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select horas as horas from prd_horas_env where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public boolean comprobarIncidencias(Integer r_ejercicio, Integer r_semana, Integer r_formato)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select incidencias as horas from prd_horas_env where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
			if(r_formato!=null && r_formato>0) sql = sql + " and formato = " + r_formato;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}


	public String guardarNuevo(MapeoHorasTrabajadasEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_horas_env ( ");
			cadenaSQL.append(" prd_horas_env.idCodigo, ");
			cadenaSQL.append(" prd_horas_env.ejercicio, ");
    		cadenaSQL.append(" prd_horas_env.semana, ");
    		cadenaSQL.append(" prd_horas_env.formato, ");
    		cadenaSQL.append(" prd_horas_env.horas, ");
    		cadenaSQL.append(" prd_horas_env.programados, ");
    		cadenaSQL.append(" prd_horas_env.mermas, ");
    		cadenaSQL.append(" prd_horas_env.incidencias, ");
    		cadenaSQL.append(" prd_horas_env.produccion5L, ");
    		cadenaSQL.append(" prd_horas_env.produccion2L, ");
    		cadenaSQL.append(" prd_horas_env.produccion5LR, ");
    		cadenaSQL.append(" prd_horas_env.produccion2LR, ");
	        cadenaSQL.append(" prd_horas_env.horasEtt ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_horas_env", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getFormato()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getFormato());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getProduccion5L()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getProduccion5L());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getProduccion2L()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getProduccion2L());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getProduccion5LR()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getProduccion5LR());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getProduccion2LR()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getProduccion2LR());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(13, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoHorasTrabajadasEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_horas_env SET ");			
			cadenaSQL.append(" prd_horas_env.ejercicio=?, ");
    		cadenaSQL.append(" prd_horas_env.semana=?, ");
    		cadenaSQL.append(" prd_horas_env.formato=?, ");
    		cadenaSQL.append(" prd_horas_env.horas=?, ");
    		cadenaSQL.append(" prd_horas_env.programados=?, ");
    		cadenaSQL.append(" prd_horas_env.mermas=?, ");
    		cadenaSQL.append(" prd_horas_env.incidencias=?, ");
    		cadenaSQL.append(" prd_horas_env.produccion5L=?, ");
    		cadenaSQL.append(" prd_horas_env.produccion2L=?, ");
    		cadenaSQL.append(" prd_horas_env.produccion5LR=?, ");
	        cadenaSQL.append(" prd_horas_env.produccion2LR=?, ");
	        cadenaSQL.append(" prd_horas_env.horasEtt=? ");
	        cadenaSQL.append(" WHERE prd_horas_env.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    if (r_mapeo.getFormato()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getFormato());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }

		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getProduccion5L()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getProduccion5L());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getProduccion2L()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getProduccion2L());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getProduccion5LR()!=null)
		    {
		    	preparedStatement.setInt(10, r_mapeo.getProduccion5LR());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getProduccion2LR()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getProduccion2LR());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(12, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    preparedStatement.setInt(13, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoHorasTrabajadasEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_horas_env ");            
			cadenaSQL.append(" WHERE prd_horas_env.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}