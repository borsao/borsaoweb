package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.ProgramacionEnvasadoraGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.modelo.MapeoOpcionesMS;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.server.consultaOpcionesMateriaSecaServer;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.modelo.MapeoTiposOF;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server.consultaTiposOFServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoProgramacionEnvasadora mapeoProgramacion = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaProgramacionEnvasadoraServer cps = null;
	 private String strSemana= null;
	 private ProgramacionEnvasadoraView uw = null;
	 private BeanFieldGroup<MapeoProgramacionEnvasadora> fieldGroup;
	 
	 public OpcionesForm(ProgramacionEnvasadoraView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoProgramacionEnvasadora>(MapeoProgramacionEnvasadora.class);
        fieldGroup.bindMemberFields(this);

        this.cargarCombo(null);        
        this.cargarTiposOf();
        this.establecerCamposObligatorios();
        this.cargarCombosMateriaSeca();
        this.cargarValidators();
        this.cargarListeners();
    }   

	private void cargarValidators()
	{
//		tipo.setValidationVisible(false);
//		class MyValidator implements Validator {
//            private static final long serialVersionUID = -8281962473854901819L;
//
//            @Override
//            public void validate(Object value)
//                    throws InvalidValueException {
//            	
//            	if ((tipo.getValue()==null))
//            	{
//            		tipo.focus();	
//                    throw new InvalidValueException("Debes darle un valor al tipo de fabricacion");
//				}
//        	}
//        }
//		tipo.addValidator(new MyValidator());
//		
//		tipo.addValueChangeListener(new ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				tipo.setValidationVisible(true);
//			}
//		});
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeoProgramacion = (MapeoProgramacionEnvasadora) uw.grid.getSelectedRow();
                eliminarProgramacion(mapeoProgramacion);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            	uw.enviarMail(true);
            }
        });  
        
        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes();
            }
        });
		
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.cmbSemana.setValue(semana.getValue());
	 				uw.txtEjercicio.setValue(ejercicio.getValue());
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo(); 
		 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 				mapeoProgramacion = hm.convertirHashAMapeo(uw.opcionesEscogidas);	 				 
		                crearProgramacion(mapeoProgramacion);
			           	removeStyleName("visible");
			           	uw.newForm();
			           	uw.enviarMail(true);
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoProgramacionEnvasadora mapeoProgramacion_orig = (MapeoProgramacionEnvasadora) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 				mapeoProgramacion= hm.convertirHashAMapeo(uw.opcionesEscogidas);
		 				modificarProgramacion(mapeoProgramacion,mapeoProgramacion_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");
			           	uw.enviarMail(true);
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((ProgramacionEnvasadoraGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		
		this.ejercicio.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				cargarCombo(ejercicio.getValue());
				if (semana.getValue()!=null && isCreacion())
				{
					obtenerOrden(ejercicio.getValue(),semana.getValue().toString());
				}
			}
		});
		
		this.semana.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (ejercicio.getValue()!=null && semana.getValue()!=null && isCreacion())
				{
					obtenerOrden(ejercicio.getValue(),semana.getValue().toString());
				}
			}
		});
		
		this.cajas.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cajas.getValue()!=null && cajas.getValue().length()>0 && !cajas.getValue().equals("0"))
				{
					if (factor.getValue()!=null && factor.getValue().length()>0)
					{
						Integer total = Math.multiplyExact(RutinasNumericas.formatearDoubleDeESP(cajas.getValue()).intValue(),RutinasNumericas.formatearDoubleDeESP(factor.getValue()).intValue());
						unidades.setValue(String.valueOf(total));
					}
		
				}
			}
		});
		
		this.factor.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (factor.getValue()!=null && factor.getValue().length()>0 && cajas.getValue()!=null && !cajas.getValue().equals("0"))
				{
					if (cajas.getValue()!=null && cajas.getValue().length()>0)
					{
						Integer total = Math.multiplyExact(RutinasNumericas.formatearDoubleDeESP(cajas.getValue()).intValue(),RutinasNumericas.formatearDoubleDeESP(factor.getValue()).intValue());
						unidades.setValue(String.valueOf(total));
						observaciones.focus();
						observaciones.selectAll();
					}
		
				}
			}
		});
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
				
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0 )
				{
					cps  = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
					String desc = cps.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
					consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
					Integer fac = ces.obtenerCantidadPaletEnvasadora(articulo.getValue()).intValue();
					factor.setValue(fac.toString());
					cajas.focus();
					cajas.setSelectionRange(0, 1);
				}
			}
		});
		this.tipo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (tipo.getValue()!=null && tipo.getValue().toString().length()>0)
				{
					if ((tipo.getValue().equals("Etiquetado") || tipo.getValue().equals("Embotellado")) && !isBusqueda())
					{
						establecerCamposObligatoriosCreacion(true);
					}
					else
					{
						establecerCamposObligatoriosCreacion(false);
					}
				}
			}
		});
		this.ejercicio.focus();
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarProgramacion(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.dia.getValue()!=null && this.dia.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("dia", this.dia.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("dia", "");
		}
		if (this.hora.getValue()!=null && this.hora.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("hora", this.hora.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("hora", "");
		}
		if (this.tipo.getValue()!=null && this.tipo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("tipoOperacion", this.tipo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("tipoOperacion", "");
		}
		if (this.vino.getValue()!=null && this.vino.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("vino", this.vino.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("vino", "");
		}
		if (this.lote.getValue()!=null && this.lote.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("lote", this.lote.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("lote", "");
		}
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		if (this.frontales.getValue()!=null && this.frontales.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("frontales", this.frontales.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("frontales", "");
		}
		{
			opcionesEscogidas.put("contras", "");
		}
		if (this.caja.getValue()!=null && this.caja.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("caja", this.caja.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("caja", "");
		}		
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.cajas.getValue()!=null && this.cajas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cajas", this.cajas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cajas", "");
		}
		if (this.factor.getValue()!=null && this.factor.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("factor", this.factor.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("factor", "");
		}
		if (this.unidades.getValue()!=null && this.unidades.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("unidades", this.unidades.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("unidades", "");
		}
		if (this.orden.getValue()!=null && this.orden.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("orden", this.orden.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("orden", "");
		}

		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarProgramacion(null);
		}
	}
	
	public void editarProgramacion(MapeoProgramacionEnvasadora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoProgramacionEnvasadora();
			if (!isBusqueda())
			{
				r_mapeo.setCajas(0);
				r_mapeo.setFactor(0);
				r_mapeo.setUnidades(0);
				r_mapeo.setCaja("Si");
				r_mapeo.setFrontales("Si");
			}
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoProgramacionEnvasadora>(r_mapeo));
		if (this.strSemana!=null&& isCreacion()) this.semana.setValue(this.strSemana);
		this.ejercicio.focus();
	}
	
	public void eliminarProgramacion(MapeoProgramacionEnvasadora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
//		cus = new consultaProgramacionServer(CurrentUser.get());
		((ProgramacionEnvasadoraGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoProgramacionEnvasadora>) ((ProgramacionEnvasadoraGrid) uw.grid).vector).remove(r_mapeo);
		if (((ProgramacionEnvasadoraGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
	}
	
	public void modificarProgramacion(MapeoProgramacionEnvasadora r_mapeo, MapeoProgramacionEnvasadora r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdProgramacion(r_mapeo_orig.getIdProgramacion());
		r_mapeo.setEstado(r_mapeo_orig.getEstado());
		r_mapeo.setStatus(r_mapeo_orig.getStatus());
		uw.cus.guardarCambios(r_mapeo);		
		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdProgramacion()));
		((ProgramacionEnvasadoraGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	
	public void crearProgramacion(MapeoProgramacionEnvasadora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdProgramacion(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdProgramacion()));
		
		if (rdo== null)
		{
			if (this.strSemana.equals(r_mapeo.getSemana()))
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoProgramacionEnvasadora>(r_mapeo));
				if (((ProgramacionEnvasadoraGrid) uw.grid)!=null )//&& )
				{
					if (!((ProgramacionEnvasadoraGrid) uw.grid).vector.isEmpty())
					{
						((ProgramacionEnvasadoraGrid) uw.grid).refresh(r_mapeo,null);
						((ProgramacionEnvasadoraGrid) uw.grid).sort("orden");
						((ProgramacionEnvasadoraGrid) uw.grid).scrollTo(r_mapeo);
					}
					else
					{
						uw.actualizarGrid();
					}
					((ArrayList<MapeoProgramacionEnvasadora>) ((ProgramacionEnvasadoraGrid) uw.grid).vector).add(r_mapeo);
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoProgramacionEnvasadora> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoProgramacion= item.getBean();
            if (this.mapeoProgramacion.getIdProgramacion()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }
    
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.ejercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.semana.removeAllItems();
		}
		
		if (uw.cmbSemana.getValue()!=null)
		{
			this.strSemana=uw.cmbSemana.getValue().toString();
		}
		else
		{
			this.strSemana=String.valueOf(RutinasFechas.semanaActual(this.ejercicio.getValue()));
		}
		
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.semana.addItem(String.valueOf(i));
		}
	}

	private void cargarTiposOf()
	{
		consultaTiposOFServer ctof = consultaTiposOFServer.getInstance(CurrentUser.get());
		ArrayList<MapeoTiposOF> tipos = ctof.datosOFGlobal(null);
		if (tipos!=null && tipos.size()>0)
		{
			for (int i = 0; i<tipos.size();i++)
			{
				MapeoTiposOF mapeo = (MapeoTiposOF) tipos.get(i);
				this.tipo.addItem(mapeo.getNombre());
			}
			
			this.tipo.setNullSelectionAllowed(false);
			this.tipo.setNewItemsAllowed(false);
		}
		tipos=null;
		ctof=null;
	}

	private void cargarCombosMateriaSeca()
	{
		consultaOpcionesMateriaSecaServer cms = consultaOpcionesMateriaSecaServer.getInstance(CurrentUser.get());
		ArrayList<MapeoOpcionesMS> valores = cms.datosOpcionesGlobal(null);
		
		if (valores!=null && valores.size()>0)
		{
			for (int i = 0; i<valores.size();i++)
			{
				MapeoOpcionesMS mapeo = (MapeoOpcionesMS) valores.get(i);
				this.frontales.addItem(mapeo.getNombre());
				this.caja.addItem(mapeo.getNombre());
			}
		}
		valores=null;
		cms=null;
		
		this.frontales.setNullSelectionAllowed(false);
		this.frontales.setNewItemsAllowed(false);

		this.caja.setNullSelectionAllowed(false);
		this.caja.setNewItemsAllowed(false);
		
	}

	private void ventanaMargenes()
	{
		ArrayList<MapeoAyudas> vectorArticulos = null;
		cps  = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		vectorArticulos=cps.vector();
		this.vHelp = new ventanaAyuda(this.articulo, this.descripcion, vectorArticulos, "Articulos");
		getUI().addWindow(this.vHelp);	
	}

	private void establecerCamposObligatorios()
	{
		this.descripcion.setRequired(true);
		this.ejercicio.setRequired(true);
		this.semana.setRequired(true);
		this.tipo.setRequired(true);
		this.frontales.setRequired(true);
		this.caja.setRequired(true);
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga)
	{
		this.cajas.setRequired(r_obliga);
		this.factor.setRequired(r_obliga);
		this.unidades.setRequired(r_obliga);
		this.articulo.setRequired(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (this.tipo.getValue()==null || this.tipo.getValue().toString().length()==0) return false;
		if (this.semana.getValue()==null || this.semana.getValue().toString().length()==0) return false;
		if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0) return false;
		if (this.orden.getValue()==null || this.orden.getValue().toString().equals("0")) return false;
			
		if (!isBusqueda() && (this.tipo.getValue().toString().equals("EMBOTELLADO") || this.tipo.getValue().toString().equals("ETIQUETADO")))
		{
			if (this.articulo.getValue()==null || this.articulo.getValue().length()==0) return false;
			if (this.cajas.getValue()==null || this.cajas.getValue().length()==0) return false;
			if (this.factor.getValue()==null || this.factor.getValue().length()==0) return false;
			if (this.unidades.getValue()==null || this.unidades.getValue().length()==0) return false;
		}
		
		return true;
	}
	
	
	private void obtenerOrden(String r_ejercicio, String r_semana)
	{
		Integer orden = null;
		
		consultaProgramacionEnvasadoraServer cps = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		orden = cps.obtenerOrden(RutinasNumericas.formatearIntegerDeESP(r_ejercicio), r_semana);
		this.orden.setValue(String.valueOf(orden));
	}
	
	public void cerrar()
	{
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
}
