package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.articulosMPEnvasadora.modelo;

import java.util.HashMap;

import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoArticulos mapeo = null;
	 
	 public MapeoArticulos convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulos();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 return mapeo;		 
	 }
	 
	 public MapeoArticulos convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoArticulos();
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoArticulos r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("articulo", r_mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 return hash;		 
	 }
}