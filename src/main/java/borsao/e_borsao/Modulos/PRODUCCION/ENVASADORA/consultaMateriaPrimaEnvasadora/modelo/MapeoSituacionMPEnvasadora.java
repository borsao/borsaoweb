package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoSituacionMPEnvasadora extends MapeoGlobal
{
	private String articulo;
	private String descripcion;
	private Integer stock_actual;
	private Integer pdte_servir;
	private Integer pdte_recibir;
	private Integer stock_real;
	private String ver = " ";
	private String negativo = " ";
	private Date primerPlazo = null;	

	public MapeoSituacionMPEnvasadora()
	{
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getStock_actual() {
		return stock_actual;
	}


	public void setStock_actual(Integer stock_actual) {
		this.stock_actual = stock_actual;
	}


	public Integer getPdte_servir() {
		return pdte_servir;
	}


	public void setPdte_servir(Integer pdte_servir) {
		this.pdte_servir = pdte_servir;
	}


	public Integer getStock_real() {
		return stock_real;
	}


	public void setStock_real(Integer stock_real) {
		this.stock_real = stock_real;
	}


	public Integer getPdte_recibir() {
		return pdte_recibir;
	}


	public void setPdte_recibir(Integer pdte_recibir) {
		this.pdte_recibir = pdte_recibir;
	}


	public String getVer() {
		return ver;
	}


	public void setVer(String ver) {
		this.ver = ver;
	}


	public String getNegativo() {
		return negativo;
	}

	public void setNegativo(String negativo) {
		this.negativo = negativo;
	}

	public String getPrimerPlazo() {
		if (primerPlazo!=null)
			return RutinasFechas.convertirDateToString(primerPlazo);
		else
			return null;
	}
	

	public void setPrimerPlazo(Date primerPlazo) {
		this.primerPlazo = primerPlazo;
	}

}