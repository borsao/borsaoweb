package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;

public class MapeoControlPreoperativoEnvasadora extends MapeoGlobal
{
	private Integer idTurno;
	private String hora;
	private String sala_ok;
	private String maquina_limpia;
	private String ausencia_cuerpos;
	private String maquina_ok;
	private String linea_ok;
	private String realizado;
	private String verificado;
	private String observaciones;
	private Date fecha;
	private MapeoProduccionTurnoEnvasadora mapeo = null;
	
	public MapeoControlPreoperativoEnvasadora()
	{
		this.mapeo= new MapeoProduccionTurnoEnvasadora();
	}


	public Integer getIdTurno() {
		return idTurno;
	}


	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}


	public String getHora() {
		return hora;
	}


	public void setHora(String hora) {
		this.hora = hora;
	}


	public String getSala_ok() {
		return sala_ok;
	}


	public void setSala_ok(String sala_ok) {
		this.sala_ok = sala_ok;
	}


	public String getMaquina_limpia() {
		return maquina_limpia;
	}


	public void setMaquina_limpia(String maquina_limpia) {
		this.maquina_limpia = maquina_limpia;
	}


	public String getAusencia_cuerpos() {
		return ausencia_cuerpos;
	}


	public void setAusencia_cuerpos(String ausencia_cuerpos) {
		this.ausencia_cuerpos = ausencia_cuerpos;
	}


	public String getMaquina_ok() {
		return maquina_ok;
	}


	public void setMaquina_ok(String maquina_ok) {
		this.maquina_ok = maquina_ok;
	}


	public String getLinea_ok() {
		return linea_ok;
	}


	public void setLinea_ok(String linea_ok) {
		this.linea_ok = linea_ok;
	}


	public String getRealizado() {
		return realizado;
	}


	public void setRealizado(String realizado) {
		this.realizado = realizado;
	}


	public String getVerificado() {
		return verificado;
	}


	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public MapeoProduccionTurnoEnvasadora getMapeo() {
		return mapeo;
	}


	public void setMapeo(MapeoProduccionTurnoEnvasadora mapeo) {
		this.mapeo = mapeo;
	}


	public Date getFecha() {
		return fecha;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


}