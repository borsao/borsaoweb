package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.view;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.modelo.MapeoControlFiltracionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.server.consultaFiltracionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaFiltracionEnvasadora extends Ventana
{
	public MapeoProgramacionEnvasadora mapeoEnvasadora = null;
	private MapeoProduccionTurnoEnvasadora mapeoProduccion = null;
	private MapeoControlFiltracionEnvasadora mapeoControlFiltracionEnvasadora = null;
	private consultaFiltracionEnvasadoraServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout framePie = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	
	private EntradaDatosFecha txtFecha = null;
	
	private Combo cmbTurno = null;
	private Combo cmbSemana = null;
	private TextField txtEjercicio = null;
	private Button btnBuscar = null;	
	
	private TextField txtHoras= null;
	
	private Combo cmbFiltro1 = null;
	private Combo cmbFiltro2 = null;
	private Combo cmbIntegridad = null;
	private Combo cmbConductividad = null;
	private Combo cmbTipo = null;
	
	private TextField txtFiltro1 = null;
	private TextField txtFiltro2 = null;
	private TextField txtIntegridad = null;
	private TextField txtConductividad = null;;
	
	private TextField txtRealizado= null;
	private TextArea txtObservaciones= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaFiltracionEnvasadora(String r_titulo, MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		this.setCaption(r_titulo);
		this.mapeoProduccion = r_mapeo;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaFiltracionEnvasadoraServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("550px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtEjercicio=new TextField("Ejercicio");
        		this.txtEjercicio.setEnabled(false);
        		this.txtEjercicio.setValue(this.mapeoProduccion.getEjercicio().toString());
        		this.txtEjercicio.setWidth("100px");
        		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.cmbSemana= new Combo("Semana");    		
        		this.cmbSemana.setNewItemsAllowed(false);
        		this.cmbSemana.setNullSelectionAllowed(false);
        		this.cmbSemana.setEnabled(false);
        		this.cmbSemana.setWidth("100px");
        		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

				this.txtFecha = new EntradaDatosFecha();
				this.txtFecha.setCaption("Fecha");
				this.txtFecha.setEnabled(false);
				this.txtFecha.setValue(this.mapeoProduccion.getFecha());
				this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFecha.setWidth("150px");
				
    			this.cmbTurno= new Combo("Turno");
    			this.cmbTurno.setWidth("150px");
    			this.cmbTurno.setEnabled(false);
    			this.cmbTurno.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbTurno.setNewItemsAllowed(false);
    			this.cmbTurno.setNullSelectionAllowed(false);

    			btnBuscar= new Button("Buscar Partes");
    			btnBuscar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnBuscar.addStyleName(ValoTheme.BUTTON_TINY);

    			linea1.addComponent(this.txtEjercicio);
    			linea1.addComponent(this.cmbSemana);
    			linea1.addComponent(this.txtFecha);
    			linea1.addComponent(this.cmbTurno);
    			linea1.addComponent(this.btnBuscar);
    			linea1.setComponentAlignment(this.btnBuscar, Alignment.BOTTOM_LEFT);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Hora");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");
    			
				this.cmbFiltro1= new Combo("Presion Filtro 1");
    			this.cmbFiltro1.setWidth("150px");
    			this.cmbFiltro1.setEnabled(false);
    			this.cmbFiltro1.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbFiltro1.setNewItemsAllowed(false);
    			this.cmbFiltro1.setNullSelectionAllowed(false);

    			this.txtFiltro1= new TextField();
    			this.txtFiltro1.setCaption("Valor Presion Filtro 1");
    			this.txtFiltro1.setVisible(false);
    			this.txtFiltro1.setWidth("120px");
				this.txtFiltro1.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFiltro1.addStyleName("rightAligned");
				
    			this.cmbFiltro2= new Combo("Presion Filtro 2");
    			this.cmbFiltro2.setWidth("150px");
    			this.cmbFiltro2.setEnabled(false);
    			this.cmbFiltro2.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbFiltro2.setNewItemsAllowed(false);
    			this.cmbFiltro2.setNullSelectionAllowed(false);
    			
    			this.txtFiltro2= new TextField();
    			this.txtFiltro2.setCaption("Valor Presion Filtro 2");
    			this.txtFiltro2.setVisible(false);
    			this.txtFiltro2.setWidth("120px");
				this.txtFiltro2.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtFiltro2.addStyleName("rightAligned");

				linea2.addComponent(this.txtHoras);
				linea2.setComponentAlignment(this.txtHoras, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.cmbFiltro1);
    			linea2.addComponent(this.txtFiltro1);
    			linea2.setComponentAlignment(this.txtFiltro1, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.cmbFiltro2);
    			linea2.addComponent(this.txtFiltro2);
    			linea2.setComponentAlignment(this.txtFiltro2, Alignment.BOTTOM_LEFT);
    			
			HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
				this.cmbIntegridad= new Combo("Test Integridad");
    			this.cmbIntegridad.setWidth("150px");
    			this.cmbIntegridad.setEnabled(false);
    			this.cmbIntegridad.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbIntegridad.setNewItemsAllowed(false);
    			this.cmbIntegridad.setNullSelectionAllowed(false);

    			this.txtIntegridad= new TextField();
    			this.txtIntegridad.setCaption("Valor Test Integridad");
    			this.txtIntegridad.setVisible(false);
    			this.txtIntegridad.setWidth("120px");
				this.txtIntegridad.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtIntegridad.addStyleName("rightAligned");
				
    			this.cmbConductividad= new Combo("Conductividad");
    			this.cmbConductividad.setWidth("150px");
    			this.cmbConductividad.setEnabled(false);
    			this.cmbConductividad.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbConductividad.setNewItemsAllowed(false);
    			this.cmbConductividad.setNullSelectionAllowed(false);

    			this.txtConductividad= new TextField();
    			this.txtConductividad.setCaption("Valor Conductividad");
    			this.txtConductividad.setVisible(false);
    			this.txtConductividad.setWidth("120px");
				this.txtConductividad.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtConductividad.addStyleName("rightAligned");

    			this.cmbTipo= new Combo("Limpieza con");
    			this.cmbTipo.setWidth("150px");
    			this.cmbTipo.setEnabled(false);
    			this.cmbTipo.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.cmbTipo.setNewItemsAllowed(false);
    			this.cmbTipo.setNullSelectionAllowed(false);


    			linea3.addComponent(this.cmbIntegridad);
    			linea3.addComponent(this.txtIntegridad);
    			linea3.setComponentAlignment(this.txtIntegridad, Alignment.BOTTOM_LEFT);
    			linea3.addComponent(this.cmbConductividad);
    			linea3.addComponent(this.txtConductividad);
    			linea3.setComponentAlignment(this.txtConductividad, Alignment.BOTTOM_LEFT);
    			linea3.addComponent(this.cmbTipo);
    			
    		HorizontalLayout linea4 = new HorizontalLayout();
    			linea4.setSpacing(true);
    			
    			this.txtRealizado= new TextField();
    			this.txtRealizado.setCaption("Realizado Por");
				this.txtRealizado.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtRealizado.setWidth("180px");
				
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			linea4.addComponent(this.txtRealizado);
    			linea4.addComponent(this.txtObservaciones);
				
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		this.frameCentral.addComponent(linea4);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
				framePie.addComponent(btnBotonAVentana);		
				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.cmbFiltro1.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
				if (cmbFiltro1.getValue().toString().contentEquals("OK"))
				{
					txtFiltro1.setValue("");
					txtFiltro1.setVisible(false);
				}
				else
				{
					txtFiltro1.setVisible(true);
				}
			}
		});
		
		this.cmbFiltro2.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
				if (cmbFiltro2.getValue().toString().contentEquals("OK"))
				{
					txtFiltro2.setValue("");
					txtFiltro2.setVisible(false);
				}
				else
				{
					txtFiltro2.setVisible(true);
				}
			}
		});

		this.cmbIntegridad.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
				if (cmbIntegridad.getValue().toString().contentEquals("OK"))
				{
					txtIntegridad.setValue("");
					txtIntegridad.setVisible(false);
				}
				else
				{
					txtIntegridad.setVisible(true);
				}
			}
		});
		
		this.cmbConductividad.addValueChangeListener(new Property.ValueChangeListener()  {
			
			public void valueChange(ValueChangeEvent event) {
				if (cmbConductividad.getValue().toString().contentEquals("OK"))
				{
					txtConductividad.setValue("");
					txtConductividad.setVisible(false);
				}
				else
				{
					txtConductividad.setVisible(true);
				}
			}
		});

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnBuscar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				llenarRegistros();
			}
		});
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlFiltracionEnvasadora mapeo = new MapeoControlFiltracionEnvasadora();
			
			mapeo.setEstadoFiltro1(this.cmbFiltro1.getValue().toString());
			mapeo.setEstadoFiltro2(this.cmbFiltro2.getValue().toString());
			mapeo.setEstadoIntegridad(this.cmbIntegridad.getValue().toString());
			mapeo.setEstadoConductividad(this.cmbConductividad.getValue().toString());
			mapeo.setTipoLimpieza(this.cmbTipo.getValue().toString());
			
			if (this.txtFiltro1.getValue()!=null && this.txtFiltro1.getValue().length()>0) mapeo.setValorFiltro1(new Double(this.txtFiltro1.getValue())); else mapeo.setValorFiltro1(null);
			if (this.txtFiltro2.getValue()!=null && this.txtFiltro2.getValue().length()>0) mapeo.setValorFiltro2(new Double(this.txtFiltro2.getValue())); else mapeo.setValorFiltro2(null);
			if (this.txtIntegridad.getValue()!=null && this.txtIntegridad.getValue().length()>0) mapeo.setValorIntegridad(new Double(this.txtIntegridad.getValue())); else mapeo.setValorIntegridad(null);
			if (this.txtConductividad.getValue()!=null && this.txtConductividad.getValue().length()>0) mapeo.setValorConductividad(new Double(this.txtConductividad.getValue())); else mapeo.setValorConductividad(null);
			
			mapeo.setHora(this.txtHoras.getValue());
			mapeo.setRealizado(this.txtRealizado.getValue());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdTurno(this.mapeoProduccion.getIdCodigo());

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
				if (rdo!=null)
				{
					this.mapeoControlFiltracionEnvasadora= new MapeoControlFiltracionEnvasadora();
					this.mapeoControlFiltracionEnvasadora.setIdCodigo(mapeo.getIdCodigo());
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlFiltracionEnvasadora.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
			}
			
			if (rdo!=null)
				Notificaciones.getInstance().mensajeError(rdo);
			else
			{
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", null, null);
				getUI().addWindow(vt);
			}
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	MapeoControlFiltracionEnvasadora mapeoFiltr = new MapeoControlFiltracionEnvasadora();
    	mapeoFiltr.setIdTurno(this.mapeoProduccion.getIdCodigo());
    	mapeoFiltr= this.cncs.datosOpcionesGlobal(mapeoFiltr);
    	
    	if (mapeoFiltr!=null) this.llenarEntradasDatos(mapeoFiltr);
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);
	}

	private void llenarEntradasDatos(MapeoControlFiltracionEnvasadora r_mapeo)
	{
		this.mapeoControlFiltracionEnvasadora= new MapeoControlFiltracionEnvasadora();
		this.mapeoControlFiltracionEnvasadora.setIdCodigo(r_mapeo.getIdCodigo());
		
		setCreacion(false);
		
		this.cmbFiltro1.setValue(r_mapeo.getEstadoFiltro1());
		this.cmbFiltro2.setValue(r_mapeo.getEstadoFiltro2());
		this.cmbIntegridad.setValue(r_mapeo.getEstadoIntegridad());
		this.cmbConductividad.setValue(r_mapeo.getEstadoConductividad());
		this.cmbTipo.setValue(r_mapeo.getTipoLimpieza());
		
		if (r_mapeo.getEstadoFiltro1().contentEquals("KO") && r_mapeo.getValorFiltro1()!=null) this.txtFiltro1.setValue(r_mapeo.getValorFiltro1().toString());
		if (r_mapeo.getEstadoFiltro2().contentEquals("KO") && r_mapeo.getValorFiltro2()!=null) this.txtFiltro2.setValue(r_mapeo.getValorFiltro1().toString());
		if (r_mapeo.getEstadoIntegridad().contentEquals("KO") && r_mapeo.getValorIntegridad()!=null) this.txtIntegridad.setValue(r_mapeo.getValorIntegridad().toString());
		if (r_mapeo.getEstadoConductividad().contentEquals("KO") && r_mapeo.getValorConductividad()!=null) this.txtConductividad.setValue(r_mapeo.getValorConductividad().toString());
		
		this.txtHoras.setValue(r_mapeo.getHora());
		this.txtRealizado.setValue(r_mapeo.getRealizado());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
//    	this.cmbTurno.setEnabled(r_activar);
//    	this.cmbSemana.setEnabled(r_activar);
//    	this.txtEjercicio.setEnabled(r_activar);
//    	this.txtFecha.setEnabled(r_activar);
    	cmbFiltro1.setEnabled(r_activar);
    	cmbFiltro2.setEnabled(r_activar);
    	cmbIntegridad.setEnabled(r_activar);
    	cmbConductividad.setEnabled(r_activar);
    	cmbTipo.setEnabled(r_activar);
    	txtHoras.setEnabled(r_activar);
    	txtRealizado.setEnabled(r_activar);
    	txtObservaciones.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtHoras.setValue("");
		this.txtRealizado.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtRealizado.getValue()==null || this.txtRealizado.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del test");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	if (this.cmbFiltro1.getValue()!=null && this.cmbFiltro1.getValue().toString().contentEquals("KO") && (this.txtFiltro1.getValue()==null || this.txtFiltro1.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de presion del filtro 1");
    		return false;
    	}
    	if (this.cmbFiltro2.getValue()!=null && this.cmbFiltro2.getValue().toString().contentEquals("KO") && (this.txtFiltro2.getValue()==null || this.txtFiltro2.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de presion del filtro 2");
    		return false;
    	}
    	if (this.cmbIntegridad.getValue()!=null && this.cmbIntegridad.getValue().toString().contentEquals("KO") && (this.txtIntegridad.getValue()==null || this.txtIntegridad.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor del test de Integridad");
    		return false;
    	}
    	if (this.cmbConductividad.getValue()!=null && this.cmbConductividad.getValue().toString().contentEquals("KO") && (this.txtConductividad.getValue()==null || this.txtConductividad.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor de la conductividad");
    		return false;
    	}
    	if (this.cmbTipo.getValue()==null || this.cmbTipo.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar el valor del tipo de limpieza");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Si has hecho cambios no se guardarán. Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
    	this.cargarComboTurnos();
    	this.cargarComboSemana();
    	this.cargarComboEstados();
    	this.cargarComboTipo();
    }


    private void cargarComboTipo()
    {
    	this.cmbTipo.addItem("Agua");
    	this.cmbTipo.addItem("Química");
    	this.cmbTipo.addItem("Peracético");
    }
    
    private void cargarComboEstados()
    {
    	this.cmbConductividad.addItem("OK");
    	this.cmbConductividad.addItem("KO");
    	
    	this.cmbIntegridad.addItem("OK");
    	this.cmbIntegridad.addItem("KO");
    	
    	this.cmbFiltro1.addItem("OK");
    	this.cmbFiltro1.addItem("KO");

    	this.cmbFiltro2.addItem("OK");
    	this.cmbFiltro2.addItem("KO");

    }
    
    private void cargarComboSemana()
    {
		if (this.txtEjercicio.getValue()==null ||  (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()==0))
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		int semanas = RutinasFechas.semanasAño(this.txtEjercicio.getValue());
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		this.cmbSemana.setValue(this.mapeoProduccion.getSemana().toString());
    }
   
    private void cargarComboTurnos()
    {
    	this.cmbTurno.addItem("Mañana");
    	this.cmbTurno.addItem("Tarde");
    	this.cmbTurno.addItem("Noche");
    	
    	this.cmbTurno.setValue(this.mapeoProduccion.getTurno().toString());
    	
    }

}