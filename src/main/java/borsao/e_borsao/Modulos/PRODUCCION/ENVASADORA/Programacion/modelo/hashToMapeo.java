package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoProgramacionEnvasadora mapeo = null;
	 
	 public MapeoProgramacionEnvasadora convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoProgramacionEnvasadora();		 
		 if (r_hash.get("id")!=null) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("id")));
		 if (r_hash.get("ejercicio")!=null) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio").replace(".", "")));
		 this.mapeo.setSemana(r_hash.get("semana"));
		 this.mapeo.setDia(r_hash.get("dia"));
		 this.mapeo.setHorario(r_hash.get("horario"));
		 this.mapeo.setTipo(r_hash.get("tipoOperacion"));
		 this.mapeo.setVino(r_hash.get("vino"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFrontales(r_hash.get("frontales"));
		 this.mapeo.setCaja(r_hash.get("caja"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 if (r_hash.get("cantidad")!=null) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad").replace(".", "")));
		 if (r_hash.get("cajas")!=null) this.mapeo.setCajas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cajas").replace(".", "")));
		 if (r_hash.get("factor")!=null) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor").replace(".", "")));
		 if (r_hash.get("unidades")!=null) this.mapeo.setUnidades(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades").replace(".", "")));
		 if (r_hash.get("status")!=null) this.mapeo.setStatus(RutinasNumericas.formatearIntegerDeESP(r_hash.get("status")));
		 if (r_hash.get("orden")!=null) this.mapeo.setOrden(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden").replace(".", "")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoProgramacionEnvasadora convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoProgramacionEnvasadora();		 
		 if (r_hash.get("id")!=null) this.mapeo.setIdProgramacion(RutinasNumericas.formatearIntegerDeESP(r_hash.get("id")));
		 if (r_hash.get("ejercicio")!=null) this.mapeo.setEjercicio(RutinasNumericas.formatearIntegerDeESP(r_hash.get("ejercicio").replace(".", "")));
		 this.mapeo.setSemana(r_hash.get("semana"));
		 this.mapeo.setDia(r_hash.get("dia"));
		 this.mapeo.setHorario(r_hash.get("horario"));
		 this.mapeo.setTipo(r_hash.get("tipoOperacion"));
		 this.mapeo.setVino(r_hash.get("vino"));
		 this.mapeo.setLote(r_hash.get("lote"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setFrontales(r_hash.get("frontales"));
		 this.mapeo.setCaja(r_hash.get("caja"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 if (r_hash.get("cantidad")!=null) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad").replace(".", "")));
		 if (r_hash.get("cajas")!=null) this.mapeo.setCajas(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cajas").replace(".", "")));
		 if (r_hash.get("factor")!=null) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor").replace(".", "")));
		 if (r_hash.get("unidades")!=null) this.mapeo.setUnidades(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades").replace(".", "")));
		 if (r_hash.get("status")!=null) this.mapeo.setStatus(RutinasNumericas.formatearIntegerDeESP(r_hash.get("status").replace(".", "")));
		 if (r_hash.get("orden")!=null) this.mapeo.setOrden(RutinasNumericas.formatearIntegerDeESP(r_hash.get("orden")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoProgramacionEnvasadora r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 
		 this.hash.put("id", this.mapeo.getIdProgramacion().toString());		 
		 this.hash.put("ejercicio", this.mapeo.getEjercicio().toString());
		 this.hash.put("semana", "Semana " + this.mapeo.getSemana());
		 this.hash.put("dia", this.mapeo.getDia());
		 this.hash.put("horario", this.mapeo.getHorario());
		 this.hash.put("tipoOperacion", this.mapeo.getTipo());
		 this.hash.put("vino", this.mapeo.getVino());
		 this.hash.put("lote", this.mapeo.getLote());
		 this.hash.put("articulo", this.mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 this.hash.put("frontales", this.mapeo.getFrontales());
		 this.hash.put("caja", this.mapeo.getCaja());
		 this.hash.put("estado", this.mapeo.getEstado());
		 
		 this.hash.put("cantidad", this.mapeo.getCantidad().toString());
		 this.hash.put("cajas", this.mapeo.getCajas().toString());
		 this.hash.put("factor", this.mapeo.getFactor().toString());
		 this.hash.put("unidades", this.mapeo.getUnidades().toString());
		 this.hash.put("status", this.mapeo.getStatus().toString());
		 this.hash.put("orden", this.mapeo.getOrden().toString());
		 
		 return hash;		 
	 }
}