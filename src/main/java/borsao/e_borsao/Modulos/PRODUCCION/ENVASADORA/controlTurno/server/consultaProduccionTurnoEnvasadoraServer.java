package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlFiltracionEnvasadora.server.consultaFiltracionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlPreoperativoEnvasadora.server.consultaPreoperativoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoProduccionTurnoEnvasadora;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaProduccionTurnoEnvasadoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaProduccionTurnoEnvasadoraServer instance;
	
	public consultaProduccionTurnoEnvasadoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProduccionTurnoEnvasadoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProduccionTurnoEnvasadoraServer(r_usuario);			
		}
		return instance;
	}

	public MapeoProduccionTurnoEnvasadora datosOpcionesGlobal(MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		MapeoProduccionTurnoEnvasadora mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_produccion_env.idCodigo prd_tur_id, ");
		cadenaSQL.append(" prd_produccion_env.ejercicio prd_tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana prd_tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno prd_tur_tur, ");
		cadenaSQL.append(" prd_produccion_env.fecha prd_tur_fec, ");
		cadenaSQL.append(" prd_produccion_env.buenas prd_tur_bue, ");
		cadenaSQL.append(" prd_produccion_env.malas prd_tur_mal ");
     	cadenaSQL.append(" FROM prd_produccion_env ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + r_mapeo.getSemana());
				}
				if (r_mapeo.getTurno()!=null && r_mapeo.getTurno().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					
					if (r_mapeo.getTurno().contentEquals("Mañana")) cadenaWhere.append(" turno = 'M' " );
					else if (r_mapeo.getTurno().contentEquals("Tarde")) cadenaWhere.append(" turno = 'T' " );
					else if (r_mapeo.getTurno().contentEquals("Noche")) cadenaWhere.append(" turno = 'N' " );
				}
				if (r_mapeo.getFecha()!=null)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())) + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, fecha, turno asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoProduccionTurnoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("prd_tur_id"));
				mapeo.setEjercicio(rsOpcion.getInt("prd_tur_eje"));
				mapeo.setSemana(rsOpcion.getInt("prd_tur_sem"));
				mapeo.setFecha(rsOpcion.getDate("prd_tur_fec"));
				mapeo.setTurno(rsOpcion.getString("prd_tur_tur"));
				mapeo.setBuenas(rsOpcion.getDouble("prd_tur_bue"));
				mapeo.setMalas(rsOpcion.getDouble("prd_tur_mal"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	public MapeoProduccionTurnoEnvasadora obtenerTurno(Integer r_id)
	{
		MapeoProduccionTurnoEnvasadora mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_produccion_env.idCodigo prd_tur_id, ");
		cadenaSQL.append(" prd_produccion_env.ejercicio prd_tur_eje, ");
		cadenaSQL.append(" prd_produccion_env.semana prd_tur_sem, ");
		cadenaSQL.append(" prd_produccion_env.turno prd_tur_tur, ");
		cadenaSQL.append(" prd_produccion_env.fecha prd_tur_fec, ");
		cadenaSQL.append(" prd_produccion_env.buenas prd_tur_bue, ");
		cadenaSQL.append(" prd_produccion_env.malas prd_tur_mal ");
     	cadenaSQL.append(" FROM prd_produccion_env ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_id!=null )
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idCodigo= " + r_id);
			}

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, fecha, turno asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoProduccionTurnoEnvasadora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("prd_tur_id"));
				mapeo.setEjercicio(rsOpcion.getInt("prd_tur_eje"));
				mapeo.setSemana(rsOpcion.getInt("prd_tur_sem"));
				mapeo.setFecha(rsOpcion.getDate("prd_tur_fec"));
				mapeo.setTurno(rsOpcion.getString("prd_tur_tur"));
				mapeo.setBuenas(rsOpcion.getDouble("prd_tur_bue"));
				mapeo.setMalas(rsOpcion.getDouble("prd_tur_mal"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	
	public String guardarNuevo(MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_produccion_env ( ");
			cadenaSQL.append(" prd_produccion_env.idCodigo, ");
			cadenaSQL.append(" prd_produccion_env.ejercicio, ");
			cadenaSQL.append(" prd_produccion_env.semana, ");
			cadenaSQL.append(" prd_produccion_env.turno, ");
			cadenaSQL.append(" prd_produccion_env.fecha, ");
			cadenaSQL.append(" prd_produccion_env.buenas, ");
			cadenaSQL.append(" prd_produccion_env.malas) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_produccion_env", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	String turno = null;
		    	if (r_mapeo.getTurno().contentEquals("Mañana")) turno="M";
		    	else if (r_mapeo.getTurno().contentEquals("Tarde")) turno="T";
		    	else if (r_mapeo.getTurno().contentEquals("Noche")) turno="N";
		    	
		    	preparedStatement.setString(4, turno);
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }

		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(5, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getBuenas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getBuenas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getMalas()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getMalas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_produccion_env SET ");
			cadenaSQL.append(" prd_produccion_env.ejercicio=?, ");
			cadenaSQL.append(" prd_produccion_env.semana=?, ");
			cadenaSQL.append(" prd_produccion_env.turno=?, ");
			cadenaSQL.append(" prd_produccion_env.fecha=?, ");
    		cadenaSQL.append(" prd_produccion_env.buenas=?, ");
    		cadenaSQL.append(" prd_produccion_env.malas=? ");
	        cadenaSQL.append(" WHERE prd_produccion_env.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	
		    	String turno = null;
		    	if (r_mapeo.getTurno().contentEquals("Mañana")) turno="M";
		    	else if (r_mapeo.getTurno().contentEquals("Tarde")) turno="T";
		    	else if (r_mapeo.getTurno().contentEquals("Noche")) turno="N";
		    	
		    	preparedStatement.setString(3, turno);
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getBuenas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getBuenas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getMalas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getMalas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoProduccionTurnoEnvasadora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_produccion_env ");            
			cadenaSQL.append(" WHERE prd_produccion_env.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	public boolean comprobarPreoperativo(Date r_fecha)
	{
		boolean hayPreoperativo = false;
		consultaPreoperativoEnvasadoraServer cps = new consultaPreoperativoEnvasadoraServer(CurrentUser.get());
		hayPreoperativo=cps.comprobarPreoperativo(r_fecha);
		return hayPreoperativo;
	}
	
	public boolean comprobarFiltracion(Date r_fecha)
	{
		boolean hayFiltracion = false;
		consultaFiltracionEnvasadoraServer cfs = new consultaFiltracionEnvasadoraServer(CurrentUser.get());
		hayFiltracion = cfs.comprobarFiltracion(r_fecha);
		return hayFiltracion;
	}
	
	
	@Override
	public String semaforos() {
		return null;
	}
}