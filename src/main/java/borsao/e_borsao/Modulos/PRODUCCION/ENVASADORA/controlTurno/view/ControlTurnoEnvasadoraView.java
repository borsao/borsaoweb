package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.MapeoControlTurnoEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.controlTurno.server.consultaControlTurnoEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.DesgloseTiemposEnvasadoraGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.MapeoTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.server.consultaTiemposEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ControlTurnoEnvasadoraView extends GridViewRefresh {

	public static final String VIEW_NAME = "Controles ControlTurno Envasadora";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;

	private Button opcSubir = null;
	private Button opcBajar = null;
	
	public TextField txtEjercicio= null;
	public ComboBox cmbSemana = null;
	public ComboBox cmbDia = null;
	public String semana = null;
	private HashMap<String,String> filtrosRec = null;
	private boolean hayGridParadas = false;
	private DesgloseTiemposEnvasadoraGrid gridParadas = null; 
	
	public void generarGrid(HashMap<String, String> opcionesEscogidas) 
	{
		
    	ArrayList<MapeoControlTurnoEnvasadora> r_vector=null;
    	consultaControlTurnoEnvasadoraServer cfs = new consultaControlTurnoEnvasadoraServer(CurrentUser.get());
    	
    	Integer ejercicio = new  Integer(this.txtEjercicio.getValue());
    	Integer semana = new  Integer(this.cmbSemana.getValue().toString());

    	r_vector = cfs.datosOpcionesGlobal(ejercicio,semana);
    	
    	grid = new OpcionesGrid(this, r_vector);
    	
    	if (((OpcionesGrid) grid).vector==null)
    	{    	
			setHayGrid(false);
    	}
    	else if (((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(true);
    		((OpcionesGrid) grid).establecerFiltros(filtrosRec);
    		if (grid.getContainerDataSource().size()!=0) grid.select(grid.getContainerDataSource().getIdByIndex(0));
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ControlTurnoEnvasadoraView() 
    {
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);

//    	this.cabLayout.addStyleName("v-panelTitulo");
    	
    	this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setEnabled(true);
		this.txtEjercicio.setWidth("125px");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		
		this.cmbSemana= new ComboBox("Semana");    	
		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setWidth("125px");
		this.cmbSemana.setNullSelectionAllowed(false);

		this.cmbDia= new ComboBox("Dia");    	
		this.cmbDia.addStyleName(ValoTheme.COMBOBOX_TINY);
		this.cmbDia.setNewItemsAllowed(false);
		this.cmbDia.setWidth("125px");
		this.cmbDia.setNullSelectionAllowed(true);
		
		this.opcSubir= new Button();    	
		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
		this.opcSubir.setDescription("Semana posterior");
		
		this.opcBajar= new Button();    	
		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		this.opcBajar.setDescription("Semana anterior");
		
    	this.cargarCombo(null);
    	this.cmbSemana.setValue(this.semana);
    	
    	this.cabLayout.addComponent(this.txtEjercicio);
    	this.cabLayout.addComponent(this.opcBajar);
		this.cabLayout.addComponent(this.cmbSemana);
		this.cabLayout.addComponent(this.opcSubir);
		this.cabLayout.addComponent(this.cmbDia);
		this.cabLayout.setComponentAlignment(this.opcBajar, Alignment.BOTTOM_LEFT);
		this.cabLayout.setComponentAlignment(this.opcSubir, Alignment.BOTTOM_LEFT);

		this.cargarListeners();
		
		this.opcBuscar.setVisible(false);
		this.opcNuevo.setVisible(false);
		
		this.generarGrid(null);
//	
    }

    private void cargarListeners()
    {
    	this.opcSubir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week += 1;
				
				if (week>53)
				{
					year = year + 1;
					week = 1;
					
					txtEjercicio.setValue(String.valueOf(year));
				}
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				
				if (isHayGridParadas())
				{			
					gridParadas.removeAllColumns();			
					barAndGridLayout.removeComponent(gridParadas);
					gridParadas=null;
					setHayGridParadas(false);
				}

	    		if (cmbDia.getValue()!=null)
	    		{
	    			filtrosRec.remove("dia");
	    			filtrosRec.put("dia", cmbDia.getValue().toString());
	    		}
				generarGrid(null);

			}
		});
    	
    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				int week = new Integer(cmbSemana.getValue().toString()).intValue();
				int year = new Integer(txtEjercicio.getValue()).intValue();
				
				week -= 1;
				
				if (week<1)
				{
					year = year - 1;
					week = 53;
					int semanas = RutinasFechas.semanasAño(String.valueOf(year));
					if (week<=semanas && cmbSemana.getItem(String.valueOf(week))==null)
					{
						cmbSemana.addItem(String.valueOf(week));
					}
					
				}
				
				txtEjercicio.setValue(String.valueOf(year));
				cmbSemana.setValue(String.valueOf(week));
				
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				
				if (isHayGridParadas())
				{			
					gridParadas.removeAllColumns();			
					barAndGridLayout.removeComponent(gridParadas);
					gridParadas=null;
					setHayGridParadas(false);
				}

	    		if (cmbDia.getValue()!=null)
	    		{
	    			filtrosRec.remove("dia");
	    			filtrosRec.put("dia", cmbDia.getValue().toString());
	    		}
				generarGrid(null);

			}
		});

		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				
				if (isHayGridParadas())
				{			
					gridParadas.removeAllColumns();			
					barAndGridLayout.removeComponent(gridParadas);
					gridParadas=null;
					setHayGridParadas(false);
				}

	    		if (cmbDia.getValue()!=null)
	    		{
	    			filtrosRec.remove("dia");
	    			filtrosRec.put("dia", cmbDia.getValue().toString());
	    		}
				generarGrid(null);
			}
		});
		
		this.cmbDia.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((OpcionesGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;
					setHayGrid(false);
				}
				if (isHayGridParadas())
				{			
					gridParadas.removeAllColumns();			
					barAndGridLayout.removeComponent(gridParadas);
					gridParadas=null;
					setHayGridParadas(false);
				}

	    		if (cmbDia.getValue()!=null)
	    		{
	    			filtrosRec.remove("dia");
	    			filtrosRec.put("dia", cmbDia.getValue().toString());
	    		}
	    		else
	    		{
	    			filtrosRec.remove("dia");
	    		}
				generarGrid(null);
			}
		});

    }
    
    public void newForm()
    {

    }
    
    public void verForm(boolean r_busqueda)
    {    	

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * mostraremos el grid con los tiempos de parada indicados por produccion
    	 */
    	
		if (isHayGridParadas())
		{			
			gridParadas.removeAllColumns();			
			barAndGridLayout.removeComponent(gridParadas);
			gridParadas=null;
			setHayGridParadas(false);
		}
		
    	consultaTiemposEnvasadoraServer cncs = new consultaTiemposEnvasadoraServer(CurrentUser.get());
    	MapeoTiemposEnvasadora mapeo = new MapeoTiemposEnvasadora();
    	MapeoControlTurnoEnvasadora mapeoControl = (MapeoControlTurnoEnvasadora) r_fila;
    	
    	mapeo.setFecha(mapeoControl.getFecha());
    	mapeo.setEjercicio(mapeoControl.getMapeo().getEjercicio());
    	mapeo.setTurno(mapeoControl.getTurno());
    	/*
    	 * una vez generado el grid
    	 */
    	ArrayList<MapeoTiemposEnvasadora>r_vector=cncs.datosOpcionesGlobal(mapeo);

    	gridParadas = new DesgloseTiemposEnvasadoraGrid(this, r_vector);
    	setHayGridParadas(true);
    	barAndGridLayout.addComponent(gridParadas);
    	barAndGridLayout.setExpandRatio(grid, 1);
    	barAndGridLayout.setExpandRatio(gridParadas, 1);
    	/*
    	 * 	
    	 *	barAndGridLayout.setExpandRatio(grid2, 1);
    	 *
    	 */
    }

    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(ControlTurnoEnvasadoraView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(ControlTurnoEnvasadoraView.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {

	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private void cargarCombo(String r_ejercicio)
	{
		if (r_ejercicio==null)
		{
			this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
		}
		else
		{
			this.cmbSemana.removeAllItems();
		}

		this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
		int semanas = RutinasFechas.semanasAño(r_ejercicio);
		
		for (int i=1; i<=semanas; i++)
		{
			this.cmbSemana.addItem(String.valueOf(i));
		}
		
		this.cmbDia.addItem("Lunes");
		this.cmbDia.addItem("Martes");
		this.cmbDia.addItem("Miercoles");
		this.cmbDia.addItem("Jueves");
		this.cmbDia.addItem("Viernes");
		this.cmbDia.addItem("Sabado");
	}

	public boolean isHayGridParadas() {
		return hayGridParadas;
	}

	public void setHayGridParadas(boolean hayGridParadas) {
		this.hayGridParadas = hayGridParadas;
	}


}
