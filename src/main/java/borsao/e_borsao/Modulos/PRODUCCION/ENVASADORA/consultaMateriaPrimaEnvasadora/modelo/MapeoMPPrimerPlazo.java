package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoMPPrimerPlazo extends MapeoGlobal
{
	private Date primerPlazo = null;
	private String nombre = null;

	public MapeoMPPrimerPlazo()
	{
	}

	public Date getPrimerPlazo() {
		return primerPlazo;
	}

	public void setPrimerPlazo(Date primerPlazo) {
		this.primerPlazo = primerPlazo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String r_nombre) {
		this.nombre= r_nombre;
	}

}