package borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo.MapeoSituacionMPEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.consultaMateriaPrimaEnvasadora.server.consultaSituacionMPEnvasadoraServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class consultaMateriaPrimaEnvasadoraView extends GridViewRefresh {

	public static final String VIEW_NAME = "Situacion Materias Primas Envasadora";
	private final String titulo = "Situacion Materias Primas Envasadora";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean generado = false;
	
	/*
	 * TODOH. Calcular en funcion del escandallo el pendiente de servir de las materias primas 
	 * asi tendremos un calculo de necesidades
	 * 
	 * 
	 */
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoSituacionMPEnvasadora> r_vector=null;
    	consultaSituacionMPEnvasadoraServer cse = new consultaSituacionMPEnvasadoraServer(CurrentUser.get());
    	r_vector=cse.datosOpcionesGlobal();
    	
    	grid = new OpcionesGrid(r_vector);
    	if (((OpcionesGrid) grid).vector==null && ((OpcionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(false);
    	}
    	else
    	{
    		generado=true;
    		setHayGrid(true);
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public consultaMateriaPrimaEnvasadoraView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	this.generarGrid(null);
    	this.barAndGridLayout.removeComponent(this.cabLayout);
    	if (isHayGrid()) this.opcImprimir.setVisible(true);
    }

    public void newForm()
    {
    }
    
    public void verForm(boolean r_busqueda)
    {    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    @Override
    public void reestablecerPantalla() {
    	this.opcImprimir.setEnabled(true);
    }
    public void print() {
    	((OpcionesGrid) this.grid).generacionPdf(generado,true);
    	this.regresarDesdeForm();
    	generado=false;

    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
