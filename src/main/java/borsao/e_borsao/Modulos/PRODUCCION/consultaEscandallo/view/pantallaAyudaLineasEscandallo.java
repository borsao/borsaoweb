package borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.AyudaEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.AyudaLineasEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAyudaLineasEscandallo extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridPadre= null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	private boolean hayGridPadre = false;
	private boolean hayGridComponentes= false;
	private VerticalLayout frameCentral = null;	
	private String articuloConsultado = null;
	
	public pantallaAyudaLineasEscandallo(String r_titulo, String r_articulo)
	{
		
		this.setCaption(r_titulo);
		this.articuloConsultado=r_articulo;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("750px");
		
		this.cargarPantalla(r_articulo);
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);

	}

	private void cargarPantalla(String r_articulo)
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

				HorizontalLayout frameCabecera = new HorizontalLayout();
				frameCabecera.setHeight("1%");
				
				this.frameCentral = new VerticalLayout();
		
				HorizontalLayout framePie = new HorizontalLayout();
		
					btnBotonCVentana = new Button("Cerrar");
					btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
					framePie.addComponent(btnBotonCVentana);		
					framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	ArrayList<MapeoLineasEscandallo> r_vector=null;
    	ArrayList<MapeoLineasEscandallo> r_vector2 =null;
    	
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	
    	r_vector=ces.recuperarEscandallo(this.articuloConsultado.trim().substring(0, 7));
    	r_vector2=ces.recuperarModificacionesEscandallo(this.articuloConsultado.trim().substring(0, 7));
    	
    	this.presentarGrid(r_vector, r_vector2);

	}

    private void presentarGrid(ArrayList<MapeoLineasEscandallo> r_vector, ArrayList<MapeoLineasEscandallo> r_vector2)
    {
    	
    	gridPadre = new AyudaEscandalloGrid(r_vector);
    	
    	if (((AyudaEscandalloGrid) gridPadre).vector==null)
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		setHayGridPadre(true);
    	}
    	
    	if (isHayGridPadre())
    	{
    		
    		gridComponentes = new AyudaLineasEscandalloGrid(r_vector2);
    		if (((AyudaLineasEscandalloGrid) gridComponentes).vector==null)
        	{
        		setHayGridComponentes(false);
        	}
        	else
        	{
        		setHayGridComponentes(true);
        	}
    		
    		if (isHayGridComponentes())
    		{
    			this.gridPadre.setHeight("100px");
    			this.gridPadre.setWidth("100%");
    			this.frameCentral.addComponent(gridPadre);
    			this.gridComponentes.setHeight("400px");
    			this.gridComponentes.setWidth("100%");
    			this.frameCentral.addComponent(gridComponentes);
    			
    			this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    			this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
    		}
    	}
    }

	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
}