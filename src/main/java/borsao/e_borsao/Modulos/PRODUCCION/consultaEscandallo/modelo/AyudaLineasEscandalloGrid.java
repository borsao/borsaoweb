package borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaLineasEscandalloGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public AyudaLineasEscandalloGrid(ArrayList<MapeoLineasEscandallo> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoLineasEscandallo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	
    	setColumnOrder("articulo", "descripcion", "fechaModificacion", "dias");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("fechaModificacion").setHeaderCaption("Fecha Modificacion");
    	this.getColumn("fechaModificacion").setSortable(false);
    	this.getColumn("fechaModificacion").setWidth(150);
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("dias").setHidden(true);
    	this.getColumn("fechaModificacion").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("dias".equals(cellReference.getPropertyId())) 
            	{
            		/*
            		 * Calculo los dias de cambio hasta la actualidad
            		 * lo marco en ambar o rojo o normal
            		 */
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}


