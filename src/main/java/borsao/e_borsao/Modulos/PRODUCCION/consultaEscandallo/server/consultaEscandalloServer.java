package borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaEscandalloServer 
{
	private connectionManager conManager = null;
	private Notificaciones serNotif = null;
	private static consultaEscandalloServer instance;

	public consultaEscandalloServer(String r_usuario) 
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}

	public static consultaEscandalloServer getInstance(String r_usuario) {
		if (instance == null) {
			instance = new consultaEscandalloServer(r_usuario);
		}
		return instance;
	}
	
	public String recuperarDescripcion(String r_articulo, String r_sufijo)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;

		Connection con = null;
		Statement cs = null;		
		String sql = null;
		try
		{
			sql="select descripcion des from e__lconj where padre = '" + r_articulo.trim() + r_sufijo + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getString("des");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}

	public boolean comrprobarEscandallo(String r_articulo)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;
		
		Connection con = null;
		Statement cs = null;		
		String sql = null;
		try
		{
			sql="select descripcion des from e__lconj where padre = '" + r_articulo.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return false;
	}

	public ArrayList<String> recuperarPadres(String r_hijo)
	{
		ResultSet rsArticulos = null;
		ArrayList<String> r_vector = null;

		Connection con = null;
		Statement cs = null;
		String sql = null;
		
		try
		{
			
			sql="select padre from e__lconj ";
			
			sql = sql + " where hijo = '" + r_hijo.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			r_vector = new ArrayList<String>();
			
			while(rsArticulos.next())
			{
				r_vector.add(rsArticulos.getString("padre").trim());
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	public ArrayList<MapeoArticulos> recuperarMapeoPadres(String r_hijo)
	{
		ResultSet rsArticulos = null;
		ArrayList<MapeoArticulos> r_vector = null;
		
		Connection con = null;
		Statement cs = null;
		String sql = null;
		
		try
		{
			
			sql="select padre, e_articu.descrip_articulo from e__lconj, e_articu ";
			
			sql = sql + " where e__lconj.padre = e_articu.articulo ";
			sql = sql + " and hijo = '" + r_hijo.trim() + "'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			System.out.println(sql.toString());
			r_vector = new ArrayList<MapeoArticulos>();
			
			while(rsArticulos.next())
			{
				MapeoArticulos map = new MapeoArticulos();
				map.setArticulo(rsArticulos.getString("padre").trim());
				map.setDescripcion(rsArticulos.getString("descrip_articulo").trim());
				r_vector.add(map);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	public ArrayList<MapeoArticulos> recuperarPadresDesc(String r_hijo)
	{
		ResultSet rsArticulos = null;

		Connection con = null;
		Statement cs = null;
		
		ArrayList<MapeoArticulos> r_vector = null;
		MapeoArticulos mapeo = null;
		String sql = null;
		
		try
		{
			
			sql="select distinct e__lconj.padre[1,7], e_articu.descrip_articulo from e__lconj,e_articu  where e__lconj.padre[1,7] = e_articu.articulo ";
			
			if (r_hijo.length()==7) sql = sql + " and e__lconj.hijo = '" + r_hijo.trim() + "'"; 
			else if (r_hijo.startsWith("0111") || r_hijo.startsWith("0102") || r_hijo.startsWith("0103") || r_hijo.startsWith("0101"))
				sql = sql + " and e__lconj.padre matches '" + r_hijo + "*'";
			else
				sql = sql + " and e__lconj.descripcion matches '*" + r_hijo.trim() + "*'";
			
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento(sql.toString());
			rsArticulos= cs.executeQuery(sql);
			
			r_vector = new ArrayList<MapeoArticulos>();
			
			while(rsArticulos.next())
			{
				mapeo= new MapeoArticulos();
				mapeo.setArticulo(rsArticulos.getString("padre").trim());
				mapeo.setDescripcion(RutinasCadenas.conversion(rsArticulos.getString("descrip_articulo")));
				r_vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	public ArrayList<MapeoArticulos> recuperarPadresDescPS(String r_hijo)
	{
		ResultSet rsArticulos = null;

		Connection con = null;
		Statement cs = null;
		ArrayList<MapeoArticulos> r_vector = null;
		MapeoArticulos mapeo = null;
		String sql = null;
		
		try
		{
			
			sql="select distinct e__lconj.padre[1,7], e_articu.descrip_articulo from e__lconj,e_articu  where e__lconj.padre[1,7] = e_articu.articulo ";
			
			if (r_hijo.length()==7) sql = sql + " and e__lconj.hijo = '" + r_hijo.trim() + "'"; 
			else if (r_hijo.startsWith("0111") || r_hijo.startsWith("0102") || r_hijo.startsWith("0103") || r_hijo.startsWith("0101"))
				sql = sql + " and e__lconj.padre matches '" + r_hijo + "*'";
			else
				sql = sql + " and e_articu.descrip_articulo matches '*" + r_hijo + "*'";
			
			sql = sql + " and e_articu.tipo_articulo ='PS'";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento(sql.toString());
			rsArticulos= cs.executeQuery(sql);
			
			r_vector = new ArrayList<MapeoArticulos>();
			
			while(rsArticulos.next())
			{
				mapeo= new MapeoArticulos();
				mapeo.setArticulo(rsArticulos.getString("padre").trim());
				mapeo.setDescripcion(RutinasCadenas.conversion(rsArticulos.getString("descrip_articulo")));
				r_vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	
	public ArrayList<String> recuperarPadres(Connection r_con, String r_hijo)
	{
		ResultSet rsArticulos = null;
		ArrayList<String> r_vector = null;

		Statement cs = null;
		String sql = null;
		
		try
		{
			
			sql="select padre from e__lconj ";
			
			sql = sql + " where hijo = '" + r_hijo.trim() + "'";
			
			cs = r_con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			r_vector = new ArrayList<String>();
			
			while(rsArticulos.next())
			{
				r_vector.add(rsArticulos.getString("padre").trim());
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return r_vector;
	}
	
	public ArrayList<String> recuperarHijos(String r_padre)
	{
		ResultSet rsArticulos = null;
		ArrayList<String> r_vector = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		
		try
		{
			
			sql="select hijo,descripcion from e__lconj ";
			
			sql = sql + " where padre = '" + r_padre.trim() + "' order by hijo ";
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			
			r_vector = new ArrayList<String>();
			
			while(rsArticulos.next())
			{
				r_vector.add(rsArticulos.getString("hijo").trim() + " " + RutinasCadenas.conversion(rsArticulos.getString("descripcion").trim()));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
	
	public String recuperarComponente(String r_articulo, String r_mascara)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			if (r_articulo==null && r_mascara==null)
			{
				return null;
			}
			
			sql="select hijo from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre matches '" + r_articulo.trim() + "'";
				if (r_mascara!=null)
				{
					sql = sql + "and hijo matches '" + r_mascara.trim() + "*'";
				}
			}
			else
			{
				if (r_mascara!=null)
				{
					sql = sql + "where hijo matches '" + r_mascara.trim() + "*'";
				}
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getString("hijo").trim();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			serNotif.mensajeSeguimiento("Recuperar Componente 1.- Padre: " + r_articulo + "Hijo: " + r_mascara);
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
				serNotif.mensajeSeguimiento("RecuperarComponente 2.-  Padre: " + r_articulo + "Hijo: " + r_mascara);
			}
		}
		return rdo_obtenido;
	}

	public String recuperarComponente(String r_articulo, String r_mascara, String r_desc)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			
			if (r_articulo==null && r_mascara==null)
			{
				return null;
			}
			
			sql="select hijo from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre matches '" + r_articulo.trim() + "'";
				if (r_mascara!=null)
				{
					sql = sql + "and hijo matches '" + r_mascara.trim() + "*'";
				}
				if (r_desc!=null)
				{
					sql = sql + "and descripcion matches '*" + r_desc.trim() + "*'";
				}
			}
			else
			{
				if (r_mascara!=null)
				{
					sql = sql + "where hijo matches '" + r_mascara.trim() + "*'";
				}
				if (r_desc!=null)
				{
					sql = sql + "and descripcion matches '*" + r_desc.trim() + "*'";
				}
			}
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getString("hijo").trim();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			serNotif.mensajeSeguimiento("Recuperar Componente 1.- Padre: " + r_articulo + "Hijo: " + r_mascara);
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
				serNotif.mensajeSeguimiento("RecuperarComponente 2.-  Padre: " + r_articulo + "Hijo: " + r_mascara);
			}
		}
		return rdo_obtenido;
	}
	
	public String recuperarComponente(Connection r_con, String r_articulo, String r_mascara)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			if (r_articulo==null && r_mascara==null)
			{
				return null;
			}
			
			sql="select hijo from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre matches '" + r_articulo.trim() + "*'";
				if (r_mascara!=null)
				{
					sql = sql + "and hijo matches '" + r_mascara.trim() + "*'";
				}
			}
			else
			{
				if (r_mascara!=null)
				{
					sql = sql + "where hijo matches '" + r_mascara.trim() + "*'";
				}
			}
			
//			con= this.conManager.establecerConexionGREENsys();			
			cs = r_con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getString("hijo").trim();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	private String recuperarEan(String r_articulo, String r_mascara)
	{
		String rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select unidades from e_art_cb where articulo = '" + r_articulo.trim() + "' and cod_barra_art matches '" + r_mascara + "*'" ;
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getString("unidades").trim();
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public Double recuperarCantidadComponente(String r_padre, String r_hijo)
	{
		Double rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select cantidad from e__lconj where padre = '" + r_padre.trim() + "' and hijo = '" + r_hijo.trim() + "'" ;
			
			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			rdo_obtenido=new Double(0);
			
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getDouble("cantidad");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public Double recuperarCantidadComponente(Connection r_con, String r_hijo)
	{
		Double rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Statement cs = null;

		String sql = null;
		try
		{
			sql="select cantidad from e__lconj where hijo = '" + r_hijo.trim() + "'" ;
			 
			cs = r_con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getDouble("cantidad");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public Double recuperarCantidadComponenteMascara(String r_padre, String r_hijo_mascara)
	{
		Double rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select cantidad from e__lconj where padre = '" + r_padre.trim() + "' and hijo matches '" + r_hijo_mascara.trim() + "*'" ;
			
			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				rdo_obtenido = rsArticulos.getDouble("cantidad");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}


	/*
	 * Metodo que devuelve el tipo de botella
	 * 
	 * 0 - bordelesa
	 * 1 - borgoña
	 * 2 - bag in box
	 * 
	 */
	
	public Integer recogerTipoBotella(String r_padre)
	{
		String articuloBotella=null;
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 0104*
		 */
		articuloBotella = this.recuperarComponente(r_padre, "0104");
				
		if (articuloBotella!=null)
		{
			switch (articuloBotella.substring(0,5))
			{
				case "01040": // bordelesa
					return 0;
				case "01041": // borgoña
					if (articuloBotella.substring(0, 7).equals("0104105")) return 3;
					else return 1;
				case "01042": // bordelesa troncoconica
					return 1;
				case "01044": // bolsa bag in box					
					return 2;				
				case "01043": // envase pet
					if (articuloBotella.equals("0104301")) 
					{
						return 0; 
					}
					else
					{
						return 1;
					}
				default:
					return null;
			}
		}
		return null;
	}

	/*
	 * Metodo que devuelve el tipo de palet
	 * 
	 * 0 - 80*120
	 * 1 - 100*120
	 * 2 - 1/2 paleta
	 */
	
	public Integer recogerTipoPalet(String r_padre)
	{
		String articuloPalet=null;
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 01193*
		 */
		
		articuloPalet= this.recuperarComponente(r_padre, "01193");

		if (articuloPalet!=null)
		{
			
			switch (articuloPalet)
			{
				case "0119337":					
					return 2;
				case "0119338":					
					return 0;
				case "0119339":					
					return 2;
				case "0119340":
					return 0;
				case "0119341":
					return 1;
				case "0119343":
					return 1;
				case "0119344":
					return 1;
				case "0119345":
					return 0;
				case "0119360":
					return 0;
				default:
					return null;
			}
		}
		return null;
	}
	
	/*
	 * Metodo que devuelve el tipo de palet
	 * 
	 * 83 - medias paletas 
	 * 6 - caja de 6
	 * 12 - caja de 12
	 * 4 caja de 4
	 * 1 caja de 1
	 * 0 - horizontal 6 bot
	 */
	
	public Integer recogerTipoCaja(String r_padre)
	{
		Double cantidadEscandallo=null;
		String articuloCaja=null;
		Integer rdo = null;
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 0109*
		 */
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		String tipoArticulo = cas.obtenerTipoArticulo(r_padre.substring(0,7));
		
		if (tipoArticulo!=null && tipoArticulo.equals("PS"))
		{
			rdo = recogerCantidadJaulon(r_padre, "18");
			if (rdo==null) rdo = recogerCantidadJaulon(r_padre, "58");
			return rdo;
		}
		
		articuloCaja = this.recuperarComponente(r_padre, "0109");
		
		if (articuloCaja==null)
		{
			rdo = recogerCantidadJaulon(r_padre, "18");
			if (rdo==null) rdo = recogerCantidadJaulon(r_padre, "58");
			return rdo;
		}
		
		cantidadEscandallo = this.recuperarCantidadComponente(r_padre, articuloCaja);
				
		if ((articuloCaja !=null) && (articuloCaja=="0109500" || articuloCaja=="0109501" || articuloCaja=="0109502" ||articuloCaja=="0109503" || articuloCaja=="0109600" || articuloCaja=="0109690" || articuloCaja=="0109750" || articuloCaja=="0109751"))
		{
			return 0;
		}
		
		if (cantidadEscandallo!=null)
		{
			if (r_padre.contentEquals("0102006")) return 280;
			switch (cantidadEscandallo.toString())
			{

				case "0.028":// 1/2 paleta envasadora 2lt
					return 36; 
				case "0.067":// 1/2 paleta envasadora 5lt
					return 15;
				case "0.014": // 1/2 paleta embotellado
					return 70*4; 
				case "0.012": // 1/2 paleta embotellado
					return 84*3; 
				case "0.083": // caja de 12
					return 12;
				case "0.021": // bandejas 1/2 2L
					return 48;					
				case "0.05": // bandejas 1/2 5L
					return 20;
				case "0.006": // cantoneras y separadores de carton
					return 180;					
				case "0.166": // caja de 6
					return 6;
				case "0.167": // caja de 6
					return 6;					
				case "0.250": // caja de 4  - bag in box de 3 Litros
					return 4;
				case "0.25": // caja de 4  - bag in box de 3 Litros
					return 4;
				case "0.500":  // caja de 5L envasadora
					return 2;
				case "0.50":  // caja de 5L envasadora
					return 2;
				case "0.5":  // caja de 5L envasadora
					return 2;
				case "1.000":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1.00":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1.0":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
			
			}
		}
		return 0;
	}
	
	public Integer recogerTipoCaja(String r_padre, String r_hijo)
	{
		Double cantidadEscandallo=null;
		String articuloCaja=null;
		Integer rdo = null;
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 0109*
		 */
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		
		String tipoArticulo = cas.obtenerTipoArticulo(r_padre);
		
		if (tipoArticulo!=null && tipoArticulo.equals("PS"))
		{
			rdo = recogerCantidadJaulon(r_padre, "18");
			if (rdo==null) rdo = recogerCantidadJaulon(r_padre, "58");
			return rdo;
		}
		
		articuloCaja = this.recuperarComponente(r_padre, r_hijo);
		
		if (articuloCaja==null)
		{
			rdo = recogerCantidadJaulon(r_padre, "18");
			if (rdo==null) rdo = recogerCantidadJaulon(r_padre, "58");
			return rdo;
		}
		
		cantidadEscandallo = this.recuperarCantidadComponente(r_padre, articuloCaja);
				
		if ((articuloCaja !=null) && (articuloCaja=="0109500" || articuloCaja=="0109501" || articuloCaja=="0109502" ||articuloCaja=="0109503" || articuloCaja=="0109600" || articuloCaja=="0109690"))
		{
			return 0;
		}
		
		if (cantidadEscandallo!=null)
		{
			if (r_padre.contentEquals("0102006")) return 280;
			switch (cantidadEscandallo.toString())
			{

				case "0.028":// 1/2 paleta envasadora 2lt
					return 36; 
				case "0.067":// 1/2 paleta envasadora 5lt
					return 15;
				case "0.006": // separador o cantonera cada 15 cajas de 12. (monte oton tray pack)
					return 180;
				case "0.014": // 1/2 paleta embotellado
					return 70*4; 
				case "0.012": // 1/2 paleta embotellado
					return 84*3; 
				case "0.083": // caja de 12
					return 12;
				case "0.021": // bandejas 1/2 2L
					return 48;					
				case "0.05": // bandejas 1/2 5L
					return 20;
				case "0.166": // caja de 6
					return 6;
				case "0.167": // caja de 6
					return 6;					
				case "0.250": // caja de 4  - bag in box de 3 Litros
					return 4;
				case "0.25": // caja de 4  - bag in box de 3 Litros
					return 4;
				case "0.500":  // caja de 5L envasadora
					return 2;
				case "0.50":  // caja de 5L envasadora
					return 2;
				case "0.5":  // caja de 5L envasadora
					return 2;
				case "1.000":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1.00":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1.0":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
				case "1":	// estuche 1  - bag in box 20L. boteelas magnum en estuche
					return 1;
			
			}
		}
		return 0;
	}

	
	public Integer recogerCantidadJaulon(String r_padre, String r_mascara)
	{
		Integer cantidadEscandallo=null;
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 0109*
		 */
		cantidadEscandallo = new Integer(this.recuperarEan(r_padre, r_mascara));
		return cantidadEscandallo;
	}
	
	public Double obtenerCantidadPaletEnvasadora(String r_articulo)
	{
		Double palesObtenidos = null;
		ArrayList<String> cantidadesObtenidas = null;
		consultaArticulosServer cas = null;
		
		cas= consultaArticulosServer.getInstance(CurrentUser.get());
		
		cantidadesObtenidas = cas.obtenerCantidadesEan(r_articulo);
		if (cantidadesObtenidas!=null && cantidadesObtenidas.size()>0)
		{
			for (int i=0;i<cantidadesObtenidas.size();i++)
			{
				palesObtenidos = new Double(cantidadesObtenidas.get(i));
				if (palesObtenidos!=null && palesObtenidos != 0) 
				{
					return palesObtenidos;
				}
			}
		}		
		return new Double(0);
		
	}
	
	public Integer obtenerCantidadPaletEmbotelladora(String r_articulo)
	{
		Integer cantidadReferencia = null;
		Integer r_can_ean = null; 
		String rdo = null;
		
		rdo = consultaArticulosServer.getInstance(CurrentUser.get()).obtenerCantidadEan(r_articulo, "1");
		
		if (rdo==null || rdo.length()==0)
		{
			rdo = consultaArticulosServer.getInstance(CurrentUser.get()).obtenerCantidadEan(r_articulo, "5");
		}
		
		if (rdo!=null && rdo.length()>0)
		{
			r_can_ean = new Integer(rdo);
			return r_can_ean;
		}
		
		Integer r_botella = this.recogerTipoBotella(r_articulo);
		Integer r_palet = this.recogerTipoPalet(r_articulo);
		Integer r_caja = this.recogerTipoCaja(r_articulo);
		boolean cajaHorizontal = this.recogerTipoCajaHorizontal(r_articulo);
		
		if (cajaHorizontal) r_botella=3;
		
		switch (r_botella)
		{
			case 0: //bordelesa
			{
				switch (r_palet)
				{
					case 0: //80x120
					{
						cantidadReferencia = 720;
						break;
					}
					case 1: //100x120
					{
						if (r_caja == 6) cantidadReferencia = 840; else cantidadReferencia = 900;
						break;
					}
					case 2: // 1/2 paleta
						
						if (r_articulo.contentEquals("0102006") || r_articulo.contentEquals("0102091") ) cantidadReferencia = 280;
						else cantidadReferencia = 252;
						break;
				}
				break;
			}
			case 1: // borgoña
			{
				switch (r_palet)
				{
					case 0: //80x120
					{
						if (r_caja == 6) cantidadReferencia = 630; else cantidadReferencia = 0;
						break;
					}
					case 1: //100x120
					{
						if (r_caja == 6) cantidadReferencia = 720; else cantidadReferencia = 840;
						break;
					}
					case 2: // 1/2 paleta
					{
						cantidadReferencia = 0;
						break;
					}
				}
				break;
			}
			case 2: // bag in box
			{
				switch (r_palet)
				{
					case 0: //80x120
					{
						cantidadReferencia = 144;
						break;
					}
					case 1: //100x120
					{
						if (r_caja==1) cantidadReferencia = 40; else cantidadReferencia = 300;
						break;
					}
					case 2: // 1/2 paleta
						cantidadReferencia = 0;
						break;
				}
				break;
			}
			case 3:
			{
				switch (r_palet)
				{
					case 0: //80x120
					{
						if (r_caja == 6) cantidadReferencia = 480; else cantidadReferencia = 504;
						break;
					}
					case 1: //100x120
					{
						if (r_caja == 6) cantidadReferencia = 504; else cantidadReferencia = 660;
						break;
					}
					case 2: // 1/2 paleta
					{
						cantidadReferencia = 0;
						break;
					}
				}
			}
		}
		return cantidadReferencia;
	}
	
	public boolean recogerTipoCajaHorizontal(String r_padre)
	{
		String articuloCaja=null;
		String descripcionCaja=null;	
		/*
		 * busco el escandallo del r_mapeo.getArticulo() y devuelvo el articulo que empiece por 0109*
		 */
		articuloCaja = this.recuperarComponente(r_padre, "0109");
		
		if (articuloCaja==null) return false;
		else
		{
			consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
			descripcionCaja=cas.obtenerDescripcionArticulo(articuloCaja);
			if (descripcionCaja.contains("HORIZ")) return true; else return false;
		}
	}
	
	
	public ArrayList<MapeoLineasEscandallo> recuperarEscandallo(String r_articulo)
	{
		ArrayList<MapeoLineasEscandallo> rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			sql="select padre esc_pad, descripcion esc_des, fecha esc_fec from e__cconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre = '" + r_articulo.trim() + "' order by fecha desc ";
				
				rdo_obtenido = new ArrayList<MapeoLineasEscandallo>();
				
				con= this.conManager.establecerConexionGestionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsArticulos= cs.executeQuery(sql);
				while(rsArticulos.next())
				{
					MapeoLineasEscandallo mapeo = new MapeoLineasEscandallo();
					
					mapeo.setArticulo(rsArticulos.getString("esc_pad"));
					mapeo.setDescripcion(rsArticulos.getString("esc_des"));
					mapeo.setFechaModificacion(rsArticulos.getDate("esc_fec"));
					mapeo.setDias(RutinasFechas.diferenciaEnDias2(new Date(), rsArticulos.getDate("esc_fec")));
					
					rdo_obtenido.add(mapeo);
					break;
				}
			}
			else
			{
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public ArrayList<MapeoLineasEscandallo> recuperarLineasEscandallo(String r_articulo)
	{
		ArrayList<MapeoLineasEscandallo> rdo_obtenido = null;
		ResultSet rsArticulos = null;

		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			
			sql="select hijo esc_hij, descripcion esc_des from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre = '" + r_articulo.trim() + "' order by hijo asc ";
				
				rdo_obtenido = new ArrayList<MapeoLineasEscandallo>();
				
				con= this.conManager.establecerConexionGestionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsArticulos= cs.executeQuery(sql);
				while(rsArticulos.next())
				{
					MapeoLineasEscandallo mapeo = new MapeoLineasEscandallo();
					
					mapeo.setArticulo(rsArticulos.getString("esc_hij"));
					mapeo.setDescripcion(rsArticulos.getString("esc_des"));
					
					rdo_obtenido.add(mapeo);
				}
			}
			else
			{
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public ArrayList<MapeoLineasEscandallo> recuperarModificacionesEscandallo(String r_articulo)
	{
		ArrayList<MapeoLineasEscandallo> rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			sql="select hijo esc_hij, descripcion esc_des, orden from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre = '" + r_articulo.trim() + "' order by orden ";
				
				rdo_obtenido = new ArrayList<MapeoLineasEscandallo>();
				
				con= this.conManager.establecerConexionGestionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsArticulos= cs.executeQuery(sql);
				while(rsArticulos.next())
				{
					MapeoLineasEscandallo mapeo = new MapeoLineasEscandallo();
					
					mapeo.setArticulo(rsArticulos.getString("esc_hij"));
					mapeo.setDescripcion(rsArticulos.getString("esc_des"));
					mapeo.setFechaModificacion(null);
					mapeo.setDias(0);
					
					rdo_obtenido.add(mapeo);
				}
			}
			else
			{
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public ArrayList<MapeoLineasEscandallo> recuperarModificacionesEscandalloInterno(String r_articulo)
	{
		ArrayList<MapeoLineasEscandallo> rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			sql="select hijo esc_hij, descripcion esc_des, orden from e__lconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre = '" + r_articulo.trim() + "' order by orden ";
				
				rdo_obtenido = new ArrayList<MapeoLineasEscandallo>();
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsArticulos= cs.executeQuery(sql);
				while(rsArticulos.next())
				{
					MapeoLineasEscandallo mapeo = new MapeoLineasEscandallo();
					
					mapeo.setArticulo(rsArticulos.getString("esc_hij"));
					mapeo.setDescripcion(rsArticulos.getString("esc_des"));
					mapeo.setFechaModificacion(null);
					mapeo.setDias(0);
					
					rdo_obtenido.add(mapeo);
				}
			}
			else
			{
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
	
	public Integer obtenerDiasModificacion(String r_articulo)
	{
		Integer rdo_obtenido = null;
		ResultSet rsArticulos = null;
		Connection con = null;
		Statement cs = null;

		String sql = null;
		try
		{
			
			sql="select max(fecha) esc_fec from e__cconj ";
			
			if (r_articulo!=null)
			{
				sql = sql + " where padre = '" + r_articulo.trim() + "' ";
				con= this.conManager.establecerConexionGestionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsArticulos= cs.executeQuery(sql);
				while(rsArticulos.next())
				{
					rdo_obtenido = RutinasFechas.diferenciaEnDias2(new Date(), rsArticulos.getDate("esc_fec"));
				}
			}
			else
			{
				return null;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return rdo_obtenido;
	}
}