package borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoLineasEscandallo extends MapeoGlobal
{
	private String articulo = null; 
	private String descripcion = null; 
	private Integer dias = null;
	private Date fechaModificacion = null;
	private String botones = null;
	
	public MapeoLineasEscandallo()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return RutinasCadenas.conversion(descripcion);
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getDias() {
		return dias;
	}

	public void setDias(Integer dias) {
		this.dias = dias;
	}

	public String getFechaModificacion() {
		if (fechaModificacion!=null)
			return RutinasFechas.convertirDateToString(fechaModificacion);
		else return "";
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getBotones() {
		return botones;
	}

	public void setBotones(String botones) {
		this.botones = botones;
	}

		
}