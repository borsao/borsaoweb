package borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo;

import java.io.File;
import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaEscandalloGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private boolean verPdf = false;
	
    public AyudaEscandalloGrid(ArrayList<MapeoLineasEscandallo> r_vector, boolean r_verPdf) 
    {
        this.vector=r_vector;
        this.verPdf=r_verPdf;
        
		this.asignarTitulo("Cambios Escandallo");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public AyudaEscandalloGrid(ArrayList<MapeoLineasEscandallo> r_vector) 
    {
        this.vector=r_vector;
        
		this.asignarTitulo("Cambios Escandallo");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    private void generarGrid()
    {
		this.crearGrid(MapeoLineasEscandallo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    

    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("articulo", "descripcion", "fechaModificacion", "botones");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("fechaModificacion").setHeaderCaption("Fecha Modificacion");
    	this.getColumn("fechaModificacion").setSortable(false);
    	this.getColumn("fechaModificacion").setWidth(150);
    	this.getColumn("botones").setHeaderCaption("");
    	this.getColumn("botones").setSortable(false);
    	this.getColumn("botones").setWidth(new Double(50));
    	
    	this.getColumn("dias").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	if (!this.verPdf) this.getColumn("botones").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            String articulo = null;
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("articulo".equals(cellReference.getPropertyId())) 
            	{
            		if (cellReference.getValue()!=null)
            		{
            			articulo = cellReference.getValue().toString();
            		}
            		return "cell-normal";
            	}
            	else if ("botones".equals(cellReference.getPropertyId())) 
            	{            		
                	File fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + articulo.trim() + ".pdf");
                	
                	if (fl.exists()) 
                		return "nativebuttonPdf"; 
                	else 
                		return "cell-normal";
            	}
            	else 
            		return "cell-normal";
            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	
            	if (event.getPropertyId()!=null)
            	{
	            	if (event.getPropertyId().toString().equals("botones"))
	            	{
	            		MapeoLineasEscandallo mapeo = (MapeoLineasEscandallo) event.getItemId();
	            		
	                	File fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + mapeo.getArticulo().trim() + ".pdf");
	                	
	                	if (fl.exists())
	                	{
	                		RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath + "/calidad/mp", mapeo.getArticulo().trim() + ".pdf", false);
	                	}

	            	}	            	
            	}
            }
            
        });
    }

	@Override
	public void calcularTotal() 
	{
		
	}

}


