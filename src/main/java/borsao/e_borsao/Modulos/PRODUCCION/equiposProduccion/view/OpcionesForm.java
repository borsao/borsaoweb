package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo.EquiposGrid;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo.MapeoEquipos;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.server.consultaEquiposProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoEquipos mapeoEquipos = null;
	 
	 private consultaEquiposProduccionServer cus = null;	 
	 private EquiposProduccionView uw = null;
	 private BeanFieldGroup<MapeoEquipos> fieldGroup;
	 
	 public OpcionesForm(EquiposProduccionView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.area,"Area");
        
        fieldGroup = new BeanFieldGroup<MapeoEquipos>(MapeoEquipos.class);
        fieldGroup.bindMemberFields(this);

        this.cargarListeners();

    }   

	private void cargarListeners()
	{
		ValueChangeListener valueLis = new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				cargarCombos(jefe,area.getValue().toString());
		        cargarCombos(operario1,area.getValue().toString());
		        cargarCombos(operario2,area.getValue().toString());
		        cargarCombos(operario3,area.getValue().toString());				
			}
		};
		this.area.addValueChangeListener(valueLis);
		
		
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoEquipos = (MapeoEquipos) uw.grid.getSelectedRow();
                eliminarEquipo(mapeoEquipos);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoEquipos = hm.convertirHashAMapeo(uw.opcionesEscogidas);	 				
	                crearEquipo(mapeoEquipos);
	 			}
	 			else
	 			{
	 				MapeoEquipos mapeoEquipos_orig = (MapeoEquipos) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoEquipos= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarEquipo(mapeoEquipos,mapeoEquipos_orig);
	 				hm=null;
	 			}
	 			if (((EquiposGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		
		switch (r_nombre)
		{
			case "Area":
			{
				r_combo.addItem("General");
				r_combo.addItem("Almacen");
				r_combo.addItem("Embotelladora");
				r_combo.addItem("Envasadora");
				break;
			}
			case "Embotelladora":
			{
				if (r_nombre==null || r_nombre.length()==0) break;
				r_combo.removeAllItems();
				consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
				MapeoOperarios mapeoOp = new MapeoOperarios();
				mapeoOp.setArea(r_nombre);
				ArrayList<MapeoOperarios> oper = cos.datosOperariosGlobal(mapeoOp);
				for (int i = 0; i< oper.size();i++)
				{
					MapeoOperarios map = oper.get(i);
					r_combo.addItem(map.getNombre());
				}
				cos=null;
				break;
			}
		}		
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarEquipo(null);
		this.creacion = creacion;		
//		if (creacion) cargarNumerador();
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.jefe.getValue()!=null && this.jefe.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("jefe", this.jefe.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("jefe", "");
		}
		if (this.operario1.getValue()!=null && this.operario1.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("botellas", this.operario1.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("botellas", "");
		}
		if (this.operario2.getValue()!=null && this.operario2.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cajas", this.operario2.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cajas", "");
		}
		if (this.operario3.getValue()!=null && this.operario3.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("toro", this.operario3.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("toro", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarEquipo(null);
	}
	
	public void editarEquipo(MapeoEquipos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoEquipos();
		fieldGroup.setItemDataSource(new BeanItem<MapeoEquipos>(r_mapeo));
		jefe.focus();
		if (r_mapeo!=null) this.area.setValue(r_mapeo.getArea());
	}
	
	public void eliminarEquipo(MapeoEquipos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaEquiposProduccionServer(CurrentUser.get());
		((EquiposGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarEquipo(MapeoEquipos r_mapeo, MapeoEquipos r_mapeo_orig)
	{
		cus  = new consultaEquiposProduccionServer(CurrentUser.get());
	    if (r_mapeo.getEquipo()!=null) r_mapeo.setEquipo(new Integer(r_mapeo_orig.getEquipo()));
		cus.guardarCambios(r_mapeo);		
		((EquiposGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearEquipo(MapeoEquipos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaEquiposProduccionServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoEquipos>(r_mapeo));
			if (((EquiposGrid) uw.grid)!=null)
			{
				((EquiposGrid) uw.grid).refresh(r_mapeo,null);
			}
			else
			{
				uw.generarGrid(uw.opcionesEscogidas);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoEquipos> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoEquipos= item.getBean();
            if (this.mapeoEquipos.getEquipo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private void cargarNumerador()
    {
    	consultaEquiposProduccionServer ceps = consultaEquiposProduccionServer.getInstance(CurrentUser.get());
    	Integer valor = ceps.obtenerSiguiente();
    	if (valor!=0) this.equipo.setValue(valor.toString());
    }
}
