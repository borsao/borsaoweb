package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo.MapeoEquipos;

public class consultaEquiposProduccionServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEquiposProduccionServer instance;
	
	public consultaEquiposProduccionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEquiposProduccionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEquiposProduccionServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoEquipos> datosEquiposProduccionGlobal(MapeoEquipos r_mapeo)
	{
		ResultSet rsEquiposProduccion = null;		
		ArrayList<MapeoEquipos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_equipos_produccion.idprd_equipos_produccion equ_id, ");
        cadenaSQL.append(" prd_equipos_produccion.prd_area equ_are, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_jefe_turno equ_jef, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario1 equ_op1, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario2 equ_op2, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario3 equ_op3, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario4 equ_op4 ");
     	cadenaSQL.append(" FROM prd_equipos_produccion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEquipo()!=null && r_mapeo.getEquipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_equipos_produccion = " + new Integer(r_mapeo.getEquipo()) + " ");
				}
				if (r_mapeo.getJefe()!=null && r_mapeo.getJefe().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_jefe_turno = '" + r_mapeo.getJefe() + "' ");
				}				
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOperario1()!=null && r_mapeo.getOperario1().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario1 = '" + r_mapeo.getOperario1() + "' ");
				}
				if (r_mapeo.getOperario2()!=null && r_mapeo.getOperario2().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario2 = '" + r_mapeo.getOperario2() + "' ");
				}
				if (r_mapeo.getOperario3()!=null && r_mapeo.getOperario3().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario3 = '" + r_mapeo.getOperario3() + "' ");
				}
				if (r_mapeo.getOperario4()!=null && r_mapeo.getOperario4().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario4 = '" + r_mapeo.getOperario4() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_equipos_produccion asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquiposProduccion.TYPE_SCROLL_SENSITIVE,rsEquiposProduccion.CONCUR_UPDATABLE);
			rsEquiposProduccion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoEquipos>();
			
			while(rsEquiposProduccion.next())
			{
				MapeoEquipos mapeoEquiposProduccion = new MapeoEquipos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquiposProduccion.setEquipo(rsEquiposProduccion.getInt("equ_id"));
				mapeoEquiposProduccion.setJefe(rsEquiposProduccion.getString("equ_jef"));
				mapeoEquiposProduccion.setArea(rsEquiposProduccion.getString("equ_are"));
				mapeoEquiposProduccion.setOperario1(rsEquiposProduccion.getString("equ_op1"));
				mapeoEquiposProduccion.setOperario2(rsEquiposProduccion.getString("equ_op2"));
				mapeoEquiposProduccion.setOperario3(rsEquiposProduccion.getString("equ_op3"));
				mapeoEquiposProduccion.setOperario4(rsEquiposProduccion.getString("equ_op4"));
				vector.add(mapeoEquiposProduccion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public MapeoEquipos datosEquiposProduccion(MapeoEquipos r_mapeo)
	{
		ResultSet rsEquiposProduccion = null;		
		StringBuffer cadenaWhere =null;
		MapeoEquipos mapeoEquiposProduccion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_equipos_produccion.idprd_equipos_produccion equ_id, ");
        cadenaSQL.append(" prd_equipos_produccion.prd_area equ_are, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_jefe_turno equ_jef, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario1 equ_op1, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario2 equ_op2, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario3 equ_op3, ");
	    cadenaSQL.append(" prd_equipos_produccion.prd_operario4 equ_op4 ");
     	cadenaSQL.append(" FROM prd_equipos_produccion ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEquipo()!=null && r_mapeo.getEquipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_equipos_produccion = " + new Integer(r_mapeo.getEquipo()) + " ");
				}
				if (r_mapeo.getJefe()!=null && r_mapeo.getJefe().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_jefe_turno = '" + r_mapeo.getJefe() + "' ");
				}				
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_area = '" + r_mapeo.getArea() + "' ");
				}
				if (r_mapeo.getOperario1()!=null && r_mapeo.getOperario1().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario1 = '" + r_mapeo.getOperario1() + "' ");
				}
				if (r_mapeo.getOperario2()!=null && r_mapeo.getOperario2().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario2 = '" + r_mapeo.getOperario2() + "' ");
				}
				if (r_mapeo.getOperario3()!=null && r_mapeo.getOperario3().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario3 = '" + r_mapeo.getOperario3() + "' ");
				}
				if (r_mapeo.getOperario4()!=null && r_mapeo.getOperario4().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_operario4 = '" + r_mapeo.getOperario4() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_equipos_produccion asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquiposProduccion.TYPE_SCROLL_SENSITIVE,rsEquiposProduccion.CONCUR_UPDATABLE);
			rsEquiposProduccion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			
			while(rsEquiposProduccion.next())
			{
				mapeoEquiposProduccion = new MapeoEquipos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEquiposProduccion.setEquipo(rsEquiposProduccion.getInt("equ_id"));
				mapeoEquiposProduccion.setJefe(rsEquiposProduccion.getString("equ_jef"));
				mapeoEquiposProduccion.setArea(rsEquiposProduccion.getString("equ_are"));
				mapeoEquiposProduccion.setOperario1(rsEquiposProduccion.getString("equ_op1"));
				mapeoEquiposProduccion.setOperario2(rsEquiposProduccion.getString("equ_op2"));
				mapeoEquiposProduccion.setOperario3(rsEquiposProduccion.getString("equ_op3"));
				mapeoEquiposProduccion.setOperario4(rsEquiposProduccion.getString("equ_op4"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeoEquiposProduccion;
	}
	
	public Integer obtenerSiguiente()
	{
		ResultSet rsEquiposProduccion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_equipos_produccion.idprd_equipos_produccion) equ_sig ");
     	cadenaSQL.append(" FROM prd_equipos_produccion ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsEquiposProduccion.TYPE_SCROLL_SENSITIVE,rsEquiposProduccion.CONCUR_UPDATABLE);
			rsEquiposProduccion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsEquiposProduccion.next())
			{
				return rsEquiposProduccion.getInt("equ_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String guardarNuevo(MapeoEquipos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_equipos_produccion ( ");
    		cadenaSQL.append(" prd_equipos_produccion.idprd_equipos_produccion, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_area, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_jefe_turno, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario1, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario2, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario3, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario4) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getJefe()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getJefe());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getOperario1()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getOperario1());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getOperario2()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getOperario2());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getOperario3()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getOperario3());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getOperario4()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getOperario4());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoEquipos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_equipos_produccion set ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_area  = ?, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_jefe_turno  = ?, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario1  = ?, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario2  = ?, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario3  = ?, ");
    		cadenaSQL.append(" prd_equipos_produccion.prd_operario4  = ? ");
		    cadenaSQL.append(" WHERE prd_equipos_produccion.idprd_equipos_produccion = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getArea());
		    preparedStatement.setString(2, r_mapeo.getJefe());
		    preparedStatement.setString(3, r_mapeo.getOperario1());
		    preparedStatement.setString(4, r_mapeo.getOperario2());
		    preparedStatement.setString(5, r_mapeo.getOperario3());
		    preparedStatement.setString(6, r_mapeo.getOperario4());
		    if (r_mapeo.getEquipo()!=null)
		    	preparedStatement.setInt(7, new Integer(r_mapeo.getEquipo()));
		    else
		    	preparedStatement.setInt(7, 0);
		    
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoEquipos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_equipos_produccion ");            
			cadenaSQL.append(" WHERE prd_equipos_produccion.idprd_equipos_produccion = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    if (r_mapeo.getEquipo()!=null)
		    	preparedStatement.setInt(1, new Integer(r_mapeo.getEquipo()));
		    else
		    	preparedStatement.setInt(1, 0);

			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public ArrayList<MapeoAyudas> ayuda(String r_area)
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsAyuda= null;
		
		String sql = null;
		try
		{

			r_vector = new ArrayList<MapeoAyudas>();
			sql="select prd_equipos_produccion.idprd_equipos_produccion equ_id, prd_equipos_produccion.prd_jefe_turno equ_jef from prd_equipos_produccion where prd_area = '" + r_area + "' order by idprd_equipos_produccion asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsAyuda= cs.executeQuery(sql);
			while(rsAyuda.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsAyuda.getString("equ_id"));
				mA.setDescripcion(rsAyuda.getString("equ_jef"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAyuda!=null)
				{
					rsAyuda.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}
}