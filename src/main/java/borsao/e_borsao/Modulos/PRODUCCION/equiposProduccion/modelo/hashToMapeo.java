package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoEquipos mapeo = null;
	 
	 public MapeoEquipos convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquipos();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setJefe(r_hash.get("jefe"));
		 this.mapeo.setOperario1(r_hash.get("botellas"));		 
		 this.mapeo.setOperario2(r_hash.get("cajas"));
		 this.mapeo.setOperario3(r_hash.get("toro"));
		 if (r_hash.get("equipo")!=null)
		 {
			 this.mapeo.setEquipo(new Integer(r_hash.get("equipo")));
		 }
		 return mapeo;		 
	 }
	 
	 public MapeoEquipos convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoEquipos();
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setJefe(r_hash.get("jefe"));
		 this.mapeo.setOperario1(r_hash.get("botellas"));		 
		 this.mapeo.setOperario2(r_hash.get("cajas"));
		 this.mapeo.setOperario3(r_hash.get("toro"));
		 if (r_hash.get("equipo")!=null)
		 {
			 this.mapeo.setEquipo(new Integer(r_hash.get("equipo")));
		 }
		 return mapeo;			 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoEquipos r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("jefe", r_mapeo.getJefe());
		 this.hash.put("botellas", r_mapeo.getOperario1());
		 this.hash.put("cajas", r_mapeo.getOperario2());
		 this.hash.put("toro", r_mapeo.getOperario3());
		 if (r_mapeo.getEquipo()!=null)
		 {
			 this.hash.put("equipo", r_mapeo.getEquipo().toString());
		 }
		 return hash;		 
	 }
}