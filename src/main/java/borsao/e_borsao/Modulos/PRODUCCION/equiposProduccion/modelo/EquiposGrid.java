package borsao.e_borsao.Modulos.PRODUCCION.equiposProduccion.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EquiposGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public EquiposGrid(ArrayList<MapeoEquipos> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Equipos Producción");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoEquipos.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("equipo", "area", "jefe","operario1","operario2","operario3","operario4");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("equipo").setHeaderCaption("Equipo");
    	this.getColumn("area").setHeaderCaption("Area Producción");    	
    	this.getColumn("jefe").setHeaderCaption("Jefe Turno");
    	this.getColumn("operario1").setHeaderCaption("Botellas/Garrafas");
    	this.getColumn("operario2").setHeaderCaption("Cajas");
    	this.getColumn("operario3").setHeaderCaption("Carretillero");
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("operario4").setHidden(true);
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
