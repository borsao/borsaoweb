package borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaArticulosSMPTServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaArticulosSMPTServer instance;
	
	public consultaArticulosSMPTServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaArticulosSMPTServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaArticulosSMPTServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoArticulos> datosOpcionesGlobal(MapeoArticulos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoArticulos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_smpt.articulo smp_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo smp_des, ");
		cadenaSQL.append(" prd_smpt.cuantos smp_qt, ");
		cadenaSQL.append(" prd_smpt.observaciones smp_obs ");
     	cadenaSQL.append(" FROM prd_smpt ");
     	cadenaSQL.append(" INNER JOIN e_articu on e_articu.articulo = prd_smpt.articulo ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().trim().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_smpt.articulo = '" + r_mapeo.getArticulo() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by prd_smpt.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoArticulos>();
			
			while(rsOpcion.next())
			{
				MapeoArticulos mapeoArticulosEnvasadora = new MapeoArticulos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoArticulosEnvasadora.setArticulo(rsOpcion.getString("smp_art"));
				mapeoArticulosEnvasadora.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("smp_des")));
				mapeoArticulosEnvasadora.setCuantos(rsOpcion.getDouble("smp_qt"));
				mapeoArticulosEnvasadora.setObservaciones(rsOpcion.getString("smp_obs"));
				vector.add(mapeoArticulosEnvasadora);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public HashMap<String,Double> hashCuantos()
	{
		ResultSet rsOpcion = null;		
		HashMap<String,Double> hashCuantos = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_smpt.articulo smp_art, ");
		cadenaSQL.append(" prd_smpt.cuantos smp_qt ");
     	cadenaSQL.append(" FROM prd_smpt ");

		try
		{
			cadenaSQL.append(" order by prd_smpt.articulo asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hashCuantos = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				hashCuantos.put(rsOpcion.getString("smp_art").trim(),rsOpcion.getDouble("smp_qt"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashCuantos;
	}

	public Double mesesMinStock(String r_articulo)
	{
		ResultSet rsOpcion = null;		
		Double cuantos = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_smpt.cuantos smp_qt ");
     	cadenaSQL.append(" FROM prd_smpt ");
     	cadenaSQL.append(" where prd_smpt.articulo = '" + r_articulo + "' ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			cuantos = new Double(0);
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				cuantos=rsOpcion.getDouble("smp_qt");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cuantos;
	}

	public String guardarNuevo(MapeoArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_smpt ( ");
			cadenaSQL.append(" prd_smpt.articulo, ");
			cadenaSQL.append(" prd_smpt.cuantos, ");
    		cadenaSQL.append(" prd_smpt.observaciones) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getCuantos()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getCuantos());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, new Double(0));
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_smpt set ");
    		cadenaSQL.append(" prd_smpt.cuantos=?, ");
    		cadenaSQL.append(" prd_smpt.observaciones=? ");
    		cadenaSQL.append(" where prd_smpt.articulo = ? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getCuantos()!=null)
		    {
		    	preparedStatement.setDouble(1, r_mapeo.getCuantos());
		    }
		    else
		    {
		    	preparedStatement.setDouble(1, new Double(0));
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoArticulos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_smpt ");            
			cadenaSQL.append(" WHERE prd_smpt.articulo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setString(1, r_mapeo.getArticulo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
		
	}
	
	public ArrayList<MapeoAyudas> vector()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where articulo between '0102000' and '0111999' order by articulo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	public ArrayList<MapeoAyudas> vectorGRSMPT()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where (articulo like '0101%' or tipo_articulo in ('PT','ST'))  order by articulo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	public ArrayList<MapeoAyudas> vectorMP()
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsArticulos = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			sql="select articulo, descrip_articulo from e_articu where tipo_articulo='OA' or (tipo_articulo='MP' and articulo like '0101%') order by articulo asc" ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsArticulos= cs.executeQuery(sql);
			while(rsArticulos.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsArticulos.getString("articulo"));
				mA.setDescripcion(rsArticulos.getString("descrip_articulo"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsArticulos!=null)
				{
					rsArticulos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

}