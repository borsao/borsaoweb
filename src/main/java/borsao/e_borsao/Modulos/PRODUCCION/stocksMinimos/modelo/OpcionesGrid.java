package borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.PRODUCCION.stocksMinimos.view.StockMinimoPTView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private StockMinimoPTView app = null;
	
    public OpcionesGrid(StockMinimoPTView r_app, ArrayList<MapeoArticulos> r_vector) 
    {
    	this.app = r_app;
        this.vector=r_vector;
		this.asignarTitulo("Articulos Control Stocks Minimos PT");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoArticulos.class);
		this.setRecords(this.vector);
		this.setStyleName("smallgrid");

		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("articulo","descripcion", "cuantos", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("cuantos").setHeaderCaption("Meses Stock");
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("cuantos".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }

	@Override
	public void establecerColumnasNoFiltro() 
	{		
	}

	@Override
	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		
	}

}
