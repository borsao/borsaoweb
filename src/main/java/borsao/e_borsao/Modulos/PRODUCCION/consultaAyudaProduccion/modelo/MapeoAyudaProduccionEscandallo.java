package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAyudaProduccionEscandallo extends MapeoGlobal
{
	private String almacen = null; 
	private String nombreAlmacen = null; 
	private String articulo = null; 
	private String descripcion = null; 
	private Integer existencias = null;
	private Integer pendienteRecibir = null;
	private String fechaEntrega = null;
	private String confirmado = null;
	private String ubicacion = null;
	private Double factor = null;
	private Double coste = null;
	private Integer necesito = null;
	private Integer faltansobran = null;
	private boolean existencias3 = false;
	private boolean articuloNC = false;
	
	
	public MapeoAyudaProduccionEscandallo()
	{
	}

	public String getAlmacen() {
		return almacen;
	}

	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}

	public String getNombreAlmacen() {
		return nombreAlmacen;
	}

	public void setNombreAlmacen(String nombreAlmacen) {
		this.nombreAlmacen = nombreAlmacen;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	public Integer getPendienteRecibir() {
		return pendienteRecibir;
	}

	public void setPendienteRecibir(Integer pendienteRecibir) {
		this.pendienteRecibir = pendienteRecibir;
	}

	public String getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public Double getFactor() {
		return factor;
	}

	public void setFactor(Double factor) {
		this.factor = factor;
	}

	public Double getCoste() {
		return coste;
	}

	public void setCoste(Double coste) {
		this.coste = coste;
	}

	public Integer getNecesito() {
		return necesito;
	}

	public void setNecesito(Integer necesito) {
		this.necesito = necesito;
	}

	public Integer getFaltansobran() {
		return faltansobran;
	}

	public void setFaltansobran(Integer faltansobran) {
		this.faltansobran = faltansobran;
	}

	public boolean isExistencias3() {
		return existencias3;
	}

	public void setExistencias3(boolean existencias3) {
		this.existencias3 = existencias3;
	}

	public boolean isArticuloNC() {
		return articuloNC;
	}

	public void setArticuloNC(boolean articuloNC) {
		this.articuloNC = articuloNC;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getConfirmado() {
		return confirmado;
	}

	public void setConfirmado(String confirmado) {
		this.confirmado = confirmado;
	}

		
}