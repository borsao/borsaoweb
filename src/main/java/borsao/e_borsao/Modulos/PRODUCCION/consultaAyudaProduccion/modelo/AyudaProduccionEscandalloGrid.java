package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMCompras;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaProduccionEscandalloGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public AyudaProduccionEscandalloGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoAyudaProduccionEscandallo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	
    	setColumnOrder("confirmado", "nombreAlmacen", "articulo", "descripcion", "existencias", "pendienteRecibir", "factor", "coste", "necesito", "faltansobran", "fechaEntrega");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombreAlmacen").setHeaderCaption("Almacen");
    	this.getColumn("nombreAlmacen").setSortable(false);
    	this.getColumn("nombreAlmacen").setWidth(new Double(200));
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("existencias").setHeaderCaption("Existencias");
    	this.getColumn("existencias").setWidth(100);
    	this.getColumn("pendienteRecibir").setHeaderCaption("Pdte. Recibir");
    	this.getColumn("pendienteRecibir").setWidth(100);
    	this.getColumn("factor").setHeaderCaption("");
    	this.getColumn("factor").setWidth(100);
    	this.getColumn("coste").setHeaderCaption("");
    	this.getColumn("coste").setWidth(100);
    	this.getColumn("necesito").setHeaderCaption("Necesidad");
    	this.getColumn("necesito").setWidth(100);
    	this.getColumn("faltansobran").setHeaderCaption("Falta / Sobra");
    	this.getColumn("faltansobran").setWidth(100);
    	this.getColumn("fechaEntrega").setHeaderCaption("Fecha Entrega");
    	this.getColumn("fechaEntrega").setWidth(150);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("existencias3").setHidden(true);
    	this.getColumn("articuloNC").setHidden(true);
    	this.getColumn("confirmado").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
    		String confirmadaEntrega = null;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("confirmado".equals(cellReference.getPropertyId()))
    			{
            		if (cellReference.getValue()!=null && !cellReference.getValue().toString().toUpperCase().trim().contentEquals("NO"))
            			confirmadaEntrega="SI";
            		else if (cellReference.getValue()!=null)
            			confirmadaEntrega="NO";
            		else
            			confirmadaEntrega="SI";
            	}
            	if ("necesito".equals(cellReference.getPropertyId()) || "faltansobran".equals(cellReference.getPropertyId()) || 
            			"factor".equals(cellReference.getPropertyId()) || "coste".equals(cellReference.getPropertyId())) 
            	{
        			return "Rcell-normal";
            	}
            	else if ("existencias".equals(cellReference.getPropertyId()) || "pendienteRecibir".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-pointer";
            	}
            	else if ("articulo".equals(cellReference.getPropertyId()))
            	{
            		return "cell-pointer";
            	}
            	else if ("fechaEntrega".equals(cellReference.getPropertyId()))
            	{
            		if (confirmadaEntrega==null || confirmadaEntrega.contentEquals("SI")) return "cell-normal"; else return "cell-error";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("pendienteRecibir"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
            		
            		pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(mapeo.getArticulo(), mapeo.getExistencias(), "Lineas Pedidos Compra del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
        			getUI().addWindow(vt);
            	}
            	else if (event.getPropertyId().toString().equals("existencias"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
            		if (mapeo!=null && mapeo.getArticulo()!=null && mapeo.getArticulo().trim().startsWith("0102"))
            		{
            			pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + mapeo.getArticulo().trim(), mapeo.getArticulo().trim(),null);
            			getUI().addWindow(vt);
            		}
            	}
            	else if (event.getPropertyId().toString().equals("articulo"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
            		if (mapeo!=null && mapeo.getArticulo()!=null && (mapeo.getArticulo().trim().startsWith("0102") || mapeo.getArticulo().trim().startsWith("0103") || mapeo.getArticulo().trim().startsWith("0111")))
            		{
            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo", mapeo.getArticulo());
            			if (vt.acceso)
            				getUI().addWindow(vt);
            			else 
            				vt=null;
            		}
            		else
            		{
            			pantallaCRMCompras vt = new pantallaCRMCompras("CRM Compras", mapeo.getArticulo());
            			if (vt.acceso)
            				getUI().addWindow(vt);
            			else 
            				vt=null;
            		}

            	}

            }
    	});
	}

	@Override
	public void calcularTotal() {
		
	}
}


