package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.server.consultaInventarioServer;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.modelo.MapeoInventarioSGA;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventarioSGA.server.consultaInventarioSGAServer;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaNoConformidadesServer;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.server.consultaPedidosComprasServer;
import borsao.e_borsao.Modulos.COMPRAS.actualizacionIntranet.server.consultaArticulosIntranetServer;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Almacenes.server.consultaAlmacenesServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionStock;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaAyudaProduccionServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAyudaProduccionServer instance;
	
	public consultaAyudaProduccionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAyudaProduccionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAyudaProduccionServer(r_usuario);			
		}
		return instance;
	}
		
	public ArrayList<MapeoAyudaProduccionStock> datosStock(String r_articulo)
	{
		ResultSet rsOpcion = null;
		ResultSet rsPrepedidos = null;		
		ArrayList<MapeoAyudaProduccionStock> vector = null;
		MapeoAyudaProduccionStock mapeo = null; 
		Connection con = null;
		Statement cs = null;		
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacenesCalculo = cas.obtenerAlmacenesCalculoPendienteServir();

		try
		{

			StringBuffer cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT a.articulo se_art, ");
	        cadenaSQL.append(" b.descrip_articulo se_des, ");
	        cadenaSQL.append(" sum(a.existencias) se_exa, ");	    
	        cadenaSQL.append(" sum(a.pdte_servir) se_pse ");
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
	     	cadenaSQL.append(" where a.articulo = b.articulo ");
	     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "' ");
	     	cadenaSQL.append(" and a.almacen in " + almacenesCalculo );
	     	
	     	cadenaSQL.append(" group by 1,2 ");
	     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion = cs.executeQuery(cadenaSQL.toString());
		
			vector = new ArrayList<MapeoAyudaProduccionStock>();  
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoAyudaProduccionStock();
				
				mapeo.setArticulo(rsOpcion.getString("se_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("se_des")));
				mapeo.setExistencias(rsOpcion.getInt("se_exa"));
				
				Integer reservado = 0;
				//Llamar a GREENsys para sacar las unidades de articulo con plazo = 31/12/2999
				consultaPedidosVentasServer cps =new consultaPedidosVentasServer(CurrentUser.get());
				reservado = cps.traerCantidadReservado(con, r_articulo);
				
				if (reservado==null) mapeo.setPendienteServir(rsOpcion.getInt("se_pse"));
				else mapeo.setPendienteServir(rsOpcion.getInt("se_pse")-reservado);
			}

			if (mapeo!=null)
			{
				cadenaSQL = new StringBuffer();
				cadenaSQL.append(" SELECT a.existencias se_exa ");	    
		     	cadenaSQL.append(" FROM a_exis_0 a, e_articu b ");
		     	cadenaSQL.append(" where a.articulo = b.articulo ");
		     	cadenaSQL.append(" and a.articulo = '" + r_articulo + "' ");
		     	cadenaSQL.append(" and a.almacen = 6 ");
	
		     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsPrepedidos = cs.executeQuery(cadenaSQL.toString());
				mapeo.setPreparadas(0);
				
				if (rsPrepedidos!=null)
				{
					while(rsPrepedidos.next())
					{
						mapeo.setPreparadas(rsPrepedidos.getInt("se_exa"));
					}
				}				
				mapeo.setPendientesPreparar(mapeo.getPendienteServir()-mapeo.getPreparadas());
				mapeo.setStockTotal(mapeo.getExistencias()+mapeo.getPreparadas());
				mapeo.setStockReal(mapeo.getStockTotal()-mapeo.getPendienteServir());
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public Double datosPendienteServir(Connection r_con, String r_articulo)
	{
		ResultSet rsOpcion = null;
		Double pendienteServir = null;
		Statement cs = null;		

		try
		{

			StringBuffer cadenaSQL = new StringBuffer();
			cadenaSQL.append(" SELECT sum(a.pdte_servir) se_pse ");
	     	cadenaSQL.append(" FROM a_exis_0 a ");
	     	
	     	if (r_articulo.length()>0)
	     	{
	     		cadenaSQL.append(" where a.articulo in ( select padre from e__lconj where hijo='" + r_articulo + "') ");         
	     	
				cs = r_con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion = cs.executeQuery(cadenaSQL.toString());
				pendienteServir=new Double(0);
				
				while(rsOpcion.next())
				{
					pendienteServir = rsOpcion.getDouble("se_pse");
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return pendienteServir;
	}
	
	public ArrayList<MapeoAyudaProduccionEscandallo> datosEscandallo(String r_articulo, Integer r_cantidad)
	{
		ResultSet rsOpcion = null;
		ResultSet rsPedido = null;
		HashMap<String, String> hashFechas = null;
		HashMap<String, String> hashConf = null;
		ArrayList<MapeoAyudaProduccionEscandallo> vector = null;
		MapeoAyudaProduccionEscandallo mapeo = null; 
		Connection con = null;
		Statement cs = null;		

		try
		{

			
			StringBuffer cadenaSQL2 = new StringBuffer();
			cadenaSQL2.append(" SELECT min(a_cc_l_0.plazo_entrega) ped_fec, ");
			cadenaSQL2.append(" a_cc_l_0.articulo ped_art, ");
			cadenaSQL2.append(" a_cc_c_0.ref_proveedor ped_conf ");
	     	cadenaSQL2.append(" FROM a_cc_l_0 , a_cc_c_0, green.e__lconj b");
	     	cadenaSQL2.append(" where a_cc_l_0.clave_tabla = a_cc_c_0.clave_tabla ");
	     	cadenaSQL2.append(" and a_cc_l_0.documento = a_cc_c_0.documento ");
	     	cadenaSQL2.append(" and a_cc_l_0.serie = a_cc_c_0.serie ");
	     	cadenaSQL2.append(" and a_cc_l_0.codigo = a_cc_c_0.codigo ");
	     	cadenaSQL2.append(" and a_cc_l_0.articulo = b.hijo " );
	     	cadenaSQL2.append(" and b.padre = '" + r_articulo.trim() + "' ");
	     	cadenaSQL2.append(" and a_cc_l_0.cumplimentacion is null ");
	     	cadenaSQL2.append(" group by 2,3 ");

	     	con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL2.toString() + " : " + new Date());
			rsPedido = cs.executeQuery(cadenaSQL2.toString());

			hashFechas = new HashMap<String, String>();
			hashConf = new HashMap<String, String>();
			
			
			while(rsPedido.next())
			{
				hashFechas.put(rsPedido.getString("ped_art").trim(),rsPedido.getString("ped_fec"));
				hashConf.put(rsPedido.getString("ped_art").trim(),rsPedido.getString("ped_conf"));
			}

			StringBuffer cadenaSQL = new StringBuffer();			
			cadenaSQL.append(" SELECT c.almacen  esc_alm, ");
			cadenaSQL.append(" c.nombre esc_nom, ");
			cadenaSQL.append(" b.hijo esc_art, ");
	        cadenaSQL.append(" b.descripcion esc_des, ");
	        cadenaSQL.append(" a.existencias esc_ex, ");	    
	        cadenaSQL.append(" a.pdte_recibir esc_pre, ");
	        cadenaSQL.append(" b.cantidad esc_can, ");
	        cadenaSQL.append(" a.ubicacion esc_ubi, ");
	        cadenaSQL.append(" a.coste_ult_compra esc_cos ");
	        
	     	cadenaSQL.append(" FROM green.a_exis_0 a, green.e__lconj b, green.e_almac c ");
	     	cadenaSQL.append(" where b.hijo = a.articulo ");
	     	cadenaSQL.append(" and a.almacen = c.almacen ");
	     	cadenaSQL.append(" and b.padre = '" + r_articulo.trim() + "' ");
	     	cadenaSQL.append(" and a.almacen not in (2,8,10,20) ");
	     	cadenaSQL.append(" order by 3,1,2 ");

	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
		
			vector = new ArrayList<MapeoAyudaProduccionEscandallo>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoAyudaProduccionEscandallo();
				
				mapeo.setAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setArticulo(rsOpcion.getString("esc_art"));
				mapeo.setCoste(rsOpcion.getDouble("esc_cos"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("esc_des")));
				mapeo.setExistencias(rsOpcion.getInt("esc_ex"));
				mapeo.setFactor(rsOpcion.getDouble("esc_can"));
				mapeo.setPendienteRecibir(rsOpcion.getInt("esc_pre"));
				mapeo.setNombreAlmacen(rsOpcion.getString("esc_nom"));
				if (!rsOpcion.getString("esc_art").startsWith("0102")) mapeo.setUbicacion(rsOpcion.getString("esc_ubi")); else mapeo.setUbicacion(null);
				mapeo.setNecesito(new Double(r_cantidad *  mapeo.getFactor()).intValue());
				
				mapeo.setFaltansobran(mapeo.getExistencias()+mapeo.getPendienteRecibir()-mapeo.getNecesito());
				mapeo.setFechaEntrega(null);
				mapeo.setFechaEntrega(hashFechas.get(rsOpcion.getString("esc_art").trim()));
				mapeo.setConfirmado(hashConf.get(rsOpcion.getString("esc_art").trim()));
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con !=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public ArrayList<String> datosArticulosEscandallo(String r_articulo)
	{
		ResultSet rsOpcion = null;
		ResultSet rsPedido = null;
		ArrayList<String> vector = null;
		Connection con = null;
		Statement cs = null;		
		
		try
		{
			
			StringBuffer cadenaSQL = new StringBuffer();			
			cadenaSQL.append(" SELECT b.hijo esc_art ");
			cadenaSQL.append(" FROM green.e__lconj b ");
			cadenaSQL.append(" where b.padre = '" + r_articulo.trim() + "' ");
			cadenaSQL.append(" order by 1 ");
			
			con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion = cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<String>();
			
			while(rsOpcion.next())
			{
				vector.add(rsOpcion.getString("esc_art"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con !=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoAyudaProduccionEscandallo> datosEscandalloGlobal(ArrayList<String> r_vector, String r_ejercicio, String r_semana, boolean r_verTres)
	{
		Connection con = null;
		Statement cs = null;		

		ResultSet rsOpcion = null;
		ResultSet rsOpcion2 = null;
		Connection conMysql = null;
		Double cantidad = null;
		String articulo_old = "";
		ArrayList<MapeoAyudaProduccionEscandallo> vector = null;
		ArrayList<String> r_vector_padres = null;
		ArrayList<String> vectorTres = null;
		HashMap<String, String> hashVectorPlazos = null;
		HashMap<String, String> hashVectorConfirmacionPlazos = null;
		
		MapeoAyudaProduccionEscandallo mapeo = null; 
		
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacenesCalculo = cas.obtenerAlmacenesCalculoPendienteServir();

		try
		{

			consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
//			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			consultaNoConformidadesServer cncs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
			consultaArticulosIntranetServer cins = consultaArticulosIntranetServer.getInstance(CurrentUser.get());
			consultaPedidosComprasServer cpcs = consultaPedidosComprasServer.getInstance(CurrentUser.get());
			
			conMysql= this.conManager.establecerConexionInd();
			
			StringBuffer cadenaSQL = new StringBuffer();
			
			if (r_vector.isEmpty()) return null;

			hashVectorPlazos = cpcs.obtenerPlazosEntrega();
			hashVectorConfirmacionPlazos = cpcs.obtenerConfirmacionPlazos();
			
			if (r_verTres)
			{
				cadenaSQL.append(" SELECT articulo esc_art ");
				cadenaSQL.append(" FROM green.a_exis_0 a ");
				cadenaSQL.append(" where a.almacen = 3 ");
				cadenaSQL.append(" and a.existencias <> 0 ");
				
		     	con= this.conManager.establecerConexionGestionInd();			
		     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
				
				vectorTres = new ArrayList<String>();
				
				while(rsOpcion.next())
				{
					vectorTres.add(rsOpcion.getString("esc_art"));
				}
				
				cadenaSQL = new StringBuffer();
			}
			
			cadenaSQL.append(" SELECT 'global' esc_alm, ");
			cadenaSQL.append(" a.articulo esc_art, ");
	        cadenaSQL.append(" b.descrip_articulo esc_des, ");
	        cadenaSQL.append(" sum(a.existencias) esc_ex, ");	    
	        cadenaSQL.append(" sum(a.pdte_recibir) esc_pre ");
	        cadenaSQL.append(" FROM green.a_exis_0 a, green.e_articu b ");
	     	cadenaSQL.append(" where b.articulo = a.articulo ");
	     	cadenaSQL.append(" and a.articulo not matches '0101*' ");
	     	cadenaSQL.append(" and a.almacen in " + almacenesCalculo);
	     	
	     	String padres = null;
	     	Integer j = 0;
	     	int partidor = 10;
	     	
	     	if (!r_vector.isEmpty() && r_vector.size()>0)
	     	{	     		
	     		padres="";
	     		if (r_vector.size()<partidor) partidor=r_vector.size(); 
	     		for (int i=0; i<r_vector.size();i++)
	     		{
	     			if (r_vector.get(i).trim().length()>0 )
	     			{
	     				if (padres!="") padres = padres + ",";
	     				padres=padres + "'" + r_vector.get(i).trim() + "'";
	     				j=j+1;
	     			}
	     			
	     			if (j==partidor && i == partidor-1 && padres !="")
	     			{
		     			cadenaSQL.append(" and a.articulo in (select hijo from e__lconj ");
		     			cadenaSQL.append(" where (padre in (" + padres + ") " );
		     			padres="";
	     			}
	     			else if (i>partidor && i%partidor==0 && padres !="")
	     			{
	     				cadenaSQL.append(" or padre in (" + padres + ") " );
	     				padres="";
	     			}
	     		}
	     		if (padres !="" && partidor!=r_vector.size()) 
	     			cadenaSQL.append(" or padre in (" + padres + ") " );
	     		else if (padres !="" )
	     		{
	     			cadenaSQL.append(" and a.articulo in (select hijo from e__lconj ");
     				cadenaSQL.append(" where (padre in (" + padres + ") " );
	     		}
	     		cadenaSQL.append("))");
	     	}
	     	
	     	
	     	cadenaSQL.append(" group by 1,2,3 ");
	     	cadenaSQL.append(" order by 2 ");

	     	if (cs!=null) cs.close();
	     	if (rsOpcion!=null) rsOpcion.close();
	     	
	     	if (con==null) con= this.conManager.establecerConexionGestionInd();			
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
		
			
			if (padres!=null)
			{
				StringBuffer sQL = new StringBuffer();
				sQL.append(" select unique padre esc_pad, ");
				sQL.append(" hijo esc_hij, ");
				sQL.append(" cantidad esc_can ");
				sQL.append(" from e__lconj ");
				j=0;
				padres="";
				for (int i=0; i<r_vector.size();i++)
	     		{
	     			if (r_vector.get(i).trim().length()>0 )
	     			{
	     				if (padres!="") padres = padres + ",";
	     				padres=padres + "'" + r_vector.get(i).trim() + "'";
	     				j=j+1;
	     			}
	     			
	     			if (j==partidor && i == partidor-1 && padres !="")
	     			{
		     			sQL.append(" where (padre in (" + padres + ") ");
		     			padres="";
	     			}
	     			else if (i>partidor && i%partidor==0 && padres !="")
	     			{
	     				sQL.append(" or padre in (" + padres + ") " );
	     				padres="";
	     			}
	     		}
	     		if (padres !="" && partidor!=r_vector.size()) 
	     			sQL.append(" or padre in (" + padres + ") " );
	     		else if (padres !="" )
	     		{	     			
     				sQL.append(" where (padre in (" + padres + ") " );
	     		}
	     		sQL.append(")");
				
	     		Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sQL.toString() + " : " + new Date());
				rsOpcion2 = cs.executeQuery(sQL.toString());				
			}
			
			vector = new ArrayList<MapeoAyudaProduccionEscandallo>();
			
			while(rsOpcion.next())
			{
				
				Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("esc_art").trim());
				
				mapeo = new MapeoAyudaProduccionEscandallo();				
				mapeo.setAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setArticulo(rsOpcion.getString("esc_art"));
				
				mapeo.setUbicacion(cins.obtenerUbicacion(rsOpcion.getString("esc_art")));
				
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("esc_des")));
				mapeo.setExistencias(rsOpcion.getInt("esc_ex"));
				mapeo.setPendienteRecibir(rsOpcion.getInt("esc_pre"));
				mapeo.setNombreAlmacen("Global");
				mapeo.setFactor(new Double(0));
				mapeo.setArticuloNC(cncs.hayNoConformidades(rsOpcion.getString("esc_art")));
				mapeo.setExistencias3(false);
				if (r_verTres) mapeo.setExistencias3(vectorTres.contains(rsOpcion.getString("esc_art")));
				
				if (!articulo_old.trim().equals(mapeo.getArticulo().trim()))
				{
					
			     	r_vector_padres = new ArrayList<String>();

			     	rsOpcion2.beforeFirst();
			     	
			     	while (rsOpcion2.next())
			     	{
			     		if (rsOpcion.getString("esc_art").trim().equals(rsOpcion2.getString("esc_hij").trim()))
	     				{
			     			r_vector_padres.add(rsOpcion2.getString("esc_pad").substring(0, 7));
			     			mapeo.setFactor(rsOpcion2.getDouble("esc_can"));
	     				}
			     	}
//			     	Double factor = ces.recuperarCantidadComponente(this.con, rsOpcion2.getString("esc_pad"), rsOpcion2.getString("esc_hij"));
//			     	if (factor==null) factor = new Double(0);
//			     	mapeo.setFactor(rsOpcion.getDouble("esc_can"));
//			     	mapeo.setFactor(factor);
			     	String tipo = null;
			     	
			     	if (mapeo.getArticulo().startsWith("0101") || mapeo.getArticulo().startsWith("0104") || mapeo.getArticulo().startsWith("0105") ) tipo="EMBOTELLADO"; else tipo=null;
			     	if (tipo==null && mapeo.getDescripcion().contains("S/E")) tipo="ETIQUETADO";
			     		
			     	cantidad = cps.obtenerSumaProgramacion(conMysql, r_ejercicio, r_semana, r_vector_padres, tipo);
			     	if (cantidad==null)
			     	{
			     		if (new Integer(r_semana)>52)
			     		{
			     			cantidad = cps.obtenerSumaProgramacion(conMysql, String.valueOf(new Integer(r_ejercicio) + 1) , "1", r_vector_padres, tipo);
			     			if (cantidad==null) cantidad=new Double(0);
			     		}
			     	}
				}
				
				articulo_old = mapeo.getArticulo();				
				mapeo.setNecesito(new Double(cantidad *  mapeo.getFactor()).intValue());			
				mapeo.setFaltansobran(mapeo.getExistencias()+mapeo.getPendienteRecibir()-mapeo.getNecesito());
				
				if (rsOpcion.getInt("esc_pre")!=0 && hashVectorPlazos!=null && hashVectorPlazos.containsKey(rsOpcion.getString("esc_art")))
				{
					mapeo.setFechaEntrega(hashVectorPlazos.get(rsOpcion.getString("esc_art")));
					
					if (hashVectorConfirmacionPlazos!=null && hashVectorConfirmacionPlazos.containsKey(rsOpcion.getString("esc_art")))
					{
						mapeo.setConfirmado(hashVectorConfirmacionPlazos.get(rsOpcion.getString("esc_art")));
					}
					else
					{
						mapeo.setConfirmado(null);
					}
				}
				else
				{
					mapeo.setFechaEntrega(null);
					mapeo.setConfirmado(null);
				}
				
				if (mapeo.getArticulo().startsWith("01193"))
					this.calcularDatosPallet(conMysql, con, r_ejercicio, r_semana, mapeo, r_vector_padres, "Botella");
				
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
				if (conMysql!=null)
				{
					conMysql.close();
					conMysql=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	private void calcularDatosPallet(Connection r_conMysql, Connection r_con, String r_ejercicio, String r_semana, MapeoAyudaProduccionEscandallo r_mapeo, ArrayList<String> r_vector_padres, String r_area)
	{
		Integer cantidad = null;
		Integer existencias = null;
		/*
		 * Calculo de la columna "necesito":
		 * 
		 * la cantidad programada divida por la paletizacion segun el ean devuelve el numero de palets
		 * esa cantidad redondeada al inmediato superior es la cantidad de palets que necesito.
		 * 
		 */
		consultaInventarioServer cis = consultaInventarioServer.getInstance(CurrentUser.get());
		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
		ArrayList<String> padresStock = ces.recuperarPadres(r_con, r_mapeo.getArticulo().trim());
		
		if (r_area.contentEquals("Botella"))
		{
			consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
			
			cantidad = cps.obtenerSumaProgramacionPallet(r_conMysql, r_ejercicio, r_semana, r_vector_padres);
			r_mapeo.setNecesito(cantidad.intValue());
			/* Calculo de la columna existencias.
			 * 
			 * Quiero mostrar en existencias el numero de palets vacios, que son los que puedo satisfacer la necesidad de produccion
			 * segun la programacion. Para eso realizamos los siguientes cálculos:
			 * 
			 * 		1.- sumando el stock de articulos que tengo en tipo de pallet, dividido por su paletizacion y redondeado al inmediato superior uno a uno
			 * 		me dan el total de palets llenos.
			 * 
			 * 		2.- restando los llenos del stock total obtengo los palets vacios
			 * 
			 */
			
				
				/*
				 * recojo el valor de palets llenos
				 */
				
				existencias=cis.obtenerSumaExistenciasUbicacion(r_con, padresStock, "4,6,7,10,12,20,30");
				Integer hay = r_mapeo.getExistencias();
				r_mapeo.setExistencias(hay-existencias);;
				
		}
		else
		{
			consultaProgramacionEnvasadoraServer cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			cantidad = cpes.obtenerSumaProgramacionPallet(r_conMysql, r_ejercicio, r_semana, r_vector_padres);
			r_mapeo.setNecesito(cantidad.intValue());
			
				/*
				 * recojo el valor de palets llenos
				 */
				existencias=cis.obtenerSumaExistenciasUbicacion(r_con, padresStock, "1,8");
				Integer hay = r_mapeo.getExistencias();
				r_mapeo.setExistencias(hay-existencias);

		}
		r_mapeo.setFaltansobran(r_mapeo.getExistencias()+r_mapeo.getPendienteRecibir()-cantidad.intValue());
		
	}
	
	public ArrayList<MapeoAyudaProduccionEscandallo> datosEscandalloAlmacen(ArrayList<String> r_vector, String r_ejercicio, String r_semana, boolean r_verTres, String r_almacenMP, String r_area)
	{
		Connection con = null;
		Statement cs = null;		

		ResultSet rsOpcion = null;
		ResultSet rsOpcion2 = null;
		Connection conMysql = null;
		Double cantidad = null;
		String articulo_old = "";
		ArrayList<MapeoAyudaProduccionEscandallo> vector = null;
		ArrayList<String> r_vector_padres = null;
		ArrayList<String> vectorTres = null;
		HashMap<String, String> hashVectorPlazos = null;
		HashMap<String, String> hashVectorConfirmacionPlazos = null;
		consultaProgramacionServer cps = null;
		consultaProgramacionEnvasadoraServer cpes = null;
		
		MapeoAyudaProduccionEscandallo mapeo = null; 
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacenesCalculo = cas.obtenerAlmacenesCalculoPendienteServir();

		try
		{

			if (r_area.contentEquals("Botella")) cps = consultaProgramacionServer.getInstance(CurrentUser.get());
			if (r_area.contentEquals("Mesa")) cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
//			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			consultaNoConformidadesServer cncs = consultaNoConformidadesServer.getInstance(CurrentUser.get());
			consultaArticulosIntranetServer cins = consultaArticulosIntranetServer.getInstance(CurrentUser.get());
			consultaPedidosComprasServer cpcs = consultaPedidosComprasServer.getInstance(CurrentUser.get());
			
			conMysql= this.conManager.establecerConexionInd();
			
			StringBuffer cadenaSQL = new StringBuffer();
			
			if (r_vector.isEmpty()) return null;

			hashVectorPlazos = cpcs.obtenerPlazosEntrega();
			hashVectorConfirmacionPlazos = cpcs.obtenerConfirmacionPlazos();
			
			if (r_verTres)
			{
				cadenaSQL.append(" SELECT articulo esc_art ");
				cadenaSQL.append(" FROM green.a_exis_0 a ");
				
				if (r_almacenMP.equals("1")) 
					cadenaSQL.append(" where a.almacen = 4 ");
				else
					cadenaSQL.append(" where a.almacen = 3 ");
				
				cadenaSQL.append(" and a.existencias <> 0 ");
				
				con= this.conManager.establecerConexionGestionInd();			
		     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
		     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
				
				vectorTres = new ArrayList<String>();
				
				while(rsOpcion.next())
				{
					vectorTres.add(rsOpcion.getString("esc_art"));
				}
				
				cadenaSQL = new StringBuffer();
			}
			
	     	if (r_almacenMP.equals("1"))
	     		cadenaSQL.append(" SELECT 'CAPUCHINOS' esc_alm, ");
	     	else if (r_almacenMP.equals("4")) 
	     		cadenaSQL.append(" SELECT 'TEJAR' esc_alm, ");	     	
			else
				cadenaSQL.append(" SELECT 'Global' esc_alm, ");
	     		
			cadenaSQL.append(" a.articulo esc_art, ");
	        cadenaSQL.append(" b.descrip_articulo esc_des, ");
	        cadenaSQL.append(" a.ubicacion esc_ubi, ");
	        cadenaSQL.append(" sum(a.existencias) esc_ex, ");	    
	        cadenaSQL.append(" sum(a.pdte_recibir) esc_pre ");
	        cadenaSQL.append(" FROM green.a_exis_0 a, green.e_articu b ");
	     	cadenaSQL.append(" where b.articulo = a.articulo ");
	     	cadenaSQL.append(" and a.articulo not matches '0101*' ");
	     	
	     	if (r_almacenMP.equals("1")) 
	     		cadenaSQL.append(" and a.almacen in (1) ");
	     	else if (r_almacenMP.equals("4")) 
	     		cadenaSQL.append(" and a.almacen in (4,10,20) ");	     	
			else
				cadenaSQL.append(" and a.almacen in " + almacenesCalculo);
	     	
	     	
	     	String padres = null;
	     	Integer j = 0;
	     	int partidor = 10;
	     	
	     	if (!r_vector.isEmpty() && r_vector.size()>0)
	     	{	     		
	     		padres="";
	     		if (r_vector.size()<partidor) partidor=r_vector.size(); 
	     		for (int i=0; i<r_vector.size();i++)
	     		{
	     			if (r_vector.get(i).trim().length()>0 )
	     			{
	     				if (padres!="") padres = padres + ",";
	     				padres=padres + "'" + r_vector.get(i).trim() + "'";
	     				j=j+1;
	     			}
	     			
	     			if (j==partidor && i == partidor-1 && padres !="")
	     			{
		     			cadenaSQL.append(" and a.articulo in (select hijo from e__lconj ");
		     			cadenaSQL.append(" where (padre in (" + padres + ") " );
		     			padres="";
	     			}
	     			else if (i>partidor && i%partidor==0 && padres !="")
	     			{
	     				cadenaSQL.append(" or padre in (" + padres + ") " );
	     				padres="";
	     			}
	     		}
	     		if (padres !="" && partidor!=r_vector.size()) 
	     			cadenaSQL.append(" or padre in (" + padres + ") " );
	     		else if (padres !="" )
	     		{
	     			cadenaSQL.append(" and a.articulo in (select hijo from e__lconj ");
     				cadenaSQL.append(" where (padre in (" + padres + ") " );
	     		}
	     		cadenaSQL.append("))");
	     	}
	     	
	     	
	     	cadenaSQL.append(" group by 1,2,3,4 ");
	     	cadenaSQL.append(" order by 2 ");

	     	if (con==null)
	     	{
	     		con= this.conManager.establecerConexionGestionInd();			
	     	}
	     	cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);

	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	     	rsOpcion = cs.executeQuery(cadenaSQL.toString());
		
			
			if (padres!=null)
			{
				StringBuffer sQL = new StringBuffer();
				sQL.append(" select unique padre esc_pad, ");
				sQL.append(" hijo esc_hij, ");
				sQL.append(" cantidad esc_can ");
				sQL.append(" from e__lconj ");
				j=0;
				padres="";
				for (int i=0; i<r_vector.size();i++)
	     		{
	     			if (r_vector.get(i).trim().length()>0 )
	     			{
	     				if (padres!="") padres = padres + ",";
	     				padres=padres + "'" + r_vector.get(i).trim() + "'";
	     				j=j+1;
	     			}
	     			
	     			if (j==partidor && i == partidor-1 && padres !="")
	     			{
		     			sQL.append(" where (padre in (" + padres + ") ");
		     			padres="";
	     			}
	     			else if (i>partidor && i%partidor==0 && padres !="")
	     			{
	     				sQL.append(" or padre in (" + padres + ") " );
	     				padres="";
	     			}
	     		}
	     		if (padres !="" && partidor!=r_vector.size()) 
	     			sQL.append(" or padre in (" + padres + ") " );
	     		else if (padres !="" )
	     		{	     			
     				sQL.append(" where (padre in (" + padres + ") " );
	     		}
	     		sQL.append(")");
				
	     		Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sQL.toString() + " : " + new Date());
				rsOpcion2 = cs.executeQuery(sQL.toString());				
			}
			
			vector = new ArrayList<MapeoAyudaProduccionEscandallo>();
			
			while(rsOpcion.next())
			{
				
				Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("esc_art").trim());
				
				mapeo = new MapeoAyudaProduccionEscandallo();				
				mapeo.setAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setArticulo(rsOpcion.getString("esc_art"));
				
				
				String ub = cins.obtenerUbicacion(rsOpcion.getString("esc_art"));
				if (ub!=null && ub.length()>0)
					mapeo.setUbicacion(ub);
				else
					if (!rsOpcion.getString("esc_art").startsWith("0102")) mapeo.setUbicacion(rsOpcion.getString("esc_ubi")); else mapeo.setUbicacion(null);
				
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("esc_des")));
				mapeo.setExistencias(rsOpcion.getInt("esc_ex"));
				mapeo.setPendienteRecibir(rsOpcion.getInt("esc_pre"));
				mapeo.setNombreAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setFactor(new Double(0));
				mapeo.setArticuloNC(cncs.hayNoConformidades(rsOpcion.getString("esc_art")));
				mapeo.setExistencias3(false);
				if (r_verTres) mapeo.setExistencias3(vectorTres.contains(rsOpcion.getString("esc_art")));
				
				if (!articulo_old.trim().equals(mapeo.getArticulo().trim()))
				{
					
			     	r_vector_padres = new ArrayList<String>();

			     	rsOpcion2.beforeFirst();
			     	
			     	while (rsOpcion2.next())
			     	{
			     		if (rsOpcion.getString("esc_art").trim().equals(rsOpcion2.getString("esc_hij").trim()))
	     				{
			     			r_vector_padres.add(rsOpcion2.getString("esc_pad").substring(0, 7));
			     			mapeo.setFactor(rsOpcion2.getDouble("esc_can"));
	     				}
			     	}
//			     	Double factor = ces.recuperarCantidadComponente(this.con, rsOpcion2.getString("esc_pad"), rsOpcion2.getString("esc_hij"));
//			     	if (factor==null) factor = new Double(0);
//			     	mapeo.setFactor(rsOpcion.getDouble("esc_can"));
//			     	mapeo.setFactor(factor);
			     	String tipo = null;
			     	
			     	if (mapeo.getArticulo().startsWith("0101") || mapeo.getArticulo().startsWith("0104") || mapeo.getArticulo().startsWith("0105") ) tipo="EMBOTELLADO"; else tipo=null;
			     	if (tipo==null && mapeo.getDescripcion().contains("S/E")) tipo="ETIQUETADO";
			     	
			     	if (r_area.equals("Botella"))
			     		cantidad = cps.obtenerSumaProgramacionAlmacen(conMysql, r_ejercicio, r_semana, r_vector_padres, tipo);
			     	else
			     		cantidad = cpes.obtenerSumaProgramacion(conMysql, r_ejercicio, r_semana, r_vector_padres);
			     	if (cantidad==null)
			     	{
			     		if (new Integer(r_semana)>52)
			     		{
			     			if (r_area.equals("Botella"))
			     				cantidad = cps.obtenerSumaProgramacionAlmacen(conMysql, String.valueOf(new Integer(r_ejercicio) + 1) , "1", r_vector_padres, tipo);
					     	else
					     		cantidad = cpes.obtenerSumaProgramacion(conMysql, String.valueOf(new Integer(r_ejercicio) + 1) , "1", r_vector_padres);
			     			
			     			if (cantidad==null) cantidad=new Double(0);
			     		}
			     	}
				}
				
				articulo_old = mapeo.getArticulo();		
				
				mapeo.setNecesito(new Double(cantidad *  mapeo.getFactor()).intValue());				
				mapeo.setFaltansobran(mapeo.getExistencias()+mapeo.getPendienteRecibir()-mapeo.getNecesito());
				
				if (rsOpcion.getInt("esc_pre")!=0 && hashVectorPlazos.containsKey(rsOpcion.getString("esc_art")))
				{
					mapeo.setFechaEntrega(hashVectorPlazos.get(rsOpcion.getString("esc_art")));
					mapeo.setConfirmado(hashVectorConfirmacionPlazos.get(rsOpcion.getString("esc_art")));
				}
				else
				{
						mapeo.setFechaEntrega(null);				
						mapeo.setConfirmado(null);
				}
				
				if (mapeo.getArticulo().startsWith("01193"))
					this.calcularDatosPallet(conMysql,con, r_ejercicio, r_semana, mapeo, r_vector_padres, r_area);
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (rsOpcion2!=null)
				{
					rsOpcion2.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conMysql!=null)
				{
					conMysql.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return vector;
	}

	public ArrayList<MapeoAyudaProduccionEscandallo> datosEscandalloAlmacen(String r_articulo, String r_ejercicio, String r_semana, String r_almacenMP, String r_area, String r_orden)
	{
		Connection con = null;
		Statement cs = null;		
		
		ResultSet rsOpcion = null;
		ResultSet rsOpcion2 = null;
		Connection conMysql = null;
		Double cantidad = null;
		ArrayList<MapeoAyudaProduccionEscandallo> vector = null;
		ArrayList<String> r_vector_padres = null;
		consultaProgramacionServer cps = null;
		consultaProgramacionEnvasadoraServer cpes = null;
		
		MapeoAyudaProduccionEscandallo mapeo = null; 
		consultaAlmacenesServer cas = consultaAlmacenesServer.getInstance(CurrentUser.get());
		String almacenesCalculo = cas.obtenerAlmacenesCalculoPendienteServir();
		String tipo="";
		try
		{
			if (r_articulo == null || r_articulo.length()==0) return null;
			r_vector_padres = new ArrayList<String>();
			r_vector_padres.add(r_articulo.substring(0, 7));
			
			if (r_area.contentEquals("Botella")) cps = consultaProgramacionServer.getInstance(CurrentUser.get());
			if (r_area.contentEquals("Mesa")) cpes = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
			consultaInventarioSGAServer csgas = consultaInventarioSGAServer.getInstance(CurrentUser.get());
			consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
			
			conMysql= this.conManager.establecerConexionInd();
			
			StringBuffer cadenaSQL = new StringBuffer();
			
			if (r_almacenMP.equals("1"))
				cadenaSQL.append(" SELECT 'CAPUCHINOS' esc_alm, ");
			else if (r_almacenMP.equals("4")) 
				cadenaSQL.append(" SELECT 'TEJAR' esc_alm, ");	     	
			else
				cadenaSQL.append(" SELECT 'Global' esc_alm, ");
			
			cadenaSQL.append(" a.articulo esc_art, ");
			cadenaSQL.append(" b.descrip_articulo esc_des, ");
			cadenaSQL.append(" a.ubicacion esc_ubi, ");
			cadenaSQL.append(" sum(a.existencias) esc_ex, ");	    
			cadenaSQL.append(" sum(a.pdte_recibir) esc_pre ");
			cadenaSQL.append(" FROM green.a_exis_0 a, green.e_articu b ");
			cadenaSQL.append(" where b.articulo = a.articulo ");
			cadenaSQL.append(" and a.articulo not matches '0101*' ");
			
			if (r_almacenMP.equals("1")) 
				cadenaSQL.append(" and a.almacen in (1) ");
			else if (r_almacenMP.equals("4")) 
				cadenaSQL.append(" and a.almacen in (4,10,20) ");	     	
			else
				cadenaSQL.append(" and a.almacen in " + almacenesCalculo);
			
			
			cadenaSQL.append(" and a.articulo not matches '01193*' ");
			cadenaSQL.append(" and a.articulo in (select hijo from e__lconj ");
			cadenaSQL.append(" where padre in ('" + r_articulo + "') and hijo not matches '0102*') " );
			cadenaSQL.append(" group by 1,2,3,4 ");
			cadenaSQL.append(" order by 2 ");
			
			if (con==null)
			{
				con= this.conManager.establecerConexionGestionInd();			
			}
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion = cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoAyudaProduccionEscandallo>();
			
			while(rsOpcion.next())
			{
				
				Notificaciones.getInstance().mensajeSeguimiento(rsOpcion.getString("esc_art").trim());
				
				mapeo = new MapeoAyudaProduccionEscandallo();				
				mapeo.setAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setNombreAlmacen(rsOpcion.getString("esc_alm"));
				mapeo.setArticulo(rsOpcion.getString("esc_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("esc_des")));
				mapeo.setExistencias(rsOpcion.getInt("esc_ex"));
				mapeo.setPendienteRecibir(rsOpcion.getInt("esc_pre"));
				
				MapeoInventarioSGA mapeoInventarioSGA = csgas.consultaUbicacionesArticulos(mapeo.getArticulo().trim());
				MapeoInventarioSGA mapeoInventarioSGA2 = csgas.consultaWipInArticulos(mapeo.getArticulo().trim());
				MapeoInventarioSGA mapeoInventarioSGA3 = csgas.consultaProduccionOrden(r_orden.trim());
				
				if (mapeoInventarioSGA!=null)
				{
					mapeo.setUbicacion(mapeoInventarioSGA.getUbicacion());
					mapeo.setConfirmado(mapeoInventarioSGA.getLpn());
				}
				else
				{
					mapeo.setUbicacion(null);
					mapeo.setConfirmado(null);
				}

				if (mapeoInventarioSGA2!=null)
				{
					mapeo.setFaltansobran(mapeoInventarioSGA2.getStock_actual());
				}
				else
				{
					mapeo.setFaltansobran(0);
				}

				mapeo.setFactor(new Double(0));
				mapeo.setExistencias3(false);
				
				if (mapeo.getArticulo().startsWith("0101") || mapeo.getArticulo().startsWith("0104") || mapeo.getArticulo().startsWith("0105") ) tipo="EMBOTELLADO"; else tipo=null;
				if (tipo==null && mapeo.getDescripcion().contains("S/E")) tipo="ETIQUETADO";
					
				if (r_area.equals("Botella"))
					cantidad = cps.obtenerSumaProgramacionAlmacen(conMysql, r_ejercicio, r_semana, r_vector_padres, tipo);
				else
					cantidad = cpes.obtenerSumaProgramacion(conMysql, r_ejercicio, r_semana, r_vector_padres);
				if (cantidad==null)
				{
					if (new Integer(r_semana)>52)
					{
						if (r_area.equals("Botella"))
							cantidad = cps.obtenerSumaProgramacionAlmacen(conMysql, String.valueOf(new Integer(r_ejercicio) + 1) , "1", r_vector_padres, tipo);
						else
							cantidad = cpes.obtenerSumaProgramacion(conMysql, String.valueOf(new Integer(r_ejercicio) + 1) , "1", r_vector_padres);
						
						if (cantidad==null) cantidad=new Double(0);
					}
				}
				mapeo.setFactor(ces.recuperarCantidadComponente(con, mapeo.getArticulo()));
				mapeo.setNecesito(new Double(cantidad *  mapeo.getFactor()).intValue());				

				if (mapeoInventarioSGA3!=null)
				{
					mapeo.setNecesito(new Double(cantidad *  mapeo.getFactor()).intValue() - new Double(mapeoInventarioSGA3.getStock_actual()* mapeo.getFactor()).intValue());
				}
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (rsOpcion2!=null)
				{
					rsOpcion2.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
				if (conMysql!=null)
				{
					conMysql.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		return vector;
	}
	
	
	@Override
	public String semaforos() {
		
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	ArrayList<MapeoProgramacion> datosProgramados = null;
    	ArrayList<String> articulos = null;
    	MapeoProgramacion mp = new MapeoProgramacion();
		boolean hayProgramacion = false;
		
    	consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
    	
    	mp.setSemana(String.valueOf(RutinasFechas.semanaActual(null)));
    	mp.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
    	
    	datosProgramados = cps.datosProgramacionTotal(mp.getEjercicio(),mp.getSemana());
    	
    	articulos = new ArrayList<String>();
    	
    	for (int i = 0; i<datosProgramados.size();i++)
    	{
    		String articulo = "";
    		
    		if (((MapeoProgramacion) datosProgramados.get(i)).getTipo().toUpperCase().equals("ETIQUETADO"))
    		{
    			articulo=((MapeoProgramacion) datosProgramados.get(i)).getArticulo() + "-1";
    		}
    		else 
    		{
    			articulo=((MapeoProgramacion) datosProgramados.get(i)).getArticulo();
    		}
    		
    		if (!articulos.contains(articulo) && articulo.length()>0) articulos.add(articulo);
    		if (articulo.trim().length()>0) hayProgramacion=true;
    	}
    	
    	if (hayProgramacion)
    	{
	    	r_vector2=this.datosEscandalloGlobal(articulos, RutinasFechas.añoActualYYYY(), String.valueOf(RutinasFechas.semanaActual(null)),false);
	    	if(r_vector2!=null)
	    	{
		    	for (int i = 0; i<r_vector2.size();i++)
		    	{
		    		if (((MapeoAyudaProduccionEscandallo) r_vector2.get(i)).getExistencias()<((MapeoAyudaProduccionEscandallo) r_vector2.get(i)).getNecesito())
		    		{
		    			return "2";
		    		}
		    	}
	    	}
    	}
    	return "0";
	}
}
