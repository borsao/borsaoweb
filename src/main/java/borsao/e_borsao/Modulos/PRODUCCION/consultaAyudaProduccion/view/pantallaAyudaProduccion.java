package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.AyudaProduccionEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.AyudaProduccionStockGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionStock;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAyudaProduccion extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridPadre= null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	private TextField cantidad= null;
	private boolean hayGridPadre = false;
	private boolean hayGridComponentes= false;
	private VerticalLayout frameCentral = null;	
	private String articuloConsultado = null;
	
	public pantallaAyudaProduccion(String r_titulo, Integer r_cantidad, String r_articulo)
	{
		
		this.setCaption(r_titulo);
		this.articuloConsultado=r_articulo;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1800px");
		this.setHeight("750px");
		
		this.cargarPantalla(r_articulo, r_cantidad);
		this.cargarListeners();
		this.llenarRegistros();
		
		
		this.setContent(principal);

	}

	private void cargarPantalla(String r_articulo, Integer r_cantidad)
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

				HorizontalLayout frameCabecera = new HorizontalLayout();
//				frameCabecera.setWidth("100%");
				frameCabecera.setHeight("1%");
				
				this.frameCentral = new VerticalLayout();
		
					this.cantidad=new TextField();
					this.cantidad.setEnabled(true);
					this.cantidad.setCaption("A Fabricar");
					this.cantidad.addStyleName("v-datefield-textfield");
					this.cantidad.setValue(r_cantidad.toString());
					
					frameCentral.addComponent(this.cantidad);
		
				HorizontalLayout framePie = new HorizontalLayout();
//				framePie.setWidthUndefined();
//				framePie.setHeight("100px");
		
					btnBotonCVentana = new Button("Cerrar");
					btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
					framePie.addComponent(btnBotonCVentana);		
					framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
//				frameCentral.setWidthUndefined();
//				framePie.setHeight("550px");
		
//		principal.addComponent(frameCabecera);
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
//		principal.setComponentAlignment(frameCabecera, Alignment.TOP_LEFT);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.cantidad.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGridPadre())
				{			
					if (cantidad.getValue()!=null && cantidad.getValue().length()>0) 
					{
						gridPadre.removeAllColumns();			
						frameCentral.removeComponent(gridPadre);
						gridPadre=null;			
						if (isHayGridComponentes())
						{
							gridComponentes.removeAllColumns();			
							frameCentral.removeComponent(gridComponentes);
							gridComponentes=null;
						}		
						llenarRegistros();
					}
				}
			}
		}); 
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	ArrayList<MapeoAyudaProduccionStock> r_vector=null;
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	
    	consultaAyudaProduccionServer caps = new consultaAyudaProduccionServer(CurrentUser.get());
    	
    	r_vector=caps.datosStock(this.articuloConsultado.trim().substring(0, 7));
    	r_vector2=caps.datosEscandallo(this.articuloConsultado.trim(), new Integer(this.cantidad.getValue()));
    	
    	this.presentarGrid(r_vector, r_vector2);

	}

    private void presentarGrid(ArrayList<MapeoAyudaProduccionStock> r_vector, ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
    {
    	
    	gridPadre = new AyudaProduccionStockGrid(r_vector);
    	
    	if (((AyudaProduccionStockGrid) gridPadre).vector==null)
    	{
    		setHayGridPadre(false);
    	}
    	else 
    	{
    		setHayGridPadre(true);
    	}
    	
    	if (isHayGridPadre())
    	{
    		
    		gridComponentes = new AyudaProduccionEscandalloGrid(r_vector2);
    		if (((AyudaProduccionEscandalloGrid) gridComponentes).vector==null)
        	{
        		setHayGridComponentes(false);
        	}
        	else
        	{
        		setHayGridComponentes(true);
        	}
    		
    		if (isHayGridComponentes())
    		{
    			this.gridPadre.setHeight("100px");
    			this.gridPadre.setWidth("100%");
    			this.frameCentral.addComponent(gridPadre);
//    			this.frameCentral.setExpandRatio(gridPadre,1 );
    			this.gridComponentes.setHeight("400px");
    			this.gridComponentes.setWidth("100%");
    			this.frameCentral.addComponent(gridComponentes);
    			
    			this.frameCentral.setComponentAlignment(gridPadre, Alignment.TOP_LEFT);
    			this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
//    			this.frameCentral.setMargin(true);
    		}
    	}
    }

//	private void llenarRegistros(ArrayList<MapeoAyudaProduccionStock> r_vector, ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
//	{
//		Iterator iterator = null;
//		MapeoLineasPedidosVentas mapeoAyuda = null;
//		iterator = r_vector.iterator();
//		
//		container = new IndexedContainer();
//		container.addContainerProperty("Fecha Documento", String.class, null);
//		container.addContainerProperty("Plazo Entrega", String.class, null);
//		container.addContainerProperty("Cliente", String.class, null);
//		container.addContainerProperty("Nombre", String.class, null);
//		container.addContainerProperty("Unidades", Integer.class, null);
//		container.addContainerProperty("Servidas", Integer.class, null);
//		container.addContainerProperty("Pendientes", Integer.class, null);
//		container.addContainerProperty("Stock", Integer.class, null);
//        container.addContainerProperty("Ejercicio", Integer.class, null);
//        container.addContainerProperty("Documento", String.class, null);
//        container.addContainerProperty("Codigo", String.class, null);
////        container.addContainerProperty("Articulo", String.class, null);
////        container.addContainerProperty("Descripcion", String.class, null);
//        
//		while (iterator.hasNext())
//		{
//			Object newItemId = container.addItem();
//			Item file = container.getItem(newItemId);
//			
//			mapeoAyuda = (MapeoLineasPedidosVentas) iterator.next();
//			
////			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
////    		file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
//    		file.getItemProperty("Nombre").setValue(RutinasCadenas.conversion(mapeoAyuda.getNombreCliente()));
//    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
//    		
//    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
//    		file.getItemProperty("Plazo Entrega").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getPlazoEntrega()));
//    		
//    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
//    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoVenta().toString());
//    		file.getItemProperty("Cliente").setValue(mapeoAyuda.getCliente().toString());
//    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
//    		file.getItemProperty("Servidas").setValue(mapeoAyuda.getUnidades_serv());
//    		file.getItemProperty("Pendientes").setValue(mapeoAyuda.getUnidades_pdte());
//    		file.getItemProperty("Stock").setValue(mapeoAyuda.getStock_queda());    		
//		}
//
//		this.gridDatos= new Grid(container);
//		this.gridDatos.setSelectionMode(SelectionMode.NONE);
//
//		this.asignarEstilos();
//	}
//	public void asignarEstilos()
//    {
//		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
//    	{
//            public String getStyle(Grid.CellReference cellReference) 
//            {
//            	if ("Unidades".equals(cellReference.getPropertyId()) || "Servidas".equals(cellReference.getPropertyId()) || "Pendientes".equals(cellReference.getPropertyId()) || "Stock".equals(cellReference.getPropertyId())
//            			|| "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId()) || "Cliente".equals(cellReference.getPropertyId())) 
//            	{
//            		if ("Stock".equals(cellReference.getPropertyId()) && new Integer(cellReference.getValue().toString()) < 0)
//            		{
//            			return "Rcell-error";
//            		}        			
//        			else 
//        			{
//        				return "Rcell-normal";
//        			}
//            	}
//            	else
//            	{
//            		return "cell-normal";
//            	}
//            }
//        });
//    }

	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
}