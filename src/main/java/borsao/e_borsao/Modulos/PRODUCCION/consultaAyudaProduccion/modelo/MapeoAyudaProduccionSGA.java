package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAyudaProduccionSGA extends MapeoGlobal
{
	private String orden = null; 
	private String pick = null; 
	private String articulo = null; 
	private String descripcion = null; 
	private String lpn = null;
	private String lote = null;
	private String gtin = null;
	private String ubicacion = null;
	private String pt = null;
	private Integer cantidad = null;
	
	
	public MapeoAyudaProduccionSGA()
	{
	}


	public String getOrden() {
		return orden;
	}


	public String getPick() {
		return pick;
	}


	public String getArticulo() {
		return articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public String getLpn() {
		return lpn;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public Integer getCantidad() {
		return cantidad;
	}


	public void setOrden(String orden) {
		this.orden = orden;
	}


	public void setPick(String pick) {
		this.pick = pick;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public void setLpn(String lpn) {
		this.lpn = lpn;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


	public String getGtin() {
		return gtin;
	}


	public void setGtin(String gtin) {
		this.gtin = gtin;
	}


	public String getPt() {
		return pt;
	}


	public void setPt(String pt) {
		this.pt = pt;
	}


	public String getLote() {
		return lote;
	}


	public void setLote(String lote) {
		this.lote = lote;
	}
		
}