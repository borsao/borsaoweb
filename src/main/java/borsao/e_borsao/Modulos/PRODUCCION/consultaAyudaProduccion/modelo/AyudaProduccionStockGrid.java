package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.view.pantallaLineasPedidosVentas;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMCompras;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaProduccionStockGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public AyudaProduccionStockGrid(ArrayList<MapeoAyudaProduccionStock> r_vector) 
    {
        this.vector=r_vector;
        
		this.asignarTitulo("Ayuda Produccion");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoAyudaProduccionStock.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    

    
    public void establecerOrdenPresentacionColumnas()
    {

    	setColumnOrder("articulo", "descripcion", "existencias", "pendienteServir", "pendientesPreparar", "preparadas", "stockTotal", "stockReal");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("existencias").setHeaderCaption("Existencias");
    	this.getColumn("existencias").setWidth(150);
    	this.getColumn("pendienteServir").setHeaderCaption("Pdte. Servir");
    	this.getColumn("pendienteServir").setWidth(150);
    	this.getColumn("preparadas").setHeaderCaption("Preparadas");
    	this.getColumn("preparadas").setWidth(150);
    	this.getColumn("pendientesPreparar").setHeaderCaption("Pdte. Preparar");
    	this.getColumn("pendientesPreparar").setWidth(150);
    	this.getColumn("stockTotal").setHeaderCaption("Stock Total");
    	this.getColumn("stockTotal").setWidth(150);
    	this.getColumn("stockReal").setHeaderCaption("Stock Real");
    	this.getColumn("stockReal").setWidth(150);

    	this.getColumn("idCodigo").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("stockReal".equals(cellReference.getPropertyId()) || "stockTotal".equals(cellReference.getPropertyId()) || 
            			"pendientesPreparar".equals(cellReference.getPropertyId()) || 
            			"preparadas".equals(cellReference.getPropertyId()) ) 
            	{
    				return "Rcell-normal";
            	}
    			else if ("existencias".equals(cellReference.getPropertyId()) || "pendienteServir".equals(cellReference.getPropertyId()))
    			{
    				return "Rcell-pointer";
    			}
    			else if ("articulo".equals(cellReference.getPropertyId()))
    			{
    				return "cell-pointer";
    			}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

    public void cargarListeners()
    {
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId().toString().equals("pendienteServir"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionStock mapeo = (MapeoAyudaProduccionStock) event.getItemId();
            		
            		pantallaLineasPedidosVentas vt = new pantallaLineasPedidosVentas(mapeo.getArticulo(), mapeo.getExistencias() + mapeo.getPreparadas(), "Lineas Pedidos Venta del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
        			getUI().addWindow(vt);
            		
            	}
            	else if (event.getPropertyId().toString().equals("existencias"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionStock mapeo = (MapeoAyudaProduccionStock) event.getItemId();
            		pantallaStocksLotes vt = new pantallaStocksLotes("Consulta Stock articulo " + mapeo.getArticulo().trim(), mapeo.getArticulo().trim(),null);
        			getUI().addWindow(vt);
            	}
            	else if (event.getPropertyId().toString().equals("articulo"))
            	{
            		/*
            		 * acceso a pendientes de servir
            		 */
            		
            		MapeoAyudaProduccionStock mapeo = (MapeoAyudaProduccionStock) event.getItemId();
            		if (mapeo!=null && mapeo.getArticulo()!=null && (mapeo.getArticulo().trim().startsWith("0102") || mapeo.getArticulo().trim().startsWith("0103") || mapeo.getArticulo().trim().startsWith("0111")))
            		{
            			pantallaCRMArticulo vt = new pantallaCRMArticulo("CRM Articulo", mapeo.getArticulo());
            			if (vt.acceso)
            				getUI().addWindow(vt);
            			else 
            				vt=null;
            		}
            		else
            		{
            			pantallaCRMCompras vt = new pantallaCRMCompras("CRM Compras", mapeo.getArticulo());
            			if (vt.acceso)
            				getUI().addWindow(vt);
            			else 
            				vt=null;
            		}
            	}
            }
        });
    }

	@Override
	public void calcularTotal() {
		
	}
}


