package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoAyudaProduccionStock extends MapeoGlobal
{
	private String articulo = null; 
	private String descripcion = null; 
	private Integer existencias = null;
	private Integer pendienteServir = null;
	private Integer preparadas = null;
	private Integer pendientesPreparar = null;
	private Integer stockTotal = null;
	private Integer stockReal = null;
	
	public MapeoAyudaProduccionStock()
	{
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	public Integer getPendienteServir() {
		return pendienteServir;
	}

	public void setPendienteServir(Integer pendienteServir) {
		this.pendienteServir = pendienteServir;
	}

	public Integer getPreparadas() {
		return preparadas;
	}

	public void setPreparadas(Integer preparadas) {
		this.preparadas = preparadas;
	}

	public Integer getPendientesPreparar() {
		return pendientesPreparar;
	}

	public void setPendientesPreparar(Integer pendientesPreparar) {
		this.pendientesPreparar = pendientesPreparar;
	}

	public Integer getStockTotal() {
		return stockTotal;
	}

	public void setStockTotal(Integer stockTotal) {
		this.stockTotal = stockTotal;
	}

	public Integer getStockReal() {
		return stockReal;
	}

	public void setStockReal(Integer stockReal) {
		this.stockReal = stockReal;
	}
	
}