package borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.AyudaProduccionEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.AyudaProduccionStockGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionStock;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class AyudaProduccionView extends GridViewRefresh {

	public static final String VIEW_NAME = "Ayuda Produccion";
	public consultaAyudaProduccionServer caps =null;
	public TextFieldAyuda articulo = null;
	public TextField cantidad= null;
	
	public Grid grid2 = null;
	
	private final String titulo = "AYUDA PRODUCCION";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	private boolean hayGrid2 = false;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public AyudaProduccionView()
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.cantidad=new TextField();
		this.cantidad.setEnabled(true);
		this.cantidad.setCaption("A Fabricar");
		this.cantidad.addStyleName("v-datefield-textfield");
		this.cantidad.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.cantidad.setValue("0");
		
    			
		this.articulo= new TextFieldAyuda("Articulo","");
    		
		this.cabLayout.addComponent(this.articulo);
		this.cabLayout.addComponent(this.cantidad);
    		
		this.cargarListeners();
    		
		this.generarGrid();
    }

    public void generarGrid()
    {
    	ArrayList<MapeoAyudaProduccionStock> r_vector=null;
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	
    	caps = new consultaAyudaProduccionServer(CurrentUser.get());
    	
    	if (this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0)
    	{
    		r_vector=this.caps.datosStock(this.articulo.txtTexto.getValue().substring(0, 7));
    		r_vector2=this.caps.datosEscandallo(this.articulo.txtTexto.getValue(), new Integer(this.cantidad.getValue()));
    	}
    	else
    	{
    		r_vector=new ArrayList<MapeoAyudaProduccionStock>();
        	r_vector2 = new ArrayList<MapeoAyudaProduccionEscandallo>();
    	}
    	
    	this.presentarGrid(r_vector, r_vector2);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoAyudaProduccionStock> r_vector, ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
    {
    	
    	grid = new AyudaProduccionStockGrid(r_vector);
    	if (((AyudaProduccionStockGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    	}
    	else if (((AyudaProduccionStockGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    	}
    	else
    	{
    		barAndGridLayout.setSpacing(false);
//    		grid.setHeight("20%");
    		setHayGrid(true);
    	}
    	
    	if (isHayGrid())
    	{
    		
    		grid2 = new AyudaProduccionEscandalloGrid(r_vector2);
    		if (((AyudaProduccionEscandalloGrid) grid2).vector==null)
        	{
        		setHayGrid2(false);
        	}
        	else if (((AyudaProduccionEscandalloGrid) grid2).vector.isEmpty())
        	{
        		setHayGrid2(true);
        	}
        	else
        	{
        		setHayGrid2(true);
//        		grid.setHeight("80%");
        	}
    		
    		if (isHayGrid2())
    		{
    			barAndGridLayout.addComponent(grid2);
        		barAndGridLayout.setExpandRatio(grid2,3 );
        		barAndGridLayout.setMargin(true);
    		}
    	}
    }
    
    private void cargarListeners()
    {

		this.articulo.txtTexto.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 && new Integer(cantidad.getValue())>0) 
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;			
						if (isHayGrid2())
						{
							grid2.removeAllColumns();			
							barAndGridLayout.removeComponent(grid2);
							grid2=null;
						}
						generarGrid();
					}
				}
			}
		}); 
		this.cantidad.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 && cantidad.getValue()!=null && cantidad.getValue().length()>0) 
					{
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;			
						if (isHayGrid2())
						{
							grid2.removeAllColumns();			
							barAndGridLayout.removeComponent(grid2);
							grid2=null;
						}		
						generarGrid();
					}
				}
			}
		}); 

    }
    

	private void navegacion(boolean r_navegar)
	{
		this.articulo.setVisible(r_navegar);
		this.cantidad.setVisible(r_navegar);
	}
	
	@Override
	public void reestablecerPantalla() {
		
	}
	
	@Override
	public void print() {
		
	}
	
	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}
	
	@Override
	public void newForm() {
		
	}
	
	@Override
	public void verForm(boolean r_busqueda) {
		
	}
	
	public boolean isHayGrid2() {
		return hayGrid2;
	}
	
	public void setHayGrid2(boolean hayGrid2) {
		this.hayGrid2 = hayGrid2;
	}
	
	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
