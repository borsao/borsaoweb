package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoProduccionDiaria extends MapeoGlobal
{
	private String usuario = null;
	private String movimiento = null; 
	private String articulo = null; 
	private String turno = null;
	private String descripcion = null; 
	private Integer unidades = null;
	private Integer unidadesEntrada = null;
	private Integer unidadesSalida = null;
	private String numero_lote = null; 
	private String ubicacion = null; 
	private String hora_ult_modif = null; 
	private String numeroPalet = null;
	private String dia = null;
	private String mes = null;
	
	public MapeoProduccionDiaria()
	{
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getNumero_lote() {
		return numero_lote;
	}

	public void setNumero_lote(String numero_lote) {
		this.numero_lote = numero_lote;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getHora_ult_modif() {
		return hora_ult_modif;
	}

	public void setHora_ult_modif(String hora_ult_modif) {
		this.hora_ult_modif = hora_ult_modif;
	}

	public String getNumeroPalet() {
		return numeroPalet;
	}

	public void setNumeroPalet(String numeroPalet) {
		this.numeroPalet = numeroPalet;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}

	public String getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(String movimiento) {
		this.movimiento = movimiento;
	}

	public Integer getUnidadesEntrada() {
		return unidadesEntrada;
	}

	public void setUnidadesEntrada(Integer unidadesEntrada) {
		this.unidadesEntrada = unidadesEntrada;
	}

	public Integer getUnidadesSalida() {
		return unidadesSalida;
	}

	public void setUnidadesSalida(Integer unidadesSalida) {
		this.unidadesSalida = unidadesSalida;
	}
	
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}


}