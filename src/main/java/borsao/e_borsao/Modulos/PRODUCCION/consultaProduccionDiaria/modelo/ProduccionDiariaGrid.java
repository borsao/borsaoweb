package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaTotalesProduccionDiaria;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ProduccionDiariaGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public ProduccionDiariaGrid(ArrayList<MapeoProduccionDiaria> r_vector) 
    {
        this.vector=r_vector;
        
		this.asignarTitulo("Produccion Diaria");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoProduccionDiaria.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    

    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("turno", "movimiento", "hora_ult_modif", "articulo", "descripcion", "numero_lote", "ubicacion", "numeroPalet", "unidades", "unidadesEntrada", "unidadesSalida");    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("turno").setHeaderCaption("T.");
    	this.getColumn("turno").setSortable(false);
    	this.getColumn("turno").setWidth(new Double(50));
    	this.getColumn("movimiento").setHeaderCaption("M.");
    	this.getColumn("movimiento").setSortable(false);
    	this.getColumn("movimiento").setWidth(new Double(50));
    	this.getColumn("hora_ult_modif").setHeaderCaption("Hora");
    	this.getColumn("hora_ult_modif").setSortable(false);
    	this.getColumn("hora_ult_modif").setWidth(new Double(100));
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("numero_lote").setHeaderCaption("Lote");
    	this.getColumn("numero_lote").setWidth(150);
    	this.getColumn("ubicacion").setHeaderCaption("Añada");
    	this.getColumn("ubicacion").setWidth(100);
    	this.getColumn("numeroPalet").setHeaderCaption("N. Palet");
    	this.getColumn("numeroPalet").setWidth(100);
    	this.getColumn("unidades").setHeaderCaption("Cantidad");
    	this.getColumn("unidades").setWidth(150);
    	this.getColumn("unidadesEntrada").setHeaderCaption("Entradas");
    	this.getColumn("unidadesEntrada").setWidth(150);
    	this.getColumn("unidadesSalida").setHeaderCaption("Salidas");
    	this.getColumn("unidadesSalida").setWidth(150);
    	
    	this.getColumn("usuario").setHidden(true);
    	this.getColumn("dia").setHidden(true);
    	this.getColumn("mes").setHidden(true);    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("unidades").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("hora_ult_modif".equals(cellReference.getPropertyId()) || "numeroPalet".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) || "unidadesEntrada".equals(cellReference.getPropertyId()) || "unidadesSalida".equals(cellReference.getPropertyId())) 
            	{
    				return "Rcell-normal";
            	}
            	else if ("articulo".equals(cellReference.getPropertyId()) || "numero_lote".equals(cellReference.getPropertyId()) || "ubicacion".equals(cellReference.getPropertyId())) 
            	{
            		return "cell-pointer";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {

				if (event.getPropertyId().toString().equals("articulo"))
		    	{
		    		
		    		MapeoProduccionDiaria mapeo = (MapeoProduccionDiaria) event.getItemId();
		    		
		    		pantallaTotalesProduccionDiaria vt = new pantallaTotalesProduccionDiaria(mapeo.getDia(), mapeo.getUsuario(), mapeo.getArticulo().trim(), null, null, "Totales por Articulo " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
					getUI().addWindow(vt);
		    		
		    	}
		    	else if (event.getPropertyId().toString().equals("numero_lote"))
		    	{
		    		MapeoProduccionDiaria mapeo = (MapeoProduccionDiaria) event.getItemId();
		    		
		    		pantallaTotalesProduccionDiaria vt = new pantallaTotalesProduccionDiaria(mapeo.getDia(), mapeo.getUsuario(), mapeo.getArticulo().trim(), mapeo.getNumero_lote().trim(), null, "Totales por Articulo " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
					getUI().addWindow(vt);
		    		
		    	}
		    	else if (event.getPropertyId().toString().equals("ubicacion"))
		    	{
		    		MapeoProduccionDiaria mapeo = (MapeoProduccionDiaria) event.getItemId();
		    		
		    		pantallaTotalesProduccionDiaria vt = new pantallaTotalesProduccionDiaria(mapeo.getDia(), mapeo.getUsuario(), mapeo.getArticulo().trim(), mapeo.getNumero_lote().trim(), mapeo.getUbicacion().trim(), "Totales por Articulo " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
					getUI().addWindow(vt);
		    	}
            }
        });
	}

	@Override
	public void calcularTotal() {
		
	}
}


