package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view;

import java.util.ArrayList;
import java.util.Date;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.SelectionEvent;
import com.vaadin.event.SelectionEvent.SelectionListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.MapeoProduccionDiaria;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.ProduccionDiariaGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaProduccionDiaria extends Window{

	public consultaProduccionDiariaServer cpds =null;
	public ComboBox cmbLinea = null;
	public EntradaDatosFecha txtFecha= null;
	
	private VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	private Grid grid = null;
	
	private Button procesar=null;
	private final String titulo = "PRODUCCION DIARIA";
	private boolean hayGrid=false;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaProduccionDiaria() 
    {
		this.setCaption(titulo);
		this.center();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);
    	
    }

    public void cargarPantalla() 
    {
    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);
    	
		this.txtFecha=new EntradaDatosFecha();
		this.txtFecha.setEnabled(true);
		this.txtFecha.setCaption("Fecha Consulta");
		this.txtFecha.setValue(new Date());
    			
		this.cmbLinea= new ComboBox("Linea");    		
		this.cmbLinea.setNewItemsAllowed(false);
		this.cmbLinea.addItem("Embotelladora");
		this.cmbLinea.addItem("Envasadora");
		this.cmbLinea.addItem("BIB");
		this.cmbLinea.setNullSelectionAllowed(false);
		this.cmbLinea.setValue("Embotelladora");
		this.cmbLinea.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.procesar=new Button("Procesar");
    	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.procesar.setIcon(FontAwesome.PRINT);

		this.cabLayout.addComponent(this.txtFecha);
		this.cabLayout.addComponent(this.cmbLinea);
		this.cabLayout.addComponent(this.procesar);
		
		this.cabLayout.setComponentAlignment(this.procesar, Alignment.BOTTOM_LEFT);
    		
		this.barAndGridLayout.addComponent(this.cabLayout);
		
		this.presentarGrid(new ArrayList<MapeoProduccionDiaria>());
    }

    public void generarGrid()
    {
    	ArrayList<MapeoProduccionDiaria> r_vector=null;
    	cpds = new consultaProduccionDiariaServer(CurrentUser.get());

    	r_vector=this.cpds.datosGlobal(this.cmbLinea.getValue().toString(), this.txtFecha.getValue());
    	this.presentarGrid(r_vector);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoProduccionDiaria> r_vector)
    {
    	
    	grid = new ProduccionDiariaGrid(r_vector);
    	if (((ProduccionDiariaGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    	}
    	else if (((ProduccionDiariaGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {

    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbLinea.getValue()!=null && cmbLinea.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
    		}
    	});

		this.cmbLinea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbLinea.getValue()!=null && cmbLinea.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
			}
		}); 
		this.txtFecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (txtFecha.getValue()!=null && txtFecha.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
			}
		}); 

    }
    
    
	private boolean isHayGrid() {
		return hayGrid;
	}

	private void setHayGrid(boolean r_hayGrid) {
		this.hayGrid = r_hayGrid;
		if (r_hayGrid)
		{
    		barAndGridLayout.addComponent(grid);
    		barAndGridLayout.setExpandRatio(grid, 1);
    		barAndGridLayout.setMargin(true);

			grid.addSelectionListener(new SelectionListener() {
				
				@Override
				public void select(SelectionEvent event) {
						
//					if (grid.getSelectionModel() instanceof SingleSelectionModel)
//						filaSeleccionada(grid.getSelectedRow());
//					else
//						mostrarFilas(grid.getSelectedRows());
				}
			});
		}
		else
		{
		}
	}
}
