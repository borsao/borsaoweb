package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ReadOnlyException;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.filter.SimpleStringFilter;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.LABORATORIO.SituacionMezclasVinos.view.pantallaLineasDetalleMezclaVinos;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.MapeoProduccionDiaria;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasProduccion extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	private String articuloSeleccionado = null;
	private Integer id = null;
	private Integer semana = null;
	private Integer ejercicio = null;
	private String articuloMP = null;
	private ArrayList<String> padresSeleccionados = null;
	private pantallaLineasDetalleMezclaVinos app = null;
	private String area = null;
	private String fecha = null;
	private String tipo = null;
	private String anada = null;
	private boolean porMeses = false;
	public ArrayList<String> camposNoFiltrar = new ArrayList<String>();
	public HashMap<String,String> widthFiltros = new HashMap<String, String>();

	
	public pantallaLineasProduccion(String r_area, Integer r_ejercicio, String r_semana, String r_titulo, String r_calculo)
	{
		this.area = r_area;
		this.semana = new Integer(r_semana);
		this.ejercicio=r_ejercicio;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE, r_calculo);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public pantallaLineasProduccion(String r_area, Integer r_ejercicio, String r_articulo)
	{
		this.area = r_area;
		this.ejercicio=r_ejercicio;
		this.articuloSeleccionado=r_articulo;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE,null);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasProduccion(String r_area, Integer r_ejercicio, String r_articulo, String r_fecha)
	{
		this.area = r_area;
		this.fecha = r_fecha;
		this.ejercicio=r_ejercicio;
		this.articuloSeleccionado=r_articulo;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE,null);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	
	public pantallaLineasProduccion(String r_tipo, String r_area, Integer r_ejercicio, String r_articulo, boolean r_porMeses)
	{
		this.area = r_area;
		this.tipo=r_tipo;
		this.ejercicio=r_ejercicio;
		this.articuloSeleccionado=r_articulo;
		this.porMeses = r_porMeses;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE,null);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	public pantallaLineasProduccion(String r_tipo, String r_area, Integer r_ejercicio, String r_articulo, boolean r_porMeses, String r_anada)
	{
		this.area = r_area;
		this.tipo=r_tipo;
		this.ejercicio=r_ejercicio;
		this.articuloSeleccionado=r_articulo;
		this.porMeses = r_porMeses;
		this.anada=r_anada;
		
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");		
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		this.llenarRegistros(SelectionMode.NONE,null);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);
		
	}
	
	private void llenarRegistros(SelectionMode r_select,String r_calculo)
	{
		Iterator iterator = null;
		String origen = "";
		Integer total = new Integer(0);
		Object newItemId = null;
		Item file = null;
		
		MapeoProduccionDiaria mapeo = null;
		ArrayList<MapeoProduccionDiaria> vector = null;		
		consultaProduccionDiariaServer cps = null;
		
		container = new IndexedContainer();
		cps = new consultaProduccionDiariaServer(CurrentUser.get());
		
		if (this.porMeses)
		{
			if (tipo!=null && tipo.contentEquals("PS"))
				vector = cps.produccionArticuloPSMes(this.area, this.ejercicio, this.articuloSeleccionado.toString());
			else if (tipo!=null && tipo.contentEquals("PT"))
				vector = cps.produccionArticuloPTMes(this.area, this.ejercicio, this.articuloSeleccionado.toString());
			else
				vector = cps.produccionArticuloMesAnada(this.area, this.ejercicio, this.articuloSeleccionado.toString(), this.anada);
		}
		else if (!this.porMeses && this.articuloSeleccionado!=null)
		{
			vector = cps.produccionArticuloAnadaAgrupada(this.area, null, this.articuloSeleccionado.toString(), this.anada);
		}
		else if (this.fecha!=null && this.fecha.length()!=0)
		{
			vector = cps.produccionDiariaAgrupada(this.area, this.articuloSeleccionado, this.ejercicio, fecha, r_calculo);
		}
		else if (this.semana==null ||this.semana.toString().length()==0)
		{
			vector = cps.produccionArticuloAgrupada(this.area, this.ejercicio, this.articuloSeleccionado.toString());
		}
		else
		{
			vector = cps.produccionSemanalAgrupada(this.area, this.ejercicio, this.semana.toString(), r_calculo);
		}
		
		container.addContainerProperty("ejercicio", Integer.class, null);
		if (this.fecha!=null)
		{
			container.addContainerProperty("fecha", String.class, null);
			container.addContainerProperty("hora", String.class, null);
		}
		if (this.semana!=null)	container.addContainerProperty("semana", String.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String .class, null);
		if (this.porMeses) container.addContainerProperty("mes", String.class, null);
		else if (this.semana==null)	container.addContainerProperty("lote", String.class, null);
		if (this.semana==null || tipo==null) container.addContainerProperty("añada", String.class, null);
		container.addContainerProperty("unidades", Integer.class, null);
	        
		iterator = vector.iterator();
		while (iterator.hasNext())
		{
			newItemId = container.addItem();
			file = container.getItem(newItemId);
			
			mapeo= (MapeoProduccionDiaria) iterator.next();
			
			if (this.ejercicio!=null)
				file.getItemProperty("ejercicio").setValue(this.ejercicio);
			else
				file.getItemProperty("ejercicio").setValue(new Integer(mapeo.getDia()));
			
    		if (this.fecha!=null)
    		{
    			try {
					file.getItemProperty("fecha").setValue(RutinasFechas.convertirDeFecha(this.fecha.toString()));
				} catch (ReadOnlyException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
    			file.getItemProperty("hora").setValue(mapeo.getHora_ult_modif());
    		}
    		if (this.semana!=null)	file.getItemProperty("semana").setValue(this.semana.toString());
    		file.getItemProperty("articulo").setValue(mapeo.getArticulo());
    		file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
    		if (this.porMeses) file.getItemProperty("mes").setValue(mapeo.getMes());
    		else if (this.semana==null)	file.getItemProperty("lote").setValue(mapeo.getNumero_lote());
    		if (this.semana==null || tipo==null) file.getItemProperty("añada").setValue(mapeo.getUbicacion());
    		file.getItemProperty("unidades").setValue(mapeo.getUnidades());

//    		total = total + mapeo.getUnidades(); 
		}
		
//		newItemId = container.addItem();
//		file = container.getItem(newItemId);
//		
//		file.getItemProperty("ejercicio").setValue(null);
//		if (this.semana!=null)	file.getItemProperty("semana").setValue("TOTAL"); else file.getItemProperty("articulo").setValue("TOTAL");
////		file.getItemProperty("dia").setValue("TOTAL");
//		if (this.semana!=null)	file.getItemProperty("articulo").setValue(null);
//		file.getItemProperty("descripcion").setValue(null);
//		if (this.porMeses) file.getItemProperty("mes").setValue(null);
//		else if (this.semana==null) file.getItemProperty("lote").setValue(null);
//		file.getItemProperty("unidades").setValue(total);
		
		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(r_select);
		
    	this.widthFiltros.put("articulo", "150");
    	this.widthFiltros.put("añada", "80");
    		
    	this.camposNoFiltrar.add("unidades");
    	this.camposNoFiltrar.add("ejercicio");
    	this.camposNoFiltrar.add("descripcion");

    	this.activarFiltro();
		this.calcularTotal();
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("añada".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) || "semana".equals(cellReference.getPropertyId()) || "mes".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
	
	private void activarFiltro()
	{
		HeaderRow filterRow = this.gridDatos.appendHeaderRow();
		
		for (Object pid: this.gridDatos.getContainerDataSource().getContainerPropertyIds()) 
		{
			HeaderCell cell = filterRow.getCell(pid);
			
			// Have an input field to use for filter
			TextField filterField = new TextField();
			filterField.addStyleName(ValoTheme.TEXTFIELD_TINY);
			filterField.setInputPrompt("Filtro");				
			
			// Update filter When the filter input is changed
			filterField.addTextChangeListener(new TextChangeListener() {
				
				@Override
				public void textChange(TextChangeEvent event) {
					// Can't modify filters so need to replace
					container.removeContainerFilters(pid);
					// (Re)create the filter if necessary
					if (!event.getText().isEmpty()) container.addContainerFilter(new SimpleStringFilter(pid,event.getText(), true, false));
					calcularTotal();
			}});
			
			if (isFiltrable(pid.toString())) 
			{
				filterField.setWidth(obtenerAncho(pid.toString()));
				cell.setComponent(filterField);
			}

		}
		
	}
	private boolean isFiltrable(String r_columna)
	{
		if (camposNoFiltrar==null) return true;
		
		for (int i=0;i<camposNoFiltrar.size();i++)
		{
			if (camposNoFiltrar.get(i)!=null && camposNoFiltrar.get(i).equals(r_columna)) return false;
		}
		return true;
	}

	private String obtenerAncho(String r_columna)
	{
		if (!widthFiltros.isEmpty())
		{
			String rdo = widthFiltros.get(r_columna);
			if (rdo!=null && rdo.length()>0) return rdo;
		}
		return "0";
	}

	public void calcularTotal() 
	{
    	Long totalU = new Long(0) ;
    	String añada = null;
    	String añada_old = "";
    	Integer valor = null;
    	
    	if (this.gridDatos.getFooterRowCount()==0) this.gridDatos.appendFooterRow();
    	
    	Indexed indexed = this.gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	valor = (Integer) item.getItemProperty("unidades").getValue();
        	totalU += valor.longValue();
				
        }
        
        this.gridDatos.getFooterRow(0).getCell("articulo").setText("Totales ");
        this.gridDatos.getFooterRow(0).getCell("unidades").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		this.gridDatos.getFooterRow(0).getCell("unidades").setStyleName("Rcell-pie");
		
	}

}

