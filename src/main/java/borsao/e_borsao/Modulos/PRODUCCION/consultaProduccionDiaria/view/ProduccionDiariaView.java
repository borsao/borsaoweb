package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.MapeoProduccionDiaria;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.ProduccionDiariaGrid;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ProduccionDiariaView extends GridViewRefresh {

	public static final String VIEW_NAME = "Produccion Diaria";
	public consultaProduccionDiariaServer cpds =null;
	public ComboBox cmbLinea = null;
	public EntradaDatosFecha txtFecha= null;
	
	private Button procesar=null;
	private final String titulo = "PRODUCCION DIARIA";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = true;
	private final boolean conFormulario = false;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ProduccionDiariaView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    	
		this.txtFecha=new EntradaDatosFecha();
		this.txtFecha.setEnabled(true);
		this.txtFecha.setCaption("Fecha Consulta");
		this.txtFecha.setValue(new Date());
    			
		this.cmbLinea= new ComboBox("Linea");    		
		this.cmbLinea.setNewItemsAllowed(false);
		this.cmbLinea.addItem("Embotelladora");
		this.cmbLinea.addItem("Envasadora");
		this.cmbLinea.addItem("BIB");
		this.cmbLinea.setNullSelectionAllowed(false);
		this.cmbLinea.setValue("Embotelladora");
		this.cmbLinea.addStyleName(ValoTheme.COMBOBOX_TINY);
		
		this.procesar=new Button("Procesar");
    	this.procesar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.procesar.addStyleName(ValoTheme.BUTTON_TINY);
    	this.procesar.setIcon(FontAwesome.PRINT);

		this.cabLayout.addComponent(this.txtFecha);
		this.cabLayout.addComponent(this.cmbLinea);
		this.cabLayout.addComponent(this.procesar);
		this.cabLayout.setComponentAlignment(this.procesar, Alignment.BOTTOM_LEFT);
    		
		this.cargarListeners();
    		
		
		this.presentarGrid(new ArrayList<MapeoProduccionDiaria>());
    }

    public void generarGrid()
    {
    	ArrayList<MapeoProduccionDiaria> r_vector=null;
    	cpds = new consultaProduccionDiariaServer(CurrentUser.get());

    	r_vector=this.cpds.datosGlobal(this.cmbLinea.getValue().toString(), this.txtFecha.getValue());
    	this.presentarGrid(r_vector);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoProduccionDiaria> r_vector)
    {
    	
    	grid = new ProduccionDiariaGrid(r_vector);
    	if (((ProduccionDiariaGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    	}
    	else if (((ProduccionDiariaGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    	}
    	else
    	{
    		setHayGrid(true);
    	}
    }
    
    private void cargarListeners()
    {

    	this.procesar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbLinea.getValue()!=null && cmbLinea.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
    		}
    	});

		this.cmbLinea.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (cmbLinea.getValue()!=null && cmbLinea.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
			}
		}); 
		this.txtFecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					if (txtFecha.getValue()!=null && txtFecha.getValue().toString().length()>0) 
					{
						generarGrid();
					}
				}
			}
		}); 

    }
    
    
    @Override
    public void reestablecerPantalla() {
    	
    }
    
    @Override
    public void generarGrid(HashMap<String, String> opcionesEscogidas) {
    	
    }
    
    @Override
    public void newForm() {
    	
    }
    
    public void print() {
    	
    }
    
    @Override
    public void verForm(boolean r_busqueda) {
    	
    }
    
    @Override
    public void mostrarFilas(Collection<Object> r_filas) {
    }

	private void navegacion(boolean r_navegar)
	{
		this.cmbLinea.setVisible(r_navegar);
		this.txtFecha.setVisible(r_navegar);
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	
	@Override
	public void eliminarRegistro() {
		
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
