package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Articulos.modelo.MapeoArticulos;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardBIB;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEmbotelladora;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.modelo.MapeoDashboardEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.modelo.MapeoProduccionDiaria;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaProduccionDiariaServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaProduccionDiariaServer instance;
	private String ambitoTemporal = null;
	
	public consultaProduccionDiariaServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProduccionDiariaServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProduccionDiariaServer(r_usuario);			
		}
		return instance;
	}
		
	public ArrayList<MapeoProduccionDiaria> datosGlobal(String r_linea, Date r_fecha)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
				
		tabla = this.nombreTabla("a_lin_", RutinasFechas.añoFecha(r_fecha));		
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
			
		try
		{
			fechaInicio = RutinasFechas.convertirDateToString(RutinasFechas.conversion(r_fecha));
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, 1);
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT a.usuario prd_usu, ");
			sql.append("a.fecha_documento prd_fec, ");			
			sql.append("a.articulo prd_art, ");
			sql.append("e_articu.descrip_articulo prd_des, ");
			sql.append("a.unidades prd_uds, ");
			sql.append("a.numero_lote prd_lot, "); 
			sql.append("a.ubicacion prd_any, ");
			sql.append("a.hora_ult_modif prd_hor, "); 
			sql.append("a.movimiento prd_mov, ");
			sql.append("a.pantalla prd_pal ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND ((a.fecha_documento='" + fechaFin + "' and a.hora_ult_modif between '00:00:00' and '05:59:59') ");
			sql.append("OR (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '06:00:00' and '23:59:59')) ");
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			
			sql.append("ORDER BY 1, 2, 8, 9, 3 ");

			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
				mapeo.setArticulo(rsProduccion.getString("prd_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsProduccion.getString("prd_des")));
				mapeo.setHora_ult_modif(rsProduccion.getString("prd_hor"));
				mapeo.setNumero_lote(rsProduccion.getString("prd_lot"));
				mapeo.setNumeroPalet(rsProduccion.getString("prd_pal"));
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));
				
				if (!RutinasFechas.convertirDeFecha(rsProduccion.getString("prd_fec")).equals(fechaInicio))
				{
					mapeo.setTurno("N");
				}
				else
				{
					if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 6 )
					{
						mapeo.setTurno("N");	
					}
					else if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 14 )
					{
						mapeo.setTurno("M");
					}
					else if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 22 )
					{
						mapeo.setTurno("T");
					}
					else 
					{
						mapeo.setTurno("N");
					}
				}
				
				mapeo.setDia(fechaInicio);
				mapeo.setUbicacion(rsProduccion.getString("prd_any"));
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
					mapeo.setUnidadesSalida(0);
				}
				else
				{
					mapeo.setUnidadesEntrada(0);
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
				mapeo.setUsuario(rsProduccion.getString("prd_usu"));

				if (mapeoOld!=null && mapeoOld.getTurno()!=mapeo.getTurno())
				{					
					MapeoProduccionDiaria mapeoTotales = new MapeoProduccionDiaria();
					if (mapeoOld.getTurno().equals("M"))
					{
						mapeoTotales =this.datosTotalTurnoM(r_linea, r_fecha);
					}
					else
					{
						mapeoTotales =this.datosTotalTurnoT(r_linea, r_fecha);
					}
						
					vector.add(mapeoTotales);					
				}
				vector.add(mapeo);
				mapeoOld=mapeo;
			}
			
			if (!vector.isEmpty() && vector.size()>0)
			{
				mapeo = new MapeoProduccionDiaria();
				if (mapeoOld.getTurno().equals("M"))
				{
					mapeo =this.datosTotalTurnoM(r_linea, r_fecha);
					vector.add(mapeo);
				}
				if (mapeoOld.getTurno().equals("T"))
				{
					mapeo =this.datosTotalTurnoT(r_linea, r_fecha);
					vector.add(mapeo);
				}
				if (mapeoOld.getTurno().equals("N"))
				{
					mapeo =this.datosTotalTurnoNoche(r_linea, r_fecha);
					vector.add(mapeo);
				}
				mapeo = new MapeoProduccionDiaria();
				mapeo =this.datosTotalDia(r_linea, r_fecha);
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProduccionDiaria> produccionSemanal(String r_linea, Integer r_ejercicio, String r_semana)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
			
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT a.fecha_documento prd_fec, ");			
			sql.append("a.articulo prd_art, ");
			sql.append("e_articu.descrip_articulo prd_des, ");
			sql.append("sum(a.unidades) prd_uds ");
			sql.append("FROM a_lin_" + digito + " a, e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.movimiento='49') ");
			sql.append("AND (week(a.fecha_documento,3)) ='" + r_semana + "' " );
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			sql.append("GROUP BY 1, 2, 3 ");
			sql.append("ORDER BY 1, 2 ");

			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
				mapeo.setDia(rsProduccion.getString("prd_fec"));
				mapeo.setArticulo(rsProduccion.getString("prd_art"));
				mapeo.setDescripcion(RutinasCadenas.conversion(rsProduccion.getString("prd_des")));
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));

				vector.add(mapeo);
				mapeoOld=mapeo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public Integer produccionSemanal(String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsProduccion = null;
		Integer valor = null;
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
		
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds ");
			sql.append("FROM a_lin_" + digito + " a, e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.movimiento='49') ");
			sql.append("AND (week(a.fecha_documento,3)) ='" + r_semana + "' " );
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			while(rsProduccion.next())
			{
				valor = rsProduccion.getInt("prd_uds");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return valor;
	}
	
	public ArrayList<MapeoProduccionDiaria> produccionSemanalAgrupada(String r_linea, Integer r_ejercicio, String r_semana, String r_calculo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
		
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT  ");
			
			if (r_linea.toUpperCase().equals("ENVASADORA"))
			{
				articulo="0103";
				
				sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
				sql.append(" e_articu.marca as formato, " );
				sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");

			}
			else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
			{
				articulo="0102";
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
			}
			else
			{
				articulo="0111";
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
			}
			
			if (r_calculo.toUpperCase().contentEquals("UNIDADES"))
			{
				sql.append("sum(a.unidades) prd_uds ");
			}
			else
			{
				sql.append("sum(a.unidades*e_articu.marca) prd_uds ");
			}
			sql.append("FROM a_lin_" + digito + " a, e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.movimiento='49') ");
			sql.append("AND (week(a.fecha_documento,3)) ='" + r_semana + "' " );
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			if (r_linea.toUpperCase().equals("ENVASADORA"))
			{
				sql.append("AND (a.almacen='1') "); 
				sql.append("GROUP BY 1, 2, 3 ");
				sql.append("ORDER BY 1 desc, 2 ");
			}
			else if (r_linea.toUpperCase().equals("BIB"))
			{
				sql.append("AND (a.almacen='1') "); 
				sql.append("GROUP BY 1, 2 ");
				sql.append("ORDER BY 1, 2 ");
			}
			else
			{
				sql.append("AND (a.almacen='4') ");
				sql.append("GROUP BY 1, 2 ");
				sql.append("ORDER BY 1, 2 ");
			}
			

			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
//				mapeo.setDia(rsProduccion.getString("prd_fec"));
				if (r_linea.toUpperCase().equals("ENVASADORA")) 
					mapeo.setArticulo(rsProduccion.getString("color") + " " + rsProduccion.getString("formato") + " L");
				else
					mapeo.setArticulo(rsProduccion.getString("color"));
				
				mapeo.setDescripcion(rsProduccion.getString("palet"));
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));

				vector.add(mapeo);
				mapeoOld=mapeo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public ArrayList<MapeoProduccionDiaria> produccionDiariaAgrupada(String r_linea, String r_articulo, Integer r_ejercicio, String r_fecha, String r_calculo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
		
		try
		{
			StringBuffer sql = new StringBuffer();
			
			sql.append("SELECT a.fecha_documento, a.hora_ult_modif pro_Hor, a.unidades prd_uds, a.articulo pro_ar, a.descrip_articulo pro_des, a.numero_lote pro_lot, a.ubicacion pro_ubi ");
			sql.append("FROM a_lin_" + digito + " a, e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.movimiento='49') ");
			sql.append("AND a.fecha_documento ='" + r_fecha + "' " );
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + r_articulo + "%')) ");
			if (r_linea.toUpperCase().equals("ENVASADORA"))
			{
				sql.append("AND (a.almacen='1') "); 
				sql.append("ORDER BY 1, 2 ");
			}
			else
			{
				sql.append("AND (a.almacen='4') ");
				sql.append("ORDER BY 1, 2 ");
			}
			
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				mapeo.setArticulo(rsProduccion.getString("pro_ar"));
				mapeo.setDescripcion(rsProduccion.getString("pro_des"));
				mapeo.setHora_ult_modif(rsProduccion.getString("pro_hor"));
				mapeo.setNumero_lote(rsProduccion.getString("pro_lot"));
				mapeo.setUbicacion(rsProduccion.getString("pro_ubi"));
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
				
				vector.add(mapeo);
				mapeoOld=mapeo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProduccionDiaria> produccionArticuloAgrupada(String r_linea, Integer r_ejercicio, String r_articulo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
		articulo = r_articulo;
		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT  ");
			
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
				sql.append(" a.numero_lote as lote, " );
				sql.append(" a.ubicacion as ubi, " );
				sql.append(" sum(a.unidades) prd_uds ");

				sql.append("FROM a_lin_" + digito + " a, e_articu ");
				sql.append("WHERE a.articulo = e_articu.articulo ");
				sql.append("AND (a.clave_tabla='B') ");
				if (articulo.startsWith("0101"))
				{
					sql.append("AND (a.movimiento='99') ");
					sql.append("AND (e_articu.tipo_articulo In ('MP')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");
				}
				else 
				{
					sql.append("AND (a.movimiento='49') ");
					sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");
				}
				sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			
				sql.append("GROUP BY 1, 2, 3, 4 ");
				sql.append("ORDER BY 1, 2, 3, 4 ");
			

			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
//				mapeo.setDia(rsProduccion.getString("prd_fec"));
				mapeo.setArticulo(rsProduccion.getString("color"));
				mapeo.setDescripcion(rsProduccion.getString("palet"));
				mapeo.setNumero_lote(rsProduccion.getString("lote"));
				mapeo.setUbicacion(rsProduccion.getString("ubi"));
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
				vector.add(mapeo);
				mapeoOld=mapeo;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProduccionDiaria> produccionArticuloMes(String r_linea, Integer r_ejercicio, String r_articulo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		int cuantos = 10;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		articulo = r_articulo;
		if (r_ejercicio == null)
		{
			cuantos = 3;
			r_ejercicio = new Integer(RutinasFechas.añoActualYYYY());
		}
		else 
		{
			cuantos = 0;
		}

		for (int i= 0; i<=cuantos;i++)
		{
			if (i!=0)r_ejercicio = r_ejercicio - 1;
			if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
			
			try
			{
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT  ");
				
					sql.append(" a.articulo as color, ");
					sql.append(" e_articu.descrip_articulo as palet, " );
					sql.append(" month(a.fecha_documento) as mes, " );
					sql.append(" a.ubicacion as ubi, " );
					sql.append(" sum(a.unidades) prd_uds ");
	
					sql.append("FROM a_lin_" + digito + " a, e_articu ");
					sql.append("WHERE a.articulo = e_articu.articulo ");
					sql.append("AND (a.clave_tabla='B') ");
					if (!articulo.startsWith("0102") && !articulo.startsWith("0103") && !articulo.startsWith("0111"))
					{
						sql.append("AND (a.movimiento='49') ");
	//					sql.append("AND (e_articu.tipo_articulo In ('OA')) ");
						if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");
						sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' and padre like '0102%') ");
					}
					else 
					{
						sql.append("AND (a.movimiento='49') ");
						sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) ");
						if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");
	
						if (articulo.startsWith("0")) sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
						else
						{
							sql.append(" and e_articu.articulo in (select padre from e__lconj where descripcion like '%" + articulo + "%' and padre like '0102%') ");
							
						}
					}
				
					sql.append("GROUP BY 1, 2, 3, 4 ");
					sql.append("ORDER BY 1, 2, 3, 4 ");
				
	
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsProduccion = cs.executeQuery(sql.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
				
				vector = new ArrayList<MapeoProduccionDiaria>();
				
				while(rsProduccion.next())
				{
					mapeo = new MapeoProduccionDiaria();
					if (cuantos!=0)
					{
							mapeo.setDia(r_ejercicio.toString());
					}
					mapeo.setArticulo(rsProduccion.getString("color"));
					mapeo.setDescripcion(rsProduccion.getString("palet"));
					mapeo.setMes(rsProduccion.getString("mes")); 
					mapeo.setUbicacion(rsProduccion.getString("ubi")); 
					mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
					vector.add(mapeo);
					mapeoOld=mapeo;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsProduccion!=null)
					{
						rsProduccion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	public ArrayList<MapeoProduccionDiaria> produccionArticuloMesAnada(String r_linea, Integer r_ejercicio, String r_articulo, String r_anada)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		int cuantos = 10;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		articulo = r_articulo;
		if (r_ejercicio == null)
		{
			cuantos = 3;
			r_ejercicio = new Integer(RutinasFechas.añoActualYYYY());
		}
		else 
		{
			cuantos = 0;
		}
		
		for (int i= 0; i<=cuantos;i++)
		{
			if (i!=0)r_ejercicio = r_ejercicio - 1;
			if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
			
			try
			{
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT  ");
				
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
				sql.append(" month(a.fecha_documento) as mes, " );
				sql.append(" a.ubicacion as ubi, " );
				
				
				
				if (!articulo.startsWith("0102") && !articulo.startsWith("0103") && !articulo.startsWith("0111"))
				{
					sql.append(" sum(unidades*replace(e_articu.marca, ',','.')) prd_uds ");
				}
				else
					sql.append(" sum(a.unidades) prd_uds ");
				
				
				sql.append("FROM a_lin_" + digito + " a, e_articu ");
				sql.append("WHERE a.articulo = e_articu.articulo ");
				sql.append("AND (a.clave_tabla='B') ");
				sql.append("AND (a.ubicacion='" + r_anada + "') ");
				if (!articulo.startsWith("0102") && !articulo.startsWith("0103") && !articulo.startsWith("0111"))
				{
					sql.append("AND (a.movimiento='49') ");
					//					sql.append("AND (e_articu.tipo_articulo In ('OA')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
//					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' and padre like '0102%') ");
					/*
					 * hago que saque todo padre que contenga mi hijo
					 */
					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' ) ");
					sql.append("GROUP BY 1, 2, 3, 4 ");
					
					/*
					 * TODO habra que restar los etiquetados
					 */

					sql.append("UNION ALL ");
					
					sql.append("SELECT  ");
					
					sql.append(" a.articulo as color, ");
					sql.append(" e_articu.descrip_articulo as palet, " );
					sql.append(" month(a.fecha_documento) as mes, " );
					sql.append(" a.ubicacion as ubi, " );
					sql.append(" sum((-1)*unidades*replace(e_articu.marca, ',','.')) prd_uds ");
					sql.append("FROM a_lin_" + digito + " a, e_articu ");
					sql.append("WHERE a.articulo = e_articu.articulo ");
					sql.append("AND (a.clave_tabla='B') ");
					sql.append("AND (a.ubicacion='" + r_anada + "') ");
					sql.append("AND (a.movimiento='99') ");
					sql.append("AND (e_articu.tipo_articulo In ('PS')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' ) ");
					
					sql.append("GROUP BY 1, 2, 3, 4 ");
					sql.append("ORDER BY 1, 2, 3, 4 ");
				}
				else 
				{
					sql.append("AND (a.movimiento='49') ");
					sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
					
					if (articulo.startsWith("0")) sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
					else
					{
						sql.append(" and e_articu.articulo in (select padre from e__lconj where descripcion like '%" + articulo + "%' and padre like '0102%') ");
						
					}
					sql.append("GROUP BY 1, 2, 3, 4 ");
					sql.append("ORDER BY 1, 2, 3, 4 ");
				}
				
				
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsProduccion = cs.executeQuery(sql.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
				
				if (i==0) vector = new ArrayList<MapeoProduccionDiaria>();
				
				while(rsProduccion.next())
				{
					mapeo = new MapeoProduccionDiaria();
					if (cuantos!=0)
					{
						mapeo.setDia(r_ejercicio.toString());
					}
					mapeo.setArticulo(rsProduccion.getString("color"));
					mapeo.setDescripcion(rsProduccion.getString("palet"));
					mapeo.setMes(rsProduccion.getString("mes")); 
					mapeo.setUbicacion(rsProduccion.getString("ubi")); 

					if (mapeoOld!=null && mapeoOld.getArticulo().contentEquals(mapeo.getArticulo()) && mapeoOld.getUbicacion().contentEquals(mapeo.getUbicacion()))
					{
						vector.remove(mapeoOld);
						mapeo.setUnidades(mapeoOld.getUnidades()+rsProduccion.getInt("prd_uds"));
					}
					else
					{
						mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
					}
					vector.add(mapeo);
					mapeoOld=mapeo;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsProduccion!=null)
					{
						rsProduccion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	public ArrayList<MapeoProduccionDiaria> produccionArticuloAnadaAgrupada(String r_linea, Integer r_ejercicio, String r_articulo, String r_anada)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		MapeoProduccionDiaria mapeoOld = null; 
		Connection con = null;	
		Statement cs = null;
		int cuantos = 10;
		
		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		String digito = "";
		
		articulo = r_articulo;
		if (r_ejercicio == null)
		{
			cuantos = 3;
			r_ejercicio = new Integer(RutinasFechas.añoActualYYYY());
		}
		else 
		{
			cuantos = 0;
		}
		
		for (int i= 0; i<=cuantos;i++)
		{
			if (i!=0)r_ejercicio = r_ejercicio - 1;
			if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();
			
			try
			{
				StringBuffer sql = new StringBuffer();
				sql.append("SELECT  ");
				
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
				sql.append(" a.ubicacion as ubi, " );
				
				
				
				if (!articulo.startsWith("0102") && !articulo.startsWith("0103") && !articulo.startsWith("0111"))
				{
					sql.append(" sum(unidades*replace(e_articu.marca, ',','.')) prd_uds ");
				}
				else
					sql.append(" sum(a.unidades) prd_uds ");
				
				
				sql.append("FROM a_lin_" + digito + " a, e_articu ");
				sql.append("WHERE a.articulo = e_articu.articulo ");
				sql.append("AND (a.clave_tabla='B') ");
				if (r_anada!=null && r_anada.length()>0) sql.append("AND (a.ubicacion='" + r_anada + "') ");
				if (!articulo.startsWith("0102") && !articulo.startsWith("0103") && !articulo.startsWith("0111"))
				{
					sql.append("AND (a.movimiento='49') ");
					//					sql.append("AND (e_articu.tipo_articulo In ('OA')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
//					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' and padre like '0102%') ");
					/*
					 * hago que saque todo padre que contenga mi hijo
					 */
					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' ) ");
					sql.append("GROUP BY 1, 2, 3 ");
					
					/*
					 * TODO habra que restar los etiquetados
					 */
					
					sql.append("UNION ALL ");
					
					sql.append("SELECT  ");
					
					sql.append(" a.articulo as color, ");
					sql.append(" e_articu.descrip_articulo as palet, " );
					sql.append(" a.ubicacion as ubi, " );
					sql.append(" sum((-1)*unidades*replace(e_articu.marca, ',','.')) prd_uds ");
					sql.append("FROM a_lin_" + digito + " a, e_articu ");
					sql.append("WHERE a.articulo = e_articu.articulo ");
					sql.append("AND (a.clave_tabla='B') ");
					if (r_anada!=null && r_anada.length()>0) sql.append("AND (a.ubicacion='" + r_anada + "') ");
					sql.append("AND (a.movimiento='99') ");
					sql.append("AND (e_articu.tipo_articulo In ('PS')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
					sql.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '%" + articulo + "%' ) ");
					
					sql.append("GROUP BY 1, 2, 3 ");
					sql.append("ORDER BY 1, 2, 3 ");
				}
				else 
				{
					sql.append("AND (a.movimiento='49') ");
					sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) ");
					if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen in ('1','4')) ");
					
					if (articulo.startsWith("0")) sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
					else
					{
						sql.append(" and e_articu.articulo in (select padre from e__lconj where descripcion like '%" + articulo + "%' and padre like '0102%') ");
						
					}
					sql.append("GROUP BY 1, 2, 3 ");
					sql.append("ORDER BY 1, 2, 3 ");
				}
				
				
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
				rsProduccion = cs.executeQuery(sql.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
				
				if (i==0) vector = new ArrayList<MapeoProduccionDiaria>();
				
				while(rsProduccion.next())
				{
					mapeo = new MapeoProduccionDiaria();
					if (cuantos!=0)
					{
						mapeo.setDia(r_ejercicio.toString());
					}
					mapeo.setArticulo(rsProduccion.getString("color"));
					mapeo.setDescripcion(rsProduccion.getString("palet"));
					mapeo.setUbicacion(rsProduccion.getString("ubi")); 
					
					if (mapeoOld!=null && mapeoOld.getArticulo().contentEquals(mapeo.getArticulo()) && mapeoOld.getUbicacion().contentEquals(mapeo.getUbicacion()))
					{
						vector.remove(mapeoOld);
						mapeo.setUnidades(mapeoOld.getUnidades()+rsProduccion.getInt("prd_uds"));
					}
					else
					{
						mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
					}
					vector.add(mapeo);
					mapeoOld=mapeo;
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsProduccion!=null)
					{
						rsProduccion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		return vector;
	}

	public ArrayList<MapeoProduccionDiaria> produccionArticuloPSMes(String r_linea, Integer r_ejercicio, String r_articulo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		String sqlPadres=null;
		java.util.Iterator<MapeoArticulos> iter = null;
		String digito = "";
		Connection con = null;	
		Statement cs = null;
		
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	ArrayList<MapeoArticulos> vectorPadres = ces.recuperarPadresDescPS(r_articulo);

    	iter = vectorPadres.iterator();
    	
    	while (iter.hasNext())
    	{
    		MapeoArticulos map = iter.next();
    		sqlPadres = sqlPadres + ",'" + map.getArticulo() + "' "; 
    	}
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT  ");
			
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
				sql.append(" month(a.fecha_documento) as mes, " );
				sql.append("sum(a.unidades) prd_uds ");

				sql.append("FROM a_lin_" + digito + " a, e_articu ");
				sql.append("WHERE a.articulo = e_articu.articulo ");
				sql.append("AND (a.clave_tabla='B') ");
				
				sql.append("AND (a.movimiento='49') ");
				sql.append("AND (e_articu.tipo_articulo In ('PS')) ");
				if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");					
				
				sql.append("AND ((e_articu.articulo in (" + sqlPadres.toString() + "))) ");
			
				sql.append("GROUP BY 1, 2, 3 ");
				sql.append("ORDER BY 1, 2, 3 ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
//				mapeo.setDia(rsProduccion.getString("prd_fec"));
				mapeo.setArticulo(rsProduccion.getString("color"));
				mapeo.setDescripcion(rsProduccion.getString("palet"));
				mapeo.setMes(rsProduccion.getString("mes")); 
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}
	
	public ArrayList<MapeoProduccionDiaria> produccionArticuloPTMes(String r_linea, Integer r_ejercicio, String r_articulo)
	{
		ResultSet rsProduccion = null;
		ArrayList<MapeoProduccionDiaria> vector = null;
		MapeoProduccionDiaria mapeo = null;
		String sqlPadres=null;
		java.util.Iterator<MapeoArticulos> iter = null;
		String digito = "";
		Connection con = null;	
		Statement cs = null;
		
    	consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
    	ArrayList<MapeoArticulos> vectorPadres = ces.recuperarPadresDesc(r_articulo);

    	iter = vectorPadres.iterator();
    	
    	while (iter.hasNext())
    	{
    		MapeoArticulos map = iter.next();
    		sqlPadres = sqlPadres + ",'" + map.getArticulo() + "' "; 
    	}
		if (RutinasFechas.añoActualYYYY().contentEquals(r_ejercicio.toString())) digito = "0"; else digito = r_ejercicio.toString();

		try
		{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT  ");
			
				sql.append(" a.articulo as color, ");
				sql.append(" e_articu.descrip_articulo as palet, " );
				sql.append(" month(a.fecha_documento) as mes, " );
				sql.append("sum(a.unidades) prd_uds ");

				sql.append("FROM a_lin_" + digito + " a, e_articu ");
				sql.append("WHERE a.articulo = e_articu.articulo ");
				sql.append("AND (a.clave_tabla='B') ");
				
				sql.append("AND (a.movimiento='49') ");
				sql.append("AND (e_articu.tipo_articulo In ('PT')) ");
				if (r_linea.toUpperCase().equals("ENVASADORA")) sql.append("AND (a.almacen='1') "); else sql.append("AND (a.almacen='4') ");					
				
				sql.append("AND ((e_articu.articulo in (" + sqlPadres.toString() + "))) ");
			
				sql.append("GROUP BY 1, 2, 3 ");
				sql.append("ORDER BY 1, 2, 3 ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProduccionDiaria>();
			
			while(rsProduccion.next())
			{
				mapeo = new MapeoProduccionDiaria();
				
//				mapeo.setDia(rsProduccion.getString("prd_fec"));
				mapeo.setArticulo(rsProduccion.getString("color"));
				mapeo.setDescripcion(rsProduccion.getString("palet"));
				mapeo.setMes(rsProduccion.getString("mes")); 
				mapeo.setUnidades(rsProduccion.getInt("prd_uds"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public StringBuilder obtenerLotesPT(String r_lotePS)
	{
		ResultSet rsProduccionPS = null;
		ResultSet rsProduccionPT = null;
		StringBuilder lotesPT = null;
		String tabla = null;
		Connection con = null;	
		Statement cs = null;
		
		String articulo = "0102";
		
		try
		{
			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			lotesPT = new StringBuilder();
			
			for (int ej=5; ej>=0; ej--)
			{
				tabla = "a_lin_" + ej;
				StringBuilder sql = new StringBuilder();
				
				StringBuilder sqlPT = new StringBuilder();
				sqlPT.append("SELECT distinct(numero_lote) prd_lpt " );
				sqlPT.append(" FROM " + tabla );
				sqlPT.append(" WHERE movimiento = '49' ");
				sqlPT.append(" AND (fecha_documento||hora_ult_modif) in  ");
				sqlPT.append(" (select fecha_documento||hora_ult_modif from " + tabla ); 
				sqlPT.append(" where numero_lote = " + r_lotePS + " ");
				sqlPT.append(" and movimiento = '99' ");
				sqlPT.append(" AND articulo matches '" + articulo + "*') ");

				rsProduccionPT = cs.executeQuery(sqlPT.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sqlPT.toString() + " : " + new Date());
	
				while(rsProduccionPT.next())
				{
					if (lotesPT.toString().length()==0)
					{
						if (!lotesPT.toString().contains(rsProduccionPT.getString("prd_lpt"))) lotesPT.append(rsProduccionPT.getString("prd_lpt"));
					}
					else
					{
						if (!lotesPT.toString().contains(rsProduccionPT.getString("prd_lpt")))
						{
							lotesPT.append(",");
							lotesPT.append(rsProduccionPT.getString("prd_lpt"));
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return lotesPT;
	}
	
	private MapeoProduccionDiaria datosTotalDia(String r_linea, Date r_fecha)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Connection con = null;	
		Statement cs = null;

		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		String tabla = "";
		
		tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(r_fecha));			
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
			
		try
		{
			fechaInicio = RutinasFechas.convertirDateToString(RutinasFechas.conversion(r_fecha));
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, 1);
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds, ");
			sql.append("a.movimiento prd_mov ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND ((a.fecha_documento='" + fechaFin + "' and a.hora_ult_modif between '00:00:00' and '05:59:59') ");
			sql.append("OR (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '06:00:00' and '23:59:59')) ");
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			sql.append("GROUP by 2");

			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				
				mapeo.setDescripcion("TOTAL DIA .........................");
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));

				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
			}
			mapeo.setMovimiento("");		
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return mapeo;
	}

	public HashMap<String, Integer> datosTotalArticulo(String r_fecha, String r_usuario, String r_articulo, String r_lote, String r_añada)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Integer totalEntradasDia = 0;
		Integer totalSalidasDia = 0;
		Integer totalEntradasNoche = 0;
		Integer totalSalidasNoche = 0;
		Connection con = null;	
		Statement cs = null;

		String tabla = "";
		
		tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(RutinasFechas.conversionDeString(r_fecha)));	

		
		String fechaInicio=null;
		String fechaFin=null;
		HashMap<String, Integer> hashRdo  =null;
		
		try
		{
			fechaInicio = r_fecha;
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, 1);
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT a.usuario prd_usu, ");
			sql.append("a.fecha_documento prd_fec, ");			
			sql.append("a.articulo prd_art, ");
			sql.append("e_articu.descrip_articulo prd_des, ");
			sql.append("a.unidades prd_uds, ");
			sql.append("a.numero_lote prd_lot, "); 
			sql.append("a.ubicacion prd_any, ");
			sql.append("a.hora_ult_modif prd_hor, "); 
			sql.append("a.movimiento prd_mov, ");
			sql.append("a.pantalla prd_pal ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND ((a.fecha_documento='" + fechaFin + "' and a.hora_ult_modif between '00:00:00' and '05:59:59') ");
			sql.append("OR (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '06:00:00' and '23:59:59')) ");
			
//			if (r_usuario!=null) sql.append("AND (a.usuario= '" + r_usuario + "')");
			
			if (r_lote!=null) sql.append("AND a.numero_lote = '" + r_lote + "' ");
			
			if (r_añada!=null) sql.append("AND a.ubicacion = '" + r_añada + "' ");
			
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			
			if (r_articulo!=null)
			{
				sql.append("AND a.articulo = '" + r_articulo + "' ");
			}
			else
			{
				sql.append("AND ((e_articu.articulo Like '0102%') OR (e_articu.articulo Like '0103%') OR (e_articu.articulo Like '0111%')) ");
			}

			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			hashRdo = new HashMap<String, Integer>();
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));	
				if (!RutinasFechas.convertirDeFecha(rsProduccion.getString("prd_fec")).equals(fechaInicio))
				{
					mapeo.setTurno("N");
				}
				else
				{
					if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 6 )
					{
						mapeo.setTurno("N");	
					}
					else if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 14 )
					{
						mapeo.setTurno("D");
					}
					else if (new Integer(rsProduccion.getString("prd_hor").substring(0, rsProduccion.getString("prd_hor").indexOf(":")))< 22 )
					{
						mapeo.setTurno("D");
					}
					else 
					{
						mapeo.setTurno("N");
					}
				}
				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
				
				switch (mapeo.getTurno())
				{
					case "N":
					{
						if (mapeo.getUnidadesEntrada()!=null) totalEntradasNoche = totalEntradasNoche + mapeo.getUnidadesEntrada();
						if (mapeo.getUnidadesSalida()!=null) totalSalidasNoche = totalSalidasNoche + mapeo.getUnidadesSalida();
						break;
					}
					case "D":
					{
						if (mapeo.getUnidadesEntrada()!=null) totalEntradasDia = totalEntradasDia + mapeo.getUnidadesEntrada();
						if (mapeo.getUnidadesSalida()!=null) totalSalidasDia = totalSalidasDia + mapeo.getUnidadesSalida();						
						break;
					}
				}
			}
			if (hashRdo!=null)
			{
				hashRdo.put("EntradasDia",totalEntradasDia);
				hashRdo.put("SalidasDia",totalSalidasDia);
				hashRdo.put("EntradasNoche",totalEntradasNoche);
				hashRdo.put("SalidasNoche",totalSalidasNoche);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return hashRdo;
	}
	
	public Double datosTotalArticuloDia(String r_fecha, String r_articulo)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Integer totalEntradasDia = 0;
		Integer totalSalidasDia = 0;
		Integer totalEntradasNoche = 0;
		Integer totalSalidasNoche = 0;
		Connection con = null;	
		Statement cs = null;
		String tabla = "";
		
		tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(RutinasFechas.conversionDeString(r_fecha)));	

		
		String fechaInicio=null;
		String fechaFin=null;
		HashMap<String, Integer> hashRdo  =null;
		
		try
		{
			fechaInicio = r_fecha;
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, 1);
			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND ((a.fecha_documento='" + fechaFin + "' and a.hora_ult_modif between '00:00:00' and '05:59:59') ");
			sql.append("OR (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '06:00:00' and '23:59:59')) ");
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			
			if (r_articulo!=null)
			{
				sql.append("AND a.articulo = '" + r_articulo + "' ");
			}
			else
			{
				sql.append("AND ((e_articu.articulo Like '0102%') OR (e_articu.articulo Like '0103%') OR (e_articu.articulo Like '0111%')) ");
			}

			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			while(rsProduccion.next())
			{
				return rsProduccion.getDouble("prd_uds");	
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return new Double(0);
	}
	
	public HashMap<String, Integer> datosTotalArticulo(String r_fecha, String r_usuario, String r_articulo, String r_lote, String r_añada, Integer r_dias)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Integer totalEntradasDia = 0;
		Integer totalSalidasDia = 0;
		Integer totalEntradasNoche = 0;
		Integer totalSalidasNoche = 0;
		Connection con = null;	
		Statement cs = null;

		String fechaInicio=null;
		String fechaFin=null;
		HashMap<String, Integer> hashRdo  =null;
		
		try
		{
			fechaInicio = RutinasFechas.restarDiasFecha(r_fecha, r_dias);
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, r_dias + 1);
			
			String tabla = "";
			
			tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(RutinasFechas.conversionDeString(r_fecha)));			

			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT a.usuario prd_usu, ");
			sql.append("a.fecha_documento prd_fec, ");			
			sql.append("a.articulo prd_art, ");
			sql.append("e_articu.descrip_articulo prd_des, ");
			sql.append("a.unidades prd_uds, ");
			sql.append("a.numero_lote prd_lot, "); 
			sql.append("a.ubicacion prd_any, ");
			sql.append("a.hora_ult_modif prd_hor, "); 
			sql.append("a.movimiento prd_mov, ");
			sql.append("a.pantalla prd_pal ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.fecha_documento between '" + fechaInicio + "' and '" + fechaFin + "') ");
//			sql.append("AND (a.usuario= '" + r_usuario + "')");
			
			if (r_lote!=null) sql.append("AND a.numero_lote = '" + r_lote + "' ");
			
			if (r_añada!=null) sql.append("AND a.ubicacion = '" + r_añada + "' ");
			
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			
			if (r_articulo!=null)
			{
				sql.append("AND a.articulo = '" + r_articulo + "' ");
			}
			else
			{
				sql.append("AND ((e_articu.articulo Like '0102%') OR (e_articu.articulo Like '0103%') OR (e_articu.articulo Like '0111%')) ");
			}

			con= this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			hashRdo = new HashMap<String, Integer>();
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));	
				mapeo.setTurno("D");
				
				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
				
				totalEntradasNoche = 0;
				totalSalidasNoche = 0;

				if (mapeo.getUnidadesEntrada()!=null) totalEntradasDia = totalEntradasDia + mapeo.getUnidadesEntrada();
				if (mapeo.getUnidadesSalida()!=null) totalSalidasDia = totalSalidasDia + mapeo.getUnidadesSalida();					
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		
		hashRdo.put("EntradasDia",totalEntradasDia);
		hashRdo.put("SalidasDia",totalSalidasDia);
		hashRdo.put("EntradasNoche",totalEntradasNoche);
		hashRdo.put("SalidasNoche",totalSalidasNoche);
		return hashRdo;
	}
	private MapeoProduccionDiaria datosTotalTurnoM(String r_linea, Date r_fecha)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Connection con = null;	
		Statement cs = null;

		String fechaInicio=null;
		String articulo = null;
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
		
		try
		{
			fechaInicio = RutinasFechas.convertirDateToString(RutinasFechas.conversion(r_fecha));
			
			String tabla = "";
			
			tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(r_fecha));			

			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds, ");
			sql.append("a.movimiento prd_mov ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '06:00:00' and '13:59:59') ");
//			sql.append("AND (a.usuario= '" + usuario + "')"); 
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			sql.append("GROUP by 2");

			con = this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				
				mapeo.setDescripcion(" TOTAL TURNO ......................");
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));

				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
			}
			mapeo.setMovimiento("");		
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}

		return mapeo;
	}
	
	private MapeoProduccionDiaria datosTotalTurnoT(String r_linea, Date r_fecha)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Connection con = null;	
		Statement cs = null;

		String fechaInicio=null;
		String articulo = null;
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}
		
			
		try
		{
			fechaInicio = RutinasFechas.convertirDateToString(RutinasFechas.conversion(r_fecha));
			String tabla = "";
			
			tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(r_fecha));			

			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds, ");
			sql.append("a.movimiento prd_mov ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '14:00:00' and '21:59:59') ");
//			sql.append("AND (a.usuario= '" + usuario + "')"); 
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			sql.append("GROUP by 2");

			con = this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());

			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				
				mapeo.setDescripcion(" TOTAL TURNO ......................");
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));

				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
			}
			mapeo.setMovimiento("");		
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeo;
	}
	
	private MapeoProduccionDiaria datosTotalTurnoNoche(String r_linea, Date r_fecha)
	{
		ResultSet rsProduccion = null;
		MapeoProduccionDiaria mapeo = null;
		Connection con = null;	
		Statement cs = null;

		String fechaInicio=null;
		String fechaFin=null;
		String articulo = null;
		
		if (r_linea.toUpperCase().equals("ENVASADORA"))
		{
			articulo="0103";
		}
		else if (r_linea.toUpperCase().equals("EMBOTELLADORA"))
		{
			articulo="0102";
		}
		else
		{
			articulo="0111";
		}

			
		try
		{
			fechaInicio = RutinasFechas.convertirDateToString(RutinasFechas.conversion(r_fecha));
			fechaFin = RutinasFechas.sumarDiasFecha(fechaInicio, 1);
			String tabla = "";
			
			tabla = this.nombreTabla("a_lin_",  RutinasFechas.añoFecha(r_fecha));			

			
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT sum(a.unidades) prd_uds, ");
			sql.append("a.movimiento prd_mov ");
			sql.append("FROM green." + tabla + " a, green.e_articu e_articu ");
			sql.append("WHERE a.articulo = e_articu.articulo ");
			sql.append("AND (a.clave_tabla='B') ");
			sql.append("AND ((a.fecha_documento='" + fechaFin + "' and a.hora_ult_modif between '00:00:00' and '05:59:59') ");
			sql.append("OR (a.fecha_documento='" + fechaInicio + "' and a.hora_ult_modif between '22:00:00' and '23:59:59')) ");
			sql.append("AND (e_articu.tipo_articulo In ('PS','PT')) "); 
			sql.append("AND ((e_articu.articulo Like '" + articulo + "%')) ");
			sql.append("GROUP by 2");

			con = this.conManager.establecerConexionGestionInd();
			cs = con.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			rsProduccion = cs.executeQuery(sql.toString());

			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + sql.toString() + " : " + new Date());
			
			mapeo = new MapeoProduccionDiaria();
			
			while(rsProduccion.next())
			{
				
				mapeo.setDescripcion(" TOTAL TURNO ......................");
				mapeo.setMovimiento(rsProduccion.getString("prd_mov"));

				if (new Integer(mapeo.getMovimiento())<50)
				{
					mapeo.setUnidadesEntrada(rsProduccion.getInt("prd_uds"));
				}
				else
				{
					mapeo.setUnidadesSalida(rsProduccion.getInt("prd_uds"));
				}
			}
			mapeo.setMovimiento("");		
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsProduccion!=null)
				{
					rsProduccion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return mapeo;
	}
	
	private String nombreTabla(String r_tabla, String r_ejercicio)
	{
		String tabla = "a_lin_";
		Integer digito = new Integer(RutinasFechas.añoActualYYYY()) - new Integer(r_ejercicio); 
		
		if (digito == 0) 
			tabla = "a_lin_0";
		else
			tabla = "a_lin_" + digito;
		
		return tabla;
	}

	public HashMap<Integer, String> obtenerHashArticulos(String r_area, String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<Integer, String> hash = null;
		int contador = 0;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		String digito ="";
		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_ejercicio.contentEquals(RutinasFechas.añoActualYYYY())) digito="0";
		if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(r_ejercicio);

		
		cadenaSQL.append(" SELECT distinct a_lin_"+digito+".articulo pro_art ");
		cadenaSQL.append(" from a_lin_" + digito);
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and movimiento ='49' "); 
		if (r_area.contentEquals("BIB"))
			cadenaSQL.append(" and articulo like '0111%' ");
		else
			cadenaSQL.append(" and articulo like '0102%' ");

		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and week(a_lin_"+digito+".fecha_documento,3) = '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and week(a_lin_"+digito+".fecha_documento,3) <= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual")) cadenaSQL.append(" and month(a_lin_0.fecha_documento) = " + r_semana);

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<Integer, String>();
			
			while(rsOpcion.next())
			{
				hash.put(contador, rsOpcion.getString("pro_art").trim());
				contador++;
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public HashMap<String, Double> obtenerHashProducciones(String r_area,String r_ejercicio, String r_semana, String r_vista)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		String digito = "0";
		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (new Integer(r_ejercicio) == ejercicioActual) digito="0";
		if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(r_ejercicio);

		
		cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
		cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito );
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and movimiento ='49' "); 
		if (r_area.contentEquals("BIB"))
			cadenaSQL.append(" and articulo like '0111%' ");
		else
			cadenaSQL.append(" and articulo like '0102%' ");
		
		if (r_vista.contentEquals("Semanal")) cadenaSQL.append(" and week(a_lin_" + digito + ".fecha_documento,3) = '" + r_semana + "' ");
		else if (r_vista.contentEquals("Acumulado")) cadenaSQL.append(" and week(a_lin_" + digito + ".fecha_documento,3) <= '" + r_semana + "' ");
		else if (r_vista.contentEquals("Mensual")) cadenaSQL.append(" and month(a_lin_0.fecha_documento) = " + r_semana);
		
		try
		{
			cadenaSQL.append(" group by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("pro_art").trim(), rsOpcion.getDouble("pro_uds"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}

	public Integer obtenerProduccionArticuloAgrupada(String r_articulo, String r_anada, String r_destino)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		StringBuffer cadenaSQL =null;
		String digito = "0";
		Integer produccionObtenida = 0;
		
		Integer r_ejercicio = new Integer(RutinasFechas.añoActualYYYY());
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		
		
		for (int i = 0; i<=3;i++)
		{
			if (new Integer(r_ejercicio) == ejercicioActual) digito="0";
			if (new Integer(r_ejercicio) < ejercicioActual) digito=String.valueOf(r_ejercicio);
	

			cadenaSQL = new StringBuffer();
			
			if (r_articulo!=null && r_articulo.startsWith("0101") && (r_destino == null || r_destino.length()==0))
			{
				/*
				 * Estoy mirando un granel sin destino 
				 */
				/*
				 * Tengo que leer todas las entradas de PS de mi añada
				 */
				cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
				cadenaSQL.append(" sum(unidades*replace(e_articu.marca, ',','.')) pro_uds from a_lin_" + digito );
				cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
				cadenaSQL.append(" where clave_tabla='B' "); 
				cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
				cadenaSQL.append(" and movimiento ='49' ");
				cadenaSQL.append(" and e_articu.tipo_articulo IN ('PT','PS') ");
				cadenaSQL.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '" + r_articulo + "%') "); 

				cadenaSQL.append(" UNION ALL ");
				
				cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
				cadenaSQL.append(" sum((-1)*unidades*replace(e_articu.marca, ',','.')) pro_uds from a_lin_" + digito );
				cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
				cadenaSQL.append(" where clave_tabla='B' "); 
				cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
				cadenaSQL.append(" and movimiento ='99' ");
				cadenaSQL.append(" and e_articu.tipo_articulo='PS' ");
				cadenaSQL.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '" + r_articulo + "%') "); 

			}
			else if (r_articulo!=null && r_articulo.startsWith("0101") && (r_destino != null && r_destino.length()>0))
			{
				/*
				 * Estoy mirando un granel con destino concreto 
				 */
				/*
				 * Tengo que leer todas las entradas de mi añada y mi destino
				 */

				cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
				cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito );
				cadenaSQL.append(" where clave_tabla='B' "); 
				cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
				cadenaSQL.append(" and movimiento ='49' ");
				cadenaSQL.append(" and articulo like '" + r_destino + "%' ");
				
			}
			else if (r_articulo!=null && r_articulo.startsWith("0102"))
			{
				/*
				 * Estoy mirando un PS sin destino concreto 
				 */
				/*
				 * Tengo que leer todas las entradas de PS de mi añada y mi destino
				 */
				cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
				cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito );
				cadenaSQL.append(" where clave_tabla='B' "); 
				cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
				cadenaSQL.append(" and movimiento ='49' ");
				cadenaSQL.append(" and articulo like '" + r_articulo + "%' ");
				
			}
//			cadenaSQL = new StringBuffer();
//			cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
//			cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito );
//			cadenaSQL.append(" where clave_tabla='B' "); 
//			cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
//			
//			if (r_destino!=null && r_destino.length()>0)
//			{
//				cadenaSQL.append(" and articulo like '" + r_destino + "%' ");
//				cadenaSQL.append(" and movimiento ='49' ");
//			}
//			else if (r_articulo!=null && r_articulo.startsWith("0101"))
//			{
//				cadenaSQL.append(" and movimiento ='49' ");
//				cadenaSQL.append(" and articulo in (select padre from e__lconj where hijo like '" + r_articulo + "%') "); 
//			}
//			else
//			{
//				cadenaSQL.append(" and articulo like '" + r_articulo + "%' "); 
//				cadenaSQL.append(" and movimiento ='49' ");
//			}
			
			try
			{
				cadenaSQL.append(" group by 1 "); 
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				
				while(rsOpcion.next())
				{
					produccionObtenida = produccionObtenida + rsOpcion.getInt("pro_uds");
				}
					
//				if (r_articulo!=null && r_articulo.startsWith("0101"))
//				{
//					cs.close();
//					rsOpcion.close();
//					
//					cadenaSQL = new StringBuffer();
//					cadenaSQL.append(" SELECT a_lin_" + digito + ".articulo pro_art, ");
//					cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito );
//					cadenaSQL.append(" where clave_tabla='B' "); 
//					cadenaSQL.append(" and ubicacion = '" + r_anada + "' ");
//					cadenaSQL.append(" and movimiento ='99' ");
//					cadenaSQL.append(" and articulo in (select padre from e__lconj where hijo like '" + r_articulo + "%') ");
//					cadenaSQL.append(" group by 1 "); 
//
//					cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
//					rsOpcion= cs.executeQuery(cadenaSQL.toString());
//					
//					while(rsOpcion.next())
//					{
//						produccionObtenida = produccionObtenida - rsOpcion.getInt("pro_uds");
//					}
//					
//				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!= null)
					{
						rsOpcion.close();
						rsOpcion=null;
					}
					if (cs !=null)
					{
						cs.close();
						cs=null;
					}
					if (con!=null)
					{
						con.close();
						con=null;
					}
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}
			}
			r_ejercicio = r_ejercicio-1;
		}
//		Double valor = new Double(0);
//		
//		if (r_articulo.startsWith("0101") && r_destino == null)
//		{
//			valor = produccionObtenida*0.75;
//			produccionObtenida = new Integer(valor.intValue());
//		}
		
		return produccionObtenida;
	}

	public HashMap<String, Double> obtenerHashProduccionesSemanales(String r_ejercicio)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		String digito = null;

		StringBuffer cadenaSQL = new StringBuffer();
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)>=0) digito="0";
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)<0) digito=String.valueOf(r_ejercicio);

		
		cadenaSQL.append(" SELECT week(a_lin_" + digito + ".fecha_documento,3) semana, ");
		cadenaSQL.append(" sum(unidades) pro_uds from a_lin_" + digito);
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and movimiento ='49' "); 
		cadenaSQL.append(" and articulo like '0102%' "); 
		
		try
		{
			cadenaSQL.append(" group by 1 "); 
			cadenaSQL.append(" order by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				hash.put(rsOpcion.getString("semana").trim(), rsOpcion.getDouble("pro_uds"));
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}
	
	public HashMap<String, Double> obtenerDistintasProduccionesFecha(String r_ejercicio)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		String digito = null;

		double botellasAUT = 0;
		double botellasJAU = 0;
		double botellasETI = 0;
		

		StringBuffer cadenaSQL = new StringBuffer();
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)==0) digito="0";
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)<0) digito=String.valueOf(r_ejercicio);

		
		cadenaSQL.append(" SELECT e_articu.tipo_articulo, movimiento, sum(unidades) pro_uds from a_lin_" + digito);
		cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and e_articu.tipo_articulo IN ('PT','PS') ");
		cadenaSQL.append(" and e_articu.articulo like '0102%' "); 
		cadenaSQL.append(" and fecha_documento in (select fecha from tmp_fechas_turnos" );  
		cadenaSQL.append(" where horario='MAÑANA') " ); 
		cadenaSQL.append(" and a_lin_" + digito + ".hora_ult_modif  <= '14:00:00' ");
		cadenaSQL.append(" group by 1,2 "); 
		
		cadenaSQL.append(" union all ");
		
		cadenaSQL.append(" SELECT e_articu.tipo_articulo, movimiento, sum(unidades) pro_uds from a_lin_" + digito);
		cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and e_articu.tipo_articulo IN ('PT','PS') ");
		cadenaSQL.append(" and e_articu.articulo like '0102%' "); 
		cadenaSQL.append(" and fecha_documento in (select fecha from tmp_fechas_turnos" );  
		cadenaSQL.append(" where horario<>'MAÑANA') " ); 
		cadenaSQL.append(" and a_lin_" + digito + ".hora_ult_modif  > '14:00:00' ");
		cadenaSQL.append(" group by 1,2 "); 
		
		try
		{
			cadenaSQL.append(" order by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("tipo_articulo").contentEquals("PS"))
				{
					if (rsOpcion.getString("movimiento").contentEquals("49"))
					{
						botellasJAU= botellasJAU + rsOpcion.getDouble("pro_uds"); 
					}
					if (rsOpcion.getString("movimiento").contentEquals("99"))
					{
						botellasETI= botellasETI + rsOpcion.getDouble("pro_uds"); 
					}
				}
				if (rsOpcion.getString("tipo_articulo").contentEquals("PT"))
				{
					if (rsOpcion.getString("movimiento").contentEquals("49"))
					{
						botellasAUT= botellasAUT + rsOpcion.getDouble("pro_uds"); 
					}
				}
				
			}
			hash.put("JAULON", botellasJAU); 
			hash.put("ETIQUETADO", botellasETI);
			hash.put("DIRECTA", botellasAUT);
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}

	public HashMap<String, Double> obtenerDistintasProduccionesHorizontalFecha(String r_ejercicio)
	{
		HashMap<String, Double> hash = null;
		
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;
		String digito = null;
		double botellasETH = 0;
		
		StringBuffer cadenaSQL = new StringBuffer();
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)==0) digito="0";
		if (new Integer(r_ejercicio).compareTo(ejercicioActual)<0) digito=String.valueOf(r_ejercicio);
		
		
		cadenaSQL.append(" SELECT e_articu.tipo_articulo, movimiento, sum(unidades) pro_uds from a_lin_" + digito);
		cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and movimiento = '49' "); 
		cadenaSQL.append(" and e_articu.tipo_articulo IN ('PT') ");
		cadenaSQL.append(" and e_articu.articulo like '0102%' "); 
		cadenaSQL.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '0109%' and (descripcion like '%HORIZ%' or descripcion like '%STUCH%')) ");
		cadenaSQL.append(" and fecha_documento in (select fecha from tmp_fechas_turnos" );  
		cadenaSQL.append(" where horario='MAÑANA') " ); 
		cadenaSQL.append(" and a_lin_" + digito + ".hora_ult_modif <= '14:00:00' ");
		cadenaSQL.append(" group by 1,2 "); 
		
		cadenaSQL.append(" union all ");
		
		cadenaSQL.append(" SELECT e_articu.tipo_articulo, movimiento, sum(unidades) pro_uds from a_lin_" + digito);
		cadenaSQL.append(" inner join e_articu on e_articu.articulo = a_lin_" + digito + ".articulo ");
		cadenaSQL.append(" where clave_tabla='B' "); 
		cadenaSQL.append(" and movimiento = '49' "); 
		cadenaSQL.append(" and e_articu.tipo_articulo IN ('PT') ");
		cadenaSQL.append(" and e_articu.articulo like '0102%' "); 
		cadenaSQL.append(" and e_articu.articulo in (select padre from e__lconj where hijo like '0109%' and (descripcion like '%HORIZ%' or descripcion like '%STUCH%')) ");
		cadenaSQL.append(" and fecha_documento in (select fecha from tmp_fechas_turnos" );  
		cadenaSQL.append(" where horario<>'MAÑANA') " ); 
		cadenaSQL.append(" and a_lin_" + digito + ".hora_ult_modif  > '14:00:00' ");
		cadenaSQL.append(" group by 1,2 "); 
		
		try
		{
			cadenaSQL.append(" order by 1 "); 
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hash = new HashMap<String,Double>();
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("tipo_articulo").contentEquals("PT"))
				{
					if (rsOpcion.getString("movimiento").contentEquals("49"))
					{
						botellasETH= botellasETH + rsOpcion.getDouble("pro_uds");
					}
				}
				
			}
			hash.put("HORIZONTAL", botellasETH);
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hash;
	}

	public ArrayList<MapeoDashboardEmbotelladora> recuperarMovimientosProduccionEmbotellado(Integer r_ejercicio, String r_semana, String r_campo, String r_AmbitoTemporal)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEmbotelladora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			this.ambitoTemporal=r_AmbitoTemporal;
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
//			sql.append(" e_articu.marca as formato, " );
			sql.append(" e_articu.tipo_articulo as tipo, " );  // PS o PT
			sql.append(" a_lin_" + digito + ".movimiento as mov, "); //49 o 99
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			*/
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0102%' ");
			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(fecha_documento,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(fecha_documento) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" and a_lin_" + digito + ".movimiento in ('49','99') ");
			sql.append(" GROUP BY  e_articu.tipo_articulo,a_lin_" + digito + ".movimiento ");
			sql.append(" order by  e_articu.tipo_articulo, a_lin_" + digito + ".movimiento ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEmbotelladora>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEmbotelladora mapeo = new MapeoDashboardEmbotelladora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoDashboardEnvasadora> recuperarMovimientosProduccionEnvasado(Integer r_ejercicio, String r_semana, String r_campo, String r_ambitoTemporal)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardEnvasadora> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		int semana=0;
		
		try
		{
			this.ambitoTemporal=r_ambitoTemporal;
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" e_articu.marca as formato, " );
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			if (!this.ambitoTemporal.contentEquals("Acumulado"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			  
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0103%' ");
			
			if (this.ambitoTemporal.contentEquals("Acumulado"))
			{
				sql.append(" and week(fecha_documento,3) <= " + r_semana);
			}
			else
				if (!r_semana.contentEquals("0") && digito.contentEquals("0")) sql.append(" and week(fecha_documento,3)= '" + semanaActual + "' ");
			
			sql.append(" and a_lin_" + digito + ".movimiento = '49' ");
			sql.append(" GROUP BY e_articu.marca, e_articu.grosor,  palet ");
			sql.append(" order by e_articu.marca desc, e_articu.grosor, palet desc ");
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardEnvasadora>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardEnvasadora mapeo = new MapeoDashboardEnvasadora();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("formato"));
				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setPalet(rsMovimientos.getString("palet"));

				if (this.ambitoTemporal.contentEquals("Anual"))
					semana = 53;
				else if (this.ambitoTemporal.contentEquals("Semanal"))
					semana = new Integer(r_semana);
				
				for (int i = 1; i<=semana;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}

				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsMovimientos!=null)
				{
					rsMovimientos.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public ArrayList<MapeoDashboardBIB> recuperarMovimientosProduccionBib(Integer r_ejercicio, String r_semana, String r_campo, String r_ambitoTemporal)
	{
		ResultSet rsMovimientos = null;
		ArrayList<MapeoDashboardBIB> vector = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		
		try
		{
			this.ambitoTemporal=r_ambitoTemporal;
			String digito = "0";
			Integer semanaActual = 0;
			Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
			
			if (r_semana.contentEquals("0")) semanaActual=new Integer(RutinasFechas.semanaActual(r_ejercicio.toString())-1);
			if (r_ejercicio == ejercicioActual) digito="0";
			if (r_ejercicio < ejercicioActual) digito=String.valueOf(r_ejercicio);

			
			sql = new StringBuffer();
			
			sql.append(" SELECT year(fecha_documento) as ejercicio, ");
//			sql.append(" e_articu.marca as formato, " );
			sql.append(" e_articu.sub_familia as tipo, " );  // PS o PT
			sql.append(" a_lin_" + digito + ".movimiento as mov, "); //49 o 99
			/*
			 * por determinar
			 
			sql.append(" case e_articu.grosor when '1' then 'TINTO' ELSE 'ROSADO' END as color, ");
			sql.append(" case substring(e_articu.articulo,7,1) when '4' then '1/2 PALETA' ELSE 'PALET' END as palet, ");
			*/
			
			if (this.ambitoTemporal.contentEquals("Semanal"))
			{
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '1' THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '2' THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '3' THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '4' THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '5' THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '6' THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '7' THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '8' THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '9' THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '10' THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '11' THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '12' THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '13' THEN unidades*" + r_campo + " ELSE 0 END ) AS '13', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '14' THEN unidades*" + r_campo + " ELSE 0 END ) AS '14', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '15' THEN unidades*" + r_campo + " ELSE 0 END ) AS '15', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '16' THEN unidades*" + r_campo + " ELSE 0 END ) AS '16', ");
				 
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '17' THEN unidades*" + r_campo + " ELSE 0 END ) AS '17', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '18' THEN unidades*" + r_campo + " ELSE 0 END ) AS '18', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '19' THEN unidades*" + r_campo + " ELSE 0 END ) AS '19', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '20' THEN unidades*" + r_campo + " ELSE 0 END ) AS '20', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '21' THEN unidades*" + r_campo + " ELSE 0 END ) AS '21', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '22' THEN unidades*" + r_campo + " ELSE 0 END ) AS '22', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '23' THEN unidades*" + r_campo + " ELSE 0 END ) AS '23', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '24' THEN unidades*" + r_campo + " ELSE 0 END ) AS '24', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '25' THEN unidades*" + r_campo + " ELSE 0 END ) AS '25', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '26' THEN unidades*" + r_campo + " ELSE 0 END ) AS '26', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '27' THEN unidades*" + r_campo + " ELSE 0 END ) AS '27', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '28' THEN unidades*" + r_campo + " ELSE 0 END ) AS '28', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '29' THEN unidades*" + r_campo + " ELSE 0 END ) AS '29', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '30' THEN unidades*" + r_campo + " ELSE 0 END ) AS '30', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '31' THEN unidades*" + r_campo + " ELSE 0 END ) AS '31', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '32' THEN unidades*" + r_campo + " ELSE 0 END ) AS '32', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '33' THEN unidades*" + r_campo + " ELSE 0 END ) AS '33', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '34' THEN unidades*" + r_campo + " ELSE 0 END ) AS '34', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '35' THEN unidades*" + r_campo + " ELSE 0 END ) AS '35', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '36' THEN unidades*" + r_campo + " ELSE 0 END ) AS '36', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '37' THEN unidades*" + r_campo + " ELSE 0 END ) AS '37', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '38' THEN unidades*" + r_campo + " ELSE 0 END ) AS '38', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '39' THEN unidades*" + r_campo + " ELSE 0 END ) AS '39', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '40' THEN unidades*" + r_campo + " ELSE 0 END ) AS '40', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '41' THEN unidades*" + r_campo + " ELSE 0 END ) AS '41', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '42' THEN unidades*" + r_campo + " ELSE 0 END ) AS '42', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '43' THEN unidades*" + r_campo + " ELSE 0 END ) AS '43', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '44' THEN unidades*" + r_campo + " ELSE 0 END ) AS '44', ");
				  
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '45' THEN unidades*" + r_campo + " ELSE 0 END ) AS '45', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '46' THEN unidades*" + r_campo + " ELSE 0 END ) AS '46', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '47' THEN unidades*" + r_campo + " ELSE 0 END ) AS '47', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '48' THEN unidades*" + r_campo + " ELSE 0 END ) AS '48', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '49' THEN unidades*" + r_campo + " ELSE 0 END ) AS '49', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '50' THEN unidades*" + r_campo + " ELSE 0 END ) AS '50', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '51' THEN unidades*" + r_campo + " ELSE 0 END ) AS '51', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '52' THEN unidades*" + r_campo + " ELSE 0 END ) AS '52', ");
				sql.append(" SUM( CASE week(fecha_documento,3) WHEN '53' THEN unidades*" + r_campo + " ELSE 0 END ) AS '53', ");
			}
			else if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 1 THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 2 THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 3 THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 4 THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 5 THEN unidades*" + r_campo + " ELSE 0 END ) AS '5', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 6 THEN unidades*" + r_campo + " ELSE 0 END ) AS '6', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 7 THEN unidades*" + r_campo + " ELSE 0 END ) AS '7', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 8 THEN unidades*" + r_campo + " ELSE 0 END ) AS '8', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 9 THEN unidades*" + r_campo + " ELSE 0 END ) AS '9', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 10 THEN unidades*" + r_campo + " ELSE 0 END ) AS '10', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 11 THEN unidades*" + r_campo + " ELSE 0 END ) AS '11', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) = 12 THEN unidades*" + r_campo + " ELSE 0 END ) AS '12', ");
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3) THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (4,5,6) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9) THEN unidades*" + r_campo + " ELSE 0 END ) AS '3', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '4', ");
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (1,2,3,4,5,6)  THEN unidades*" + r_campo + " ELSE 0 END ) AS '1', ");
				sql.append(" SUM( CASE WHEN month(fecha_documento) in (7,8,9,10,11,12) THEN unidades*" + r_campo + " ELSE 0 END ) AS '2', ");
			}
			sql.append(" SUM( unidades*" + r_campo + " ) AS Total ");
			
			sql.append(" FROM a_lin_" + digito + ", e_articu ");
			sql.append(" where a_lin_" + digito + ".articulo = e_articu.articulo ");
			sql.append(" and e_articu.articulo like '0111%' ");
			if (ambitoTemporal.contentEquals("Acumulado") ) sql.append(" and week(fecha_documento,3)<= '" + r_semana + "' ");
			
			if (ambitoTemporal.contentEquals("Mensual"))
			{
				sql.append(" and month(fecha_documento) = " + r_semana);
			}
			else if (ambitoTemporal.contentEquals("Trimestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (4,5,6) ") ;
				if (r_semana.contentEquals("3")) sql.append(" and month(fecha_documento) in (7,8,9) ") ;
				if (r_semana.contentEquals("4")) sql.append(" and month(fecha_documento) in (10,11,12) ") ;
			}
			else if (ambitoTemporal.contentEquals("Semestral"))
			{
				if (r_semana.contentEquals("1")) sql.append(" and month(fecha_documento) in (1,2,3,4,5,6) ") ;
				if (r_semana.contentEquals("2")) sql.append(" and month(fecha_documento) in (7,8,9,10,11,12) ") ;
			}
			sql.append(" and a_lin_" + digito + ".movimiento in ('49') ");
			sql.append(" GROUP BY  e_articu.sub_familia,a_lin_" + digito + ".movimiento ");
			sql.append(" order by  e_articu.sub_familia, a_lin_" + digito + ".movimiento ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsMovimientos = cs.executeQuery(sql.toString());
			
			vector = new ArrayList<MapeoDashboardBIB>();
			
			while(rsMovimientos.next())
			{
				MapeoDashboardBIB mapeo = new MapeoDashboardBIB();
				HashMap<String, Double> hash = new HashMap<String, Double>();
				int tope = 0;
				
				if (ambitoTemporal.contentEquals("Semanal"))
				{
					tope = 53;
				}
				else if (ambitoTemporal.contentEquals("Mensual"))
				{
					tope = 12;
				}
				else if (ambitoTemporal.contentEquals("Trimestral"))
				{
					tope = 4;
				}
				else if (ambitoTemporal.contentEquals("Semestral"))
				{
					tope = 2;
				}
				else if (ambitoTemporal.contentEquals("Acumulado") || ambitoTemporal.contentEquals("Anual"))
				{
					tope = -1;
				}
				
				mapeo.setEjercicio(rsMovimientos.getInt("ejercicio"));
				mapeo.setFormato(rsMovimientos.getInt("mov"));
//				mapeo.setColor(rsMovimientos.getString("color"));
				mapeo.setTipo(rsMovimientos.getString("tipo"));

				for (int i = 1; i<=tope;i++)
				{
					hash.put(String.valueOf(i), rsMovimientos.getDouble(String.valueOf(i)));
				}
				
				mapeo.setHashValores(hash);
				mapeo.setTotal(rsMovimientos.getDouble("Total"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
}
