package borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view;

import java.util.HashMap;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaTotalesProduccionDiaria extends Window
{
	private VerticalLayout principal = null;
	private HorizontalLayout botonera = null;
	private HorizontalLayout pnlDatos = null;
	private Button btnBotonCVentana = null;

	private String tituloPantalla = null;
	private String articulo = null;
	private String fecha = null;
	private String usuario = null;
	private String lote = null;
	private String añada = null;
	private String tipoTotal = null;
	private Integer dias = null;
	
	private Label lblArticulo = null;
	private Label lblLote = null;
	private Label lblAñada= null;
	
	private Label lblEntradaTotalDia= null;
	private Label lblSalidaTotalDia= null;
	private Label lblEntradaTotalNoche= null;
	private Label lblSalidaTotalNoche= null;
	
	public pantallaTotalesProduccionDiaria(String r_fecha, String r_usuario, String r_articulo, String r_lote, String r_añada, String r_titulo)
	{
		this.tituloPantalla=r_titulo;
		this.articulo=r_articulo;
		this.fecha=r_fecha;
		this.usuario=r_usuario;
		this.dias = 0;
		
		this.tipoTotal="A";
		
		if (r_lote!=null)
		{
			this.lote=r_lote;
			this.tipoTotal="L";
		}
		
		if (r_añada!=null)
		{
			this.añada=r_añada;
			this.tipoTotal="U";
		}
		
		this.cargarPantalla();
		this.cargarListeners();
		this.cargarDatos();
	}
	
	public pantallaTotalesProduccionDiaria(String r_fecha, String r_usuario, String r_articulo, String r_lote, String r_añada, String r_titulo, Integer r_dias)
	{
		this.tituloPantalla="Produccion últimos " + r_dias + " dias ";
		this.articulo=r_articulo;
		this.fecha=r_fecha;
		this.usuario=r_usuario;
		this.dias=r_dias;
		
		this.tipoTotal="A";
		
		if (r_lote!=null)
		{
			this.lote=r_lote;
			this.tipoTotal="L";
		}
		
		if (r_añada!=null)
		{
			this.añada=r_añada;
			this.tipoTotal="U";
		}
		
		this.cargarPantalla();
		this.cargarListeners();
		this.cargarDatos();
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setSpacing(true);
		principal.setMargin(true);

		botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		botonera.setSpacing(true);
		botonera.setMargin(true);
		
		this.setCaption(this.tituloPantalla);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("300px");
		
		
		this.pnlDatos = new HorizontalLayout();
		this.pnlDatos.setWidthUndefined();
		this.pnlDatos.setHeight("100px");
		this.pnlDatos.setSpacing(true);
		this.pnlDatos.setMargin(true);

		if (this.tipoTotal.equals("A"))
		{
			lblArticulo = new Label();
			lblArticulo.setCaption("Articulo:");	
			
			this.pnlDatos.addComponent(lblArticulo);
		}
		else if (this.tipoTotal.equals("L"))
		{
			lblArticulo = new Label("Articulo:");
			lblArticulo.setCaption("Articulo:");
			lblLote = new Label("Lote:");		
			lblLote.setCaption("Lote");
			
			this.pnlDatos.addComponent(lblArticulo);
			this.pnlDatos.addComponent(lblLote);
		}

		else if (this.tipoTotal.equals("U"))
		{
			lblArticulo = new Label("Articulo:");
			lblArticulo.setCaption("Articulo:");
			lblLote = new Label("Lote:");		
			lblLote.setCaption("Lote");
			lblAñada = new Label("Añada:");
			lblAñada.setCaption("Añada");
			
			this.pnlDatos.addComponent(lblArticulo);
			this.pnlDatos.addComponent(lblLote);
			this.pnlDatos.addComponent(lblAñada);

		}

		lblEntradaTotalDia= new Label();
		
		if (this.dias==0)
		{
			lblEntradaTotalDia.setCaption("Entradas Turno Dia");
			lblSalidaTotalDia = new Label();
			lblSalidaTotalDia.setCaption("Salidas Turno Dia");
			lblEntradaTotalNoche = new Label();
			lblEntradaTotalNoche.setCaption("Entradas Turno Noche");
			lblSalidaTotalNoche = new Label();
			lblSalidaTotalNoche.setCaption("Salidas Turno Noche");
			
			this.pnlDatos.addComponent(lblEntradaTotalDia);
			this.pnlDatos.addComponent(lblSalidaTotalDia);
			this.pnlDatos.addComponent(lblEntradaTotalNoche);
			this.pnlDatos.addComponent(lblSalidaTotalNoche);
		}
		else
		{
			lblEntradaTotalDia.setCaption("Total Entradas ");
			lblSalidaTotalDia = new Label();
			lblSalidaTotalDia.setCaption("Total Salidas ");
			lblEntradaTotalNoche = new Label();
			lblEntradaTotalNoche.setCaption("Entradas Turno Noche");
			lblSalidaTotalNoche = new Label();
			lblSalidaTotalNoche.setCaption("Salidas Turno Noche");
			
			this.pnlDatos.addComponent(lblEntradaTotalDia);
			this.pnlDatos.addComponent(lblSalidaTotalDia);
//			this.pnlDatos.addComponent(lblEntradaTotalNoche);
//			this.pnlDatos.addComponent(lblSalidaTotalNoche);			
		}
		principal.addComponent(this.pnlDatos);
		
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
	}
	
	private void cargarDatos()
	{
		
		consultaProduccionDiariaServer cps = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
		HashMap<String, Integer> rdo =	null;
		
		if (this.dias!=0)
		{
			rdo= cps.datosTotalArticulo(this.fecha, this.usuario, this.articulo, this.lote, this.añada, this.dias);
		}
		else
		{
			rdo= cps.datosTotalArticulo(this.fecha, this.usuario, this.articulo, this.lote, this.añada);
		}
		
		this.lblArticulo.setValue(this.articulo);
		
		switch (this.tipoTotal)
		{
			case "L":
			{
				this.lblLote.setValue(this.lote);
				break;
			}
			case "U":
			{
				this.lblLote.setValue(this.lote);
				this.lblAñada.setValue(this.añada);
				break;
			}
		}
		
		lblEntradaTotalDia.setValue(rdo.get("EntradasDia").toString());
		lblSalidaTotalDia.setValue(rdo.get("SalidasDia").toString());
		lblEntradaTotalNoche.setValue(rdo.get("EntradasNoche").toString());
		lblSalidaTotalNoche.setValue(rdo.get("SalidasNoche").toString());

	}
}