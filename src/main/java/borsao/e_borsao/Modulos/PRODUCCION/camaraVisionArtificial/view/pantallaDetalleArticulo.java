package borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.server.consultaDashboardArticuloServer;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMArticulo;
import borsao.e_borsao.Modulos.GENERALES.DashboardLights.view.pantallaCRMCompras;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo.MapeoGridCamaraVisionArtificial;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.server.consultaCamaraVisionArtificialServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.view.pantallaLineasProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaDetalleArticulo extends Window
{

	private final String titulo = "";
	
	private VerticalLayout barAndGridLayout = null;    
    private HorizontalLayout topLayoutL = null;
    private Label lblTitulo = null; 
    private Button opcSalir = null;
    private Combo cmbTipo = null;
    
    private Panel panelResumen =null;
    private GridLayout centralTop2 = null;

    private Panel panelResumenSemana =null;
    private GridLayout centralTopSemana = null;
    
	public TextField txtEjercicio= null;

	private String descripcion = null;
	private String articulo = null;
	private String ejercicio = null;
	public boolean acceso = true;
	public Grid gridDatos = null;
	
	public pantallaDetalleArticulo(String r_titulo, String r_ejercicio, String r_articulo, String r_descr)
	{
		acceso = this.obtenerPermisos();
		
		if (acceso)
		{
			this.descripcion= r_descr;
			this.articulo= r_articulo;
			this.ejercicio = r_ejercicio;
			
			this.setCaption(r_titulo);
			this.addStyleName("crud-view");
			this.center();
			
			this.setModal(true);
			this.setClosable(true);
			this.setResizable(true);
			this.setResponsive(true);
			
			this.cargarPantalla();
			this.setSizeFull();
	
			this.cargarListeners();
			cargarGrid();
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia("No tienes acceso a esta pantalla");
			close();
		}
	}

	private boolean obtenerPermisos()
	{
		String permisos = consultaDashboardArticuloServer.getInstance(CurrentUser.get()).comprobarAccesos();
		
		if (permisos!=null)
		{
			Integer per = new Integer(permisos);
			if (per>=80)
			{
				return true;
			}
			else 
				return false;
		}
		else
			return false;
	}
	private void cargarPantalla()
	{
    	setSizeFull();
    	addStyleName("scrollable");
    	
    	this.barAndGridLayout = new VerticalLayout();
        this.barAndGridLayout.setSizeFull();
        this.barAndGridLayout.setStyleName("crud-main-layout");

	    	this.topLayoutL = new HorizontalLayout();
	    	this.topLayoutL.setSpacing(true);
	    	this.topLayoutL.setSizeUndefined();
	    	this.topLayoutL.setMargin(true);
		    	
		    	this.opcSalir= new Button("Salir");
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
		    	this.opcSalir.addStyleName(ValoTheme.BUTTON_TINY);
		    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
		    	
		    	this.txtEjercicio=new TextField("Ejercicio");
		    	this.txtEjercicio.setValue(this.ejercicio);
	    		this.txtEjercicio.setEnabled(false);
	    		this.txtEjercicio.setWidth("125px");
	    		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
	    		
		    	this.lblTitulo= new Label();
		    	this.lblTitulo.setValue(titulo);
		    	this.lblTitulo.setStyleName("labelTituloVentas");
	    	
	    	this.topLayoutL.addComponent(this.opcSalir);
	    	this.topLayoutL.setComponentAlignment(this.opcSalir,  Alignment.BOTTOM_LEFT);
    		this.topLayoutL.addComponent(this.txtEjercicio);
    		this.topLayoutL.addComponent(this.lblTitulo);
	    	this.topLayoutL.setComponentAlignment(this.lblTitulo,  Alignment.BOTTOM_RIGHT);

	    	this.panelResumenSemana = new Panel();
	    	this.panelResumenSemana.setSizeFull();
//	    	this.panelResumenSemana.addStyleName("showpointer");

		    	this.centralTopSemana = new GridLayout(2,1);
		    	this.centralTopSemana.setSizeFull();
		    	this.centralTopSemana.setMargin(false);

	    	this.panelResumenSemana.setContent(this.centralTopSemana);
	    	
    	this.barAndGridLayout.addComponent(this.topLayoutL);
    	this.barAndGridLayout.addComponent(this.panelResumenSemana);
    	this.barAndGridLayout.setExpandRatio(this.panelResumenSemana, 1);
    	this.setContent(this.barAndGridLayout);
	}

	private void cargarListeners() 
    {

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			close();
    		}
    	});
    }
    
	private void cargarGrid()
	{
		IndexedContainer container =null;
		Iterator<MapeoGridCamaraVisionArtificial> iterator = null;
		consultaCamaraVisionArtificialServer cus = null;
		ArrayList<MapeoGridCamaraVisionArtificial> r_vector=null;
		DecimalFormat formato = null;
		
    	formato = new DecimalFormat();
		formato.setMinimumFractionDigits(2);
		formato.setMaximumFractionDigits(2);

		cus= consultaCamaraVisionArtificialServer.getInstance(CurrentUser.get());
		centralTopSemana.removeAllComponents();
    	
    	r_vector=cus.datosOpcionesEjercicioArticulo(txtEjercicio.getValue(), this.articulo);

    	MapeoGridCamaraVisionArtificial mapeo = null;
    		
		container = new IndexedContainer();
		container.addContainerProperty("fecha", String.class, null);
		container.addContainerProperty("articulo", String.class, null);
		container.addContainerProperty("descripcion", String.class, null);
		container.addContainerProperty("totalVistas", Integer.class, null);
		container.addContainerProperty("totalOK", Integer.class, null);
		container.addContainerProperty("totalNOK", Integer.class, null);
		container.addContainerProperty("def", Double.class, null);
			
		iterator = r_vector.iterator();
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeo= (MapeoGridCamaraVisionArtificial) iterator.next();
			
			file.getItemProperty("fecha").setValue(mapeo.getFecha().substring(6,8) + "/" + mapeo.getFecha().substring(4,6) + "/" + mapeo.getFecha().substring(0,4));
			file.getItemProperty("articulo").setValue(mapeo.getArticulo());
			file.getItemProperty("descripcion").setValue(mapeo.getDescripcion());
			file.getItemProperty("totalVistas").setValue(mapeo.getTotalVistas());
			file.getItemProperty("totalOK").setValue(mapeo.getTotalOK());
			file.getItemProperty("totalNOK").setValue(mapeo.getTotalNOK());
			file.getItemProperty("def").setValue(mapeo.getPorcDefectivo());
		}

		gridDatos= new Grid(container);
		gridDatos.setSelectionMode(SelectionMode.SINGLE);
		gridDatos.setSizeFull();
		gridDatos.addStyleName("smallgrid");
		
		gridDatos.getColumn("fecha").setHeaderCaption("Fecha");
		gridDatos.getColumn("fecha").setWidth(110);
		gridDatos.getColumn("articulo").setHeaderCaption("Articulo");
		gridDatos.getColumn("articulo").setWidth(120);
		gridDatos.getColumn("descripcion").setHeaderCaption("Descripcion");
		gridDatos.getColumn("descripcion").setWidth(250);
		gridDatos.getColumn("totalVistas").setHeaderCaption("Vistas");
		gridDatos.getColumn("totalVistas").setWidth(110);
		gridDatos.getColumn("totalVistas").setRenderer((Renderer) new NumberRenderer());
		gridDatos.getColumn("totalOK").setHeaderCaption("OK");
		gridDatos.getColumn("totalOK").setWidth(110);
		gridDatos.getColumn("totalOK").setRenderer((Renderer) new NumberRenderer());
		gridDatos.getColumn("totalNOK").setHeaderCaption("NOK");
		gridDatos.getColumn("totalNOK").setWidth(85);
		gridDatos.getColumn("totalNOK").setRenderer((Renderer) new NumberRenderer());
		gridDatos.getColumn("def").setHeaderCaption("%");
		gridDatos.getColumn("def").setWidth(75);
		gridDatos.getColumn("def").setRenderer((Renderer) new NumberRenderer(formato));

		gridDatos.getColumn("articulo").setHidden(true);
		gridDatos.getColumn("descripcion").setHidden(true);
		this.calcularTotalGrid();
		this.cargarListenersGrid();
		
//		Double totalU = new Double(0) ;
//    	Double valor = null;
//        	
//    	FooterRow footer = gridDatos.appendFooterRow();
//        	
//    	Indexed indexed = gridDatos.getContainerDataSource();
//    	List<?> list = new ArrayList<Object>(indexed.getItemIds());
//    	
//		totalU = new Double(0) ;
//    	for(Object itemId : list)
//    	{
//    		Item item = indexed.getItem(itemId);
//        		
//    		valor = (Double) item.getItemProperty("tiempo").getValue();
//    		if (valor!=null)
//    			totalU += valor;
//				
//    	}
//    	footer.getCell("incidencia").setText("Totales ");
//    	footer.getCell("tiempo").setText(RutinasNumericas.formatearDouble(totalU.doubleValue()));
//    	footer.getCell("tiempo").setStyleName("Rcell-pie");
		
    	gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("totalVistas".equals(cellReference.getPropertyId())||"totalOK".equals(cellReference.getPropertyId())||"totalNOK".equals(cellReference.getPropertyId())||"def".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
		
		String titulo = null;
		titulo = "Detalle Articulo " + this.articulo + " " + this.descripcion + " " + this.txtEjercicio.getValue(); 

		Panel panel = new Panel(titulo);
    	panel.setSizeUndefined(); // Shrink to fit content
    	panel.setContent(gridDatos);
    	centralTopSemana.addComponent(panel,0,0);
    	
    	this.cargarResumen();
    }
	
	private void cargarListenersGrid()
	{
		this.gridDatos.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	
            	if (event.getPropertyId().toString().equals("fecha"))
            	{
            		/*
            		 * acceso a parte produccion
            		 */
            		
            	}
            	else if (event.getPropertyId().toString().equals("totalVistas"))
            	{
            		/*
            		 * acceso a produccion diaria
            		 */
            		Item it = gridDatos.getContainerDataSource().getItem(event.getItemId());
            		String fec = it.getItemProperty("fecha").getValue().toString();
            		
					try 
					{
						pantallaLineasProduccion vt;
						vt = new pantallaLineasProduccion("EMBOTELLADORA", new Integer(txtEjercicio.getValue()), articulo, RutinasFechas.convertirAFechaMysql(fec));
						getUI().addWindow(vt);
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
            	}

            }
    	});
	}
	private void calcularTotalGrid()
	{
		FooterRow footer=null;
    	Double totales=null;
    	Double totalOK=null;
    	Double totalNOK=null;
    	Double valorTotales = new Double(0);
    	Double valorOK = new Double(0);
    	Double valorNOK = new Double(0);

    	if (this.gridDatos.getFooterRowCount()==0 ) footer=this.gridDatos.appendFooterRow();
    	
    	Indexed indexed = this.gridDatos.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	if (item.getItemProperty("totalVistas").getValue()!=null)
        	{
        		totales = new Double(item.getItemProperty("totalVistas").getValue().toString());
        		valorTotales=valorTotales+totales;
        	}
        	if (item.getItemProperty("totalOK").getValue()!=null)
        	{
        		totalOK = new Double(item.getItemProperty("totalOK").getValue().toString());
        		valorOK=valorOK+totalOK;
        	}
        	if (item.getItemProperty("totalNOK").getValue()!=null)
        	{
        		totalNOK = new Double(item.getItemProperty("totalNOK").getValue().toString());
        		valorNOK=valorNOK+totalNOK;
        	}
        }
        
        footer.getCell("fecha").setText("Totales ");
		footer.getCell("totalVistas").setText(RutinasNumericas.formatearDoubleDecimales(valorTotales,0).toString());
		footer.getCell("totalOK").setText(RutinasNumericas.formatearDoubleDecimales(valorOK,0).toString());
		footer.getCell("totalNOK").setText(RutinasNumericas.formatearDoubleDecimales(valorNOK,0).toString());
		footer.getCell("def").setText(RutinasNumericas.formatearDoubleDecimales(new Double((valorNOK/valorTotales *100)),2).toString());		
		footer.getCell("totalVistas").setStyleName("Rcell-pie");
		footer.getCell("totalOK").setStyleName("Rcell-pie");
		footer.getCell("totalNOK").setStyleName("Rcell-pie");
		footer.getCell("def").setStyleName("Rcell-pie");
	}
	
    private void cargarResumen()
	{
    	
		Double media=null;
		HashMap<String, Double> hashDefectivo = null;
		HashMap<String, Double> hashObjetivoDefectivo = null;
	
		consultaCamaraVisionArtificialServer ccvs = new consultaCamaraVisionArtificialServer(CurrentUser.get());
		hashDefectivo= ccvs.recuperarDefectivoArticuloEjercicio(this.txtEjercicio.getValue(), this.articulo);
		hashObjetivoDefectivo= ccvs.recuperarObjetivoDefectivoArticuloEjercicio(this.txtEjercicio.getValue(), this.articulo);
		
		String tituloY = null;
		tituloY="Defectivo / Mes";
		JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Defectivo", "Meses", tituloY, hashDefectivo, hashObjetivoDefectivo, null, txtEjercicio.getValue(), "Objetivo",null,null, null,1, 0, 15, true,false,false,12);
		
		centralTopSemana.addComponent(wrapper,1,0);
		centralTopSemana.setColumnExpandRatio(1, 1);	    
	}
}