package borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.vaadin.addon.JFreeChartWrapper;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.graficas.linea;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.CalendarioVacacionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo.CamaraVisionArtificialGrid;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo.MapeoGridCamaraVisionArtificial;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.server.consultaCamaraVisionArtificialServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class CamaraVisionArtificialView extends GridViewRefresh {

	private final String titulo = "control Camara Vision Artificial";
	public static final String VIEW_NAME = "Control Camara Vision Artificial";

	public consultaCamaraVisionArtificialServer cus =null;
	public CamaraVisionArtificialView app = null;
	public boolean conTotales = true;
	public boolean modificando = false;
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean soloConsulta = false;
	private final boolean autoSincronizacion = false;
	private HashMap<String,String> filtrosRec = null;	
	private HashMap<String,Double> hashTotales = null;	

	public HorizontalLayout hor = null;
	
	public TextField txtEjercicio = null;
	public TextField txtFecha = null;
	public Label lblDefectivo = null;
	public Label lblTotales = null;
	public Label lblOK = null;
	public Label lblNOK= null;
	public Label lblValorDefectivo = null;
	public Label lblValorTotales = null;
	public Label lblValorOK = null;
	public Label lblValorNOK= null;
	private String anchoLabelTitulo = "160px";
	private String anchoLabelDatos = "150px";
	private Panel panelResumen = null;
	private GridLayout centralTop2 = null;
	private Panel panelGrafica = null;
	private GridLayout centralGrafica = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public CamaraVisionArtificialView() 
    {
    	this.cus= consultaCamaraVisionArtificialServer.getInstance(CurrentUser.get());
    	this.opcionesEscogidas = new HashMap<String , String>();
    	
    	
    }

    public void cargarPantalla() 
    {

    	this.txtEjercicio = new TextField("Ejercicio");
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());

		this.txtFecha = new TextField("Fecha");
		this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
			
		this.cabLayout.addComponent(this.txtEjercicio);
		this.cabLayout.addComponent(this.txtFecha);
		
    	this.hor = new HorizontalLayout();
    	this.hor.setSizeUndefined();
    	this.hor.setSpacing(true);

		this.cargarResumenCamara();
		this.cargarCombo();
		this.cargarListeners();
		this.establecerModo();
	
		this.barAndGridLayout.addComponent(this.hor);
		this.generarGrid(opcionesEscogidas);
		
		app = this;
		
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    }

    private void cargarResumenCamara()
    {
    		this.hashTotales=this.cus.devolverTotalesCV();
    	
	    	panelResumen = new Panel("Resumen Global");
		    panelResumen.setSizeUndefined();
	//	    panelResumen.setWidth("100%");
		    panelResumen.setVisible(true);
		    panelResumen.addStyleName("showpointer");
//		    new PanelCaptionBarToggler<Panel>( panelResumen );
	
		    	this.centralTop2 = new GridLayout(2,4);
		    	this.centralTop2.setSpacing(false);
	//	    	this.centralTop2.setWidth("100%");
		    	this.centralTop2.setMargin(true);
		    	
				this.lblDefectivo = new Label();
				this.lblDefectivo.setWidth(anchoLabelTitulo);
				this.lblDefectivo.addStyleName("lblTitulo");
				this.lblDefectivo.setValue("% Defectivo Global");
			    	
					this.lblValorDefectivo = new Label();
					this.lblValorDefectivo.setWidth(anchoLabelDatos);
					this.lblValorDefectivo.addStyleName("lblDatos");
					this.lblValorDefectivo.setValue(RutinasNumericas.formatearDoubleDecimales(hashTotales.get("def"),2).toString());
				
				this.lblTotales= new Label();
				this.lblTotales.setWidth(anchoLabelTitulo);
				this.lblTotales.addStyleName("lblTitulo");
				this.lblTotales.setValue("Total Botellas Vistas");
				
					this.lblValorTotales = new Label();
					this.lblValorTotales.setWidth(anchoLabelDatos);
					this.lblValorTotales.addStyleName("lblDatos");
					this.lblValorTotales.setValue(RutinasNumericas.formatearDoubleDecimales(hashTotales.get("totales"),2).toString());
				
				this.lblOK= new Label();
				this.lblOK.setWidth(anchoLabelTitulo);
				this.lblOK.addStyleName("lblTitulo");
				this.lblOK.setValue("Total OK");
	
					this.lblValorOK = new Label();
					this.lblValorOK.setWidth(anchoLabelDatos);
					this.lblValorOK.addStyleName("lblDatos");
					this.lblValorOK.setValue(RutinasNumericas.formatearDoubleDecimales(hashTotales.get("ok"),2).toString());
				
				this.lblNOK= new Label();
				this.lblNOK.setWidth(anchoLabelTitulo);
				this.lblNOK.addStyleName("lblTitulo");
				this.lblNOK.setValue("Total NOK");
				
					this.lblValorNOK = new Label();
					this.lblValorNOK.setWidth(anchoLabelDatos);
					this.lblValorNOK.addStyleName("lblDatos");
					this.lblValorNOK.setValue(RutinasNumericas.formatearDoubleDecimales(hashTotales.get("nok"),2).toString());
					
				this.centralTop2.addComponent(this.lblDefectivo, 0, 0);
				this.centralTop2.addComponent(this.lblValorDefectivo, 1, 0);
				this.centralTop2.addComponent(this.lblTotales, 0, 1);
				this.centralTop2.addComponent(this.lblValorTotales, 1, 1);
				this.centralTop2.addComponent(this.lblOK, 0, 2);
				this.centralTop2.addComponent(this.lblValorOK, 1, 2);
				this.centralTop2.addComponent(this.lblNOK, 0, 3);
				this.centralTop2.addComponent(this.lblValorNOK, 1, 3);
				this.panelResumen.setContent(centralTop2);
			
		hor.addComponent(this.panelResumen);

	    	panelGrafica = new Panel("Evolución " + this.txtEjercicio.getValue());
		    panelGrafica.setSizeFull();
		    panelGrafica.setWidth("100%");
		    panelGrafica.setVisible(true);
		    panelGrafica.addStyleName("showpointer");

			    this.centralGrafica = new GridLayout(1,1);
//		    	this.centralGrafica.setSpacing(false);
		    	this.centralGrafica.setSizeFull();
		    	centralGrafica.setWidth("100%");
//		    	this.centralGrafica.setMargin(true);
		    	this.panelGrafica.setContent(centralGrafica);
		    	
		    	
				HashMap<String, Double> hashDefectivo = null;
				HashMap<String, Double> hashObjetivoDefectivo = null;
			
				consultaCamaraVisionArtificialServer ccvs = new consultaCamaraVisionArtificialServer(CurrentUser.get());
				hashDefectivo= ccvs.recuperarDefectivoEjercicio(new Integer(this.txtEjercicio.getValue()));
				hashObjetivoDefectivo= ccvs.recuperarObjetivoDefectivoEjercicio(new Integer(this.txtEjercicio.getValue()));
			
				String tituloY = null;
				tituloY="Defectivo / Mes";
				JFreeChartWrapper wrapper = linea.generarGrafico("Evolución Def.", "Meses", tituloY, hashDefectivo, hashObjetivoDefectivo, null, txtEjercicio.getValue(), "Objetivo", null, null, null,1, 0, 10, true,false,false,12);
				centralGrafica.addComponent(wrapper,0,0);
				
				com.vaadin.event.MouseEvents.ClickListener listenerDisp = new com.vaadin.event.MouseEvents.ClickListener() {
					
					@Override
					public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
						pantallaEvolucion vt = new pantallaEvolucion("Disponibilidad", txtEjercicio.getValue().toString());
						getUI().addWindow(vt);
					}
				};
				wrapper.addClickListener(listenerDisp);
				panelGrafica.addClickListener(listenerDisp);

				
		hor.addComponent(this.panelGrafica);
		    
		
			
    }
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoGridCamaraVisionArtificial> r_vector=null;
    	MapeoGridCamaraVisionArtificial mapeoCamaraVisionArtificial =null;
    	
    	mapeoCamaraVisionArtificial= new MapeoGridCamaraVisionArtificial();
    	mapeoCamaraVisionArtificial.setEjercicio(new Integer(txtEjercicio.getValue().toString()));

    	r_vector=this.cus.datosOpcionesGlobal(this.txtEjercicio.getValue(),null,this.txtFecha.getValue());
    	
    	this.presentarGrid(r_vector);
    	((CamaraVisionArtificialGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    }
    
    public void verForm(boolean r_busqueda)
    {
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((CalendarioVacacionesGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((CalendarioVacacionesGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((CamaraVisionArtificialGrid) this.grid).activadaVentanaPeticion && !((CamaraVisionArtificialGrid) this.grid).ordenando)
    	{
    	}    		
    }
    
    public void generarGrid(MapeoGridCamaraVisionArtificial r_mapeo)
    {
    	ArrayList<MapeoGridCamaraVisionArtificial> r_vector=null;
    	r_vector=this.cus.datosOpcionesGlobal(this.txtEjercicio.getValue(),null,this.txtFecha.getValue());
    	this.presentarGrid(r_vector);
    	((CamaraVisionArtificialGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoGridCamaraVisionArtificial> r_vector)
    {
    	grid = new CamaraVisionArtificialGrid(this, r_vector);
    	if (((CamaraVisionArtificialGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((CamaraVisionArtificialGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
		this.txtEjercicio.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((CamaraVisionArtificialGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					hor.removeAllComponents();
					grid=null;	
					centralTop2.removeAllComponents();
					centralGrafica.removeAllComponents();
					panelResumen = null;
					panelGrafica = null;
					cargarResumenCamara();
					MapeoGridCamaraVisionArtificial mapeo = new MapeoGridCamaraVisionArtificial();
					generarGrid(mapeo);
				}
			}
		});

		this.txtFecha.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((CamaraVisionArtificialGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					hor.removeAllComponents();
					grid=null;	
					centralTop2.removeAllComponents();
					centralGrafica.removeAllComponents();
					panelResumen = null;
					panelGrafica = null;
					cargarResumenCamara();
					MapeoGridCamaraVisionArtificial mapeo = new MapeoGridCamaraVisionArtificial();
					generarGrid(mapeo);
				}
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
		this.setSoloConsulta(true);
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	private void establecerBotonAccion(Object r_fila)
	{
	}

	public void imprimirVacaciones()
	{
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	private void cargarCombo()
	{
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((CalendarioVacacionesGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

