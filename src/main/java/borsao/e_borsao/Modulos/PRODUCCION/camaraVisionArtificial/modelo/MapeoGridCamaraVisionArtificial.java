package borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoGridCamaraVisionArtificial extends MapeoGlobal
{
	private String articulo = null;
	private String descripcion = null;
	private String fecha = null;
	private String mes= null;
	private Integer totalVistas = null;
	private Integer totalOK = null;
	private Integer totalNOK = null;
	private Double porcDefectivo = null;
	private String bar;
	private Integer ejercicio = null;
	
	public MapeoGridCamaraVisionArtificial() {
	}

	public String getArticulo() {
		return articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getFecha() {
		return fecha;
	}

	public String getMes() {
		return mes;
	}

	public Integer getTotalVistas() {
		return totalVistas;
	}

	public Integer getTotalOK() {
		return totalOK;
	}

	public Integer getTotalNOK() {
		return totalNOK;
	}

	public Double getPorcDefectivo() {
		return porcDefectivo;
	}

	public String getBar() {
		return bar;
	}

	public Integer getEjercicio() {
		return ejercicio;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public void setTotalVistas(Integer totalVistas) {
		this.totalVistas = totalVistas;
	}

	public void setTotalOK(Integer totalOK) {
		this.totalOK = totalOK;
	}

	public void setTotalNOK(Integer totalNOK) {
		this.totalNOK = totalNOK;
	}

	public void setPorcDefectivo(Double porcDefectivo) {
		this.porcDefectivo = porcDefectivo;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


}