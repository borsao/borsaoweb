package borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.GENERALES.Calendario.modelo.MapeoCalendario;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo.MapeoGridCamaraVisionArtificial;

public class consultaCamaraVisionArtificialServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaCamaraVisionArtificialServer instance;
	private static int min = 60;
	public consultaCamaraVisionArtificialServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCamaraVisionArtificialServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCamaraVisionArtificialServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoGridCamaraVisionArtificial> datosOpcionesGlobal(String r_ejercicio, String r_articulo, String r_fecha)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGridCamaraVisionArtificial> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT ");
//		cadenaSQL.append(" substring(fecha,1,6) cv_fec, ");
		cadenaSQL.append(" qc_cv_generalCounters.articulo cv_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo cv_des, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO)/sum(Total)*100 cv_def ");
     	cadenaSQL.append(" FROM qc_cv_generalCounters ");
     	cadenaSQL.append(" inner join e_articu on e_articu.articulo = qc_cv_generalCounters.articulo ");
     	
     	if (r_articulo!=null && r_articulo.length()>0)
     	{
     		cadenaSQL.append(" WHERE qc_cv_generalCounters.articulo = '" + r_articulo + "'");
     	}
     	else if (r_fecha!=null && r_fecha.length()>0)
     	{
     		cadenaSQL.append(" where fecha like '" + r_fecha + "%'");
     	}
     	else if (r_ejercicio!=null && r_ejercicio.length()>0)
     	{
     		cadenaSQL.append(" WHERE fecha like '" + r_ejercicio + "%'");
     	}
//     	cadenaSQL.append(" group by substring(fecha,1,6),2,3 ");
     	cadenaSQL.append(" group by 1,2 ");
     	
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoGridCamaraVisionArtificial>();
			
			while(rsOpcion.next())
			{
				MapeoGridCamaraVisionArtificial mapeo = new MapeoGridCamaraVisionArtificial();
				/*
				 * recojo mapeo calendario
				 */
//				mapeo.setIdCodigo(rsOpcion.getInt("cv_id"));
//				mapeo.setFecha(rsOpcion.getString("cv_fec").substring(4, 6));
		    	mapeo.setArticulo(rsOpcion.getString("cv_art"));
		    	mapeo.setDescripcion(rsOpcion.getString("cv_des"));

		    	mapeo.setTotalVistas(rsOpcion.getInt("cv_tot"));
		    	mapeo.setTotalOK(rsOpcion.getInt("cv_ok"));
		    	mapeo.setTotalNOK(rsOpcion.getInt("cv_nok"));
		    	
		    	mapeo.setEjercicio(new Integer(r_ejercicio));
		    	mapeo.setPorcDefectivo(rsOpcion.getDouble("cv_def"));
		    	
				vector.add(mapeo);				
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoGridCamaraVisionArtificial> datosOpcionesEjercicioArticulo(String r_ejercicio, String r_articulo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoGridCamaraVisionArtificial> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT ");
		cadenaSQL.append(" fecha cv_fec, ");
		cadenaSQL.append(" qc_cv_generalCounters.articulo cv_art, ");
		cadenaSQL.append(" e_articu.descrip_articulo cv_des, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO)/sum(Total)*100 cv_def ");
		cadenaSQL.append(" FROM qc_cv_generalCounters ");
		cadenaSQL.append(" inner join e_articu on e_articu.articulo = qc_cv_generalCounters.articulo ");
		
		if (r_ejercicio!=null && r_ejercicio.length()>0)
		{
			cadenaSQL.append(" WHERE fecha like '" + r_ejercicio + "%'");
		}
		if (r_articulo!=null && r_articulo.length()>0)
		{
			cadenaSQL.append(" and qc_cv_generalCounters.articulo = '" + r_articulo + "'");
		}
		cadenaSQL.append(" group by 1,2,3 ");
		cadenaSQL.append(" order by 1,2,3 ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoGridCamaraVisionArtificial>();
			
			while(rsOpcion.next())
			{
				MapeoGridCamaraVisionArtificial mapeo = new MapeoGridCamaraVisionArtificial();
				/*
				 * recojo mapeo calendario
				 */
//				mapeo.setIdCodigo(rsOpcion.getInt("cv_id"));
				mapeo.setFecha(rsOpcion.getString("cv_fec"));
				mapeo.setArticulo(rsOpcion.getString("cv_art"));
				mapeo.setDescripcion(rsOpcion.getString("cv_des"));
				
				mapeo.setTotalVistas(rsOpcion.getInt("cv_tot"));
				mapeo.setTotalOK(rsOpcion.getInt("cv_ok"));
				mapeo.setTotalNOK(rsOpcion.getInt("cv_nok"));
				
				mapeo.setEjercicio(new Integer(rsOpcion.getString("cv_fec").substring(0, 4)));
				mapeo.setPorcDefectivo(rsOpcion.getDouble("cv_def"));
				
				vector.add(mapeo);				
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	

	public HashMap<String, Double> recuperarDefectivoEjercicio(Integer r_ejercicio)
	{
		HashMap<String, Double> hashDefectivo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT substring(fecha,5,2) cv_mes, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO)/sum(Total) * 100 cv_def ");
     	cadenaSQL.append(" FROM qc_cv_generalCounters ");
     	
     	if (r_ejercicio!=null && r_ejercicio!=0)
     	{
     		cadenaSQL.append(" WHERE substring(fecha,1,4) = '" + r_ejercicio.toString() + "'");
     	}
     	cadenaSQL.append(" group by 1 ");
     	cadenaSQL.append(" order by 1 ");

     	try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			hashDefectivo = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hashDefectivo.put(String.valueOf(rsOpcion.getInt("cv_mes")), rsOpcion.getDouble("cv_def"));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashDefectivo;
	}

	public HashMap<String, Double> recuperarObjetivoDefectivoEjercicio(Integer r_ejercicio)
	{
		HashMap<String, Double> hashDefectivo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT substring(fecha,5,2) cv_mes, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO/Total) cv_def ");
		cadenaSQL.append(" FROM qc_cv_generalCounters ");
		
		if (r_ejercicio!=null && r_ejercicio!=0)
		{
			cadenaSQL.append(" WHERE substring(fecha,1,4) = '" + r_ejercicio.toString() + "'");
		}
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" order by 1 ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			hashDefectivo = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo calendario
				 */
				hashDefectivo.put(String.valueOf(rsOpcion.getInt("cv_mes")), new Double(3.5));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashDefectivo;
	}
	public HashMap<String, Double> recuperarDefectivoArticuloEjercicio(String r_ejercicio, String r_articulo)
	{
		HashMap<String, Double> hashDefectivo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT substring(fecha,5,2) cv_mes, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO)/sum(Total) * 100 cv_def ");
		cadenaSQL.append(" FROM qc_cv_generalCounters ");
		
		if (r_ejercicio!=null && r_ejercicio.length()>=0)
		{
			cadenaSQL.append(" WHERE substring(fecha,1,4) = '" + r_ejercicio.toString() + "'");
			if (r_articulo!=null && r_articulo.length()>=0)
			{
				cadenaSQL.append(" AND articulo = '" + r_articulo.toString() + "'");
			}
		}
		else if (r_articulo!=null && r_articulo.length()>=0)
		{
			cadenaSQL.append(" WHERE articulo = '" + r_articulo.toString() + "'");
		}
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" order by 1 ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			hashDefectivo = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hashDefectivo.put(String.valueOf(rsOpcion.getInt("cv_mes")), rsOpcion.getDouble("cv_def"));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashDefectivo;
	}
	
	public HashMap<String, Double> recuperarObjetivoDefectivoArticuloEjercicio(String r_ejercicio, String r_articulo)
	{
		HashMap<String, Double> hashDefectivo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT substring(fecha,5,2) cv_mes, ");
		cadenaSQL.append(" sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO/Total) cv_def ");
		cadenaSQL.append(" FROM qc_cv_generalCounters ");
		
		if (r_ejercicio!=null && r_ejercicio.length()>=0)
		{
			cadenaSQL.append(" WHERE substring(fecha,1,4) = '" + r_ejercicio.toString() + "'");
			if (r_articulo!=null && r_articulo.length()>=0)
			{
				cadenaSQL.append(" AND articulo = '" + r_articulo.toString() + "'");
			}
		}
		else if (r_articulo!=null && r_articulo.length()>=0)
		{
			cadenaSQL.append(" WHERE articulo = '" + r_articulo.toString() + "'");
		}
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" order by 1 ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			hashDefectivo = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo calendario
				 */
				hashDefectivo.put(String.valueOf(rsOpcion.getInt("cv_mes")), new Double(3.5));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashDefectivo;
	}
	
	public String guardarNuevo(MapeoCalendario r_mapeo)
	{
		return null;
	}

	public String guardarCambios(MapeoCalendario r_mapeo)
	{
		return null;
	}
	
	public String eliminar(Integer r_id)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM qc_cv_generalCounters");            
			cadenaSQL.append(" WHERE qc_cv_generalCounters.id = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_id);
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return "Error al eliminar";
		}
		return null;
	}

	public boolean comprobarFecha(String r_fecha)
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaWhere = null;
		String guia = null;
		boolean entradasErroneas = false;
//			boolean salidasErroneas = false;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT fecha cv_fec ");
		cadenaSQL.append(" from qc_cv_generalCounters ");
		cadenaSQL.append(" where fecha = '" + r_fecha + "' ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				cadenaSQL = new StringBuffer();
				
				cadenaSQL.append(" delete from qc_cv_generalCounters ");
				cadenaSQL.append(" where fecha = '" + r_fecha + "' ");
				PreparedStatement preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
				preparedStatement.executeUpdate();
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				return false;
			}
		}
		return true;
	}		
	
	public String comprobarUltimaSincronizacionCV()
	{
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(fecha) cv_fec ");
		cadenaSQL.append(" from qc_cv_generalCounters ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getString("cv_fec");
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}
		return null;
	}
	
	public HashMap<String, Double> devolverTotalesCV()
	{
		Connection con = null;	
		Statement cs = null;
		HashMap<String, Double> hashTotales = null;
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(Total) cv_tot, ");
		cadenaSQL.append(" sum(TotalOK) cv_ok, ");
		cadenaSQL.append(" sum(TotalKO) cv_nok, ");
		cadenaSQL.append(" sum(TotalKO)/sum(Total)*100 cv_def ");
		cadenaSQL.append(" from qc_cv_generalCounters ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			hashTotales = new HashMap<String, Double>();
			while(rsOpcion.next())
			{
				hashTotales.put("totales", rsOpcion.getDouble("cv_tot"));
				hashTotales.put("ok", rsOpcion.getDouble("cv_ok"));
				hashTotales.put("nok", rsOpcion.getDouble("cv_nok"));
				hashTotales.put("def", rsOpcion.getDouble("cv_def"));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}
		return hashTotales;
	}
}