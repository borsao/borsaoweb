package borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view.CamaraVisionArtificialView;
import borsao.e_borsao.Modulos.PRODUCCION.camaraVisionArtificial.view.pantallaDetalleArticulo;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class CamaraVisionArtificialGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	private boolean editable = false;
	private boolean conFiltro = true;
	
	private CamaraVisionArtificialView app = null;
	
    public CamaraVisionArtificialGrid(CamaraVisionArtificialView r_app, ArrayList<MapeoGridCamaraVisionArtificial> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("CONTROL CAMARA");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoGridCamaraVisionArtificial.class);
		this.setRecords(this.vector);
		this.setConTotales(this.conTotales);
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setStyleName("smallgrid");

		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "fecha", "articulo", "descripcion", "totalVistas", "totalOK", "totalNOK", "porcDefectivo", "bar");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("ejercicio", "80");
    	this.widthFiltros.put("fecha", "80");
    	this.widthFiltros.put("articulo", "80");
    	this.widthFiltros.put("descripcion", "210");

    	this.getColumn("bar").setHeaderCaption("");
    	this.getColumn("bar").setSortable(false);
    	this.getColumn("bar").setWidth(new Double(40));

    	this.getColumn("ejercicio").setHeaderCaption("EJERCICIO");
    	this.getColumn("ejercicio").setWidthUndefined();
    	this.getColumn("ejercicio").setWidth(120);
    	this.getColumn("fecha").setHeaderCaption("MES");
    	this.getColumn("fecha").setWidthUndefined();
    	this.getColumn("fecha").setWidth(120);
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setWidthUndefined();
    	this.getColumn("articulo").setWidth(120);
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidthUndefined();
    	this.getColumn("descripcion").setWidth(250);
    	this.getColumn("totalVistas").setHeaderCaption("Total");
    	this.getColumn("totalVistas").setWidthUndefined();
    	this.getColumn("totalVistas").setWidth(120);
    	this.getColumn("totalOK").setHeaderCaption("OK");
    	this.getColumn("totalOK").setWidthUndefined();
    	this.getColumn("totalOK").setWidth(120);
    	this.getColumn("totalNOK").setHeaderCaption("NOK");
    	this.getColumn("totalNOK").setWidthUndefined();
    	this.getColumn("totalNOK").setWidth(120);
    	this.getColumn("porcDefectivo").setHeaderCaption("% Defectivo");
    	this.getColumn("porcDefectivo").setWidthUndefined();
    	this.getColumn("porcDefectivo").setWidth(120);
    	
    	this.getColumn("totalVistas").setRenderer((Renderer) new NumberRenderer());
    	this.getColumn("totalOK").setRenderer((Renderer) new NumberRenderer());
    	this.getColumn("totalNOK").setRenderer((Renderer) new NumberRenderer());
    	DecimalFormat formato = new DecimalFormat();
		formato.setMinimumFractionDigits(2);
		formato.setMaximumFractionDigits(2);
    	this.getColumn("porcDefectivo").setRenderer((Renderer) new NumberRenderer(formato));
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("fecha").setHidden(true);
    	this.getColumn("mes").setHidden(true);
    }
    
    private void asignarTooltips()
    {
//    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoRetrabajos) {
                MapeoRetrabajos progRow = (MapeoRetrabajos)bean;
                // The actual description text is depending on the application
                if ("bar".equals(cell.getPropertyId()))
                {
                	descriptionText = "Detalle articulo";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "totalVistas".equals(cellReference.getPropertyId()) || "totalOK".equals(cellReference.getPropertyId()) || "totalNOK".equals(cellReference.getPropertyId()) || "porcDefectivo".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else if ( "bar".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoGridCamaraVisionArtificial mapeo = (MapeoGridCamaraVisionArtificial) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("bar"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		pantallaDetalleArticulo vt = null;
            		vt = new pantallaDetalleArticulo("Titulo", mapeo.getEjercicio().toString(), mapeo.getArticulo(), mapeo.getDescripcion());
//        			pantallaLineasVacaciones vt = null;
//    	    		vt= new pantallaLineasVacaciones(app, mapeo);
    	    		getUI().addWindow(vt);
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("totalVistas");
		this.camposNoFiltrar.add("totalOK");
		this.camposNoFiltrar.add("bar");
		this.camposNoFiltrar.add("totalNOK");
		this.camposNoFiltrar.add("porcDefectivo");
	}

	public void generacionPdf(MapeoRetrabajos r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    }
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() 
	{
    	Double totales=null;
    	Double totalOK=null;
    	Double totalNOK=null;
    	Double valorTotales = new Double(0);
    	Double valorOK = new Double(0);
    	Double valorNOK = new Double(0);

    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	if (item.getItemProperty("totalVistas").getValue()!=null)
        	{
        		totales = new Double(item.getItemProperty("totalVistas").getValue().toString());
        		valorTotales=valorTotales+totales;
        	}
        	if (item.getItemProperty("totalOK").getValue()!=null)
        	{
        		totalOK = new Double(item.getItemProperty("totalOK").getValue().toString());
        		valorOK=valorOK+totalOK;
        	}
        	if (item.getItemProperty("totalNOK").getValue()!=null)
        	{
        		totalNOK = new Double(item.getItemProperty("totalNOK").getValue().toString());
        		valorNOK=valorNOK+totalNOK;
        	}
        }
        
        footer.getCell("fecha").setText("Totales ");
		footer.getCell("totalVistas").setText(RutinasNumericas.formatearDoubleDecimales(valorTotales,0).toString());
		footer.getCell("totalOK").setText(RutinasNumericas.formatearDoubleDecimales(valorOK,0).toString());
		footer.getCell("totalNOK").setText(RutinasNumericas.formatearDoubleDecimales(valorNOK,0).toString());
		footer.getCell("porcDefectivo").setText(RutinasNumericas.formatearDoubleDecimales(new Double((valorNOK/valorTotales *100)),2).toString());		
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.hor.getHeight()-this.app.topLayout.getHeight()-10)+"%");
		footer.getCell("totalVistas").setStyleName("Rcell-pie");
		footer.getCell("totalOK").setStyleName("Rcell-pie");
		footer.getCell("totalNOK").setStyleName("Rcell-pie");
		footer.getCell("porcDefectivo").setStyleName("Rcell-pie");
	}
}


