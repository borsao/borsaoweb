package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEstadoTareasProtocolo extends MapeoGlobal
{
	private Integer porcentaje;

	private String descripcion;
	
	
	public MapeoEstadoTareasProtocolo()
	{ 
	}


	public Integer getPorcentaje() {
		
		return porcentaje;
	}


	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}