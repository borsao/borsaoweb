package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.EstadoTareasProtocoloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.MapeoEstadoTareasProtocolo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.server.consultaEstadoTareasProtocoloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class EstadosTareasProtocoloView extends GridViewRefresh {

	public static final String VIEW_NAME = "Estados Tareas Protocolo";
	public consultaEstadoTareasProtocoloServer cus =null;
	public boolean modificando = false;
	
	private final String titulo = "Tareas Protocolo";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
//	private final boolean soloConsulta = false;
	private OpcionesForm form = null;
	public boolean conTotales = true;
	public EstadosTareasProtocoloView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public EstadosTareasProtocoloView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaEstadoTareasProtocoloServer.getInstance(CurrentUser.get());
		this.intervaloRefresco=0;
		this.setSoloConsulta(false);
		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
		this.cargarListeners();
//		this.establecerModo();
		
		this.generarGrid(opcionesEscogidas);
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoEstadoTareasProtocolo> r_vector=null;
    	MapeoEstadoTareasProtocolo mapeoEstadoTareasProtocolo =null;
    	hashToMapeo hm = new hashToMapeo();
    	mapeoEstadoTareasProtocolo=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);

    	r_vector=this.cus.datosEstadoTareasProtocoloGlobal(mapeoEstadoTareasProtocolo);
    	this.presentarGrid(r_vector);
    	((EstadoTareasProtocoloGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((EstadoTareasProtocoloGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((EstadoTareasProtocoloGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
//    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((EstadoTareasProtocoloGrid) this.grid).activadaVentanaPeticion && !((EstadoTareasProtocoloGrid) this.grid).ordenando)
    	{
    		if (((MapeoEstadoTareasProtocolo) r_fila)!=null)
    		{
	    		this.modificando=true;
	    		this.verForm(false);
		    	this.form.editarEstado((MapeoEstadoTareasProtocolo) r_fila);
    		}	    	
    	}    		
    }
    
    public void generarGrid(MapeoEstadoTareasProtocolo r_mapeo)
    {
    	ArrayList<MapeoEstadoTareasProtocolo> r_vector=null;
    	r_vector=this.cus.datosEstadoTareasProtocoloGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    	((EstadoTareasProtocoloGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoEstadoTareasProtocolo> r_vector)
    {
    	grid = new EstadoTareasProtocoloGrid(this,r_vector);
    	if (((EstadoTareasProtocoloGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((EstadoTareasProtocoloGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
//    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
//	private void establecerModo()
//	{
//		/*
//		 * Establece:  0 - Sin permisos
//		 * 			  10 - Laboratorio
//		 * 			  30 - Solo Consulta
//		 * 			  40 - Completar
//		 * 			  50 - Cambio Observaciones
//		 * 			  70 - Cambio observaciones y materia seca
//		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
//		 *  			 
//		 * 		      99 - Acceso total
//		 */
//		this.opcNuevo.setVisible(false);
//		this.opcNuevo.setEnabled(false);
////		this.opcImprimir.setVisible(true);
////		this.opcImprimir.setEnabled(true);
//
//		this.verBotones(true);
//		this.activarBotones(true);
//		this.navegacion(true);
//				
//		this.opcNuevo.setVisible(true);
//		this.opcNuevo.setEnabled(true);
//		setSoloConsulta(this.soloConsulta);
//	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
	}

	private void verBotones(Boolean r_visibles)
	{
	}
	
	public void imprimirTareasProtocolo()
	{
//		((TareasProtocoloGrid) grid).activadaVentanaPeticion=false;
//		((TareasProtocoloGrid) grid).ordenando = false;
//		
//		
//		MapeoTareasProtocolo mapeo=(MapeoTareasProtocolo) grid.getSelectedRow();
//		
//		if (form!=null) form.cerrar();
//		
//		if (mapeo!=null && mapeo.getIdRetrabajo()!=null)
//		{
//			PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion(mapeo.getIdqc_incidencias());
//			getUI().addWindow(vtPeticion);
//		}
//		else
//		{
//			Notificaciones.getInstance().mensajeInformativo("Debes seleccionar el registro a imprimir");
//			this.opcImprimir.setEnabled(true);
//		}
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(EstadosTareasProtocoloView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(EstadosTareasProtocoloView.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((EstadoTareasProtocoloGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

