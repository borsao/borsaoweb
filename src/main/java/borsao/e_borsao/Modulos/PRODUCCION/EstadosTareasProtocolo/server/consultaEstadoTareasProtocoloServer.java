package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasProtocolo.modelo.MapeoEstadoTareasProtocolo;

public class consultaEstadoTareasProtocoloServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaEstadoTareasProtocoloServer instance;
	
	public consultaEstadoTareasProtocoloServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaEstadoTareasProtocoloServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaEstadoTareasProtocoloServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoEstadoTareasProtocolo> datosEstadoTareasProtocoloGlobal(MapeoEstadoTareasProtocolo r_mapeo)
	{
		ResultSet rsOpcion = null;
		ArrayList<MapeoEstadoTareasProtocolo> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoEstadoTareasProtocolo mapeoEstadoTareasProtocolo=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_estados_protocolo.idCodigo tar_id,");
		cadenaSQL.append(" prd_estados_protocolo.descripcion tar_des,");
		cadenaSQL.append(" prd_estados_protocolo.porc tar_por ");
		cadenaSQL.append(" from prd_estados_protocolo ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdCodigo()!=null && r_mapeo.getIdCodigo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idCodigo = '" + r_mapeo.getIdCodigo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getPorcentaje()!=null && r_mapeo.getPorcentaje()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" porc = " + r_mapeo.getPorcentaje() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by porc asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoEstadoTareasProtocolo>();
			
			while(rsOpcion.next())
			{
				mapeoEstadoTareasProtocolo = new MapeoEstadoTareasProtocolo();
				/*
				 * recojo mapeo operarios
				 */
				mapeoEstadoTareasProtocolo.setIdCodigo(rsOpcion.getInt("tar_id"));
				mapeoEstadoTareasProtocolo.setPorcentaje(rsOpcion.getInt("tar_por"));
				mapeoEstadoTareasProtocolo.setDescripcion(rsOpcion.getString("tar_des"));
				vector.add(mapeoEstadoTareasProtocolo);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public boolean comprobarEstadoTareasProtocolo(String r_des)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT prd_estados_protocolo.porc tar_ord ");
		cadenaSQL.append(" from prd_estados_protocolo ");
		cadenaSQL.append(" where descripcion = '" + r_des + "' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public Integer obenerIdEstadoTareasProtocolo(String r_des)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
//		Connection conG = null;
//		Statement csG = null;
		
		cadenaSQL.append(" SELECT prd_estados_protocolo.idCodigo tar_id ");
		cadenaSQL.append(" from prd_estados_protocolo ");
		cadenaSQL.append(" where descripcion = '" + r_des + "' ");
		
		try
		{
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_id");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	
	public String imprimirTarea(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_estados_protocolo.idCodigo) tar_sig ");
     	cadenaSQL.append(" FROM prd_estados_protocolo ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tar_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public String guardarNuevo(MapeoEstadoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_estados_protocolo( ");
			cadenaSQL.append(" prd_estados_protocolo.idCodigo,");			
			cadenaSQL.append(" prd_estados_protocolo.descripcion, ");			
			cadenaSQL.append(" prd_estados_protocolo.porc ) VALUES (");
			cadenaSQL.append(" ?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getPorcentaje()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getPorcentaje());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		return null;
	}
	
	public String guardarCambios(MapeoEstadoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
		    cadenaSQL.append(" UPDATE prd_estados_protocolo set ");
		    cadenaSQL.append(" prd_estados_protocolo.descripcion =?,");
		    cadenaSQL.append(" prd_estados_protocolo.porc =? ");
			cadenaSQL.append(" where prd_estados_protocolo.idCodigo = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getPorcentaje()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getPorcentaje());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    preparedStatement.setInt(3, r_mapeo.getIdCodigo());
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		return null;
	}
	

	public void eliminar(MapeoEstadoTareasProtocolo r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_estados_protocolo ");            
			cadenaSQL.append(" WHERE prd_estados_protocolo.idCodigo = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
	

	@Override
	public String semaforos() 
	{
		return "0";
	}

	public String generarInforme(MapeoEstadoTareasProtocolo r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		libImpresion.setCodigo(r_mapeo.getIdRetrabajo());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/EstadoTareasProtocolo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("EstadoTareasProtocolo.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("EstadoTareasProtocolo");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}