package borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.modelo.MapeoHorasTrabajadasEmbotelladora;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaHorasTrabajadasEmbotelladoraServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaHorasTrabajadasEmbotelladoraServer instance;
	
	public consultaHorasTrabajadasEmbotelladoraServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaHorasTrabajadasEmbotelladoraServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaHorasTrabajadasEmbotelladoraServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoHorasTrabajadasEmbotelladora> datosOpcionesGlobal(MapeoHorasTrabajadasEmbotelladora r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoHorasTrabajadasEmbotelladora> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_horas_emb.idCodigo hor_id, ");
		cadenaSQL.append(" prd_horas_emb.ejercicio hor_eje, ");
		cadenaSQL.append(" prd_horas_emb.semana hor_sem, ");
		cadenaSQL.append(" prd_horas_emb.horas hor_hor, ");
		cadenaSQL.append(" prd_horas_emb.horasEtt hor_hor_ett, ");
		cadenaSQL.append(" prd_horas_emb.programados hor_prg, ");
        cadenaSQL.append(" prd_horas_emb.incidencias hor_inc, ");
        cadenaSQL.append(" prd_horas_emb.fueraLinea hor_flin, ");
        cadenaSQL.append(" prd_horas_emb.horasReales hor_horr, ");
        cadenaSQL.append(" prd_horas_emb.programadosReales hor_prgr, ");
        cadenaSQL.append(" prd_horas_emb.incidenciasReales hor_incr, ");
        cadenaSQL.append(" prd_horas_emb.fueraLineaReales hor_flinr, ");
        cadenaSQL.append(" prd_horas_emb.horasAutomatico hor_aut, ");
        cadenaSQL.append(" prd_horas_emb.horasJaulon hor_jau, ");
        cadenaSQL.append(" prd_horas_emb.horasEtiquetado hor_eti, ");
        cadenaSQL.append(" prd_horas_emb.horasHorizontal hor_hz, ");
        cadenaSQL.append(" prd_horas_emb.mermas hor_mer, ");
        cadenaSQL.append(" prd_horas_emb.produccion hor_prod ");
     	cadenaSQL.append(" FROM prd_horas_emb ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + r_mapeo.getSemana());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoHorasTrabajadasEmbotelladora>();
			
			while(rsOpcion.next())
			{
				MapeoHorasTrabajadasEmbotelladora mapeo = new MapeoHorasTrabajadasEmbotelladora();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("hor_id"));
				mapeo.setEjercicio(rsOpcion.getInt("hor_eje"));
				mapeo.setSemana(rsOpcion.getInt("hor_sem"));
				mapeo.setHoras(rsOpcion.getDouble("hor_hor"));
				mapeo.setHorasEtt(rsOpcion.getDouble("hor_hor_ett"));
				mapeo.setFueraLinea(rsOpcion.getDouble("hor_flin"));
				mapeo.setProgramadas(rsOpcion.getDouble("hor_prg"));
				mapeo.setIncidencias(rsOpcion.getDouble("hor_inc"));
				mapeo.setHorasReales(rsOpcion.getDouble("hor_horr"));
				mapeo.setFueraLineaReales(rsOpcion.getDouble("hor_flinr"));
				mapeo.setProgramadasReales(rsOpcion.getDouble("hor_prgr"));
				mapeo.setIncidenciasReales(rsOpcion.getDouble("hor_incr"));
				mapeo.setHorasAutomatico(rsOpcion.getDouble("hor_aut"));
				mapeo.setHorasJaulon(rsOpcion.getDouble("hor_jau"));
				mapeo.setHorasEtiquetado(rsOpcion.getDouble("hor_eti"));
				mapeo.setHorasHorizontal(rsOpcion.getDouble("hor_hz"));
				mapeo.setMermas(rsOpcion.getInt("hor_mer"));
				mapeo.setProduccion(rsOpcion.getInt("hor_prod"));

				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	

	public HashMap<String, Double> recuperarDisponibilidad(Integer r_ejercicio)
	{
		HashMap<String, Double> disp_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select semana, (sum(horasReales - incidenciasReales - programadosReales)/ sum(horasReales - programadosReales))*100  as disp from prd_horas_emb where ejercicio = " + r_ejercicio + " group by 1 ";
			
			disp_obtenido = new HashMap<String, Double>();
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			while(rsHoras.next())
			{
//				if (rsHoras.getDouble("disp")!=0) 
					disp_obtenido.put(rsHoras.getString("semana"), rsHoras.getDouble("disp"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return disp_obtenido;
	}

	public HashMap<String, Double> recuperarMediaDisponibilidad(Integer r_ejercicio)
	{
		HashMap<String, Double> disp_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		Double media = new Double(0);
		Double disp = new Double(0);
		
		Integer cuantos = 0;
		
		try
		{
			sql="select semana, (sum(horasReales - incidenciasReales - programadosReales)/ sum(horasReales - programadosReales))*100  as disp from prd_horas_emb where ejercicio = " + r_ejercicio + " group by 1 ";
			
			disp_obtenido = new HashMap<String, Double>();
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			while(rsHoras.next())
			{
				if (rsHoras.getDouble("disp")!=0)
				{
					cuantos++;
					disp = disp + rsHoras.getDouble("disp");
					media = disp/cuantos;
					
				}
			}
			rsHoras.beforeFirst();
			while(rsHoras.next())
			{
				disp_obtenido.put(rsHoras.getString("semana"), media);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return disp_obtenido;
	}
	
	public Double recuperarHoras(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horas) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarHorasEtt(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horasEtt) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}
	
	public Double recuperarHorasReales(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horasReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}
	
	public HashMap<String, Double> recuperarHorasRealesAño(Integer r_ejercicio)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		HashMap<String, Double> hashHoras = null;
		try
		{
			sql="select semana, sum(horasReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			hashHoras = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				hashHoras.put(rsHoras.getString("semana"), rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashHoras;
	}
	public Double recuperarHorasEmbotelladoReales(String r_horas, Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(" + r_horas +") as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public HashMap<String, Double> recuperarHorasEmbotelladoAño(String r_horas, Integer r_ejercicio)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		HashMap<String, Double> hashHoras = null;
		try
		{
			sql="select semana, sum(" + r_horas +") as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			hashHoras = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				hashHoras.put(rsHoras.getString("semana"), rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashHoras;
	}
	
	public HashMap<String, Double> recuperarProgramadasRealesAño(Integer r_ejercicio)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		HashMap<String, Double> hashHoras = null;
		try
		{
			sql="select semana, sum(programadosReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			hashHoras = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				hashHoras.put(rsHoras.getString("semana"), rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashHoras;
	}
	
	public HashMap<String, Double> recuperarIncidenciasRealesAño(Integer r_ejercicio)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		HashMap<String, Double> hashHoras = null;
		try
		{
			sql="select semana, sum(incidenciasReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			hashHoras = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				hashHoras.put(rsHoras.getString("semana"), rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashHoras;
	}
	
	public Double recuperarIncidencias(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(incidencias) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarIncidenciasReales(Integer r_ejercicio, Integer r_semana,String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			sql = new StringBuffer();
			
			sql.append("select sum(tiempo) as horas from prd_paradas_parte ");
			sql.append("inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo");
			sql.append(" and ejercicio = " + r_ejercicio); 
			sql.append(" and tipo in ('C','N') " ); 
			sql.append(" and linea = 'EMBOTELLADO' " ); 
			
			if (acumulado.contentEquals("Acumulado")) sql.append(" and semana <= " + r_semana);
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql.append(" and semana between " + desde + " and " + hasta);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql.append(" and semana = " + r_semana );
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql.toString());
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarFueraLinea(Integer r_ejercicio, Integer r_semana)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(fueraLinea) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarFueraLineaReales(Integer r_ejercicio, Integer r_semana)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(fueraLineaReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}
	
	public Double recuperarProgramadas(Integer r_ejercicio, Integer r_semana,String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(programados) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarProgramadasReales(Integer r_ejercicio, Integer r_semana,String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(programadosReales) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarMermas(Integer r_ejercicio, Integer r_semana, String r_acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(mermas) as horas from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (r_acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (r_acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public HashMap<String, Double> recuperarCalidad(Integer r_ejercicio)
	{
		HashMap<String, Double> hashCalidad= null;
		HashMap<String, Double> hashProduccion = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			consultaProduccionDiariaServer cps = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
			
			hashProduccion = cps.obtenerHashProduccionesSemanales(r_ejercicio.toString());
			
			sql="select semana, sum(mermas) as mermas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana "; 
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			hashCalidad = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				if (hashProduccion.get(rsHoras.getString("semana"))!=null)
					hashCalidad.put(rsHoras.getString("semana"),100*(hashProduccion.get(rsHoras.getString("semana")) - rsHoras.getDouble("mermas"))/hashProduccion.get(rsHoras.getString("semana")));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashCalidad;
	}

	public HashMap<String, Double> recuperarMediaCalidad(Integer r_ejercicio)
	{
		HashMap<String, Double> disp_obtenido = null;
		HashMap<String, Double> hashProduccion = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		Double media = new Double(0);
		Double disp = new Double(0);
		
		Integer cuantos = 0;
		
		try
		{
			sql="select semana, sum(mermas) as mermas from prd_horas_emb where ejercicio = " + r_ejercicio + " group by semana ";
			
			consultaProduccionDiariaServer cps = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
			
			hashProduccion = cps.obtenerHashProduccionesSemanales(r_ejercicio.toString());
			disp_obtenido = new HashMap<String, Double>();
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			while(rsHoras.next())
			{
				if (rsHoras.getDouble("mermas")!=0 && rsHoras.getString("semana")!=null)
				{
					cuantos++;
					disp = disp + (100*(hashProduccion.get(rsHoras.getString("semana")) - rsHoras.getDouble("mermas"))/hashProduccion.get(rsHoras.getString("semana")));
					media = disp/cuantos;
				}
			}
			rsHoras.beforeFirst();
			while(rsHoras.next())
			{
				disp_obtenido.put(rsHoras.getString("semana"), media);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return disp_obtenido;
	}

	public Double recuperarProduccion(Integer r_ejercicio, Integer r_semana)
	{
		Double prod_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(produccion5L) as prod from prd_horas_emb where ejercicio = " + r_ejercicio; 
			if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;

			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				prod_obtenido = rsHoras.getDouble("prod");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return prod_obtenido;
	}

	public boolean comprobarHoras(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select horas as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean comprobarIncidencias(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select incidencias as horas from prd_horas_emb where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
				
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return false;
	}


	public String guardarNuevo(MapeoHorasTrabajadasEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_horas_emb ( ");
			cadenaSQL.append(" prd_horas_emb.idCodigo, ");
			cadenaSQL.append(" prd_horas_emb.ejercicio, ");
    		cadenaSQL.append(" prd_horas_emb.semana, ");
    		cadenaSQL.append(" prd_horas_emb.horas, ");
    		cadenaSQL.append(" prd_horas_emb.programados, ");
    		cadenaSQL.append(" prd_horas_emb.mermas, ");
    		cadenaSQL.append(" prd_horas_emb.incidencias, ");
    		cadenaSQL.append(" prd_horas_emb.fueraLinea, ");
    		cadenaSQL.append(" prd_horas_emb.horasReales, ");
    		cadenaSQL.append(" prd_horas_emb.programadosReales, ");
    		cadenaSQL.append(" prd_horas_emb.incidenciasReales, ");
    		cadenaSQL.append(" prd_horas_emb.fueraLineaReales, ");
    		cadenaSQL.append(" prd_horas_emb.horasAutomatico, ");
    		cadenaSQL.append(" prd_horas_emb.horasEtiquetado, ");
    		cadenaSQL.append(" prd_horas_emb.horasJaulon, ");
    		cadenaSQL.append(" prd_horas_emb.horasHorizontal, ");
    		cadenaSQL.append(" prd_horas_emb.produccion, ");
    		cadenaSQL.append(" prd_horas_emb.horasEtt ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_horas_emb", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getFueraLinea()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getFueraLinea());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }

		    if (r_mapeo.getHorasReales()!=null)
		    {
		    	preparedStatement.setDouble(9, r_mapeo.getHorasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(9, 0);
		    }
		    if (r_mapeo.getProgramadasReales()!=null)
		    {
		    	preparedStatement.setDouble(10, r_mapeo.getProgramadasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(10, 0);
		    }
		    if (r_mapeo.getIncidenciasReales()!=null)
		    {
		    	preparedStatement.setDouble(11, r_mapeo.getIncidenciasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(11, 0);
		    }
		    if (r_mapeo.getFueraLineaReales()!=null)
		    {
		    	preparedStatement.setDouble(12, r_mapeo.getFueraLineaReales());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getHorasAutomatico()!=null)
		    {
		    	preparedStatement.setDouble(13, r_mapeo.getHorasAutomatico());
		    }
		    else
		    {
		    	preparedStatement.setDouble(13, 0);
		    }
		    if (r_mapeo.getHorasJaulon()!=null)
		    {
		    	preparedStatement.setDouble(14, r_mapeo.getHorasJaulon());
		    }
		    else
		    {
		    	preparedStatement.setDouble(14, 0);
		    }
		    if (r_mapeo.getHorasEtiquetado()!=null)
		    {
		    	preparedStatement.setDouble(15, r_mapeo.getHorasEtiquetado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(15, 0);
		    }
		    if (r_mapeo.getHorasHorizontal()!=null)
		    {
		    	preparedStatement.setDouble(16, r_mapeo.getHorasHorizontal());
		    }
		    else
		    {
		    	preparedStatement.setDouble(16, 0);
		    }
		    if (r_mapeo.getProduccion()!=null)
		    {
		    	preparedStatement.setInt(17, r_mapeo.getProduccion());
		    }
		    else
		    {
		    	preparedStatement.setInt(17, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(18, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setDouble(18, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoHorasTrabajadasEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_horas_emb SET ");			
			cadenaSQL.append(" prd_horas_emb.ejercicio=?, ");
    		cadenaSQL.append(" prd_horas_emb.semana=?, ");
    		cadenaSQL.append(" prd_horas_emb.horas=?, ");
    		cadenaSQL.append(" prd_horas_emb.programados=?, ");
    		cadenaSQL.append(" prd_horas_emb.mermas=?, ");
    		cadenaSQL.append(" prd_horas_emb.incidencias=?, ");
    		cadenaSQL.append(" prd_horas_emb.fueraLinea=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasReales=?, ");
    		cadenaSQL.append(" prd_horas_emb.programadosReales=?, ");
    		cadenaSQL.append(" prd_horas_emb.incidenciasReales=?, ");
    		cadenaSQL.append(" prd_horas_emb.fueraLineaReales=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasAutomatico=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasJaulon=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasEtiquetado=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasHorizontal=?, ");
    		cadenaSQL.append(" prd_horas_emb.produccion=?, ");
    		cadenaSQL.append(" prd_horas_emb.horasEtt=? ");
	        cadenaSQL.append(" WHERE prd_horas_emb.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }

		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getFueraLinea()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getFueraLinea());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getHorasReales()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getHorasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getProgramadasReales()!=null)
		    {
		    	preparedStatement.setDouble(9, r_mapeo.getProgramadasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(9, 0);
		    }
		    if (r_mapeo.getIncidenciasReales()!=null)
		    {
		    	preparedStatement.setDouble(10, r_mapeo.getIncidenciasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(10, 0);
		    }
		    if (r_mapeo.getFueraLineaReales()!=null)
		    {
		    	preparedStatement.setDouble(11, r_mapeo.getFueraLineaReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(11, 0);
		    }
		    
		    if (r_mapeo.getHorasAutomatico()!=null)
		    {
		    	preparedStatement.setDouble(12, r_mapeo.getHorasAutomatico());
		    }
		    else
		    {
		    	preparedStatement.setDouble(12, 0);
		    }
		    if (r_mapeo.getHorasJaulon()!=null)
		    {
		    	preparedStatement.setDouble(13, r_mapeo.getHorasJaulon());
		    }
		    else
		    {
		    	preparedStatement.setDouble(13, 0);
		    }
		    if (r_mapeo.getHorasEtiquetado()!=null)
		    {
		    	preparedStatement.setDouble(14, r_mapeo.getHorasEtiquetado());
		    }
		    else
		    {
		    	preparedStatement.setDouble(14, 0);
		    }
		    if (r_mapeo.getHorasHorizontal()!=null)
		    {
		    	preparedStatement.setDouble(15, r_mapeo.getHorasHorizontal());
		    }
		    else
		    {
		    	preparedStatement.setDouble(15, 0);
		    }
		    
		    if (r_mapeo.getProduccion()!=null)
		    {
		    	preparedStatement.setInt(16, r_mapeo.getProduccion());
		    }
		    else
		    {
		    	preparedStatement.setInt(16, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(17, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setDouble(17, 0);
		    }
		    preparedStatement.setInt(18, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoHorasTrabajadasEmbotelladora r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_horas_emb ");            
			cadenaSQL.append(" WHERE prd_horas_emb.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}