package borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.view;

import java.util.ArrayList;
import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.modelo.MapeoHorasTrabajadasEmbotelladora;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.server.consultaHorasTrabajadasEmbotelladoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoHorasTrabajadasEmbotelladora mapeoHoras = null;
	 
	 private consultaHorasTrabajadasEmbotelladoraServer cus = null;	 
	 private HorasTrabajadasEmbotelladoraView uw = null;
	 private BeanFieldGroup<MapeoHorasTrabajadasEmbotelladora> fieldGroup;
	 
	 public OpcionesForm(HorasTrabajadasEmbotelladoraView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        cus=consultaHorasTrabajadasEmbotelladoraServer.getInstance(CurrentUser.get());
        fieldGroup = new BeanFieldGroup<MapeoHorasTrabajadasEmbotelladora>(MapeoHorasTrabajadasEmbotelladora.class);
        fieldGroup.bindMemberFields(this);
        
        this.cargarListeners();
        
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoHoras = (MapeoHorasTrabajadasEmbotelladora) uw.grid.getSelectedRow();
                eliminarHorasTrabajadas(mapeoHoras);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
        
        this.btnRecuperar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{				
	 			if (!isBusqueda())
	 			{
	 				recuperarDatosParte();	 				
	 			}
			}
		});
		
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 				uw.setHayGrid(false);
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones(null);
	 				mapeoHoras = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearHorasTrabajadas(mapeoHoras);
	 			}
	 			else
	 			{
	 				MapeoHorasTrabajadasEmbotelladora mapeoHoras_orig = (MapeoHorasTrabajadasEmbotelladora) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones("M");
	 				mapeoHoras = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarHorasTrabajadas(mapeoHoras,mapeoHoras_orig);
	 				hm=null;
	 			}
	 			if (((OpcionesGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;		
		editarHorasTrabajadas(null);
	}

	private HashMap<String , String> rellenarHashOpciones(String r_accion)
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.ejercicio.getValue()!=null && this.ejercicio.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("ejercicio", this.ejercicio.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("ejercicio", "");
		}
		
		if (this.horas.getValue()!=null && this.horas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horas", this.horas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horas", "");
		}
		if (this.horasEtt.getValue()!=null && this.horasEtt.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasEtt", this.horasEtt.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasEtt", "");
		}
		if (this.fueraLinea.getValue()!=null && this.fueraLinea.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fueraLinea", this.fueraLinea.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fueraLinea", "");
		}
		if (this.incidencias.getValue()!=null && this.incidencias.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("incidencias", this.incidencias.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("incidencias", "");
		}
		if (this.semana.getValue()!=null && this.semana.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("semana", this.semana.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("semana", "");
		}
		if (this.programadas.getValue()!=null && this.programadas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("programadas", this.programadas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("programadas", "");
		}
		if (this.horasReales.getValue()!=null && this.horasReales.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasReales", this.horasReales.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasReales", "");
		}
		if (this.fueraLineaReales.getValue()!=null && this.fueraLineaReales.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fueraLineaReales", this.fueraLineaReales.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fueraLineaReales", "");
		}
		if (this.incidenciasReales.getValue()!=null && this.incidenciasReales.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("incidenciasReales", this.incidenciasReales.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("incidenciasReales", "");
		}
		if (this.programadasReales.getValue()!=null && this.programadasReales.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("programadasReales", this.programadasReales.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("programadasReales", "");
		}
		if (this.mermas.getValue()!=null && this.mermas.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("mermas", this.mermas.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("mermas", "");
		}
		if (this.produccion.getValue()!=null && this.produccion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("produccion", this.produccion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("produccion", "");
		}
		if (this.horasAutomatico.getValue()!=null && this.horasAutomatico.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasAutomatico", this.horasAutomatico.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasAutomatico", "");
		}
		if (this.horasJaulon.getValue()!=null && this.horasJaulon.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasJaulon", this.horasJaulon.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasJaulon", "");
		}
		if (this.horasEtiquetado.getValue()!=null && this.horasEtiquetado.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasEtiquetado", this.horasEtiquetado.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasEtiquetado", "");
		}
		if (this.horasHorizontal.getValue()!=null && this.horasHorizontal.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("horasHorizontal", this.horasHorizontal.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("horasHorizontal", "");
		}
		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarHorasTrabajadas(null);
	}
	
	public void editarHorasTrabajadas(MapeoHorasTrabajadasEmbotelladora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null)
		{
			r_mapeo=new MapeoHorasTrabajadasEmbotelladora();
			r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
			
			if (isCreacion())
			{
				r_mapeo.setEjercicio(new Integer(RutinasFechas.añoActualYYYY()));
				r_mapeo.setSemana(new Integer(RutinasFechas.semanaActual(RutinasFechas.añoActualYYYY())));
			}
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoHorasTrabajadasEmbotelladora>(r_mapeo));
		ejercicio.focus();
	}
	
	public void eliminarHorasTrabajadas(MapeoHorasTrabajadasEmbotelladora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((OpcionesGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
		((ArrayList<MapeoHorasTrabajadasEmbotelladora>) ((OpcionesGrid) uw.grid).vector).remove(r_mapeo);
	}
	
	public void modificarHorasTrabajadas(MapeoHorasTrabajadasEmbotelladora r_mapeo, MapeoHorasTrabajadasEmbotelladora r_mapeo_orig)
	{
		if (todoEnOrden())
		{
			cus  = new consultaHorasTrabajadasEmbotelladoraServer(CurrentUser.get());
			r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
			cus.guardarCambios(r_mapeo);		
			((OpcionesGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
		}
	}
	public void crearHorasTrabajadas(MapeoHorasTrabajadasEmbotelladora r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		
		cus  = new consultaHorasTrabajadasEmbotelladoraServer(CurrentUser.get());
		
		if (todoEnOrden())
		{
			String rdo = cus.guardarNuevo(r_mapeo);
			if (rdo== null)
			{
				fieldGroup.setItemDataSource(new BeanItem<MapeoHorasTrabajadasEmbotelladora>(r_mapeo));
				if (((OpcionesGrid) uw.grid)!=null)
				{
					((OpcionesGrid) uw.grid).refresh(r_mapeo,null);
				}
				else
				{
					uw.generarGrid(uw.opcionesEscogidas);
				}
				this.setCreacion(false);
				uw.regresarDesdeForm();
			}
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoHorasTrabajadasEmbotelladora> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoHoras= item.getBean();
            if (this.mapeoHoras.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

    private boolean todoEnOrden()
    {
    	if (this.ejercicio.getValue()==null || this.ejercicio.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar el ejercicio");
    		return false;
    	}
    	if (this.semana.getValue()==null || this.semana.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar La semana");
    		return false;
    	}
    	if (this.horas.getValue()==null || this.horas.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Rellenar las horas");
    		return false;
    	}
    	return true;
    }

    private void recuperarDatosParte()
    {
    	if ((semana.getValue()!=null && semana.getValue().length()>0) && (ejercicio.getValue()!=null && ejercicio.getValue().length()>0))
    	{
//    		Notificaciones.getInstance().mensajeAdvertencia("Pendiente de implementar");
    		consultaParteProduccionServer parteProd = new consultaParteProduccionServer(CurrentUser.get());
    		consultaProduccionDiariaServer cps = consultaProduccionDiariaServer.getInstance(CurrentUser.get());
    		consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    		
    		Integer prodSemana = cps.produccionSemanal("EMBOTELLADORA", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())),  new Integer(this.semana.getValue()));
    		Integer mermasSemana = ces.mermasSemanal("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())),  new Integer(this.semana.getValue()));
    		Double horasLinea = parteProd.recuperarHorasParteSemana("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));
    		Double horasFueraLinea = parteProd.recuperarHorasFueraParteSemana("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));
    		Double incidenciasProgramadas  = parteProd.recuperarIncidenciasSemana("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()), "P");
    		Double incidenciasNoProgramadas  = parteProd.recuperarIncidenciasSemana("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()), "N");
    		Double incidenciasCalidad= parteProd.recuperarIncidenciasSemana("EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()), "C");

    		Double horasAutomatico= parteProd.recuperarHorasSemana("horasAutomatico", "EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));
    		Double horasJaulon= parteProd.recuperarHorasSemana("horasJaulon", "EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));
    		Double horasEtiquetado= parteProd.recuperarHorasSemana("horasEtiquetado", "EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));
    		Double horasHorizontal= parteProd.recuperarHorasSemana("horasManual","EMBOTELLADO", new Integer(RutinasCadenas.quitarPuntoMiles(this.ejercicio.getValue())), new Integer(this.semana.getValue()));

    		this.horasReales.setValue(RutinasCadenas.reemplazarPuntoMiles(String.valueOf(horasLinea)));
    		this.fueraLineaReales.setValue(RutinasCadenas.reemplazarPuntoMiles(String.valueOf(horasFueraLinea)));
    		this.incidenciasReales.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(incidenciasNoProgramadas + incidenciasCalidad).toString()));
    		this.programadasReales.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(incidenciasProgramadas).toString()));
    		this.horasAutomatico.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(horasAutomatico).toString()));
    		this.horasJaulon.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(horasJaulon).toString()));
    		this.horasEtiquetado.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(horasEtiquetado).toString()));
    		this.horasHorizontal.setValue(RutinasCadenas.reemplazarPuntoMiles(new Double(horasHorizontal).toString()));
    		this.produccion.setValue(prodSemana.toString());
    		this.mermas.setValue(mermasSemana.toString());
    		
    		/*
    		 * La idea es recoger todos los tiempos del parte de produccion de cada linea
    		 */
    	}
    	else
    	{
    		Notificaciones.getInstance().mensajeAdvertencia("Tienes que rellenar el ejercicio y/o la semana antes de traer los datos.");
    	}
    }
}
