package borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.HorasTrabajadasEmbotelladora.view.HorasTrabajadasEmbotelladoraView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private GridViewRefresh app = null;	
	
    public OpcionesGrid(HorasTrabajadasEmbotelladoraView r_app, ArrayList<MapeoHorasTrabajadasEmbotelladora> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("Horas Embotelladora");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
        this.crearGrid(MapeoHorasTrabajadasEmbotelladora.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ejercicio", "semana", "horas", "fueraLinea", "programadas", "incidencias", "horasReales", "fueraLineaReales", "programadasReales", "incidenciasReales", "mermas","produccion","horasAutomatico","horasJaulon","horasEtiquetado","horasHorizontal", "horasEtt");

    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("ejercicio").setHeaderCaption("Ejercicio");
    	this.getColumn("ejercicio").setWidth(new Double(120));
    	this.getColumn("produccion").setHeaderCaption("Prod.");
    	this.getColumn("produccion").setWidth(new Double(120));
    	this.getColumn("mermas").setHeaderCaption("Mermas");
    	this.getColumn("mermas").setWidth(new Double(120));

    	this.getColumn("semana").setHeaderCaption("Semana");
    	this.getColumn("semana").setWidth(new Double(120));    	
    	this.getColumn("horas").setHeaderCaption("Horas");
    	this.getColumn("horas").setWidth(new Double(120));
    	this.getColumn("horasEtt").setHeaderCaption("Horas Ett");
    	this.getColumn("horasEtt").setWidth(new Double(120));
    	this.getColumn("fueraLinea").setHeaderCaption("Fuera Linea");
    	this.getColumn("fueraLinea").setWidth(new Double(120));
    	this.getColumn("programadas").setHeaderCaption("Programadas");
    	this.getColumn("programadas").setWidth(new Double(120));
    	this.getColumn("incidencias").setHeaderCaption("Inc");
    	this.getColumn("incidencias").setWidth(new Double(120));
    	this.getColumn("horasReales").setHeaderCaption("Horas Reales");
    	this.getColumn("horasReales").setWidth(new Double(120));
    	this.getColumn("fueraLineaReales").setHeaderCaption("Fuera Linea Reales");
    	this.getColumn("fueraLineaReales").setWidth(new Double(120));
    	this.getColumn("programadasReales").setHeaderCaption("Programadas Reales");
    	this.getColumn("programadasReales").setWidth(new Double(120));
    	this.getColumn("incidenciasReales").setHeaderCaption("Inc Reales");
    	this.getColumn("incidenciasReales").setWidth(new Double(120));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("horasAutomatico").setHidden(true);
    	this.getColumn("horasJaulon").setHidden(true);
    	this.getColumn("horasEtiquetado").setHidden(true);
    	this.getColumn("horasHorizontal").setHidden(true);
    	
    }

    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("produccion".equals(cellReference.getPropertyId()) 
            	|| "horas".equals(cellReference.getPropertyId()) || "fueraLinea".equals(cellReference.getPropertyId()) 
            	|| "horasReales".equals(cellReference.getPropertyId()) || "fueraLineaReales".equals(cellReference.getPropertyId()) 
            	|| "horasEtt".equals(cellReference.getPropertyId())  
            	|| "programadas".equals(cellReference.getPropertyId()) || "incidencias".equals(cellReference.getPropertyId()) 
            	|| "programadasReales".equals(cellReference.getPropertyId()) || "incidenciasReales".equals(cellReference.getPropertyId()) 
            	|| "horasAutomatico".equals(cellReference.getPropertyId()) || "horasJaulon".equals(cellReference.getPropertyId()) 
            	|| "horasEtiquetado".equals(cellReference.getPropertyId()) || "horasHorizontal".equals(cellReference.getPropertyId()) 
            	|| "mermas".equals(cellReference.getPropertyId()) || "ejercicio".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	@Override
	public void calcularTotal() {
		//"","","litros", "reservado", "prepedido", "stock_real"
		Double totalHoras = new Double(0) ;
		Double totalHorasEtt = new Double(0) ;
		Double totalProgramadas = new Double(0) ;
		Double totalFuera = new Double(0) ;
		Double totalIncidencias = new Double(0) ;
		Double totalHorasReales = new Double(0) ;
		Double totalProgramadasReales = new Double(0) ;
		Double totalFueraReales = new Double(0) ;
		Double totalIncidenciasReales = new Double(0) ;
		Double totalHorasAutomatico = new Double(0) ;
		Double totalHorasJaulon = new Double(0) ;
		Double totalHorasEtiquetado = new Double(0) ;
		Double totalHorasHorizontal = new Double(0) ;
		Integer totalMermas = new Integer(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
	        	Double q1Value = (Double) item.getItemProperty("horas").getValue();
	        	Double q2Value = (Double) item.getItemProperty("incidencias").getValue();
	        	Double q3Value = (Double) item.getItemProperty("programadas").getValue();
	        	Integer q4Value = (Integer) item.getItemProperty("mermas").getValue();
	        	Double q5Value = (Double) item.getItemProperty("fueraLinea").getValue();
	        	Double q6Value = (Double) item.getItemProperty("horasReales").getValue();
	        	Double q7Value = (Double) item.getItemProperty("incidenciasReales").getValue();
	        	Double q8Value = (Double) item.getItemProperty("programadasReales").getValue();
	        	Double q9Value = (Double) item.getItemProperty("fueraLineaReales").getValue();
	        	Double q10Value = (Double) item.getItemProperty("horasAutomatico").getValue();
	        	Double q11Value = (Double) item.getItemProperty("horasJaulon").getValue();
	        	Double q12Value = (Double) item.getItemProperty("horasEtiquetado").getValue();
	        	Double q13Value = (Double) item.getItemProperty("horasHorizontal").getValue();
	        	Double q14Value = (Double) item.getItemProperty("horasEtt").getValue();
	        	if (q1Value!=null) totalHoras += q1Value;
	        	if (q2Value!=null) totalIncidencias += q2Value;
	        	if (q3Value!=null) totalProgramadas+= q3Value;
	        	if (q4Value!=null) totalMermas += q4Value;
	        	if (q5Value!=null) totalFuera+= q5Value;
	        	if (q6Value!=null) totalHorasReales += q6Value;
	        	if (q7Value!=null) totalIncidenciasReales += q7Value;
	        	if (q8Value!=null) totalProgramadasReales += q8Value;
	        	if (q9Value!=null) totalFueraReales += q9Value;
	        	if (q10Value!=null) totalHorasAutomatico += q10Value;
	        	if (q11Value!=null) totalHorasJaulon += q11Value;
	        	if (q12Value!=null) totalHorasEtiquetado += q12Value;
	        	if (q13Value!=null) totalHorasHorizontal += q13Value;
	        	if (q14Value!=null) totalHorasEtt += q14Value;
        	}
        }
        
        footer.getCell("semana").setText("Totales");
		footer.getCell("horas").setText(RutinasNumericas.formatearDouble(totalHoras).toString());
		footer.getCell("horasEtt").setText(RutinasNumericas.formatearDouble(totalHorasEtt).toString());
		footer.getCell("fueraLinea").setText(RutinasNumericas.formatearDouble(totalFuera).toString());
		footer.getCell("incidencias").setText(RutinasNumericas.formatearDouble(totalIncidencias).toString());
		footer.getCell("programadas").setText(RutinasNumericas.formatearDouble(totalProgramadas).toString());
		footer.getCell("horasReales").setText(RutinasNumericas.formatearDouble(totalHorasReales).toString());
		footer.getCell("fueraLineaReales").setText(RutinasNumericas.formatearDouble(totalFueraReales).toString());
		footer.getCell("incidenciasReales").setText(RutinasNumericas.formatearDouble(totalIncidenciasReales).toString());
		footer.getCell("programadasReales").setText(RutinasNumericas.formatearDouble(totalProgramadasReales).toString());
		footer.getCell("horasAutomatico").setText(RutinasNumericas.formatearDouble(totalHorasAutomatico).toString());
		footer.getCell("horasJaulon").setText(RutinasNumericas.formatearDouble(totalHorasJaulon).toString());
		footer.getCell("horasEtiquetado").setText(RutinasNumericas.formatearDouble(totalHorasEtiquetado).toString());
		footer.getCell("horasHorizontal").setText(RutinasNumericas.formatearDouble(totalHorasHorizontal).toString());
		footer.getCell("mermas").setText(totalMermas.toString());
		
//		footer.setStyleName("smallgrid");
		footer.getCell("horas").setStyleName("Rcell-pie");
		footer.getCell("horasEtt").setStyleName("Rcell-pie");
		footer.getCell("fueraLinea").setStyleName("Rcell-pie");
		footer.getCell("incidencias").setStyleName("Rcell-pie");
		footer.getCell("programadas").setStyleName("Rcell-pie");
		footer.getCell("horasReales").setStyleName("Rcell-pie");
		footer.getCell("fueraLineaReales").setStyleName("Rcell-pie");
		footer.getCell("incidenciasReales").setStyleName("Rcell-pie");
		footer.getCell("programadasReales").setStyleName("Rcell-pie");
		footer.getCell("horasAutomatico").setStyleName("Rcell-pie");
		footer.getCell("horasJaulon").setStyleName("Rcell-pie");
		footer.getCell("horasEtiquetado").setStyleName("Rcell-pie");
		footer.getCell("horasHorizontal").setStyleName("Rcell-pie");
		footer.getCell("mermas").setStyleName("Rcell-pie");
		this.app.barAndGridLayout.setWidth("100%");
		this.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-20)+"%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
	}
	
}
