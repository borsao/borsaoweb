package borsao.e_borsao.Modulos.PRODUCCION.ProgramacionAux.modelo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.renderers.HtmlRenderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.CALIDAD.observacionesProgramacion.view.pantallaObservacionesProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.OpcionesMateriaSeca.server.consultaOpcionesMateriaSecaServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionEtiquetasBotella;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionEtiquetasEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ProgramacionAux.server.consultaProgramacionServerAux;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server.consultaTiposOFServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ProgramacionAuxGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	public HashMap<String, String> hayObs = null;

	private PeticionEtiquetasEmbotellado vtPeticion = null;
	private boolean editable = false;
	private boolean conFiltro = false;
	
	public Integer permisos = null;
//	private Integer indice = null;	
	private MapeoProgramacion mapeo=null;
	private MapeoProgramacion mapeoOrig=null;
	
    public ProgramacionAuxGrid(Integer r_permisos, ArrayList<MapeoProgramacion> r_vector) 
    {
        this.permisos=r_permisos;
        this.vector=r_vector;
        
		this.asignarTitulo("PROGRAMACION AUXILIAR EMBOTELLADO");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public ProgramacionAuxGrid(Integer r_permisos, ArrayList<MapeoProgramacion> r_vector, HashMap<String, String> r_obs) 
    {
        this.permisos=r_permisos;
        this.vector=r_vector;
        this.hayObs = r_obs;
        
		this.asignarTitulo("PROGRAMACION AUXILIAR EMBOTELLADO");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoProgramacion.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(4);
		
		if (this.permisos==10)
		{
			this.setStyleName("programacionLaboratorio");
		}
		else
		{
			this.setStyleName("programacion");
		}
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		if (this.permisos<40)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.NONE);
		}
		else if (this.permisos<50)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.SINGLE);
		}
		else 
		{
			if (this.permisos!=99) setEditorEnabled(true);
			this.setSeleccion(SelectionMode.SINGLE);
		}
    }
    

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	
    	this.mapeoOrig = new MapeoProgramacion();
    	this.mapeo=((MapeoProgramacion) getEditedItemId());
    	
    	if (this.permisos<99) 
    	{
    		this.mapeoOrig.setLote(this.mapeo.getLote());
    		this.mapeoOrig.setArticulo(this.mapeo.getArticulo());
    		this.mapeoOrig.setTipo(this.mapeo.getTipo());
    		this.mapeoOrig.setCantidad(this.mapeo.getCantidad());
    		this.mapeoOrig.setLote(this.mapeo.getLote());
    		this.mapeoOrig.setVino(this.mapeo.getVino());
    		this.mapeoOrig.setDescripcion(this.mapeo.getDescripcion());
    		this.mapeoOrig.setOrden(this.mapeo.getOrden());
    		this.mapeoOrig.setCajas(this.mapeo.getCajas());
    		this.mapeoOrig.setFactor(this.mapeo.getFactor());
    		this.mapeoOrig.setUnidades(this.mapeo.getUnidades());
    		this.mapeoOrig.setEstado(this.mapeo.getEstado());
    		this.mapeoOrig.setVelocidad(this.mapeo.getVelocidad());
    	}
    	if (this.permisos<70) 
    	{
    		this.mapeoOrig.setCaja(this.mapeo.getCaja());
    		this.mapeoOrig.setFrontales(this.mapeo.getFrontales());
    		this.mapeoOrig.setCapsulas(this.mapeo.getCapsulas());
    		this.mapeoOrig.setContras(this.mapeo.getContras());
    		this.mapeoOrig.setTapones(this.mapeo.getTapones());
    		
    	}
    	
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("ojo", "papel", "biblia", "esc", "etiqueta", "cambios", "subir", "bajar", "estado", "dia", "tipo", "cantidad", "vino", "lote","velocidad", "orden","articulo" , "descripcion", "cajas", "factor" , "unidades", "observaciones",   "frontales", "contras", "capsulas", "tapones","caja","nuevo");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("cambios").setHidden(false);
    	this.getColumn("ojo").setHeaderCaption("");
    	this.getColumn("ojo").setSortable(false);
    	this.getColumn("ojo").setWidth(new Double(50));
    	this.getColumn("papel").setHeaderCaption("");
    	this.getColumn("papel").setSortable(false);
    	this.getColumn("papel").setWidth(new Double(40));
    	this.getColumn("biblia").setHeaderCaption("");
    	this.getColumn("biblia").setSortable(false);
    	this.getColumn("biblia").setWidth(new Double(40));
    	this.getColumn("esc").setHeaderCaption("");
    	this.getColumn("esc").setSortable(false);
    	this.getColumn("esc").setWidth(new Double(40));
    	this.getColumn("etiqueta").setHeaderCaption("");
    	this.getColumn("etiqueta").setSortable(false);
    	this.getColumn("etiqueta").setWidth(new Double(40));
    	this.getColumn("cambios").setHeaderCaption("");
    	this.getColumn("cambios").setSortable(false);
    	this.getColumn("cambios").setWidth(new Double(40));
    	this.getColumn("subir").setHeaderCaption("");
    	this.getColumn("subir").setSortable(false);
    	this.getColumn("subir").setWidth(new Double(40));
    	this.getColumn("bajar").setHeaderCaption("");
    	this.getColumn("bajar").setSortable(false);
    	this.getColumn("bajar").setWidth(new Double(40));

    	this.getColumn("dia").setHeaderCaption("DIA");
    	this.getColumn("dia").setWidthUndefined();
    	this.getColumn("dia").setWidth(100);
    	this.getColumn("tipo").setHeaderCaption("TIPO");
    	this.getColumn("tipo").setWidth(85);
    	this.getColumn("cantidad").setHeaderCaption("CANT.");
    	this.getColumn("cantidad").setWidth(50);
    	this.getColumn("orden").setHeaderCaption("ORD.");
    	this.getColumn("orden").setWidth(50);
    	this.getColumn("vino").setHeaderCaption("VINO");
    	this.getColumn("vino").setWidth(150);
    	this.getColumn("lote").setHeaderCaption("LOTE");
    	this.getColumn("lote").setWidth(75);
    	this.getColumn("articulo").setHeaderCaption("CODIGO");
    	this.getColumn("articulo").setWidth(65);
    	this.getColumn("descripcion").setHeaderCaption("ARTICULO");
    	this.getColumn("descripcion").setWidth(300);
    	this.getColumn("cajas").setHeaderCaption("CAJAS");
    	this.getColumn("cajas").setWidth(50);
    	this.getColumn("factor").setHeaderCaption("B/C");
    	this.getColumn("factor").setWidth(50);
    	this.getColumn("unidades").setHeaderCaption("UDS.");
    	this.getColumn("unidades").setWidth(50);
    	this.getColumn("observaciones").setHeaderCaption("OBSERVACIONES");
    	this.getColumn("observaciones").setWidthUndefined();    	
    	this.getColumn("observaciones").setEditorField(getTextArea());
    	this.getColumn("frontales").setHeaderCaption("FRONT.");
    	this.getColumn("frontales").setWidth(65);
    	this.getColumn("contras").setHeaderCaption("CONT.");
    	this.getColumn("contras").setWidth(65);
    	this.getColumn("capsulas").setHeaderCaption("CAPS.");
    	this.getColumn("capsulas").setWidth(65);
    	this.getColumn("tapones").setHeaderCaption("TAPON");
    	this.getColumn("tapones").setWidth(65);
    	this.getColumn("caja").setHeaderCaption("CAJAS");
    	this.getColumn("caja").setWidth(65);
    	
    	this.getColumn("velocidad").setHeaderCaption("ETIQ");
    	this.getColumn("velocidad").setWidth(50);
    	
    	this.getColumn("frontales").setEditorField(getComboBox("El campo es obligatorio!"));
    	this.getColumn("contras").setEditorField(getComboBox("El campo es obligatorio!"));
    	this.getColumn("capsulas").setEditorField(getComboBox("El campo es obligatorio!"));
    	this.getColumn("tapones").setEditorField(getComboBox("El campo es obligatorio!"));
    	this.getColumn("caja").setEditorField(getComboBox("El campo es obligatorio!"));
    	
    	this.getColumn("idProgramacion").setHidden(true);
    	this.getColumn("ejercicio").setHidden(true);    	
    	this.getColumn("semana").setHidden(true);
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("status").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("ojo").setHidden(true);
    	this.getColumn("subir").setHidden(true);
    	this.getColumn("bajar").setHidden(true);
    	this.getColumn("orden").setHidden(true);
    	this.getColumn("etiqueta").setHidden(true);
    	this.getColumn("nuevo").setHidden(true);
    	
//    	this.getColumn("cambios").setHidden(true);
    	if (permisos < 80)
    	{
    		this.getColumn("etiqueta").setHidden(true);
    	}
    	
    	if (permisos<30)
    	{
    		this.getColumn("papel").setHidden(true);
    		this.getColumn("etiqueta").setHidden(true);
    	}

    	if (permisos<=20)
    	{
    		this.getColumn("frontales").setHidden(true);
    		this.getColumn("contras").setHidden(true);
    		this.getColumn("capsulas").setHidden(true);
    		this.getColumn("tapones").setHidden(true);
    		this.getColumn("caja").setHidden(true);
    	}
    	if (permisos <= 10)
    	{
    		this.getColumn("observaciones").setHidden(true);
    		this.getColumn("unidades").setHidden(true);
    		this.getColumn("factor").setHidden(true);
    		this.getColumn("cajas").setHidden(true);
    		this.getColumn("descripcion").setHidden(true);
    		this.getColumn("articulo").setHidden(true);
    		this.getColumn("orden").setHidden(true);
    	}
    	
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoProgramacion) {
                MapeoProgramacion progRow = (MapeoProgramacion)bean;
                // The actual description text is depending on the application
                if ("observaciones".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getObservaciones();
                }
                else if ("lote".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getLote();
                }
                else if ("descripcion".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getDescripcion();
                }
                else if ("biblia".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ficha de producto";
                }
                else if ("esc".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ayuda produccion";
                }
                else if ("cambios".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso Ficha Calidad";
                }
                else if ("papel".equals(cell.getPropertyId()))
                {
                	descriptionText = "Impresion Etiquetas Pale";
                }
                else if ("etiqueta".equals(cell.getPropertyId()))
                {
                	descriptionText = "Impresion Etiquetas Botella";
                }
            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            String estilo = null;
            String color = null;
            String completado = null;
//            boolean articuloNuevo= true;
            boolean parada = false;
            boolean separador = false;
            boolean vino = false;
//            Integer dias = 0;
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	consultaTiposOFServer tiposOF = consultaTiposOFServer.getInstance(CurrentUser.get());
            	consultaOpcionesMateriaSecaServer matSeca = consultaOpcionesMateriaSecaServer.getInstance(CurrentUser.get());
            	if ("ojo".equals(cellReference.getPropertyId())) 
            	{            		
            		if (cellReference.getValue()!=null && cellReference.getValue().toString().equals("S"))
            		{
//            			articuloNuevo=true;
            		}
            		separador=false;
            		parada=false;
            		color = "#lincur";
            	}
            	if ("estado".equals(cellReference.getPropertyId())) 
            	{            		
            		completado = cellReference.getValue().toString();
            		if ("T".equals(completado)) 
        			{
            			color = "#linfin";
        			}
            		else
            		{            			
            			color = "#lincur";
            		}
            	}
            	else if ("tipo".equals(cellReference.getPropertyId())) 
            	{
            		if (cellReference.getValue().toString().equals("PARADA LINEA")) parada=true;
            		if (cellReference.getValue().toString().equals("LINEA")) separador = true;
            		if (cellReference.getValue().toString().equals("VINO"))
            		{
            			color = "#linsep";
            			vino = true;
            		}
            		else
            		{
            			color = tiposOF.obtenerColor(cellReference.getValue().toString());
            			vino = false;
            		}
                }
            	else if ("frontales".equals(cellReference.getPropertyId())) {
            		color = matSeca.obtenerColor(cellReference.getValue().toString());
            	}

            	else if ("contras".equals(cellReference.getPropertyId())) {
            		color = matSeca.obtenerColor(cellReference.getValue().toString());
            	}
            	else if ("capsulas".equals(cellReference.getPropertyId())) {
            		color = matSeca.obtenerColor(cellReference.getValue().toString());
            	}
            	else if ("tapones".equals(cellReference.getPropertyId())) {
            		color = matSeca.obtenerColor(cellReference.getValue().toString());
            	}
            	else if ("caja".equals(cellReference.getPropertyId())) {
            		color = matSeca.obtenerColor(cellReference.getValue().toString());
            	}
            	else if ("lote".equals(cellReference.getPropertyId())) {            		
            		color = "#linlot" ; 
            	}
            	else if ("velocidad".equals(cellReference.getPropertyId()))
            	{
            		color ="#linvel"; 
            	}
        		else if ("dia".equals(cellReference.getPropertyId())) {            		
                		color = "#lincun" ; 
            	}
//            	else if ("dia".equals(cellReference.getPropertyId())) {
//            		color = "#lincur";
//            	}
//            	else if ("cantidad".equals(cellReference.getPropertyId())) {
//            		color = "#lincur";
//            	}
            	else if ("vino".equals(cellReference.getPropertyId())) {
            		if (vino)
            		{
            			if ("N".equals(completado))
                		{
                			color = "#lincuo";

                		} 	
            			else
            			{
            				color = "#linvin";
            			}
            		}
            		else
            		{
            			color = "#lincui";
            		}
            	}
//            	else if ("descripcion".equals(cellReference.getPropertyId())) {            		
////            		if (articuloNuevo && completado.equals("A")) color = "#linout";
////            		color = "#lincur";
//            	}
            	else if ("articulo".equals(cellReference.getPropertyId())) 
            	{
            		if ("P".equals(completado))
            		{
            			color = "#linpen";

            		}
            	}

            	else
            	{
            		if ("T".equals(completado)) 
        			{
            			color = "#linfin";
        			}
            		else if ("N".equals(completado) && !vino)
            		{
            			color = "#linout";

            		} 	
            		else 
            		{
            			color = "#lincur";            			
            		}
            	}
            	
            	if ("cantidad".equals(cellReference.getPropertyId()) && cellReference.getValue()!=null && !cellReference.getValue().toString().equals("0"))
            	{
            		if ("P".equals(completado))
            		{
            			color = "#linpen";

            		}
            		else 
            		{
            			if (vino)
            			color = "#lincun";
            			else
            				color = "#lincui";
            		}
            	}
            	else if ("cantidad".equals(cellReference.getPropertyId()) && cellReference.getValue()!=null && cellReference.getValue().toString().equals("0"))
            	{
            		color = "#linsep";
            	}

            	if (parada) 
        		{
            		color="#linout";
        		}
            	if (separador) 
            	{
            		color = "#linsep";	
            	}
            	
            	estilo = "cell-" + color.substring(1, 7);
            	
            	if ("orden".equals(cellReference.getPropertyId()) || "cajas".equals(cellReference.getPropertyId()) 
            			|| "factor".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId())
            			|| "cantidad".equals(cellReference.getPropertyId()) || "velocidad".equals(cellReference.getPropertyId()))
            	{
            		estilo = 'R' + estilo;
            	}
             	            	
//            	else if ("vino".equals(cellReference.getPropertyId()))
//            	{
//            		if ("T".equals(completado)) 
//        			{
//            			color = "#linfin";
//        			}
//            		else if ("P".equals(completado))
//            		{
//            			color = "#linpen";
//
//            		}
//            		else 
//            		{
//            			color = "#lincur";            			
//            		}
//            		estilo = "cell-" + color.substring(1, 7);
//            	}

            	if ( "biblia".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonBiblia";
            	}
            	if ( "esc".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonEsc";
            	}
            	if ( "etiqueta".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonEtiqueta";
            	}

            	if ("cambios".equals(cellReference.getPropertyId()))
            	{
                    Item item = cellReference.getItem();
                    if (item instanceof BeanItem<?>) 
                    {
                        Object bean = ((BeanItem<?>)item).getBean();
                        if (bean instanceof MapeoProgramacion) 
                        {
                            MapeoProgramacion progRow = (MapeoProgramacion)bean;

                            if (progRow!=null && progRow.getArticulo()!=null)
                    		{
                    			if (hayObs!=null && hayObs.get(progRow.getArticulo())!=null && hayObs.get(progRow.getArticulo()).contentEquals("S"))
                    			{
                    				estilo = "cell-nativebuttonCambiosW";
                    			}
                    			else estilo = "cell-nativebuttonCambios";
                    		}
                    		else
                    		{
                            	estilo = "cell-nativebuttonCambios";
                    		}
                    	}
                        else
                        {
                        	estilo = "cell-nativebuttonCambios";
                        }
                    }
                    else
                    {
                    	estilo = "cell-nativebuttonCambios";
                    }
            	}
            	if ( "papel".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonEan";
            	}
            	if ( "subir".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonUp";
            	}
            	if ( "bajar".equals(cellReference.getPropertyId()))
            	{
            		estilo = "cell-nativebuttonDw";
            	}

            	return estilo;
            }
        });
    	
    }
    
    public void cargarListeners()
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
        		mapeo = (MapeoProgramacion) getEditedItemId();
//        		indice = getContainer().indexOfId(mapeo);
		        
	        	consultaProgramacionServerAux cs = new consultaProgramacionServerAux(CurrentUser.get());
	        	Integer status = cs.obtenerStatus(mapeo.getIdProgramacion());
	        	if (mapeo.getStatus()==status || status == null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeAdvertencia("Registro modificado por otro usuario. Actualiza los cambios antes de modificar los datos.");
	        		doCancelEditor();
	        	}
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		mapeo = (MapeoProgramacion) getEditedItemId();
	        		mapeoOrig=(MapeoProgramacion) getEditedItemId();
	        		
	        		consultaProgramacionServerAux cs = new consultaProgramacionServerAux(CurrentUser.get());
		        
	        		if (mapeo.getIdProgramacion()!=null)
		        	{
	        			if (permisos<99) 
	        	    	{
	        				mapeo.setLote(mapeoOrig.getLote());
	        				mapeo.setArticulo(mapeoOrig.getArticulo());
	        				mapeo.setTipo(mapeoOrig.getTipo());
	        				mapeo.setCantidad(mapeoOrig.getCantidad());
	        				mapeo.setVino(mapeoOrig.getVino());
	        				mapeo.setDescripcion(mapeoOrig.getDescripcion());
	        				mapeo.setOrden(mapeoOrig.getOrden());
	        				mapeo.setCajas(mapeoOrig.getCajas());
	        				mapeo.setFactor(mapeoOrig.getFactor());
	        				mapeo.setUnidades(mapeoOrig.getUnidades());
	        				mapeo.setEstado(mapeoOrig.getEstado());
	        				mapeo.setVelocidad(mapeoOrig.getVelocidad());
        				}
	        			
	        	    	if (permisos<70) 
	        	    	{
	        	    		mapeo.setCaja(mapeoOrig.getCaja());
	        	    		mapeo.setFrontales(mapeoOrig.getFrontales());
	        	    		mapeo.setCapsulas(mapeoOrig.getCapsulas());
	        	    		mapeo.setContras(mapeoOrig.getContras());
	        	    		mapeo.setTapones(mapeoOrig.getTapones());
	        	    	}
	        			
		        		cs.guardarCambios(mapeo);
		        		mapeo.setStatus(mapeo.getStatus()+1);
		        		
		        		((ArrayList<MapeoProgramacion>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoProgramacion>) vector).add(mapeo);

		    			refreshAllRows();
		    			scrollTo(mapeo);
		    			select(mapeo);
		        	}
	        	}
	        }
		});
		
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoProgramacion mapeo = (MapeoProgramacion) event.getItemId();
            		
	            	if (event.getPropertyId().toString().equals("papel"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
		        	    	if (permisos>=30)
		        	    	{
		        	    		
		        	    		/*
		        	    		 * Abriremos ventana peticion parametros
		        	    		 * rellenaremos la variables publicas
		        	    		 * 		cajas y anada
		        	    		 * 		llamaremos al metodo generacionPdf
		        	    		 */
		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionAuxGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
		        	    		getUI().addWindow(vtPeticion);
		        	    		
		        	    		
		        	    	}	    			
							else
							{
								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
							}
	            		}
	            	}
	            	else if (event.getPropertyId().toString().equals("biblia"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		String host = UI.getCurrent().getPage().getLocation().getHost().trim();
	            		
	            		if (host.equals("localhost")) host = "192.168.6.6";
	            		
	            		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + mapeo.getArticulo().trim();
	            		getUI().getPage().open(url, "_blank");
	            		
	            	}
	            	else if (event.getPropertyId().toString().equals("esc"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	            			pantallaAyudaProduccion vt = null;
	            			if (mapeo.getTipo().toUpperCase().equals("EMBOTELLADO"))
	            			{
	            				vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo());
	            			}
	            			else
	            			{	            				
	            				vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo()+"-1");
	            			}
	            			getUI().addWindow(vt);	            			
	            		}	            		
	            	}
	            	else if (event.getPropertyId().toString().equals("etiqueta"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	        	    		PeticionEtiquetasBotella vt = null;
	        	    		vt= new PeticionEtiquetasBotella((ProgramacionAuxGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas");
	        	    		getUI().addWindow(vt);
	            		}
	            	}
	            	else if (event.getPropertyId().toString().equals("cambios"))
	            	{
	            		activadaVentanaPeticion=true;
	            		ordenando = false;
	            		
	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
	            		{
	            			pantallaObservacionesProgramacion vt = null;
	            			vt = new pantallaObservacionesProgramacion(mapeo.getArticulo(), mapeo.getIdProgramacion(), mapeo.getTipo());
	            			getUI().addWindow(vt);	            			
	            		}	            		
	            	}	            	
	            	else if (event.getPropertyId().toString().equals("subir"))
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = true;
	            		
	            		vector.remove(mapeo);
	            		
	            		Object itemId = event.getItemId();
	            		int indexOfRow = getContainer().indexOfId(itemId); 
	
	            		try
	            		{
		            		MapeoProgramacion mapeoPrevio = (MapeoProgramacion)  getContainer().getIdByIndex(indexOfRow-1);
		            		
		            		vector.remove(mapeoPrevio);
		            		
		            		Integer origen = mapeo.getOrden();
		            		Integer destino = mapeoPrevio.getOrden();
		            		
		            		mapeo.setOrden(destino);
		            		mapeoPrevio.setOrden(origen);
		            		
		            		guardarCambios(mapeo, mapeoPrevio);
		            		select(getContainer().getIdByIndex(destino));
	            		}
	        			catch (IndexOutOfBoundsException ex)
	        			{
	        				
	        			}
	            	}
	            	else if (event.getPropertyId().toString().equals("bajar"))
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = true;
	            		
	            		vector.remove(mapeo);
	    				
	            		Object itemId = event.getItemId();
	            		int indexOfRow = getContainer().indexOfId(itemId); 
	
	        			try
	        			{
		            		MapeoProgramacion mapeoPrevio = (MapeoProgramacion)  getContainer().getIdByIndex(indexOfRow+1);
		            		
		            		vector.remove(mapeoPrevio);
		            		
		            		Integer origen = mapeo.getOrden();
		            		Integer destino = mapeoPrevio.getOrden();
		            		
		            		mapeo.setOrden(destino);
		            		mapeoPrevio.setOrden(origen);
		            		
		            		guardarCambios(mapeo, mapeoPrevio);
		            		select(getContainer().getIdByIndex(destino));
		            		
	        			}
	        			catch (IndexOutOfBoundsException ex)
	        			{
	        				
	        			}
	            	}            	
	            	else
	            	{
	            		activadaVentanaPeticion=false;
	            		ordenando = false;
	            	}
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("papel");
		this.camposNoFiltrar.add("biblia");
		this.camposNoFiltrar.add("esc");
		this.camposNoFiltrar.add("etiqueta");
		this.camposNoFiltrar.add("cambios");
	}
	/**
	 * @return
	 */
	private Field<?> getComboBox(String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();
		ArrayList<String> valores = new ArrayList<String>();
		
		consultaOpcionesMateriaSecaServer cms = new consultaOpcionesMateriaSecaServer(CurrentUser.get());
		valores = cms.datosOpcionesCombo();
		
		IndexedContainer container = new IndexedContainer(valores);
		comboBox.setContainerDataSource(container);
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
		comboBox.setStyleName("blanco");
		return comboBox;
	}

	public void impresionEtiqueta(String r_archivo, Integer r_cuantas, String r_impresora) 
    {
		if(r_archivo.length()>0)
		{
			try
            { 
				String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/germark" + r_archivo;
				
				RutinasFicheros.imprimirCuantas(null, archivo, r_impresora,r_cuantas,null);
            }
			catch(Exception e)
			{ 
				Notificaciones.getInstance().mensajeError(e.getMessage()); 
			}
		}
		else
			Notificaciones.getInstance().mensajeError("Imprimir Etiqueta Programacion EMB. Fichero de contra inexistente.");
    }

	private Field<?> getTextArea() {
		TextArea textArea = new TextArea();	
		textArea.setWidth("400px");
		textArea.setStyleName("areablanca");
		
		return textArea;
	}
 
	public void generacionPdf(MapeoProgramacion r_mapeo, String r_lote, String r_tipo, Integer r_cajas, String r_anada, String r_cantidad, boolean r_paletCompleto, boolean r_eliminar, String r_impresora) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	
    	
    	consultaProgramacionServerAux cps = new consultaProgramacionServerAux(CurrentUser.get());
    	pdfGenerado = cps.generarInforme(r_mapeo,r_lote, r_tipo, r_cajas, r_anada, r_cantidad, r_paletCompleto);
    		if(pdfGenerado.length()>0)
    		{
    			try
                { 
    				if (r_impresora.toUpperCase().equals("PANTALLA"))
    				{
    					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
    				}
    				else
    				{
    					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
    					
    					RutinasFicheros.imprimir(archivo, r_impresora);
					    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
    				}
    				
                }
    			catch(Exception e)
                 { 
    				Notificaciones.getInstance().mensajeError("Generar PDF Programacion EMB" + e.getMessage()); 
                 }
    		}
    		else
    			Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	public String guardarCambios(MapeoProgramacion r_mapeo, MapeoProgramacion r_mapeoPrevio)
	{
		String rdo =null;
		consultaProgramacionServerAux cus = new consultaProgramacionServerAux(CurrentUser.get());
		if (r_mapeo!=null)
		{
			rdo = cus.guardarCambios(r_mapeo);
			if (rdo== null)
			{
				r_mapeo.setStatus(r_mapeo.getStatus()+1);
				
				((ArrayList<MapeoProgramacion>) vector).add(r_mapeo);
				
				if (r_mapeoPrevio!=null)
				{
					rdo = cus.guardarCambios(r_mapeoPrevio);
					if (rdo==null)
					{
						r_mapeoPrevio.setStatus(r_mapeoPrevio.getStatus()+1);
						((ArrayList<MapeoProgramacion>) vector).add(r_mapeoPrevio);
					}
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError(rdo);
			}
			
			if (!this.vector.isEmpty())
			{
				
				Indexed indexed = this.getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            this.removeAllColumns();
	            this.generarGrid();
    
				this.sort("orden");
				this.select(r_mapeo);
				this.scrollTo(r_mapeo);
			}
		}		
		return rdo;
	}
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	@Override
	public void calcularTotal() {
		
	}
    
}


