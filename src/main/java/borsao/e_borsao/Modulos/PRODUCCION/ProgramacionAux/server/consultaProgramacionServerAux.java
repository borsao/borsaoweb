package borsao.e_borsao.Modulos.PRODUCCION.ProgramacionAux.server;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.EtiquetasEmbotellado.server.consultaEtiquetasEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.calculoSituacionEmbotellado.modelo.MapeoSituacionEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.modelo.MapeoLineasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaProgramacionServerAux
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaProgramacionServerAux instance;
	
	public consultaProgramacionServerAux(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaProgramacionServerAux getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaProgramacionServerAux(r_usuario);			
		}
		return instance;
	}
	
	
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}
	
	public Integer obtenerVelocidadArticulo(String r_articulo)
	{
		Integer rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerVelocidadArticulo(r_articulo);
		return rdo;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobal(MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_aux.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_aux.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_aux.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_aux.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_aux.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_aux.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_aux.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_aux.contras pro_con,");
		cadenaSQL.append(" prd_programacion_aux.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion_aux.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion_aux.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_aux.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_aux.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVelocidad()!=null && r_mapeo.getVelocidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" velocidad = " + r_mapeo.getVelocidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
					
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));				
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public Integer cuantosProgramacionGlobal(MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		Integer cuantos = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT count(prd_programacion_aux.idprd_programacion) pro_num ");
		cadenaSQL.append(" from prd_programacion_aux ");
		try
		{
			cadenaWhere = new StringBuffer();
			cadenaWhere.append(" prd_programacion_aux.tipoOperacion <> 'RETRABAJO' ");

			if (r_mapeo!=null)
			{
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			cuantos = 0;
			while(rsOpcion.next())
			{
				cuantos = rsOpcion.getInt("pro_num");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return cuantos;
	}
	public MapeoProgramacion datosProgramacionGlobal(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		MapeoProgramacion mapeoProgramacion = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_aux.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_aux.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_aux.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_aux.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_aux.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_aux.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_aux.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_aux.contras pro_con,");
		cadenaSQL.append(" prd_programacion_aux.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion_aux.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion_aux.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_aux.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_aux.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			if (r_id!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_id!=null && r_id.toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_id + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
					
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeoProgramacion;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobalNuevo(MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_aux.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_aux.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_aux.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_aux.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_aux.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_aux.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_aux.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_aux.contras pro_con,");
		cadenaSQL.append(" prd_programacion_aux.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion_aux.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion_aux.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_aux.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_aux.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_programacion = '" + r_mapeo.getIdProgramacion() + "' ");
				}
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()) + " ");
				}
				if (r_mapeo.getDia()!=null && r_mapeo.getDia().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" dia = '" + r_mapeo.getDia() + "' ");
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getVelocidad()!=null && r_mapeo.getVelocidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" velocidad = " + r_mapeo.getVelocidad() + " ");
				}
				if (r_mapeo.getVino()!=null && r_mapeo.getVino().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" vino = '" + r_mapeo.getVino() + "' ");
				}
				if (r_mapeo.getLote()!=null && r_mapeo.getLote().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" lote = '" + r_mapeo.getLote() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden = " + r_mapeo.getOrden() + " ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCajas()!=null && r_mapeo.getCajas().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cajas = " + r_mapeo.getCajas() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
				if (r_mapeo.getFrontales()!=null && r_mapeo.getFrontales().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" frontales = '" + r_mapeo.getFrontales() + "' ");
				}
				if (r_mapeo.getContras()!=null && r_mapeo.getContras().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" contras = '" + r_mapeo.getContras() + "' ");
				}
				if (r_mapeo.getCapsulas()!=null && r_mapeo.getCapsulas().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" capsulas = '" + r_mapeo.getCapsulas() + "' ");
				}
				if (r_mapeo.getTapones()!=null && r_mapeo.getTapones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tapon = '" + r_mapeo.getTapones() + "' ");
				}
				if (r_mapeo.getCaja()!=null && r_mapeo.getCaja().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" caja = '" + r_mapeo.getCaja() + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("X")) 
						cadenaWhere.append(" estado in ('A','N') ");
					else
						cadenaWhere.append(" estado = '" + r_mapeo.getEstado() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				
				String dia = "";
				if (rsOpcion.getString("pro_dia")==null || (rsOpcion.getString("pro_dia")!=null && rsOpcion.getString("pro_dia").length()==0))
				{
					dia = this.obtenerDia(rsOpcion.getInt("pro_ej"), rsOpcion.getString("pro_sem"), rsOpcion.getInt("pro_ord"));
				}

//				dia = rsOpcion.getString("pro_dia");
				mapeoProgramacion.setDia(dia);
					
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoProgramacion> datosProgramacionTotal(Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_aux.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_aux.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_aux.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_aux.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_aux.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_aux.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_aux.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_aux.contras pro_con,");
		cadenaSQL.append(" prd_programacion_aux.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion_aux.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion_aux.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_aux.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_aux.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where ((prd_programacion_aux.ejercicio = " + r_ejercicio + " and semana >= " + new Integer(r_semana) + ")  ") ;
		cadenaSQL.append(" or (prd_programacion_aux.ejercicio > " + r_ejercicio + "))  ") ;
		cadenaSQL.append(" and estado in ('A','N')  ") ;
		
		try
		{
			
			cadenaSQL.append(" order by orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			System.out.println("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		return vector;
	}
	
	public ArrayList<MapeoProgramacion> datosProgramacionGlobal(ArrayList<String> r_articulos, boolean r_etiquetados)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoProgramacion> vector = null;
		boolean jaulon = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.tipoOperacion pro_ope,");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can,");
		cadenaSQL.append(" prd_programacion_aux.vino pro_vin,");
		cadenaSQL.append(" prd_programacion_aux.lote pro_lot,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord,");
		cadenaSQL.append(" prd_programacion_aux.articulo pro_art,");
		cadenaSQL.append(" prd_programacion_aux.descripcion pro_des,");
		cadenaSQL.append(" prd_programacion_aux.cajas pro_caj,");
		cadenaSQL.append(" prd_programacion_aux.factor pro_fac,");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_ud,");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs,");
		cadenaSQL.append(" prd_programacion_aux.frontales pro_fro,");
		cadenaSQL.append(" prd_programacion_aux.contras pro_con,");
		cadenaSQL.append(" prd_programacion_aux.capsulas pro_cap,");
		cadenaSQL.append(" prd_programacion_aux.tapon pro_tap,");
		cadenaSQL.append(" prd_programacion_aux.caja pro_cj, ");
		cadenaSQL.append(" prd_programacion_aux.estado pro_es, ");
		cadenaSQL.append(" prd_programacion_aux.velocidad pro_vel, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			
     		String articulos="";
     		for (int i=0; i<r_articulos.size();i++)
     		{
     			if (r_articulos.get(i).trim().endsWith("-1")) jaulon=true;
     			if (articulos!="") articulos = articulos + ",";
     			articulos =articulos + "'" + r_articulos.get(i).trim().substring(0, 7) + "'";
     		}
     		if (articulos!="") cadenaSQL.append(" where articulo in (" + articulos + ") ");

     		if (r_etiquetados || jaulon) cadenaSQL.append(" and tipoOperacion in ('EMBOTELLADO','ETIQUETADO') ");
     		else cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' ");
     		
     		
     		cadenaSQL.append(" and estado in ('A','N') ");
			cadenaSQL.append(" order by ejercicio, semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoProgramacion>();
			
			while(rsOpcion.next())
			{
				MapeoProgramacion mapeoProgramacion = new MapeoProgramacion();
				/*
				 * recojo mapeo operarios
				 */
				mapeoProgramacion.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeoProgramacion.setEjercicio(rsOpcion.getInt("pro_ej"));
				mapeoProgramacion.setSemana(rsOpcion.getString("pro_sem"));
				
				mapeoProgramacion.setTipo(rsOpcion.getString("pro_ope").toUpperCase());
				mapeoProgramacion.setCantidad(rsOpcion.getInt("pro_can"));
				mapeoProgramacion.setVelocidad(rsOpcion.getInt("pro_vel"));
				mapeoProgramacion.setVino(rsOpcion.getString("pro_vin"));
				mapeoProgramacion.setLote(rsOpcion.getString("pro_lot"));
				mapeoProgramacion.setOrden(rsOpcion.getInt("pro_ord"));
				mapeoProgramacion.setArticulo(rsOpcion.getString("pro_art"));
				mapeoProgramacion.setDescripcion(rsOpcion.getString("pro_des"));
				mapeoProgramacion.setCajas(rsOpcion.getInt("pro_caj"));
				mapeoProgramacion.setFactor(rsOpcion.getInt("pro_fac"));
				mapeoProgramacion.setUnidades(rsOpcion.getInt("pro_ud"));
				mapeoProgramacion.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeoProgramacion.setFrontales(rsOpcion.getString("pro_fro").toUpperCase());
				mapeoProgramacion.setContras(rsOpcion.getString("pro_con").toUpperCase());
				mapeoProgramacion.setCapsulas(rsOpcion.getString("pro_cap").toUpperCase());
				mapeoProgramacion.setTapones(rsOpcion.getString("pro_tap").toUpperCase());
				mapeoProgramacion.setCaja(rsOpcion.getString("pro_cj").toUpperCase());
				mapeoProgramacion.setEstado(rsOpcion.getString("pro_es"));
				mapeoProgramacion.setStatus(rsOpcion.getInt("pro_st"));
				if (rsOpcion.getString("pro_dia")!=null && rsOpcion.getString("pro_dia").trim().length()==0)
				{
					mapeoProgramacion.setDia(this.buscoDiaSemana(this.con, mapeoProgramacion));
				}
				else
				{
					mapeoProgramacion.setDia(rsOpcion.getString("pro_dia"));
				}
				vector.add(mapeoProgramacion);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	private String buscoDiaSemana(Connection r_con, MapeoProgramacion r_mapeo)
	{
		String diaSemana = null;
		StringBuffer cadenaSQL = new StringBuffer();
		ResultSet rsOpcion = null;
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id,");
		cadenaSQL.append(" prd_programacion_aux.ejercicio pro_ej,");
		cadenaSQL.append(" prd_programacion_aux.semana pro_sem,");
		cadenaSQL.append(" prd_programacion_aux.dia pro_dia,");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			
     		cadenaSQL.append(" where ejercicio = " + r_mapeo.getEjercicio());
     		cadenaSQL.append(" and semana = " + new Integer(r_mapeo.getSemana()) + " ");
     		cadenaSQL.append(" and orden <= " + r_mapeo.getOrden());
     		cadenaSQL.append(" and length(dia) > 0  ");
			cadenaSQL.append(" order by ejercicio, semana, orden desc");
			
			cs = r_con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				diaSemana = rsOpcion.getString("pro_dia");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return diaSemana;
	}
	public Double obtenerSumaProgramacion(String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion_aux.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and semana = " + new Integer(r_semana) + " ");

		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
					cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			if (this.con==null)
				this.con= this.conManager.establecerConexion();
			
			this.cs = this.con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= this.cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		
		return cantidad;		
	}
	
	public Double obtenerSumaProgramacion(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres, String r_tipoLinea)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion_aux.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and estado in ('A','N') ");
		cadenaSQL.append(" and semana >= " + new Integer(r_semana) + " ");
		if (r_tipoLinea=="EMBOTELLADO")	cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' " );
		if (r_tipoLinea=="ETIQUETADO") cadenaSQL.append(" and tipoOperacion = 'ETIQUETADO' " );
		
		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			Statement cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			ex.printStackTrace();
		}
		
		return cantidad;		
	}
	
	public Double obtenerSumaProgramacionAlmacen(Connection r_con, String r_ejercicio, String r_semana, ArrayList<String> r_vector_padres, String r_tipoLinea)
	{
		Double cantidad = null;
		ResultSet rsOpcion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_programacion_aux.unidades) pro_ud ");		
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and estado in ('A','N') ");
		cadenaSQL.append(" and semana = " + new Integer(r_semana) + " ");
		if (r_tipoLinea=="EMBOTELLADO")	cadenaSQL.append(" and tipoOperacion = 'EMBOTELLADO' " );
		if (r_tipoLinea=="ETIQUETADO") cadenaSQL.append(" and tipoOperacion = 'ETIQUETADO' " );
		
		try
		{
			cantidad = new Double(0);
			
			if (r_vector_padres!=null && !r_vector_padres.isEmpty())
			{
				StringBuffer cadenaWhere = new StringBuffer();
				
				
				for (int i = 0;i<r_vector_padres.size();i++)
				{
					if (r_vector_padres.get(i).trim().length()>0)
					{
						if (cadenaWhere.length()!=0) cadenaWhere=cadenaWhere.append(",");
					
						cadenaWhere=cadenaWhere.append("'" + r_vector_padres.get(i).trim() + "'");
					}
	     		}
	     		if (!cadenaWhere.equals("")) cadenaSQL.append(" and articulo in (" + cadenaWhere.toString() + ") ");
			}
			
			Statement cs = r_con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				cantidad = cantidad + rsOpcion.getInt("pro_ud");
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		
		return cantidad;		
	}
	
	public String generarInforme(MapeoProgramacion r_mapeo, String r_lote, String r_tipo, Integer r_cajas, String r_anada, String r_cantidad, boolean r_paletCompleto)
	{
		boolean generado = false;
		String informe = "";
		
		consultaEtiquetasEmbotelladoServer ces = consultaEtiquetasEmbotelladoServer.getInstance(CurrentUser.get());
		generado = ces.generarDatosEtiquetas(r_mapeo, r_lote, r_tipo, r_cajas, r_anada,r_cantidad, r_paletCompleto);
		
		if (generado) informe = ces.generarInforme(r_mapeo.getIdProgramacion());
		
		
		return informe;
		
	}

	public String imprimirProgramacion(String r_ejercicio, String r_semana, String r_formato)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio);
		libImpresion.setCodigo2Txt(r_semana);
		libImpresion.setArchivoDefinitivo("/programacionAux" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("ListadoProgramacionNormalAux.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("programacion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public String imprimirProgramacion(String r_valores)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_valores);
		libImpresion.setArchivoDefinitivo("/programacionAux" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		libImpresion.setArchivoPlantilla("ListadoProgramacionSelectorAux.jrxml");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzulH.jpg");
		
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("programacion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion_aux.idprd_programacion) pro_sig ");
     	cadenaSQL.append(" FROM prd_programacion_aux ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}


	public Integer obtenerOrden(Integer r_ejercicio, String r_semana)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_programacion_aux.orden) pro_ord ");
     	cadenaSQL.append(" FROM prd_programacion_aux ");
     	cadenaSQL.append(" WHERE ejercicio =  " + r_ejercicio);
     	cadenaSQL.append(" AND semana = " + new Integer(r_semana) );
     	
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_ord") + 10 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 10;
	}

	public String guardarNuevo(MapeoProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			Integer permisos = new Integer(this.comprobarAccesos());		    
			if (permisos<99 && permisos>70 ) 
				r_mapeo.setEstado("P");
			else 
				if (r_mapeo.getNuevo().equals("S")) r_mapeo.setEstado("N");
				else
				r_mapeo.setEstado("A");
			
			cadenaSQL.append(" INSERT INTO prd_programacion_aux( ");
			cadenaSQL.append(" prd_programacion_aux.idprd_programacion,");
			cadenaSQL.append(" prd_programacion_aux.ejercicio ,");
			cadenaSQL.append(" prd_programacion_aux.semana ,");
			cadenaSQL.append(" prd_programacion_aux.dia ,");
			cadenaSQL.append(" prd_programacion_aux.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion_aux.cantidad ,");
			cadenaSQL.append(" prd_programacion_aux.vino ,");
			cadenaSQL.append(" prd_programacion_aux.lote ,");
			cadenaSQL.append(" prd_programacion_aux.orden ,");
			cadenaSQL.append(" prd_programacion_aux.articulo ,");
			cadenaSQL.append(" prd_programacion_aux.descripcion ,");
			cadenaSQL.append(" prd_programacion_aux.cajas ,");
			cadenaSQL.append(" prd_programacion_aux.factor ,");
			cadenaSQL.append(" prd_programacion_aux.unidades ,");
			cadenaSQL.append(" prd_programacion_aux.observaciones ,");
			cadenaSQL.append(" prd_programacion_aux.frontales ,");
			cadenaSQL.append(" prd_programacion_aux.contras ,");
			cadenaSQL.append(" prd_programacion_aux.capsulas ,");
			cadenaSQL.append(" prd_programacion_aux.tapon ,");
			cadenaSQL.append(" prd_programacion_aux.caja, ");
			cadenaSQL.append(" prd_programacion_aux.estado, ");
			cadenaSQL.append(" prd_programacion_aux.status, ");
			cadenaSQL.append(" prd_programacion_aux.velocidad ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    

		    preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_mapeo.getSemana()));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getDia()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getContras()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getContras().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getCapsulas()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getCapsulas().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getTapones()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getTapones().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(21, null);
		    }
		    
		    preparedStatement.setInt(22, 0);
		    if (r_mapeo.getVelocidad()!=null)
		    {
		    	preparedStatement.setInt(23, r_mapeo.getVelocidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(23, 0);
		    }
		    
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String programarNuevoRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana, String r_sql)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());

		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion_aux( ");
			cadenaSQL.append(" prd_programacion_aux.idprd_programacion,");
			cadenaSQL.append(" prd_programacion_aux.ejercicio ,");
			cadenaSQL.append(" prd_programacion_aux.semana ,");
			cadenaSQL.append(" prd_programacion_aux.dia ,");
			cadenaSQL.append(" prd_programacion_aux.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion_aux.cantidad ,");
			cadenaSQL.append(" prd_programacion_aux.vino ,");
			cadenaSQL.append(" prd_programacion_aux.lote ,");
			cadenaSQL.append(" prd_programacion_aux.orden ,");
			cadenaSQL.append(" prd_programacion_aux.articulo ,");
			cadenaSQL.append(" prd_programacion_aux.descripcion ,");
			cadenaSQL.append(" prd_programacion_aux.cajas ,");
			cadenaSQL.append(" prd_programacion_aux.factor ,");
			cadenaSQL.append(" prd_programacion_aux.unidades ,");
			cadenaSQL.append(" prd_programacion_aux.observaciones ,");
			cadenaSQL.append(" prd_programacion_aux.frontales ,");
			cadenaSQL.append(" prd_programacion_aux.contras ,");
			cadenaSQL.append(" prd_programacion_aux.capsulas ,");
			cadenaSQL.append(" prd_programacion_aux.tapon ,");
			cadenaSQL.append(" prd_programacion_aux.caja, ");
			cadenaSQL.append(" prd_programacion_aux.estado, ");
			cadenaSQL.append(" prd_programacion_aux.status ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer idNuevo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idNuevo);		    
		    if (r_ejercicio!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_ejercicio));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_semana!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_semana));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    //dia
		    preparedStatement.setString(4, null);

		    //tipo y cantidad 
		    preparedStatement.setString(5, "RETRABAJO");
		    preparedStatement.setInt(6, 0);
		    //VINO, LOTE
		    preparedStatement.setString(7, null);
		    preparedStatement.setString(8, null);
		    
		    preparedStatement.setInt(9, this.obtenerOrden(new Integer(r_ejercicio), r_semana));
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    //Frontales,Contras, Capsulas, Tapones, Cajas, Estado
		    preparedStatement.setString(16, "SI");
		    preparedStatement.setString(17, "SI");
		    preparedStatement.setString(18, "SI");
		    preparedStatement.setString(19, "SI");
		    preparedStatement.setString(20, "SI");
		    preparedStatement.setString(21, "A");
		    
		    preparedStatement.setInt(22, 0);
		    
	        preparedStatement.executeUpdate();
	        r_mapeo.setIdProgramacion(idNuevo);
	        rdo = crs.asociarProgramacion(r_mapeo);
	        
        	if (r_sql!=null && r_sql.length()>0 )
        	{
        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
        		cps.actualizarPedidoProgramado(idNuevo, r_sql);
        	}

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        

		return rdo;
		
	}
	
	public String programarDesdeNecesidades(MapeoSituacionEmbotellado r_mapeo,String r_ejercicio,String r_semana, String r_sql, Integer r_velocidad)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();

		try
		{
			cadenaSQL.append(" INSERT INTO prd_programacion_aux( ");
			cadenaSQL.append(" prd_programacion_aux.idprd_programacion,");
			cadenaSQL.append(" prd_programacion_aux.ejercicio ,");
			cadenaSQL.append(" prd_programacion_aux.semana ,");
			cadenaSQL.append(" prd_programacion_aux.dia ,");
			cadenaSQL.append(" prd_programacion_aux.tipoOperacion ,");
			cadenaSQL.append(" prd_programacion_aux.cantidad ,");
			cadenaSQL.append(" prd_programacion_aux.vino ,");
			cadenaSQL.append(" prd_programacion_aux.lote ,");
			cadenaSQL.append(" prd_programacion_aux.orden ,");
			cadenaSQL.append(" prd_programacion_aux.articulo ,");
			cadenaSQL.append(" prd_programacion_aux.descripcion ,");
			cadenaSQL.append(" prd_programacion_aux.cajas ,");
			cadenaSQL.append(" prd_programacion_aux.factor ,");
			cadenaSQL.append(" prd_programacion_aux.unidades ,");
			cadenaSQL.append(" prd_programacion_aux.observaciones ,");
			cadenaSQL.append(" prd_programacion_aux.frontales ,");
			cadenaSQL.append(" prd_programacion_aux.contras ,");
			cadenaSQL.append(" prd_programacion_aux.capsulas ,");
			cadenaSQL.append(" prd_programacion_aux.tapon ,");
			cadenaSQL.append(" prd_programacion_aux.caja, ");
			cadenaSQL.append(" prd_programacion_aux.estado, ");
			cadenaSQL.append(" prd_programacion_aux.status, ");
			cadenaSQL.append(" prd_programacion_aux.velocidad ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer idNuevo = this.obtenerSiguiente();
		    
		    preparedStatement.setInt(1, idNuevo);		    
		    if (r_ejercicio!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_ejercicio));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_semana!=null)
		    {
		    	preparedStatement.setInt(3, new Integer(r_semana));
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
		    //dia
		    preparedStatement.setString(4, null);

		    //tipo y cantidad
		    
		    preparedStatement.setString(5, r_mapeo.getOperacion());
		    
		    preparedStatement.setInt(6, 0);
		    //VINO, LOTE
		    preparedStatement.setString(7, null);
		    preparedStatement.setString(8, null);
		    
		    preparedStatement.setInt(9, this.obtenerOrden(new Integer(r_ejercicio), r_semana)+10);
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(11, null);
		    }
		    
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getStock_real()!=null)
		    {
		    	preparedStatement.setInt(14, r_mapeo.getStock_real());
		    }
		    else
		    {
		    	preparedStatement.setInt(14, 0);
		    }
		    //observaciones
		    
		    preparedStatement.setString(15, r_mapeo.getObservaciones());
		    
		    //Frontales,Contras, Capsulas, Tapones, Cajas, Estado
		    preparedStatement.setString(16, "SI");
		    preparedStatement.setString(17, "SI");
		    preparedStatement.setString(18, "SI");
		    preparedStatement.setString(19, "SI");
		    preparedStatement.setString(20, "SI");
		    preparedStatement.setString(21, "A");

		    
		    preparedStatement.setInt(22, 0);
		    preparedStatement.setInt(23, r_velocidad);
		    
	        preparedStatement.executeUpdate();
	        
	        
	        if (r_mapeo.getPdte_servir()!=null && r_mapeo.getPdte_servir()!=0)
	        {
	        	/*
	        	 * Actualizamos los pedidos de greensys de este articulo al haberlos programado
	        	 */
	        	if (r_sql!=null && r_sql.length()>0 )
	        	{
	        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
	        		cps.actualizarPedidoProgramado(idNuevo, r_sql);
	        	}
	        }
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        

		return rdo;
		
	}
	
	public String asignarRetrabajo(MapeoRetrabajos r_mapeo,String r_ejercicio,String r_semana)
	{
		String rdo = null;
		consultaRetrabajosServer crs = consultaRetrabajosServer.getInstance(CurrentUser.get());

		try
		{
	        rdo = crs.asociarProgramacion(r_mapeo);
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	rdo = ex.getMessage();
	    }        

		return rdo;
		
	}
	public String guardarCambios(MapeoProgramacion r_mapeo)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion_aux set ");
			cadenaSQL.append(" prd_programacion_aux.ejercicio =?,");
			cadenaSQL.append(" prd_programacion_aux.semana =?,");
			cadenaSQL.append(" prd_programacion_aux.dia =?,");
			cadenaSQL.append(" prd_programacion_aux.tipoOperacion =?,");
			cadenaSQL.append(" prd_programacion_aux.cantidad =?,");
			cadenaSQL.append(" prd_programacion_aux.vino =?,");
			cadenaSQL.append(" prd_programacion_aux.lote =?,");
			cadenaSQL.append(" prd_programacion_aux.orden =?,");
			cadenaSQL.append(" prd_programacion_aux.articulo =?,");
			cadenaSQL.append(" prd_programacion_aux.descripcion =?,");
			cadenaSQL.append(" prd_programacion_aux.cajas =?,");
			cadenaSQL.append(" prd_programacion_aux.factor =?,");
			cadenaSQL.append(" prd_programacion_aux.unidades =?,");
			cadenaSQL.append(" prd_programacion_aux.observaciones =?,");
			cadenaSQL.append(" prd_programacion_aux.frontales =?,");
			cadenaSQL.append(" prd_programacion_aux.contras =?,");
			cadenaSQL.append(" prd_programacion_aux.capsulas =?,");
			cadenaSQL.append(" prd_programacion_aux.tapon =?,");
			cadenaSQL.append(" prd_programacion_aux.caja = ?, ");
			cadenaSQL.append(" prd_programacion_aux.estado = ?, ");
			cadenaSQL.append(" prd_programacion_aux.status = ?, ");
			cadenaSQL.append(" prd_programacion_aux.velocidad = ? ");
			cadenaSQL.append(" where prd_programacion_aux.idprd_programacion = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setInt(2, new Integer(r_mapeo.getSemana()));
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getDia()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDia());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getTipo().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getVino()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getVino());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getLote()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getLote());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getOrden()!=null)
		    {
		    	preparedStatement.setInt(8, r_mapeo.getOrden());
		    }
		    else
		    {
		    	preparedStatement.setInt(8, 0);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getCajas()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getCajas());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(12, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(12, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getFrontales()!=null)
		    {
		    	preparedStatement.setString(15, r_mapeo.getFrontales().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(15, null);
		    }
		    if (r_mapeo.getContras()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getContras().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getCapsulas()!=null)
		    {
		    	preparedStatement.setString(17, r_mapeo.getCapsulas().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(17, null);
		    }
		    if (r_mapeo.getTapones()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getTapones().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getCaja()!=null)
		    {
		    	preparedStatement.setString(19, r_mapeo.getCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(19, null);
		    }
		    if (r_mapeo.getEstado()!=null)
		    {
		    	preparedStatement.setString(20, r_mapeo.getEstado());
		    }
		    else
		    {
		    	preparedStatement.setString(20, null);
		    }
		    if (r_mapeo.getStatus()!=null)
		    {
		    	preparedStatement.setInt(21, r_mapeo.getStatus()+1);
		    }
		    else
		    {
		    	preparedStatement.setInt(21, 0);
		    }
		    if (r_mapeo.getVelocidad()!=null)
		    {
		    	preparedStatement.setInt(22, r_mapeo.getVelocidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(22, 0);
		    }
		    preparedStatement.setInt(23, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()) || statusActual==null)
    		{
		    	preparedStatement.executeUpdate();
		    	
		    	if (r_mapeo.getEstado().contentEquals("T"))
		    	{
		    		/*
		    		 * TODO: Liberar Pedido
		    		 * Liberar el pedido de greensys
		    		 * poniendo el orden_fabric a nulos
		    		 * 
		    		 */
		    		consultaPedidosVentasServer cpvs = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		    		cpvs.liberarPedidoProgramado(r_mapeo.getIdProgramacion());
		    	}
		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		return rdo;
	}

	public String actualizarProgramacion(MapeoProgramacion r_mapeo, String r_sql)
	{
		String rdo = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_programacion_aux set ");
		    cadenaSQL.append(" prd_programacion_aux.cantidad = ?, ");
		    cadenaSQL.append(" prd_programacion_aux.unidades = ?, ");
			cadenaSQL.append(" prd_programacion_aux.observaciones = ? ");
			cadenaSQL.append(" where prd_programacion_aux.idprd_programacion = ? ");
			
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    preparedStatement.setInt(1, r_mapeo.getCantidad());
		    preparedStatement.setInt(2, r_mapeo.getUnidades());
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

		    preparedStatement.setInt(4, r_mapeo.getIdProgramacion());
		    
		    Integer statusActual = this.obtenerStatus(r_mapeo.getIdProgramacion());
		    
		    if (statusActual.equals(r_mapeo.getStatus()))
    		{
		    	preparedStatement.executeUpdate();
	        	if (r_sql!=null && r_sql.length()>0 )
	        	{
	        		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
	        		cps.actualizarPedidoProgramado(r_mapeo.getIdProgramacion(), r_sql);
	        	}

		    	rdo=null;
    		}
		    else
		    {
		    	rdo="Registro modificado por otro usuario. Actualiza los datos y prueba de nuevo";
		    }
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
		return rdo;
	}

	public void eliminar(MapeoProgramacion r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_programacion_aux ");            
			cadenaSQL.append(" WHERE prd_programacion_aux.idprd_programacion = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdProgramacion());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	

	/*
	 * Devuelve el nivel de acceso a programaciones del usuario
	 * Recibe como parametro el nombre del usuario con el que buscamos a su operario equivalente
	 * 
	 * Devuelve:  0 - Sin permisos
	 * 			 10 - Laboratorio
	 * 			 20 - Solo Consulta
	 * 			 30 - Papeles 
	 * 			 40 - Completar
	 * 			 50 - Cambio Observaciones
	 * 			 70 - Cambio observaciones y materia seca
	 * 			 80 - Cambio observaciones y materia seca y Creacion provisional
	 *  			 
	 * 			 99 - Acceso total 
	 */
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Embotelladora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("BIB");
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="0";
				}
			}
		}
		return permisos;
	}
	
	public Integer obtenerStatus(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.status pro_st ");
     	cadenaSQL.append(" FROM prd_programacion_aux ");
     	cadenaSQL.append(" where prd_programacion_aux.idprd_programacion = " + r_id);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_st");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public String buscarLote (MapeoProgramacion r_mapeo)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.lote pro_lot ");
		cadenaSQL.append(" from prd_programacion_aux ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio() + " ");
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + new Integer(r_mapeo.getSemana()));
				}
				if (r_mapeo.getTipo()!=null && r_mapeo.getTipo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" tipoOperacion = '" + r_mapeo.getTipo() + "' ");
				}
				if (r_mapeo.getOrden()!=null && r_mapeo.getOrden().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" orden < " + r_mapeo.getOrden() + " ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by orden desc ");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("pro_lot")!=null && rsOpcion.getString("pro_lot").length()>0)
				{
					return rsOpcion.getString("pro_lot").trim();
					
				}
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}		
		return null;
	}

	public boolean comprobarProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id ");
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where prd_programacion_aux.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion_aux.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion_aux.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public boolean comprobarExisteReal(String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_programacion.idprd_programacion pro_id ");
		cadenaSQL.append(" from prd_programacion ");
		cadenaSQL.append(" where prd_programacion.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion.semana= '" + r_semana + "' ");

		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}

	public MapeoProgramacion recogerProgramado(String r_articulo, String r_ejercicio, String r_semana)
	{
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		MapeoProgramacion mapeo = null;
		
		cadenaSQL.append(" SELECT prd_programacion_aux.idprd_programacion pro_id, ");
		cadenaSQL.append(" prd_programacion_aux.cantidad pro_can, ");
		cadenaSQL.append(" prd_programacion_aux.unidades pro_uds, ");
		cadenaSQL.append(" prd_programacion_aux.observaciones pro_obs, ");
		cadenaSQL.append(" prd_programacion_aux.status pro_st ");
		
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where prd_programacion_aux.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_programacion_aux.semana= '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion_aux.articulo = '" + r_articulo + "' ");

		try
		{
			cadenaSQL.append(" order by ejercicio,semana, orden asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			mapeo=new MapeoProgramacion();
			
			while(rsOpcion.next())
			{
				mapeo.setIdProgramacion(rsOpcion.getInt("pro_id"));
				mapeo.setCantidad(rsOpcion.getInt("pro_can"));
				mapeo.setUnidades(rsOpcion.getInt("pro_uds"));
				mapeo.setObservaciones(rsOpcion.getString("pro_obs"));
				mapeo.setStatus(rsOpcion.getInt("pro_st"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	private String obtenerDia(Integer r_ejercicio, String r_semana, Integer r_orden)
	{
	
		String dia = "";
		ResultSet rsOpcion = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_programacion_aux.dia pro_dia, ");
		cadenaSQL.append(" prd_programacion_aux.orden pro_ord ");
		cadenaSQL.append(" from prd_programacion_aux ");
		cadenaSQL.append(" where prd_programacion_aux.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_programacion_aux.semana = '" + r_semana + "' ");
		cadenaSQL.append(" and prd_programacion_aux.orden <= " + r_orden);
		cadenaSQL.append(" and (prd_programacion_aux.dia is not null and prd_programacion_aux.dia<>'') ");
		cadenaSQL.append(" order by prd_programacion_aux.orden desc ");
		
		try
		{
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				dia = rsOpcion.getString("pro_dia");
				break;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return dia;
	}
	

	public HashMap<String, String> hayObservacionesCalidad(ArrayList<MapeoProgramacion> r_mapeo)
	{
		boolean hayObservaciones = false;
		HashMap<String, String> obs = null;
		ArrayList<MapeoLineasEscandallo> vector = null;
		String art=null;
		File fl =null;
		obs = new HashMap<String, String>();
		
		if (r_mapeo!=null)
		{
			for (int j=0; j<r_mapeo.size();j++)
			{
				
				MapeoProgramacion mapeo = r_mapeo.get(j);
				if (mapeo.getArticulo()!=null)
				{
	
					art = mapeo.getArticulo().trim();
				
					if (art.contentEquals("0102100"))
					{
						System.out.println("Estamos");
					}
					fl =new File(LecturaProperties.basePdfPath + "/calidad/pt/" + art.trim() + ".pdf");
			    	
			    	if (fl.exists())
			    	{
			    		obs.put(art,"S");
			    	}
			    	else
			    	{
						consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
						vector = ces.recuperarModificacionesEscandalloInterno(art);
						String valor= "N";
						for (int i = 0; i<vector.size();i++)
						{
							MapeoLineasEscandallo map = (MapeoLineasEscandallo) vector.get(i);
							fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + map.getArticulo().trim() + ".pdf");
							if (fl.exists())
							{
								valor="S";
								break;
							}
						}
						obs.put(art,valor);
			    	}
				}
			}
		}
		return obs;
	}

	public boolean hayObservacionesCalidad(String r_articulo)
	{
		boolean hayObservaciones = false;
		ArrayList<MapeoLineasEscandallo> vector = null;
		
		File fl =new File(LecturaProperties.basePdfPath + "/calidad/pt/" + r_articulo.trim() + ".pdf");
    	
    	if (fl.exists())
    	{
    		hayObservaciones = true;
    	}
    	else
    	{
			consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
			vector = ces.recuperarModificacionesEscandalloInterno(r_articulo);
			
			for (int i = 0; i<vector.size();i++)
			{
				MapeoLineasEscandallo mapeo = (MapeoLineasEscandallo) vector.get(i);
				fl =new File(LecturaProperties.basePdfPath + "/calidad/mp/" + mapeo.getArticulo().trim() + ".pdf");
				if (fl.exists())
				{
					hayObservaciones= true;
					break;
				}
			}
    	}
		return hayObservaciones;
	}
}
