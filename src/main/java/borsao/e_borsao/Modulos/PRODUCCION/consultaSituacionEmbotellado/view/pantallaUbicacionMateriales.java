package borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view;

import java.util.ArrayList;
import java.util.Date;

import com.github.wolfie.refresher.Refresher;
import com.github.wolfie.refresher.Refresher.RefreshListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo.ubicacionMaterialesGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class pantallaUbicacionMateriales extends Window 
{
	public ubicacionMaterialesGrid grid2 = null;
	public ArrayList<String> listaOrdenes = null;
	private ChatRefreshListener cr = null;
	private int intervaloRefresco=3*60*1000;
	
	private VerticalLayout principal = null;
	private HorizontalLayout central = null;
	public TextField txtSemana = null;
	public TextField txtEjercicio= null;
	public TextField txtOf= null;
	public TextField txtArticulo= null;
	public Label lblDescripcion= null;
	private Button btnBotonCVentana = null;
	private Button btnBotonRefresh = null;
	
	private final String titulo = "Explosión Materiales Lineas Programacion";
	private boolean hayGrid2 = false;
	public String area= null;
	public String articulo= null;
	public String loteJaulon= null;
	public String tipoOrden= null;
	public Long ordenFabricacion= null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */    
    public pantallaUbicacionMateriales(ArrayList<String> r_ordenes, String r_articulo, String r_descripcion, String r_ejercicio, String r_semana, String r_area, String r_loteJaulon, Long r_ordenFabricacion, String r_tipo)
    {
    	this.listaOrdenes=r_ordenes;
    	this.loteJaulon = r_loteJaulon;
    	this.ordenFabricacion = r_ordenFabricacion;
    	this.area=r_area;
    	this.tipoOrden = r_tipo;
    	if (this.tipoOrden.contentEquals("ETIQUETADO"))
    	{
    		this.articulo = r_articulo + "-1";
    	}
    	else
    	{
    		this.articulo = r_articulo ;
    	}
    	
    	this.center();
    	this.setModal(true);
    	this.setClosable(true);
    	this.setResizable(true);
    	this.setWidth("1150px");
    	this.setHeight("575px");
    	this.setCaption(this.titulo);

    	principal = new VerticalLayout();
    	principal.setSizeFull();
    	principal.setMargin(true);
    	
	    	HorizontalLayout cabecera = new HorizontalLayout();
			cabecera.setSpacing(true);
	
				this.txtEjercicio=new TextField("Ejercicio");
				this.txtEjercicio.setEnabled(false);
				this.txtEjercicio.setValue(r_ejercicio);
				
				this.txtSemana= new TextField("Semana");    		
				this.txtSemana.setEnabled(false);
				this.txtSemana.setValue(r_semana);

				this.txtOf= new TextField("Orden Produccion");    		
				this.txtOf.setWidth("120px");    		
				this.txtOf.setEnabled(false);
				this.txtOf.setValue(r_ordenFabricacion.toString());
				
				this.txtArticulo= new TextField("Articulo");    		
				this.txtArticulo.setWidth("120px");    		
				this.txtArticulo.setEnabled(false);
				this.txtArticulo.setValue(r_articulo);
				
				this.lblDescripcion= new Label("Descripcion");    		
				this.lblDescripcion.setValue(r_descripcion);
				
				btnBotonRefresh = new Button("Actualizar");
				btnBotonRefresh.setIcon(FontAwesome.REFRESH);
//				btnBotonRefresh.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonRefresh.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
//			cabecera.addComponent(this.txtEjercicio);	
//			cabecera.addComponent(this.txtSemana);
			cabecera.addComponent(this.txtOf);
			cabecera.addComponent(this.txtArticulo);
			cabecera.addComponent(this.lblDescripcion);
			cabecera.setComponentAlignment(this.lblDescripcion, Alignment.BOTTOM_LEFT);
			cabecera.addComponent(this.btnBotonRefresh);
			cabecera.setComponentAlignment(this.btnBotonRefresh, Alignment.BOTTOM_RIGHT);
			
			central = new HorizontalLayout();
			central.setSizeFull();
			central.setSpacing(true);
	
			HorizontalLayout botonera = new HorizontalLayout();
			botonera.setSpacing(true);
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
			botonera.addComponent(btnBotonCVentana);		
			botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
			
		principal.addComponent(cabecera);
		principal.addComponent(central);
		principal.addComponent(botonera);

		if (this.area.toUpperCase().contentEquals("BOTELLA"))
			this.generarGrid("4");
		else
			this.generarGrid("1");
			
		this.cargarListeners();
		this.setContent(principal);
    }

    public void generarGrid(String r_almacenMateriaPrima)
    {
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	
    	consultaAyudaProduccionServer caps  = new consultaAyudaProduccionServer(CurrentUser.get());
    	
    	if (articulo!=null && articulo.length()>0)
    	{
    		r_vector2=caps.datosEscandalloAlmacen(articulo, this.txtEjercicio.getValue(), this.txtSemana.getValue(), r_almacenMateriaPrima, area, this.txtOf.getValue());
    	}
    	else
    	{
        	r_vector2 = new ArrayList<MapeoAyudaProduccionEscandallo>();
    	}
    	
    	this.presentarGrid(r_vector2);
    }
    
    private void presentarGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
    {
    	
    	if (isHayGrid2())
		{			
			grid2.removeAllColumns();			
			central.removeComponent(grid2);
			grid2=null;			
		}
		grid2 = new ubicacionMaterialesGrid(r_vector2, area,this.txtEjercicio.getValue().toString());
		if (((ubicacionMaterialesGrid) grid2).vector==null)
    	{
    		setHayGrid2(false);
    	}
    	else if (((ubicacionMaterialesGrid) grid2).vector.isEmpty())
    	{
    		setHayGrid2(true);
    	}
    	else
    	{
    		setHayGrid2(true);
    	}
		
		if (isHayGrid2())
		{
			central.addComponent(grid2);
			this.principal.setExpandRatio(central, 1);
//    		central.setExpandRatio(grid2,1 );
		}
    }
    
    private void cargarListeners()
    {
    	this.ejecutarTimer(this.intervaloRefresco);
    	
    	btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

    	btnBotonRefresh.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (area.toUpperCase().contentEquals("BOTELLA"))
    				generarGrid("4");
    			else
    				generarGrid("1");
    		}
    	});
    }
    

    public void print() 
    {
    }
    
    public boolean isHayGrid2() {
    	return hayGrid2;
    }
    
    public void setHayGrid2(boolean hayGrid2) {
    	this.hayGrid2 = hayGrid2;
    }

    private void ejecutarTimer(int r_intervaloRefresco)
    {
        Refresher REFRESHER = new Refresher();
        REFRESHER.setRefreshInterval(r_intervaloRefresco);
        cr = new ChatRefreshListener();
        REFRESHER.addListener(cr);
        addExtension(REFRESHER);
    }

	private class ChatRefreshListener implements RefreshListener {
    	@Override
    	public void refresh(final Refresher source) {
    		Notificaciones.getInstance().mensajeSeguimiento("pantalla ubicacion materiales referesco: " + new Date());
			if (area.toUpperCase().contentEquals("BOTELLA"))
				generarGrid("4");
			else
				generarGrid("1");
    	}
    }

	public void destructor()
	{
	}
}
