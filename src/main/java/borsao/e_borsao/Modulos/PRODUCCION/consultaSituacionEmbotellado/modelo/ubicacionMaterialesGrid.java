package borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ubicacionMaterialesGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private String area = null;
	private Integer ejercicio = null;
	
    public ubicacionMaterialesGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector, String r_area) 
    {
        this.vector=r_vector;
        this.area =r_area;
       
		this.asignarTitulo("");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public ubicacionMaterialesGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector, String r_area, String r_ejercicio) 
    {
    	this.vector=r_vector;
    	this.area =r_area;
    	this.ejercicio = new Integer(r_ejercicio);
    	 
    	this.asignarTitulo("");
    	
    	
    	if (this.vector==null || this.vector.size()==0)
    	{
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
    	}
    	else
    	{
    		this.generarGrid();
    	}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoAyudaProduccionEscandallo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	
    	setColumnOrder("nombreAlmacen", "articulo", "descripcion","existencias", "pendienteRecibir", "necesito", "confirmado", "ubicacion");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombreAlmacen").setHeaderCaption("Almacen");
    	this.getColumn("nombreAlmacen").setSortable(false);
    	this.getColumn("nombreAlmacen").setWidth(new Double(180));
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("existencias").setHeaderCaption("Existencias");
    	this.getColumn("existencias").setWidth(100);
    	this.getColumn("pendienteRecibir").setHeaderCaption("Pdte. Recibir");
    	this.getColumn("pendienteRecibir").setWidth(100);
    	this.getColumn("necesito").setHeaderCaption("Necesidad");
    	this.getColumn("necesito").setWidth(100);
    	this.getColumn("confirmado").setHeaderCaption("LPN");
    	this.getColumn("confirmado").setWidth(200);
    	this.getColumn("ubicacion").setHeaderCaption("Ubicacion");
    	this.getColumn("ubicacion").setWidth(120);
    	this.getColumn("faltansobran").setHeaderCaption("WIP-IN");
    	this.getColumn("faltansobran").setWidth(120);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("existencias3").setHidden(true);
    	this.getColumn("articuloNC").setHidden(true);
    	this.getColumn("factor").setHidden(true);
    	this.getColumn("coste").setHidden(true);
//    	this.getColumn("faltansobran").setHidden(true);
    	this.getColumn("fechaEntrega").setHidden(true);
    	this.getColumn("nombreAlmacen").setHidden(true);
    	this.getColumn("existencias").setHidden(true);
    	this.getColumn("pendienteRecibir").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
    		Integer exis = null;
    		Integer nece = null;
    		Integer pdte = null;
    		boolean pt = false;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("articulo".equals(cellReference.getPropertyId()))
            	{
            		if (((String) cellReference.getValue()).startsWith("0102"))
            		{
            			pt=true;
            		}
            		else
            		{
            			pt = false;
            		}
            	}
            	if ("necesito".equals(cellReference.getPropertyId()))
            	{
            		nece = (Integer) cellReference.getValue();
            	}
            	if ("existencias".equals(cellReference.getPropertyId()))
            	{
            		exis = (Integer) cellReference.getValue();
            	}
            	if ("pendienteRecibir".equals(cellReference.getPropertyId()))
            	{
            		pdte = (Integer) cellReference.getValue();
            	}
            	if ("necesito".equals(cellReference.getPropertyId())  || ("pendienteRecibir".equals(cellReference.getPropertyId())) && pdte.intValue()!=0)
            	{
            		return "Rcell-pointer";
            	}
            	else if ("pendienteRecibir".equals(cellReference.getPropertyId()) || "faltansobran".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-pointer";
            	}
            	else if ("existencias".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-pointer";
            	}
            	else if ("factor".equals(cellReference.getPropertyId()) || "coste".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ("articulo".equals(cellReference.getPropertyId())) 
            	{
        			return "cell-normal"; 
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
	
            }
    	});	
	}

	@Override
	public void calcularTotal() {
		
	}
}


