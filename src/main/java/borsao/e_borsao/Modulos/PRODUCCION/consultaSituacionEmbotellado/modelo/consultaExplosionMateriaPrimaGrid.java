package borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo;

import java.util.ArrayList;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.ALMACEN.consultaInventario.view.pantallaStocksLotes;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.view.pantallaLineasNoConformidades;
import borsao.e_borsao.Modulos.COMPRAS.PedidosCompras.view.pantallaLineasPedidosCompra;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class consultaExplosionMateriaPrimaGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	private String area = null;
	private Integer ejercicio = null;
	private boolean conSGA = false;
	
    public consultaExplosionMateriaPrimaGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector, String r_area) 
    {
        this.vector=r_vector;
        this.area =r_area;
       
		this.asignarTitulo("");
		

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public consultaExplosionMateriaPrimaGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector, String r_area, String r_ejercicio, boolean r_sga) 
    {
    	this.vector=r_vector;
    	this.area =r_area;
    	this.ejercicio = new Integer(r_ejercicio);
    	this.conSGA = r_sga;
    	 
    	this.asignarTitulo("");
    	
    	
    	if (this.vector==null || this.vector.size()==0)
    	{
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
    	}
    	else
    	{
    		this.generarGrid();
    	}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoAyudaProduccionEscandallo.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.NONE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	
    	setColumnOrder("nombreAlmacen", "articuloNC", "articulo", "descripcion","existencias3","existencias", "pendienteRecibir", "factor", "coste", "necesito", "faltansobran", "fechaEntrega");
    	
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombreAlmacen").setHeaderCaption("Almacen");
    	this.getColumn("nombreAlmacen").setSortable(false);
    	this.getColumn("nombreAlmacen").setWidth(new Double(200));
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("existencias").setHeaderCaption("Existencias");
    	this.getColumn("existencias").setWidth(100);
    	this.getColumn("pendienteRecibir").setHeaderCaption("Pdte. Recibir");
    	this.getColumn("pendienteRecibir").setWidth(100);
//    	this.getColumn("factor").setHeaderCaption("");
//    	this.getColumn("factor").setWidth(100);
//    	this.getColumn("coste").setHeaderCaption("");
//    	this.getColumn("coste").setWidth(100);
    	this.getColumn("necesito").setHeaderCaption("Necesidad");
    	this.getColumn("necesito").setWidth(100);
    	this.getColumn("faltansobran").setHeaderCaption("Falta / Sobra");
    	this.getColumn("faltansobran").setWidth(100);
    	this.getColumn("fechaEntrega").setHeaderCaption("Fecha Entrega");
    	this.getColumn("fechaEntrega").setWidth(150);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("almacen").setHidden(true);
    	this.getColumn("existencias3").setHidden(true);
    	this.getColumn("articuloNC").setHidden(true);
    	this.getColumn("factor").setHidden(true);
    	this.getColumn("coste").setHidden(true);
    	
    	 this.getColumn("ubicacion").setHidden(conSGA);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
    		Integer exis = null;
    		Integer nece = null;
    		Integer pdte = null;
    		boolean hay3 = false;
    		boolean hayNC = false;
    		boolean pt = false;
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("articuloNC".equals(cellReference.getPropertyId()))
            	{
            		hayNC = (boolean) cellReference.getValue();
            	}
            	if ("articulo".equals(cellReference.getPropertyId()))
            	{
            		if (((String) cellReference.getValue()).startsWith("0102"))
            		{
            			pt=true;
            		}
            		else
            		{
            			pt = false;
            		}
            	}
            	if ("necesito".equals(cellReference.getPropertyId()))
            	{
            		nece = (Integer) cellReference.getValue();
            	}
            	if ("existencias".equals(cellReference.getPropertyId()))
            	{
            		exis = (Integer) cellReference.getValue();
            	}
            	if ("pendienteRecibir".equals(cellReference.getPropertyId()))
            	{
            		pdte = (Integer) cellReference.getValue();
            	}
            	if ("existencias3".equals(cellReference.getPropertyId()))
            	{
            		hay3 = (boolean) cellReference.getValue();
            	}
            	if ("faltansobran".equals(cellReference.getPropertyId()))
        		{
        			String estilo = "";
        			if (exis + pdte < nece)
        			{
    					estilo = "Rcell-error";
        			}
        			else if (exis<nece && exis +  pdte > nece)
        			{
        				estilo = "Rcell-warning";
        			}
            		else
            		{
            			return "Rcell-normal";
            		}
        			if (pt) estilo = estilo + "P";
        			return estilo;
        		}

            	if ("necesito".equals(cellReference.getPropertyId())  || ("pendienteRecibir".equals(cellReference.getPropertyId())) && pdte.intValue()!=0)
            	{
            		return "Rcell-pointer";
            	}
            	else if ("pendienteRecibir".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-pointer";
            	}
            	else if ("existencias".equals(cellReference.getPropertyId()))
            	{
            		if (hay3) return "Rcell-pointer-green";
            		
            		return "Rcell-pointer";
            	}
            	else if ("factor".equals(cellReference.getPropertyId()) || "coste".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else if ("articulo".equals(cellReference.getPropertyId())) 
            	{
            		if (hayNC) return "cell-warningP"; 
            		
        			return "cell-normal"; 
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
	
				if (event.getPropertyId().toString().equals("faltansobran"))
	        	{
	        		/*
	        		 * acceso a ayuda produccion
	        		 */
					MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
//	        		MapeoSituacionEmbotellado mapeo = (MapeoSituacionEmbotellado) event.getItemId();
//	        		
					if (mapeo.getArticulo().startsWith("0102") && mapeo.getFaltansobran().intValue()<0)
					{
						pantallaAyudaProduccion vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getFaltansobran().toString(), Math.abs(mapeo.getFaltansobran().intValue()), mapeo.getArticulo());
						getUI().addWindow(vt);
					}
	        		
	        	}
				else if (event.getPropertyId().toString().equals("existencias"))
	        	{
	        		/*
	        		 * acceso a ayuda produccion
	        		 */
					MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
//	        		MapeoSituacionEmbotellado mapeo = (MapeoSituacionEmbotellado) event.getItemId();
//	        		
					pantallaStocksLotes vt = new pantallaStocksLotes("Existencias del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getArticulo(),null);
					getUI().addWindow(vt);
	        		
	        	}
	        	else if (event.getPropertyId().toString().equals("pendienteRecibir"))
	        	{
	        		/*
	        		 * acceso a pendientes de servir
	        		 */
	        		
	        		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
	        		if (mapeo.getPendienteRecibir()!=0)
	        		{
	        			pantallaLineasPedidosCompra vt = new pantallaLineasPedidosCompra(mapeo.getArticulo(), mapeo.getExistencias(), "Lineas Pedidos Compra del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
	        			getUI().addWindow(vt);
	        		}
	        		
	        	}
	        	else if (event.getPropertyId().toString().equals("necesito"))
	        	{
	        		ArrayList<String> vectorPadres = new ArrayList<String>();
            		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
            		
//            		if (mapeo.getNecesito()!=0)
            		{
	            		consultaEscandalloServer ces = consultaEscandalloServer.getInstance(CurrentUser.get());
	            		vectorPadres = ces.recuperarPadres(mapeo.getArticulo());
	            		            		
	            		pantallaLineasProgramaciones vt = new pantallaLineasProgramaciones(vectorPadres, "Lineas Programadas con la materia prima ... " + mapeo.getArticulo() + " " + mapeo.getDescripcion(), mapeo.getArticulo(), area, ejercicio);
	            		getUI().addWindow(vt);
            		}
	        	}
	        	else if (event.getPropertyId().toString().equals("articulo"))
	        	{
            		MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) event.getItemId();
            		
//            		if (mapeo.getNecesito()!=0)
            		{
	            		pantallaLineasNoConformidades vt = new pantallaLineasNoConformidades(mapeo.getArticulo(), "Lineas No Conformidades con la materia prima ... " + mapeo.getArticulo() + " " + mapeo.getDescripcion());
	            		getUI().addWindow(vt);
            		}
	        	}
            }
    	});	
	}

	@Override
	public void calcularTotal() {
		
	}
}


