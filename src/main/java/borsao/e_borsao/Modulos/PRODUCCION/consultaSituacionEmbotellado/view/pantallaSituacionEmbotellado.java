package borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo.consultaExplosionMateriaPrimaGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class pantallaSituacionEmbotellado extends Window 
{

	private final String titulo = "Explosión Materiales Lineas Programacion";
	
	public TextField txtSemana = null;
	public TextField txtEjercicio= null;
	public ComboBox cmbAlmacenes= null;
	public ArrayList<String> listaARticulos = null;
	public ArrayList<String> listaOrdenes = null;
	public consultaExplosionMateriaPrimaGrid grid2 = null;
	public CheckBox chkSGA = null;
	
	private VerticalLayout principal = null;
	private HorizontalLayout central = null;
	private Button btnBotonCVentana = null;
	private Button btnImprimir = null;
	private boolean hayGrid2 = false;
	public String area= null;
	public String loteJaulon= null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */    
    public pantallaSituacionEmbotellado(ArrayList<String> r_ordenes, ArrayList<String> r_articulos, String r_ejercicio, String r_semana, String r_area, String r_loteJaulon)
    {
    	this.listaARticulos=r_articulos;
    	this.listaOrdenes=r_ordenes;
    	this.loteJaulon = r_loteJaulon;
    	this.area=r_area;
    	
    	this.center();
    	this.setModal(true);
    	this.setClosable(true);
    	this.setResizable(true);
    	this.setWidth("1500px");
    	this.setHeight("750px");
    	this.setCaption(this.titulo);

    	principal = new VerticalLayout();
    	principal.setSizeFull();
    	principal.setMargin(true);
    	
	    	HorizontalLayout cabecera = new HorizontalLayout();
			cabecera.setSpacing(true);
	
				this.txtEjercicio=new TextField("Ejercicio");
				this.txtEjercicio.setEnabled(false);
				this.txtEjercicio.setValue(r_ejercicio);
				
				this.txtSemana= new TextField("Semana");    		
				this.txtSemana.setEnabled(false);
				this.txtSemana.setValue(r_semana);
				
				btnImprimir = new Button("Imprimir");
				btnImprimir.addStyleName(ValoTheme.BUTTON_PRIMARY);
				btnImprimir.setIcon(FontAwesome.PRINT);
				btnImprimir.setEnabled(false);
				
				this.chkSGA= new CheckBox("Trabajar con SGA");
				this.chkSGA.setValue(true);
				
				this.cmbAlmacenes = new ComboBox("Almacen Materia Prima");
	    		this.cmbAlmacenes.setNewItemsAllowed(false);
	    		this.cmbAlmacenes.setNullSelectionAllowed(false);
	    		this.cargarCombo();
	    		this.cmbAlmacenes.setValue("Todos");


			cabecera.addComponent(this.txtEjercicio);	
			cabecera.addComponent(this.txtSemana);
			cabecera.addComponent(this.cmbAlmacenes);
			cabecera.addComponent(this.btnImprimir);
			cabecera.setComponentAlignment(this.btnImprimir, Alignment.BOTTOM_CENTER);
			cabecera.addComponent(chkSGA);
			cabecera.setComponentAlignment(chkSGA, Alignment.MIDDLE_LEFT);
	
			central = new HorizontalLayout();
			central.setSizeFull();
			central.setSpacing(true);
	
			HorizontalLayout botonera = new HorizontalLayout();
			botonera.setSpacing(true);
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
			botonera.addComponent(btnBotonCVentana);		
			botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
			
		principal.addComponent(cabecera);
//		principal.setComponentAlignment(cabecera, Alignment.TOP_LEFT);
		principal.addComponent(central);
//		principal.setComponentAlignment(central, Alignment.TOP_LEFT);
		principal.addComponent(botonera);
//		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);

		this.generarGrid(this.cmbAlmacenes.getValue().toString());
		this.cargarListeners();
		
		this.setContent(principal);
    }

    public void generarGrid(String r_almacenMateriaPrima)
    {
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	
    	consultaAyudaProduccionServer caps  = new consultaAyudaProduccionServer(CurrentUser.get());
    	
    	if (listaARticulos!=null && listaARticulos.size()>0)
    	{
    		r_vector2=caps.datosEscandalloAlmacen(listaARticulos, this.txtEjercicio.getValue(), this.txtSemana.getValue(), true, r_almacenMateriaPrima, area);
    	}
    	else
    	{
        	r_vector2 = new ArrayList<MapeoAyudaProduccionEscandallo>();
    	}
    	
    	this.presentarGrid(r_vector2);
    }
    
    private void presentarGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
    {
    	
    	if (isHayGrid2())
		{			
			grid2.removeAllColumns();			
			central.removeComponent(grid2);
			grid2=null;			
		}
		grid2 = new consultaExplosionMateriaPrimaGrid(r_vector2, area,this.txtEjercicio.getValue().toString(),this.chkSGA.getValue());
		if (((consultaExplosionMateriaPrimaGrid) grid2).vector==null)
    	{
    		setHayGrid2(false);
    	}
    	else if (((consultaExplosionMateriaPrimaGrid) grid2).vector.isEmpty())
    	{
    		setHayGrid2(true);
    	}
    	else
    	{
    		setHayGrid2(true);
    	}
		
		if (isHayGrid2())
		{
			central.addComponent(grid2);
			this.principal.setExpandRatio(central, 1);
//    		central.setExpandRatio(grid2,1 );
		}
    }
    
    private void cargarListeners()
    {

    	this.cmbAlmacenes.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid2())
				{			
					grid2.removeAllColumns();			
					central.removeComponent(grid2);
					grid2=null;			
					setHayGrid2(false);
				};			
				if (cmbAlmacenes.getValue()!=null) generarGrid(cmbAlmacenes.getValue().toString());
			}
		}); 
    	btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

    	btnImprimir.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				print();
			}
		});

	
    }
    

    public void print() 
    {
		String pdfGenerado = null;
		
    	consultaControlEmbotelladoServer cps = new consultaControlEmbotelladoServer(CurrentUser.get());
    	
    	if (chkSGA.getValue())
    		pdfGenerado = cps.imprimirExplosionSGA(listaOrdenes,listaARticulos,loteJaulon);
    	else
    		pdfGenerado = cps.imprimirExplosion((ArrayList<MapeoAyudaProduccionEscandallo>)  ((consultaExplosionMateriaPrimaGrid) grid2).vector);
		
		if(pdfGenerado!=null && pdfGenerado.length()>0)
		{
			RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		}
		else
			Notificaciones.getInstance().mensajeInformativo("No hay datos a imrpimir.");
    }
    
    public boolean isHayGrid2() {
    	return hayGrid2;
    }
    
    public void setHayGrid2(boolean hayGrid2) {
    	this.hayGrid2 = hayGrid2;
    	btnImprimir.setEnabled(hayGrid2);
    }
    

    private void cargarCombo()
    {
    	this.cmbAlmacenes.addItem("Todos");
    	this.cmbAlmacenes.addItem("1");
    	this.cmbAlmacenes.addItem("4");
    }
    
	public void destructor()
	{
	}
}
