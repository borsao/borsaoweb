package borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.view;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaSituacionEmbotellado.modelo.consultaSituacionEmbotelladoGrid;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class pantallaControlProgramacionEnvasasdo extends Window 
{

	private final String titulo = "Control Programacion Envasasdo";
	
	public VerticalLayout barAndGridLayout = null;
	public HorizontalLayout cabLayout = null;
	public TextField txtEjercicio= null;
	public ComboBox cmbSemana = null;
	public String semana = null;
	public Grid grid2 = null;
	private boolean hayGrid2 = false;
	
	public consultaProgramacionServer cps =null;
	public consultaProgramacionEnvasadoraServer cpes =null;

	/*
     * METODOS PROPIOS PERO GENERICOS
     */    
    public pantallaControlProgramacionEnvasasdo(String r_titulo)
    {
    	
		this.setCaption(r_titulo);
		this.center();
		
    	this.setSizeFull();
    	this.addStyleName("crud-view");
    	this.setResponsive(true);

    	this.cargarPantalla();
		this.cargarListeners();

		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setSizeFull();
		
    	this.setContent(this.barAndGridLayout);
    	this.setResponsive(true);
    }

    private void cargarPantalla()
    {

    	this.barAndGridLayout = new VerticalLayout();
    	this.barAndGridLayout.setSpacing(false);
    	this.barAndGridLayout.setSizeFull();
    	this.barAndGridLayout.setStyleName("crud-main-layout");
    	this.barAndGridLayout.setResponsive(true);

    	this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);

		this.txtEjercicio=new TextField("Ejercicio");
		this.txtEjercicio.setEnabled(true);
		this.txtEjercicio.addStyleName(ValoTheme.TEXTFIELD_TINY);
		
		this.cmbSemana= new ComboBox("Semana");    		
		this.cmbSemana.setNewItemsAllowed(false);
		this.cmbSemana.setNullSelectionAllowed(false);
		this.cmbSemana.addStyleName(ValoTheme.COMBOBOX_TINY);

		this.cabLayout.addComponent(this.txtEjercicio);	
		this.cabLayout.addComponent(this.cmbSemana);

		this.barAndGridLayout.addComponent(this.cabLayout);

		this.cargarCombo(null);    		
		this.cmbSemana.setValue(this.semana);
		
		this.generarGrid();
		this.cargarListeners();

    }
    public void generarGrid()
    {
    	ArrayList<MapeoAyudaProduccionEscandallo> r_vector2 =null;
    	ArrayList<String> articulos = null;
    	ArrayList<MapeoProgramacionEnvasadora> datosProgramadosEnv = null;
    	
    	consultaAyudaProduccionServer caps  = new consultaAyudaProduccionServer(CurrentUser.get());
    	articulos = new ArrayList<String>();
    	
		cpes = new consultaProgramacionEnvasadoraServer(CurrentUser.get());
		datosProgramadosEnv = cpes.datosProgramacionGlobal(new Integer(this.txtEjercicio.getValue()),this.cmbSemana.getValue().toString());
		
		for (int i = 0; i<datosProgramadosEnv.size();i++)
		{
			String articulo = "";
			
			articulo=((MapeoProgramacionEnvasadora) datosProgramadosEnv.get(i)).getArticulo();
			
			if (!articulos.contains(articulo) && articulo.length()>0) articulos.add(articulo);    		
		}
		if (this.txtEjercicio.getValue()!=null && this.txtEjercicio.getValue().length()>0 && this.cmbSemana.getValue()!=null)
		{
			r_vector2=caps.datosEscandalloAlmacen(articulos, this.txtEjercicio.getValue(), this.cmbSemana.getValue().toString(), false,"1","Mesa");
		}
		else
		{
			r_vector2 = new ArrayList<MapeoAyudaProduccionEscandallo>();
		}
    	
    	this.presentarGrid(r_vector2);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoAyudaProduccionEscandallo> r_vector2)
    {
    	
    	if (isHayGrid2() && txtEjercicio.getValue()!=null)
		{			
			grid2.removeAllColumns();			
			barAndGridLayout.removeComponent(grid2);
			grid2=null;			
		}
		grid2 = new consultaSituacionEmbotelladoGrid(r_vector2,"Mesa",new Integer(this.txtEjercicio.getValue().toString()));
		if (((consultaSituacionEmbotelladoGrid) grid2).vector==null)
    	{
    		setHayGrid2(false);
    	}
    	else if (((consultaSituacionEmbotelladoGrid) grid2).vector.isEmpty())
    	{
    		setHayGrid2(true);
    	}
    	else
    	{
    		setHayGrid2(true);
    	}
		
		if (isHayGrid2())
		{
			barAndGridLayout.addComponent(grid2);
    		barAndGridLayout.setExpandRatio(grid2,3 );
    		barAndGridLayout.setMargin(true);
		}
    }
    
    private void cargarListeners()
    {

		this.cmbSemana.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid2() && txtEjercicio.getValue()!=null)
				{			
					grid2.removeAllColumns();			
					barAndGridLayout.removeComponent(grid2);
					grid2=null;	
					setHayGrid2(false);
				}
				generarGrid();
			}
		});
		this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid2() && txtEjercicio.getValue()!=null)
				{			
					grid2.removeAllColumns();			
					barAndGridLayout.removeComponent(grid2);
					grid2=null;
					setHayGrid2(false);
				}
				generarGrid();
			}
		});			
    }
    

    public void print() 
    {
    }
    
	public void destructor()
	{
	}
	
	private void cargarCombo(String r_ejercicio)
    {
    	if (r_ejercicio==null)
    	{
    		this.txtEjercicio.setValue(RutinasFechas.añoActualYYYY());
    	}
    	else
    	{
    		this.cmbSemana.removeAllItems();
    	}
    	
    	this.semana=String.valueOf(RutinasFechas.semanaActual(r_ejercicio));
    	int semanas = RutinasFechas.semanasAño(r_ejercicio);
    	
    	for (int i=1; i<=semanas; i++)
    	{
    		this.cmbSemana.addItem(String.valueOf(i));
    	}
    }
    public boolean isHayGrid2() {
    	return hayGrid2;
    }
    
    public void setHayGrid2(boolean hayGrid2) {
    	this.hayGrid2 = hayGrid2;
    }
    
}
