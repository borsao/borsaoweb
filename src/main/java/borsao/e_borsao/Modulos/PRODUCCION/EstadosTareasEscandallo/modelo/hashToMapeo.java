package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoEstadoTareasEscandallo mapeo = null;
	 
	 public MapeoEstadoTareasEscandallo convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoEstadoTareasEscandallo();

		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 if (r_hash.get("porc")!=null && r_hash.get("porc").length()>0) this.mapeo.setPorcentaje(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("porc"))));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoEstadoTareasEscandallo convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 this.mapeo = new MapeoEstadoTareasEscandallo();

		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 
		 if (r_hash.get("porc")!=null && r_hash.get("porc").length()>0) this.mapeo.setPorcentaje(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("porc"))));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoEstadoTareasEscandallo r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 
		 this.hash.put("porc", String.valueOf(this.mapeo.getPorcentaje()));

		 if (this.mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", String.valueOf(this.mapeo.getIdCodigo()));
		 
		 return hash;		 
	 }
}