package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoEstadoTareasEscandallo extends MapeoGlobal
{
	private Integer porcentaje;

	private String descripcion;
	
	
	public MapeoEstadoTareasEscandallo()
	{ 
	}


	public Integer getPorcentaje() {
//		if (porcentaje==999) porcentaje = null;
		return porcentaje;
	}


	public void setPorcentaje(Integer porcentaje) {
		this.porcentaje = porcentaje;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}