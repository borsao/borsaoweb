package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.EstadoTareasEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.MapeoEstadoTareasEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.server.consultaEstadoTareasEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoEstadoTareasEscandallo mapeo = null;
	 public MapeoEstadoTareasEscandallo mapeoModificado = null;
	 
	 private consultaEstadoTareasEscandalloServer cps = null;
	 private EstadosTareasEscandalloView uw = null;
	 private BeanFieldGroup<MapeoEstadoTareasEscandallo> fieldGroup;
	 
	 public OpcionesForm(EstadosTareasEscandalloView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoEstadoTareasEscandallo>(MapeoEstadoTareasEscandallo.class);
        fieldGroup.bindMemberFields(this);
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoEstadoTareasEscandallo) uw.grid.getSelectedRow();
                eliminarEstado(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;	
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearEstado(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoEstadoTareasEscandallo mapeo_orig= (MapeoEstadoTareasEscandallo) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setIdCodigo(mapeo_orig.getIdCodigo());
		 				
		 				modificarEstado(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((EstadoTareasEscandalloGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarEstado(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.porcentaje.getValue()!=null && this.porcentaje.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("porc", this.porcentaje.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("porc", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.porcentaje.getValue()!=null && this.porcentaje.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("porc", this.porcentaje.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("porc", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarEstado(null);
		}
	}
	
	public void editarEstado(MapeoEstadoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoEstadoTareasEscandallo();
			
			if (!isBusqueda())
			{
				cps = consultaEstadoTareasEscandalloServer.getInstance(CurrentUser.get());
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoEstadoTareasEscandallo>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarEstado(MapeoEstadoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((EstadoTareasEscandalloGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((EstadoTareasEscandalloGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
			HashMap<String,String> filtrosRec = ((EstadoTareasEscandalloGrid) uw.grid).recogerFiltros();
			ArrayList<MapeoEstadoTareasEscandallo> r_vector = (ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector;
			
			Indexed indexed = ((EstadoTareasEscandalloGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((EstadoTareasEscandalloGrid) uw.grid).sort("porcentaje");
			((EstadoTareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
		}
	}
	
	public void modificarEstado(MapeoEstadoTareasEscandallo r_mapeo, MapeoEstadoTareasEscandallo r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdCodigo(r_mapeo_orig.getIdCodigo());
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo==null)
		{
			if (!((EstadoTareasEscandalloGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoEstadoTareasEscandallo> r_vector = (ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((EstadoTareasEscandalloGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((EstadoTareasEscandalloGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	//            indexed.addItemAt(list.size()+1, r_mapeo);
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	//            uw.generarGrid(uw.opcionesEscogidas);
	            uw.presentarGrid(r_vector);
	            
				((EstadoTareasEscandalloGrid) uw.grid).setRecords((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector);
	////			((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);
				((EstadoTareasEscandalloGrid) uw.grid).sort("porcentaje");
				((EstadoTareasEscandalloGrid) uw.grid).scrollTo(r_mapeo);
				((EstadoTareasEscandalloGrid) uw.grid).select(r_mapeo);
				
				((EstadoTareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearEstado(MapeoEstadoTareasEscandallo r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdCodigo(uw.cus.obtenerSiguiente());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
		
		if (rdo==null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoEstadoTareasEscandallo>(r_mapeo));
			if (((EstadoTareasEscandalloGrid) uw.grid)!=null )//&& )
			{
				if (!((EstadoTareasEscandalloGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((EstadoTareasEscandalloGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((EstadoTareasEscandalloGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector).add(r_mapeo);
					((EstadoTareasEscandalloGrid) uw.grid).setRecords((ArrayList<MapeoEstadoTareasEscandallo>) ((EstadoTareasEscandalloGrid) uw.grid).vector);
					((EstadoTareasEscandalloGrid) uw.grid).sort("porcentaje");
					((EstadoTareasEscandalloGrid) uw.grid).scrollTo(r_mapeo);
					((EstadoTareasEscandalloGrid) uw.grid).select(r_mapeo);
					
					((EstadoTareasEscandalloGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoEstadoTareasEscandallo> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdCodigo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
//		this.articulo.setRequired(r_obliga);
//		this.numerador.setRequired(r_obliga);
//		this.origen.setRequired(r_obliga);
//		this.notificador.setRequired(r_obliga);
//		this.cantidad.setRequired(r_obliga);
//		this.fechaNotificacion.setRequired(r_obliga);
//		this.fechaNotificacion.setEnabled(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.porcentaje.getValue()==null || this.porcentaje.getValue().toString().length()==0) return false;
			if (this.descripcion.getValue()==null || this.descripcion.getValue().toString().length()==0) return false;
		}
		
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}

}
