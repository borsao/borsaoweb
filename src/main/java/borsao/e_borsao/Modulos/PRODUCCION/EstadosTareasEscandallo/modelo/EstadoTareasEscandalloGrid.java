package borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.modelo;

import java.util.ArrayList;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.EstadosTareasEscandallo.view.EstadosTareasEscandalloView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class EstadoTareasEscandalloGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
	public Integer permisos = null;
	private EstadosTareasEscandalloView app = null;
	
    public EstadoTareasEscandalloGrid(EstadosTareasEscandalloView r_app, ArrayList<MapeoEstadoTareasEscandallo> r_vector) 
    {
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("Estado Tareas Escandallo");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoEstadoTareasEscandallo.class);
		this.setRecords(this.vector);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		this.setEditorEnabled(false);
		this.setSeleccion(SelectionMode.SINGLE);
		
		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("descripcion", "porcentaje");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setWidthUndefined();
    	this.getColumn("porcentaje").setHeaderCaption("Porcentaje");
    	this.getColumn("porcentaje").setWidth(100);

    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ( "porcentaje".equals(cellReference.getPropertyId()))
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoEstadoTareasEscandallo mapeo = (MapeoEstadoTareasEscandallo) event.getItemId();
            	
            		activadaVentanaPeticion=false;
            		ordenando = false;
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}

	public void generacionPdf(MapeoEstadoTareasEscandallo r_mapeo, boolean r_eliminar, String r_impresora) 
    {
//    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    }
	
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {

	}
}


