package borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTurnos extends MapeoGlobal
{
    private Integer idTurno;
	private String turno;
	private String descripcion;
	private Integer horas;
	private Integer status;
	private boolean eliminar=false;
	

	public MapeoTurnos()
	{
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getHoras() {
		return horas;
	}

	public void setHoras(Integer horas) {
		this.horas = horas;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public boolean isEliminar() {
		return eliminar;
	}

	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}


}