package borsao.e_borsao.Modulos.PRODUCCION.Turnos.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo.MapeoTurnos;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.server.consultaTurnosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class TurnosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Turnos";
	private final String titulo = "TURNOS PRODUCCION";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoTurnos mapeoTurnos =null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoTurnos> r_vector=null;
    	consultaTurnosServer cus = new consultaTurnosServer(CurrentUser.get());
    	r_vector=cus.datosOpcionesGlobal(null);
    	
//    	if (r_vector!=null && r_vector.size()>0)
//    	{
    		grid = new OpcionesGrid(r_vector,this);
    		setHayGrid(true);
//    	}
//    	else
//    	{
//    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");
//    		setHayGrid(false);
//    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public TurnosView() 
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
        
    	this.generarGrid(null);
//        this.form = new OpcionesForm(this);
//        this.verForm(true);
        
//        addComponent(this.form);
    }

    public void newForm()
    {
    	if (grid==null)
    	{
    		grid = new OpcionesGrid(null,this);
			this.mapeoTurnos=new MapeoTurnos();
			mapeoTurnos.setTurno("");
			mapeoTurnos.setDescripcion("");
			mapeoTurnos.setEliminar(false);
			mapeoTurnos.setHoras(0);
			grid.getContainerDataSource().addItem(mapeoTurnos);    	
			grid.editItem(mapeoTurnos);
    	}
    	else if (grid.getEditedItemId()==null)
    	{
    		this.mapeoTurnos=new MapeoTurnos();
			mapeoTurnos.setTurno("");
			mapeoTurnos.setDescripcion("");
			mapeoTurnos.setEliminar(false);
			mapeoTurnos.setHoras(0);
			grid.getContainerDataSource().addItem(mapeoTurnos);    	
			grid.editItem(mapeoTurnos);
    	}
    	
//    	this.form.setCreacion(true);
//    	this.form.setBusqueda(false);
//    	this.form.btnGuardar.setCaption("Guardar");
//    	this.form.btnEliminar.setEnabled(false);    	
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
    }
    
    public void verForm(boolean r_busqueda)
    {    	
//    	this.form.setBusqueda(r_busqueda);
//    	this.form.setCreacion(false);
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
//    	this.form.btnGuardar.setCaption("Buscar");
//    	this.form.btnEliminar.setEnabled(false);    	
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.mapeoTurnos = (MapeoTurnos) r_fila;
//    	this.form.editarOpcion((MapeoTurnos) r_fila);
//    	this.form.addStyleName("visible");
//    	this.form.setEnabled(true);
//    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
