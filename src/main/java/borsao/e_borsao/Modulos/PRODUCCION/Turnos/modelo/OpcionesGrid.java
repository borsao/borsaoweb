package borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo;

import java.util.ArrayList;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.server.consultaTurnosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.view.TurnosView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OpcionesGrid extends GridPropio 
{
	private TurnosView av = null;
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	
	
    public OpcionesGrid(ArrayList<MapeoTurnos> r_vector,  TurnosView r_view) 
    {
    	this.av=r_view;
        this.vector=r_vector;
		this.asignarTitulo("Turnos Produccion");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		actualizar = false;
        this.crearGrid(MapeoTurnos.class);
        
        if (this.vector == null || this.vector.size()==0)
        {
        	this.vector = new ArrayList<MapeoTurnos>();
        }
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);

    }
    
//    @Override
    public void doEditItem() {
//    	this.mostrar();
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	super.doEditItem();
    	av.botonesGenerales(true);
    }
    
    @Override
    public void doCancelEditor(){
	     //Your Code
//    	 if (getEditedItemId()!=null) remove((MapeoTurnos) getEditedItemId());
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
	     super.doCancelEditor();
	     av.botonesGenerales(true);
	  }
	
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("turno","descripcion","horas");

    }
    
    public void establecerTitulosColumnas()
    {
    	
    	this.getColumn("turno").setHeaderCaption("Turno");
    	this.getColumn("turno").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("horas").setHeaderCaption("Horas");
    	this.getColumn("horas").setWidth(new Double(100));
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("status").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	
    	
    	this.getColumn("eliminar").setWidth(new Double(100));
    	
    	
    	this.getColumn("turno").setEditorField(
				getComboBox("El turno es obligatorio!"));

    }

    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
    		   @Override
    		   protected String getTrueString() {
    		      return "";
    		   }
    		   @Override
    		   protected String getFalseString() {
    		      return "";
    		   }
    		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("horas".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }

    public void cargarListeners()
    {
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException 
	        {
	        	
	        	MapeoTurnos mapeo = (MapeoTurnos) getEditedItemId();
		        
	        	consultaTurnosServer cs = new consultaTurnosServer(CurrentUser.get());
	        	Integer status = cs.obtenerStatus(mapeo.getIdTurno());
	        	if (mapeo.getStatus()==status || status == null)
	        	{
	        		actualizar=true;
	        	}
	        	else
	        	{
	        		actualizar=false;
	        		Notificaciones.getInstance().mensajeAdvertencia("Registro modificado por otro usuario. Actualiza los cambios antes de modificar los datos.");
	        		doCancelEditor();
	        	}
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        	if (actualizar)
	        	{
		        	MapeoTurnos mapeo = (MapeoTurnos) getEditedItemId();
		        	consultaTurnosServer cs = new consultaTurnosServer(CurrentUser.get());
		        
		        	if (mapeo.isEliminar())
		        	{
		        		cs.eliminar(mapeo);
		        		remove(mapeo);
		        	}
		        	else
		        	{
		        		if (mapeo.getIdTurno()!=null)
			        	{
			        		cs.guardarCambios(mapeo);
			        	}
			        	else
			        	{
			        		cs.guardarNuevo(mapeo);
			        	}
		        	}
	        	}
	        }
	        
		});

    }

	@Override
	public void establecerColumnasNoFiltro() 
	{
	}
	
	/**
	 * @return
	 */
	private Field<?> getComboBox(String requiredErrorMsg) {
		ComboBox comboBox = new ComboBox();
		comboBox.setNullSelectionAllowed(true);
		ArrayList<String> valores = new ArrayList<String>();
		valores.add("T");
		valores.add("M");
		valores.add("N");
		valores.add("B");
		valores.add("R");
		valores.add("V");
		valores.add("D");
		valores.add("C");
		IndexedContainer container = new IndexedContainer(valores);
		comboBox.setContainerDataSource(container);
		comboBox.setRequired(true);
		comboBox.setRequiredError(requiredErrorMsg);
		comboBox.setInvalidAllowed(false);
		comboBox.setNullSelectionAllowed(false);
		return comboBox;
	}

	@Override
	public void calcularTotal() {
		
	}
}
