package borsao.e_borsao.Modulos.PRODUCCION.Turnos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.Turnos.modelo.MapeoTurnos;

public class consultaTurnosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTurnosServer instance;
	
	public consultaTurnosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTurnosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTurnosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoTurnos> datosOpcionesGlobal(MapeoTurnos r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoTurnos> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_turnos.idprd_turnos tur_id, ");
        cadenaSQL.append(" prd_turnos.prd_turno tur_tur, ");
        cadenaSQL.append(" prd_turnos.prd_descripcion tur_des, ");
        cadenaSQL.append(" prd_turnos.prd_horasTurno tur_hor, ");
	    cadenaSQL.append(" prd_turnos.prd_status tur_sta ");
     	cadenaSQL.append(" FROM prd_turnos ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdTurno()!=null && r_mapeo.getIdTurno().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_turnos = '" + r_mapeo.getIdTurno() + "' ");
				}
				if (r_mapeo.getTurno()!=null && r_mapeo.getTurno().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_turno = '" + r_mapeo.getTurno() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getHoras()!=null && r_mapeo.getHoras().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_horasTurno= " + r_mapeo.getHoras() + " ");
				}
				if (r_mapeo.getStatus()!=null && r_mapeo.getStatus().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" prd_status= " + r_mapeo.getStatus() + " ");
				}
				
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_turnos asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoTurnos>();
			
			while(rsOpcion.next())
			{
				MapeoTurnos mapeoTurnos = new MapeoTurnos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoTurnos.setIdTurno(rsOpcion.getInt("tur_id"));
				mapeoTurnos.setTurno(rsOpcion.getString("tur_tur"));
				mapeoTurnos.setDescripcion(rsOpcion.getString("tur_des"));
				mapeoTurnos.setHoras(rsOpcion.getInt("tur_hor"));
				mapeoTurnos.setStatus(rsOpcion.getInt("tur_sta"));
				vector.add(mapeoTurnos);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_turnos.idprd_turnos) tur_sig ");
     	cadenaSQL.append(" FROM prd_turnos ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tur_sig")+1;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}
	
	public Integer obtenerStatus(Integer r_status)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_turnos.prd_status tur_sta ");
     	cadenaSQL.append(" FROM prd_turnos ");
     	cadenaSQL.append(" WHERE prd_turnos.idprd_turnos = " + r_status);
     	
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tur_sta");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String guardarNuevo(MapeoTurnos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_turnos ( ");
    		cadenaSQL.append(" prd_turnos.idprd_turnos, ");
	        cadenaSQL.append(" prd_turnos.prd_turno, ");
	        cadenaSQL.append(" prd_turnos.prd_descripcion, ");
	        cadenaSQL.append(" prd_turnos.prd_horasTurno, ");
		    cadenaSQL.append(" prd_turnos.prd_status ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getTurno()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTurno());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    preparedStatement.setInt(5, 0);

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoTurnos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_turnos set ");
	        cadenaSQL.append(" prd_turnos.prd_turno=?, ");
	        cadenaSQL.append(" prd_turnos.prd_descripcion =?, ");
		    cadenaSQL.append(" prd_turnos.prd_horasTurno=?, ");
		    cadenaSQL.append(" prd_turnos.prd_status=? ");
		    cadenaSQL.append(" WHERE prd_turnos.idprd_turnos = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getTurno());
		    preparedStatement.setString(2, r_mapeo.getDescripcion());
		    preparedStatement.setInt(3, r_mapeo.getHoras());
		    preparedStatement.setInt(4, r_mapeo.getStatus()+1);
		    preparedStatement.setInt(5, r_mapeo.getIdTurno());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoTurnos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_turnos ");            
			cadenaSQL.append(" WHERE prd_turnos.idprd_turnos = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdTurno());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}