package borsao.e_borsao.Modulos.PRODUCCION.TiposOF.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.TiposOF.modelo.MapeoTiposOF;

public class consultaTiposOFServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaTiposOFServer instance;
	
	public consultaTiposOFServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaTiposOFServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaTiposOFServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoTiposOF> datosOFGlobal(MapeoTiposOF r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoTiposOF> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_tiposof.idprd_tiposof tof_id, ");
        cadenaSQL.append(" prd_tiposof.valor tof_nom, ");
	    cadenaSQL.append(" prd_tiposof.color tof_col ");
     	cadenaSQL.append(" FROM prd_tiposof ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdTipo()!=null && r_mapeo.getIdTipo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_tiposof = '" + r_mapeo.getIdTipo() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" valor = '" + r_mapeo.getNombre() + "' ");
				}
//				if (r_mapeo.getColor()!=null && r_mapeo.getColor().length()>0)
//				{
//					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
//					cadenaWhere.append(" color = '" + r_mapeo.getColor() + "' ");
//				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_tiposof asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoTiposOF>();
			
			while(rsOpcion.next())
			{
				MapeoTiposOF mapeotIPOSof = new MapeoTiposOF();
				/*
				 * recojo mapeo operarios
				 */
				mapeotIPOSof.setIdTipo(rsOpcion.getInt("tof_id"));
				mapeotIPOSof.setNombre(rsOpcion.getString("tof_nom").toUpperCase());
				mapeotIPOSof.setColor(rsOpcion.getString("tof_col"));
				vector.add(mapeotIPOSof);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_tiposof.idprd_tiposof) tof_sig ");
     	cadenaSQL.append(" FROM prd_tiposof ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("tof_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	public String obtenerColor(String r_ope)
	{
		ResultSet rsOpcion= null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_tiposof.color tof_col ");
     	cadenaSQL.append(" FROM prd_tiposof ");
     	cadenaSQL.append(" where prd_tiposof.valor = '" + r_ope + "'");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getString("tof_col");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}

	public String guardarNuevo(MapeoTiposOF r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_tiposof ( ");
    		cadenaSQL.append(" prd_tiposof.idprd_tiposof, ");
	        cadenaSQL.append(" prd_tiposof.valor, ");
		    cadenaSQL.append(" prd_tiposof.color ) VALUES (");
		    cadenaSQL.append(" ?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getNombre().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getColor()!=null)
		    {
		    	preparedStatement.setString(3, String.valueOf(r_mapeo.getColor()));
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoTiposOF r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_tiposof set ");
	        cadenaSQL.append(" prd_tiposof.valor=?, ");
		    cadenaSQL.append(" prd_tiposof.color=? ");
		    cadenaSQL.append(" WHERE prd_tiposof.idprd_tiposof = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getNombre().toUpperCase());
		    preparedStatement.setString(2, String.valueOf(r_mapeo.getColor()));
		    preparedStatement.setInt(3, r_mapeo.getIdTipo());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoTiposOF r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_tiposof ");            
			cadenaSQL.append(" WHERE prd_tiposof.idprd_tiposof = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdTipo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}