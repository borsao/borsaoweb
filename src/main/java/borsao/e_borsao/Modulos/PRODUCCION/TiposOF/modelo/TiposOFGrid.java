package borsao.e_borsao.Modulos.PRODUCCION.TiposOF.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class TiposOFGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;

    public TiposOFGrid(ArrayList<MapeoTiposOF> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("Tipos OF");
		this.generarGrid();
    }

    private void generarGrid()
    {
    	this.crearGrid(MapeoTiposOF.class);
		this.setRecords(this.vector);
		this.addStyleName("programacion");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("color", "nombre");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombre").setHeaderCaption("Valor");
    	this.getColumn("color").setHidden(true);
    	this.getColumn("idTipo").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    }
    
    /*
     * METODOS PUBLICOS
     */
    

    public void asignarEstilos()
    {
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String estilo = null;
            @Override
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("color".equals(cellReference.getPropertyId())) 
            	{
                    	estilo = "cell-" + cellReference.getValue().toString().substring(1, 7); 
                }
            	if ("nombre".equals(cellReference.getPropertyId())) 
            	{
            		return estilo; 
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    	
    }

	public void establecerColumnasNoFiltro() 
	{
	}
	
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}

}
