package borsao.e_borsao.Modulos.PRODUCCION.TiposOF.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoTiposOF mapeo = null;
	 
	 public MapeoTiposOF convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoTiposOF();
		 this.mapeo.setColor(null);
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdTipo(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public MapeoTiposOF convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoTiposOF();
		 this.mapeo.setColor(r_hash.get("color"));
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null) this.mapeo.setIdTipo(new Integer(r_hash.get("id")));
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoTiposOF r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("color", r_mapeo.getColor());
		 this.hash.put("nombre", this.mapeo.getNombre());
		 this.hash.put("id", this.mapeo.getIdTipo().toString());
		 return hash;		 
	 }
}