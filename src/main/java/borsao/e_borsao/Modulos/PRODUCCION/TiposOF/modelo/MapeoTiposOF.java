package borsao.e_borsao.Modulos.PRODUCCION.TiposOF.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoTiposOF extends MapeoGlobal
{
    private Integer idTipo;
	private String nombre;
	private String color;
//	private Color colorReal;

	public MapeoTiposOF()
	{
		this.setNombre("");
		this.setColor("");
	}

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String r_nombre) {
		this.nombre = r_nombre;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String r_color) {
		this.color= r_color;
//		if (r_color!=null && r_color.length()>0 ) this.setColorReal( new Color(new Integer(r_color)));
	}
	
//	public Color getColorReal() {
//		return colorReal;
//	}
//
//	public void setColorReal(Color colorReal) {
//		this.colorReal = colorReal;
//	}

}