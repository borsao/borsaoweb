package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoHorasTurnoAbsentismo  extends MapeoGlobal
{
    private Integer idTurno;
    private String operario;
    private String horasAbsentismo;
    private String motivoAbsentismo;

	public MapeoHorasTurnoAbsentismo() 
	{
		
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getOperario() {
		return operario;
	}

	public void setOperario(String operario) {
		this.operario = operario;
	}

	public String getHorasAbsentismo() {
		return horasAbsentismo;
	}

	public void setHorasAbsentismo(String horasAbsentismo) {
		this.horasAbsentismo = horasAbsentismo;
	}

	public String getMotivoAbsentismo() {
		return motivoAbsentismo;
	}

	public void setMotivoAbsentismo(String motivoAbsentismo) {
		this.motivoAbsentismo = motivoAbsentismo;
	}
	
}