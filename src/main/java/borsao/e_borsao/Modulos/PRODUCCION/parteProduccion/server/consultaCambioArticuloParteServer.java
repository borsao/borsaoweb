package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoCambioArticuloParte;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaCambioArticuloParteServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaCambioArticuloParteServer instance;
	
	public consultaCambioArticuloParteServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaCambioArticuloParteServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaCambioArticuloParteServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoCambioArticuloParte> recuperarCambioArticuloParte(MapeoCambioArticuloParte r_mapeo)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		MapeoCambioArticuloParte mapeoCambioArticuloParte = null;
		ArrayList<MapeoCambioArticuloParte> vector = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_ca_parte.idCodigo ca_id,");
		cadenaSQL.append(" prd_paradas_ca_parte.articulo ca_art,");
		cadenaSQL.append(" prd_paradas_ca_parte.global ca_glb,");
		cadenaSQL.append(" prd_paradas_ca_parte.tapon ca_tap,");
		cadenaSQL.append(" prd_paradas_ca_parte.botella ca_bot,");
		cadenaSQL.append(" prd_paradas_ca_parte.caja ca_caj,");
		cadenaSQL.append(" prd_paradas_ca_parte.tiempo ca_hor,");
		cadenaSQL.append(" prd_paradas_ca_parte.observaciones ca_obs, ");
		cadenaSQL.append(" prd_paradas_ca_parte.idParadasParte ca_ipp ");
		cadenaSQL.append(" from prd_paradas_ca_parte ");
		cadenaSQL.append(" where prd_paradas_ca_parte.idParadasParte = " + r_mapeo.getIdIncidenciaProgramada());

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoCambioArticuloParte>();
			while(rsOpcion.next())
			{
				mapeoCambioArticuloParte = new MapeoCambioArticuloParte();
				/*
				 * recojo mapeo operarios
				 */
				mapeoCambioArticuloParte.setIdCodigo(rsOpcion.getInt("ca_id"));
				mapeoCambioArticuloParte.setArticulo(rsOpcion.getString("ca_art"));
				mapeoCambioArticuloParte.setCambioArticulo(rsOpcion.getString("ca_glb"));
				mapeoCambioArticuloParte.setCambioTapon(rsOpcion.getString("ca_tap"));
				mapeoCambioArticuloParte.setCambioBotella(rsOpcion.getString("ca_bot"));
				mapeoCambioArticuloParte.setCambioCaja(rsOpcion.getString("ca_caj"));
				mapeoCambioArticuloParte.setObservaciones(rsOpcion.getString("ca_obs"));
				mapeoCambioArticuloParte.setIdIncidenciaProgramada(rsOpcion.getInt("ca_ipp"));
				mapeoCambioArticuloParte.setTiempo(rsOpcion.getString("ca_hor"));
				
				vector.add(mapeoCambioArticuloParte);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public String recuperarHorasCambioArticuloParte(Integer r_idParadasParte)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_ca_parte.idCodigo ca_id,");
		cadenaSQL.append(" sum(prd_paradas_ca_parte.tiempo) ca_hor");
		cadenaSQL.append(" from prd_paradas_ca_parte ");
		cadenaSQL.append(" where prd_paradas_ca_parte.idParadasParte = " + r_idParadasParte);
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getString("ca_hor");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return "0";
	}
	
	public StringBuffer recuperarObservacionesCambioArticuloParte(Integer r_idParadasParte)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer rdoSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_ca_parte.idCodigo ca_id,");
		cadenaSQL.append(" prd_paradas_ca_parte.observaciones ca_obs");
		cadenaSQL.append(" from prd_paradas_ca_parte ");
		cadenaSQL.append(" where prd_paradas_ca_parte.idParadasParte = " + r_idParadasParte);
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				
				if (rsOpcion.getString("ca_obs")!=null && rsOpcion.getString("ca_obs").length()>0)
				{
					rdoSQL.append(rsOpcion.getString("ca_obs").trim());
					rdoSQL.append(" ");
				}
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return rdoSQL;
	}

	public Integer obtenerSiguienteCambioArticuloParte()
	{
		ResultSet rsOpcion = null;		
		Connection con = null;	
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_paradas_ca_parte.idCodigo) cab_sig ");
     	cadenaSQL.append(" FROM prd_paradas_ca_parte ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("cab_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return 1;
	}

	public String guardarNuevo(MapeoCambioArticuloParte r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_paradas_ca_parte( ");
			cadenaSQL.append(" prd_paradas_ca_parte.idCodigo,");
			cadenaSQL.append(" prd_paradas_ca_parte.articulo,");
			cadenaSQL.append(" prd_paradas_ca_parte.global,");
			cadenaSQL.append(" prd_paradas_ca_parte.tapon ,");
			cadenaSQL.append(" prd_paradas_ca_parte.botella ,");
			cadenaSQL.append(" prd_paradas_ca_parte.caja,");
			cadenaSQL.append(" prd_paradas_ca_parte.tiempo,");
			cadenaSQL.append(" prd_paradas_ca_parte.observaciones,");
			cadenaSQL.append(" prd_paradas_ca_parte.idParadasParte) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguienteCambioArticuloParte());
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCambioArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCambioArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCambioTapon()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCambioTapon().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCambioBotella()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getCambioBotella().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCambioCaja()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getCambioCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getIdIncidenciaProgramada()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getIdIncidenciaProgramada());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}
	public String guardarCambios(MapeoCambioArticuloParte r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE prd_paradas_ca_parte SET ");
			cadenaSQL.append(" prd_paradas_ca_parte.articulo =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.global =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.tapon =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.botella =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.caja =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.tiempo =?,");
			cadenaSQL.append(" prd_paradas_ca_parte.observaciones =? ");
			cadenaSQL.append(" WHERE prd_paradas_ca_parte.idCodigo = ? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(1, r_mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
		    if (r_mapeo.getCambioArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getCambioArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCambioTapon()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCambioTapon().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCambioBotella()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getCambioBotella().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCambioCaja()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getCambioCaja().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(8, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(8, 0);
			}
			
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}


	public String eliminarCambioArticuloParte(Integer r_idCambio)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		String rdo = null;
		
		StringBuffer cadenaSQL = null;        
		
		try
		{
			con= this.conManager.establecerConexionInd();	          
			/*
			 * Aqui borraremos las tablas relacionadas cuando las tengamos con datos"
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_paradas_ca_parte ");            
			cadenaSQL.append(" WHERE prd_paradas_ca_parte.idCodigo = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idCambio);
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				rdo = ex.getMessage();
			}
		}
		return rdo;
	}
}