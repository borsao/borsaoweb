package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.pantallaCambioArticulo;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.parteProduccionView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class gridIncidenciasNoProgramadasCalidadGrid extends GridPropio 
{
	private boolean editable = false;
	private boolean conFiltro = false;
	private parteProduccionView app = null;
	private Integer idTurno = null;
	private MapeoIncidenciasTurno mapeoOrig=null;
	private MapeoIncidenciasTurno mapeo=null;
	
    public gridIncidenciasNoProgramadasCalidadGrid(ArrayList<MapeoIncidenciasTurno> r_vector, parteProduccionView r_app) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("Incidencias No Programadas Calidad");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoIncidenciasTurno>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    public gridIncidenciasNoProgramadasCalidadGrid(ArrayList<MapeoIncidenciasTurno> r_vector, parteProduccionView r_app, Integer r_idTurno) 
    {
        this.vector=r_vector;
        this.idTurno = r_idTurno;
        this.app=r_app;
        
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoIncidenciasTurno>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoIncidenciasTurno.class);
		if (this.vector!=null) this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("acc", "fecha", "maquina", "horas", "observaciones");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("acc").setHeaderCaption("");
    	this.getColumn("acc").setSortable(true);
    	this.getColumn("acc").setWidth(new Double(50));
    	
    	this.getColumn("maquina").setHeaderCaption("Incidencia Calidad");
    	this.getColumn("maquina").setSortable(true);
    	this.getColumn("maquina").setWidth(new Double(200));
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setSortable(false);
    	this.getColumn("fecha").setWidth(150);
    	this.getColumn("horas").setHeaderCaption("Horas");
    	this.getColumn("horas").setSortable(true);
    	this.getColumn("horas").setWidth(new Double(80));
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("observaciones").setSortable(false);
    	this.getColumn("observaciones").setWidth(new Double(200));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	this.getColumn("fecha").setHidden(true);
    	this.getColumn("tipo").setHidden(true);
    	
    }
    
    private void asignarTooltips()
    {
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }

    public void asignarEstilos()
    {
    	asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("acc".equals(cellReference.getPropertyId())) 
            	{            		
            		return "cell-nativebuttonProgramacion";
            	}
            	else if ("horas".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	consultaParteProduccionServer cs = new consultaParteProduccionServer(CurrentUser.get());
        		MapeoIncidenciasTurno mapeo = (MapeoIncidenciasTurno) getEditedItemId();
        		MapeoIncidenciasTurno mapeoOrig=(MapeoIncidenciasTurno) getEditedItemId();
        		
        		if (app!=null)
        		{
        			if (app.idTurno!=null && app.idTurno>0)
        			{
        				idTurno = new Integer(app.idTurno);
        			}
        		}
        		
        		if ((mapeo.getHoras()==null || mapeo.getHoras().length()==0) || (new Double(mapeo.getHoras())==0 && mapeo.getObservaciones()!=null && mapeo.getObservaciones().length()>0))
        		{
        			Notificaciones.getInstance().mensajeError("Rellena correctamente el tiempo de parada");
        			doCancelEditor();
        		}
        		else
        		{
	        		mapeo.setIdTurno(idTurno);
	        		mapeo.setIdCodigo(mapeoOrig.getIdCodigo());
	        		mapeo.setTipo(mapeoOrig.getTipo());
	        		
	        		cs.guardarCambiosIncidenciaParte(mapeo);
	        		if (mapeo.getIdTurno()!=null)
		        	{
		        		((ArrayList<MapeoIncidenciasTurno>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoIncidenciasTurno>) vector).add(mapeo);
	
		    			refreshAllRows();
		    			calcularTotal();
		    			scrollTo(mapeo);
		        	}
        		}
	        }
		});
		this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoIncidenciasTurno mapeoOrig = (MapeoIncidenciasTurno) event.getItemId();
	            	
            		if (event.getPropertyId().toString().equals("acc"))
	            	{
	    				
	        			try
	        			{
	        				if (mapeoOrig.getMaquina().contains("Articulo"))
	        				{           		
			        			pantallaCambioArticulo vt = new pantallaCambioArticulo(app, mapeoOrig, "Tiempos cambio Articulo", true);
			        			app.getUI().addWindow(vt);
	        				}
	        				else
	        				{
			        			pantallaCambioArticulo vt = new pantallaCambioArticulo(app, mapeoOrig, "Tiempos cambio " + mapeoOrig.getMaquina() ,false);
			        			app.getUI().addWindow(vt);        					
	        				}
		            		
	        			}
	        			catch (IndexOutOfBoundsException ex)
	        			{
	        				
	        			}
	            	}            	
	            	else
	            	{
	            	}
	            }
    		}
        });				
	}
	
	public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	this.mapeo= new MapeoIncidenciasTurno();
		this.mapeoOrig=((MapeoIncidenciasTurno) getEditedItemId());
    	
		this.mapeo.setIdCodigo(this.mapeoOrig.getIdCodigo());
		this.mapeo.setIdTurno(this.mapeoOrig.getIdTurno());
		this.mapeo.setTipo(this.mapeoOrig.getTipo());
		
    	super.doEditItem();
    }

	@Override
	public void doCancelEditor()
	{
		  super.doCancelEditor();
	}

	@Override
	public void calcularTotal() {
		Double total = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null && item.getItemProperty("horas").getValue()!=null)
        	{
	        	Double q1Value = new Double(RutinasCadenas.reemplazarComaMiles(item.getItemProperty("horas").getValue().toString()));
	        	if (q1Value!=null) total += q1Value;
        	}
        }
        
        footer.getCell("maquina").setText("Total:");
		footer.getCell("horas").setText(RutinasNumericas.formatearDoubleDecimales(total, new Integer(2)).toString());
		
		if (this.app!=null)
		{
//			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-50)+"%");
		}
		footer.getCell("maquina").setStyleName("Rcell-pie");
		footer.getCell("horas").setStyleName("Rcell-pie");

	}
	
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoIncidenciasTurno) {
                MapeoIncidenciasTurno progRow = (MapeoIncidenciasTurno)bean;
                // The actual description text is depending on the application
                if ("observaciones".equals(cell.getPropertyId()))
                {
                	descriptionText = progRow.getObservaciones();
                }
            }
        }
        return descriptionText;
    }

}


