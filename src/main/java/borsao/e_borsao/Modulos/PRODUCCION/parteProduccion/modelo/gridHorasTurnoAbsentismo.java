package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.parteProduccionView;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class gridHorasTurnoAbsentismo extends GridPropio 
{
	private boolean editable = true;
	private boolean conFiltro = false;
	private parteProduccionView app = null;
	private Integer idTurno = null;
	
    public gridHorasTurnoAbsentismo(ArrayList<MapeoHorasTurnoAbsentismo> r_vector, parteProduccionView r_app) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
        this.asignarTitulo("Horas Absentismo");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoHorasTurno>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    public gridHorasTurnoAbsentismo(ArrayList<MapeoHorasTurnoAbsentismo> r_vector, parteProduccionView r_app, Integer r_idTurno) 
    {
        this.vector=r_vector;
        this.idTurno = r_idTurno;
        this.app=r_app;
		this.asignarTitulo("Horas Absentismo");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoHorasTurnoAbsentismo>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoHorasTurnoAbsentismo.class);
		if (this.vector!=null) this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("operario", "horasAbsentismo","motivoAbsentismo");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("operario").setHeaderCaption("Operario");
    	this.getColumn("operario").setSortable(true);
    	this.getColumn("operario").setWidth(new Double(200));
    	this.getColumn("horasAbsentismo").setHeaderCaption("Horas Absentismo");
    	this.getColumn("horasAbsentismo").setSortable(true);
    	this.getColumn("horasAbsentismo").setWidth(new Double(80));
    	
    	this.getColumn("motivoAbsentismo").setHeaderCaption("Observaciones");
    	this.getColumn("motivoAbsentismo").setSortable(false);
    	this.getColumn("motivoAbsentismo").setWidth(new Double(200));
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("horasmotivoAbsentismo".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
        		MapeoHorasTurnoAbsentismo mapeo = (MapeoHorasTurnoAbsentismo) getEditedItemId();
        		MapeoHorasTurnoAbsentismo mapeoOrig=(MapeoHorasTurnoAbsentismo) getEditedItemId();
        		
        		if (app!=null)
        		{
        			if (app.idTurno!=null && app.idTurno>0)
        			{
        				idTurno = new Integer(app.idTurno);
        			}
        		}
        		
        		if (mapeo.getHorasAbsentismo()==null || new Integer(mapeo.getHorasAbsentismo())==0)
        		{
        			Notificaciones.getInstance().mensajeError("Rellena correctamente el tiempo ");
        			doCancelEditor();
        		}
        		else
        		{
	        		mapeo.setIdTurno(idTurno);
	        		
	        		if (mapeo.getIdTurno()!=null)
		        	{
		        		((ArrayList<MapeoHorasTurnoAbsentismo>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoHorasTurnoAbsentismo>) vector).add(mapeo);
	
		    			refreshAllRows();
		    			calcularTotal();
		    			scrollTo(mapeo);
		        	}
        		}
	        }
		});
					
	}
	
	@Override
	public void doCancelEditor()
	{
		  super.doCancelEditor();
	}

	@Override
	public void calcularTotal() {
		Double total = new Double(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
	        	Double q1Value = new Double(RutinasCadenas.reemplazarComaMiles(item.getItemProperty("horasAbsentismo").getValue().toString()));
	        	if (q1Value!=null) total += q1Value;
        	}
        }
        
		footer.getCell("horasAbsentismo").setText(RutinasNumericas.formatearDoubleDecimales(total, new Integer(2)).toString());
		
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-50)+"%");
		}
		footer.getCell("horasAbsentismo").setStyleName("Rcell-pie");

	}
}


