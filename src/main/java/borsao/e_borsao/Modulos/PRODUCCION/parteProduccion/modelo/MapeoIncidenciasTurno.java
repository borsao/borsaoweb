package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoIncidenciasTurno  extends MapeoGlobal
{
    private Integer idTurno;
    private String maquina;
    private String tipo;
    private String horas;
    private String Observaciones;
    private String acc;
    private String fecha;

	public String getAcc() {
		return acc;
	}

	public void setAcc(String acc) {
		this.acc = acc;
	}

	public MapeoIncidenciasTurno() 
	{
		
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getMaquina() {
		return maquina;
	}

	public void setMaquina(String maquina) {
		this.maquina = maquina;
	}

	public String getHoras() {
		return horas;
	}

	public void setHoras(String horas) {
		this.horas = horas;
	}

	public String getObservaciones() {
		return Observaciones;
	}

	public void setObservaciones(String observaciones) {
		Observaciones = observaciones;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}