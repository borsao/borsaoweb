package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoCambioArticuloParte;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoIncidenciasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaCambioArticuloParteServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaCambioArticulo extends Ventana
{
	private consultaCambioArticuloParteServer cncs = null; 
	private parteProduccionView App = null;
//	private MapeoCabeceraParte mapeoCabeceraParte = null;
	private MapeoIncidenciasTurno mapeoIncidencias = null;
	private MapeoCambioArticuloParte mapeoCambiosArticuloParte = null;
	
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;

	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	
	private TextField txtHoras= null;
	private TextField txtArticulo= null;
	private Label lblDescripcionArticulo= null;
	private TextArea txtObservaciones= null;
	
	private CheckBox chkCambioArticulo = null;
	private CheckBox chkCambioRoscaCorcho = null;
	private CheckBox chkCambioBotella = null;
	private CheckBox chkCambioCaja = null;
	
	private boolean creacion = true;
	public boolean actualizar = false;
	public boolean cambioArticulo = false;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaCambioArticulo(GridViewRefresh r_App, MapeoIncidenciasTurno r_mapeoIncidencia, String r_titulo, boolean r_cambioArticulo)
	{
		this.setCaption(r_titulo);
		this.App=(parteProduccionView) r_App;
//		this.mapeoCabeceraParte= r_mapeoCabecera;
		this.mapeoIncidencias= r_mapeoIncidencia;
		this.cambioArticulo=r_cambioArticulo;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaCambioArticuloParteServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("850px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.txtArticulo =new TextField("Articulo");
        		this.txtArticulo.setVisible(cambioArticulo);
        		this.txtArticulo.setWidth("100px");
        		this.txtArticulo.addStyleName(ValoTheme.TEXTFIELD_TINY);

        		this.lblDescripcionArticulo= new Label("");    		
        		this.lblDescripcionArticulo.setEnabled(false);
        		this.lblDescripcionArticulo.setWidth("250px");
//        		this.lblDescripcionArticulo.addStyleName(ValoTheme.LABEL_TINY);

    			linea1.addComponent(this.txtArticulo);
    			linea1.addComponent(this.lblDescripcionArticulo);
    			linea1.setComponentAlignment(this.lblDescripcionArticulo, Alignment.BOTTOM_LEFT);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Tiempo");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");
    			
    			this.chkCambioArticulo= new CheckBox();
    			this.chkCambioArticulo.setCaption("Cambio Articulo ?");
    			this.chkCambioArticulo.setVisible(cambioArticulo);
    			
    			this.chkCambioRoscaCorcho = new CheckBox();
    			this.chkCambioRoscaCorcho.setCaption("Cambio ROSCA-CORCHO ?");
    			this.chkCambioRoscaCorcho.setVisible(cambioArticulo);
    			
				this.chkCambioBotella= new CheckBox();
    			this.chkCambioBotella.setCaption("Cambio Formato Botella ?");
    			this.chkCambioBotella.setVisible(cambioArticulo);

				this.chkCambioCaja= new CheckBox();
    			this.chkCambioCaja.setCaption("Cambio Caja 6/12 ?");
    			this.chkCambioCaja.setVisible(cambioArticulo);

    			linea2.addComponent(this.chkCambioArticulo);
    			linea2.setComponentAlignment(this.chkCambioArticulo, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkCambioRoscaCorcho);
    			linea2.setComponentAlignment(this.chkCambioRoscaCorcho, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkCambioBotella);
    			linea2.setComponentAlignment(this.chkCambioBotella, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.chkCambioCaja);
    			linea2.setComponentAlignment(this.chkCambioCaja, Alignment.BOTTOM_LEFT);
    			linea2.addComponent(this.txtHoras);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			this.txtObservaciones= new TextArea();
    			this.txtObservaciones.setCaption("Observaciones");
    			this.txtObservaciones.setWidth("300");
    			
    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);
    			
    			linea3.addComponent(this.txtObservaciones);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		txtArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
				lblDescripcionArticulo.setValue(cas.obtenerDescripcionArticulo(txtArticulo.getValue()));
			}
		});
		chkCambioArticulo.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				chkCambioBotella.setEnabled(chkCambioArticulo.getValue());
				chkCambioCaja.setEnabled(chkCambioArticulo.getValue());
				chkCambioRoscaCorcho.setEnabled(chkCambioArticulo.getValue());
			}
		});
		chkCambioBotella.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				chkCambioArticulo.setEnabled(!chkCambioBotella.getValue());
			}
		});
		chkCambioRoscaCorcho.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				chkCambioArticulo.setEnabled(!chkCambioRoscaCorcho.getValue());
			}
		});
		chkCambioCaja.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				chkCambioArticulo.setEnabled(!chkCambioCaja.getValue());
			}
		});
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});
		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void guardar()
	{
		String rdo = null;
		Double tiempo = null;
		if (todoEnOrden())
		{
			tiempo = new Double(this.txtHoras.getValue());
				
			MapeoCambioArticuloParte mapeo = new MapeoCambioArticuloParte();
			
			if (chkCambioArticulo.getValue()) mapeo.setCambioArticulo("S"); else mapeo.setCambioArticulo("N");
			if (chkCambioBotella.getValue()) mapeo.setCambioBotella("S"); else mapeo.setCambioBotella("N");
			if (chkCambioCaja.getValue()) mapeo.setCambioCaja("S"); else mapeo.setCambioCaja("N");
			if (chkCambioRoscaCorcho.getValue()) mapeo.setCambioTapon("S"); else mapeo.setCambioTapon("N");
			
			if (cambioArticulo)
				mapeo.setArticulo(this.txtArticulo.getValue());
			else
				mapeo.setArticulo(" ");
			mapeo.setTiempo(tiempo.toString());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			mapeo.setIdIncidenciaProgramada(this.mapeoIncidencias.getIdCodigo());

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevo(mapeo);
//				if (rdo!=null)
//				{
//					this.mapeoCambiosArticuloParte .setIdCodigo(mapeo.getIdCodigo());
//				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoCambiosArticuloParte.getIdCodigo());
				rdo = cncs.guardarCambios(mapeo);
			}
			
			if (rdo!=null) 
				Notificaciones.getInstance().mensajeError(rdo);
			else
				cerrar();
		}
	}

	private void eliminar()
	{
		
			VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres eliminar el registro?", "Si", "No", "AceptarEliminar", "CancelarEliminar");
			getUI().addWindow(vt);
			
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoCambioArticuloParte> vector=null;

    	MapeoCambioArticuloParte mapeoCambioArticulo = new MapeoCambioArticuloParte();
    	mapeoCambioArticulo.setIdIncidenciaProgramada(this.mapeoIncidencias.getIdCodigo());
    	
    	vector = this.cncs.recuperarCambioArticuloParte(mapeoCambioArticulo);

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoCambioArticuloParte r_mapeo)
	{
		this.setCreacion(false);
		this.mapeoCambiosArticuloParte= new MapeoCambioArticuloParte();
		this.mapeoCambiosArticuloParte.setIdCodigo(r_mapeo.getIdCodigo());

		if (r_mapeo.getCambioArticulo().contentEquals("S")) chkCambioArticulo.setValue(true);
		if (r_mapeo.getCambioBotella().contentEquals("S")) chkCambioBotella.setValue(true);
		if (r_mapeo.getCambioCaja().contentEquals("S")) chkCambioCaja.setValue(true);
		if (r_mapeo.getCambioTapon().contentEquals("S")) chkCambioRoscaCorcho.setValue(true);
		
		this.txtArticulo.setValue(r_mapeo.getArticulo());
		this.txtHoras.setValue(r_mapeo.getTiempo());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
	}

	private void limpiarCampos()
	{
		this.chkCambioArticulo.setValue(false);
		this.chkCambioBotella.setValue(false);
		this.chkCambioCaja.setValue(false);
		this.chkCambioRoscaCorcho.setValue(false);

		this.chkCambioArticulo.setEnabled(true);
		this.chkCambioBotella.setEnabled(true);
		this.chkCambioCaja.setEnabled(true);
		this.chkCambioRoscaCorcho.setEnabled(true);
		
		this.txtArticulo.setValue("");
		this.lblDescripcionArticulo.setValue("");
		this.txtHoras.setValue("");
		this.txtObservaciones.setValue("");
		
	}
	
    private boolean todoEnOrden()
    {
    	if (this.txtArticulo.getValue()==null || this.txtArticulo.getValue().toString().length()==0 && cambioArticulo)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el articulo");
    		return false;
    	}
    	if (this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el tiempo de Cambio");
    		return false;
    	}
    	if (!chkCambioArticulo.getValue() && !chkCambioBotella.getValue() && !chkCambioCaja.getValue() && !chkCambioRoscaCorcho.getValue() && this.cambioArticulo)
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar al menos una causa de cambio");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		
		consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		cps.guardarHorasCambioArticuloParte(this.mapeoIncidencias);

		this.App.generarGridIncidencias();
		close();
	}

    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoCambioArticuloParte) r_fila)!=null)
    	{
    		btnEliminar.setEnabled(true);
    		MapeoCambioArticuloParte mapeoCambio = (MapeoCambioArticuloParte) r_fila;
    		if (mapeoCambio!=null) this.llenarEntradasDatos(mapeoCambio);
    	}    			
    	else this.limpiarCampos();
    	
    }

    private void generarGrid(ArrayList<MapeoCambioArticuloParte> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Cambio Articulo");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoCambioArticuloParte> container = new BeanItemContainer<MapeoCambioArticuloParte>(MapeoCambioArticuloParte.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
					if (cambioArticulo)
					{
						getColumn("articulo").setHeaderCaption("Articulo");
						getColumn("articulo").setWidth(new Double(120));
						getColumn("cambioArticulo").setHeaderCaption("Cambio Articulo");
						getColumn("cambioArticulo").setWidth(new Double(120));
						getColumn("cambioTapon").setHeaderCaption("Rosca/Corcho");
						getColumn("cambioTapon").setWidth(new Double(130));
						getColumn("cambioBotella").setHeaderCaption("Formato Botella");
						getColumn("cambioBotella").setWidth(new Double(120));
						getColumn("cambioCaja").setHeaderCaption("Caja 6/12");
						getColumn("cambioCaja").setWidth(new Double(120));
					}
			    	
			    	getColumn("tiempo").setHeaderCaption("Hora");
			    	getColumn("tiempo").setWidth(new Double(100));
			    	getColumn("observaciones").setHeaderCaption("Observaciones");
			    	if (!cambioArticulo)
			    		getColumn("observaciones").setWidthUndefined();
			    	else
			    		getColumn("observaciones").setWidth(new Double(100));

					getColumn("idCodigo").setHidden(true);
					getColumn("idIncidenciaProgramada").setHidden(true);
					if (!cambioArticulo)
					{
						getColumn("articulo").setHidden(true);
						getColumn("cambioArticulo").setHidden(true);
						getColumn("cambioCaja").setHidden(true);
						getColumn("cambioTapon").setHidden(true);
						getColumn("cambioBotella").setHidden(true);
						
					}
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					if (cambioArticulo)
						setColumnOrder("articulo", "cambioArticulo", "cambioTapon","cambioBotella", "cambioCaja", "tiempo", "observaciones");
					else
						setColumnOrder("tiempo", "observaciones");
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
				
				
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("smallgrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
	//    	gridDatos.setCellStyleGenerator(new CellStyleGenerator() {
	//    		@Override
	//    		public String getStyle(CellReference cellReference) {
	//    			if (cellReference.getPropertyId().toString().contains("Capacidad")||cellReference.getPropertyId().toString().contains("Numero") || cellReference.getPropertyId().toString().contains("Año")) {
	//    				return "Rcell-normal";
	//    			}
	//    			return null;
	//    		}
	//    	});
			panelGrid.setContent(gridDatos);
		}    	
		else
		{
			setCreacion(true);
		}
    	this.frameGrid.addComponent(panelGrid);
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
	}

	@Override
	public void aceptarProceso(String r_accion) {
		String rdo = null;
		rdo = cncs.eliminarCambioArticuloParte(this.mapeoCambiosArticuloParte.getIdCodigo());
		
		if (rdo!=null) 
			Notificaciones.getInstance().mensajeError(rdo);
		else
			cerrar();

	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}