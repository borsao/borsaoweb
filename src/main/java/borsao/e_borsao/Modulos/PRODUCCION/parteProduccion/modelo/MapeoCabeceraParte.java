package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoCabeceraParte extends MapeoGlobal
{
    private String ejercicio;
    private String semana;
    private String fecha;
    private String linea;
    private String turno;
    private Integer produccion;
    private String observaciones;
    private String tecnicoCalidad;
        
	public MapeoCabeceraParte() 
	{
		
	}

	public String getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getSemana() {
		return semana;
	}

	public void setSemana(String semana) {
		this.semana = semana;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getTurno() {
		return turno;
	}

	public void setTurno(String turno) {
		this.turno = turno;
	}

	public Integer getProduccion() {
		return produccion;
	}

	public void setProduccion(Integer produccion) {
		this.produccion = produccion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getTecnicoCalidad() {
		return tecnicoCalidad;
	}

	public void setTecnicoCalidad(String tecnicoCalidad) {
		this.tecnicoCalidad = tecnicoCalidad;
	}


}