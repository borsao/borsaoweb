package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property.ReadOnlyException;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.converter.Converter.ConversionException;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoCabeceraParte;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoHorasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoIncidenciasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.gridHorasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.gridIncidenciasNoProgramadasCalidadGrid;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.gridIncidenciasNoProgramadasGrid;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.gridIncidenciasProgramadasGrid;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class parteProduccionView extends GridViewRefresh {

	public static final String VIEW_NAME = "Parte Producción";
//	private final String titulo = "Parte Producción";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	
	private VerticalLayout panelCabecera = null;
	
		private HorizontalLayout panelGenerales = null;
			public TextField ejercicio= null;
			public ComboBox cmbSerie = null;
			public ComboBox cmbSemana = null;
			private Button opcBajar = null;
			public EntradaDatosFecha fecha = null;
			private Button opcSubir = null;

			public ComboBox cmbTurno = null;
			public TextArea txtObservaciones=null;
			public TextField produccion= null;
			public TextField horas= null;
			private Combo cmbTecnicos = null;
			
		private VerticalLayout panelHorasTurno = null;
		
			private HorizontalLayout panelControles = null;
			
					private Button btnInsertarOperario = null;
					private Button btnRepartoPorVinos = null;
					private Combo cmbOperario = null;
					private TextField txtHorasOperario = null;
					private TextField txtHorasOperarioAbsentismo = null;
					private TextField txtMotivoOperarioAbsentismo = null;
					private TextField txtHorasOperarioFueraLinea = null;
					private TextField txtMotivoOperarioFueraLinea= null;
				
			private Panel panelGridOperarios = null;
				public Grid gridHorasTurno = null;
				public Grid gridHorasTurnoAbsentismo = null;
			
		private HorizontalLayout panelIncidenciasTurno = null;
			private GridLayout panelGridIncidencias = null;
				public Grid gridIncidenciasProgramadasGrid = null;
				public Grid gridIncidenciasNoProgramadasGrid = null;
				public Grid gridIncidenciasNoProgramadasCalidadGrid = null;
		
	public consultaParteProduccionServer cpps =null;
	public Integer idTurno = null;
	public boolean buscando;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public parteProduccionView()
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
//    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);    	
//    	this.opcImprimir.setVisible(true);
    	this.opcEliminar.setVisible(true);

//    	this.topLayoutL.addComponent(this.opcBuscar);
//    	this.topLayoutL.setMargin(true);
//    	this.topLayoutL.setSpacing(false);
    	
	    	this.panelCabecera = new VerticalLayout();
	    	this.panelCabecera.setCaptionAsHtml(true);
	    	this.panelCabecera.setCaption("<h3><b><center>Parte Trabajo</center></b></h3>");
	    	this.panelCabecera.setSizeUndefined();
	    	this.panelCabecera.setSpacing(true);
	    	
		    	this.panelGenerales = new HorizontalLayout();
		    	this.panelGenerales.setSpacing(true);
//		    	this.panelGenerales.setMargin(true);
//		    	this.panelGenerales.setSizeFull();

			    	this.ejercicio= new TextField("Ejercicio");
			    	this.ejercicio.setEnabled(false);
			    	this.ejercicio.setWidth("100px");
			    	this.ejercicio.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    	this.ejercicio.setValue(RutinasFechas.añoActualYYYY());

		    		this.opcBajar= new Button();
		    		this.opcBajar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcBajar.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcBajar.setIcon(FontAwesome.ARROW_CIRCLE_LEFT);
		    		
		    		this.fecha = new EntradaDatosFecha();
		    		this.fecha.setCaption("Fecha Parte");
		    		this.fecha.setEnabled(false);
		    		this.fecha.setStyleName(ValoTheme.DATEFIELD_TINY);
		    		this.fecha.setWidth("120px");
		    		this.fecha.setDateFormat("dd/MM/yyyy");
		    		this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));

		    		this.opcSubir= new Button();    	
		    		this.opcSubir.addStyleName(ValoTheme.BUTTON_FRIENDLY);
		    		this.opcSubir.addStyleName(ValoTheme.BUTTON_TINY);
		    		this.opcSubir.setIcon(FontAwesome.ARROW_CIRCLE_RIGHT);
			
			    	this.cmbSerie=new ComboBox("Linea");
					this.cmbSerie.addItem("EMBOTELLADO");
					this.cmbSerie.addItem("ENVASADO");
					this.cmbSerie.addItem("BIB");
					this.cmbSerie.setWidth("180px");
					this.cmbSerie.setStyleName(ValoTheme.COMBOBOX_TINY);
					this.cmbSerie.setInvalidAllowed(false);
					this.cmbSerie.setNewItemsAllowed(false);
					this.cmbSerie.setNullSelectionAllowed(false);

					this.cmbTurno=new ComboBox("Turno");
					this.cmbTurno.addItem("MAÑANA");
					this.cmbTurno.addItem("TARDE");
					this.cmbTurno.addItem("NOCHE");
					this.cmbTurno.setWidth("120px");
					this.cmbTurno.setStyleName(ValoTheme.COMBOBOX_TINY);
					this.cmbTurno.setInvalidAllowed(false);
					this.cmbTurno.setNewItemsAllowed(false);
					this.cmbTurno.setNullSelectionAllowed(false);
			    	
					this.produccion=new TextField();
					this.produccion.setEnabled(true);
					this.produccion.setWidth("100px");
					this.produccion.setStyleName(ValoTheme.TEXTFIELD_TINY);
					this.produccion.setCaption("Produccion");
					this.produccion.addStyleName("rightAligned");
					this.produccion.setValue("0");
	
					this.horas=new TextField();
					this.horas.setEnabled(true);
					this.horas.setWidth("80px");
					this.horas.setStyleName(ValoTheme.TEXTFIELD_TINY);
					this.horas.setCaption("Horas Turno:");
					this.horas.addStyleName("rightAligned");
					this.horas.setValue("8");
	
					this.txtObservaciones=new TextArea();
					this.txtObservaciones.setRows(0);
					this.txtObservaciones.setEnabled(false);
					this.txtObservaciones.setWidth("250px");
					this.txtObservaciones.setCaption("Observaciones");
				
					this.cmbTecnicos = new Combo("Tec. Calidad");
					this.cmbTecnicos.setWidth("200px");
					this.cmbTecnicos.setStyleName(ValoTheme.TEXTFIELD_TINY);
					this.cmbTecnicos.setNullSelectionAllowed(false);
					this.cmbTecnicos.setNewItemsAllowed(false);
					this.cmbTecnicos.setVisible(false);

				this.panelGenerales.addComponent(this.ejercicio);
				this.panelGenerales.addComponent(this.opcBajar);
				this.panelGenerales.setComponentAlignment(this.opcBajar,Alignment.MIDDLE_LEFT);
				this.panelGenerales.addComponent(this.fecha);
				this.panelGenerales.addComponent(this.opcSubir);
				this.panelGenerales.setComponentAlignment(this.opcSubir,Alignment.MIDDLE_LEFT);

				this.panelGenerales.addComponent(this.cmbSerie);
				this.panelGenerales.addComponent(this.cmbTecnicos);
				this.panelGenerales.addComponent(this.cmbTurno);
//				this.panelGenerales.addComponent(this.horas);
				this.panelGenerales.addComponent(this.produccion);
				this.panelGenerales.addComponent(this.txtObservaciones);
			
		    	this.panelHorasTurno= new VerticalLayout();
		    	this.panelHorasTurno.setSpacing(true);
		    	this.panelHorasTurno.setSizeUndefined();
		    	
		    		panelControles = new HorizontalLayout();
			    	panelControles.setSpacing(true);
			    	
			    			btnInsertarOperario = new Button("Insertar Operario");
			    			btnInsertarOperario.setStyleName(ValoTheme.BUTTON_TINY);
			    			btnInsertarOperario.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			    			
			    			btnRepartoPorVinos = new Button("Reparto Tiempos Produccion");
			    			btnRepartoPorVinos.setStyleName(ValoTheme.BUTTON_TINY);
			    			btnRepartoPorVinos.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			    			
			    			cmbOperario = new Combo("Operario");
			    			cmbOperario.setWidth("200px");
			    			cmbOperario.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			cmbOperario.setNullSelectionAllowed(false);
			    			cmbOperario.setNewItemsAllowed(true);
			    			
			    			txtHorasOperario = new TextField("Horas");
			    			txtHorasOperario.setWidth("80px");
			    			txtHorasOperario.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			
			    			txtHorasOperarioFueraLinea= new TextField("Horas Fuera Linea");
			    			txtHorasOperarioFueraLinea.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			txtHorasOperarioFueraLinea.setWidth("80px");
			    			txtMotivoOperarioFueraLinea= new TextField("Motivo");
			    			txtMotivoOperarioFueraLinea.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			txtMotivoOperarioFueraLinea.setWidth("200px");
			    			txtHorasOperarioAbsentismo = new TextField("Horas Absentismo");
			    			txtHorasOperarioAbsentismo.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			txtHorasOperarioAbsentismo.setWidth("80px");
			    			txtMotivoOperarioAbsentismo = new TextField("Motivo");
			    			txtMotivoOperarioAbsentismo.setStyleName(ValoTheme.TEXTFIELD_TINY);
			    			txtMotivoOperarioAbsentismo.setWidth("200px");
//			    			
			    			panelControles.addComponent(cmbOperario);
			    			panelControles.addComponent(txtHorasOperario);
			    			panelControles.addComponent(btnInsertarOperario);
			    			panelControles.setComponentAlignment(btnInsertarOperario, Alignment.BOTTOM_LEFT);
			    			panelControles.addComponent(txtHorasOperarioFueraLinea);
			    			panelControles.addComponent(txtMotivoOperarioFueraLinea);
			    			panelControles.addComponent(txtHorasOperarioAbsentismo);
			    			panelControles.addComponent(txtMotivoOperarioAbsentismo);
			    			panelControles.addComponent(btnRepartoPorVinos);
			    			panelControles.setComponentAlignment(btnRepartoPorVinos, Alignment.BOTTOM_LEFT);

		    			panelHorasTurno.addComponent(panelControles);
		    			
    			panelGridOperarios = new Panel();
				panelGridOperarios.setSizeFull();

		    	this.panelIncidenciasTurno= new HorizontalLayout();
		    	this.panelIncidenciasTurno.setSpacing(true);
		    	this.panelIncidenciasTurno.setSizeUndefined();
			
	    	this.panelCabecera.addComponent(this.panelGenerales);
	    	this.panelCabecera.addComponent(this.panelHorasTurno);
		
    	this.cabLayout.addComponent(this.panelCabecera);
    	this.cabLayout.setSizeUndefined();
    	this.cabLayout.setMargin(true);

    	this.barAndGridLayout.addComponent(this.panelGridOperarios);
    	this.barAndGridLayout.setMargin(true);
    	
    	if (eBorsao.get().accessControl.getNombre().contains("Embotelladora"))
		{
			this.cmbSerie.setValue("EMBOTELLADO");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("Envasadora"))
		{
			this.cmbSerie.setValue("ENVASADO");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("bib"))
		{
			this.cmbSerie.setValue("BIB");
		}
		else
		{
			this.cmbSerie.setValue("EMBOTELLADO");
		}
    	this.cmbTecnicos.setVisible(!this.cmbSerie.getValue().toString().contentEquals("ENVASADO"));
    	this.cargarComboOperarios();
    	this.cargarComboTecnicos();
		this.cargarListeners();
		this.generarGridOperarios();
		this.generarGridIncidencias();
		this.establecerSituacion("Inicial");
    }

    public void generarGridIncidencias()
    {
    	ArrayList<MapeoIncidenciasTurno> vectorProgramadas = null;
    	ArrayList<MapeoIncidenciasTurno> vectorNoProgramadas = null;
    	ArrayList<MapeoIncidenciasTurno> vectorNoProgramadasCalidad = null;
    	
    	cpps = new consultaParteProduccionServer(CurrentUser.get());
    	
    	vectorProgramadas=this.cpps.recuperarIncidenciasParte(this.idTurno, "P");
    	vectorNoProgramadas=this.cpps.recuperarIncidenciasParte(this.idTurno, "N");
    	vectorNoProgramadasCalidad=this.cpps.recuperarIncidenciasParte(this.idTurno, "C");
    	
    	this.presentarGrid(vectorProgramadas,vectorNoProgramadas, vectorNoProgramadasCalidad);
    	
    	if (panelIncidenciasTurno!=null)
    	{
    		this.barAndGridLayout.addComponent(panelIncidenciasTurno);
    		this.barAndGridLayout.setExpandRatio(panelIncidenciasTurno,1);
    		this.barAndGridLayout.setSizeUndefined();
    	}
    }

    public void generarGridOperarios()
    {
    	ArrayList<MapeoHorasTurno> vectorHorasTurno = null;
    	
    	consultaParteProduccionServer cpps = new consultaParteProduccionServer(CurrentUser.get());
    	vectorHorasTurno=cpps.recuperarOperariosParte(this.idTurno);
    	
    	this.presentarGridOperarios(vectorHorasTurno);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoIncidenciasTurno> r_vectorProgramadas, ArrayList<MapeoIncidenciasTurno> r_vectorNoProgramadas, ArrayList<MapeoIncidenciasTurno> r_vectorNoProgramadasCalidad)
    {

    	
//    		gridHorasTurno = new gridHorasTurno(null, this, idTurno);
//    		gridHorasTurno.setWidth("100%");
//    		panelGridOperarios.setContent(gridHorasTurno);    		
//    	barAndGridLayout.addComponent(panelGridOperarios);
    		
//    		gridHorasTurnoAbsentismo = new gridHorasTurnoAbsentismo(null, this, idTurno);
//    		panelGridOperarios.addComponent(gridHorasTurnoAbsentismo);
//    		panelGridOperarios.setExpandRatio(gridHorasTurno, 1);
    		
    		
		if (panelIncidenciasTurno!=null)
		{
			gridIncidenciasProgramadasGrid=null;
			gridIncidenciasNoProgramadasGrid=null;
			gridIncidenciasNoProgramadasCalidadGrid=null;
			panelIncidenciasTurno.removeAllComponents();
		}
		panelGridIncidencias = new GridLayout(3,1);
		panelGridIncidencias.setMargin(true);
//		panelGridIncidencias.setHeight("100%");
		
			gridIncidenciasProgramadasGrid = new gridIncidenciasProgramadasGrid(r_vectorProgramadas,this);
		
			panelGridIncidencias.addComponent(gridIncidenciasProgramadasGrid,0,0);

			gridIncidenciasNoProgramadasGrid = new gridIncidenciasNoProgramadasGrid(r_vectorNoProgramadas,this);
			gridIncidenciasNoProgramadasGrid.setEditorEnabled(true);
			
			panelGridIncidencias.addComponent(gridIncidenciasNoProgramadasGrid,1,0);

			gridIncidenciasNoProgramadasCalidadGrid = new gridIncidenciasNoProgramadasCalidadGrid(r_vectorNoProgramadasCalidad,this);
			gridIncidenciasNoProgramadasCalidadGrid.setEditorEnabled(true);
			
			panelGridIncidencias.addComponent(gridIncidenciasNoProgramadasCalidadGrid,2,0);
		
		panelIncidenciasTurno.addComponent(panelGridIncidencias);
		
    }

    private void presentarGridOperarios(ArrayList<MapeoHorasTurno> r_vectorHorasTurno)
    {
    	
//    	if (panelGridOperarios.getContent()!=null)
//    	{
//    		gridHorasTurno.removeAllColumns();
//    		gridHorasTurno=null;
//    		panelGridOperarios.setContent(null);
//    	}
//    	
//    	gridHorasTurno = new gridHorasTurno(r_vectorHorasTurno, this, idTurno);
//    	gridHorasTurno.setWidth("100%");
//    	panelGridOperarios.setContent(gridHorasTurno);
    	if (panelGridOperarios!=null && (r_vectorHorasTurno== null || r_vectorHorasTurno.isEmpty()))
		{
    		gridHorasTurno=null;
    		panelGridOperarios.setContent(null);
		}
    	if (gridHorasTurno!=null)
    	{
    		((GridPropio) gridHorasTurno).setRecords(r_vectorHorasTurno);
    		((GridPropio) gridHorasTurno).calcularTotal();
    		gridHorasTurno.refreshAllRows();
    	}
    	else
    	{
        	gridHorasTurno = new gridHorasTurno(r_vectorHorasTurno, this, idTurno);
        	gridHorasTurno.setWidth("100%");
        	panelGridOperarios.setContent(gridHorasTurno);    		
    	}
    }
    
    private void cargarListeners()
    {

    	this.opcBajar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {

				if (fecha.getValue()!=null)
				{
					try {
						fecha.setValue(RutinasFechas.convertirADate(RutinasFechas.restarDiasFecha(RutinasFechas.convertirDateToString(fecha.getValue()), 1)));
					} catch (ReadOnlyException e) {
						e.printStackTrace();
					} catch (ConversionException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (datosIdentificativosParte())
					{
						recuperarTurno();	
					}
				}
			}
		});

    	this.opcSubir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (fecha.getValue()!=null)
    			{
	    			try {
	    				fecha.setValue(RutinasFechas.convertirADate(RutinasFechas.sumarDiasFecha(RutinasFechas.convertirDateToString(fecha.getValue()), 1)));
	    			} catch (ReadOnlyException e) {
	    				e.printStackTrace();
	    			} catch (ConversionException e) {
	    				e.printStackTrace();
	    			} catch (Exception e) {
	    				e.printStackTrace();
	    			}
	    			if (datosIdentificativosParte())
	    			{
	    				recuperarTurno();	
	    			}
    			}
    		}
    	});
    	
    	this.cmbSerie.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
			
				if (cmbSerie.getValue()!=null)
				{
					cargarComboOperarios();
					cmbTecnicos.setVisible(!cmbSerie.getValue().toString().contentEquals("ENVASADO"));
					
				}
				
				if (datosIdentificativosParte())
				{
					/*
					 * creamos idTurno y generamos grid incidencias con idTurno relleno
					 */
					recuperarTurno();
				}
			}
		});
    	this.cmbTurno.addValueChangeListener(new ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			
    			if (datosIdentificativosParteCreacion())
    			{
    				/*
    				 * creamos idTurno y generamos grid incidencias con idTurno relleno
    				 */
    				recuperarTurno();
    			}
    			else
    			{
    				cmbTurno.setValue(null);
    				cmbTecnicos.focus();
    			}
    		}
    	});

    	this.fecha.addValueChangeListener(new ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			
    			if (datosIdentificativosParte())
    			{
    				/*
    				 * creamos idTurno y generamos grid incidencias con idTurno relleno
    				 */
    				recuperarTurno();
    			}
    		}
    	});
    	this.ejercicio.addValueChangeListener(new ValueChangeListener() {
    		
    		@Override
    		public void valueChange(ValueChangeEvent event) {
    			
    			if (datosIdentificativosParte())
    			{
    				/*
    				 * creamos idTurno y generamos grid incidencias con idTurno relleno
    				 */
    				recuperarTurno();
    			}
    		}
    	});
    	this.btnInsertarOperario.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {

				if (todoEnOrden())
				{
					guardarOperariosTurno();
				}
				else
				{
					Notificaciones.getInstance().mensajeError("Rellena correctamente los datos");
					cmbOperario.focus();
				}
			}
		});
    	this.btnRepartoPorVinos.addClickListener(new ClickListener() {
    		
    		@Override
    		public void buttonClick(ClickEvent event) 
    		{
    			actualizarTiemposOperariosTurno();
    		}
    	});
    }

	@Override
	public void reestablecerPantalla() {
		
	}
	
	@Override
	public void print() 
	{
		String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaParteProduccionServer cpps = consultaParteProduccionServer.getInstance(CurrentUser.get());
    	pdfGenerado = cpps.imprimirParteProduccion(this.ejercicio.getValue() , String.valueOf(RutinasFechas.obtenerSemanaFecha(this.fecha.getValue())), RutinasFechas.convertirDateToString(this.fecha.getValue()), this.cmbSerie.getValue().toString());
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		
		pdfGenerado = null;
		botonesGenerales(true);
		establecerSituacion("Encontrado");
	}
	
	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}
	
	@Override
	public void newForm() {
//		
		if (opcNuevo.getCaption().equals("Crear"))
		{
			this.limpiarCampos();
			this.establecerSituacion("Inicial");
		}
		else if (opcNuevo.getCaption().equals("Guardar"))
		{
			if (datosIdentificativosParteCreacion())
				this.crearTurno();
			else
				Notificaciones.getInstance().mensajeError("Rellena correctamente los campos.");
		}
	}
	
	@Override
	public void verForm(boolean r_busqueda) 
	{
		
		/*
		 * Abrimos pantalla peticion busqueda controles de embotellado
		 * Tendra un grid con los encontrados y devolvera el prd_idcontrolembotellado que hemos seleccionado
		 * con este rellenaremos el campo numerador y devolveremos el registro 
		 */
		this.fecha.setValue("");
		this.cmbSerie.setValue(null);
		this.cmbTurno.setValue(null);
		this.limpiarCampos();
		this.establecerSituacion("Inicial");
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}
	
	public void establecerSituacion (String r_modo)
	{
		
		switch (r_modo)
		{
			case "Inicial":
			{

				this.setBuscando(false);
				
				this.fecha.setEnabled(true);
				this.cmbSerie.setEnabled(true);
				this.cmbTecnicos.setEnabled(true);
				this.cmbTurno.setEnabled(true);
				this.ejercicio.setEnabled(true);
				
//				this.opcBuscar.setEnabled(true);
//				this.opcBuscar.setVisible(true);
				this.opcImprimir.setEnabled(false);
				this.opcEliminar.setEnabled(false);
				this.opcSalir.setEnabled(true);
				this.opcNuevo.setEnabled(true);
				this.opcNuevo.setCaption("Crear");
				
				this.horas.setEnabled(false);
				this.produccion.setEnabled(false);
				this.cmbOperario.setEnabled(false);
				this.txtObservaciones.setEnabled(false);
				this.txtHorasOperario.setEnabled(false);
				this.txtHorasOperarioFueraLinea.setEnabled(false);
				this.txtMotivoOperarioFueraLinea.setEnabled(false);
				this.txtHorasOperarioAbsentismo.setEnabled(false);
				this.txtMotivoOperarioAbsentismo.setEnabled(false);
				this.btnInsertarOperario.setEnabled(false);
				this.btnRepartoPorVinos.setEnabled(false);
//				this.btnInsertarOperarioAbsentismo.setEnabled(false);
				
				gridHorasTurno.setEnabled(false);
//				gridHorasTurnoAbsentismo.setEnabled(false);
				gridIncidenciasNoProgramadasCalidadGrid.setEnabled(false);
				gridIncidenciasNoProgramadasGrid.setEnabled(false);
				gridIncidenciasProgramadasGrid.setEnabled(false);
				
				this.fecha.setValue("");
				break;
			}
			case "Creacion":
			{
				this.setBuscando(false);

				this.ejercicio.setEnabled(true);
				this.fecha.setEnabled(true);
//				this.txtObservaciones.setEnabled(true);
				this.produccion.setEnabled(true);
				this.horas.setEnabled(true);
				this.cmbTurno.setEnabled(true);
				this.cmbSerie.setEnabled(true);
				this.btnRepartoPorVinos.setEnabled(false);
				this.btnInsertarOperario.setEnabled(true);
				
				this.cmbTecnicos.setEnabled(true);
				this.cmbOperario.setEnabled(true);
				this.txtHorasOperario.setEnabled(true);
				this.txtObservaciones.setEnabled(true);
				this.txtHorasOperarioFueraLinea.setEnabled(true);
				this.txtMotivoOperarioFueraLinea.setEnabled(true);
				this.txtHorasOperarioAbsentismo.setEnabled(true);
				this.txtMotivoOperarioAbsentismo.setEnabled(true);
				this.btnInsertarOperario.setEnabled(true);
				this.btnRepartoPorVinos.setEnabled(true);

				this.txtHorasOperario.setValue("0");
				this.txtHorasOperarioFueraLinea.setValue("0");
				this.txtHorasOperarioAbsentismo.setValue("0");

				opcNuevo.setEnabled(true);
				opcNuevo.setCaption("Guardar");
				opcSalir.setEnabled(true);
				opcEliminar.setEnabled(false);
				opcBuscar.setEnabled(false);
				opcImprimir.setEnabled(true);

				gridHorasTurno.setEnabled(true);
				gridIncidenciasNoProgramadasCalidadGrid.setEnabled(true);
				gridIncidenciasNoProgramadasGrid.setEnabled(true);
				gridIncidenciasProgramadasGrid.setEnabled(true);

				break;
			}
			case "Encontrado":
			{
				
				this.fecha.setEnabled(true);
				this.cmbSerie.setEnabled(true);
				this.cmbTurno.setEnabled(true);
				this.ejercicio.setEnabled(true);

				this.txtObservaciones.setEnabled(true);
				this.produccion.setEnabled(true);
				this.cmbTecnicos.setEnabled(true);
				this.cmbOperario.setEnabled(true);
				this.txtHorasOperario.setEnabled(true);
				this.txtHorasOperarioFueraLinea.setEnabled(true);
				this.txtMotivoOperarioFueraLinea.setEnabled(true);
				this.txtHorasOperarioAbsentismo.setEnabled(true);
				this.txtMotivoOperarioAbsentismo.setEnabled(true);
				
				this.txtHorasOperario.setValue("0");
				this.txtHorasOperarioFueraLinea.setValue("0");
				this.txtHorasOperarioAbsentismo.setValue("0");

				gridHorasTurno.setEnabled(true);
				gridIncidenciasNoProgramadasCalidadGrid.setEnabled(true);
				gridIncidenciasNoProgramadasGrid.setEnabled(true);
				gridIncidenciasProgramadasGrid.setEnabled(true);

				this.setBuscando(false);
				this.btnInsertarOperario.setEnabled(true);
				this.btnRepartoPorVinos.setEnabled(true);
				this.opcEliminar.setEnabled(true);
				this.opcImprimir.setEnabled(true);
//				this.opcBuscar.setEnabled(true);
				this.opcSalir.setEnabled(true);
				this.opcNuevo.setEnabled(true);
				this.opcNuevo.setCaption("Guardar");
				break;
			}
		}
	}

	private void limpiarCampos()
	{
		this.produccion.setValue("0");
		this.horas.setValue("0");
		this.cmbOperario.setValue("");
		this.txtHorasOperario.setValue("0");
		this.txtHorasOperarioAbsentismo.setValue("0");
		this.txtHorasOperarioFueraLinea.setValue("0");
		this.txtMotivoOperarioFueraLinea.setValue("");
		this.txtMotivoOperarioAbsentismo.setValue("");
		this.txtObservaciones.setValue("");
		this.generarGridOperarios();
		this.generarGridIncidencias();
	}


	@Override
	public void eliminarRegistro() 
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres Eliminar el parte completo", "Si", "No", "aceptarEliminar", "cancelarEliminar");
		getUI().addWindow(vt);
		
//		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(CurrentUser.get());
//		Integer idGreensys = null;
//		
//		if (this.lblIdGreensys.getCaption()!=null) idGreensys= new Integer(this.lblIdGreensys.getCaption());
//			
//		boolean rdo = ces.eliminar(cmbSerie.getValue().toString(), new Integer(this.numero.getValue()),idGreensys);
//		if (rdo) Notificaciones.getInstance().mensajeInformativo("Registro eliminado correctamente");
//		else Notificaciones.getInstance().mensajeInformativo("No se ha podido eliminar el registro");
//		
//		botonesGenerales(true);
//		limpiarCampos();
//		establecerSituacion("Inicial");
//		limpiarCampos();
	}
	
	public void cerrarVentanaBusqueda()
	{
//		if (this.numeradorSeleccionado!=null)
//		{
//			this.setBuscando(true);
//			this.cmbSerie.setValue(this.areaSeleccionada.toString());
//			this.numero.setValue(this.numeradorSeleccionado.toString());
//			
//			limpiarCampos();
//			consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
//			mapeoControl = ces.recuperarControlEmbotellado(cmbSerie.getValue().toString(), numero.getValue());
//			if (isHayGrid2())
//			{
//				grid2.removeAllColumns();			
//				panGrid2.removeComponent(grid2);
//				grid2=null;						
//			}
//			if (isHayGrid3())
//			{
//				grid3.removeAllColumns();			
//				panGrid3.removeComponent(grid3);
//				grid3=null;						
//			}
//			
//			if (mapeoControl!=null)
//			{
//				rellenarEntradasDatos(mapeoControl);
//				r_vector2 = ces.recuperarEscandallo(cmbSerie.getValue().toString(), numero.getValue());
//				r_vector3 = ces.recuperarNumeracionConsejo(cmbSerie.getValue().toString(), numero.getValue());
//				presentarGrid(r_vector2, r_vector3);
//				establecerSituacion("Encontrado");
//			}
//		}
//		else
//			this.establecerSituacion("Inicial");
	}
	@Override
	public void aceptarProceso(String r_accion) {
		switch (r_accion)
		{
			case "aceptarEliminar":
			{
				consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
				String rdo = cps.eliminarParte(this.idTurno);
				if (rdo!=null)
				{
					Notificaciones.getInstance().mensajeError("No se pudo eliminar el parte. " + rdo.toString());
				}
				else
				{
					this.limpiarCampos();
					this.cmbSerie.setValue(null);
					this.cmbTecnicos.setValue(null);
					this.cmbTurno.setValue(null);
					this.fecha.setValue("");
					this.establecerSituacion("Inicial");
					this.fecha.focus();
				}
				break;
			}
			case "aceptarCrear":
			{
				this.opcNuevo.setCaption("Crear");
				if (datosIdentificativosParteCreacion())
					this.crearTurno();
				else
					Notificaciones.getInstance().mensajeError("Rellena correctamente los campos.");
				
				break;
			}
		}
	}

	@Override
	public void cancelarProceso(String r_accion) 
	{
		switch (r_accion)
		{
			case "cancelarEliminar":
			{
				Notificaciones.getInstance().mensajeError("No se quiere eliminar el parte");
				this.establecerSituacion("Encontrado");
				break;
			}
			case "cancelarCrear":
			{
				this.limpiarCampos();
				this.cmbSerie.setValue(null);
				this.cmbTurno.setValue(null);
				this.fecha.setValue("");
				this.establecerSituacion("Inicial");
				this.fecha.focus();
				break;
			}
		}
	}
	
	private boolean datosIdentificativosParte()
	{
		boolean correcto = false;
		
		if (this.fecha.getValue()!=null && 
				this.ejercicio.getValue()!=null && 
				this.ejercicio.getValue().length()>0 && 
				this.cmbSerie.getValue()!=null && 
				this.cmbSerie.getValue().toString().length()>0 && 
				this.cmbTurno.getValue()!=null)
			correcto = true;
		else
			correcto = false;
		return correcto;
	}

	private boolean datosIdentificativosParteCreacion()
	{
		boolean correcto = false;
		
		if (this.fecha.getValue()!=null && 
				this.ejercicio.getValue()!=null && 
				this.ejercicio.getValue().length()>0 && 
				this.cmbSerie.getValue()!=null && !this.cmbSerie.getValue().toString().contentEquals("ENVASADO") &&  
				this.cmbSerie.getValue().toString().length()>0 && 
				this.cmbTurno.getValue()!=null && 
				this.cmbTecnicos.getValue()!=null)
			correcto = true;
		else if (this.fecha.getValue()!=null && 
				this.ejercicio.getValue()!=null && 
				this.ejercicio.getValue().length()>0 && 
				this.cmbSerie.getValue()!=null && this.cmbSerie.getValue().toString().contentEquals("ENVASADO") &&  
				this.cmbSerie.getValue().toString().length()>0 && 
				this.cmbTurno.getValue()!=null)
			correcto = true;
		else
			
			correcto = false;
		return correcto;
	}
	
	private void crearTurno()
	{
		String rdo = null;
		/*
		 * Proceso de creacion del turno
		 * 
		 * Obtendrremos el siguiente idTurno para asignarlo e inicializaremos todos los datos
		 * y generaremos los grid de incidencias con todos los datos y el idTurno recogido.
		 */
		consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		MapeoCabeceraParte mapeo = new MapeoCabeceraParte();
		
		try
		{
			mapeo.setEjercicio(this.ejercicio.getValue());
			mapeo.setFecha(RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(this.fecha.getValue())));
			mapeo.setSemana(String.valueOf(RutinasFechas.obtenerSemanaFecha(this.fecha.getValue())));
			mapeo.setLinea(this.cmbSerie.getValue().toString());
			mapeo.setTurno(this.cmbTurno.getValue().toString());
			mapeo.setObservaciones(this.txtObservaciones.getValue());
			if (this.cmbSerie.getValue().toString().contentEquals("ENVASADO"))
				mapeo.setTecnicoCalidad("envasadora");
			else
				mapeo.setTecnicoCalidad(this.cmbTecnicos.getValue().toString());
			
			if (this.produccion.getValue()!=null && this.produccion.getValue().length()>0)
			{
				try
				{
					mapeo.setProduccion(new Integer(this.produccion.getValue()));
				}
				catch (Exception e) {
					mapeo.setProduccion(new Integer(0));
				}
			}
			else
				mapeo.setProduccion(new Integer(0));
			
			if (opcNuevo.getCaption().equals("Guardar"))
			{
				mapeo.setIdCodigo(this.idTurno);
				rdo = cps.guardarCambiosCabeceraParte(mapeo);
				if (rdo==null)
				{
					this.establecerSituacion("Encontrado");
				}
			}
			else
			{
				rdo = cps.guardarCabeceraParte(mapeo);
				if (rdo==null)
				{
					this.idTurno = mapeo.getIdCodigo();
					this.establecerSituacion("Creacion");
					this.limpiarCampos();
				}
			}
			
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
	}

	private void recuperarTurno()
	{
		/*
		 * Proceso de busqeuda del turno
		 * 
		 * Si lo encontramos asignamos el id a la variable de control idTurno y rellenarmeos todos los datos
		 * En caso contrario quedará nula y llamaremos a crear el turno
		 */
		consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		try
		{
			MapeoCabeceraParte mapeo  = cps.recuperarParte(this.ejercicio.getValue(), RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(this.fecha.getValue())), this.cmbSerie.getValue().toString(), this.cmbTurno.getValue().toString());
			if (mapeo==null)
			{
				VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres crear el parte de " + this.cmbSerie.getValue().toString() + " del " + RutinasFechas.obtenerDiaDeLaSemanaLetra(this.fecha.getValue()) + " " + RutinasFechas.convertirDateToString(this.fecha.getValue()) + " y turno " + this.cmbTurno.getValue().toString(), "Si", "No", "aceptarCrear", "cancelarCrear");
				getUI().addWindow(vt);
			}
			else
			{
				this.idTurno = mapeo.getIdCodigo();
				if (mapeo.getProduccion()!=null && mapeo.getProduccion().toString().length()>0)this.produccion.setValue(mapeo.getProduccion().toString());
				this.txtObservaciones.setValue(mapeo.getObservaciones());
				this.cmbTecnicos.setValue(mapeo.getTecnicoCalidad());
				
				this.generarGridOperarios();
				this.generarGridIncidencias();
				this.establecerSituacion("Encontrado");
			}
		}
		catch (Exception ex)
		{
			Notificaciones.getInstance().mensajeError(ex.getMessage());
		}
	}
	
	private void guardarOperariosTurno()
	{
		consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		
		MapeoHorasTurno mapeo = new MapeoHorasTurno();
		mapeo.setOperario(cmbOperario.getValue().toString());
		mapeo.setHorasTotalesLinea(txtHorasOperario.getValue());
		mapeo.setHorasTotalesFueraLinea(txtHorasOperarioFueraLinea.getValue());
		mapeo.setMotivoFueraLinea(txtMotivoOperarioFueraLinea.getValue());
		mapeo.setHorasAbsentismo(txtHorasOperarioAbsentismo.getValue());
		mapeo.setMotivoAbsentismo(txtMotivoOperarioAbsentismo.getValue());
		mapeo.setIdTurno(this.idTurno);
		
		String rdo = cps.guardarOperariosParte(mapeo);
		if (rdo!=null)
		{
			Notificaciones.getInstance().mensajeError("Se produjo un error al guardar el operario");
		}
		else
		{
			this.generarGridOperarios();
			this.cmbOperario.setValue(null);
//			this.txtHorasOperario.setValue("0");
//			this.txtHorasOperarioAbsentismo.setValue("0");
//			this.txtHorasOperarioFueraLinea.setValue("0");
//			this.txtMotivoOperarioFueraLinea.setValue("");
//			this.txtMotivoOperarioAbsentismo.setValue("");
			this.txtObservaciones.setValue("");
		}
	}

	private void actualizarTiemposOperariosTurno()
	{
		
		MapeoCabeceraParte mapeoCabecera = new MapeoCabeceraParte();
		mapeoCabecera.setTurno(cmbTurno.getValue().toString());
		mapeoCabecera.setLinea(cmbSerie.getValue().toString());
		mapeoCabecera.setEjercicio(ejercicio.getValue().toString());
		mapeoCabecera.setFecha(RutinasFechas.convertirDateToString(fecha.getValue()));
		mapeoCabecera.setIdCodigo(this.idTurno);
		
		MapeoHorasTurno mapeo = new MapeoHorasTurno();
		mapeo.setHorasTotalesLinea(txtHorasOperario.getValue());
		mapeo.setHorasTotalesFueraLinea(txtHorasOperarioFueraLinea.getValue());
		mapeo.setMotivoFueraLinea(txtMotivoOperarioFueraLinea.getValue());
		mapeo.setHorasAbsentismo(txtHorasOperarioAbsentismo.getValue());
		mapeo.setMotivoAbsentismo(txtMotivoOperarioAbsentismo.getValue());
		mapeo.setIdTurno(this.idTurno);
		
		pantallaActualizacionOperariosParte vt = new pantallaActualizacionOperariosParte(this, mapeoCabecera, mapeo, "Actualizacion Tiempos Productivos");
		getUI().addWindow(vt);
	}
	
	public boolean isBuscando() {
		return buscando;
	}

	public void setBuscando(boolean buscando) {
		this.buscando = buscando;
	}

	private boolean todoEnOrden()
	{
		boolean correcto = false;

		if (cmbOperario.getValue()==null) 
			correcto =false;
		else if ((txtMotivoOperarioAbsentismo.getValue()!=null && txtMotivoOperarioAbsentismo.getValue().length()> 0 && (txtHorasOperarioAbsentismo.getValue()==null || txtHorasOperarioAbsentismo.getValue().length()==0)))
			correcto = false;
		else if ((txtMotivoOperarioAbsentismo.getValue()==null || txtMotivoOperarioAbsentismo.getValue().length()== 0 && (txtHorasOperarioAbsentismo.getValue()!=null && txtHorasOperarioAbsentismo.getValue().length()>0 && !txtHorasOperarioAbsentismo.getValue().toString().contentEquals("0"))))
			correcto = false;
		else if ((txtMotivoOperarioFueraLinea.getValue()!=null && txtMotivoOperarioFueraLinea.getValue().length()> 0 && (txtHorasOperarioFueraLinea.getValue()==null || txtHorasOperarioFueraLinea.getValue().length()==0)))
			correcto = false;
		else if ((txtMotivoOperarioFueraLinea.getValue()==null || txtMotivoOperarioFueraLinea.getValue().length()== 0 && (txtHorasOperarioFueraLinea.getValue()!=null && txtHorasOperarioFueraLinea.getValue().length()>0 && !txtHorasOperarioFueraLinea.getValue().toString().contentEquals("0"))))
			correcto = false;
		else 
		{
			correcto = true;
		}
		return correcto;
	}
	private void cargarComboOperarios()
	{
		cmbOperario.removeAllItems();
		
    	consultaOperariosServer cos = new consultaOperariosServer(CurrentUser.get());
    	ArrayList<MapeoOperarios> vector = null;
    	
    	MapeoOperarios mapeo = new MapeoOperarios();
    	if (this.cmbSerie.getValue().toString().contentEquals("EMBOTELLADO"))
    		mapeo.setArea("Embotelladora");
    	else if (this.cmbSerie.getValue().toString().contentEquals("ENVASADO"))
    		mapeo.setArea("Envasadora");
    	else
    		mapeo.setArea("Embotelladora','Envasadora");
    	vector = cos.datosOperariosInArea(mapeo);
    	
    	for (int i = 0; i< vector.size();i++)
    	{
    		mapeo=vector.get(i);
    		cmbOperario.addItem(mapeo.getNombre());
    	}
	}

	private void cargarComboTecnicos()
	{
		cmbTecnicos.removeAllItems();
		
		consultaOperariosServer cos = new consultaOperariosServer(CurrentUser.get());
		ArrayList<MapeoOperarios> vector = null;
		
		MapeoOperarios mapeo = new MapeoOperarios();
		if (this.cmbSerie.getValue().toString().contentEquals("EMBOTELLADO"))
			mapeo.setArea("Calidad");
		else if (this.cmbSerie.getValue().toString().contentEquals("ENVASADO"))
			mapeo.setArea("Calidad");
		else
			mapeo.setArea("Calidad','Calidad");
		vector = cos.datosOperariosInArea(mapeo);
		
		for (int i = 0; i< vector.size();i++)
		{
			mapeo=vector.get(i);
			cmbTecnicos.addItem(mapeo.getNombre());
		}
		cmbTecnicos.addItem("");
	}

	public void actualizarDatosOperario(MapeoHorasTurno r_mapeo)
	{
		consultaParteProduccionServer cps = consultaParteProduccionServer.getInstance(CurrentUser.get());
		String rdo = cps.guardarTiemposProduccionParte(r_mapeo);
		
		if (rdo != null)
		{
			Notificaciones.getInstance().mensajeError("Error al actualizar los tiempos productivos del parte");
		}
		else
		{
			this.generarGridOperarios();
		}
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	
}

