package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.ClasesPropias.graficas.MapeoBarras;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoCabeceraParte;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoHorasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoIncidenciasTurno;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaParteProduccionServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaParteProduccionServer instance;
	
	public consultaParteProduccionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaParteProduccionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaParteProduccionServer(r_usuario);			
		}
		return instance;
	}
	
	public MapeoCabeceraParte recuperarParte(String r_ejercicio, String r_fecha, String r_linea, String r_turno)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		MapeoCabeceraParte mapeoParte = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_cabecera_parte.idCodigo cab_id,");
		cadenaSQL.append(" prd_cabecera_parte.ejercicio cab_ej,");
		cadenaSQL.append(" prd_cabecera_parte.fecha cab_fec,");
		cadenaSQL.append(" prd_cabecera_parte.semana cab_sem,");
		cadenaSQL.append(" prd_cabecera_parte.linea cab_lin,");
		cadenaSQL.append(" prd_cabecera_parte.turno cab_tur,");
		cadenaSQL.append(" prd_cabecera_parte.buenas cab_pro, ");
		cadenaSQL.append(" prd_cabecera_parte.observaciones cab_obs, ");
		cadenaSQL.append(" prd_cabecera_parte.tecnicoCalidad cab_tec ");
		cadenaSQL.append(" from prd_cabecera_parte ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and prd_cabecera_parte.fecha = '" + r_fecha + "' ");
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		cadenaSQL.append(" and prd_cabecera_parte.turno = '" + r_turno + "' ");

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeoParte = new MapeoCabeceraParte();
				/*
				 * recojo mapeo operarios
				 */
				mapeoParte.setIdCodigo(rsOpcion.getInt("cab_id"));
				mapeoParte.setEjercicio(rsOpcion.getString("cab_ej"));
				mapeoParte.setSemana(rsOpcion.getString("cab_sem"));
				mapeoParte.setLinea(rsOpcion.getString("cab_lin"));
				mapeoParte.setTurno(rsOpcion.getString("cab_tur"));
				mapeoParte.setProduccion(rsOpcion.getInt("cab_pro"));
				mapeoParte.setObservaciones(rsOpcion.getString("cab_obs"));
				mapeoParte.setTecnicoCalidad(rsOpcion.getString("cab_tec"));
				
				return mapeoParte;				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return mapeoParte;
	}
	public ArrayList<MapeoHorasTurno> recuperarOperariosParte(Integer r_turno)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		MapeoHorasTurno mapeoOperarios= null;
		ArrayList<MapeoHorasTurno> vector = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_operarios_parte.idCodigo cab_id,");
		cadenaSQL.append(" prd_operarios_parte.operario ope_ope,");
		cadenaSQL.append(" prd_operarios_parte.horasAUTOMATICO ope_aut,");
		cadenaSQL.append(" prd_operarios_parte.horasJAULON ope_jau,");
		cadenaSQL.append(" prd_operarios_parte.horasETIQUETADO ope_eti,");
		cadenaSQL.append(" prd_operarios_parte.horasMANUAL ope_man,");
		cadenaSQL.append(" prd_operarios_parte.horas ope_hor,");
		cadenaSQL.append(" prd_operarios_parte.fuera_linea ope_hfl,");
		cadenaSQL.append(" prd_operarios_parte.motivo_fuera_linea ope_mfl,");
		cadenaSQL.append(" prd_operarios_parte.ausencia ope_aus,");
		cadenaSQL.append(" prd_operarios_parte.motivo_ausencia ope_maus,");
		cadenaSQL.append(" prd_operarios_parte.idCabeceraParte ope_cab ");
		cadenaSQL.append(" from prd_operarios_parte ");
		cadenaSQL.append(" where prd_operarios_parte.idCabeceraParte = " + r_turno );
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoHorasTurno>();
			while(rsOpcion.next())
			{
				mapeoOperarios = new MapeoHorasTurno();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOperarios.setIdCodigo(rsOpcion.getInt("cab_id"));
				mapeoOperarios.setIdTurno(rsOpcion.getInt("ope_cab"));
				mapeoOperarios.setOperario(rsOpcion.getString("ope_ope"));
				mapeoOperarios.setMotivoFueraLinea(rsOpcion.getString("ope_mfl"));
				mapeoOperarios.setMotivoAbsentismo(rsOpcion.getString("ope_maus"));

				if (rsOpcion.getString("ope_aut")!=null && rsOpcion.getString("ope_aut").length()>0 && !rsOpcion.getString("ope_aut").contentEquals("0.00")) mapeoOperarios.setHorasAUTOMATICO(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_aut"))); else mapeoOperarios.setHorasAUTOMATICO("");
				if (rsOpcion.getString("ope_jau")!=null && rsOpcion.getString("ope_jau").length()>0 && !rsOpcion.getString("ope_jau").contentEquals("0.00")) mapeoOperarios.setHorasJAULON(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_jau"))); else mapeoOperarios.setHorasJAULON("");
				if (rsOpcion.getString("ope_eti")!=null && rsOpcion.getString("ope_eti").length()>0 && !rsOpcion.getString("ope_eti").contentEquals("0.00")) mapeoOperarios.setHorasETIQUETADO(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_eti"))); else mapeoOperarios.setHorasETIQUETADO("");
				if (rsOpcion.getString("ope_man")!=null && rsOpcion.getString("ope_man").length()>0 && !rsOpcion.getString("ope_man").contentEquals("0.00")) mapeoOperarios.setHorasMANUAL(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_man"))); else mapeoOperarios.setHorasMANUAL("");
				if (rsOpcion.getString("ope_hor")!=null && rsOpcion.getString("ope_hor").length()>0 && !rsOpcion.getString("ope_hor").contentEquals("0.00")) mapeoOperarios.setHorasTotalesLinea(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_hor"))); else mapeoOperarios.setHorasTotalesLinea("");
				if (rsOpcion.getString("ope_hfl")!=null && rsOpcion.getString("ope_hfl").length()>0 && !rsOpcion.getString("ope_hfl").contentEquals("0.00")) mapeoOperarios.setHorasTotalesFueraLinea(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_hfl"))); else mapeoOperarios.setHorasTotalesFueraLinea("");
				if (rsOpcion.getString("ope_aus")!=null && rsOpcion.getString("ope_aus").length()>0 && !rsOpcion.getString("ope_aus").contentEquals("0.00")) mapeoOperarios.setHorasAbsentismo(RutinasCadenas.reemplazarPuntoMiles(rsOpcion.getString("ope_aus"))); else mapeoOperarios.setHorasAbsentismo("");
				
				vector.add(mapeoOperarios);				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public Double recuperarHorasParteSemana(String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Double horasLinea = new Double(0);
		
		if (r_linea.contentEquals("BIB"))
		{
			cadenaSQL.append(" SELECT round(sum(horas)/count(operario)*count(distinct idCabeceraParte),2) ope_hor ");
		}
		else
		{
			cadenaSQL.append(" SELECT round(sum(prd_operarios_parte.horas)/4,2) ope_hor ");
		}
		cadenaSQL.append(" from prd_operarios_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_operarios_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.semana = " + r_semana);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				if (rsOpcion.getString("ope_hor")!=null && rsOpcion.getString("ope_hor").length()>0 && !rsOpcion.getString("ope_hor").contentEquals("0.00")) horasLinea=rsOpcion.getDouble("ope_hor");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horasLinea;
	}

	public Double recuperarHorasSemana(String r_horas, String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Double horasLinea = new Double(0);
		
		cadenaSQL.append(" SELECT round(sum(prd_operarios_parte."+ r_horas + ")/4,2) ope_hor ");
		cadenaSQL.append(" from prd_operarios_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_operarios_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.semana = " + r_semana);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				if (rsOpcion.getString("ope_hor")!=null && rsOpcion.getString("ope_hor").length()>0 && !rsOpcion.getString("ope_hor").contentEquals("0.00")) horasLinea=rsOpcion.getDouble("ope_hor");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horasLinea;
	}

	public Double recuperarHorasFueraParteSemana(String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Double horasLinea = new Double(0);
		
		cadenaSQL.append(" SELECT round(sum(prd_operarios_parte.fuera_linea)/4,2) ope_hor ");
		cadenaSQL.append(" from prd_operarios_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_operarios_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.semana = " + r_semana);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				/*
				 * recojo mapeo operarios
				 */
				if (rsOpcion.getString("ope_hor")!=null && rsOpcion.getString("ope_hor").length()>0 && !rsOpcion.getString("ope_hor").contentEquals("0.00")) horasLinea=rsOpcion.getDouble("ope_hor");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horasLinea;
	}
	
	public String imprimirParteProduccion(String r_ejercicio, String r_semana, String r_fecha, String r_linea)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio);
		libImpresion.setCodigo2Txt(r_semana);
		libImpresion.setArchivoDefinitivo("/programacion" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		
		if (r_linea.equals("Embotelladora"))
		{
			libImpresion.setArchivoPlantilla("ListadoParteProduccionEmbotelladora.jrxml");
		}
		else if (r_linea.equals("Envasadora"))
		{
			libImpresion.setArchivoPlantilla("ListadoParteProduccionEnvasadora.jrxml");
		}
		else
		{
			libImpresion.setArchivoPlantilla("ListadoParteProduccionBib.jrxml");
		}
		
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("partesProduccion");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public String guardarCabeceraParte(MapeoCabeceraParte r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_cabecera_parte( ");
			cadenaSQL.append(" prd_cabecera_parte.idCodigo,");
			cadenaSQL.append(" prd_cabecera_parte.ejercicio ,");
			cadenaSQL.append(" prd_cabecera_parte.semana ,");
			cadenaSQL.append(" prd_cabecera_parte.fecha ,");
			cadenaSQL.append(" prd_cabecera_parte.linea ,");
			cadenaSQL.append(" prd_cabecera_parte.turno ,");
			cadenaSQL.append(" prd_cabecera_parte.buenas,");
			cadenaSQL.append(" prd_cabecera_parte.observaciones,");
			cadenaSQL.append(" prd_cabecera_parte.tecnicoCalidad ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguienteCodigoInterno(con, "prd_cabecera_parte", "idCodigo"));
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getFecha());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getLinea()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getLinea().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getTurno()!=null)
		    {
		    	preparedStatement.setString(6, r_mapeo.getTurno().toUpperCase());
		    }
		    else
		    {
		    	preparedStatement.setString(6, null);
		    }
		    if (r_mapeo.getProduccion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getProduccion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getTecnicoCalidad()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getTecnicoCalidad());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    
	        preparedStatement.executeUpdate();
	        
	        generarTablaIncidenciasParte(r_mapeo.getIdCodigo(), r_mapeo.getLinea().toUpperCase());
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}
	public String guardarCambiosCabeceraParte(MapeoCabeceraParte r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE prd_cabecera_parte SET ");
			cadenaSQL.append(" prd_cabecera_parte.buenas =?,");
			cadenaSQL.append(" prd_cabecera_parte.observaciones =?, ");
			cadenaSQL.append(" prd_cabecera_parte.tecnicoCalidad =? ");
			cadenaSQL.append(" WHERE prd_cabecera_parte.idCodigo = ? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getProduccion()!=null)
			{
				preparedStatement.setInt(1, r_mapeo.getProduccion());
			}
			else
			{
				preparedStatement.setInt(1, 0);
			}
			if (r_mapeo.getObservaciones()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getObservaciones());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getTecnicoCalidad()!=null)
			{
				preparedStatement.setString(3, r_mapeo.getTecnicoCalidad());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(4, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(4, 0);
			}
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String guardarOperariosParte(MapeoHorasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_operarios_parte( ");
			cadenaSQL.append(" prd_operarios_parte.idCodigo,");
			cadenaSQL.append(" prd_operarios_parte.operario ,");
			cadenaSQL.append(" prd_operarios_parte.horasAUTOMATICO,");
			cadenaSQL.append(" prd_operarios_parte.horasJAULON,");
			cadenaSQL.append(" prd_operarios_parte.horasETIQUETADO,");
			cadenaSQL.append(" prd_operarios_parte.horasMANUAL,");
			cadenaSQL.append(" prd_operarios_parte.horas ,");
			cadenaSQL.append(" prd_operarios_parte.fuera_linea ,");
			cadenaSQL.append(" prd_operarios_parte.motivo_fuera_linea ,");
			cadenaSQL.append(" prd_operarios_parte.ausencia ,");
			cadenaSQL.append(" prd_operarios_parte.motivo_ausencia,");
			cadenaSQL.append(" prd_operarios_parte.idCabeceraParte ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			r_mapeo.setIdCodigo(this.obtenerSiguienteCodigoInterno(con, "prd_operarios_parte", "idCodigo"));
			
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			
			if (r_mapeo.getOperario()!=null)
			{
				preparedStatement.setString(2, r_mapeo.getOperario());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getHorasAUTOMATICO()!=null && r_mapeo.getHorasAUTOMATICO().length()>0)
			{
				preparedStatement.setDouble(3, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasAUTOMATICO())));
			}
			else
			{
				preparedStatement.setDouble(3, 0);
			}
			if (r_mapeo.getHorasJAULON()!=null && r_mapeo.getHorasJAULON().length()>0)
			{
				preparedStatement.setDouble(4, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasJAULON())));
			}
			else
			{
				preparedStatement.setInt(4, 0);
			}
			if (r_mapeo.getHorasETIQUETADO()!=null && r_mapeo.getHorasETIQUETADO().length()>0)
			{
				preparedStatement.setDouble(5, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasETIQUETADO())));
			}
			else
			{
				preparedStatement.setDouble(5, 0);
			}
			if (r_mapeo.getHorasMANUAL()!=null && r_mapeo.getHorasMANUAL().length()>0)
			{
				preparedStatement.setDouble(6, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasMANUAL())));
			}
			else
			{
				preparedStatement.setDouble(6, 0);
			}
			if (r_mapeo.getHorasTotalesLinea()!=null)
			{
				preparedStatement.setDouble(7, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesLinea())));
			}
			else
			{
				preparedStatement.setDouble(7, 0);
			}
			if (r_mapeo.getHorasTotalesFueraLinea()!=null)
			{
				preparedStatement.setDouble(8, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesFueraLinea())));
			}
			else
			{
				preparedStatement.setDouble(8, 0);
			}
			if (r_mapeo.getMotivoFueraLinea()!=null)
			{
				preparedStatement.setString(9, r_mapeo.getMotivoFueraLinea());
			}
			else
			{
				preparedStatement.setString(9, null);
			}
			if (r_mapeo.getHorasAbsentismo()!=null)
			{
				preparedStatement.setDouble(10, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasAbsentismo())));
			}
			else
			{
				preparedStatement.setDouble(10, 0);
			}
			if (r_mapeo.getMotivoAbsentismo()!=null)
			{
				preparedStatement.setString(11, r_mapeo.getMotivoAbsentismo());
			}
			else
			{
				preparedStatement.setString(11, null);
			}
			if (r_mapeo.getIdTurno()!=null)
			{
				preparedStatement.setInt(12, r_mapeo.getIdTurno());
			}
			else
			{
				preparedStatement.setInt(12, 0);
			}

			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String guardarCambiosOperariosParte(MapeoHorasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE prd_operarios_parte SET ");
			cadenaSQL.append(" prd_operarios_parte.horasAUTOMATICO =?,");
			cadenaSQL.append(" prd_operarios_parte.horasJAULON =?,");
			cadenaSQL.append(" prd_operarios_parte.horasETIQUETADO=?,");
			cadenaSQL.append(" prd_operarios_parte.horasMANUAL=?,");
			cadenaSQL.append(" prd_operarios_parte.horas =?,");
			cadenaSQL.append(" prd_operarios_parte.fuera_linea =?,");
			cadenaSQL.append(" prd_operarios_parte.motivo_fuera_linea =? ,");
			cadenaSQL.append(" prd_operarios_parte.ausencia =?,");
			cadenaSQL.append(" prd_operarios_parte.motivo_ausencia=? ");
			cadenaSQL.append(" WHERE prd_operarios_parte.idCodigo = ? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getHorasAUTOMATICO()!=null && r_mapeo.getHorasAUTOMATICO().length()>0)
			{
				preparedStatement.setDouble(1, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasAUTOMATICO())));
			}
			else
			{
				preparedStatement.setDouble(1, 0);
			}
			if (r_mapeo.getHorasJAULON()!=null && r_mapeo.getHorasJAULON().length()>0)
			{
				preparedStatement.setDouble(2, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasJAULON())));
			}
			else
			{
				preparedStatement.setInt(2, 0);
			}
			if (r_mapeo.getHorasETIQUETADO()!=null && r_mapeo.getHorasETIQUETADO().length()>0)
			{
				preparedStatement.setDouble(3, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasETIQUETADO())));
			}
			else
			{
				preparedStatement.setDouble(3, 0);
			}
			if (r_mapeo.getHorasMANUAL()!=null && r_mapeo.getHorasMANUAL().length()>0)
			{
				preparedStatement.setDouble(4, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasMANUAL())));
			}
			else
			{
				preparedStatement.setDouble(4, 0);
			}
			if (r_mapeo.getHorasTotalesLinea()!=null && r_mapeo.getHorasTotalesLinea().length()>0)
			{
				preparedStatement.setDouble(5, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesLinea())));
			}
			else
			{
				preparedStatement.setDouble(5, 0);
			}
			if (r_mapeo.getHorasTotalesFueraLinea()!=null && r_mapeo.getHorasTotalesFueraLinea().length()>0)
			{
				preparedStatement.setDouble(6, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesFueraLinea())));
			}
			else
			{
				preparedStatement.setDouble(6, 0);
			}
			if (r_mapeo.getMotivoFueraLinea()!=null)
			{
				preparedStatement.setString(7, r_mapeo.getMotivoFueraLinea());
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			if (r_mapeo.getHorasAbsentismo()!=null && r_mapeo.getHorasAbsentismo().length()>0)
			{
				preparedStatement.setDouble(8, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasAbsentismo())));
			}
			else
			{
				preparedStatement.setDouble(8, 0);
			}
			if (r_mapeo.getMotivoAbsentismo()!=null)
			{
				preparedStatement.setString(9, r_mapeo.getMotivoAbsentismo());
			}
			else
			{
				preparedStatement.setString(9, null);
			}
			if (r_mapeo.getIdTurno()!=null)
			{
				preparedStatement.setInt(10, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(10, 0);
			}
			
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}
	public String guardarTiemposProduccionParte(MapeoHorasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE prd_operarios_parte SET ");
			cadenaSQL.append(" prd_operarios_parte.horasAUTOMATICO=?,");
			cadenaSQL.append(" prd_operarios_parte.horasJAULON=?,");
			cadenaSQL.append(" prd_operarios_parte.horasETIQUETADO=?,");
			cadenaSQL.append(" prd_operarios_parte.horasMANUAL=? ");
			
			cadenaSQL.append(", prd_operarios_parte.horas=?, ");
			cadenaSQL.append(" prd_operarios_parte.fuera_linea=?,");
			cadenaSQL.append(" prd_operarios_parte.motivo_fuera_linea=?");
			
			cadenaSQL.append(" WHERE idCabeceraParte = ? ");
			cadenaSQL.append(" AND ausencia = 0 ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getHorasAUTOMATICO()!=null && r_mapeo.getHorasAUTOMATICO().length()>0)
			{
				preparedStatement.setDouble(1, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasAUTOMATICO())));
			}
			else
			{
				preparedStatement.setDouble(1, 0);
			}
			if (r_mapeo.getHorasJAULON()!=null && r_mapeo.getHorasJAULON().length()>0)
			{
				preparedStatement.setDouble(2, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasJAULON())));
			}
			else
			{
				preparedStatement.setDouble(2, 0);
			}
			if (r_mapeo.getHorasETIQUETADO()!=null && r_mapeo.getHorasETIQUETADO().length()>0)
			{
				preparedStatement.setDouble(3, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasETIQUETADO())));
			}
			else
			{
				preparedStatement.setDouble(3, 0);
			}
			if (r_mapeo.getHorasMANUAL()!=null && r_mapeo.getHorasMANUAL().length()>0)
			{
				preparedStatement.setDouble(4, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasMANUAL())));
			}
			else
			{
				preparedStatement.setDouble(4, 0);
			}
			if (r_mapeo.getHorasTotalesLinea()!=null && r_mapeo.getHorasTotalesLinea().length()>0 )
			{
				preparedStatement.setDouble(5, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesLinea())));
			}
			else
			{
				preparedStatement.setDouble(5, 0);
			}
			
			if (r_mapeo.getHorasTotalesFueraLinea()!=null && r_mapeo.getHorasTotalesFueraLinea().length()>0 )
			{
				preparedStatement.setDouble(6, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHorasTotalesFueraLinea())));
			}
			else
			{
				preparedStatement.setDouble(6, 0);
			}
			if (r_mapeo.getMotivoFueraLinea()!=null && r_mapeo.getMotivoFueraLinea().length()>0)
			{
				preparedStatement.setString(7, r_mapeo.getMotivoFueraLinea());
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			preparedStatement.setInt(8, r_mapeo.getIdTurno());
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String guardarCambiosIncidenciaParte(MapeoIncidenciasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" UPDATE prd_paradas_parte SET ");
			cadenaSQL.append(" prd_paradas_parte.tiempo =?,");
			cadenaSQL.append(" prd_paradas_parte.observaciones =? ");
			cadenaSQL.append(" WHERE idCodigo = ? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (r_mapeo.getHoras()!=null && r_mapeo.getHoras().length()>0)
			{
				preparedStatement.setDouble(1, new Double(RutinasCadenas.reemplazarComaMiles(r_mapeo.getHoras())));
			}
			else
			{
				preparedStatement.setDouble(1, 0);
			}
			if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
			{
				preparedStatement.setString(2, r_mapeo.getObservaciones());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(3, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(3, 0);
			}
			
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}	

	public String guardarHorasCambioArticuloParte(MapeoIncidenciasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			consultaCambioArticuloParteServer ccap = consultaCambioArticuloParteServer.getInstance(CurrentUser.get());
			String tiempo = ccap.recuperarHorasCambioArticuloParte(r_mapeo.getIdCodigo());
			StringBuffer obs = ccap.recuperarObservacionesCambioArticuloParte(r_mapeo.getIdCodigo());
			
			cadenaSQL.append(" UPDATE prd_paradas_parte SET ");
			cadenaSQL.append(" prd_paradas_parte.tiempo =?, ");
			cadenaSQL.append(" prd_paradas_parte.observaciones =? ");
			cadenaSQL.append(" WHERE idCodigo = ? ");
			
			con= this.conManager.establecerConexionInd();	
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (tiempo!=null && tiempo.length()>0)
			{
				preparedStatement.setDouble(1, new Double(RutinasCadenas.reemplazarComaMiles(tiempo)));
			}
			else
			{
				preparedStatement.setDouble(1, 0);
			}
			if (obs!=null && obs.toString().length()>0)
			{
				preparedStatement.setString(2, obs.toString().trim());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			if (r_mapeo.getIdCodigo()!=null)
			{
				preparedStatement.setInt(3, r_mapeo.getIdCodigo());
			}
			else
			{
				preparedStatement.setInt(3, 0);
			}
			
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
			return ex.getMessage();
		}        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}	

	public String eliminarParte(Integer r_idTurno)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		String rdo = null;
		
		StringBuffer cadenaSQL = null;        
		
		try
		{
			con= this.conManager.establecerConexionInd();	          
			/*
			 * Aqui borraremos las tablas relacionadas cuando las tengamos con datos"
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_paradas_ca_parte ");            
			cadenaSQL.append(" WHERE prd_paradas_ca_parte.idParadasParte in (select idCodigo from prd_paradas_parte where prd_paradas_parte.idCabeceraParte = ?)" ); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idTurno);
			preparedStatement.executeUpdate();
			preparedStatement.close();

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_paradas_parte ");            
			cadenaSQL.append(" WHERE prd_paradas_parte.idCabeceraParte = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idTurno);
			preparedStatement.executeUpdate();
			preparedStatement.close();

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_operarios_parte ");            
			cadenaSQL.append(" WHERE prd_operarios_parte.idCabeceraParte = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idTurno);
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_cabecera_parte ");            
			cadenaSQL.append(" WHERE prd_cabecera_parte.idCodigo = ?");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idTurno);
			preparedStatement.executeUpdate();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				rdo = ex.getMessage();
			}
		}
		return rdo;
	}
	
	public String eliminarOperarioTurno(Integer r_idOperario)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		String rdo = null;
		StringBuffer cadenaSQL = null;        
		
		try
		{
			con= this.conManager.establecerConexionInd();	          
			/*
			 * Aqui borraremos las tablas relacionadas cuando las tengamos con datos"
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_operarios_parte ");            
			cadenaSQL.append(" WHERE prd_operarios_parte.idCodigo = ?"); 
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_idOperario);
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				rdo = ex.getMessage();
			}
		}
		return rdo;
	}
	
	public ArrayList<MapeoIncidenciasTurno> recuperarIncidenciasParte(Integer r_turno, String r_tipo)
	{
		ArrayList<MapeoIncidenciasTurno> vector = null;
		MapeoIncidenciasTurno mapeo  = null; 
		
		vector = new ArrayList<MapeoIncidenciasTurno>();

		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
			
		cadenaSQL.append(" SELECT prd_paradas_parte.idCodigo par_id,");
		cadenaSQL.append(" prd_paradas_parte.tipo par_tip,");
		cadenaSQL.append(" prd_paradas_parte.descripcion par_des,");
		cadenaSQL.append(" prd_paradas_parte.tiempo par_hor,");
		cadenaSQL.append(" prd_paradas_parte.observaciones par_obs, ");
		cadenaSQL.append(" prd_paradas_parte.idCabeceraParte par_tur");
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" where prd_paradas_parte.idCabeceraParte = " + r_turno);
		cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		cadenaSQL.append(" order by idCodigo asc ");

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoIncidenciasTurno>();
			while(rsOpcion.next())
			{
				mapeo = new MapeoIncidenciasTurno();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("par_id"));
				mapeo.setTipo(rsOpcion.getString("par_tip"));
				mapeo.setMaquina(rsOpcion.getString("par_des"));
				mapeo.setHoras(rsOpcion.getString("par_hor"));
				mapeo.setObservaciones(rsOpcion.getString("par_obs"));
				mapeo.setIdTurno(rsOpcion.getInt("par_tur"));
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}

	public Double recuperarIncidenciasSemana(String r_linea, Integer r_ejercicio, Integer r_semana, String r_tipo)
	{
		Double horasIncidencia = new Double(0);
		
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT sum(prd_paradas_parte.tiempo) par_hor ");
		
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.semana = " + r_semana);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				horasIncidencia=rsOpcion.getDouble("par_hor");
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horasIncidencia;
	}
	
	public HashMap<String, Double> recuperarIncidenciasMaquinas(String r_linea, Integer r_ejercicio, String r_semana, String r_tipo, String r_ambitoTemporal)
	{
		HashMap<String, Double> hashIncidencias = null;		
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_parte.descripcion par_des, sum(prd_paradas_parte.tiempo) par_hor ");
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		if (r_semana!=null)
			if (r_ambitoTemporal.contentEquals("Acumulado"))
				cadenaSQL.append(" and prd_cabecera_parte.semana <= '" + r_semana + "' ");
			else if (r_ambitoTemporal.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					cadenaSQL.append(" and prd_cabecera_parte.semana between " + desde + " and " + hasta);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
				cadenaSQL.append(" and prd_cabecera_parte.semana = '" + r_semana + "' ");
		
		if (r_tipo!=null && !r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		else if (r_tipo!=null && r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo in ('C','N') ");
		
		cadenaSQL.append(" group by 1 ");
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hashIncidencias = new HashMap<String, Double>();
			
			while(rsOpcion.next())
			{
				hashIncidencias.put(rsOpcion.getString("par_des"), rsOpcion.getDouble("par_hor"));
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashIncidencias;
	}

	public ArrayList<MapeoBarras> recuperarArrayIncidenciasMaquinas(String r_linea, Integer r_ejercicio, String r_semana, String r_tipo, String r_ambitoTemporal)
	{
		ArrayList<MapeoBarras> vectorIncidencias = null;
		MapeoBarras mapeoBarras = null;
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_parte.descripcion par_des, sum(prd_paradas_parte.tiempo) par_hor ");
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		if (r_semana!=null)
		{
			if (r_ambitoTemporal.contentEquals("Acumulado"))
				cadenaSQL.append(" and prd_cabecera_parte.semana <= '" + r_semana + "' ");
			else if (r_ambitoTemporal.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					cadenaSQL.append(" and prd_cabecera_parte.semana between " + desde + " and " + hasta);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
				cadenaSQL.append(" and prd_cabecera_parte.semana = '" + r_semana + "' ");
		
		}
		if (r_tipo!=null && !r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		else if (r_tipo!=null && r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo in ('C','N') ");
		
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" order by prd_paradas_parte.idCodigo ");
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vectorIncidencias = new ArrayList<MapeoBarras>();
			
			while(rsOpcion.next())
			{
				mapeoBarras = new MapeoBarras();
				mapeoBarras.setClave(rsOpcion.getString("par_des"));
				mapeoBarras.setValorActual(rsOpcion.getDouble("par_hor"));
				
				vectorIncidencias.add(mapeoBarras);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorIncidencias;
	}

	public ArrayList<MapeoIncidenciasTurno> datosGlobalIncidenciasMaquinas(String r_linea, Integer r_ejercicio, String r_semana, String r_tipo, String r_maquina, String r_ambitoTemporal)
	{
		ArrayList<MapeoIncidenciasTurno> vectorIncidencias = null;
		MapeoIncidenciasTurno mapeoBarras = null;
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_paradas_parte.descripcion par_des, prd_paradas_parte.tiempo par_hor , prd_cabecera_parte.fecha prd_fec, prd_paradas_parte.observaciones par_obs ");
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		cadenaSQL.append(" and prd_paradas_parte.descripcion = '" + r_maquina + "' ");
		cadenaSQL.append(" and prd_paradas_parte.tiempo != 0 " );
		
		if (r_semana!=null)
			if (r_ambitoTemporal.contentEquals("Acumulado"))
				cadenaSQL.append(" and prd_cabecera_parte.semana <= '" + r_semana + "' ");
			else if (r_ambitoTemporal.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					cadenaSQL.append(" and prd_cabecera_parte.semana between " + desde + " and " + hasta);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
				cadenaSQL.append(" and prd_cabecera_parte.semana = '" + r_semana + "' ");
		
		if (r_tipo!=null && !r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		else if (r_tipo!=null && r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo in ('C','N') ");
		
		cadenaSQL.append(" order by prd_paradas_parte.idCodigo ");
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vectorIncidencias = new ArrayList<MapeoIncidenciasTurno>();
			
			while(rsOpcion.next())
			{
				mapeoBarras = new MapeoIncidenciasTurno();
				mapeoBarras.setMaquina(rsOpcion.getString("par_des"));
				mapeoBarras.setHoras(rsOpcion.getString("par_hor"));
				mapeoBarras.setObservaciones(rsOpcion.getString("par_obs"));
				mapeoBarras.setFecha(RutinasFechas.convertirDeFecha(rsOpcion.getString("prd_fec")));
				
				vectorIncidencias.add(mapeoBarras);
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vectorIncidencias;
	}
	
	public HashMap<Integer, String> recuperarMaquinas(String r_linea, Integer r_ejercicio, String r_semana, String r_tipo)
	{
		HashMap<Integer, String> hashIncidencias = new HashMap<Integer, String>();
		Integer contador = 0;
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT distinct prd_paradas_parte.descripcion par_des ");
		cadenaSQL.append(" from prd_paradas_parte ");
		cadenaSQL.append(" inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and prd_cabecera_parte.linea = '" + r_linea + "' ");
		
		if (r_semana!=null)
			cadenaSQL.append(" and prd_cabecera_parte.semana = '" + r_semana + "' ");
		
		if (r_tipo!=null && !r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo = '" + r_tipo + "' ");
		else if (r_tipo!=null && r_tipo.contentEquals("Todas"))
			cadenaSQL.append(" and prd_paradas_parte.tipo in ('C','N') ");
		
		
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" order by prd_paradas_parte.idCodigo ");
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			hashIncidencias = new HashMap<Integer, String>();
			
			while(rsOpcion.next())
			{
				contador++;
				hashIncidencias.put(contador, rsOpcion.getString("par_des"));
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashIncidencias;
	}


	public void guardarIncidenciasParte(MapeoIncidenciasTurno r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO prd_paradas_parte( ");
			cadenaSQL.append(" prd_paradas_parte.idCodigo,");
			cadenaSQL.append(" prd_paradas_parte.tipo ,");
			cadenaSQL.append(" prd_paradas_parte.descripcion ,");
			cadenaSQL.append(" prd_paradas_parte.tiempo,");
			cadenaSQL.append(" prd_paradas_parte.observaciones ,");
			cadenaSQL.append(" prd_paradas_parte.idCabeceraParte ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    r_mapeo.setIdCodigo(this.obtenerSiguienteCodigoInterno(con, "prd_paradas_parte", "idCodigo"));
		    
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getTipo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getTipo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getMaquina()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getMaquina());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getIdTurno()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdTurno());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}
	
	private void generarIncidenciasProgramadas(Integer r_turno, String r_area)
	{
		MapeoIncidenciasTurno mapeo  = null; 

		switch (r_area)
		{
			case "EMBOTELLADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Puesta en Marcha");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
//				mapeo = new MapeoIncidenciasTurno();
//				mapeo.setMaquina("Cambio Fomato Botella");
//				mapeo.setHoras("0");
//				mapeo.setObservaciones("");			
//				mapeo.setTipo("P");
//				mapeo.setIdTurno(r_turno);
//				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio Rollos");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cantidad Exacta");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
//				mapeo = new MapeoIncidenciasTurno();
//				mapeo.setMaquina("Cambio Caja 6/12");
//				mapeo.setHoras("0");
//				mapeo.setObservaciones("");			
//				mapeo.setTipo("P");
//				mapeo.setIdTurno(r_turno);
//				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio Articulo");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Chequeos Calidad Zona 2");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Chequeos Calidad Zona 4");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Chequeos Calidad Zona 5");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Limpiar, Recojer, Mantenimiento");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Bocadillo");
				mapeo.setHoras("0.33");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;
			}
			case "ENVASADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Esperar Vino");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Esperar Ok Vino");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato palets");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato Garrafas");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio de articulo (vino)");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtracion");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Possimat");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Taponadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Camara Vision");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Etiquetadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);		

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encajonadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
								
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Paletizador");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Robot");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Limpiar");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Bocadillo");
				mapeo.setHoras("0.33");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				break;
			}
			case "BIB":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio de formato caja");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Limpiar, mantenimiento y recoger");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Bocadillo");
				mapeo.setHoras("0.33");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("P");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;				
			}
		}
	}
	
	private void generarIncidenciasNoProgramadas(Integer r_turno, String r_area)
	{
		MapeoIncidenciasTurno mapeo  = null; 
		
		switch (r_area)
		{
			case "EMBOTELLADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");		
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Despaletizadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Enjuagadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encorchadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pilfer");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pulpo");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Lavadora" );
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Capsuladora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Etiquetadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Camara Vision");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);


				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora Cajas");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encajonadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pesadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Paletizador");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Retractiladora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;
			}
			case "ENVASADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato palets");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato garrafa");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio de articulo (vino)");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtración");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Possimat");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Taponadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Camara Vision");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Etiquetadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);		

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encajonadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
								
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Paletizador");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Robot");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Limpiar");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				break;
			}
			case "BIB":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Puesta en Marcha");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora cajas");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("N");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;
			}
		}
	}
	
	private void generarIncidenciasNoProgramadasCalidad(Integer r_turno, String r_area)
	{
		MapeoIncidenciasTurno mapeo  = null; 
		switch (r_area)
		{
			case "EMBOTELLADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");		
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Despaletizadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Enjuagadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encorchadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pilfer");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pulpo");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Lavadora" );
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Capsuladora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Etiquetadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Camara Vision");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora Cajas");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encajonadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Pesadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Paletizador");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Retractiladora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;
			}
			case "ENVASADO":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato palets");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio formato garrafa");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cambio de articulo (vino)");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Filtración");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Possimat");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Taponadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Camara Vision");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Etiquetadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);		

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Encajonadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
								
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Paletizador");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Robot");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Limpiar");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);				

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				break;
			}
			case "BIB":
			{
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Puesta en Marcha");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Formadora cajas");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Llenadora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);

				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Cerradora");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				
				mapeo = new MapeoIncidenciasTurno();
				mapeo.setMaquina("Otros");
				mapeo.setHoras("0");
				mapeo.setObservaciones("");			
				mapeo.setTipo("C");
				mapeo.setIdTurno(r_turno);
				this.guardarIncidenciasParte(mapeo);
				break;
			}
		}
	}
	
	
	private void generarTablaIncidenciasParte(Integer r_turno, String r_area)
	{
		this.generarIncidenciasNoProgramadas(r_turno,r_area);
		this.generarIncidenciasProgramadas(r_turno, r_area);
		this.generarIncidenciasNoProgramadasCalidad(r_turno, r_area);
	}
	
	/*
	 * Metodo que se encarga de devolvernos cuantas personas por turno 
	 * hemos utilizado en la embotelladora
	 * 
	 * @parm	Recibe el ejercicio a consultar
	 * 
	 * @return	Devuelve un array con el distinto numero de personas por turno
	 */
	public ArrayList<Integer> personasPorTurno(Integer r_ejercicio, String r_semana)
	{
		ArrayList<Integer> personas = null;
		
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" select distinct count(distinct operario) AS CUANTOS ");
		cadenaSQL.append(" from prd_cabecera_parte ");
		cadenaSQL.append(" inner join prd_operarios_parte on prd_operarios_parte.idCabeceraParte=prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where prd_cabecera_parte.ejercicio = '" + r_ejercicio + "' ");
		cadenaSQL.append(" and linea = 'EMBOTELLADO' ");
		if (r_semana!=null && r_semana.length()>0) cadenaSQL.append(" and semana = '" + r_semana + "' ");
		cadenaSQL.append(" group by FECHA,TURNO ");
		cadenaSQL.append(" order by 1 ");

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			personas = new ArrayList<Integer>();
			
			while(rsOpcion.next())
			{
				personas.add(rsOpcion.getInt("CUANTOS"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}		
		return personas;
	}
	/*
	 * Metodo que se encarga de devolvernos el total de horas invertidas por los turnos  
	 * de un tipo determinado
	 * 
	 * @parm	Recibe el ejercicio a consultar
	 * 			Recibe numero de personas que tiene que haber en el turno
	 * 			Recibe las horas a consultar (Automatico, jaulon, etc.
	 * 
	 * @return	Devuelve double con el total de horas encontradas
	 */
	public Double recuperarHorasParte(Integer r_ejercicio, Integer r_personas, String r_tipoHora, String r_semana)
	{
		Double totalHoras = null;
		
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" select sum(" + r_tipoHora + ")/" + r_personas + " as horas ");
		cadenaSQL.append(" from prd_operarios_parte");
		cadenaSQL.append(" where idCabeceraParte in ( ");
		cadenaSQL.append(" select prd_cabecera_parte.idCodigo from prd_cabecera_parte ");
		cadenaSQL.append(" inner join prd_operarios_parte on prd_operarios_parte.idCabeceraParte=prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where ejercicio = " + r_ejercicio);
		if (r_semana!=null && r_semana.length()>0) cadenaSQL.append(" and week(prd_cabecera_parte.fecha,3)='" + r_semana + "' ");
		cadenaSQL.append(" and linea = 'EMBOTELLADO' ");
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" having count(distinct operario)= " + r_personas + ") ");
		

		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				totalHoras = rsOpcion.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}		
		
		return totalHoras;
	}
	/*
	 * Metodo que se encarga de devolvernos fecha y turno en los que ha  
	 * habido un numero determinado de operarios
	 * 
	 * @parm	Recibe el ejercicio a consultar
	 * 			Recibe numero de personas que tiene que haber en el turno
	 * 			Recibe las horas a consultar (Automatico, jaulon, etc.
	 * 
	 * @return	Devuelve double con el total de horas encontradas
	 */
	public boolean recuperarFechasTurnos(Integer r_ejercicio, Integer r_personas, String r_semana)
	{
		boolean datosGenerados = false;
		
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" select prd_cabecera_parte.fecha as par_fec, ");
		cadenaSQL.append(" prd_cabecera_parte.turno as par_tur ");
		cadenaSQL.append(" from prd_cabecera_parte ");
		cadenaSQL.append(" where idCodigo in ( ");
		cadenaSQL.append(" select prd_cabecera_parte.idCodigo from prd_cabecera_parte ");
		cadenaSQL.append(" inner join prd_operarios_parte on prd_operarios_parte.idCabeceraParte=prd_cabecera_parte.idCodigo ");
		cadenaSQL.append(" where ejercicio = " + r_ejercicio);
		cadenaSQL.append(" and linea = 'EMBOTELLADO' ");
		cadenaSQL.append(" group by 1 ");
		cadenaSQL.append(" having count(distinct operario)= " + r_personas + ") ");
		if (r_semana!=null && r_semana.length()>0) cadenaSQL.append(" and week(prd_cabecera_parte.fecha,3)='" + r_semana + "' ");
		
		try
		{
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			this.eliminarFechasTurnos();
			while(rsOpcion.next())
			{
				if (rsOpcion.getString("par_tur")!=null && rsOpcion.getString("par_tur").contentEquals("MAÑANA"))
					this.guardarFechasTurnos(rsOpcion.getString("par_fec"), rsOpcion.getString("par_tur"));
				else if (rsOpcion.getString("par_tur")!=null && !rsOpcion.getString("par_tur").contentEquals("MAÑANA"))
					this.guardarFechasTurnos(rsOpcion.getString("par_fec"), rsOpcion.getString("par_tur"));
			}
			datosGenerados=true;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}		
		
		return datosGenerados;
	}
	
	public String guardarFechasTurnos(String r_fecha, String r_horario)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			cadenaSQL.append(" INSERT INTO tmp_fechas_turnos( ");
			cadenaSQL.append(" tmp_fechas_turnos.idCodigo,");
			cadenaSQL.append(" tmp_fechas_turnos.fecha ,");
			cadenaSQL.append(" tmp_fechas_turnos.horario ) VALUES (");
			cadenaSQL.append(" ?,?,?) ");
			
			con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    Integer id = this.obtenerSiguienteCodigoInterno(con, "tmp_fechas_turnos", "idCodigo");
		    
		    preparedStatement.setInt(1, id);
		    
		    if (r_fecha!=null)
		    {
		    	preparedStatement.setString(2, r_fecha);
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_horario!=null)
		    {
		    	preparedStatement.setString(3, r_horario);
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return null;
	}

	public String eliminarFechasTurnos()
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;	
		String rdo = null;
		StringBuffer cadenaSQL = null;        
		
		try
		{
			con= this.conManager.establecerConexionInd();	          
			/*
			 * Aqui borraremos las tablas relacionadas cuando las tengamos con datos"
			 */
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM tmp_fechas_turnos ");            
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				rdo = ex.getMessage();
			}
		}
		return rdo;
	}
	@Override
	public String semaforos() {
		return null;
	}
}