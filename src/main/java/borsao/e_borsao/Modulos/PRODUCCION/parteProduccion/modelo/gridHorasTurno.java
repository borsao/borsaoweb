package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.converter.StringToBooleanConverter;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.tiemposEnvasadora.modelo.MapeoTiemposEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view.parteProduccionView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class gridHorasTurno extends GridPropio 
{
	private boolean editable = true;
	private boolean conFiltro = false;
	public boolean actualizar = false;
	private parteProduccionView app = null;
	private Integer idTurno = null;
	private String area = null;
	private MapeoHorasTurno mapeo=null;
	private MapeoHorasTurno mapeoOrig=null;
	
    public gridHorasTurno(ArrayList<MapeoHorasTurno> r_vector, parteProduccionView r_app) 
    {
        this.vector=r_vector;
        this.app=r_app;
        if (this.app.cmbSerie.getValue()==null) 
        	this.area="EMBOTELLADO";
        else
        	this.area = this.app.cmbSerie.getValue().toString();

        
        this.asignarTitulo("Horas Turno");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoHorasTurno>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    public gridHorasTurno(ArrayList<MapeoHorasTurno> r_vector, parteProduccionView r_app, Integer r_idTurno) 
    {
        this.vector=r_vector;
        this.idTurno = r_idTurno;
        this.app=r_app;
        if (this.app.cmbSerie.getValue()==null) 
        	this.area="EMBOTELLADO";
        else
        	this.area = this.app.cmbSerie.getValue().toString();

        this.asignarTitulo("Horas Turno");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoHorasTurno>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoHorasTurno.class);
		if (this.vector!=null) this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	if (this.area.contentEquals("EMBOTELLADO"))
    		setColumnOrder("operario", "horasAUTOMATICO", "horasJAULON", "horasETIQUETADO","horasMANUAL", "horasTotalesLinea","horasTotalesFueraLinea","motivoFueraLinea","horasAbsentismo", "motivoAbsentismo", "eliminar");
    	else
    		setColumnOrder("operario", "horasTotalesLinea","horasTotalesFueraLinea","motivoFueraLinea","horasAbsentismo", "motivoAbsentismo", "eliminar");
    		
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("operario").setHeaderCaption("Operario");
    	this.getColumn("operario").setSortable(true);
    	this.getColumn("operario").setWidth(new Double(160));
    	if (this.area.contentEquals("EMBOTELLADO"))
    	{
	    	this.getColumn("horasAUTOMATICO").setHeaderCaption("AUTOMATICO");
	    	this.getColumn("horasAUTOMATICO").setSortable(true);
	    	this.getColumn("horasAUTOMATICO").setWidth(new Double(120));
	    	this.getColumn("horasJAULON").setHeaderCaption("JAULON");
	    	this.getColumn("horasJAULON").setSortable(true);
	    	this.getColumn("horasJAULON").setWidth(new Double(120));
	    	this.getColumn("horasETIQUETADO").setHeaderCaption("ETIQUETADO");
	    	this.getColumn("horasETIQUETADO").setSortable(true);
	    	this.getColumn("horasETIQUETADO").setWidth(new Double(120));
	    	this.getColumn("horasMANUAL").setHeaderCaption("MANUAL");
	    	this.getColumn("horasMANUAL").setSortable(true);
	    	this.getColumn("horasMANUAL").setWidth(new Double(120));
    	}
    	this.getColumn("horasTotalesLinea").setHeaderCaption("Turno");
    	this.getColumn("horasTotalesLinea").setSortable(true);
    	this.getColumn("horasTotalesLinea").setWidth(new Double(120));
    	this.getColumn("horasTotalesFueraLinea").setHeaderCaption("Fuera Linea");
    	this.getColumn("horasTotalesFueraLinea").setSortable(true);
    	this.getColumn("horasTotalesFueraLinea").setWidth(new Double(120));
    	this.getColumn("motivoFueraLinea").setHeaderCaption("Motivo");
    	this.getColumn("motivoFueraLinea").setSortable(true);
    	this.getColumn("motivoFueraLinea").setWidth(new Double(250));
    	this.getColumn("horasAbsentismo").setHeaderCaption("Absentismo");
    	this.getColumn("horasAbsentismo").setSortable(true);
    	this.getColumn("horasAbsentismo").setWidth(new Double(120));
    	this.getColumn("motivoAbsentismo").setHeaderCaption("Motivo");
    	this.getColumn("motivoAbsentismo").setSortable(true);
    	this.getColumn("motivoAbsentismo").setWidth(new Double(250));
    	this.getColumn("observaciones").setHeaderCaption("Observaciones");
    	this.getColumn("observaciones").setSortable(false);
    	this.getColumn("observaciones").setWidth(new Double(250));
    	
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
    	this.getColumn("eliminar").setWidth(100);
    	
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("idTurno").setHidden(true);
    	
//    	if (this.area.contentEquals("EMBOTELLADO")) this.getColumn("observaciones").setHidden(true);
    	if (!this.area.contentEquals("EMBOTELLADO"))
    	{
    		this.getColumn("horasAUTOMATICO").setHidden(true);
    		this.getColumn("horasJAULON").setHidden(true);
    		this.getColumn("horasETIQUETADO").setHidden(true);
    		this.getColumn("horasMANUAL").setHidden(true);
    	}
    	
    }
    
    public void asignarEstilos()
    {
    	
    	this.getColumn("eliminar").setConverter(new StringToBooleanConverter() {
 		   @Override
 		   protected String getTrueString() {
 		      return "";
 		   }
 		   @Override
 		   protected String getFalseString() {
 		      return "";
 		   }
 		});
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("horasAUTOMATICO".equals(cellReference.getPropertyId()) || "horasJAULON".equals(cellReference.getPropertyId()) || "horasETIQUETADO".equals(cellReference.getPropertyId()) 
            			|| "horasMANUAL".equals(cellReference.getPropertyId()) || "horasTotalesLinea".equals(cellReference.getPropertyId()) || "horasTotalesFueraLinea".equals(cellReference.getPropertyId()) || "horasAbsentismo".equals(cellReference.getPropertyId())) 
            	{            		
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	actualizar=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	if (actualizar)
	        	{
	        		
	        		MapeoHorasTurno mapeo = (MapeoHorasTurno) getEditedItemId();
	        		
	        		consultaParteProduccionServer cs = new consultaParteProduccionServer(CurrentUser.get());
			        
	        		if (mapeo.getIdCodigo()!=null)
		        	{
	        			if (mapeo.isEliminar())
	        			{
	        				cs.eliminarOperarioTurno(mapeo.getIdCodigo());
	        				((ArrayList<MapeoTiemposEnvasadora>) vector).remove(mapeo);
	        				remove(mapeo);
			    			refreshAllRows();
	        			}
	        			else
	        			{
	        				mapeo.setIdCodigo(mapeoOrig.getIdCodigo());
	        				mapeo.setIdTurno(mapeoOrig.getIdTurno());
		        			mapeo.setOperario(mapeoOrig.getOperario());
		        			
			        		cs.guardarCambiosOperariosParte(mapeo);
			        		
			        		((ArrayList<MapeoHorasTurno>) vector).remove(mapeoOrig);
			    			((ArrayList<MapeoHorasTurno>) vector).add(mapeo);
	
			    			refreshAllRows();
			    			scrollTo(mapeo);
			    			select(mapeo);
	        			}
	        		}
        		}
	        }
		});
	}
	
	public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	this.getDefaultHeaderRow().getCell("eliminar").setHtml(FontAwesome.REMOVE.getHtml());
    	
    	this.mapeo = new MapeoHorasTurno();
		this.mapeoOrig=((MapeoHorasTurno) getEditedItemId());
    	
		this.mapeo.setIdCodigo(this.mapeoOrig.getIdCodigo());
		this.mapeo.setIdTurno(this.mapeoOrig.getIdTurno());
		this.mapeo.setOperario(this.mapeoOrig.getOperario());
		
    	super.doEditItem();
    }
  
	@Override
  	public void doCancelEditor()
  	{
		this.getDefaultHeaderRow().getCell("eliminar").setHtml("");
		super.doCancelEditor();
  	}

	@Override
	public void calcularTotal() {
		Double total = new Double(0) ;
		Integer cuantos = new Integer(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
        		if (item.getItemProperty("horasTotalesLinea")!=null && item.getItemProperty("horasTotalesLinea").getValue().toString().length()>0)
        		{
		        	Double q1Value = new Double(RutinasCadenas.reemplazarComaMiles(item.getItemProperty("horasTotalesLinea").getValue().toString()));
		        	if (item.getItemProperty("horasTotalesFueraLinea").getValue()!=null && item.getItemProperty("horasTotalesFueraLinea").getValue().toString().length()>0) q1Value = q1Value + new Double(RutinasCadenas.reemplazarComaMiles(item.getItemProperty("horasTotalesFueraLinea").getValue().toString()));
		        	if (q1Value!=null)
		        	{
		        		total += q1Value;
		        		cuantos++; 
		        	}
        		}
        	}
        }
        
        footer.getCell("horasMANUAL").setText("Total:");
		footer.getCell("horasTotalesLinea").setText(RutinasNumericas.formatearDoubleDecimales((total/cuantos), new Integer(0)).toString());
		
		if (this.app!=null)
		{
//			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-50)+"%");
		}
		footer.getCell("horasMANUAL").setStyleName("Rcell-pie");
		footer.getCell("horasTotalesLinea").setStyleName("Rcell-pie");

	}
}


