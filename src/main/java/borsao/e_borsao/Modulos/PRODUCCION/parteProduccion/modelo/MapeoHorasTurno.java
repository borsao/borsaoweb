package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class MapeoHorasTurno  extends MapeoGlobal
{
    private Integer idTurno;
    private String operario;
    private String horasAUTOMATICO;
    private String horasJAULON;
    private String horasETIQUETADO;
    private String horasMANUAL;
    private String horasTotalesLinea;
    private String horasTotalesFueraLinea;
    private String motivoFueraLinea;
    private String Observaciones;
    private String horasAbsentismo;
    private String motivoAbsentismo;
    
    private boolean eliminar;
    
	public MapeoHorasTurno() 
	{
	}

	public Integer getIdTurno() {
		return idTurno;
	}

	public void setIdTurno(Integer idTurno) {
		this.idTurno = idTurno;
	}

	public String getOperario() {
		return operario;
	}

	public void setOperario(String operario) {
		this.operario = operario;
	}

	public String getHorasAUTOMATICO() {
		if (horasAUTOMATICO==null || horasAUTOMATICO.length()==0) return "";
		return horasAUTOMATICO;
	}

	public void setHorasAUTOMATICO(String horasAUTOMATICO) {
		this.horasAUTOMATICO = horasAUTOMATICO;
	}

	public String getHorasJAULON() {
		if (horasJAULON==null || horasJAULON.length()==0) return "";
		return horasJAULON;
	}

	public void setHorasJAULON(String horasJAULON) {		
		this.horasJAULON = horasJAULON;
	}

	public String getHorasETIQUETADO() {
		if (horasETIQUETADO==null || horasETIQUETADO.length()==0) return "";
		return horasETIQUETADO;
	}

	public void setHorasETIQUETADO(String horasETIQUETADO) {
		this.horasETIQUETADO = horasETIQUETADO;
	}

	public String getHorasMANUAL() {
		if (horasMANUAL==null || horasMANUAL.length()==0) return "";
		return horasMANUAL;
	}

	public void setHorasMANUAL(String horasMANUAL) {
		this.horasMANUAL = horasMANUAL;
	}

	public String getHorasTotalesLinea() {
		return horasTotalesLinea;
	}

	public void setHorasTotalesLinea(String horasTotalesLinea) {
		this.horasTotalesLinea = horasTotalesLinea;
	}

	public String getHorasTotalesFueraLinea() {
		return horasTotalesFueraLinea;
	}

	public void setHorasTotalesFueraLinea(String horasTotalesFueraLinea) {
		this.horasTotalesFueraLinea = horasTotalesFueraLinea;
	}

	public String getMotivoFueraLinea() {
		return motivoFueraLinea;
	}

	public void setMotivoFueraLinea(String motivoFueraLinea) {
		this.motivoFueraLinea = motivoFueraLinea;
	}

	public String getObservaciones() {
		return Observaciones;
	}

	public void setObservaciones(String observaciones) {
		Observaciones = observaciones;
	}

	public String getHorasAbsentismo() {
		return horasAbsentismo;
	}

	public void setHorasAbsentismo(String horasAbsentismo) {
		this.horasAbsentismo = horasAbsentismo;
	}

	public String getMotivoAbsentismo() {
		return motivoAbsentismo;
	}

	public void setMotivoAbsentismo(String motivoAbsentismo) {
		this.motivoAbsentismo = motivoAbsentismo;
	}
	
	public boolean isEliminar() {
		return eliminar;
	}


	public void setEliminar(boolean eliminar) {
		this.eliminar = eliminar;
	}
	
}