package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoCambioArticuloParte extends MapeoGlobal
{
    private Integer idIncidenciaProgramada;
    
    private String articulo;
    private String cambioArticulo;
    private String cambioTapon;
    private String cambioBotella;
    private String cambioCaja;
    private String tiempo;
    private String Observaciones;
    
	public MapeoCambioArticuloParte() 
	{
		
	}

	public Integer getIdIncidenciaProgramada() {
		return idIncidenciaProgramada;
	}

	public void setIdIncidenciaProgramada(Integer idIncidenciaProgramada) {
		this.idIncidenciaProgramada = idIncidenciaProgramada;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getCambioArticulo() {
		return cambioArticulo;
	}

	public void setCambioArticulo(String cambioArticulo) {
		this.cambioArticulo = cambioArticulo;
	}

	public String getCambioTapon() {
		return cambioTapon;
	}

	public void setCambioTapon(String cambioTapon) {
		this.cambioTapon = cambioTapon;
	}

	public String getCambioBotella() {
		return cambioBotella;
	}

	public void setCambioBotella(String cambioBotella) {
		this.cambioBotella = cambioBotella;
	}

	public String getCambioCaja() {
		return cambioCaja;
	}

	public void setCambioCaja(String cambioCaja) {
		this.cambioCaja = cambioCaja;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

	public String getObservaciones() {
		return Observaciones;
	}

	public void setObservaciones(String observaciones) {
		Observaciones = observaciones;
	}


}