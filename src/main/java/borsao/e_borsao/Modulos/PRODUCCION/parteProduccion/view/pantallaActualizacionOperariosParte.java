package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoCabeceraParte;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoHorasTurno;

public class pantallaActualizacionOperariosParte extends Window
{
	private MapeoCabeceraParte mapeoCabeceraParte = null;
	private MapeoHorasTurno mapeoHorasTurno = null;
	private parteProduccionView App = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonAVentana = null;
	private Button btnBotonCVentana = null;
	
	private TextField txtHorasAutomatico = null;
	private TextField txtHorasEtiquetado= null;
	private TextField txtHorasJaulon= null;
	private TextField txtHorasManual = null;
	private TextField txtHorasLinea = null;
	private TextField txtHorasFuera = null;
	private TextField txtMotivoFuera = null;
	
	private Label lblSerie = null;
	private Label lblTurno = null;
	private Label lblFecha = null;
	private Label lblEjercicio = null;
	
	/*
	 * Valores devueltos
	 */
	private Integer teoricas = null;
	
	public pantallaActualizacionOperariosParte(GridViewRefresh r_App, MapeoCabeceraParte r_mapeoCabecera, MapeoHorasTurno r_mapeo, String r_titulo)
	{
		this.mapeoHorasTurno= r_mapeo;
		this.mapeoCabeceraParte= r_mapeoCabecera;
		this.App = (parteProduccionView) r_App;
//		setWindowMode(WindowMode.MAXIMIZED);
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		VerticalLayout controles = new VerticalLayout();
		controles.setMargin(true);
		controles.setSpacing(true);
		controles.setHeightUndefined();
		
			HorizontalLayout fila1 = new HorizontalLayout();
			fila1.setCaption("Datos Turno");
			fila1.setWidthUndefined();
			fila1.setSpacing(true);
				this.lblEjercicio=new Label();
				this.lblEjercicio.setValue(r_mapeoCabecera.getEjercicio());
				this.lblEjercicio.setWidth("140px");
				this.lblFecha=new Label();
				this.lblFecha.setValue(r_mapeoCabecera.getFecha());
//				this.lblFecha.setValue(RutinasFechas.conversionDeString(r_mapeoCabecera.getFecha()).toString());
				this.lblFecha.setWidth("140px");
				this.lblSerie=new Label();
				this.lblSerie.setValue(r_mapeoCabecera.getLinea());
				this.lblSerie.setWidth("160px");
				this.lblTurno=new Label();
				this.lblTurno.setValue(r_mapeoCabecera.getTurno());
				this.lblTurno.setWidth("160px");
				
				fila1.addComponent(lblEjercicio);
				fila1.addComponent(lblFecha);
				fila1.addComponent(lblSerie);
				fila1.addComponent(lblTurno);
				
			HorizontalLayout fila2 = new HorizontalLayout();
			fila2.setWidthUndefined();
			fila2.setSpacing(true);
				this.txtHorasAutomatico=new TextField("Horas en Automatico ");				
				this.txtHorasAutomatico.setWidth("150px");
				this.txtHorasAutomatico.setRequired(true);
				this.txtHorasAutomatico.setValue("0");
				
				this.txtHorasJaulon=new TextField("Horas a Jaulon ");
				this.txtHorasJaulon.setWidth("150px");
				this.txtHorasJaulon.setRequired(true);
				this.txtHorasJaulon.setValue("0");

				this.txtHorasEtiquetado=new TextField("Horas Etiquetando Automatico ");
				this.txtHorasEtiquetado.setWidth("150px");
				this.txtHorasEtiquetado.setRequired(true);
				this.txtHorasEtiquetado.setValue("0");
				
				this.txtHorasManual=new TextField("Horas Etiquetando Manual ");
				this.txtHorasManual.setWidth("150px");
				this.txtHorasManual.setRequired(true);
				this.txtHorasManual.setValue("0");
				
				fila2.addComponent(txtHorasAutomatico);
				fila2.addComponent(txtHorasJaulon);
				fila2.addComponent(txtHorasEtiquetado);
				fila2.addComponent(txtHorasManual);

			HorizontalLayout fila3 = new HorizontalLayout();
			fila3.setWidthUndefined();
			fila3.setSpacing(true);
			
				this.txtHorasLinea=new TextField("Horas en Linea ");				
				this.txtHorasLinea.setWidth("150px");
				this.txtHorasLinea.setRequired(true);
				this.txtHorasLinea.setValue("0");
				
				this.txtHorasFuera=new TextField("Horas Fuera Linea ");
				this.txtHorasFuera.setWidth("150px");
				this.txtHorasFuera.setRequired(true);
				this.txtHorasFuera.setValue("0");
				
				this.txtMotivoFuera=new TextField("Motivo Fuera Linea ");
				this.txtMotivoFuera.setWidth("250px");
				this.txtMotivoFuera.setRequired(true);
				this.txtMotivoFuera.setValue("");
				
				fila3.addComponent(txtHorasLinea);
				fila3.addComponent(txtHorasFuera);
				fila3.addComponent(txtMotivoFuera);

		controles.addComponent(fila1);
		controles.addComponent(fila2);
		controles.addComponent(fila3);
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setMargin(true);
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("750px");
		this.setHeight("550px");
		
		btnBotonCVentana = new Button("Cancelar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
		btnBotonAVentana = new Button("Aceptar");
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_PRIMARY);
		btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
		
		botonera.addComponent(btnBotonAVentana);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_LEFT);
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_RIGHT);
		
		principal.addComponent(controles);
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		this.setContent(principal);

		this.habilitarCampos();
		this.cargarListeners();
	}

	private void habilitarCampos()
	{
	}
	private void calcularHorasLinea()
	{
		Double horasTotales = new Double(0);

		if (txtHorasAutomatico.getValue()!=null && txtHorasAutomatico.getValue().length()>0) horasTotales=	horasTotales + RutinasNumericas.formatearDoubleDeESP(RutinasCadenas.reemplazarPuntoMiles(txtHorasAutomatico.getValue()));
		if (txtHorasJaulon.getValue()!=null && txtHorasJaulon.getValue().length()>0) horasTotales=	horasTotales + RutinasNumericas.formatearDoubleDeESP(RutinasCadenas.reemplazarPuntoMiles(txtHorasJaulon.getValue()));
		if (txtHorasEtiquetado.getValue()!=null && txtHorasEtiquetado.getValue().length()>0) horasTotales=	horasTotales + RutinasNumericas.formatearDoubleDeESP(RutinasCadenas.reemplazarPuntoMiles(txtHorasEtiquetado.getValue()));
		if (txtHorasManual.getValue()!=null && txtHorasManual.getValue().length()>0) horasTotales=	horasTotales + RutinasNumericas.formatearDoubleDeESP(RutinasCadenas.reemplazarPuntoMiles(txtHorasManual.getValue()));

		txtHorasLinea.setValue(horasTotales.toString());
	}
	
	private void cargarListeners()
	{

		txtHorasJaulon.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularHorasLinea();
			}
		});
		txtHorasEtiquetado.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularHorasLinea();
			}
		});
		txtHorasManual.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularHorasLinea();
			}
		});
		txtHorasAutomatico.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				calcularHorasLinea();
			}
		});
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event)			
			{
				/*
				 * Establecemos los parametros correspondientes
				 */
				if (todoEnOrden())
				{
					if (txtHorasAutomatico.getValue()!=null && txtHorasAutomatico.getValue().length()>0) mapeoHorasTurno.setHorasAUTOMATICO(txtHorasAutomatico.getValue());
					if (txtHorasJaulon.getValue()!=null && txtHorasJaulon.getValue().length()>0) mapeoHorasTurno.setHorasJAULON(txtHorasJaulon.getValue());
					if (txtHorasEtiquetado.getValue()!=null && txtHorasEtiquetado.getValue().length()>0) mapeoHorasTurno.setHorasETIQUETADO(txtHorasEtiquetado.getValue());
					if (txtHorasManual.getValue()!=null && txtHorasManual.getValue().length()>0) mapeoHorasTurno.setHorasMANUAL(txtHorasManual.getValue());
					
					if (txtHorasLinea.getValue()!=null && txtHorasLinea.getValue().length()>0) mapeoHorasTurno.setHorasTotalesLinea(txtHorasLinea.getValue());
					if (txtHorasFuera.getValue()!=null && txtHorasFuera.getValue().length()>0) mapeoHorasTurno.setHorasTotalesFueraLinea(txtHorasFuera.getValue());
					if (txtMotivoFuera.getValue()!=null && txtMotivoFuera.getValue().length()>0) mapeoHorasTurno.setMotivoFueraLinea(txtMotivoFuera.getValue());
					
					App.actualizarDatosOperario(mapeoHorasTurno);
					
					close();
				}
				else
				{
					Notificaciones.getInstance().mensajeAdvertencia("Rellena correctamente los campos.");
				}
			}
		});
		btnBotonCVentana.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});	
	}

	/*
	 * Metodo que comprobara que tengamos correctamente cumplimentada la pantalla de peticion
	 */
	private boolean todoEnOrden()
	{
		/*
		 * Comprueba los campso obligatorios en pantalla 
		 */
		boolean devolver = true;
		Double horasAut = null;
		Double horasJau = null;
		Double horasEti = null;
		Double horasMan = null;
		
		if (txtHorasAutomatico.getValue()!=null && txtHorasAutomatico.getValue().length()>0) horasAut = new Double(txtHorasAutomatico.getValue());
		if (txtHorasJaulon.getValue()!=null && txtHorasJaulon.getValue().length()>0) horasJau = new Double(txtHorasJaulon.getValue());
		if (txtHorasEtiquetado.getValue()!=null && txtHorasEtiquetado.getValue().length()>0) horasEti = new Double(txtHorasEtiquetado.getValue());
		if (txtHorasManual.getValue()!=null && txtHorasManual.getValue().length()>0) horasJau = new Double(txtHorasManual.getValue());
		
		return devolver;
	}
}