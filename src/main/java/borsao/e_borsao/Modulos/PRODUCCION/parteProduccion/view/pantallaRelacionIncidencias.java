package borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.view;

import java.util.ArrayList;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.MapeoIncidenciasTurno;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.modelo.gridIncidenciasNoProgramadasCalidadGrid;
import borsao.e_borsao.Modulos.PRODUCCION.parteProduccion.server.consultaParteProduccionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaRelacionIncidencias extends Ventana
{
	private consultaParteProduccionServer cpps = null;
	/*
	 * Parte Grafica
	 */
	private Button btnBotonCVentana = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;

	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Integer ejercicio = null;
	private String semana = null;
	private String tipo = null;
	private String ambitoTemporal = null;
	private String maquina = null;
	private String lineaProduccion = null;
	
	private boolean creacion = false;
	public boolean actualizar = false;
	public boolean cambioArticulo = false;
	/*
	 * Entradas datos equipo
	 */
	
	public pantallaRelacionIncidencias(String r_area, String r_maquina, Integer r_ejercicio, String r_semana, String r_tipo, String r_calculo)
	{
		this.setCaption("Releacion incidencias");
		this.ejercicio=r_ejercicio;
		this.semana=r_semana;
		this.ambitoTemporal=r_calculo;
		this.tipo=r_tipo;
		this.maquina=r_maquina;
		this.lineaProduccion=r_area;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cpps = new consultaParteProduccionServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("850px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");

    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoIncidenciasTurno> vector=null;

    	vector = this.cpps.datosGlobalIncidenciasMaquinas(this.lineaProduccion, this.ejercicio, this.semana, this.tipo, this.maquina, this.ambitoTemporal);

    	this.generarGrid(vector);
    	
	}

	private void limpiarCampos()
	{
	}
	
	private void cerrar()
	{
		close();
	}

    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
//    	if (((MapeoCambioArticuloParte) r_fila)!=null)
//    	{
//    		btnEliminar.setEnabled(true);
//    		MapeoCambioArticuloParte mapeoCambio = (MapeoCambioArticuloParte) r_fila;
//    		if (mapeoCambio!=null) this.llenarEntradasDatos(mapeoCambio);
//    	}    			
//    	else this.limpiarCampos();
    	
    }

    private void generarGrid(ArrayList<MapeoIncidenciasTurno> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Incidencias");
    	panelGrid.setSizeFull(); // Shrink to fit content
    	
    	gridDatos = new gridIncidenciasNoProgramadasCalidadGrid(r_vector, null);
			
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setEditorEnabled(false);
		this.gridDatos.setConFiltro(false);
		this.gridDatos.setSeleccion(SelectionMode.SINGLE);
		this.gridDatos.setSizeFull();
		this.gridDatos.setWidth("100%");
		this.gridDatos.getColumn("acc").setHidden(true);
		this.gridDatos.getColumn("fecha").setHidden(false);
		this.gridDatos.getColumn("observaciones").setWidth(new Double(650));
		
		panelGrid.setContent(gridDatos);

		this.frameGrid.addComponent(panelGrid);
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
	}

	@Override
	public void aceptarProceso(String r_accion) {
//		String rdo = null;
//		rdo = cncs.eliminarCambioArticuloParte(this.mapeoCambiosArticuloParte.getIdCodigo());
//		
//		if (rdo!=null) 
//			Notificaciones.getInstance().mensajeError(rdo);
//		else
//			cerrar();

	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}