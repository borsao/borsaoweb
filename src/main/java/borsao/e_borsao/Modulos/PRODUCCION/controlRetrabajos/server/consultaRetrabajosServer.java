package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CRM.PedidosVentas.server.consultaPedidosVentasServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.Clientes.server.consultaClientesServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.modelo.MapeoProgramacionEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.server.consultaProgramacionEnvasadoraServer;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.server.consultaProgramacionServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaRetrabajosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private ArrayList<MapeoProgramacion> vectorProgramado = null;
	private static consultaRetrabajosServer instance;
	
	public consultaRetrabajosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRetrabajosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRetrabajosServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoRetrabajos> datosRetrabajosGlobal(MapeoRetrabajos r_mapeo)
	{
		ResultSet rsOpcion = null;
		Connection con = null;	
		Statement cs = null;
		Connection conG = null;
		Statement csG = null;
		ArrayList<MapeoRetrabajos> vector = null;
		StringBuffer cadenaWhere =null;
		MapeoRetrabajos mapeoRetrabajos=null;
		
		StringBuffer cadenaSQL = new StringBuffer();
//		r_mapeo.setArticulo("0102458");
		
		this.recogerProgramado();
		
		cadenaSQL.append(" SELECT prd_retrabajos.idRetrabajo rt_id,");
		cadenaSQL.append(" prd_retrabajos.origen rt_ori,");
		cadenaSQL.append(" prd_retrabajos.cliente rt_cli,");
		cadenaSQL.append(" prd_retrabajos.articulo rt_art,");
		cadenaSQL.append(" prd_retrabajos.descripcion rt_des,");
		cadenaSQL.append(" prd_retrabajos.cantidad rt_can,");
		cadenaSQL.append(" prd_retrabajos.factor rt_fac,");
		cadenaSQL.append(" prd_retrabajos.unidades rt_uni,");
		cadenaSQL.append(" prd_retrabajos.fecha rt_fec,");
		cadenaSQL.append(" prd_retrabajos.cumplimentacion rt_cum,");
		cadenaSQL.append(" prd_retrabajos.observaciones rt_obs, ");
		cadenaSQL.append(" prd_retrabajos.vencimiento rt_ven ");
		cadenaSQL.append(" from prd_retrabajos ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdRetrabajo()!=null && r_mapeo.getIdRetrabajo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idRetrabajo = '" + r_mapeo.getIdRetrabajo() + "' ");
				}
				if (r_mapeo.getOrigen()!=null && r_mapeo.getOrigen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" origen = '" + r_mapeo.getOrigen() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" articulo = '" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getDescripcion()!=null && r_mapeo.getDescripcion().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" descripcion = '" + r_mapeo.getDescripcion() + "' ");
				}
				if (r_mapeo.getCantidad()!=null && r_mapeo.getCantidad().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cantidad = " + r_mapeo.getCantidad() + " ");
				}
				if (r_mapeo.getFactor()!=null && r_mapeo.getFactor().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" factor = " + r_mapeo.getFactor() + " ");
				}
				if (r_mapeo.getUnidades()!=null && r_mapeo.getUnidades().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" unidades = " + r_mapeo.getUnidades() + " ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0)  cadenaWhere.append(" and ");
					if (r_mapeo.getEstado().equals("A") || r_mapeo.getEstado().equals("I")) cadenaWhere.append(" cumplimentacion is null ");
					if (r_mapeo.getEstado().equals("C")) cadenaWhere.append(" cumplimentacion is not null ");
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" observaciones = '" + r_mapeo.getObservaciones() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by fecha, idRetrabajo asc");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoRetrabajos>();
			
			while(rsOpcion.next())
			{
				mapeoRetrabajos = new MapeoRetrabajos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRetrabajos.setIdRetrabajo(rsOpcion.getInt("rt_id"));
				mapeoRetrabajos.setOrigen(rsOpcion.getString("rt_ori"));
				mapeoRetrabajos.setCliente(rsOpcion.getString("rt_cli"));
				mapeoRetrabajos.setNombre(this.obtenerNombreCliente(new Integer(mapeoRetrabajos.getCliente())));
				mapeoRetrabajos.setArticulo(rsOpcion.getString("rt_art"));
				mapeoRetrabajos.setDescripcion(rsOpcion.getString("rt_des"));
				mapeoRetrabajos.setCantidad(rsOpcion.getInt("rt_can"));
				mapeoRetrabajos.setFactor(rsOpcion.getInt("rt_fac"));
				mapeoRetrabajos.setUnidades(rsOpcion.getInt("rt_uni"));
				mapeoRetrabajos.setFecha(RutinasFechas.conversion(rsOpcion.getDate("rt_fec")));
				mapeoRetrabajos.setFechaCarga(RutinasFechas.conversion(rsOpcion.getDate("rt_ven")));
				mapeoRetrabajos.setCumplimentacion(rsOpcion.getString("rt_cum"));
				mapeoRetrabajos.setObservaciones(rsOpcion.getString("rt_obs"));
				
				MapeoRetrabajos mapCons = this.consultarRetrabajoProgramacion(rsOpcion.getString("rt_ori"), rsOpcion.getInt("rt_id"),null);
				
				mapeoRetrabajos.setIdProgramacion(mapCons.getIdProgramacion());
				mapeoRetrabajos.setIdProgramacionEnvasadora(mapCons.getIdProgramacionEnvasadora());

				if (mapeoRetrabajos.getCumplimentacion()!=null && mapeoRetrabajos.getCumplimentacion().length()>0) 
					mapeoRetrabajos.setEstado("C");
				else 
				{
					
					if ((mapCons.getIdProgramacion()!=null && mapCons.getIdProgramacion()!=0) || (mapCons.getIdProgramacionEnvasadora()!=null && mapCons.getIdProgramacionEnvasadora()!=0))
					{
						mapeoRetrabajos.setEstado("I");
					}
					else 
					{
						mapeoRetrabajos.setEstado("A");
					}
				}
				
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0 && r_mapeo.getEstado().equals(mapeoRetrabajos.getEstado()))
				{
					vector.add(mapeoRetrabajos);				
				}
				else if (r_mapeo.getEstado()==null)
				{
					vector.add(mapeoRetrabajos);				
				}
			}
			
			/*
			 * busqueda retrabajos greensys
			 */
			
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.codigo rt_id,");
			cadenaSQL.append(" a.posicion rt_lin,");
			cadenaSQL.append(" a.movimiento rt_ori,");
			cadenaSQL.append(" a.articulo rt_art,");
			cadenaSQL.append(" b.descrip_articulo rt_des,");
			cadenaSQL.append(" a.bultos rt_can,");
			cadenaSQL.append(" a.etiquetas rt_fac,");
			cadenaSQL.append(" a.unidades rt_uni,");
			cadenaSQL.append(" a.fecha_documento rt_fec,");
			cadenaSQL.append(" a.cumplimentacion rt_cum,");
			cadenaSQL.append(" a.descrip_articulo rt_obs, ");
			cadenaSQL.append(" a.receptor_emisor rt_cli, ");
			cadenaSQL.append(" p.fecha_list_ruta rt_fc, ");
			cadenaSQL.append(" p.ref_cliente rt_ref, ");
			cadenaSQL.append(" p.finalidad rt_fin, ");
			cadenaSQL.append(" c.nombre_comercial rt_nom ");
			cadenaSQL.append(" from a_vp_l_0 a, e_articu b, e_client c, a_vp_c_0 p ");
			
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				cadenaWhere.append(" a.articulo[2,8]=b.articulo  ");
				cadenaWhere.append(" and a.receptor_emisor=c.cliente ");
				cadenaWhere.append(" and a.codigo=p.codigo");
				cadenaWhere.append(" and a.serie=p.serie");
				cadenaWhere.append(" and a.documento=p.documento");
				cadenaWhere.append(" and a.almacen=p.almacen");
				cadenaWhere.append(" and a.movimiento = 'RE' ");
				
				if (r_mapeo.getIdRetrabajo()!=null && r_mapeo.getIdRetrabajo().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.codigo = '" + r_mapeo.getIdRetrabajo() + "' ");
				}
				if (r_mapeo.getArticulo()!=null && r_mapeo.getArticulo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" b.articulo matches '*" + r_mapeo.getArticulo() + "' ");
				}
				if (r_mapeo.getFecha()!=null && r_mapeo.getFecha().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.fecha_documento = '" + RutinasFechas.convertirDateToString(r_mapeo.getFecha()) + "' ");
				}
				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0)
				{
					if (cadenaWhere.length()>0) 
					{
						if (!r_mapeo.getEstado().equals("T")) cadenaWhere.append(" and ");
						if (r_mapeo.getEstado().equals("A") || r_mapeo.getEstado().equals("I")) cadenaWhere.append(" a.cumplimentacion is null ");
						if (r_mapeo.getEstado().equals("C")) cadenaWhere.append(" a.cumplimentacion is not null ");
					}
				}
				if (r_mapeo.getObservaciones()!=null && r_mapeo.getObservaciones().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" a.descrip_articulo matches '*" + r_mapeo.getObservaciones() + "*' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			rsOpcion.close();
			cadenaSQL.append(" order by 9, 1, 2");
			conG= this.conManager.establecerConexionGestionInd();			
			csG = conG.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= csG.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			while(rsOpcion.next())
			{
				mapeoRetrabajos = new MapeoRetrabajos();
				/*
				 * recojo mapeo operarios
				 */
				mapeoRetrabajos.setIdRetrabajo(rsOpcion.getInt("rt_id"));
				mapeoRetrabajos.setOrigen("PEDIDOS");
				mapeoRetrabajos.setArticulo(rsOpcion.getString("rt_art").substring(1, 8));
				mapeoRetrabajos.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("rt_des")));
				mapeoRetrabajos.setCliente(rsOpcion.getString("rt_cli"));
				mapeoRetrabajos.setNombre(RutinasCadenas.conversion(rsOpcion.getString("rt_nom")));
				mapeoRetrabajos.setCantidad(rsOpcion.getInt("rt_can"));
				mapeoRetrabajos.setFactor(rsOpcion.getInt("rt_fac"));
				mapeoRetrabajos.setUnidades(rsOpcion.getInt("rt_uni"));
				mapeoRetrabajos.setFecha(RutinasFechas.conversion(rsOpcion.getDate("rt_fec")));
				mapeoRetrabajos.setFechaCarga(RutinasFechas.conversion(rsOpcion.getDate("rt_fc")));
				mapeoRetrabajos.setReferencia(rsOpcion.getString("rt_ref"));
				mapeoRetrabajos.setFinalidad(RutinasCadenas.conversion(rsOpcion.getString("rt_fin")));
				mapeoRetrabajos.setCumplimentacion(rsOpcion.getString("rt_cum"));
				mapeoRetrabajos.setObservaciones(RutinasCadenas.conversion(rsOpcion.getString("rt_obs")));
				mapeoRetrabajos.setLinea(rsOpcion.getInt("rt_lin"));
				
				MapeoRetrabajos mapCons = this.consultarRetrabajoProgramacion("PEDIDOS", rsOpcion.getInt("rt_id"),rsOpcion.getInt("rt_lin"));
				mapeoRetrabajos.setIdProgramacion(mapCons.getIdProgramacion());
				mapeoRetrabajos.setIdProgramacionEnvasadora(mapCons.getIdProgramacionEnvasadora());

				if (mapeoRetrabajos.getCumplimentacion()!=null && mapeoRetrabajos.getCumplimentacion().length()>0) 
					mapeoRetrabajos.setEstado("C");
				else 
				{
					
					if ((mapCons.getIdProgramacion()!=null && mapCons.getIdProgramacion()!=0) || (mapCons.getIdProgramacionEnvasadora()!=null && mapCons.getIdProgramacionEnvasadora()!=0))
					{
						mapeoRetrabajos.setEstado("I");
					}
					else 
					{
						mapeoRetrabajos.setEstado("A");
					}
				}

				if (r_mapeo.getEstado()!=null && r_mapeo.getEstado().length()>0 && r_mapeo.getEstado().equals(mapeoRetrabajos.getEstado()))
				{
					vector.add(mapeoRetrabajos);				
				}
				else if (r_mapeo.getEstado()==null)
				{
					vector.add(mapeoRetrabajos);				
				}			
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (csG !=null)
				{
					csG.close();
					csG=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
				if (conG!=null)
				{
					conG.close();
					conG=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return vector;
	}
	
	public boolean comprobarRetrabajos(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;
		
		cadenaSQL.append(" SELECT prd_retrabajos.idRetrabajo rt_id, ");
		cadenaSQL.append(" prd_retrabajos.articulo rt_art ");
		cadenaSQL.append(" from prd_retrabajos ");
		cadenaSQL.append(" where idNoConformidad = " + r_id);
		cadenaSQL.append(" and origen = 'CALIDAD'" );
		
		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return true;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return false;
	}
	
	public ArrayList<String> comprobarRetrabajos()
	{
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;
		ArrayList<String> vectRetr = null;
		consultaPedidosVentasServer cps = consultaPedidosVentasServer.getInstance(CurrentUser.get());
		
		vectRetr = cps.recogerRetrabajos();
		
		cadenaSQL.append(" SELECT prd_retrabajos.idRetrabajo rt_id, ");
		cadenaSQL.append(" prd_retrabajos.articulo rt_art ");
		cadenaSQL.append(" from prd_retrabajos ");
		cadenaSQL.append(" where cumplimentacion = 'N'");
		
		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			if (vectRetr==null || vectRetr.size()==0) vectRetr = new ArrayList<String>();
			
			while(rsOpcion.next())
			{
				vectRetr.add(rsOpcion.getString("rt_art"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		return vectRetr;
	}
	
	public String imprimirRetrabajo(Integer r_id)
	{
		String resultadoGeneracion = null;

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		Connection con=null;
		Statement cs = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_retrabajos.idRetrabajo) rt_sig ");
     	cadenaSQL.append(" FROM prd_retrabajos ");
		try
		{
     	
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("rt_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return 1;
	}


	public String guardarNuevo(MapeoRetrabajos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con=null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_retrabajos( ");
			cadenaSQL.append(" prd_retrabajos.idRetrabajo,");			
			cadenaSQL.append(" prd_retrabajos.origen, ");			
			cadenaSQL.append(" prd_retrabajos.articulo, ");
			cadenaSQL.append(" prd_retrabajos.descripcion, ");
			cadenaSQL.append(" prd_retrabajos.cantidad, ");
			cadenaSQL.append(" prd_retrabajos.factor, ");
			cadenaSQL.append(" prd_retrabajos.unidades, ");
			cadenaSQL.append(" prd_retrabajos.fecha, ");
			cadenaSQL.append(" prd_retrabajos.cumplimentacion, ");
			cadenaSQL.append(" prd_retrabajos.observaciones, ");
			cadenaSQL.append(" prd_retrabajos.cliente,");
			cadenaSQL.append(" prd_retrabajos.vencimiento ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    preparedStatement.setInt(1, r_mapeo.getIdRetrabajo());
		    String fecha = "";
		    
		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
		    
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
	    	preparedStatement.setString(8, RutinasFechas.convertirAFechaMysql(fecha));
	    	
		    if (r_mapeo.getCumplimentacion()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getCumplimentacion());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    
		    if (r_mapeo.getCliente()!=null)
		    {
		    	preparedStatement.setString(11, r_mapeo.getCliente());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getFechaCarga()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaCarga());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(12, RutinasFechas.convertirAFechaMysql(fecha));
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return "A";
	}
	
	public String guardarCambios(MapeoRetrabajos r_mapeo)
	{
		PreparedStatement preparedStatement = null;		
		Connection con = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		if (r_mapeo.getOrigen().equals("PEDIDOS")) return "Registro no modificable. Modifica en GREENsys";
		try
		{
			
		    cadenaSQL.append(" UPDATE prd_retrabajos set ");
		    cadenaSQL.append(" prd_retrabajos.origen =?,");
		    cadenaSQL.append(" prd_retrabajos.articulo =?,");
		    cadenaSQL.append(" prd_retrabajos.descripcion =?,");
		    cadenaSQL.append(" prd_retrabajos.cantidad =?,");
		    cadenaSQL.append(" prd_retrabajos.factor =?,");
			cadenaSQL.append(" prd_retrabajos.unidades =?,");
			cadenaSQL.append(" prd_retrabajos.fecha =?,");
			cadenaSQL.append(" prd_retrabajos.cumplimentacion =?,");
			cadenaSQL.append(" prd_retrabajos.observaciones = ?, ");
			cadenaSQL.append(" prd_retrabajos.cliente = ?, ");
			cadenaSQL.append(" prd_retrabajos.vencimiento = ? ");
			cadenaSQL.append(" where prd_retrabajos.idRetrabajo = ? ");
			
		    con= this.conManager.establecerConexionInd();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    
		    String fecha ="";

		    if (r_mapeo.getOrigen()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getOrigen());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getFactor()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getFactor());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
			   
		    if (r_mapeo.getUnidades()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getUnidades());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getFecha()!=null)
		    {
		    	fecha=RutinasFechas.convertirDateToString(r_mapeo.getFecha());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    
		    preparedStatement.setString(7, RutinasFechas.convertirAFechaMysql(fecha));
		    if (r_mapeo.getCumplimentacion()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getCumplimentacion());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(9, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(9, null);
		    }
		    if (r_mapeo.getCliente()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getCliente());
		    }
		    else
		    {
		    	preparedStatement.setInt(10, 0);
		    }
		    if (r_mapeo.getFechaCarga()!=null)
		    {
		    	fecha =RutinasFechas.convertirDateToString(r_mapeo.getFechaCarga());
		    }
		    else
		    {
		    	fecha = RutinasFechas.convertirDateToString(new Date());
		    }
		    preparedStatement.setString(11, RutinasFechas.convertirAFechaMysql(fecha));
		    preparedStatement.setInt(12, r_mapeo.getIdRetrabajo());
		    
		    preparedStatement.executeUpdate();
		    
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return r_mapeo.getEstado();
	}
	
	public String cumplimentar(Integer r_id, String r_origen, Integer r_linea, boolean r_cumplimentar)
	{
		Connection conG = null;
		Connection con = null;
		PreparedStatement preparedStatement = null;		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
		   
		    if(r_origen.equals("PEDIDOS"))
		    {
		    	/*
		    	 * cumplimentar linea en greensys
		    	 */
		    	cadenaSQL = new StringBuffer();
		    	
		    	cadenaSQL.append(" UPDATE a_vp_l_0 set ");
			    cadenaSQL.append(" cumplimentacion = ?, ");
			    if (r_cumplimentar) cadenaSQL.append(" cantidad_serv = cantidad, "); else cadenaSQL.append(" cantidad_serv = 0, "); 
			    if (r_cumplimentar) cadenaSQL.append(" piezas_serv = piezas, "); else cadenaSQL.append(" piezas_serv = 0, ");
			    if (r_cumplimentar) cadenaSQL.append(" unidades_serv = unidades "); else cadenaSQL.append(" unidades_serv = 0 ");
				cadenaSQL.append(" where codigo = ? ");
				cadenaSQL.append(" and posicion = ? ");
				
				conG= this.conManager.establecerConexionGestionInd();	 
			    preparedStatement = conG.prepareStatement(cadenaSQL.toString());
			    
			    if (r_cumplimentar)
			    	preparedStatement.setString(1, RutinasFechas.fechaActual());
			    else
			    	preparedStatement.setString(1, "");
			    
			    preparedStatement.setInt(2, r_id);
			    preparedStatement.setInt(3, r_linea);
			    
			    preparedStatement.executeUpdate();
		    }
		    else
		    {
		    	cadenaSQL = new StringBuffer();
		    	
		    	cadenaSQL.append(" UPDATE prd_retrabajos set ");
		    	cadenaSQL.append(" prd_retrabajos.cumplimentacion =? ");
		    	cadenaSQL.append(" where prd_retrabajos.idRetrabajo = ? ");
		    	
		    	con= this.conManager.establecerConexionInd();	 
		    	preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    	
		    	preparedStatement.setString(1, RutinasFechas.convertirAFechaMysql(RutinasFechas.fechaActual()));
		    	preparedStatement.setInt(2, r_id);
		    	
		    	preparedStatement.executeUpdate();

		    }
		    
	    	MapeoRetrabajos mapCons = this.consultarRetrabajoProgramacion(r_origen, r_id,r_linea);

	    	if (mapCons!=null && mapCons.getIdProgramacion()!=null && mapCons.getIdProgramacion()!=0)
	    	{
	    		consultaProgramacionServer cps = consultaProgramacionServer.getInstance(CurrentUser.get());
	    		MapeoProgramacion mapeoProgr = new MapeoProgramacion();
				mapeoProgr = cps.datosProgramacionGlobal(mapCons.getIdProgramacion());
					
				if (r_cumplimentar)
				{
					mapeoProgr.setEstado("T");
			    }
			    else
			    {
			    	mapeoProgr.setEstado("A");
			    }
				cps.guardarCambios(mapeoProgr);
	    	}
	    	else if (mapCons!=null && mapCons.getIdProgramacionEnvasadora()!=null && mapCons.getIdProgramacionEnvasadora()!=0)
	    	{
	    		consultaProgramacionEnvasadoraServer ces = consultaProgramacionEnvasadoraServer.getInstance(CurrentUser.get());
	    		MapeoProgramacionEnvasadora mapeoProgr = new MapeoProgramacionEnvasadora();
				mapeoProgr = ces.datosProgramacionGlobal(mapCons.getIdProgramacionEnvasadora());
					
				if (r_cumplimentar)
				{
					mapeoProgr.setEstado("T");
			    }
			    else
			    {
			    	mapeoProgr.setEstado("A");
			    }
				ces.guardarCambios(mapeoProgr);	    		
	    	}
		}
		catch (Exception ex)
	    {
			serNotif.mensajeError(ex.getMessage());	    
			return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
				if (conG!=null)
				{
					conG.close();
					conG=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return "C";
	}
	public void eliminar(MapeoRetrabajos r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_retrabajos ");            
			cadenaSQL.append(" WHERE prd_retrabajos.idRetrabajo = ?"); 
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdRetrabajo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		permisos = cos.obtenerPermisos("Embotelladora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("bib");
			
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="99";
				}
			}
		}
		return permisos;
	}

	public String asociarProgramacion(MapeoRetrabajos r_mapeo)
	{
		//prd_retrabajos_prg
		
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_retrabajos_prg( ");
			cadenaSQL.append(" prd_retrabajos_prg.idRetrabajo,");			
			cadenaSQL.append(" prd_retrabajos_prg.codigoPedido, ");			
			cadenaSQL.append(" prd_retrabajos_prg.lineaPedido, ");
			cadenaSQL.append(" prd_retrabajos_prg.idProgramacion, ");
			cadenaSQL.append(" prd_retrabajos_prg.idProgramacionEnvasadora ) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

		    if (r_mapeo.getOrigen().equals("PEDIDOS"))
		    {
		    	preparedStatement.setInt(1, 0);
		    	preparedStatement.setInt(2, r_mapeo.getIdRetrabajo());
		    	preparedStatement.setInt(3, r_mapeo.getLinea());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, r_mapeo.getIdRetrabajo());
		    	preparedStatement.setInt(2, 0);
		    	preparedStatement.setInt(3, 0);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    
		    if (r_mapeo.getIdProgramacionEnvasadora()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getIdProgramacionEnvasadora());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
	        preparedStatement.executeUpdate();
	        
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return null;
	}

	public MapeoRetrabajos consultarRetrabajoProgramacion(String r_orig, Integer r_id, Integer r_lin)
	{
		//prd_retrabajos_prg
		Connection con = null;	
		Statement cs = null;
		ResultSet rsOpcion = null;
		MapeoRetrabajos mapeo = null;
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" select prd_retrabajos_prg.idRetrabajo rt_id,");			
			cadenaSQL.append(" prd_retrabajos_prg.codigoPedido rt_cd, ");			
			cadenaSQL.append(" prd_retrabajos_prg.lineaPedido rd_lin, ");
			cadenaSQL.append(" prd_retrabajos_prg.idProgramacion rt_idp, ");
			cadenaSQL.append(" prd_retrabajos_prg.idProgramacionEnvasadora rt_idpe ");
			cadenaSQL.append(" from prd_retrabajos_prg ");
			if (r_orig.equals("MANUALES") || r_orig.equals("CALIDAD")) cadenaSQL.append(" where prd_retrabajos_prg.idRetrabajo = " + r_id);
			else if (r_orig.equals("PEDIDOS"))
			{
				cadenaSQL.append(" where prd_retrabajos_prg.codigoPedido = " + r_id);
				cadenaSQL.append(" and prd_retrabajos_prg.lineaPedido = " + r_lin);
			}
			
		    con= this.conManager.establecerConexionInd();	
		    cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
		    rsOpcion= cs.executeQuery(cadenaSQL.toString());
		    Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
		    mapeo=new MapeoRetrabajos();
		    
		    while(rsOpcion.next())
		    {
		    	mapeo.setIdRetrabajo(rsOpcion.getInt("rt_id"));
		    	mapeo.setIdProgramacion(rsOpcion.getInt("rt_idp"));
		    	mapeo.setIdProgramacionEnvasadora(rsOpcion.getInt("rt_idpe"));
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    }
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return mapeo;
	}

	@Override
	public String semaforos() 
	{
		ResultSet rsOpcion = null;
		Connection con = null;
		Statement cs = null;

		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_retrabajos.idRetrabajo rt_id,");
		cadenaSQL.append(" prd_retrabajos.origen rt_ori,");
		cadenaSQL.append(" prd_retrabajos.articulo rt_art,");
		cadenaSQL.append(" prd_retrabajos.descripcion rt_des,");
		cadenaSQL.append(" prd_retrabajos.cantidad rt_can,");
		cadenaSQL.append(" prd_retrabajos.factor rt_fac,");
		cadenaSQL.append(" prd_retrabajos.unidades rt_uni,");
		cadenaSQL.append(" prd_retrabajos.fecha rt_fec,");
		cadenaSQL.append(" prd_retrabajos.cumplimentacion rt_cum,");
		cadenaSQL.append(" prd_retrabajos.observaciones rt_obs, ");
		cadenaSQL.append(" prd_retrabajos.idProgramacion rt_id_pro ");
		cadenaSQL.append(" from prd_retrabajos ");
		
		try
		{
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				return "2";
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return "0";
	}

	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}

	public String obtenerNombreCliente(Integer r_cliente)
	{
		String rdo = null;
		consultaClientesServer cas = consultaClientesServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerNombreCliente(r_cliente);
		return rdo;
	}

	public ArrayList<MapeoAyudas> vector(String r_area)
	{
		ArrayList<MapeoAyudas> r_vector = null;
		ResultSet rsAyuda = null;
		Connection con = null;
		Statement cs = null;
		
		String sql = null;
		try
		{
			r_vector = new ArrayList<MapeoAyudas>();
			
			if (r_area.equals("Articulo")) sql="select articulo codigo, descrip_articulo descr from e_articu where articulo between '0100000' and '0109999' order by 1 asc" ;
			if (r_area.equals("Cliente")) sql="select cliente codigo, nombre_comercial descr from e_client order by 1 asc" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
			rsAyuda = cs.executeQuery(sql);
			while(rsAyuda.next())
			{
				MapeoAyudas mA = new MapeoAyudas();
				mA.setCodigo(rsAyuda.getString("codigo"));
				mA.setDescripcion(rsAyuda.getString("descr"));
				r_vector.add(mA);				
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsAyuda!=null)
				{
					rsAyuda.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return r_vector;
	}

	private void recogerProgramado()
	{
		
		MapeoProgramacion mapeo = null;
		
		consultaProgramacionServer cps = new consultaProgramacionServer(CurrentUser.get());
		mapeo = new MapeoProgramacion();
		mapeo.setEstado("X");		
		vectorProgramado = cps.datosProgramacionGlobal(mapeo);
     	
	}

	private Integer obtenerProgramado(String r_articulo)
	{
		for (int i=0;i<vectorProgramado.size();i++)
		{
			MapeoProgramacion map = (MapeoProgramacion) vectorProgramado.get(i);
			if (map.getArticulo().trim().equals(r_articulo.trim()))
			{
				return map.getIdProgramacion();
			}
		}
		return null;
	}
	
	public String generarInforme(MapeoRetrabajos r_mapeo)
	{
		String informe = "";
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

//		if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()!=0)
//				libImpresion.setCodigo(r_mapeo.getIdProgramacion());
//		else
//			libImpresion.setCodigo(r_mapeo.get());
		libImpresion.setCodigo(1);
		libImpresion.setArchivoDefinitivo("/retrabajo" + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("retrabajos.jasper");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("retrabajos");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlanco.jpg");

		resultadoGeneracion = libImpresion.generacionInformeInteger();

		if (resultadoGeneracion == null)
			informe = libImpresion.getArchivoDefinitivo();
		
		return informe;
		
	}
}