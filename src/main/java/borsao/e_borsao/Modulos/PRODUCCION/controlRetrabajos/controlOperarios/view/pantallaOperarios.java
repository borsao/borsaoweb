package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.view;

import java.util.ArrayList;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.Renderer;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.Combo;
import borsao.e_borsao.ClasesPropias.EntradaDatosFecha;
import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.Ventana;
import borsao.e_borsao.ClasesPropias.VentanaAceptarCancelar;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.modelo.MapeoControlOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.server.consultaRetrabajosOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view.RetrabajosView;
import borsao.e_borsao.modelo.Login.CurrentUser;


public class pantallaOperarios extends Ventana
{
	private MapeoControlOperarios mapeoControlOperarios = null;
	private consultaRetrabajosOperariosServer cncs  = null;
	
	private VerticalLayout principal=null;
	private VerticalLayout frameCentral = null;	
	private HorizontalLayout frameGrid = null;	
	private HorizontalLayout framePie = null;
	private Panel panelGrid = null;
	private GridPropio gridDatos = null;

	private Button btnBotonCVentana = null;
	private Button btnBotonAVentana = null;
	private Button btnLimpiar = null;
	private Button btnInsertar = null;
	private Button btnEliminar = null;
	
	private EntradaDatosFecha txtFecha = null;
	private TextField txtHoras= null;
	private Combo cmbOperario = null;
	
	private Label lblArticulo= null;
	private Label lblDescripcion= null;
	
	private String articulo= null;
	private String descripcion= null;
	
	private boolean creacion = false;
	private boolean hayGridPadre = false;
	private RetrabajosView app=null;

	/*
	 * Entradas datos equipo
	 */
	
	public pantallaOperarios(RetrabajosView r_app, String r_titulo, MapeoControlOperarios r_mapeo, String r_articulo, String r_descripcion)
	{
		this.setCaption(r_titulo);
		this.app=r_app;
		this.mapeoControlOperarios = r_mapeo;
		this.articulo=r_articulo;
		this.descripcion=r_descripcion;
		
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		
    	this.cncs = new consultaRetrabajosOperariosServer(CurrentUser.get());

    	this.setWidth("1200px");
    	this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarCombos();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);
		
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			
			/*
    		 * cargo controles registro pulsado
    		 */
    		
    		HorizontalLayout linea1 = new HorizontalLayout();
    			linea1.setSpacing(true);
    			
        		this.lblArticulo =new Label();
        		this.lblArticulo.setCaption(this.articulo);
        		this.lblArticulo.setWidth("100px");
        		this.lblArticulo.addStyleName(ValoTheme.LABEL_TINY);

        		this.lblDescripcion =new Label();
        		this.lblDescripcion.setCaption(this.descripcion);
        		this.lblDescripcion.setWidth("250px");
        		this.lblDescripcion.addStyleName(ValoTheme.LABEL_TINY);

				
    			linea1.addComponent(this.lblArticulo);
    			linea1.addComponent(this.lblDescripcion);
    			
    		HorizontalLayout linea2 = new HorizontalLayout();
    			linea2.setSpacing(true);

    			this.txtFecha = new EntradaDatosFecha();
    			this.txtFecha.setCaption("Fecha");
    			this.txtFecha.setEnabled(true);
    			this.txtFecha.setValue(RutinasFechas.fechaActual());
    			this.txtFecha.addStyleName(ValoTheme.TEXTFIELD_TINY);
    			this.txtFecha.setWidth("150px");

    			this.txtHoras = new TextField();
    			this.txtHoras.setCaption("Tiempo (minutos)");
    			this.txtHoras.setWidth("120px");
				this.txtHoras.addStyleName(ValoTheme.TEXTFIELD_TINY);
				this.txtHoras.addStyleName("rightAligned");

    			cmbOperario = new Combo("Operario");
    			cmbOperario.setWidth("200px");
    			cmbOperario.setStyleName(ValoTheme.TEXTFIELD_TINY);
    			cmbOperario.setNullSelectionAllowed(true);
    			cmbOperario.setNewItemsAllowed(true);
    			
				linea2.addComponent(this.txtFecha);
    			linea2.addComponent(this.txtHoras);
    			linea2.addComponent(this.cmbOperario);
    			
    		HorizontalLayout linea3 = new HorizontalLayout();
    			linea3.setSpacing(true);
    			
    			btnLimpiar= new Button("Nuevo");
    			btnLimpiar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnInsertar= new Button("Guardar");
    			btnInsertar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    			btnInsertar.addStyleName(ValoTheme.BUTTON_TINY);

    			btnEliminar= new Button("Eliminar");
    			btnEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
    			btnEliminar.addStyleName(ValoTheme.BUTTON_TINY);
    			btnEliminar.setEnabled(false);

    			linea3.addComponent(this.btnLimpiar);
    			linea3.setComponentAlignment(this.btnLimpiar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnInsertar);
    			linea3.setComponentAlignment(this.btnInsertar,Alignment.TOP_LEFT);
    			linea3.addComponent(this.btnEliminar);
    			linea3.setComponentAlignment(this.btnEliminar,Alignment.TOP_LEFT);

    		this.frameGrid = new HorizontalLayout();
    		this.frameGrid.setWidth("100%");
    		
    		this.frameCentral.addComponent(linea1);
    		this.frameCentral.addComponent(linea2);
    		this.frameCentral.addComponent(linea3);
    		
    		this.frameCentral.addComponent(this.frameGrid);
    		
			this.framePie = new HorizontalLayout();
			
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				
				btnBotonAVentana = new Button("Guardar");
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_TINY);
				btnBotonAVentana.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
//				framePie.addComponent(btnBotonAVentana);		
//				framePie.setComponentAlignment(btnBotonAVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(this.framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnBotonAVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				cerrar();
			}
		});
		
		btnLimpiar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				limpiar();
			}
		});

		btnInsertar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				guardar();
			}
		});

		btnEliminar.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				eliminar();
			}
		});
	}
	
	private void limpiar()
	{
		limpiarCampos();
	}
	
	private void eliminar()
	{
		MapeoControlOperarios mapeo = new MapeoControlOperarios();
		mapeo.setIdCodigo(this.mapeoControlOperarios.getIdCodigo());
		
		cncs.eliminar(mapeo);
		if (gridDatos!=null)
		{
			gridDatos.removeAllColumns();
			gridDatos=null;
			frameGrid.removeComponent(panelGrid);
			panelGrid=null;
		}
		llenarRegistros();
	}
	
	private void guardar()
	{
		String rdo = null;
		
		if (todoEnOrden())
		{
			MapeoControlOperarios mapeo = new MapeoControlOperarios();
			
			
			mapeo.setFecha(this.txtFecha.getValue());
			mapeo.setTiempo(new Integer(this.txtHoras.getValue()));
			mapeo.setOperario(this.cmbOperario.getValue().toString());
			mapeo.setCodigoPedido(this.mapeoControlOperarios.getCodigoPedido());
			mapeo.setLineaPedido(this.mapeoControlOperarios.getLineaPedido());
			mapeo.setIdProgramacion(this.mapeoControlOperarios.getIdProgramacion());

			if (isCreacion())
			{
				/*
				 * no tengo registros, luego llamo al guardar nuevo
				 */
				rdo = cncs.guardarNuevoOperario(mapeo);
				if (rdo==null)
				{
//					this.mapeoControlPreoprativo.setIdCodigo(mapeo.getIdCodigo());
					if (gridDatos!=null)
					{
						gridDatos.removeAllColumns();
						gridDatos=null;
					}
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
					llenarRegistros();
					limpiarCampos();
				}
			}
			else
			{
				/*
				 * como tengo datos de produccion del turno llamo a guardar cambios
				 */
				mapeo.setIdCodigo(this.mapeoControlOperarios.getIdCodigo());
				rdo = cncs.guardarCambiosOperario(mapeo);
				
				if (gridDatos!=null)
				{
					gridDatos.removeAllColumns();
					gridDatos=null;
					frameGrid.removeComponent(panelGrid);
					panelGrid=null;
				}
				llenarRegistros();
				limpiarCampos();
			}
			
			if (rdo!=null) Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
	
	private void llenarRegistros()
	{
    	
    	ArrayList<MapeoControlOperarios> vector=null;

    	vector = this.cncs.datosOpcionesGlobal(this.mapeoControlOperarios);

    	this.generarGrid(vector);
    	
	}

	private void llenarEntradasDatos(MapeoControlOperarios r_mapeo)
	{
		this.mapeoControlOperarios = r_mapeo;
		setCreacion(false);
		this.txtHoras.setValue(r_mapeo.getTiempo().toString());
		this.txtFecha.setValue(r_mapeo.getFecha());
		this.cmbOperario.setValue(r_mapeo.getOperario());
	}
	
    private void activarEntradasDatos(boolean r_activar)
    {
    	txtHoras.setEnabled(r_activar);
    	cmbOperario.setEnabled(r_activar);
    }
    
	public boolean isHayGridPadre() {
		return hayGridPadre;
	}

	public void setHayGridPadre(boolean hayGridPadre) {
		this.hayGridPadre = hayGridPadre;
	}

	private void limpiarCampos()
	{
		this.setCreacion(true);
		
		this.txtHoras.setValue("");
		this.cmbOperario.setValue(null);
	}
	
    private boolean todoEnOrden()
    {
    	if (this.cmbOperario.getValue()==null || this.cmbOperario.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar el realizador del control");
    		return false;
    	}
    	if (this.txtFecha.getValue()==null || this.txtFecha.getValue().toString().length()==0)
    	{
    		Notificaciones.getInstance().mensajeError("Debes indicar la fecha del retrabajo");
    		return false;
    	}
    	if ((this.txtHoras.getValue()==null || this.txtHoras.getValue().toString().length()==0))
    	{
    		Notificaciones.getInstance().mensajeError("Debes Indicar la hora de realizacion");
    		return false;
    	}
    	return true;
    }

	private void cerrar()
	{
		VentanaAceptarCancelar vt = new VentanaAceptarCancelar(this, "Quieres salir?", "Si", "No", "Salir", null);
		getUI().addWindow(vt);
	}

	@Override
	public void aceptarProceso(String r_accion) {
		close();
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}

	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.btnEliminar.setEnabled(!creacion);
		this.creacion = creacion;
	}

    private void cargarCombos()
    {
		cmbOperario.removeAllItems();
		
    	consultaOperariosServer cos = new consultaOperariosServer(CurrentUser.get());
    	ArrayList<MapeoOperarios> vector = null;
    	
    	MapeoOperarios mapeo = new MapeoOperarios();
    	mapeo.setArea("Embotelladora','Envasadora");
    	vector = cos.datosOperariosInArea(mapeo);
    	
    	for (int i = 0; i< vector.size();i++)
    	{
    		mapeo=vector.get(i);
    		cmbOperario.addItem(mapeo.getNombre());
    	}

    }

    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (((MapeoControlOperarios) r_fila)!=null)
    	{
    		MapeoControlOperarios mapeoOperarios = (MapeoControlOperarios) r_fila;
    		if (mapeoOperarios!=null) this.llenarEntradasDatos(mapeoOperarios);
    	}    			
    	else this.limpiarCampos();
    	
    	this.activarEntradasDatos(true);

    }

    private void generarGrid(ArrayList<MapeoControlOperarios> r_vector)
	{
    	/*
    	 * Creo el panel con el grid
    	 */
    	panelGrid = new Panel("Operarios Retrabajo");
    	panelGrid.setSizeFull(); // Shrink to fit content
//    	panelGrid.setHeight("450px");
//    	panelGrid.setWidth("100%");

    	/*
    	 * Creo el grid con el contenido del vector recibido
    	 */
		if (r_vector!= null && !r_vector.isEmpty())
		{
			BeanItemContainer<MapeoControlOperarios> container = new BeanItemContainer<MapeoControlOperarios>(MapeoControlOperarios.class);
			this.gridDatos=new GridPropio() {
				
				@Override
				public void establecerTitulosColumnas() {
					
			    	getColumn("fecha").setHeaderCaption("Fecha");
			    	getColumn("fecha").setWidth(new Double(120));
			    	getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
			    	getColumn("tiempo").setHeaderCaption("Tiempo");
			    	getColumn("tiempo").setWidth(new Double(100));
			    	getColumn("operario").setHeaderCaption("Operario");
			    	getColumn("operario").setWidth(new Double(320));

					getColumn("idCodigo").setHidden(true);
					getColumn("codigoPedido").setHidden(true);
					getColumn("lineaPedido").setHidden(true);
					getColumn("idProgramacion").setHidden(true);
				}
				
				@Override
				public void establecerOrdenPresentacionColumnas() {
					
					setColumnOrder("fecha", "tiempo", "operario");
					
				}
				
				@Override
				public void establecerColumnasNoFiltro() {
				}
				
				@Override
				public void cargarListeners() {
					addItemClickListener(new ItemClickEvent.ItemClickListener() 
			    	{
			            public void itemClick(ItemClickEvent event) 
			            {
			        		filaSeleccionada(event.getItemId());
			    		}
			        });
	
				}
				
				@Override
				public void calcularTotal() {
				}
				
				@Override
				public void asignarEstilos() {
				}
			};
			this.gridDatos.setContainerDataSource(container);
	
			this.gridDatos.getContainer().removeAllItems();
			this.gridDatos.getContainer().addAll(r_vector);
	
			
			this.gridDatos.addStyleName("minigrid");
			this.gridDatos.setEditorEnabled(false);
			this.gridDatos.setConFiltro(false);
			this.gridDatos.setSeleccion(SelectionMode.SINGLE);
			this.gridDatos.setSizeFull();
			this.gridDatos.setWidth("100%");
	    	
			this.gridDatos.establecerTitulosColumnas();
			this.gridDatos.establecerOrdenPresentacionColumnas();
			this.gridDatos.cargarListeners();
			
			panelGrid.setContent(gridDatos);
		}
		this.limpiarCampos();
		this.activarEntradasDatos(true);
    	this.frameGrid.addComponent(panelGrid);
	}
}