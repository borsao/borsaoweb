package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.modelo.MapeoControlOperarios;

public class consultaRetrabajosOperariosServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaRetrabajosOperariosServer  instance;
	
	public consultaRetrabajosOperariosServer (String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaRetrabajosOperariosServer  getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaRetrabajosOperariosServer (r_usuario);			
		}
		return instance;
	}


	public ArrayList<MapeoControlOperarios> datosOpcionesGlobal(MapeoControlOperarios r_mapeo)
	{
		ResultSet rsOpcion = null;		
		MapeoControlOperarios mapeo = null;
		ArrayList<MapeoControlOperarios> vectorMapeo = null;
		StringBuffer cadenaWhere =null;
		String tabla = null;
		String campo = null;
		StringBuffer cadenaSQL = null; 
		
		
		cadenaSQL = new StringBuffer();
		cadenaWhere = new StringBuffer();
		
		tabla = "prd_retrabajos_op";
		campo= "idProgramacion";
		cadenaWhere.append(" inner join prd_programacion prd_prog on prd_prog.idPrd_programacion = prd_retr.idProgramacion ");
		
		if (tabla!=null && tabla.length()>0)
		{
			cadenaSQL.append(" SELECT prd_retr.idCodigo rop_id, ");
			cadenaSQL.append(" prd_retr.tiempo rop_time, ");
			cadenaSQL.append(" prd_retr.fecha rop_fec, ");
			cadenaSQL.append(" prd_retr.operario rop_ope, ");
			cadenaSQL.append(" prd_retr.codigoPedido rop_ped, ");
			cadenaSQL.append(" prd_retr.lineaPedido rop_lin, ");
			cadenaSQL.append(" prd_retr.idProgramacion rop_idp ");
			
			cadenaSQL.append(" FROM " + tabla + " prd_retr ");
			
			try
			{
				if (r_mapeo!=null)
				{
				
					if (r_mapeo.getFecha()!=null)
					{
						cadenaWhere.append(" where prd_retr.fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())) + "' ");
					}
					if (r_mapeo.getIdProgramacion()!=null && r_mapeo.getIdProgramacion()>0)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_retr." + campo + " = " + r_mapeo.getIdProgramacion());
					}
					if (r_mapeo.getCodigoPedido()!=null)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_retr.codigoPedido = " + r_mapeo.getCodigoPedido());
					}
					if (r_mapeo.getLineaPedido()!=null)
					{
						if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
						cadenaWhere.append(" prd_retr.lineaPedido = " + r_mapeo.getLineaPedido());
					}
				}
				
				if (cadenaWhere!=null && cadenaWhere.length()>0) cadenaSQL.append(cadenaWhere.toString());
				
				con= this.conManager.establecerConexionInd();			
				cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
				Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
				rsOpcion= cs.executeQuery(cadenaSQL.toString());
				vectorMapeo = new ArrayList<MapeoControlOperarios>();
				
				while(rsOpcion.next())
				{
					mapeo = new MapeoControlOperarios();
					/*
					 * recojo mapeo operarios
					 */
					mapeo.setIdCodigo(rsOpcion.getInt("rop_id"));
					mapeo.setIdProgramacion(rsOpcion.getInt("rop_idp"));
					mapeo.setCodigoPedido(rsOpcion.getInt("rop_ped"));
					mapeo.setLineaPedido(rsOpcion.getInt("rop_lin"));
					mapeo.setTiempo(rsOpcion.getInt("rop_time"));
					mapeo.setOperario(rsOpcion.getString("rop_ope"));
//					mapeo.setObservaciones(rsOpcion.getString("rop_obs"));
					mapeo.setFecha(rsOpcion.getDate("rop_fec"));
					
					vectorMapeo.add(mapeo);
				}
				
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());			
			}
			finally
			{
				try
				{
					if (rsOpcion!=null)
					{
						rsOpcion.close();
					}
					if (cs!=null)
					{
						cs.close();
					}
					if (con!=null)
					{
						con.close();
					}
				}
				catch (Exception ex)
				{
					serNotif.mensajeError(ex.getMessage());
				}
			}
		}
		
		return vectorMapeo;
	}
	
	public String guardarNuevoOperario(MapeoControlOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null;
		
		tabla = "prd_retrabajos_op";

		try
		{
			cadenaSQL.append(" INSERT INTO " + tabla + " ( ");
			cadenaSQL.append(tabla + ".idCodigo, ");
			cadenaSQL.append(tabla + ".operario, ");
			cadenaSQL.append(tabla + ".tiempo, ");
			cadenaSQL.append(tabla + ".fecha, ");
			cadenaSQL.append(tabla + ".codigoPedido, ");
			cadenaSQL.append(tabla + ".lineaPedido, ");
			cadenaSQL.append(tabla + ".idProgramacion) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, tabla , "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getOperario()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getOperario());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setInt(3, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setInt(3, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(4, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(4, null);
	    	}

		    if (r_mapeo.getCodigoPedido()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getCodigoPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }
		    if (r_mapeo.getLineaPedido()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getLineaPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(7, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(7, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	
	public String guardarCambiosOperario(MapeoControlOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		String tabla = null; 
		tabla = "prd_retrabajos_op";
		try
		{
			cadenaSQL.append(" UPDATE " + tabla + " set ");
			cadenaSQL.append(tabla + ".operario=?, ");
			cadenaSQL.append(tabla + ".tiempo =?, ");
			cadenaSQL.append(tabla + ".fecha=?, ");
			cadenaSQL.append(tabla + ".codigoPedido=?, ");
			cadenaSQL.append(tabla + ".lineaPedido=?, ");
			cadenaSQL.append(tabla + ".idProgramacion =? ");
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo=? ");
		    
		    con= this.conManager.establecerConexionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getOperario()!=null)
		    {
		    	preparedStatement.setString(1, r_mapeo.getOperario());
		    }
		    else
		    {
		    	preparedStatement.setString(1, null);
		    }
		    if (r_mapeo.getTiempo()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getTiempo());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
	    	if (r_mapeo.getFecha()!=null)
	    	{
	    		preparedStatement.setString(3, RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFecha())));
	    	}
	    	else
	    	{
	    		preparedStatement.setString(3, null);
	    	}

		    if (r_mapeo.getCodigoPedido()!=null)
		    {
		    	preparedStatement.setInt(4, r_mapeo.getCodigoPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(4, 0);
		    }
		    if (r_mapeo.getLineaPedido()!=null)
		    {
		    	preparedStatement.setInt(5, r_mapeo.getLineaPedido());
		    }
		    else
		    {
		    	preparedStatement.setInt(5, 0);
		    }

		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    preparedStatement.setInt(7, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return null;
	}
	

	public void eliminar(MapeoControlOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		String tabla = null; 
		tabla = "prd_retrabajos_op";

		try
		{
			cadenaSQL.append(" DELETE FROM " + tabla );            
			cadenaSQL.append(" WHERE " + tabla + ".idCodigo = ?");
			
			con= this.conManager.establecerConexionInd();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}