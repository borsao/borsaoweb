package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoRetrabajos mapeo = null;
	 
	 public MapeoRetrabajos convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoRetrabajos();

		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setCumplimentacion(r_hash.get("cumplimentacion"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 
		 this.mapeo.setFecha(rtFecha.conversionDeString(r_hash.get("fecha")));
		 this.mapeo.setFechaCarga(rtFecha.conversionDeString(r_hash.get("fechaCarga")));
		 
		 if (r_hash.get("unidades")!=null && r_hash.get("unidades").length()>0) this.mapeo.setUnidades(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades"))));
		 if (r_hash.get("factor")!=null && r_hash.get("factor").length()>0) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor")));
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("idRetrabajo")!=null && r_hash.get("idRetrabajo").length()>0) this.mapeo.setIdRetrabajo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idRetrabajo")));
		 if (r_hash.get("linea")!=null && r_hash.get("linea").length()>0) this.mapeo.setLinea(RutinasNumericas.formatearIntegerDeESP(r_hash.get("linea")));
		 
		 return mapeo;		 
	 }
	 
	 public MapeoRetrabajos convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 RutinasFechas rtFecha = new RutinasFechas();
				 
		 this.mapeo = new MapeoRetrabajos();
		 
		 this.mapeo.setOrigen(r_hash.get("origen"));
		 this.mapeo.setCumplimentacion(r_hash.get("cumplimentacion"));
		 this.mapeo.setArticulo(r_hash.get("articulo"));
		 this.mapeo.setDescripcion(r_hash.get("descripcion"));
		 this.mapeo.setObservaciones(r_hash.get("observaciones"));
		 this.mapeo.setEstado(r_hash.get("estado"));
		 this.mapeo.setCliente(r_hash.get("cliente"));
		 
		 this.mapeo.setFecha(rtFecha.conversionDeString(r_hash.get("fecha")));
		 this.mapeo.setFechaCarga(rtFecha.conversionDeString(r_hash.get("fechaCarga")));
		 
		 if (r_hash.get("unidades")!=null && r_hash.get("unidades").length()>0) this.mapeo.setUnidades(new Integer(RutinasNumericas.formatearIntegerDeESP(r_hash.get("unidades"))));
		 if (r_hash.get("factor")!=null && r_hash.get("factor").length()>0) this.mapeo.setFactor(RutinasNumericas.formatearIntegerDeESP(r_hash.get("factor")));
		 if (r_hash.get("cantidad")!=null && r_hash.get("cantidad").length()>0) this.mapeo.setCantidad(RutinasNumericas.formatearIntegerDeESP(r_hash.get("cantidad")));
		 if (r_hash.get("idRetrabajo")!=null && r_hash.get("idRetrabajo").length()>0) this.mapeo.setIdRetrabajo(RutinasNumericas.formatearIntegerDeESP(r_hash.get("idRetrabajo")));
		 if (r_hash.get("linea")!=null && r_hash.get("linea").length()>0) this.mapeo.setLinea(RutinasNumericas.formatearIntegerDeESP(r_hash.get("linea")));
		 
		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoRetrabajos r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 this.hash.put("origen", r_mapeo.getOrigen());
		 this.hash.put("cumplimentacion", this.mapeo.getCumplimentacion());
		 this.hash.put("articulo", this.mapeo.getArticulo());
		 this.hash.put("descripcion", this.mapeo.getDescripcion());
		 this.hash.put("observaciones", this.mapeo.getObservaciones());
		 this.hash.put("estado", this.mapeo.getEstado());
		 this.hash.put("cliente", this.mapeo.getCliente());
		 
		 if (r_mapeo.getFecha()!=null)
		 {
			 this.hash.put("fecha", r_mapeo.getFecha().toString());
		 }
		 else
		 {
			 this.hash.put("fecha", null);
		 }
		 
		 if (r_mapeo.getFechaCarga()!=null)
		 {
			 this.hash.put("fechaCarga", r_mapeo.getFechaCarga().toString());
		 }
		 else
		 {
			 this.hash.put("fechaCarga", null);
		 }
		 
		 this.hash.put("unidades", String.valueOf(this.mapeo.getUnidades()));
		 this.hash.put("factor", String.valueOf(this.mapeo.getFactor()));
		 this.hash.put("cantidad", String.valueOf(this.mapeo.getCantidad()));
		 if (this.mapeo.getIdRetrabajo()!=null) this.hash.put("idRetrabajo", String.valueOf(this.mapeo.getIdRetrabajo()));
		 if (this.mapeo.getLinea()!=null) this.hash.put("linea", String.valueOf(this.mapeo.getLinea()));
		 
		 return hash;		 
	 }
}