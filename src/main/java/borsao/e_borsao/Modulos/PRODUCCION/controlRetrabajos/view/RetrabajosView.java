package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view;
 
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionAsignacionProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.RetrabajosGrid;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class RetrabajosView extends GridViewRefresh {

	public static final String VIEW_NAME = "Control RETRABAJOS";
	public consultaRetrabajosServer cus =null;
	public boolean modificando = false;
	public ComboBox cmbEstado = null;
	
	private final String titulo = "control RETRABAJOS";
	private int intervaloRefresco = 0*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private Integer permisos = 0;	
	private OpcionesForm form = null;
	private Button opcCompletar = null;	
	public boolean conTotales = true;
	public RetrabajosView app = null;
	private HashMap<String,String> filtrosRec = null;	
//	private Button opcImprimir = null;
//	private ExcelExporter opcExcel = null;
	
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public RetrabajosView() 
    {
    	this.opcionesEscogidas = new HashMap<String , String>();
    	app = this;
    }

    public void cargarPantalla() 
    {
    	    	
		this.cus= consultaRetrabajosServer.getInstance(CurrentUser.get());
		
		this.permisos = new Integer(this.cus.comprobarAccesos());
		this.permisos=99;
		if (this.permisos==99) this.intervaloRefresco=0;
    	
		
    	if (this.permisos==0)
    	{
    		Notificaciones.getInstance().mensajeError("No tienes acceso a este programa");
    		this.destructor();
    	}
    	else
    	{
    		setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    		
    		lblTitulo.setValue(this.titulo);//, ContentMode.HTML);
    		
    		this.opcCompletar= new Button("Completar");    	
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    		this.opcCompletar.addStyleName(ValoTheme.BUTTON_TINY);
    		this.opcCompletar.setIcon(FontAwesome.CHECK_CIRCLE);

    		this.cmbEstado= new ComboBox("Estado Retrabajos");    		
    		this.cmbEstado.setNewItemsAllowed(false);
    		this.cmbEstado.setNullSelectionAllowed(false);
    		this.cmbEstado.addStyleName(ValoTheme.COMBOBOX_TINY);

    		this.cabLayout.addComponent(this.cmbEstado);
    		this.cabLayout.addComponent(this.opcCompletar);
    		this.cabLayout.setComponentAlignment(this.opcCompletar,Alignment.BOTTOM_LEFT);
    		
    		this.cargarCombo();
    		this.cargarListeners();
    		this.establecerModo();
    		
    		this.generarGrid(opcionesEscogidas);
    	}
    }

    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoRetrabajos> r_vector=null;
    	MapeoRetrabajos mapeoRetrabajos =null;
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoRetrabajos=hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	if (this.cmbEstado.getValue().toString().equals("Pendientes"))
    	{
    		mapeoRetrabajos.setEstado("A");
    	}
    	else if (this.cmbEstado.getValue().toString().equals("Curso"))
    	{
    		mapeoRetrabajos.setEstado("I");
    	}
    	else if (this.cmbEstado.getValue().toString().equals("Terminados"))
    	{
    		mapeoRetrabajos.setEstado("C");
    	}
    	else
    	{
    		mapeoRetrabajos.setEstado("T");
    	}
    	r_vector=this.cus.datosRetrabajosGlobal(mapeoRetrabajos);
    	this.presentarGrid(r_vector);
    	((RetrabajosGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void newForm()
    {   
    	this.activarBotones(false);
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
    	this.modificando=false;
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    }
    
    public void verForm(boolean r_busqueda)
    {
    	
    	if (!isHayGrid())
    	{
    		this.navegacion(false);
    		this.verBotones(false);
    	}
    	
    	if (this.form==null)
    	{
    		this.form = new OpcionesForm(this);
    		addComponent(this.form);
    	}
		
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	
    	if (r_busqueda)
    	{
    		this.form.btnGuardar.setCaption("Buscar");
	    	this.form.btnEliminar.setEnabled(false);
    	}
    	else
    	{
    		this.form.btnGuardar.setCaption("Guardar");
    		this.form.btnEliminar.setEnabled(true);
    	}
    }
    
    public void reestablecerPantalla()
    {
//    	this.botonesGenerales(true);
    	if (grid==null || ((RetrabajosGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RetrabajosGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerModo();
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	if (!((RetrabajosGrid) this.grid).activadaVentanaPeticion && !((RetrabajosGrid) this.grid).ordenando)
    	{
    		if (((MapeoRetrabajos) r_fila)!=null)
    		{
	    		if (((MapeoRetrabajos) r_fila).getEstado().equals("I")) opcCompletar.setCaption("Completar");
	    		if (((MapeoRetrabajos) r_fila).getEstado().equals("C")) opcCompletar.setCaption("Reabrir");
	    		if (((MapeoRetrabajos) r_fila).getEstado().equals("A")) opcCompletar.setCaption("Iniciar");
	    		
		    	switch (this.permisos)
		    	{
			    	case 0:
			    		break;
			    	case 10:
			    		break;
			    	case 20:
			    		break;
			    	case 30:
			    		break;
			    	case 40:
			    		this.establecerBotonAccion(r_fila);
			    		break;
			    	case 50:
			    		break;
			    	case 70:
			    		break;
			    	case 80:
			    		break;
			    	case 99:
			    		this.establecerBotonAccion(r_fila);
			    		this.modificando=true;
			    		this.verForm(false);
				    	this.form.editarRetrabajo((MapeoRetrabajos) r_fila);
			    		break;
		    	}
    		}	    	
	    	
    	}    		
    }
    
    public void generarGrid(MapeoRetrabajos r_mapeo)
    {
    	ArrayList<MapeoRetrabajos> r_vector=null;
    	r_vector=this.cus.datosRetrabajosGlobal(r_mapeo);
    	this.presentarGrid(r_vector);
    	((RetrabajosGrid) grid).establecerFiltros(filtrosRec);
    }
    
    public void presentarGrid(ArrayList<MapeoRetrabajos> r_vector)
    {
    	grid = new RetrabajosGrid(this, this.permisos,r_vector);
    	if (((RetrabajosGrid) grid).vector==null)
    	{
    		setHayGrid(false);
    		this.navegacion(false);
    		this.verBotones(false);
    		this.activarBotones(false);
    	}
    	else if (((RetrabajosGrid) grid).vector.isEmpty())
    	{
    		setHayGrid(true);
    		this.navegacion(true);
    		this.verBotones(false);
    		this.activarBotones(false);	
    	}
    	else
    	{
    		setHayGrid(true);
    		this.establecerTableExportable();
    		this.establecerModo();
    	}
    }
    
    private void cargarListeners()
    {
//    	this.txtEjercicio.addValueChangeListener(new Property.ValueChangeListener() {
//			
//			@Override
//			public void valueChange(ValueChangeEvent event) {
//				if (isHayGrid())
//				{
//	 				grid.removeAllColumns();			
//	 				barAndGridLayout.removeComponent(grid);
//	 				grid=null;			
//	 				cargarCombo(txtEjercicio.getValue());
//				}
//			}
//		});
		this.cmbEstado.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (isHayGrid())
				{			
					filtrosRec = ((RetrabajosGrid) grid).recogerFiltros();
					grid.removeAllColumns();			
					barAndGridLayout.removeComponent(grid);
					grid=null;			
					MapeoRetrabajos mapeo = new MapeoRetrabajos();
					
					if (cmbEstado.getValue()!=null) 
					{
						if (cmbEstado.getValue().toString().equals("Pendientes"))
						{
							mapeo.setEstado("A");
							opcCompletar.setCaption("Iniciar");
						}
						else if (cmbEstado.getValue().toString().equals("Terminados"))
						{
							mapeo.setEstado("C");
							opcCompletar.setCaption("Reabrir");
						}
						else if (cmbEstado.getValue().toString().equals("Curso"))
						{
							mapeo.setEstado("I");
							opcCompletar.setCaption("Completar");
						}
					}
					else
					{
						mapeo.setEstado("A");
						opcCompletar.setCaption("Iniciar");
					}
					generarGrid(mapeo);
				}
			}
		}); 
    	this.opcCompletar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				MapeoRetrabajos mapeo=(MapeoRetrabajos) grid.getSelectedRow();
				
				if (mapeo!=null)
				{
//					if (opcCompletar.getCaption().equals("Completar"))
//					{
//	 					mapeo.setEstado("T");
//					}
//					else if (opcCompletar.getCaption().equals("Reabrir"))
//					{
//						mapeo.setEstado("A");
//					}
//					else if (opcCompletar.getCaption().equals("Aceptar"))
//					{
//						mapeo.setEstado("A");
//					}
//					else if (opcCompletar.getCaption().equals("Validar"))
//					{
//						mapeo.setEstado("A");
//					}
					if (opcCompletar.getCaption().equals("Iniciar"))
					{
						PeticionAsignacionProgramacion vt = null;
	    	    		vt= new PeticionAsignacionProgramacion(app, mapeo, "Programación Origen Retrabajo de " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
	    	    		getUI().addWindow(vt);
					}
					else if (opcCompletar.getCaption().equals("Completar"))
					{
						cus.cumplimentar(mapeo.getIdRetrabajo(), mapeo.getOrigen(), mapeo.getLinea(),true);
					}
					else if (opcCompletar.getCaption().equals("Reabrir"))
					{
						cus.cumplimentar(mapeo.getIdRetrabajo(), mapeo.getOrigen(), mapeo.getLinea(),false);
					}
					if (isHayGrid())
					{
						filtrosRec = ((RetrabajosGrid) grid).recogerFiltros();
						grid.removeAllColumns();			
						barAndGridLayout.removeComponent(grid);
						grid=null;
						generarGrid(opcionesEscogidas);
					}
				}				
			}
		});
    	
    	this.opcImprimir.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
//				imprimirRetrabajo();
			}
		});
    }
    
	private void establecerModo()
	{
		/*
		 * Establece:  0 - Sin permisos
		 * 			  10 - Laboratorio
		 * 			  30 - Solo Consulta
		 * 			  40 - Completar
		 * 			  50 - Cambio Observaciones
		 * 			  70 - Cambio observaciones y materia seca
		 * 			  80 - Cambio observaciones y materia seca y Creacion provisional
		 *  			 
		 * 		      99 - Acceso total
		 */
		this.opcNuevo.setVisible(false);
		this.opcNuevo.setEnabled(false);
//		this.opcImprimir.setVisible(true);
//		this.opcImprimir.setEnabled(true);

		switch (this.permisos)
		{
			case 10:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				break;
			case 20:
				this.setSoloConsulta(true);
				this.navegacion(true);
				this.verBotones(true);
				this.activarBotones(false);
				break;
			case 30:
				this.setSoloConsulta(true);
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 40:
				this.navegacion(true);
				this.verBotones(false);
				this.activarBotones(false);
				this.opcCompletar.setEnabled(true);
				this.opcCompletar.setVisible(true);
				break;
			case 50:
				this.verBotones(false);
				this.navegacion(true);
				this.activarBotones(false);
				break;
			case 70:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				break;
			case 80:
				this.verBotones(false);
				this.activarBotones(false);
				this.navegacion(true);
				
//				this.opcImprimir.setVisible(true);
//				this.opcImprimir.setEnabled(true);				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				break;				
			case 99:
				this.verBotones(true);
				this.activarBotones(true);
				this.navegacion(true);
				
				this.opcNuevo.setVisible(true);
				this.opcNuevo.setEnabled(true);
				setSoloConsulta(this.soloConsulta);
				break;			
		}
	}

	private void navegacion(boolean r_navegar)
	{
	}
	
	private void activarBotones(Boolean r_activo)
	{
		this.opcCompletar.setEnabled(r_activo);
//		this.opcExcel.setEnabled(r_activo);
	}

	private void verBotones(Boolean r_visibles)
	{
		this.opcCompletar.setVisible(r_visibles);
//		this.opcExcel.setVisible(r_visibles);
	}
	
	private void establecerBotonAccion(Object r_fila)
	{
		this.opcCompletar.setVisible(true);

    	if (r_fila!=null)
    	{
    		this.opcCompletar.setEnabled(true);
	    	if (((MapeoRetrabajos) r_fila).getEstado().equals("A") || ((MapeoRetrabajos) r_fila).getEstado().equals("P"))
	    	{
	    		this.opcCompletar.setCaption("Iniciar");    		
	    	}
	    	else if (((MapeoRetrabajos) r_fila).getEstado().equals("I"))
	    	{
	    		this.opcCompletar.setCaption("Completar");    		
	    	}
	    	else if (((MapeoRetrabajos) r_fila).getEstado().equals("C"))
	    	{
	    		if (this.permisos==99)
	    		{
	    			this.opcCompletar.setCaption("Reabrir");
	    		}
	    		else
	    		{
	    			this.opcCompletar.setCaption("");
	    			this.opcCompletar.setEnabled(false);
	    		}
	    	}
    	}
	}

	public void imprimirRetrabajos()
	{
//		((RetrabajosGrid) grid).activadaVentanaPeticion=false;
//		((RetrabajosGrid) grid).ordenando = false;
//		
//		
//		MapeoRetrabajos mapeo=(MapeoRetrabajos) grid.getSelectedRow();
//		
//		if (form!=null) form.cerrar();
//		
//		if (mapeo!=null && mapeo.getIdRetrabajo()!=null)
//		{
//			PeticionFormatoImpresion vtPeticion = new PeticionFormatoImpresion(mapeo.getIdqc_incidencias());
//			getUI().addWindow(vtPeticion);
//		}
//		else
//		{
//			Notificaciones.getInstance().mensajeInformativo("Debes seleccionar el registro a imprimir");
//			this.opcImprimir.setEnabled(true);
//		}
	}
	
    public void print() {
    	
    }
	
	private void establecerTableExportable()
	{
		
//		Table table = new Table();
//		table.setContainerDataSource(this.grid.getContainerDataSource());		
//		Object[] columns = new Object[]{"dia", "tipo", "cantidad", "vino", "lote", "orden", "articulo" , "descripcion" , "cajas", "factor" , "unidades", "observaciones", "frontales", "contras", "capsulas", "tapones","caja"};
//		table.setVisibleColumns(columns);
//		
//		this.opcExcel.setTableToBeExported(table);
		
		
	}
	
	private void cargarCombo()
	{
		this.cmbEstado.removeAllItems();
		this.cmbEstado.addItem("Pendientes");
		this.cmbEstado.addItem("Curso");
		this.cmbEstado.addItem("Terminados");
		this.cmbEstado.addItem("Todas");
		
		
		if (eBorsao.get().accessControl.getNombre().contains("botell"))
		{
			this.cmbEstado.setValue("Curso");
			this.opcCompletar.setCaption("Completar");
		}
		else 
		{
			this.cmbEstado.setValue("Pendientes");
			this.opcCompletar.setCaption("Iniciar");
		}
		
	}
	
	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}

	public void actualizarDatos()
	{
		if (isHayGrid())
		{			
			filtrosRec = ((RetrabajosGrid) grid).recogerFiltros();
			grid.removeAllColumns();			
			barAndGridLayout.removeComponent(grid);
			grid=null;			
			generarGrid(opcionesEscogidas);
		}

	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}

