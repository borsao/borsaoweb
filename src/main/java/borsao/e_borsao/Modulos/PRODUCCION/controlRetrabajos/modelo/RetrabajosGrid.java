package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.renderers.DateRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.renderers.NumberRenderer;
import com.vaadin.ui.renderers.Renderer;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.modelo.MapeoImpresorasUsuarios;
import borsao.e_borsao.Modulos.GENERALES.ImpresorasUsuarios.server.consultaImpresorasUsuariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ENVASADORA.Programacion.view.pantallaLineasProgramacionesEnvasadora;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.PeticionAsignacionProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.view.pantallaLineasProgramaciones;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.view.pantallaAyudaProduccion;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.modelo.MapeoControlOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.view.pantallaOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view.RetrabajosView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class RetrabajosGrid extends GridPropio {
	
	public boolean actualizar = false;
	public boolean activadaVentanaPeticion = false;
	public boolean ordenando = false;
	private boolean conTotales = false;
	
//	private PeticionEtiquetasEmbotellado vtPeticion = null;
	private boolean editable = false;
	private boolean conFiltro = true;
	
	public Integer permisos = null;
//	private Integer indice = null;	
	private MapeoRetrabajos mapeo=null;
	private MapeoRetrabajos mapeoOrig=null;
	private RetrabajosView app = null;
	
    public RetrabajosGrid(RetrabajosView r_app , Integer r_permisos, ArrayList<MapeoRetrabajos> r_vector) 
    {
        this.permisos=r_permisos;
        this.vector=r_vector;
        this.app=r_app;
        this.setSizeFull();
		this.asignarTitulo("CONTROL RETRABAJOS");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		actualizar = false;
		this.crearGrid(MapeoRetrabajos.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(1);
		this.setConTotales(this.conTotales);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);

		if (this.permisos<40)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.NONE);
		}
		else if (this.permisos<50)
		{
			this.setEditorEnabled(false);
			this.setSeleccion(SelectionMode.SINGLE);
		}
		else 
		{
			if (this.permisos!=99) setEditorEnabled(true);
			this.setSeleccion(SelectionMode.SINGLE);
		}
		
		if (this.app.conTotales) this.calcularTotal();
    }
    

    public void doEditItem() 
    {
	  
    	/*
    	 * controlar en funcion de los permisos las celdas editables
    	 */
    	
    	this.mapeoOrig = new MapeoRetrabajos();
    	this.mapeo=((MapeoRetrabajos) getEditedItemId());
    	
    	if (this.permisos<99) 
    	{
    		this.mapeoOrig.setArticulo(this.mapeo.getArticulo());
    		this.mapeoOrig.setCantidad(this.mapeo.getCantidad());
    		this.mapeoOrig.setUnidades(this.mapeo.getUnidades());
    		this.mapeoOrig.setFecha(this.mapeo.getFecha());
    		this.mapeoOrig.setIdRetrabajo(this.mapeo.getIdRetrabajo());
    		this.mapeoOrig.setDescripcion(this.mapeo.getDescripcion());
    		this.mapeoOrig.setFactor(this.mapeo.getFactor());
    		this.mapeoOrig.setObservaciones(this.mapeo.getObservaciones());
    		this.mapeoOrig.setOrigen(this.mapeo.getOrigen());
    		this.mapeoOrig.setCumplimentacion(this.mapeo.getCumplimentacion());
    	}
    	if (this.permisos<70) 
    	{
    		this.mapeoOrig.setDescripcion(this.mapeo.getDescripcion());
    		this.mapeoOrig.setCantidad(this.mapeo.getCantidad());
    		this.mapeoOrig.setFactor(this.mapeo.getFactor());
    		this.mapeoOrig.setUnidades(this.mapeo.getUnidades());
    		this.mapeoOrig.setObservaciones(this.mapeo.getObservaciones());
    	}
    	
    	super.doEditItem();
    }
  
  @Override
  	public void doCancelEditor()
  	{
	  super.doCancelEditor();
  	}
	
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("estado", "finalidad", "instr", "bar", "biblia", "idPrg", "esc", "fecha","fechaCarga" , "idRetrabajo", "origen", "articulo", "descripcion",  "cantidad", "factor", "unidades", "observaciones", "referencia", "cliente","nombre","idProgramacion","idProgramacionEnvasadora", "idNoConformidad");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.widthFiltros.put("fecha", "85");
    	this.widthFiltros.put("fechaCarga", "85");
    	this.widthFiltros.put("articulo", "85");
    	this.widthFiltros.put("cliente", "85");
    	this.widthFiltros.put("descripcion", "225");
    	this.widthFiltros.put("nombre", "225");
    	this.widthFiltros.put("numero", "85");
    	this.widthFiltros.put("observaciones", "400");
    	this.widthFiltros.put("origen", "85");
    	this.widthFiltros.put("idRetrabajo", "100");
    	this.widthFiltros.put("referencia", "100");

    	this.getColumn("bar").setHeaderCaption("");
    	this.getColumn("bar").setSortable(false);
    	this.getColumn("bar").setWidth(new Double(40));

    	this.getColumn("instr").setHeaderCaption("");
    	this.getColumn("instr").setSortable(false);
    	this.getColumn("instr").setWidth(new Double(40));

    	this.getColumn("idPrg").setHeaderCaption("");
    	this.getColumn("idPrg").setSortable(false);
    	this.getColumn("idPrg").setWidth(new Double(40));
    	this.getColumn("esc").setHeaderCaption("");
    	this.getColumn("esc").setSortable(false);
    	this.getColumn("esc").setWidth(new Double(40));
    	this.getColumn("biblia").setHeaderCaption("");
    	this.getColumn("biblia").setSortable(false);
    	this.getColumn("biblia").setWidth(new Double(40));

    	this.getColumn("fecha").setHeaderCaption("FECHA");
    	this.getColumn("fecha").setWidthUndefined();
    	this.getColumn("fecha").setWidth(120);
    	this.getColumn("fechaCarga").setHeaderCaption("CARGA");
    	this.getColumn("fechaCarga").setWidthUndefined();
    	this.getColumn("fechaCarga").setWidth(120);
    	this.getColumn("idRetrabajo").setHeaderCaption("NUMERO");
    	this.getColumn("idRetrabajo").setWidth(120);
    	this.getColumn("articulo").setHeaderCaption("CODIGO");
    	this.getColumn("articulo").setWidth(120);
    	this.getColumn("cantidad").setHeaderCaption("CAJAS");
    	this.getColumn("cantidad").setWidth(100);
    	this.getColumn("factor").setHeaderCaption("BT/CJ");
    	this.getColumn("factor").setWidth(85);
    	this.getColumn("unidades").setHeaderCaption("CANT.");
    	this.getColumn("unidades").setWidth(125);

    	this.getColumn("descripcion").setHeaderCaption("DESCR.");
    	this.getColumn("descripcion").setWidth(300);
    	this.getColumn("origen").setHeaderCaption("ORIGEN");
    	this.getColumn("origen").setWidth(120);
    	this.getColumn("observaciones").setHeaderCaption("OBSERVACIONES");
    	this.getColumn("observaciones").setWidth(500);
    	this.getColumn("cliente").setHeaderCaption("CLIENTE");
    	this.getColumn("cliente").setWidth(120);
    	this.getColumn("nombre").setHeaderCaption("NOMBRE");
    	this.getColumn("nombre").setWidth(300);
    	this.getColumn("referencia").setHeaderCaption("REFERENCIA");
    	this.getColumn("referencia").setWidth(120);
    	this.getColumn("estado").setHidden(true);
    	this.getColumn("linea").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("cumplimentacion").setHidden(true);
    	this.getColumn("finalidad").setHidden(true);
    	this.getColumn("idProgramacion").setHidden(true);
    	this.getColumn("idProgramacionEnvasadora").setHidden(true);
    	this.getColumn("idNoConformidad").setHidden(true);
//    	this.getColumn("instr").setHidden(true);
    	
    	this.getColumn("fecha").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("fechaCarga").setRenderer((Renderer) new DateRenderer("%1$td/%1$tm/%1$tY"));
    	this.getColumn("idRetrabajo").setRenderer(new NumberRenderer());
//    	this.getColumn("cliente").setRenderer(new NumberRenderer());
    }
    
    private void asignarTooltips()
    {
    	this.getColumn("observaciones").setRenderer(new HtmlRenderer(), commentsConverter);
    	setCellDescriptionGenerator(cell -> getCellDescription(cell));
    }
    
    private String getCellDescription(CellReference cell) {
        String descriptionText = null;
        Item item = cell.getItem();
        if (item instanceof BeanItem<?>) {
            Object bean = ((BeanItem<?>)item).getBean();
            if (bean instanceof MapeoRetrabajos) {
                MapeoRetrabajos progRow = (MapeoRetrabajos)bean;
                // The actual description text is depending on the application
                if ("idRetrabajo".equals(cell.getPropertyId()))
                {
                	switch (progRow.getEstado())
                	{
                		case "C":
                		{
                			descriptionText = "Cerrada";
                			break;
                		}
                		case "P":
                		{
                			descriptionText = "Programada";
                			break;
                		}
                		case "A":
                		{
                			descriptionText = "Pendiente";
                			break;
                		}
                	}
                	
                }
                else if ("bar".equals(cell.getPropertyId()))
                {
                	descriptionText = "Impreso Horas";
                }
                else if ("instr".equals(cell.getPropertyId()))
                {
                	descriptionText = "Instrucciones";
                }
                else if ("esc".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ayuda produccion";
                }
                else if ("idPrg".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Programación";
                }
                else if ("biblia".equals(cell.getPropertyId()))
                {
                	descriptionText = "Acceso a Ficha de Producto";
                }

            }
        }
        return descriptionText;
    }
    /*
     * METODOS PUBLICOS
     */
    
    public void asignarEstilos()
    {
    	this.asignarTooltips();
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            String strEstado = "";
            String strCarga = "";
            		
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("estado".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strEstado=cellReference.getValue().toString();
            	}
            	if ("finalidad".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getValue()!=null) strCarga=cellReference.getValue().toString();
            	}

            	if ("idRetrabajo".equals(cellReference.getPropertyId())) 
            	{
            		switch (strEstado.toUpperCase().trim())
            		{
	            		case "C":
	        			{
	        				return "Rcell-normal";
	        			}
	        			case "A":
	        			{
	        				return "Rcell-error";
	        			}
	        			case "P":
	        			{
	        				return "Rcell-warning";
	        			}
	        			case "I":
	        			{
	        				return "Rcell-warning";
	        			}
	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	if ("fechaCarga".equals(cellReference.getPropertyId())) 
            	{
            		switch (strCarga.toUpperCase().trim())
            		{
	            		case "OK":
	        			{
	        				return "Rcell-green";
	        			}
	        			default:
	        			{
	        				return "Rcell-error";
	        			}
            		}
            	}
            	if ( "idPrg".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonProgramacion";
            	}
            	else if ( "esc".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEsc";
            	}
            	else if ( "bar".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonEan";
            	}
            	else if ( "instr".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonImagen";
            	}
            	else if ( "biblia".equals(cellReference.getPropertyId()))
            	{
            		return "cell-nativebuttonBiblia";
            	}
            	else if ( "cliente".equals(cellReference.getPropertyId()) || "cantidad".equals(cellReference.getPropertyId()) || "factor".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId()) )
            	{
            		return "Rcell-normal";
            	}
            	else
        		{
            		return "cell-normal";
            	}
            }
        });
    	
    }
    
    public void cargarListeners()
	{
    	this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	MapeoRetrabajos mapeo = (MapeoRetrabajos) event.getItemId();
            	
            	if (event.getPropertyId().toString().equals("idPrg"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		if (mapeo.getIdProgramacion()!=null && mapeo.getIdProgramacion()!=0)
            		{
            			pantallaLineasProgramaciones vt = null;
        	    		vt= new pantallaLineasProgramaciones(mapeo.getIdProgramacion(), "Programación Origen Retrabajo");
        	    		getUI().addWindow(vt);
            		}
            		else if (mapeo.getIdProgramacionEnvasadora()!=null && mapeo.getIdProgramacionEnvasadora()!=0)
            		{
            			pantallaLineasProgramacionesEnvasadora vt = null;
        	    		vt= new pantallaLineasProgramacionesEnvasadora(mapeo.getIdProgramacionEnvasadora(),"Programación Origen Retrabajo");
        	    		getUI().addWindow(vt);
            		}
            		else
            		{
            			PeticionAsignacionProgramacion vt = null;
        	    		vt= new PeticionAsignacionProgramacion(app, mapeo, "Programación Origen Retrabajo de " + mapeo.getArticulo().trim() + " " + mapeo.getDescripcion().trim());
        	    		getUI().addWindow(vt);
            		}
            	}
            	else if (event.getPropertyId().toString().equals("esc"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		pantallaAyudaProduccion vt = null;
            		
            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
            		{
            			vt = new pantallaAyudaProduccion("Situación del Articulo " + mapeo.getArticulo() + " " + mapeo.getDescripcion() + " para: " + mapeo.getUnidades().toString(), mapeo.getUnidades(), mapeo.getArticulo());
            			getUI().addWindow(vt);	            			
            		}	            		
            	}
            	else if (event.getPropertyId().toString().equals("bar"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
            		{
	        	    	if (permisos>=30)
	        	    	{
	        	    		
	        	    		/*
	        	    		 * Abriremos ventana peticion parametros
	        	    		 * rellenaremos la variables publicas
	        	    		 * 		cajas y anada
	        	    		 * 		llamaremos al metodo generacionPdf
	        	    		 */
	        	    		MapeoControlOperarios mapeoOperarios = new MapeoControlOperarios();
	        	    		MapeoRetrabajos mapeoPdf = (MapeoRetrabajos) event.getItemId();
//	        	    		generacionPdf(mapeoPdf, true, "PANTALLA");
	        	    		
	        	    		if (mapeoPdf.getIdProgramacion()!=null)
	        	    		{
		        	    		mapeoOperarios.setIdProgramacion(mapeoPdf.getIdProgramacion());
		        	    		mapeoOperarios.setLineaPedido(mapeoPdf.getLinea());
		        	    		mapeoOperarios.setCodigoPedido(mapeoPdf.getIdRetrabajo());
		        	    		
		        	    		pantallaOperarios vt = new pantallaOperarios(app, "Operarios Retrabajo", mapeoOperarios, mapeoPdf.getArticulo(), mapeoPdf.getDescripcion());
		        	    		getUI().addWindow(vt);
	        	    		}
	        	    		else
	        	    		{
	        	    			Notificaciones.getInstance().mensajeError("Sólo podemos asignar tiempos y operarios a retrabajos programados.");
	        	    		}
	        	    	}	    			
						else
						{
							Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
						}
            		}	            		
            	}
            	else if (event.getPropertyId().toString().equals("instr"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		
            		if ((mapeo.getCliente()!=null && mapeo.getCliente().length()>0) && (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0))
            		{
            			if (permisos>=30)
            			{
            				
            				/*
            				 * Abriremos ventana peticion parametros
            				 * rellenaremos la variables publicas
            				 * 		cajas y anada
            				 * 		llamaremos al metodo generacionPdf
            				 */
            				MapeoRetrabajos mapeoPdf = (MapeoRetrabajos) event.getItemId();
            				
        					String [] archivos = RutinasFicheros.recuperarNombresArchivosCarpeta(LecturaProperties.basePdfPath + "/calidad/retrabajos/" + mapeo.getCliente(), mapeo.getArticulo(), "JPG");
        					if (archivos.length>0)
        						RutinasFicheros.abrirImagenGenerado(getUI(), archivos,LecturaProperties.basePdfPath + "/calidad/retrabajos/" + mapeo.getCliente());
        					else
        						Notificaciones.getInstance().mensajeError("No hay imagenes a mostrar sobre este retrabajo.");
        						
            			}	    			
            			else
            			{
            				Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
            			}
            		}	            		
            	}
            	else if (event.getPropertyId().toString().equals("biblia"))
            	{
            		activadaVentanaPeticion=true;
            		ordenando = false;
            		String host = UI.getCurrent().getPage().getLocation().getHost().trim();
            		
            		if (host.equals("localhost")) host = "192.168.6.6";
            		
            		String url = "http://" + host + ":9090/Borsao-Client/Producto/" + mapeo.getArticulo().trim();
            		getUI().getPage().open(url, "_blank");
            		
            	}
            	else
            	{
            		activadaVentanaPeticion=false;
            		ordenando = false;
//            		app.verForm(false);
//            		MapeoRetrabajos mapeo = (MapeoRetrabajos) event.getItemId();
            		
//	            	if (event.getPropertyId().toString().equals("papel"))
//	            	{
//	            		activadaVentanaPeticion=true;
//	            		ordenando = false;
//	            		if (mapeo.getArticulo()!=null && mapeo.getArticulo().length()>0)
//	            		{
//		        	    	if (permisos>=30)
//		        	    	{
//		        	    		
//		        	    		/*
//		        	    		 * Abriremos ventana peticion parametros
//		        	    		 * rellenaremos la variables publicas
//		        	    		 * 		cajas y anada
//		        	    		 * 		llamaremos al metodo generacionPdf
//		        	    		 */
//		        	    		vtPeticion = new PeticionEtiquetasEmbotellado((ProgramacionGrid) event.getSource(), mapeo, "Parámetros Impresión Etiquetas Embotellado");
//		        	    		getUI().addWindow(vtPeticion);
//		        	    	}	    			
//							else
//							{
//								Notificaciones.getInstance().mensajeError("No tienes permiso para ejecutar esta opción.");
//							}
//	            		}
//	            	}
//	            	}            	
//	            	else
//	            	{
//	            		activadaVentanaPeticion=false;
//	            		ordenando = false;
//	            	}
	            }
    		}
        });
	}

	@Override
	public void establecerColumnasNoFiltro() 
	{
		this.camposNoFiltrar.add("idPrg");
		this.camposNoFiltrar.add("esc");
		this.camposNoFiltrar.add("biblia");
		this.camposNoFiltrar.add("estado");
		this.camposNoFiltrar.add("cantidad");
		this.camposNoFiltrar.add("unidades");
		this.camposNoFiltrar.add("factor");
		this.camposNoFiltrar.add("bar");
		this.camposNoFiltrar.add("instr");
	}

	public void generacionPdf(MapeoRetrabajos r_mapeo, boolean r_eliminar, String r_impresora) 
    {
    	String pdfGenerado = null;
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */

    	/*
    	 * 
    	 */
    	String defecto=null;
		consultaImpresorasUsuariosServer cius = new consultaImpresorasUsuariosServer(CurrentUser.get());
		ArrayList<MapeoImpresorasUsuarios> vector = cius.datosOpcionesGlobalUsuario(CurrentUser.get());
		
		for (int i=0;i<vector.size();i++)
		{
			if (((MapeoImpresorasUsuarios) vector.get(i)).getDefecto().equals("S")) defecto=((MapeoImpresorasUsuarios) vector.get(i)).getImpresora();
		}
		
		if (defecto.length()==0) defecto="PANTALLA";
		
		
    	consultaRetrabajosServer cps = new consultaRetrabajosServer(CurrentUser.get());
    	pdfGenerado = cps.generarInforme(r_mapeo);
    	if(pdfGenerado.length()>0)
    	{
			try
            { 
				if (r_impresora.toUpperCase().equals("PANTALLA"))
				{
					RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, r_eliminar);
				}
				else
				{
					String archivo = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF" + pdfGenerado;
					
					RutinasFicheros.imprimir(archivo, r_impresora);
				    if (r_eliminar)  RutinasFicheros.eliminarFichero(LecturaProperties.basePdfPath + "/" + pdfGenerado);
				}
				
            }
			catch(Exception e)
			{ 
                 Notificaciones.getInstance().mensajeSeguimiento(e.getMessage()); 
			}
    	}
    	else
    		Notificaciones.getInstance().mensajeError("Error en la generacion");
    }
	
	public String guardarCambios(MapeoRetrabajos r_mapeo, MapeoRetrabajos r_mapeoPrevio)
	{
		String rdo =null;
		consultaRetrabajosServer cus = new consultaRetrabajosServer(CurrentUser.get());
		if (r_mapeo!=null)
		{
			rdo = cus.guardarCambios(r_mapeo);
			if (rdo== null)
			{
				((ArrayList<MapeoRetrabajos>) vector).add(r_mapeo);
				
				if (r_mapeoPrevio!=null)
				{
					rdo = cus.guardarCambios(r_mapeoPrevio);
					if (rdo==null)
					{
						((ArrayList<MapeoRetrabajos>) vector).add(r_mapeoPrevio);
	
					}
				}
			}
			else
			{
				Notificaciones.getInstance().mensajeError(rdo);
			}
			
			if (!this.vector.isEmpty())
			{
				
				Indexed indexed = this.getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	            this.removeAllColumns();
	            
	            this.generarGrid();
    
				this.sort("idRetrabajo");
				this.select(r_mapeo);
				this.scrollTo(r_mapeo);
			}
		}		
		return rdo;
	}
	
	Converter<String, String> commentsConverter = new Converter<String,  String>(){
        @Override
        public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {

            return value;
        }

        @Override
        public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
                throws com.vaadin.data.util.converter.Converter.ConversionException {
            if(value !=null){
                return "<p class=\"wrap\">"+value+"</p>";
            }else{
                return "";
            }
        }

        @Override
        public Class<String> getModelType() {
            return String.class;
        }

        @Override
        public Class<String> getPresentationType() {
            return String.class;
        }

    };

	public void calcularTotal() {
    	Long totalU = new Long(0) ;
    	Long totalC = new Long(0) ;
    	String articulo = null;
    	Integer codigo = null;
    	String articulo_old = "";
    	Integer codigo_old = 0;
    	Integer valor = null;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	
        	articulo = (String) item.getItemProperty("articulo").getValue();
        	codigo = (Integer) item.getItemProperty("idRetrabajo").getValue();
        	
        	if ((!articulo_old.equals(articulo)) || (!codigo_old.equals(codigo)))
        	{
        		valor = (Integer) item.getItemProperty("unidades").getValue();
				totalU += valor.longValue();
				valor = (Integer) item.getItemProperty("cantidad").getValue();
				totalC += valor.longValue();
				
        		articulo_old=articulo;
        		codigo_old = codigo;
        	}
        }
        
        footer.getCell("articulo").setText("Totales ");
		footer.getCell("unidades").setText(RutinasNumericas.formatearDouble(totalU.toString()));
		footer.getCell("cantidad").setText(RutinasNumericas.formatearDouble(totalC.toString()));
		this.app.barAndGridLayout.setWidth("100%");
		this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-10)+"%");
//		footer.setStyleName("smallgrid");
		footer.getCell("unidades").setStyleName("Rcell-pie");
		footer.getCell("cantidad").setStyleName("Rcell-pie");
		

	}
}


