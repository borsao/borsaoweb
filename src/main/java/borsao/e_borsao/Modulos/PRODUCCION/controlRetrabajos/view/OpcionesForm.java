package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.vaadin.data.Container.Indexed;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;

import borsao.e_borsao.ClasesPropias.MapeoAyudas;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.ventanaAyuda;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.MapeoRetrabajos;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.RetrabajosGrid;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.server.consultaRetrabajosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoRetrabajos mapeo = null;
	 
	 private ventanaAyuda vHelp  =null;
	 private consultaRetrabajosServer cps = null;
	 private RetrabajosView uw = null;
	 public MapeoRetrabajos mapeoModificado = null;
	 private BeanFieldGroup<MapeoRetrabajos> fieldGroup;
	 
	 public OpcionesForm(RetrabajosView r_uw) {
        super();
        uw=r_uw;
        
        addStyleName("mytheme product-form");
        fieldGroup = new BeanFieldGroup<MapeoRetrabajos>(MapeoRetrabajos.class);
        fieldGroup.bindMemberFields(this);
        this.cargarCombo();
        this.establecerCamposObligatorios();
        this.cargarValidators();
        this.cargarListeners();
        
        this.fecha.addStyleName("v-datefield-textfield");
        this.fecha.setDateFormat("dd/MM/yyyy");
        this.fecha.setShowISOWeekNumbers(true);
        this.fecha.setResolution(Resolution.DAY);
        
        this.fechaCarga.addStyleName("v-datefield-textfield");
        this.fechaCarga.setDateFormat("dd/MM/yyyy");
        this.fechaCarga.setShowISOWeekNumbers(true);
        this.fechaCarga.setResolution(Resolution.DAY);

    }   

	private void cargarValidators()
	{
	}
	
	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
            	mapeo  = (MapeoRetrabajos) uw.grid.getSelectedRow();
                eliminarRetrabajo(mapeo);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });  
        
        this.btnArticulo.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes("Articulo");
            }
        });
		
        this.btnCliente.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	ventanaMargenes("Cliente");
            }
        });
		
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
//	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				uw.opcionesEscogidas  = rellenarHashOpcionesBusqueda();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			
		           	removeStyleName("visible");
		           	uw.regresarDesdeForm();
//		           	activarDesactivarControles(true);

	 			}
	 			else if (isCreacion())
	 			{
	 				mapeoModificado=null;
	 				uw.modificando=false;
	 				if (comprobarCamposObligatorios())
	 				{
		 				hashToMapeo hm = new hashToMapeo();
		 				// TODO pasar esta modificacion al resto de opcionesForm. Al crear y modificar no hay que usar el hash de la view
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo = hm.convertirHashAMapeo(opcionesEscogidas);	 				 
		                crearRetrabajo(mapeo);
			           	removeStyleName("visible");
			           	uw.newForm();
	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}
	 			}
	 			else
	 			{
	 				
	 				if (comprobarCamposObligatorios())
	 				{
		 				MapeoRetrabajos mapeo_orig= (MapeoRetrabajos) uw.grid.getSelectedRow();
		 				hashToMapeo hm = new hashToMapeo(); 
		 				HashMap<String , String> opcionesEscogidas  = rellenarHashOpciones();
		 				mapeo= hm.convertirHashAMapeo(opcionesEscogidas);
		 				mapeo.setFecha(mapeo_orig.getFecha());
		 				mapeo.setFechaCarga(mapeo_orig.getFechaCarga());
		 				mapeo.setIdRetrabajo(mapeo_orig.getIdRetrabajo());
		 				
		 				modificarRetrabajo(mapeo,mapeo_orig);
		 				hm=null;
		 				
			           	removeStyleName("visible");

	 				}
	 				else
	 				{
	 					Notificaciones.getInstance().mensajeError("Debes rellenar todos los campos obligatorios");
	 				}

	 			}
	 			
	 			if (((RetrabajosGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}
	 			
 			}
 		});	
        
		this.btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 	cerrar();
            }
        });
		
		this.articulo.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.getValue()!=null && articulo.getValue().length()>0)
				{
					cps  = new consultaRetrabajosServer(CurrentUser.get());
					String desc = cps.obtenerDescripcionArticulo(articulo.getValue());
					descripcion.setValue(desc);
				}
//				else if (mapeoModificado!=null)
//				{
//					descripcion.setValue(mapeoModificado.getDescripcion());
//					factor.setValue(mapeoModificado.getFactor().toString());					
//				}
			}
		});
		
		this.cliente.addValueChangeListener( new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cliente.getValue()!=null && cliente.getValue().length()>0)
				{
					cps  = new consultaRetrabajosServer(CurrentUser.get());
					String desc = cps.obtenerNombreCliente(new Integer(cliente.getValue()));
					nombre.setValue(desc);
				}
//				else if (mapeoModificado!=null)
//				{
//					descripcion.setValue(mapeoModificado.getDescripcion());
//					factor.setValue(mapeoModificado.getFactor().toString());					
//				}
			}
		});
		
	}
    
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		this.creacion = creacion;
		if (creacion)
		{
			this.setBusqueda(false);
			editarRetrabajo(null);
		}
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.factor.getValue()!=null && this.factor.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("factor", this.factor.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("factor", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", RutinasFechas.convertirDateToString(this.fecha.getValue()));
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.cliente.getValue()!=null && this.cliente.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cliente", this.cliente.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cliente", "");
		}
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cantidad", this.cantidad.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cantidad", "");
		}
		if (this.unidades.getValue()!=null && this.unidades.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("unidades", this.unidades.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("unidades", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		if (this.nombre.getValue()!=null && this.nombre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.observaciones.getValue()!=null && this.observaciones.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("observaciones", this.observaciones.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("observaciones", "");
		}
		return opcionesEscogidas;
		
	}

	private HashMap<String , String> rellenarHashOpcionesBusqueda()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		if (this.cliente.getValue()!=null && this.cliente.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("cliente", this.cliente.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cliente", "");
		}
		if (this.nombre.getValue()!=null && this.nombre.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.origen.getValue()!=null && this.origen.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("origen", this.origen.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("origen", "");
		}
		if (this.articulo.getValue()!=null && this.articulo.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("articulo", this.articulo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("articulo", "");
		}
		if (this.fecha.getValue()!=null && this.fecha.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("fecha", this.fecha.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("fecha", "");
		}
		if (this.descripcion.getValue()!=null && this.descripcion.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("descripcion", this.descripcion.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("descripcion", "");
		}
		return opcionesEscogidas;
		
	}

	public void setBusqueda(boolean r_buscar)
	{	
		this.busqueda = r_buscar;
		
		if (r_buscar) 
		{
			this.setCreacion(false);
			editarRetrabajo(null);
		}
	}
	
	public void editarRetrabajo(MapeoRetrabajos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) 
		{
			r_mapeo=new MapeoRetrabajos();
			
			if (!isBusqueda())
			{
				cps = consultaRetrabajosServer.getInstance(CurrentUser.get());
//				Integer numerador = cps.obtenerSiguiente();
//				this.numerador.setValue(numerador.toString());
				this.fecha.setValue(new Date());
//				r_mapeo.setNumerador(numerador);
				r_mapeo.setFecha(this.fecha.getValue());
				establecerCamposObligatoriosCreacion(true);
			}
		}
		else
		{
		}
		fieldGroup.setItemDataSource(new BeanItem<MapeoRetrabajos>(r_mapeo));
		this.uw.modificando=false;
	}
	
	public void eliminarRetrabajo(MapeoRetrabajos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		((RetrabajosGrid) uw.grid).remove(r_mapeo);
		uw.cus.eliminar(r_mapeo);
		((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector).remove(r_mapeo);
		
		if (((RetrabajosGrid) uw.grid).vector.isEmpty())
		{
			uw.reestablecerPantalla();
		}
		else
		{
			HashMap<String,String> filtrosRec = ((RetrabajosGrid) uw.grid).recogerFiltros();
			ArrayList<MapeoRetrabajos> r_vector = (ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector;
			
			Indexed indexed = ((RetrabajosGrid) uw.grid).getContainerDataSource();
            List<?> list = new ArrayList<Object>(indexed.getItemIds());
            for(Object itemId : list)
            {
                indexed.removeItem(itemId);
            }
            
            uw.grid.removeAllColumns();			
            uw.barAndGridLayout.removeComponent(uw.grid);
            uw.grid=null;			

            uw.presentarGrid(r_vector);

			((RetrabajosGrid) uw.grid).sort("idRetrabajo");
			((RetrabajosGrid) uw.grid).establecerFiltros(filtrosRec);
		}
	}
	
	public void modificarRetrabajo(MapeoRetrabajos r_mapeo, MapeoRetrabajos r_mapeo_orig)
	{
//		uw.cus  = new consultaProgramacionServer(CurrentUser.get());
		r_mapeo.setIdRetrabajo(r_mapeo_orig.getIdRetrabajo());
		r_mapeo.setEstado(r_mapeo_orig.getEstado());
		String rdo = uw.cus.guardarCambios(r_mapeo);		
		if (rdo.length()==1)
		{
			r_mapeo.setEstado(rdo);
		
			if (!((RetrabajosGrid) uw.grid).vector.isEmpty())
			{
				
				((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector).remove(r_mapeo_orig);
				((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector).add(r_mapeo);
				ArrayList<MapeoRetrabajos> r_vector = (ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector;
				
				
				HashMap<String,String> filtrosRec = ((RetrabajosGrid) uw.grid).recogerFiltros();

				Indexed indexed = ((RetrabajosGrid) uw.grid).getContainerDataSource();
	            List<?> list = new ArrayList<Object>(indexed.getItemIds());
	            for(Object itemId : list)
	            {
	                indexed.removeItem(itemId);
	            }
	//            indexed.addItemAt(list.size()+1, r_mapeo);
	            
	            uw.grid.removeAllColumns();			
	            uw.barAndGridLayout.removeComponent(uw.grid);
	            uw.grid=null;			
	//            uw.generarGrid(uw.opcionesEscogidas);
	            uw.presentarGrid(r_vector);
	            
				((RetrabajosGrid) uw.grid).setRecords((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector);
	////			((ProgramacionGrid) uw.grid).refresh(r_mapeo,null);
				((RetrabajosGrid) uw.grid).sort("idRetrabajo");
				((RetrabajosGrid) uw.grid).scrollTo(r_mapeo);
				((RetrabajosGrid) uw.grid).select(r_mapeo);
				
				((RetrabajosGrid) uw.grid).establecerFiltros(filtrosRec);
				
			}
		}
		else
		{
			Notificaciones.getInstance().mensajeAdvertencia(rdo);
		}
		mapeoModificado=null;
		uw.modificando=false;

	}
	
	public void crearRetrabajo(MapeoRetrabajos r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		r_mapeo.setIdRetrabajo(uw.cus.obtenerSiguiente());
		r_mapeo.setFecha(new Date());
		String rdo = uw.cus.guardarNuevo(r_mapeo);
//		r_mapeo.setStatus(uw.cus.obtenerStatus(r_mapeo.getIdqc_incidencias()));
		if (rdo.length()==1) r_mapeo.setEstado(rdo);
		
		if (rdo.length()==1)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoRetrabajos>(r_mapeo));
			if (((RetrabajosGrid) uw.grid)!=null )//&& )
			{
				if (!((RetrabajosGrid) uw.grid).vector.isEmpty())
				{
					HashMap<String,String> filtrosRec = ((RetrabajosGrid) uw.grid).recogerFiltros();
					
					Indexed indexed = ((RetrabajosGrid) uw.grid).getContainerDataSource();
	                List<?> list = new ArrayList<Object>(indexed.getItemIds());
	                for(Object itemId : list)
	                {
	                    indexed.removeItem(itemId);
	                }
	                
					((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector).add(r_mapeo);
					((RetrabajosGrid) uw.grid).setRecords((ArrayList<MapeoRetrabajos>) ((RetrabajosGrid) uw.grid).vector);
					((RetrabajosGrid) uw.grid).sort("idRetrabajo");
					((RetrabajosGrid) uw.grid).scrollTo(r_mapeo);
					((RetrabajosGrid) uw.grid).select(r_mapeo);
					
					((RetrabajosGrid) uw.grid).establecerFiltros(filtrosRec);
				}
				else
				{
					uw.actualizarGrid();
				}
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
		else
		{
			Notificaciones.getInstance().mensajeError(rdo);
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoRetrabajos> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeo = item.getBean();
            if (this.mapeo.getIdRetrabajo()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

	private void ventanaMargenes(String r_area)
	{
		ArrayList<MapeoAyudas> vectorAyuda = null;
		cps  = new consultaRetrabajosServer(CurrentUser.get());
		vectorAyuda=cps.vector(r_area);
		switch (r_area)
		{
			case "Articulo":
			{
				this.vHelp = new ventanaAyuda(this.articulo, this.descripcion, vectorAyuda, "Articulos");
				break;
			}
			case "Cliente":
			{
				this.vHelp = new ventanaAyuda(this.cliente, this.nombre, vectorAyuda, "Clientes");
				break;
			}
		}
		getUI().addWindow(this.vHelp);	
	}

	private void establecerCamposObligatorios()
	{
	}

	private void establecerCamposObligatoriosCreacion(boolean r_obliga) 
	{
//		this.articulo.setRequired(r_obliga);
//		this.numerador.setRequired(r_obliga);
//		this.origen.setRequired(r_obliga);
//		this.notificador.setRequired(r_obliga);
//		this.cantidad.setRequired(r_obliga);
//		this.fechaNotificacion.setRequired(r_obliga);
//		this.fechaNotificacion.setEnabled(r_obliga);
	}

	private boolean comprobarCamposObligatorios()
	{
		if (!isBusqueda())
		{
			if (this.origen.getValue()==null || this.origen.getValue().toString().length()==0) return false;
			if (this.articulo.getValue()==null || this.articulo.getValue().length()==0) return false;
			if (this.fecha.getValue()==null || this.fecha.getValue().toString().length()==0) return false;
			if (this.fechaCarga.getValue()==null || this.fechaCarga.getValue().toString().length()==0) return false;
			if (this.unidades.getValue()==null || this.unidades.getValue().length()==0) return false;
		}
		
		return true;
	}
	
	
	public void cerrar()
	{
		mapeoModificado=null;
		this.uw.modificando=false;
		removeStyleName("visible");
		btnGuardar.setCaption("Guardar");
		btnEliminar.setEnabled(true);
        setEnabled(false);        
        uw.regresarDesdeForm();
	}
	
	private void cargarCombo()
	{
		this.origen.addItem("PEDIDOS");
		this.origen.addItem("MANUALES");
		this.origen.addItem("CALIDAD");
	
		this.origen.setNewItemsAllowed(false);
	}
}
