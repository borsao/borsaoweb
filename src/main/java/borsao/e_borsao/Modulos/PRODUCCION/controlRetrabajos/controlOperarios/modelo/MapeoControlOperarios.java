package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.controlOperarios.modelo;


import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlOperarios extends MapeoGlobal
{
	private Integer tiempo;
	private Integer lineaPedido;
	private Integer codigoPedido;
	private Integer idProgramacion;
	
	private String operario;
	
	private String observaciones;
	private Date fecha;
	
	public MapeoControlOperarios()
	{
	}

	public Integer getTiempo() {
		return tiempo;
	}

	public Integer getLineaPedido() {
		return lineaPedido;
	}

	public Integer getCodigoPedido() {
		return codigoPedido;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public String getOperario() {
		return operario;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}

	public void setLineaPedido(Integer lineaPedido) {
		this.lineaPedido = lineaPedido;
	}

	public void setCodigoPedido(Integer codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public void setOperario(String operario) {
		this.operario = operario;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}