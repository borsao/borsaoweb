package borsao.e_borsao.Modulos.PRODUCCION.controlRetrabajos.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoRetrabajos extends MapeoGlobal
{
    private Integer idRetrabajo;
	private Integer unidades;
	private Integer cantidad;
	private Integer factor;
	private Integer idProgramacion;
	private Integer idNoConformidad;
	private Integer idProgramacionEnvasadora;
	private Integer linea;
	
	private String cliente;
	private String origen;
	private String cumplimentacion;
	private String articulo;
	private String descripcion;
	private String nombre;
	private String observaciones;
	private String finalidad;
	private String referencia;
	
	private String estado;
	private String esc;
	private String bar;
	private String biblia;
	private String instr;
	private String idPrg;

	private Date fecha;
	private Date fechaCarga;
	
	public MapeoRetrabajos()
	{
		this.setOrigen("");
		this.setCumplimentacion("");
		this.setObservaciones("");
		this.setDescripcion("");
		this.setArticulo("");
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha= fecha;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public Integer getIdRetrabajo() {
		return idRetrabajo;
	}

	public void setIdRetrabajo(Integer idRetrabajo) {
		this.idRetrabajo = idRetrabajo;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public Integer getFactor() {
		return factor;
	}

	public void setFactor(Integer factor) {
		this.factor = factor;
	}

	public String getCumplimentacion() {
		return cumplimentacion;
	}

	public void setCumplimentacion(String cumplimentacion) {
		this.cumplimentacion = cumplimentacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getLinea() {
		return linea;
	}

	public String getInstr() {
		return instr;
	}

	public void setInstr(String instr) {
		this.instr = instr;
	}

	public void setLinea(Integer linea) {
		this.linea = linea;
	}

	public String getCliente() {
		if (cliente!=null)  return cliente.toString(); else return null;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFinalidad() {
		return finalidad;
	}

	public void setFinalidad(String finalidad) {
		this.finalidad = finalidad;
	}

	public Date getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(Date fechaCarga) {
		this.fechaCarga = fechaCarga;
	}
	
	public String getEsc() {
		return esc;
	}
	public void setEsc(String esc) {
		this.esc = esc;
	}

	public String getBiblia() {
		return biblia;
	}
	public void setBiblia(String biblia) {
		this.biblia = biblia;
	}
	
	public String getIdPrg() {
		return idPrg;
	}

	public void setIdPrg(String idPrg) {
		this.idPrg = idPrg;
	}

	public Integer getIdProgramacionEnvasadora() {
		return idProgramacionEnvasadora;
	}

	public void setIdProgramacionEnvasadora(Integer idProgramacionEnvasadora) {
		this.idProgramacionEnvasadora = idProgramacionEnvasadora;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public Integer getIdNoConformidad() {
		return idNoConformidad;
	}

	public void setIdNoConformidad(Integer idNoConformidad) {
		this.idNoConformidad = idNoConformidad;
	}


}




/*
CREATE TABLE `prd_retrabajos` (
  `idRetrabajo` int(11) NOT NULL,
  `origen` varchar(45) NOT NULL,
  `articulo` varchar(45) NOT NULL DEFAULT '',
  `descripcion` varchar(45) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `factor` int(11) NOT NULL,
  `unidades` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `cumplimentacion` varchar(45) DEFAULT NULL,
  `observaciones` varchar(500) DEFAULT NULL,
  `idProgramacion` int(11) NOT NULL,
  PRIMARY KEY (`articulo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

*/
