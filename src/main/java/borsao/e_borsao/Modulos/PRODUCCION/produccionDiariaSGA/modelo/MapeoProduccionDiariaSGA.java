package borsao.e_borsao.Modulos.PRODUCCION.produccionDiariaSGA.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoProduccionDiariaSGA extends MapeoGlobal
{
	private String articulo;
	private Integer cantidad;
	private String anada;
	private String sscc;
	private String lote;
	private String operacion;
	
	private String fecha;
	private String hora;

	public MapeoProduccionDiariaSGA()
	{
		
	}

	public String getArticulo() {
		return articulo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public String getAnada() {
		return anada;
	}

	public String getSscc() {
		return sscc;
	}

	public String getLote() {
		return lote;
	}

	public String getOperacion() {
		return operacion;
	}

	public String getFecha() {
		return fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public void setAnada(String anada) {
		this.anada = anada;
	}

	public void setSscc(String sscc) {
		this.sscc = sscc;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

}