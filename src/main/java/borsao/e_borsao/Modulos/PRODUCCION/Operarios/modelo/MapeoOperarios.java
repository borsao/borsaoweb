package borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoOperarios  extends MapeoGlobal
{
    private Integer idOperario;
	private String nombre;
	private String cargo;
	private String area;
	private String permisos;

	public MapeoOperarios()
	{
		this.setNombre("");
		this.setCargo("");
		this.setArea("");
		this.setPermisos("");
	}

	public Integer getIdOperario() {
		return idOperario;
	}

	public void setIdOperario(Integer idOperario) {
		this.idOperario = idOperario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String r_nombre) {
		this.nombre = r_nombre;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String r_cargo) {
		this.cargo= r_cargo;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String r_area) {
		this.area = r_area;
	}

	public String getPermisos() {
		return permisos;
	}

	public void setPermisos(String r_permisos) {
		this.permisos = r_permisos;
	}
}