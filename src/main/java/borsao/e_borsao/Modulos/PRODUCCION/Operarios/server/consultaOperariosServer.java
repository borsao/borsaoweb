package borsao.e_borsao.Modulos.PRODUCCION.Operarios.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;

public class consultaOperariosServer
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaOperariosServer instance;
	
	public consultaOperariosServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaOperariosServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaOperariosServer(r_usuario);			
		}
		return instance;
	}
	
	
	public ArrayList<MapeoOperarios> datosOperariosGlobal(MapeoOperarios r_mapeo)
	{
		ResultSet rsOperarios = null;		
		ArrayList<MapeoOperarios> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_operarios.idprd_operarios ope_id, ");
        cadenaSQL.append(" prd_operarios.nombre ope_nom, ");
	    cadenaSQL.append(" prd_operarios.cargo ope_car, ");
	    cadenaSQL.append(" prd_operarios.area ope_are, ");
	    cadenaSQL.append(" prd_operarios.permisos ope_per ");
     	cadenaSQL.append(" FROM prd_operarios ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdOperario()!=null && r_mapeo.getIdOperario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_operarios = '" + r_mapeo.getIdOperario() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getCargo()!=null && r_mapeo.getCargo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cargo = '" + r_mapeo.getCargo() + "' ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" upper(area) = '" + r_mapeo.getArea().toUpperCase() + "' ");
				}
				if (r_mapeo.getPermisos()!=null && r_mapeo.getPermisos().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" permisos = '" + r_mapeo.getPermisos() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_operarios asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOperarios.TYPE_SCROLL_SENSITIVE,rsOperarios.CONCUR_UPDATABLE);
			rsOperarios= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOperarios>();
			
			while(rsOperarios.next())
			{
				MapeoOperarios mapeoOperarios = new MapeoOperarios();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOperarios.setIdOperario(rsOperarios.getInt("ope_id"));
				mapeoOperarios.setNombre(rsOperarios.getString("ope_nom"));
				mapeoOperarios.setCargo(rsOperarios.getString("ope_car"));
				mapeoOperarios.setArea(rsOperarios.getString("ope_are"));
				mapeoOperarios.setPermisos(rsOperarios.getString("ope_per"));
				vector.add(mapeoOperarios);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}

	public ArrayList<MapeoOperarios> datosOperariosInArea(MapeoOperarios r_mapeo)
	{
		ResultSet rsOperarios = null;		
		ArrayList<MapeoOperarios> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_operarios.idprd_operarios ope_id, ");
		cadenaSQL.append(" prd_operarios.nombre ope_nom, ");
		cadenaSQL.append(" prd_operarios.cargo ope_car, ");
		cadenaSQL.append(" prd_operarios.area ope_are, ");
		cadenaSQL.append(" prd_operarios.permisos ope_per ");
		cadenaSQL.append(" FROM prd_operarios ");
		
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getIdOperario()!=null && r_mapeo.getIdOperario().toString().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" idprd_operarios = '" + r_mapeo.getIdOperario() + "' ");
				}
				if (r_mapeo.getNombre()!=null && r_mapeo.getNombre().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" nombre = '" + r_mapeo.getNombre() + "' ");
				}
				if (r_mapeo.getCargo()!=null && r_mapeo.getCargo().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" cargo = '" + r_mapeo.getCargo() + "' ");
				}
				if (r_mapeo.getArea()!=null && r_mapeo.getArea().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" upper(area) in ('" + r_mapeo.getArea().toUpperCase() + "') ");
				}
				if (r_mapeo.getPermisos()!=null && r_mapeo.getPermisos().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" permisos = '" + r_mapeo.getPermisos() + "' ");
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by idprd_operarios asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOperarios.TYPE_SCROLL_SENSITIVE,rsOperarios.CONCUR_UPDATABLE);
			rsOperarios= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoOperarios>();
			
			while(rsOperarios.next())
			{
				MapeoOperarios mapeoOperarios = new MapeoOperarios();
				/*
				 * recojo mapeo operarios
				 */
				mapeoOperarios.setIdOperario(rsOperarios.getInt("ope_id"));
				mapeoOperarios.setNombre(rsOperarios.getString("ope_nom"));
				mapeoOperarios.setCargo(rsOperarios.getString("ope_car"));
				mapeoOperarios.setArea(rsOperarios.getString("ope_are"));
				mapeoOperarios.setPermisos(rsOperarios.getString("ope_per"));
				vector.add(mapeoOperarios);				
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public HashMap<Integer, String> datosOperarios()
	{
		ResultSet rsOperarios = null;		
		HashMap<Integer, String> hashOperarios = null;
		
		Integer contador = 0;
		StringBuffer cadenaSQL = new StringBuffer();
		hashOperarios = new HashMap<Integer, String>();
		
		try
		{
			cadenaSQL.append(" SELECT distinct prd_operarios.nombre ope_nom ");
			cadenaSQL.append(" FROM prd_operarios ");
			
			cadenaSQL.append(" order by nombre asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOperarios.TYPE_SCROLL_SENSITIVE,rsOperarios.CONCUR_UPDATABLE);
			rsOperarios= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOperarios.next())
			{
				hashOperarios.put(contador, rsOperarios.getString("ope_nom"));
				contador++;
			}
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return hashOperarios;
	}
	
	private Integer obtenerSiguiente()
	{
		ResultSet rsOperarios = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_operarios.idprd_operarios) ope_sig ");
     	cadenaSQL.append(" FROM prd_operarios ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOperarios.TYPE_SCROLL_SENSITIVE,rsOperarios.CONCUR_UPDATABLE);
			rsOperarios= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOperarios.next())
			{
				return rsOperarios.getInt("ope_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 0;
	}

	/*
	 * Devuelve el nivel de acceso a programaciones del usuario
	 * Recibe como parametro el nombre del usuario con el que buscamos a su operario equivalente
	 * 
	 * Devuelve:  0 - Sin permisos
	 * 			 10 - Solo Consulta
	 * 			 20 - Completar
	 * 			 30 - Cambio Observaciones
	 * 			 40 - Cambio observaciones y materia seca
	 * 			 50 - Cambio observaciones y materia seca y Creacion provisional
	 *  			 
	 * 			 99 - Acceso total 
	 */
	public String obtenerPermisos(String r_area)
	{
		ResultSet rsOperarios = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_operarios.permisos ope_per ");
     	cadenaSQL.append(" FROM prd_operarios ");
     	cadenaSQL.append(" WHERE prd_operarios.nombre = '" + eBorsao.get().accessControl.getNombre() + "'");
     	
     	if (r_area!=null) cadenaSQL.append(" and prd_operarios.area = '" + r_area + "'");
     	
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOperarios.TYPE_SCROLL_SENSITIVE,rsOperarios.CONCUR_UPDATABLE);
			rsOperarios= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOperarios.next())
			{
				return rsOperarios.getString("ope_per");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public String obtenerAlmacenDefecto(String r_nombre)
	{

		if (r_nombre.startsWith("Embotelladora"))
		{
			return "4";
		}
		else if (r_nombre.startsWith("Envasadora"))
		{
			return "1";
		}
		else if (r_nombre.startsWith("bib"))
		{
			return "1";
		}
		else if (r_nombre.startsWith("almacen capuchinos"))
		{
			return "1";
		}
		else if (r_nombre.startsWith("almacen"))
		{
			return "4";
		}
		return "4";
	}

	public String guardarNuevo(MapeoOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_operarios ( ");
    		cadenaSQL.append(" prd_operarios.idprd_operarios, ");
	        cadenaSQL.append(" prd_operarios.nombre, ");
		    cadenaSQL.append(" prd_operarios.cargo, ");
		    cadenaSQL.append(" prd_operarios.area, ");
		    cadenaSQL.append(" prd_operarios.permisos ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setInt(1, this.obtenerSiguiente());
		    
		    if (r_mapeo.getNombre()!=null)
		    {
		    	preparedStatement.setString(2, r_mapeo.getNombre());
		    }
		    else
		    {
		    	preparedStatement.setString(2, null);
		    }
		    if (r_mapeo.getCargo()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getCargo());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getArea()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArea());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getPermisos()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getPermisos());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }

	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void guardarCambios(MapeoOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			
			cadenaSQL.append(" UPDATE prd_operarios set ");
	        cadenaSQL.append(" prd_operarios.nombre=?, ");
		    cadenaSQL.append(" prd_operarios.cargo=?, ");
		    cadenaSQL.append(" prd_operarios.area=?, ");
		    cadenaSQL.append(" prd_operarios.permisos=? ");
		    cadenaSQL.append(" WHERE prd_operarios.idprd_operarios = ? ");
		    
		    con= this.conManager.establecerConexion();	 
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    preparedStatement.setString(1, r_mapeo.getNombre());
		    preparedStatement.setString(2, r_mapeo.getCargo());
		    preparedStatement.setString(3, r_mapeo.getArea());
		    preparedStatement.setString(4, r_mapeo.getPermisos());
		    preparedStatement.setInt(5, r_mapeo.getIdOperario());
		    preparedStatement.executeUpdate();
		}
		catch (Exception ex)
	    {
			ex.printStackTrace();
			serNotif.mensajeError(ex.getMessage());	    	
	    }
	}
	
	public void eliminar(MapeoOperarios r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_operarios ");            
			cadenaSQL.append(" WHERE prd_operarios.idprd_operarios = ?"); 
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdOperario());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}
}