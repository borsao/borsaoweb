package borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo;

import java.util.HashMap;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoOperarios mapeo = null;
	 
	 public MapeoOperarios convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoOperarios();
		 this.mapeo.setCargo(r_hash.get("cargo"));
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setPermisos(r_hash.get("permisos"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null)
		 {
			 this.mapeo.setIdOperario(new Integer(r_hash.get("id")));
		 }
		 return mapeo;		 
	 }
	 
	 public MapeoOperarios convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
				 
		 this.mapeo = new MapeoOperarios();
		 this.mapeo.setCargo(r_hash.get("cargo"));
		 this.mapeo.setArea(r_hash.get("area"));
		 this.mapeo.setPermisos(r_hash.get("permisos"));		 
		 this.mapeo.setNombre(r_hash.get("nombre"));
		 if (r_hash.get("id")!=null)
		 {
			 this.mapeo.setIdOperario(new Integer(r_hash.get("id")));
		 }

		 return mapeo;		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoOperarios r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();
		 this.hash.put("cargo", r_mapeo.getCargo());
		 this.hash.put("area", r_mapeo.getArea());
		 this.hash.put("nombre", r_mapeo.getNombre());
		 this.hash.put("permisos", r_mapeo.getPermisos());
		 if (r_mapeo.getIdOperario()!=null)
		 {
			 this.hash.put("id", r_mapeo.getIdOperario().toString());
		 }
		 return hash;		 
	 }
}