package borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo;

import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.GridPropio;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class OperariosGrid extends GridPropio {
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public OperariosGrid(ArrayList<MapeoOperarios> r_vector) {
        
        this.vector=r_vector;
		/*
		 * codigo para cargar el jtree-grid
		 */
		this.asignarTitulo("Operarios");
		this.generarGrid();
    }
    
    private void generarGrid()
    {
		
        this.crearGrid(MapeoOperarios.class);
		this.setRecords(this.vector);
		this.addStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("nombre", "cargo","area","permisos");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("nombre").setHeaderCaption("Nombre");
    	this.getColumn("cargo").setHeaderCaption("Cargo");
    	this.getColumn("area").setHeaderCaption("Area Producción");    	
    	this.getColumn("permisos").setHeaderCaption("Permisos App");
    	this.getColumn("idOperario").setHidden(true);
    	this.getColumn("idCodigo").setHidden(true);
    	
    }

	public void establecerColumnasNoFiltro() 
	{
	}

	public void asignarEstilos() 
	{		
	}

	public void cargarListeners() 
	{		
	}

	@Override
	public void calcularTotal() {
		
	}
}
