package borsao.e_borsao.Modulos.PRODUCCION.Operarios.view;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Field;

import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.MapeoOperarios;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.OperariosGrid;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A form for editing a single product.
 *
 * Using responsive layouts, the form can be displayed either sliding out on the
 * side of the view or filling the whole screen - see the theme for the related
 * CSS rules.
 */
public class OpcionesForm extends OpcionesFormDesign 	{	
	 
	 public boolean busqueda=false;
	 public boolean creacion=false;
	 public MapeoOperarios mapeoOperarios = null;
	 
	 private consultaOperariosServer cus = null;	 
	 private OperariosView uw = null;
	 private BeanFieldGroup<MapeoOperarios> fieldGroup;
	 
	 public OpcionesForm(OperariosView r_uw) {
        super();
        uw=r_uw;
        addStyleName("mytheme product-form");
        
        this.cargarCombos(this.cargo,"Cargo");
        this.cargarCombos(this.area,"Area");
        
        fieldGroup = new BeanFieldGroup<MapeoOperarios>(MapeoOperarios.class);
        fieldGroup.bindMemberFields(this);

        this.aviso.setCaptionAsHtml(true);
        this.aviso.setCaption("<p> AVISO </p>");
        this.aviso.setStyleName("bordeado");
        this.avisoTexto.setCaptionAsHtml(true);
        this.avisoTexto.setStyleName("bordeado");
        this.avisoTexto.setCaption("<p>Los valores posibles en el campo Permisos:</p>" +
                                   "<p>0 - Sin permiso </p>" + "<p>10 - Consulta Laboratorio </p>" + "<p>20 - Solo Consulta </p>" + "<p>30 - Impresion Papeles </p>" +
                                   "<p>40 - Abrir / Completar </p>" + "<p>50 - Cambio Observaciones </p>" +
                                   "<p>70 - Cambio Observaciones y Materia Seca </p>" +
                                   "<p>80 - Cambio Observaciones, Materia Seca y Altas provisionales </p>" +
                                    "<p>99 - Acceso total </p>");
        
                
        this.cargarListeners();
    }   

	private void cargarListeners()
	{
    	
        ValueChangeListener valueListener = new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                formHasChanged();
            }
        };
        for (Field f : fieldGroup.getFields()) {
    		f.addValueChangeListener(valueListener);
        }

        this.btnEliminar.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                mapeoOperarios = (MapeoOperarios) uw.grid.getSelectedRow();
                eliminarOperario(mapeoOperarios);
                removeStyleName("visible");
                btnGuardar.setCaption("Guardar");
                btnEliminar.setEnabled(true);
            	setEnabled(false);
            }
        });        
    	
		this.btnGuardar.addClickListener(new ClickListener() 
	 	{			
	 		@Override
	 		public void buttonClick(com.vaadin.ui.Button.ClickEvent event) 
			{			
				/*
				 * Llamaremos a analiticas view para regenerar el grid pasando el has escogido 			
				 */
	 			
	 			activarDesactivarControles(false);
	 			if (isBusqueda())
	 			{
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
		 			if (uw.isHayGrid())
		 			{			
		 				uw.grid.removeAllColumns();			
		 				uw.barAndGridLayout.removeComponent(uw.grid);
		 				uw.grid=null;			
		 			}
		 			uw.generarGrid(uw.opcionesEscogidas);
		 			btnGuardar.setCaption("Guardar");
		 			btnEliminar.setEnabled(true);
		 			uw.regresarDesdeForm();
	 			}
	 			else if (isCreacion())
	 			{
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoOperarios = hm.convertirHashAMapeo(uw.opcionesEscogidas);
	                crearOperario(mapeoOperarios);
	 			}
	 			else
	 			{
	 				MapeoOperarios mapeoOperarios_orig = (MapeoOperarios) uw.grid.getSelectedRow();
	 				hashToMapeo hm = new hashToMapeo(); 
	 				uw.opcionesEscogidas  = rellenarHashOpciones();
	 				mapeoOperarios= hm.convertirHashAMapeo(uw.opcionesEscogidas);
	 				modificarOperario(mapeoOperarios,mapeoOperarios_orig);
	 				hm=null;
	 			}
	 			if (((OperariosGrid) uw.grid)!=null)
	 			{
	 				uw.setHayGrid(true);
	 			}

	           	removeStyleName("visible");
	           	activarDesactivarControles(true);
 			}
 		});	
        
		btnCancel.addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
           	 removeStyleName("visible");
           	 btnGuardar.setCaption("Guardar");
           	 btnEliminar.setEnabled(true);
             setEnabled(false);
             uw.regresarDesdeForm();
            }
        });
		
	}
    
	private void cargarCombos(ComboBox r_combo, String r_nombre)
	{
		switch (r_nombre)
		{
			case "Cargo":
			{
				r_combo.addItem("Director Logistica");
				r_combo.addItem("Coordinador Produccion");
				r_combo.addItem("Tecnico Calidad");
				r_combo.addItem("Jefe Produccion");
				r_combo.addItem("Jefe Turno");
				r_combo.addItem("Suplente JT");
				r_combo.addItem("Operario");
				r_combo.addItem("Torero");
				r_combo.addItem("Botellas/Garrafas");
				r_combo.addItem("Cajas");
				r_combo.addItem("Otros");
				break;
			}
			case "Area":
			{
				r_combo.addItem("General");
				r_combo.addItem("Almacen");
				r_combo.addItem("Calidad");
				r_combo.addItem("Embotelladora");
				r_combo.addItem("Envasadora");
				r_combo.addItem("BIB");
				r_combo.addItem("Bodega");
				r_combo.addItem("Limpieza");
				r_combo.addItem("Laboratorio");
				r_combo.addItem("Tecnicos");
				break;
			}
		}
	}
	
	private void activarDesactivarControles(boolean r_activoono)
	{
		btnCancel.setEnabled(r_activoono);
		btnEliminar.setEnabled(r_activoono);
		btnGuardar.setEnabled(r_activoono);
	}
	
	private boolean isBusqueda()
	{
		return this.busqueda;
	}
	
	public boolean isCreacion() {
		return creacion;
	}

	public void setCreacion(boolean creacion) {
		editarOperario(null);
		this.creacion = creacion;		
	}

	private HashMap<String , String> rellenarHashOpciones()
	{
		HashMap<String , String> opcionesEscogidas=null;
		
		opcionesEscogidas = new HashMap<String , String>();
		
		if (this.cargo.getValue()!=null && this.cargo.getValue().toString().length()>0)
		{
			opcionesEscogidas.put("cargo", this.cargo.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("cargo", "");
		}
		
		if (this.area.getValue()!=null && this.area.getValue().toString().length()>0) 
		{
			opcionesEscogidas.put("area", this.area.getValue().toString());
		}
		else
		{
			opcionesEscogidas.put("area", "");
		}
		
		if (this.nombre.getValue()!=null && this.nombre.getValue().length()>0) 
		{
			opcionesEscogidas.put("nombre", this.nombre.getValue());
		}
		else
		{
			opcionesEscogidas.put("nombre", "");
		}
		if (this.permisos.getValue()!=null && this.permisos.getValue().length()>0) 
		{
			opcionesEscogidas.put("permisos", this.permisos.getValue());
		}
		else
		{
			opcionesEscogidas.put("permisos", "");
		}

		return opcionesEscogidas;
		
	}
	
	public void setBusqueda(boolean r_buscar)
	{		
		this.busqueda=r_buscar;
		if (r_buscar) editarOperario(null);
	}
	
	public void editarOperario(MapeoOperarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		if (r_mapeo==null) r_mapeo=new MapeoOperarios();
		fieldGroup.setItemDataSource(new BeanItem<MapeoOperarios>(r_mapeo));
		nombre.focus();
	}
	
	public void eliminarOperario(MapeoOperarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus = new consultaOperariosServer(CurrentUser.get());
		((OperariosGrid) uw.grid).remove(r_mapeo);
		cus.eliminar(r_mapeo);
		
	}
	
	public void modificarOperario(MapeoOperarios r_mapeo, MapeoOperarios r_mapeo_orig)
	{
		cus  = new consultaOperariosServer(CurrentUser.get());
		r_mapeo.setIdOperario(r_mapeo_orig.getIdOperario());
		cus.guardarCambios(r_mapeo);		
		((OperariosGrid) uw.grid).refresh(r_mapeo, r_mapeo_orig);
	}
	public void crearOperario(MapeoOperarios r_mapeo) {		
		/*
		 * Aqui le digo al form que rellene los campos 
		 */
		cus  = new consultaOperariosServer(CurrentUser.get());
		String rdo = cus.guardarNuevo(r_mapeo);
		if (rdo== null)
		{
			fieldGroup.setItemDataSource(new BeanItem<MapeoOperarios>(r_mapeo));
			if (((OperariosGrid) uw.grid)!=null)
			{
				((OperariosGrid) uw.grid).refresh(r_mapeo,null);
			}
			else
			{
				uw.generarGrid(uw.opcionesEscogidas);
			}
			this.setCreacion(false);
			uw.regresarDesdeForm();
		}
	}
	
    private void formHasChanged() {

        // only products that have been saved should be removable
        boolean canRemoveProduct = false;
        BeanItem<MapeoOperarios> item = fieldGroup.getItemDataSource();
        if (item != null) {
            this.mapeoOperarios= item.getBean();
            if (this.mapeoOperarios.getIdOperario()!=null) canRemoveProduct = true;
        }
        btnEliminar.setEnabled(canRemoveProduct);
    }

}
