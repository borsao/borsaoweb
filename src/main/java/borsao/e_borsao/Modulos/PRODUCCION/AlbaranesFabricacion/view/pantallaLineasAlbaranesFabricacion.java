package borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.modelo.MapeoLineasAlbaranesFabricacion;
import borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.server.consultaAlbaranesFabricacionServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaLineasAlbaranesFabricacion extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	
	public pantallaLineasAlbaranesFabricacion(String r_titulo, String r_documento, Integer r_albaran, String r_almacen)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaAlbaranesFabricacionServer cacs = new consultaAlbaranesFabricacionServer(CurrentUser.get());
		MapeoLineasAlbaranesFabricacion mapeo = new MapeoLineasAlbaranesFabricacion();
		
		mapeo.setDocumento(r_documento);
		mapeo.setIdCodigoCompra(r_albaran);
		mapeo.setAlmacen(r_almacen);
		ArrayList<MapeoLineasAlbaranesFabricacion> vector = cacs.datosLineasAlbaranesGlobal(mapeo);
		
		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}
	
	public pantallaLineasAlbaranesFabricacion(String r_titulo, String r_documento, Date r_fecha, String r_almacen, String r_codigonc)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaAlbaranesFabricacionServer cacs = new consultaAlbaranesFabricacionServer(CurrentUser.get());
		MapeoLineasAlbaranesFabricacion mapeo = new MapeoLineasAlbaranesFabricacion();
		
		mapeo.setDocumento(r_documento);
		mapeo.setFechaDocumento(r_fecha);
		mapeo.setAlmacen(r_almacen);
		mapeo.setClave_tabla("B");
		ArrayList<MapeoLineasAlbaranesFabricacion> vector = cacs.datosLineasAlbaranesGlobal(mapeo,r_codigonc);
		
		this.llenarRegistros(vector);
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoLineasAlbaranesFabricacion> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasAlbaranesFabricacion mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Ejercicio", Integer.class, null);
		container.addContainerProperty("Documento", String.class, null);
		container.addContainerProperty("Codigo", String.class, null);
		container.addContainerProperty("Fecha Documento", String.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Descripcion", String.class, null);
		container.addContainerProperty("Unidades", Double.class, null);
        
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasAlbaranesFabricacion) iterator.next();
			
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Descripcion").setValue(RutinasCadenas.conversion(mapeoAyuda.getDescripcion()));
			
    		file.getItemProperty("Documento").setValue(RutinasCadenas.conversion(mapeoAyuda.getDocumento()));
    		file.getItemProperty("Fecha Documento").setValue(RutinasFechas.convertirDateToString(mapeoAyuda.getFechaDocumento()));
    		
    		file.getItemProperty("Ejercicio").setValue(mapeoAyuda.getEjercicio());
    		file.getItemProperty("Codigo").setValue(mapeoAyuda.getIdCodigoCompra().toString());
    		file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.asignarEstilos();
	}
	
	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Unidades".equals(cellReference.getPropertyId()) || "Ejercicio".equals(cellReference.getPropertyId()) || "Codigo".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}