package borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.modelo.MapeoLineasAlbaranesFabricacion;

public class consultaAlbaranesFabricacionServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private static consultaAlbaranesFabricacionServer instance;
	
	
	public consultaAlbaranesFabricacionServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaAlbaranesFabricacionServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaAlbaranesFabricacionServer(r_usuario);			
		}
		return instance;
	}
	
	public String generarInforme(Integer codigoInterno, Integer numeroDocumento)
	{
		
		String resultadoGeneracion = null;
    	return resultadoGeneracion;
	}
	

	public boolean actualizarLineasAlbaran(Integer r_albaran)
	{
		/*
		 * obs_envio2
		 * establecemos el valor "S" cuando hemos enviado el mail
		 * 
		 */
		
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		Connection con = null;
		
		String ejer = "0";
		
		try
		{
	        
			cadenaSQL.append(" UPDATE a_lin_" + ejer + " set ");
    		cadenaSQL.append(" observacion = ? ");
	        cadenaSQL.append(" where clave_tabla = 'B' ");
	        cadenaSQL.append(" and documento = 'B1' ");
	        cadenaSQL.append(" and serie = 'b' ");
	        cadenaSQL.append(" and codigo = ? ");
		    
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    preparedStatement.setString(1, "IE" );
		    preparedStatement.setInt(2, r_albaran);

	        preparedStatement.executeUpdate();
			
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return false;
	    }
		finally
		{
			try
			{
				if (preparedStatement!=null)
				{
					preparedStatement.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return true;
	}
	
	public String semaforo()
	{
		String errores = "0";
		return errores;
	}
	

	public ArrayList<MapeoLineasAlbaranesFabricacion> datosLineasAlbaranesGlobal(MapeoLineasAlbaranesFabricacion r_mapeo)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesFabricacion> vector = null;
		MapeoLineasAlbaranesFabricacion mapeo = null;
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;		

		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_mapeo.getEjercicio()!=null)
		{
			if (r_mapeo.getEjercicio().intValue() == ejercicioActual.intValue()) digito="0";
			if (r_mapeo.getEjercicio().intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());
		}

		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.almacen det_alm, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.num_linea det_lin ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p");
     	
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.codigo = " + r_mapeo.getIdCodigoCompra());
				}
				if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.almacen = '" + r_mapeo.getAlmacen() + "' ");
				}
				if (r_mapeo.getFechaDocumento()!=null && (RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento())).length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.fecha_documento = '" + RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento()) + "' ");
				}
			}

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by 8,2 ");
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesFabricacion>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesFabricacion();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public ArrayList<MapeoLineasAlbaranesFabricacion> datosLineasAlbaranesGlobal(MapeoLineasAlbaranesFabricacion r_mapeo, String r_codigonc)
	{
		
		ResultSet rsOpcion = null;		
		ArrayList<MapeoLineasAlbaranesFabricacion> vector = null;
		MapeoLineasAlbaranesFabricacion mapeo = null;
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		Connection con = null;
		Statement cs = null;		

		String digito = "0";
		Integer ejercicioActual = new Integer(RutinasFechas.añoActualYYYY());
		
		if (r_mapeo.getEjercicio()!=null)
		{
			if (r_mapeo.getEjercicio().intValue() == ejercicioActual.intValue()) digito="0";
			if (r_mapeo.getEjercicio().intValue() < ejercicioActual.intValue()) digito=String.valueOf(ejercicioActual-r_mapeo.getEjercicio());
		}

		cadenaSQL.append(" SELECT p.ejercicio det_eje, ");
		cadenaSQL.append(" p.fecha_documento det_fec, ");
        cadenaSQL.append(" p.documento det_doc, ");
        cadenaSQL.append(" p.codigo det_cod, ");
        cadenaSQL.append(" p.almacen det_alm, ");
        cadenaSQL.append(" p.articulo det_art, ");
        cadenaSQL.append(" p.descrip_articulo det_des, ");
        cadenaSQL.append(" p.unidades det_uds, ");
        cadenaSQL.append(" p.num_linea det_lin ");
     	cadenaSQL.append(" FROM a_lin_" + digito + " p");
     	cadenaSQL.append(" inner join e_articu b on b.articulo COLLATE latin1_spanish_ci = p.articulo COLLATE latin1_spanish_ci  and b.vl_caract_2 = '" + r_codigonc.trim() + "' ");
     	
		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getDocumento()!=null && r_mapeo.getDocumento().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.documento = '" + r_mapeo.getDocumento() + "' ");
				}
				if (r_mapeo.getClave_tabla()!=null && r_mapeo.getClave_tabla().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.clave_tabla = '" + r_mapeo.getClave_tabla() + "' ");
				}
				if (r_mapeo.getIdCodigoCompra()!=null && r_mapeo.getIdCodigoCompra()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.codigo = " + r_mapeo.getIdCodigoCompra());
				}
				if (r_mapeo.getAlmacen()!=null && r_mapeo.getAlmacen().length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.almacen = '" + r_mapeo.getAlmacen() + "' ");
				}
				if (r_mapeo.getFechaDocumento()!=null && (RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento())).length()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" p.fecha_documento = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_mapeo.getFechaDocumento())) + "' ");
				}
			}

			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by 2,4 ");
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoLineasAlbaranesFabricacion>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasAlbaranesFabricacion();
				mapeo.setEjercicio(rsOpcion.getInt("det_eje"));
				mapeo.setFechaDocumento(rsOpcion.getDate("det_fec"));
				mapeo.setDocumento(rsOpcion.getString("det_doc"));
				mapeo.setIdCodigoCompra(rsOpcion.getInt("det_cod"));
				mapeo.setArticulo(rsOpcion.getString("det_art"));
				mapeo.setDescripcion(rsOpcion.getString("det_des"));
				mapeo.setUnidades(rsOpcion.getDouble("det_uds"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}		
		finally
		{
			try
			{
				if (rsOpcion!=null)
				{
					rsOpcion.close();
				}
				if (cs!=null)
				{
					cs.close();
				}
				if (con!=null)
				{
					con.close();
				}
			}
			catch (Exception ex)
			{
				serNotif.mensajeError(ex.getMessage());
			}
		}
		return vector;
	}

	public String semaforos() {

		return null;
	}
}
