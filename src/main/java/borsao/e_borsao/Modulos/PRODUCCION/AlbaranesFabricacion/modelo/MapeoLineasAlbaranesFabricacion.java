package borsao.e_borsao.Modulos.PRODUCCION.AlbaranesFabricacion.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoLineasAlbaranesFabricacion extends MapeoGlobal
{
	private Integer idCodigo;
	private String clave_tabla;
	private String documento;
	
	private String serie;
	private String alias;
	private String direccion;
	private String codigoPostal;
	private String almacen;
	private Integer idCodigoCompra;
	private Integer ejercicio;
	private Integer posicion;
	private Integer linea;
	private String articulo;
	private String descripcion;
	private Double cantidad;
	private Double unidades;

	private Double precio;
	private Double descuento;
	private Double importe;
	private String moneda;
	private Double importeLocal;
    private Date fechaDocumento;
    private Date plazoEntrega;
	private Integer proveedor;
	private String nombreProveedor;
	private String nif;

	
	
	public MapeoLineasAlbaranesFabricacion()
	{
	}


	public Integer getIdCodigoCompra() {
		return idCodigoCompra;
	}


	public void setIdCodigoCompra(Integer idCodigoCompra) {
		this.idCodigoCompra = idCodigoCompra;
	}

	public String getDocumento() {
		return documento;
	}


	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}



	public Integer getPosicion() {
		return posicion;
	}


	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}


	public Integer getLinea() {
		return linea;
	}


	public void setLinea(Integer linea) {
		this.linea = linea;
	}


	public String getArticulo() {
		return articulo;
	}


	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Double getCantidad() {
		return cantidad;
	}


	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}


	public Double getUnidades() {
		return unidades;
	}


	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public Double getDescuento() {
		return descuento;
	}


	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}


	public Double getImporte() {
		return importe;
	}


	public void setImporte(Double importe) {
		this.importe = importe;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public Double getImporteLocal() {
		return importeLocal;
	}


	public void setImporteLocal(Double importeLocal) {
		this.importeLocal = importeLocal;
	}


	public Integer getIdCodigo() {
		return idCodigo;
	}


	public void setIdCodigo(Integer idCodigo) {
		this.idCodigo = idCodigo;
	}


	
	public Date getFechaDocumento() {
		return fechaDocumento;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public void setFechaDocumento(Date fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}


	public Date getPlazoEntrega() {
		return plazoEntrega;
	}


	public void setPlazoEntrega(Date plazoEntrega) {
		this.plazoEntrega = plazoEntrega;
	}


	public Integer getProveedor() {
		return proveedor;
	}


	public void setProveedor(Integer proveedor) {
		this.proveedor = proveedor;
	}


	public String getNombreProveedor() {
		return nombreProveedor;
	}


	public void setNombreProveedor(String nombreProveedor) {
		this.nombreProveedor = nombreProveedor;
	}

	public String getNif() {
		return nif;
	}


	public void setNif(String nif) {
		this.nif = nif;
	}


	public String getAlmacen() {
		return almacen;
	}


	public void setAlmacen(String almacen) {
		this.almacen = almacen;
	}


	public String getClave_tabla() {
		return clave_tabla;
	}

	public void setClave_tabla(String clave_tabla) {
		this.clave_tabla = clave_tabla;
	}


	public String getAlias() {
		return alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}