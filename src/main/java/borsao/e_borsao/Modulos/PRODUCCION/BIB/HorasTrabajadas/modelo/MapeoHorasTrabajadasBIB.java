package borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoHorasTrabajadasBIB extends MapeoGlobal
{
	private Integer ejercicio;
	private Integer semana;
	private Double horas;
	private Double horasEtt;
	private Double incidencias;
	private Double programadas;
	private Double fueraLinea;
	private Double mermas;
	private Double horasReales;
	private Double fueraLineaReales;
	private Double incidenciasReales;
	private Double programadasReales;

	public MapeoHorasTrabajadasBIB()
	{
	}


	public Integer getEjercicio() {
		return ejercicio;
	}


	public void setEjercicio(Integer ejercicio) {
		this.ejercicio = ejercicio;
	}


	public Integer getSemana() {
		return semana;
	}


	public void setSemana(Integer semana) {
		this.semana = semana;
	}


	public Double getHoras() {
		return horas;
	}


	public void setHoras(Double horas) {
		this.horas = horas;
	}


	public Double getIncidencias() {
		return incidencias;
	}


	public void setIncidencias(Double incidencias) {
		this.incidencias = incidencias;
	}


	public Double getProgramadas() {
		return programadas;
	}


	public void setProgramadas(Double programadas) {
		this.programadas = programadas;
	}


	public Double getMermas() {
		return mermas;
	}


	public void setMermas(Double mermas) {
		this.mermas = mermas;
	}


	public Double getHorasReales() {
		return horasReales;
	}


	public void setHorasReales(Double horasReales) {
		this.horasReales = horasReales;
	}


	public Double getFueraLineaReales() {
		return fueraLineaReales;
	}


	public void setFueraLineaReales(Double fueraLineaReales) {
		this.fueraLineaReales = fueraLineaReales;
	}


	public Double getIncidenciasReales() {
		return incidenciasReales;
	}


	public void setIncidenciasReales(Double incidenciasReales) {
		this.incidenciasReales = incidenciasReales;
	}


	public Double getProgramadasReales() {
		return programadasReales;
	}


	public void setProgramadasReales(Double programadasReales) {
		this.programadasReales = programadasReales;
	}


	public Double getFueraLinea() {
		return fueraLinea;
	}


	public void setFueraLinea(Double fueraLinea) {
		this.fueraLinea = fueraLinea;
	}


	public Double getHorasEtt() {
		return horasEtt;
	}


	public void setHorasEtt(Double horasEtt) {
		this.horasEtt = horasEtt;
	}


}