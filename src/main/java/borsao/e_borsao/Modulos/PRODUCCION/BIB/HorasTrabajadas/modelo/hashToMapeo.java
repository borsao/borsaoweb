package borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo;

import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;

public class hashToMapeo
{
	 public HashMap<String , String> hash = null;
	 public MapeoHorasTrabajadasBIB mapeo = null;
	 
	 public MapeoHorasTrabajadasBIB convertirHashAMapeoBusqueda(HashMap<String , String> r_hash)
	 {
		 try {
			 
			 this.mapeo = new MapeoHorasTrabajadasBIB();
			 
			 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
			 
			 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));		 
			 if (r_hash.get("horas")!=null && r_hash.get("horas").length()>0) this.mapeo.setHoras(new Double(r_hash.get("horas")));
			 if (r_hash.get("horasEtt")!=null && r_hash.get("horasEtt").length()>0) this.mapeo.setHorasEtt(new Double(r_hash.get("horasEtt")));
			 if (r_hash.get("fueraLinea")!=null && r_hash.get("fueraLinea").length()>0) this.mapeo.setFueraLinea(new Double(RutinasCadenas.reemplazar(r_hash.get("fueraLinea"),",",".")));
			 if (r_hash.get("incidencias")!=null && r_hash.get("incidencias").length()>0) this.mapeo.setIncidencias(new Double(RutinasCadenas.reemplazar(r_hash.get("incidencias"),",",".")));
			 if (r_hash.get("programadas")!=null && r_hash.get("programadas").length()>0) this.mapeo.setProgramadas(new Double(RutinasCadenas.reemplazar(r_hash.get("programadas"),",",".")));
			 if (r_hash.get("mermas")!=null && r_hash.get("mermas").length()>0) this.mapeo.setMermas(new Double(RutinasCadenas.reemplazar(r_hash.get("mermas"),",",".")));
			 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(r_hash.get("semana")));
			 if (r_hash.get("horasReales")!=null && r_hash.get("horasReales").length()>0) this.mapeo.setHorasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("horasReales"),",",".")));
			 if (r_hash.get("fueraLineaReales")!=null && r_hash.get("fueraLineaReales").length()>0) this.mapeo.setFueraLineaReales(new Double(RutinasCadenas.reemplazar(r_hash.get("fueraLineaReales"),",",".")));
			 if (r_hash.get("incidenciasReales")!=null && r_hash.get("incidenciasReales").length()>0) this.mapeo.setIncidenciasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("incidenciasReales"),",",".")));
			 if (r_hash.get("programadasReales")!=null && r_hash.get("programadasReales").length()>0) this.mapeo.setProgramadasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("programadasReales"),",",".")));

	 	} catch (Exception e) {
			e.printStackTrace();
		} 
		 return mapeo;		 
	 }
	 
	 public MapeoHorasTrabajadasBIB convertirHashAMapeo(HashMap<String , String> r_hash)
	 {
		 
		 try {
			 
			 this.mapeo = new MapeoHorasTrabajadasBIB();
			 
			 if (r_hash.get("idCodigo")!=null) this.mapeo.setIdCodigo(new Integer(r_hash.get("idCodigo")));
			 
			 if (r_hash.get("ejercicio")!=null && r_hash.get("ejercicio").length()>0) this.mapeo.setEjercicio(new Integer(RutinasCadenas.quitarPuntoMiles(r_hash.get("ejercicio"))));		 
			 if (r_hash.get("horas")!=null && r_hash.get("horas").length()>0) this.mapeo.setHoras(new Double(r_hash.get("horas")));
			 if (r_hash.get("horasEtt")!=null && r_hash.get("horasEtt").length()>0) this.mapeo.setHorasEtt(new Double(r_hash.get("horasEtt")));
			 if (r_hash.get("fueraLinea")!=null && r_hash.get("fueraLinea").length()>0) this.mapeo.setFueraLinea(new Double(RutinasCadenas.reemplazar(r_hash.get("fueraLinea"),",",".")));
			 if (r_hash.get("incidencias")!=null && r_hash.get("incidencias").length()>0) this.mapeo.setIncidencias(new Double(RutinasCadenas.reemplazar(r_hash.get("incidencias"),",",".")));
			 if (r_hash.get("programadas")!=null && r_hash.get("programadas").length()>0) this.mapeo.setProgramadas(new Double(RutinasCadenas.reemplazar(r_hash.get("programadas"),",",".")));
			 if (r_hash.get("mermas")!=null && r_hash.get("mermas").length()>0) this.mapeo.setMermas(new Double(RutinasCadenas.reemplazar(r_hash.get("mermas"),",",".")));
			 if (r_hash.get("semana")!=null && r_hash.get("semana").length()>0) this.mapeo.setSemana(new Integer(r_hash.get("semana")));
			 if (r_hash.get("horasReales")!=null && r_hash.get("horasReales").length()>0) this.mapeo.setHorasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("horasReales"),",",".")));
			 if (r_hash.get("fueraLineaReales")!=null && r_hash.get("fueraLineaReales").length()>0) this.mapeo.setFueraLineaReales(new Double(RutinasCadenas.reemplazar(r_hash.get("fueraLineaReales"),",",".")));
			 if (r_hash.get("incidenciasReales")!=null && r_hash.get("incidenciasReales").length()>0) this.mapeo.setIncidenciasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("incidenciasReales"),",",".")));
			 if (r_hash.get("programadasReales")!=null && r_hash.get("programadasReales").length()>0) this.mapeo.setProgramadasReales(new Double(RutinasCadenas.reemplazar(r_hash.get("programadasReales"),",",".")));
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return mapeo;		 
		 
	 }
	 
	 public HashMap<String , String> convertirMapeoAHash(MapeoHorasTrabajadasBIB r_mapeo)
	 {
		 this.hash= new HashMap<String , String>();

		 if (r_mapeo.getIdCodigo()!=null) this.hash.put("idCodigo", r_mapeo.getIdCodigo().toString());
		 
		 if (r_mapeo.getEjercicio()!=null) this.hash.put("ejercicio", r_mapeo.getEjercicio().toString());
		 if (r_mapeo.getHoras()!=null) this.hash.put("horas", r_mapeo.getHoras().toString());
		 if (r_mapeo.getHorasEtt()!=null) this.hash.put("horasEtt", r_mapeo.getHorasEtt().toString());
		 if (r_mapeo.getFueraLinea()!=null) this.hash.put("fueraLinea", r_mapeo.getFueraLinea().toString());
		 if (r_mapeo.getIncidencias()!=null) this.hash.put("incidencias", r_mapeo.getIncidencias().toString());
		 if (r_mapeo.getProgramadas()!=null) this.hash.put("programadas", r_mapeo.getProgramadas().toString());
		 if (r_mapeo.getMermas()!=null) this.hash.put("mermas", r_mapeo.getMermas().toString());
		 if (r_mapeo.getSemana()!=null) this.hash.put("semana", r_mapeo.getSemana().toString());
		 if (r_mapeo.getHorasReales()!=null) this.hash.put("horasReales", r_mapeo.getHoras().toString());
		 if (r_mapeo.getFueraLineaReales()!=null) this.hash.put("fueraLineaReales", r_mapeo.getFueraLineaReales().toString());
		 if (r_mapeo.getIncidenciasReales()!=null) this.hash.put("incidenciasReales", r_mapeo.getIncidencias().toString());
		 if (r_mapeo.getProgramadasReales()!=null) this.hash.put("programadasReales", r_mapeo.getProgramadas().toString());
		 return hash;		 
	 }
}