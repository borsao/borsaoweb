package borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo.MapeoHorasTrabajadasBIB;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo.OpcionesGrid;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo.hashToMapeo;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.server.consultaHorasTrabajadasBIBServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class HorasTrabajadasBIBView extends GridViewRefresh {

	public static final String VIEW_NAME = "Horas Trabajadas BIB";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private MapeoHorasTrabajadasBIB mapeoHoras=null;
	private OpcionesForm form = null;
	
    public void generarGrid(HashMap<String , String> opcionesEscogidas)
    {
    	ArrayList<MapeoHorasTrabajadasBIB> r_vector=null;
    	consultaHorasTrabajadasBIBServer ccs = new consultaHorasTrabajadasBIBServer(CurrentUser.get());
    	
    	hashToMapeo hm = new hashToMapeo();
    	
    	mapeoHoras =hm.convertirHashAMapeoBusqueda(opcionesEscogidas);
    	
    	r_vector=ccs.datosOpcionesGlobal(mapeoHoras);
    	if (r_vector!=null && r_vector.size()>0)
    	{    	
	    	grid = new OpcionesGrid(this, r_vector);
			setHayGrid(true);
    	}
    	else
    	{
    		setHayGrid(false);
    		Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");	
    	}
    }
    
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public HorasTrabajadasBIBView() 
    {
    }

    public void cargarPantalla() 
    {
    	setConFormulario(this.conFormulario);
    	    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);
//    	lblTitulo.setValue(this.VIEW_NAME);//, ContentMode.HTML);
        
        this.form = new OpcionesForm(this);
        this.verForm(true);
        
        addComponent(this.form);

    }

    public void newForm()
    {
    	this.form.setCreacion(true);
    	this.form.setBusqueda(false);
    	this.form.btnGuardar.setCaption("Guardar");
    	this.form.btnEliminar.setEnabled(false);    	
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);

    }
    
    public void verForm(boolean r_busqueda)
    {    	
    	this.form.setBusqueda(r_busqueda);
    	this.form.setCreacion(false);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.btnGuardar.setCaption("Buscar");
    	this.form.btnEliminar.setEnabled(false);    	

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    	/*
    	 * deberia mostrar el formulario de opciones con este mapeo rellenado
    	 */
    	this.form.editarHorasTrabajadas((MapeoHorasTrabajadasBIB) r_fila);
    	this.form.addStyleName("visible");
    	this.form.setEnabled(true);
    	this.form.setBusqueda(false);
    }
    
    @Override
    public void reestablecerPantalla() {
    	
    }

    public void print() {
    	
    }

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
	}
	
    public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(HorasTrabajadasBIBView.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(HorasTrabajadasBIBView.VIEW_NAME, this.getClass());
	}

	@Override
	public void eliminarRegistro() {
		
	}

	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
}
