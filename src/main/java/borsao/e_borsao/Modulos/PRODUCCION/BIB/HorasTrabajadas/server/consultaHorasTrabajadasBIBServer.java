package borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.PRODUCCION.BIB.HorasTrabajadas.modelo.MapeoHorasTrabajadasBIB;

public class consultaHorasTrabajadasBIBServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private static consultaHorasTrabajadasBIBServer instance;
	
	public consultaHorasTrabajadasBIBServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaHorasTrabajadasBIBServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaHorasTrabajadasBIBServer(r_usuario);			
		}
		return instance;
	}
	
	public ArrayList<MapeoHorasTrabajadasBIB> datosOpcionesGlobal(MapeoHorasTrabajadasBIB r_mapeo)
	{
		ResultSet rsOpcion = null;		
		ArrayList<MapeoHorasTrabajadasBIB> vector = null;
		StringBuffer cadenaWhere =null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		
		cadenaSQL.append(" SELECT prd_horas_bib.idCodigo hor_id, ");
		cadenaSQL.append(" prd_horas_bib.ejercicio hor_eje, ");
		cadenaSQL.append(" prd_horas_bib.semana hor_sem, ");
		cadenaSQL.append(" prd_horas_bib.horas hor_hor, ");
		cadenaSQL.append(" prd_horas_bib.horasEtt hor_hor_ett, ");
		cadenaSQL.append(" prd_horas_bib.programados hor_prg, ");
		cadenaSQL.append(" prd_horas_bib.mermas hor_mer, ");
        cadenaSQL.append(" prd_horas_bib.incidencias hor_inc, ");
        cadenaSQL.append(" prd_horas_bib.horasReales hor_horr, ");
        cadenaSQL.append(" prd_horas_bib.programadosReales hor_prgr, ");
        cadenaSQL.append(" prd_horas_bib.incidenciasReales hor_incr, ");
        cadenaSQL.append(" prd_horas_bib.fueraLineaReales hor_flinr ");
     	cadenaSQL.append(" FROM prd_horas_bib ");

		try
		{
			if (r_mapeo!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (r_mapeo.getEjercicio()!=null && r_mapeo.getEjercicio()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" ejercicio = " + r_mapeo.getEjercicio());
				}
				if (r_mapeo.getSemana()!=null && r_mapeo.getSemana()>0)
				{
					if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
					cadenaWhere.append(" semana = " + r_mapeo.getSemana());
				}
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaSQL.append(" order by ejercicio, semana asc");
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoHorasTrabajadasBIB>();
			
			while(rsOpcion.next())
			{
				MapeoHorasTrabajadasBIB mapeo = new MapeoHorasTrabajadasBIB();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdCodigo(rsOpcion.getInt("hor_id"));
				mapeo.setEjercicio(rsOpcion.getInt("hor_eje"));
				mapeo.setSemana(rsOpcion.getInt("hor_sem"));
				mapeo.setHoras(rsOpcion.getDouble("hor_hor"));
				mapeo.setHorasEtt(rsOpcion.getDouble("hor_hor_ett"));
				mapeo.setProgramadas(rsOpcion.getDouble("hor_prg"));
				mapeo.setMermas(rsOpcion.getDouble("hor_mer"));
				mapeo.setIncidencias(rsOpcion.getDouble("hor_inc"));
				mapeo.setHorasReales(rsOpcion.getDouble("hor_horr"));
				mapeo.setFueraLineaReales(rsOpcion.getDouble("hor_flinr"));
				mapeo.setProgramadasReales(rsOpcion.getDouble("hor_prgr"));
				mapeo.setIncidenciasReales(rsOpcion.getDouble("hor_incr"));
				
				vector.add(mapeo);				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	
	public Double recuperarHoras(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horas) as horas from prd_horas_bib where ejercicio = " + r_ejercicio;
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarHorasEtt(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horasEtt) as horas from prd_horas_bib where ejercicio = " + r_ejercicio;
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarHorasReales(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(horasReales) as horas from prd_horas_bib where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}
	
	public HashMap<String, Double> recuperarHorasRealesAño(Integer r_ejercicio)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		HashMap<String, Double> hashHoras = null;
		try
		{
			sql="select semana, sum(horasReales) as horas from prd_horas_bib where ejercicio = " + r_ejercicio + " group by semana" ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			
			hashHoras = new HashMap<String, Double>();
			
			while(rsHoras.next())
			{
				hashHoras.put(rsHoras.getString("semana"), rsHoras.getDouble("horas"));
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return hashHoras;
	}
	
	public Double recuperarIncidencias(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(incidencias) as horas from prd_horas_bib where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;

			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}
	
	public Double recuperarIncidenciasReales(Integer r_ejercicio, Integer r_semana,String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		StringBuffer sql = null;
		try
		{
			sql = new StringBuffer();
			
			sql.append("select sum(tiempo) as horas from prd_paradas_parte ");
			sql.append("inner join prd_cabecera_parte on prd_paradas_parte.idCabeceraParte = prd_cabecera_parte.idCodigo");
			sql.append(" and ejercicio = " + r_ejercicio); 
			sql.append(" and tipo in ('C','N') " ); 
			sql.append(" and linea = 'BIB' " ); 
			
			if (acumulado.contentEquals("Acumulado")) sql.append(" and semana <= " + r_semana);
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql.append(" and semana between " + desde + " and " + hasta);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql.append(" and semana = " + r_semana );
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql.toString());
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}
	
	public Double recuperarProgramadas(Integer r_ejercicio, Integer r_semana, String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(programados) as horas from prd_horas_bib where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public Double recuperarProgramadasReales(Integer r_ejercicio, Integer r_semana,String acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(programadosReales) as horas from prd_horas_bib where ejercicio = " + r_ejercicio; 
			if (acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			
			con= this.conManager.establecerConexionInd();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsHoras!= null)
				{
					rsHoras.close();
					rsHoras=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return horas_obtenido;
	}

	public Double recuperarMermas(Integer r_ejercicio, Integer r_semana, String r_acumulado)
	{
		Double horas_obtenido = null;
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select sum(mermas) as horas from prd_horas_bib where ejercicio = " + r_ejercicio; 
			if (r_acumulado.contentEquals("Acumulado")) sql = sql + " and semana <= " + r_semana ;
			else if (r_acumulado.contentEquals("Mensual"))
			{
				try {
					int desde = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate("01/" + r_semana + "/" + r_ejercicio));
					int diaFinMes=RutinasFechas.obtenerUltimoDiaMesActual(Integer.valueOf(r_ejercicio).intValue(), Integer.valueOf(r_semana).intValue());
					int hasta = RutinasFechas.obtenerSemanaFecha(RutinasFechas.convertirADate(diaFinMes + "/" + r_semana + "/" + r_ejercicio));
					if (desde>hasta) desde = 1;
					sql = sql + " and semana between " + desde + " and " + hasta;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (r_semana !=null && r_semana !=0) sql = sql + " and semana = " + r_semana ;
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				horas_obtenido = rsHoras.getDouble("horas");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return horas_obtenido;
	}

	public boolean comprobarHoras(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select horas as horas from prd_horas_bib where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}
	
	public boolean comprobarIncidencias(Integer r_ejercicio, Integer r_semana)
	{
		ResultSet rsHoras = null;
		Connection con = null;
		Statement cs = null;
		String sql = null;
		try
		{
			sql="select incidencias as horas from prd_horas_bib where ejercicio = " + r_ejercicio + " and semana = " + r_semana ;
				
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			rsHoras = cs.executeQuery(sql);
			while(rsHoras.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return false;
	}


	public String guardarNuevo(MapeoHorasTrabajadasBIB r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" INSERT INTO prd_horas_bib ( ");
			cadenaSQL.append(" prd_horas_bib.idCodigo, ");
			cadenaSQL.append(" prd_horas_bib.ejercicio, ");
    		cadenaSQL.append(" prd_horas_bib.semana, ");
    		cadenaSQL.append(" prd_horas_bib.horas, ");
    		cadenaSQL.append(" prd_horas_bib.programados, ");
    		cadenaSQL.append(" prd_horas_bib.mermas, ");
    		cadenaSQL.append(" prd_horas_bib.incidencias, ");
    		cadenaSQL.append(" prd_horas_bib.horasEtt ) VALUES (");
		    cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString());
		    r_mapeo.setIdCodigo(obtenerSiguienteCodigoInterno(con, "prd_horas_bib", "idCodigo"));
		    preparedStatement.setInt(1, r_mapeo.getIdCodigo());
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(2, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(2, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String guardarCambios(MapeoHorasTrabajadasBIB r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		try
		{
			cadenaSQL.append(" update prd_horas_bib SET ");			
			cadenaSQL.append(" prd_horas_bib.ejercicio=?, ");
    		cadenaSQL.append(" prd_horas_bib.semana=?, ");
    		cadenaSQL.append(" prd_horas_bib.horas=?, ");
    		cadenaSQL.append(" prd_horas_bib.programados=?, ");
    		cadenaSQL.append(" prd_horas_bib.mermas=?, ");
    		cadenaSQL.append(" prd_horas_bib.incidencias=?, ");
    		cadenaSQL.append(" prd_horas_bib.horasReales=?, ");
    		cadenaSQL.append(" prd_horas_bib.programadosReales=?, ");
    		cadenaSQL.append(" prd_horas_bib.incidenciasReales=?, ");
    		cadenaSQL.append(" prd_horas_bib.fueraLineaReales=?, ");
    		cadenaSQL.append(" prd_horas_bib.horasEtt=? ");
	        cadenaSQL.append(" WHERE prd_horas_bib.idCodigo=? ");
		    
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
		    if (r_mapeo.getEjercicio()!=null)
		    {
		    	preparedStatement.setInt(1, r_mapeo.getEjercicio());
		    }
		    else
		    {
		    	preparedStatement.setInt(1, 0);
		    }
		    if (r_mapeo.getSemana()!=null)
		    {
		    	preparedStatement.setDouble(2, r_mapeo.getSemana());
		    }
		    else
		    {
		    	preparedStatement.setDouble(2, 0);
		    }
		    if (r_mapeo.getHoras()!=null)
		    {
		    	preparedStatement.setDouble(3, r_mapeo.getHoras());
		    }
		    else
		    {
		    	preparedStatement.setDouble(3, 0);
		    }
		    if (r_mapeo.getProgramadas()!=null)
		    {
		    	preparedStatement.setDouble(4, r_mapeo.getProgramadas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(4, 0);
		    }
		    if (r_mapeo.getMermas()!=null)
		    {
		    	preparedStatement.setDouble(5, r_mapeo.getMermas());
		    }
		    else
		    {
		    	preparedStatement.setDouble(5, 0);
		    }

		    if (r_mapeo.getIncidencias()!=null)
		    {
		    	preparedStatement.setDouble(6, r_mapeo.getIncidencias());
		    }
		    else
		    {
		    	preparedStatement.setDouble(6, 0);
		    }
		    if (r_mapeo.getHorasReales()!=null)
		    {
		    	preparedStatement.setDouble(7, r_mapeo.getHorasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(7, 0);
		    }
		    if (r_mapeo.getProgramadasReales()!=null)
		    {
		    	preparedStatement.setDouble(8, r_mapeo.getProgramadasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(8, 0);
		    }
		    if (r_mapeo.getIncidenciasReales()!=null)
		    {
		    	preparedStatement.setDouble(9, r_mapeo.getIncidenciasReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(9, 0);
		    }
		    if (r_mapeo.getFueraLineaReales()!=null)
		    {
		    	preparedStatement.setDouble(10, r_mapeo.getFueraLineaReales());
		    }
		    else
		    {
		    	preparedStatement.setDouble(10, 0);
		    }
		    if (r_mapeo.getHorasEtt()!=null)
		    {
		    	preparedStatement.setDouble(11, r_mapeo.getHorasEtt());
		    }
		    else
		    {
		    	preparedStatement.setDouble(11, 0);
		    }
		    preparedStatement.setInt(12, r_mapeo.getIdCodigo());
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public void eliminar(MapeoHorasTrabajadasBIB r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();        
		
		try
		{
			cadenaSQL.append(" DELETE FROM prd_horas_bib ");            
			cadenaSQL.append(" WHERE prd_horas_bib.idCodigo = ?");
			
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.setInt(1, r_mapeo.getIdCodigo());
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
	}

	@Override
	public String semaforos() {
		return null;
	}
}