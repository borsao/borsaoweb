package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoControlEmbotellado  extends MapeoGlobal
{
	private String serie;
	private Integer idControlEmbotellado;
	private Date fecha;
	private String articulo;
	private String descripcion;
	private Integer cantidad;
	private Integer litros;
	private String tipoProduccion;
	private String tipoCalculo;
	private Integer cajas1;
	private String lote1;
	private Integer cajas2;
	private String lote2;
	private Integer cajas3;
	private String lote3;
	private Integer cajas4;
	private String lote4;
	private Integer cajas5;
	private String lote5;
	private Integer embalaje;
	private String observaciones;
	private String verficado;
	private Integer idGreensys;
	private Integer idProgramacion;
	
	public MapeoControlEmbotellado()
	{
	}

	public Integer getIdControlEmbotellado() {
		return idControlEmbotellado;
	}

	public void setIdControlEmbotellado(Integer idControlEmbotellado) {
		this.idControlEmbotellado = idControlEmbotellado;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getTipoProduccion() {
		return tipoProduccion;
	}

	public void setTipoProduccion(String tipoProduccion) {
		this.tipoProduccion = tipoProduccion;
	}

	public Integer getCajas1() {
		return cajas1;
	}

	public void setCajas1(Integer cajas1) {
		this.cajas1 = cajas1;
	}

	public String getLote1() {
		return lote1;
	}

	public void setLote1(String lote1) {
		this.lote1 = lote1;
	}

	public Integer getCajas2() {
		return cajas2;
	}

	public void setCajas2(Integer cajas2) {
		this.cajas2 = cajas2;
	}

	public String getLote2() {
		return lote2;
	}

	public void setLote2(String lote2) {
		this.lote2 = lote2;
	}

	public Integer getCajas3() {
		return cajas3;
	}

	public void setCajas3(Integer cajas3) {
		this.cajas3 = cajas3;
	}

	public String getLote3() {
		return lote3;
	}

	public void setLote3(String lote3) {
		this.lote3 = lote3;
	}

	public Integer getCajas4() {
		return cajas4;
	}

	public void setCajas4(Integer cajas4) {
		this.cajas4 = cajas4;
	}

	public String getLote4() {
		return lote4;
	}

	public void setLote4(String lote4) {
		this.lote4 = lote4;
	}

	public Integer getLitros() {
		return litros;
	}

	public void setLitros(Integer litros) {
		this.litros = litros;
	}

	public String getTipoCalculo() {
		return tipoCalculo;
	}

	public void setTipoCalculo(String tipoCalculo) {
		this.tipoCalculo = tipoCalculo;
	}

	public Integer getCajas5() {
		return cajas5;
	}

	public void setCajas5(Integer cajas5) {
		this.cajas5 = cajas5;
	}

	public String getLote5() {
		return lote5;
	}

	public void setLote5(String lote5) {
		this.lote5 = lote5;
	}

	public Integer getEmbalaje() {
		return embalaje;
	}

	public void setEmbalaje(Integer embalaje) {
		this.embalaje = embalaje;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getVerficado() {
		return verficado;
	}

	public void setVerficado(String verficado) {
		this.verficado = verficado;
	}

	public Integer getIdGreensys() {
		return idGreensys;
	}

	public void setIdGreensys(Integer idGreensys) {
		this.idGreensys = idGreensys;
	}

	public Integer getIdProgramacion() {
		return idProgramacion;
	}

	public void setIdProgramacion(Integer idProgramacion) {
		this.idProgramacion = idProgramacion;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}


}