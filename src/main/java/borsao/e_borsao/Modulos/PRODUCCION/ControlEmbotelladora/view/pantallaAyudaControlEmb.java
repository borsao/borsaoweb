package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.AyudaControlEmbotelladoGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaAyudaControlEmb extends Window
{
	private ControlEmbotelladoView app = null;
	private Button btnBotonCVentana = null;
	private Grid gridComponentes = null;
	private VerticalLayout principal=null;
	
	private TextFieldAyuda articulo = null;
	private TextField numero= null;
	private TextField consejo= null;
	private TextField serieConsejo= null;
	private DateField fecha = null;

	private boolean hayGridComponentes= false;
	private VerticalLayout frameCentral = null;	
	
	public pantallaAyudaControlEmb(ControlEmbotelladoView r_app, String r_titulo)
	{
		this.app=r_app;
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1500px");
		this.setHeight("950px");
		
		this.cargarPantalla();
		this.cargarListeners();
		this.llenarRegistros();
		
		this.setContent(principal);
	}

	private void cargarPantalla()
	{
		principal = new VerticalLayout();
		principal.setSizeFull();

			this.fecha = new DateField("Fecha");
			this.fecha.setEnabled(true);
			this.fecha.setDateFormat("dd/MM/yyyy");
//			this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));
			
			this.numero=new TextField();
			this.numero.setEnabled(true);
			this.numero.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.numero.addStyleName("rightAligned");
			this.numero.setCaption("Numerador");
			
			this.consejo=new TextField();
			this.consejo.setEnabled(true);
			this.consejo.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.consejo.addStyleName("rightAligned");
			this.consejo.setCaption("Numerador Consejo");

			this.serieConsejo=new TextField();
			this.serieConsejo.setEnabled(true);
			this.serieConsejo.addStyleName(ValoTheme.TEXTFIELD_TINY);
			this.serieConsejo.setCaption("Serie Consejo");
			
			this.articulo= new TextFieldAyuda("Articulo","");					
			
			this.frameCentral = new VerticalLayout();
			this.frameCentral.setSpacing(true);
			this.frameCentral.setMargin(true);
			
			this.frameCentral.addComponent(this.fecha);
			this.frameCentral.addComponent(this.numero);
			this.frameCentral.addComponent(this.articulo);
		
			this.frameCentral.addComponent(this.serieConsejo);
			this.frameCentral.addComponent(this.consejo);

			HorizontalLayout framePie = new HorizontalLayout();
		
				btnBotonCVentana = new Button("Cerrar");
				btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);
				framePie.addComponent(btnBotonCVentana);		
				framePie.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(this.frameCentral);
		principal.addComponent(framePie);
		principal.setComponentAlignment(this.frameCentral, Alignment.TOP_LEFT);
		principal.setComponentAlignment(framePie, Alignment.BOTTOM_LEFT);
	}
	
	private void cargarListeners()
	{
		this.consejo.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (consejo.getValue()!=null && consejo.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 
		this.serieConsejo.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (serieConsejo.getValue()!=null && serieConsejo.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		});
		this.fecha.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (fecha.getValue()!=null && fecha.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		this.numero.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (numero.getValue()!=null && numero.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		this.articulo.txtTexto.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().toString().length()>0) 
				{
					if (isHayGridComponentes())
					{
						gridComponentes.removeAllColumns();			
						frameCentral.removeComponent(gridComponentes);
						gridComponentes=null;
					}		
					llenarRegistros();
				}
			}
		}); 

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) 
			{
				MapeoControlEmbotellado mapeo=(MapeoControlEmbotellado) gridComponentes.getSelectedRow();
				if (mapeo!=null && mapeo.getIdControlEmbotellado()!=null)
				{
					app.numeradorSeleccionado=mapeo.getIdControlEmbotellado();				
					app.areaSeleccionada=mapeo.getSerie();
				}
				else 
				{
					app.numeradorSeleccionado=null;
					app.areaSeleccionada=null;
				}
				app.cerrarVentanaBusqueda();
				close();
			}
		});
		
	}
	
	private void llenarRegistros()
	{
		Integer numerador = null; 
    	ArrayList<MapeoControlEmbotellado> r_vector2 =null;
    	
    	consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(CurrentUser.get());
    	
    	if (this.numero.getValue()!=null && this.numero.getValue().length()>0)
    	{
    		numerador = new Integer(this.numero.getValue());
    	}
    	
    	r_vector2=ces.recuperarControlEmbotellado(this.articulo.txtTexto.getValue().trim(), numerador, this.fecha.getValue(), this.serieConsejo.getValue(), this.consejo.getValue());
    	
    	this.presentarGrid(r_vector2);

	}

    private void presentarGrid(ArrayList<MapeoControlEmbotellado> r_vector2)
    {
    	
		gridComponentes = new AyudaControlEmbotelladoGrid(r_vector2);
		if (((AyudaControlEmbotelladoGrid) gridComponentes).vector==null)
    	{
    		setHayGridComponentes(false);
    	}
    	else
    	{
    		setHayGridComponentes(true);
    	}
		
		if (isHayGridComponentes())
		{
			this.gridComponentes.setHeight("300px");
			this.gridComponentes.setWidth("100%");
			this.frameCentral.addComponent(gridComponentes);
			
			this.frameCentral.setComponentAlignment(gridComponentes, Alignment.MIDDLE_LEFT);
		}
    }

	public boolean isHayGridComponentes() {
		return hayGridComponentes;
	}

	public void setHayGridComponentes(boolean hayGridComponentes) {
		this.hayGridComponentes = hayGridComponentes;
	}
	
	public void close() {
		app.cerrarVentanaBusqueda();
		super.close();
	}
}