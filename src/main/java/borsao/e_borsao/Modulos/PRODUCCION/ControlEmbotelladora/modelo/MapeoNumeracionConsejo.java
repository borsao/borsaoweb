package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;

public class MapeoNumeracionConsejo extends MapeoGlobal
{
	private Integer idControlEmbotellado;
	private Integer desdeNumeracion;
	private Integer hastaNumeracion;
	private Integer numeroRollo;
	private Integer diferencias;
	private String serie;
	private String serieNum;
	
	public MapeoNumeracionConsejo()
	{
		this.setSerie("");
		this.setDesdeNumeracion(new Integer(0));
		this.setHastaNumeracion(new Integer(0));
	}

	public Integer getIdControlEmbotellado() {
		return idControlEmbotellado;
	}

	public void setIdControlEmbotellado(Integer idControlEmbotellado) {
		this.idControlEmbotellado = idControlEmbotellado;
	}

	public Integer getDesdeNumeracion() {
		return desdeNumeracion;
	}

	public void setDesdeNumeracion(Integer desdeNumeracion) {
		this.desdeNumeracion = desdeNumeracion;
	}

	public Integer getHastaNumeracion() {
		return hastaNumeracion;
	}

	public void setHastaNumeracion(Integer hastaNumeracion) {
		this.hastaNumeracion = hastaNumeracion;
	}

	public Integer getNumeroRollo() {
		return numeroRollo;
	}

	public void setNumeroRollo(Integer numeroRollo) {
		this.numeroRollo = numeroRollo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Integer getDiferencias() {
		return diferencias;
	}

	public void setDiferencias(Integer diferencias) {
		this.diferencias = diferencias;
	}

	public String getSerieNum() {
		return serieNum;
	}

	public void setSerieNum(String serieNum) {
		this.serieNum = serieNum;
	}

	
}