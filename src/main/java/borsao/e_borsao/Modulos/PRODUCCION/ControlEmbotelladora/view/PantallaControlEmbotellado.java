package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view;

import java.util.ArrayList;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.ControlEmbotelladoConsejoGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.ControlEmbotelladoEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoLineasControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoNumeracionConsejo;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.Programacion.modelo.MapeoProgramacion;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class PantallaControlEmbotellado extends Window 
{
    
	public TextField cantidad= null;
	public TextField litros= null;
	public ComboBox cmbSerie = null;
	public TextArea txtObservaciones=null;
	public TextFieldAyuda articulo = null;
	public TextField numero= null;
	public DateField fecha = null;
	public Label descripcion= null;
	public ComboBox cmbCalculos = null;
    public Button opcAgregarComponente = null;
    public Button opcAgregarLote = null;
    public Button opcNuevo = null;
    public Button opcImprimir = null;
    public Button opcEliminar = null;
    public Button opcSalir = null;
    
	public TextField cajas1= null;
	public TextField lotes1= null;
	public TextField cajas2= null;
	public TextField lotes2= null;
	public TextField cajas3= null;
	public TextField lotes3= null;
	public TextField cajas4= null;
	public TextField lotes4= null;
	public TextField cajas5= null;
	public TextField lotes5= null;
	
	public Grid grid2 = null;
	public Grid grid3 = null;
	
	private HorizontalLayout panelArticulo = null;
	private HorizontalLayout panelCajas = null;
	public HorizontalLayout cabLayout = null;
	public HorizontalLayout topLayout = null;
	private VerticalLayout principal=null;
	
	public HorizontalLayout horGrid = null;
	private VerticalLayout panGrid2 = null;
	private VerticalLayout panGrid3 = null;
	
	public String permisos = null;
	private boolean hayGrid2 = false;
	private boolean hayGrid3 = false;
	private ArrayList<MapeoLineasControlEmbotellado> r_vector2 =null;
	private ArrayList<MapeoNumeracionConsejo> r_vector3 =null;
	private Integer tipoEmbalaje = null;
	
	private MapeoControlEmbotellado mapeoControl = null;
	private MapeoProgramacion mapeoProgramacion = null;
	public consultaControlEmbotelladoServer caps =null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public PantallaControlEmbotellado(MapeoProgramacion r_mapeo, String r_titulo)
    {
		this.setCaption(r_titulo);
		this.mapeoProgramacion=r_mapeo;
		this.center();
		this.setModal(true);
		this.setClosable(true);
		
		this.setResizable(true);
		this.setWidth("1700px");
		this.setHeight("950px");
		
		this.cargarPantalla();
		
		this.setContent(principal);
		
    }

    public void cargarPantalla() 
    {
    	consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    	this.permisos = ces.comprobarAccesos();
    	
		principal = new VerticalLayout();
		principal.setSizeFull();
		principal.setMargin(true);

		this.topLayout = new HorizontalLayout();
    	this.topLayout.setSpacing(true);
    	
	    	this.opcNuevo= new Button("Crear");
	    	this.opcNuevo.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcNuevo.setIcon(FontAwesome.PLUS_CIRCLE);
	
	    	this.opcEliminar= new Button("Eliminar");
	    	this.opcEliminar.addStyleName(ValoTheme.BUTTON_DANGER);
	    	this.opcEliminar.setVisible(false);
	    	this.opcEliminar.setIcon(FontAwesome.MINUS_CIRCLE);
	
	    	this.opcImprimir= new Button("Imprimir");
	    	this.opcImprimir.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcImprimir.setIcon(FontAwesome.PRINT);
		
	    	this.opcSalir= new Button("Salir");
	    	this.opcSalir.addStyleName(ValoTheme.BUTTON_PRIMARY);
	    	this.opcSalir.setIcon(FontAwesome.POWER_OFF);
	
    	this.topLayout.addComponent(this.opcNuevo);
    	this.topLayout.addComponent(this.opcEliminar);
    	this.topLayout.addComponent(this.opcImprimir);
    	this.topLayout.addComponent(this.opcSalir);
    	
		this.cabLayout = new HorizontalLayout();
    	this.cabLayout.setSpacing(true);
    	
	    	this.fecha = new DateField("Fecha");
	    	this.fecha.setEnabled(true);
	    	this.fecha.setDateFormat("dd/MM/yyyy");
	    	this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));

	    	this.cmbSerie=new ComboBox("Linea");
			this.cmbSerie.addItem("EMBOTELLADO");
			this.cmbSerie.addItem("ENVASADO");
			this.cmbSerie.addItem("BIB");
			this.cmbSerie.setInvalidAllowed(false);
			this.cmbSerie.setNewItemsAllowed(false);
			this.cmbSerie.setNullSelectionAllowed(false);

			this.numero=new TextField();
			this.numero.setEnabled(true);
			this.numero.addStyleName("rightAligned");
			this.numero.setCaption("Numerador");
			
			this.cmbCalculos=new ComboBox("Cálculo por Botellas o Cajas?");
			this.cmbCalculos.addItem("BOTELLAS");
			this.cmbCalculos.addItem("CAJAS");
			this.cmbCalculos.setValue("BOTELLAS");
			this.cmbCalculos.setInvalidAllowed(false);
			this.cmbCalculos.setNewItemsAllowed(false);
			this.cmbCalculos.setNullSelectionAllowed(false);
	    	
			this.litros=new TextField();
			this.litros.setEnabled(false);
			this.litros.setCaption("Litros");
			this.litros.addStyleName("rightAligned");
			this.litros.setValue("0");
	
			this.txtObservaciones=new TextArea();
			this.txtObservaciones.setEnabled(false);
			this.txtObservaciones.setWidth("500px");
			this.txtObservaciones.setCaption("Observaciones");
	
		this.cabLayout.addComponent(this.fecha);
		this.cabLayout.addComponent(this.cmbSerie);
		this.cabLayout.addComponent(this.numero);
		this.cabLayout.addComponent(this.cmbCalculos);
		this.cabLayout.addComponent(this.litros);
		this.cabLayout.addComponent(this.txtObservaciones);
		
    	this.panelArticulo = new HorizontalLayout();
    	this.panelArticulo.setSpacing(true);

			this.articulo= new TextFieldAyuda("Articulo","");
			
			this.descripcion = new Label();
			this.descripcion.setWidth("375px");
	
			this.cantidad=new TextField();
			this.cantidad.setEnabled(true);
			this.cantidad.setCaption("A Fabricar");
			this.cantidad.addStyleName("rightAligned");
			this.cantidad.setValue("0");
			
		this.panelArticulo.addComponent(this.articulo);
		this.panelArticulo.addComponent(this.descripcion);
		this.panelArticulo.setComponentAlignment(this.descripcion, Alignment.MIDDLE_LEFT);
		this.panelArticulo.addComponent(this.cantidad);
		
    	this.panelCajas = new HorizontalLayout();
    	this.panelCajas.setSpacing(true);
    	
	    	this.cajas1=new TextField();
			this.cajas1.setEnabled(true);
			this.cajas1.setCaption("UDS.");
			this.cajas1.setWidth("75px");
			this.cajas1.addStyleName("rightAligned");
			this.cajas1.setValue("");
	
	    	this.lotes1=new TextField();
			this.lotes1.setEnabled(true);
			this.lotes1.setCaption("LOTE");
			this.lotes1.setWidth("125px");
			this.lotes1.setValue("");
	
	    	this.cajas2=new TextField();
			this.cajas2.setEnabled(true);
			this.cajas2.setCaption("UDS.");
			this.cajas2.setWidth("75px");
			this.cajas2.addStyleName("rightAligned");
			this.cajas2.setValue("");
			
			this.lotes2=new TextField();
			this.lotes2.setEnabled(true);
			this.lotes2.setCaption("LOTE");
			this.lotes2.setWidth("125px");
			this.lotes2.setValue("");
	
	    	this.cajas3=new TextField();
			this.cajas3.setEnabled(true);
			this.cajas3.setCaption("UDS.");
			this.cajas3.setWidth("75px");
			this.cajas3.addStyleName("rightAligned");
			this.cajas3.setValue("");
	
			this.lotes3=new TextField();
			this.lotes3.setEnabled(true);
			this.lotes3.setCaption("LOTE");
			this.lotes3.setWidth("125px");
			this.lotes3.setValue("");
			
	    	this.cajas4=new TextField();
			this.cajas4.setEnabled(true);
			this.cajas4.setCaption("UDS.");
			this.cajas4.setWidth("75px");
			this.cajas4.addStyleName("rightAligned");
			this.cajas4.setValue("");
	
			this.lotes4=new TextField();
			this.lotes4.setEnabled(true);
			this.lotes4.setCaption("LOTE");
			this.lotes4.setWidth("125px");
			this.lotes4.setValue("");
	
	    	this.cajas5=new TextField();
			this.cajas5.setEnabled(true);
			this.cajas5.setCaption("UDS.");
			this.cajas5.setWidth("75px");
			this.cajas5.addStyleName("rightAligned");
			this.cajas5.setValue("");
	
			this.lotes5=new TextField();
			this.lotes5.setEnabled(true);
			this.lotes5.setCaption("LOTE");
			this.lotes5.setWidth("125px");
			this.lotes5.setValue("");

		this.panelCajas.addComponent(this.cajas1);
		this.panelCajas.addComponent(this.lotes1);
		this.panelCajas.addComponent(this.cajas2);
		this.panelCajas.addComponent(this.lotes2);
		this.panelCajas.addComponent(this.cajas3);
		this.panelCajas.addComponent(this.lotes3);
		this.panelCajas.addComponent(this.cajas4);
		this.panelCajas.addComponent(this.lotes4);
		this.panelCajas.addComponent(this.cajas5);
		this.panelCajas.addComponent(this.lotes5);
		
    	this.opcAgregarComponente= new Button("Agregar");
    	this.opcAgregarComponente.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcAgregarComponente.setIcon(FontAwesome.PLUS_CIRCLE);
    	
    	this.opcAgregarLote= new Button("Numeración Etiquetas");
    	this.opcAgregarLote.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcAgregarLote.setIcon(FontAwesome.PLUS_CIRCLE);
    	
    	horGrid = new HorizontalLayout();
    	horGrid.setSizeFull();
    	
    	
    	principal.addComponent(this.topLayout);
    	principal.addComponent(this.cabLayout);
    	principal.addComponent(this.panelArticulo);
    	principal.addComponent(this.panelCajas);
    	
    	
    	this.establecerSituacion("Inicial");
    	
    	this.generarGrid();
    	
    	this.cargarListeners();
		
		if (eBorsao.get().accessControl.getNombre().contains("Embotelladora"))
		{
			this.cmbSerie.setValue("EMBOTELLADO");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("Envasadora"))
		{
			this.cmbSerie.setValue("ENVASADO");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("bib"))
		{
			this.cmbSerie.setValue("BIB");
		}
		else
		{
		}
		
    }

    public void generarGrid()
    {
    	consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
    	Integer cantidad = 0;
    	ArrayList<MapeoNumeracionConsejo> vectorConsejo = null;
    	
    	caps = new consultaControlEmbotelladoServer(CurrentUser.get());
    	this.mapeoControl = caps.recuperarControlEmbotellado(this.mapeoProgramacion.getIdProgramacion());
    	
    	if (mapeoControl!=null && this.mapeoControl.getIdProgramacion()>0 && this.opcNuevo.getCaption().equals("Crear"))
    	{
    		this.opcNuevo.setCaption("Guardar");
    		this.numero.setValue(this.mapeoControl.getIdControlEmbotellado().toString());
    		rellenarEntradasDatos(this.mapeoControl);
			r_vector2 = ces.recuperarEscandallo(cmbSerie.getValue().toString(),numero.getValue());
			vectorConsejo = ces.recuperarNumeracionConsejo(cmbSerie.getValue().toString(), numero.getValue());
			presentarGrid(r_vector2, vectorConsejo);
			establecerSituacion("Encontrado");
    	}    	
    	else if (this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0)
    	{
        	if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
        	{
        		cantidad =  RutinasNumericas.formatearIntegerDeESP(this.cantidad.getValue());
        	}
        	else
        	{
        		cantidad = 0;
        	}

    		r_vector2=this.caps.generarDatosEscandallo(this.articulo.txtTexto.getValue(),cantidad);
    		
    		if (((ControlEmbotelladoConsejoGrid) grid3)!=null) 
        		vectorConsejo = (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
    		else
    		{
    			r_vector2 = new ArrayList<MapeoLineasControlEmbotellado>();
    			vectorConsejo = new ArrayList<MapeoNumeracionConsejo>();
    		}
    	}
    	
    	this.presentarGrid(r_vector2,vectorConsejo);
    	if (r_vector2!=null && !r_vector2.isEmpty()) this.calcularLitros(cantidad,r_vector2);
    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoLineasControlEmbotellado> r_vector2, ArrayList<MapeoNumeracionConsejo> r_vector3)
    {

		if (horGrid!=null) horGrid.removeAllComponents();
		
		grid2 = new ControlEmbotelladoEscandalloGrid(r_vector2);
		if (((ControlEmbotelladoEscandalloGrid) grid2).vector==null || ((ControlEmbotelladoEscandalloGrid) grid2).vector.size()==0)
    	{
    		setHayGrid2(false);
    	}
    	else
    	{
    		setHayGrid2(true);
    		grid2.setEditorEnabled(true);
    	}
		
//		if (isHayGrid2())
		{
			panGrid2 = new VerticalLayout();
			panGrid3 = new VerticalLayout();
			
			panGrid2.addComponent(this.opcAgregarComponente);
			panGrid2.addComponent(grid2);
			
			horGrid.addComponent(panGrid2);
			horGrid.setExpandRatio(panGrid2, 2);
			
			Integer idControl = null;
			
			if (this.numero.getValue()!=null && this.numero.getValue().length()>0)
			{
				idControl = new Integer(this.numero.getValue());
			}
			
			grid3 = new ControlEmbotelladoConsejoGrid(this, r_vector3,idControl);
			
			if (((ControlEmbotelladoConsejoGrid) grid3).vector==null || ((ControlEmbotelladoConsejoGrid) grid3).vector.size()==0)
	    	{
	    		setHayGrid3(false);
	    	}
	    	else
	    	{
	    		setHayGrid3(true);
	    		grid3.setEditorEnabled(true);
	    	}
			
			panGrid3.addComponent(this.opcAgregarLote);
			panGrid3.addComponent(grid3);
			
			horGrid.addComponent(panGrid3);
			horGrid.setExpandRatio(panGrid3, 1);
			
			this.principal.addComponent(horGrid);
			this.principal.setExpandRatio(horGrid, 1);
			this.principal.setSpacing(true);
			
			if (((ControlEmbotelladoEscandalloGrid) grid2).vector!=null && ((ControlEmbotelladoEscandalloGrid) grid2).vector.size()>0)
			{
				this.opcAgregarLote.setEnabled(true);
				this.opcAgregarComponente.setEnabled(true);
			}
		}
    }
    
    private void cargarListeners()
    {
    	this.cmbSerie.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (opcNuevo.getCaption().equals("Crear"))
				{
					limpiarCampos();
					if (isHayGrid2())
					{
						grid2.removeAllColumns();			
						panGrid2.removeComponent(grid2);
						grid2=null;						
					}
					if (isHayGrid3())
					{
						grid3.removeAllColumns();			
						panGrid3.removeComponent(grid3);
						grid3=null;						
					}
				}
				
				consultaContadoresEjercicioServer cces = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
				Long contador = cces.recuperarContador(new Integer(RutinasFechas.añoActualYYYY()), "PRODUCCION", cmbSerie.getValue().toString());
				numero.setValue(contador.toString());
			}
		});

    	this.opcNuevo.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			establecerSituacion("Creacion");
    			newForm();
    		}
    	});

    	this.opcEliminar.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			eliminarRegistro();
    		}
    	});

    	this.opcImprimir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			print();
    		}
    	});

    	this.opcSalir.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			close();
    		}
    	});

    	this.opcAgregarComponente.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid2() && grid2.getEditedItemId()==null)
		    	{
		    		MapeoLineasControlEmbotellado mapeo =new MapeoLineasControlEmbotellado();
		    		mapeo.setArticulo("");
					mapeo.setDescripcion("");
					mapeo.setUnidades(0);
					mapeo.setMermas(0);
					mapeo.setOrigen(1);
					grid2.getContainerDataSource().addItem(mapeo);    					
					grid2.editItem(mapeo);
		    	}
			}
    	});

    	this.opcAgregarLote.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid2() && grid3.getEditedItemId()==null)
		    	{
		    		MapeoNumeracionConsejo mapeo =new MapeoNumeracionConsejo();
		    		mapeo.setNumeroRollo(obtenerSiguienteRollo());
		    		mapeo.setSerie("");
					mapeo.setDesdeNumeracion(0);
					mapeo.setHastaNumeracion(0);
					grid3.getContainerDataSource().addItem(mapeo);    	
					grid3.editItem(mapeo);
		    	}
			}
    	});

		this.numero.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (numero.getValue()!=null && numero.getValue().length()>0 && opcNuevo.getCaption().equals("Crear")  ) 
				{
					consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
					mapeoControl = ces.recuperarControlEmbotellado(cmbSerie.getValue().toString(),numero.getValue());
//					
					if (mapeoControl!=null)
					{
						Notificaciones.getInstance().mensajeAdvertencia("Numerador existente. Dame otro.");
					}
					else
					{
						establecerSituacion("Inicial");
						newForm();
					}
				}
				else if (numero.getValue()!=null && numero.getValue().length()>0 && opcNuevo.getCaption().equals("Guardar") )
				{
					consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);

					if (mapeoControl!=null)
					{
						rellenarEntradasDatos(mapeoControl);
						r_vector2 = ces.recuperarEscandallo(cmbSerie.getValue().toString(), numero.getValue());
						r_vector3 = ces.recuperarNumeracionConsejo(cmbSerie.getValue().toString(),numero.getValue());
						presentarGrid(r_vector2, r_vector3);
						establecerSituacion("Encontrado");
					}
				}
				else
				{
					establecerSituacion("Inicial");
					newForm();
				}
			}
		});

		this.cantidad.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbCalculos.getValue().toString().toUpperCase().equals("BOTELLAS"))
				{
					if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 && cantidad.getValue()!=null && cantidad.getValue().length()>0) 
					{
						boolean eliminar = true;
						ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
						if (vectorEscandallo!=null)
						{
							for (int i=0;i<vectorEscandallo.size();i++)
							{
								MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(i);
								
								if (mapeo.getMermas()!=0)
								{									
									eliminar=false;
								}
							}
						}
						if (eliminar)
						{
							if (isHayGrid2())
							{
								grid2.removeAllColumns();			
								principal.removeComponent(grid2);
								grid2=null;
							}
							generarGrid();
						}
					}
				}
			}
		}); 
		
		this.cajas1.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas1.getValue()!=null && cajas1.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();							
					}
				}
			}
		}); 

		this.cajas2.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas2.getValue()!=null && cajas2.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		
		this.cajas3.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas3.getValue()!=null && cajas3.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		
		this.cajas4.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas4.getValue()!=null && cajas4.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		this.cajas5.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas5.getValue()!=null && cajas5.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
    }
    

	public void print() 
	{
		String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    	pdfGenerado = ces.generarInforme(cmbSerie.getValue().toString(),new Integer(this.numero.getValue()));
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		
		pdfGenerado = null;
		establecerSituacion("Encontrado");
	}
	
	public void newForm() 
	{
		String strArticulo = null;
		
		if (mapeoProgramacion!=null && mapeoProgramacion.getArticulo()!=null)
		{
			if (mapeoProgramacion.getTipo().equals("ETIQUETADO")) strArticulo = mapeoProgramacion.getArticulo() + "-1"; else strArticulo =mapeoProgramacion.getArticulo();
		}
		
		if (opcNuevo.getCaption().equals("Crear"))
		{
			
			this.articulo.txtTexto.setValue(strArticulo);
			this.descripcion.setValue(mapeoProgramacion.getDescripcion());
			this.cantidad.focus();

			if (cmbCalculos.getValue().toString().toUpperCase().equals("BOTELLAS"))
			{
				if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 && cantidad.getValue()!=null && cantidad.getValue().length()>0) 
				{
					boolean eliminar = true;
					ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
					if (vectorEscandallo!=null)
					{
						for (int i=0;i<vectorEscandallo.size();i++)
						{
							MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(i);
							
							if (mapeo.getMermas()!=0)
							{									
								eliminar=false;
							}
						}
					}
					if (eliminar)
					{
						if (isHayGrid2())
						{
							grid2.removeAllColumns();			
							principal.removeComponent(grid2);
							grid2=null;
						}
						generarGrid();
					}
				}
			}
			this.establecerSituacion("Creacion");
		}
		else if (opcNuevo.getCaption().equals("Guardar"))
		{
			consultaControlEmbotelladoServer cces = new consultaControlEmbotelladoServer(CurrentUser.get());
			/*
			 * 1.- Valores entradas de datos
			 * 2.- vector del escandallo
			 * 3.- vector de los rollos
			 * 
			 * 4.- pasarlo todo para guardar
			 * 
			 */
			Integer cant = null;
			MapeoControlEmbotellado mapeoControlEmbotellado =  this.rellenarMapeo();
			
			caps = new consultaControlEmbotelladoServer(CurrentUser.get());			
        	if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
        	{
        		cant =  RutinasNumericas.formatearIntegerDeESP(this.cantidad.getValue());
        	}
        	else
        	{
        		cant = 0;
        	}
			r_vector2=caps.generarDatosEscandallo(this.articulo.txtTexto.getValue(),cant);
			
			ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
			for (int i =0; i<r_vector2.size(); i++)
			{
				MapeoLineasControlEmbotellado mapeoNuevo = (MapeoLineasControlEmbotellado) r_vector2.get(i);
				
				for (int j=0;j<vectorEscandallo.size();j++)
				{
					MapeoLineasControlEmbotellado mapeoViejo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(j);
					if (mapeoNuevo.getArticulo()==mapeoViejo.getArticulo())
					{
						mapeoNuevo.setOrden(mapeoViejo.getOrden());
						mapeoNuevo.setMermas(mapeoViejo.getMermas());
						vectorEscandallo.remove(j);
						vectorEscandallo.add(j, mapeoNuevo);
						break;
					}
				}
			}
			
			ArrayList<MapeoNumeracionConsejo> vectorNumeracionConsejo =  (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
			
			
			boolean resultado = cces.guardarDatos(mapeoControlEmbotellado, vectorEscandallo, vectorNumeracionConsejo);
			
			if (resultado)
			{
				establecerSituacion("Encontrado");
			}
			else
			{
				Notificaciones.getInstance().mensajeAdvertencia("Revisa los datos");
			}
			
		}
	}
	
	private MapeoControlEmbotellado rellenarMapeo()
	{
		MapeoControlEmbotellado mapeo = null;
		
		mapeo = new MapeoControlEmbotellado();
		mapeo.setIdProgramacion(this.mapeoProgramacion.getIdProgramacion());
		mapeo.setIdControlEmbotellado(new Integer(this.numero.getValue()));
		mapeo.setFecha(this.fecha.getValue());
		mapeo.setSerie(this.cmbSerie.getValue().toString());
		mapeo.setArticulo(this.articulo.txtTexto.getValue());
		mapeo.setDescripcion(this.descripcion.getValue());
		mapeo.setObservaciones(this.txtObservaciones.getValue());
		
		if (this.cajas1.getValue()!=null && this.cajas1.getValue().length()>0) mapeo.setCajas1(new Integer(this.cajas1.getValue()));
		if (this.cajas2.getValue()!=null && this.cajas2.getValue().length()>0) mapeo.setCajas2(new Integer(this.cajas2.getValue()));
		if (this.cajas3.getValue()!=null && this.cajas3.getValue().length()>0) mapeo.setCajas3(new Integer(this.cajas3.getValue()));
		if (this.cajas4.getValue()!=null && this.cajas4.getValue().length()>0) mapeo.setCajas4(new Integer(this.cajas4.getValue()));
		if (this.cajas5.getValue()!=null && this.cajas5.getValue().length()>0) mapeo.setCajas5(new Integer(this.cajas5.getValue()));
		mapeo.setLote1(this.lotes1.getValue());
		mapeo.setLote2(this.lotes2.getValue());
		mapeo.setLote3(this.lotes3.getValue());
		mapeo.setLote4(this.lotes4.getValue());
		mapeo.setLote5(this.lotes5.getValue());
		
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
		{
			mapeo.setCantidad(new Integer(this.cantidad.getValue()));
		}
		else
		{
			mapeo.setCantidad(null);
		}
		mapeo.setTipoCalculo(this.cmbCalculos.getValue().toString());
		mapeo.setLitros(new Integer(this.litros.getValue()));
		if (this.articulo.txtTexto.getValue().contains("-")) 
			mapeo.setTipoProduccion("ETIQUETADO"); 
		else 
			if (descripcion.getValue().contains("S/E"))
					mapeo.setTipoProduccion("SEMITERMINADO");
			else
				mapeo.setTipoProduccion("EMBOTELLADO");
		
		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
		this.tipoEmbalaje=ces.obtenerEmbalajeArticulo(this.articulo.txtTexto.getValue().substring(0,7));		
		mapeo.setEmbalaje(this.tipoEmbalaje);
		
		return mapeo;
	}
	
	public boolean isHayGrid2() {
		return hayGrid2;
	}
	
	public void setHayGrid2(boolean hayGrid2) {
		this.hayGrid2 = hayGrid2;
	}

	public boolean isHayGrid3() {
		return hayGrid3;
	}
	
	public void setHayGrid3(boolean hayGrid3) {
		this.hayGrid3 = hayGrid3;
	}

	public void calcularLitros(Integer r_botellas, ArrayList<MapeoLineasControlEmbotellado> r_vector2)
	{
		for (int i=0;i<r_vector2.size();i++)
		{
			MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) r_vector2.get(i);
			
			if (mapeo.getArticulo().substring(0, 4).equals("0101"))
			{
				this.litros.setValue(mapeo.getUnidades().toString());
				break;
			}
		}
	}

	public void calcularBotellas()
	{
		Double cantidad = null;
		Integer cantidadCaja = null;
		Integer cajas = null;
		
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		cajas = 0;
		
		if (cajas1.getValue()!=null && cajas1.getValue().length()>0) cajas = cajas + new Integer(cajas1.getValue());
		if (cajas2.getValue()!=null && cajas2.getValue().length()>0) cajas = cajas + new Integer(cajas2.getValue());
		if (cajas3.getValue()!=null && cajas3.getValue().length()>0) cajas = cajas + new Integer(cajas3.getValue());
		if (cajas4.getValue()!=null && cajas4.getValue().length()>0) cajas = cajas + new Integer(cajas4.getValue());
		if (cajas5.getValue()!=null && cajas5.getValue().length()>0) cajas = cajas + new Integer(cajas5.getValue());
		
		cantidadCaja = ces.recogerTipoCaja(this.articulo.txtTexto.getValue().toString());
		cantidad = cantidadCaja.doubleValue()*cajas.doubleValue();
		
		this.cantidad.setValue(String.valueOf(cantidad.intValue()));
	}

	public void establecerSituacion (String r_modo)
	{
		this.litros.setEnabled(false);
		
		switch (r_modo)
		{
			case "Inicial":
			{
				
				this.fecha.setValue(null);
				
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(false);
				this.cmbCalculos.setEnabled(false);
				this.articulo.setEnabled(false);
				this.cantidad.setEnabled(false);
				this.cajas1.setEnabled(false);
				this.lotes1.setEnabled(false);
				this.cajas2.setEnabled(false);
				this.lotes2.setEnabled(false);
				this.cajas3.setEnabled(false);
				this.lotes3.setEnabled(false);
				this.cajas4.setEnabled(false);
				this.lotes4.setEnabled(false);
				this.cajas5.setEnabled(false);
				this.lotes5.setEnabled(false);
				cmbSerie.setEnabled(true);
				this.opcImprimir.setEnabled(false);
				this.opcAgregarComponente.setEnabled(false);
				this.opcAgregarLote.setEnabled(false);
				this.opcEliminar.setVisible(false);
				this.opcNuevo.setEnabled(false);
				this.opcNuevo.setCaption("Crear");
				this.opcSalir.setEnabled(true);
				break;
			}
			case "Validado":
			{
				
//				this.fecha.setValue(null);
				
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(false);
				this.cmbCalculos.setEnabled(false);
				this.articulo.setEnabled(false);
				this.cantidad.setEnabled(false);
				this.cajas1.setEnabled(false);
				this.lotes1.setEnabled(false);
				this.cajas2.setEnabled(false);
				this.lotes2.setEnabled(false);
				this.cajas3.setEnabled(false);
				this.lotes3.setEnabled(false);
				this.cajas4.setEnabled(false);
				this.lotes4.setEnabled(false);
				this.cajas5.setEnabled(false);
				this.lotes5.setEnabled(false);
				cmbSerie.setEnabled(true);
				this.opcImprimir.setEnabled(true);
				this.opcAgregarComponente.setEnabled(false);
				this.opcAgregarLote.setEnabled(false);
				this.opcEliminar.setVisible(false);
				this.opcNuevo.setCaption("Crear");
				this.opcNuevo.setEnabled(false);
				this.opcSalir.setEnabled(true);
				break;
			}
			case "Creacion":
			{

				this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(true);
				this.cmbCalculos.setEnabled(true);
				this.articulo.setEnabled(true);
				this.cantidad.setEnabled(true);
				this.cajas1.setEnabled(true);
				this.lotes1.setEnabled(true);
				this.cajas2.setEnabled(true);
				this.lotes2.setEnabled(true);
				this.cajas3.setEnabled(true);
				this.lotes3.setEnabled(true);
				this.cajas4.setEnabled(true);
				this.lotes4.setEnabled(true);
				this.cajas5.setEnabled(true);
				this.lotes5.setEnabled(true);
				cmbSerie.setEnabled(false);
				opcNuevo.setEnabled(true);
				opcNuevo.setCaption("Guardar");				
				opcSalir.setEnabled(true);
				opcEliminar.setVisible(false);
				opcImprimir.setEnabled(false);

				break;
			}
			case "Modificacion":
			{
				
				break;
			}
			case "Encontrado":
			{
				this.txtObservaciones.setEnabled(true);
				this.cmbCalculos.setEnabled(true);
				this.articulo.setEnabled(true);
				this.cantidad.setEnabled(true);
				this.cajas1.setEnabled(true);
				this.lotes1.setEnabled(true);
				this.cajas2.setEnabled(true);
				this.lotes2.setEnabled(true);
				this.cajas3.setEnabled(true);
				this.lotes3.setEnabled(true);
				this.cajas4.setEnabled(true);
				this.lotes4.setEnabled(true);
				this.cajas5.setEnabled(true);
				this.lotes5.setEnabled(true);
				
				opcEliminar.setVisible(true);
				this.opcNuevo.setEnabled(true);
				opcNuevo.setCaption("Guardar");
				opcImprimir.setEnabled(true);
				opcSalir.setEnabled(true);
				break;
			}
		}
		this.articulo.setEnabled(false);
	}

	private void limpiarCampos()
	{
		this.articulo.txtTexto.setValue("");
		this.txtObservaciones.setValue("");
		this.cajas1.setValue("");
		this.cajas2.setValue("");
		this.cajas3.setValue("");
		this.cajas4.setValue("");
		this.cajas5.setValue("");
		this.lotes1.setValue("");
		this.lotes2.setValue("");
		this.lotes3.setValue("");
		this.lotes4.setValue("");
		this.lotes5.setValue("");
		this.cantidad.setValue("");
		this.litros.setValue("0");
		this.generarGrid();
	}

	private void rellenarEntradasDatos(MapeoControlEmbotellado r_mapeo)
	{
		this.fecha.setValue(r_mapeo.getFecha());
//		this.numero.setValue(r_mapeo.getIdControlEmbotellado().toString());
		this.articulo.txtTexto.setValue(r_mapeo.getArticulo());
		this.descripcion.setValue(r_mapeo.getDescripcion());
		this.cmbSerie.setValue(r_mapeo.getSerie());
		
		this.cajas1.setValue(r_mapeo.getCajas1().toString());
		this.cajas2.setValue(r_mapeo.getCajas2().toString());
		this.cajas3.setValue(r_mapeo.getCajas3().toString());
		this.cajas4.setValue(r_mapeo.getCajas4().toString());
		this.cajas5.setValue(r_mapeo.getCajas5().toString());
		this.lotes1.setValue(r_mapeo.getLote1());
		this.lotes2.setValue(r_mapeo.getLote2());
		this.lotes3.setValue(r_mapeo.getLote3());
		this.lotes4.setValue(r_mapeo.getLote4());
		this.lotes5.setValue(r_mapeo.getLote5());
		this.cantidad.setValue(r_mapeo.getCantidad().toString());
		this.litros.setValue(r_mapeo.getLitros().toString());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
//		this.mapeoControl=r_mapeo;
	}

	
	public void eliminarRegistro() 
	{
		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(CurrentUser.get());		
			
		boolean rdo = ces.eliminar(cmbSerie.getValue().toString(), new Integer(this.numero.getValue()),null);

		if (rdo) Notificaciones.getInstance().mensajeInformativo("Registro eliminado correctamente");
		else Notificaciones.getInstance().mensajeInformativo("No se ha podido eliminar el registro");
		
		limpiarCampos();
		establecerSituacion("Inicial");
		limpiarCampos();
	}
	
	private Integer obtenerSiguienteRollo()
	{
		Integer resultado=null;
		ArrayList<MapeoNumeracionConsejo> vectorNumeracionConsejo =  (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
		if (vectorNumeracionConsejo==null)
			resultado = 1;
		else
			resultado = vectorNumeracionConsejo.size() + 1;
		
		return resultado;
	}
	
	private void recalcularEscandallo()
	{
		boolean eliminar = true;
		ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
		if (vectorEscandallo!=null)
		{
			for (int i=0;i<vectorEscandallo.size();i++)
			{
				MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(i);
				
				if (mapeo.getMermas()!=0)
				{									
					eliminar=false;
				}
			}
		}
		if (eliminar)
		{
			if (isHayGrid2())
			{
				grid2.removeAllColumns();			
				principal.removeComponent(grid2);
				grid2=null;
			}
			generarGrid();
		}
	}
	
	public void destructor()
	{
	}

}

