package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.GridViewRefresh;
import borsao.e_borsao.ClasesPropias.LecturaProperties;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.RutinasFicheros;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.ClasesPropias.TextFieldAyuda;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.ControlEmbotelladoConsejoGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.ControlEmbotelladoEscandalloGrid;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoLineasControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoNumeracionConsejo;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaProduccionDiaria.server.consultaProduccionDiariaServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * A view for performing create-read-update-delete operations on products.
 *
 * See also {@link SampleCrudLogic} for fetching the data, the actual CRUD
 * operations and controlling the view based on events from outside.
 */
public class ControlEmbotelladoView extends GridViewRefresh {

	public static final String VIEW_NAME = "Control Embotelladora";
	public consultaControlEmbotelladoServer caps =null;
	public consultaProduccionDiariaServer cps =null;
	public TextField cantidad= null;
	public TextField litros= null;
	public TextArea txtObservaciones=null;
	public TextFieldAyuda articulo = null;
	public ComboBox cmbSerie = null;
	public TextField numero= null;
	public DateField fecha = null;
	public Label descripcion= null;
	public ComboBox cmbCalculos = null;
    public Button opcAgregarComponente = null;
    public Button opcAgregarLote = null;
    public boolean buscando;
    public Integer numeradorSeleccionado = null;
    public String areaSeleccionada = null;
    
    /*
     * meter boton validacion
     * metere check de procesado en GREENsys
     */
    public Button opcValidacion = null;
    public Label procesado = null;
    public Label lblIdGreensys = null;
    public Label lblLeidoGreensys = null;
	
	public TextField cajas1= null;
	public TextField lotes1= null;
	public TextField cajas2= null;
	public TextField lotes2= null;
	public TextField cajas3= null;
	public TextField lotes3= null;
	public TextField cajas4= null;
	public TextField lotes4= null;
	public TextField cajas5= null;
	public TextField lotes5= null;
	
	public Grid grid2 = null;
	public Grid grid3 = null;
	public String permisos = null;
	
	private VerticalLayout panelCabecera = null;
	private HorizontalLayout panelGenerales = null;
	private HorizontalLayout panelArticulo = null;
	private HorizontalLayout panelCajas = null;
	private HorizontalLayout horGrid = null;
	private VerticalLayout panGrid2 = null;
	private VerticalLayout panGrid3 = null;
	private final String titulo = "Control de la Embotelladora: PRODUCTO TERMINADO";
	private final int intervaloRefresco = 15*60*1000; //milisegundos
	private final boolean autoSincronizacion = false;
	private final boolean soloConsulta = false;
	private final boolean conFormulario = false;
	private boolean hayGrid2 = false;
	private boolean hayGrid3 = false;
	private ArrayList<MapeoLineasControlEmbotellado> r_vector2 =null;
	private ArrayList<MapeoNumeracionConsejo> r_vector3 =null;
	private MapeoControlEmbotellado mapeoControl = null;
	private Integer tipoEmbalaje = null;
    /*
     * METODOS PROPIOS PERO GENERICOS
     */
    
    public ControlEmbotelladoView()
    {
    	setConFormulario(this.conFormulario);
    }

    public void cargarPantalla() 
    {
    	consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    	this.permisos = ces.comprobarAccesos();
    	
    	setActivarSync(this.autoSincronizacion,this.intervaloRefresco);
    	setSoloConsulta(this.soloConsulta);

    	this.topLayoutL.addComponent(this.opcBuscar);
    	this.opcImprimir.setVisible(true);
    	
    	this.panelCabecera = new VerticalLayout();
    	this.panelCabecera.setSizeFull();
    	this.panelCabecera.setSpacing(true);
    	this.panelGenerales = new HorizontalLayout();
    	this.panelGenerales.setSpacing(true);
    	this.panelGenerales.setSizeFull();
    	
    	lblTitulo.setValue(this.titulo);//, ContentMode.HTML);

    	this.fecha = new DateField("Fecha");
    	this.fecha.setEnabled(false);
    	this.fecha.setDateFormat("dd/MM/yyyy");
    	this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));

    	this.cmbSerie=new ComboBox("Linea");
		this.cmbSerie.addItem("EMBOTELLADO");
		this.cmbSerie.addItem("ENVASADO");
		this.cmbSerie.addItem("BIB");
		this.cmbSerie.setInvalidAllowed(false);
		this.cmbSerie.setNewItemsAllowed(false);
		this.cmbSerie.setNullSelectionAllowed(false);
    	
		this.numero=new TextField();
		this.numero.setEnabled(true);
		this.numero.setWidth("120px");
		this.numero.addStyleName("rightAligned");
		this.numero.setCaption("Numerador");
		
		this.cmbCalculos=new ComboBox("Cálculo por Botellas o Cajas?");
		this.cmbCalculos.addItem("BOTELLAS");
		this.cmbCalculos.addItem("CAJAS");
		this.cmbCalculos.setValue("BOTELLAS");
		this.cmbCalculos.setInvalidAllowed(false);
		this.cmbCalculos.setNewItemsAllowed(false);
		this.cmbCalculos.setNullSelectionAllowed(false);
    	
		this.litros=new TextField();
		this.litros.setEnabled(false);
		this.litros.setCaption("Litros");
		this.litros.addStyleName("rightAligned");
		this.litros.setValue("0");

		this.txtObservaciones=new TextArea();
		this.txtObservaciones.setEnabled(false);
		this.txtObservaciones.setWidth("500px");
		this.txtObservaciones.setCaption("Observaciones");
		
		this.panelGenerales.addComponent(this.fecha);
		this.panelGenerales.addComponent(this.cmbSerie);
		this.panelGenerales.addComponent(this.numero);
		this.panelGenerales.addComponent(this.cmbCalculos);
		this.panelGenerales.addComponent(this.litros);
		this.panelGenerales.addComponent(this.txtObservaciones);
		
    	this.panelArticulo = new HorizontalLayout();
    	this.panelArticulo.setSpacing(true);

		this.articulo= new TextFieldAyuda("Articulo","");
		
		this.descripcion = new Label();
		this.descripcion.setWidth("375px");

		this.cantidad=new TextField();
		this.cantidad.setEnabled(true);
		this.cantidad.setCaption("A Fabricar");
		this.cantidad.addStyleName("rightAligned");
		this.cantidad.setValue("0");
		
    	this.opcValidacion= new Button("Validar");
    	this.opcValidacion.addStyleName(ValoTheme.BUTTON_FRIENDLY);
    	this.opcValidacion.setIcon(FontAwesome.CHECK_CIRCLE_O);
//    	this.opcValidacion.setVisible(true);
    	
    	this.lblIdGreensys = new Label();
		this.lblIdGreensys.setWidth("175px");

    	this.lblLeidoGreensys = new Label();
		this.lblLeidoGreensys.setWidth("175px");
		this.lblLeidoGreensys.setCaption("Producción Leida");
		this.lblLeidoGreensys.setValue("0");

		this.panelArticulo.addComponent(this.articulo);
		this.panelArticulo.addComponent(this.descripcion);
		this.panelArticulo.setComponentAlignment(this.descripcion, Alignment.MIDDLE_LEFT);
		this.panelArticulo.addComponent(this.cantidad);
		this.panelArticulo.addComponent(this.opcValidacion);
		this.panelArticulo.setComponentAlignment(this.opcValidacion, Alignment.BOTTOM_RIGHT);
		this.panelArticulo.addComponent(this.lblIdGreensys);		
		this.panelArticulo.setComponentAlignment(this.lblIdGreensys, Alignment.BOTTOM_RIGHT);
		if (this.permisos.equals("80") || this.permisos.equals("99"))
		{
			this.panelArticulo.addComponent(this.lblLeidoGreensys);		
			this.panelArticulo.setComponentAlignment(this.lblLeidoGreensys, Alignment.BOTTOM_RIGHT);
		}
		
    	this.panelCajas = new HorizontalLayout();
    	this.panelCajas.setSpacing(true);
    	
    	this.cajas1=new TextField();
		this.cajas1.setEnabled(true);
		this.cajas1.setCaption("UDS.");
		this.cajas1.setWidth("75px");
		this.cajas1.addStyleName("rightAligned");
		this.cajas1.setValue("");

    	this.lotes1=new TextField();
		this.lotes1.setEnabled(true);
		this.lotes1.setCaption("LOTE");
		this.lotes1.setWidth("125px");
		this.lotes1.setValue("");

    	this.cajas2=new TextField();
		this.cajas2.setEnabled(true);
		this.cajas2.setCaption("UDS.");
		this.cajas2.setWidth("75px");
		this.cajas2.addStyleName("rightAligned");
		this.cajas2.setValue("");
		
		this.lotes2=new TextField();
		this.lotes2.setEnabled(true);
		this.lotes2.setCaption("LOTE");
		this.lotes2.setWidth("125px");
		this.lotes2.setValue("");

    	this.cajas3=new TextField();
		this.cajas3.setEnabled(true);
		this.cajas3.setCaption("UDS.");
		this.cajas3.setWidth("75px");
		this.cajas3.addStyleName("rightAligned");
		this.cajas3.setValue("");

		this.lotes3=new TextField();
		this.lotes3.setEnabled(true);
		this.lotes3.setCaption("LOTE");
		this.lotes3.setWidth("125px");
		this.lotes3.setValue("");
		
    	this.cajas4=new TextField();
		this.cajas4.setEnabled(true);
		this.cajas4.setCaption("UDS.");
		this.cajas4.setWidth("75px");
		this.cajas4.addStyleName("rightAligned");
		this.cajas4.setValue("");

		this.lotes4=new TextField();
		this.lotes4.setEnabled(true);
		this.lotes4.setCaption("LOTE");
		this.lotes4.setWidth("125px");
		this.lotes4.setValue("");

    	this.cajas5=new TextField();
		this.cajas5.setEnabled(true);
		this.cajas5.setCaption("UDS.");
		this.cajas5.setWidth("75px");
		this.cajas5.addStyleName("rightAligned");
		this.cajas5.setValue("");

		this.lotes5=new TextField();
		this.lotes5.setEnabled(true);
		this.lotes5.setCaption("LOTE");
		this.lotes5.setWidth("125px");
		this.lotes5.setValue("");

		this.panelCajas.addComponent(this.cajas1);
		this.panelCajas.addComponent(this.lotes1);
		this.panelCajas.addComponent(this.cajas2);
		this.panelCajas.addComponent(this.lotes2);
		this.panelCajas.addComponent(this.cajas3);
		this.panelCajas.addComponent(this.lotes3);
		this.panelCajas.addComponent(this.cajas4);
		this.panelCajas.addComponent(this.lotes4);
		this.panelCajas.addComponent(this.cajas5);
		this.panelCajas.addComponent(this.lotes5);
		
    	this.opcAgregarComponente= new Button("Agregar");
    	this.opcAgregarComponente.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcAgregarComponente.setIcon(FontAwesome.PLUS_CIRCLE);
    	
    	this.opcAgregarLote= new Button("Numeración Etiquetas");
    	this.opcAgregarLote.addStyleName(ValoTheme.BUTTON_PRIMARY);
    	this.opcAgregarLote.setIcon(FontAwesome.PLUS_CIRCLE);
    	
    	horGrid = new HorizontalLayout();
    	horGrid.setSizeFull();
    	
    	this.panelCabecera.addComponent(this.panelGenerales);
		this.panelCabecera.addComponent(this.panelArticulo);
    	this.panelCabecera.addComponent(this.panelCajas);
    	this.cabLayout.addComponent(this.panelCabecera);
    	this.barAndGridLayout.setMargin(true);
    	
    	this.establecerSituacion("Inicial");
    	
		this.cargarListeners();
		
		
		if (eBorsao.get().accessControl.getNombre().contains("Embotelladora"))
		{
			this.cmbSerie.setValue("EMBOTELLADO");

		}
		else if (eBorsao.get().accessControl.getNombre().contains("Envasadora"))
		{
			this.cmbSerie.setValue("ENVASADO");
		}
		else if (eBorsao.get().accessControl.getNombre().contains("bib"))
		{
			this.cmbSerie.setValue("BIB");
		}
		else
		{
		}
		
		this.generarGrid();
    }

    public void generarGrid()
    {
    	Integer cantidad = null;
    	ArrayList<MapeoNumeracionConsejo> vectorConsejo = null;
    	
    	caps = new consultaControlEmbotelladoServer(CurrentUser.get());
    	
    	if (this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0)
    	{
        	if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
        	{
        		cantidad =  RutinasNumericas.formatearIntegerDeESP(this.cantidad.getValue());
        	}
        	else
        	{
        		cantidad = 0;
        	}

    		r_vector2=this.caps.generarDatosEscandallo(this.articulo.txtTexto.getValue(),cantidad);
    		
    		if (((ControlEmbotelladoConsejoGrid) grid3)!=null) 
        		vectorConsejo = (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
    	}
    	else
    	{
        	r_vector2 = new ArrayList<MapeoLineasControlEmbotellado>();
        	vectorConsejo = new ArrayList<MapeoNumeracionConsejo>();
    	}
    	
    	this.presentarGrid(r_vector2,vectorConsejo);
    	this.calcularLitros(cantidad,r_vector2);
    	
    	if (horGrid!=null)
    	{
    		this.barAndGridLayout.addComponent(horGrid);
    		this.barAndGridLayout.setExpandRatio(horGrid, 1);
    		this.barAndGridLayout.setSpacing(true);
    		this.pp.setSizeUndefined();
    		this.pp.setWidth("100%");
    	}

    }
    
    public void filaSeleccionada(Object r_fila)
    {
    }
    
    private void presentarGrid(ArrayList<MapeoLineasControlEmbotellado> r_vector2, ArrayList<MapeoNumeracionConsejo> r_vector3)
    {

		if (horGrid!=null) horGrid.removeAllComponents();
		
		grid2 = new ControlEmbotelladoEscandalloGrid(r_vector2,this);
			if (((ControlEmbotelladoEscandalloGrid) grid2).vector==null || ((ControlEmbotelladoEscandalloGrid) grid2).vector.size()==0)
	    	{
	    		setHayGrid2(false);
	    	}
	    	else
	    	{
	    		setHayGrid2(true);
	    		grid2.setEditorEnabled(true);
	    	}
			
			panGrid2 = new VerticalLayout();
//			panGrid2.setSizeFull();
			
			panGrid2.addComponent(this.opcAgregarComponente);
			panGrid2.addComponent(grid2);
		
		horGrid.addComponent(panGrid2);
		
		grid3 = new ControlEmbotelladoConsejoGrid(r_vector3,this);
		
			if (((ControlEmbotelladoConsejoGrid) grid3).vector==null || ((ControlEmbotelladoConsejoGrid) grid3).vector.size()==0)
	    	{
	    		setHayGrid3(false);
	    	}
	    	else
	    	{
	    		setHayGrid3(true);
	    		grid3.setEditorEnabled(true);
	    	}
			
			panGrid3 = new VerticalLayout();
//			panGrid3.setSizeFull();
			
			panGrid3.addComponent(this.opcAgregarLote);
			panGrid3.addComponent(grid3);
		
		
		horGrid.addComponent(panGrid3);
		
		if (((ControlEmbotelladoEscandalloGrid) grid2).vector.size()>0)
		{
			this.opcAgregarLote.setEnabled(true);
			this.opcAgregarComponente.setEnabled(true);
		}
    }
    
    private void cargarListeners()
    {

    	this.cmbSerie.addValueChangeListener(new ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (!isBuscando())
				{
					if (opcNuevo.getCaption().equals("Crear"))
					{
						limpiarCampos();
						if (isHayGrid2())
						{
							grid2.removeAllColumns();			
							panGrid2.removeComponent(grid2);
							grid2=null;						
						}
						if (isHayGrid3())
						{
							grid3.removeAllColumns();			
							panGrid3.removeComponent(grid3);
							grid3=null;						
						}
					}
					
					consultaContadoresEjercicioServer cces = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
					Long contador = cces.recuperarContador(new Integer(RutinasFechas.añoActualYYYY()), "PRODUCCION", cmbSerie.getValue().toString());
					numero.setValue(contador.toString());
				}
			}
		});
    	
    	this.opcAgregarComponente.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid2() && grid2.getEditedItemId()==null)
		    	{
		    		MapeoLineasControlEmbotellado mapeo =new MapeoLineasControlEmbotellado();
		    		mapeo.setArticulo("");
					mapeo.setDescripcion("");
					mapeo.setUnidades(0);
					mapeo.setMermas(0);
					mapeo.setOrigen(1);
					grid2.getContainerDataSource().addItem(mapeo);    					
					grid2.editItem(mapeo);
		    	}
			}
    	});

    	this.opcAgregarLote.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid2() && grid3.getEditedItemId()==null)
		    	{
		    		MapeoNumeracionConsejo mapeo =new MapeoNumeracionConsejo();
		    		mapeo.setNumeroRollo(obtenerSiguienteRollo());
		    		mapeo.setSerie("");
					mapeo.setDesdeNumeracion(0);
					mapeo.setHastaNumeracion(0);
					grid3.getContainerDataSource().addItem(mapeo);    	
					grid3.editItem(mapeo);
		    	}
			}
    	});

    	this.opcValidacion.addClickListener(new ClickListener() {
    		@Override
    		public void buttonClick(ClickEvent event) {
    			if (isHayGrid2() && grid2.getEditedItemId()==null)
		    	{
    				/*
    				 * Llamar a crear datos en greensys
    				 * co nlos datos de los mapeos
    				 */
    				MapeoControlEmbotellado mapeoControlEmbotellado =  rellenarMapeo();
    				ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
    				consultaControlEmbotelladoServer cces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    				
					Integer idGreensys = cces.obtenerSiguienteGreensys();
					mapeoControlEmbotellado.setIdGreensys(idGreensys);
					lblIdGreensys.setValue(idGreensys.toString());
					String resultado = cces.guardarControlEmbotelladoGreensys(mapeoControlEmbotellado, vectorEscandallo);
				
    				if (resultado==null)
    				{
    					consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
    					resultado = ces.actualizarControlEmbotellado(mapeoControlEmbotellado);
    					
						establecerSituacion("Validado");
    				}
    				else
    				{
    					Notificaciones.getInstance().mensajeError(resultado);
    				}
		    	}
			}
    	});
    	
		this.numero.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (numero.getValue()!=null && numero.getValue().length()>0 && !isBuscando())// && opcNuevo.getCaption().equals("Crear")  ) 
				{
					limpiarCampos();
					consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
					mapeoControl = ces.recuperarControlEmbotellado(cmbSerie.getValue().toString(), numero.getValue());
					if (isHayGrid2())
					{
						grid2.removeAllColumns();			
						panGrid2.removeComponent(grid2);
						grid2=null;						
					}
					if (isHayGrid3())
					{
						grid3.removeAllColumns();			
						panGrid3.removeComponent(grid3);
						grid3=null;						
					}
					
					if (mapeoControl!=null)
					{
						rellenarEntradasDatos(mapeoControl);
						r_vector2 = ces.recuperarEscandallo(cmbSerie.getValue().toString(), numero.getValue());
						r_vector3 = ces.recuperarNumeracionConsejo(cmbSerie.getValue().toString(), numero.getValue());
						presentarGrid(r_vector2, r_vector3);
						establecerSituacion("Encontrado");
					}
					else
					{
						establecerSituacion("Inicial");
						newForm();
					}
				}
//				else if (numero.getValue()!=null && numero.getValue().length()>0 && opcNuevo.getCaption().equals("Guardar") )
//				{
//					consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
//					mapeoControl = ces.recuperarControlEmbotellado(numero.getValue());
//					if (isHayGrid2())
//					{
//						grid2.removeAllColumns();			
//						panGrid2.removeComponent(grid2);
//						grid2=null;						
//					}
//					if (isHayGrid3())
//					{
//						grid3.removeAllColumns();			
//						panGrid3.removeComponent(grid3);
//						grid3=null;						
//					}
//					
//					limpiarCampos();
//					
//					if (mapeoControl!=null)
//					{
//						rellenarEntradasDatos(mapeoControl);
//						r_vector2 = ces.recuperarEscandallo(numero.getValue());
//						r_vector3 = ces.recuperarNumeracionConsejo(numero.getValue());
//						presentarGrid(r_vector2, r_vector3);
//						establecerSituacion("Encontrado");
//					}
//					else
//					{
//						establecerSituacion("Inicial");
//						newForm();
//					}
//				}
				else
				{
					limpiarCampos();
					establecerSituacion("Inicial");
					newForm();
				}
			}
		});

    	this.articulo.txtTexto.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) 
			{
				if (chequeo())
				{
					if (opcNuevo.getCaption().equals("Guardar") && articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 ) 
					{
						consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
						cps = new consultaProduccionDiariaServer(CurrentUser.get());
						
						descripcion.setValue(ces.obtenerDescripcionArticulo(articulo.txtTexto.getValue().substring(0,7)));
						tipoEmbalaje=ces.obtenerEmbalajeArticulo(articulo.txtTexto.getValue().substring(0,7));
						if (permisos.equals("80") || permisos.equals("99"))
						{
							Double prod = cps.datosTotalArticuloDia(RutinasFechas.convertirDateToString(fecha.getValue()), articulo.txtTexto.getValue().substring(0,7));
							lblLeidoGreensys.setValue(prod.toString());
						}
						
						if (mapeoControl==null)
						{
							if (isHayGrid2())
							{
								grid2.removeAllColumns();			
								panGrid2.removeComponent(grid2);
								grid2=null;
							}
							generarGrid();
						}
					}
				}
				else
				{
					Notificaciones.getInstance().mensajeError("El articulo no crresponde a la linea de producción seleccioanda.");
					articulo.txtTexto.setValue("");
					articulo.txtTexto.focus();
				}
			}
		});

		this.cantidad.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				
				if (cmbCalculos.getValue().toString().toUpperCase().equals("BOTELLAS"))
				{
					if (articulo.txtTexto.getValue()!=null && articulo.txtTexto.getValue().length()>0 && cantidad.getValue()!=null && cantidad.getValue().length()>0) 
					{
						boolean eliminar = true;
						ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
						if (vectorEscandallo!=null)
						{
							for (int i=0;i<vectorEscandallo.size();i++)
							{
								MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(i);
								
								if (mapeo.getMermas()!=0)
								{									
									eliminar=false;
								}
							}
						}
						if (eliminar)
						{
							if (isHayGrid2())
							{
								grid2.removeAllColumns();			
								panGrid2.removeComponent(grid2);
								grid2=null;
							}
							generarGrid();
						}
					}
				}
			}
		}); 
		
		this.cajas1.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas1.getValue()!=null && cajas1.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();							
					}
				}
			}
		}); 

		this.cajas2.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas2.getValue()!=null && cajas2.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		
		this.cajas3.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas3.getValue()!=null && cajas3.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		
		this.cajas4.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas4.getValue()!=null && cajas4.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
		this.cajas5.addValueChangeListener(new Property.ValueChangeListener() {
			
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (cmbCalculos.getValue().toString().toUpperCase().equals("CAJAS"))
				{
					if (cajas5.getValue()!=null && cajas5.getValue().length()>0) 
					{
						calcularBotellas();
						recalcularEscandallo();
					}
				}
			}
		}); 
    }
    

	@Override
	public void reestablecerPantalla() {
		
	}
	
	@Override
	public void print() 
	{
		String pdfGenerado = null;
    	
    	/*
    	 * generacion del pdf
    	 * el proceso de generacion devolverá la ruta y el nombre del fichero generado
    	 */
    	consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
    	pdfGenerado = ces.generarInforme(cmbSerie.getValue().toString(),new Integer(this.numero.getValue()));
    	
    	RutinasFicheros.abrirPdfGenerado(getUI(), LecturaProperties.basePdfPath, pdfGenerado, true);
		
		pdfGenerado = null;
		botonesGenerales(true);
		establecerSituacion("Encontrado");
	}
	
	@Override
	public void generarGrid(HashMap<String, String> opcionesEscogidas) {
		
	}
	
	@Override
	public void newForm() {
		
		if (opcNuevo.getCaption().equals("Crear"))
		{
			this.limpiarCampos();
//			consultaContadoresServer ccs = consultaContadoresServer.getInstance(CurrentUser.get());
//			Integer numerador = ccs.recuperarContador("CtrlEmbotelladora");
//			this.numero.setValue(numerador.toString());
//			articulo.txtTexto.focus();
			
			this.numero.focus();
			this.establecerSituacion("Creacion");
		}
		else if (opcNuevo.getCaption().equals("Guardar"))
		{
			consultaControlEmbotelladoServer cces = new consultaControlEmbotelladoServer(CurrentUser.get());
			/*
			 * 1.- Valores entradas de datos
			 * 2.- vector del escandallo
			 * 3.- vector de los rollos
			 * 
			 * 4.- pasarlo todo para guardar
			 * 
			 */
			Integer cant = null;
			MapeoControlEmbotellado mapeoControlEmbotellado =  this.rellenarMapeo();
			
			caps = new consultaControlEmbotelladoServer(CurrentUser.get());			
        	if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
        	{
        		cant =  RutinasNumericas.formatearIntegerDeESP(this.cantidad.getValue());
        	}
        	else
        	{
        		cant = 0;
        	}
			r_vector2=caps.generarDatosEscandallo(this.articulo.txtTexto.getValue(),cant);
			
			ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
			for (int i =0; i<r_vector2.size(); i++)
			{
				MapeoLineasControlEmbotellado mapeoNuevo = (MapeoLineasControlEmbotellado) r_vector2.get(i);
				
				for (int j=0;j<vectorEscandallo.size();j++)
				{
					MapeoLineasControlEmbotellado mapeoViejo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(j);
					if (mapeoNuevo.getArticulo()==mapeoViejo.getArticulo())
					{
						mapeoNuevo.setOrden(mapeoViejo.getOrden());
						mapeoNuevo.setMermas(mapeoViejo.getMermas());
						vectorEscandallo.remove(j);
						vectorEscandallo.add(j, mapeoNuevo);
						break;
					}
				}
			}
			
			ArrayList<MapeoNumeracionConsejo> vectorNumeracionConsejo =  (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
			
			
			boolean resultado = cces.guardarDatos(mapeoControlEmbotellado, vectorEscandallo, vectorNumeracionConsejo);
			
			if (resultado)
			{
				botonesGenerales(true);
				establecerSituacion("Encontrado");
			}
			else
			{
				Notificaciones.getInstance().mensajeAdvertencia("Revisa los datos");
			}
			
		}
	}
	
	private MapeoControlEmbotellado rellenarMapeo()
	{
		MapeoControlEmbotellado mapeo = null;
		
		mapeo = new MapeoControlEmbotellado();
		mapeo.setIdControlEmbotellado(new Integer(this.numero.getValue()));
		
		mapeo.setSerie(this.cmbSerie.getValue().toString());
		
		mapeo.setFecha(this.fecha.getValue());
		mapeo.setArticulo(this.articulo.txtTexto.getValue());
		mapeo.setDescripcion(this.descripcion.getValue());
		mapeo.setObservaciones(this.txtObservaciones.getValue());
		
		if (this.cajas1.getValue()!=null && this.cajas1.getValue().length()>0) mapeo.setCajas1(new Integer(this.cajas1.getValue()));
		if (this.cajas2.getValue()!=null && this.cajas2.getValue().length()>0) mapeo.setCajas2(new Integer(this.cajas2.getValue()));
		if (this.cajas3.getValue()!=null && this.cajas3.getValue().length()>0) mapeo.setCajas3(new Integer(this.cajas3.getValue()));
		if (this.cajas4.getValue()!=null && this.cajas4.getValue().length()>0) mapeo.setCajas4(new Integer(this.cajas4.getValue()));
		if (this.cajas5.getValue()!=null && this.cajas5.getValue().length()>0) mapeo.setCajas5(new Integer(this.cajas5.getValue()));
		mapeo.setLote1(this.lotes1.getValue());
		mapeo.setLote2(this.lotes2.getValue());
		mapeo.setLote3(this.lotes3.getValue());
		mapeo.setLote4(this.lotes4.getValue());
		mapeo.setLote5(this.lotes5.getValue());
		
		if (this.cantidad.getValue()!=null && this.cantidad.getValue().length()>0)
		{
			mapeo.setCantidad(new Integer(this.cantidad.getValue()));
		}
		else
		{
			mapeo.setCantidad(null);
		}
		mapeo.setTipoCalculo(this.cmbCalculos.getValue().toString());
		mapeo.setLitros(new Integer(this.litros.getValue()));
		if (this.articulo.txtTexto.getValue().contains("-")) 
			mapeo.setTipoProduccion("ETIQUETADO"); 
		else 
			if (descripcion.getValue().contains("S/E"))
					mapeo.setTipoProduccion("SEMITERMINADO");
			else
				mapeo.setTipoProduccion("EMBOTELLADO");
		
		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
		this.tipoEmbalaje=ces.obtenerEmbalajeArticulo(this.articulo.txtTexto.getValue().substring(0,7));		
		mapeo.setEmbalaje(this.tipoEmbalaje);
		
		if (this.lblIdGreensys.getCaption()!=null && this.lblIdGreensys.getCaption().length()>0) mapeo.setIdGreensys(new Integer(this.lblIdGreensys.getCaption().toString()));
		return mapeo;
	}
	
	@Override
	public void verForm(boolean r_busqueda) 
	{
		
		/*
		 * Abrimos pantalla peticion busqueda controles de embotellado
		 * Tendra un grid con los encontrados y devolvera el prd_idcontrolembotellado que hemos seleccionado
		 * con este rellenaremos el campo numerador y devolveremos el registro 
		 */
		this.establecerSituacion("Buscar");
		
		/*
		 * llamo al filtro
		 */
		
		pantallaAyudaControlEmb vt = new pantallaAyudaControlEmb(this, "Busqueda Controles Embotellado ");
		getUI().addWindow(vt);
		

	}
	
	public boolean isHayGrid2() {
		return hayGrid2;
	}
	
	public void setHayGrid2(boolean hayGrid2) {
		this.hayGrid2 = hayGrid2;
	}

	public boolean isHayGrid3() {
		return hayGrid3;
	}
	
	public void setHayGrid3(boolean hayGrid3) {
		this.hayGrid3 = hayGrid3;
	}

	@Override
	public void mostrarFilas(Collection<Object> r_filas) {
		
	}
	
	public void calcularLitros(Integer r_botellas, ArrayList<MapeoLineasControlEmbotellado> r_vector2)
	{
		for (int i=0;i<r_vector2.size();i++)
		{
			MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) r_vector2.get(i);
			
			if (mapeo.getArticulo().substring(0, 4).equals("0101"))
			{
				this.litros.setValue(mapeo.getUnidades().toString());
				break;
			}
		}
	}

	public void calcularBotellas()
	{
		Double cantidad = null;
		Integer cantidadCaja = null;
		Integer cajas = null;
		
		consultaEscandalloServer ces = new consultaEscandalloServer(CurrentUser.get());
		cajas = 0;
		
		if (cajas1.getValue()!=null && cajas1.getValue().length()>0) cajas = cajas + new Integer(cajas1.getValue());
		if (cajas2.getValue()!=null && cajas2.getValue().length()>0) cajas = cajas + new Integer(cajas2.getValue());
		if (cajas3.getValue()!=null && cajas3.getValue().length()>0) cajas = cajas + new Integer(cajas3.getValue());
		if (cajas4.getValue()!=null && cajas4.getValue().length()>0) cajas = cajas + new Integer(cajas4.getValue());
		if (cajas5.getValue()!=null && cajas5.getValue().length()>0) cajas = cajas + new Integer(cajas5.getValue());
		
		cantidadCaja = ces.recogerTipoCaja(this.articulo.txtTexto.getValue().toString());
		cantidad = cantidadCaja.doubleValue()*cajas.doubleValue();
		
		this.cantidad.setValue(String.valueOf(cantidad.intValue()));
	}

	public void establecerSituacion (String r_modo)
	{
		this.litros.setEnabled(false);
		
		switch (r_modo)
		{
			case "Inicial":
			{
				
				this.fecha.setValue(null);
				this.setBuscando(false);
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(false);
				this.cmbCalculos.setEnabled(false);
				this.articulo.setEnabled(false);
				this.cantidad.setEnabled(false);
				this.cajas1.setEnabled(false);
				this.lotes1.setEnabled(false);
				this.cajas2.setEnabled(false);
				this.lotes2.setEnabled(false);
				this.cajas3.setEnabled(false);
				this.lotes3.setEnabled(false);
				this.cajas4.setEnabled(false);
				this.lotes4.setEnabled(false);
				this.cajas5.setEnabled(false);
				this.lotes5.setEnabled(false);
				
				this.opcBuscar.setEnabled(true);
				this.opcBuscar.setVisible(true);
				this.opcImprimir.setEnabled(false);
				this.opcAgregarComponente.setEnabled(false);
				this.opcAgregarLote.setEnabled(false);
				this.opcValidacion.setEnabled(false);
				this.opcEliminar.setVisible(false);
				this.opcNuevo.setEnabled(true);
				this.opcNuevo.setCaption("Crear");
				this.opcSalir.setEnabled(true);
				cmbSerie.setEnabled(true);
				break;
			}
			case "Validado":
			{
				
//				this.fecha.setValue(null);
				
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(false);
				this.cmbCalculos.setEnabled(false);
				this.articulo.setEnabled(false);
				this.cantidad.setEnabled(false);
				this.cajas1.setEnabled(false);
				this.lotes1.setEnabled(false);
				this.cajas2.setEnabled(false);
				this.lotes2.setEnabled(false);
				this.cajas3.setEnabled(false);
				this.lotes3.setEnabled(false);
				this.cajas4.setEnabled(false);
				this.lotes4.setEnabled(false);
				this.cajas5.setEnabled(false);
				this.lotes5.setEnabled(false);
				
				this.opcImprimir.setEnabled(true);
				this.opcAgregarComponente.setEnabled(false);
				this.opcAgregarLote.setEnabled(false);
				this.opcValidacion.setEnabled(false);
				this.opcEliminar.setVisible(false);
				this.opcNuevo.setCaption("Crear");
				cmbSerie.setEnabled(true);
				this.opcSalir.setEnabled(true);
				break;
			}
			case "Creacion":
			{

				this.fecha.setEnabled(true);
				this.setBuscando(false);
				this.fecha.setValue(RutinasFechas.conversionDeString(RutinasFechas.fechaHoy()));
//				this.numero.setValue(null);
				this.txtObservaciones.setEnabled(true);
				this.cmbCalculos.setEnabled(true);
				this.articulo.setEnabled(true);
				this.cantidad.setEnabled(true);
				this.cajas1.setEnabled(true);
				this.lotes1.setEnabled(true);
				this.cajas2.setEnabled(true);
				this.lotes2.setEnabled(true);
				this.cajas3.setEnabled(true);
				this.lotes3.setEnabled(true);
				this.cajas4.setEnabled(true);
				this.lotes4.setEnabled(true);
				this.cajas5.setEnabled(true);
				this.lotes5.setEnabled(true);

				opcNuevo.setEnabled(true);
				opcNuevo.setCaption("Guardar");
				cmbSerie.setEnabled(false);
				opcSalir.setEnabled(true);
				opcEliminar.setVisible(false);
				opcBuscar.setEnabled(false);
				opcImprimir.setEnabled(true);

				break;
			}
			case "Modificacion":
			{
				
				break;
			}
			case "Encontrado":
			{
				this.fecha.setEnabled(true);
				this.txtObservaciones.setEnabled(true);
				this.cmbCalculos.setEnabled(true);
				this.articulo.setEnabled(true);
				this.cantidad.setEnabled(true);
				this.cajas1.setEnabled(true);
				this.lotes1.setEnabled(true);
				this.cajas2.setEnabled(true);
				this.lotes2.setEnabled(true);
				this.cajas3.setEnabled(true);
				this.lotes3.setEnabled(true);
				this.cajas4.setEnabled(true);
				this.lotes4.setEnabled(true);
				this.cajas5.setEnabled(true);
				this.lotes5.setEnabled(true);
				this.setBuscando(false);
				consultaControlEmbotelladoServer ces = consultaControlEmbotelladoServer.getInstance(CurrentUser.get());
				boolean existe =false;
				if (this.lblIdGreensys.getCaption()!=null)
				{
					existe = ces.obtenerExisteGreensys(new Integer(this.lblIdGreensys.getCaption()));
				}
				if (existe) 
				{
					opcEliminar.setVisible(false);
					if (!eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))
					{
						opcNuevo.setCaption("Crear");
						cmbSerie.setEnabled(true);
						if (!ces.obtenerStatusGreensys(new Integer(this.lblIdGreensys.getCaption())))
						{
							this.opcValidacion.addStyleName(ValoTheme.BUTTON_DANGER);
							this.opcValidacion.setCaption("Procesado");
						}
						
					}
					else
					{
						opcNuevo.setCaption("Guardar");
						cmbSerie.setEnabled(false);
						if (!ces.obtenerStatusGreensys(new Integer(this.lblIdGreensys.getCaption())))
						{
							this.opcValidacion.addStyleName(ValoTheme.BUTTON_DANGER);
							this.opcValidacion.setCaption("Procesado");
						}
						opcNuevo.setCaption("Guardar");
						cmbSerie.setEnabled(false);
						opcNuevo.setEnabled(true);
					}
				}
				else
				{
					opcEliminar.setVisible(true);
					this.lblIdGreensys.setCaption(null);
					opcNuevo.setCaption("Guardar");
					cmbSerie.setEnabled(false);
					opcNuevo.setEnabled(true);
					this.opcValidacion.setStyleName(ValoTheme.BUTTON_FRIENDLY);
					this.opcValidacion.setCaption("Validar");
				}
				opcImprimir.setEnabled(true);
				opcBuscar.setEnabled(true);
				opcSalir.setEnabled(true);
				break;
			}
		}
	}

	private void limpiarCampos()
	{
		this.articulo.txtTexto.setValue("");
		this.txtObservaciones.setValue("");
		this.cajas1.setValue("");
		this.cajas2.setValue("");
		this.cajas3.setValue("");
		this.cajas4.setValue("");
		this.cajas5.setValue("");
		this.lotes1.setValue("");
		this.lotes2.setValue("");
		this.lotes3.setValue("");
		this.lotes4.setValue("");
		this.lotes5.setValue("");
		this.cantidad.setValue("");
		this.litros.setValue("0");
		this.lblIdGreensys.setCaption(null);
		this.lblIdGreensys.setValue(null);
		this.generarGrid();
	}

	private void rellenarEntradasDatos(MapeoControlEmbotellado r_mapeo)
	{
		this.fecha.setValue(r_mapeo.getFecha());
		this.numero.setValue(r_mapeo.getIdControlEmbotellado().toString());
		this.cmbSerie.setValue(r_mapeo.getSerie());
		this.articulo.txtTexto.setValue(r_mapeo.getArticulo());
		this.descripcion.setValue(r_mapeo.getDescripcion());
		
		this.cajas1.setValue(r_mapeo.getCajas1().toString());
		this.cajas2.setValue(r_mapeo.getCajas2().toString());
		this.cajas3.setValue(r_mapeo.getCajas3().toString());
		this.cajas4.setValue(r_mapeo.getCajas4().toString());
		this.cajas5.setValue(r_mapeo.getCajas5().toString());
		this.lotes1.setValue(r_mapeo.getLote1());
		this.lotes2.setValue(r_mapeo.getLote2());
		this.lotes3.setValue(r_mapeo.getLote3());
		this.lotes4.setValue(r_mapeo.getLote4());
		this.lotes5.setValue(r_mapeo.getLote5());
		this.cantidad.setValue(r_mapeo.getCantidad().toString());
		this.litros.setValue(r_mapeo.getLitros().toString());
		this.txtObservaciones.setValue(r_mapeo.getObservaciones());
		if (r_mapeo.getIdGreensys()!=null) this.lblIdGreensys.setCaption(r_mapeo.getIdGreensys().toString()); else this.lblIdGreensys.setCaption(null); 
		this.mapeoControl=r_mapeo;
		
		if ((r_mapeo.getIdGreensys()==null || r_mapeo.getIdGreensys()==0) && (this.permisos.equals("80") || this.permisos.equals("99"))) this.opcValidacion.setEnabled(true);
		if (permisos.equals("80") || permisos.equals("99"))
		{
			cps = new consultaProduccionDiariaServer(CurrentUser.get());
			Double prod = cps.datosTotalArticuloDia(RutinasFechas.convertirDateToString(fecha.getValue()), articulo.txtTexto.getValue().substring(0,7));
			lblLeidoGreensys.setValue(prod.toString());
		}

	}

	public void destructor()
	{
		eBorsao.getCurrent().getNavigator().removeView(this.VIEW_NAME);
		eBorsao.getCurrent().getNavigator().addView(this.VIEW_NAME, this.getClass());
	}
	@Override
	public void eliminarRegistro() {
		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(CurrentUser.get());
		Integer idGreensys = null;
		
		if (this.lblIdGreensys.getCaption()!=null) idGreensys= new Integer(this.lblIdGreensys.getCaption());
			
		boolean rdo = ces.eliminar(cmbSerie.getValue().toString(), new Integer(this.numero.getValue()),idGreensys);
		if (rdo) Notificaciones.getInstance().mensajeInformativo("Registro eliminado correctamente");
		else Notificaciones.getInstance().mensajeInformativo("No se ha podido eliminar el registro");
		
		botonesGenerales(true);
		limpiarCampos();
		establecerSituacion("Inicial");
		limpiarCampos();
	}
	
	private Integer obtenerSiguienteRollo()
	{
		Integer resultado=null;
		ArrayList<MapeoNumeracionConsejo> vectorNumeracionConsejo =  (ArrayList<MapeoNumeracionConsejo>) ((ControlEmbotelladoConsejoGrid) grid3).vector;
		if (vectorNumeracionConsejo==null)
			resultado = 1;
		else
			resultado = vectorNumeracionConsejo.size() + 1;
		
		return resultado;
	}
	
	private void recalcularEscandallo()
	{
		boolean eliminar = true;
		ArrayList<MapeoLineasControlEmbotellado> vectorEscandallo = (ArrayList<MapeoLineasControlEmbotellado>) ((ControlEmbotelladoEscandalloGrid) grid2).vector;
		if (vectorEscandallo!=null)
		{
			for (int i=0;i<vectorEscandallo.size();i++)
			{
				MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) vectorEscandallo.get(i);
				
				if (mapeo.getMermas()!=0)
				{									
					eliminar=false;
				}
			}
		}
		if (eliminar)
		{
			if (isHayGrid2())
			{
				grid2.removeAllColumns();			
				panGrid2.removeComponent(grid2);
				grid2=null;
			}
			generarGrid();
		}
	}
	
	public void cerrarVentanaBusqueda()
	{
		if (this.numeradorSeleccionado!=null)
		{
			this.setBuscando(true);
			this.cmbSerie.setValue(this.areaSeleccionada.toString());
			this.numero.setValue(this.numeradorSeleccionado.toString());
			
			limpiarCampos();
			consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(null);
			mapeoControl = ces.recuperarControlEmbotellado(cmbSerie.getValue().toString(), numero.getValue());
			if (isHayGrid2())
			{
				grid2.removeAllColumns();			
				panGrid2.removeComponent(grid2);
				grid2=null;						
			}
			if (isHayGrid3())
			{
				grid3.removeAllColumns();			
				panGrid3.removeComponent(grid3);
				grid3=null;						
			}
			
			if (mapeoControl!=null)
			{
				rellenarEntradasDatos(mapeoControl);
				r_vector2 = ces.recuperarEscandallo(cmbSerie.getValue().toString(), numero.getValue());
				r_vector3 = ces.recuperarNumeracionConsejo(cmbSerie.getValue().toString(), numero.getValue());
				presentarGrid(r_vector2, r_vector3);
				establecerSituacion("Encontrado");
			}
		}
		else
			this.establecerSituacion("Inicial");
	}
	@Override
	public void aceptarProceso(String r_accion) {
		
	}

	@Override
	public void cancelarProceso(String r_accion) {
		
	}
	
	private boolean chequeo()
	{
		if (this.cmbSerie.getValue()== null) return false;
		else if (this.cmbSerie.getValue().toString().contentEquals("EMBOTELLADO") && this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0 && !this.articulo.txtTexto.getValue().startsWith("0102")) return false;
		else if (this.cmbSerie.getValue().toString().contentEquals("ENVASADO") && this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0 && !this.articulo.txtTexto.getValue().startsWith("0103")) return false;
		else if (this.cmbSerie.getValue().toString().contentEquals("BIB") && this.articulo.txtTexto.getValue()!=null && this.articulo.txtTexto.getValue().length()>0 && !this.articulo.txtTexto.getValue().startsWith("0111")) return false;
		else return true;
	}

	public boolean isBuscando() {
		return buscando;
	}

	public void setBuscando(boolean buscando) {
		this.buscando = buscando;
	}
}

