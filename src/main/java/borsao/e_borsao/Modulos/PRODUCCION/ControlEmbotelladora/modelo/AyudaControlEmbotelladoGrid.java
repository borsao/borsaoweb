package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import java.util.ArrayList;

import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class AyudaControlEmbotelladoGrid extends GridPropio 
{
	
	private boolean editable = false;
	private boolean conFiltro = false;
	
    public AyudaControlEmbotelladoGrid(ArrayList<MapeoControlEmbotellado> r_vector) 
    {
        this.vector=r_vector;
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }
    
    private void generarGrid()
    {
		this.crearGrid(MapeoControlEmbotellado.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("fecha", "idControlEmbotellado", "articulo" ,"descripcion", "cantidad");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("fecha").setHeaderCaption("Fecha");
    	this.getColumn("fecha").setSortable(false);
    	this.getColumn("fecha").setWidth(new Double(200));
    	
    	this.getColumn("idControlEmbotellado").setHeaderCaption("Numero");
    	this.getColumn("idControlEmbotellado").setSortable(false);
    	this.getColumn("idControlEmbotellado").setWidth(new Double(100));
    	
    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(450));
    	this.getColumn("cantidad").setHeaderCaption("Cantidad");
    	this.getColumn("cantidad").setWidth(100);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("embalaje").setHidden(true);
    	this.getColumn("observaciones").setHidden(true);
    	this.getColumn("tipoCalculo").setHidden(true);
    	this.getColumn("verficado").setHidden(true);    	
    	this.getColumn("tipoProduccion").setHidden(true);
    	this.getColumn("idGreensys").setHidden(true);
    	this.getColumn("litros").setHidden(true);
    	this.getColumn("cajas1").setHidden(true);
    	this.getColumn("cajas2").setHidden(true);
    	this.getColumn("cajas3").setHidden(true);
    	this.getColumn("cajas4").setHidden(true);
    	this.getColumn("cajas5").setHidden(true);
    	this.getColumn("lote1").setHidden(true);
    	this.getColumn("lote2").setHidden(true);
    	this.getColumn("lote3").setHidden(true);
    	this.getColumn("lote4").setHidden(true);
    	this.getColumn("lote5").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	
            	if ("idControlEmbotellado".equals(cellReference.getPropertyId()) || "cantidad".equals(cellReference.getPropertyId()) ) 
            	{
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
	}

	@Override
	public void calcularTotal() {
		
	}
}


