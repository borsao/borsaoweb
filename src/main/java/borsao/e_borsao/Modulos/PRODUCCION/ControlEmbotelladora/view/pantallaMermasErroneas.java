package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view;

import java.util.ArrayList;
import java.util.Iterator;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoLineasControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server.consultaControlEmbotelladoServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class pantallaMermasErroneas extends Window
{
	
	private Button btnBotonCVentana = null;
	private Grid gridDatos = null;
	private IndexedContainer container =null;
	
	public pantallaMermasErroneas(String r_titulo)
	{
		VerticalLayout principal = new VerticalLayout();
		principal.setSizeFull();
		
		HorizontalLayout botonera = new HorizontalLayout();
		botonera.setWidthUndefined();
		botonera.setHeight("100px");
		
		this.setCaption(r_titulo);
		this.center();
		this.setModal(true);
		this.setClosable(true);
		this.setResizable(true);
		this.setWidth("1200px");
		this.setHeight("750px");
		
		consultaControlEmbotelladoServer ces = new consultaControlEmbotelladoServer(CurrentUser.get());
		ArrayList<MapeoLineasControlEmbotellado> vector = ces.recuperarMermasErroneas();
		
		this.llenarRegistros(vector);
		this.cargarListeners();
		
		this.gridDatos.setSizeFull();
		this.gridDatos.addStyleName("smallgrid");
		this.gridDatos.setWidth("100%");
		this.gridDatos.setHeight("650");
		this.gridDatos.setFrozenColumnCount(3);
		
		btnBotonCVentana = new Button("Cerrar");
		btnBotonCVentana.addStyleName(ValoTheme.BUTTON_TINY);

		btnBotonCVentana.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		
		principal.addComponent(this.gridDatos);
		botonera.addComponent(btnBotonCVentana);		
		botonera.setComponentAlignment(btnBotonCVentana, Alignment.BOTTOM_CENTER);
		
		principal.addComponent(botonera);
		principal.setComponentAlignment(botonera, Alignment.BOTTOM_LEFT);
		principal.setComponentAlignment(this.gridDatos, Alignment.TOP_LEFT);
		this.setContent(principal);

	}

	private void llenarRegistros(ArrayList<MapeoLineasControlEmbotellado> r_vector)
	{
		Iterator iterator = null;
		MapeoLineasControlEmbotellado mapeoAyuda = null;
		iterator = r_vector.iterator();
		
		container = new IndexedContainer();
		container.addContainerProperty("Orden", Integer.class, null);
		container.addContainerProperty("Articulo", String.class, null);
		container.addContainerProperty("Unidades", Integer.class, null);
		container.addContainerProperty("Fecha", String.class, null);
		
		while (iterator.hasNext())
		{
			Object newItemId = container.addItem();
			Item file = container.getItem(newItemId);
			
			mapeoAyuda = (MapeoLineasControlEmbotellado) iterator.next();
			
			file.getItemProperty("Orden").setValue(mapeoAyuda.getIdControlEmbotellado());
			file.getItemProperty("Articulo").setValue(RutinasCadenas.conversion(mapeoAyuda.getArticulo()));
			file.getItemProperty("Unidades").setValue(mapeoAyuda.getUnidades());
			file.getItemProperty("Fecha").setValue(mapeoAyuda.getFecha());
			
		}

		this.gridDatos= new Grid(container);
		this.gridDatos.setSelectionMode(SelectionMode.NONE);
		this.asignarEstilos();
	}
	
    public void cargarListeners()
    {    	
    }

	public void asignarEstilos()
    {
		this.gridDatos.setCellStyleGenerator(new Grid.CellStyleGenerator() 
    	{
            public String getStyle(Grid.CellReference cellReference) 
            {
            	if ("Unidades".equals(cellReference.getPropertyId()) || "Orden".equals(cellReference.getPropertyId())) 
            	{
            		return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}
            }
        });
    }	
}