package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.Container.Indexed;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.ui.Grid;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasNumericas;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.ControlEmbotelladoView;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.PantallaControlEmbotellado;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ControlEmbotelladoConsejoGrid extends GridPropio 
{
	private boolean editable = true;
	private boolean conFiltro = false;
	private ControlEmbotelladoView app = null;
	private PantallaControlEmbotellado appVt = null;
	private Integer idPrdControl = null;
	
    public ControlEmbotelladoConsejoGrid(ArrayList<MapeoNumeracionConsejo> r_vector,ControlEmbotelladoView r_app) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoNumeracionConsejo>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    public ControlEmbotelladoConsejoGrid(PantallaControlEmbotellado r_vt, ArrayList<MapeoNumeracionConsejo> r_vector,Integer r_numerador) 
    {
        this.vector=r_vector;
        this.idPrdControl = r_numerador;
        this.appVt=r_vt;
        
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			this.vector = new ArrayList<MapeoNumeracionConsejo>(); 			
		}
		else
		{
		}
		this.generarGrid();
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoNumeracionConsejo.class);
		if (this.vector!=null) this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
		
		this.calcularTotal();
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	setColumnOrder("numeroRollo", "serie", "desdeNumeracion", "hastaNumeracion", "diferencias");
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("numeroRollo").setHeaderCaption("Rollo");
    	this.getColumn("numeroRollo").setSortable(true);
    	this.getColumn("numeroRollo").setWidth(new Double(100));
    	this.getColumn("serie").setHeaderCaption("Serie");
    	this.getColumn("serie").setSortable(true);
    	this.getColumn("serie").setWidth(new Double(100));
    	this.getColumn("desdeNumeracion").setHeaderCaption("Desde");
    	this.getColumn("desdeNumeracion").setSortable(false);
    	this.getColumn("desdeNumeracion").setWidth(new Double(150));
    	this.getColumn("hastaNumeracion").setHeaderCaption("Hasta");
    	this.getColumn("hastaNumeracion").setWidth(150);
    	this.getColumn("diferencias").setHeaderCaption("Cuantas");
    	this.getColumn("diferencias").setWidth(150);
    	this.getColumn("diferencias").setEditable(false);

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("serieNum").setHidden(true);
    	this.getColumn("idControlEmbotellado").setHidden(true);
    	
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	return "Rcell-normal";
            }
        });
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
        		MapeoNumeracionConsejo mapeo = (MapeoNumeracionConsejo) getEditedItemId();
        		MapeoNumeracionConsejo mapeoOrig=(MapeoNumeracionConsejo) getEditedItemId();
        		
        		if (app!=null)
        		{
        			if (app.numero.getValue()!=null && app.numero.getValue().length()>0)
        			{
        				idPrdControl = new Integer(app.numero.getValue());
        			}
        		}
        		
        		if ((mapeo.getHastaNumeracion()<mapeo.getDesdeNumeracion()) || (mapeo.getHastaNumeracion()==0) || (mapeo.getDesdeNumeracion()==0))
        		{
        			Notificaciones.getInstance().mensajeError("Rellena correctamente la numeración");
        			doCancelEditor();
        		}
        		else
        		{
	        		mapeo.setIdControlEmbotellado(idPrdControl);
	        		mapeo.setDiferencias(mapeo.getHastaNumeracion()-mapeo.getDesdeNumeracion()+1);
	        		
	        		if (mapeo.getIdControlEmbotellado()!=null)
		        	{
		        		((ArrayList<MapeoNumeracionConsejo>) vector).remove(mapeoOrig);
		    			((ArrayList<MapeoNumeracionConsejo>) vector).add(mapeo);
	
		    			refreshAllRows();
		    			calcularTotal();
		    			scrollTo(mapeo);
		        	}
        		}
	        }
		});
					
	}
	
	@Override
	public void doCancelEditor()
	{
		  super.doCancelEditor();
	}

	@Override
	public void calcularTotal() {
		//"","","litros", "reservado", "prepedido", "stock_real"
		Integer total = new Integer(0) ;
    	
    	if (this.footer==null) this.footer=this.appendFooterRow();
    	
    	Indexed indexed = this.getContainerDataSource();
        List<?> list = new ArrayList<Object>(indexed.getItemIds());
        for(Object itemId : list)
        {
        	Item item = indexed.getItem(itemId);
        	if (item!=null)
        	{
	        	Integer q1Value = (Integer) item.getItemProperty("diferencias").getValue();
	        	if (q1Value!=null) total += q1Value;
        	}
        }
        
//        footer.getCell("").setText("Totales");
		footer.getCell("diferencias").setText(RutinasNumericas.formatearIntegerDigitos(total, new Integer(0)).toString());
		
//		this.app.barAndGridLayout.setWidth("100%");
		if (this.app!=null)
		{
			this.app.barAndGridLayout.setHeight((this.app.getHeight()-this.app.cabLayout.getHeight()-this.app.lblSeparador.getHeight()-this.app.topLayout.getHeight()-50)+"%");
		}
		else if (this.appVt!=null)
		{
			this.appVt.horGrid.setHeight((this.appVt.getHeight()-this.appVt.cabLayout.getHeight()-this.appVt.topLayout.getHeight()-50)+"%");
		}
//		footer.setStyleName("smallgrid");
		footer.getCell("diferencias").setStyleName("Rcell-pie");

	}
}


