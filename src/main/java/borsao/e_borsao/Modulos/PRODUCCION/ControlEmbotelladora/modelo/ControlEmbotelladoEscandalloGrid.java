package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;

import borsao.e_borsao.ClasesPropias.GridPropio;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.view.ControlEmbotelladoView;
import borsao.e_borsao.modelo.Login.CurrentUser;

/**
 * Grid of products, handling the visual presentation and filtering of a set of
 * items. This version uses an in-memory data source that is suitable for small
 * data sets.
 */
public class ControlEmbotelladoEscandalloGrid extends GridPropio 
{
	
	private boolean editable = true;
	private boolean conFiltro = false;
	private String articuloIntroducido =null;
	private ControlEmbotelladoView app = null;
	boolean  nuevo = false;
	
    public ControlEmbotelladoEscandalloGrid(ArrayList<MapeoLineasControlEmbotellado> r_vector, ControlEmbotelladoView r_app) 
    {
        this.vector=r_vector;
        this.app=r_app;
        
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    public ControlEmbotelladoEscandalloGrid(ArrayList<MapeoLineasControlEmbotellado> r_vector) 
    {
        this.vector=r_vector;
        
		this.asignarTitulo("");

		if (this.vector==null || this.vector.size()==0)
		{
			Notificaciones.getInstance().mensajeInformativo("No se han encontrado registros");			
		}
		else
		{
			this.generarGrid();
		}
    }

    private void generarGrid()
    {
		this.crearGrid(MapeoLineasControlEmbotellado.class);
		this.setRecords(this.vector);
		this.setFrozenColumnCount(0);
		
		this.setStyleName("smallgrid");
		
		this.setEditorEnabled(this.editable);
		this.setConFiltro(this.conFiltro);
		this.setSeleccion(SelectionMode.SINGLE);
    }
    
    public void establecerOrdenPresentacionColumnas()
    {
    	
    	setColumnOrder("btn", "articulo", "descripcion", "unidades", "mermas");
    	/*
    	 * TODO Controlar que no salga el almacen articulo
    	 */
    }
    
    public void establecerTitulosColumnas()
    {
    	this.getColumn("btn").setHeaderCaption("");
    	this.getColumn("btn").setSortable(false);
    	this.getColumn("btn").setWidth(new Double(40));

    	this.getColumn("articulo").setHeaderCaption("Articulo");
    	this.getColumn("articulo").setSortable(false);
    	this.getColumn("articulo").setWidth(new Double(100));
    	this.getColumn("descripcion").setHeaderCaption("Descripcion");
    	this.getColumn("descripcion").setSortable(false);
    	this.getColumn("descripcion").setWidth(new Double(400));
    	this.getColumn("unidades").setHeaderCaption("Cantidad");
    	this.getColumn("unidades").setWidth(100);
    	this.getColumn("mermas").setHeaderCaption("mermas");
    	this.getColumn("mermas").setWidth(100);
    	this.getColumn("articulo").setEditorField(
				getTextField("El articulo es obligatoria", "articulo"));
    	this.getColumn("descripcion").setEditorField(
				getTextField("El Descripcion es obligatoria", "descripcion"));

    	this.getColumn("idCodigo").setHidden(true);
    	this.getColumn("fecha").setHidden(true);
    	this.getColumn("serie").setHidden(true);
    	this.getColumn("orden").setHidden(true);
    	this.getColumn("idControlEmbotellado").setHidden(true);
    	this.getColumn("origen").setHidden(true);
    	this.getColumn("btn").setHidden(true);
    }
    
    public void asignarEstilos()
    {
    	
    	setCellStyleGenerator(new Grid.CellStyleGenerator() {
            String art = "";
            @Override
            public String getStyle(Grid.CellReference cellReference) {
            	if ("btn".equals(cellReference.getPropertyId()))
            	{
            		if (cellReference.getItem().getItemProperty("articulo").getValue()!=null && (cellReference.getItem().getItemProperty("articulo").getValue().toString().startsWith("0104")||cellReference.getItem().getItemProperty("articulo").getValue().toString().startsWith("0102"))) return "cell-nativebuttonBotella"; else return "cell-normal";
            	}
            	else if ("mermas".equals(cellReference.getPropertyId()) || "unidades".equals(cellReference.getPropertyId())) 
            	{
        			return "Rcell-normal";
            	}
            	else
            	{
            		return "cell-normal";
            	}

            }
        });
    	
    }
    
	@Override
	public void establecerColumnasNoFiltro() 
	{
//		this.camposNoFiltrar.add("papel");		
	}

	@Override
	public void cargarListeners() 
	{
		
		
		this.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler() {
			
			MapeoLineasControlEmbotellado mapeo = null;
			
	        @Override
	        public void preCommit(FieldGroup.CommitEvent commitEvent) throws FieldGroup.CommitException {
	        //...
	        	MapeoLineasControlEmbotellado mapeoOrig = (MapeoLineasControlEmbotellado) getEditedItemId();
        		if (mapeoOrig.getArticulo()==null || mapeoOrig.getArticulo().length()==0) nuevo=true;
	        }

	        @Override
	        public void postCommit(FieldGroup.CommitEvent commitEvent) throws     FieldGroup.CommitException {
	        //...
	        	MapeoLineasControlEmbotellado mapeoOrig = (MapeoLineasControlEmbotellado) getEditedItemId();
        		mapeo = (MapeoLineasControlEmbotellado) getEditedItemId();
        		mapeo.setOrigen(1);
	        
        		if (!nuevo)
	        	{
        			int i = ((ArrayList<MapeoLineasControlEmbotellado>) vector).indexOf(mapeoOrig);
	        		((ArrayList<MapeoLineasControlEmbotellado>) vector).remove(i);
	        		((ArrayList<MapeoLineasControlEmbotellado>) vector).add(i,mapeo);
	        	}
        		else
        		{
        			((ArrayList<MapeoLineasControlEmbotellado>) vector).add(mapeo);
        		}
    			refreshAllRows();
    			scrollTo(mapeo);
    			select(mapeo);
	        }
		});
		this.addItemClickListener(new ItemClickEvent.ItemClickListener() 
    	{
            public void itemClick(ItemClickEvent event) 
            {
            	if (event.getPropertyId()!=null)
            	{
            		MapeoLineasControlEmbotellado mapeo = (MapeoLineasControlEmbotellado) event.getItemId();
            		
	            	if (event.getPropertyId().toString().equals("btn"))
	            	{
	            	}
	            	else
	            	{
	            	}
	            }
    		}
        });
	}
	
	private Field<?> getTextField(String requiredErrorMsg, String r_nombre) {
		TextField texto = new TextField();
		
		switch (r_nombre)
		{
			case "articulo":
			{
				texto.setRequired(true);
				texto.addValueChangeListener(new ValueChangeListener() {
					
					@Override
					public void valueChange(ValueChangeEvent event) {
						if (texto.getValue()!=null)
						{
							consultaArticulosServer cas = new consultaArticulosServer(CurrentUser.get());
							articuloIntroducido=cas.obtenerDescripcionArticulo(texto.getValue());
							
						}
					}
				});
				
				break;
			}
			case "descripcion":
			{
//				texto.setEnabled(false);
				texto.addFocusListener(new FocusListener() {

					@Override
					public void focus(FocusEvent event) {
						texto.setValue(articuloIntroducido.trim());						
					}
				});
				break;
			}
		}
		return texto;
	}

	@Override
	public void calcularTotal() {
		
	}


}


