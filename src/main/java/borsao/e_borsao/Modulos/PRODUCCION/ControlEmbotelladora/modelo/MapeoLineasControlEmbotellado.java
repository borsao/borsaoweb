package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo;

import java.util.Date;

import borsao.e_borsao.ClasesPropias.MapeoGlobal;
import borsao.e_borsao.ClasesPropias.RutinasFechas;

public class MapeoLineasControlEmbotellado  extends MapeoGlobal
{
	private Integer idControlEmbotellado;
	private Integer orden;
	private Date fecha;
	private String serie;
	private String articulo;
	private String descripcion;
	private Integer unidades;
	private Integer mermas;
	private Integer origen;
	private String btn;

	public MapeoLineasControlEmbotellado()
	{
		
	}

	public Integer getIdControlEmbotellado() {
		return idControlEmbotellado;
	}

	public void setIdControlEmbotellado(Integer idControlEmbotellado) {
		this.idControlEmbotellado = idControlEmbotellado;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getFecha() {
		if (fecha != null) return RutinasFechas.convertirDateToString(fecha);
		else return null;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public Integer getMermas() {
		return mermas;
	}

	public void setMermas(Integer mermas) {
		this.mermas = mermas;
	}

	public Integer getOrigen() {
		return origen;
	}

	public void setOrigen(Integer origen) {
		this.origen = origen;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}

}