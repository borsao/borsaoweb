package borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import borsao.e_borsao.eBorsao;
import borsao.e_borsao.ClasesPropias.LibreriaImpresion;
import borsao.e_borsao.ClasesPropias.Notificaciones;
import borsao.e_borsao.ClasesPropias.RutinasCadenas;
import borsao.e_borsao.ClasesPropias.RutinasFechas;
import borsao.e_borsao.ClasesPropias.connectionManager;
import borsao.e_borsao.ClasesPropias.serverBasico;
import borsao.e_borsao.Modulos.CALIDAD.NoConformidades.server.consultaMermasPtServer;
import borsao.e_borsao.Modulos.GENERALES.Articulos.server.consultaArticulosServer;
import borsao.e_borsao.Modulos.GENERALES.ContadoresEjercicio.server.consultaContadoresEjercicioServer;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoLineasControlEmbotellado;
import borsao.e_borsao.Modulos.PRODUCCION.ControlEmbotelladora.modelo.MapeoNumeracionConsejo;
import borsao.e_borsao.Modulos.PRODUCCION.Operarios.server.consultaOperariosServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionEscandallo;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.modelo.MapeoAyudaProduccionSGA;
import borsao.e_borsao.Modulos.PRODUCCION.consultaAyudaProduccion.server.consultaAyudaProduccionServer;
import borsao.e_borsao.Modulos.PRODUCCION.consultaEscandallo.server.consultaEscandalloServer;
import borsao.e_borsao.modelo.Login.CurrentUser;

public class consultaControlEmbotelladoServer extends serverBasico
{
	private connectionManager conManager = null;
	private Notificaciones serNotif=null;
	private Connection con = null;	
	private Statement cs = null;
	private String almacen = null;
	private static consultaControlEmbotelladoServer instance;
	
	public consultaControlEmbotelladoServer(String r_usuario)
	{
		this.conManager = connectionManager.getInstance();
		this.serNotif = Notificaciones.getInstance();
	}
	
	public static consultaControlEmbotelladoServer getInstance(String r_usuario) 
	{
		if (instance == null) 
		{
			instance = new consultaControlEmbotelladoServer(r_usuario);			
		}
		return instance;
	}
	
	public String obtenerDescripcionArticulo(String r_articulo)
	{
		String rdo = null;
		consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
		rdo = cas.obtenerDescripcionArticulo(r_articulo);
		return rdo;
	}

//	public String obtenerAlmacenUsuario()
//	{
//		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
//		
//		String almacen = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());
//		
//		return almacen;
//		
//	}
	
	public String comprobarAccesos()
	{
		String permisos = null;
		
		consultaOperariosServer cos = consultaOperariosServer.getInstance(CurrentUser.get());
		
		almacen = cos.obtenerAlmacenDefecto(eBorsao.get().accessControl.getNombre());

		permisos = cos.obtenerPermisos("Embotelladora");
		
		if (permisos!=null)
		{
			if (permisos.length()==0) permisos="0";				
		}
		else
		{
			permisos = cos.obtenerPermisos("BIB");
			if (permisos!=null)
			{
				if (permisos.length()==0) permisos="0";				
			}
			else
			{
				permisos = cos.obtenerPermisos(null);
				if (permisos!=null)
				{
					if (permisos.length()==0) permisos="0";
				}
				else
				{
					permisos="0";
				}
			}
		}
		return permisos;
	}

	public Integer obtenerEmbalajeArticulo(String r_articulo)
	{
		Integer rdo = null;
		consultaEscandalloServer cas = consultaEscandalloServer.getInstance(CurrentUser.get());
		rdo = cas.recogerTipoCaja(r_articulo);
		switch (rdo)
		{
			case 83:
			{
				return 3; 
			}
			case 6:
			{
				return 1;
			}
			case 12:
			{
				return 2;
			}
			case 4:
			{
				return 1;
			}
			case 1:
			{
				return 2;
			}
			case 0:
			{
				return 1;
			}
		}
		return 3;
	}

	public String generarInforme(String r_area, Integer r_idControl)
	{
		
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;

    	libImpresion=new LibreriaImpresion();
    	
    	libImpresion.setArchivoDefinitivo("/controlEmbotellado" + String.valueOf((new Date()).getTime()) + ".pdf");
    	libImpresion.setCodigo(r_idControl);
    	if (r_area.contentEquals("EMBOTELLADO"))
    	{
    		libImpresion.setArchivoPlantilla("controlEmbotellado.jrxml");
        	libImpresion.setArchivoPlantillaDetalle("controlEmbotelladoEscandallo.jrxml");
        	libImpresion.setArchivoPlantillaDetalleCompilado("controlEmbotelladoEscandallo.jasper");
        	libImpresion.setArchivoPlantillaNotas("controlEmbotelladoConsejo.jrxml");
        	libImpresion.setArchivoPlantillaNotasCompilado("controlEmbotelladoConsejo.jasper");
    	}
    	else if (r_area.contentEquals("ENVASADO"))
    	{
    		libImpresion.setArchivoPlantilla("controlEnvasado.jrxml");
        	libImpresion.setArchivoPlantillaDetalle("controlEmbotelladoEscandallo.jrxml");
        	libImpresion.setArchivoPlantillaDetalleCompilado("controlEmbotelladoEscandallo.jasper");
    	}
    	else if (r_area.contentEquals("BIB"))
    	{
    		libImpresion.setArchivoPlantilla("controlBib.jrxml");
        	libImpresion.setArchivoPlantillaDetalle("controlEmbotelladoEscandallo.jrxml");
        	libImpresion.setArchivoPlantillaDetalleCompilado("controlEmbotelladoEscandallo.jasper");
    	}
    	libImpresion.setCarpeta("embotelladora");
    	libImpresion.setBackGround(null);
    	
    	resultadoGeneracion=libImpresion.generacionFinal();
    	
    	if (resultadoGeneracion==null) resultadoGeneracion=libImpresion.getArchivoDefinitivo();
    	
    	return resultadoGeneracion;
	}
	
	private String imprimirControlEmbotellado(String r_ejercicio, String r_semana)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		libImpresion = new LibreriaImpresion();

		libImpresion.setCodigoTxt(r_ejercicio);
		libImpresion.setCodigo2Txt(r_semana);
		libImpresion.setArchivoDefinitivo("/ControlEmbotellado semana - " + r_semana.toString() + " - " + RutinasFechas.horaActualSinSeparador() + ".pdf");
		libImpresion.setArchivoPlantilla("ListadoControlEmbotellado.jrxml");
		libImpresion.setArchivoPlantillaDetalle(null);
		libImpresion.setArchivoPlantillaDetalleCompilado(null);
		libImpresion.setArchivoPlantillaNotas(null);
		libImpresion.setArchivoPlantillaNotasCompilado(null);
		libImpresion.setCarpeta("ControlEmbotellado");
		libImpresion.setBackGroundEtiqueta("/fondoA4LogoAzul.jpg");

		resultadoGeneracion = libImpresion.generacionInformeTxtXML();

		if (resultadoGeneracion == null)
			resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		return resultadoGeneracion;		
	}

	public Integer obtenerSiguiente()
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT max(prd_control_embotellado.idprd_control) pro_sig ");
     	cadenaSQL.append(" FROM prd_control_embotellado ");
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_UPDATABLE);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_sig") + 1 ;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return 1;
	}

	public MapeoControlEmbotellado recuperarControlEmbotellado(String r_area, String r_numeroControl)
	{
		MapeoControlEmbotellado mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_control_embotellado.idprd_control ctr_id,");
		cadenaSQL.append(" prd_control_embotellado.serie ctr_ser,");
		cadenaSQL.append(" prd_control_embotellado.fecha ctr_fe,");
		cadenaSQL.append(" prd_control_embotellado.articulo ctr_art,");
		cadenaSQL.append(" prd_control_embotellado.descripcion ctr_des,");
		cadenaSQL.append(" prd_control_embotellado.unidades ctr_uds,");
		cadenaSQL.append(" prd_control_embotellado.litros ctr_ltr,");
		cadenaSQL.append(" prd_control_embotellado.tipo ctr_tip,");
		cadenaSQL.append(" prd_control_embotellado.tipoCalculo ctr_tc,");
		cadenaSQL.append(" prd_control_embotellado.embalaje ctr_emb,");
		cadenaSQL.append(" prd_control_embotellado.cajas1 ctr_cj1,");
		cadenaSQL.append(" prd_control_embotellado.lote1 ctr_lo1,");
		cadenaSQL.append(" prd_control_embotellado.cajas2 ctr_cj2,");
		cadenaSQL.append(" prd_control_embotellado.lote2 ctr_lo2,");
		cadenaSQL.append(" prd_control_embotellado.cajas3 ctr_cj3,");
		cadenaSQL.append(" prd_control_embotellado.lote3 ctr_lo3,");
		cadenaSQL.append(" prd_control_embotellado.cajas4 ctr_cj4,");
		cadenaSQL.append(" prd_control_embotellado.lote4 ctr_lo4, ");
		cadenaSQL.append(" prd_control_embotellado.cajas5 ctr_cj5,");
		cadenaSQL.append(" prd_control_embotellado.lote5 ctr_lo5, ");
		cadenaSQL.append(" prd_control_embotellado.verificado ctr_ver, ");
		cadenaSQL.append(" prd_control_embotellado.idGreensys ctr_idg, ");
		cadenaSQL.append(" prd_control_embotellado.idProgramacion ctr_prg, ");
		cadenaSQL.append(" prd_control_embotellado.observaciones ctr_obs");
		cadenaSQL.append(" from prd_control_embotellado ");
		
		try
		{
			cadenaWhere = new StringBuffer();
			
			cadenaWhere.append(" serie = '" + r_area + "' ");
			
			if (r_numeroControl!=null)
			{
				
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idprd_control = '" + r_numeroControl + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlEmbotellado();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("ctr_id"));
				mapeo.setSerie(rsOpcion.getString("ctr_ser"));
				mapeo.setFecha(rsOpcion.getDate("ctr_fe"));
				mapeo.setArticulo(rsOpcion.getString("ctr_art"));
				mapeo.setDescripcion(rsOpcion.getString("ctr_des"));
				mapeo.setObservaciones(rsOpcion.getString("ctr_obs"));
				mapeo.setCantidad(rsOpcion.getInt("ctr_uds"));
				mapeo.setTipoProduccion(rsOpcion.getString("ctr_tip"));
				mapeo.setTipoCalculo(rsOpcion.getString("ctr_tc"));
				mapeo.setCajas1(rsOpcion.getInt("ctr_cj1"));
				mapeo.setLote1(rsOpcion.getString("ctr_lo1"));
				mapeo.setCajas2(rsOpcion.getInt("ctr_cj2"));
				mapeo.setLote2(rsOpcion.getString("ctr_lo2"));
				mapeo.setCajas3(rsOpcion.getInt("ctr_cj3"));
				mapeo.setLote3(rsOpcion.getString("ctr_lo3"));
				mapeo.setCajas4(rsOpcion.getInt("ctr_cj4"));
				mapeo.setLote4(rsOpcion.getString("ctr_lo4"));
				mapeo.setCajas5(rsOpcion.getInt("ctr_cj5"));
				mapeo.setLote5(rsOpcion.getString("ctr_lo5"));
				mapeo.setLitros(rsOpcion.getInt("ctr_ltr"));
				mapeo.setEmbalaje(rsOpcion.getInt("ctr_emb"));
				mapeo.setVerficado(rsOpcion.getString("ctr_ver"));
				mapeo.setIdGreensys(rsOpcion.getInt("ctr_idg"));
				mapeo.setIdProgramacion(rsOpcion.getInt("ctr_prg"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}

	public MapeoControlEmbotellado recuperarControlEmbotellado(Integer r_idProgramacion)
	{
		MapeoControlEmbotellado mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_control_embotellado.idprd_control ctr_id,");
		cadenaSQL.append(" prd_control_embotellado.fecha ctr_fe,");
		cadenaSQL.append(" prd_control_embotellado.serie ctr_ser,");
		cadenaSQL.append(" prd_control_embotellado.articulo ctr_art,");
		cadenaSQL.append(" prd_control_embotellado.descripcion ctr_des,");
		cadenaSQL.append(" prd_control_embotellado.unidades ctr_uds,");
		cadenaSQL.append(" prd_control_embotellado.litros ctr_ltr,");
		cadenaSQL.append(" prd_control_embotellado.tipo ctr_tip,");
		cadenaSQL.append(" prd_control_embotellado.tipoCalculo ctr_tc,");
		cadenaSQL.append(" prd_control_embotellado.embalaje ctr_emb,");
		cadenaSQL.append(" prd_control_embotellado.cajas1 ctr_cj1,");
		cadenaSQL.append(" prd_control_embotellado.lote1 ctr_lo1,");
		cadenaSQL.append(" prd_control_embotellado.cajas2 ctr_cj2,");
		cadenaSQL.append(" prd_control_embotellado.lote2 ctr_lo2,");
		cadenaSQL.append(" prd_control_embotellado.cajas3 ctr_cj3,");
		cadenaSQL.append(" prd_control_embotellado.lote3 ctr_lo3,");
		cadenaSQL.append(" prd_control_embotellado.cajas4 ctr_cj4,");
		cadenaSQL.append(" prd_control_embotellado.lote4 ctr_lo4, ");
		cadenaSQL.append(" prd_control_embotellado.cajas5 ctr_cj5,");
		cadenaSQL.append(" prd_control_embotellado.lote5 ctr_lo5, ");
		cadenaSQL.append(" prd_control_embotellado.verificado ctr_ver, ");
		cadenaSQL.append(" prd_control_embotellado.idGreensys ctr_idg, ");
		cadenaSQL.append(" prd_control_embotellado.idProgramacion ctr_prg, ");
		cadenaSQL.append(" prd_control_embotellado.observaciones ctr_obs");
		cadenaSQL.append(" from prd_control_embotellado ");

		try
		{
			if (r_idProgramacion!=null)
			{
				cadenaWhere = new StringBuffer();
				
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idProgramacion = '" + r_idProgramacion + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlEmbotellado();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("ctr_id"));
				mapeo.setFecha(rsOpcion.getDate("ctr_fe"));
				mapeo.setSerie(rsOpcion.getString("ctr_ser"));
				mapeo.setArticulo(rsOpcion.getString("ctr_art"));
				mapeo.setDescripcion(rsOpcion.getString("ctr_des"));
				mapeo.setObservaciones(rsOpcion.getString("ctr_obs"));
				mapeo.setCantidad(rsOpcion.getInt("ctr_uds"));
				mapeo.setTipoProduccion(rsOpcion.getString("ctr_tip"));
				mapeo.setTipoCalculo(rsOpcion.getString("ctr_tc"));
				mapeo.setCajas1(rsOpcion.getInt("ctr_cj1"));
				mapeo.setLote1(rsOpcion.getString("ctr_lo1"));
				mapeo.setCajas2(rsOpcion.getInt("ctr_cj2"));
				mapeo.setLote2(rsOpcion.getString("ctr_lo2"));
				mapeo.setCajas3(rsOpcion.getInt("ctr_cj3"));
				mapeo.setLote3(rsOpcion.getString("ctr_lo3"));
				mapeo.setCajas4(rsOpcion.getInt("ctr_cj4"));
				mapeo.setLote4(rsOpcion.getString("ctr_lo4"));
				mapeo.setCajas5(rsOpcion.getInt("ctr_cj5"));
				mapeo.setLote5(rsOpcion.getString("ctr_lo5"));
				mapeo.setLitros(rsOpcion.getInt("ctr_ltr"));
				mapeo.setEmbalaje(rsOpcion.getInt("ctr_emb"));
				mapeo.setVerficado(rsOpcion.getString("ctr_ver"));
				mapeo.setIdGreensys(rsOpcion.getInt("ctr_idg"));
				mapeo.setIdProgramacion(rsOpcion.getInt("ctr_prg"));
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}

	public MapeoControlEmbotellado recuperarControlEmbotellado(Date r_fecha)
	{
		MapeoControlEmbotellado mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_control_embotellado.idprd_control ctr_id,");
		cadenaSQL.append(" prd_control_embotellado.fecha ctr_fe,");
		cadenaSQL.append(" prd_control_embotellado.serie ctr_ser,");
		cadenaSQL.append(" prd_control_embotellado.articulo ctr_art,");
		cadenaSQL.append(" prd_control_embotellado.descripcion ctr_des,");
		cadenaSQL.append(" prd_control_embotellado.unidades ctr_uds,");
		cadenaSQL.append(" prd_control_embotellado.litros ctr_ltr,");
		cadenaSQL.append(" prd_control_embotellado.tipo ctr_tip,");
		cadenaSQL.append(" prd_control_embotellado.tipoCalculo ctr_tc,");
		cadenaSQL.append(" prd_control_embotellado.embalaje ctr_emb,");
		cadenaSQL.append(" prd_control_embotellado.cajas1 ctr_cj1,");
		cadenaSQL.append(" prd_control_embotellado.lote1 ctr_lo1,");
		cadenaSQL.append(" prd_control_embotellado.cajas2 ctr_cj2,");
		cadenaSQL.append(" prd_control_embotellado.lote2 ctr_lo2,");
		cadenaSQL.append(" prd_control_embotellado.cajas3 ctr_cj3,");
		cadenaSQL.append(" prd_control_embotellado.lote3 ctr_lo3,");
		cadenaSQL.append(" prd_control_embotellado.cajas4 ctr_cj4,");
		cadenaSQL.append(" prd_control_embotellado.lote4 ctr_lo4, ");
		cadenaSQL.append(" prd_control_embotellado.cajas5 ctr_cj5,");
		cadenaSQL.append(" prd_control_embotellado.lote5 ctr_lo5, ");
		cadenaSQL.append(" prd_control_embotellado.verificado ctr_ver, ");
		cadenaSQL.append(" prd_control_embotellado.idGreensys ctr_idg, ");
		cadenaSQL.append(" prd_control_embotellado.idProgramacion ctr_prg, ");
		cadenaSQL.append(" prd_control_embotellado.observaciones ctr_obs");
		cadenaSQL.append(" from prd_control_embotellado ");

		try
		{
			cadenaWhere = new StringBuffer();
			
			if (r_fecha!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" fecha = '" + RutinasFechas.convertirDateToString(r_fecha) + "' ");
			}
			
			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
			cadenaWhere.append(" verificado = 'N' ");
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			cadenaWhere.append(" order by prd_control_embotellado.verificado, prd_control_embotellado.idprd_control ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlEmbotellado();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("ctr_id"));
				mapeo.setFecha(rsOpcion.getDate("ctr_fe"));
				mapeo.setSerie(rsOpcion.getString("ctr_ser"));
				mapeo.setArticulo(rsOpcion.getString("ctr_art"));
				mapeo.setDescripcion(rsOpcion.getString("ctr_des"));
				mapeo.setObservaciones(rsOpcion.getString("ctr_obs"));
				mapeo.setCantidad(rsOpcion.getInt("ctr_uds"));
				mapeo.setTipoProduccion(rsOpcion.getString("ctr_tip"));
				mapeo.setTipoCalculo(rsOpcion.getString("ctr_tc"));
				mapeo.setCajas1(rsOpcion.getInt("ctr_cj1"));
				mapeo.setLote1(rsOpcion.getString("ctr_lo1"));
				mapeo.setCajas2(rsOpcion.getInt("ctr_cj2"));
				mapeo.setLote2(rsOpcion.getString("ctr_lo2"));
				mapeo.setCajas3(rsOpcion.getInt("ctr_cj3"));
				mapeo.setLote3(rsOpcion.getString("ctr_lo3"));
				mapeo.setCajas4(rsOpcion.getInt("ctr_cj4"));
				mapeo.setLote4(rsOpcion.getString("ctr_lo4"));
				mapeo.setCajas5(rsOpcion.getInt("ctr_cj5"));
				mapeo.setLote5(rsOpcion.getString("ctr_lo5"));
				mapeo.setLitros(rsOpcion.getInt("ctr_ltr"));
				mapeo.setEmbalaje(rsOpcion.getInt("ctr_emb"));
				mapeo.setVerficado(rsOpcion.getString("ctr_ver"));
				mapeo.setIdGreensys(rsOpcion.getInt("ctr_idg"));
				mapeo.setIdProgramacion(rsOpcion.getInt("ctr_prg"));
				
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return mapeo;
	}
	
	public ArrayList<MapeoControlEmbotellado> recuperarControlEmbotellado(String r_articulo, Integer r_numero, Date r_fecha, String r_serie, String r_numeroConsejo)
	{
		ArrayList<MapeoControlEmbotellado> vector = null;
		MapeoControlEmbotellado mapeo = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_control_embotellado.idprd_control ctr_id,");
		cadenaSQL.append(" prd_control_embotellado.fecha ctr_fe,");
		cadenaSQL.append(" prd_control_embotellado.serie ctr_ser,");
		cadenaSQL.append(" prd_control_embotellado.articulo ctr_art,");
		cadenaSQL.append(" prd_control_embotellado.descripcion ctr_des,");
		cadenaSQL.append(" prd_control_embotellado.unidades ctr_uds,");
		cadenaSQL.append(" prd_control_embotellado.litros ctr_ltr,");
		cadenaSQL.append(" prd_control_embotellado.tipo ctr_tip,");
		cadenaSQL.append(" prd_control_embotellado.tipoCalculo ctr_tc,");
		cadenaSQL.append(" prd_control_embotellado.embalaje ctr_emb,");
		cadenaSQL.append(" prd_control_embotellado.cajas1 ctr_cj1,");
		cadenaSQL.append(" prd_control_embotellado.lote1 ctr_lo1,");
		cadenaSQL.append(" prd_control_embotellado.cajas2 ctr_cj2,");
		cadenaSQL.append(" prd_control_embotellado.lote2 ctr_lo2,");
		cadenaSQL.append(" prd_control_embotellado.cajas3 ctr_cj3,");
		cadenaSQL.append(" prd_control_embotellado.lote3 ctr_lo3,");
		cadenaSQL.append(" prd_control_embotellado.cajas4 ctr_cj4,");
		cadenaSQL.append(" prd_control_embotellado.lote4 ctr_lo4, ");
		cadenaSQL.append(" prd_control_embotellado.cajas5 ctr_cj5,");
		cadenaSQL.append(" prd_control_embotellado.lote5 ctr_lo5, ");
		cadenaSQL.append(" prd_control_embotellado.verificado ctr_ver, ");
		cadenaSQL.append(" prd_control_embotellado.idGreensys ctr_idg, ");
		cadenaSQL.append(" prd_control_embotellado.idProgramacion ctr_prg, ");
		cadenaSQL.append(" prd_control_embotellado.observaciones ctr_obs");
		cadenaSQL.append(" from prd_control_embotellado ");
		

		try
		{
			cadenaWhere = new StringBuffer();
			
			if ((r_serie !=null && r_serie.length()>0))
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_control_embotellado.idPrd_control in (select idPrd_control from prd_eti_control_embotellado ");
				cadenaWhere.append(" where serie ='" + r_serie.trim() + "' and prd_control_embotellado.serie = prd_eti_control_embotellado.serieNum ) ");
			}
			
			if ((r_numeroConsejo!=null && r_numeroConsejo.length()>0))
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" prd_control_embotellado.idPrd_control in (select idPrd_control from prd_eti_control_embotellado ");
				cadenaWhere.append(" where desde <= " + r_numeroConsejo.trim() + " and hasta >= " + r_numeroConsejo + " and prd_control_embotellado.serie = prd_eti_control_embotellado.serieNum) ");
			}
			
			if (r_fecha!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" fecha = '" + RutinasFechas.convertirAFechaMysql(RutinasFechas.convertirDateToString(r_fecha)) + "' ");
			}
			
			if (r_numero!=null)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idprd_control = " + r_numero);
			}
			
			if (r_articulo!=null && r_articulo.length()>0)
			{
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" articulo = '" + r_articulo + "' ");
			}

//			if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
//			cadenaWhere.append(" verificado = 'N' ");
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			else
			{
				vector=new ArrayList<MapeoControlEmbotellado>();
				vector.add(new MapeoControlEmbotellado());
				return vector;
			}
			
			cadenaWhere.append(" order by prd_control_embotellado.verificado, prd_control_embotellado.idprd_control ");
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoControlEmbotellado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoControlEmbotellado();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("ctr_id"));
				mapeo.setFecha(rsOpcion.getDate("ctr_fe"));
				mapeo.setSerie(rsOpcion.getString("ctr_ser"));
				mapeo.setArticulo(rsOpcion.getString("ctr_art"));
				mapeo.setDescripcion(rsOpcion.getString("ctr_des"));
				mapeo.setObservaciones(rsOpcion.getString("ctr_obs"));
				mapeo.setCantidad(rsOpcion.getInt("ctr_uds"));
				mapeo.setTipoProduccion(rsOpcion.getString("ctr_tip"));
				mapeo.setTipoCalculo(rsOpcion.getString("ctr_tc"));
				mapeo.setCajas1(rsOpcion.getInt("ctr_cj1"));
				mapeo.setLote1(rsOpcion.getString("ctr_lo1"));
				mapeo.setCajas2(rsOpcion.getInt("ctr_cj2"));
				mapeo.setLote2(rsOpcion.getString("ctr_lo2"));
				mapeo.setCajas3(rsOpcion.getInt("ctr_cj3"));
				mapeo.setLote3(rsOpcion.getString("ctr_lo3"));
				mapeo.setCajas4(rsOpcion.getInt("ctr_cj4"));
				mapeo.setLote4(rsOpcion.getString("ctr_lo4"));
				mapeo.setCajas5(rsOpcion.getInt("ctr_cj5"));
				mapeo.setLote5(rsOpcion.getString("ctr_lo5"));
				mapeo.setLitros(rsOpcion.getInt("ctr_ltr"));
				mapeo.setEmbalaje(rsOpcion.getInt("ctr_emb"));
				mapeo.setVerficado(rsOpcion.getString("ctr_ver"));
				mapeo.setIdGreensys(rsOpcion.getInt("ctr_idg"));
				mapeo.setIdProgramacion(rsOpcion.getInt("ctr_prg"));
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	public ArrayList<MapeoNumeracionConsejo> recuperarNumeracionConsejo(String r_area, String r_numeroControl)
	{
		MapeoNumeracionConsejo mapeo = null;
		ArrayList<MapeoNumeracionConsejo> vector = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_eti_control_embotellado.idprd_control num_id,");
		cadenaSQL.append(" prd_eti_control_embotellado.numero num_rol,");
		cadenaSQL.append(" prd_eti_control_embotellado.serie num_ser,");
		cadenaSQL.append(" prd_eti_control_embotellado.serieNum num_ser_num,");
		cadenaSQL.append(" prd_eti_control_embotellado.desde num_dsd,");
		cadenaSQL.append(" prd_eti_control_embotellado.hasta num_hst ");
		cadenaSQL.append(" from prd_eti_control_embotellado ");

		try
		{
			cadenaWhere = new StringBuffer();
			cadenaWhere.append(" serieNum = '" + r_area+ "' ");
			
			if (r_numeroControl!=null)
			{
				
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idprd_control = '" + r_numeroControl + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoNumeracionConsejo>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoNumeracionConsejo();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("num_id"));
				mapeo.setNumeroRollo(rsOpcion.getInt("num_rol"));
				mapeo.setDesdeNumeracion(rsOpcion.getInt("num_dsd"));
				mapeo.setHastaNumeracion(rsOpcion.getInt("num_hst"));
				if (mapeo.getHastaNumeracion()!=null && mapeo.getDesdeNumeracion()!=null)
				{
					mapeo.setDiferencias(mapeo.getHastaNumeracion()-mapeo.getDesdeNumeracion());
				}
				else
				{
					mapeo.setDiferencias(0);	
				}
				mapeo.setSerie(rsOpcion.getString("num_ser"));
				mapeo.setSerieNum(rsOpcion.getString("num_ser_num"));
				
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public ArrayList<MapeoLineasControlEmbotellado> recuperarEscandallo(String r_area, String r_numeroControl)
	{
		MapeoLineasControlEmbotellado mapeo = null;
		ArrayList<MapeoLineasControlEmbotellado> vector = null;
		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere = null;
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_lin_control_embotellado.idprd_control lin_id,");
		cadenaSQL.append(" prd_lin_control_embotellado.orden lin_ord,");
		cadenaSQL.append(" prd_lin_control_embotellado.serie lin_ser,");
		cadenaSQL.append(" prd_lin_control_embotellado.articulo lin_art,");
		cadenaSQL.append(" prd_lin_control_embotellado.descripcion lin_des,");
		cadenaSQL.append(" prd_lin_control_embotellado.unidades lin_uds, ");
		cadenaSQL.append(" prd_lin_control_embotellado.fecha lin_fec, ");
		cadenaSQL.append(" prd_lin_control_embotellado.origen lin_ori, ");
		cadenaSQL.append(" prd_lin_control_embotellado.mermas lin_mer ");
		cadenaSQL.append(" from prd_lin_control_embotellado ");

		try
		{
			cadenaWhere = new StringBuffer();
			cadenaWhere.append(" serie = '" + r_area + "' ");
			
			if (r_numeroControl!=null)
			{
				
				if (cadenaWhere.length()>0) cadenaWhere.append(" and ");
				cadenaWhere.append(" idprd_control = '" + r_numeroControl + "' ");
			}
			
			if (cadenaWhere!=null && cadenaWhere.length()>0)
			{
				cadenaSQL.append(" where ");
				cadenaSQL.append(cadenaWhere.toString() );
			}
			
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoLineasControlEmbotellado>();
			
			while(rsOpcion.next())
			{
				mapeo = new MapeoLineasControlEmbotellado();
				/*
				 * recojo mapeo operarios
				 */
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("lin_id"));
				mapeo.setOrden(rsOpcion.getInt("lin_ord"));
				mapeo.setSerie(rsOpcion.getString("lin_ser"));
				mapeo.setArticulo(rsOpcion.getString("lin_art"));
				mapeo.setDescripcion(rsOpcion.getString("lin_des"));
				mapeo.setUnidades(rsOpcion.getInt("lin_uds"));
				mapeo.setMermas(rsOpcion.getInt("lin_mer"));
				mapeo.setFecha(rsOpcion.getDate("lin_fec"));
				mapeo.setOrigen(rsOpcion.getInt("lin_ori"));
				vector.add(mapeo);
			}
				
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return vector;
	}
	
	public Integer obtenerStatus(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT prd_control_embotellado.status pro_st ");
     	cadenaSQL.append(" FROM prd_control_embotellado ");
     	cadenaSQL.append(" where prd_control_embotellado.idprd_control = " + r_id);
		try
		{
     	
			con= this.conManager.establecerConexion();			
			cs = con.createStatement(rsOpcion.TYPE_SCROLL_SENSITIVE,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return rsOpcion.getInt("pro_st");
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		return null;
	}
	
	public boolean guardarDatos(MapeoControlEmbotellado r_mapeoControlEmbotellado, ArrayList<MapeoLineasControlEmbotellado> r_vectorEscandallo, ArrayList<MapeoNumeracionConsejo>  r_vectorNumeracionConsejo)
	{
		boolean resultado = false;
		boolean elim = false;
		String rdo = null;
		elim = this.eliminar(r_mapeoControlEmbotellado.getSerie(), r_mapeoControlEmbotellado.getIdControlEmbotellado(), r_mapeoControlEmbotellado.getIdGreensys());
		
		if (elim)
		{
			rdo = this.guardarNuevoControlEmbotellado(r_mapeoControlEmbotellado);

			if (rdo==null)
			{
				consultaContadoresEjercicioServer ccs = consultaContadoresEjercicioServer.getInstance(CurrentUser.get());
				
				Long actual = ccs.recuperarContador(new Integer(RutinasFechas.añoActualYYYY()), "PRODUCCION", r_mapeoControlEmbotellado.getSerie());
				
				if (r_mapeoControlEmbotellado.getIdControlEmbotellado() == actual.intValue())
				{
					ccs.actualizarContador(new Integer(RutinasFechas.añoActualYYYY()), actual.doubleValue(), "PRODUCCION", r_mapeoControlEmbotellado.getSerie());
				}
				
				rdo = this.guardarNuevoLineasControlEmbotellado(r_mapeoControlEmbotellado.getSerie(), r_mapeoControlEmbotellado.getIdControlEmbotellado(), r_vectorEscandallo);
				if (rdo==null)
				{
					rdo = this.guardarNuevoNumeracionConsejoControlEmbotellado(r_mapeoControlEmbotellado.getSerie(), r_mapeoControlEmbotellado.getIdControlEmbotellado(), r_vectorNumeracionConsejo);
					
					if (rdo==null) resultado = true;
					else
					{
						serNotif.mensajeError("No se han guardado las numeraciones del consejo del control de embotellado");
					}
				}
				else
				{
					serNotif.mensajeError("No se han guardado las lineas del control de embotellado");
				}
			}
			else
			{
				serNotif.mensajeError("Problemas al guardar el control de embotellado");
			}
		}
		else
		{
			/*
			 * esta validado y por lo tanto no podemos hacer grabacion masiva.
			 * 
			 * si el usuario es master vamos a actualizar:
			 *  
			 *  - los datos de las numeraciones
			 *  
			 */
			
			if (eBorsao.get().accessControl.isUserInRole(eBorsao.get().accessControl.getDepartamento()).equals("Master"))
			{
				elim = this.eliminarEtiquetas(r_mapeoControlEmbotellado.getSerie(), r_mapeoControlEmbotellado.getIdControlEmbotellado(), r_mapeoControlEmbotellado.getIdGreensys());
				if (elim)
				{
					rdo = this.guardarNuevoNumeracionConsejoControlEmbotellado(r_mapeoControlEmbotellado.getSerie(), r_mapeoControlEmbotellado.getIdControlEmbotellado(), r_vectorNumeracionConsejo);
					
					if (rdo==null)
					{
						resultado = true;
						serNotif.mensajeError("Se han actualizado las numeraciones del consejo");
					}
					else
					{
						serNotif.mensajeError("No se han guardado las numeraciones del consejo del control de embotellado");
					}
				}
				else
				{
					serNotif.mensajeError("No hay numeraciones a modificar");
				}
			}
		}
		return resultado;
	}
	
	private String guardarNuevoControlEmbotellado(MapeoControlEmbotellado r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" INSERT INTO prd_control_embotellado( ");
			cadenaSQL.append(" prd_control_embotellado.idprd_control,");
			cadenaSQL.append(" prd_control_embotellado.fecha, ");
			cadenaSQL.append(" prd_control_embotellado.serie, ");
			cadenaSQL.append(" prd_control_embotellado.articulo, ");
			cadenaSQL.append(" prd_control_embotellado.descripcion, ");
			cadenaSQL.append(" prd_control_embotellado.unidades, ");			
			cadenaSQL.append(" prd_control_embotellado.tipo, ");
			cadenaSQL.append(" prd_control_embotellado.tipoCalculo, ");
			cadenaSQL.append(" prd_control_embotellado.cajas1, ");
			cadenaSQL.append(" prd_control_embotellado.lote1, ");
			cadenaSQL.append(" prd_control_embotellado.cajas2, ");
			cadenaSQL.append(" prd_control_embotellado.lote2, ");
			cadenaSQL.append(" prd_control_embotellado.cajas3, ");
			cadenaSQL.append(" prd_control_embotellado.lote3, ");
			cadenaSQL.append(" prd_control_embotellado.cajas4, ");
			cadenaSQL.append(" prd_control_embotellado.lote4, ");
			cadenaSQL.append(" prd_control_embotellado.cajas5, ");
			cadenaSQL.append(" prd_control_embotellado.lote5, ");			
			cadenaSQL.append(" prd_control_embotellado.embalaje, ");
			cadenaSQL.append(" prd_control_embotellado.litros, ");
			cadenaSQL.append(" prd_control_embotellado.observaciones, ");
			cadenaSQL.append(" prd_control_embotellado.verificado, ");
			cadenaSQL.append(" prd_control_embotellado.idGreensys, ");
			cadenaSQL.append(" prd_control_embotellado.idProgramacion) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
//		    r_mapeo.setIdCodigo(this.obtenerSiguiente());

		    preparedStatement.setInt(1, r_mapeo.getIdControlEmbotellado());
		    
		    if (r_mapeo.getFecha()!=null)
		    {
		    	preparedStatement.setDate(2, new java.sql.Date(r_mapeo.getFecha().getTime()));
		    }
		    else
		    {
		    	preparedStatement.setDate(2, null);
		    }
		    if (r_mapeo.getSerie()!=null)
		    {
		    	preparedStatement.setString(3, r_mapeo.getSerie());
		    }
		    else
		    {
		    	preparedStatement.setString(3, null);
		    }
		    if (r_mapeo.getArticulo()!=null)
		    {
		    	preparedStatement.setString(4, r_mapeo.getArticulo());
		    }
		    else
		    {
		    	preparedStatement.setString(4, null);
		    }
		    if (r_mapeo.getDescripcion()!=null)
		    {
		    	preparedStatement.setString(5, r_mapeo.getDescripcion());
		    }
		    else
		    {
		    	preparedStatement.setString(5, null);
		    }
		    if (r_mapeo.getCantidad()!=null)
		    {
		    	preparedStatement.setInt(6, r_mapeo.getCantidad());
		    }
		    else
		    {
		    	preparedStatement.setInt(6, 0);
		    }
		    
		    if (r_mapeo.getTipoProduccion()!=null)
		    {
		    	preparedStatement.setString(7, r_mapeo.getTipoProduccion());
		    }
		    else
		    {
		    	preparedStatement.setString(7, null);
		    }
		    if (r_mapeo.getTipoCalculo()!=null)
		    {
		    	preparedStatement.setString(8, r_mapeo.getTipoCalculo());
		    }
		    else
		    {
		    	preparedStatement.setString(8, null);
		    }
		    
		    if (r_mapeo.getCajas1()!=null)
		    {
		    	preparedStatement.setInt(9, r_mapeo.getCajas1());
		    }
		    else
		    {
		    	preparedStatement.setInt(9, 0);
		    }
		    if (r_mapeo.getLote1()!=null)
		    {
		    	preparedStatement.setString(10, r_mapeo.getLote1());
		    }
		    else
		    {
		    	preparedStatement.setString(10, null);
		    }
		    if (r_mapeo.getCajas2()!=null)
		    {
		    	preparedStatement.setInt(11, r_mapeo.getCajas2());
		    }
		    else
		    {
		    	preparedStatement.setInt(11, 0);
		    }
		    if (r_mapeo.getLote2()!=null)
		    {
		    	preparedStatement.setString(12, r_mapeo.getLote2());
		    }
		    else
		    {
		    	preparedStatement.setString(12, null);
		    }
		    if (r_mapeo.getCajas3()!=null)
		    {
		    	preparedStatement.setInt(13, r_mapeo.getCajas3());
		    }
		    else
		    {
		    	preparedStatement.setInt(13, 0);
		    }
		    if (r_mapeo.getLote3()!=null)
		    {
		    	preparedStatement.setString(14, r_mapeo.getLote3());
		    }
		    else
		    {
		    	preparedStatement.setString(14, null);
		    }
		    if (r_mapeo.getCajas4()!=null)
		    {
		    	preparedStatement.setInt(15, r_mapeo.getCajas4());
		    }
		    else
		    {
		    	preparedStatement.setInt(15, 0);
		    }
		    if (r_mapeo.getLote4()!=null)
		    {
		    	preparedStatement.setString(16, r_mapeo.getLote4());
		    }
		    else
		    {
		    	preparedStatement.setString(16, null);
		    }
		    if (r_mapeo.getCajas5()!=null)
		    {
		    	preparedStatement.setInt(17, r_mapeo.getCajas5());
		    }
		    else
		    {
		    	preparedStatement.setInt(17, 0);
		    }
		    if (r_mapeo.getLote5()!=null)
		    {
		    	preparedStatement.setString(18, r_mapeo.getLote5());
		    }
		    else
		    {
		    	preparedStatement.setString(18, null);
		    }
		    if (r_mapeo.getEmbalaje()!=null)
		    {
		    	preparedStatement.setInt(19, r_mapeo.getEmbalaje());
		    }
		    else
		    {
		    	preparedStatement.setInt(19, 0);
		    }
		    if (r_mapeo.getLitros()!=null)
		    {
		    	preparedStatement.setInt(20, r_mapeo.getLitros());
		    }
		    else
		    {
		    	preparedStatement.setInt(20, 0);
		    }
		    if (r_mapeo.getObservaciones()!=null)
		    {
		    	preparedStatement.setString(21, r_mapeo.getObservaciones());
		    }
		    else
		    {
		    	preparedStatement.setString(21, null);
		    }
		    preparedStatement.setString(22, "N");
		    preparedStatement.setInt(23, 0);
		    if (r_mapeo.getIdProgramacion()!=null)
		    {
		    	preparedStatement.setInt(24, r_mapeo.getIdProgramacion());
		    }
		    else
		    {
		    	preparedStatement.setInt(24, 0);
		    }
		    
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public String actualizarControlEmbotellado(MapeoControlEmbotellado r_mapeo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = new StringBuffer();  

		try
		{
			
			cadenaSQL.append(" UPDATE prd_control_embotellado set ");
			cadenaSQL.append(" prd_control_embotellado.idGreensys = ? ");
			cadenaSQL.append(" where prd_control_embotellado.idprd_control = ? ");
			cadenaSQL.append(" and prd_control_embotellado.serie = ? ");
			
		    con= this.conManager.establecerConexion();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
//		    r_mapeo.setIdCodigo(this.obtenerSiguiente());

		    preparedStatement.setInt(1, r_mapeo.getIdGreensys());
		    preparedStatement.setInt(2, r_mapeo.getIdControlEmbotellado());
		    preparedStatement.setString(3, r_mapeo.getSerie());
		    
	        preparedStatement.executeUpdate();
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			if (preparedStatement!=null)
			{
				preparedStatement=null;
			}
			if (con!=null)
			{
				con=null;
			}			
		}
		return null;
	}
	
	private String guardarNuevoLineasControlEmbotellado(String r_area, Integer r_idControl, ArrayList<MapeoLineasControlEmbotellado> r_vectorEscandallo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = null;  

		try
		{
			
			for (int i=0;i<r_vectorEscandallo.size();i++)
			{
				cadenaSQL = new StringBuffer();
				MapeoLineasControlEmbotellado r_mapeo = r_vectorEscandallo.get(i);
			
				cadenaSQL.append(" INSERT INTO prd_lin_control_embotellado( ");
				cadenaSQL.append(" prd_lin_control_embotellado.idprd_control,");
				cadenaSQL.append(" prd_lin_control_embotellado.serie,");
				cadenaSQL.append(" prd_lin_control_embotellado.orden, ");
				cadenaSQL.append(" prd_lin_control_embotellado.articulo, ");
				cadenaSQL.append(" prd_lin_control_embotellado.descripcion, ");
				cadenaSQL.append(" prd_lin_control_embotellado.unidades, ");			
				cadenaSQL.append(" prd_lin_control_embotellado.mermas, ");
				cadenaSQL.append(" prd_lin_control_embotellado.origen ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?) ");
				
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
			    preparedStatement.setInt(1, r_idControl);
			    preparedStatement.setString(2, r_area);
	    		preparedStatement.setInt(3, i+1);
	    		
	    		if (r_mapeo.getArticulo()!=null)
	    		{
	    			preparedStatement.setString(4, r_mapeo.getArticulo());
	    		}
	    		else
	    		{
	    			preparedStatement.setString(4, null);
	    		}
			    if (r_mapeo.getDescripcion()!=null)
			    {
			    	preparedStatement.setString(5, r_mapeo.getDescripcion());
			    }
			    else
			    {
			    	preparedStatement.setString(5, null);
			    }
			    if (r_mapeo.getUnidades()!=null)
			    {
			    	preparedStatement.setInt(6, r_mapeo.getUnidades());
			    }
			    else
			    {
			    	preparedStatement.setInt(6, 0);
			    }
		    
			    if (r_mapeo.getMermas()!=null)
			    {
			    	preparedStatement.setInt(7, r_mapeo.getMermas());
			    }
			    else
			    {
			    	preparedStatement.setInt(7, 0);
			    }
			    if (r_mapeo.getOrigen()!=null)
			    {
			    	preparedStatement.setInt(8, r_mapeo.getOrigen());
			    }
			    else
			    {
			    	preparedStatement.setInt(8, 0);
			    }
		    
			    preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	private String guardarNuevoNumeracionConsejoControlEmbotellado(String r_area, Integer r_numerador, ArrayList<MapeoNumeracionConsejo> r_vectorNumeracionConsejo)
	{
		PreparedStatement preparedStatement = null;
		
		StringBuffer cadenaSQL = null;  

		try
		{
			
			for (int i=0;i<r_vectorNumeracionConsejo.size();i++)
			{
				cadenaSQL = new StringBuffer();
				MapeoNumeracionConsejo r_mapeo = r_vectorNumeracionConsejo.get(i);
				
				cadenaSQL.append(" INSERT INTO prd_eti_control_embotellado( ");
				cadenaSQL.append(" prd_eti_control_embotellado.idprd_control,");
				cadenaSQL.append(" prd_eti_control_embotellado.serieNum,");
				cadenaSQL.append(" prd_eti_control_embotellado.numero, ");
				cadenaSQL.append(" prd_eti_control_embotellado.serie, ");
				cadenaSQL.append(" prd_eti_control_embotellado.desde, ");
				cadenaSQL.append(" prd_eti_control_embotellado.hasta ) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?) ");
				
			    con= this.conManager.establecerConexion();	
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		    
			    preparedStatement.setInt(1, r_numerador);
			    preparedStatement.setString(2, r_area);
	    		preparedStatement.setInt(3, r_mapeo.getNumeroRollo());
	    		preparedStatement.setString(4, r_mapeo.getSerie());
	    		preparedStatement.setInt(5, r_mapeo.getDesdeNumeracion());
	    		preparedStatement.setInt(6, r_mapeo.getHastaNumeracion());
	    		
			    preparedStatement.executeUpdate();
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		return null;
	}
	
	public boolean eliminar(String r_area, Integer r_idControl, Integer r_idGreensys)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		boolean rdo = false;
		try
		{
			
			if (r_idGreensys!=null)
			{
				rdo = this.obtenerExisteGreensys(r_idGreensys);
				if (rdo=true) return false;
			}
			
			cadenaSQL.append(" DELETE FROM prd_control_embotellado ");
			cadenaSQL.append(" WHERE prd_control_embotellado.idprd_control = " + r_idControl);
			cadenaSQL.append(" and prd_control_embotellado.serie = '" + r_area + "' ");
			con= this.conManager.establecerConexion();	          
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_eti_control_embotellado ");
			cadenaSQL.append(" WHERE prd_eti_control_embotellado.idprd_control = " + r_idControl);
			cadenaSQL.append(" and prd_eti_control_embotellado.serieNum = '" + r_area + "' ");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_lin_control_embotellado ");
			cadenaSQL.append(" WHERE prd_lin_control_embotellado.idprd_control = " + r_idControl);
			cadenaSQL.append(" and prd_lin_control_embotellado.serie = '" + r_area + "' ");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_rot_control_embotellado ");
			cadenaSQL.append(" WHERE prd_rot_control_embotellado.idprd_control = " + r_idControl);
			cadenaSQL.append(" and prd_rot_control_embotellado.serie = '" + r_area + "' ");
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();

			rdo = true;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		return rdo;
	}

	private boolean eliminarEtiquetas(String r_area, Integer r_idControl, Integer r_idGreensys)
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();        
		boolean rdo = false;
		try
		{
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" DELETE FROM prd_eti_control_embotellado ");
			cadenaSQL.append(" WHERE prd_eti_control_embotellado.idprd_control = " + r_idControl);
			cadenaSQL.append(" and prd_eti_control_embotellado.serieNum = '" + r_area + "' ");
			con= this.conManager.establecerConexion();
			
			preparedStatement = con.prepareStatement(cadenaSQL.toString());
			preparedStatement.executeUpdate();

			rdo = true;
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());	
		}
		return rdo;
	}

	
	/*
	 * METODOS SOBRE GREENsys
	 */
	
	public Boolean obtenerStatusGreensys(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a__oml_0.orden omc_id ");
     	cadenaSQL.append(" FROM a__oml_0 ");
     	cadenaSQL.append(" where a__oml_0.orden = " + r_id);
     	cadenaSQL.append(" and a__oml_0.estado = 3 ");
     	
		try
		{
     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return false;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return true;
	}
	
	public Boolean obtenerExisteGreensys(Integer r_id)
	{
		ResultSet rsOpcion = null;		
		Connection con = null;
		Statement cs = null;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		cadenaSQL.append(" SELECT a__oml_0.orden omc_id ");
     	cadenaSQL.append(" FROM a__oml_0 ");
     	cadenaSQL.append(" where a__oml_0.orden = " + r_id);
     	
		try
		{
     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				return true;
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return false;
	}
	
	public ArrayList<MapeoLineasControlEmbotellado> generarDatosEscandallo(String r_articulo, Integer r_cantidad)
	{
		ArrayList<MapeoLineasControlEmbotellado> vector = null;
		vector = this.datosEscandallo(r_articulo, r_cantidad);
		return vector;
	}
	
	
	private ArrayList<MapeoLineasControlEmbotellado> datosEscandallo(String r_articulo, Integer r_cantidad)
	{
		ResultSet rsOpcion = null;
		Connection conG = null;
		Statement csG = null;
		
		ArrayList<MapeoLineasControlEmbotellado> vector_2 = null;
		MapeoLineasControlEmbotellado mapeoLineasControl = null; 
		
		try
		{

			StringBuffer cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT unique b.hijo esc_art, ");
	        cadenaSQL.append(" b.descripcion esc_des, ");
	        cadenaSQL.append(" b.cantidad esc_can, ");
	        cadenaSQL.append(" 0 esc_mer ");
	        
	     	cadenaSQL.append(" FROM a_exis_0 a, e__lconj b ");
	     	/*
	     	 * TODO Agregar inner con articulos para traer el almacen del articulo
	     	 */
	     	cadenaSQL.append(" where b.hijo = a.articulo ");
	     	cadenaSQL.append(" and b.padre = '" + r_articulo.trim() + "' ");
	     	cadenaSQL.append(" and a.almacen in (1,4) ");
	     	cadenaSQL.append(" order by 1 ");

	     	conG = this.conManager.establecerConexionGestionInd();			
	     	
	     	csG = conG.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);

	     	Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
	     	rsOpcion = csG.executeQuery(cadenaSQL.toString());
		
			vector_2 = new ArrayList<MapeoLineasControlEmbotellado>();
			
			while(rsOpcion.next())
			{
				
				mapeoLineasControl = new MapeoLineasControlEmbotellado();
				mapeoLineasControl.setArticulo(rsOpcion.getString("esc_art"));
				mapeoLineasControl.setDescripcion(RutinasCadenas.conversion(rsOpcion.getString("esc_des")));
				
				Double factor = rsOpcion.getDouble("esc_can")*r_cantidad;
				
				if (mapeoLineasControl.getArticulo().startsWith("0109") && (!r_articulo.startsWith("0111") && !r_articulo.startsWith("0103004") && !r_articulo.startsWith("0103104")))
				{
					consultaEscandalloServer ces= new consultaEscandalloServer(CurrentUser.get());
					Integer cantidadCaja = ces.recogerTipoCaja(r_articulo,mapeoLineasControl.getArticulo().toString());
					
					mapeoLineasControl.setUnidades(new Double(Math.floor(r_cantidad / cantidadCaja)).intValue());
					ces = null;
				}
				else if (mapeoLineasControl.getArticulo().startsWith("0109") && !r_articulo.startsWith("0111") && (r_articulo.startsWith("0103004") || r_articulo.startsWith("0103104")))
				{
					consultaEscandalloServer ces= new consultaEscandalloServer(CurrentUser.get());
					Integer cantidadCaja = ces.recogerTipoCaja(r_articulo,mapeoLineasControl.getArticulo());
					
					mapeoLineasControl.setUnidades(new Double(Math.floor(r_cantidad / cantidadCaja)).intValue());
					ces = null;
				}

				else
				{
					mapeoLineasControl.setUnidades(factor.intValue());
				}
				
				mapeoLineasControl.setMermas(0);
				mapeoLineasControl.setOrigen(0);
				/*
				 * TODO Rellenar almacen ficha articulo
				 */
				vector_2.add(mapeoLineasControl);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (csG !=null)
				{
					csG.close();
					csG=null;
				}
				if (conG!=null)
				{
					conG.close();
					conG=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return vector_2;
	}
	
	public String guardarControlEmbotelladoGreensys(MapeoControlEmbotellado r_mapeo, ArrayList<MapeoLineasControlEmbotellado> r_vectorEscandallo)
	{
		PreparedStatement preparedStatement = null;
		Connection con = null;
		StringBuffer cadenaSQL = null;  
		if ((r_mapeo.getArticulo().startsWith("0103") || r_mapeo.getArticulo().startsWith("0111")) && !almacen.equals("1"))
		{
			return "Este usuario no debe procesar estos controles de embotellado";
		}
			
		try
		{
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO a__omc_0( ");
			cadenaSQL.append(" a__omc_0.orden,");
			cadenaSQL.append(" a__omc_0.pedido, ");
			cadenaSQL.append(" a__omc_0.estado) VALUES (");
			cadenaSQL.append(" ?,?,?) ");
			
		    con= this.conManager.establecerConexionGestionInd();	
		    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
	    
		    preparedStatement.setInt(1, r_mapeo.getIdGreensys());
    		preparedStatement.setString(2, r_mapeo.getIdControlEmbotellado() + "/" + RutinasFechas.añoFecha(r_mapeo.getFecha()));
    		preparedStatement.setInt(3, 0);

    		preparedStatement.executeUpdate();

			for (int i=0;i<r_vectorEscandallo.size();i++)
			{
				MapeoLineasControlEmbotellado mapeo = r_vectorEscandallo.get(i);
				if (mapeo.getMermas()!=0 && !mapeo.getArticulo().startsWith("0102"))
				{
					cadenaSQL = new StringBuffer();
					cadenaSQL.append(" INSERT INTO a__oml_0( ");
					cadenaSQL.append(" a__oml_0.orden, ");
					cadenaSQL.append(" a__oml_0.linea,");
					cadenaSQL.append(" a__oml_0.articulo, ");
					cadenaSQL.append(" a__oml_0.almacen, ");
					cadenaSQL.append(" a__oml_0.fecha, ");			
					cadenaSQL.append(" a__oml_0.cantidad, ");
					cadenaSQL.append(" a__oml_0.fabricar, ");
					cadenaSQL.append(" a__oml_0.servido, ");
					cadenaSQL.append(" a__oml_0.pendiente, ");
					cadenaSQL.append(" a__oml_0.pedido, ");
					cadenaSQL.append(" a__oml_0.estado, ");
					cadenaSQL.append(" a__oml_0.usuario, ");
					cadenaSQL.append(" a__oml_0.fecha_mod, ");
					cadenaSQL.append(" a__oml_0.hora, ");
					cadenaSQL.append(" a__oml_0.lote, ");
					cadenaSQL.append(" a__oml_0.anada ) VALUES (");
					cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
					
//				    con= this.conManager.establecerConexionGestion();	
				    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			    
				    preparedStatement.setInt(1, r_mapeo.getIdGreensys());
		    		preparedStatement.setInt(2, mapeo.getOrden());
		    		
		    		if (r_mapeo.getArticulo()!=null)
		    		{
		    			preparedStatement.setString(3, mapeo.getArticulo());
		    		}
		    		else
		    		{
		    			preparedStatement.setString(3, null);
		    		}

		    		preparedStatement.setInt(4, new Integer(almacen));
		    		/*
		    		 * TODO Traer el almacen de la ficha del articulo
		    		 */
		    		
				    if (r_mapeo.getFecha()!=null)
				    {
				    	preparedStatement.setString(5, RutinasFechas.convertirDateToString(r_mapeo.getFecha()));
				    }
				    else
				    {
				    	preparedStatement.setString(5, null);
				    }
			    
				    if (mapeo.getMermas()!=null)
				    {
				    	preparedStatement.setInt(6, mapeo.getMermas());
				    }
				    else
				    {
				    	preparedStatement.setInt(6, 0);
				    }
				    if (mapeo.getMermas()!=null)
				    {
				    	preparedStatement.setInt(7, mapeo.getMermas());
				    }
				    else
				    {
				    	preparedStatement.setInt(7, 0);
				    }
				    preparedStatement.setInt(8, 0);
				    preparedStatement.setInt(9, 0);
				    preparedStatement.setString(10, null);
				    preparedStatement.setInt(11, 2);
				    preparedStatement.setString(12, "almacen");
				    preparedStatement.setString(13, RutinasFechas.fechaActual());
				    preparedStatement.setString(14, RutinasFechas.horaActual());
				    preparedStatement.setInt(15, 0);
				    preparedStatement.setString(16, null);
				    preparedStatement.executeUpdate();
				}
			}
		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    	return ex.getMessage();
	    }        
		finally
		{
			try
			{
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return null;
	}
	
	public Integer mermasSemanal(String r_linea, Integer r_ejercicio, Integer r_semana)
	{
		Integer valor = null;
		
		consultaMermasPtServer cmpts = consultaMermasPtServer.getInstance(CurrentUser.get());
		valor = cmpts.mermasSemanalProduccion(r_linea, r_ejercicio, r_semana);
		return valor;
	}
	
	public ArrayList<MapeoLineasControlEmbotellado> recuperarMermasErroneas()
	{
		ResultSet rsOpcion = null;
		StringBuffer cadenaSQL =null;
		ArrayList<MapeoLineasControlEmbotellado> vector=null;
		Connection con = null;
		Statement cs = null;
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT a.orden mer_id, ");
			cadenaSQL.append(" a.articulo mer_ar, ");
			cadenaSQL.append(" a.cantidad mer_ud, ");
			cadenaSQL.append(" a.fecha mer_fec ");
	     	cadenaSQL.append(" FROM a__oml_0 a ");
	     	cadenaSQL.append(" where a.estado = 2 ");
	     	cadenaSQL.append(" and a.fecha < '" + RutinasFechas.fechaActual() + "' ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			vector = new ArrayList<MapeoLineasControlEmbotellado>();
			while(rsOpcion.next())
			{
				MapeoLineasControlEmbotellado mapeo = new MapeoLineasControlEmbotellado();
				mapeo.setIdControlEmbotellado(rsOpcion.getInt("mer_id"));
				mapeo.setArticulo(rsOpcion.getString("mer_ar"));
				mapeo.setFecha(rsOpcion.getDate("mer_fec"));
				mapeo.setUnidades(rsOpcion.getInt("mer_ud"));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return vector ;
	}
	
	public Integer obtenerSiguienteGreensys()
	{
		ResultSet rsOpcion = null;		
		Integer numerador = null;
		StringBuffer cadenaSQL = new StringBuffer();
		PreparedStatement preparedStatement = null;
		Connection con =null;
		Statement cs = null;
		cadenaSQL.append(" SELECT max(a__omc_0.orden) omc_id ");
     	cadenaSQL.append(" FROM a__omc_0 ");

		try
		{
     	
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			while(rsOpcion.next())
			{
				numerador= rsOpcion.getInt("omc_id") + 1 ;
			}

			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" update e_numero ");
			cadenaSQL.append(" set e_numero.numero = ? ");
			cadenaSQL.append(" where e_numero.clave_tabla ='B' ");
			cadenaSQL.append(" and e_numero.serie = 'o' ");
			
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			preparedStatement.setInt(1, numerador + 1 );
			
			preparedStatement.executeUpdate();
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
		}
		
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (preparedStatement !=null)
				{
					preparedStatement.close();
					preparedStatement=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return numerador;
	}
	
	private void guardarDatosExplosion(ArrayList<MapeoAyudaProduccionEscandallo> r_vector2) throws Exception
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		/*
		 * Borramos el contenido de la tabla de explosion de materiales
		 */
		cadenaSQL.append(" delete from lst_explosionmp ");
		con= this.conManager.establecerConexion();	
		preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		preparedStatement.executeUpdate();
		
		/*
		 * para el vector recibido guardamos los registros en la tabla
		 */
		for (int i=0;i<r_vector2.size();i++)
		{
			MapeoAyudaProduccionEscandallo mapeo = (MapeoAyudaProduccionEscandallo) r_vector2.get(i);
			 	/*
			 	 * insertamos los datos del vector que tenemos en pantalla
			 	 */
			 	cadenaSQL = new StringBuffer();
				cadenaSQL.append(" INSERT INTO lst_explosionmp( ");
				cadenaSQL.append(" lst_explosionmp.mp_almacen,");
				cadenaSQL.append(" lst_explosionmp.mp_nombre,");
				cadenaSQL.append(" lst_explosionmp.mp_articulo,");
				cadenaSQL.append(" lst_explosionmp.mp_descripcion,");
				cadenaSQL.append(" lst_explosionmp.mp_cantidad,");
				cadenaSQL.append(" lst_explosionmp.mp_pdte_rec,");
				cadenaSQL.append(" lst_explosionmp.mp_fecha,");
				cadenaSQL.append(" lst_explosionmp.mp_ubicacion,");
				cadenaSQL.append(" lst_explosionmp.mp_necesito,");
				cadenaSQL.append(" lst_explosionmp.mp_diferencia,");
				cadenaSQL.append(" lst_explosionmp.mp_almacen3) VALUES (");
				cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?,?) ");
				
			    preparedStatement = con.prepareStatement(cadenaSQL.toString()); 

			    if (mapeo.getAlmacen()!=null)
			    {
			    	preparedStatement.setString(1, mapeo.getAlmacen());
			    }
			    else
			    {
			    	preparedStatement.setString(1, null);
			    }
			    
			    if (mapeo.getNombreAlmacen()!=null)
			    {
			    	preparedStatement.setString(2, mapeo.getNombreAlmacen());
			    }
			    else
			    {
			    	preparedStatement.setString(2, null);
			    }
			    
			    if (mapeo.getArticulo()!=null)
			    {
			    	preparedStatement.setString(3,mapeo.getArticulo());
			    }
			    else
			    {
			    	preparedStatement.setString(3, null);
			    }
			    if (mapeo.getDescripcion()!=null)
			    {
			    	preparedStatement.setString(4, mapeo.getDescripcion());
			    }
			    else
			    {
			    	preparedStatement.setString(4, null);
			    }
			    preparedStatement.setInt(5, mapeo.getExistencias());
			    preparedStatement.setInt(6, mapeo.getPendienteRecibir());
			    
			    if (mapeo.getFechaEntrega()!=null)
			    {
			    	preparedStatement.setString(7, RutinasFechas.convertirAFechaMysql(mapeo.getFechaEntrega()));
			    }
			    else
			    {
			    	preparedStatement.setString(7, null);
			    }
			    if (mapeo.getUbicacion()!=null)
			    {
			    	preparedStatement.setString(8, mapeo.getUbicacion());
			    }
			    else
			    {
			    	preparedStatement.setString(8, null);
			    }
			    preparedStatement.setInt(9, mapeo.getNecesito());
			    preparedStatement.setInt(10, mapeo.getFaltansobran());
			    if (mapeo.isExistencias3()==true)
			    {
			    	preparedStatement.setString(11, "S");
			    }
			    else
			    {
			    	preparedStatement.setString(11, "N");
			    }
		        preparedStatement.executeUpdate();
			       
		}
	}

	private void guardarDatosExplosionSGA(ArrayList<String> r_listaOrdenes,ArrayList<String> r_listaArticulos, String r_lote) throws Exception
	{
		PreparedStatement preparedStatement = null;
		StringBuffer cadenaSQL = new StringBuffer();  
		
		/*
		 * Borramos el contenido de la tabla de explosion de materiales
		 */
		cadenaSQL.append(" delete from lst_explosionmpSGA ");
		con= this.conManager.establecerConexion();	
		preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
		preparedStatement.executeUpdate();
		
		/*
		 * para el vector recibido guardamos los registros en la tabla
		 */
		ArrayList<MapeoAyudaProduccionSGA> vector = this.traerDatosSGA(r_listaOrdenes,r_listaArticulos, r_lote);

		for (int i=0;i<vector.size();i++)
		{
			/*
			 * insertamos los datos del vector que tenemos en pantalla
			 */
			MapeoAyudaProduccionSGA mapeo = vector.get(i);
			
			cadenaSQL = new StringBuffer();
			cadenaSQL.append(" INSERT INTO lst_explosionmpSGA( ");
			cadenaSQL.append(" lst_explosionmpSGA.mp_orden,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_pick,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_articulo,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_descripcion,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_gtin,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_lpn,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_lote,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_ubicacion,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_cantidad,");
			cadenaSQL.append(" lst_explosionmpSGA.mp_pt) VALUES (");
			cadenaSQL.append(" ?,?,?,?,?,?,?,?,?,?) ");
			
			preparedStatement = con.prepareStatement(cadenaSQL.toString()); 
			
			if (mapeo.getOrden()!=null)
			{
				preparedStatement.setString(1, mapeo.getOrden());
			}
			else
			{
				preparedStatement.setString(1, null);
			}
			
			if (mapeo.getPick()!=null)
			{
				preparedStatement.setString(2, mapeo.getPick());
			}
			else
			{
				preparedStatement.setString(2, null);
			}
			
			if (mapeo.getArticulo()!=null)
			{
				preparedStatement.setString(3,mapeo.getArticulo());
			}
			else
			{
				preparedStatement.setString(3, null);
			}
			if (mapeo.getDescripcion()!=null)
			{
				preparedStatement.setString(4, mapeo.getDescripcion());
			}
			else
			{
				preparedStatement.setString(4, null);
			}
			if (mapeo.getGtin()!=null)
			{
				preparedStatement.setString(5, mapeo.getGtin());
			}
			else
			{
				preparedStatement.setString(5, null);
			}
			if (mapeo.getLpn()!=null)
			{
				preparedStatement.setString(6, mapeo.getLpn());
			}
			else
			{
				preparedStatement.setString(6, null);
			}
			if (mapeo.getLote()!=null)
			{
				preparedStatement.setDouble(7, new Double(mapeo.getLote()));
			}
			else
			{
				preparedStatement.setString(7, null);
			}
			
			if (mapeo.getUbicacion()!=null)
			{
				preparedStatement.setString(8, mapeo.getUbicacion());
			}
			else
			{
				preparedStatement.setString(8, null);
			}
			preparedStatement.setInt(9, mapeo.getCantidad());
			if (mapeo.getPt()!=null)
			{
				preparedStatement.setString(10, mapeo.getPt());
			}
			else
			{
				preparedStatement.setString(10, null);
			}
			
			preparedStatement.executeUpdate();
			
		}
	}
	
	
	private ArrayList<MapeoAyudaProduccionSGA> traerDatosSGALista(ArrayList<String> r_listaOrdenes, ArrayList<String> r_listaArticulos)
	{
		ArrayList<MapeoAyudaProduccionSGA> vector = null;
		
		Connection con = null;	
		Statement cs = null;

		ResultSet rsOpcion = null;		
		StringBuffer cadenaWhere =null;
		Connection conDelete = null;
		Boolean insertado = false;
		Boolean borrado = false;
		
		StringBuffer cadenaSQL = new StringBuffer();
		
		//
		// sql para lista materiales objective
		//
		
		cadenaSQL.append(" SELECT ORDLIST.NAME AS pickList, ");
		cadenaSQL.append(" prodorder.NAME as idOrden, ");
        cadenaSQL.append(" ART.NAME AS articulo, ");
        cadenaSQL.append(" ART.DESCRIPTION AS descripcion, ");
        cadenaSQL.append(" ART.EANCODE AS gtin, ");
        cadenaSQL.append(" PALET.NAME AS lpn, ");        
        cadenaSQL.append(" LIST.QTY qty, ");        
        cadenaSQL.append(" UBIC.FULLNAME AS ubic ");
        
        cadenaSQL.append(" from OBJT_PICKORDER AS PICK ");
        cadenaSQL.append(" LEFT JOIN OBJT_TASK AS LIST ON PICK.OID=LIST.ORDEROID ");
        cadenaSQL.append(" LEFT JOIN OBJT_TASKORDER AS ORDLIST ON LIST.TASKORDEROID = ORDLIST.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_ITEM AS ART ON LIST.ITEMOID = ART.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_OPERATIONORDERLINK AS oper ON pick.OID = oper.ORDEROID "); 
		cadenaSQL.append(" LEFT JOIN OBJT_PRODUCTIONOPERATION AS prodoper ON oper.OPERATIONOID = prodoper.OID" ); 
		cadenaSQL.append(" LEFT JOIN OBJT_PRODUCTIONORDER AS prodorder ON prodoper.PRODUCTIONORDEROID = prodorder.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_ORDERLINK AS ORD ON PICK.OID = ORD.CHILDOID ");
        cadenaSQL.append(" LEFT JOIN OBJT_OUTBOUNDORDER AS CAB ON ORD.PARENTOID=CAB.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS UBIC ON LIST.SOURCELOCATIONOID = UBIC.OID ");
        cadenaSQL.append(" LEFT JOIN OBJT_CONTAINERSTORAGEITEMQTY AS PALET ON LIST.CONTAINERSTORAGEITEMQTYOID = PALET.OID ");
        
        cadenaSQL.append(" where prodorder.NAME in('");
        for (int i = 0; i< r_listaOrdenes.size();i++)
        {
        	if (i==0)
        		cadenaSQL.append(r_listaOrdenes.get(i));
        	else
        		cadenaSQL.append("','" + r_listaOrdenes.get(i));
        }
        cadenaSQL.append("') ");
        
		cadenaSQL.append(" and LIST.STATUS = '2' ");
		cadenaSQL.append(" and prodorder.NAME is not null ");
			
		try
		{
			/*
			 * conectamos con SQL real
			 * Buscamos el pedido y nos lo traemos para insertar los datos a imprimir en el mysql
			 * 
			 * tablaNueva a crear: pedidosSGA
			 * tablaNueva a crear: lineasPedidosSGA
			 * 
			 * posteriormente generaremos el informe de pedido de venta partiendo de estos datos y esta nueva tabla
			 * 
			 */
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			System.out.println(cadenaSQL.toString());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			consultaArticulosServer cas = consultaArticulosServer.getInstance(CurrentUser.get());
			
			vector = new ArrayList<MapeoAyudaProduccionSGA>();
			
			while(rsOpcion.next())
			{
				MapeoAyudaProduccionSGA mapeo = new MapeoAyudaProduccionSGA();
				mapeo.setOrden(rsOpcion.getString("idOrden"));
				mapeo.setPick(rsOpcion.getString("pickList"));
				mapeo.setArticulo(rsOpcion.getString("articulo"));
				mapeo.setDescripcion(rsOpcion.getString("descripcion"));
				mapeo.setGtin(rsOpcion.getString("gtin"));
				mapeo.setLpn(rsOpcion.getString("lpn"));
				mapeo.setCantidad(rsOpcion.getInt("qty"));
				mapeo.setUbicacion(rsOpcion.getString("ubic"));
				mapeo.setPt(cas.obtenerDescripcionArticulo(r_listaArticulos.get(r_listaOrdenes.indexOf(mapeo.getOrden()))));
				vector.add(mapeo);
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	private ArrayList<MapeoAyudaProduccionSGA> traerDatosSGA(ArrayList<String> r_listaOrdenes, ArrayList<String> r_listaArticulos, String r_lote)
	{
		ArrayList<MapeoAyudaProduccionSGA> vector = null;
		ArrayList<String> materiales = null;
		ArrayList<String> jaulones = null;
		
		Connection con = null;	
		Statement cs = null;
		
		ResultSet rsOpcion = null;		
		StringBuffer cadenaSQL = new StringBuffer();
		StringBuffer cadenaSQL2 = new StringBuffer();
		StringBuffer cadenaWhereMateriales = new StringBuffer();
		StringBuffer cadenaWhereJaulones = new StringBuffer();
		
		cadenaSQL.append(" SELECT DISTINCT T1.FULLNAME AS se_ubi, ");
 		cadenaSQL.append(" t3.ITEM_NAME as se_art, ");
 		cadenaSQL.append(" T3.ITEM_DESCRIPTION AS se_des, ");
 		cadenaSQL.append(" T3.LOT AS se_lot, ");
 		cadenaSQL.append(" T3.LPN AS se_lpn, ");
 		cadenaSQL.append(" T3.VALUE AS se_ex ");	    
 		
 		cadenaSQL.append(" FROM OBJT_RESOURCELINK AS T0 ");
 		cadenaSQL.append(" LEFT JOIN OBJT_WAREHOUSELOCATION AS T1 ON T0.CHILDOID = T1.OID");
 		cadenaSQL.append(" LEFT JOIN DCEREPORT_INVENTORY AS T3 ON T1.FULLNAME = T3.LOCATION");
// 		cadenaSQL.append(" WHERE PARENTCLASSNAME = 'objt.wms.bo.inventorymgt.Zone' ");
 		cadenaSQL.append(" WHERE  ");
 		
 		materiales = new ArrayList<String>();
 		jaulones = new ArrayList<String>();
 		
		for (int i = 0; i< r_listaArticulos.size();i++)
		{
	    	consultaAyudaProduccionServer caps = new consultaAyudaProduccionServer(CurrentUser.get());
	    	ArrayList<String> r_vector2=caps.datosArticulosEscandallo(r_listaArticulos.get(i));
			for (int j = 0; j< r_vector2.size();j++)
			{
				if (r_vector2.get(j).trim().startsWith("0102"))
				{
					if (!jaulones.contains(r_vector2.get(j).trim()))
					{
						jaulones.add(r_vector2.get(j).trim());
					}
				}
				else
				{
					if (!materiales.contains(r_vector2.get(j).trim()))
					{
						materiales.add(r_vector2.get(j).trim());
					}
				}
			}
		}	    	

		
		cadenaWhereMateriales.append(" t3.ITEM_NAME in ('");

    	if (jaulones!=null && jaulones.size()>0)
    	{
    		cadenaSQL2.append(cadenaSQL.toString());
    	}
    	
    	for (int i = 0; i< materiales.size();i++)
    	{
			if (i==0)
				cadenaWhereMateriales.append(materiales.get(i));
			else
				cadenaWhereMateriales.append("','" + materiales.get(i));
		}
    	cadenaWhereMateriales.append("') ");

	    cadenaSQL.append(cadenaWhereMateriales.toString());
	    
	    cadenaSQL.append(" and T1.FULLNAME not like 'PT.S.99' ");
	    cadenaSQL.append(" and T1.FULLNAME not like 'PT.S.95' ");
	    cadenaSQL.append(" and T1.FULLNAME not like 'PS.OUT%' ");
	    cadenaSQL.append(" and T1.FULLNAME not like 'PS.NO C%' ");
		cadenaSQL.append(" ORDER BY t3.ITEM_NAME asc, T3.LPN ASC ");
		
		try
		{
			/*
			 * conectamos con SQL real
			 * Buscamos el pedido y nos lo traemos para insertar los datos a imprimir en el mysql
			 * 
			 */
			con= this.conManager.establecerConexionSGAInd();
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			System.out.println(cadenaSQL.toString());
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			
			vector = new ArrayList<MapeoAyudaProduccionSGA>();
			
			while(rsOpcion.next())
			{
				MapeoAyudaProduccionSGA mapeo = new MapeoAyudaProduccionSGA();
				
				mapeo.setOrden(null);
				mapeo.setPick(null);
				mapeo.setArticulo(rsOpcion.getString("se_art"));
				mapeo.setDescripcion(rsOpcion.getString("se_des"));
				mapeo.setGtin(null);
				mapeo.setLpn(rsOpcion.getString("se_lpn"));
				mapeo.setCantidad(rsOpcion.getInt("se_ex"));
				mapeo.setUbicacion(rsOpcion.getString("se_ubi"));
				mapeo.setPt(null);
				if (rsOpcion.getString("se_lpn")!=null) vector.add(mapeo);
			}
			

	    	if (jaulones!=null && jaulones.size()>0)
	    	{
	    		cadenaWhereJaulones.append(" t3.ITEM_NAME in ('");
				
		    	for (int i = 0; i< jaulones.size();i++)
		    	{
					if (i==0)
						cadenaWhereJaulones.append(jaulones.get(i));
					else
						cadenaWhereJaulones.append("','" + jaulones.get(i));
				}
		    	cadenaWhereJaulones.append("') ");
		    	
		    	
		    	if (r_lote.contains(","))
		    	{
			    	String lotes[] = r_lote.split(",");
			    	for (int i = 0; i< lotes.length;i++)
			    	{
			    		if (i==0)
			    			cadenaWhereJaulones.append(" and T3.lot in ('" + lotes[i] + "' ");
			    		else
			    			cadenaWhereJaulones.append(" ,'" + lotes[i] + "'");
			    	}
			    	cadenaWhereJaulones.append(") ");
		    	}
		    	else
		    	{
		    		cadenaWhereJaulones.append(" and T3.lot in ('" + r_lote + "') ");		
		    	}
		    	
		    	cadenaSQL2.append(cadenaWhereJaulones.toString());
		    	cadenaSQL2.append(" ORDER BY t3.ITEM_NAME asc, T3.LOT, T3.LPN ASC ");
		    	
		    	System.out.println(cadenaSQL2.toString());
				rsOpcion= cs.executeQuery(cadenaSQL2.toString());
				
//				vector = new ArrayList<MapeoAyudaProduccionSGA>();
				
				while(rsOpcion.next())
				{
					MapeoAyudaProduccionSGA mapeo = new MapeoAyudaProduccionSGA();
					
					mapeo.setOrden(null);
					mapeo.setPick(null);
					mapeo.setArticulo(rsOpcion.getString("se_art"));
					mapeo.setDescripcion(rsOpcion.getString("se_des"));
					mapeo.setGtin(null);
					mapeo.setLpn(rsOpcion.getString("se_lpn"));
					mapeo.setCantidad(rsOpcion.getInt("se_ex"));
					mapeo.setUbicacion(rsOpcion.getString("se_ubi"));
					mapeo.setPt(null);
					mapeo.setLote(rsOpcion.getString("se_lot"));
					if (rsOpcion.getString("se_lpn")!=null) vector.add(mapeo);
				}
			
	    	}
		
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		return vector;
	}
	
	public String imprimirExplosionSGA(ArrayList<String> r_listaOrdenes,ArrayList<String> r_listaArticulos, String r_lote)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		
		try
		{
			this.guardarDatosExplosionSGA(r_listaOrdenes,r_listaArticulos, r_lote);
			/*
			 * si no salta excepcion generamos el fichero
			 */
			libImpresion = new LibreriaImpresion();
			
			libImpresion.setArchivoDefinitivo("/explosionSGA" + RutinasFechas.horaActualSinSeparador() + ".pdf");
			
			libImpresion.setArchivoPlantilla("explosionMaterialesSGA.jrxml");
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlancoH.jpg");
			
			libImpresion.setArchivoPlantillaDetalle(null);
			libImpresion.setArchivoPlantillaDetalleCompilado(null);
			libImpresion.setArchivoPlantillaNotas(null);
			libImpresion.setArchivoPlantillaNotasCompilado(null);
			libImpresion.setCarpeta("programacion");
			
			resultadoGeneracion = libImpresion.generacionInformeTxtXML();
			
			if (resultadoGeneracion == null)
				resultadoGeneracion = libImpresion.getArchivoDefinitivo();
			
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());
		} 
		
		return resultadoGeneracion;
	}
	public String imprimirExplosion(ArrayList<MapeoAyudaProduccionEscandallo> r_vector)
	{
		String resultadoGeneracion = null;
		LibreriaImpresion libImpresion = null;
		
		try
		{
			this.guardarDatosExplosion(r_vector);
			/*
			 * si no salta excepcion generamos el fichero
			 */
			libImpresion = new LibreriaImpresion();

			libImpresion.setArchivoDefinitivo("/explosion" + RutinasFechas.horaActualSinSeparador() + ".pdf");
			
			libImpresion.setArchivoPlantilla("explosionMateriales.jrxml");
			libImpresion.setBackGroundEtiqueta("/fondoA4LogoBlancoH.jpg");
			
			libImpresion.setArchivoPlantillaDetalle(null);
			libImpresion.setArchivoPlantillaDetalleCompilado(null);
			libImpresion.setArchivoPlantillaNotas(null);
			libImpresion.setArchivoPlantillaNotasCompilado(null);
			libImpresion.setCarpeta("programacion");

			resultadoGeneracion = libImpresion.generacionInformeTxtXML();

			if (resultadoGeneracion == null)
				resultadoGeneracion = libImpresion.getArchivoDefinitivo();

		}
	    catch (Exception ex)
	    {
	    	serNotif.mensajeError(ex.getMessage());
	    } 

		return resultadoGeneracion;
	}
	
	public String semaforos() 
	{
		ResultSet rsOpcion = null;
		String errores = "0";
		StringBuffer cadenaSQL =null;
		Connection con = null;
		Statement cs = null;
		
		try
		{
			cadenaSQL = new StringBuffer();
			
			cadenaSQL.append(" SELECT count(*) mer_num ");
	        
	     	cadenaSQL.append(" FROM a__oml_0 a ");
	     	cadenaSQL.append(" where a.estado = 2 ");
	     	cadenaSQL.append(" and a.fecha < '" + RutinasFechas.fechaActual() + "' ");
			
			con= this.conManager.establecerConexionGestionInd();			
			cs = con.createStatement(rsOpcion.TYPE_FORWARD_ONLY,rsOpcion.CONCUR_READ_ONLY);
			rsOpcion= cs.executeQuery(cadenaSQL.toString());
			Notificaciones.getInstance().mensajeSeguimiento("SQL ejecutado " + cadenaSQL.toString() + " : " + new Date());
			
			while(rsOpcion.next())
			{
				errores = "2";					
			}
		}
		catch (Exception ex)
		{
			serNotif.mensajeError(ex.getMessage());			
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (rsOpcion!= null)
				{
					rsOpcion.close();
					rsOpcion=null;
				}
				if (cs !=null)
				{
					cs.close();
					cs=null;
				}
				if (con!=null)
				{
					con.close();
					con=null;
				}
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}

		return errores ;
	}
}
